package com.imocha;

import MobWin.cnst.PROTOCOL_ENCODING;
import android.content.Context;
import android.location.Location;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

final class ai {
    private static String e = null;
    private WeakReference a;
    private String b;
    private p c = new p();
    private am d = null;

    ai(IMochaAdView iMochaAdView) {
        this.a = new WeakReference(iMochaAdView);
        this.b = new StringBuilder().append(hashCode()).toString();
    }

    private int a(String str) {
        IMochaAdView iMochaAdView = (IMochaAdView) this.a.get();
        if (iMochaAdView == null) {
            return 2;
        }
        ar arVar = new ar(iMochaAdView.getContext());
        try {
            SAXParserFactory.newInstance().newSAXParser().parse(new InputSource(new ByteArrayInputStream(str.getBytes())), arVar);
            if (arVar.a != null) {
                this.d = arVar.a;
            }
            return 6;
        } catch (SAXException e2) {
            e2.printStackTrace();
            arVar.a = null;
            return 4;
        } catch (IOException e3) {
            e3.printStackTrace();
            arVar.a = null;
            return 4;
        } catch (ParserConfigurationException e4) {
            arVar.a = null;
            e4.printStackTrace();
            return 4;
        } catch (FactoryConfigurationError e5) {
            arVar.a = null;
            e5.printStackTrace();
            return 4;
        }
    }

    private String a(Context context) {
        byte[] bArr;
        StringBuffer stringBuffer = new StringBuffer();
        IMochaAdView iMochaAdView = (IMochaAdView) this.a.get();
        if (iMochaAdView == null) {
            return null;
        }
        try {
            stringBuffer.append("pid=" + iMochaAdView.e);
            stringBuffer.append("&guid=" + af.a().d(iMochaAdView.getContext()));
            stringBuffer.append("&os=1");
            String b2 = af.b();
            try {
                if (b2.length() > 20) {
                    b2 = b2.substring(0, 20);
                }
                stringBuffer.append("&osv=" + URLEncoder.encode(new String(b2.getBytes(), PROTOCOL_ENCODING.value), PROTOCOL_ENCODING.value));
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            }
            stringBuffer.append("&sdkv=A2.352");
            stringBuffer.append("&scrn-w=" + af.a().e(iMochaAdView.getContext()));
            stringBuffer.append("&scrn-h=" + af.a().f(iMochaAdView.getContext()));
            if (iMochaAdView.k != null) {
                stringBuffer.append("&sub-id=" + iMochaAdView.k);
            }
            stringBuffer.append("&funcs=v");
            stringBuffer.append("t");
            stringBuffer.append("g");
            if (SmsManager.getDefault() != null) {
                stringBuffer.append("s");
            }
            if (((TelephonyManager) context.getSystemService("phone")) != null) {
                stringBuffer.append("c");
            }
            stringBuffer.append(af.c(context) ? "w" : "");
            Location b3 = af.a().b(iMochaAdView.getContext());
            if (e != null) {
                stringBuffer.append("&gps=" + e);
            } else if (b3 != null) {
                double longitude = b3.getLongitude();
                double latitude = b3.getLatitude();
                DecimalFormat decimalFormat = new DecimalFormat("######0.0000000000");
                stringBuffer.append("&gps=" + decimalFormat.format(longitude) + "," + decimalFormat.format(latitude));
            }
            byte[] bytes = af.c().getBytes();
            if (bytes.length > 32) {
                byte[] bArr2 = new byte[32];
                System.arraycopy(bytes, 0, bArr2, 0, bArr2.length);
                bArr = bArr2;
            } else {
                byte[] bArr3 = new byte[bytes.length];
                System.arraycopy(bytes, 0, bArr3, 0, bArr3.length);
                bArr = bArr3;
            }
            stringBuffer.append("&ua=" + URLEncoder.encode(new String(bArr, PROTOCOL_ENCODING.value), PROTOCOL_ENCODING.value));
            stringBuffer.append("&imei=" + URLEncoder.encode(this.c.a(new StringBuilder().append(System.currentTimeMillis()).toString().getBytes()), PROTOCOL_ENCODING.value));
        } catch (Exception e3) {
            e3.printStackTrace();
        } catch (Throwable th) {
        }
        return stringBuffer.toString();
    }

    private int b() {
        IMochaAdView iMochaAdView = (IMochaAdView) this.a.get();
        if (iMochaAdView == null) {
            return 0;
        }
        IMochaAdView.onEvent(iMochaAdView, IMochaEventCode.downloadRes);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        String b2 = this.d.b();
        if (!b2.endsWith(".html.zip") && !b2.endsWith(".gif")) {
            return 2;
        }
        HttpGet httpGet = new HttpGet(b2);
        if (this.d.k == m.Video && !af.c(iMochaAdView.getContext())) {
            return 7;
        }
        try {
            HttpResponse execute = defaultHttpClient.execute(new HttpHead(this.d.b()));
            if (execute.getStatusLine().getStatusCode() != 200) {
                return 2;
            }
            Header[] headers = execute.getHeaders("Content-Length");
            if (headers == null || headers.length <= 0) {
                return 2;
            }
            if (!ak.a(Long.parseLong(headers[0].getValue()))) {
                return 8;
            }
            HttpResponse execute2 = defaultHttpClient.execute(httpGet);
            if (execute2.getStatusLine().getStatusCode() != 200) {
                return 2;
            }
            HttpEntity entity = execute2.getEntity();
            if (entity == null) {
                return 2;
            }
            byte[] byteArray = EntityUtils.toByteArray(entity);
            if (byteArray.length <= 2) {
                return 1;
            }
            this.d.a(byteArray);
            if (this.d.b(iMochaAdView)) {
                return 6;
            }
            this.d = null;
            return 9;
        } catch (Exception e2) {
            e2.printStackTrace();
            return 3;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00e9 A[Catch:{ Exception -> 0x00fb }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int a() {
        /*
            r9 = this;
            r2 = 60000(0xea60, float:8.4078E-41)
            r8 = 6
            r7 = 5
            r6 = 1
            r5 = 2
            java.lang.ref.WeakReference r0 = r9.a
            java.lang.Object r0 = r0.get()
            com.imocha.IMochaAdView r0 = (com.imocha.IMochaAdView) r0
            if (r0 != 0) goto L_0x0013
            r0 = 0
        L_0x0012:
            return r0
        L_0x0013:
            org.apache.http.params.BasicHttpParams r1 = new org.apache.http.params.BasicHttpParams
            r1.<init>()
            org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r1, r2)
            org.apache.http.params.HttpConnectionParams.setSoTimeout(r1, r2)
            org.apache.http.client.params.HttpClientParams.setRedirecting(r1, r6)
            org.apache.http.impl.client.DefaultHttpClient r2 = new org.apache.http.impl.client.DefaultHttpClient
            r2.<init>(r1)
            android.content.Context r1 = r0.getContext()
            java.lang.String r1 = r9.a(r1)
            if (r1 != 0) goto L_0x0032
            r0 = r5
            goto L_0x0012
        L_0x0032:
            boolean r3 = r0.h
            if (r3 == 0) goto L_0x0070
            int r3 = r0.i
            if (r3 != r6) goto L_0x0060
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "http://labs.ra.icast.cn/imocha/ads/?"
            r3.<init>(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
        L_0x0049:
            org.apache.http.client.methods.HttpGet r3 = new org.apache.http.client.methods.HttpGet
            r3.<init>(r1)
            org.apache.http.HttpResponse r1 = r2.execute(r3)     // Catch:{ Exception -> 0x00fb }
            org.apache.http.StatusLine r2 = r1.getStatusLine()     // Catch:{ Exception -> 0x00fb }
            int r2 = r2.getStatusCode()     // Catch:{ Exception -> 0x00fb }
            r3 = 200(0xc8, float:2.8E-43)
            if (r2 == r3) goto L_0x0080
            r0 = r5
            goto L_0x0012
        L_0x0060:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "http://122.225.32.34:8088/p/?"
            r3.<init>(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            goto L_0x0049
        L_0x0070:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r4 = "http://cast.ra.imocha.com/p/?"
            r3.<init>(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            goto L_0x0049
        L_0x0080:
            org.apache.http.HttpEntity r1 = r1.getEntity()     // Catch:{ Exception -> 0x00fb }
            if (r1 == 0) goto L_0x00f8
            java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x00fb }
            byte[] r3 = org.apache.http.util.EntityUtils.toByteArray(r1)     // Catch:{ Exception -> 0x00fb }
            r2.<init>(r3)     // Catch:{ Exception -> 0x00fb }
            r1.consumeContent()     // Catch:{ Exception -> 0x00fb }
            r2.trim()     // Catch:{ Exception -> 0x00fb }
            int r1 = r2.length()     // Catch:{ Exception -> 0x00fb }
            if (r1 <= r5) goto L_0x00f5
            int r2 = r9.a(r2)     // Catch:{ Exception -> 0x00fb }
            if (r2 != r8) goto L_0x0102
            com.imocha.IMochaAdType r1 = r0.j     // Catch:{ Exception -> 0x00fb }
            com.imocha.IMochaAdType r3 = com.imocha.IMochaAdType.BANNER     // Catch:{ Exception -> 0x00fb }
            if (r1 != r3) goto L_0x00be
            com.imocha.am r1 = r9.d     // Catch:{ Exception -> 0x00fb }
            java.util.Hashtable r1 = r1.g     // Catch:{ Exception -> 0x00fb }
            java.lang.String r3 = "type"
            java.lang.Object r1 = r1.get(r3)     // Catch:{ Exception -> 0x00fb }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x00fb }
            java.lang.String r3 = "banner"
            boolean r1 = r1.equals(r3)     // Catch:{ Exception -> 0x00fb }
            if (r1 != 0) goto L_0x00d5
            r0 = r7
            goto L_0x0012
        L_0x00be:
            com.imocha.am r1 = r9.d     // Catch:{ Exception -> 0x00fb }
            java.util.Hashtable r1 = r1.g     // Catch:{ Exception -> 0x00fb }
            java.lang.String r3 = "type"
            java.lang.Object r1 = r1.get(r3)     // Catch:{ Exception -> 0x00fb }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x00fb }
            java.lang.String r3 = "banner"
            boolean r1 = r1.equals(r3)     // Catch:{ Exception -> 0x00fb }
            if (r1 == 0) goto L_0x00d5
            r0 = r7
            goto L_0x0012
        L_0x00d5:
            com.imocha.am r1 = r9.d     // Catch:{ Exception -> 0x00fb }
            java.lang.String r3 = r0.e     // Catch:{ Exception -> 0x00fb }
            r1.j = r3     // Catch:{ Exception -> 0x00fb }
            com.imocha.am r1 = r9.d     // Catch:{ Exception -> 0x00fb }
            boolean r1 = r1.a(r0)     // Catch:{ Exception -> 0x00fb }
            if (r1 != 0) goto L_0x0102
            int r1 = r9.b()     // Catch:{ Exception -> 0x00fb }
        L_0x00e7:
            if (r1 != r8) goto L_0x00f2
            java.util.HashMap r2 = com.imocha.IMochaAdView.f     // Catch:{ Exception -> 0x00fb }
            java.lang.String r0 = r0.e     // Catch:{ Exception -> 0x00fb }
            com.imocha.am r3 = r9.d     // Catch:{ Exception -> 0x00fb }
            r2.put(r0, r3)     // Catch:{ Exception -> 0x00fb }
        L_0x00f2:
            r0 = r1
            goto L_0x0012
        L_0x00f5:
            r0 = r6
            goto L_0x0012
        L_0x00f8:
            r0 = r5
            goto L_0x0012
        L_0x00fb:
            r0 = move-exception
            r0.printStackTrace()
            r0 = 3
            goto L_0x0012
        L_0x0102:
            r1 = r2
            goto L_0x00e7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.imocha.ai.a():int");
    }
}
