package com.imocha;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;

final class as extends View {
    private /* synthetic */ FullScreenAdActivity a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    as(FullScreenAdActivity fullScreenAdActivity, Context context) {
        super(context);
        this.a = fullScreenAdActivity;
    }

    public final void draw(Canvas canvas) {
        super.draw(canvas);
        Paint paint = new Paint(1);
        paint.setTextSize(18.0f);
        paint.setColor(this.a.m);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.a.n.size()) {
                canvas.drawText((String) this.a.n.get(i2), 3.0f, paint.getTextSize() + (((float) i2) * (paint.getTextSize() + 3.0f)), paint);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
