package com.imocha;

public enum IMochaAdType {
    BANNER("banner"),
    FULLSCREEN("IMAGE AND VIDEO AD");
    
    private String a;

    private IMochaAdType(String str) {
        this.a = str;
    }

    public final String getValue() {
        return this.a;
    }
}
