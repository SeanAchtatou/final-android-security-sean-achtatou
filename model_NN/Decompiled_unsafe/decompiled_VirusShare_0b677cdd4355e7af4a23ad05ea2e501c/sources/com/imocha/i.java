package com.imocha;

import android.util.Log;
import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

final class i implements Runnable {
    private String a;

    public i(String str) {
        this.a = str;
    }

    public final void run() {
        try {
            HttpResponse execute = new DefaultHttpClient().execute(new HttpGet(this.a));
            System.out.println(execute.getStatusLine() + " -->" + this.a + "  statusCode-->" + execute.getStatusLine().getStatusCode());
        } catch (ClientProtocolException e) {
            Log.e("iMocha SDK", "Caught ClientProtocolException in PingUrlRunnable", e);
        } catch (IOException e2) {
            Log.e("iMocha SDK", "Caught IOException in PingUrlRunnable", e2);
        }
    }
}
