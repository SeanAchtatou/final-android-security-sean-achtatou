package com.imocha;

import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

final class o extends WebChromeClient {
    private /* synthetic */ a a;

    /* synthetic */ o(a aVar) {
        this(aVar, (byte) 0);
    }

    private o(a aVar, byte b) {
        this.a = aVar;
    }

    public final void onProgressChanged(WebView webView, int i) {
        Log.v("MyWebView", "WebChromeClient onCloseWindow");
        if (this.a.b != null) {
            this.a.b.a(i);
        }
        super.onProgressChanged(webView, i);
    }

    public final void onReceivedTitle(WebView webView, String str) {
        Log.v("MyWebView", "WebChromeClient onCloseWindow");
        if (this.a.b != null) {
            this.a.b.a(str);
        }
        super.onReceivedTitle(webView, str);
    }
}
