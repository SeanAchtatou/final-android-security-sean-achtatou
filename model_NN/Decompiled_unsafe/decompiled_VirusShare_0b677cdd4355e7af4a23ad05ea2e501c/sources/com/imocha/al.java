package com.imocha;

import android.content.Context;
import android.view.MotionEvent;

final class al extends a {
    private /* synthetic */ FullScreenAdActivity d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    al(FullScreenAdActivity fullScreenAdActivity, Context context, am amVar) {
        super(context, null, amVar, 1);
        this.d = fullScreenAdActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.imocha.FullScreenAdActivity.a(com.imocha.FullScreenAdActivity, boolean):void
     arg types: [com.imocha.FullScreenAdActivity, int]
     candidates:
      com.imocha.FullScreenAdActivity.a(android.os.Bundle, java.lang.String):void
      com.imocha.FullScreenAdActivity.a(com.imocha.FullScreenAdActivity, boolean):void */
    public final boolean onTouchEvent(MotionEvent motionEvent) {
        boolean onTouchEvent = super.onTouchEvent(motionEvent);
        if (this.d.a.b.getClass().getName().equals(ae.class.getName())) {
            this.d.g = onTouchEvent;
        } else {
            this.d.g = true;
        }
        return true;
    }
}
