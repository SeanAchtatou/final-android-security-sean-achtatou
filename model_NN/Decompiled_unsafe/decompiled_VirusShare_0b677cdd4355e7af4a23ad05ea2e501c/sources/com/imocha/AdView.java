package com.imocha;

public interface AdView {
    public static final int FORM_MODE = 0;
    public static final int INSIDE_TEST_MODE = 1;
    public static final int PUBLIC_TEST_MODE = 2;
    public static final int REQUEST_CODE = 16711697;
    public static final int RESULT_CODE = 16711698;

    void downloadAd();

    void playFullscreenAd();

    void setAppId(String str);

    void setCacheSize(int i);

    void setIMochaAdListener(IMochaAdListener iMochaAdListener);

    void setRefresh(boolean z);

    void setTestMode(boolean z);

    void setUpdateTime(long j);
}
