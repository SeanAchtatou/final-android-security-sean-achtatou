package com.android.mmreader1062;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.baidu.AdType;
import com.baidu.AdView;
import com.baidu.AdViewListener;
import com.baidu.FailReason;
import com.imocha.IMochaAdListener;
import com.imocha.IMochaAdType;
import com.imocha.IMochaAdView;
import com.imocha.IMochaEventCode;
import com.izp.views.IZPView;
import com.mobisage.android.MobiSageAdBanner;
import com.mobisage.android.MobiSageAdSize;
import com.taobao.munion.ads.clientSDK.TaoAds;
import com.taobao.munion.ads.clientSDK.TaoAdsListener;
import com.tencent.mobwin.core.m;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Timer;
import java.util.TimerTask;

public class chartact extends Activity implements View.OnClickListener {
    public static final int EXIT_ID = 23;
    public static final int GOOD_ID = 22;
    static final int RG_REQUESTCON = 1;
    public static final int START_ID = 21;
    private int Activityadsheight;
    /* access modifiers changed from: private */
    public int Adsid;
    private int Adsid1;
    /* access modifiers changed from: private */
    public int MAXBOOKNUMS = m.a;
    private final int WC = -2;
    /* access modifiers changed from: private */
    public Handler adshandler = new Handler() {
        public void handleMessage(Message msg) {
            chartact.this.Adsid = chartact.this.appCell.readcuradsid();
            boolean access$8 = chartact.this.viewAds(chartact.this.Adsid);
            super.handleMessage(msg);
        }
    };
    private TimerTask adstask = new TimerTask() {
        public void run() {
            Message msg = new Message();
            msg.arg1 = 1;
            chartact.this.adshandler.sendMessage(msg);
        }
    };
    private final Timer adstimer = new Timer();
    /* access modifiers changed from: private */
    public appcell appCell;
    AdView baiduadView = null;
    private String chartidstr = "";
    /* access modifiers changed from: private */
    public int curChartid = 1;
    private int displayHeight;
    private int displayWidth;
    private GestureDetector gestureDetector = null;
    IMochaAdView imochaView = null;
    private int isexit = 0;
    IZPView izpView = null;
    private View.OnKeyListener mChangeChartKeyListener = new View.OnKeyListener() {
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            View viewToFoucus = null;
            if (event.getAction() == 0) {
                switch (keyCode) {
                    case 19:
                        if (chartact.this.curChartid != chartact.this.MAXBOOKNUMS) {
                            if (chartact.this.curChartid <= 1) {
                                if (chartact.this.curChartid == 1) {
                                    chartact.this.curChartid = chartact.this.MAXBOOKNUMS;
                                    viewToFoucus = chartact.this.findViewById(chartact.this.curChartid);
                                    break;
                                }
                            } else {
                                chartact chartact = chartact.this;
                                chartact.curChartid = chartact.curChartid - 1;
                                viewToFoucus = chartact.this.findViewById(chartact.this.curChartid);
                                break;
                            }
                        } else {
                            chartact.this.curChartid = chartact.this.totalchartnums;
                            viewToFoucus = chartact.this.findViewById(chartact.this.curChartid);
                            break;
                        }
                        break;
                    case MobiSageAdSize.Poster_480X800 /*20*/:
                        if (chartact.this.curChartid != chartact.this.MAXBOOKNUMS) {
                            if (chartact.this.curChartid != chartact.this.totalchartnums) {
                                if (chartact.this.curChartid < chartact.this.totalchartnums) {
                                    chartact chartact2 = chartact.this;
                                    chartact2.curChartid = chartact2.curChartid + 1;
                                    viewToFoucus = chartact.this.findViewById(chartact.this.curChartid);
                                    break;
                                }
                            } else {
                                chartact.this.curChartid = chartact.this.MAXBOOKNUMS;
                                viewToFoucus = chartact.this.findViewById(chartact.this.curChartid);
                                break;
                            }
                        } else {
                            chartact.this.curChartid = 1;
                            viewToFoucus = chartact.this.findViewById(chartact.this.curChartid);
                            break;
                        }
                        break;
                }
            }
            if (viewToFoucus == null) {
                return false;
            }
            viewToFoucus.requestFocus();
            return true;
        }
    };
    private ScrollView mScrollView;
    private LinearLayout mainlayout;
    MobiSageAdBanner mobiView = null;
    com.tencent.mobwin.AdView mobwinadView = null;
    /* access modifiers changed from: private */
    public int myid;
    private int readedidx = 0;
    private TableLayout tablelayout;
    TaoAds taobaoadView = null;
    RelativeLayout taobaocontainer = null;
    /* access modifiers changed from: private */
    public int totalchartnums;
    /* access modifiers changed from: private */
    public Handler updatehandler = new Handler() {
        public void handleMessage(Message msg) {
            if (chartact.this.appCell.getoldversionid() < chartact.this.appCell.getversionid()) {
                int updatetype = chartact.this.appCell.getupdatetype();
                String updatetip = chartact.this.appCell.getupdatetip();
                if (updatetype == 1) {
                    new AlertDialog.Builder(chartact.this).setTitle("搜象文学提醒你").setMessage(updatetip).setPositiveButton("立刻下载", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            String imsi = chartact.this.appCell.getimsi();
                            String imei = chartact.this.appCell.getimei();
                            String tel = chartact.this.appCell.getitel();
                            int width = chartact.this.appCell.getdisplayWidth();
                            int height = chartact.this.appCell.getdisplayHeight();
                            Intent intent1 = new Intent("android.intent.action.VIEW");
                            intent1.setData(Uri.parse("http://www.welovemm.cn/phonedownload4.php?os=1&vs=31&xmlid=" + chartact.this.myid + "&" + "type=1" + "&" + "tel=" + tel + "&" + "imei=" + imei + "&" + "imsi=" + imsi + "&" + "width=" + String.valueOf(width) + "&" + "height=" + String.valueOf(height) + "&" + "session=" + chartact.this.appCell.getsession()));
                            chartact.this.startActivity(intent1);
                            chartact.this.Terminal(1);
                        }
                    }).setNeutralButton("以后下载", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.cancel();
                        }
                    }).create().show();
                } else if (updatetype == 2) {
                    new AlertDialog.Builder(chartact.this).setTitle("搜象文学提醒你").setMessage(updatetip).setPositiveButton("立刻下载", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            String imsi = chartact.this.appCell.getimsi();
                            String imei = chartact.this.appCell.getimei();
                            String tel = chartact.this.appCell.getitel();
                            int width = chartact.this.appCell.getdisplayWidth();
                            int height = chartact.this.appCell.getdisplayHeight();
                            Intent intent1 = new Intent("android.intent.action.VIEW");
                            intent1.setData(Uri.parse("http://www.welovemm.cn/phonedownload4.php?os=1&vs=31&xmlid=" + chartact.this.myid + "&" + "type=2" + "&" + "tel=" + tel + "&" + "imei=" + imei + "&" + "imsi=" + imsi + "&" + "width=" + String.valueOf(width) + "&" + "height=" + String.valueOf(height) + "&" + "session=" + chartact.this.appCell.getsession()));
                            chartact.this.startActivity(intent1);
                            chartact.this.Terminal(1);
                        }
                    }).create().show();
                }
            }
            super.handleMessage(msg);
        }
    };
    private TimerTask updatetask = new TimerTask() {
        public void run() {
            Message msg = new Message();
            msg.arg1 = 1;
            chartact.this.updatehandler.sendMessage(msg);
        }
    };
    private final Timer updatetimer = new Timer();

    public void onCreate(Bundle savedInstanceState) {
        int btnmargin;
        super.onCreate(savedInstanceState);
        this.mainlayout = new LinearLayout(this);
        setContentView(this.mainlayout);
        this.mainlayout.setOrientation(1);
        this.mainlayout.setGravity(1);
        this.mainlayout.setPadding(0, 2, 0, 0);
        this.mainlayout.setBackgroundResource(R.drawable.bg);
        this.appCell = (appcell) getApplication();
        Display dm = getWindowManager().getDefaultDisplay();
        this.displayHeight = dm.getHeight();
        this.displayWidth = dm.getWidth();
        this.mScrollView = new ScrollView(this);
        this.mScrollView.setScrollContainer(true);
        this.mScrollView.setFocusable(true);
        this.mScrollView.setScrollBarStyle(1);
        this.mScrollView.setClickable(true);
        this.mScrollView.setPadding(0, 3, 0, 0);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -2);
        this.Activityadsheight = 100;
        if (this.displayHeight > 1150) {
            this.Activityadsheight = 135;
        } else if (this.displayHeight > 854 && this.displayHeight <= 1080) {
            this.Activityadsheight = 100;
        } else if (this.displayHeight >= 500 && this.displayHeight <= 854) {
            this.Activityadsheight = 85;
        } else if (this.displayHeight > 450 && this.displayHeight < 500) {
            this.Activityadsheight = 85;
        } else if (this.displayHeight < 350 || this.displayHeight > 450) {
            this.Activityadsheight = 44;
        } else {
            this.Activityadsheight = 60;
        }
        this.myid = this.appCell.getmyid();
        this.Adsid = this.appCell.readcuradsid();
        this.Adsid1 = this.appCell.getadsid(1);
        boolean isselfads = false;
        if (this.Adsid1 == 101 || this.Adsid1 == 102) {
            this.Activityadsheight = 1;
        }
        layoutParams.height = this.displayHeight - this.Activityadsheight;
        this.mainlayout.addView(this.mScrollView, layoutParams);
        String adsids = "";
        int i = 1;
        while (true) {
            if (i > 20) {
                break;
            }
            adsids = String.valueOf(this.appCell.getid(i));
            String charts = String.valueOf(this.appCell.getchart(i));
            String heightstrs = String.valueOf(this.appCell.getheight(i));
            if (charts.length() > 0) {
                int chart = -5;
                try {
                    chart = Integer.parseInt(charts);
                } catch (NumberFormatException e) {
                }
                if (chart == 0 || chart == -1) {
                    isselfads = true;
                }
            }
            i++;
        }
        isselfads = true;
        if (isselfads) {
            String adsimgfile = "adsimg" + adsids + ".png";
            if (this.appCell.fileIsExists("/data/data/com.android.mmreader" + this.myid + "/files/adsimg" + adsids + ".png")) {
                int id = 0;
                try {
                    id = Integer.parseInt(adsids) + 1000;
                } catch (NumberFormatException e2) {
                }
                ImageView imageView = new ImageView(this);
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                try {
                    imageView.setImageBitmap(BitmapFactory.decodeStream(openFileInput(adsimgfile)));
                } catch (FileNotFoundException e3) {
                }
                imageView.setOnClickListener(this);
                imageView.setId(id);
                this.Adsid = 1000;
                this.mainlayout.addView(imageView);
            }
        }
        this.tablelayout = new TableLayout(this);
        this.mScrollView.addView(this.tablelayout, new RelativeLayout.LayoutParams(-2, -2));
        this.tablelayout.setGravity(3);
        try {
            InputStream is = getAssets().open("ids.txt");
            int size = is.available();
            if (size > 0 && size < 1048576) {
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();
                this.chartidstr = new String(buffer, "GB2312");
            }
        } catch (IOException e4) {
        }
        try {
            FileInputStream stream = openFileInput("index.txt");
            StringBuffer sb = new StringBuffer();
            while (true) {
                int c = stream.read();
                if (c == -1) {
                    break;
                }
                sb.append((char) c);
            }
            stream.close();
            String content = sb.toString();
            int idx = content.indexOf("\r\n", 0);
            if (idx >= 0) {
                try {
                    this.readedidx = Integer.parseInt(content.substring(0, idx));
                } catch (NumberFormatException e5) {
                }
            }
        } catch (FileNotFoundException | IOException e6) {
        }
        if (this.readedidx <= 0) {
            this.readedidx = 0;
        } else {
            Button button = new Button(this);
            button.setText("上一次阅读内容");
            button.setTextSize(18.0f);
            button.setWidth(this.displayWidth - 10);
            button.setHeight(25);
            button.setTextColor(-16777216);
            button.setGravity(1);
            button.setOnClickListener(this);
            button.setOnKeyListener(this.mChangeChartKeyListener);
            button.setId(this.MAXBOOKNUMS);
            TableLayout.LayoutParams layoutParams2 = new TableLayout.LayoutParams(-2, -2);
            layoutParams2.topMargin = 10;
            TableRow tableRow = new TableRow(this);
            tableRow.setGravity(1);
            this.tablelayout.addView(tableRow, layoutParams2);
            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setGravity(1);
            tableRow.addView(linearLayout);
            linearLayout.addView(button);
        }
        try {
            InputStream is2 = getAssets().open("chart.txt");
            int size2 = is2.available();
            if (size2 > 0 && size2 < 1048576) {
                byte[] buffer2 = new byte[size2];
                is2.read(buffer2);
                is2.close();
                int start = 0;
                String str = new String(buffer2, "GB2312");
                int idx2 = str.indexOf("\r\n", 0);
                int btnid = 1;
                while (idx2 >= 0) {
                    String chartname = str.substring(start, idx2);
                    start = idx2 + 2;
                    idx2 = str.indexOf("\r\n", start);
                    if (chartname != "") {
                        float strwid = new TextView(this).getPaint().measureText(chartname);
                        Button button2 = new Button(this);
                        button2.setText(chartname);
                        button2.setTextSize(18.0f);
                        button2.setWidth(this.displayWidth - 10);
                        if (this.displayHeight > 800) {
                            if (((int) strwid) >= this.displayWidth - 190) {
                                button2.setTextSize(16.0f);
                                button2.setHeight(110);
                            } else {
                                button2.setTextSize(18.0f);
                                button2.setHeight(30);
                            }
                            btnmargin = 10;
                        } else if (this.displayHeight >= 500 && this.displayHeight <= 800) {
                            if (((int) strwid) >= this.displayWidth - j.i) {
                                button2.setTextSize(16.0f);
                                button2.setHeight(85);
                            } else {
                                button2.setTextSize(18.0f);
                                button2.setHeight(30);
                            }
                            btnmargin = 8;
                        } else if (this.displayHeight > 450 && this.displayHeight < 500) {
                            if (((int) strwid) >= this.displayWidth - 100) {
                                button2.setTextSize(16.0f);
                                button2.setHeight(60);
                            } else {
                                button2.setTextSize(18.0f);
                                button2.setHeight(30);
                            }
                            btnmargin = 6;
                        } else if (this.displayHeight < 350 || this.displayHeight > 450) {
                            if (((int) strwid) >= this.displayWidth - 70) {
                                button2.setTextSize(16.0f);
                                button2.setHeight(45);
                            } else {
                                button2.setTextSize(18.0f);
                                button2.setHeight(30);
                            }
                            btnmargin = 4;
                        } else {
                            if (((int) strwid) >= this.displayWidth - 30) {
                                button2.setTextSize(16.0f);
                                button2.setHeight(50);
                            } else {
                                button2.setTextSize(18.0f);
                                button2.setHeight(30);
                            }
                            btnmargin = 5;
                        }
                        button2.setGravity(1);
                        button2.setOnClickListener(this);
                        button2.setOnKeyListener(this.mChangeChartKeyListener);
                        String tmps = "," + btnid + ",";
                        if (this.chartidstr.indexOf(",b,", 0) < 0) {
                            button2.setTextColor(-16777216);
                        } else if (this.chartidstr.indexOf(tmps, 0) >= 0) {
                            button2.setTextColor(-16776961);
                        } else {
                            button2.setTextColor(-16777216);
                        }
                        button2.setId(btnid);
                        btnid++;
                        TableLayout.LayoutParams layoutParams3 = new TableLayout.LayoutParams(-2, -2);
                        layoutParams3.topMargin = btnmargin;
                        TableRow tableRow2 = new TableRow(this);
                        tableRow2.setGravity(1);
                        this.tablelayout.addView(tableRow2, layoutParams3);
                        LinearLayout linearLayout2 = new LinearLayout(this);
                        linearLayout2.setGravity(1);
                        tableRow2.addView(linearLayout2);
                        linearLayout2.addView(button2);
                    }
                }
                this.totalchartnums = btnid;
                if (this.readedidx > 0) {
                    this.curChartid = this.MAXBOOKNUMS;
                } else {
                    this.curChartid = 1;
                }
                findViewById(this.curChartid).setFocusable(true);
                findViewById(this.curChartid).requestFocus();
                findViewById(this.curChartid).setFocusableInTouchMode(true);
            }
        } catch (IOException e7) {
        }
        this.updatetimer.schedule(this.updatetask, 2000);
        if (this.Adsid1 != 101 && this.Adsid1 != 102) {
            this.adstimer.schedule(this.adstask, 1000);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.appCell.getisexitapp()) {
            finish();
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 21, 0, "阅读");
        menu.add(0, 22, 1, "推荐");
        menu.add(0, 23, 2, "退出");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 21:
                startread(this.curChartid);
                break;
            case 22:
                String imsi = this.appCell.getimsi();
                String imei = this.appCell.getimei();
                String tel = this.appCell.getitel();
                int width = this.appCell.getdisplayWidth();
                int height = this.appCell.getdisplayHeight();
                Intent intent1 = new Intent("android.intent.action.VIEW");
                intent1.setData(Uri.parse("http://www.welovemm.cn/wostore3.php?os=1&vs=31&xmlid=" + this.myid + "&" + "type=2" + "&" + "tel=" + tel + "&" + "imei=" + imei + "&" + "imsi=" + imsi + "&" + "width=" + String.valueOf(width) + "&" + "height=" + String.valueOf(height) + "&" + "session=" + this.appCell.getsession()));
                startActivity(intent1);
                break;
            case EXIT_ID /*23*/:
                Terminal(1);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == 0) {
            float x = ev.getX();
            float y = ev.getRawY();
            if (x >= 0.0f && x <= ((float) this.displayWidth) && y >= ((float) (this.displayHeight - this.Activityadsheight)) && y <= ((float) this.displayHeight)) {
                int curpoint = this.appCell.getadscurpoint();
                int adspoint = this.appCell.getadspoint();
                if (curpoint < adspoint + adspoint) {
                    if (1 == 1) {
                        this.appCell.setadscurpoint(curpoint + adspoint);
                    }
                    if (!(this.Adsid1 == 101 || this.Adsid1 == 102)) {
                        String newclickview = String.valueOf(this.appCell.getadsclickview()) + "," + String.valueOf(this.Adsid) + ",";
                        this.appCell.setadsclickview(newclickview);
                        this.appCell.writecuradsid(newclickview);
                    }
                }
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    public void onClick(View v) {
        int readid = v.getId();
        if (readid < 1000) {
            startread(readid);
            return;
        }
        String imsi = this.appCell.getimsi();
        String imei = this.appCell.getimei();
        String tel = this.appCell.getitel();
        int width = this.appCell.getdisplayWidth();
        int height = this.appCell.getdisplayHeight();
        Intent intent1 = new Intent("android.intent.action.VIEW");
        intent1.setData(Uri.parse("http://www.welovemm.cn/phoneadsclick3.php?os=1&vs=31&xmlid=" + this.myid + "&" + "adsid=" + (readid - 1000) + "&" + "tel=" + tel + "&" + "imei=" + imei + "&" + "imsi=" + imsi + "&" + "width=" + String.valueOf(width) + "&" + "height=" + String.valueOf(height) + "&" + "session=" + this.appCell.getsession()));
        startActivity(intent1);
    }

    public void startread(int idx) {
        int aidi = this.chartidstr.indexOf(",a,", 0);
        if (this.chartidstr.indexOf("," + idx + ",", 0) >= 0 || idx == this.MAXBOOKNUMS || aidi >= 0) {
            int curpoint = this.appCell.getadscurpoint();
            if (curpoint > 0 || this.Adsid1 == 101 || this.Adsid1 == 102) {
                if (this.appCell.getisbaidugps() == 1 && this.appCell.getlocsend() == 0) {
                    this.appCell.getlocinfo();
                    if (this.appCell.getlocresult() == 161) {
                        String imsi = this.appCell.getimsi();
                        String imei = this.appCell.getimei();
                        String tel = this.appCell.getitel();
                        String latitude = this.appCell.getlatitude();
                        String longitude = this.appCell.getlongitude();
                        String session = this.appCell.getsession();
                        String locaddr = this.appCell.getlocaddr();
                        String locurl = "";
                        try {
                            locurl = "http://www.welovemm.cn/location3.php?os=1&vs=31&xmlid=" + this.myid + "&" + "tel=" + tel + "&" + "imei=" + imei + "&" + "imsi=" + imsi + "&" + "latitude=" + latitude + "&" + "longitude=" + longitude + "&" + "locaddr=" + URLEncoder.encode(locaddr, "gb2312") + "&" + "loctime=" + this.appCell.getloctime() + "&" + "locradius=" + String.valueOf(this.appCell.getlocradius()) + "&" + "session=" + session;
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        try {
                            try {
                                HttpURLConnection urlCon = (HttpURLConnection) new URL(locurl).openConnection();
                                urlCon.setRequestMethod("GET");
                                urlCon.setConnectTimeout(10000);
                                urlCon.setRequestProperty("Charset", "gb2312");
                                urlCon.setUseCaches(false);
                                urlCon.connect();
                                if (urlCon.getResponseCode() == 200) {
                                    this.appCell.setlocsend(1);
                                } else {
                                    this.appCell.setlocsend(0);
                                }
                            } catch (IOException e2) {
                            }
                        } catch (MalformedURLException e3) {
                        }
                    }
                }
                this.appCell.setadscurpoint(curpoint - 1);
                Intent intent = new Intent(this, readtext.class);
                Bundle b = new Bundle();
                b.putString("chart", String.valueOf(idx));
                intent.putExtras(b);
                startActivityForResult(intent, 1);
            } else if (1 == 1) {
                new AlertDialog.Builder(this).setTitle("友情提醒").setMessage("您的阅读积分已经用完，如需继续阅读，请先点击底部广告条赚取阅读积分，谢谢您支持搜象").setPositiveButton("知道了", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                }).create().show();
            }
        } else {
            new AlertDialog.Builder(this).setTitle("搜象文学提醒你").setMessage("本章内容为付费阅读内容，请去购买该作者正版纸质书！支持正版，谢谢理解！").setPositiveButton("知道了", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.cancel();
                }
            }).create().show();
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        this.adstimer.cancel();
        finish();
        return true;
    }

    /* access modifiers changed from: private */
    public void Terminal(int isexit2) {
        if (isexit2 == 1) {
            this.appCell.setisexitapp(true);
        }
        finish();
    }

    /* access modifiers changed from: private */
    public boolean viewAds(int adsid) {
        if (this.mobwinadView != null) {
            this.mainlayout.removeView(this.mobwinadView);
            this.mobwinadView = null;
        }
        if (this.taobaocontainer != null) {
            this.mainlayout.removeView(this.taobaocontainer);
            this.taobaocontainer = null;
            this.taobaoadView = null;
        }
        if (this.baiduadView != null) {
            this.baiduadView.setVisibility(4);
            this.mainlayout.removeView(this.baiduadView);
        }
        if (this.imochaView != null) {
            this.mainlayout.removeView(this.imochaView);
            this.imochaView = null;
        }
        if (this.izpView != null) {
            this.mainlayout.removeView(this.izpView);
            this.izpView = null;
        }
        if (adsid == 15) {
            try {
                if (this.mobwinadView == null) {
                    this.mobwinadView = new com.tencent.mobwin.AdView(this);
                }
                if (this.mobwinadView != null) {
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(-1, -2);
                    params.addRule(14);
                    params.addRule(12);
                    this.mainlayout.addView(this.mobwinadView, params);
                }
            } catch (Exception e) {
                Log.i("mmreader", "mmreader10");
            }
        } else if (adsid == 29) {
            if (this.baiduadView == null) {
                this.baiduadView = new AdView(this, AdType.IMAGE);
                this.mainlayout.addView(this.baiduadView, new LinearLayout.LayoutParams(-1, -2));
                this.baiduadView.setListener(new AdViewListener() {
                    public void onAdSwitch() {
                    }

                    public void onReceiveSuccess() {
                    }

                    public void onReceiveFail(FailReason reason) {
                        if (chartact.this.appCell.getisbaidu() == 1) {
                            chartact.this.appCell.setcuradsid(29);
                            chartact.this.Adsid = chartact.this.appCell.readcuradsid();
                            boolean unused = chartact.this.viewAds(chartact.this.Adsid);
                        }
                    }
                });
            }
            if (this.baiduadView != null) {
                this.baiduadView.setVisibility(0);
                this.mainlayout.addView(this.baiduadView, new LinearLayout.LayoutParams(-1, -2));
            }
        } else if (adsid == 34) {
            if (this.taobaocontainer == null && this.taobaoadView == null) {
                this.taobaocontainer = new RelativeLayout(this);
                this.taobaoadView = new TaoAds(this);
            }
            if (!(this.taobaocontainer == null || this.taobaoadView == null)) {
                this.taobaoadView.setContainer(this.taobaocontainer);
                this.taobaoadView.requestAds();
                this.mainlayout.addView(this.taobaocontainer, new ViewGroup.LayoutParams(-1, -2));
                this.taobaoadView.setListener(new TaoAdsListener() {
                    public void onAdEvent(int eventType, String eventMsg) {
                        if ((eventType == 1003 || eventType == 1005) && chartact.this.appCell.getistaobao() == 1) {
                            chartact.this.appCell.setcuradsid(34);
                            chartact.this.Adsid = chartact.this.appCell.readcuradsid();
                            boolean unused = chartact.this.viewAds(chartact.this.Adsid);
                        }
                    }
                });
            }
        } else if (adsid == 38) {
            if (this.imochaView == null) {
                this.imochaView = new IMochaAdView(this, IMochaAdType.BANNER, "4843");
                this.imochaView.setAppId("1062");
                this.imochaView.setRefresh(true);
                this.imochaView.setUpdateTime(30);
                this.imochaView.setIMochaAdListener(new IMochaAdListener() {
                    public void onEvent(com.imocha.AdView ad, IMochaEventCode event) {
                        if ((event == IMochaEventCode.downloadError1 || event == IMochaEventCode.downloadError2) && chartact.this.appCell.getishdt() == 1) {
                            chartact.this.appCell.setcuradsid(38);
                            chartact.this.Adsid = chartact.this.appCell.readcuradsid();
                            boolean unused = chartact.this.viewAds(chartact.this.Adsid);
                        }
                    }
                });
                this.imochaView.downloadAd();
            }
            if (this.imochaView != null) {
                RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(-1, -2);
                params2.addRule(14);
                params2.addRule(12);
                this.mainlayout.addView(this.imochaView, params2);
            }
        } else if (adsid == 42) {
            if (this.izpView == null) {
                this.izpView = new IZPView(this);
                this.izpView.adType = "1";
                this.izpView.productID = this.appCell.getizpappid();
                this.izpView.isDev = false;
                this.izpView.startAdExchange();
            }
            if (this.izpView != null) {
                RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(-1, -2);
                params3.addRule(14);
                params3.addRule(12);
                this.mainlayout.addView(this.izpView, params3);
            }
        } else if (adsid != 36) {
            return false;
        } else {
            if (this.mobiView == null) {
                this.mobiView = new MobiSageAdBanner(this, "90fec5bc11b54d8bb86cc12f30bad416", null, null);
                this.mobiView.setAdRefreshInterval(4);
                this.mobiView.setAnimeType(1);
            }
            if (this.mobiView != null) {
                RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(-1, -2);
                params4.addRule(14);
                params4.addRule(12);
                this.mainlayout.addView(this.mobiView, params4);
            }
        }
        return true;
    }
}
