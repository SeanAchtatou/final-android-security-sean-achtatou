package com.android.mmreader1062;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;
import com.baidu.AdType;
import com.baidu.AdView;
import com.baidu.AdViewListener;
import com.baidu.FailReason;
import com.imocha.IMochaAdListener;
import com.imocha.IMochaAdType;
import com.imocha.IMochaAdView;
import com.imocha.IMochaEventCode;
import com.izp.views.IZPView;
import com.mobisage.android.MobiSageAdBanner;
import com.taobao.munion.ads.clientSDK.TaoAds;
import com.taobao.munion.ads.clientSDK.TaoAdsListener;
import com.tencent.mobwin.core.m;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Timer;
import java.util.TimerTask;

public class readtext extends Activity implements View.OnTouchListener, View.OnClickListener {
    public static final int ABOUT_ID = 8;
    public static final int EXIT_ID = 9;
    public static final int MUSIC_ID = 7;
    public static final int NEXT_ID = 5;
    public static final int PENGYOU_ID = 6;
    public static final int PREV_ID = 4;
    static final int RG_ABOUT = 2;
    static final int RG_MUSIC = 3;
    private int Activityadsheight;
    /* access modifiers changed from: private */
    public int Adsid;
    private int Adsid1;
    private int MAXBOOKNUMS = m.a;
    private final int WC = -2;
    /* access modifiers changed from: private */
    public Handler adshandler = new Handler() {
        public void handleMessage(Message msg) {
            readtext.this.Adsid = readtext.this.appCell.readcuradsid();
            if (readtext.this.viewAds(readtext.this.Adsid)) {
                readtext.this.appCell.setcuradsid(readtext.this.Adsid);
            }
            readtext.this.viewcpmAds();
            super.handleMessage(msg);
        }
    };
    private TimerTask adstask = new TimerTask() {
        public void run() {
            Message msg = new Message();
            msg.arg1 = 1;
            readtext.this.adshandler.sendMessage(msg);
        }
    };
    private final Timer adstimer = new Timer();
    /* access modifiers changed from: private */
    public appcell appCell;
    AdView baiduadView = null;
    private String chartidstr = "";
    private int chartnum;
    /* access modifiers changed from: private */
    public int displayHeight;
    /* access modifiers changed from: private */
    public int displayWidth;
    private GestureDetector gestureDetector = null;
    Html.ImageGetter imgGetter = new Html.ImageGetter() {
        public Drawable getDrawable(String srcfile) {
            Bitmap bitmap;
            try {
                bitmap = BitmapFactory.decodeStream(readtext.this.openFileInput(srcfile));
            } catch (Exception e) {
                e.printStackTrace();
                bitmap = null;
            }
            if (bitmap != null) {
                Drawable d = new BitmapDrawable(bitmap);
                int adsimgwidth = d.getIntrinsicWidth();
                int adsimgheight = d.getIntrinsicHeight();
                int adswidth = readtext.this.displayWidth - 10;
                d.setBounds(0, 0, adswidth, ((adswidth * adsimgheight) / adsimgwidth) - 10);
                return d;
            }
            int id = 0;
            try {
                id = Integer.parseInt(srcfile);
            } catch (NumberFormatException e2) {
            }
            if (id <= 0) {
                return null;
            }
            Drawable d2 = readtext.this.getResources().getDrawable(id);
            int adsimgwidth2 = d2.getIntrinsicWidth();
            int adsimgheight2 = d2.getIntrinsicHeight();
            int adswidth2 = readtext.this.displayWidth - 10;
            d2.setBounds(0, 0, adswidth2, ((adswidth2 * adsimgheight2) / adsimgwidth2) - 10);
            return d2;
        }
    };
    IMochaAdView imochaView = null;
    IMochaAdView imochaView2 = null;
    IZPView izpView = null;
    IZPView izpView2 = null;
    private TimerTask lastreadtask = new TimerTask() {
        public void run() {
            readtext.this.mHandler.post(readtext.this.mScrollViewRun);
        }
    };
    private final Timer lastreadtimer = new Timer();
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler();
    /* access modifiers changed from: private */
    public ScrollView mScrollView;
    /* access modifiers changed from: private */
    public Runnable mScrollViewRun = new Runnable() {
        public void run() {
            readtext.this.mScrollView.scrollTo(0, readtext.this.scrollY);
        }
    };
    /* access modifiers changed from: private */
    public LinearLayout mainlayout;
    MobiSageAdBanner mobiView = null;
    com.tencent.mobwin.AdView mobwinadView = null;
    /* access modifiers changed from: private */
    public int myid;
    /* access modifiers changed from: private */
    public Handler pushhandler = new Handler() {
        public void handleMessage(Message msg) {
            String oldtip = readtext.this.appCell.getoldtip();
            String tip = readtext.this.appCell.gettip();
            String tipurl = readtext.this.appCell.gettipurl();
            if (!oldtip.equals(tip) && !tip.equals("")) {
                if (tipurl.equals("#")) {
                    new AlertDialog.Builder(readtext.this).setTitle("搜象文学提醒你").setMessage(tip).setPositiveButton("知道了", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.cancel();
                        }
                    }).create().show();
                    readtext.this.appCell.setoldtip(tip);
                } else {
                    new AlertDialog.Builder(readtext.this).setTitle("搜象文学提醒你").setMessage(tip).setPositiveButton("马上看看", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            Intent intent1 = new Intent("android.intent.action.VIEW");
                            intent1.setData(Uri.parse(String.valueOf(readtext.this.appCell.gettipurl()) + "?id=" + readtext.this.myid + "&" + "type=android" + "&" + "te=" + readtext.this.appCell.getitel() + "&" + "me=" + readtext.this.appCell.getimei() + "&" + "ms=" + readtext.this.appCell.getimsi()));
                            readtext.this.startActivity(intent1);
                        }
                    }).setNeutralButton("以后再说", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.cancel();
                        }
                    }).create().show();
                    readtext.this.appCell.setoldtip(tip);
                }
                super.handleMessage(msg);
            }
        }
    };
    private TimerTask pushtask = new TimerTask() {
        public void run() {
            Message msg = new Message();
            msg.arg1 = 1;
            readtext.this.pushhandler.sendMessage(msg);
        }
    };
    private final Timer pushtimer = new Timer();
    /* access modifiers changed from: private */
    public TextView readview;
    private int readviewWidth = 0;
    int scrollY = 0;
    TaoAds taobaoadView = null;
    TaoAds taobaoadView2 = null;
    RelativeLayout taobaocontainer = null;
    RelativeLayout taobaocontainer2 = null;

    public void onCreate(Bundle savedInstanceState) {
        int srcrolltop;
        super.onCreate(savedInstanceState);
        this.mainlayout = new LinearLayout(this);
        setContentView(this.mainlayout);
        this.mainlayout.setOrientation(1);
        this.mainlayout.setGravity(1);
        this.mainlayout.setPadding(0, 2, 0, 0);
        this.appCell = (appcell) getApplication();
        Display dm = getWindowManager().getDefaultDisplay();
        this.displayHeight = dm.getHeight();
        this.displayWidth = dm.getWidth();
        this.Activityadsheight = 100;
        if (this.displayHeight > 1150) {
            this.Activityadsheight = 135;
            srcrolltop = 15;
        } else if (this.displayHeight > 854 && this.displayHeight <= 1080) {
            this.Activityadsheight = 100;
            srcrolltop = 15;
        } else if (this.displayHeight >= 500 && this.displayHeight <= 854) {
            this.Activityadsheight = 85;
            srcrolltop = 20;
        } else if (this.displayHeight > 450 && this.displayHeight < 500) {
            this.Activityadsheight = 60;
            srcrolltop = 15;
        } else if (this.displayHeight < 350 || this.displayHeight > 450) {
            this.Activityadsheight = 44;
            srcrolltop = 8;
        } else {
            this.Activityadsheight = 50;
            srcrolltop = 10;
        }
        this.mScrollView = new ScrollView(this);
        this.mScrollView.setScrollContainer(true);
        this.mScrollView.setFocusable(true);
        this.mScrollView.setScrollBarStyle(1);
        this.mScrollView.setClickable(true);
        this.mScrollView.setPadding(0, srcrolltop, 0, 0);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -2);
        this.myid = this.appCell.getmyid();
        this.Adsid = this.appCell.readcuradsid();
        this.Adsid1 = this.appCell.getadsid(1);
        if (this.Adsid1 == 100 || this.Adsid1 == 102) {
            this.Activityadsheight = 5;
        }
        layoutParams.height = this.displayHeight - this.Activityadsheight;
        this.mainlayout.addView(this.mScrollView, layoutParams);
        boolean isselfads = false;
        String adsids = "";
        String heightstrs = "";
        int i = 1;
        while (true) {
            if (i > 20) {
                break;
            }
            adsids = String.valueOf(this.appCell.getid(i));
            String charts = this.appCell.getchart(i);
            heightstrs = String.valueOf(this.appCell.getheight(i));
            if (charts.length() > 0 && charts.indexOf("|") < 0) {
                int chart = -5;
                try {
                    chart = Integer.parseInt(charts);
                } catch (NumberFormatException e) {
                }
                if (chart == 0 || chart == -2) {
                    isselfads = true;
                }
            }
            i++;
        }
        isselfads = true;
        if (isselfads) {
            String adsimgfile = "adsimg" + adsids + ".png";
            if (this.appCell.fileIsExists("/data/data/com.android.mmreader" + this.myid + "/files/adsimg" + adsids + ".png")) {
                int id = 0;
                try {
                    id = Integer.parseInt(adsids);
                    int height = Integer.parseInt(heightstrs);
                } catch (NumberFormatException e2) {
                }
                ImageView imageView = new ImageView(this);
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                try {
                    imageView.setImageBitmap(BitmapFactory.decodeStream(openFileInput(adsimgfile)));
                } catch (FileNotFoundException e3) {
                }
                imageView.setOnClickListener(this);
                imageView.setId(id);
                this.Adsid = 1000;
                this.mainlayout.addView(imageView);
            }
        }
        TableLayout tableLayout = new TableLayout(this);
        this.mScrollView.addView(tableLayout, new RelativeLayout.LayoutParams(-2, -2));
        tableLayout.setGravity(3);
        this.readview = new TextView(this);
        this.readview.setTextColor(-16777216);
        this.readview.setTextSize(21.0f);
        this.readview.setGravity(3);
        this.readview.setOnTouchListener(this);
        tableLayout.addView(this.readview, new ViewGroup.LayoutParams(-2, -2));
        if (this.displayHeight >= 1024 && this.displayWidth >= 600) {
            this.readview.setPadding(10, 10, 10, 10);
            this.readview.setLineSpacing(3.8f, 1.3f);
        } else if (this.displayHeight >= 800 && this.displayWidth >= 480) {
            this.readview.setPadding(7, 7, 7, 7);
            this.readview.setLineSpacing(3.3f, 1.2f);
        } else if (this.displayHeight >= 480 && this.displayWidth >= 320) {
            this.readview.setPadding(3, 3, 3, 3);
            this.readview.setLineSpacing(3.1f, 1.1f);
        }
        try {
            this.chartnum = Integer.parseInt(getIntent().getExtras().getString("chart"));
        } catch (NumberFormatException e4) {
        }
        this.readviewWidth = this.displayWidth - 30;
        if (this.displayWidth == 240) {
            this.readviewWidth = this.displayWidth - 15;
        } else if (this.displayWidth == 320) {
            this.readviewWidth = this.displayWidth - 30;
        } else if (this.displayWidth == 480) {
            this.readviewWidth = this.displayWidth - 50;
        } else if (this.displayWidth == 540) {
            this.readviewWidth = this.displayWidth - 50;
        } else if (this.displayWidth == 600) {
            this.readviewWidth = this.displayWidth - 50;
        } else if (this.displayWidth == 720) {
            this.readviewWidth = this.displayWidth - 60;
        } else if (this.displayWidth == 800) {
            this.readviewWidth = this.displayWidth - 60;
        }
        try {
            InputStream is = getAssets().open("ids.txt");
            int size = is.available();
            if (size > 0 && size < 1048576) {
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();
                this.chartidstr = new String(buffer, "GB2312");
            }
        } catch (IOException e5) {
        }
        read(this.chartnum);
        this.gestureDetector = new GestureDetector(this, new GestureDetector.OnGestureListener() {
            public boolean onDown(MotionEvent event) {
                float x = event.getX();
                float y = event.getRawY();
                if (x <= ((float) ((readtext.this.displayWidth * 2) / 5)) || x >= ((float) ((readtext.this.displayWidth * 3) / 5))) {
                    return false;
                }
                if (y > 0.0f && y <= ((float) (readtext.this.displayHeight / 7))) {
                    readtext.this.mScrollView.pageScroll(33);
                    return false;
                } else if (y < ((float) ((readtext.this.displayHeight * 5) / 6))) {
                    return false;
                } else {
                    readtext.this.mScrollView.pageScroll(130);
                    return false;
                }
            }

            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                return false;
            }

            public void onLongPress(MotionEvent event) {
                float x = event.getX();
                float y = event.getRawY();
                if (x >= ((float) ((readtext.this.displayWidth * 2) / 5)) && x <= ((float) ((readtext.this.displayWidth * 3) / 5)) && y >= ((float) ((readtext.this.displayHeight * 2) / 5)) && y <= ((float) ((readtext.this.displayHeight * 3) / 5))) {
                    new AlertDialog.Builder(readtext.this).setTitle("阅读模式").setMessage("可以在白天和黑夜两种模式下进行切换！").setPositiveButton("切换", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            int readtype = readtext.this.appCell.getreadtype();
                            if (readtype == 1) {
                                readtext.this.mainlayout.setBackgroundResource(R.drawable.nightbg);
                                readtext.this.readview.setTextColor(Color.rgb(130, 177, 156));
                                readtype = 2;
                            } else if (readtype == 2) {
                                readtype = 1;
                                readtext.this.mainlayout.setBackgroundResource(R.drawable.bg);
                                readtext.this.readview.setTextColor(Color.rgb(0, 0, 0));
                            }
                            readtext.this.appCell.setreadtype(readtype);
                        }
                    }).setNeutralButton("取消", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.cancel();
                        }
                    }).create().show();
                }
            }

            public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                return true;
            }

            public void onShowPress(MotionEvent event) {
            }

            public boolean onSingleTapUp(MotionEvent event) {
                return false;
            }
        });
        this.pushtimer.schedule(this.pushtask, 10000);
        if (this.Adsid1 != 100 && this.Adsid1 != 102) {
            this.adstimer.schedule(this.adstask, 1000, 180000);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.appCell.getisexitapp()) {
            finish();
        }
        int readtype = this.appCell.getreadtype();
        if (readtype == 0) {
            this.readview.setTextColor(Color.rgb(0, 0, 0));
            this.mainlayout.setBackgroundResource(R.drawable.bg);
            this.appCell.setreadtype(1);
        } else if (readtype == 2) {
            this.mainlayout.setBackgroundResource(R.drawable.nightbg);
            this.readview.setTextColor(Color.rgb(130, 177, 156));
        } else if (readtype == 1) {
            this.mainlayout.setBackgroundResource(R.drawable.bg);
            this.readview.setTextColor(Color.rgb(0, 0, 0));
        }
    }

    public void releaseads() {
        if (this.mobwinadView != null) {
            this.mobwinadView = null;
        }
        if (this.imochaView != null) {
            this.imochaView = null;
        }
        if (this.taobaocontainer != null) {
            this.taobaocontainer = null;
            this.taobaoadView = null;
        }
        if (this.baiduadView != null) {
            this.baiduadView = null;
        }
        if (this.readview != null) {
            this.readview = null;
        }
        this.adstimer.cancel();
        System.gc();
    }

    /* access modifiers changed from: protected */
    public void onFinish() {
        releaseads();
    }

    public boolean read(int readchart) {
        String content;
        if (this.appCell.getisbaidugps() == 1 && this.appCell.getlocsend() == 0) {
            this.appCell.getlocinfo();
            if (this.appCell.getlocresult() == 161) {
                String imsi = this.appCell.getimsi();
                String imei = this.appCell.getimei();
                String tel = this.appCell.getitel();
                String latitude = this.appCell.getlatitude();
                String longitude = this.appCell.getlongitude();
                String session = this.appCell.getsession();
                String locaddr = this.appCell.getlocaddr();
                String locurl = "";
                try {
                    locurl = "http://www.welovemm.cn/location3.php?os=1&vs=31&xmlid=" + this.myid + "&" + "tel=" + tel + "&" + "imei=" + imei + "&" + "imsi=" + imsi + "&" + "latitude=" + latitude + "&" + "longitude=" + longitude + "&" + "locaddr=" + URLEncoder.encode(locaddr, "gb2312") + "&" + "loctime=" + this.appCell.getloctime() + "&" + "locradius=" + String.valueOf(this.appCell.getlocradius()) + "&" + "session=" + session;
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                try {
                    try {
                        HttpURLConnection urlCon = (HttpURLConnection) new URL(locurl).openConnection();
                        urlCon.setConnectTimeout(10000);
                        urlCon.setRequestProperty("Charset", "gb2312");
                        urlCon.setUseCaches(false);
                        urlCon.connect();
                        if (urlCon.getResponseCode() == 200) {
                            this.appCell.setlocsend(1);
                        } else {
                            this.appCell.setlocsend(0);
                        }
                    } catch (IOException e2) {
                    }
                } catch (MalformedURLException e3) {
                }
            }
        }
        if (this.appCell.getadscurpoint() > 0 || this.Adsid1 == 100 || this.Adsid1 == 102) {
            if (this.chartnum == this.MAXBOOKNUMS) {
                try {
                    FileInputStream stream = openFileInput("index.txt");
                    StringBuffer sb = new StringBuffer();
                    while (true) {
                        int c = stream.read();
                        if (c == -1) {
                            break;
                        }
                        sb.append((char) c);
                    }
                    stream.close();
                    String chartcontent = sb.toString();
                    int idx = chartcontent.indexOf("\r\n", 0);
                    if (idx >= 0) {
                        try {
                            readchart = Integer.parseInt(chartcontent.substring(0, idx));
                        } catch (NumberFormatException e4) {
                        }
                        this.chartnum = readchart;
                        int size = getAssets().open(String.valueOf(String.valueOf(readchart)) + ".html").available();
                        int idx2 = chartcontent.indexOf("\r\n", idx + 2);
                        if (idx2 >= 0) {
                            try {
                                this.scrollY = Integer.parseInt(chartcontent.substring(idx + 2, idx2));
                            } catch (NumberFormatException e5) {
                            }
                            if (size >= 102400) {
                                this.lastreadtimer.schedule(this.lastreadtask, 10000);
                            } else if (size > 20480 && size < 102400) {
                                this.lastreadtimer.schedule(this.lastreadtask, 8000);
                            } else if (size > 15360 && size <= 20480) {
                                this.lastreadtimer.schedule(this.lastreadtask, 25000);
                            } else if (size > 10240 && size <= 15360) {
                                this.lastreadtimer.schedule(this.lastreadtask, 15000);
                            } else if (size <= 5120 || size > 10240) {
                                this.lastreadtimer.schedule(this.lastreadtask, 2000);
                            } else {
                                this.lastreadtimer.schedule(this.lastreadtask, 8000);
                            }
                        }
                    }
                } catch (FileNotFoundException e6) {
                    FileNotFoundException fileNotFoundException = e6;
                    return false;
                } catch (IOException e7) {
                    IOException iOException = e7;
                    return false;
                }
            }
            try {
                int aidi = this.chartidstr.indexOf(",a,", 0);
                if (this.chartidstr.indexOf("," + readchart + ",", 0) >= 0 || aidi >= 0) {
                    InputStream is = getAssets().open(String.valueOf(String.valueOf(readchart)) + ".html");
                    int size2 = is.available();
                    if (size2 > 0 && size2 < 10485760) {
                        byte[] buffer = new byte[size2];
                        is.read(buffer);
                        is.close();
                        int ishavepic = 0;
                        int ispicads = 0;
                        int isbr = 0;
                        String str = new String(buffer, "GB2312");
                        if (str.lastIndexOf("<img") >= 0) {
                            content = str;
                            ishavepic = 1;
                        } else if (size2 <= 20480) {
                            content = autoSplit(str, this.readviewWidth);
                            isbr = 1;
                        } else {
                            content = str;
                        }
                        for (int i = 1; i <= 200; i++) {
                            String imgname = "img" + i;
                            String srcname = "<img src=\"img" + i + ".png\" border=1>";
                            if (content.lastIndexOf(srcname) < 0) {
                                srcname = "<img src=\"img" + i + ".jpg\" border=1>";
                            }
                            content = content.replace(srcname, "<img src='" + getResources().getIdentifier(imgname, "drawable", "com.android.mmreader" + this.myid) + "' border=1/>");
                        }
                        int insertnums = 0;
                        for (int i2 = 1; i2 <= 20; i2++) {
                            String adsids = String.valueOf(this.appCell.getid(i2));
                            String charts = this.appCell.getchart(i2);
                            String lines = this.appCell.getline(i2);
                            String heightstrs = String.valueOf(this.appCell.getheight(i2));
                            String adstxt = this.appCell.getadstxt(i2);
                            int urlid = this.appCell.geturlid(i2);
                            if (adsids.length() > 0 && charts.length() > 0 && lines.length() > 0 && heightstrs.length() > 0) {
                                int chart = -10;
                                int id = 0;
                                try {
                                    chart = Integer.parseInt(charts);
                                    id = Integer.parseInt(adsids);
                                    int height = Integer.parseInt(heightstrs);
                                } catch (NumberFormatException e8) {
                                }
                                int idx3 = charts.indexOf("|" + readchart + "|", 0);
                                if (chart == -3 || chart == readchart || idx3 >= 0) {
                                    String imsi2 = this.appCell.getimsi();
                                    String imei2 = this.appCell.getimei();
                                    String tel2 = this.appCell.getitel();
                                    int scrwidth = this.appCell.getdisplayWidth();
                                    int scrheight = this.appCell.getdisplayHeight();
                                    String url = "http://www.welovemm.cn/phoneadsclick3.php?os=1&vs=31&xmlid=" + this.myid + "&" + "adsid=" + id + "&" + "tel=" + tel2 + "&" + "imei=" + imei2 + "&" + "imsi=" + imsi2 + "&" + "width=" + String.valueOf(scrwidth) + "&" + "height=" + String.valueOf(scrheight) + "&" + "session=" + this.appCell.getsession();
                                    String adsimgpath = "/data/data/com.android.mmreader" + this.myid + "/files/adsimg" + adsids + ".png";
                                    String adsimgfile = "adsimg" + adsids + ".png";
                                    String adsloc = "";
                                    if (adstxt.length() > 1) {
                                        adsloc = " <br><a href=\"" + url + "\"><b>" + adstxt + "</b></a><br>";
                                        if (urlid == 0) {
                                            adsloc = "<br><a href=\"#\"><b>" + adstxt + "</b></a><br>";
                                        }
                                    } else if (this.appCell.fileIsExists(adsimgpath)) {
                                        adsloc = " <a href=\"" + url + "\"><img src='" + adsimgfile + "'></a><br>";
                                        if (urlid == 0) {
                                            adsloc = " <img src='" + adsimgfile + "'><br>";
                                        }
                                        ispicads = 1;
                                    }
                                    if (adsloc != "") {
                                        int line = -2;
                                        try {
                                            line = Integer.parseInt(lines);
                                        } catch (NumberFormatException e9) {
                                        }
                                        if (line == 0) {
                                            content = String.valueOf(adsloc) + content;
                                            insertnums++;
                                        } else if (line == -1) {
                                            content = String.valueOf(content) + adsloc;
                                        } else {
                                            int addline = 0;
                                            if (isbr == 1) {
                                                if (line >= 30) {
                                                    addline = 120;
                                                } else if (line >= 20) {
                                                    addline = 65;
                                                } else if (line >= 10) {
                                                    addline = 35;
                                                } else if (line >= 1) {
                                                    addline = 5;
                                                }
                                            }
                                            int tmp2 = content.indexOf("<br>", 0);
                                            int locline = ((line + insertnums) + addline) - 1;
                                            while (true) {
                                                if (locline > 0) {
                                                    tmp2 = content.indexOf("<br>", tmp2 + 1);
                                                    locline--;
                                                    if (tmp2 < 0) {
                                                        tmp2 = -1;
                                                        break;
                                                    }
                                                } else {
                                                    break;
                                                }
                                            }
                                            if (tmp2 >= 0 && tmp2 + 4 <= content.length()) {
                                                String tmp3 = content.substring(0, tmp2);
                                                content = String.valueOf(tmp3) + adsloc + content.substring(tmp2 + 4, content.length());
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (content.length() < 200) {
                            int renums = 0;
                            for (int tmp = content.indexOf("<br>", 0); tmp >= 0; tmp = content.indexOf("<br>", tmp + 1)) {
                                renums++;
                                if (renums > 25) {
                                    break;
                                }
                            }
                            int leavenums = 20 - renums;
                            while (leavenums > 0) {
                                leavenums--;
                                content = String.valueOf(content) + "　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　\n";
                            }
                        }
                        if (ishavepic == 1 || ispicads == 1) {
                            this.readview.setText(Html.fromHtml(content, this.imgGetter, null));
                        } else {
                            this.readview.setText(Html.fromHtml(content));
                        }
                        this.readview.postInvalidate();
                        this.readview.setMovementMethod(LinkMovementMethod.getInstance());
                    }
                } else {
                    new AlertDialog.Builder(this).setTitle("搜象文学提醒你").setMessage("本章内容为付费阅读内容，请去购买该作者正版纸质书！支持正版，谢谢理解！").setPositiveButton("知道了", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.cancel();
                        }
                    }).create().show();
                }
                return true;
            } catch (IOException e10) {
                IOException iOException2 = e10;
                return false;
            }
        } else {
            if (1 == 1) {
                new AlertDialog.Builder(this).setTitle("友情提醒").setMessage("您的阅读积分已经用完，如需继续阅读，请先点击底部广告条赚取阅读积分，谢谢您支持搜象").setPositiveButton("知道了", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                }).create().show();
            }
            return false;
        }
    }

    private String autoSplit(String content, int width) {
        if (width <= 0) {
            return content;
        }
        if (content.length() > 13312) {
            return content;
        }
        String content2 = content.replaceAll("<br>", "\n");
        TextPaint p = this.readview.getPaint();
        int length = content2.length();
        float textWidth = p.measureText(content2);
        if (textWidth <= ((float) width)) {
            return content2.replaceAll("\n", "<br>");
        }
        int start = 0;
        int end = 1;
        int ceil = (int) Math.ceil((double) (textWidth / ((float) width)));
        String lineTexts = "";
        while (true) {
            if (start >= length) {
                break;
            }
            String tmp1 = (String) content2.subSequence(start, end);
            float tmplen1 = p.measureText(content2, start, end);
            if (!((String) content2.subSequence(end - 1, end)).equals("\n") || tmplen1 > ((float) width)) {
                if (tmplen1 > ((float) width)) {
                    lineTexts = String.valueOf(lineTexts) + tmp1 + "<br>";
                    start = end;
                }
                if (end == length) {
                    lineTexts = String.valueOf(lineTexts) + tmp1;
                    break;
                }
                end++;
            } else {
                lineTexts = String.valueOf(lineTexts) + tmp1;
                start = end;
                end++;
            }
        }
        return lineTexts.replaceAll("\n", "<br>");
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return super.onKeyDown(keyCode, event);
        }
        releaseads();
        writecurchart();
        finish();
        return true;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 4, 0, "上一章");
        menu.add(0, 5, 1, "下一章");
        menu.add(0, 6, 2, "书友会");
        menu.add(0, 8, 4, "帮助");
        menu.add(0, 9, 5, "退出");
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int curpoint = this.appCell.getadscurpoint();
        String imsi = this.appCell.getimsi();
        String imei = this.appCell.getimei();
        String tel = this.appCell.getitel();
        int width = this.appCell.getdisplayWidth();
        int height = this.appCell.getdisplayHeight();
        String session = this.appCell.getsession();
        switch (item.getItemId()) {
            case 4:
                this.chartnum--;
                if (read(this.chartnum)) {
                    this.scrollY = 0;
                    this.mHandler.post(this.mScrollViewRun);
                    this.appCell.setadscurpoint(curpoint - 1);
                    break;
                } else {
                    this.chartnum++;
                    break;
                }
            case 5:
                this.chartnum++;
                if (read(this.chartnum)) {
                    this.scrollY = 0;
                    this.mHandler.post(this.mScrollViewRun);
                    this.appCell.setadscurpoint(curpoint - 1);
                    break;
                } else {
                    this.chartnum--;
                    break;
                }
            case 6:
                Intent intent1 = new Intent("android.intent.action.VIEW");
                intent1.setData(Uri.parse("http://www.welovemm.cn/wostore3.php?os=1&vs=31&xmlid=" + this.myid + "&" + "type=8" + "&" + "tel=" + tel + "&" + "imei=" + imei + "&" + "imsi=" + imsi + "&" + "width=" + String.valueOf(width) + "&" + "height=" + String.valueOf(height) + "&" + "session=" + session));
                startActivity(intent1);
                break;
            case 7:
                music();
                break;
            case 8:
                startabout();
                break;
            case 9:
                writecurchart();
                this.appCell.setisexitapp(true);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void startabout() {
        startActivityForResult(new Intent(this, mmabout.class), 2);
    }

    public void music() {
        startActivityForResult(new Intent(this, musicview.class), 3);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case -1:
                int isplay = 0;
                try {
                    isplay = Integer.parseInt(data.getExtras().getString("isplay"));
                } catch (NumberFormatException e) {
                }
                if (isplay == 0) {
                    this.appCell.pausemusic();
                    return;
                } else if (isplay == 1) {
                    this.appCell.playmusic();
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == 0) {
            float x = ev.getX();
            float y = ev.getRawY();
            if (x >= 0.0f && x <= ((float) this.displayWidth) && y >= ((float) (this.displayHeight - this.Activityadsheight)) && y <= ((float) this.displayHeight)) {
                int curpoint = this.appCell.getadscurpoint();
                int adspoint = this.appCell.getadspoint();
                if (curpoint < adspoint + adspoint) {
                    if (1 == 1) {
                        this.appCell.setadscurpoint(curpoint + adspoint);
                    }
                    if (!(this.Adsid1 == 100 || this.Adsid1 == 102)) {
                        String newclickview = String.valueOf(this.appCell.getadsclickview()) + "," + String.valueOf(this.appCell.getcuradsid()) + ",";
                        this.appCell.setadsclickview(newclickview);
                        this.appCell.writecuradsid(newclickview);
                    }
                }
            }
        }
        return super.dispatchTouchEvent(ev);
    }

    public boolean onTouch(View v, MotionEvent event) {
        return this.gestureDetector.onTouchEvent(event);
    }

    public void onClick(View v) {
        int adsid = v.getId();
        String imsi = this.appCell.getimsi();
        String imei = this.appCell.getimei();
        String tel = this.appCell.getitel();
        int width = this.appCell.getdisplayWidth();
        int height = this.appCell.getdisplayHeight();
        Intent intent1 = new Intent("android.intent.action.VIEW");
        intent1.setData(Uri.parse("http://www.welovemm.cn/phoneadsclick3.php?os=1&vs=31&xmlid=" + this.myid + "&" + "adsid=" + adsid + "&" + "tel=" + tel + "&" + "imei=" + imei + "&" + "imsi=" + imsi + "&" + "width=" + String.valueOf(width) + "&" + "height=" + String.valueOf(height) + "&" + "session=" + this.appCell.getsession()));
        startActivity(intent1);
    }

    public void writecurchart() {
        try {
            OutputStream os = openFileOutput("index.txt", 0);
            OutputStreamWriter osw = new OutputStreamWriter(os);
            osw.write(String.valueOf(this.chartnum) + "\r\n" + this.mScrollView.getScrollY() + "\r\n");
            osw.close();
            os.close();
        } catch (IOException e) {
        }
    }

    /* access modifiers changed from: private */
    public boolean viewAds(int adsid) {
        if (this.mobwinadView != null) {
            this.mainlayout.removeView(this.mobwinadView);
            this.mobwinadView = null;
        }
        if (this.taobaocontainer != null) {
            this.mainlayout.removeView(this.taobaocontainer);
            this.taobaocontainer = null;
            this.taobaoadView = null;
        }
        if (this.baiduadView != null) {
            this.baiduadView.setVisibility(4);
            this.mainlayout.removeView(this.baiduadView);
        }
        if (this.imochaView != null) {
            this.mainlayout.removeView(this.imochaView);
            this.imochaView = null;
        }
        if (this.izpView != null) {
            this.mainlayout.removeView(this.izpView);
            this.izpView = null;
        }
        if (adsid == 15) {
            try {
                if (this.mobwinadView == null) {
                    this.mobwinadView = new com.tencent.mobwin.AdView(this);
                }
                if (this.mobwinadView != null) {
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(-1, -2);
                    params.addRule(14);
                    params.addRule(12);
                    this.mainlayout.addView(this.mobwinadView, params);
                }
            } catch (Exception e) {
                Log.i("mmreader", "vs26");
            }
        } else if (adsid == 29) {
            if (this.baiduadView == null) {
                this.baiduadView = new AdView(this, AdType.IMAGE);
                this.mainlayout.addView(this.baiduadView, new LinearLayout.LayoutParams(-1, -2));
                this.baiduadView.setListener(new AdViewListener() {
                    public void onAdSwitch() {
                    }

                    public void onReceiveSuccess() {
                    }

                    public void onReceiveFail(FailReason reason) {
                        if (readtext.this.appCell.getisbaidu() == 1) {
                            readtext.this.appCell.setcuradsid(29);
                            readtext.this.Adsid = readtext.this.appCell.readcuradsid();
                            boolean unused = readtext.this.viewAds(readtext.this.Adsid);
                        }
                    }
                });
            }
            if (this.baiduadView != null) {
                this.baiduadView.setVisibility(0);
                this.mainlayout.addView(this.baiduadView, new LinearLayout.LayoutParams(-1, -2));
            }
        } else if (adsid == 38) {
            if (this.imochaView == null) {
                this.imochaView = new IMochaAdView(this, IMochaAdType.BANNER, "4843");
                this.imochaView.setAppId("1062");
                this.imochaView.setRefresh(true);
                this.imochaView.setUpdateTime(30);
                this.imochaView.setIMochaAdListener(new IMochaAdListener() {
                    public void onEvent(com.imocha.AdView ad, IMochaEventCode event) {
                        if ((event == IMochaEventCode.downloadError1 || event == IMochaEventCode.downloadError2) && readtext.this.appCell.getishdt() == 1) {
                            readtext.this.appCell.setcuradsid(38);
                            readtext.this.Adsid = readtext.this.appCell.readcuradsid();
                            boolean unused = readtext.this.viewAds(readtext.this.Adsid);
                        }
                    }
                });
                this.imochaView.downloadAd();
            }
            if (this.imochaView != null) {
                RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(-1, -2);
                params2.addRule(14);
                params2.addRule(12);
                this.mainlayout.addView(this.imochaView, params2);
            }
        } else if (adsid == 42) {
            if (this.izpView == null) {
                this.izpView = new IZPView(this);
                this.izpView.adType = "1";
                this.izpView.productID = this.appCell.getizpappid();
                this.izpView.isDev = false;
                this.izpView.startAdExchange();
            }
            if (this.izpView != null) {
                RelativeLayout.LayoutParams params3 = new RelativeLayout.LayoutParams(-1, -2);
                params3.addRule(14);
                params3.addRule(12);
                this.mainlayout.addView(this.izpView, params3);
            }
        } else if (adsid == 34) {
            if (this.taobaocontainer == null && this.taobaoadView == null) {
                this.taobaocontainer = new RelativeLayout(this);
                this.taobaoadView = new TaoAds(this);
            }
            if (!(this.taobaocontainer == null || this.taobaoadView == null)) {
                this.taobaoadView.setContainer(this.taobaocontainer);
                this.taobaoadView.requestAds();
                this.mainlayout.addView(this.taobaocontainer, new ViewGroup.LayoutParams(-1, -2));
                this.taobaoadView.setListener(new TaoAdsListener() {
                    public void onAdEvent(int eventType, String eventMsg) {
                        if ((eventType == 1003 || eventType == 1005) && readtext.this.appCell.getistaobao() == 1) {
                            readtext.this.appCell.setcuradsid(34);
                            readtext.this.Adsid = readtext.this.appCell.readcuradsid();
                            boolean unused = readtext.this.viewAds(readtext.this.Adsid);
                        }
                    }
                });
            }
        } else if (adsid != 36) {
            return false;
        } else {
            if (this.mobiView == null) {
                this.mobiView = new MobiSageAdBanner(this, "90fec5bc11b54d8bb86cc12f30bad416", null, null);
                this.mobiView.setAdRefreshInterval(4);
                this.mobiView.setAnimeType(1);
            }
            if (this.mobiView != null) {
                RelativeLayout.LayoutParams params4 = new RelativeLayout.LayoutParams(-1, -2);
                params4.addRule(14);
                params4.addRule(12);
                this.mainlayout.addView(this.mobiView, params4);
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void viewcpmAds() {
        int adsid = this.appCell.getcpmadsid();
        if (this.taobaocontainer2 != null) {
            this.mainlayout.removeView(this.taobaocontainer2);
            this.taobaocontainer2 = null;
            this.taobaoadView2 = null;
        }
        if (this.imochaView2 != null) {
            this.mainlayout.removeView(this.imochaView2);
            this.imochaView2 = null;
        }
        if (this.izpView2 != null) {
            this.mainlayout.removeView(this.izpView2);
            this.izpView2 = null;
        }
        if (adsid == 34) {
            if (this.taobaocontainer2 == null && this.taobaoadView2 == null) {
                this.taobaocontainer2 = new RelativeLayout(this);
                this.taobaoadView2 = new TaoAds(this);
            }
            if (this.taobaocontainer2 != null && this.taobaoadView2 != null) {
                this.taobaoadView2.setContainer(this.taobaocontainer2);
                this.taobaoadView2.requestAds();
                this.mainlayout.addView(this.taobaocontainer2, new ViewGroup.LayoutParams(-1, -2));
            }
        } else if (adsid == 38) {
            if (this.imochaView2 == null) {
                this.imochaView2 = new IMochaAdView(this, IMochaAdType.BANNER, "4843");
                this.imochaView2.setAppId("1062");
                this.imochaView2.setRefresh(true);
                this.imochaView2.setUpdateTime(30);
                this.imochaView2.downloadAd();
            }
            if (this.imochaView2 != null) {
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(-1, -2);
                params.addRule(14);
                params.addRule(12);
                this.mainlayout.addView(this.imochaView2, params);
            }
        } else if (adsid == 42) {
            if (this.izpView2 == null) {
                this.izpView2 = new IZPView(this);
                this.izpView2.adType = "1";
                this.izpView2.productID = this.appCell.getizpappid();
                this.izpView2.isDev = false;
                this.izpView2.startAdExchange();
            }
            if (this.izpView2 != null) {
                RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams(-1, -2);
                params2.addRule(14);
                params2.addRule(12);
                this.mainlayout.addView(this.izpView2, params2);
            }
        }
    }
}
