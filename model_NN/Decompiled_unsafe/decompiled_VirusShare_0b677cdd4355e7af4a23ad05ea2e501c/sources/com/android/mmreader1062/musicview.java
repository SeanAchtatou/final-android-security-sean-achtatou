package com.android.mmreader1062;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class musicview extends Activity implements View.OnClickListener {
    private appcell appCell;
    Button closebtn;
    Button filebtn;
    TextView musicname;
    Button playbtn;
    /* access modifiers changed from: private */
    public boolean sureplay = false;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.dailog);
        this.appCell = (appcell) getApplication();
        int isplaymusic = this.appCell.getisplaymusic();
        String musicfile = this.appCell.getmusicfile();
        this.musicname = (TextView) findViewById(R.id.musicname);
        if (musicfile == "defaultmusic") {
            this.musicname.setText("当前选择为系统默认音乐,会耗费网络流量。\n请使用手机WIFI连接网络或优先选择本地音乐播放");
        } else {
            this.musicname.setText(musicfile);
        }
        this.playbtn = (Button) findViewById(R.id.playbtn);
        if (isplaymusic == 0 || isplaymusic == 2 || isplaymusic == 4) {
            this.playbtn.setText("播 放");
        } else {
            this.playbtn.setText("暂 停");
        }
        this.playbtn.setOnClickListener(this);
        this.filebtn = (Button) findViewById(R.id.filebtn);
        this.filebtn.setOnClickListener(this);
        this.closebtn = (Button) findViewById(R.id.closebtn);
        this.closebtn.setOnClickListener(this);
    }

    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.playbtn) {
            String musicfile = this.appCell.getmusicfile();
            int isplay = this.appCell.getisplaymusic();
            if (isplay == 0 || isplay == 2 || isplay == 4) {
                if (musicfile == "defaultmusic") {
                    new AlertDialog.Builder(this).setTitle("搜象文学提醒你").setMessage("您即将播放的是系统默认音乐，会消耗网络流量，请使用手机WIFI连接网络。建议您优先选择手机里的音乐文件来播放").setPositiveButton("继续播放", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            musicview.this.sureplay = true;
                            dialog.cancel();
                        }
                    }).setNeutralButton("暂不播放", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.cancel();
                        }
                    }).create().show();
                } else {
                    this.sureplay = true;
                }
                if (this.sureplay) {
                    Intent i = new Intent();
                    Bundle b2 = new Bundle();
                    b2.putString("isplay", "1");
                    i.putExtras(b2);
                    setResult(-1, i);
                    finish();
                }
            } else if (isplay == 1) {
                Intent i2 = new Intent();
                Bundle b22 = new Bundle();
                b22.putString("isplay", "0");
                i2.putExtras(b22);
                setResult(-1, i2);
                finish();
            }
        }
        if (id == R.id.filebtn) {
            startActivityForResult(new Intent(this, fileview.class), 1);
        }
        if (id == R.id.closebtn) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case -1:
                String musicfile = this.appCell.getmusicfile();
                if (!musicfile.equalsIgnoreCase(this.musicname.getText().toString())) {
                    this.appCell.setisplaymusic(4);
                }
                this.musicname.setText(musicfile);
                return;
            default:
                return;
        }
    }
}
