package com.baidu;

import MobWin.cnst.FUNCTION_CLICK;
import android.content.Context;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class h extends Thread {
    final /* synthetic */ t a;
    final /* synthetic */ c b;

    h(c cVar, t tVar) {
        this.b = cVar;
        this.a = tVar;
    }

    public void run() {
        boolean z;
        synchronized (this.b.k) {
            try {
                JSONObject jSONObject = this.b.k.getJSONObject(FUNCTION_CLICK.a);
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    try {
                        String obj = keys.next().toString();
                        bk.b("AdCache.sendClicklog " + obj + "{");
                        JSONObject jSONObject2 = jSONObject.getJSONObject(obj);
                        JSONArray jSONArray = jSONObject2.getJSONArray("record");
                        for (int length = jSONArray.length() - 1; length >= 0; length--) {
                            if (!jSONArray.isNull(length)) {
                                JSONObject jSONObject3 = jSONArray.getJSONObject(length);
                                String str = jSONObject2.getString("clklogurl") + "&extra=" + ax.a(String.format("%05x%05x%05x%x,%s", Integer.valueOf(jSONObject3.getInt("show")), Integer.valueOf(jSONObject3.getInt(FUNCTION_CLICK.a)), Integer.valueOf(jSONObject3.getInt("phone")), Integer.valueOf(this.a.b()), jSONObject3.getString("stamp")));
                                bk.b("AdCache.sendClicklog", obj + " " + jSONObject3.getString("date"));
                                bk.b("AdCache.sendClicklog", str);
                                if (!"{error}".equals(w.d((Context) c.i.get(), str))) {
                                    jSONArray.put(length, JSONObject.NULL);
                                }
                            }
                        }
                        int i = 0;
                        while (true) {
                            if (i >= jSONArray.length()) {
                                z = true;
                                break;
                            } else if (!jSONArray.isNull(i)) {
                                z = false;
                                break;
                            } else {
                                i++;
                            }
                        }
                        if (z) {
                            keys.remove();
                            bk.b("AdCache.sendClicklog", obj + " clicklog removed");
                        }
                        this.b.u();
                        bk.b("AdCache.sendClicklog }");
                    } catch (JSONException e) {
                        bk.a("AdCache.sendClicklog", "", e);
                    } catch (Exception e2) {
                        bk.a("AdCache.sendClicklog", "", e2);
                    }
                }
            } catch (JSONException e3) {
                bk.a("AdCache.sendClicklog", "", e3);
            }
        }
    }
}
