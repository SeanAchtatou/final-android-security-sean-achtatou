package com.baidu.location;

import android.content.Context;
import android.content.IntentFilter;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class e {
    private static final int B = 3;
    private static final int C = 0;
    private static final int L = 20;
    private static Method S = null;
    private static final int T = 10000;
    private static final double e = 1.0E-4d;

    /* renamed from: else  reason: not valid java name */
    private static final int f20else = 10;
    private static Class h = null;
    private static final int n = 2000;
    private static Method t = null;
    /* access modifiers changed from: private */
    public Handler A = null;
    /* access modifiers changed from: private */
    public Runnable D = null;
    private Runnable E = null;
    private long F;
    /* access modifiers changed from: private */
    public int G;
    WifiManager H = null;
    private List I;
    /* access modifiers changed from: private */
    public List J;
    private c K = null;
    /* access modifiers changed from: private */
    public int M = 3000;
    /* access modifiers changed from: private */
    public int N;
    /* access modifiers changed from: private */
    public Location O;
    /* access modifiers changed from: private */
    public int P;
    /* access modifiers changed from: private */
    public boolean Q;
    private int R;
    private String U = "bd09";
    /* access modifiers changed from: private */
    public GpsStatus a;
    /* access modifiers changed from: private */
    public int b;
    /* access modifiers changed from: private */

    /* renamed from: byte  reason: not valid java name */
    public LocationManager f21byte = null;
    /* access modifiers changed from: private */
    public List c = null;

    /* renamed from: case  reason: not valid java name */
    private final long f22case = 1000;

    /* renamed from: char  reason: not valid java name */
    private b f23char = null;
    private final long d = 65000;

    /* renamed from: do  reason: not valid java name */
    private final long f24do = 15000;
    /* access modifiers changed from: private */
    public long f;

    /* renamed from: for  reason: not valid java name */
    private final int f25for = 30000;
    private TelephonyManager g = null;
    /* access modifiers changed from: private */

    /* renamed from: goto  reason: not valid java name */
    public Location f26goto = null;
    private String i;
    /* access modifiers changed from: private */

    /* renamed from: if  reason: not valid java name */
    public ArrayList f27if = new ArrayList();

    /* renamed from: int  reason: not valid java name */
    private String f28int = "detail";
    private boolean j = false;
    private Context k;
    /* access modifiers changed from: private */
    public int l;
    /* access modifiers changed from: private */

    /* renamed from: long  reason: not valid java name */
    public String f29long;
    private String m;

    /* renamed from: new  reason: not valid java name */
    private String f30new = "default";
    private C0000e o;
    /* access modifiers changed from: private */
    public boolean p = true;
    /* access modifiers changed from: private */
    public int q;
    private String r;
    private d s = null;

    /* renamed from: try  reason: not valid java name */
    private boolean f31try = true;
    private int u;
    private LocServiceMode v = LocServiceMode.Background;
    /* access modifiers changed from: private */

    /* renamed from: void  reason: not valid java name */
    public String f32void;
    private final int w = 3000;
    private ArrayList x = null;
    private final long y = 1000;
    /* access modifiers changed from: private */
    public Runnable z = null;

    private class a implements Runnable {
        private a() {
        }

        /* synthetic */ a(e eVar, a aVar) {
            this();
        }

        public void run() {
            if (!e.this.p && !e.this.Q) {
                try {
                    if (e.this.H.isWifiEnabled()) {
                        e.this.H.startScan();
                    }
                    e.this.A.postDelayed(e.this.z, (long) e.this.N);
                } catch (Exception e) {
                    e.printStackTrace();
                    e.this.z = (Runnable) null;
                }
            }
        }
    }

    private class b implements GpsStatus.Listener {
        private b() {
        }

        /* synthetic */ b(e eVar, b bVar) {
            this();
        }

        public void onGpsStatusChanged(int i) {
            if (e.this.f21byte != null) {
                switch (i) {
                    case 2:
                        e.this.O = (Location) null;
                        return;
                    case 3:
                    default:
                        return;
                    case 4:
                        if (e.this.a == null) {
                            e.this.a = e.this.f21byte.getGpsStatus(null);
                        } else {
                            e.this.f21byte.getGpsStatus(e.this.a);
                        }
                        int i2 = 0;
                        for (GpsSatellite usedInFix : e.this.a.getSatellites()) {
                            if (usedInFix.usedInFix()) {
                                i2++;
                            }
                        }
                        if (i2 < 3 && e.this.l >= 3) {
                            e.this.f = System.currentTimeMillis();
                        }
                        e.this.l = i2;
                        return;
                }
            }
        }
    }

    private class c implements LocationListener {
        private c() {
        }

        /* synthetic */ c(e eVar, c cVar) {
            this();
        }

        public void onLocationChanged(Location location) {
            if (location != null) {
                e.this.O = location;
            }
        }

        public void onProviderDisabled(String str) {
            e.this.O = (Location) null;
        }

        public void onProviderEnabled(String str) {
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
            switch (i) {
                case 0:
                    e.this.O = (Location) null;
                    return;
                default:
                    return;
            }
        }
    }

    private class d implements Runnable {
        private d() {
        }

        /* synthetic */ d(e eVar, d dVar) {
            this();
        }

        public void run() {
            if (!e.this.p && !e.this.Q) {
                e.this.e();
            }
        }
    }

    /* renamed from: com.baidu.location.e$e  reason: collision with other inner class name */
    private class C0000e extends PhoneStateListener {
        public C0000e() {
        }

        public void onCellLocationChanged(CellLocation cellLocation) {
            if (cellLocation != null) {
                try {
                    e.this.a(cellLocation);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class f implements Runnable {
        private f() {
        }

        /* synthetic */ f(e eVar, f fVar) {
            this();
        }

        public void run() {
            if (!e.this.p && !e.this.Q) {
                if (e.this.m16char() && e.this.m28int()) {
                    e.this.q = e.this.b;
                    e.this.P = e.this.G;
                    e.this.c = e.this.J;
                    e.this.f26goto = e.this.O;
                    if (e.this.f27if != null) {
                        int i = 0;
                        while (true) {
                            int i2 = i;
                            if (i2 >= e.this.f27if.size()) {
                                break;
                            }
                            ((a) e.this.f27if.get(i2)).a(e.this.f29long);
                            i = i2 + 1;
                        }
                        e.this.f32void = e.this.f29long;
                    }
                }
                e.this.A.postDelayed(e.this.D, (long) e.this.M);
            }
        }
    }

    public e(Context context) {
        this.k = context;
    }

    private String a(int i2, int i3, int i4, int i5, List list) {
        int size;
        StringBuffer stringBuffer = new StringBuffer(256);
        stringBuffer.append(String.format("&cl=%d|%d|%d|%d", Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5)));
        if (!(list == null || (size = list.size()) == 0)) {
            stringBuffer.append("&cls=");
            for (int i6 = 0; i6 < size; i6++) {
                int cid = ((NeighboringCellInfo) list.get(i6)).getCid();
                if (i6 != size - 1) {
                    stringBuffer.append(String.format("%d|%d|%d|%d;", Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(cid / 100000), Integer.valueOf(cid % 100000)));
                } else {
                    stringBuffer.append(String.format("%d|%d|%d|%d", Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(cid / 100000), Integer.valueOf(cid % 100000)));
                }
            }
        }
        int size2 = this.I.size();
        if (size2 > 0) {
            stringBuffer.append("&clt=");
            for (int i7 = 0; i7 < size2; i7++) {
                b bVar = (b) this.I.get(i7);
                if (i7 != size2 - 1) {
                    stringBuffer.append(String.format("%d|%d|%d|%d|%d;", Integer.valueOf(bVar.f17if), Integer.valueOf(bVar.a), Integer.valueOf(bVar.f15do), Integer.valueOf(bVar.f16for), Long.valueOf(bVar.f18int / 1000)));
                } else {
                    stringBuffer.append(String.format("%d|%d|%d|%d|%d;%d", Integer.valueOf(bVar.f17if), Integer.valueOf(bVar.a), Integer.valueOf(bVar.f15do), Integer.valueOf(bVar.f16for), Long.valueOf((System.currentTimeMillis() - bVar.f18int) / 1000), Integer.valueOf(this.M / 1000)));
                }
            }
        }
        return stringBuffer.toString();
    }

    private String a(Location location) {
        if (location.getLongitude() > -1.0E-4d && location.getLongitude() < e && location.getLatitude() > -1.0E-4d && location.getLatitude() < e) {
            return null;
        }
        return String.format("&ll=%.5f|%.5f&s=%.1f&d=%.1f", Double.valueOf(location.getLongitude()), Double.valueOf(location.getLatitude()), Float.valueOf((float) (((double) location.getSpeed()) * 3.6d)), Float.valueOf(location.getBearing()));
    }

    /* access modifiers changed from: private */
    public void a(CellLocation cellLocation) {
        try {
            Log.d("locSDK", "locsdk version 1.0.2.1");
            if (cellLocation instanceof GsmCellLocation) {
                this.b = ((GsmCellLocation) cellLocation).getLac();
                this.G = ((GsmCellLocation) cellLocation).getCid();
            } else {
                try {
                    if (Integer.parseInt(Build.VERSION.SDK) < 5) {
                        return;
                    }
                    if (!(h == null && -1 == m19else()) && h.isInstance(cellLocation)) {
                        Object invoke = S.invoke(cellLocation, new Object[0]);
                        if (invoke instanceof Integer) {
                            this.G = ((Integer) invoke).intValue();
                            Object invoke2 = t.invoke(cellLocation, new Object[0]);
                            if (invoke2 instanceof Integer) {
                                this.b = ((Integer) invoke2).intValue();
                            } else {
                                return;
                            }
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                    return;
                }
            }
            String networkOperator = this.g.getNetworkOperator();
            if (networkOperator == null || networkOperator.length() <= 0) {
                this.u = 0;
                this.R = 0;
            } else {
                try {
                    if (networkOperator.length() >= 3) {
                        this.u = Integer.valueOf(networkOperator.substring(0, 3)).intValue();
                    } else {
                        this.u = 0;
                    }
                    if (networkOperator.length() >= 5) {
                        this.R = Integer.valueOf(networkOperator.substring(3, 5)).intValue();
                    } else {
                        this.R = 0;
                    }
                } catch (Exception e3) {
                    Log.d("mcc+mnc", e3.getMessage());
                }
            }
            try {
                if (h != null && h.isInstance(cellLocation)) {
                    this.R = ((Integer) h.getMethod("getSystemId", new Class[0]).invoke(cellLocation, new Object[0])).intValue();
                }
            } catch (Exception e4) {
            }
            if (this.b > 0 || this.G > 0) {
                long currentTimeMillis = System.currentTimeMillis();
                int size = this.I.size();
                if (size != 0) {
                    b bVar = (b) this.I.get(size - 1);
                    if (bVar.f16for != this.G || bVar.f15do != this.b) {
                        bVar.f18int = currentTimeMillis - bVar.f18int;
                    } else {
                        return;
                    }
                }
                b bVar2 = new b();
                bVar2.f16for = this.G;
                bVar2.f15do = this.b;
                bVar2.f17if = this.u;
                bVar2.a = this.R;
                bVar2.f18int = currentTimeMillis;
                this.I.add(bVar2);
                if (this.I.size() > 3) {
                    this.I.remove(0);
                }
            } else if (this.I.size() != 0) {
                this.I.clear();
            }
        } catch (Exception e5) {
            e5.printStackTrace();
        }
    }

    private void a(List list) {
        boolean z2 = true;
        for (int size = list.size() - 1; size >= 1 && z2; size--) {
            z2 = false;
            for (int i2 = 0; i2 < size; i2++) {
                if (((ScanResult) list.get(i2)).level < ((ScanResult) list.get(i2 + 1)).level) {
                    list.set(i2 + 1, (ScanResult) list.get(i2));
                    list.set(i2, (ScanResult) list.get(i2 + 1));
                    z2 = true;
                }
            }
        }
    }

    private boolean a(List list, List list2) {
        if (list == list2) {
            return true;
        }
        if (list == null || list2 == null) {
            return false;
        }
        int size = list.size();
        int size2 = list2.size();
        if (size == 0 && size2 == 0) {
            return true;
        }
        if (size == 0 || size2 == 0) {
            return false;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < size; i3++) {
            String str = ((ScanResult) list.get(i3)).BSSID;
            if (str != null) {
                int i4 = 0;
                while (true) {
                    if (i4 >= size2) {
                        break;
                    } else if (str.equals(((ScanResult) list2.get(i4)).BSSID)) {
                        i2++;
                        break;
                    } else {
                        i4++;
                    }
                }
            }
        }
        return i2 > size2 / 2;
    }

    /* access modifiers changed from: private */
    /* renamed from: char  reason: not valid java name */
    public boolean m16char() {
        this.f29long = "";
        StringBuffer stringBuffer = new StringBuffer(1024);
        if (this.g != null) {
            a(this.g.getCellLocation());
        }
        String a2 = a(this.u, this.R, this.b, this.G, null);
        if (a2.length() > 0) {
            stringBuffer.append(a2);
        }
        if (this.O != null) {
            String a3 = a(this.O);
            if (a3.length() > 0) {
                stringBuffer.append(a3);
            }
        }
        if (this.J != null && this.J.size() > 0) {
            String str = m24if(this.J);
            if (str.length() > 0) {
                stringBuffer.append(str);
            }
        }
        if (stringBuffer.toString().length() <= 0) {
            return false;
        }
        stringBuffer.append("&addr=");
        stringBuffer.append(this.f28int);
        stringBuffer.append("&coor=");
        stringBuffer.append(this.U);
        stringBuffer.append("&os=android&prod=");
        stringBuffer.append(this.f30new);
        if (this.r != null && this.r.length() > 0) {
            stringBuffer.append("&im=");
            stringBuffer.append(this.r);
        }
        if (this.m != null && this.m.length() > 0) {
            stringBuffer.append("&mb=");
            stringBuffer.append(this.m);
        }
        this.f29long = String.valueOf(c.a(stringBuffer.toString())) + "|tp=2";
        return true;
    }

    /* access modifiers changed from: private */
    public void e() {
        String str;
        if (this.v == LocServiceMode.Immediat && this.j) {
            if (!m16char() || !m28int()) {
                str = null;
            } else {
                String str2 = this.f29long;
                this.q = this.b;
                this.P = this.G;
                this.c = this.J;
                this.f26goto = this.O;
                this.f32void = this.f29long;
                str = str2;
            }
            if (this.x != null && this.x.size() > 0) {
                Iterator it = this.x.iterator();
                while (it.hasNext()) {
                    ((ReceiveListener) it.next()).onReceive(str);
                }
            }
            this.j = false;
            if (!(this.A == null || this.E == null)) {
                this.A.removeCallbacks(this.E);
            }
            this.E = null;
        }
    }

    /* renamed from: else  reason: not valid java name */
    private int m19else() {
        try {
            h = Class.forName("android.telephony.cdma.CdmaCellLocation");
            S = h.getMethod("getBaseStationId", new Class[0]);
            t = h.getMethod("getNetworkId", new Class[0]);
            return 0;
        } catch (Exception e2) {
            e2.printStackTrace();
            return -1;
        }
    }

    /* renamed from: if  reason: not valid java name */
    private String m24if(List list) {
        StringBuffer stringBuffer = new StringBuffer(256);
        int size = list.size();
        a(list);
        if (size > 10) {
            size = 10;
        }
        for (int i2 = 0; i2 < size; i2++) {
            if (i2 != 0) {
                if (((ScanResult) list.get(i2)).level == 0) {
                    break;
                }
                stringBuffer.append("|");
                stringBuffer.append(((ScanResult) list.get(i2)).BSSID.replace(":", ""));
                int i3 = ((ScanResult) list.get(i2)).level;
                if (i3 < 0) {
                    i3 = -i3;
                }
                stringBuffer.append(String.format(";%d;", Integer.valueOf(i3)));
            } else if (((ScanResult) list.get(0)).level == 0) {
                return null;
            } else {
                stringBuffer.append("&wf=");
                stringBuffer.append(((ScanResult) list.get(i2)).BSSID.replace(":", ""));
                int i4 = ((ScanResult) list.get(0)).level;
                if (i4 < 0) {
                    i4 = -i4;
                }
                stringBuffer.append(String.format(";%d;", Integer.valueOf(i4)));
            }
        }
        return stringBuffer.toString();
    }

    /* access modifiers changed from: private */
    /* renamed from: int  reason: not valid java name */
    public boolean m28int() {
        if (this.J != null && System.currentTimeMillis() - this.F > 65000) {
            this.J = null;
        }
        if (this.f31try && this.l < 3 && this.O != null && System.currentTimeMillis() - this.f > 15000) {
            this.O = null;
            try {
                this.f21byte.removeUpdates(this.K);
                this.f21byte.requestLocationUpdates("gps", 1000, 20.0f, this.K);
            } catch (Exception e2) {
            }
        }
        if (this.O == null && ((this.J == null || this.J.size() == 0) && this.G <= 0 && this.b <= 0)) {
            return false;
        }
        if ((this.O != null && this.f26goto == null) || (this.O == null && this.f26goto != null)) {
            return true;
        }
        if (this.O != null && this.f26goto != null && (Math.abs(this.O.getLatitude() - this.f26goto.getLatitude()) > e || Math.abs(this.O.getLongitude() - this.f26goto.getLongitude()) > e)) {
            return true;
        }
        if (!a(this.J, this.c)) {
            return true;
        }
        return (this.q == this.b && this.P == this.G) ? false : true;
    }

    public void a(int i2) {
        if (this.p) {
            if (i2 < 3000) {
                this.M = 3000;
            } else {
                this.M = i2;
            }
        }
    }

    public void a(LocServiceMode locServiceMode) {
        this.v = locServiceMode;
    }

    public void a(ReceiveListener receiveListener) {
        if (this.x == null) {
            this.x = new ArrayList();
        }
        if (!this.x.contains(receiveListener)) {
            this.x.add(receiveListener);
        }
    }

    public void a(a aVar) {
        if (aVar != null && this.f27if != null && !this.f27if.contains(aVar)) {
            this.f27if.add(aVar);
        }
    }

    public void a(String str) {
        int length;
        if (str != null && (length = str.length()) > 0) {
            if (length <= 128) {
                this.f28int = str;
            } else {
                this.f28int = str.substring(0, 128);
            }
        }
    }

    public boolean a() {
        if (!this.p) {
            return false;
        }
        this.f31try = false;
        return true;
    }

    public boolean b() {
        if (!this.p) {
            return false;
        }
        this.f31try = true;
        return true;
    }

    /* renamed from: byte  reason: not valid java name */
    public boolean m33byte() {
        if (this.v != LocServiceMode.Immediat || this.A == null || this.E != null) {
            return false;
        }
        int i2 = 10;
        if (this.H.isWifiEnabled() && this.H.startScan()) {
            i2 = T;
        }
        this.j = true;
        this.E = new d(this, null);
        this.A.postDelayed(this.E, (long) i2);
        return true;
    }

    public void c() {
        if (!this.p) {
            this.p = true;
            if (!this.Q) {
                if (!(this.s == null || this.k == null)) {
                    this.k.unregisterReceiver(this.s);
                }
                if (this.f21byte != null) {
                    try {
                        if (this.K != null) {
                            this.f21byte.removeUpdates(this.K);
                        }
                        if (this.f23char != null) {
                            this.f21byte.removeGpsStatusListener(this.f23char);
                        }
                    } catch (Exception e2) {
                    }
                }
                if (!(this.g == null || this.o == null)) {
                    this.g.listen(this.o, 0);
                }
            }
            this.f21byte = null;
            this.H = null;
            this.g = null;
            this.s = null;
            if (this.A != null) {
                try {
                    if (this.D != null) {
                        this.A.removeCallbacks(this.D);
                    }
                    if (this.z != null) {
                        this.A.removeCallbacks(this.z);
                    }
                } catch (Exception e3) {
                    Log.d("stop:", e3.getMessage());
                }
            }
            this.A = null;
            this.D = null;
            this.z = null;
            this.E = null;
            this.f32void = null;
            this.O = null;
            this.f27if.clear();
            m39goto();
            if (this.I != null) {
                this.I.clear();
                this.I = null;
            }
        }
    }

    /* renamed from: case  reason: not valid java name */
    public boolean m34case() {
        return this.l >= 3 && this.O != null;
    }

    public void d() {
        if (!this.p && this.Q) {
            this.l = 0;
            this.Q = false;
            if (!(this.g == null || this.o == null)) {
                this.g.listen(this.o, 16);
            }
            if (!(this.H == null || this.s == null)) {
                this.k.registerReceiver(this.s, new IntentFilter("android.net.wifi.SCAN_RESULTS"));
            }
            if (!(this.f21byte == null || this.K == null || this.f23char == null)) {
                long j2 = (long) this.M;
                this.a = null;
                this.f21byte.requestLocationUpdates("gps", j2 < 1000 ? 1000 : j2 > 1000 ? 1000 : j2, 20.0f, this.K);
                this.f21byte.addGpsStatusListener(this.f23char);
            }
            if (!(this.H == null || this.A == null || this.z == null)) {
                this.A.postDelayed(this.z, 0);
            }
            if (this.A != null && this.D != null) {
                this.A.postDelayed(this.D, 2000);
            }
        }
    }

    /* renamed from: do  reason: not valid java name */
    public Location m35do() {
        return this.O;
    }

    /* renamed from: do  reason: not valid java name */
    public void m36do(String str) {
        if (str == null) {
            return;
        }
        if (str.length() < 32) {
            this.f30new = str;
        } else {
            this.f30new = str.substring(0, 32);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: do  reason: not valid java name */
    public void m37do(List list) {
        this.J = list;
        this.F = System.currentTimeMillis();
        e();
    }

    public int f() {
        if (!this.p) {
            return 1;
        }
        this.p = false;
        this.Q = false;
        this.l = -1;
        this.J = null;
        this.O = null;
        this.b = 0;
        this.G = 0;
        this.u = 0;
        this.R = 0;
        this.q = 0;
        this.P = 0;
        this.c = null;
        this.f26goto = null;
        this.f29long = null;
        this.f32void = null;
        this.F = 0;
        this.f = 0;
        this.I = new LinkedList();
        this.N = this.M;
        if (this.N < 3000) {
            this.N = 3000;
        } else if (this.N > 30000) {
            this.N = 30000;
        }
        if (this.g == null) {
            this.g = (TelephonyManager) this.k.getSystemService("phone");
            if (this.g != null) {
                this.o = new C0000e();
                this.g.listen(this.o, 16);
                this.r = this.g.getDeviceId();
                this.m = Build.MODEL;
            }
        }
        if (this.H == null) {
            this.H = (WifiManager) this.k.getSystemService("wifi");
            if (this.H != null) {
                if (this.s == null) {
                    this.s = new d(this);
                }
                this.k.registerReceiver(this.s, new IntentFilter("android.net.wifi.SCAN_RESULTS"));
            }
        }
        if (!this.f31try) {
            this.f21byte = null;
            this.K = null;
        } else if (this.f21byte == null) {
            this.f21byte = (LocationManager) this.k.getSystemService("location");
            if (this.f21byte != null) {
                this.a = null;
                this.K = new c(this, null);
                long j2 = (long) this.M;
                this.f21byte.requestLocationUpdates("gps", j2 < 1000 ? 1000 : j2 > 1000 ? 1000 : j2, 20.0f, this.K);
                this.f23char = new b(this, null);
                this.f21byte.addGpsStatusListener(this.f23char);
            }
        }
        if (this.A == null) {
            this.A = new Handler();
        }
        if (this.v == LocServiceMode.Background) {
            if (this.H != null && this.z == null) {
                this.z = new a(this, null);
                this.A.postDelayed(this.z, 0);
            }
            if (this.D == null) {
                this.D = new f(this, null);
                this.A.postDelayed(this.D, 2000);
            }
        } else {
            LocServiceMode locServiceMode = LocServiceMode.Immediat;
        }
        this.E = null;
        return 0;
    }

    /* renamed from: for  reason: not valid java name */
    public String m38for() {
        return this.r;
    }

    /* renamed from: goto  reason: not valid java name */
    public void m39goto() {
        if (this.x != null) {
            this.x.clear();
            this.x = null;
        }
    }

    /* renamed from: if  reason: not valid java name */
    public String m40if() {
        StringBuffer stringBuffer = new StringBuffer(1024);
        stringBuffer.append(this.i);
        stringBuffer.append(10);
        if (this.c != null) {
            stringBuffer.append("OrginWifiList:\n");
            int size = this.c.size();
            for (int i2 = 0; i2 < size; i2++) {
                if (i2 != 0) {
                    if (((ScanResult) this.c.get(i2)).level == 0) {
                        break;
                    }
                    stringBuffer.append("|");
                    stringBuffer.append(((ScanResult) this.c.get(i2)).BSSID.replace(":", ""));
                    stringBuffer.append(String.format(";%d;", Integer.valueOf(((ScanResult) this.c.get(i2)).level >= 0 ? ((ScanResult) this.c.get(i2)).level : -((ScanResult) this.c.get(i2)).level)));
                } else if (((ScanResult) this.c.get(0)).level == 0) {
                    return null;
                } else {
                    stringBuffer.append("&wf=");
                    stringBuffer.append(((ScanResult) this.c.get(i2)).BSSID.replace(":", ""));
                    stringBuffer.append(String.format(";%d;", Integer.valueOf(((ScanResult) this.c.get(0)).level >= 0 ? ((ScanResult) this.c.get(0)).level : -((ScanResult) this.c.get(0)).level)));
                }
            }
        }
        return stringBuffer.toString();
    }

    /* renamed from: if  reason: not valid java name */
    public void m41if(String str) {
        int length;
        if (str != null && (length = str.length()) > 0) {
            if (length <= 16) {
                this.U = str;
            } else {
                this.U = str.substring(0, 16);
            }
        }
    }

    /* renamed from: long  reason: not valid java name */
    public void m42long() {
        if (!this.p && !this.Q) {
            if (!(this.s == null || this.k == null)) {
                this.k.unregisterReceiver(this.s);
            }
            if (this.f21byte != null) {
                if (this.K != null) {
                    this.f21byte.removeUpdates(this.K);
                }
                if (this.f23char != null) {
                    this.f21byte.removeGpsStatusListener(this.f23char);
                }
            }
            if (!(this.g == null || this.o == null)) {
                this.g.listen(this.o, 0);
            }
            if (this.I != null) {
                this.I.clear();
            }
            if (this.A != null) {
                try {
                    if (this.D != null) {
                        this.A.removeCallbacks(this.D);
                    }
                    if (this.z != null) {
                        this.A.removeCallbacks(this.z);
                    }
                } catch (Exception e2) {
                    Log.d("stop timer:", e2.getMessage());
                }
            }
            this.J = null;
            this.O = null;
            this.f32void = null;
            this.b = 0;
            this.G = 0;
            this.u = 0;
            this.R = 0;
            this.c = null;
            this.f26goto = null;
            this.q = 0;
            this.P = 0;
            this.Q = true;
        }
    }

    /* renamed from: new  reason: not valid java name */
    public String m43new() {
        StringBuffer stringBuffer = new StringBuffer(1024);
        boolean z2 = false;
        if (this.O != null && this.l >= 3) {
            if (!(this.O == null || this.f26goto == null)) {
                if (Math.abs(this.O.getLatitude() - this.f26goto.getLatitude()) <= e && Math.abs(this.O.getLongitude() - this.f26goto.getLongitude()) <= e) {
                    return null;
                }
                z2 = true;
            }
            String a2 = a(this.O);
            if (a2.length() > 0) {
                stringBuffer.append(a2);
            }
        }
        if (this.H.isWifiEnabled()) {
            if (this.H != null && this.z == null) {
                this.z = new a(this, null);
                this.A.postDelayed(this.z, 0);
            }
            if (this.J != null && this.J.size() > 0) {
                if (!z2 && a(this.J, this.c)) {
                    return null;
                }
                String str = m24if(this.J);
                if (str.length() > 0) {
                    stringBuffer.append(str);
                }
                z2 = true;
            }
        }
        if ((!z2 && this.q == this.b) || this.P == this.G) {
            return null;
        }
        String a3 = a(this.u, this.R, this.b, this.G, null);
        if (a3.length() > 0) {
            stringBuffer.append(a3);
        }
        if (stringBuffer.toString().length() <= 0) {
            return null;
        }
        stringBuffer.append("&addr=");
        stringBuffer.append(this.f28int);
        stringBuffer.append("&coor=");
        stringBuffer.append(this.U);
        stringBuffer.append("&os=android&prod=");
        stringBuffer.append(this.f30new);
        if (this.r != null && this.r.length() > 0) {
            stringBuffer.append("&im=");
            stringBuffer.append(this.r);
        }
        return String.valueOf(c.a(stringBuffer.toString())) + "|tp=2";
    }

    /* renamed from: try  reason: not valid java name */
    public String m44try() {
        return this.f32void;
    }

    /* renamed from: void  reason: not valid java name */
    public void m45void() {
        if (this.f27if != null) {
            this.f27if.clear();
        }
    }
}
