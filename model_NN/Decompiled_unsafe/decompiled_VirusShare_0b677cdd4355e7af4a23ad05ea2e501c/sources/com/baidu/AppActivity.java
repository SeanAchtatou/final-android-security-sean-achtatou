package com.baidu;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

public class AppActivity extends Activity {
    private static final String a = AppActivity.class.getSimpleName();
    /* access modifiers changed from: private */
    public static List<App> b = new ArrayList();
    private static BitmapDrawable s;
    private static BitmapDrawable t;
    private static BitmapDrawable u;
    /* access modifiers changed from: private */
    public App c;
    /* access modifiers changed from: private */
    public Handler d = new Handler();
    /* access modifiers changed from: private */
    public ProgressDialog e;
    private boolean f = true;
    private ScrollView g;
    /* access modifiers changed from: private */
    public TextView h;
    /* access modifiers changed from: private */
    public TextView i;
    /* access modifiers changed from: private */
    public TextView j;
    /* access modifiers changed from: private */
    public TextView k;
    /* access modifiers changed from: private */
    public ImageView l;
    /* access modifiers changed from: private */
    public Button m;
    /* access modifiers changed from: private */
    public TextView n;
    /* access modifiers changed from: private */
    public HorizontalScrollView o;
    /* access modifiers changed from: private */
    public LinearLayout p;
    private LinearLayout q;
    /* access modifiers changed from: private */
    public ImageView r;

    static {
        Bitmap createBitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.RGB_565);
        createBitmap.setPixel(0, 0, -394759);
        s = new BitmapDrawable(createBitmap);
        Rect rect = new Rect(0, 0, 1, 27);
        Bitmap createBitmap2 = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap2);
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-2105377, -394759});
        gradientDrawable.setBounds(rect);
        gradientDrawable.draw(canvas);
        t = new BitmapDrawable(createBitmap2);
        Rect rect2 = new Rect(0, 0, 1, 23);
        Bitmap createBitmap3 = Bitmap.createBitmap(rect2.width(), rect2.height(), Bitmap.Config.ARGB_8888);
        createBitmap3.setPixel(0, 0, -2500135);
        createBitmap3.setPixel(0, rect2.bottom - 2, -1973791);
        createBitmap3.setPixel(0, rect2.bottom - 1, -4737097);
        Canvas canvas2 = new Canvas(createBitmap3);
        GradientDrawable gradientDrawable2 = new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-5000269, -1973791});
        gradientDrawable2.setBounds(rect2.left, 1, rect2.right, rect2.bottom - 2);
        gradientDrawable2.draw(canvas2);
        u = new BitmapDrawable(createBitmap3);
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        this.d.post(new aq(this, z));
    }

    /* access modifiers changed from: private */
    public void b(boolean z) {
        this.f = z;
        if (z) {
            setContentView(this.g);
            setTitle("应用详情查看");
            return;
        }
        setContentView(this.q);
        setTitle("应用截图查看");
    }

    private boolean b() {
        return this.f;
    }

    private void c() {
        String stringExtra = getIntent().getStringExtra("limg");
        this.c = App.a(getIntent().getStringExtra("clklogurl"), getIntent().getStringExtra("surl"), getIntent().getStringExtra("tit"), "", getIntent().getLongExtra("ad_charge", 0), !stringExtra.equals("") ? w.d(false, this, stringExtra) : null);
        if (b.contains(this.c)) {
            this.c = b.get(b.indexOf(this.c));
            a(false);
            return;
        }
        new ah(this, this).start();
    }

    /* access modifiers changed from: private */
    public void d() {
        bk.b("setApp", this.c.toString());
        if (this.c.n() == null && as.a().c(this.c)) {
            this.c.a(as.a().f(this.c));
        }
        this.d.post(new aj(this, this));
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        bk.b(a, "onCreate");
        getWindow().setBackgroundDrawable(s);
        setRequestedOrientation(1);
        this.e = ProgressDialog.show(this, "正在连接服务器", "连接中,请稍后..", true, true);
        if (b.size() > 20) {
            b.clear();
        }
        this.g = new ScrollView(this);
        b(true);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(1);
        linearLayout.setGravity(1);
        this.g.addView(linearLayout);
        TextView textView = new TextView(this);
        textView.setLayoutParams(new LinearLayout.LayoutParams(-1, t.getBitmap().getHeight()));
        textView.setBackgroundDrawable(t);
        linearLayout.addView(textView);
        LinearLayout linearLayout2 = new LinearLayout(this);
        linearLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        linearLayout2.setPadding(17, 0, 17, 17);
        linearLayout.addView(linearLayout2);
        this.l = new ImageView(this);
        this.l.setLayoutParams(new LinearLayout.LayoutParams(94, 94));
        linearLayout2.addView(this.l);
        LinearLayout linearLayout3 = new LinearLayout(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.setMargins(17, 0, 0, 0);
        linearLayout3.setLayoutParams(layoutParams);
        linearLayout3.setOrientation(1);
        linearLayout2.addView(linearLayout3);
        this.h = new TextView(this);
        this.h.setTextSize(22.0f);
        this.h.setLineSpacing(0.0f, 1.1f);
        this.h.setTextColor(-16777216);
        linearLayout3.addView(this.h);
        LinearLayout linearLayout4 = new LinearLayout(this);
        linearLayout3.addView(linearLayout4);
        this.i = new TextView(this);
        this.i.setTextSize(13.0f);
        linearLayout4.addView(this.i);
        this.j = new TextView(this);
        this.j.setTextSize(13.0f);
        linearLayout4.addView(this.j);
        LinearLayout linearLayout5 = new LinearLayout(this);
        linearLayout3.addView(linearLayout5);
        TextView textView2 = new TextView(this);
        textView2.setText("免费");
        textView2.setTextColor(-11565370);
        textView2.setTextSize(18.0f);
        linearLayout5.addView(textView2);
        this.m = new Button(this);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(105, 45);
        layoutParams2.setMargins(17, 0, 0, 0);
        this.m.setLayoutParams(layoutParams2);
        this.m.setTextSize(18.0f);
        this.m.setPadding(0, 0, 0, 0);
        this.m.setGravity(17);
        this.m.setText("安 装");
        linearLayout5.addView(this.m);
        TextView textView3 = new TextView(this);
        textView3.setLayoutParams(new LinearLayout.LayoutParams(-1, u.getBitmap().getHeight()));
        textView3.setBackgroundDrawable(u);
        textView3.setPadding(17, 0, 0, 0);
        textView3.setText("简介");
        textView3.setTextSize(16.0f);
        textView3.setTextColor(-1);
        linearLayout.addView(textView3);
        ScrollView scrollView = new ScrollView(this);
        scrollView.setLayoutParams(new LinearLayout.LayoutParams(-1, (int) j.i));
        scrollView.setPadding(17, 17, 17, 0);
        linearLayout.addView(scrollView);
        this.n = new TextView(this);
        this.n.setTextColor(-12105913);
        scrollView.addView(this.n);
        this.o = new HorizontalScrollView(this);
        this.o.setVisibility(8);
        this.o.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.o.setPadding(17, 17, 17, 17);
        linearLayout.addView(this.o);
        this.p = new LinearLayout(this);
        this.p.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        this.p.setOrientation(0);
        this.o.addView(this.p);
        TextView textView4 = new TextView(this);
        textView4.setLayoutParams(new LinearLayout.LayoutParams(-1, 1));
        textView4.setBackgroundColor(-1315861);
        linearLayout.addView(textView4);
        this.k = new TextView(this);
        this.k.setPadding(17, 17, 17, 17);
        this.k.setTextColor(-12105913);
        this.k.setTextSize(13.0f);
        this.k.setLineSpacing(0.0f, 1.2f);
        linearLayout.addView(this.k);
        this.q = new LinearLayout(this);
        this.q.setOrientation(1);
        this.q.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.r = new ImageView(this);
        this.r.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.q.addView(this.r);
        c();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        bk.b(a, "onDestroy");
        super.onDestroy();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        if (b()) {
            return super.onKeyDown(i2, keyEvent);
        }
        b(true);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        bk.b(a, "onPause");
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        bk.b(a, "onRestart");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        d();
        bk.b(a, "onResume");
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        bk.b(a, "onStart");
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        bk.b(a, "onStop");
        super.onStop();
    }
}
