package com.baidu;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewParent;
import android.widget.RelativeLayout;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

public final class AdView extends RelativeLayout {
    private static List<WeakReference<Context>> a = new ArrayList();
    /* access modifiers changed from: private */
    public Handler b;
    /* access modifiers changed from: private */
    public l c;
    private boolean d;
    private Timer e;
    private String f;
    private t g;
    private int h;
    private int i;
    private int j;
    private final int k;
    private final s l;
    private AdViewListener m;
    private int n;
    private int o;
    private boolean p;
    private boolean q;

    public AdView(Context context) {
        this(context, AdType.TEXT);
    }

    public AdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, AdType.TEXT);
    }

    AdView(Context context, AttributeSet attributeSet, int i2, AdType adType) {
        super(context, attributeSet, i2);
        AdType adType2;
        this.b = new Handler();
        this.h = -11184811;
        this.i = 255;
        this.j = -1;
        this.k = 30;
        this.l = new x(this);
        this.m = new y(this);
        this.p = true;
        setFocusable(false);
        setClickable(false);
        a(context);
        c.a(context);
        if (attributeSet != null) {
            String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
            setTextColor(Color.parseColor(attributeSet.getAttributeValue(str, "textColor")));
            setBackgroundColor(Color.parseColor(attributeSet.getAttributeValue(str, "backgroundColor")));
            setBackgroundTransparent(attributeSet.getAttributeIntValue(str, "backgroundTransparent", this.i));
            adType2 = AdType.parse(attributeSet.getAttributeUnsignedIntValue(str, "adType", AdType.TEXT.getValue()));
        } else {
            adType2 = adType;
        }
        a(adType2.a());
        b();
    }

    public AdView(Context context, AdType adType) {
        this(context, null, 0, adType);
    }

    /* access modifiers changed from: private */
    public ag a(int i2, int i3, Ad ad) {
        int i4;
        int i5;
        int i6 = r.i(getContext());
        int h2 = r.h(getContext());
        if (d() == t.TEXT || d() == t.APP) {
            int i7 = (i2 == 0 || i2 == -2) ? i6 : i2;
            if (i7 < 200 && i7 != -1) {
                i7 = 200;
            }
            i5 = (i3 == 0 || i3 == -2) ? (int) (((float) Math.min(i6, h2)) * 0.15f) : i3;
            if (i5 >= 30 || i5 == -1) {
                i4 = i7;
            } else {
                i5 = 30;
                i4 = i7;
            }
        } else if (d() == t.IMAGE) {
            int height = (int) ((((double) ad.c().getHeight()) / ((double) ad.c().getWidth())) * ((double) Math.min(i6, h2)));
            if (height == i6) {
                i4 = i6;
                i5 = (int) (((float) i6) * 0.15f);
            } else {
                int i8 = height;
                i4 = i6;
                i5 = i8;
            }
        } else {
            i5 = i3;
            i4 = i2;
        }
        return new ag(i4, i5);
    }

    private void a(Context context) {
        boolean z;
        r.m(context);
        int i2 = 0;
        while (true) {
            if (i2 >= a.size()) {
                z = false;
                break;
            } else if (a.get(i2).get() == context) {
                z = true;
                break;
            } else {
                i2++;
            }
        }
        if (z) {
            throw new SecurityException("More than one AdView instance in an activity!");
        }
        a.add(new WeakReference(context));
    }

    /* access modifiers changed from: private */
    public void a(t tVar) {
        this.g = tVar;
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        this.f = str;
    }

    private void a(boolean z) {
        this.p = z;
    }

    private void b() {
        new z(this, this).start();
    }

    private void b(boolean z) {
        this.q = z;
    }

    /* access modifiers changed from: private */
    public AdViewListener c() {
        return this.m;
    }

    private void c(boolean z) {
        if (z) {
            bk.b("Timer Start");
            if (this.e != null) {
                this.e.cancel();
            }
            this.e = new Timer();
            this.e.schedule(new ac(this), 30000, 30000);
        } else if (this.e != null) {
            bk.b("Timer Cancel");
            this.e.cancel();
            this.e = null;
        }
    }

    /* access modifiers changed from: private */
    public t d() {
        return this.g;
    }

    /* access modifiers changed from: private */
    public void d(boolean z) {
        this.d = z;
    }

    /* access modifiers changed from: private */
    public int e() {
        return 30;
    }

    /* access modifiers changed from: private */
    public void f() {
        if (p() && getVisibility() == 0) {
            bk.b("AdView.refresh");
            g();
            h();
        }
    }

    /* access modifiers changed from: private */
    public void g() {
        if (p()) {
            c(false);
            d(false);
        }
    }

    private void h() {
        if (p() && getVisibility() == 0) {
            bk.b("AdView.start", "AdSrc:" + d());
            k();
            c(true);
        }
    }

    private boolean i() {
        bk.b("AdView.hasSpam =>");
        if (c.a().f()) {
            bk.c("AdView", "没有可展示的推广 (No ad to show)");
            return true;
        } else if (c.a().e()) {
            bk.c("AdView", "App已被搁置 (App is hanged up)");
            return true;
        } else {
            ViewParent parent = getParent();
            while (parent != null) {
                if (!(parent instanceof View) || ((View) parent).getVisibility() == 0) {
                    parent = parent.getParent();
                } else {
                    bk.c("AdView", "AdView的一个祖先控件不为 View.VISIBLE (One of AdView's ancestor is not View.VISIBLE)");
                    return true;
                }
            }
            int[] iArr = new int[2];
            getLocationOnScreen(iArr);
            Rect rect = new Rect(iArr[0], iArr[1], iArr[0] + getWidth(), iArr[1] + getHeight());
            bk.b("AdView thisBox", rect.toShortString());
            if (getContext() instanceof Activity) {
                Rect rect2 = new Rect(0, ((Activity) getContext()).getWindow().findViewById(16908290).getTop(), r.i(getContext()), r.h(getContext()));
                bk.b("AdView availBox", rect2.toShortString());
                if (!rect2.contains(rect)) {
                    bk.c("AdView", "推广边界不完全在窗口内 (Ad is not in window)");
                    a(false);
                    int i2 = this.o;
                    this.o = i2 + 1;
                    if (i2 < 2) {
                    }
                    return true;
                }
                a(true);
            }
            if (getContext() instanceof Activity) {
            }
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        setBackgroundTransparent(this.i);
        if (l() == null) {
            return;
        }
        if (a()) {
            l().setClickable(false);
            l().setFocusable(false);
            return;
        }
        l().setClickable(true);
        l().setFocusable(true);
    }

    /* access modifiers changed from: private */
    public void k() {
        bk.b("requestFreshAd");
        try {
            b(i());
        } catch (Exception e2) {
            bk.a("requestFreshAd", e2);
        }
        if (this.d) {
            bk.b("Ignoring requestFreshAd() because we are already getting a fresh ad.");
        } else if (getVisibility() != 0) {
            g();
        } else {
            d(true);
            this.b.post(new ab(this, this));
        }
    }

    /* access modifiers changed from: private */
    public l l() {
        return this.c;
    }

    /* access modifiers changed from: private */
    public void m() {
        if (l() != null && l().d()) {
            new ae(this, l().h(), l().c()).start();
        }
    }

    /* access modifiers changed from: private */
    public String n() {
        return this.f;
    }

    /* access modifiers changed from: private */
    public s o() {
        return this.l;
    }

    private boolean p() {
        return l() != null;
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.q;
    }

    public AdType getAdType() {
        return this.g.c();
    }

    public int getBackgroundColor() {
        return this.h;
    }

    public int getBackgroundTransparent() {
        return this.i;
    }

    public int getTextColor() {
        return this.j;
    }

    public int getVisibility() {
        return this.n;
    }

    public boolean isInWindow() {
        return this.p;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        bk.b("AdView.onDetachedFromWindow");
        if (p()) {
            m();
            c.a().a(d(), e());
            super.onDetachedFromWindow();
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i2) {
        this.n = i2;
        super.onWindowVisibilityChanged(i2);
        bk.b("AdView.onWindowVisibilityChanged", String.format("visibility: %s dim(%d, %d)", Integer.valueOf(i2), Integer.valueOf(getWidth()), Integer.valueOf(getHeight())));
        if (p()) {
            if (i2 != 0) {
                g();
                c.a().a(d(), e());
            } else if (getWidth() != 0 && getHeight() != 0) {
                c(true);
            }
        }
    }

    public void refresh() {
        if (!p() || !r.c(getContext())) {
            bk.b("AdView.refresh", "Not in TestMode or not inited");
        } else {
            f();
        }
    }

    public void setAlpha(float f2) {
        bk.b("AdView.setAlpha", "override, do nothing");
    }

    public void setBackgroundColor(int i2) {
        this.h = i2;
        if (l() != null) {
            l().setBackgroundColor(i2);
        }
    }

    public void setBackgroundTransparent(int i2) {
        int i3 = 127;
        if (i2 > 255) {
            i3 = 255;
        } else if (i2 >= 127) {
            i3 = i2;
        }
        this.i = i3;
        if (l() != null) {
            l l2 = l();
            if (a()) {
                i3 = 0;
            }
            l2.b(i3);
        }
    }

    public void setListener(AdViewListener adViewListener) {
        this.m = adViewListener;
    }

    public void setTextColor(int i2) {
        this.j = i2;
        if (l() != null) {
            l().a(i2);
        }
    }

    public void setVisibility(int i2) {
        if (getVisibility() != i2) {
            this.n = i2;
            super.setVisibility(i2);
            bk.b("AdView.setVisibility", "visibility:" + i2);
            if (p()) {
                synchronized (this) {
                    int childCount = getChildCount();
                    for (int i3 = 0; i3 < childCount; i3++) {
                        getChildAt(i3).setVisibility(i2);
                    }
                }
                if (i2 != 0) {
                    m();
                }
                onWindowVisibilityChanged(i2);
            }
        }
    }
}
