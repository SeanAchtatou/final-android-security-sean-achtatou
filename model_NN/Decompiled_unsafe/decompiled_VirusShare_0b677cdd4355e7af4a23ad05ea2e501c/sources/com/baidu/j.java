package com.baidu;

import org.json.JSONException;

class j extends Thread {
    final /* synthetic */ boolean a;
    final /* synthetic */ c b;

    j(c cVar, boolean z) {
        this.b = cVar;
        this.a = z;
    }

    public void run() {
        synchronized (this.b.k) {
            try {
                this.b.k.put("stopped", this.a);
                this.b.u();
                bk.b("AdCache.setStopped", this.a + "");
            } catch (JSONException e) {
                bk.a("AdCache.setStopped", e);
            }
        }
    }
}
