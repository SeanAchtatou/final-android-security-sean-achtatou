package com.baidu;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

class as {
    private static as b = new as();
    private static boolean c;
    ActivityManager a;
    private Timer d;
    /* access modifiers changed from: private */
    public List<bl> e = new ArrayList();
    /* access modifiers changed from: private */
    public List<bl> f = new ArrayList();
    /* access modifiers changed from: private */
    public WeakReference<Context> g;
    /* access modifiers changed from: private */
    public PackageManager h;

    as() {
    }

    public static as a() {
        return b;
    }

    /* access modifiers changed from: private */
    public boolean a(List<PackageInfo> list, bl blVar) {
        if (blVar.n() == null) {
            return false;
        }
        for (PackageInfo packageInfo : list) {
            if (packageInfo.packageName.equals(blVar.n())) {
                return true;
            }
        }
        return false;
    }

    private void b() {
        this.d = new Timer();
        this.d.schedule(new at(this), 5000, 5000);
    }

    private String g(bl blVar) {
        return String.format("%s/download/%s", Environment.getExternalStorageDirectory(), blVar.f());
    }

    public void a(Context context) {
        this.g = new WeakReference<>(context);
        this.h = context.getPackageManager();
        this.a = (ActivityManager) context.getSystemService("activity");
        if (!c) {
            c = true;
            b.b();
        }
    }

    public void a(bl blVar) {
        if (!this.e.contains(blVar)) {
            bk.b("append to downloadings" + blVar);
            this.e.add(blVar);
        }
    }

    public boolean b(bl blVar) {
        return this.e.contains(blVar);
    }

    public boolean c(bl blVar) {
        return !"".equals(blVar.f()) && w.a(g(blVar), true);
    }

    public boolean d(bl blVar) {
        return a(this.h.getInstalledPackages(0), blVar);
    }

    public void e(bl blVar) {
        if (d(blVar)) {
            bk.b(String.format("[%s] previously installed", blVar.n()));
            return;
        }
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.parse("file://" + g(blVar)), "application/vnd.android.package-archive");
        this.g.get().startActivity(intent);
        bk.b(String.format("[%s] to install", blVar.n()));
        if (!this.f.contains(blVar)) {
            this.f.add(blVar);
        }
    }

    public String f(bl blVar) {
        try {
            return this.h.getPackageArchiveInfo(g(blVar), 0).packageName;
        } catch (Exception e2) {
            bk.a(e2);
            return null;
        }
    }
}
