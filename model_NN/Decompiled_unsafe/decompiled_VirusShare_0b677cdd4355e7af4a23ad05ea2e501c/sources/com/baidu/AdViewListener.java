package com.baidu;

public interface AdViewListener {
    void onAdSwitch();

    void onReceiveFail(FailReason failReason);

    void onReceiveSuccess();
}
