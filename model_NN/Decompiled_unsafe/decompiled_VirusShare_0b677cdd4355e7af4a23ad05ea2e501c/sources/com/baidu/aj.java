package com.baidu;

import android.app.Activity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.baidu.App;

class aj implements Runnable {
    final /* synthetic */ Activity a;
    final /* synthetic */ AppActivity b;

    aj(AppActivity appActivity, Activity activity) {
        this.b = appActivity;
        this.a = activity;
    }

    public void run() {
        this.b.h.setText(this.b.c.j());
        if (this.b.c.e() != null) {
            this.b.l.setImageBitmap(this.b.c.e());
        }
        this.b.i.setText("已下载 " + this.b.c.d());
        this.b.n.setText(this.b.c.k());
        this.b.k.setText(String.format("开发者：%s\n版本：%s\n发布日期：%s\nAPP大小：%s\n类别：%s", this.b.c.g(), this.b.c.l(), this.b.c.h(), this.b.c.i(), this.b.c.m()));
        this.b.j.setText("   ");
        this.b.m.setEnabled(false);
        if (as.a().d(this.b.c)) {
            this.b.j.append("应用已安装");
        } else if (as.a().c(this.b.c)) {
            this.b.j.append("应用已下载");
            this.b.m.setEnabled(true);
            this.b.m.setOnClickListener(new ak(this));
        } else if (as.a().b(this.b.c)) {
            this.b.j.append("下载中...");
        } else {
            this.b.j.append("");
            this.b.m.setEnabled(true);
            this.b.m.setOnClickListener(new al(this));
        }
        if (this.b.c.a().size() > 0) {
            this.b.o.setVisibility(0);
            this.b.p.removeAllViews();
            for (App.Snag next : this.b.c.a()) {
                ImageView imageView = new ImageView(this.a);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(135, 67);
                if (this.b.c.a().indexOf(next) != this.b.c.a().size() - 1) {
                    layoutParams.setMargins(0, 0, 17, 0);
                }
                imageView.setLayoutParams(layoutParams);
                imageView.setImageBitmap(next.getThumbBmp());
                imageView.setOnClickListener(new an(this, next));
                this.b.p.addView(imageView);
            }
        }
    }
}
