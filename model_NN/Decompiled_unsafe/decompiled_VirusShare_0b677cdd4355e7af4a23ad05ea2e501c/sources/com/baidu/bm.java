package com.baidu;

import android.telephony.PhoneStateListener;

class bm extends PhoneStateListener {
    private l a;

    private l a() {
        return this.a;
    }

    public void a(l lVar) {
        this.a = lVar;
    }

    public void onCallStateChanged(int i, String str) {
        super.onCallStateChanged(i, str);
        switch (i) {
            case 0:
                try {
                    bk.b("CALL_STATE_IDLE");
                    return;
                } catch (Exception e) {
                    bk.a("onCallStateChanged", e);
                }
            case 1:
                bk.b("CALL_STATE_RINGING");
                return;
            case 2:
                bk.b(String.format("CALL_STATE_OFFHOOK in:%s", str));
                if (a() == null) {
                    return;
                }
                if (str == null || str.equals("")) {
                    bk.b(String.format("CALL_STATE_OFFHOOK in:%s adcon:%s", str, a().h().getPhone()));
                    a().e();
                    a(null);
                    return;
                }
                return;
            default:
                return;
        }
        bk.a("onCallStateChanged", e);
    }
}
