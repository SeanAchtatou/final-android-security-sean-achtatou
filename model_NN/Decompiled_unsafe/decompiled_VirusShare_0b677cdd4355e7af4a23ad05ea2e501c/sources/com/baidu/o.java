package com.baidu;

import android.content.Context;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.Vector;

class o {
    private static Map<t, o> a = new HashMap();
    private q b = new q(this);
    private Vector<Ad> c;
    private int d = 0;
    private Timer e;
    private boolean f;
    /* access modifiers changed from: private */
    public t g;

    static {
        for (t tVar : t.a()) {
            a.put(tVar, new o(tVar));
        }
    }

    private o(t tVar) {
        this.g = tVar;
    }

    public static o a(t tVar) {
        return a.get(tVar);
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        this.f = z;
    }

    private boolean a() {
        return this.f;
    }

    /* access modifiers changed from: private */
    public boolean b() {
        return this.c == null || this.c.size() <= 0;
    }

    public Ad a(Context context, String str, s sVar) {
        if (a()) {
            a(false);
            c.a().c();
            this.c = null;
            this.d = 0;
        }
        if (this.c == null) {
            this.c = c.a().a(this.g);
            this.d = 0;
        }
        this.b.a(context, str, sVar);
        if (this.e == null) {
            this.e = new Timer();
            this.e.schedule(this.b, 0, 3600000);
        }
        if (this.c == null || this.c.size() <= 0) {
            return null;
        }
        Vector<Ad> vector = this.c;
        int i = this.d;
        this.d = i + 1;
        Ad elementAt = vector.elementAt(i % this.c.size());
        bk.b("AdLoader.fetchAd", String.format("[%d] %s", Integer.valueOf(this.d - 1), elementAt.toString()));
        return elementAt;
    }
}
