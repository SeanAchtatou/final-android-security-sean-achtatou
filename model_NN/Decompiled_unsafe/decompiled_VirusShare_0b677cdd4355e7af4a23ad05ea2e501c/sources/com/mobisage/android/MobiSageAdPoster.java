package com.mobisage.android;

import android.content.Context;
import android.util.AttributeSet;

public final class MobiSageAdPoster extends C0012l {
    public final /* bridge */ /* synthetic */ String getCustomData() {
        return super.getCustomData();
    }

    public final /* bridge */ /* synthetic */ String getKeyword() {
        return super.getKeyword();
    }

    public final /* bridge */ /* synthetic */ void setCustomData(String x0) {
        super.setCustomData(x0);
    }

    public final /* bridge */ /* synthetic */ void setKeyword(String x0) {
        super.setKeyword(x0);
    }

    public final /* bridge */ /* synthetic */ void setMobiSageAdViewListener(IMobiSageAdViewListener x0) {
        super.setMobiSageAdViewListener(x0);
    }

    public MobiSageAdPoster(Context context, int adSize, String publisherID, String keyword, String customData) {
        super(context, adSize, publisherID, keyword, customData);
    }

    public MobiSageAdPoster(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /* access modifiers changed from: protected */
    public final void initMobiSageAdView(Context context) {
        this.adSize = MobiSageAdSize.a(this.adSize);
        super.initMobiSageAdView(context);
    }

    public final void startRequestAd() {
        super.requestADFromDE();
    }

    public final void destoryAdPoster() {
        super.destoryAdView();
    }
}
