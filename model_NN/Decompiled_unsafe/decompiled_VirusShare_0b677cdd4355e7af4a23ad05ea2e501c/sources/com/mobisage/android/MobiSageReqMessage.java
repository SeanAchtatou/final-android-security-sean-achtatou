package com.mobisage.android;

import android.os.Bundle;

public class MobiSageReqMessage extends MobiSageMessage {
    public Bundle params = new Bundle();

    public MobiSageReqMessage() {
        this.b = 1;
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
        this.params.clear();
    }

    public Runnable createMessageRunnable() {
        return new J(this);
    }
}
