package com.mobisage.android;

final class Z extends N {
    Z(X x) {
        super(x);
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        java.lang.Thread.sleep(1000, 0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x008e, code lost:
        if (r5.a.callback != null) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0090, code lost:
        r5.a.callback.onMobiSageMessageFinish(r5.a);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x009a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x009f, code lost:
        if (r5.a.callback != null) goto L_0x00a1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00a1, code lost:
        r5.a.callback.onMobiSageMessageFinish(r5.a);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00aa, code lost:
        throw r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x009a A[ExcHandler: all (r0v0 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:0:0x0000] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void run() {
        /*
            r5 = this;
            org.apache.http.conn.scheme.SchemeRegistry r0 = new org.apache.http.conn.scheme.SchemeRegistry     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            r0.<init>()     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            org.apache.http.conn.scheme.Scheme r1 = new org.apache.http.conn.scheme.Scheme     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            java.lang.String r2 = "http"
            org.apache.http.conn.scheme.PlainSocketFactory r3 = org.apache.http.conn.scheme.PlainSocketFactory.getSocketFactory()     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            r4 = 80
            r1.<init>(r2, r3, r4)     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            r0.register(r1)     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            org.apache.http.conn.scheme.Scheme r1 = new org.apache.http.conn.scheme.Scheme     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            java.lang.String r2 = "https"
            com.mobisage.android.SNSSSLSocketFactory r3 = new com.mobisage.android.SNSSSLSocketFactory     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            r3.<init>()     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            r4 = 443(0x1bb, float:6.21E-43)
            r1.<init>(r2, r3, r4)     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            r0.register(r1)     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            org.apache.http.params.BasicHttpParams r1 = new org.apache.http.params.BasicHttpParams     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            r1.<init>()     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            java.lang.String r2 = "http.connection.timeout"
            r3 = 5000(0x1388, float:7.006E-42)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            r1.setParameter(r2, r3)     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            java.lang.String r2 = "http.socket.timeout"
            r3 = 5000(0x1388, float:7.006E-42)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            r1.setParameter(r2, r3)     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            org.apache.http.HttpVersion r2 = org.apache.http.HttpVersion.HTTP_1_1     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            org.apache.http.params.HttpProtocolParams.setVersion(r1, r2)     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            java.lang.String r2 = "UTF-8"
            org.apache.http.params.HttpProtocolParams.setContentCharset(r1, r2)     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            org.apache.http.impl.conn.SingleClientConnManager r2 = new org.apache.http.impl.conn.SingleClientConnManager     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            r2.<init>(r1, r0)     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            org.apache.http.impl.client.DefaultHttpClient r0 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            r0.<init>(r2, r1)     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            com.mobisage.android.MobiSageMessage r1 = r5.a     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            org.apache.http.client.methods.HttpRequestBase r1 = r1.createHttpRequest()     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            org.apache.http.HttpResponse r0 = r0.execute(r1)     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            com.mobisage.android.MobiSageMessage r1 = r5.a     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            android.os.Bundle r1 = r1.result     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            java.lang.String r2 = "StatusCode"
            org.apache.http.StatusLine r0 = r0.getStatusLine()     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            int r0 = r0.getStatusCode()     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            r1.putInt(r2, r0)     // Catch:{ Exception -> 0x0083, all -> 0x009a }
            com.mobisage.android.MobiSageMessage r0 = r5.a
            com.mobisage.android.IMobiSageMessageCallback r0 = r0.callback
            if (r0 == 0) goto L_0x007f
            com.mobisage.android.MobiSageMessage r0 = r5.a
            com.mobisage.android.IMobiSageMessageCallback r0 = r0.callback
            com.mobisage.android.MobiSageMessage r1 = r5.a
            r0.onMobiSageMessageFinish(r1)
        L_0x007f:
            super.run()
            return
        L_0x0083:
            r0 = move-exception
            r0 = 1000(0x3e8, double:4.94E-321)
            r2 = 0
            java.lang.Thread.sleep(r0, r2)     // Catch:{ InterruptedException -> 0x00ab, all -> 0x009a }
        L_0x008a:
            com.mobisage.android.MobiSageMessage r0 = r5.a
            com.mobisage.android.IMobiSageMessageCallback r0 = r0.callback
            if (r0 == 0) goto L_0x007f
            com.mobisage.android.MobiSageMessage r0 = r5.a
            com.mobisage.android.IMobiSageMessageCallback r0 = r0.callback
            com.mobisage.android.MobiSageMessage r1 = r5.a
            r0.onMobiSageMessageFinish(r1)
            goto L_0x007f
        L_0x009a:
            r0 = move-exception
            com.mobisage.android.MobiSageMessage r1 = r5.a
            com.mobisage.android.IMobiSageMessageCallback r1 = r1.callback
            if (r1 == 0) goto L_0x00aa
            com.mobisage.android.MobiSageMessage r1 = r5.a
            com.mobisage.android.IMobiSageMessageCallback r1 = r1.callback
            com.mobisage.android.MobiSageMessage r2 = r5.a
            r1.onMobiSageMessageFinish(r2)
        L_0x00aa:
            throw r0
        L_0x00ab:
            r0 = move-exception
            goto L_0x008a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.mobisage.android.Z.run():void");
    }
}
