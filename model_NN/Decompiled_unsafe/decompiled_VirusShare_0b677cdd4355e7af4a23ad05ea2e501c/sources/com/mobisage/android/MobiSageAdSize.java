package com.mobisage.android;

import com.android.mmreader1062.chartact;
import com.tencent.mobwin.core.a.a;
import com.tencent.mobwin.utils.b;

public final class MobiSageAdSize {
    public static final int Poster_320X320 = 22;
    public static final int Poster_480X480 = 21;
    public static final int Poster_480X800 = 20;
    public static final int Size_320X270 = 2;
    public static final int Size_320X48 = 3;
    public static final int Size_480X40 = 1;
    public static final int Size_480X64 = 7;
    public static final int Size_480X72 = 9;
    public static final int Size_48X48 = 15;
    public static final int Size_540X80 = 8;
    public static final int Size_748X110 = 4;
    public static final int Size_No_Banner = 25;

    static final int a(int i) {
        switch (i) {
            case Poster_480X800 /*20*/:
                return 20;
            case 21:
                return 21;
            case 22:
            default:
                return 22;
        }
    }

    static final int b(int i) {
        switch (i) {
            case 1:
            case 7:
            case 9:
            case Poster_480X800 /*20*/:
            case 21:
                return 480;
            case 2:
                return b.a;
            case 3:
                return b.a;
            case 4:
                return 748;
            case 5:
            case 6:
            case 10:
            case 11:
            case 12:
            case 13:
            case t.n /*14*/:
            case t.p /*16*/:
            case 17:
            case 18:
            case 19:
            case chartact.EXIT_ID:
            case a.n /*24*/:
            default:
                return 0;
            case 8:
                return 540;
            case 15:
                return 48;
            case 22:
                return b.a;
            case Size_No_Banner /*25*/:
                return 1;
        }
    }

    static final int c(int i) {
        switch (i) {
            case 1:
                return 40;
            case 2:
                return 270;
            case 3:
            case 15:
                return 48;
            case 4:
                return 110;
            case 5:
            case 6:
            case 10:
            case 11:
            case 12:
            case 13:
            case t.n /*14*/:
            case t.p /*16*/:
            case 17:
            case 18:
            case 19:
            case chartact.EXIT_ID:
            case a.n /*24*/:
            default:
                return 0;
            case 7:
                return 64;
            case 8:
                return 80;
            case 9:
                return 72;
            case Poster_480X800 /*20*/:
                return 800;
            case 21:
                return 480;
            case 22:
                return b.a;
            case Size_No_Banner /*25*/:
                return 1;
        }
    }

    static int a() {
        int i;
        int i2;
        int i3 = C0025y.b;
        int i4 = i3 - b.a;
        int i5 = i3 - 480;
        if (i5 < 0 || i4 <= i5) {
            i = i4;
            i2 = 3;
        } else {
            i2 = 9;
            i = i5;
        }
        int i6 = i3 - 540;
        if (i6 >= 0 && i > i6) {
            i2 = 8;
            i = i6;
        }
        int i7 = i3 - 748;
        if (i7 < 0 || i <= i7) {
            return i2;
        }
        return 4;
    }
}
