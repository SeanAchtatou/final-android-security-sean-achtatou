package com.mobisage.android;

import MobWin.cnst.PROTOCOL_ENCODING;
import android.os.Handler;
import android.os.Message;
import com.mobisage.android.SNSSSLSocketFactory;
import com.taobao.munion.ads.clientSDK.TaoAdsListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.UUID;
import org.apache.http.util.EncodingUtils;

/* renamed from: com.mobisage.android.i  reason: case insensitive filesystem */
final class C0009i extends O {
    private a g = new a(this, (byte) 0);

    public C0009i(Handler handler) {
        super(handler);
        this.c = 1005;
        this.d.add(new C0008h());
    }

    public final void a(Message message) {
        if (message.obj instanceof C0002b) {
            if (b((C0002b) message.obj)) {
                c((C0002b) message.obj);
                return;
            }
            C0002b bVar = (C0002b) message.obj;
            this.e.put(bVar.b, bVar);
            I i = new I();
            i.callback = this.g;
            i.params = bVar.c;
            bVar.f.add(i);
            this.f.put(i.c, bVar.b);
            H.a().a(i);
        } else if (message.obj instanceof I) {
            I i2 = (I) message.obj;
            if (this.f.containsKey(i2.c)) {
                UUID uuid = (UUID) this.f.get(i2.c);
                if (this.e.containsKey(uuid)) {
                    C0002b bVar2 = (C0002b) this.e.get(uuid);
                    this.f.remove(i2.c);
                    bVar2.f.remove(i2);
                    int i3 = i2.result.getInt("StatusCode");
                    if (i3 == 200 || i3 == 302) {
                        String string = EncodingUtils.getString(i2.result.getByteArray("ResponseBody"), PROTOCOL_ENCODING.value);
                        if (string == null || string.length() == 0) {
                            c(bVar2);
                            return;
                        }
                        ArrayList arrayList = new ArrayList();
                        String a2 = a(string, bVar2, arrayList);
                        if (a2 == null) {
                            c(bVar2);
                            return;
                        }
                        Iterator<C0002b> it = bVar2.e.iterator();
                        while (it.hasNext()) {
                            Message obtainMessage = this.a.obtainMessage(TaoAdsListener.EVENT_TYPE_HOSTS_MODIFIED);
                            obtainMessage.obj = it.next();
                            obtainMessage.sendToTarget();
                        }
                        bVar2.d.putString("BannerHTML", a2);
                        if (arrayList.size() != 0) {
                            bVar2.d.putStringArrayList("LpgCache", arrayList);
                        }
                        if (bVar2.a()) {
                            this.e.remove(bVar2.b);
                            if (bVar2.g != null) {
                                bVar2.g.a(bVar2);
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    c(bVar2);
                }
            }
        }
    }

    private String a(String str, C0002b bVar, ArrayList<String> arrayList) {
        String str2;
        LinkedList linkedList = new LinkedList();
        if (!SNSSSLSocketFactory.a.a(str, linkedList)) {
            return null;
        }
        if (!SNSSSLSocketFactory.a.b(str, linkedList)) {
            return null;
        }
        int i = 0;
        String str3 = str;
        while (i < linkedList.size()) {
            String str4 = (String) linkedList.get(i);
            if (str3.indexOf(str4) != -1) {
                StringBuilder sb = new StringBuilder();
                StringBuilder sb2 = new StringBuilder();
                if (!SNSSSLSocketFactory.a.a(null, str4, sb, sb2)) {
                    return null;
                }
                String substring = str4.substring(str4.lastIndexOf(".") + 1);
                if (substring.equals("htm") || substring.equals("html")) {
                    arrayList.add("file://" + ((Object) sb));
                }
                C0002b bVar2 = new C0002b();
                bVar2.c.putString("SourceURL", str4);
                bVar2.c.putString("TempURL", sb2.toString());
                bVar2.c.putString("TargetURL", sb.toString());
                bVar2.g = this.b;
                bVar2.a = bVar.b;
                bVar.e.add(bVar2);
                str2 = str3.replace(str4, "file://" + ((Object) sb));
            } else {
                str2 = str3;
            }
            i++;
            str3 = str2;
        }
        return str3;
    }

    /* renamed from: com.mobisage.android.i$a */
    class a implements IMobiSageMessageCallback {
        private a() {
        }

        /* synthetic */ a(C0009i iVar, byte b) {
            this();
        }

        public final void onMobiSageMessageFinish(MobiSageMessage msg) {
            Message obtainMessage = C0009i.this.a.obtainMessage(1005);
            obtainMessage.obj = msg;
            obtainMessage.sendToTarget();
        }
    }
}
