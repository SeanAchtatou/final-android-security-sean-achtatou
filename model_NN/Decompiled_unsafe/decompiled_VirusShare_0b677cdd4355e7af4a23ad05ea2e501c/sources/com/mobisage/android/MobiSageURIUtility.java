package com.mobisage.android;

import java.net.URLDecoder;
import java.util.HashMap;

public abstract class MobiSageURIUtility {
    public static HashMap<String, String> parserURIQuery(String queryText) {
        HashMap<String, String> hashMap = new HashMap<>();
        StringBuilder sb = new StringBuilder();
        String str = null;
        for (int i = 0; i < queryText.length(); i++) {
            char charAt = queryText.charAt(i);
            switch (charAt) {
                case '&':
                    hashMap.put(str, URLDecoder.decode(sb.toString()));
                    sb.delete(0, sb.length());
                    str = null;
                    break;
                case '=':
                    str = sb.toString();
                    sb.delete(0, sb.length());
                    break;
                default:
                    sb.append(charAt);
                    break;
            }
        }
        if (!(sb.length() == 0 || str == null)) {
            hashMap.put(str, URLDecoder.decode(sb.toString()));
        }
        return hashMap;
    }
}
