package com.mobisage.android;

import MobWin.cnst.PROTOCOL_ENCODING;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpProtocolParams;

/* renamed from: com.mobisage.android.q  reason: case insensitive filesystem */
final class C0017q extends N {
    private InputStream c;
    private BufferedInputStream d;
    private FileOutputStream e;

    public C0017q(MobiSageResMessage mobiSageResMessage) {
        super(mobiSageResMessage);
    }

    public final void a() {
        super.a();
        try {
            if (this.d != null) {
                this.d.close();
            }
            this.d = null;
            if (this.c != null) {
                this.c.close();
            }
            this.c = null;
            if (this.e != null) {
                this.e.close();
            }
            this.e = null;
        } catch (IOException e2) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public final void run() {
        String str;
        String str2;
        String str3;
        MobiSageResMessage mobiSageResMessage = (MobiSageResMessage) this.a;
        try {
            SchemeRegistry schemeRegistry = new SchemeRegistry();
            schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            schemeRegistry.register(new Scheme("https", new SNSSSLSocketFactory(), 443));
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            basicHttpParams.setParameter("http.connection.timeout", 5000);
            basicHttpParams.setParameter("http.socket.timeout", 5000);
            HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(basicHttpParams, PROTOCOL_ENCODING.value);
            this.b = new DefaultHttpClient(new SingleClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
            HttpResponse execute = this.b.execute(mobiSageResMessage.createHttpRequest());
            int statusCode = execute.getStatusLine().getStatusCode();
            mobiSageResMessage.result.putInt("StatusCode", statusCode);
            if (statusCode < 400) {
                int parseInt = Integer.parseInt(execute.getHeaders("Content-Length")[0].getValue());
                File file = new File(mobiSageResMessage.tempURL);
                if (!file.exists()) {
                    file.createNewFile();
                }
                if (mobiSageResMessage.d.booleanValue() && statusCode != 206) {
                    file.delete();
                    file.createNewFile();
                }
                long currentTimeMillis = System.currentTimeMillis();
                a((int) file.length(), parseInt);
                this.c = execute.getEntity().getContent();
                this.d = new BufferedInputStream(this.c);
                this.e = new FileOutputStream(file, true);
                byte[] bArr = new byte[65535];
                while (true) {
                    int read = this.d.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    this.e.write(bArr, 0, read);
                    long currentTimeMillis2 = System.currentTimeMillis();
                    if (currentTimeMillis2 - currentTimeMillis > 2000) {
                        a((int) file.length(), (int) (((long) parseInt) + mobiSageResMessage.e));
                        currentTimeMillis = currentTimeMillis2;
                    }
                }
                this.d.close();
                this.c.close();
                this.e.close();
                if (file.length() != ((long) parseInt) + mobiSageResMessage.e) {
                    file.delete();
                    mobiSageResMessage.result.putInt("StatusCode", 400);
                } else {
                    file.renameTo(new File(mobiSageResMessage.targetURL));
                }
            }
        } catch (IOException e2) {
            mobiSageResMessage.result.putInt("StatusCode", 400);
            mobiSageResMessage.result.putString("ErrorText", e2.getLocalizedMessage());
        } finally {
            Context context = C0018r.h;
            Intent intent = new Intent(context, MobiSageApkService.class);
            str = "action";
            mobiSageResMessage.result.putInt(str, 3);
            str2 = "did";
            mobiSageResMessage.result.putString(str2, mobiSageResMessage.c.toString());
            str3 = "ExtraData";
            intent.putExtra(str3, mobiSageResMessage.result);
            context.startService(intent);
        }
    }

    private void a(int i, int i2) {
        Context context = C0018r.h;
        C0015o oVar = (C0015o) this.a;
        Intent intent = new Intent(context, MobiSageApkService.class);
        Bundle bundle = new Bundle();
        bundle.putString("did", oVar.c.toString());
        bundle.putInt("action", 1);
        intent.putExtra("ExtraData", bundle);
        PendingIntent service = PendingIntent.getService(context, 0, intent, 134217728);
        Notification notification = new Notification();
        notification.tickerText = oVar.a;
        notification.icon = 17301633;
        notification.flags = 16;
        notification.setLatestEventInfo(context, oVar.a, "下载中" + ((i * 100) / i2) + "%，点击取消下载", service);
        ((NotificationManager) context.getSystemService("notification")).notify(oVar.c.hashCode(), notification);
    }
}
