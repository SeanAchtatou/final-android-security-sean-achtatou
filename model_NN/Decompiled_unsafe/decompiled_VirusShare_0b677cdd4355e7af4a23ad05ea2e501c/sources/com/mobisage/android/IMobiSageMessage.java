package com.mobisage.android;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import org.apache.http.client.methods.HttpRequestBase;

public interface IMobiSageMessage {
    HttpRequestBase createHttpRequest() throws IOException, NoSuchAlgorithmException, InvalidKeyException;

    Runnable createMessageRunnable();
}
