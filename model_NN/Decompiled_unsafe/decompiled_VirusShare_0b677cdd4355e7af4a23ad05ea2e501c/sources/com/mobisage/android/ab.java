package com.mobisage.android;

import android.os.Handler;
import android.text.format.Time;
import com.mobisage.android.SNSSSLSocketFactory;
import java.io.File;
import java.util.UUID;

final class ab extends aa {
    ab(Handler handler) {
        super(handler);
        this.c = u.m;
    }

    /* access modifiers changed from: protected */
    public final void f(C0002b bVar) {
        Time time = new Time();
        time.setToNow();
        String str = C0018r.f + "/Track" + "/" + String.valueOf(time.toMillis(true) / 1000) + "_" + UUID.randomUUID().toString() + ".dat";
        File file = new File(str);
        file.mkdirs();
        SNSSSLSocketFactory.a.a(file, bVar.c.getString("TrackData"));
        this.e.put(bVar.b, bVar);
        X x = new X();
        x.a = str;
        x.callback = this.g;
        bVar.f.add(x);
        this.f.put(x.c, bVar.b);
        H.a().a(x);
    }
}
