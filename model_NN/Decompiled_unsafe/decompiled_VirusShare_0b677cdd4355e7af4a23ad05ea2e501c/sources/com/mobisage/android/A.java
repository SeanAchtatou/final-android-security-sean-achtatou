package com.mobisage.android;

import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;

final class A extends P {
    A() {
    }

    public final Animation a(int i) {
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.0f, 0.0f, 1.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setDuration(1000);
        scaleAnimation.setInterpolator(new DecelerateInterpolator());
        return scaleAnimation;
    }

    public final Animation b(int i) {
        ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 1.0f, 1.0f, 0.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setDuration(1000);
        scaleAnimation.setInterpolator(new DecelerateInterpolator());
        return scaleAnimation;
    }
}
