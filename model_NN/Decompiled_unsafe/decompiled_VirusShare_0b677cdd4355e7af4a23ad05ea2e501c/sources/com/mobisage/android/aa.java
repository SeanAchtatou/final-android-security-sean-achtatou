package com.mobisage.android;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import java.io.File;
import java.util.UUID;

abstract class aa extends O {
    protected a g = new a(this, (byte) 0);
    /* access modifiers changed from: private */
    public aa h = this;

    /* access modifiers changed from: protected */
    public abstract void f(C0002b bVar);

    protected aa(Handler handler) {
        super(handler);
    }

    public final void a(Message message) {
        if (message.obj instanceof C0002b) {
            f((C0002b) message.obj);
        } else if (message.obj instanceof X) {
            X x = (X) message.obj;
            int i = x.result.getInt("StatusCode");
            if (i == 200 || i == 302) {
                new File(x.a).delete();
                this.f.remove(x.c);
                C0002b bVar = (C0002b) this.e.get((UUID) this.f.get(x.c));
                bVar.f.remove(x);
                if (bVar.f.size() == 0) {
                    this.e.remove(bVar.b);
                    if (bVar.g != null) {
                        bVar.g.a(bVar);
                        return;
                    }
                    return;
                }
                return;
            }
            x.result = new Bundle();
            H.a().a(x);
        }
    }

    class a implements IMobiSageMessageCallback {
        private a() {
        }

        /* synthetic */ a(aa aaVar, byte b) {
            this();
        }

        public final void onMobiSageMessageFinish(MobiSageMessage msg) {
            Message obtainMessage = aa.this.a.obtainMessage(aa.this.h.c);
            obtainMessage.obj = msg;
            obtainMessage.sendToTarget();
        }
    }
}
