package com.mobisage.android;

public interface IMobiSageMessageCallback {
    void onMobiSageMessageFinish(MobiSageMessage mobiSageMessage);
}
