package com.mobisage.android;

import android.content.Context;
import android.util.AttributeSet;

public final class MobiSageAdNoBanner extends C0007g {
    public final /* bridge */ /* synthetic */ Integer getAdRefreshInterval() {
        return super.getAdRefreshInterval();
    }

    public final /* bridge */ /* synthetic */ String getCustomData() {
        return super.getCustomData();
    }

    public final /* bridge */ /* synthetic */ String getKeyword() {
        return super.getKeyword();
    }

    public final /* bridge */ /* synthetic */ void setAdRefreshInterval(Integer x0) {
        super.setAdRefreshInterval(x0);
    }

    public final /* bridge */ /* synthetic */ void setCustomData(String x0) {
        super.setCustomData(x0);
    }

    public final /* bridge */ /* synthetic */ void setKeyword(String x0) {
        super.setKeyword(x0);
    }

    public final /* bridge */ /* synthetic */ void setMobiSageAdViewListener(IMobiSageAdViewListener x0) {
        super.setMobiSageAdViewListener(x0);
    }

    public MobiSageAdNoBanner(Context context, String publisherID, String keyword, String customData) {
        super(context, 25, publisherID, keyword, customData);
    }

    public MobiSageAdNoBanner(Context context, int i, String publisherID, String keyword, String customData) {
        super(context, 25, publisherID, keyword, customData);
    }

    public MobiSageAdNoBanner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /* access modifiers changed from: protected */
    public final void initMobiSageAdView(Context context) {
        this.adSize = 25;
        super.initMobiSageAdView(context);
    }

    public final void popAdView() {
        if (this.frontWebView != null) {
            this.frontWebView.loadUrl("javascript:bannerlinkclick()");
        }
    }
}
