package com.mobisage.android;

import java.util.concurrent.LinkedBlockingQueue;

final class K extends MobiSageResMessage {
    public LinkedBlockingQueue<MobiSageResMessage> a = new LinkedBlockingQueue<>();

    K() {
        this.callback = new a(this, (byte) 0);
    }

    /* access modifiers changed from: protected */
    public final void finalize() throws Throwable {
        super.finalize();
        this.a.clear();
        this.callback = null;
    }

    class a implements IMobiSageMessageCallback {
        private a() {
        }

        /* synthetic */ a(K k, byte b) {
            this();
        }

        public final void onMobiSageMessageFinish(MobiSageMessage mobiSageMessage) {
            while (K.this.a.size() != 0) {
                MobiSageResMessage poll = K.this.a.poll();
                poll.result = K.this.result;
                if (poll.callback != null) {
                    poll.callback.onMobiSageMessageFinish(poll);
                }
            }
        }
    }
}
