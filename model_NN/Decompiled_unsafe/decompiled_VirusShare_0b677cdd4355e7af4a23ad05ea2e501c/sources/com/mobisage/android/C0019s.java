package com.mobisage.android;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.mobisage.android.s  reason: case insensitive filesystem */
final class C0019s extends af {
    /* access modifiers changed from: private */
    public static int a = 1;
    /* access modifiers changed from: private */
    public static int b = 2;
    /* access modifiers changed from: private */
    public String c;
    private a d = new a(this, (byte) 0);
    /* access modifiers changed from: private */
    public Handler e;
    private b f;

    static /* synthetic */ void a(C0019s sVar, C0002b bVar) {
        try {
            JSONObject jSONObject = new JSONObject(bVar.d.getString("ResponseBody")).getJSONObject("baidu");
            sVar.loadUrl("http://" + jSONObject.getString("url") + "/s?from=" + jSONObject.getString("baiduid") + "&word=" + sVar.c);
        } catch (JSONException e2) {
            sVar.loadUrl("http://m1.baidu.com/s?from=&word=" + sVar.c);
        }
    }

    C0019s(Context context, Intent intent) {
        super(context);
        getSettings().setSupportZoom(true);
        getSettings().setBuiltInZoomControls(true);
        setHorizontalScrollBarEnabled(true);
        setVerticalScrollBarEnabled(true);
        setWebViewClient(new c(this, (byte) 0));
        this.c = intent.getStringExtra("keyword");
        this.f = new b(this, (byte) 0);
        this.e = new Handler(context.getMainLooper(), this.f);
        Object a2 = C0023w.a().a("baidu");
        if (a2 == null) {
            C0002b bVar = new C0002b();
            bVar.c.putString("key", "baidu");
            bVar.g = this.d;
            C0023w.a().a(3000, bVar);
            return;
        }
        try {
            JSONObject jSONObject = (JSONObject) a2;
            loadUrl("http://" + jSONObject.getString("url") + "/s?from=" + jSONObject.getString("baiduid") + "&word=" + this.c);
        } catch (JSONException e2) {
            C0002b bVar2 = new C0002b();
            bVar2.c.putString("key", "baidu");
            bVar2.g = this.d;
            C0023w.a().a(3000, bVar2);
        }
    }

    /* renamed from: com.mobisage.android.s$b */
    class b implements Handler.Callback {
        private b() {
        }

        /* synthetic */ b(C0019s sVar, byte b) {
            this();
        }

        /* Debug info: failed to restart local var, previous not found, register: 3 */
        public final boolean handleMessage(Message message) {
            if (message.what == C0019s.a) {
                C0019s.a(C0019s.this, (C0002b) message.obj);
                return true;
            } else if (message.what != C0019s.b) {
                return false;
            } else {
                C0019s.this.loadUrl("http://m1.baidu.com/s?from=&word=" + C0019s.this.c);
                return true;
            }
        }
    }

    /* renamed from: com.mobisage.android.s$a */
    class a implements C0001a {
        private a() {
        }

        /* synthetic */ a(C0019s sVar, byte b) {
            this();
        }

        public final void a(C0002b bVar) {
            Message obtainMessage = C0019s.this.e.obtainMessage(C0019s.a);
            obtainMessage.obj = bVar;
            obtainMessage.sendToTarget();
        }

        public final void b(C0002b bVar) {
            Message obtainMessage = C0019s.this.e.obtainMessage(C0019s.b);
            obtainMessage.obj = bVar;
            obtainMessage.sendToTarget();
        }
    }

    /* renamed from: com.mobisage.android.s$c */
    class c extends WebViewClient {
        private boolean a;

        private c() {
            this.a = true;
        }

        /* synthetic */ c(C0019s sVar, byte b2) {
            this();
        }

        public final void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (this.a) {
                C0002b bVar = new C0002b();
                bVar.c.putString("keyword", C0019s.this.c);
                Y.a().a(u.p, bVar);
                this.a = false;
            }
        }
    }
}
