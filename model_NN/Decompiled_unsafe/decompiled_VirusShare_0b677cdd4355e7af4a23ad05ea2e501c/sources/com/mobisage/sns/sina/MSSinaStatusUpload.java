package com.mobisage.sns.sina;

import MobWin.cnst.PROTOCOL_ENCODING;
import com.tencent.lbsapi.core.e;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ByteArrayEntity;

public class MSSinaStatusUpload extends MSSinaWeiboMessage {
    private String a;

    public MSSinaStatusUpload(String appKey, String accessToken) {
        super(appKey, accessToken);
        this.urlPath = "https://api.weibo.com/2/statuses/upload.json";
        this.httpMethod = "POST";
    }

    public void addParam(String name, String value) {
        if (name.equals("pic")) {
            this.a = value;
        } else {
            super.addParam(name, value);
        }
    }

    public HttpRequestBase createHttpRequest() throws IOException {
        UUID randomUUID = UUID.randomUUID();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        String str = "\r\n--" + randomUUID.toString() + "\r\n";
        byte[] bytes = str.getBytes(e.e);
        byteArrayOutputStream.write(bytes, 0, bytes.length);
        for (String str2 : this.paramMap.keySet()) {
            byte[] bytes2 = ("Content-Disposition: form-data; name=\"" + str2 + "\"\r\n\r\n" + ((String) this.paramMap.get(str2))).getBytes(PROTOCOL_ENCODING.value);
            byteArrayOutputStream.write(bytes2, 0, bytes2.length);
            byte[] bytes3 = str.getBytes(PROTOCOL_ENCODING.value);
            byteArrayOutputStream.write(bytes3, 0, bytes3.length);
        }
        byte[] bytes4 = ("Content-Disposition: form-data; name=\"pic\"; filename=\"" + this.a + "\"\r\n").getBytes(PROTOCOL_ENCODING.value);
        byteArrayOutputStream.write(bytes4, 0, bytes4.length);
        byte[] bytes5 = "Content-Type:application/octet-stream\r\n\r\n".getBytes(PROTOCOL_ENCODING.value);
        byteArrayOutputStream.write(bytes5, 0, bytes5.length);
        FileInputStream fileInputStream = new FileInputStream(new File(this.a));
        byte[] bArr = new byte[1024];
        while (true) {
            int read = fileInputStream.read(bArr);
            if (read != -1) {
                byteArrayOutputStream.write(bArr, 0, read);
            } else {
                fileInputStream.close();
                byte[] bytes6 = ("\r\n--" + randomUUID.toString() + "--\r\n").getBytes(PROTOCOL_ENCODING.value);
                byteArrayOutputStream.write(bytes6, 0, bytes6.length);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                HttpPost httpPost = new HttpPost(this.urlPath);
                httpPost.setHeader("Authorization", "OAuth2 " + this.accessToken);
                httpPost.setHeader("Content-Type", "multipart/form-data; boundary=" + randomUUID.toString());
                httpPost.setEntity(new ByteArrayEntity(byteArray));
                byteArrayOutputStream.close();
                return httpPost;
            }
        }
    }
}
