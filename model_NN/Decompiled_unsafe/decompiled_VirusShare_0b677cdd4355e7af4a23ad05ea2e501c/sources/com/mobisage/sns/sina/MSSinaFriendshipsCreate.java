package com.mobisage.sns.sina;

public class MSSinaFriendshipsCreate extends MSSinaWeiboMessage {
    public MSSinaFriendshipsCreate(String appKey, String accessToken) {
        super(appKey, accessToken);
        this.urlPath = "https://api.weibo.com/2/friendships/create.json";
        this.httpMethod = "POST";
    }
}
