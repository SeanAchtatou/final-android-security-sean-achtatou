package com.mobisage.sns.sina;

import com.mobisage.android.MobiSageReqMessage;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

public abstract class MSSinaWeiboMessage extends MobiSageReqMessage {
    protected String accessToken;
    protected String appKey;
    protected String httpMethod;
    protected HashMap<String, String> paramMap = new HashMap<>();
    protected String urlPath;

    public MSSinaWeiboMessage(String appKey2) {
        this.appKey = appKey2;
        this.paramMap.put("source", appKey2);
    }

    public MSSinaWeiboMessage(String appKey2, String accessToken2) {
        this.appKey = appKey2;
        this.accessToken = accessToken2;
        this.paramMap.put("source", appKey2);
    }

    public void addParam(String name, String value) {
        this.paramMap.put(name, value);
    }

    public HttpRequestBase createHttpRequest() throws IOException {
        if (this.httpMethod.equals("GET")) {
            StringBuilder sb = new StringBuilder();
            for (String next : this.paramMap.keySet()) {
                if (sb.length() == 0) {
                    sb.append("?");
                } else {
                    sb.append("&");
                }
                sb.append(next + "=" + URLEncoder.encode(this.paramMap.get(next)));
            }
            HttpGet httpGet = new HttpGet(this.urlPath + sb.toString());
            if (this.accessToken == null) {
                return httpGet;
            }
            httpGet.setHeader("Authorization", "OAuth2 " + this.accessToken);
            return httpGet;
        } else if (this.httpMethod.equals("POST")) {
            return a();
        } else {
            return null;
        }
    }

    private HttpRequestBase a() throws UnsupportedEncodingException {
        HttpPost httpPost = new HttpPost(this.urlPath);
        StringBuilder sb = new StringBuilder();
        for (String next : this.paramMap.keySet()) {
            if (sb.length() != 0) {
                sb.append("&");
            }
            sb.append(next + "=" + URLEncoder.encode(this.paramMap.get(next)));
        }
        httpPost.setEntity(new StringEntity(sb.toString()));
        if (this.accessToken != null) {
            httpPost.setHeader("Authorization", "OAuth2 " + this.accessToken);
        }
        httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
        return httpPost;
    }
}
