package com.mobisage.sns.tencent;

import com.mobisage.sns.common.MSOAConsumer;
import com.mobisage.sns.common.MSOAToken;
import java.net.URLEncoder;

public class MSTencentAuthorize extends MSTencentWeiboMessage {
    public MSTencentAuthorize(MSOAToken token, MSOAConsumer consumer) {
        super(token, consumer);
        this.urlPath = "https://open.t.qq.com/cgi-bin/authorize";
        this.httpMethod = "GET";
        this.paramMap.put("oauth_token", "");
    }

    public String generateAuthorizeURL() {
        StringBuilder sb = new StringBuilder();
        for (String str : this.paramMap.keySet()) {
            if (sb.length() == 0) {
                sb.append("?");
            }
            if (str.equals("oauth_token")) {
                sb.append(str + "=" + URLEncoder.encode((String) this.paramMap.get(str)));
            }
        }
        return this.urlPath + sb.toString();
    }
}
