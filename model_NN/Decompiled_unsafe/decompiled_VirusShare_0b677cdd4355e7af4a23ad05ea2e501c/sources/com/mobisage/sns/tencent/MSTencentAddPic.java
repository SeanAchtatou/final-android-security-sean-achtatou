package com.mobisage.sns.tencent;

import MobWin.cnst.PROTOCOL_ENCODING;
import com.mobisage.sns.common.MSOAConsumer;
import com.mobisage.sns.common.MSOAToken;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ByteArrayEntity;

public class MSTencentAddPic extends MSTencentWeiboMessage {
    private String a;

    public MSTencentAddPic(MSOAToken token, MSOAConsumer consumer) {
        super(token, consumer);
        this.urlPath = "http://open.t.qq.com/api/t/add_pic";
        this.httpMethod = "POST";
        this.paramMap.put("format", "json");
    }

    public void addParam(String name, String value) {
        if (name.equals("pic")) {
            this.a = value;
        } else {
            super.addParam(name, value);
        }
    }

    public HttpRequestBase createHttpRequest() throws IOException, NoSuchAlgorithmException, InvalidKeyException {
        UUID randomUUID = UUID.randomUUID();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        String str = "\r\n--" + randomUUID.toString() + "\r\n";
        byte[] bytes = str.getBytes(PROTOCOL_ENCODING.value);
        byteArrayOutputStream.write(bytes, 0, bytes.length);
        String[] split = generateOAuthParams().split("&");
        for (String split2 : split) {
            String[] split3 = split2.split("=");
            byte[] bytes2 = ("Content-Disposition: form-data; name=\"" + split3[0] + "\"\r\n\r\n" + URLDecoder.decode(split3[1])).getBytes(PROTOCOL_ENCODING.value);
            byteArrayOutputStream.write(bytes2, 0, bytes2.length);
            byte[] bytes3 = str.getBytes(PROTOCOL_ENCODING.value);
            byteArrayOutputStream.write(bytes3, 0, bytes3.length);
        }
        byte[] bytes4 = ("Content-Disposition: form-data; name=\"pic\"; filename=\"" + this.a + "\"\r\n").getBytes(PROTOCOL_ENCODING.value);
        byteArrayOutputStream.write(bytes4, 0, bytes4.length);
        byte[] bytes5 = "Content-Type:application/octet-stream\r\n\r\n".getBytes(PROTOCOL_ENCODING.value);
        byteArrayOutputStream.write(bytes5, 0, bytes5.length);
        FileInputStream fileInputStream = new FileInputStream(new File(this.a));
        byte[] bArr = new byte[1024];
        while (true) {
            int read = fileInputStream.read(bArr);
            if (read != -1) {
                byteArrayOutputStream.write(bArr, 0, read);
            } else {
                fileInputStream.close();
                byte[] bytes6 = ("\r\n--" + randomUUID.toString() + "--\r\n").getBytes(PROTOCOL_ENCODING.value);
                byteArrayOutputStream.write(bytes6, 0, bytes6.length);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                HttpPost httpPost = new HttpPost(this.urlPath);
                httpPost.setHeader("Content-Type", "multipart/form-data; boundary=" + randomUUID.toString());
                httpPost.setEntity(new ByteArrayEntity(byteArray));
                byteArrayOutputStream.close();
                return httpPost;
            }
        }
    }
}
