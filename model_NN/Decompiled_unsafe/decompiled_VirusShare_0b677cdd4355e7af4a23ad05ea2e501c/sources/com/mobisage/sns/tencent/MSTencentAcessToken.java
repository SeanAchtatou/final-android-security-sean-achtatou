package com.mobisage.sns.tencent;

import com.mobisage.sns.common.MSOAConsumer;
import com.mobisage.sns.common.MSOAToken;

public class MSTencentAcessToken extends MSTencentWeiboMessage {
    public MSTencentAcessToken(MSOAToken token, MSOAConsumer consumer) {
        super(token, consumer);
        this.urlPath = "https://open.t.qq.com/cgi-bin/access_token";
        this.httpMethod = "GET";
    }
}
