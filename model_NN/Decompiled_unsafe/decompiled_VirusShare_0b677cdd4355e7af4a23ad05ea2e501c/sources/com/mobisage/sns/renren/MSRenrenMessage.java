package com.mobisage.sns.renren;

import MobWin.cnst.PROTOCOL_ENCODING;
import com.mobisage.android.MobiSageReqMessage;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;

public abstract class MSRenrenMessage extends MobiSageReqMessage {
    protected String accessToken;
    protected String clientID;
    protected String httpMethod;
    protected boolean isSignature = false;
    protected HashMap<String, String> paramMap = new HashMap<>();
    protected String secretKey;
    protected String urlPath;

    protected MSRenrenMessage(String clientID2) {
        this.clientID = clientID2;
    }

    protected MSRenrenMessage(String accessToken2, String secretKey2) {
        this.accessToken = accessToken2;
        this.secretKey = secretKey2;
    }

    public void addParam(String name, String value) {
        this.paramMap.put(name, value);
    }

    /* access modifiers changed from: protected */
    public void generateSignature() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        LinkedList linkedList = new LinkedList();
        for (String next : this.paramMap.keySet()) {
            linkedList.add(next + "=" + this.paramMap.get(next));
        }
        Collections.sort(linkedList);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < linkedList.size(); i++) {
            sb.append((String) linkedList.get(i));
        }
        sb.append(this.secretKey);
        MessageDigest instance = MessageDigest.getInstance("MD5");
        instance.reset();
        byte[] digest = instance.digest(sb.toString().getBytes(PROTOCOL_ENCODING.value));
        StringBuilder sb2 = new StringBuilder(digest.length << 1);
        for (int i2 = 0; i2 < digest.length; i2++) {
            sb2.append(Character.forDigit((digest[i2] & 240) >> 4, 16));
            sb2.append(Character.forDigit(digest[i2] & 15, 16));
        }
        this.paramMap.put("sig", sb2.toString());
    }

    public HttpRequestBase createHttpRequest() throws IOException, NoSuchAlgorithmException {
        if (this.httpMethod.equals("GET")) {
            if (this.isSignature) {
                generateSignature();
            }
            StringBuilder sb = new StringBuilder();
            for (String next : this.paramMap.keySet()) {
                if (sb.length() == 0) {
                    sb.append("?");
                } else {
                    sb.append("&");
                }
                sb.append(next + "=" + URLEncoder.encode(this.paramMap.get(next)));
            }
            return new HttpGet(this.urlPath + sb.toString());
        } else if (this.httpMethod.equals("POST")) {
            return a();
        } else {
            return null;
        }
    }

    private HttpRequestBase a() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        if (this.isSignature) {
            generateSignature();
        }
        HttpPost httpPost = new HttpPost(this.urlPath);
        StringBuilder sb = new StringBuilder();
        for (String next : this.paramMap.keySet()) {
            if (sb.length() != 0) {
                sb.append("&");
            }
            sb.append(next + "=" + URLEncoder.encode(this.paramMap.get(next)));
        }
        httpPost.setEntity(new StringEntity(sb.toString()));
        httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
        return httpPost;
    }
}
