package com.qq.jce.wup;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class BasicClassTypeUtil {
    /* JADX WARNING: Removed duplicated region for block: B:3:0x0010  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void addType(java.util.ArrayList<java.lang.String> r4, java.lang.String r5) {
        /*
            r3 = 0
            int r0 = r5.length()
        L_0x0005:
            r1 = 1
            int r1 = r0 - r1
            char r1 = r5.charAt(r1)
            r2 = 62
            if (r1 != r2) goto L_0x0014
            int r0 = r0 + -1
            if (r0 != 0) goto L_0x0005
        L_0x0014:
            java.lang.String r1 = r5.substring(r3, r0)
            java.lang.String r1 = uni2JavaType(r1)
            r4.add(r3, r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.qq.jce.wup.BasicClassTypeUtil.addType(java.util.ArrayList, java.lang.String):void");
    }

    public static ArrayList<String> getTypeList(String fullType) {
        ArrayList<String> type = new ArrayList<>();
        int point = 0;
        int splitPoint = fullType.indexOf("<");
        while (point < splitPoint) {
            addType(type, fullType.substring(point, splitPoint));
            point = splitPoint + 1;
            splitPoint = fullType.indexOf("<", point);
            int mapPoint = fullType.indexOf(",", point);
            if (splitPoint == -1) {
                splitPoint = mapPoint;
            }
            if (mapPoint != -1 && mapPoint < splitPoint) {
                splitPoint = mapPoint;
            }
        }
        addType(type, fullType.substring(point, fullType.length()));
        return type;
    }

    public static void main(String[] args) {
        ArrayList<String> src = new ArrayList<>();
        src.add("char");
        src.add("list<char>");
        src.add("list<list<char>>");
        src.add("map<short,string>");
        src.add("map<double,map<float,list<bool>>>");
        src.add("map<int64,list<Test.UserInfo>>");
        src.add("map<short,Test.FriendInfo>");
        Iterator it = src.iterator();
        while (it.hasNext()) {
            ArrayList<String> list = getTypeList((String) it.next());
            Iterator i$ = list.iterator();
            while (i$.hasNext()) {
                System.out.println(i$.next());
            }
            Collections.reverse(list);
            System.out.println("-------------finished " + transTypeList(list));
        }
    }

    public static String transTypeList(ArrayList<String> listTpye) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < listTpye.size(); i++) {
            listTpye.set(i, java2UniType(listTpye.get(i)));
        }
        Collections.reverse(listTpye);
        for (int i2 = 0; i2 < listTpye.size(); i2++) {
            String type = listTpye.get(i2);
            if (type.equals("list")) {
                listTpye.set(i2 - 1, "<" + listTpye.get(i2 - 1));
                listTpye.set(0, listTpye.get(0) + ">");
            } else if (type.equals("map")) {
                listTpye.set(i2 - 1, "<" + listTpye.get(i2 - 1) + ",");
                listTpye.set(0, listTpye.get(0) + ">");
            } else if (type.equals("Array")) {
                listTpye.set(i2 - 1, "<" + listTpye.get(i2 - 1));
                listTpye.set(0, listTpye.get(0) + ">");
            }
        }
        Collections.reverse(listTpye);
        Iterator i$ = listTpye.iterator();
        while (i$.hasNext()) {
            sb.append(i$.next());
        }
        return sb.toString();
    }

    public static Object createClassByUni(String className) throws ObjectCreateException {
        Object obj;
        Object obj2;
        boolean z;
        Object returnObject = null;
        Iterator i$ = getTypeList(className).iterator();
        Object obj3 = null;
        Object last2 = null;
        while (i$.hasNext()) {
            returnObject = createClassByName(i$.next());
            if (returnObject instanceof String) {
                if ("Array".equals((String) returnObject)) {
                    if (last2 == null) {
                        returnObject = Array.newInstance(Byte.class, 0);
                        obj = obj3;
                        obj2 = last2;
                    } else {
                        obj = obj3;
                        obj2 = last2;
                    }
                } else if ("?".equals((String) returnObject)) {
                    obj = obj3;
                    obj2 = last2;
                } else if (last2 == null) {
                    obj = obj3;
                    obj2 = returnObject;
                } else {
                    obj = last2;
                    obj2 = returnObject;
                }
            } else if (returnObject instanceof List) {
                if (last2 == null || !(last2 instanceof Byte)) {
                    if (last2 != null) {
                        ((List) returnObject).add(last2);
                    }
                    obj = obj3;
                    obj2 = null;
                } else {
                    returnObject = Array.newInstance(Byte.class, 1);
                    Array.set(returnObject, 0, last2);
                    obj = obj3;
                    obj2 = last2;
                }
            } else if (returnObject instanceof Map) {
                if (last2 != null) {
                    z = true;
                } else {
                    z = false;
                }
                if (z && (obj3 != null)) {
                    ((Map) returnObject).put(last2, obj3);
                }
                obj = null;
                obj2 = null;
            } else if (last2 == null) {
                obj = obj3;
                obj2 = returnObject;
            } else {
                obj = last2;
                obj2 = returnObject;
            }
            last2 = obj2;
            obj3 = obj;
        }
        return returnObject;
    }

    public static Object createClassByName(String name) throws ObjectCreateException {
        if (name.equals("java.lang.Integer")) {
            return 0;
        }
        if (name.equals("java.lang.Boolean")) {
            return false;
        }
        if (name.equals("java.lang.Byte")) {
            return (byte) 0;
        }
        if (name.equals("java.lang.Double")) {
            return Double.valueOf(0.0d);
        }
        if (name.equals("java.lang.Float")) {
            return Float.valueOf(0.0f);
        }
        if (name.equals("java.lang.Long")) {
            return 0L;
        }
        if (name.equals("java.lang.Short")) {
            return (short) 0;
        }
        if (name.equals("java.lang.Character")) {
            throw new IllegalArgumentException("can not support java.lang.Character");
        } else if (name.equals("java.lang.String")) {
            return "";
        } else {
            if (name.equals("java.util.List")) {
                return new ArrayList();
            }
            if (name.equals("java.util.Map")) {
                return new HashMap();
            }
            if (name.equals("Array")) {
                return "Array";
            }
            if (name.equals("?")) {
                return name;
            }
            try {
                return Class.forName(name).getConstructor(new Class[0]).newInstance(new Object[0]);
            } catch (Exception e) {
                Exception e2 = e;
                e2.printStackTrace();
                throw new ObjectCreateException(e2);
            }
        }
    }

    public static String java2UniType(String srcType) {
        if (srcType.equals("java.lang.Integer") || srcType.equals("int")) {
            return "int32";
        }
        if (srcType.equals("java.lang.Boolean") || srcType.equals("boolean")) {
            return "bool";
        }
        if (srcType.equals("java.lang.Byte") || srcType.equals("byte")) {
            return "char";
        }
        if (srcType.equals("java.lang.Double") || srcType.equals("double")) {
            return "double";
        }
        if (srcType.equals("java.lang.Float") || srcType.equals("float")) {
            return "float";
        }
        if (srcType.equals("java.lang.Long") || srcType.equals("long")) {
            return "int64";
        }
        if (srcType.equals("java.lang.Short") || srcType.equals("short")) {
            return "short";
        }
        if (srcType.equals("java.lang.Character")) {
            throw new IllegalArgumentException("can not support java.lang.Character");
        } else if (srcType.equals("java.lang.String")) {
            return "string";
        } else {
            if (srcType.equals("java.util.List")) {
                return "list";
            }
            return srcType.equals("java.util.Map") ? "map" : srcType;
        }
    }

    public static String uni2JavaType(String srcType) {
        if (srcType.equals("int32")) {
            return "java.lang.Integer";
        }
        if (srcType.equals("bool")) {
            return "java.lang.Boolean";
        }
        if (srcType.equals("char")) {
            return "java.lang.Byte";
        }
        if (srcType.equals("double")) {
            return "java.lang.Double";
        }
        if (srcType.equals("float")) {
            return "java.lang.Float";
        }
        if (srcType.equals("int64")) {
            return "java.lang.Long";
        }
        if (srcType.equals("short")) {
            return "java.lang.Short";
        }
        if (srcType.equals("string")) {
            return "java.lang.String";
        }
        if (srcType.equals("list")) {
            return "java.util.List";
        }
        return srcType.equals("map") ? "java.util.Map" : srcType;
    }

    public static boolean isBasicType(String name) {
        if (name.equals("int")) {
            return true;
        }
        if (name.equals("boolean")) {
            return true;
        }
        if (name.equals("byte")) {
            return true;
        }
        if (name.equals("double")) {
            return true;
        }
        if (name.equals("float")) {
            return true;
        }
        if (name.equals("long")) {
            return true;
        }
        if (name.equals("short")) {
            return true;
        }
        if (name.equals("char")) {
            return true;
        }
        if (name.equals("Integer")) {
            return true;
        }
        if (name.equals("Boolean")) {
            return true;
        }
        if (name.equals("Byte")) {
            return true;
        }
        if (name.equals("Double")) {
            return true;
        }
        if (name.equals("Float")) {
            return true;
        }
        if (name.equals("Long")) {
            return true;
        }
        if (name.equals("Short")) {
            return true;
        }
        if (name.equals("Char")) {
            return true;
        }
        return false;
    }

    public static String getClassTransName(String name) {
        if (name.equals("int")) {
            return "Integer";
        }
        if (name.equals("boolean")) {
            return "Boolean";
        }
        if (name.equals("byte")) {
            return "Byte";
        }
        if (name.equals("double")) {
            return "Double";
        }
        if (name.equals("float")) {
            return "Float";
        }
        if (name.equals("long")) {
            return "Long";
        }
        if (name.equals("short")) {
            return "Short";
        }
        if (name.equals("char")) {
            return "Character";
        }
        return name;
    }

    public static String getVariableInit(String name, String type) {
        if (type.equals("int")) {
            return type + " " + name + "=0 ;\n";
        }
        if (type.equals("boolean")) {
            return type + " " + name + "=false ;\n";
        }
        if (type.equals("byte")) {
            return type + " " + name + " ;\n";
        }
        if (type.equals("double")) {
            return type + " " + name + "=0 ;\n";
        }
        if (type.equals("float")) {
            return type + " " + name + "=0 ;\n";
        }
        if (type.equals("long")) {
            return type + " " + name + "=0 ;\n";
        }
        if (type.equals("short")) {
            return type + " " + name + "=0 ;\n";
        }
        if (type.equals("char")) {
            return type + " " + name + " ;\n";
        }
        return type + " " + name + " = null ;\n";
    }
}
