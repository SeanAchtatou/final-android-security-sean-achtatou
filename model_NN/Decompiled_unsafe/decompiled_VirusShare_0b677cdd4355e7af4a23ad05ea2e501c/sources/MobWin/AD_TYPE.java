package MobWin;

public final class AD_TYPE {
    static final /* synthetic */ boolean $assertionsDisabled = (!AD_TYPE.class.desiredAssertionStatus());
    public static final AD_TYPE LOGO_WORDS = new AD_TYPE(2, 2, "LOGO_WORDS");
    public static final AD_TYPE PIC = new AD_TYPE(1, 1, "PIC");
    public static final AD_TYPE SPOT = new AD_TYPE(3, 3, "SPOT");
    public static final AD_TYPE WORDS = new AD_TYPE(0, 0, "WORDS");
    public static final int _LOGO_WORDS = 2;
    public static final int _PIC = 1;
    public static final int _SPOT = 3;
    public static final int _WORDS = 0;
    private static AD_TYPE[] __values = new AD_TYPE[4];
    private String __T = new String();
    private int __value;

    public static AD_TYPE convert(int val) {
        for (int __i = 0; __i < __values.length; __i++) {
            if (__values[__i].value() == val) {
                return __values[__i];
            }
        }
        if ($assertionsDisabled) {
            return null;
        }
        throw new AssertionError();
    }

    public static AD_TYPE convert(String val) {
        for (int __i = 0; __i < __values.length; __i++) {
            if (__values[__i].toString().equals(val)) {
                return __values[__i];
            }
        }
        if ($assertionsDisabled) {
            return null;
        }
        throw new AssertionError();
    }

    public int value() {
        return this.__value;
    }

    public String toString() {
        return this.__T;
    }

    private AD_TYPE(int index, int val, String s) {
        this.__T = s;
        this.__value = val;
        __values[index] = this;
    }
}
