package MobWin;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;

public final class SettingVersions extends JceStruct {
    static final /* synthetic */ boolean $assertionsDisabled = (!SettingVersions.class.desiredAssertionStatus());
    public long appSetting = 0;
    public long sysSetting = 0;

    public String className() {
        return "MobWin.SettingVersions";
    }

    public long getSysSetting() {
        return this.sysSetting;
    }

    public void setSysSetting(long sysSetting2) {
        this.sysSetting = sysSetting2;
    }

    public long getAppSetting() {
        return this.appSetting;
    }

    public void setAppSetting(long appSetting2) {
        this.appSetting = appSetting2;
    }

    public SettingVersions() {
        setSysSetting(this.sysSetting);
        setAppSetting(this.appSetting);
    }

    public SettingVersions(long sysSetting2, long appSetting2) {
        setSysSetting(sysSetting2);
        setAppSetting(appSetting2);
    }

    public boolean equals(Object o) {
        SettingVersions t = (SettingVersions) o;
        return JceUtil.equals(this.sysSetting, t.sysSetting) && JceUtil.equals(this.appSetting, t.appSetting);
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            if ($assertionsDisabled) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public void writeTo(JceOutputStream _os) {
        _os.write(this.sysSetting, 1);
        _os.write(this.appSetting, 2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long */
    public void readFrom(JceInputStream _is) {
        setSysSetting(_is.read(this.sysSetting, 1, true));
        setAppSetting(_is.read(this.appSetting, 2, true));
    }

    public void display(StringBuilder _os, int _level) {
        JceDisplayer _ds = new JceDisplayer(_os, _level);
        _ds.display(this.sysSetting, "sysSetting");
        _ds.display(this.appSetting, "appSetting");
    }
}
