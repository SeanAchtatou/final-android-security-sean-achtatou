package MobWin;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;

public final class ADClickAccInfo extends JceStruct {
    static final /* synthetic */ boolean $assertionsDisabled = (!ADClickAccInfo.class.desiredAssertionStatus());
    public int ad_id = 0;
    public int click_count = 0;

    public String className() {
        return "MobWin.ADClickAccInfo";
    }

    public int getAd_id() {
        return this.ad_id;
    }

    public void setAd_id(int ad_id2) {
        this.ad_id = ad_id2;
    }

    public int getClick_count() {
        return this.click_count;
    }

    public void setClick_count(int click_count2) {
        this.click_count = click_count2;
    }

    public ADClickAccInfo() {
        setAd_id(this.ad_id);
        setClick_count(this.click_count);
    }

    public ADClickAccInfo(int ad_id2, int click_count2) {
        setAd_id(ad_id2);
        setClick_count(click_count2);
    }

    public boolean equals(Object o) {
        ADClickAccInfo t = (ADClickAccInfo) o;
        return JceUtil.equals(this.ad_id, t.ad_id) && JceUtil.equals(this.click_count, t.click_count);
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            if ($assertionsDisabled) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public void writeTo(JceOutputStream _os) {
        _os.write(this.ad_id, 0);
        _os.write(this.click_count, 1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    public void readFrom(JceInputStream _is) {
        setAd_id(_is.read(this.ad_id, 0, true));
        setClick_count(_is.read(this.click_count, 1, true));
    }

    public void display(StringBuilder _os, int _level) {
        JceDisplayer _ds = new JceDisplayer(_os, _level);
        _ds.display(this.ad_id, "ad_id");
        _ds.display(this.click_count, "click_count");
    }
}
