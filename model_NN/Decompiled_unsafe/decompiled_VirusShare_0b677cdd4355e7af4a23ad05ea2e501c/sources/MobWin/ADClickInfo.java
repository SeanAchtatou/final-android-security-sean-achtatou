package MobWin;

import com.qq.taf.jce.JceDisplayer;
import com.qq.taf.jce.JceInputStream;
import com.qq.taf.jce.JceOutputStream;
import com.qq.taf.jce.JceStruct;
import com.qq.taf.jce.JceUtil;

public final class ADClickInfo extends JceStruct {
    static final /* synthetic */ boolean $assertionsDisabled = (!ADClickInfo.class.desiredAssertionStatus());
    public int activated = 0;
    public int ad_id = 0;
    public String vri_key = "";

    public String className() {
        return "MobWin.ADClickInfo";
    }

    public int getAd_id() {
        return this.ad_id;
    }

    public void setAd_id(int ad_id2) {
        this.ad_id = ad_id2;
    }

    public String getVri_key() {
        return this.vri_key;
    }

    public void setVri_key(String vri_key2) {
        this.vri_key = vri_key2;
    }

    public int getActivated() {
        return this.activated;
    }

    public void setActivated(int activated2) {
        this.activated = activated2;
    }

    public ADClickInfo() {
        setAd_id(this.ad_id);
        setVri_key(this.vri_key);
        setActivated(this.activated);
    }

    public ADClickInfo(int ad_id2, String vri_key2, int activated2) {
        setAd_id(ad_id2);
        setVri_key(vri_key2);
        setActivated(activated2);
    }

    public boolean equals(Object o) {
        ADClickInfo t = (ADClickInfo) o;
        return JceUtil.equals(this.ad_id, t.ad_id) && JceUtil.equals(this.vri_key, t.vri_key) && JceUtil.equals(this.activated, t.activated);
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            if ($assertionsDisabled) {
                return null;
            }
            throw new AssertionError();
        }
    }

    public void writeTo(JceOutputStream _os) {
        _os.write(this.ad_id, 0);
        _os.write(this.vri_key, 1);
        _os.write(this.activated, 2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.qq.taf.jce.JceInputStream.read(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.qq.taf.jce.JceInputStream.read(byte, int, boolean):byte
      com.qq.taf.jce.JceInputStream.read(double, int, boolean):double
      com.qq.taf.jce.JceInputStream.read(float, int, boolean):float
      com.qq.taf.jce.JceInputStream.read(long, int, boolean):long
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct, int, boolean):com.qq.taf.jce.JceStruct
      com.qq.taf.jce.JceInputStream.read(java.lang.Object, int, boolean):java.lang.Object
      com.qq.taf.jce.JceInputStream.read(java.lang.String, int, boolean):java.lang.String
      com.qq.taf.jce.JceInputStream.read(short, int, boolean):short
      com.qq.taf.jce.JceInputStream.read(boolean, int, boolean):boolean
      com.qq.taf.jce.JceInputStream.read(byte[], int, boolean):byte[]
      com.qq.taf.jce.JceInputStream.read(double[], int, boolean):double[]
      com.qq.taf.jce.JceInputStream.read(float[], int, boolean):float[]
      com.qq.taf.jce.JceInputStream.read(int[], int, boolean):int[]
      com.qq.taf.jce.JceInputStream.read(long[], int, boolean):long[]
      com.qq.taf.jce.JceInputStream.read(com.qq.taf.jce.JceStruct[], int, boolean):com.qq.taf.jce.JceStruct[]
      com.qq.taf.jce.JceInputStream.read(java.lang.String[], int, boolean):java.lang.String[]
      com.qq.taf.jce.JceInputStream.read(short[], int, boolean):short[]
      com.qq.taf.jce.JceInputStream.read(boolean[], int, boolean):boolean[]
      com.qq.taf.jce.JceInputStream.read(int, int, boolean):int */
    public void readFrom(JceInputStream _is) {
        setAd_id(_is.read(this.ad_id, 0, true));
        setVri_key(_is.readString(1, true));
        setActivated(_is.read(this.activated, 2, false));
    }

    public void display(StringBuilder _os, int _level) {
        JceDisplayer _ds = new JceDisplayer(_os, _level);
        _ds.display(this.ad_id, "ad_id");
        _ds.display(this.vri_key, "vri_key");
        _ds.display(this.activated, "activated");
    }
}
