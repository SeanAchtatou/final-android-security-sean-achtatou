package LBSAPIProtocol;

public final class GPSTYPE {
    static final /* synthetic */ boolean $assertionsDisabled = (!GPSTYPE.class.desiredAssertionStatus());
    public static final GPSTYPE GPS_MARS = new GPSTYPE(1, 1, "GPS_MARS");
    public static final GPSTYPE GPS_WGS84 = new GPSTYPE(0, 0, "GPS_WGS84");
    public static final GPSTYPE GPS_WGS_REAL = new GPSTYPE(2, 2, "GPS_WGS_REAL");
    public static final int _GPS_MARS = 1;
    public static final int _GPS_WGS84 = 0;
    public static final int _GPS_WGS_REAL = 2;
    private static GPSTYPE[] __values = new GPSTYPE[3];
    private String __T = new String();
    private int __value;

    private GPSTYPE(int i, int i2, String str) {
        this.__T = str;
        this.__value = i2;
        __values[i] = this;
    }

    public static GPSTYPE convert(int i) {
        for (int i2 = 0; i2 < __values.length; i2++) {
            if (__values[i2].value() == i) {
                return __values[i2];
            }
        }
        if ($assertionsDisabled) {
            return null;
        }
        throw new AssertionError();
    }

    public static GPSTYPE convert(String str) {
        for (int i = 0; i < __values.length; i++) {
            if (__values[i].toString().equals(str)) {
                return __values[i];
            }
        }
        if ($assertionsDisabled) {
            return null;
        }
        throw new AssertionError();
    }

    public String toString() {
        return this.__T;
    }

    public int value() {
        return this.__value;
    }
}
