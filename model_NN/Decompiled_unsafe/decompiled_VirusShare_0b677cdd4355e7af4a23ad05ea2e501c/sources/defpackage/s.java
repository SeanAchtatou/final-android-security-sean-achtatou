package defpackage;

import android.view.animation.Animation;

/* renamed from: s  reason: default package */
final class s implements Animation.AnimationListener {
    final /* synthetic */ m a;

    s(m mVar) {
        this.a = mVar;
    }

    public final void onAnimationEnd(Animation animation) {
        this.a.removeAllViews();
        this.a.setVisibility(8);
        this.a.x.destroy();
        this.a.n.enableAdView(true);
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
