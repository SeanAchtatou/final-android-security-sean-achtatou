package defpackage;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.WindowManager;
import java.util.Locale;

/* renamed from: h  reason: default package */
public final class h {
    public static final char[] a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    private static final String b = "AdService";
    private static int c = 0;
    private static long d = 0;
    private static String e = "";
    private static float f = 0.0f;
    private static String g = null;
    private static String h = null;
    private static String i = null;
    private static int j = 0;
    private static int k = 0;
    private static String l = null;
    private static String m = null;
    private static boolean n = false;

    private static int a() {
        int i2 = c + 1;
        c = i2;
        return i2;
    }

    public static int a(Context context) {
        if (j == 0) {
            j = ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getWidth();
        }
        return j;
    }

    public static String a(byte[] bArr) {
        StringBuilder sb = new StringBuilder(bArr.length << 1);
        for (int i2 = 0; i2 < bArr.length; i2++) {
            sb.append(a[(bArr[i2] & 240) >>> 4]);
            sb.append(a[bArr[i2] & 15]);
        }
        return sb.toString();
    }

    public static void a(Context context, String str, String str2) {
        i iVar = new i(context, str, str2);
        if (!n) {
            new Thread(iVar).start();
            n = true;
        }
    }

    public static int b(Context context) {
        if (k == 0) {
            k = ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getHeight();
        }
        return k;
    }

    private static String b() {
        if (e != null) {
            StringBuffer stringBuffer = new StringBuffer();
            String str = Build.VERSION.RELEASE;
            if (str.length() > 0) {
                stringBuffer.append(str);
            } else {
                stringBuffer.append("1.0");
            }
            stringBuffer.append("; ");
            Locale locale = Locale.getDefault();
            String language = locale.getLanguage();
            if (language != null) {
                stringBuffer.append(language.toLowerCase());
                String country = locale.getCountry();
                if (country != null) {
                    stringBuffer.append("-");
                    stringBuffer.append(country.toLowerCase());
                }
            } else {
                stringBuffer.append("en");
            }
            String str2 = Build.MODEL;
            if (str2.length() > 0) {
                stringBuffer.append("; ");
                stringBuffer.append(str2);
            }
            String str3 = Build.ID;
            if (str3.length() > 0) {
                stringBuffer.append(" Build/");
                stringBuffer.append(str3);
            }
            e = String.format("Mozilla/5.0 (Linux; U; Android %s) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2 (TAOBAO-ANDROID-%s)", stringBuffer, "20101013");
            if (Log.isLoggable("TaobaoSDK", 3)) {
                Log.d("TaobaoSDK", "Phone's user-agent is:  " + stringBuffer);
            }
        }
        return e;
    }

    /* JADX WARNING: Removed duplicated region for block: B:94:0x0374  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String c(android.content.Context r13) {
        /*
            r12 = 0
            r11 = 1
            java.net.HttpURLConnection.setFollowRedirects(r11)
            java.net.URL r2 = new java.net.URL     // Catch:{ MalformedURLException -> 0x02c6 }
            java.lang.String r1 = "http://110.75.2.108/b"
            r2.<init>(r1)     // Catch:{ MalformedURLException -> 0x02c6 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            int r1 = defpackage.h.c     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            int r6 = r1 + 1
            defpackage.h.c = r6     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = defpackage.j.d     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            if (r1 == 0) goto L_0x02c9
            java.lang.String r1 = defpackage.j.d     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
        L_0x0021:
            java.lang.String r7 = "AdService"
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r9 = "PID: "
            r8.<init>(r9)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.StringBuilder r8 = r8.append(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            android.util.Log.d(r7, r8)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r8 = "pid="
            r7.<init>(r8)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.StringBuilder r1 = r7.append(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            r3.append(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = "&sv=Android-SDK-1.0.0"
            r3.append(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r7 = "&sd="
            r1.<init>(r7)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            int r7 = a(r13)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r7 = "x"
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            int r7 = b(r13)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            r3.append(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r7 = "&d="
            r1.<init>(r7)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            float r7 = defpackage.h.f     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            double r7 = (double) r7     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            r9 = 4591870180066957722(0x3fb999999999999a, double:0.1)
            int r7 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r7 >= 0) goto L_0x008f
            android.content.res.Resources r7 = r13.getResources()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            android.util.DisplayMetrics r7 = r7.getDisplayMetrics()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            float r7 = r7.density     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            defpackage.h.f = r7     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
        L_0x008f:
            float r7 = defpackage.h.f     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            r3.append(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r7 = "&t="
            r1.<init>(r7)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            r7 = 1000(0x3e8, double:4.94E-321)
            long r7 = r4 / r7
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r7 = "."
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            r7 = 1000(0x3e8, double:4.94E-321)
            long r4 = r4 % r7
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            r3.append(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = "&so="
            r4.<init>(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = "window"
            java.lang.Object r1 = r13.getSystemService(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            android.view.WindowManager r1 = (android.view.WindowManager) r1     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            android.view.Display r1 = r1.getDefaultDisplay()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            int r1 = r1.getOrientation()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            if (r1 != r11) goto L_0x02cf
            java.lang.String r1 = "l"
        L_0x00da:
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            r3.append(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r4 = "&ov="
            r1.<init>(r4)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r4 = android.os.Build.VERSION.RELEASE     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            r3.append(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = "&pt="
            r4.<init>(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = "phone"
            java.lang.Object r1 = r13.getSystemService(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            android.telephony.TelephonyManager r1 = (android.telephony.TelephonyManager) r1     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            int r1 = r1.getPhoneType()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            switch(r1) {
                case 0: goto L_0x02db;
                case 1: goto L_0x02d7;
                case 2: goto L_0x02d3;
                default: goto L_0x010f;
            }     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
        L_0x010f:
            java.lang.String r1 = "NONE"
        L_0x0111:
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            r3.append(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = "&nt="
            r4.<init>(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = "phone"
            java.lang.Object r1 = r13.getSystemService(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            android.telephony.TelephonyManager r1 = (android.telephony.TelephonyManager) r1     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            int r1 = r1.getNetworkType()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            switch(r1) {
                case 0: goto L_0x0307;
                case 1: goto L_0x02f3;
                case 2: goto L_0x02e7;
                case 3: goto L_0x0303;
                case 4: goto L_0x02e3;
                case 5: goto L_0x02eb;
                case 6: goto L_0x02ef;
                case 7: goto L_0x02df;
                case 8: goto L_0x02f7;
                case 9: goto L_0x02ff;
                case 10: goto L_0x02fb;
                default: goto L_0x0132;
            }     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
        L_0x0132:
            java.lang.String r1 = "UNKNOWN"
        L_0x0134:
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            r3.append(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = "&imei="
            r4.<init>(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = defpackage.h.h     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            if (r1 != 0) goto L_0x0158
            java.lang.String r1 = "phone"
            java.lang.Object r1 = r13.getSystemService(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            android.telephony.TelephonyManager r1 = (android.telephony.TelephonyManager) r1     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = r1.getDeviceId()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            defpackage.h.h = r1     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
        L_0x0158:
            java.lang.String r1 = defpackage.h.h     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            r3.append(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = "&imsi="
            r4.<init>(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = defpackage.h.i     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            if (r1 != 0) goto L_0x017e
            java.lang.String r1 = "phone"
            java.lang.Object r1 = r13.getSystemService(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            android.telephony.TelephonyManager r1 = (android.telephony.TelephonyManager) r1     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = r1.getSubscriberId()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            defpackage.h.i = r1     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
        L_0x017e:
            java.lang.String r1 = defpackage.h.i     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.StringBuilder r1 = r4.append(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            r3.append(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            if (r6 <= r11) goto L_0x030b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r4 = "&stats[reqs]="
            r1.<init>(r4)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r4 = "&stats[time]="
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            long r6 = defpackage.h.d     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            long r4 = r4 - r6
            r6 = 1000(0x3e8, double:4.94E-321)
            long r4 = r4 / r6
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            r3.append(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
        L_0x01b3:
            java.net.URLConnection r13 = r2.openConnection()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.net.HttpURLConnection r13 = (java.net.HttpURLConnection) r13     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            r1 = 1
            r13.setDoOutput(r1)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r1 = 1
            r13.setDoInput(r1)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r1 = 5000(0x1388, float:7.006E-42)
            r13.setReadTimeout(r1)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.lang.String r1 = "Content-Type"
            java.lang.String r2 = "application/x-www-form-urlencoded"
            r13.setRequestProperty(r1, r2)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.lang.String r4 = "User-Agent"
            java.lang.String r1 = defpackage.h.e     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            if (r1 == 0) goto L_0x0279
            java.lang.StringBuffer r5 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r5.<init>()     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.lang.String r2 = android.os.Build.VERSION.RELEASE     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r0 = r2
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r1 = r0
            int r1 = r1.length()     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            if (r1 <= 0) goto L_0x035f
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r5.append(r2)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
        L_0x01e9:
            java.lang.String r1 = "; "
            r5.append(r1)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.util.Locale r2 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r0 = r2
            java.util.Locale r0 = (java.util.Locale) r0     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r1 = r0
            java.lang.String r1 = r1.getLanguage()     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            if (r1 == 0) goto L_0x0369
            java.lang.String r1 = r1.toLowerCase()     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r5.append(r1)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.util.Locale r2 = (java.util.Locale) r2     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.lang.String r1 = r2.getCountry()     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            if (r1 == 0) goto L_0x0219
            java.lang.String r2 = "-"
            r5.append(r2)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.lang.String r1 = r1.toLowerCase()     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r5.append(r1)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
        L_0x0219:
            java.lang.String r2 = android.os.Build.MODEL     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r0 = r2
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r1 = r0
            int r1 = r1.length()     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            if (r1 <= 0) goto L_0x022f
            java.lang.String r1 = "; "
            r5.append(r1)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r5.append(r2)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
        L_0x022f:
            java.lang.String r2 = android.os.Build.ID     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r0 = r2
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r1 = r0
            int r1 = r1.length()     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            if (r1 <= 0) goto L_0x0245
            java.lang.String r1 = " Build/"
            r5.append(r1)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r5.append(r2)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
        L_0x0245:
            java.lang.String r1 = "Mozilla/5.0 (Linux; U; Android %s) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2 (TAOBAO-ANDROID-%s)"
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r6 = 0
            r2[r6] = r5     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r6 = 1
            java.lang.String r7 = "20101013"
            r2[r6] = r7     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.lang.String r1 = java.lang.String.format(r1, r2)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            defpackage.h.e = r1     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.lang.String r1 = "TaobaoSDK"
            r2 = 3
            boolean r1 = android.util.Log.isLoggable(r1, r2)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            if (r1 == 0) goto L_0x0279
            java.lang.String r1 = "TaobaoSDK"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r2.<init>()     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.lang.String r6 = "Phone's user-agent is:  "
            java.lang.StringBuilder r2 = r2.append(r6)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            android.util.Log.d(r1, r2)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
        L_0x0279:
            java.lang.String r1 = defpackage.h.e     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r13.setRequestProperty(r4, r1)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.io.OutputStream r1 = r13.getOutputStream()     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.io.BufferedWriter r2 = new java.io.BufferedWriter     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.io.OutputStreamWriter r4 = new java.io.OutputStreamWriter     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.lang.String r5 = "utf-8"
            r4.<init>(r1, r5)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r2.<init>(r4)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r2.write(r3)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r2.flush()     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r13.connect()     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.io.InputStream r2 = r13.getInputStream()     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.lang.String r5 = "UTF-8"
            r4.<init>(r2, r5)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r2 = 2048(0x800, float:2.87E-42)
            r3.<init>(r4, r2)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.lang.StringBuffer r2 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r2.<init>()     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
        L_0x02b2:
            java.lang.String r4 = r3.readLine()     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            if (r4 != 0) goto L_0x0378
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            r1.close()     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            if (r13 == 0) goto L_0x02c4
            r13.disconnect()
        L_0x02c4:
            r1 = r2
        L_0x02c5:
            return r1
        L_0x02c6:
            r1 = move-exception
            r1 = r12
            goto L_0x02c5
        L_0x02c9:
            java.lang.String r1 = k(r13)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            goto L_0x0021
        L_0x02cf:
            java.lang.String r1 = "p"
            goto L_0x00da
        L_0x02d3:
            java.lang.String r1 = "CDMA"
            goto L_0x0111
        L_0x02d7:
            java.lang.String r1 = "GSM"
            goto L_0x0111
        L_0x02db:
            java.lang.String r1 = "NONE"
            goto L_0x0111
        L_0x02df:
            java.lang.String r1 = "1xRTT"
            goto L_0x0134
        L_0x02e3:
            java.lang.String r1 = "CDMA"
            goto L_0x0134
        L_0x02e7:
            java.lang.String r1 = "EDGE"
            goto L_0x0134
        L_0x02eb:
            java.lang.String r1 = "EVDO_0"
            goto L_0x0134
        L_0x02ef:
            java.lang.String r1 = "EVDO_A"
            goto L_0x0134
        L_0x02f3:
            java.lang.String r1 = "GPRS"
            goto L_0x0134
        L_0x02f7:
            java.lang.String r1 = "HSDPA"
            goto L_0x0134
        L_0x02fb:
            java.lang.String r1 = "HSPA"
            goto L_0x0134
        L_0x02ff:
            java.lang.String r1 = "HSUPA"
            goto L_0x0134
        L_0x0303:
            java.lang.String r1 = "UMTS"
            goto L_0x0134
        L_0x0307:
            java.lang.String r1 = "UNKNOWN"
            goto L_0x0134
        L_0x030b:
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            defpackage.h.d = r4     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r4 = "&app[name]="
            r1.<init>(r4)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r4 = h(r13)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            r3.append(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r4 = "&app[version]="
            r1.<init>(r4)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r4 = i(r13)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            r3.append(r1)     // Catch:{ Exception -> 0x033f, all -> 0x0383 }
            goto L_0x01b3
        L_0x033f:
            r1 = move-exception
            r2 = r12
        L_0x0341:
            java.lang.String r3 = "AdService"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0386 }
            java.lang.String r5 = "Failed to request ads:"
            r4.<init>(r5)     // Catch:{ all -> 0x0386 }
            java.lang.String r5 = defpackage.h.g     // Catch:{ all -> 0x0386 }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0386 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0386 }
            android.util.Log.e(r3, r4, r1)     // Catch:{ all -> 0x0386 }
            if (r2 == 0) goto L_0x0388
            r2.disconnect()
            r1 = r12
            goto L_0x02c5
        L_0x035f:
            java.lang.String r1 = "1.0"
            r5.append(r1)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            goto L_0x01e9
        L_0x0366:
            r1 = move-exception
            r2 = r13
            goto L_0x0341
        L_0x0369:
            java.lang.String r1 = "en"
            r5.append(r1)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            goto L_0x0219
        L_0x0370:
            r1 = move-exception
            r2 = r13
        L_0x0372:
            if (r2 == 0) goto L_0x0377
            r2.disconnect()
        L_0x0377:
            throw r1
        L_0x0378:
            java.lang.StringBuffer r4 = r2.append(r4)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            java.lang.String r5 = "\n"
            r4.append(r5)     // Catch:{ Exception -> 0x0366, all -> 0x0370 }
            goto L_0x02b2
        L_0x0383:
            r1 = move-exception
            r2 = r12
            goto L_0x0372
        L_0x0386:
            r1 = move-exception
            goto L_0x0372
        L_0x0388:
            r1 = r12
            goto L_0x02c5
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.h.c(android.content.Context):java.lang.String");
    }

    private static String d(Context context) {
        if (h == null) {
            h = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        }
        return h;
    }

    private static String e(Context context) {
        if (i == null) {
            i = ((TelephonyManager) context.getSystemService("phone")).getSubscriberId();
        }
        return i;
    }

    private static String f(Context context) {
        switch (((TelephonyManager) context.getSystemService("phone")).getPhoneType()) {
            case 0:
                return "NONE";
            case 1:
                return "GSM";
            case 2:
                return "CDMA";
            default:
                return "NONE";
        }
    }

    private static String g(Context context) {
        switch (((TelephonyManager) context.getSystemService("phone")).getNetworkType()) {
            case 0:
                return "UNKNOWN";
            case 1:
                return "GPRS";
            case 2:
                return "EDGE";
            case 3:
                return "UMTS";
            case 4:
                return "CDMA";
            case 5:
                return "EVDO_0";
            case 6:
                return "EVDO_A";
            case 7:
                return "1xRTT";
            case 8:
                return "HSDPA";
            case 9:
                return "HSUPA";
            case 10:
                return "HSPA";
            default:
                return "UNKNOWN";
        }
    }

    private static String h(Context context) {
        if (m == null) {
            try {
                m = String.valueOf(context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).packageName);
            } catch (Exception e2) {
                m = e2.getMessage();
            }
        }
        return m;
    }

    private static String i(Context context) {
        if (l == null) {
            try {
                l = String.valueOf(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode);
            } catch (Exception e2) {
                l = e2.getMessage();
            }
        }
        return l;
    }

    private static String j(Context context) {
        return ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getOrientation() == 1 ? "l" : "p";
    }

    private static String k(Context context) {
        if (g == null) {
            try {
                ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
                if (!(applicationInfo == null || applicationInfo.metaData == null)) {
                    g = applicationInfo.metaData.getString("TAOBAO_PID");
                }
            } catch (PackageManager.NameNotFoundException e2) {
            }
            if (g == null) {
                g = "mm_0_0_0";
            }
        }
        return g;
    }

    private static float l(Context context) {
        if (((double) f) < 0.1d) {
            f = context.getResources().getDisplayMetrics().density;
        }
        return f;
    }
}
