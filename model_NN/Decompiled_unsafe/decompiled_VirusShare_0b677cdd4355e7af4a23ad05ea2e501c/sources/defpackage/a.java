package defpackage;

import android.content.Context;
import com.taobao.munion.ads.AdView;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/* renamed from: a  reason: default package */
public final class a implements Runnable {
    private static Executor c = null;
    private Context a;
    private AdView b;

    public a(Context context, AdView adView) {
        this.a = context;
        this.b = adView;
    }

    public final void a() {
        if (c == null) {
            c = Executors.newCachedThreadPool();
        }
        c.execute(this);
    }

    public final void run() {
        if (j.c != null) {
            j.c.onAdEvent(1001, "Start request ad");
        }
        String c2 = h.c(this.a);
        if (c2 != null) {
            c2 = c2.trim();
        }
        if (c2 == null || "".equals(c2)) {
            if (j.c != null) {
                j.c.onAdEvent(1003, "Get null ad");
            }
            this.b.a();
            return;
        }
        if (j.c != null) {
            j.c.onAdEvent(1002, "Get ad success");
        }
        this.b.a(c2);
    }
}
