package defpackage;

import android.view.animation.Animation;
import com.taobao.munion.ads.AdView;

/* renamed from: e  reason: default package */
public final class e implements Animation.AnimationListener {
    final /* synthetic */ AdView a;

    public e(AdView adView) {
        this.a = adView;
    }

    public final void onAnimationEnd(Animation animation) {
        this.a.o.removeAllViews();
        this.a.addView(this.a.n, this.a.c());
        this.a.n.startAnimation(this.a.s);
        this.a.o = this.a.n;
    }

    public final void onAnimationRepeat(Animation animation) {
    }

    public final void onAnimationStart(Animation animation) {
    }
}
