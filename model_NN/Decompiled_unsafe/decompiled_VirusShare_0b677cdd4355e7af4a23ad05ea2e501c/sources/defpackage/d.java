package defpackage;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import com.taobao.munion.ads.AdView;

/* renamed from: d  reason: default package */
public final class d implements View.OnTouchListener {
    final /* synthetic */ AdView a;

    public d(AdView adView) {
        this.a = adView;
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        Log.d("AdView", "Ads Container is touched " + motionEvent.getAction());
        return true;
    }
}
