package com.google.i18n.phonenumbers;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberMatcher;
import com.google.i18n.phonenumbers.Phonemetadata;
import com.google.i18n.phonenumbers.Phonenumber;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneNumberUtil {
    private static final Map<Character, Character> ALL_PLUS_NUMBER_GROUPING_SYMBOLS;
    private static final Map<Character, Character> ALPHA_MAPPINGS;
    private static final Map<Character, Character> ALPHA_PHONE_MAPPINGS;
    private static final Pattern CAPTURING_DIGIT_PATTERN = Pattern.compile("(\\p{Nd})");
    private static final String CAPTURING_EXTN_DIGITS = "(\\p{Nd}{1,7})";
    private static final Pattern CC_PATTERN = Pattern.compile("\\$CC");
    private static final String COLOMBIA_MOBILE_TO_FIXED_LINE_PREFIX = "3";
    private static final String DEFAULT_EXTN_PREFIX = " ext. ";
    static final MetadataLoader DEFAULT_METADATA_LOADER = new MetadataLoader() {
        public InputStream loadMetadata(String metadataFileName) {
            return PhoneNumberUtil.class.getResourceAsStream(metadataFileName);
        }
    };
    private static final Map<Character, Character> DIALLABLE_CHAR_MAPPINGS;
    private static final String DIGITS = "\\p{Nd}";
    private static final Pattern EXTN_PATTERN;
    static final String EXTN_PATTERNS_FOR_MATCHING = createExtnPattern("xｘ#＃~～");
    private static final String EXTN_PATTERNS_FOR_PARSING;
    private static final Pattern FG_PATTERN = Pattern.compile("\\$FG");
    private static final Pattern FIRST_GROUP_ONLY_PREFIX_PATTERN = Pattern.compile("\\(?\\$1\\)?");
    private static final Pattern FIRST_GROUP_PATTERN = Pattern.compile("(\\$\\d)");
    private static final int MAX_INPUT_STRING_LENGTH = 250;
    static final int MAX_LENGTH_COUNTRY_CODE = 3;
    static final int MAX_LENGTH_FOR_NSN = 17;
    private static final String META_DATA_FILE_PREFIX = "/com/google/i18n/phonenumbers/data/PhoneNumberMetadataProto";
    private static final int MIN_LENGTH_FOR_NSN = 2;
    private static final Map<Integer, String> MOBILE_TOKEN_MAPPINGS;
    private static final int NANPA_COUNTRY_CODE = 1;
    static final Pattern NON_DIGITS_PATTERN = Pattern.compile("(\\D+)");
    private static final Pattern NP_PATTERN = Pattern.compile("\\$NP");
    static final String PLUS_CHARS = "+＋";
    static final Pattern PLUS_CHARS_PATTERN = Pattern.compile("[+＋]+");
    static final char PLUS_SIGN = '+';
    static final int REGEX_FLAGS = 66;
    public static final String REGION_CODE_FOR_NON_GEO_ENTITY = "001";
    private static final String RFC3966_EXTN_PREFIX = ";ext=";
    private static final String RFC3966_ISDN_SUBADDRESS = ";isub=";
    private static final String RFC3966_PHONE_CONTEXT = ";phone-context=";
    private static final String RFC3966_PREFIX = "tel:";
    private static final String SECOND_NUMBER_START = "[\\\\/] *x";
    static final Pattern SECOND_NUMBER_START_PATTERN = Pattern.compile(SECOND_NUMBER_START);
    private static final Pattern SEPARATOR_PATTERN = Pattern.compile("[-x‐-―−ー－-／  ­​⁠　()（）［］.\\[\\]/~⁓∼～]+");
    private static final char STAR_SIGN = '*';
    private static final Pattern UNIQUE_INTERNATIONAL_PREFIX = Pattern.compile("[\\d]+(?:[~⁓∼～][\\d]+)?");
    private static final String UNKNOWN_REGION = "ZZ";
    private static final String UNWANTED_END_CHARS = "[[\\P{N}&&\\P{L}]&&[^#]]+$";
    static final Pattern UNWANTED_END_CHAR_PATTERN = Pattern.compile(UNWANTED_END_CHARS);
    private static final String VALID_ALPHA;
    private static final Pattern VALID_ALPHA_PHONE_PATTERN = Pattern.compile("(?:.*?[A-Za-z]){3}.*");
    private static final String VALID_PHONE_NUMBER;
    private static final Pattern VALID_PHONE_NUMBER_PATTERN;
    static final String VALID_PUNCTUATION = "-x‐-―−ー－-／  ­​⁠　()（）［］.\\[\\]/~⁓∼～";
    private static final String VALID_START_CHAR = "[+＋\\p{Nd}]";
    private static final Pattern VALID_START_CHAR_PATTERN = Pattern.compile(VALID_START_CHAR);
    private static PhoneNumberUtil instance = null;
    private static final Logger logger = Logger.getLogger(PhoneNumberUtil.class.getName());
    private final Map<Integer, List<String>> countryCallingCodeToRegionCodeMap;
    private final Map<Integer, Phonemetadata.PhoneMetadata> countryCodeToNonGeographicalMetadataMap = Collections.synchronizedMap(new HashMap());
    private final Set<Integer> countryCodesForNonGeographicalRegion = new HashSet();
    private final String currentFilePrefix;
    private final MetadataLoader metadataLoader;
    private final Set<String> nanpaRegions = new HashSet(35);
    private final RegexCache regexCache = new RegexCache(100);
    private final Map<String, Phonemetadata.PhoneMetadata> regionToMetadataMap = Collections.synchronizedMap(new HashMap());
    private final Set<String> supportedRegions = new HashSet(320);

    static {
        String str;
        HashMap<Integer, String> mobileTokenMap = new HashMap<>();
        mobileTokenMap.put(52, "1");
        mobileTokenMap.put(54, "9");
        MOBILE_TOKEN_MAPPINGS = Collections.unmodifiableMap(mobileTokenMap);
        HashMap<Character, Character> asciiDigitMappings = new HashMap<>();
        asciiDigitMappings.put('0', '0');
        asciiDigitMappings.put('1', '1');
        asciiDigitMappings.put('2', '2');
        asciiDigitMappings.put('3', '3');
        asciiDigitMappings.put('4', '4');
        asciiDigitMappings.put('5', '5');
        asciiDigitMappings.put('6', '6');
        asciiDigitMappings.put('7', '7');
        asciiDigitMappings.put('8', '8');
        asciiDigitMappings.put('9', '9');
        HashMap<Character, Character> alphaMap = new HashMap<>(40);
        alphaMap.put('A', '2');
        alphaMap.put('B', '2');
        alphaMap.put('C', '2');
        alphaMap.put('D', '3');
        alphaMap.put('E', '3');
        alphaMap.put('F', '3');
        alphaMap.put('G', '4');
        alphaMap.put('H', '4');
        alphaMap.put('I', '4');
        alphaMap.put('J', '5');
        alphaMap.put('K', '5');
        alphaMap.put('L', '5');
        alphaMap.put('M', '6');
        alphaMap.put('N', '6');
        alphaMap.put('O', '6');
        alphaMap.put('P', '7');
        alphaMap.put('Q', '7');
        alphaMap.put('R', '7');
        alphaMap.put('S', '7');
        alphaMap.put('T', '8');
        alphaMap.put('U', '8');
        alphaMap.put('V', '8');
        alphaMap.put('W', '9');
        alphaMap.put('X', '9');
        alphaMap.put('Y', '9');
        alphaMap.put('Z', '9');
        ALPHA_MAPPINGS = Collections.unmodifiableMap(alphaMap);
        HashMap<Character, Character> combinedMap = new HashMap<>(100);
        combinedMap.putAll(ALPHA_MAPPINGS);
        combinedMap.putAll(asciiDigitMappings);
        ALPHA_PHONE_MAPPINGS = Collections.unmodifiableMap(combinedMap);
        HashMap<Character, Character> diallableCharMap = new HashMap<>();
        diallableCharMap.putAll(asciiDigitMappings);
        diallableCharMap.put(Character.valueOf(PLUS_SIGN), Character.valueOf(PLUS_SIGN));
        diallableCharMap.put(Character.valueOf(STAR_SIGN), Character.valueOf(STAR_SIGN));
        DIALLABLE_CHAR_MAPPINGS = Collections.unmodifiableMap(diallableCharMap);
        HashMap<Character, Character> allPlusNumberGroupings = new HashMap<>();
        for (Character charValue : ALPHA_MAPPINGS.keySet()) {
            char c = charValue.charValue();
            allPlusNumberGroupings.put(Character.valueOf(Character.toLowerCase(c)), Character.valueOf(c));
            allPlusNumberGroupings.put(Character.valueOf(c), Character.valueOf(c));
        }
        allPlusNumberGroupings.putAll(asciiDigitMappings);
        allPlusNumberGroupings.put('-', '-');
        allPlusNumberGroupings.put(65293, '-');
        allPlusNumberGroupings.put(8208, '-');
        allPlusNumberGroupings.put(8209, '-');
        allPlusNumberGroupings.put(8210, '-');
        allPlusNumberGroupings.put(8211, '-');
        allPlusNumberGroupings.put(8212, '-');
        allPlusNumberGroupings.put(8213, '-');
        allPlusNumberGroupings.put(8722, '-');
        allPlusNumberGroupings.put('/', '/');
        allPlusNumberGroupings.put(65295, '/');
        allPlusNumberGroupings.put(' ', ' ');
        allPlusNumberGroupings.put(12288, ' ');
        allPlusNumberGroupings.put(8288, ' ');
        allPlusNumberGroupings.put('.', '.');
        allPlusNumberGroupings.put(65294, '.');
        ALL_PLUS_NUMBER_GROUPING_SYMBOLS = Collections.unmodifiableMap(allPlusNumberGroupings);
        String valueOf = String.valueOf(Arrays.toString(ALPHA_MAPPINGS.keySet().toArray()).replaceAll("[, \\[\\]]", ""));
        String valueOf2 = String.valueOf(Arrays.toString(ALPHA_MAPPINGS.keySet().toArray()).toLowerCase().replaceAll("[, \\[\\]]", ""));
        if (valueOf2.length() != 0) {
            str = valueOf.concat(valueOf2);
        } else {
            str = new String(valueOf);
        }
        VALID_ALPHA = str;
        String valueOf3 = String.valueOf(String.valueOf("\\p{Nd}{2}|[+＋]*+(?:[-x‐-―−ー－-／  ­​⁠　()（）［］.\\[\\]/~⁓∼～*]*\\p{Nd}){3,}[-x‐-―−ー－-／  ­​⁠　()（）［］.\\[\\]/~⁓∼～*"));
        String valueOf4 = String.valueOf(String.valueOf(VALID_ALPHA));
        String valueOf5 = String.valueOf(String.valueOf(DIGITS));
        VALID_PHONE_NUMBER = new StringBuilder(valueOf3.length() + MIN_LENGTH_FOR_NSN + valueOf4.length() + valueOf5.length()).append(valueOf3).append(valueOf4).append(valueOf5).append("]*").toString();
        String valueOf6 = String.valueOf("xｘ#＃~～");
        EXTN_PATTERNS_FOR_PARSING = createExtnPattern(valueOf6.length() != 0 ? ",".concat(valueOf6) : new String(","));
        String valueOf7 = String.valueOf(String.valueOf(EXTN_PATTERNS_FOR_PARSING));
        EXTN_PATTERN = Pattern.compile(new StringBuilder(valueOf7.length() + 5).append("(?:").append(valueOf7).append(")$").toString(), REGEX_FLAGS);
        String valueOf8 = String.valueOf(String.valueOf(VALID_PHONE_NUMBER));
        String valueOf9 = String.valueOf(String.valueOf(EXTN_PATTERNS_FOR_PARSING));
        VALID_PHONE_NUMBER_PATTERN = Pattern.compile(new StringBuilder(valueOf8.length() + 5 + valueOf9.length()).append(valueOf8).append("(?:").append(valueOf9).append(")?").toString(), REGEX_FLAGS);
    }

    private static String createExtnPattern(String singleExtnSymbols) {
        String valueOf = String.valueOf(String.valueOf(";ext=(\\p{Nd}{1,7})|[  \\t,]*(?:e?xt(?:ensi(?:ó?|ó))?n?|ｅ?ｘｔｎ?|["));
        String valueOf2 = String.valueOf(String.valueOf(singleExtnSymbols));
        String valueOf3 = String.valueOf(String.valueOf(CAPTURING_EXTN_DIGITS));
        String valueOf4 = String.valueOf(String.valueOf(DIGITS));
        return new StringBuilder(valueOf.length() + 48 + valueOf2.length() + valueOf3.length() + valueOf4.length()).append(valueOf).append(valueOf2).append("]|int|anexo|ｉｎｔ)").append("[:\\.．]?[  \\t,-]*").append(valueOf3).append("#?|").append("[- ]+(").append(valueOf4).append("{1,5})#").toString();
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class PhoneNumberFormat extends Enum<PhoneNumberFormat> {
        private static final /* synthetic */ PhoneNumberFormat[] $VALUES;
        public static final PhoneNumberFormat E164 = new PhoneNumberFormat("E164", 0);
        public static final PhoneNumberFormat INTERNATIONAL = new PhoneNumberFormat("INTERNATIONAL", PhoneNumberUtil.NANPA_COUNTRY_CODE);
        public static final PhoneNumberFormat NATIONAL = new PhoneNumberFormat("NATIONAL", PhoneNumberUtil.MIN_LENGTH_FOR_NSN);
        public static final PhoneNumberFormat RFC3966 = new PhoneNumberFormat("RFC3966", PhoneNumberUtil.MAX_LENGTH_COUNTRY_CODE);

        private PhoneNumberFormat(String str, int i) {
        }

        public static PhoneNumberFormat valueOf(String name) {
            return (PhoneNumberFormat) Enum.valueOf(PhoneNumberFormat.class, name);
        }

        public static PhoneNumberFormat[] values() {
            return (PhoneNumberFormat[]) $VALUES.clone();
        }

        static {
            PhoneNumberFormat[] phoneNumberFormatArr = new PhoneNumberFormat[4];
            phoneNumberFormatArr[0] = E164;
            phoneNumberFormatArr[PhoneNumberUtil.NANPA_COUNTRY_CODE] = INTERNATIONAL;
            phoneNumberFormatArr[PhoneNumberUtil.MIN_LENGTH_FOR_NSN] = NATIONAL;
            phoneNumberFormatArr[PhoneNumberUtil.MAX_LENGTH_COUNTRY_CODE] = RFC3966;
            $VALUES = phoneNumberFormatArr;
        }
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class PhoneNumberType extends Enum<PhoneNumberType> {
        private static final /* synthetic */ PhoneNumberType[] $VALUES;
        public static final PhoneNumberType FIXED_LINE = new PhoneNumberType("FIXED_LINE", 0);
        public static final PhoneNumberType FIXED_LINE_OR_MOBILE = new PhoneNumberType("FIXED_LINE_OR_MOBILE", PhoneNumberUtil.MIN_LENGTH_FOR_NSN);
        public static final PhoneNumberType MOBILE = new PhoneNumberType("MOBILE", PhoneNumberUtil.NANPA_COUNTRY_CODE);
        public static final PhoneNumberType PAGER = new PhoneNumberType("PAGER", 8);
        public static final PhoneNumberType PERSONAL_NUMBER = new PhoneNumberType("PERSONAL_NUMBER", 7);
        public static final PhoneNumberType PREMIUM_RATE = new PhoneNumberType("PREMIUM_RATE", 4);
        public static final PhoneNumberType SHARED_COST = new PhoneNumberType("SHARED_COST", 5);
        public static final PhoneNumberType TOLL_FREE = new PhoneNumberType("TOLL_FREE", PhoneNumberUtil.MAX_LENGTH_COUNTRY_CODE);
        public static final PhoneNumberType UAN = new PhoneNumberType("UAN", 9);
        public static final PhoneNumberType UNKNOWN = new PhoneNumberType("UNKNOWN", 11);
        public static final PhoneNumberType VOICEMAIL = new PhoneNumberType("VOICEMAIL", 10);
        public static final PhoneNumberType VOIP = new PhoneNumberType("VOIP", 6);

        private PhoneNumberType(String str, int i) {
        }

        public static PhoneNumberType valueOf(String name) {
            return (PhoneNumberType) Enum.valueOf(PhoneNumberType.class, name);
        }

        public static PhoneNumberType[] values() {
            return (PhoneNumberType[]) $VALUES.clone();
        }

        static {
            PhoneNumberType[] phoneNumberTypeArr = new PhoneNumberType[12];
            phoneNumberTypeArr[0] = FIXED_LINE;
            phoneNumberTypeArr[PhoneNumberUtil.NANPA_COUNTRY_CODE] = MOBILE;
            phoneNumberTypeArr[PhoneNumberUtil.MIN_LENGTH_FOR_NSN] = FIXED_LINE_OR_MOBILE;
            phoneNumberTypeArr[PhoneNumberUtil.MAX_LENGTH_COUNTRY_CODE] = TOLL_FREE;
            phoneNumberTypeArr[4] = PREMIUM_RATE;
            phoneNumberTypeArr[5] = SHARED_COST;
            phoneNumberTypeArr[6] = VOIP;
            phoneNumberTypeArr[7] = PERSONAL_NUMBER;
            phoneNumberTypeArr[8] = PAGER;
            phoneNumberTypeArr[9] = UAN;
            phoneNumberTypeArr[10] = VOICEMAIL;
            phoneNumberTypeArr[11] = UNKNOWN;
            $VALUES = phoneNumberTypeArr;
        }
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class MatchType extends Enum<MatchType> {
        private static final /* synthetic */ MatchType[] $VALUES;
        public static final MatchType EXACT_MATCH = new MatchType("EXACT_MATCH", 4);
        public static final MatchType NOT_A_NUMBER = new MatchType("NOT_A_NUMBER", 0);
        public static final MatchType NO_MATCH = new MatchType("NO_MATCH", PhoneNumberUtil.NANPA_COUNTRY_CODE);
        public static final MatchType NSN_MATCH = new MatchType("NSN_MATCH", PhoneNumberUtil.MAX_LENGTH_COUNTRY_CODE);
        public static final MatchType SHORT_NSN_MATCH = new MatchType("SHORT_NSN_MATCH", PhoneNumberUtil.MIN_LENGTH_FOR_NSN);

        private MatchType(String str, int i) {
        }

        public static MatchType valueOf(String name) {
            return (MatchType) Enum.valueOf(MatchType.class, name);
        }

        public static MatchType[] values() {
            return (MatchType[]) $VALUES.clone();
        }

        static {
            MatchType[] matchTypeArr = new MatchType[5];
            matchTypeArr[0] = NOT_A_NUMBER;
            matchTypeArr[PhoneNumberUtil.NANPA_COUNTRY_CODE] = NO_MATCH;
            matchTypeArr[PhoneNumberUtil.MIN_LENGTH_FOR_NSN] = SHORT_NSN_MATCH;
            matchTypeArr[PhoneNumberUtil.MAX_LENGTH_COUNTRY_CODE] = NSN_MATCH;
            matchTypeArr[4] = EXACT_MATCH;
            $VALUES = matchTypeArr;
        }
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static final class ValidationResult extends Enum<ValidationResult> {
        private static final /* synthetic */ ValidationResult[] $VALUES;
        public static final ValidationResult INVALID_COUNTRY_CODE = new ValidationResult("INVALID_COUNTRY_CODE", PhoneNumberUtil.NANPA_COUNTRY_CODE);
        public static final ValidationResult IS_POSSIBLE = new ValidationResult("IS_POSSIBLE", 0);
        public static final ValidationResult TOO_LONG = new ValidationResult("TOO_LONG", PhoneNumberUtil.MAX_LENGTH_COUNTRY_CODE);
        public static final ValidationResult TOO_SHORT = new ValidationResult("TOO_SHORT", PhoneNumberUtil.MIN_LENGTH_FOR_NSN);

        private ValidationResult(String str, int i) {
        }

        public static ValidationResult valueOf(String name) {
            return (ValidationResult) Enum.valueOf(ValidationResult.class, name);
        }

        public static ValidationResult[] values() {
            return (ValidationResult[]) $VALUES.clone();
        }

        static {
            ValidationResult[] validationResultArr = new ValidationResult[4];
            validationResultArr[0] = IS_POSSIBLE;
            validationResultArr[PhoneNumberUtil.NANPA_COUNTRY_CODE] = INVALID_COUNTRY_CODE;
            validationResultArr[PhoneNumberUtil.MIN_LENGTH_FOR_NSN] = TOO_SHORT;
            validationResultArr[PhoneNumberUtil.MAX_LENGTH_COUNTRY_CODE] = TOO_LONG;
            $VALUES = validationResultArr;
        }
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    public static abstract class Leniency extends Enum<Leniency> {
        private static final /* synthetic */ Leniency[] $VALUES;
        public static final Leniency EXACT_GROUPING = new Leniency("EXACT_GROUPING", PhoneNumberUtil.MAX_LENGTH_COUNTRY_CODE) {
            /* access modifiers changed from: package-private */
            public boolean verify(Phonenumber.PhoneNumber number, String candidate, PhoneNumberUtil util) {
                if (!util.isValidNumber(number) || !PhoneNumberMatcher.containsOnlyValidXChars(number, candidate, util) || PhoneNumberMatcher.containsMoreThanOneSlashInNationalNumber(number, candidate) || !PhoneNumberMatcher.isNationalPrefixPresentIfRequired(number, util)) {
                    return false;
                }
                return PhoneNumberMatcher.checkNumberGroupingIsValid(number, candidate, util, new PhoneNumberMatcher.NumberGroupingChecker() {
                    public boolean checkGroups(PhoneNumberUtil util, Phonenumber.PhoneNumber number, StringBuilder normalizedCandidate, String[] expectedNumberGroups) {
                        return PhoneNumberMatcher.allNumberGroupsAreExactlyPresent(util, number, normalizedCandidate, expectedNumberGroups);
                    }
                });
            }
        };
        public static final Leniency POSSIBLE = new Leniency("POSSIBLE", 0) {
            /* access modifiers changed from: package-private */
            public boolean verify(Phonenumber.PhoneNumber number, String candidate, PhoneNumberUtil util) {
                return util.isPossibleNumber(number);
            }
        };
        public static final Leniency STRICT_GROUPING = new Leniency("STRICT_GROUPING", PhoneNumberUtil.MIN_LENGTH_FOR_NSN) {
            /* access modifiers changed from: package-private */
            public boolean verify(Phonenumber.PhoneNumber number, String candidate, PhoneNumberUtil util) {
                if (!util.isValidNumber(number) || !PhoneNumberMatcher.containsOnlyValidXChars(number, candidate, util) || PhoneNumberMatcher.containsMoreThanOneSlashInNationalNumber(number, candidate) || !PhoneNumberMatcher.isNationalPrefixPresentIfRequired(number, util)) {
                    return false;
                }
                return PhoneNumberMatcher.checkNumberGroupingIsValid(number, candidate, util, new PhoneNumberMatcher.NumberGroupingChecker() {
                    public boolean checkGroups(PhoneNumberUtil util, Phonenumber.PhoneNumber number, StringBuilder normalizedCandidate, String[] expectedNumberGroups) {
                        return PhoneNumberMatcher.allNumberGroupsRemainGrouped(util, number, normalizedCandidate, expectedNumberGroups);
                    }
                });
            }
        };
        public static final Leniency VALID = new Leniency("VALID", PhoneNumberUtil.NANPA_COUNTRY_CODE) {
            /* access modifiers changed from: package-private */
            public boolean verify(Phonenumber.PhoneNumber number, String candidate, PhoneNumberUtil util) {
                if (!util.isValidNumber(number) || !PhoneNumberMatcher.containsOnlyValidXChars(number, candidate, util)) {
                    return false;
                }
                return PhoneNumberMatcher.isNationalPrefixPresentIfRequired(number, util);
            }
        };

        /* access modifiers changed from: package-private */
        public abstract boolean verify(Phonenumber.PhoneNumber phoneNumber, String str, PhoneNumberUtil phoneNumberUtil);

        private Leniency(String str, int i) {
        }

        public static Leniency valueOf(String name) {
            return (Leniency) Enum.valueOf(Leniency.class, name);
        }

        public static Leniency[] values() {
            return (Leniency[]) $VALUES.clone();
        }

        static {
            Leniency[] leniencyArr = new Leniency[4];
            leniencyArr[0] = POSSIBLE;
            leniencyArr[PhoneNumberUtil.NANPA_COUNTRY_CODE] = VALID;
            leniencyArr[PhoneNumberUtil.MIN_LENGTH_FOR_NSN] = STRICT_GROUPING;
            leniencyArr[PhoneNumberUtil.MAX_LENGTH_COUNTRY_CODE] = EXACT_GROUPING;
            $VALUES = leniencyArr;
        }
    }

    PhoneNumberUtil(String filePrefix, MetadataLoader metadataLoader2, Map<Integer, List<String>> countryCallingCodeToRegionCodeMap2) {
        this.currentFilePrefix = filePrefix;
        this.metadataLoader = metadataLoader2;
        this.countryCallingCodeToRegionCodeMap = countryCallingCodeToRegionCodeMap2;
        for (Map.Entry<Integer, List<String>> entry : countryCallingCodeToRegionCodeMap2.entrySet()) {
            List<String> regionCodes = entry.getValue();
            if (regionCodes.size() != NANPA_COUNTRY_CODE || !REGION_CODE_FOR_NON_GEO_ENTITY.equals(regionCodes.get(0))) {
                this.supportedRegions.addAll(regionCodes);
            } else {
                this.countryCodesForNonGeographicalRegion.add(entry.getKey());
            }
        }
        if (this.supportedRegions.remove(REGION_CODE_FOR_NON_GEO_ENTITY)) {
            logger.log(Level.WARNING, "invalid metadata (country calling code was mapped to the non-geo entity as well as specific region(s))");
        }
        this.nanpaRegions.addAll(countryCallingCodeToRegionCodeMap2.get(Integer.valueOf((int) NANPA_COUNTRY_CODE)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00d7  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00ec  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x013f  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0145  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void loadMetadataFromFile(java.lang.String r16, java.lang.String r17, int r18, com.google.i18n.phonenumbers.MetadataLoader r19) {
        /*
            r15 = this;
            java.lang.String r10 = "001"
            r0 = r17
            boolean r5 = r10.equals(r0)
            java.lang.String r10 = java.lang.String.valueOf(r16)
            java.lang.String r11 = java.lang.String.valueOf(r10)
            if (r5 == 0) goto L_0x0075
            java.lang.String r10 = java.lang.String.valueOf(r18)
        L_0x0016:
            java.lang.String r10 = java.lang.String.valueOf(r10)
            java.lang.String r10 = java.lang.String.valueOf(r10)
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            int r13 = r11.length()
            int r13 = r13 + 1
            int r14 = r10.length()
            int r13 = r13 + r14
            r12.<init>(r13)
            java.lang.StringBuilder r11 = r12.append(r11)
            java.lang.String r12 = "_"
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r10 = r11.append(r10)
            java.lang.String r2 = r10.toString()
            r0 = r19
            java.io.InputStream r9 = r0.loadMetadata(r2)
            if (r9 != 0) goto L_0x0084
            java.util.logging.Logger r11 = com.google.i18n.phonenumbers.PhoneNumberUtil.logger
            java.util.logging.Level r12 = java.util.logging.Level.SEVERE
            java.lang.String r13 = "missing metadata: "
            java.lang.String r10 = java.lang.String.valueOf(r2)
            int r14 = r10.length()
            if (r14 == 0) goto L_0x0078
            java.lang.String r10 = r13.concat(r10)
        L_0x005c:
            r11.log(r12, r10)
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException
            java.lang.String r12 = "missing metadata: "
            java.lang.String r10 = java.lang.String.valueOf(r2)
            int r13 = r10.length()
            if (r13 == 0) goto L_0x007e
            java.lang.String r10 = r12.concat(r10)
        L_0x0071:
            r11.<init>(r10)
            throw r11
        L_0x0075:
            r10 = r17
            goto L_0x0016
        L_0x0078:
            java.lang.String r10 = new java.lang.String
            r10.<init>(r13)
            goto L_0x005c
        L_0x007e:
            java.lang.String r10 = new java.lang.String
            r10.<init>(r12)
            goto L_0x0071
        L_0x0084:
            r3 = 0
            java.io.ObjectInputStream r4 = new java.io.ObjectInputStream     // Catch:{ IOException -> 0x014b }
            r4.<init>(r9)     // Catch:{ IOException -> 0x014b }
            com.google.i18n.phonenumbers.Phonemetadata$PhoneMetadataCollection r7 = loadMetadataAndCloseInput(r4)     // Catch:{ IOException -> 0x00c5 }
            java.util.List r8 = r7.getMetadataList()     // Catch:{ IOException -> 0x00c5 }
            boolean r10 = r8.isEmpty()     // Catch:{ IOException -> 0x00c5 }
            if (r10 == 0) goto L_0x0100
            java.util.logging.Logger r11 = com.google.i18n.phonenumbers.PhoneNumberUtil.logger     // Catch:{ IOException -> 0x00c5 }
            java.util.logging.Level r12 = java.util.logging.Level.SEVERE     // Catch:{ IOException -> 0x00c5 }
            java.lang.String r13 = "empty metadata: "
            java.lang.String r10 = java.lang.String.valueOf(r2)     // Catch:{ IOException -> 0x00c5 }
            int r14 = r10.length()     // Catch:{ IOException -> 0x00c5 }
            if (r14 == 0) goto L_0x00f4
            java.lang.String r10 = r13.concat(r10)     // Catch:{ IOException -> 0x00c5 }
        L_0x00ac:
            r11.log(r12, r10)     // Catch:{ IOException -> 0x00c5 }
            java.lang.IllegalStateException r11 = new java.lang.IllegalStateException     // Catch:{ IOException -> 0x00c5 }
            java.lang.String r12 = "empty metadata: "
            java.lang.String r10 = java.lang.String.valueOf(r2)     // Catch:{ IOException -> 0x00c5 }
            int r13 = r10.length()     // Catch:{ IOException -> 0x00c5 }
            if (r13 == 0) goto L_0x00fa
            java.lang.String r10 = r12.concat(r10)     // Catch:{ IOException -> 0x00c5 }
        L_0x00c1:
            r11.<init>(r10)     // Catch:{ IOException -> 0x00c5 }
            throw r11     // Catch:{ IOException -> 0x00c5 }
        L_0x00c5:
            r1 = move-exception
            r3 = r4
        L_0x00c7:
            java.util.logging.Logger r11 = com.google.i18n.phonenumbers.PhoneNumberUtil.logger
            java.util.logging.Level r12 = java.util.logging.Level.SEVERE
            java.lang.String r13 = "cannot load/parse metadata: "
            java.lang.String r10 = java.lang.String.valueOf(r2)
            int r14 = r10.length()
            if (r14 == 0) goto L_0x013f
            java.lang.String r10 = r13.concat(r10)
        L_0x00db:
            r11.log(r12, r10, r1)
            java.lang.RuntimeException r11 = new java.lang.RuntimeException
            java.lang.String r12 = "cannot load/parse metadata: "
            java.lang.String r10 = java.lang.String.valueOf(r2)
            int r13 = r10.length()
            if (r13 == 0) goto L_0x0145
            java.lang.String r10 = r12.concat(r10)
        L_0x00f0:
            r11.<init>(r10, r1)
            throw r11
        L_0x00f4:
            java.lang.String r10 = new java.lang.String     // Catch:{ IOException -> 0x00c5 }
            r10.<init>(r13)     // Catch:{ IOException -> 0x00c5 }
            goto L_0x00ac
        L_0x00fa:
            java.lang.String r10 = new java.lang.String     // Catch:{ IOException -> 0x00c5 }
            r10.<init>(r12)     // Catch:{ IOException -> 0x00c5 }
            goto L_0x00c1
        L_0x0100:
            int r10 = r8.size()     // Catch:{ IOException -> 0x00c5 }
            r11 = 1
            if (r10 <= r11) goto L_0x011e
            java.util.logging.Logger r11 = com.google.i18n.phonenumbers.PhoneNumberUtil.logger     // Catch:{ IOException -> 0x00c5 }
            java.util.logging.Level r12 = java.util.logging.Level.WARNING     // Catch:{ IOException -> 0x00c5 }
            java.lang.String r13 = "invalid metadata (too many entries): "
            java.lang.String r10 = java.lang.String.valueOf(r2)     // Catch:{ IOException -> 0x00c5 }
            int r14 = r10.length()     // Catch:{ IOException -> 0x00c5 }
            if (r14 == 0) goto L_0x0131
            java.lang.String r10 = r13.concat(r10)     // Catch:{ IOException -> 0x00c5 }
        L_0x011b:
            r11.log(r12, r10)     // Catch:{ IOException -> 0x00c5 }
        L_0x011e:
            r10 = 0
            java.lang.Object r6 = r8.get(r10)     // Catch:{ IOException -> 0x00c5 }
            com.google.i18n.phonenumbers.Phonemetadata$PhoneMetadata r6 = (com.google.i18n.phonenumbers.Phonemetadata.PhoneMetadata) r6     // Catch:{ IOException -> 0x00c5 }
            if (r5 == 0) goto L_0x0137
            java.util.Map<java.lang.Integer, com.google.i18n.phonenumbers.Phonemetadata$PhoneMetadata> r10 = r15.countryCodeToNonGeographicalMetadataMap     // Catch:{ IOException -> 0x00c5 }
            java.lang.Integer r11 = java.lang.Integer.valueOf(r18)     // Catch:{ IOException -> 0x00c5 }
            r10.put(r11, r6)     // Catch:{ IOException -> 0x00c5 }
        L_0x0130:
            return
        L_0x0131:
            java.lang.String r10 = new java.lang.String     // Catch:{ IOException -> 0x00c5 }
            r10.<init>(r13)     // Catch:{ IOException -> 0x00c5 }
            goto L_0x011b
        L_0x0137:
            java.util.Map<java.lang.String, com.google.i18n.phonenumbers.Phonemetadata$PhoneMetadata> r10 = r15.regionToMetadataMap     // Catch:{ IOException -> 0x00c5 }
            r0 = r17
            r10.put(r0, r6)     // Catch:{ IOException -> 0x00c5 }
            goto L_0x0130
        L_0x013f:
            java.lang.String r10 = new java.lang.String
            r10.<init>(r13)
            goto L_0x00db
        L_0x0145:
            java.lang.String r10 = new java.lang.String
            r10.<init>(r12)
            goto L_0x00f0
        L_0x014b:
            r1 = move-exception
            goto L_0x00c7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.i18n.phonenumbers.PhoneNumberUtil.loadMetadataFromFile(java.lang.String, java.lang.String, int, com.google.i18n.phonenumbers.MetadataLoader):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    private static Phonemetadata.PhoneMetadataCollection loadMetadataAndCloseInput(ObjectInputStream source) {
        Phonemetadata.PhoneMetadataCollection metadataCollection = new Phonemetadata.PhoneMetadataCollection();
        try {
            metadataCollection.readExternal(source);
            try {
                source.close();
            } catch (IOException e) {
                logger.log(Level.WARNING, "error closing input stream (ignored)", (Throwable) e);
            } catch (Throwable th) {
            }
        } catch (IOException e2) {
            logger.log(Level.WARNING, "error reading input (ignored)", (Throwable) e2);
            try {
                source.close();
            } catch (IOException e3) {
                logger.log(Level.WARNING, "error closing input stream (ignored)", (Throwable) e3);
            } catch (Throwable th2) {
            }
        } catch (Throwable th3) {
            try {
                source.close();
            } catch (IOException e4) {
                logger.log(Level.WARNING, "error closing input stream (ignored)", (Throwable) e4);
            } catch (Throwable th4) {
            }
        }
        return metadataCollection;
    }

    static String extractPossibleNumber(String number) {
        Matcher m = VALID_START_CHAR_PATTERN.matcher(number);
        if (!m.find()) {
            return "";
        }
        String number2 = number.substring(m.start());
        Matcher trailingCharsMatcher = UNWANTED_END_CHAR_PATTERN.matcher(number2);
        if (trailingCharsMatcher.find()) {
            number2 = number2.substring(0, trailingCharsMatcher.start());
            Logger logger2 = logger;
            Level level = Level.FINER;
            String valueOf = String.valueOf(number2);
            logger2.log(level, valueOf.length() != 0 ? "Stripped trailing characters: ".concat(valueOf) : new String("Stripped trailing characters: "));
        }
        Matcher secondNumber = SECOND_NUMBER_START_PATTERN.matcher(number2);
        if (secondNumber.find()) {
            return number2.substring(0, secondNumber.start());
        }
        return number2;
    }

    static boolean isViablePhoneNumber(String number) {
        if (number.length() < MIN_LENGTH_FOR_NSN) {
            return false;
        }
        return VALID_PHONE_NUMBER_PATTERN.matcher(number).matches();
    }

    static String normalize(String number) {
        if (VALID_ALPHA_PHONE_PATTERN.matcher(number).matches()) {
            return normalizeHelper(number, ALPHA_PHONE_MAPPINGS, true);
        }
        return normalizeDigitsOnly(number);
    }

    static void normalize(StringBuilder number) {
        number.replace(0, number.length(), normalize(number.toString()));
    }

    public static String normalizeDigitsOnly(String number) {
        return normalizeDigits(number, false).toString();
    }

    static StringBuilder normalizeDigits(String number, boolean keepNonDigits) {
        StringBuilder normalizedDigits = new StringBuilder(number.length());
        char[] arr$ = number.toCharArray();
        int len$ = arr$.length;
        for (int i$ = 0; i$ < len$; i$ += NANPA_COUNTRY_CODE) {
            char c = arr$[i$];
            int digit = Character.digit(c, 10);
            if (digit != -1) {
                normalizedDigits.append(digit);
            } else if (keepNonDigits) {
                normalizedDigits.append(c);
            }
        }
        return normalizedDigits;
    }

    static String normalizeDiallableCharsOnly(String number) {
        return normalizeHelper(number, DIALLABLE_CHAR_MAPPINGS, true);
    }

    public static String convertAlphaCharactersInNumber(String number) {
        return normalizeHelper(number, ALPHA_PHONE_MAPPINGS, false);
    }

    public int getLengthOfGeographicalAreaCode(Phonenumber.PhoneNumber number) {
        Phonemetadata.PhoneMetadata metadata = getMetadataForRegion(getRegionCodeForNumber(number));
        if (metadata == null) {
            return 0;
        }
        if ((metadata.hasNationalPrefix() || number.isItalianLeadingZero()) && isNumberGeographical(number)) {
            return getLengthOfNationalDestinationCode(number);
        }
        return 0;
    }

    public int getLengthOfNationalDestinationCode(Phonenumber.PhoneNumber number) {
        Phonenumber.PhoneNumber copiedProto;
        if (number.hasExtension()) {
            copiedProto = new Phonenumber.PhoneNumber();
            copiedProto.mergeFrom(number);
            copiedProto.clearExtension();
        } else {
            copiedProto = number;
        }
        String[] numberGroups = NON_DIGITS_PATTERN.split(format(copiedProto, PhoneNumberFormat.INTERNATIONAL));
        if (numberGroups.length <= MAX_LENGTH_COUNTRY_CODE) {
            return 0;
        }
        if (getNumberType(number) != PhoneNumberType.MOBILE || getCountryMobileToken(number.getCountryCode()).equals("")) {
            return numberGroups[MIN_LENGTH_FOR_NSN].length();
        }
        return numberGroups[MIN_LENGTH_FOR_NSN].length() + numberGroups[MAX_LENGTH_COUNTRY_CODE].length();
    }

    public static String getCountryMobileToken(int countryCallingCode) {
        if (MOBILE_TOKEN_MAPPINGS.containsKey(Integer.valueOf(countryCallingCode))) {
            return MOBILE_TOKEN_MAPPINGS.get(Integer.valueOf(countryCallingCode));
        }
        return "";
    }

    private static String normalizeHelper(String number, Map<Character, Character> normalizationReplacements, boolean removeNonMatches) {
        StringBuilder normalizedNumber = new StringBuilder(number.length());
        for (int i = 0; i < number.length(); i += NANPA_COUNTRY_CODE) {
            char character = number.charAt(i);
            Character newDigit = normalizationReplacements.get(Character.valueOf(Character.toUpperCase(character)));
            if (newDigit != null) {
                normalizedNumber.append(newDigit);
            } else if (!removeNonMatches) {
                normalizedNumber.append(character);
            }
        }
        return normalizedNumber.toString();
    }

    static synchronized void setInstance(PhoneNumberUtil util) {
        synchronized (PhoneNumberUtil.class) {
            instance = util;
        }
    }

    public Set<String> getSupportedRegions() {
        return Collections.unmodifiableSet(this.supportedRegions);
    }

    public Set<Integer> getSupportedGlobalNetworkCallingCodes() {
        return Collections.unmodifiableSet(this.countryCodesForNonGeographicalRegion);
    }

    public static synchronized PhoneNumberUtil getInstance() {
        PhoneNumberUtil phoneNumberUtil;
        synchronized (PhoneNumberUtil.class) {
            if (instance == null) {
                setInstance(createInstance(DEFAULT_METADATA_LOADER));
            }
            phoneNumberUtil = instance;
        }
        return phoneNumberUtil;
    }

    public static PhoneNumberUtil createInstance(MetadataLoader metadataLoader2) {
        if (metadataLoader2 != null) {
            return new PhoneNumberUtil(META_DATA_FILE_PREFIX, metadataLoader2, CountryCodeToRegionCodeMap.getCountryCodeToRegionCodeMap());
        }
        throw new IllegalArgumentException("metadataLoader could not be null.");
    }

    static boolean formattingRuleHasFirstGroupOnly(String nationalPrefixFormattingRule) {
        return nationalPrefixFormattingRule.length() == 0 || FIRST_GROUP_ONLY_PREFIX_PATTERN.matcher(nationalPrefixFormattingRule).matches();
    }

    /* access modifiers changed from: package-private */
    public boolean isNumberGeographical(Phonenumber.PhoneNumber phoneNumber) {
        PhoneNumberType numberType = getNumberType(phoneNumber);
        return numberType == PhoneNumberType.FIXED_LINE || numberType == PhoneNumberType.FIXED_LINE_OR_MOBILE;
    }

    private boolean isValidRegionCode(String regionCode) {
        return regionCode != null && this.supportedRegions.contains(regionCode);
    }

    private boolean hasValidCountryCallingCode(int countryCallingCode) {
        return this.countryCallingCodeToRegionCodeMap.containsKey(Integer.valueOf(countryCallingCode));
    }

    public String format(Phonenumber.PhoneNumber number, PhoneNumberFormat numberFormat) {
        if (number.getNationalNumber() == 0 && number.hasRawInput()) {
            String rawInput = number.getRawInput();
            if (rawInput.length() > 0) {
                return rawInput;
            }
        }
        StringBuilder formattedNumber = new StringBuilder(20);
        format(number, numberFormat, formattedNumber);
        return formattedNumber.toString();
    }

    public void format(Phonenumber.PhoneNumber number, PhoneNumberFormat numberFormat, StringBuilder formattedNumber) {
        formattedNumber.setLength(0);
        int countryCallingCode = number.getCountryCode();
        String nationalSignificantNumber = getNationalSignificantNumber(number);
        if (numberFormat == PhoneNumberFormat.E164) {
            formattedNumber.append(nationalSignificantNumber);
            prefixNumberWithCountryCallingCode(countryCallingCode, PhoneNumberFormat.E164, formattedNumber);
        } else if (!hasValidCountryCallingCode(countryCallingCode)) {
            formattedNumber.append(nationalSignificantNumber);
        } else {
            Phonemetadata.PhoneMetadata metadata = getMetadataForRegionOrCallingCode(countryCallingCode, getRegionCodeForCountryCode(countryCallingCode));
            formattedNumber.append(formatNsn(nationalSignificantNumber, metadata, numberFormat));
            maybeAppendFormattedExtension(number, metadata, numberFormat, formattedNumber);
            prefixNumberWithCountryCallingCode(countryCallingCode, numberFormat, formattedNumber);
        }
    }

    public String formatByPattern(Phonenumber.PhoneNumber number, PhoneNumberFormat numberFormat, List<Phonemetadata.NumberFormat> userDefinedFormats) {
        int countryCallingCode = number.getCountryCode();
        String nationalSignificantNumber = getNationalSignificantNumber(number);
        if (!hasValidCountryCallingCode(countryCallingCode)) {
            return nationalSignificantNumber;
        }
        Phonemetadata.PhoneMetadata metadata = getMetadataForRegionOrCallingCode(countryCallingCode, getRegionCodeForCountryCode(countryCallingCode));
        StringBuilder formattedNumber = new StringBuilder(20);
        Phonemetadata.NumberFormat formattingPattern = chooseFormattingPatternForNumber(userDefinedFormats, nationalSignificantNumber);
        if (formattingPattern == null) {
            formattedNumber.append(nationalSignificantNumber);
        } else {
            Phonemetadata.NumberFormat numFormatCopy = new Phonemetadata.NumberFormat();
            numFormatCopy.mergeFrom(formattingPattern);
            String nationalPrefixFormattingRule = formattingPattern.getNationalPrefixFormattingRule();
            if (nationalPrefixFormattingRule.length() > 0) {
                String nationalPrefix = metadata.getNationalPrefix();
                if (nationalPrefix.length() > 0) {
                    numFormatCopy.setNationalPrefixFormattingRule(FG_PATTERN.matcher(NP_PATTERN.matcher(nationalPrefixFormattingRule).replaceFirst(nationalPrefix)).replaceFirst("\\$1"));
                } else {
                    numFormatCopy.clearNationalPrefixFormattingRule();
                }
            }
            formattedNumber.append(formatNsnUsingPattern(nationalSignificantNumber, numFormatCopy, numberFormat));
        }
        maybeAppendFormattedExtension(number, metadata, numberFormat, formattedNumber);
        prefixNumberWithCountryCallingCode(countryCallingCode, numberFormat, formattedNumber);
        return formattedNumber.toString();
    }

    public String formatNationalNumberWithCarrierCode(Phonenumber.PhoneNumber number, String carrierCode) {
        int countryCallingCode = number.getCountryCode();
        String nationalSignificantNumber = getNationalSignificantNumber(number);
        if (!hasValidCountryCallingCode(countryCallingCode)) {
            return nationalSignificantNumber;
        }
        Phonemetadata.PhoneMetadata metadata = getMetadataForRegionOrCallingCode(countryCallingCode, getRegionCodeForCountryCode(countryCallingCode));
        StringBuilder formattedNumber = new StringBuilder(20);
        formattedNumber.append(formatNsn(nationalSignificantNumber, metadata, PhoneNumberFormat.NATIONAL, carrierCode));
        maybeAppendFormattedExtension(number, metadata, PhoneNumberFormat.NATIONAL, formattedNumber);
        prefixNumberWithCountryCallingCode(countryCallingCode, PhoneNumberFormat.NATIONAL, formattedNumber);
        return formattedNumber.toString();
    }

    private Phonemetadata.PhoneMetadata getMetadataForRegionOrCallingCode(int countryCallingCode, String regionCode) {
        return REGION_CODE_FOR_NON_GEO_ENTITY.equals(regionCode) ? getMetadataForNonGeographicalRegion(countryCallingCode) : getMetadataForRegion(regionCode);
    }

    public String formatNationalNumberWithPreferredCarrierCode(Phonenumber.PhoneNumber number, String fallbackCarrierCode) {
        if (number.hasPreferredDomesticCarrierCode()) {
            fallbackCarrierCode = number.getPreferredDomesticCarrierCode();
        }
        return formatNationalNumberWithCarrierCode(number, fallbackCarrierCode);
    }

    public String formatNumberForMobileDialing(Phonenumber.PhoneNumber number, String regionCallingFrom, boolean withFormatting) {
        int countryCallingCode = number.getCountryCode();
        if (hasValidCountryCallingCode(countryCallingCode)) {
            String formattedNumber = "";
            Phonenumber.PhoneNumber numberNoExt = new Phonenumber.PhoneNumber().mergeFrom(number).clearExtension();
            String regionCode = getRegionCodeForCountryCode(countryCallingCode);
            PhoneNumberType numberType = getNumberType(numberNoExt);
            boolean isValidNumber = numberType != PhoneNumberType.UNKNOWN;
            if (regionCallingFrom.equals(regionCode)) {
                boolean isFixedLineOrMobile = numberType == PhoneNumberType.FIXED_LINE || numberType == PhoneNumberType.MOBILE || numberType == PhoneNumberType.FIXED_LINE_OR_MOBILE;
                if (regionCode.equals("CO") && numberType == PhoneNumberType.FIXED_LINE) {
                    formattedNumber = formatNationalNumberWithCarrierCode(numberNoExt, COLOMBIA_MOBILE_TO_FIXED_LINE_PREFIX);
                } else if (regionCode.equals("BR") && isFixedLineOrMobile) {
                    formattedNumber = numberNoExt.hasPreferredDomesticCarrierCode() ? formatNationalNumberWithPreferredCarrierCode(numberNoExt, "") : "";
                } else if (isValidNumber && regionCode.equals("HU")) {
                    String valueOf = String.valueOf(String.valueOf(getNddPrefixForRegion(regionCode, true)));
                    String valueOf2 = String.valueOf(String.valueOf(format(numberNoExt, PhoneNumberFormat.NATIONAL)));
                    formattedNumber = new StringBuilder(valueOf.length() + NANPA_COUNTRY_CODE + valueOf2.length()).append(valueOf).append(" ").append(valueOf2).toString();
                } else if (countryCallingCode == NANPA_COUNTRY_CODE) {
                    formattedNumber = (!canBeInternationallyDialled(numberNoExt) || isShorterThanPossibleNormalNumber(getMetadataForRegion(regionCallingFrom), getNationalSignificantNumber(numberNoExt))) ? format(numberNoExt, PhoneNumberFormat.NATIONAL) : format(numberNoExt, PhoneNumberFormat.INTERNATIONAL);
                } else {
                    formattedNumber = ((regionCode.equals(REGION_CODE_FOR_NON_GEO_ENTITY) || ((regionCode.equals("MX") || regionCode.equals("CL")) && isFixedLineOrMobile)) && canBeInternationallyDialled(numberNoExt)) ? format(numberNoExt, PhoneNumberFormat.INTERNATIONAL) : format(numberNoExt, PhoneNumberFormat.NATIONAL);
                }
            } else if (isValidNumber && canBeInternationallyDialled(numberNoExt)) {
                return withFormatting ? format(numberNoExt, PhoneNumberFormat.INTERNATIONAL) : format(numberNoExt, PhoneNumberFormat.E164);
            }
            if (!withFormatting) {
                formattedNumber = normalizeDiallableCharsOnly(formattedNumber);
            }
            return formattedNumber;
        } else if (number.hasRawInput()) {
            return number.getRawInput();
        } else {
            return "";
        }
    }

    public String formatOutOfCountryCallingNumber(Phonenumber.PhoneNumber number, String regionCallingFrom) {
        if (!isValidRegionCode(regionCallingFrom)) {
            Logger logger2 = logger;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(String.valueOf(regionCallingFrom));
            logger2.log(level, new StringBuilder(valueOf.length() + 79).append("Trying to format number from invalid region ").append(valueOf).append(". International formatting applied.").toString());
            return format(number, PhoneNumberFormat.INTERNATIONAL);
        }
        int countryCallingCode = number.getCountryCode();
        String nationalSignificantNumber = getNationalSignificantNumber(number);
        if (!hasValidCountryCallingCode(countryCallingCode)) {
            return nationalSignificantNumber;
        }
        if (countryCallingCode == NANPA_COUNTRY_CODE) {
            if (isNANPACountry(regionCallingFrom)) {
                String valueOf2 = String.valueOf(String.valueOf(format(number, PhoneNumberFormat.NATIONAL)));
                return new StringBuilder(valueOf2.length() + 12).append(countryCallingCode).append(" ").append(valueOf2).toString();
            }
        } else if (countryCallingCode == getCountryCodeForValidRegion(regionCallingFrom)) {
            return format(number, PhoneNumberFormat.NATIONAL);
        }
        Phonemetadata.PhoneMetadata metadataForRegionCallingFrom = getMetadataForRegion(regionCallingFrom);
        String internationalPrefix = metadataForRegionCallingFrom.getInternationalPrefix();
        String internationalPrefixForFormatting = "";
        if (UNIQUE_INTERNATIONAL_PREFIX.matcher(internationalPrefix).matches()) {
            internationalPrefixForFormatting = internationalPrefix;
        } else if (metadataForRegionCallingFrom.hasPreferredInternationalPrefix()) {
            internationalPrefixForFormatting = metadataForRegionCallingFrom.getPreferredInternationalPrefix();
        }
        Phonemetadata.PhoneMetadata metadataForRegion = getMetadataForRegionOrCallingCode(countryCallingCode, getRegionCodeForCountryCode(countryCallingCode));
        StringBuilder formattedNumber = new StringBuilder(formatNsn(nationalSignificantNumber, metadataForRegion, PhoneNumberFormat.INTERNATIONAL));
        maybeAppendFormattedExtension(number, metadataForRegion, PhoneNumberFormat.INTERNATIONAL, formattedNumber);
        if (internationalPrefixForFormatting.length() > 0) {
            formattedNumber.insert(0, " ").insert(0, countryCallingCode).insert(0, " ").insert(0, internationalPrefixForFormatting);
        } else {
            prefixNumberWithCountryCallingCode(countryCallingCode, PhoneNumberFormat.INTERNATIONAL, formattedNumber);
        }
        return formattedNumber.toString();
    }

    public String formatInOriginalFormat(Phonenumber.PhoneNumber number, String regionCallingFrom) {
        String formattedNumber;
        if (number.hasRawInput() && (hasUnexpectedItalianLeadingZero(number) || !hasFormattingPatternForNumber(number))) {
            return number.getRawInput();
        }
        if (!number.hasCountryCodeSource()) {
            return format(number, PhoneNumberFormat.NATIONAL);
        }
        switch (AnonymousClass3.$SwitchMap$com$google$i18n$phonenumbers$Phonenumber$PhoneNumber$CountryCodeSource[number.getCountryCodeSource().ordinal()]) {
            case NANPA_COUNTRY_CODE /*1*/:
                formattedNumber = format(number, PhoneNumberFormat.INTERNATIONAL);
                break;
            case MIN_LENGTH_FOR_NSN /*2*/:
                formattedNumber = formatOutOfCountryCallingNumber(number, regionCallingFrom);
                break;
            case MAX_LENGTH_COUNTRY_CODE /*3*/:
                formattedNumber = format(number, PhoneNumberFormat.INTERNATIONAL).substring(NANPA_COUNTRY_CODE);
                break;
            default:
                String regionCode = getRegionCodeForCountryCode(number.getCountryCode());
                String nationalPrefix = getNddPrefixForRegion(regionCode, NANPA_COUNTRY_CODE);
                String nationalFormat = format(number, PhoneNumberFormat.NATIONAL);
                if (nationalPrefix != null && nationalPrefix.length() != 0) {
                    if (!rawInputContainsNationalPrefix(number.getRawInput(), nationalPrefix, regionCode)) {
                        Phonemetadata.PhoneMetadata metadata = getMetadataForRegion(regionCode);
                        Phonemetadata.NumberFormat formatRule = chooseFormattingPatternForNumber(metadata.numberFormats(), getNationalSignificantNumber(number));
                        if (formatRule != null) {
                            String candidateNationalPrefixRule = formatRule.getNationalPrefixFormattingRule();
                            int indexOfFirstGroup = candidateNationalPrefixRule.indexOf("$1");
                            if (indexOfFirstGroup > 0) {
                                if (normalizeDigitsOnly(candidateNationalPrefixRule.substring(0, indexOfFirstGroup)).length() != 0) {
                                    Phonemetadata.NumberFormat numFormatCopy = new Phonemetadata.NumberFormat();
                                    numFormatCopy.mergeFrom(formatRule);
                                    numFormatCopy.clearNationalPrefixFormattingRule();
                                    List<Phonemetadata.NumberFormat> numberFormats = new ArrayList<>(NANPA_COUNTRY_CODE);
                                    numberFormats.add(numFormatCopy);
                                    formattedNumber = formatByPattern(number, PhoneNumberFormat.NATIONAL, numberFormats);
                                    break;
                                } else {
                                    formattedNumber = nationalFormat;
                                    break;
                                }
                            } else {
                                formattedNumber = nationalFormat;
                                break;
                            }
                        } else {
                            formattedNumber = nationalFormat;
                            break;
                        }
                    } else {
                        formattedNumber = nationalFormat;
                        break;
                    }
                } else {
                    formattedNumber = nationalFormat;
                    break;
                }
        }
        String rawInput = number.getRawInput();
        if (formattedNumber == null || rawInput.length() <= 0 || normalizeDiallableCharsOnly(formattedNumber).equals(normalizeDiallableCharsOnly(rawInput))) {
            return formattedNumber;
        }
        return rawInput;
    }

    private boolean rawInputContainsNationalPrefix(String rawInput, String nationalPrefix, String regionCode) {
        String normalizedNationalNumber = normalizeDigitsOnly(rawInput);
        if (!normalizedNationalNumber.startsWith(nationalPrefix)) {
            return false;
        }
        try {
            return isValidNumber(parse(normalizedNationalNumber.substring(nationalPrefix.length()), regionCode));
        } catch (NumberParseException e) {
            return false;
        }
    }

    private boolean hasUnexpectedItalianLeadingZero(Phonenumber.PhoneNumber number) {
        return number.isItalianLeadingZero() && !isLeadingZeroPossible(number.getCountryCode());
    }

    private boolean hasFormattingPatternForNumber(Phonenumber.PhoneNumber number) {
        int countryCallingCode = number.getCountryCode();
        Phonemetadata.PhoneMetadata metadata = getMetadataForRegionOrCallingCode(countryCallingCode, getRegionCodeForCountryCode(countryCallingCode));
        if (metadata == null) {
            return false;
        }
        if (chooseFormattingPatternForNumber(metadata.numberFormats(), getNationalSignificantNumber(number)) != null) {
            return true;
        }
        return false;
    }

    public String formatOutOfCountryKeepingAlphaChars(Phonenumber.PhoneNumber number, String regionCallingFrom) {
        int firstNationalNumberDigit;
        String rawInput = number.getRawInput();
        if (rawInput.length() == 0) {
            return formatOutOfCountryCallingNumber(number, regionCallingFrom);
        }
        int countryCode = number.getCountryCode();
        if (!hasValidCountryCallingCode(countryCode)) {
            return rawInput;
        }
        String rawInput2 = normalizeHelper(rawInput, ALL_PLUS_NUMBER_GROUPING_SYMBOLS, true);
        String nationalNumber = getNationalSignificantNumber(number);
        if (nationalNumber.length() > MAX_LENGTH_COUNTRY_CODE && (firstNationalNumberDigit = rawInput2.indexOf(nationalNumber.substring(0, MAX_LENGTH_COUNTRY_CODE))) != -1) {
            rawInput2 = rawInput2.substring(firstNationalNumberDigit);
        }
        Phonemetadata.PhoneMetadata metadataForRegionCallingFrom = getMetadataForRegion(regionCallingFrom);
        if (countryCode == NANPA_COUNTRY_CODE) {
            if (isNANPACountry(regionCallingFrom)) {
                String valueOf = String.valueOf(String.valueOf(rawInput2));
                return new StringBuilder(valueOf.length() + 12).append(countryCode).append(" ").append(valueOf).toString();
            }
        } else if (metadataForRegionCallingFrom != null && countryCode == getCountryCodeForValidRegion(regionCallingFrom)) {
            Phonemetadata.NumberFormat formattingPattern = chooseFormattingPatternForNumber(metadataForRegionCallingFrom.numberFormats(), nationalNumber);
            if (formattingPattern == null) {
                return rawInput2;
            }
            Phonemetadata.NumberFormat newFormat = new Phonemetadata.NumberFormat();
            newFormat.mergeFrom(formattingPattern);
            newFormat.setPattern("(\\d+)(.*)");
            newFormat.setFormat("$1$2");
            return formatNsnUsingPattern(rawInput2, newFormat, PhoneNumberFormat.NATIONAL);
        }
        String internationalPrefixForFormatting = "";
        if (metadataForRegionCallingFrom != null) {
            String internationalPrefix = metadataForRegionCallingFrom.getInternationalPrefix();
            internationalPrefixForFormatting = UNIQUE_INTERNATIONAL_PREFIX.matcher(internationalPrefix).matches() ? internationalPrefix : metadataForRegionCallingFrom.getPreferredInternationalPrefix();
        }
        StringBuilder formattedNumber = new StringBuilder(rawInput2);
        maybeAppendFormattedExtension(number, getMetadataForRegionOrCallingCode(countryCode, getRegionCodeForCountryCode(countryCode)), PhoneNumberFormat.INTERNATIONAL, formattedNumber);
        if (internationalPrefixForFormatting.length() > 0) {
            formattedNumber.insert(0, " ").insert(0, countryCode).insert(0, " ").insert(0, internationalPrefixForFormatting);
        } else {
            Logger logger2 = logger;
            Level level = Level.WARNING;
            String valueOf2 = String.valueOf(String.valueOf(regionCallingFrom));
            logger2.log(level, new StringBuilder(valueOf2.length() + 79).append("Trying to format number from invalid region ").append(valueOf2).append(". International formatting applied.").toString());
            prefixNumberWithCountryCallingCode(countryCode, PhoneNumberFormat.INTERNATIONAL, formattedNumber);
        }
        return formattedNumber.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(char[], char):void}
     arg types: [char[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(long[], long):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void}
      ClspMth{java.util.Arrays.fill(char[], char):void} */
    public String getNationalSignificantNumber(Phonenumber.PhoneNumber number) {
        StringBuilder nationalNumber = new StringBuilder();
        if (number.isItalianLeadingZero()) {
            char[] zeros = new char[number.getNumberOfLeadingZeros()];
            Arrays.fill(zeros, '0');
            nationalNumber.append(new String(zeros));
        }
        nationalNumber.append(number.getNationalNumber());
        return nationalNumber.toString();
    }

    private void prefixNumberWithCountryCallingCode(int countryCallingCode, PhoneNumberFormat numberFormat, StringBuilder formattedNumber) {
        switch (AnonymousClass3.$SwitchMap$com$google$i18n$phonenumbers$PhoneNumberUtil$PhoneNumberFormat[numberFormat.ordinal()]) {
            case NANPA_COUNTRY_CODE /*1*/:
                formattedNumber.insert(0, countryCallingCode).insert(0, (char) PLUS_SIGN);
                return;
            case MIN_LENGTH_FOR_NSN /*2*/:
                formattedNumber.insert(0, " ").insert(0, countryCallingCode).insert(0, (char) PLUS_SIGN);
                return;
            case MAX_LENGTH_COUNTRY_CODE /*3*/:
                formattedNumber.insert(0, "-").insert(0, countryCallingCode).insert(0, (char) PLUS_SIGN).insert(0, RFC3966_PREFIX);
                return;
            default:
                return;
        }
    }

    private String formatNsn(String number, Phonemetadata.PhoneMetadata metadata, PhoneNumberFormat numberFormat) {
        return formatNsn(number, metadata, numberFormat, null);
    }

    private String formatNsn(String number, Phonemetadata.PhoneMetadata metadata, PhoneNumberFormat numberFormat, String carrierCode) {
        Phonemetadata.NumberFormat formattingPattern = chooseFormattingPatternForNumber((metadata.intlNumberFormats().size() == 0 || numberFormat == PhoneNumberFormat.NATIONAL) ? metadata.numberFormats() : metadata.intlNumberFormats(), number);
        return formattingPattern == null ? number : formatNsnUsingPattern(number, formattingPattern, numberFormat, carrierCode);
    }

    /* access modifiers changed from: package-private */
    public Phonemetadata.NumberFormat chooseFormattingPatternForNumber(List<Phonemetadata.NumberFormat> availableFormats, String nationalNumber) {
        for (Phonemetadata.NumberFormat numFormat : availableFormats) {
            int size = numFormat.leadingDigitsPatternSize();
            if ((size == 0 || this.regexCache.getPatternForRegex(numFormat.getLeadingDigitsPattern(size - 1)).matcher(nationalNumber).lookingAt()) && this.regexCache.getPatternForRegex(numFormat.getPattern()).matcher(nationalNumber).matches()) {
                return numFormat;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public String formatNsnUsingPattern(String nationalNumber, Phonemetadata.NumberFormat formattingPattern, PhoneNumberFormat numberFormat) {
        return formatNsnUsingPattern(nationalNumber, formattingPattern, numberFormat, null);
    }

    private String formatNsnUsingPattern(String nationalNumber, Phonemetadata.NumberFormat formattingPattern, PhoneNumberFormat numberFormat, String carrierCode) {
        String formattedNationalNumber;
        String numberFormatRule = formattingPattern.getFormat();
        Matcher m = this.regexCache.getPatternForRegex(formattingPattern.getPattern()).matcher(nationalNumber);
        if (numberFormat != PhoneNumberFormat.NATIONAL || carrierCode == null || carrierCode.length() <= 0 || formattingPattern.getDomesticCarrierCodeFormattingRule().length() <= 0) {
            String nationalPrefixFormattingRule = formattingPattern.getNationalPrefixFormattingRule();
            if (numberFormat != PhoneNumberFormat.NATIONAL || nationalPrefixFormattingRule == null || nationalPrefixFormattingRule.length() <= 0) {
                formattedNationalNumber = m.replaceAll(numberFormatRule);
            } else {
                formattedNationalNumber = m.replaceAll(FIRST_GROUP_PATTERN.matcher(numberFormatRule).replaceFirst(nationalPrefixFormattingRule));
            }
        } else {
            formattedNationalNumber = m.replaceAll(FIRST_GROUP_PATTERN.matcher(numberFormatRule).replaceFirst(CC_PATTERN.matcher(formattingPattern.getDomesticCarrierCodeFormattingRule()).replaceFirst(carrierCode)));
        }
        if (numberFormat != PhoneNumberFormat.RFC3966) {
            return formattedNationalNumber;
        }
        Matcher matcher = SEPARATOR_PATTERN.matcher(formattedNationalNumber);
        if (matcher.lookingAt()) {
            formattedNationalNumber = matcher.replaceFirst("");
        }
        return matcher.reset(formattedNationalNumber).replaceAll("-");
    }

    public Phonenumber.PhoneNumber getExampleNumber(String regionCode) {
        return getExampleNumberForType(regionCode, PhoneNumberType.FIXED_LINE);
    }

    public Phonenumber.PhoneNumber getExampleNumberForType(String regionCode, PhoneNumberType type) {
        String str;
        if (!isValidRegionCode(regionCode)) {
            Logger logger2 = logger;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(regionCode);
            if (valueOf.length() != 0) {
                str = "Invalid or unknown region code provided: ".concat(valueOf);
            } else {
                str = new String("Invalid or unknown region code provided: ");
            }
            logger2.log(level, str);
            return null;
        }
        Phonemetadata.PhoneNumberDesc desc = getNumberDescByType(getMetadataForRegion(regionCode), type);
        try {
            if (desc.hasExampleNumber()) {
                return parse(desc.getExampleNumber(), regionCode);
            }
        } catch (NumberParseException e) {
            logger.log(Level.SEVERE, e.toString());
        }
        return null;
    }

    public Phonenumber.PhoneNumber getExampleNumberForNonGeoEntity(int countryCallingCode) {
        Phonemetadata.PhoneMetadata metadata = getMetadataForNonGeographicalRegion(countryCallingCode);
        if (metadata != null) {
            Phonemetadata.PhoneNumberDesc desc = metadata.getGeneralDesc();
            try {
                if (desc.hasExampleNumber()) {
                    String valueOf = String.valueOf(String.valueOf(desc.getExampleNumber()));
                    return parse(new StringBuilder(valueOf.length() + 12).append("+").append(countryCallingCode).append(valueOf).toString(), UNKNOWN_REGION);
                }
            } catch (NumberParseException e) {
                logger.log(Level.SEVERE, e.toString());
            }
        } else {
            logger.log(Level.WARNING, new StringBuilder(61).append("Invalid or unknown country calling code provided: ").append(countryCallingCode).toString());
        }
        return null;
    }

    private void maybeAppendFormattedExtension(Phonenumber.PhoneNumber number, Phonemetadata.PhoneMetadata metadata, PhoneNumberFormat numberFormat, StringBuilder formattedNumber) {
        if (number.hasExtension() && number.getExtension().length() > 0) {
            if (numberFormat == PhoneNumberFormat.RFC3966) {
                formattedNumber.append(RFC3966_EXTN_PREFIX).append(number.getExtension());
            } else if (metadata.hasPreferredExtnPrefix()) {
                formattedNumber.append(metadata.getPreferredExtnPrefix()).append(number.getExtension());
            } else {
                formattedNumber.append(DEFAULT_EXTN_PREFIX).append(number.getExtension());
            }
        }
    }

    /* renamed from: com.google.i18n.phonenumbers.PhoneNumberUtil$3  reason: invalid class name */
    static /* synthetic */ class AnonymousClass3 {
        static final /* synthetic */ int[] $SwitchMap$com$google$i18n$phonenumbers$PhoneNumberUtil$PhoneNumberFormat = new int[PhoneNumberFormat.values().length];
        static final /* synthetic */ int[] $SwitchMap$com$google$i18n$phonenumbers$PhoneNumberUtil$PhoneNumberType = new int[PhoneNumberType.values().length];
        static final /* synthetic */ int[] $SwitchMap$com$google$i18n$phonenumbers$Phonenumber$PhoneNumber$CountryCodeSource = new int[Phonenumber.PhoneNumber.CountryCodeSource.values().length];

        static {
            try {
                $SwitchMap$com$google$i18n$phonenumbers$PhoneNumberUtil$PhoneNumberType[PhoneNumberType.PREMIUM_RATE.ordinal()] = PhoneNumberUtil.NANPA_COUNTRY_CODE;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$google$i18n$phonenumbers$PhoneNumberUtil$PhoneNumberType[PhoneNumberType.TOLL_FREE.ordinal()] = PhoneNumberUtil.MIN_LENGTH_FOR_NSN;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$google$i18n$phonenumbers$PhoneNumberUtil$PhoneNumberType[PhoneNumberType.MOBILE.ordinal()] = PhoneNumberUtil.MAX_LENGTH_COUNTRY_CODE;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$google$i18n$phonenumbers$PhoneNumberUtil$PhoneNumberType[PhoneNumberType.FIXED_LINE.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$google$i18n$phonenumbers$PhoneNumberUtil$PhoneNumberType[PhoneNumberType.FIXED_LINE_OR_MOBILE.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$google$i18n$phonenumbers$PhoneNumberUtil$PhoneNumberType[PhoneNumberType.SHARED_COST.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$google$i18n$phonenumbers$PhoneNumberUtil$PhoneNumberType[PhoneNumberType.VOIP.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$google$i18n$phonenumbers$PhoneNumberUtil$PhoneNumberType[PhoneNumberType.PERSONAL_NUMBER.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$google$i18n$phonenumbers$PhoneNumberUtil$PhoneNumberType[PhoneNumberType.PAGER.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$com$google$i18n$phonenumbers$PhoneNumberUtil$PhoneNumberType[PhoneNumberType.UAN.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$com$google$i18n$phonenumbers$PhoneNumberUtil$PhoneNumberType[PhoneNumberType.VOICEMAIL.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                $SwitchMap$com$google$i18n$phonenumbers$PhoneNumberUtil$PhoneNumberFormat[PhoneNumberFormat.E164.ordinal()] = PhoneNumberUtil.NANPA_COUNTRY_CODE;
            } catch (NoSuchFieldError e12) {
            }
            try {
                $SwitchMap$com$google$i18n$phonenumbers$PhoneNumberUtil$PhoneNumberFormat[PhoneNumberFormat.INTERNATIONAL.ordinal()] = PhoneNumberUtil.MIN_LENGTH_FOR_NSN;
            } catch (NoSuchFieldError e13) {
            }
            try {
                $SwitchMap$com$google$i18n$phonenumbers$PhoneNumberUtil$PhoneNumberFormat[PhoneNumberFormat.RFC3966.ordinal()] = PhoneNumberUtil.MAX_LENGTH_COUNTRY_CODE;
            } catch (NoSuchFieldError e14) {
            }
            try {
                $SwitchMap$com$google$i18n$phonenumbers$PhoneNumberUtil$PhoneNumberFormat[PhoneNumberFormat.NATIONAL.ordinal()] = 4;
            } catch (NoSuchFieldError e15) {
            }
            try {
                $SwitchMap$com$google$i18n$phonenumbers$Phonenumber$PhoneNumber$CountryCodeSource[Phonenumber.PhoneNumber.CountryCodeSource.FROM_NUMBER_WITH_PLUS_SIGN.ordinal()] = PhoneNumberUtil.NANPA_COUNTRY_CODE;
            } catch (NoSuchFieldError e16) {
            }
            try {
                $SwitchMap$com$google$i18n$phonenumbers$Phonenumber$PhoneNumber$CountryCodeSource[Phonenumber.PhoneNumber.CountryCodeSource.FROM_NUMBER_WITH_IDD.ordinal()] = PhoneNumberUtil.MIN_LENGTH_FOR_NSN;
            } catch (NoSuchFieldError e17) {
            }
            try {
                $SwitchMap$com$google$i18n$phonenumbers$Phonenumber$PhoneNumber$CountryCodeSource[Phonenumber.PhoneNumber.CountryCodeSource.FROM_NUMBER_WITHOUT_PLUS_SIGN.ordinal()] = PhoneNumberUtil.MAX_LENGTH_COUNTRY_CODE;
            } catch (NoSuchFieldError e18) {
            }
            try {
                $SwitchMap$com$google$i18n$phonenumbers$Phonenumber$PhoneNumber$CountryCodeSource[Phonenumber.PhoneNumber.CountryCodeSource.FROM_DEFAULT_COUNTRY.ordinal()] = 4;
            } catch (NoSuchFieldError e19) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Phonemetadata.PhoneNumberDesc getNumberDescByType(Phonemetadata.PhoneMetadata metadata, PhoneNumberType type) {
        switch (AnonymousClass3.$SwitchMap$com$google$i18n$phonenumbers$PhoneNumberUtil$PhoneNumberType[type.ordinal()]) {
            case NANPA_COUNTRY_CODE /*1*/:
                return metadata.getPremiumRate();
            case MIN_LENGTH_FOR_NSN /*2*/:
                return metadata.getTollFree();
            case MAX_LENGTH_COUNTRY_CODE /*3*/:
                return metadata.getMobile();
            case 4:
            case 5:
                return metadata.getFixedLine();
            case 6:
                return metadata.getSharedCost();
            case 7:
                return metadata.getVoip();
            case 8:
                return metadata.getPersonalNumber();
            case 9:
                return metadata.getPager();
            case 10:
                return metadata.getUan();
            case 11:
                return metadata.getVoicemail();
            default:
                return metadata.getGeneralDesc();
        }
    }

    public PhoneNumberType getNumberType(Phonenumber.PhoneNumber number) {
        Phonemetadata.PhoneMetadata metadata = getMetadataForRegionOrCallingCode(number.getCountryCode(), getRegionCodeForNumber(number));
        if (metadata == null) {
            return PhoneNumberType.UNKNOWN;
        }
        return getNumberTypeHelper(getNationalSignificantNumber(number), metadata);
    }

    private PhoneNumberType getNumberTypeHelper(String nationalNumber, Phonemetadata.PhoneMetadata metadata) {
        Phonemetadata.PhoneNumberDesc generalNumberDesc = metadata.getGeneralDesc();
        if (!generalNumberDesc.hasNationalNumberPattern() || !isNumberMatchingDesc(nationalNumber, generalNumberDesc)) {
            return PhoneNumberType.UNKNOWN;
        }
        if (isNumberMatchingDesc(nationalNumber, metadata.getPremiumRate())) {
            return PhoneNumberType.PREMIUM_RATE;
        }
        if (isNumberMatchingDesc(nationalNumber, metadata.getTollFree())) {
            return PhoneNumberType.TOLL_FREE;
        }
        if (isNumberMatchingDesc(nationalNumber, metadata.getSharedCost())) {
            return PhoneNumberType.SHARED_COST;
        }
        if (isNumberMatchingDesc(nationalNumber, metadata.getVoip())) {
            return PhoneNumberType.VOIP;
        }
        if (isNumberMatchingDesc(nationalNumber, metadata.getPersonalNumber())) {
            return PhoneNumberType.PERSONAL_NUMBER;
        }
        if (isNumberMatchingDesc(nationalNumber, metadata.getPager())) {
            return PhoneNumberType.PAGER;
        }
        if (isNumberMatchingDesc(nationalNumber, metadata.getUan())) {
            return PhoneNumberType.UAN;
        }
        if (isNumberMatchingDesc(nationalNumber, metadata.getVoicemail())) {
            return PhoneNumberType.VOICEMAIL;
        }
        if (isNumberMatchingDesc(nationalNumber, metadata.getFixedLine())) {
            if (metadata.isSameMobileAndFixedLinePattern()) {
                return PhoneNumberType.FIXED_LINE_OR_MOBILE;
            }
            if (isNumberMatchingDesc(nationalNumber, metadata.getMobile())) {
                return PhoneNumberType.FIXED_LINE_OR_MOBILE;
            }
            return PhoneNumberType.FIXED_LINE;
        } else if (metadata.isSameMobileAndFixedLinePattern() || !isNumberMatchingDesc(nationalNumber, metadata.getMobile())) {
            return PhoneNumberType.UNKNOWN;
        } else {
            return PhoneNumberType.MOBILE;
        }
    }

    /* access modifiers changed from: package-private */
    public Phonemetadata.PhoneMetadata getMetadataForRegion(String regionCode) {
        if (!isValidRegionCode(regionCode)) {
            return null;
        }
        synchronized (this.regionToMetadataMap) {
            if (!this.regionToMetadataMap.containsKey(regionCode)) {
                loadMetadataFromFile(this.currentFilePrefix, regionCode, 0, this.metadataLoader);
            }
        }
        return this.regionToMetadataMap.get(regionCode);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return r4.countryCodeToNonGeographicalMetadataMap.get(java.lang.Integer.valueOf(r5));
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.google.i18n.phonenumbers.Phonemetadata.PhoneMetadata getMetadataForNonGeographicalRegion(int r5) {
        /*
            r4 = this;
            java.util.Map<java.lang.Integer, com.google.i18n.phonenumbers.Phonemetadata$PhoneMetadata> r1 = r4.countryCodeToNonGeographicalMetadataMap
            monitor-enter(r1)
            java.util.Map<java.lang.Integer, java.util.List<java.lang.String>> r0 = r4.countryCallingCodeToRegionCodeMap     // Catch:{ all -> 0x0035 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x0035 }
            boolean r0 = r0.containsKey(r2)     // Catch:{ all -> 0x0035 }
            if (r0 != 0) goto L_0x0012
            r0 = 0
            monitor-exit(r1)     // Catch:{ all -> 0x0035 }
        L_0x0011:
            return r0
        L_0x0012:
            java.util.Map<java.lang.Integer, com.google.i18n.phonenumbers.Phonemetadata$PhoneMetadata> r0 = r4.countryCodeToNonGeographicalMetadataMap     // Catch:{ all -> 0x0035 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x0035 }
            boolean r0 = r0.containsKey(r2)     // Catch:{ all -> 0x0035 }
            if (r0 != 0) goto L_0x0027
            java.lang.String r0 = r4.currentFilePrefix     // Catch:{ all -> 0x0035 }
            java.lang.String r2 = "001"
            com.google.i18n.phonenumbers.MetadataLoader r3 = r4.metadataLoader     // Catch:{ all -> 0x0035 }
            r4.loadMetadataFromFile(r0, r2, r5, r3)     // Catch:{ all -> 0x0035 }
        L_0x0027:
            monitor-exit(r1)     // Catch:{ all -> 0x0035 }
            java.util.Map<java.lang.Integer, com.google.i18n.phonenumbers.Phonemetadata$PhoneMetadata> r0 = r4.countryCodeToNonGeographicalMetadataMap
            java.lang.Integer r1 = java.lang.Integer.valueOf(r5)
            java.lang.Object r0 = r0.get(r1)
            com.google.i18n.phonenumbers.Phonemetadata$PhoneMetadata r0 = (com.google.i18n.phonenumbers.Phonemetadata.PhoneMetadata) r0
            goto L_0x0011
        L_0x0035:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0035 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.i18n.phonenumbers.PhoneNumberUtil.getMetadataForNonGeographicalRegion(int):com.google.i18n.phonenumbers.Phonemetadata$PhoneMetadata");
    }

    /* access modifiers changed from: package-private */
    public boolean isNumberPossibleForDesc(String nationalNumber, Phonemetadata.PhoneNumberDesc numberDesc) {
        return this.regexCache.getPatternForRegex(numberDesc.getPossibleNumberPattern()).matcher(nationalNumber).matches();
    }

    /* access modifiers changed from: package-private */
    public boolean isNumberMatchingDesc(String nationalNumber, Phonemetadata.PhoneNumberDesc numberDesc) {
        return isNumberPossibleForDesc(nationalNumber, numberDesc) && this.regexCache.getPatternForRegex(numberDesc.getNationalNumberPattern()).matcher(nationalNumber).matches();
    }

    public boolean isValidNumber(Phonenumber.PhoneNumber number) {
        return isValidNumberForRegion(number, getRegionCodeForNumber(number));
    }

    public boolean isValidNumberForRegion(Phonenumber.PhoneNumber number, String regionCode) {
        int countryCode = number.getCountryCode();
        Phonemetadata.PhoneMetadata metadata = getMetadataForRegionOrCallingCode(countryCode, regionCode);
        if (metadata == null || (!REGION_CODE_FOR_NON_GEO_ENTITY.equals(regionCode) && countryCode != getCountryCodeForValidRegion(regionCode))) {
            return false;
        }
        Phonemetadata.PhoneNumberDesc generalNumDesc = metadata.getGeneralDesc();
        String nationalSignificantNumber = getNationalSignificantNumber(number);
        if (!generalNumDesc.hasNationalNumberPattern()) {
            int numberLength = nationalSignificantNumber.length();
            if (numberLength <= MIN_LENGTH_FOR_NSN || numberLength > MAX_LENGTH_FOR_NSN) {
                return false;
            }
            return true;
        } else if (getNumberTypeHelper(nationalSignificantNumber, metadata) == PhoneNumberType.UNKNOWN) {
            return false;
        } else {
            return true;
        }
    }

    public String getRegionCodeForNumber(Phonenumber.PhoneNumber number) {
        int countryCode = number.getCountryCode();
        List<String> regions = this.countryCallingCodeToRegionCodeMap.get(Integer.valueOf(countryCode));
        if (regions == null) {
            String numberString = getNationalSignificantNumber(number);
            Logger logger2 = logger;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(String.valueOf(numberString));
            logger2.log(level, new StringBuilder(valueOf.length() + 54).append("Missing/invalid country_code (").append(countryCode).append(") for number ").append(valueOf).toString());
            return null;
        } else if (regions.size() == NANPA_COUNTRY_CODE) {
            return (String) regions.get(0);
        } else {
            return getRegionCodeForNumberFromRegionList(number, regions);
        }
    }

    private String getRegionCodeForNumberFromRegionList(Phonenumber.PhoneNumber number, List<String> regionCodes) {
        String nationalNumber = getNationalSignificantNumber(number);
        for (String regionCode : regionCodes) {
            Phonemetadata.PhoneMetadata metadata = getMetadataForRegion(regionCode);
            if (metadata.hasLeadingDigits()) {
                if (this.regexCache.getPatternForRegex(metadata.getLeadingDigits()).matcher(nationalNumber).lookingAt()) {
                    return regionCode;
                }
            } else if (getNumberTypeHelper(nationalNumber, metadata) != PhoneNumberType.UNKNOWN) {
                return regionCode;
            }
        }
        return null;
    }

    public String getRegionCodeForCountryCode(int countryCallingCode) {
        List<String> regionCodes = this.countryCallingCodeToRegionCodeMap.get(Integer.valueOf(countryCallingCode));
        return regionCodes == null ? UNKNOWN_REGION : (String) regionCodes.get(0);
    }

    public List<String> getRegionCodesForCountryCode(int countryCallingCode) {
        List<String> regionCodes = this.countryCallingCodeToRegionCodeMap.get(Integer.valueOf(countryCallingCode));
        if (regionCodes == null) {
            regionCodes = new ArrayList<>(0);
        }
        return Collections.unmodifiableList(regionCodes);
    }

    public int getCountryCodeForRegion(String regionCode) {
        if (isValidRegionCode(regionCode)) {
            return getCountryCodeForValidRegion(regionCode);
        }
        Logger logger2 = logger;
        Level level = Level.WARNING;
        if (regionCode == null) {
            regionCode = "null";
        }
        String valueOf = String.valueOf(String.valueOf(regionCode));
        logger2.log(level, new StringBuilder(valueOf.length() + 43).append("Invalid or missing region code (").append(valueOf).append(") provided.").toString());
        return 0;
    }

    private int getCountryCodeForValidRegion(String regionCode) {
        Phonemetadata.PhoneMetadata metadata = getMetadataForRegion(regionCode);
        if (metadata != null) {
            return metadata.getCountryCode();
        }
        String valueOf = String.valueOf(regionCode);
        throw new IllegalArgumentException(valueOf.length() != 0 ? "Invalid region code: ".concat(valueOf) : new String("Invalid region code: "));
    }

    public String getNddPrefixForRegion(String regionCode, boolean stripNonDigits) {
        Phonemetadata.PhoneMetadata metadata = getMetadataForRegion(regionCode);
        if (metadata == null) {
            Logger logger2 = logger;
            Level level = Level.WARNING;
            if (regionCode == null) {
                regionCode = "null";
            }
            String valueOf = String.valueOf(String.valueOf(regionCode));
            logger2.log(level, new StringBuilder(valueOf.length() + 43).append("Invalid or missing region code (").append(valueOf).append(") provided.").toString());
            return null;
        }
        String nationalPrefix = metadata.getNationalPrefix();
        if (nationalPrefix.length() == 0) {
            return null;
        }
        if (stripNonDigits) {
            return nationalPrefix.replace("~", "");
        }
        return nationalPrefix;
    }

    public boolean isNANPACountry(String regionCode) {
        return this.nanpaRegions.contains(regionCode);
    }

    /* access modifiers changed from: package-private */
    public boolean isLeadingZeroPossible(int countryCallingCode) {
        Phonemetadata.PhoneMetadata mainMetadataForCallingCode = getMetadataForRegionOrCallingCode(countryCallingCode, getRegionCodeForCountryCode(countryCallingCode));
        if (mainMetadataForCallingCode == null) {
            return false;
        }
        return mainMetadataForCallingCode.isLeadingZeroPossible();
    }

    public boolean isAlphaNumber(String number) {
        if (!isViablePhoneNumber(number)) {
            return false;
        }
        StringBuilder strippedNumber = new StringBuilder(number);
        maybeStripExtension(strippedNumber);
        return VALID_ALPHA_PHONE_PATTERN.matcher(strippedNumber).matches();
    }

    public boolean isPossibleNumber(Phonenumber.PhoneNumber number) {
        return isPossibleNumberWithReason(number) == ValidationResult.IS_POSSIBLE;
    }

    private ValidationResult testNumberLengthAgainstPattern(Pattern numberPattern, String number) {
        Matcher numberMatcher = numberPattern.matcher(number);
        if (numberMatcher.matches()) {
            return ValidationResult.IS_POSSIBLE;
        }
        if (numberMatcher.lookingAt()) {
            return ValidationResult.TOO_LONG;
        }
        return ValidationResult.TOO_SHORT;
    }

    private boolean isShorterThanPossibleNormalNumber(Phonemetadata.PhoneMetadata regionMetadata, String number) {
        return testNumberLengthAgainstPattern(this.regexCache.getPatternForRegex(regionMetadata.getGeneralDesc().getPossibleNumberPattern()), number) == ValidationResult.TOO_SHORT;
    }

    public ValidationResult isPossibleNumberWithReason(Phonenumber.PhoneNumber number) {
        String nationalNumber = getNationalSignificantNumber(number);
        int countryCode = number.getCountryCode();
        if (!hasValidCountryCallingCode(countryCode)) {
            return ValidationResult.INVALID_COUNTRY_CODE;
        }
        Phonemetadata.PhoneNumberDesc generalNumDesc = getMetadataForRegionOrCallingCode(countryCode, getRegionCodeForCountryCode(countryCode)).getGeneralDesc();
        if (generalNumDesc.hasNationalNumberPattern()) {
            return testNumberLengthAgainstPattern(this.regexCache.getPatternForRegex(generalNumDesc.getPossibleNumberPattern()), nationalNumber);
        }
        logger.log(Level.FINER, "Checking if number is possible with incomplete metadata.");
        int numberLength = nationalNumber.length();
        if (numberLength < MIN_LENGTH_FOR_NSN) {
            return ValidationResult.TOO_SHORT;
        }
        if (numberLength > MAX_LENGTH_FOR_NSN) {
            return ValidationResult.TOO_LONG;
        }
        return ValidationResult.IS_POSSIBLE;
    }

    public boolean isPossibleNumber(String number, String regionDialingFrom) {
        try {
            return isPossibleNumber(parse(number, regionDialingFrom));
        } catch (NumberParseException e) {
            return false;
        }
    }

    public boolean truncateTooLongNumber(Phonenumber.PhoneNumber number) {
        if (isValidNumber(number)) {
            return true;
        }
        Phonenumber.PhoneNumber numberCopy = new Phonenumber.PhoneNumber();
        numberCopy.mergeFrom(number);
        long nationalNumber = number.getNationalNumber();
        do {
            nationalNumber /= 10;
            numberCopy.setNationalNumber(nationalNumber);
            if (isPossibleNumberWithReason(numberCopy) == ValidationResult.TOO_SHORT || nationalNumber == 0) {
                return false;
            }
        } while (!isValidNumber(numberCopy));
        number.setNationalNumber(nationalNumber);
        return true;
    }

    public AsYouTypeFormatter getAsYouTypeFormatter(String regionCode) {
        return new AsYouTypeFormatter(regionCode);
    }

    /* access modifiers changed from: package-private */
    public int extractCountryCode(StringBuilder fullNumber, StringBuilder nationalNumber) {
        if (fullNumber.length() == 0 || fullNumber.charAt(0) == '0') {
            return 0;
        }
        int numberLength = fullNumber.length();
        int i = NANPA_COUNTRY_CODE;
        while (i <= MAX_LENGTH_COUNTRY_CODE && i <= numberLength) {
            int potentialCountryCode = Integer.parseInt(fullNumber.substring(0, i));
            if (this.countryCallingCodeToRegionCodeMap.containsKey(Integer.valueOf(potentialCountryCode))) {
                nationalNumber.append(fullNumber.substring(i));
                return potentialCountryCode;
            }
            i += NANPA_COUNTRY_CODE;
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public int maybeExtractCountryCode(String number, Phonemetadata.PhoneMetadata defaultRegionMetadata, StringBuilder nationalNumber, boolean keepRawInput, Phonenumber.PhoneNumber phoneNumber) throws NumberParseException {
        if (number.length() == 0) {
            return 0;
        }
        StringBuilder fullNumber = new StringBuilder(number);
        String possibleCountryIddPrefix = "NonMatch";
        if (defaultRegionMetadata != null) {
            possibleCountryIddPrefix = defaultRegionMetadata.getInternationalPrefix();
        }
        Phonenumber.PhoneNumber.CountryCodeSource countryCodeSource = maybeStripInternationalPrefixAndNormalize(fullNumber, possibleCountryIddPrefix);
        if (keepRawInput) {
            phoneNumber.setCountryCodeSource(countryCodeSource);
        }
        if (countryCodeSource == Phonenumber.PhoneNumber.CountryCodeSource.FROM_DEFAULT_COUNTRY) {
            if (defaultRegionMetadata != null) {
                int defaultCountryCode = defaultRegionMetadata.getCountryCode();
                String defaultCountryCodeString = String.valueOf(defaultCountryCode);
                String normalizedNumber = fullNumber.toString();
                if (normalizedNumber.startsWith(defaultCountryCodeString)) {
                    StringBuilder potentialNationalNumber = new StringBuilder(normalizedNumber.substring(defaultCountryCodeString.length()));
                    Phonemetadata.PhoneNumberDesc generalDesc = defaultRegionMetadata.getGeneralDesc();
                    Pattern validNumberPattern = this.regexCache.getPatternForRegex(generalDesc.getNationalNumberPattern());
                    maybeStripNationalPrefixAndCarrierCode(potentialNationalNumber, defaultRegionMetadata, null);
                    Pattern possibleNumberPattern = this.regexCache.getPatternForRegex(generalDesc.getPossibleNumberPattern());
                    if ((!validNumberPattern.matcher(fullNumber).matches() && validNumberPattern.matcher(potentialNationalNumber).matches()) || testNumberLengthAgainstPattern(possibleNumberPattern, fullNumber.toString()) == ValidationResult.TOO_LONG) {
                        nationalNumber.append((CharSequence) potentialNationalNumber);
                        if (keepRawInput) {
                            phoneNumber.setCountryCodeSource(Phonenumber.PhoneNumber.CountryCodeSource.FROM_NUMBER_WITHOUT_PLUS_SIGN);
                        }
                        phoneNumber.setCountryCode(defaultCountryCode);
                        return defaultCountryCode;
                    }
                }
            }
            phoneNumber.setCountryCode(0);
            return 0;
        } else if (fullNumber.length() <= MIN_LENGTH_FOR_NSN) {
            throw new NumberParseException(NumberParseException.ErrorType.TOO_SHORT_AFTER_IDD, "Phone number had an IDD, but after this was not long enough to be a viable phone number.");
        } else {
            int potentialCountryCode = extractCountryCode(fullNumber, nationalNumber);
            if (potentialCountryCode != 0) {
                phoneNumber.setCountryCode(potentialCountryCode);
                return potentialCountryCode;
            }
            throw new NumberParseException(NumberParseException.ErrorType.INVALID_COUNTRY_CODE, "Country calling code supplied was not recognised.");
        }
    }

    private boolean parsePrefixAsIdd(Pattern iddPattern, StringBuilder number) {
        Matcher m = iddPattern.matcher(number);
        if (!m.lookingAt()) {
            return false;
        }
        int matchEnd = m.end();
        Matcher digitMatcher = CAPTURING_DIGIT_PATTERN.matcher(number.substring(matchEnd));
        if (digitMatcher.find() && normalizeDigitsOnly(digitMatcher.group((int) NANPA_COUNTRY_CODE)).equals("0")) {
            return false;
        }
        number.delete(0, matchEnd);
        return NANPA_COUNTRY_CODE;
    }

    /* access modifiers changed from: package-private */
    public Phonenumber.PhoneNumber.CountryCodeSource maybeStripInternationalPrefixAndNormalize(StringBuilder number, String possibleIddPrefix) {
        if (number.length() == 0) {
            return Phonenumber.PhoneNumber.CountryCodeSource.FROM_DEFAULT_COUNTRY;
        }
        Matcher m = PLUS_CHARS_PATTERN.matcher(number);
        if (m.lookingAt()) {
            number.delete(0, m.end());
            normalize(number);
            return Phonenumber.PhoneNumber.CountryCodeSource.FROM_NUMBER_WITH_PLUS_SIGN;
        }
        Pattern iddPattern = this.regexCache.getPatternForRegex(possibleIddPrefix);
        normalize(number);
        return parsePrefixAsIdd(iddPattern, number) ? Phonenumber.PhoneNumber.CountryCodeSource.FROM_NUMBER_WITH_IDD : Phonenumber.PhoneNumber.CountryCodeSource.FROM_DEFAULT_COUNTRY;
    }

    /* access modifiers changed from: package-private */
    public boolean maybeStripNationalPrefixAndCarrierCode(StringBuilder number, Phonemetadata.PhoneMetadata metadata, StringBuilder carrierCode) {
        int numberLength = number.length();
        String possibleNationalPrefix = metadata.getNationalPrefixForParsing();
        if (numberLength == 0 || possibleNationalPrefix.length() == 0) {
            return false;
        }
        Matcher prefixMatcher = this.regexCache.getPatternForRegex(possibleNationalPrefix).matcher(number);
        if (!prefixMatcher.lookingAt()) {
            return false;
        }
        Pattern nationalNumberRule = this.regexCache.getPatternForRegex(metadata.getGeneralDesc().getNationalNumberPattern());
        boolean isViableOriginalNumber = nationalNumberRule.matcher(number).matches();
        int numOfGroups = prefixMatcher.groupCount();
        String transformRule = metadata.getNationalPrefixTransformRule();
        if (transformRule != null && transformRule.length() != 0 && prefixMatcher.group(numOfGroups) != null) {
            StringBuilder transformedNumber = new StringBuilder(number);
            transformedNumber.replace(0, numberLength, prefixMatcher.replaceFirst(transformRule));
            if (isViableOriginalNumber && !nationalNumberRule.matcher(transformedNumber.toString()).matches()) {
                return false;
            }
            if (carrierCode != null && numOfGroups > NANPA_COUNTRY_CODE) {
                carrierCode.append(prefixMatcher.group((int) NANPA_COUNTRY_CODE));
            }
            number.replace(0, number.length(), transformedNumber.toString());
            return NANPA_COUNTRY_CODE;
        } else if (isViableOriginalNumber && !nationalNumberRule.matcher(number.substring(prefixMatcher.end())).matches()) {
            return false;
        } else {
            if (!(carrierCode == null || numOfGroups <= 0 || prefixMatcher.group(numOfGroups) == null)) {
                carrierCode.append(prefixMatcher.group((int) NANPA_COUNTRY_CODE));
            }
            number.delete(0, prefixMatcher.end());
            return NANPA_COUNTRY_CODE;
        }
    }

    /* access modifiers changed from: package-private */
    public String maybeStripExtension(StringBuilder number) {
        Matcher m = EXTN_PATTERN.matcher(number);
        if (m.find() && isViablePhoneNumber(number.substring(0, m.start()))) {
            int length = m.groupCount();
            for (int i = NANPA_COUNTRY_CODE; i <= length; i += NANPA_COUNTRY_CODE) {
                if (m.group(i) != null) {
                    String extension = m.group(i);
                    number.delete(m.start(), number.length());
                    return extension;
                }
            }
        }
        return "";
    }

    private boolean checkRegionForParsing(String numberToParse, String defaultRegion) {
        if (isValidRegionCode(defaultRegion) || (numberToParse != null && numberToParse.length() != 0 && PLUS_CHARS_PATTERN.matcher(numberToParse).lookingAt())) {
            return true;
        }
        return false;
    }

    public Phonenumber.PhoneNumber parse(String numberToParse, String defaultRegion) throws NumberParseException {
        Phonenumber.PhoneNumber phoneNumber = new Phonenumber.PhoneNumber();
        parse(numberToParse, defaultRegion, phoneNumber);
        return phoneNumber;
    }

    public void parse(String numberToParse, String defaultRegion, Phonenumber.PhoneNumber phoneNumber) throws NumberParseException {
        parseHelper(numberToParse, defaultRegion, false, true, phoneNumber);
    }

    public Phonenumber.PhoneNumber parseAndKeepRawInput(String numberToParse, String defaultRegion) throws NumberParseException {
        Phonenumber.PhoneNumber phoneNumber = new Phonenumber.PhoneNumber();
        parseAndKeepRawInput(numberToParse, defaultRegion, phoneNumber);
        return phoneNumber;
    }

    public void parseAndKeepRawInput(String numberToParse, String defaultRegion, Phonenumber.PhoneNumber phoneNumber) throws NumberParseException {
        parseHelper(numberToParse, defaultRegion, true, NANPA_COUNTRY_CODE, phoneNumber);
    }

    public Iterable<PhoneNumberMatch> findNumbers(CharSequence text, String defaultRegion) {
        return findNumbers(text, defaultRegion, Leniency.VALID, Long.MAX_VALUE);
    }

    public Iterable<PhoneNumberMatch> findNumbers(CharSequence text, String defaultRegion, Leniency leniency, long maxTries) {
        final CharSequence charSequence = text;
        final String str = defaultRegion;
        final Leniency leniency2 = leniency;
        final long j = maxTries;
        return new Iterable<PhoneNumberMatch>() {
            public Iterator<PhoneNumberMatch> iterator() {
                return new PhoneNumberMatcher(PhoneNumberUtil.this, charSequence, str, leniency2, j);
            }
        };
    }

    static void setItalianLeadingZerosForPhoneNumber(String nationalNumber, Phonenumber.PhoneNumber phoneNumber) {
        if (nationalNumber.length() > NANPA_COUNTRY_CODE && nationalNumber.charAt(0) == '0') {
            phoneNumber.setItalianLeadingZero(true);
            int numberOfLeadingZeros = NANPA_COUNTRY_CODE;
            while (numberOfLeadingZeros < nationalNumber.length() - 1 && nationalNumber.charAt(numberOfLeadingZeros) == '0') {
                numberOfLeadingZeros += NANPA_COUNTRY_CODE;
            }
            if (numberOfLeadingZeros != NANPA_COUNTRY_CODE) {
                phoneNumber.setNumberOfLeadingZeros(numberOfLeadingZeros);
            }
        }
    }

    private void parseHelper(String numberToParse, String defaultRegion, boolean keepRawInput, boolean checkRegion, Phonenumber.PhoneNumber phoneNumber) throws NumberParseException {
        int countryCode;
        if (numberToParse == null) {
            throw new NumberParseException(NumberParseException.ErrorType.NOT_A_NUMBER, "The phone number supplied was null.");
        } else if (numberToParse.length() > MAX_INPUT_STRING_LENGTH) {
            throw new NumberParseException(NumberParseException.ErrorType.TOO_LONG, "The string supplied was too long to parse.");
        } else {
            StringBuilder nationalNumber = new StringBuilder();
            buildNationalNumberForParsing(numberToParse, nationalNumber);
            if (!isViablePhoneNumber(nationalNumber.toString())) {
                throw new NumberParseException(NumberParseException.ErrorType.NOT_A_NUMBER, "The string supplied did not seem to be a phone number.");
            } else if (!checkRegion || checkRegionForParsing(nationalNumber.toString(), defaultRegion)) {
                if (keepRawInput) {
                    phoneNumber.setRawInput(numberToParse);
                }
                String extension = maybeStripExtension(nationalNumber);
                if (extension.length() > 0) {
                    phoneNumber.setExtension(extension);
                }
                Phonemetadata.PhoneMetadata regionMetadata = getMetadataForRegion(defaultRegion);
                StringBuilder normalizedNationalNumber = new StringBuilder();
                try {
                    countryCode = maybeExtractCountryCode(nationalNumber.toString(), regionMetadata, normalizedNationalNumber, keepRawInput, phoneNumber);
                } catch (NumberParseException e) {
                    Matcher matcher = PLUS_CHARS_PATTERN.matcher(nationalNumber.toString());
                    if (e.getErrorType() != NumberParseException.ErrorType.INVALID_COUNTRY_CODE || !matcher.lookingAt()) {
                        throw new NumberParseException(e.getErrorType(), e.getMessage());
                    }
                    countryCode = maybeExtractCountryCode(nationalNumber.substring(matcher.end()), regionMetadata, normalizedNationalNumber, keepRawInput, phoneNumber);
                    if (countryCode == 0) {
                        throw new NumberParseException(NumberParseException.ErrorType.INVALID_COUNTRY_CODE, "Could not interpret numbers after plus-sign.");
                    }
                }
                if (countryCode != 0) {
                    String phoneNumberRegion = getRegionCodeForCountryCode(countryCode);
                    if (!phoneNumberRegion.equals(defaultRegion)) {
                        regionMetadata = getMetadataForRegionOrCallingCode(countryCode, phoneNumberRegion);
                    }
                } else {
                    normalize(nationalNumber);
                    normalizedNationalNumber.append((CharSequence) nationalNumber);
                    if (defaultRegion != null) {
                        phoneNumber.setCountryCode(regionMetadata.getCountryCode());
                    } else if (keepRawInput) {
                        phoneNumber.clearCountryCodeSource();
                    }
                }
                if (normalizedNationalNumber.length() < MIN_LENGTH_FOR_NSN) {
                    throw new NumberParseException(NumberParseException.ErrorType.TOO_SHORT_NSN, "The string supplied is too short to be a phone number.");
                }
                if (regionMetadata != null) {
                    StringBuilder carrierCode = new StringBuilder();
                    StringBuilder sb = new StringBuilder(normalizedNationalNumber);
                    maybeStripNationalPrefixAndCarrierCode(sb, regionMetadata, carrierCode);
                    if (!isShorterThanPossibleNormalNumber(regionMetadata, sb.toString())) {
                        normalizedNationalNumber = sb;
                        if (keepRawInput) {
                            phoneNumber.setPreferredDomesticCarrierCode(carrierCode.toString());
                        }
                    }
                }
                int lengthOfNationalNumber = normalizedNationalNumber.length();
                if (lengthOfNationalNumber < MIN_LENGTH_FOR_NSN) {
                    throw new NumberParseException(NumberParseException.ErrorType.TOO_SHORT_NSN, "The string supplied is too short to be a phone number.");
                } else if (lengthOfNationalNumber > MAX_LENGTH_FOR_NSN) {
                    throw new NumberParseException(NumberParseException.ErrorType.TOO_LONG, "The string supplied is too long to be a phone number.");
                } else {
                    setItalianLeadingZerosForPhoneNumber(normalizedNationalNumber.toString(), phoneNumber);
                    phoneNumber.setNationalNumber(Long.parseLong(normalizedNationalNumber.toString()));
                }
            } else {
                throw new NumberParseException(NumberParseException.ErrorType.INVALID_COUNTRY_CODE, "Missing or invalid default region.");
            }
        }
    }

    private void buildNationalNumberForParsing(String numberToParse, StringBuilder nationalNumber) {
        int indexOfPhoneContext = numberToParse.indexOf(RFC3966_PHONE_CONTEXT);
        if (indexOfPhoneContext > 0) {
            int phoneContextStart = indexOfPhoneContext + RFC3966_PHONE_CONTEXT.length();
            if (numberToParse.charAt(phoneContextStart) == '+') {
                int phoneContextEnd = numberToParse.indexOf(59, phoneContextStart);
                if (phoneContextEnd > 0) {
                    nationalNumber.append(numberToParse.substring(phoneContextStart, phoneContextEnd));
                } else {
                    nationalNumber.append(numberToParse.substring(phoneContextStart));
                }
            }
            int indexOfRfc3966Prefix = numberToParse.indexOf(RFC3966_PREFIX);
            nationalNumber.append(numberToParse.substring(indexOfRfc3966Prefix >= 0 ? indexOfRfc3966Prefix + RFC3966_PREFIX.length() : 0, indexOfPhoneContext));
        } else {
            nationalNumber.append(extractPossibleNumber(numberToParse));
        }
        int indexOfIsdn = nationalNumber.indexOf(RFC3966_ISDN_SUBADDRESS);
        if (indexOfIsdn > 0) {
            nationalNumber.delete(indexOfIsdn, nationalNumber.length());
        }
    }

    public MatchType isNumberMatch(Phonenumber.PhoneNumber firstNumberIn, Phonenumber.PhoneNumber secondNumberIn) {
        Phonenumber.PhoneNumber firstNumber = new Phonenumber.PhoneNumber();
        firstNumber.mergeFrom(firstNumberIn);
        Phonenumber.PhoneNumber secondNumber = new Phonenumber.PhoneNumber();
        secondNumber.mergeFrom(secondNumberIn);
        firstNumber.clearRawInput();
        firstNumber.clearCountryCodeSource();
        firstNumber.clearPreferredDomesticCarrierCode();
        secondNumber.clearRawInput();
        secondNumber.clearCountryCodeSource();
        secondNumber.clearPreferredDomesticCarrierCode();
        if (firstNumber.hasExtension() && firstNumber.getExtension().length() == 0) {
            firstNumber.clearExtension();
        }
        if (secondNumber.hasExtension() && secondNumber.getExtension().length() == 0) {
            secondNumber.clearExtension();
        }
        if (firstNumber.hasExtension() && secondNumber.hasExtension() && !firstNumber.getExtension().equals(secondNumber.getExtension())) {
            return MatchType.NO_MATCH;
        }
        int firstNumberCountryCode = firstNumber.getCountryCode();
        int secondNumberCountryCode = secondNumber.getCountryCode();
        if (firstNumberCountryCode == 0 || secondNumberCountryCode == 0) {
            firstNumber.setCountryCode(secondNumberCountryCode);
            if (firstNumber.exactlySameAs(secondNumber)) {
                return MatchType.NSN_MATCH;
            }
            if (isNationalNumberSuffixOfTheOther(firstNumber, secondNumber)) {
                return MatchType.SHORT_NSN_MATCH;
            }
            return MatchType.NO_MATCH;
        } else if (firstNumber.exactlySameAs(secondNumber)) {
            return MatchType.EXACT_MATCH;
        } else {
            if (firstNumberCountryCode != secondNumberCountryCode || !isNationalNumberSuffixOfTheOther(firstNumber, secondNumber)) {
                return MatchType.NO_MATCH;
            }
            return MatchType.SHORT_NSN_MATCH;
        }
    }

    private boolean isNationalNumberSuffixOfTheOther(Phonenumber.PhoneNumber firstNumber, Phonenumber.PhoneNumber secondNumber) {
        String firstNumberNationalNumber = String.valueOf(firstNumber.getNationalNumber());
        String secondNumberNationalNumber = String.valueOf(secondNumber.getNationalNumber());
        return firstNumberNationalNumber.endsWith(secondNumberNationalNumber) || secondNumberNationalNumber.endsWith(firstNumberNationalNumber);
    }

    public MatchType isNumberMatch(String firstNumber, String secondNumber) {
        try {
            return isNumberMatch(parse(firstNumber, UNKNOWN_REGION), secondNumber);
        } catch (NumberParseException e) {
            if (e.getErrorType() == NumberParseException.ErrorType.INVALID_COUNTRY_CODE) {
                try {
                    return isNumberMatch(parse(secondNumber, UNKNOWN_REGION), firstNumber);
                } catch (NumberParseException e2) {
                    if (e2.getErrorType() == NumberParseException.ErrorType.INVALID_COUNTRY_CODE) {
                        try {
                            Phonenumber.PhoneNumber firstNumberProto = new Phonenumber.PhoneNumber();
                            Phonenumber.PhoneNumber secondNumberProto = new Phonenumber.PhoneNumber();
                            parseHelper(firstNumber, null, false, false, firstNumberProto);
                            parseHelper(secondNumber, null, false, false, secondNumberProto);
                            return isNumberMatch(firstNumberProto, secondNumberProto);
                        } catch (NumberParseException e3) {
                        }
                    }
                }
            }
            return MatchType.NOT_A_NUMBER;
        }
    }

    public MatchType isNumberMatch(Phonenumber.PhoneNumber firstNumber, String secondNumber) {
        try {
            return isNumberMatch(firstNumber, parse(secondNumber, UNKNOWN_REGION));
        } catch (NumberParseException e) {
            if (e.getErrorType() == NumberParseException.ErrorType.INVALID_COUNTRY_CODE) {
                String firstNumberRegion = getRegionCodeForCountryCode(firstNumber.getCountryCode());
                try {
                    if (!firstNumberRegion.equals(UNKNOWN_REGION)) {
                        MatchType match = isNumberMatch(firstNumber, parse(secondNumber, firstNumberRegion));
                        if (match == MatchType.EXACT_MATCH) {
                            return MatchType.NSN_MATCH;
                        }
                        return match;
                    }
                    Phonenumber.PhoneNumber secondNumberProto = new Phonenumber.PhoneNumber();
                    parseHelper(secondNumber, null, false, false, secondNumberProto);
                    return isNumberMatch(firstNumber, secondNumberProto);
                } catch (NumberParseException e2) {
                    return MatchType.NOT_A_NUMBER;
                }
            }
            return MatchType.NOT_A_NUMBER;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean canBeInternationallyDialled(Phonenumber.PhoneNumber number) {
        Phonemetadata.PhoneMetadata metadata = getMetadataForRegion(getRegionCodeForNumber(number));
        if (metadata != null && isNumberMatchingDesc(getNationalSignificantNumber(number), metadata.getNoInternationalDialling())) {
            return false;
        }
        return true;
    }

    public boolean isMobileNumberPortableRegion(String regionCode) {
        Phonemetadata.PhoneMetadata metadata = getMetadataForRegion(regionCode);
        if (metadata != null) {
            return metadata.isMobileNumberPortableRegion();
        }
        Logger logger2 = logger;
        Level level = Level.WARNING;
        String valueOf = String.valueOf(regionCode);
        logger2.log(level, valueOf.length() != 0 ? "Invalid or unknown region code provided: ".concat(valueOf) : new String("Invalid or unknown region code provided: "));
        return false;
    }
}
