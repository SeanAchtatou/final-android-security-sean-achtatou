package defpackage;

import java.io.File;

/* renamed from: b  reason: default package */
public class b {
    public static int a = 4096;

    public static void a(File file) {
        if (file.exists()) {
            if (file.isFile()) {
                file.delete();
            } else if (file.isDirectory()) {
                File[] listFiles = file.listFiles();
                for (File a2 : listFiles) {
                    a(a2);
                }
            }
            file.delete();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r1.closeEntry();
        r4.flush();
        r4.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0020, code lost:
        r3 = new java.io.File(r7);
        a(r3);
        r3.createNewFile();
        r4 = new java.io.FileOutputStream(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0030, code lost:
        r3 = r1.read(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0035, code lost:
        if (r3 == -1) goto L_0x0041;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0037, code lost:
        r4.write(r2, 0, r3);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.lang.String r6, java.lang.String r7) {
        /*
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ Exception -> 0x003c }
            r0.<init>(r6)     // Catch:{ Exception -> 0x003c }
            java.util.zip.ZipInputStream r1 = new java.util.zip.ZipInputStream     // Catch:{ Exception -> 0x003c }
            r1.<init>(r0)     // Catch:{ Exception -> 0x003c }
            r2 = 256(0x100, float:3.59E-43)
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x003c }
        L_0x000e:
            java.util.zip.ZipEntry r3 = r1.getNextEntry()     // Catch:{ Exception -> 0x003c }
            if (r3 == 0) goto L_0x004a
            java.lang.String r3 = r3.getName()     // Catch:{ Exception -> 0x003c }
            java.lang.String r4 = "AndroidManifest.xml"
            boolean r3 = r3.equals(r4)     // Catch:{ Exception -> 0x003c }
            if (r3 == 0) goto L_0x000e
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x003c }
            r3.<init>(r7)     // Catch:{ Exception -> 0x003c }
            a(r3)     // Catch:{ Exception -> 0x003c }
            r3.createNewFile()     // Catch:{ Exception -> 0x003c }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x003c }
            r4.<init>(r3)     // Catch:{ Exception -> 0x003c }
        L_0x0030:
            int r3 = r1.read(r2)     // Catch:{ Exception -> 0x003c }
            r5 = -1
            if (r3 == r5) goto L_0x0041
            r5 = 0
            r4.write(r2, r5, r3)     // Catch:{ Exception -> 0x003c }
            goto L_0x0030
        L_0x003c:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0040:
            return
        L_0x0041:
            r1.closeEntry()     // Catch:{ Exception -> 0x003c }
            r4.flush()     // Catch:{ Exception -> 0x003c }
            r4.close()     // Catch:{ Exception -> 0x003c }
        L_0x004a:
            r0.close()     // Catch:{ Exception -> 0x003c }
            r1.close()     // Catch:{ Exception -> 0x003c }
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.b.a(java.lang.String, java.lang.String):void");
    }
}
