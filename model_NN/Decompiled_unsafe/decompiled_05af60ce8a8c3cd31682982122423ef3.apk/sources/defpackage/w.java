package defpackage;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Message;

/* renamed from: w  reason: default package */
class w implements DialogInterface.OnClickListener {
    final /* synthetic */ aj a;

    w(aj ajVar) {
        this.a = ajVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        Message message = new Message();
        Bundle bundle = new Bundle();
        bundle.putString("filePath", this.a.a.p + this.a.a.o);
        message.setData(bundle);
        message.what = 109;
        new e(this.a.a, this.a.a.r, message, "正在执行扫描...").start();
    }
}
