package defpackage;

import android.content.res.XmlResourceParser;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import org.xmlpull.v1.XmlPullParserException;

/* renamed from: bh  reason: default package */
public class bh implements XmlResourceParser {
    private bg a;
    private boolean b = false;
    private aw c;
    private int[] d;
    private n e = new n();
    private boolean f;
    private int g;
    private int h;
    private int i;
    private int j;
    private int[] k;
    private int l;
    private int m;
    private int n;

    public bh() {
        a();
    }

    private final int a(String str, String str2) {
        if (this.c == null || str2 == null) {
            return -1;
        }
        int a2 = this.c.a(str2);
        if (a2 == -1) {
            return -1;
        }
        int a3 = str != null ? this.c.a(str) : -1;
        for (int i2 = 0; i2 != this.k.length; i2++) {
            if (a2 == this.k[i2 + 1] && (a3 == -1 || a3 == this.k[i2 + 0])) {
                return i2 / 5;
            }
        }
        return -1;
    }

    private final void a() {
        this.g = -1;
        this.h = -1;
        this.i = -1;
        this.j = -1;
        this.k = null;
        this.l = -1;
        this.m = -1;
        this.n = -1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00c1, code lost:
        throw new java.io.IOException("Invalid chunk type (" + r1 + ").");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void b() {
        /*
            r9 = this;
            r8 = 0
            r7 = 1048834(0x100102, float:1.46973E-39)
            r6 = 1048832(0x100100, float:1.469727E-39)
            r5 = 3
            r4 = 1
            aw r0 = r9.c
            if (r0 != 0) goto L_0x0029
            bg r0 = r9.a
            r1 = 524291(0x80003, float:7.34688E-40)
            defpackage.bf.a(r0, r1)
            bg r0 = r9.a
            r0.c()
            bg r0 = r9.a
            aw r0 = defpackage.aw.a(r0)
            r9.c = r0
            n r0 = r9.e
            r0.e()
            r9.b = r4
        L_0x0029:
            int r0 = r9.g
            if (r0 != r4) goto L_0x002e
        L_0x002d:
            return
        L_0x002e:
            int r0 = r9.g
            r9.a()
        L_0x0033:
            boolean r1 = r9.f
            if (r1 == 0) goto L_0x003e
            r9.f = r8
            n r1 = r9.e
            r1.f()
        L_0x003e:
            if (r0 != r5) goto L_0x0053
            n r1 = r9.e
            int r1 = r1.d()
            if (r1 != r4) goto L_0x0053
            n r1 = r9.e
            int r1 = r1.b()
            if (r1 != 0) goto L_0x0053
            r9.g = r4
            goto L_0x002d
        L_0x0053:
            if (r0 != 0) goto L_0x0088
            r1 = r7
        L_0x0056:
            r2 = 524672(0x80180, float:7.35222E-40)
            if (r1 != r2) goto L_0x009c
            bg r1 = r9.a
            int r1 = r1.b()
            r2 = 8
            if (r1 < r2) goto L_0x0069
            int r2 = r1 % 4
            if (r2 == 0) goto L_0x008f
        L_0x0069:
            java.io.IOException r0 = new java.io.IOException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Invalid resource ids size ("
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = ")."
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x0088:
            bg r1 = r9.a
            int r1 = r1.b()
            goto L_0x0056
        L_0x008f:
            bg r2 = r9.a
            int r1 = r1 / 4
            r3 = 2
            int r1 = r1 - r3
            int[] r1 = r2.b(r1)
            r9.d = r1
            goto L_0x0033
        L_0x009c:
            if (r1 < r6) goto L_0x00a3
            r2 = 1048836(0x100104, float:1.469732E-39)
            if (r1 <= r2) goto L_0x00c2
        L_0x00a3:
            java.io.IOException r0 = new java.io.IOException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Invalid chunk type ("
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r2 = ")."
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x00c2:
            if (r1 != r7) goto L_0x00cb
            r2 = -1
            if (r0 != r2) goto L_0x00cb
            r9.g = r8
            goto L_0x002d
        L_0x00cb:
            bg r2 = r9.a
            r2.c()
            bg r2 = r9.a
            int r2 = r2.b()
            bg r3 = r9.a
            r3.c()
            if (r1 == r6) goto L_0x00e2
            r3 = 1048833(0x100101, float:1.469728E-39)
            if (r1 != r3) goto L_0x0108
        L_0x00e2:
            if (r1 != r6) goto L_0x00f7
            bg r1 = r9.a
            int r1 = r1.b()
            bg r2 = r9.a
            int r2 = r2.b()
            n r3 = r9.e
            r3.a(r1, r2)
            goto L_0x0033
        L_0x00f7:
            bg r1 = r9.a
            r1.c()
            bg r1 = r9.a
            r1.c()
            n r1 = r9.e
            r1.c()
            goto L_0x0033
        L_0x0108:
            r9.h = r2
            if (r1 != r7) goto L_0x016f
            bg r0 = r9.a
            int r0 = r0.b()
            r9.j = r0
            bg r0 = r9.a
            int r0 = r0.b()
            r9.i = r0
            bg r0 = r9.a
            r0.c()
            bg r0 = r9.a
            int r0 = r0.b()
            int r1 = r0 >>> 16
            int r1 = r1 - r4
            r9.l = r1
            r1 = 65535(0xffff, float:9.1834E-41)
            r0 = r0 & r1
            bg r1 = r9.a
            int r1 = r1.b()
            r9.m = r1
            int r1 = r9.m
            int r1 = r1 >>> 16
            int r1 = r1 - r4
            r9.n = r1
            int r1 = r9.m
            r2 = 65535(0xffff, float:9.1834E-41)
            r1 = r1 & r2
            int r1 = r1 - r4
            r9.m = r1
            bg r1 = r9.a
            int r0 = r0 * 5
            int[] r0 = r1.b(r0)
            r9.k = r0
            r0 = r5
        L_0x0153:
            int[] r1 = r9.k
            int r1 = r1.length
            if (r0 >= r1) goto L_0x0165
            int[] r1 = r9.k
            int[] r2 = r9.k
            r2 = r2[r0]
            int r2 = r2 >>> 24
            r1[r0] = r2
            int r0 = r0 + 5
            goto L_0x0153
        L_0x0165:
            n r0 = r9.e
            r0.e()
            r0 = 2
            r9.g = r0
            goto L_0x002d
        L_0x016f:
            r2 = 1048835(0x100103, float:1.469731E-39)
            if (r1 != r2) goto L_0x018a
            bg r0 = r9.a
            int r0 = r0.b()
            r9.j = r0
            bg r0 = r9.a
            int r0 = r0.b()
            r9.i = r0
            r9.g = r5
            r9.f = r4
            goto L_0x002d
        L_0x018a:
            r2 = 1048836(0x100104, float:1.469732E-39)
            if (r1 != r2) goto L_0x0033
            bg r0 = r9.a
            int r0 = r0.b()
            r9.i = r0
            bg r0 = r9.a
            r0.c()
            bg r0 = r9.a
            r0.c()
            r0 = 4
            r9.g = r0
            goto L_0x002d
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.bh.b():void");
    }

    private final int c(int i2) {
        if (this.g != 2) {
            throw new IndexOutOfBoundsException("Current event is not START_TAG.");
        }
        int i3 = i2 * 5;
        if (i3 < this.k.length) {
            return i3;
        }
        throw new IndexOutOfBoundsException("Invalid attribute index (" + i2 + ").");
    }

    public int a(int i2) {
        return this.k[c(i2) + 3];
    }

    public void a(InputStream inputStream) {
        close();
        if (inputStream != null) {
            this.a = new bg(inputStream, false);
        }
    }

    public int b(int i2) {
        return this.k[c(i2) + 4];
    }

    public void close() {
        if (this.b) {
            this.b = false;
            this.a.a();
            this.a = null;
            this.c = null;
            this.d = null;
            this.e.a();
            a();
        }
    }

    public void defineEntityReplacementText(String str, String str2) {
        throw new XmlPullParserException("Method is not supported.");
    }

    public boolean getAttributeBooleanValue(int i2, boolean z) {
        return getAttributeIntValue(i2, z ? 1 : 0) != 0;
    }

    public boolean getAttributeBooleanValue(String str, String str2, boolean z) {
        int a2 = a(str, str2);
        return a2 == -1 ? z : getAttributeBooleanValue(a2, z);
    }

    public int getAttributeCount() {
        if (this.g != 2) {
            return -1;
        }
        return this.k.length / 5;
    }

    public float getAttributeFloatValue(int i2, float f2) {
        int c2 = c(i2);
        return this.k[c2 + 3] == 4 ? Float.intBitsToFloat(this.k[c2 + 4]) : f2;
    }

    public float getAttributeFloatValue(String str, String str2, float f2) {
        int a2 = a(str, str2);
        return a2 == -1 ? f2 : getAttributeFloatValue(a2, f2);
    }

    public int getAttributeIntValue(int i2, int i3) {
        int c2 = c(i2);
        int i4 = this.k[c2 + 3];
        return (i4 < 16 || i4 > 31) ? i3 : this.k[c2 + 4];
    }

    public int getAttributeIntValue(String str, String str2, int i2) {
        int a2 = a(str, str2);
        return a2 == -1 ? i2 : getAttributeIntValue(a2, i2);
    }

    public int getAttributeListValue(int i2, String[] strArr, int i3) {
        return 0;
    }

    public int getAttributeListValue(String str, String str2, String[] strArr, int i2) {
        return 0;
    }

    public String getAttributeName(int i2) {
        int i3 = this.k[c(i2) + 1];
        return i3 == -1 ? "" : this.c.a(i3);
    }

    public int getAttributeNameResource(int i2) {
        int i3 = this.k[c(i2) + 1];
        if (this.d == null || i3 < 0 || i3 >= this.d.length) {
            return 0;
        }
        return this.d[i3];
    }

    public String getAttributeNamespace(int i2) {
        int i3 = this.k[c(i2) + 0];
        return i3 == -1 ? "" : this.c.a(i3);
    }

    public String getAttributePrefix(int i2) {
        int d2 = this.e.d(this.k[c(i2) + 0]);
        return d2 == -1 ? "" : this.c.a(d2);
    }

    public int getAttributeResourceValue(int i2, int i3) {
        int c2 = c(i2);
        return this.k[c2 + 3] == 1 ? this.k[c2 + 4] : i3;
    }

    public int getAttributeResourceValue(String str, String str2, int i2) {
        int a2 = a(str, str2);
        return a2 == -1 ? i2 : getAttributeResourceValue(a2, i2);
    }

    public String getAttributeType(int i2) {
        return "CDATA";
    }

    public int getAttributeUnsignedIntValue(int i2, int i3) {
        return getAttributeIntValue(i2, i3);
    }

    public int getAttributeUnsignedIntValue(String str, String str2, int i2) {
        int a2 = a(str, str2);
        return a2 == -1 ? i2 : getAttributeUnsignedIntValue(a2, i2);
    }

    public String getAttributeValue(int i2) {
        int c2 = c(i2);
        if (this.k[c2 + 3] == 3) {
            return this.c.a(this.k[c2 + 2]);
        }
        int i3 = this.k[c2 + 4];
        return "";
    }

    public String getAttributeValue(String str, String str2) {
        int a2 = a(str, str2);
        if (a2 == -1) {
            return null;
        }
        return getAttributeValue(a2);
    }

    public String getClassAttribute() {
        if (this.m == -1) {
            return null;
        }
        return this.c.a(this.k[c(this.m) + 2]);
    }

    public int getColumnNumber() {
        return -1;
    }

    public int getDepth() {
        return this.e.d() - 1;
    }

    public int getEventType() {
        return this.g;
    }

    public boolean getFeature(String str) {
        return false;
    }

    public String getIdAttribute() {
        if (this.l == -1) {
            return null;
        }
        return this.c.a(this.k[c(this.l) + 2]);
    }

    public int getIdAttributeResourceValue(int i2) {
        if (this.l == -1) {
            return i2;
        }
        int c2 = c(this.l);
        return this.k[c2 + 3] != 1 ? i2 : this.k[c2 + 4];
    }

    public String getInputEncoding() {
        return null;
    }

    public int getLineNumber() {
        return this.h;
    }

    public String getName() {
        if (this.i == -1 || (this.g != 2 && this.g != 3)) {
            return null;
        }
        return this.c.a(this.i);
    }

    public String getNamespace() {
        return this.c.a(this.j);
    }

    public String getNamespace(String str) {
        throw new RuntimeException("Method is not supported.");
    }

    public int getNamespaceCount(int i2) {
        return this.e.a(i2);
    }

    public String getNamespacePrefix(int i2) {
        return this.c.a(this.e.b(i2));
    }

    public String getNamespaceUri(int i2) {
        return this.c.a(this.e.c(i2));
    }

    public String getPositionDescription() {
        return "XML line #" + getLineNumber();
    }

    public String getPrefix() {
        return this.c.a(this.e.d(this.j));
    }

    public Object getProperty(String str) {
        return null;
    }

    public int getStyleAttribute() {
        if (this.n == -1) {
            return 0;
        }
        return this.k[c(this.n) + 4];
    }

    public String getText() {
        if (this.i == -1 || this.g != 4) {
            return null;
        }
        return this.c.a(this.i);
    }

    public char[] getTextCharacters(int[] iArr) {
        String text = getText();
        if (text == null) {
            return null;
        }
        iArr[0] = 0;
        iArr[1] = text.length();
        char[] cArr = new char[text.length()];
        text.getChars(0, text.length(), cArr, 0);
        return cArr;
    }

    public boolean isAttributeDefault(int i2) {
        return false;
    }

    public boolean isEmptyElementTag() {
        return false;
    }

    public boolean isWhitespace() {
        return false;
    }

    public int next() {
        if (this.a == null) {
            throw new XmlPullParserException("Parser is not opened.", this, null);
        }
        try {
            b();
            return this.g;
        } catch (IOException e2) {
            close();
            throw e2;
        }
    }

    public int nextTag() {
        int next = next();
        if (next == 4 && isWhitespace()) {
            next = next();
        }
        if (next == 2 || next == 3) {
            return next;
        }
        throw new XmlPullParserException("Expected start or end tag.", this, null);
    }

    public String nextText() {
        if (getEventType() != 2) {
            throw new XmlPullParserException("Parser must be on START_TAG to read next text.", this, null);
        }
        int next = next();
        if (next == 4) {
            String text = getText();
            if (next() == 3) {
                return text;
            }
            throw new XmlPullParserException("Event TEXT must be immediately followed by END_TAG.", this, null);
        } else if (next == 3) {
            return "";
        } else {
            throw new XmlPullParserException("Parser must be on START_TAG or TEXT to read text.", this, null);
        }
    }

    public int nextToken() {
        return next();
    }

    public void require(int i2, String str, String str2) {
        if (i2 != getEventType() || ((str != null && !str.equals(getNamespace())) || (str2 != null && !str2.equals(getName())))) {
            throw new XmlPullParserException(TYPES[i2] + " is expected.", this, null);
        }
    }

    public void setFeature(String str, boolean z) {
        throw new XmlPullParserException("Method is not supported.");
    }

    public void setInput(InputStream inputStream, String str) {
        throw new XmlPullParserException("Method is not supported.");
    }

    public void setInput(Reader reader) {
        throw new XmlPullParserException("Method is not supported.");
    }

    public void setProperty(String str, Object obj) {
        throw new XmlPullParserException("Method is not supported.");
    }
}
