package defpackage;

import android.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import com.ninepvi.shield.MainActivity;
import com.ninepvi.shield.R;
import java.io.File;

/* renamed from: aj  reason: default package */
public class aj implements AdapterView.OnItemClickListener {
    final /* synthetic */ MainActivity a;

    public aj(MainActivity mainActivity) {
        this.a = mainActivity;
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        File file = new File((String) this.a.n.get(i));
        if (file.isDirectory()) {
            this.a.e((String) this.a.n.get(i));
            return;
        }
        this.a.o = file.getName().toUpperCase();
        if (this.a.o.indexOf(".APK") == -1 || this.a.o.substring(this.a.o.indexOf(".APK")).length() != 4) {
            new AlertDialog.Builder(this.a).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.s_title_prompt).setMessage("请选择APK格式的文件").setPositiveButton("确定", new y(this)).show();
        } else {
            new AlertDialog.Builder(this.a).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.s_title_prompt).setMessage("确定扫描[" + this.a.o + "]文件吗？").setPositiveButton("确定", new w(this)).setNegativeButton("取消", new ac(this)).show();
        }
    }
}
