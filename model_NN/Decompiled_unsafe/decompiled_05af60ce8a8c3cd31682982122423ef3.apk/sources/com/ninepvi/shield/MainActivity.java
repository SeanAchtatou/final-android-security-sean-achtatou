package com.ninepvi.shield;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MainActivity extends Activity {
    public static String a = "tmp.xml";
    public static int b = 0;
    public static int c = 0;
    public static ResolveInfo g;
    public static List h = null;
    public static String i = "";
    public ProgressDialog d;
    public List e = null;
    public List f = null;
    public String j = "";
    public List k;
    public List l;
    public List m = null;
    public List n = null;
    public String o = "";
    public String p;
    public l q;
    public Handler r = new bi(this);
    /* access modifiers changed from: private */
    public LinearLayout s;
    /* access modifiers changed from: private */
    public LinearLayout t;
    /* access modifiers changed from: private */
    public LinearLayout u;
    /* access modifiers changed from: private */
    public LinearLayout v;
    /* access modifiers changed from: private */
    public Intent w;
    /* access modifiers changed from: private */
    public String x = "/";

    /* access modifiers changed from: private */
    public void d(String str) {
        this.p = str;
        List b2 = b(str);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("<font color='#7ADD00'>文件名称</font> " + this.o + "<br>");
        stringBuffer.append("<font color='#7ADD00'>文件路径  </font>" + str + "<br>");
        stringBuffer.append("<br>");
        if (!(b2 == null || h == null)) {
            for (i iVar : h) {
                if (b2.indexOf(iVar.a()) != -1) {
                    stringBuffer.append("<font color='#7ADD00'>权限名称 </font> " + iVar.a() + "<br>");
                    stringBuffer.append("<font color='#7ADD00'>权限描述  </font>" + iVar.b() + "<br>");
                    stringBuffer.append("<br>");
                }
            }
        }
        a(stringBuffer.toString());
        i = stringBuffer.toString();
        a(7);
    }

    /* access modifiers changed from: private */
    public void e(String str) {
        this.p = str;
        this.x = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator;
        this.m = new ArrayList();
        this.n = new ArrayList();
        File file = new File(str);
        File[] listFiles = file.listFiles();
        if (this.x.indexOf(str) == -1) {
            this.m.add("Back to " + this.x);
            this.n.add(this.x);
            this.m.add("Back to ../");
            this.n.add(file.getParent());
        }
        for (File file2 : listFiles) {
            this.m.add(file2.getName());
            this.n.add(file2.getPath());
        }
        ListView listView = (ListView) findViewById(R.id.lv_content);
        listView.setAdapter((ListAdapter) new ArrayAdapter(this, (int) R.layout.file_row, this.m));
        listView.setOnItemClickListener(new aj(this));
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0091  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0111  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void g() {
        /*
            r13 = this;
            r10 = 1
            r9 = 0
            r8 = -1
            java.lang.String r12 = "android.permission.RECEIVE_SMS"
            java.lang.String r11 = "android.permission.INTERNET"
            com.ninepvi.shield.MainActivity.b = r9
            com.ninepvi.shield.MainActivity.c = r9
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r13.f = r1
            l r1 = r13.q
            java.util.List r3 = r1.a()
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r13.k = r1
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r13.l = r1
            r4 = r9
        L_0x0027:
            java.util.List r1 = r13.e
            int r1 = r1.size()
            if (r4 >= r1) goto L_0x011c
            int r1 = com.ninepvi.shield.MainActivity.c
            int r1 = r1 + 1
            com.ninepvi.shield.MainActivity.c = r1
            android.os.Handler r1 = r13.r
            r2 = 5
            r1.sendEmptyMessage(r2)
            java.util.List r1 = r13.e
            java.lang.Object r1 = r1.get(r4)
            r0 = r1
            android.content.pm.ResolveInfo r0 = (android.content.pm.ResolveInfo) r0
            r2 = r0
            android.content.pm.ActivityInfo r1 = r2.activityInfo
            java.lang.String r1 = r1.packageName
            java.lang.String r1 = r1.trim()
            java.lang.String r5 = "com.android"
            boolean r1 = r1.contains(r5)
            if (r1 != 0) goto L_0x0065
            android.content.pm.ActivityInfo r1 = r2.activityInfo
            java.lang.String r1 = r1.packageName
            java.lang.String r1 = r1.trim()
            java.lang.String r5 = ".shield"
            boolean r1 = r1.contains(r5)
            if (r1 == 0) goto L_0x0153
        L_0x0065:
            r5 = r9
        L_0x0066:
            if (r5 == 0) goto L_0x0150
            if (r3 == 0) goto L_0x0150
            int r1 = r3.size()
            if (r1 <= 0) goto L_0x0150
            java.util.Iterator r6 = r3.iterator()
        L_0x0074:
            boolean r1 = r6.hasNext()
            if (r1 == 0) goto L_0x0150
            java.lang.Object r1 = r6.next()
            z r1 = (defpackage.z) r1
            java.lang.String r1 = r1.b()
            android.content.pm.ActivityInfo r7 = r2.activityInfo
            java.lang.String r7 = r7.packageName
            boolean r1 = r1.equalsIgnoreCase(r7)
            if (r1 == 0) goto L_0x0074
            r1 = r9
        L_0x008f:
            if (r1 == 0) goto L_0x0111
            android.content.pm.ActivityInfo r1 = r2.activityInfo
            android.content.pm.ApplicationInfo r1 = r1.applicationInfo
            java.lang.String r1 = r1.sourceDir
            java.util.List r1 = r13.b(r1)
            java.lang.String r5 = "android.permission.SEND_SMS"
            int r5 = r1.indexOf(r5)
            if (r5 != r8) goto L_0x00b3
            java.lang.String r5 = "android.permission.RECEIVE_SMS"
            int r5 = r1.indexOf(r12)
            if (r5 != r8) goto L_0x00b3
            java.lang.String r5 = "android.permission.INTERNET"
            int r5 = r1.indexOf(r11)
            if (r5 == r8) goto L_0x010c
        L_0x00b3:
            int r5 = com.ninepvi.shield.MainActivity.b
            int r5 = r5 + 1
            com.ninepvi.shield.MainActivity.b = r5
            java.util.List r5 = r13.f
            r5.add(r2)
            java.util.HashMap r5 = new java.util.HashMap
            r5.<init>()
            java.lang.String r6 = "name"
            android.content.pm.PackageManager r7 = r13.getPackageManager()
            java.lang.CharSequence r7 = r2.loadLabel(r7)
            java.lang.String r7 = r7.toString()
            r5.put(r6, r7)
            java.lang.String r6 = "packageName"
            android.content.pm.ActivityInfo r7 = r2.activityInfo
            java.lang.String r7 = r7.packageName
            r5.put(r6, r7)
            java.lang.String r6 = "img"
            android.content.pm.PackageManager r7 = r13.getPackageManager()
            android.graphics.drawable.Drawable r2 = r2.loadIcon(r7)
            r5.put(r6, r2)
            java.lang.String r2 = "android.permission.SEND_SMS"
            int r2 = r1.indexOf(r2)
            if (r2 != r8) goto L_0x00fa
            java.lang.String r2 = "android.permission.RECEIVE_SMS"
            int r2 = r1.indexOf(r12)
            if (r2 == r8) goto L_0x00ff
        L_0x00fa:
            java.util.List r2 = r13.k
            r2.add(r5)
        L_0x00ff:
            java.lang.String r2 = "android.permission.INTERNET"
            int r1 = r1.indexOf(r11)
            if (r1 == r8) goto L_0x010c
            java.util.List r1 = r13.l
            r1.add(r5)
        L_0x010c:
            int r1 = r4 + 1
            r4 = r1
            goto L_0x0027
        L_0x0111:
            r1 = 100
            java.lang.Thread.sleep(r1)     // Catch:{ InterruptedException -> 0x0117 }
            goto L_0x010c
        L_0x0117:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x010c
        L_0x011c:
            int r1 = com.ninepvi.shield.MainActivity.b
            if (r1 != 0) goto L_0x0131
            r1 = 2130968599(0x7f040017, float:1.7545856E38)
            java.lang.String r1 = r13.getString(r1)
            com.ninepvi.shield.MainActivity.i = r1
            android.os.Handler r1 = r13.r
            r2 = 102(0x66, float:1.43E-43)
            r1.sendEmptyMessage(r2)
        L_0x0130:
            return
        L_0x0131:
            r1 = 2130968600(0x7f040018, float:1.7545858E38)
            java.lang.String r1 = r13.getString(r1)
            java.lang.Object[] r2 = new java.lang.Object[r10]
            int r3 = com.ninepvi.shield.MainActivity.b
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r2[r9] = r3
            java.lang.String r1 = java.lang.String.format(r1, r2)
            com.ninepvi.shield.MainActivity.i = r1
            android.os.Handler r1 = r13.r
            r2 = 103(0x67, float:1.44E-43)
            r1.sendEmptyMessage(r2)
            goto L_0x0130
        L_0x0150:
            r1 = r5
            goto L_0x008f
        L_0x0153:
            r5 = r10
            goto L_0x0066
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ninepvi.shield.MainActivity.g():void");
    }

    /* access modifiers changed from: private */
    public void h() {
        if (this.e == null) {
            this.w = new Intent("android.intent.action.MAIN", (Uri) null);
            this.w.addCategory("android.intent.category.LAUNCHER");
            this.e = getPackageManager().queryIntentActivities(this.w, 0);
        }
        Button button = (Button) findViewById(R.id.bt_content_top);
        button.setText("扫描已安装程序");
        button.setOnClickListener(new ag(this));
        ArrayList arrayList = new ArrayList();
        for (ResolveInfo resolveInfo : this.e) {
            HashMap hashMap = new HashMap();
            hashMap.put("name", resolveInfo.loadLabel(getPackageManager()).toString());
            hashMap.put("packageName", resolveInfo.activityInfo.packageName);
            hashMap.put("img", resolveInfo.loadIcon(getPackageManager()));
            arrayList.add(hashMap);
        }
        ListView listView = (ListView) findViewById(R.id.lv_content);
        listView.setAdapter((ListAdapter) new av(this, arrayList, R.layout.list_content_main, new String[]{"name", "packageName", "img"}, new int[]{R.id.activityInfo_name, R.id.activityInfo_packageName, R.id.activityInfo_img}));
        listView.setOnItemClickListener(new ba(this));
        try {
            if (h == null || h.size() <= 0) {
                h = r.a(getResources().getAssets().open("permission.xml"));
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        setTitle("共安装" + this.e.size() + "个程序");
    }

    private void i() {
        this.s = (LinearLayout) findViewById(R.id.lin_install);
        this.s.setOnClickListener(new az(this));
        this.s.setBackgroundResource(R.drawable.tab_install_app_sel);
        this.t = (LinearLayout) findViewById(R.id.lin_appoint);
        this.t.setOnClickListener(new bd(this));
        this.u = (LinearLayout) findViewById(R.id.lin_trust);
        this.u.setOnClickListener(new bc(this));
        this.v = (LinearLayout) findViewById(R.id.lin_more);
        this.v.setOnClickListener(new ax(this));
    }

    public String a() {
        return this.j;
    }

    public void a(int i2) {
        switch (i2) {
            case 1:
                new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.s_title_prompt).setMessage(i).setPositiveButton("确定", new ao(this)).show();
                return;
            case 2:
                new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.s_title_warn).setMessage(i).setPositiveButton("查看详情", new aq(this)).setNegativeButton("取消", new ap(this)).show();
                return;
            case 3:
                f();
                TextView textView = new TextView(this);
                ScrollView scrollView = new ScrollView(this);
                textView.setText(Html.fromHtml(a()));
                AlertDialog.Builder positiveButton = new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.s_title_permission).setNegativeButton("关闭", new ab(this)).setNeutralButton("信任", new as(this)).setPositiveButton("卸载", new ar(this));
                scrollView.addView(textView);
                positiveButton.setView(scrollView);
                positiveButton.show();
                return;
            case 4:
                ScrollView scrollView2 = new ScrollView(this);
                TextView textView2 = new TextView(this);
                textView2.setText((int) R.string.s_about);
                AlertDialog.Builder positiveButton2 = new AlertDialog.Builder(this).setTitle("关于我们").setPositiveButton("确定", new aa(this));
                textView2.setClickable(true);
                textView2.setFocusable(true);
                scrollView2.addView(textView2);
                positiveButton2.setView(scrollView2);
                positiveButton2.show();
                return;
            case 5:
                new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.s_title_prompt).setMessage("确认退出系统？").setPositiveButton("退出", new v(this)).setNegativeButton("取消", new x(this)).show();
                return;
            case 6:
                ScrollView scrollView3 = new ScrollView(this);
                TextView textView3 = new TextView(this);
                textView3.setText((int) R.string.s_help);
                AlertDialog.Builder positiveButton3 = new AlertDialog.Builder(this).setTitle("帮助").setPositiveButton("确定", new u(this));
                textView3.setClickable(true);
                textView3.setFocusable(true);
                scrollView3.addView(textView3);
                positiveButton3.setView(scrollView3);
                positiveButton3.show();
                return;
            case 7:
                TextView textView4 = new TextView(this);
                ScrollView scrollView4 = new ScrollView(this);
                textView4.setText(Html.fromHtml(a()));
                AlertDialog.Builder positiveButton4 = new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.s_title_permission).setNegativeButton("关闭", new s(this)).setPositiveButton("删除", new t(this));
                scrollView4.addView(textView4);
                positiveButton4.setView(scrollView4);
                positiveButton4.show();
                return;
            default:
                return;
        }
    }

    public void a(Message message) {
        ArrayList<CharSequence> charSequenceArrayList = message.getData().getCharSequenceArrayList("trustPackageNames");
        if (charSequenceArrayList != null && charSequenceArrayList.size() > 0) {
            Iterator<CharSequence> it = charSequenceArrayList.iterator();
            while (it.hasNext()) {
                String obj = it.next().toString();
                String substring = obj.substring(0, obj.indexOf("|"));
                String substring2 = obj.substring(obj.indexOf("|") + 1);
                this.q.a(substring, substring2, "");
                if (this.f != null && this.f.size() > 0) {
                    Iterator it2 = this.f.iterator();
                    while (true) {
                        if (it2.hasNext()) {
                            if (((ResolveInfo) it2.next()).activityInfo.applicationInfo.packageName.equals(substring2)) {
                                it2.remove();
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }
                if (this.k != null && this.k.size() > 0) {
                    Iterator it3 = this.k.iterator();
                    while (true) {
                        if (it3.hasNext()) {
                            if (((Map) it3.next()).get("packageName").toString().trim().equals(substring2)) {
                                it3.remove();
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }
                if (this.l != null && this.l.size() > 0) {
                    Iterator it4 = this.l.iterator();
                    while (true) {
                        if (it4.hasNext()) {
                            if (((Map) it4.next()).get("packageName").toString().trim().equals(substring2)) {
                                it4.remove();
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }
            }
        }
    }

    public void a(String str) {
        this.j = str;
    }

    public List b(String str) {
        String str2 = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + a;
        b.a(str, str2);
        bb.a(str2);
        return bb.a;
    }

    public void b() {
        Button button = (Button) findViewById(R.id.bt_content_top);
        button.setText("信任已勾选程序");
        button.setOnClickListener(new al(this));
        this.s.setBackgroundResource(R.drawable.tab_install_app_nor);
        this.t.setBackgroundResource(R.drawable.tab_assign_app_nor);
        this.u.setBackgroundResource(R.drawable.tab_trust_app_nor);
        this.v.setBackgroundResource(R.drawable.tab_scan_result_sel);
        setTitle("扫描结果");
        if (this.f == null || this.f.size() <= 0) {
            i = "没有扫描结果数据";
            this.r.sendEmptyMessage(102);
            return;
        }
        ArrayList arrayList = new ArrayList();
        HashMap hashMap = new HashMap();
        hashMap.put("name", Html.fromHtml("短信权限的警告程序(" + this.k.size() + ")"));
        hashMap.put("packageName", "");
        hashMap.put("img", null);
        arrayList.add(hashMap);
        arrayList.addAll(this.k);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("name", Html.fromHtml("网络权限的警告程序(" + this.l.size() + ")"));
        hashMap2.put("packageName", "");
        hashMap2.put("img", null);
        arrayList.add(hashMap2);
        arrayList.addAll(this.l);
        ListView listView = (ListView) findViewById(R.id.lv_content);
        listView.setAdapter((ListAdapter) new av(this, arrayList, R.layout.list_content, new String[]{"name", "packageName", "img"}, new int[]{R.id.activityInfo_name, R.id.activityInfo_packageName, R.id.activityInfo_img}));
        listView.setOnItemClickListener(new an(this));
    }

    public void c() {
        ResolveInfo resolveInfo = g;
        if (this.q.c(resolveInfo.activityInfo.processName.trim())) {
            i = "信任程序中已经存在";
            this.r.sendEmptyMessage(102);
            return;
        }
        this.q.a(resolveInfo.loadLabel(getPackageManager()).toString().trim(), resolveInfo.activityInfo.processName.trim(), resolveInfo.activityInfo.applicationInfo.sourceDir.trim());
        d();
    }

    public void c(String str) {
        Toast.makeText(this, str, 0).show();
    }

    public void d() {
        Button button = (Button) findViewById(R.id.bt_content_top);
        button.setText("删除已勾选程序");
        button.setOnClickListener(new ai(this));
        List e2 = e();
        if (e2 == null || e2.size() <= 0) {
            i = "目前没有信任程序";
            this.r.sendEmptyMessage(102);
            return;
        }
        ListView listView = (ListView) findViewById(R.id.lv_content);
        listView.setAdapter((ListAdapter) new av(this, e2, R.layout.list_content, new String[]{"name", "packageName", "img"}, new int[]{R.id.activityInfo_name, R.id.activityInfo_packageName, R.id.activityInfo_img}));
        listView.setOnItemClickListener(new ah(this));
        this.s.setBackgroundResource(R.drawable.tab_install_app_nor);
        this.t.setBackgroundResource(R.drawable.tab_assign_app_nor);
        this.u.setBackgroundResource(R.drawable.tab_trust_app_sel);
        this.v.setBackgroundResource(R.drawable.tab_scan_result_nor);
        setTitle("信任程序");
    }

    public List e() {
        ArrayList arrayList = new ArrayList();
        ArrayList<ResolveInfo> arrayList2 = new ArrayList<>();
        List a2 = this.q.a();
        if (this.e == null) {
            Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
            intent.addCategory("android.intent.category.LAUNCHER");
            this.e = getPackageManager().queryIntentActivities(intent, 0);
        }
        if (a2 != null && a2.size() > 0) {
            for (ResolveInfo resolveInfo : this.e) {
                Iterator it = a2.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (((z) it.next()).b().trim().equalsIgnoreCase(resolveInfo.activityInfo.applicationInfo.packageName.trim())) {
                            arrayList2.add(resolveInfo);
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
        }
        if (arrayList2 != null && arrayList2.size() > 0) {
            for (ResolveInfo resolveInfo2 : arrayList2) {
                HashMap hashMap = new HashMap();
                hashMap.put("name", resolveInfo2.loadLabel(getPackageManager()).toString());
                hashMap.put("packageName", resolveInfo2.activityInfo.packageName);
                hashMap.put("img", resolveInfo2.loadIcon(getPackageManager()));
                arrayList.add(hashMap);
            }
        }
        return arrayList;
    }

    public void f() {
        ResolveInfo resolveInfo = g;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("<font color='#7ADD00'>名称</font> " + ((Object) resolveInfo.loadLabel(getPackageManager())) + "<br>");
        stringBuffer.append("<font color='#7ADD00'>程序包 </font>" + resolveInfo.activityInfo.packageName + "<br>");
        stringBuffer.append("<font color='#7ADD00'>路径  </font>" + resolveInfo.activityInfo.applicationInfo.sourceDir + "<br>");
        stringBuffer.append("<br>");
        List b2 = b(resolveInfo.activityInfo.applicationInfo.sourceDir);
        if (!(b2 == null || h == null)) {
            for (i iVar : h) {
                if (b2.indexOf(iVar.a()) != -1) {
                    stringBuffer.append("<font color='#7ADD00'>权限名称 </font> " + iVar.a() + "<br>");
                    stringBuffer.append("<font color='#7ADD00'>权限描述  </font>" + iVar.b() + "<br>");
                    stringBuffer.append("<br>");
                }
            }
        }
        a(stringBuffer.toString());
        i = stringBuffer.toString();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 1) {
            new e(this, this.r, 1).start();
        } else {
            if (i2 == 2) {
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().setFlags(1024, 1024);
        setContentView((int) R.layout.main);
        i();
        new e(this, this.r, 1).start();
        for (Map.Entry next : getSharedPreferences("preferences_data", 0).getAll().entrySet()) {
            if (next.getKey().toString().contains(ak.o)) {
                System.out.println("本地存储过滤信息：" + next.getValue().toString());
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 1, 1, "关于");
        menu.add(0, 2, 2, "帮助");
        menu.add(0, 3, 3, "退出");
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 1:
                a(4);
                return false;
            case 2:
                a(6);
                return false;
            case 3:
                a(5);
                return false;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.q.close();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        this.q = new l(this);
        super.onStart();
    }
}
