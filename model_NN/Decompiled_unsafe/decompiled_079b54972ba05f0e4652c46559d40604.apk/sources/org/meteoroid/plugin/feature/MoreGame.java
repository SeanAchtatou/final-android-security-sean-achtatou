package org.meteoroid.plugin.feature;

import org.meteoroid.core.l;
import xsyjrxgdnxnw.zy_rlw.R;

public class MoreGame extends AbstractOptionMenuItemFeature {
    private String hK;

    public void bN() {
        if (this.hK != null) {
            l.bq(this.hK);
        }
    }

    public void bu(String str) {
        super.bu(str);
        String value = getValue("URL");
        if (value != null) {
            this.hK = value;
        }
        V(l.getString(R.string.more_game));
    }

    public void onDestroy() {
    }
}
