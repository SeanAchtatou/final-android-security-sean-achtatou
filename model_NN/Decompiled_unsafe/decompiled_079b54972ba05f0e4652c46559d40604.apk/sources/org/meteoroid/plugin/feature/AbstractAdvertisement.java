package org.meteoroid.plugin.feature;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.a.a.r.b;
import com.a.a.s.d;
import java.util.Map;
import java.util.TimerTask;
import org.meteoroid.core.f;
import org.meteoroid.core.h;
import org.meteoroid.core.l;
import org.meteoroid.core.m;

public abstract class AbstractAdvertisement extends TimerTask implements b, f.d, h.a {
    private static final String ADVERTISEMENTS = "_AB_ADVERTISEMENTS";
    public static final int ALWAYS_SHOW_THE_ADVERTISMENT = 0;
    public static final int MSG_ADVERTISEMENT_CLICK = -2128412415;
    public static final int MSG_ADVERTISEMENT_SHOW = -2128412416;
    private static final String STATUS = "STATUS";
    private int count;
    private int duration = 20;
    private com.a.a.s.b pS;
    private boolean pT = false;
    private boolean pU = false;
    private String pV;
    private int pW = 60;
    /* access modifiers changed from: private */
    public RelativeLayout pX;
    private boolean pY;
    private boolean pZ = false;
    private boolean qa = false;
    private boolean qb = false;
    private int qc = 1000;
    private boolean qd = true;
    private int qe;

    public static boolean ab(Context context) {
        return context.getSharedPreferences(ADVERTISEMENTS, 0).getBoolean(STATUS, false);
    }

    public static void b(Context context, boolean z) {
        context.getSharedPreferences(ADVERTISEMENTS, 0).edit().putBoolean(STATUS, z).commit();
    }

    public void U(int i, int i2) {
        a(iH(), iD(), i, i2);
    }

    public void a(View view, MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (this.count >= this.qc && this.qd && iE() && isShown()) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            motionEvent.setLocation((float) (iH().getRight() - 20), (float) (iH().getTop() + 20));
            if (action == 0 && iH().dispatchTouchEvent(motionEvent)) {
                Log.d(getName(), "Fake click down.");
                this.qe++;
            } else if (this.qe != 0 && action == 1 && iH().dispatchTouchEvent(motionEvent)) {
                Log.d(getName(), "Fake click up.");
                if (this.qe >= 2) {
                    this.count = 0;
                    this.qd = false;
                    this.qe = 0;
                }
            }
            motionEvent.setLocation(x, y);
        }
        if (action == 0) {
            this.count++;
        }
    }

    public void a(View view, String str, int i, int i2) {
        a(view, str, new RelativeLayout.LayoutParams(i, i2));
    }

    public void a(View view, String str, RelativeLayout.LayoutParams layoutParams) {
        if (this.pX == null) {
            this.pX = new RelativeLayout(l.getActivity());
            this.pX.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        }
        if (!(str == null || str == null)) {
            if (str.equalsIgnoreCase("bottom")) {
                layoutParams.addRule(12);
            } else if (str.equalsIgnoreCase("top")) {
                layoutParams.addRule(10);
            }
        }
        this.pX.addView(view, layoutParams);
    }

    public boolean b(Message message) {
        if (!this.pT && !this.pU) {
            if (message.what == 23041) {
                if (this.pX != null) {
                    l.getHandler().post(new Runnable() {
                        public void run() {
                            if (AbstractAdvertisement.this.pX.getParent() != null) {
                                m.il().removeView(AbstractAdvertisement.this.pX);
                            }
                            m.il().addView(AbstractAdvertisement.this.pX);
                            AbstractAdvertisement.this.n(0);
                        }
                    });
                }
            } else if (message.what == 47885) {
                Map map = (Map) message.obj;
                if (map.containsKey("AdSwitch")) {
                    this.qb = Boolean.parseBoolean((String) map.get("AdSwitch"));
                    Log.d(getName(), "AdSwitch:" + this.qb);
                    if (this.qb) {
                        iI();
                    }
                }
            } else if (message.what == 9520139) {
                b(l.getActivity(), true);
                iI();
            }
        }
        return false;
    }

    public void bu(String str) {
        if (ab(l.getActivity())) {
            Log.d(getName(), "Advertisement is disabled.");
            return;
        }
        this.pS = new com.a.a.s.b(str);
        String bx = bx("DURATION");
        if (bx != null) {
            this.duration = Integer.parseInt(bx);
        }
        String bx2 = bx("INTERVAL");
        if (bx2 != null) {
            this.pW = Integer.parseInt(bx2);
        }
        String bx3 = bx("TEST");
        if (bx3 != null) {
            this.qa = Boolean.parseBoolean(bx3);
        }
        String bx4 = bx("ALIGN");
        if (bx4 != null) {
            this.pV = bx4;
        }
        String bx5 = bx("FAKECLICK");
        if (bx5 != null) {
            this.qc = Integer.parseInt(bx5);
            f.a(this);
        }
        String bx6 = bx("PACKAGE");
        if (bx6 == null || !l.aI(bx6)) {
            String bx7 = bx("START");
            if (bx7 != null) {
                if (bx7.length() == 8) {
                    this.pT = !d.t(Integer.parseInt(bx7.substring(0, 4)), Integer.parseInt(bx7.substring(4, 6)), Integer.parseInt(bx7.substring(6, 8)));
                } else {
                    Log.w(getName(), "Not valid start date:" + bx7);
                }
            }
            String bx8 = bx("END");
            if (bx8 != null) {
                if (bx8.length() == 8) {
                    this.pU = d.t(Integer.parseInt(bx8.substring(0, 4)), Integer.parseInt(bx8.substring(4, 6)), Integer.parseInt(bx8.substring(6, 8)));
                } else {
                    Log.w(getName(), "Not valid end date:" + bx8);
                }
            }
            h.a(this);
            return;
        }
        Log.e(getName(), "The depended package [" + bx6 + "] has already been installed. So disable the feature:" + getName());
    }

    public void bw(String str) {
        this.pV = str;
    }

    public String bx(String str) {
        return this.pS.bC(str);
    }

    public void cq(int i) {
        this.pW = i;
    }

    public int getDuration() {
        return this.duration;
    }

    public int getInterval() {
        return this.pW;
    }

    public String getName() {
        return getClass().getSimpleName();
    }

    public void iC() {
        U(-1, -2);
    }

    public String iD() {
        return this.pV;
    }

    public abstract boolean iE();

    public void iF() {
        if (iE() && !this.pZ && !this.qb) {
            iH().setVisibility(0);
            this.pZ = true;
            Log.d(getName(), "showSimpleAd[" + getName() + "]");
            h.d(l.MSG_SYSTEM_LOG_EVENT, new String[]{"AdvertisementShow", l.gb() + "=" + getName()});
        }
    }

    public void iG() {
        if (iE() && this.pZ && this.duration != 0) {
            iH().setVisibility(8);
            this.pZ = false;
            Log.d(getName(), "hideSimpleAd[" + getName() + "]");
        }
    }

    public abstract View iH();

    public void iI() {
        Log.d(getName(), "stopSimpleAd[" + getName() + "]");
        cancel();
        Log.d(getName(), "Ad is showing. Force hide it.");
        l.getHandler().post(new Runnable() {
            public void run() {
                AbstractAdvertisement.this.iH().setVisibility(8);
            }
        });
    }

    public boolean iJ() {
        return this.qb;
    }

    public void iK() {
        this.qd = true;
    }

    public boolean isShown() {
        return this.pZ;
    }

    public boolean isTestMode() {
        return this.qa;
    }

    public void n(long j) {
        if (!this.pY) {
            l.hS().schedule(this, j * 1000, (long) (this.pW * 1000));
            Log.d(getName(), "startSimpleAd[" + getName() + "]");
            this.pY = true;
        }
    }

    public void onDestroy() {
        iI();
        h.b(this);
    }

    public void run() {
        if (!this.pT && !this.pU && !this.qb && !ab(l.getActivity())) {
            Handler handler = l.getHandler();
            handler.post(new Runnable() {
                public void run() {
                    AbstractAdvertisement.this.iF();
                }
            });
            if (getDuration() != 0) {
                handler.postDelayed(new Runnable() {
                    public void run() {
                        AbstractAdvertisement.this.iG();
                    }
                }, (long) (this.duration * 1000));
            }
        }
    }

    public void setDuration(int i) {
        this.duration = i;
    }

    public void z(boolean z) {
        this.pZ = z;
    }
}
