package org.meteoroid.plugin.feature;

import android.content.DialogInterface;
import android.os.Message;
import android.util.Log;
import com.a.a.s.d;
import java.util.TimerTask;
import org.meteoroid.core.a;
import org.meteoroid.core.h;
import org.meteoroid.core.k;
import org.meteoroid.core.l;
import org.meteoroid.plugin.feature.AbstractPaymentManager;

public abstract class AbstractTrialActivation extends AbstractPaymentManager {
    public static final int LAUNCH_LIMIT = 1;
    public static final int SYSTEM_MSG_TRAILACTIVATION = -1742057216;
    public static final int TIME_LIMIT = 0;
    private int mode = 0;
    private boolean pT;
    private int qD = 60000;
    private int qE = 0;
    private int qF;
    private boolean qG;
    private boolean qH;
    private int qI;
    private boolean qJ = false;

    private void jd() {
        l.hS().schedule(new TimerTask() {
            public void run() {
                k.cj(0).getEditor().putBoolean("trial", true).commit();
                if (!AbstractTrialActivation.this.jc()) {
                    h.ci(AbstractTrialActivation.SYSTEM_MSG_TRAILACTIVATION);
                }
            }
        }, (long) this.qD);
    }

    public void A(boolean z) {
        k.cj(0).getEditor().putBoolean("actived", z).commit();
    }

    public boolean b(Message message) {
        if (this.pT) {
            return super.b(message);
        }
        if (message.what == 47872 && !this.qJ) {
            this.qJ = true;
            Log.d(getName(), "IsTrialEnded is " + jb());
            if (jb()) {
                Log.d(getName(), "IgnoreBlockAtStart is " + this.qI);
                if (this.qI != 0) {
                    this.qD = this.qI;
                    if (this.mode == 0) {
                        try {
                            jd();
                            Log.d(getName(), "startActivationCounter...in " + this.qD);
                            return false;
                        } catch (Exception e) {
                            Log.w(getName(), e);
                            return false;
                        }
                    } else if (this.mode != 1) {
                        return false;
                    } else {
                        Log.w(getName(), "IgnoreBlockAtStart would cause activation based on launch time not work.");
                        l.hA();
                        return false;
                    }
                } else {
                    Log.d(getName(), "HasActived is " + jc());
                    if (!jc()) {
                        h.ci(SYSTEM_MSG_TRAILACTIVATION);
                        return true;
                    }
                }
            } else if (this.mode == 0) {
                try {
                    jd();
                    Log.d(getName(), "startActivationCounter...in " + this.qD);
                } catch (Exception e2) {
                    Log.w(getName(), e2);
                }
            }
        } else if (message.what == -1742057216) {
            je();
            return true;
        } else if (message.what == 61700) {
            k.cj(0).getEditor().putBoolean("nomore", true).commit();
            this.qH = true;
        } else if (message.what == 61698) {
            Log.d(getName(), "Payment:" + ((AbstractPaymentManager.Payment) message.obj).bK() + " is successed.");
            gl();
            jf();
            if (a.mL == null) {
                h.c(h.c(l.MSG_SYSTEM_INIT_COMPLETE, null));
            }
            return true;
        } else if (message.what == 61699) {
            Log.d(getName(), "Payment:" + ((AbstractPaymentManager.Payment) message.obj).bK() + " is failed.");
            gl();
            fail();
            return true;
        }
        return super.b(message);
    }

    public void bu(String str) {
        boolean z = true;
        super.bu(str);
        h.h(SYSTEM_MSG_TRAILACTIVATION, "SYSTEM_MSG_TRAILACTIVATION");
        String bx = bx("EXPIRED");
        if (bx != null) {
            this.qD = Integer.parseInt(bx) * 1000;
            this.mode = 0;
        }
        String bx2 = bx("LAUNCH");
        if (bx2 != null) {
            this.qE = Integer.parseInt(bx2);
            this.mode = 1;
        }
        String bx3 = bx("MULTI");
        if (bx3 != null) {
            this.qG = Boolean.parseBoolean(bx3);
        }
        String bx4 = bx("IGNORE");
        if (bx4 != null) {
            this.qI = Integer.parseInt(bx4) * 1000;
        }
        if (this.mode == 1) {
            this.qF = k.cj(0).getSharedPreferences().getInt("launch", 0);
            k.cj(0).getEditor().putInt("launch", this.qF + 1).commit();
            Log.d(getName(), "CurrentLaunchTime is " + this.qF);
        }
        this.qH = k.cj(0).getSharedPreferences().getBoolean("nomore", false);
        Log.d(getName(), "NoMoreActivation is " + this.qH);
        String bx5 = bx("START");
        if (bx5 == null) {
            return;
        }
        if (bx5.length() == 8) {
            if (d.t(Integer.parseInt(bx5.substring(0, 4)), Integer.parseInt(bx5.substring(4, 6)), Integer.parseInt(bx5.substring(6, 8)))) {
                z = false;
            }
            this.pT = z;
            return;
        }
        Log.w(getName(), "Not valid start date:" + bx5);
    }

    public abstract void cancel();

    public void fail() {
        A(false);
        l.resume();
    }

    public boolean jb() {
        return this.mode == 0 ? k.cj(0).getSharedPreferences().getBoolean("trial", false) : this.mode != 1 || this.qF >= this.qE;
    }

    public boolean jc() {
        return this.qG ? this.qH : k.cj(0).getSharedPreferences().getBoolean("actived", false);
    }

    public abstract void je();

    public void jf() {
        A(true);
        if (this.qG) {
            if (this.mode == 0) {
                k.cj(0).getEditor().putBoolean("trial", false).commit();
            } else if (this.mode == 1) {
                k.cj(0).getEditor().putInt("launch", 0).commit();
                this.qF = 0;
            }
        }
        l.resume();
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (i >= iQ().size() || i < 0) {
            Log.w(getName(), "Selected an unknown payment." + i);
            fail();
            return;
        }
        iT();
        Log.d(getName(), "Paying with " + iQ().get(i).bK());
        iQ().get(i).iV();
    }

    public void onDestroy() {
        super.onDestroy();
    }
}
