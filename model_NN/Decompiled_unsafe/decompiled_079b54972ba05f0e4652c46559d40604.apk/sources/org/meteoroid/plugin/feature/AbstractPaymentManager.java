package org.meteoroid.plugin.feature;

import android.content.DialogInterface;
import android.os.Message;
import android.util.Log;
import com.a.a.r.b;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.meteoroid.core.d;
import org.meteoroid.core.h;
import org.meteoroid.core.l;
import org.meteoroid.core.m;
import xsyjrxgdnxnw.zy_rlw.R;

public abstract class AbstractPaymentManager implements DialogInterface.OnClickListener, b, h.a {
    private com.a.a.s.b pS;
    private Set<String> qu;
    private boolean qv = false;
    private LinkedList<Payment> qw;

    public interface Payment extends b {
        public static final int MSG_PAYMENT_FAIL = 61699;
        public static final int MSG_PAYMENT_NO_MORE = 61700;
        public static final int MSG_PAYMENT_QUERY = 61697;
        public static final int MSG_PAYMENT_REQUEST = 61696;
        public static final int MSG_PAYMENT_SUCCESS = 61698;

        String bK();

        void iV();
    }

    public void a(Payment payment) {
        if (!this.qw.contains(payment)) {
            Log.d(getName(), payment.bK() + " has added into availiable payments.");
            this.qw.add(payment);
        }
    }

    public void b(Message message, String str) {
        Iterator<Payment> it = this.qw.iterator();
        while (it.hasNext()) {
            if (it.next().getClass().getSimpleName().equalsIgnoreCase(str)) {
                h.a(message, str);
            }
        }
    }

    public boolean b(Message message) {
        if (message.what != 47876) {
            return false;
        }
        iR();
        return false;
    }

    public void bu(String str) {
        this.pS = new com.a.a.s.b(str);
        String bC = this.pS.bC("EXCLUDE");
        this.qu = new HashSet();
        if (bC != null) {
            this.qu.addAll(Arrays.asList(bC.split(";")));
        }
        this.qw = new LinkedList<>();
        h.h(Payment.MSG_PAYMENT_REQUEST, "MSG_PAYMENT_REQUEST");
        h.h(Payment.MSG_PAYMENT_QUERY, "MSG_PAYMENT_QUERY");
        h.h(Payment.MSG_PAYMENT_SUCCESS, "MSG_PAYMENT_SUCCESS");
        h.h(Payment.MSG_PAYMENT_FAIL, "MSG_PAYMENT_FAIL");
        h.a(this);
    }

    public String bx(String str) {
        return this.pS.bC(str);
    }

    public void by(String str) {
        Iterator<Payment> it = this.qw.iterator();
        while (it.hasNext()) {
            Payment next = it.next();
            if (next.getClass().getSimpleName().equalsIgnoreCase(str)) {
                next.iV();
            }
        }
    }

    public void bz(String str) {
        if (this.qu == null) {
            this.qu = new HashSet();
        }
        this.qu.add(str);
    }

    public void gl() {
        this.qv = false;
        m.ig();
    }

    public List<Payment> iQ() {
        return this.qw;
    }

    public void iR() {
        boolean z;
        Iterator<b> it = d.hg().iterator();
        while (it.hasNext()) {
            b next = it.next();
            if (next instanceof Payment) {
                Iterator<String> it2 = this.qu.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        z = false;
                        break;
                    }
                    if (next.getClass().getSimpleName().equalsIgnoreCase(it2.next())) {
                        z = true;
                        break;
                    }
                }
                if (!z) {
                    a((Payment) next);
                } else {
                    Log.d(getName(), next.getClass().getSimpleName() + " is excluded.");
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.meteoroid.core.m.a(java.lang.String, java.lang.String, java.lang.String[], android.content.DialogInterface$OnClickListener, boolean):void
     arg types: [?[OBJECT, ARRAY], ?[OBJECT, ARRAY], java.lang.String[], org.meteoroid.plugin.feature.AbstractPaymentManager, int]
     candidates:
      org.meteoroid.core.m.a(java.lang.String, java.lang.String, android.view.View, boolean, android.content.DialogInterface$OnCancelListener):void
      org.meteoroid.core.m.a(java.lang.String, java.lang.String, java.lang.String[], android.content.DialogInterface$OnClickListener, boolean):void */
    public void iS() {
        Log.d(getName(), "There is " + this.qw.size() + " payment(s) in all.");
        if (this.qw.size() == 1) {
            iT();
            this.qw.get(0).iV();
        } else if (this.qw.size() > 1) {
            String[] strArr = new String[this.qw.size()];
            for (int i = 0; i < strArr.length; i++) {
                strArr[i] = this.qw.get(i).bK();
            }
            m.a((String) null, (String) null, strArr, (DialogInterface.OnClickListener) this, false);
        } else {
            l.j("没有可用的计费插件，计费插件初始化失败", 0);
            l.hA();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.meteoroid.core.m.a(java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [?[OBJECT, ARRAY], java.lang.String, int, int]
     candidates:
      org.meteoroid.core.m.a(java.lang.String, java.lang.String, java.lang.String, android.content.DialogInterface$OnClickListener):void
      org.meteoroid.core.m.a(java.lang.String, java.lang.String, boolean, boolean):void */
    public void iT() {
        this.qv = true;
        m.a((String) null, l.getString(R.string.activating), false, true);
    }

    public boolean iU() {
        return this.qv;
    }

    public void onDestroy() {
        this.qw.clear();
        this.qw = null;
    }
}
