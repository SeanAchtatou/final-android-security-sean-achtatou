package org.meteoroid.plugin.feature;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Message;
import android.provider.Settings;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.a.a.s.b;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.util.HashMap;
import java.util.Map;
import me.gall.sgp.sdk.service.BossService;
import mm.sms.purchasesdk.OnSMSPurchaseListener;
import mm.sms.purchasesdk.PurchaseCode;
import mm.sms.purchasesdk.SMSPurchase;
import org.meteoroid.core.h;
import org.meteoroid.core.l;
import org.meteoroid.plugin.device.MIDPDevice;
import org.meteoroid.plugin.feature.AbstractPaymentManager;
import xsyjrxgdnxnw.zy_rlw.R;

public class MMIAPSMS implements OnSMSPurchaseListener, h.a, AbstractPaymentManager.Payment {
    /* access modifiers changed from: private */
    public String PAYCODE;
    private String qM;
    private String qN;
    /* access modifiers changed from: private */
    public int qO = 1;
    /* access modifiers changed from: private */
    public SMSPurchase qP;
    private String qQ;
    private boolean qv;

    public static class SMSReceiver extends BroadcastReceiver {
        private static final Class<?>[] qS = {Context.class, String.class, Map.class};
        private static final Class<?>[] qT = {Context.class};

        public static void a(Context context, String str, Map<String, String> map) {
            try {
                Class.forName("com.umeng.analytics.MobclickAgent").getMethod("onEvent", qS).invoke(null, context, str, map);
            } catch (Exception e) {
                Log.w("SMSReceiver", e);
            }
        }

        public static void ac(Context context) {
            try {
                Class.forName("com.umeng.analytics.MobclickAgent").getMethod("onResume", qT).invoke(null, context);
            } catch (Exception e) {
                Log.w("SMSReceiver", e);
            }
        }

        public static String ad(Context context) {
            String devicePhonenumber = getDevicePhonenumber(context);
            if (devicePhonenumber == null || devicePhonenumber.trim().equals("")) {
                Log.d("SMSReceiver", "phoneNumber is empty.Try imei.");
                devicePhonenumber = getDeviceIMEI(context);
            }
            if (devicePhonenumber == null || devicePhonenumber.trim().equals("")) {
                Log.d("SMSReceiver", "imei is empty. Try IDHash.");
                devicePhonenumber = getDeviceIDHash(context);
            }
            Log.d("SMSReceiver", "uniqueID is " + devicePhonenumber);
            return devicePhonenumber;
        }

        public static String getDeviceIDHash(Context context) {
            String str;
            String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
            if (!"9774d56d682e549c".equals(string)) {
                try {
                    Class<?> cls = Class.forName("android.os.SystemProperties");
                    str = (String) cls.getMethod("get", String.class).invoke(cls, "ro.serialno");
                } catch (Exception e) {
                    str = string;
                }
            } else {
                str = string;
            }
            Log.d("SMSReceiver", "androidId=" + str);
            return str;
        }

        public static String getDeviceIMEI(Context context) {
            if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") != 0) {
                return getDeviceIDHash(context);
            }
            String str = null;
            try {
                str = getTelephonyManager(context).getDeviceId();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d("SMSReceiver", "imei=" + str);
            return str;
        }

        public static String getDevicePhonenumber(Context context) {
            String str = null;
            if (context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
                try {
                    str = getTelephonyManager(context).getLine1Number();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d("SMSReceiver", "phoneNumber=" + str);
            }
            return str;
        }

        public static TelephonyManager getTelephonyManager(Context context) {
            return (TelephonyManager) context.getSystemService("phone");
        }

        public void onReceive(Context context, Intent intent) {
            Object[] objArr = (Object[]) intent.getExtras().get("pdus");
            SmsMessage[] smsMessageArr = new SmsMessage[objArr.length];
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < objArr.length) {
                    smsMessageArr[i2] = SmsMessage.createFromPdu((byte[]) objArr[i2]);
                    String originatingAddress = smsMessageArr[i2].getOriginatingAddress();
                    String messageBody = smsMessageArr[i2].getMessageBody();
                    Log.d("SMSReceiver", "Receiving sms from " + originatingAddress + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + messageBody);
                    String string = context.getString(R.string.app_name);
                    if (originatingAddress.contains("10086") && messageBody.contains(string) && ((messageBody.contains("未收取") || messageBody.contains("成功取消") || messageBody.contains("未扣费") || messageBody.contains("不收取") || messageBody.contains("不扣除") || messageBody.contains("成功退订")) && messageBody.contains("费用"))) {
                        ac(context);
                        HashMap hashMap = new HashMap();
                        hashMap.put("应用名称", string);
                        hashMap.put("uid", ad(context));
                        hashMap.put("机型", Build.MANUFACTURER + "_" + Build.MODEL);
                        a(context, "MMBillingRefund", hashMap);
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    private void fail() {
        this.qv = false;
        h.c(h.c(MIDPDevice.j.MSG_GCF_SMS_CONNECTION_SEND_FAIL, null));
        h.c(h.c(AbstractPaymentManager.Payment.MSG_PAYMENT_FAIL, this));
        h.d(l.MSG_SYSTEM_LOG_EVENT, new String[]{"MMIAPSMSFail", "计费点", this.PAYCODE, "应用名称", l.gb(), "uid", l.hV(), "原因", this.qQ});
        Log.d(getName(), "Billing fail...");
    }

    private void jf() {
        this.qv = false;
        h.c(h.c(MIDPDevice.j.MSG_GCF_SMS_CONNECTION_SEND_COMPLETE, null));
        h.c(h.c(AbstractPaymentManager.Payment.MSG_PAYMENT_SUCCESS, this));
        h.d(l.MSG_SYSTEM_LOG_EVENT, new String[]{"MMIAPSMSSuccess", "total", l.gb() + "_" + this.PAYCODE, "计费点", this.PAYCODE, "应用名称", l.gb(), "uid", l.hV()});
        Log.d(getName(), "Billing success." + this.PAYCODE);
    }

    public boolean b(Message message) {
        if (message.what == 15391744) {
            if (message.obj == null) {
                iV();
            } else {
                com.a.a.q.h hVar = (com.a.a.q.h) message.obj;
                if (hVar instanceof com.a.a.q.h) {
                    String fS = hVar.fS();
                    String address = hVar.getAddress();
                    if (address.startsWith("sms://")) {
                        address = address.substring(6);
                    }
                    if (address.startsWith("+86")) {
                        address = address.substring(3);
                    }
                    if (address.length() == 11 && (address.startsWith("13") || address.startsWith("15") || address.startsWith("18") || address.startsWith("14"))) {
                        return false;
                    }
                    if (fS != null) {
                        String[] split = fS.split(BossService.ID_SEPARATOR);
                        this.PAYCODE = split[0];
                        if (split.length > 1) {
                            this.qO = Integer.parseInt(split[1]);
                        }
                    }
                    iV();
                }
            }
            return true;
        }
        if (message.what == 61697) {
            ((AbstractPaymentManager) message.obj).a(this);
        }
        return false;
    }

    public String bK() {
        return getName();
    }

    public void bu(String str) {
        b bVar = new b(str);
        String bC = bVar.bC("APPID");
        if (bC != null) {
            this.qM = bC;
        }
        String bC2 = bVar.bC("APPKEY");
        if (bC2 != null) {
            this.qN = bC2;
        }
        String bC3 = bVar.bC("PAYCODE");
        if (bC3 != null) {
            this.PAYCODE = bC3;
        }
        String bC4 = bVar.bC("ORDERS");
        if (bC4 != null) {
            this.qO = Integer.parseInt(bC4);
        }
        this.qP = SMSPurchase.getInstance();
        try {
            this.qP.setAppInfo(this.qM, this.qN);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d(getName(), "init:" + System.currentTimeMillis());
        h.a(this);
    }

    public String getName() {
        return getClass().getSimpleName();
    }

    public void iV() {
        if (!this.qv) {
            this.qv = true;
            l.getHandler().post(new Runnable() {
                public void run() {
                    Log.d(MMIAPSMS.this.getName(), "Paycode:" + MMIAPSMS.this.PAYCODE + " OrderCount:" + MMIAPSMS.this.qO);
                    try {
                        MMIAPSMS.this.qP.smsOrder(l.getActivity(), MMIAPSMS.this.PAYCODE, MMIAPSMS.this);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public void onBillingFinish(int i, HashMap hashMap) {
        String str;
        Log.d(getName(), "billing finish, status code = " + i);
        if (1001 == 1001) {
            str = "订购结果：订购成功。";
            jf();
        } else {
            this.qQ = SMSPurchase.getReason(PurchaseCode.ORDER_OK);
            str = "订购结果：" + this.qQ;
            fail();
        }
        Log.d(getName(), str);
    }

    public void onDestroy() {
    }

    public void onInitFinish(int i) {
        Log.d(getName(), "Init finish, status code = " + i);
        Log.d(getName(), "初始化结果：" + SMSPurchase.getReason(i));
    }
}
