package org.meteoroid.plugin.feature;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.os.Message;
import android.util.Log;
import com.a.a.m.m;
import com.a.a.r.b;
import com.umeng.analytics.MobclickAgent;
import com.umeng.update.UmengUpdateAgent;
import com.umeng.update.UmengUpdateListener;
import com.umeng.update.UpdateResponse;
import java.util.HashMap;
import me.gall.sgp.sdk.service.BossService;
import org.meteoroid.core.h;
import org.meteoroid.core.k;
import org.meteoroid.core.l;

public class Umeng implements b, h.a {
    private static final long DISABLE = 0;
    public static final String UMENG_CONFIGURATIONS = "UMENG_CONFIGURATIONS";
    private long qU = DISABLE;

    private static boolean ae(Context context) {
        return context.getApplicationContext().getSharedPreferences("UMENG_CONFIGURATIONS", 0).getBoolean("DAILYCHECKIN", true);
    }

    private static void c(Context context, boolean z) {
        context.getApplicationContext().getSharedPreferences("UMENG_CONFIGURATIONS", 0).edit().putBoolean("DAILYCHECKIN", z).commit();
    }

    private void jg() {
        new Thread() {
            public void run() {
                String hY;
                StringBuffer stringBuffer = new StringBuffer();
                try {
                    hY = l.hJ().getDeviceId();
                } catch (Exception e) {
                    Exception exc = e;
                    hY = l.hY();
                    exc.printStackTrace();
                }
                stringBuffer.append(hY + "=");
                stringBuffer.append(l.gG() + "=");
                stringBuffer.append(l.gv() + "=");
                for (ApplicationInfo next : l.getActivity().getPackageManager().getInstalledApplications(128)) {
                    if (!next.packageName.startsWith("com.google") && !next.packageName.startsWith("com.android")) {
                        stringBuffer.append(next.packageName + BossService.ID_SEPARATOR);
                    }
                }
                MobclickAgent.onEvent(l.getActivity(), "CollectAppInfo", stringBuffer.toString());
            }
        }.start();
    }

    public boolean b(Message message) {
        if (message.what == 40960) {
            MobclickAgent.onPause((Context) message.obj);
        } else if (message.what == 40961) {
            MobclickAgent.onResume((Context) message.obj);
        } else if (message.what == 47887 || message.what == 47902) {
            String[] strArr = (String[]) message.obj;
            if (strArr.length == 1) {
                MobclickAgent.onEvent(l.getActivity(), strArr[0]);
            } else if (strArr.length == 2) {
                MobclickAgent.onEvent(l.getActivity(), strArr[0], strArr[1]);
            } else if (strArr.length > 2) {
                HashMap hashMap = new HashMap();
                for (int i = 1; i < strArr.length; i += 2) {
                    if (i + 1 < strArr.length) {
                        hashMap.put(strArr[i], strArr[i + 1]);
                    }
                }
                MobclickAgent.onEvent(l.getActivity(), strArr[0], hashMap);
            }
        }
        return false;
    }

    public void bu(String str) {
        MobclickAgent.onError(l.getActivity());
        UmengUpdateAgent.setUpdateOnlyWifi(false);
        UmengUpdateAgent.update(l.getActivity());
        UmengUpdateAgent.setUpdateListener(new UmengUpdateListener() {
            public void onUpdateReturned(int i, UpdateResponse updateResponse) {
                switch (i) {
                    case 0:
                        UmengUpdateAgent.showUpdateDialog(l.getActivity(), updateResponse);
                        HashMap hashMap = new HashMap();
                        hashMap.put(m.PROP_VERSION, updateResponse.version);
                        MobclickAgent.onEvent(l.getActivity(), "UmengUpdateShow", hashMap);
                        return;
                    default:
                        Log.d(Umeng.this.getName(), "None update.");
                        return;
                }
            }
        });
        com.a.a.s.b bVar = new com.a.a.s.b(str);
        String bC = bVar.bC("COLLECTINFO");
        if (bC != null) {
            this.qU = Long.parseLong(bC) * 1000 * 60 * 60 * 24;
        }
        if (this.qU != DISABLE) {
            long currentTimeMillis = System.currentTimeMillis();
            SharedPreferences sharedPreferences = k.cj(0).getSharedPreferences();
            long j = sharedPreferences.getLong("LastDay", DISABLE);
            if (j == DISABLE || currentTimeMillis - j >= this.qU) {
                sharedPreferences.edit().putLong("LastDay", currentTimeMillis).commit();
                jg();
                Log.d(getName(), "Time to collect.");
            }
        }
        String bC2 = bVar.bC("DAILYCHECKIN");
        if (bC2 != null) {
            c(l.getActivity(), Boolean.parseBoolean(bC2));
        }
        h.a(this);
    }

    public String getName() {
        return "Umeng";
    }

    public void onDestroy() {
    }
}
