package org.meteoroid.plugin.feature;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Message;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import java.util.ArrayList;
import org.meteoroid.core.h;
import org.meteoroid.core.l;
import org.meteoroid.plugin.feature.AbstractPaymentManager;

public abstract class AbstractSMSPayment implements h.a, AbstractPaymentManager.Payment {
    public static final int CARRIER_CMCC = 1;
    public static final int CARRIER_TELECOM = 3;
    public static final int CARRIER_UNICOM = 2;
    public static final int CARRIER_UNKNOWN = 0;
    public static final String SMS_DELIEVERED_ACTION = "org.meteoroid.plugin.feature.sms_delievered";
    public static final String SMS_RECV_ACTION = "android.provider.Telephony.SMS_RECEIVED";
    public static final String SMS_SENT_ACTION = "org.meteoroid.plugin.feature.sms_sent";
    private static final String[] qx = {"未知运营商", "中国移动", "中国联通", "中国电信"};
    private int qy;

    private void a(final String str, String str2, final long j) {
        if (l.getActivity().checkCallingOrSelfPermission("android.permission.SEND_SMS") == 0) {
            final String str3 = str;
            final long j2 = j;
            final String str4 = str2;
            l.getActivity().registerReceiver(new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    if (intent.getAction().equals(AbstractSMSPayment.SMS_SENT_ACTION + str3 + j2)) {
                        int resultCode = getResultCode();
                        if (resultCode == -1) {
                            Log.d(AbstractSMSPayment.SMS_SENT_ACTION, "Success!");
                            String[] strArr = new String[2];
                            strArr[0] = "SMS_sent_success";
                            strArr[1] = (str4 == null ? str3 : str4) + "=" + l.gb();
                            h.d(l.MSG_SYSTEM_LOG_EVENT, strArr);
                            AbstractSMSPayment.this.iX();
                            return;
                        }
                        Log.w(AbstractSMSPayment.SMS_SENT_ACTION, "ResultCode:" + resultCode);
                        String[] strArr2 = new String[2];
                        strArr2[0] = "SMS_delievered_fail";
                        strArr2[1] = (str4 == null ? str3 : str4) + "=" + l.gb();
                        h.d(l.MSG_SYSTEM_LOG_EVENT, strArr2);
                        AbstractSMSPayment.this.cv(resultCode);
                    }
                }
            }, new IntentFilter(SMS_SENT_ACTION + str + j));
            l.getActivity().registerReceiver(new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    if (intent.getAction().equals(AbstractSMSPayment.SMS_DELIEVERED_ACTION + str + j)) {
                        int resultCode = getResultCode();
                        if (resultCode == -1) {
                            Log.d(AbstractSMSPayment.SMS_DELIEVERED_ACTION, "Success!");
                            AbstractSMSPayment.this.iY();
                            return;
                        }
                        Log.w(AbstractSMSPayment.SMS_DELIEVERED_ACTION, "ResultCode:" + resultCode);
                        AbstractSMSPayment.this.cw(resultCode);
                    }
                }
            }, new IntentFilter(SMS_DELIEVERED_ACTION + str + j));
        }
    }

    private PendingIntent b(String str, long j) {
        return PendingIntent.getBroadcast(l.getActivity(), 0, new Intent(SMS_SENT_ACTION + str + j), 0);
    }

    private PendingIntent c(String str, long j) {
        return PendingIntent.getBroadcast(l.getActivity(), 0, new Intent(SMS_DELIEVERED_ACTION + str + j), 0);
    }

    public abstract void A(String str, String str2);

    public void a(String str, short s, byte[] bArr) {
        long currentTimeMillis = System.currentTimeMillis();
        a(str, (String) null, currentTimeMillis);
        SmsManager.getDefault().sendDataMessage(str, null, s, bArr, b(str, currentTimeMillis), c(str, currentTimeMillis));
        Log.d(getName(), "Send " + bArr + " to " + str);
    }

    public boolean b(Message message) {
        if (message.what != 61697) {
            return false;
        }
        ((AbstractPaymentManager) message.obj).a(this);
        return false;
    }

    public void bu(String str) {
        if (l.getActivity().checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            try {
                String hB = l.hB();
                if (hB != null) {
                    String str2 = "";
                    if (hB.startsWith("46000") || hB.startsWith("46002") || hB.startsWith("46007")) {
                        str2 = "13800100500";
                        this.qy = 1;
                    } else if (hB.startsWith("46001") || hB.startsWith("46006")) {
                        str2 = "13010200500";
                        this.qy = 2;
                    } else if (hB.startsWith("46003") || hB.startsWith("46005")) {
                        this.qy = 3;
                    }
                    System.setProperty("wireless.messaging.sms.smsc", str2);
                    Log.d(getName(), "Set smsc:" + str2);
                }
            } catch (Exception e) {
                Log.w(getName(), "Couldn't get the operator.");
            }
        }
        h.a(this);
        if (l.getActivity().checkCallingOrSelfPermission("android.permission.RECEIVE_SMS") == 0) {
            l.getActivity().registerReceiver(new BroadcastReceiver() {
                public void onReceive(Context context, Intent intent) {
                    Bundle extras;
                    if (intent.getAction().equals(AbstractSMSPayment.SMS_RECV_ACTION) && (extras = intent.getExtras()) != null) {
                        try {
                            Object[] objArr = (Object[]) extras.get("pdus");
                            SmsMessage[] smsMessageArr = new SmsMessage[objArr.length];
                            for (int i = 0; i < objArr.length; i++) {
                                smsMessageArr[i] = SmsMessage.createFromPdu((byte[]) objArr[i]);
                            }
                            for (SmsMessage smsMessage : smsMessageArr) {
                                Log.d(AbstractSMSPayment.SMS_RECV_ACTION, "Success!");
                                AbstractSMSPayment.this.A(smsMessage.getDisplayOriginatingAddress(), smsMessage.getDisplayMessageBody());
                            }
                        } catch (Exception e) {
                            Log.e(AbstractSMSPayment.this.getName(), "Fail to receive sms." + e);
                        }
                    }
                }
            }, new IntentFilter(SMS_RECV_ACTION));
        }
    }

    public String ct(int i) {
        return qx[i];
    }

    public void cu(int i) {
        this.qy = i;
    }

    public abstract void cv(int i);

    public abstract void cw(int i);

    public String gx() {
        return qx[this.qy];
    }

    public int iW() {
        return this.qy;
    }

    public abstract void iX();

    public abstract void iY();

    public void iZ() {
        h.c(h.c(AbstractPaymentManager.Payment.MSG_PAYMENT_SUCCESS, this));
    }

    public void ja() {
        h.c(h.c(AbstractPaymentManager.Payment.MSG_PAYMENT_FAIL, this));
    }

    public void onDestroy() {
    }

    public void z(String str, String str2) {
        SmsManager smsManager = SmsManager.getDefault();
        ArrayList<String> divideMessage = smsManager.divideMessage(str2);
        long currentTimeMillis = System.currentTimeMillis();
        a(str, str2, currentTimeMillis);
        for (String next : divideMessage) {
            smsManager.sendTextMessage(str, null, next, b(str, currentTimeMillis), c(str, currentTimeMillis));
            Log.d(getName(), "Send " + next + " to " + str);
        }
    }
}
