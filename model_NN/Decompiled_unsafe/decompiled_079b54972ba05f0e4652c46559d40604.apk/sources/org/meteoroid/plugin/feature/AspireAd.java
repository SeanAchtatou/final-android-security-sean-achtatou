package org.meteoroid.plugin.feature;

import android.util.Log;
import android.view.View;
import com.aspire.ad.AdBannar;
import com.aspire.ad.AdManager;
import org.meteoroid.core.l;

public class AspireAd extends AbstractAdvertisement {
    private AdBannar qL;

    public void bu(String str) {
        long j;
        super.bu(str);
        try {
            j = Long.parseLong(bx("APPID"));
        } catch (Exception e) {
            Log.e(getName(), "Hey, the most important APPID is missed." + e);
            j = 0;
        }
        AdManager.z(l.getActivity()).a(j, bx("APPCODE"));
        this.qL = new AdBannar(l.getActivity());
        iC();
        n(0);
    }

    public boolean iE() {
        return true;
    }

    public View iH() {
        return this.qL;
    }

    public void onDestroy() {
        AdManager.z(l.getActivity()).finalize();
    }
}
