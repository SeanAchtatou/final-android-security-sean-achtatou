package org.meteoroid.plugin.feature;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Message;
import android.util.Log;
import com.a.a.s.e;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.meteoroid.core.h;
import org.meteoroid.core.k;
import org.meteoroid.core.l;
import org.meteoroid.core.m;
import org.meteoroid.plugin.feature.AbstractPaymentManager;

public abstract class AbstractDownloadAndInstall extends BroadcastReceiver implements h.a, AbstractPaymentManager.Payment {
    public static final int INSTALL_FAIL = 3;
    public static final int INSTALL_NOT_START = 0;
    public static final int INSTALL_RUNNING = 1;
    public static final int INSTALL_SUCCESS = 2;
    private static final String fileName = "target.apk";
    public com.a.a.s.b pS;
    private a[] qg;
    private ArrayList<a> qh;
    private a qi;
    private boolean qj;
    private int qk = 0;
    private boolean ql;
    public boolean qm;
    /* access modifiers changed from: private */
    public String qn;
    /* access modifiers changed from: private */
    public String qo;

    public class a {
        public String packageName;
        public String qp;
        public boolean qq;
        public String qr;
        public String qs;

        public a() {
        }
    }

    private final class b extends AsyncTask<URL, Integer, Long> {
        private b() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Long doInBackground(URL... urlArr) {
            File file;
            try {
                if (!new File(AbstractDownloadAndInstall.this.qo).exists()) {
                    File file2 = new File(AbstractDownloadAndInstall.this.qn);
                    file2.mkdirs();
                    if (!l.hC()) {
                        return 0L;
                    }
                    HttpURLConnection httpURLConnection = (HttpURLConnection) urlArr[0].openConnection();
                    httpURLConnection.setRequestMethod(com.a.a.c.h.GET);
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.connect();
                    File file3 = new File(file2, AbstractDownloadAndInstall.fileName);
                    try {
                        InputStream inputStream = httpURLConnection.getInputStream();
                        AbstractDownloadAndInstall.this.e(inputStream);
                        inputStream.close();
                    } catch (IOException e) {
                        file = file3;
                        Log.e(AbstractDownloadAndInstall.this.getName(), "Fail to download the file.");
                        if (file != null && file.exists()) {
                            file.delete();
                        }
                        return -1L;
                    }
                }
                return 100L;
            } catch (IOException e2) {
                file = null;
                Log.e(AbstractDownloadAndInstall.this.getName(), "Fail to download the file.");
                file.delete();
                return -1L;
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public void onPostExecute(Long l) {
            if (l.longValue() == 100) {
                m.ig();
                AbstractDownloadAndInstall.this.iN();
            } else if (l.longValue() == -1) {
                m.ig();
                if (!AbstractDownloadAndInstall.this.qm) {
                    h.c(h.c(AbstractPaymentManager.Payment.MSG_PAYMENT_FAIL, this));
                }
            } else {
                Log.d(AbstractDownloadAndInstall.this.getName(), "On Post Execute result..." + l);
            }
        }
    }

    /* access modifiers changed from: private */
    public void e(InputStream inputStream) {
        FileOutputStream openFileOutput = l.getActivity().openFileOutput(fileName, 1);
        byte[] bArr = new byte[102400];
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                openFileOutput.write(bArr, 0, read);
            } else {
                openFileOutput.flush();
                openFileOutput.close();
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void iN() {
        if (this.qo == null) {
            this.qn = l.getActivity().getFilesDir().getAbsolutePath() + File.separator;
            this.qo = this.qn + fileName;
        }
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setDataAndType(Uri.fromFile(new File(this.qo)), "application/vnd.android.package-archive");
        l.getActivity().startActivity(intent);
    }

    private void iO() {
        InputStream bl = l.bl(e.bE(this.qi.qp));
        e(bl);
        bl.close();
        iN();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.meteoroid.core.m.a(java.lang.String, java.lang.String, boolean, boolean):void
     arg types: [?[OBJECT, ARRAY], java.lang.String, int, int]
     candidates:
      org.meteoroid.core.m.a(java.lang.String, java.lang.String, java.lang.String, android.content.DialogInterface$OnClickListener):void
      org.meteoroid.core.m.a(java.lang.String, java.lang.String, boolean, boolean):void */
    public void a(a aVar) {
        if (aVar.qq) {
            l.j("不能重复安装同一个应用程序", 0);
            if (!this.qm) {
                h.c(h.c(AbstractPaymentManager.Payment.MSG_PAYMENT_FAIL, this));
                return;
            }
            return;
        }
        this.qi = aVar;
        this.qk = 1;
        cs(this.qk);
        h.c(h.c(l.MSG_SYSTEM_LOG_EVENT, new String[]{"StartInstallApp", l.gb() + "=" + aVar.packageName}));
        if (aVar.qp.startsWith("market:") || aVar.qp.startsWith("http:") || aVar.qp.startsWith("https:")) {
            l.bq(aVar.qp);
        } else if (aVar.qp.startsWith("download:")) {
            String str = "http:" + aVar.qp.substring("download:".length());
            m.a((String) null, "正在下载", false, true);
            try {
                new b().execute(new URL(str));
            } catch (MalformedURLException e) {
                m.ig();
                b(e);
            }
        } else {
            try {
                iO();
            } catch (Exception e2) {
                b(e2);
            }
        }
    }

    public void b(Exception exc) {
        this.qk = 3;
        cs(this.qk);
        if (exc != null) {
            Log.w(getName(), "Invalid package:" + this.qi.packageName + exc);
        }
        if (!this.qm) {
            h.c(h.c(AbstractPaymentManager.Payment.MSG_PAYMENT_FAIL, this));
        }
    }

    public boolean b(Message message) {
        if (message.what == 61697) {
            ((AbstractPaymentManager) message.obj).a(this);
        }
        if (message.what != 40961 || this.qk != 1) {
            return false;
        }
        b((Exception) null);
        return false;
    }

    public void bu(String str) {
        this.qh = new ArrayList<>();
        this.qn = l.getActivity().getFilesDir().getAbsolutePath() + File.separator;
        this.qo = this.qn + fileName;
        this.pS = new com.a.a.s.b(str);
        String bC = this.pS.bC("DOWNLOAD");
        if (bC != null) {
            String[] split = bC.split("\\;");
            this.qg = new a[split.length];
            for (int i = 0; i < split.length; i++) {
                this.qg[i] = new a();
                this.qg[i].qp = split[i];
                Log.d(getName(), "app[" + i + "] target is " + this.qg[i].qp);
            }
        } else {
            Log.e(getName(), "No app target url !!!!!!!");
        }
        String bC2 = this.pS.bC("PACKAGE");
        if (bC2 != null) {
            String[] split2 = bC2.split("\\;");
            for (int i2 = 0; i2 < split2.length; i2++) {
                this.qg[i2].packageName = split2[i2];
                this.qg[i2].qq = k.cj(0).getSharedPreferences().getBoolean(this.qg[i2].packageName, false);
                Log.d(getName(), "app[" + i2 + "] packageName is " + this.qg[i2].packageName + " and install state is " + this.qg[i2].qq);
            }
        }
        String bC3 = this.pS.bC("IMAGE");
        if (bC3 != null) {
            String[] split3 = bC3.split("\\;");
            for (int i3 = 0; i3 < split3.length; i3++) {
                this.qg[i3].qr = split3[i3];
                Log.d(getName(), "app[" + i3 + "] image is " + this.qg[i3].qr);
            }
        }
        String bC4 = this.pS.bC("THUMB");
        if (bC4 != null) {
            String[] split4 = bC4.split("\\;");
            for (int i4 = 0; i4 < split4.length; i4++) {
                this.qg[i4].qs = split4[i4];
                Log.d(getName(), "app[" + i4 + "] thumb is " + this.qg[i4].qs);
            }
        }
        String bC5 = this.pS.bC("FORCELAUNCH");
        if (bC5 != null) {
            this.qj = Boolean.parseBoolean(bC5);
        }
        String bC6 = this.pS.bC("SKIPCHECK");
        if (bC6 != null) {
            this.ql = Boolean.parseBoolean(bC6);
        }
        for (int i5 = 0; i5 < this.qg.length; i5++) {
            if (!this.qg[i5].qq) {
                this.qh.add(this.qg[i5]);
            }
        }
        h.a(this);
        IntentFilter intentFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addDataScheme(com.umeng.common.a.c);
        l.getActivity().registerReceiver(this, intentFilter);
    }

    public void cr(int i) {
        this.qk = i;
    }

    public abstract void cs(int i);

    public List<a> iL() {
        iM();
        return this.qh;
    }

    public void iM() {
        if (this.ql) {
            Log.d(getName(), "Skip Check App is enable !!!");
            return;
        }
        for (int i = 0; i < this.qg.length; i++) {
            if (l.aI(this.qg[i].packageName)) {
                this.qh.remove(this.qg[i]);
            }
        }
        Log.d(getName(), "Available apps are " + this.qh.size());
    }

    public int iP() {
        return this.qk;
    }

    public void onDestroy() {
        l.getActivity().unregisterReceiver(this);
    }

    public void onReceive(Context context, Intent intent) {
        Log.d(getName(), "The package has installed." + intent.getDataString());
        if (this.qi != null && this.qi.packageName != null && intent.getDataString().indexOf(this.qi.packageName) != -1) {
            this.qk = 2;
            cs(this.qk);
            k.cj(0).getEditor().putBoolean(this.qi.packageName, true).commit();
            this.qi.qq = true;
            if (iL().isEmpty()) {
                h.c(h.c(AbstractPaymentManager.Payment.MSG_PAYMENT_NO_MORE, this));
            }
            h.c(h.c(AbstractPaymentManager.Payment.MSG_PAYMENT_SUCCESS, this));
            h.c(h.c(l.MSG_SYSTEM_LOG_EVENT, new String[]{"InstallAppSuccess", this.qi.packageName + "=" + l.gb()}));
            if (this.qj) {
                l.aJ(this.qi.packageName);
            }
        }
    }
}
