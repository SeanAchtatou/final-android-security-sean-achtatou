package org.meteoroid.plugin.feature;

import android.os.Message;
import com.a.a.r.b;
import org.meteoroid.core.h;
import org.meteoroid.core.i;

public abstract class AbstractOptionMenuItemFeature implements b, h.a, i.c {
    private String label = "Unknown";
    private com.a.a.s.b pS;

    public void V(String str) {
        this.label = str;
    }

    public boolean b(Message message) {
        if (message.what != 47872) {
            return false;
        }
        i.a(this);
        h.b(this);
        return false;
    }

    public String bK() {
        return this.label;
    }

    public void bu(String str) {
        this.pS = new com.a.a.s.b(str);
        String bC = this.pS.bC("LABEL");
        if (bC != null) {
            this.label = bC;
        }
        h.a(this);
    }

    public int getId() {
        return 143858403;
    }

    public String getName() {
        return bK();
    }

    public String getValue(String str) {
        return this.pS.bC(str);
    }
}
