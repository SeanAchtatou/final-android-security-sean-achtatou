package org.meteoroid.plugin.vd;

public abstract class Switcher extends AbstractButton {
    public abstract void V(int i, int i2);

    public abstract void W(int i, int i2);

    public abstract void X(int i, int i2);

    public boolean p(int i, int i2, int i3, int i4) {
        if (!this.qW.contains(i2, i3)) {
            return false;
        }
        if (i == 0) {
            V(i2, i3);
            return false;
        } else if (i == 2) {
            W(i2, i3);
            return false;
        } else if (i != 1) {
            return false;
        } else {
            X(i2, i3);
            return false;
        }
    }
}
