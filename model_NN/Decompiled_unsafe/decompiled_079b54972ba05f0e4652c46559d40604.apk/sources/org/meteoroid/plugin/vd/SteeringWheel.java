package org.meteoroid.plugin.vd;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.AttributeSet;
import com.a.a.r.c;
import com.a.a.s.e;

public class SteeringWheel extends AbstractRoundWidget {
    private static final int ANTICLOCKWISE = 2;
    private static final int CLOCKWISE = 1;
    private static final int LEFT = 0;
    private static final int RIGHT = 1;
    private static final int UNKNOWN = 0;
    private static final VirtualKey[] rJ = new VirtualKey[2];
    private final Matrix ht = new Matrix();
    private int orientation = 0;
    private float rK = -1.0f;
    private float rL = 0.0f;
    public int rM = 135;
    public int rN = 5;

    private final float b(float f, float f2) {
        float f3 = f2 - f;
        if (this.orientation == 0) {
            if (Math.abs(f3) > 180.0f) {
                if (f3 > 0.0f) {
                    this.orientation = 1;
                } else if (f3 < 0.0f) {
                    this.orientation = 2;
                }
            } else if (f2 > f) {
                this.orientation = 2;
            } else if (f2 < f) {
                this.orientation = 1;
            }
        }
        return (this.orientation != 1 || f3 <= 0.0f) ? (this.orientation != 2 || f3 >= 0.0f) ? f3 : f3 + 360.0f : f3 - 360.0f;
    }

    public void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.rM = attributeSet.getAttributeIntValue(str, "angleMax", 135);
        this.rN = attributeSet.getAttributeIntValue(str, "angleMin", 5);
        this.qY = e.bJ(attributeSet.getAttributeValue(str, "bitmap"));
    }

    public void a(c cVar) {
        super.a(cVar);
        rJ[0] = new VirtualKey();
        rJ[0].rP = "LEFT";
        rJ[1] = new VirtualKey();
        rJ[1].rP = "RIGHT";
        reset();
    }

    public String getName() {
        return "SteeringWheel";
    }

    public boolean hj() {
        return true;
    }

    public void onDraw(Canvas canvas) {
        if (this.delay > 0) {
            this.delay--;
            return;
        }
        if (this.state == 0) {
            this.rb = 0;
            this.hZ = true;
        }
        if (this.hZ) {
            if (this.ra > 0 && this.state == 1) {
                this.rb++;
                this.fV.setAlpha(255 - ((this.rb * 255) / this.ra));
                if (this.rb >= this.ra) {
                    this.rb = 0;
                    this.hZ = false;
                }
            }
            if (this.state == 1) {
                if (this.rL > 0.0f) {
                    this.rL -= 15.0f;
                    this.rL = this.rL < 0.0f ? 0.0f : this.rL;
                } else if (this.rL < 0.0f) {
                    this.rL += 15.0f;
                    this.rL = this.rL > 0.0f ? 0.0f : this.rL;
                }
            }
            if (this.qY != null) {
                canvas.save();
                if (this.rL != 0.0f) {
                    this.ht.setRotate(this.rL * -1.0f, (float) (this.qY[this.state].getWidth() >> 1), (float) (this.qY[this.state].getHeight() >> 1));
                }
                canvas.translate((float) (this.centerX - (this.qY[this.state].getWidth() >> 1)), (float) (this.centerY - (this.qY[this.state].getHeight() >> 1)));
                canvas.drawBitmap(this.qY[this.state], this.ht, null);
                canvas.restore();
                this.ht.reset();
            }
        }
    }

    public final boolean p(int i, int i2, int i3, int i4) {
        double sqrt = Math.sqrt(Math.pow((double) (i2 - this.centerX), 2.0d) + Math.pow((double) (i3 - this.centerY), 2.0d));
        VirtualKey virtualKey = null;
        Thread.yield();
        if (sqrt > ((double) this.re) || sqrt < ((double) this.rf)) {
            if (i == 1 && this.id == i4) {
                release();
            }
            return false;
        }
        switch (i) {
            case 0:
                if (this.rK == -1.0f) {
                    this.rK = a((float) i2, (float) i3);
                }
                this.id = i4;
                break;
            case 1:
                if (this.id == i4) {
                    release();
                    break;
                }
                break;
            case 2:
                if (this.id == i4) {
                    this.state = 0;
                    this.rL = b(this.rK, a((float) i2, (float) i3));
                    if (this.rL > ((float) this.rM)) {
                        this.rL = (float) this.rM;
                    } else if (this.rL < ((float) (this.rM * -1))) {
                        this.rL = (float) (this.rM * -1);
                    }
                    if (this.rL >= ((float) this.rN) && this.rL <= ((float) this.rM)) {
                        virtualKey = rJ[0];
                    } else if (this.rL > ((float) (this.rN * -1)) || this.rL < ((float) (this.rM * -1))) {
                        if (this.rL < ((float) this.rN) && this.rL > ((float) (this.rN * -1))) {
                            this.orientation = 0;
                            break;
                        }
                    } else {
                        virtualKey = rJ[1];
                    }
                    if (this.rg != virtualKey) {
                        if (this.rg != null && this.rg.state == 0) {
                            this.rg.state = 1;
                            VirtualKey.b(this.rg);
                        }
                        this.rg = virtualKey;
                    }
                    if (this.rg != null) {
                        this.rg.state = 0;
                        VirtualKey.b(this.rg);
                        break;
                    }
                }
                break;
        }
        return true;
    }

    public final void reset() {
        this.state = 1;
        this.rK = -1.0f;
        this.orientation = 0;
    }
}
