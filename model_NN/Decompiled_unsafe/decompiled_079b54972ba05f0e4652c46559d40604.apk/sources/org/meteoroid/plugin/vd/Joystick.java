package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import com.a.a.r.c;
import com.a.a.s.e;

public class Joystick extends AbstractRoundWidget {
    public static final int EIGHT_DIR_CONTROL = 8;
    public static final int EIGHT_DIR_CONTROL_NUM = 10;
    public static final int FOUR_DIR_CONTROL = 4;
    public static final int FOUR_DIR_CONTROL_NUM = 6;
    private static final VirtualKey[] rh = new VirtualKey[5];
    private static final VirtualKey[] rr = new VirtualKey[9];
    private boolean hZ = true;
    int mode;
    int rs;
    int rt;
    Bitmap[] ru;
    Bitmap[] rv;

    /* access modifiers changed from: package-private */
    public VirtualKey a(float f, int i) {
        return i == 8 ? rr[(int) ((((double) f) + 22.5d) / 45.0d)] : rh[(int) ((45.0f + f) / 90.0f)];
    }

    public void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.ru = e.bJ(attributeSet.getAttributeValue(str, "stick"));
        this.rv = e.bJ(attributeSet.getAttributeValue(str, "base"));
        this.mode = attributeSet.getAttributeIntValue(str, "mode", 4);
    }

    public void a(c cVar) {
        super.a(cVar);
        if (this.mode == 8) {
            rr[0] = new VirtualKey();
            rr[0].rP = "RIGHT";
            rr[1] = new VirtualKey();
            rr[1].rP = "NUM_3";
            rr[2] = new VirtualKey();
            rr[2].rP = "UP";
            rr[3] = new VirtualKey();
            rr[3].rP = "NUM_1";
            rr[4] = new VirtualKey();
            rr[4].rP = "LEFT";
            rr[5] = new VirtualKey();
            rr[5].rP = "NUM_7";
            rr[6] = new VirtualKey();
            rr[6].rP = "DOWN";
            rr[7] = new VirtualKey();
            rr[7].rP = "NUM_9";
            rr[8] = rr[0];
        } else if (this.mode == 10) {
            rr[0] = new VirtualKey();
            rr[0].rP = "NUM_6";
            rr[1] = new VirtualKey();
            rr[1].rP = "NUM_3";
            rr[2] = new VirtualKey();
            rr[2].rP = "NUM_2";
            rr[3] = new VirtualKey();
            rr[3].rP = "NUM_1";
            rr[4] = new VirtualKey();
            rr[4].rP = "NUM_4";
            rr[5] = new VirtualKey();
            rr[5].rP = "NUM_7";
            rr[6] = new VirtualKey();
            rr[6].rP = "NUM_8";
            rr[7] = new VirtualKey();
            rr[7].rP = "NUM_9";
            rr[8] = rr[0];
        } else if (this.mode == 6) {
            rh[0] = new VirtualKey();
            rh[0].rP = "NUM_6";
            rh[1] = new VirtualKey();
            rh[1].rP = "NUM_2";
            rh[2] = new VirtualKey();
            rh[2].rP = "NUM_4";
            rh[3] = new VirtualKey();
            rh[3].rP = "NUM_8";
            rh[4] = rh[0];
        } else {
            rh[0] = new VirtualKey();
            rh[0].rP = "RIGHT";
            rh[1] = new VirtualKey();
            rh[1].rP = "UP";
            rh[2] = new VirtualKey();
            rh[2].rP = "LEFT";
            rh[3] = new VirtualKey();
            rh[3].rP = "DOWN";
            rh[4] = rh[0];
        }
        reset();
    }

    public String getName() {
        return "Joystick";
    }

    public boolean iw() {
        return (this.rv == null && this.ru == null) ? false : true;
    }

    public void onDraw(Canvas canvas) {
        Paint paint = null;
        if (this.delay > 0) {
            this.delay--;
            return;
        }
        if (this.state == 0) {
            this.rb = 0;
            this.hZ = true;
        }
        if (this.hZ) {
            if (this.ra > 0 && this.state == 1) {
                this.rb++;
                this.fV.setAlpha(255 - ((this.rb * 255) / this.ra));
                if (this.rb >= this.ra) {
                    this.rb = 0;
                    this.hZ = false;
                }
            }
            if (a(this.rv)) {
                canvas.drawBitmap(this.rv[this.state], (float) (this.centerX - (this.rv[this.state].getWidth() / 2)), (float) (this.centerY - (this.rv[this.state].getHeight() / 2)), (this.ra == -1 || this.state != 1) ? null : this.fV);
            }
            if (a(this.ru)) {
                Bitmap bitmap = this.ru[this.state];
                float width = (float) (this.rs - (this.ru[this.state].getWidth() / 2));
                float height = (float) (this.rt - (this.ru[this.state].getHeight() / 2));
                if (this.ra != -1 && this.state == 1) {
                    paint = this.fV;
                }
                canvas.drawBitmap(bitmap, width, height, paint);
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public boolean p(int i, int i2, int i3, int i4) {
        double sqrt = Math.sqrt(Math.pow((double) (i2 - this.centerX), 2.0d) + Math.pow((double) (i3 - this.centerY), 2.0d));
        Thread.yield();
        if (sqrt > ((double) this.re) || sqrt < ((double) this.rf)) {
            if (i == 1 && this.id == i4) {
                release();
            }
            return false;
        }
        switch (i) {
            case 0:
                this.id = i4;
                break;
            case 1:
                if (this.id != i4) {
                    return true;
                }
                release();
                return true;
            case 2:
                break;
            default:
                return true;
        }
        if (this.id != i4) {
            return true;
        }
        this.state = 0;
        this.rs = i2;
        this.rt = i3;
        VirtualKey a = a(a((float) i2, (float) i3), this.mode);
        if (this.rg != a) {
            if (this.rg != null && this.rg.state == 0) {
                this.rg.state = 1;
                VirtualKey.b(this.rg);
            }
            this.rg = a;
        }
        this.rg.state = 0;
        VirtualKey.b(this.rg);
        return true;
    }

    public final void reset() {
        this.rs = this.centerX;
        this.rt = this.centerY;
        this.state = 1;
    }

    public void setVisible(boolean z) {
        this.hZ = z;
    }
}
