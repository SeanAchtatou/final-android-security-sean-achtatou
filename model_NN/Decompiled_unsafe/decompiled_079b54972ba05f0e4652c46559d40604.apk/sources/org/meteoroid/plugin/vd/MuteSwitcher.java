package org.meteoroid.plugin.vd;

import org.meteoroid.core.g;

public final class MuteSwitcher extends BooleanSwitcher {
    public String getName() {
        return "MuteSwitcher";
    }

    public void jk() {
        g.v(true);
    }

    public void jl() {
        g.v(false);
    }
}
