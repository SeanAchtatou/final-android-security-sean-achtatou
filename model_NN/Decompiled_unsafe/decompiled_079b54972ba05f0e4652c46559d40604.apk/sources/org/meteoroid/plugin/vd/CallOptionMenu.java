package org.meteoroid.plugin.vd;

import org.meteoroid.core.l;

public class CallOptionMenu extends SimpleButton {
    private boolean jW;

    public String getName() {
        return "CallOptionMenu";
    }

    public void onClick() {
        if (this.jW) {
            l.getActivity().closeOptionsMenu();
        } else {
            l.getActivity().openOptionsMenu();
        }
        this.jW = !this.jW;
    }
}
