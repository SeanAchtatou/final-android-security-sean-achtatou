package org.meteoroid.plugin.vd;

import android.util.AttributeSet;
import org.meteoroid.core.h;

public class VirtualKey extends AbstractButton {
    public static final int MSG_VIRTUAL_KEY_EVENT = 7833601;
    public String rP;
    private boolean rQ = true;

    public static final synchronized void b(VirtualKey virtualKey) {
        synchronized (VirtualKey.class) {
            h.c(h.c(MSG_VIRTUAL_KEY_EVENT, virtualKey));
        }
    }

    public void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.rP = attributeSet.getAttributeValue(str, "keyname");
        this.rQ = attributeSet.getAttributeBooleanValue(str, "clickThrough", true);
    }

    public String getName() {
        return jv();
    }

    public String jv() {
        return this.rP;
    }

    public boolean p(int i, int i2, int i3, int i4) {
        if (this.qW.contains(i2, i3)) {
            switch (i) {
                case 0:
                case 2:
                    this.id = i4;
                    this.state = 0;
                    b(this);
                    break;
                case 1:
                    this.id = -1;
                    this.state = 1;
                    b(this);
                    break;
            }
            if (!this.rQ) {
                return true;
            }
        } else if (this.state == 0 && this.id == i4) {
            this.id = -1;
            this.state = 1;
            b(this);
        }
        return false;
    }
}
