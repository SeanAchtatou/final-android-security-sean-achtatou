package org.meteoroid.plugin.vd;

import org.meteoroid.core.h;
import org.meteoroid.core.l;

public class CheckinButton extends SimpleButton {
    public String getName() {
        return "Checkin";
    }

    public void onClick() {
        h.d(l.MSG_SYSTEM_LOG_EVENT, new String[]{"Checkin", l.gb()});
        h.ci(CommandButton.MSG_CHECKIN);
    }
}
