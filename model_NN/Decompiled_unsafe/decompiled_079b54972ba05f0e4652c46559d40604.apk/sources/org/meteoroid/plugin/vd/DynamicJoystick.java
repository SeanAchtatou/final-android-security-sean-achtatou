package org.meteoroid.plugin.vd;

import android.graphics.Canvas;
import android.util.AttributeSet;

public class DynamicJoystick extends Joystick {
    private int rp;
    private int rq;

    public void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.rp = this.centerX;
        this.rq = this.centerY;
    }

    public void onDraw(Canvas canvas) {
        if (this.state != 1) {
            super.onDraw(canvas);
        }
    }

    public boolean p(int i, int i2, int i3, int i4) {
        if (this.state == 1 && i == 0 && Math.sqrt(Math.pow((double) (i2 - this.rp), 2.0d) + Math.pow((double) (i3 - this.rq), 2.0d)) <= ((double) this.re)) {
            this.id = i4;
            this.centerX = i2;
            this.centerY = i3;
            this.state = 0;
        }
        double sqrt = Math.sqrt(Math.pow((double) (i2 - this.centerX), 2.0d) + Math.pow((double) (i3 - this.centerY), 2.0d));
        Thread.yield();
        if (sqrt > ((double) this.re) || sqrt < ((double) this.rf)) {
            if (i == 1 && this.id == i4) {
                release();
            }
            return false;
        }
        switch (i) {
            case 1:
                if (this.id != i4) {
                    return true;
                }
                release();
                return true;
            case 2:
                if (this.id != i4) {
                    return true;
                }
                this.state = 0;
                this.rs = i2;
                this.rt = i3;
                VirtualKey a = a(a((float) i2, (float) i3), this.mode);
                if (this.rg != a) {
                    if (this.rg != null && this.rg.state == 0) {
                        this.rg.state = 1;
                        VirtualKey.b(this.rg);
                    }
                    this.rg = a;
                }
                this.rg.state = 0;
                VirtualKey.b(this.rg);
                return true;
            default:
                return true;
        }
    }
}
