package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet;
import com.a.a.s.e;

public final class IntellegenceBackground extends Background {
    public static final int SCREEN_BOTTOM = 1;
    public static final int SCREEN_LEFT = 2;
    public static final int SCREEN_RIGHT = 3;
    public static final int SCREEN_TOP = 0;
    int mode;

    public void a(AttributeSet attributeSet, String str) {
        this.rect = e.bH(attributeSet.getAttributeValue(str, "rect"));
        this.mode = attributeSet.getAttributeIntValue(str, "mode", 0);
    }

    public String getName() {
        return "IntellegenceBackground";
    }

    public void onDraw(Canvas canvas) {
        Bitmap iy;
        if (!(jh() == null || (iy = ((DefaultVirtualDevice) jh()).jo().iy()) == null)) {
            int i = -16777216;
            int width = iy.getWidth();
            int height = iy.getHeight();
            switch (this.mode) {
                case 0:
                    i = iy.getPixel(width >> 1, 0);
                    break;
                case 1:
                    i = iy.getPixel(width >> 1, height - 1);
                    break;
                case 2:
                    i = iy.getPixel(0, height >> 1);
                    break;
                case 3:
                    i = iy.getPixel(width - 1, height >> 1);
                    break;
            }
            if (this.rect != null) {
                this.color = i;
            }
        }
        super.onDraw(canvas);
    }
}
