package org.meteoroid.plugin.vd;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import com.a.a.r.c;
import com.a.a.s.e;

public class ArcadeJoyStick extends AbstractRoundWidget {
    public static final int CENTER = 0;
    public static final int DOWN = 2;
    public static final int EIGHT_DIR_CONTROL = 8;
    public static final int FOUR_DIR_CONTROL = 4;
    public static final int LEFT = 3;
    public static final int RIGHT = 4;
    public static final int UP = 1;
    private static final VirtualKey[] rh = new VirtualKey[5];
    private boolean hZ = true;

    private final VirtualKey i(float f) {
        return rh[(int) ((45.0f + f) / 90.0f)];
    }

    private final int j(float f) {
        if (f >= 45.0f && f < 135.0f) {
            return 1;
        }
        if (f >= 135.0f && f < 225.0f) {
            return 3;
        }
        if (f < 225.0f || f >= 315.0f) {
            return (f >= 315.0f || f < 45.0f) ? 4 : 0;
        }
        return 2;
    }

    public void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.qY = e.bJ(attributeSet.getAttributeValue(str, "bitmap"));
    }

    public void a(c cVar) {
        super.a(cVar);
        rh[0] = new VirtualKey();
        rh[0].rP = "RIGHT";
        rh[1] = new VirtualKey();
        rh[1].rP = "UP";
        rh[2] = new VirtualKey();
        rh[2].rP = "LEFT";
        rh[3] = new VirtualKey();
        rh[3].rP = "DOWN";
        rh[4] = rh[0];
        reset();
    }

    public String getName() {
        return "ArcadeJoystick";
    }

    public boolean iw() {
        return this.qY != null;
    }

    public void onDraw(Canvas canvas) {
        if (this.delay > 0) {
            this.delay--;
            return;
        }
        if (this.state == 0) {
            this.rb = 0;
            this.hZ = true;
        }
        if (this.hZ) {
            if (this.ra > 0 && this.state == 1) {
                this.rb++;
                this.fV.setAlpha(255 - ((this.rb * 255) / this.ra));
                if (this.rb >= this.ra) {
                    this.rb = 0;
                    this.hZ = false;
                }
            }
            if (a(this.qY)) {
                canvas.drawBitmap(this.qY[this.state], (float) (this.centerX - (this.qY[this.state].getWidth() / 2)), (float) (this.centerY - (this.qY[this.state].getHeight() / 2)), (Paint) null);
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final boolean p(int i, int i2, int i3, int i4) {
        double sqrt = Math.sqrt(Math.pow((double) (i2 - this.centerX), 2.0d) + Math.pow((double) (i3 - this.centerY), 2.0d));
        Thread.yield();
        if (sqrt > ((double) this.re) || sqrt < ((double) this.rf)) {
            if (i == 1 && this.id == i4) {
                release();
            }
            return false;
        }
        switch (i) {
            case 0:
                this.id = i4;
                break;
            case 1:
                if (this.id != i4) {
                    return true;
                }
                release();
                return true;
            case 2:
                break;
            default:
                return true;
        }
        if (this.id != i4) {
            return true;
        }
        float a = a((float) i2, (float) i3);
        this.state = j(a);
        VirtualKey i5 = i(a);
        if (this.rg != i5) {
            if (this.rg != null && this.rg.state == 0) {
                this.rg.state = 1;
                VirtualKey.b(this.rg);
            }
            this.rg = i5;
        }
        this.rg.state = 0;
        VirtualKey.b(this.rg);
        return true;
    }

    public final void reset() {
        this.state = 0;
    }

    public void setVisible(boolean z) {
        this.hZ = z;
    }
}
