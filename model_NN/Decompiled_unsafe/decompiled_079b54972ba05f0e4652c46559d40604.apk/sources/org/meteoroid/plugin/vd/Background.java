package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import com.a.a.r.c;
import com.a.a.s.e;

public class Background implements c.a {
    int color = -16777216;
    private boolean hZ = true;
    private c od;
    private Paint pb;
    Rect rect;
    Bitmap ri;

    public void a(AttributeSet attributeSet, String str) {
        this.rect = e.bH(attributeSet.getAttributeValue(str, "rect"));
        String attributeValue = attributeSet.getAttributeValue(str, "bitmap");
        if (attributeValue != null) {
            this.ri = e.bI(attributeValue);
        } else {
            this.color = Integer.valueOf(attributeSet.getAttributeValue(str, "color"), 16).intValue();
        }
    }

    public void a(c cVar) {
        this.od = cVar;
        if (this.ri == null) {
            this.pb = new Paint();
            this.pb.setAntiAlias(true);
            this.pb.setStyle(Paint.Style.FILL);
        }
    }

    public String getName() {
        return "Background";
    }

    public boolean hj() {
        return true;
    }

    public boolean isTouchable() {
        return false;
    }

    public boolean iw() {
        return true;
    }

    public c jh() {
        return this.od;
    }

    public boolean o(int i, int i2, int i3, int i4) {
        return false;
    }

    public void onDraw(Canvas canvas) {
        if (this.hZ) {
            if (this.ri == null) {
                this.pb.setColor(this.color);
                canvas.drawRect(this.rect, this.pb);
                return;
            }
            canvas.drawBitmap(this.ri, (Rect) null, this.rect, (Paint) null);
        }
    }

    public void setVisible(boolean z) {
        this.hZ = z;
    }
}
