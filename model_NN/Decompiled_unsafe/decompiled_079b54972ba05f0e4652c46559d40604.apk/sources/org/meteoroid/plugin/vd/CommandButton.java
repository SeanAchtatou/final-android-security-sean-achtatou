package org.meteoroid.plugin.vd;

import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import me.gall.sgp.sdk.entity.app.StructuredData;
import org.meteoroid.core.h;
import org.meteoroid.core.i;
import org.meteoroid.core.l;
import org.meteoroid.plugin.device.MIDPDevice;
import org.meteoroid.plugin.feature.AbstractAdvertisement;
import org.meteoroid.plugin.feature.AbstractDownloadAndInstall;

public final class CommandButton extends SimpleButton {
    public static final int MSG_APPLOTTERY = 74710;
    public static final int MSG_CHECKIN = -2004318072;
    public static final int MSG_DISABLE_AD = 9520139;
    public static final int MSG_REQUEST_RELOAD_SKIN = 952848;
    public static final int MSG_SOCIAL_PLATFORM_LAUNCH = 1013249;
    public static final int MSG_TIME_UP = 9520138;
    private String rj;

    private String bA(String str) {
        int indexOf;
        int indexOf2 = str.indexOf(40);
        if (indexOf2 == -1 || (indexOf = str.indexOf(41, indexOf2)) == -1) {
            return null;
        }
        return str.substring(indexOf2 + 1, indexOf);
    }

    public void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.rj = attributeSet.getAttributeValue(str, StructuredData.TYPE_OF_VALUE).trim();
        h.h(MSG_APPLOTTERY, "APPLOTTERY");
        h.h(MSG_CHECKIN, "CHECKIN");
        h.h(MSG_SOCIAL_PLATFORM_LAUNCH, "MSG_SOCIAL_PLATFORM_LAUNCH");
        h.h(MSG_REQUEST_RELOAD_SKIN, "MSG_REQUEST_RELOAD_SKIN");
        h.h(MSG_TIME_UP, "MSG_TIME_UP");
        h.h(MSG_DISABLE_AD, "MSG_DISABLE_AD");
    }

    public String getName() {
        return "CommandButton";
    }

    public void onClick() {
        if (this.rj.startsWith("CMD_EXIT")) {
            l.hF();
        } else if (this.rj.equalsIgnoreCase("CMD_ABOUT")) {
            h.ci(i.MSG_OPTIONMENU_ABOUT);
        } else if (this.rj.equalsIgnoreCase("CMD_MENU")) {
            l.getHandler().post(new Runnable() {
                public void run() {
                    l.getActivity().openOptionsMenu();
                }
            });
        } else if (this.rj.startsWith("CMD_CHECKIN")) {
            h.ci(MSG_CHECKIN);
        } else if (this.rj.startsWith("CMD_APPLOTTERY")) {
            h.ci(MSG_APPLOTTERY);
        } else if (this.rj.startsWith("CMD_SNS")) {
            h.ci(MSG_SOCIAL_PLATFORM_LAUNCH);
        } else if (this.rj.startsWith("CMD_RELOADSKIN")) {
            h.ci(MSG_REQUEST_RELOAD_SKIN);
        } else if (this.rj.startsWith("CMD_TIMEUP")) {
            h.ci(MSG_TIME_UP);
        } else if (this.rj.startsWith("CMD_INSTALLAPP")) {
            AnonymousClass2 r1 = new AbstractDownloadAndInstall() {
                public String bK() {
                    return getName();
                }

                public void cs(int i) {
                }

                public String getName() {
                    return "AbstractDownloadAndInstall";
                }

                public void iV() {
                }
            };
            r1.bu(bA(this.rj));
            r1.qm = true;
            if (r1.iL().isEmpty()) {
                l.j("您已经安装了该应用", 0);
            } else {
                r1.a(r1.iL().get(0));
            }
        } else if (!this.rj.startsWith("CMD_DISABLE_AD")) {
            Log.w(getName(), "NULL command:" + this.rj);
        } else if (AbstractAdvertisement.ab(l.getActivity())) {
            Log.d(getName(), "Already disabled.");
            l.j("广告功能已经关闭", 0);
        } else {
            h.a(new h.a() {
                public boolean b(Message message) {
                    if (message.what != 61698) {
                        return false;
                    }
                    h.ci(CommandButton.MSG_DISABLE_AD);
                    l.j("广告功能关闭成功", 0);
                    return true;
                }
            });
            h.ci(MIDPDevice.j.MSG_GCF_SMS_CONNECTION_SEND);
        }
    }
}
