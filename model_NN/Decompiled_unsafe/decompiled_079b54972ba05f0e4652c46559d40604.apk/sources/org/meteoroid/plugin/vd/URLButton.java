package org.meteoroid.plugin.vd;

import android.util.AttributeSet;
import me.gall.sgp.sdk.entity.app.StructuredData;
import me.gall.sgp.sdk.service.BossService;
import org.meteoroid.core.h;
import org.meteoroid.core.l;

public final class URLButton extends SimpleButton {
    private int index;
    public String[] rO;

    public void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        this.rO = attributeSet.getAttributeValue(str, StructuredData.TYPE_OF_VALUE).split(BossService.ID_SEPARATOR);
    }

    public String getName() {
        return "URLButton";
    }

    public void onClick() {
        if (this.rO != null && this.rO[this.index] != null) {
            h.d(l.MSG_SYSTEM_LOG_EVENT, new String[]{"URLClick", l.gb() + "=" + this.rO[this.index]});
            l.bq(this.rO[this.index]);
        }
    }
}
