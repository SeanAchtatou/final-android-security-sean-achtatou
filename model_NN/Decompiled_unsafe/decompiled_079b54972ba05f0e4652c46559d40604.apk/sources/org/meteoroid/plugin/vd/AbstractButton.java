package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import com.a.a.r.c;
import com.a.a.s.e;
import me.gall.sgp.sdk.service.BossService;

public abstract class AbstractButton implements c.a {
    public static final int PRESSED = 0;
    public static final int RELEASED = 1;
    public static final int UNAVAILABLE_POINTER_ID = -1;
    private int count;
    int delay;
    Paint fV = new Paint();
    boolean hZ = true;
    int id = -1;
    private c od;
    int pW;
    Rect qW;
    Rect qX;
    Bitmap[] qY;
    boolean qZ;
    int ra;
    int rb;
    boolean rc = true;
    private int rd;
    int state = 1;

    public void a(AttributeSet attributeSet, String str) {
        this.qY = e.bJ(attributeSet.getAttributeValue(str, "bitmap"));
        String attributeValue = attributeSet.getAttributeValue(str, "touch");
        if (attributeValue != null) {
            this.qW = e.bH(attributeValue);
        }
        String attributeValue2 = attributeSet.getAttributeValue(str, "rect");
        if (attributeValue2 != null) {
            this.qX = e.bH(attributeValue2);
        }
        String attributeValue3 = attributeSet.getAttributeValue(str, "fade");
        if (attributeValue3 != null) {
            String[] split = attributeValue3.split(BossService.ID_SEPARATOR);
            if (split.length >= 1) {
                this.ra = Integer.parseInt(split[0]);
            } else {
                this.ra = -1;
            }
            if (split.length >= 2) {
                this.delay = Integer.parseInt(split[1]);
            }
        }
        if (this.qY != null && this.qY.length > 2) {
            this.qZ = true;
            this.pW = attributeSet.getAttributeIntValue(str, "interval", 40);
        }
    }

    public void a(c cVar) {
        this.od = cVar;
        if (this.qW == null) {
            this.qW = this.qX;
        }
    }

    public boolean a(Bitmap[] bitmapArr) {
        return bitmapArr != null && this.state >= 0 && this.state <= bitmapArr.length + -1 && bitmapArr[this.state] != null;
    }

    public int getKeyState() {
        return this.state;
    }

    public boolean hj() {
        return this.rc;
    }

    public boolean isTouchable() {
        return true;
    }

    public boolean iw() {
        return this.qY != null;
    }

    public c jh() {
        return this.od;
    }

    public boolean o(int i, int i2, int i3, int i4) {
        return p(i, i2, i3, i4);
    }

    public void onDraw(Canvas canvas) {
        if (this.qZ) {
            this.count++;
            if (this.count >= this.pW) {
                this.count = 0;
                this.rd++;
                if (this.qY != null && this.rd >= this.qY.length) {
                    this.rd = 0;
                }
            }
        } else if (this.delay > 0) {
            this.delay--;
            return;
        } else {
            if (this.state == 0) {
                this.rb = 0;
                this.hZ = true;
            }
            if (this.hZ) {
                if (this.ra > 0 && this.state == 1) {
                    this.rb++;
                    this.fV.setAlpha(255 - ((this.rb * 255) / this.ra));
                    if (this.rb >= this.ra) {
                        this.rb = 0;
                        this.hZ = false;
                    }
                }
                this.rd = this.state;
            } else {
                return;
            }
        }
        if (a(this.qY)) {
            canvas.drawBitmap(this.qY[this.rd], (Rect) null, this.qX, (this.ra == -1 || this.state != 1) ? null : this.fV);
        }
    }

    public abstract boolean p(int i, int i2, int i3, int i4);

    public void setVisible(boolean z) {
        this.hZ = z;
    }
}
