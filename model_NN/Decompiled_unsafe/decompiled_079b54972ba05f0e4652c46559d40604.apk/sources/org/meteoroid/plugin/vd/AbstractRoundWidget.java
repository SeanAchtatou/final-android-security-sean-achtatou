package org.meteoroid.plugin.vd;

import android.util.AttributeSet;
import me.gall.sgp.sdk.service.BossService;
import org.meteoroid.core.l;

public abstract class AbstractRoundWidget extends AbstractButton {
    int centerX;
    int centerY;
    int re;
    int rf;
    VirtualKey rg;

    public final float a(float f, float f2) {
        float f3 = f - ((float) this.centerX);
        float f4 = f2 - ((float) this.centerY);
        double degrees = Math.toDegrees(Math.atan((double) (f4 / f3)));
        if (f3 >= 0.0f && f4 < 0.0f) {
            degrees = Math.abs(degrees);
        } else if (f3 < 0.0f && f4 <= 0.0f) {
            degrees = 180.0d - degrees;
        } else if (f3 <= 0.0f && f4 > 0.0f) {
            degrees = Math.abs(degrees) + 180.0d;
        } else if (f3 > 0.0f && f4 >= 0.0f) {
            degrees = 360.0d - Math.abs(degrees);
        }
        return (float) degrees;
    }

    public void a(AttributeSet attributeSet, String str) {
        this.centerX = attributeSet.getAttributeIntValue(str, "x", l.gy() / 2);
        this.centerY = attributeSet.getAttributeIntValue(str, "y", l.gz() / 2);
        this.re = attributeSet.getAttributeIntValue(str, "max", 60);
        this.rf = attributeSet.getAttributeIntValue(str, "min", 5);
        String attributeValue = attributeSet.getAttributeValue(str, "fade");
        if (attributeValue != null) {
            String[] split = attributeValue.split(BossService.ID_SEPARATOR);
            if (split.length >= 1) {
                this.ra = Integer.parseInt(split[0]);
            } else {
                this.ra = -1;
            }
            if (split.length >= 2) {
                this.delay = Integer.parseInt(split[1]);
            }
        }
    }

    public boolean p(int i, int i2, int i3, int i4) {
        return false;
    }

    public void release() {
        this.id = -1;
        if (this.rg != null && this.rg.state == 0) {
            this.rg.state = 1;
            VirtualKey.b(this.rg);
        }
        reset();
    }

    public abstract void reset();
}
