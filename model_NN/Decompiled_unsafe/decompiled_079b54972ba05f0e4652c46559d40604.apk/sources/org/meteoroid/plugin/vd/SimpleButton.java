package org.meteoroid.plugin.vd;

public abstract class SimpleButton extends AbstractButton {
    public abstract void onClick();

    public boolean p(int i, int i2, int i3, int i4) {
        if (this.qW.contains(i2, i3)) {
            switch (i) {
                case 0:
                    this.id = i4;
                    this.state = 0;
                    onClick();
                    break;
                case 1:
                    this.id = -1;
                    this.state = 1;
                    break;
            }
        } else if (this.state == 0 && this.id == i4) {
            this.id = -1;
            this.state = 1;
        }
        return false;
    }
}
