package org.meteoroid.plugin.vd;

import com.a.a.r.c;
import java.util.Iterator;
import org.meteoroid.core.n;

public class HideVirtualDeviceSwitcher extends BooleanSwitcher {
    public String getName() {
        return "HideVirtualDeviceSwitcher";
    }

    public void jk() {
        DefaultVirtualDevice defaultVirtualDevice = (DefaultVirtualDevice) n.od;
        Iterator<c.a> it = defaultVirtualDevice.jn().iterator();
        while (it.hasNext()) {
            c.a next = it.next();
            if (next != defaultVirtualDevice.jo()) {
                next.setVisible(false);
            }
        }
    }

    public void jl() {
        DefaultVirtualDevice defaultVirtualDevice = (DefaultVirtualDevice) n.od;
        Iterator<c.a> it = defaultVirtualDevice.jn().iterator();
        while (it.hasNext()) {
            c.a next = it.next();
            if (next != defaultVirtualDevice.jo()) {
                next.setVisible(true);
            }
        }
    }

    public void setVisible(boolean z) {
    }
}
