package org.meteoroid.plugin.vd;

import android.util.AttributeSet;
import me.gall.sgp.sdk.entity.app.StructuredData;

public abstract class BooleanSwitcher extends Switcher {
    public static final int OFF = 1;
    public static final int ON = 0;

    private void ji() {
        if (jj()) {
            jk();
        } else {
            jl();
        }
    }

    public void B(boolean z) {
        this.state = z ? 1 : 0;
        ji();
    }

    public void V(int i, int i2) {
        int i3 = 1;
        if (this.state == 1) {
            i3 = 0;
        }
        this.state = i3;
        ji();
    }

    public void W(int i, int i2) {
    }

    public void X(int i, int i2) {
    }

    public void a(AttributeSet attributeSet, String str) {
        super.a(attributeSet, str);
        B(attributeSet.getAttributeBooleanValue(str, StructuredData.TYPE_OF_VALUE, false));
    }

    public boolean jj() {
        return this.state == 0;
    }

    public abstract void jk();

    public abstract void jl();
}
