package org.meteoroid.plugin.vd;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.util.AttributeSet;
import android.util.Log;
import com.a.a.r.c;
import com.a.a.s.e;
import me.gall.sgp.sdk.entity.app.StructuredData;
import me.gall.sgp.sdk.service.BossService;
import org.meteoroid.core.f;
import org.meteoroid.core.n;

public final class SensorSwitcher extends BooleanSwitcher implements SensorEventListener {
    private static final int DOWN = 1;
    private static final int LEFT = 2;
    private static final int RIGHT = 3;
    private static final int UP = 0;
    private static final VirtualKey[] rh = new VirtualKey[4];
    private int[] rD;
    private int rE = 2;
    private int rF = 2;
    private VirtualKey rG;
    private final int[] rH = new int[3];
    private int rI = 0;
    private int ro;
    private int x;
    private int y;
    private int z;

    public void a(AttributeSet attributeSet, String str) {
        this.qY = e.bJ(attributeSet.getAttributeValue(str, "bitmap"));
        String attributeValue = attributeSet.getAttributeValue(str, "touch");
        if (attributeValue != null) {
            this.qW = e.bH(attributeValue);
        }
        String attributeValue2 = attributeSet.getAttributeValue(str, "rect");
        if (attributeValue2 != null) {
            this.qX = e.bH(attributeValue2);
        }
        this.state = 1;
        this.ra = attributeSet.getAttributeIntValue(str, "fade", -1);
        String attributeValue3 = attributeSet.getAttributeValue(str, StructuredData.TYPE_OF_VALUE);
        if (attributeValue3 != null) {
            String[] split = attributeValue3.split(BossService.ID_SEPARATOR);
            if (split.length >= 1) {
                try {
                    if (Integer.parseInt(split[0]) == 0) {
                        jk();
                    }
                } catch (NumberFormatException e) {
                    if (Boolean.parseBoolean(split[0])) {
                        jk();
                    }
                }
            }
            if (split.length >= 3) {
                this.rE = Integer.parseInt(split[1]);
                this.rF = Integer.parseInt(split[2]);
            }
            if (split.length >= 6) {
                this.rD = new int[3];
                this.rD[0] = Integer.parseInt(split[3]);
                this.rD[1] = Integer.parseInt(split[4]);
                this.rD[2] = Integer.parseInt(split[5]);
            }
        }
    }

    public void a(c cVar) {
        super.a(cVar);
        rh[0] = new VirtualKey();
        rh[0].rP = "UP";
        rh[0].state = 1;
        rh[1] = new VirtualKey();
        rh[1].rP = "DOWN";
        rh[1].state = 1;
        rh[2] = new VirtualKey();
        rh[2].rP = "LEFT";
        rh[2].state = 1;
        rh[3] = new VirtualKey();
        rh[3].rP = "RIGHT";
        rh[3].state = 1;
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.rI = 0;
        this.rH[0] = 0;
        this.rH[1] = 0;
        this.rH[2] = 0;
        ju();
    }

    public synchronized void a(VirtualKey virtualKey) {
        if (virtualKey != this.rG) {
            ju();
        }
        if (virtualKey != null && virtualKey.state == 1) {
            virtualKey.state = 0;
            VirtualKey.b(virtualKey);
            this.rG = virtualKey;
        }
        if (virtualKey != null) {
            Log.d(getName(), "sensor vb:[" + virtualKey.rP + "|" + virtualKey.state + "]");
        }
    }

    public String getName() {
        return "SensorSwitcher";
    }

    public void jk() {
        f.a((SensorEventListener) this);
        Log.w(getName(), "Switch on.");
    }

    public void jl() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.rI = 0;
        this.rH[0] = 0;
        this.rH[1] = 0;
        this.rH[2] = 0;
        ju();
        f.b((SensorEventListener) this);
        Log.w(getName(), "Switch off.");
    }

    public void ju() {
        if (this.rG != null && this.rG.state == 0) {
            this.rG.state = 1;
            VirtualKey.b(this.rG);
            this.rG = null;
        }
    }

    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        Log.d(getName(), "sensor:[" + ((int) sensorEvent.values[0]) + "|" + ((int) sensorEvent.values[1]) + "|" + ((int) sensorEvent.values[2]) + "]");
        this.ro = n.od.getOrientation();
        if (this.ro == 1) {
            this.x = -((int) sensorEvent.values[0]);
            this.y = (int) sensorEvent.values[1];
        } else if (this.ro == 0) {
            this.x = (int) sensorEvent.values[1];
            this.y = (int) sensorEvent.values[0];
        } else {
            Log.w(getName(), "deviceOrientation:" + this.ro);
            return;
        }
        this.z = (int) sensorEvent.values[2];
        if (this.rI < 1) {
            if (this.rD != null) {
                this.rH[0] = this.rD[0];
                this.rH[1] = this.rD[1];
                this.rH[2] = this.rD[2];
            } else {
                this.rH[0] = this.x;
                this.rH[1] = this.y;
                this.rH[2] = this.z;
            }
            Log.d(getName(), "sensor_init:[" + this.rH[0] + "|" + this.rH[1] + "|" + this.rH[2] + "]");
            this.rI++;
            return;
        }
        int i = this.x - this.rH[0];
        int i2 = this.y - this.rH[1];
        int i3 = this.z - this.rH[2];
        if (Math.abs(i) < this.rE && Math.abs(i2) < this.rF && Math.abs(i3) < this.rF) {
            ju();
        } else if (Math.abs(i) >= this.rE) {
            if (i < 0) {
                a(rh[2]);
            } else {
                a(rh[3]);
            }
        } else if (Math.abs(i2) >= this.rF) {
            if (i2 > 0) {
                a(rh[1]);
            } else {
                a(rh[0]);
            }
            a(this.rG);
        } else if (Math.abs(i3) < this.rF) {
        } else {
            if (i3 > 0) {
                a(rh[0]);
            } else {
                a(rh[1]);
            }
        }
    }
}
