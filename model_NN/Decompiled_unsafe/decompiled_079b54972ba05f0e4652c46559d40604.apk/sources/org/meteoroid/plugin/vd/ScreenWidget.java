package org.meteoroid.plugin.vd;

import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import com.a.a.r.c;

public abstract class ScreenWidget implements c.a {
    private float rA;
    private boolean rB = false;
    private boolean rC;
    private final Rect rw = new Rect();
    public final Paint rx = new Paint();
    public Rect ry;
    private float rz;

    public void C(boolean z) {
        this.rx.setFilterBitmap(z);
        this.rB = true;
    }

    public abstract void a(int i, float f, float f2, int i2);

    public final void a(Rect rect) {
        this.rw.left = rect.left;
        this.rw.top = rect.top;
        this.rw.right = rect.right;
        this.rw.bottom = rect.bottom;
    }

    public boolean isTouchable() {
        return this.rC;
    }

    public boolean iw() {
        return true;
    }

    public abstract Bitmap iy();

    public Rect jq() {
        return this.rw;
    }

    public final float jr() {
        return this.rz;
    }

    public final float js() {
        return this.rA;
    }

    public final void jt() {
        if (iy() != null) {
            this.rz = ((float) this.rw.width()) / ((float) iy().getWidth());
            this.rA = ((float) this.rw.height()) / ((float) iy().getHeight());
            if (!this.rB) {
                if (this.rz == 1.0f && this.rA == 1.0f) {
                    this.rx.setFilterBitmap(false);
                } else {
                    this.rx.setFilterBitmap(true);
                }
            }
            Log.d(c.LOG_TAG, "Scale Screen to " + this.rw.width() + "x" + this.rw.height());
        }
    }

    public boolean o(int i, int i2, int i3, int i4) {
        if (!jq().contains(i2, i3) || !this.rC) {
            return false;
        }
        a(i, ((float) (i2 - jq().left)) / this.rz, ((float) (i3 - jq().top)) / this.rA, i4);
        return false;
    }

    public void setTouchable(boolean z) {
        this.rC = z;
    }
}
