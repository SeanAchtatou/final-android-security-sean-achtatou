package org.meteoroid.plugin.vd;

import android.util.Log;
import android.view.Display;
import com.a.a.r.c;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Properties;
import org.meteoroid.core.e;
import org.meteoroid.core.f;
import org.meteoroid.core.h;
import org.meteoroid.core.l;

public class DefaultVirtualDevice implements c {
    public static final int WIDGET_TYPE_ARCADE_JOYSTICK = 11;
    public static final int WIDGET_TYPE_BACKGROUND = 0;
    public static final int WIDGET_TYPE_CALL_OPTIONMENU = 12;
    public static final int WIDGET_TYPE_CHECKIN = 13;
    public static final int WIDGET_TYPE_DEVICE_SCREEN = 2;
    public static final int WIDGET_TYPE_DYNAMICJOYSTICK = 15;
    public static final int WIDGET_TYPE_EXIT_BUTTON = 6;
    public static final int WIDGET_TYPE_HIDE_VD = 10;
    public static final int WIDGET_TYPE_INTELEGENCE_BG = 7;
    public static final int WIDGET_TYPE_JOYSTICK = 3;
    public static final int WIDGET_TYPE_MUTE_SWITCHER = 4;
    public static final int WIDGET_TYPE_SENSOR_SWITCHER = 5;
    public static final int WIDGET_TYPE_SNSBUTTON = 14;
    public static final int WIDGET_TYPE_STEERINGWHEEL = 9;
    public static final int WIDGET_TYPE_URL_BUTTON = 8;
    public static final int WIDGET_TYPE_VIRTUAL_BUTTON = 1;
    public static final String[] rn = {"Background", "VirtualKey", "ScreenWidget", "Joystick", "MuteSwitcher", "SensorSwitcher", "CommandButton", "IntellegenceBackground", "URLButton", "SteeringWheel", "HideVirtualDeviceSwitcher", "ArcadeJoyStick", "CallOptionMenu", "CheckinButton", "SNSButton", "DynamicJoystick"};
    private final LinkedHashSet<c.a> rl = new LinkedHashSet<>();
    private ScreenWidget rm;
    private int ro;

    public static final String jp() {
        Display defaultDisplay = l.getActivity().getWindowManager().getDefaultDisplay();
        int width = defaultDisplay.getWidth();
        int height = defaultDisplay.getHeight();
        int min = Math.min(width, height);
        int max = Math.max(width, height);
        Log.d(c.LOG_TAG, "Screen full width is " + min + "px and height is " + max + "px.");
        if (min == 480 && max == 854) {
            return "fwvga";
        }
        if (min == 480 && max == 960) {
            return "uwvga";
        }
        if (min == 800 && max == 960) {
            return "dualwvga";
        }
        if (min == 480 && max > 640 && max <= 800) {
            return "wvga";
        }
        if (min == 360 && max == 640) {
            return "nhd";
        }
        if (min == 320 && max >= 460 && max <= 500) {
            return "hvga";
        }
        if (min == 240 && max == 320) {
            return "qvga";
        }
        if (min == 240 && max <= 400 && max > 320) {
            return "wqvga";
        }
        if (min == 480 && max == 640) {
            return "vga";
        }
        if (min == 600 && max == 800) {
            return "svga";
        }
        if (min <= 600 && min > 540 && max <= 1024 && max > 960) {
            return "wsvga";
        }
        if (min <= 768 && min > 700 && max == 1024) {
            return "xga";
        }
        if (min == 640 && max <= 960 && max >= 800) {
            return "retina";
        }
        if (min == 540 && max <= 960 && max >= 800) {
            return "qhd";
        }
        if (min <= 800 && min > 720 && max <= 1280 && max >= 1100) {
            return "wxga";
        }
        if (min <= 720 && min > 640 && max <= 1280 && max >= 1100) {
            return "720hd";
        }
        if (min <= 768 && min >= 640 && max == 1366) {
            return "hd";
        }
        if (min <= 1080 && min >= 900 && max >= 1760) {
            return "1080hd";
        }
        if (min == 320 && max == 320) {
            return "gear";
        }
        Log.w(c.LOG_TAG, "Unkown screen resolution:" + min + "x" + max);
        return null;
    }

    public void a(c.a aVar) {
        if (!this.rl.contains(aVar)) {
            aVar.a(this);
            if (aVar instanceof ScreenWidget) {
                ScreenWidget screenWidget = (ScreenWidget) aVar;
                screenWidget.jt();
                a(screenWidget);
            }
            this.rl.add(aVar);
            if (aVar.iw()) {
                e.a(aVar);
            }
            if (aVar.isTouchable()) {
                f.a(aVar);
            }
        }
    }

    public void a(Properties properties) {
        if (properties.containsKey("widget.orientation")) {
            String property = properties.getProperty("widget.orientation");
            this.ro = 2;
            if (property.equals("landscape")) {
                this.ro = 0;
                Log.d(c.LOG_TAG, "Device orientation is force to landscape.");
            } else if (property.equals("portrait")) {
                this.ro = 1;
                Log.d(c.LOG_TAG, "Device orientation is force to portrait.");
            } else if (property.equals("auto")) {
                this.ro = 4;
                Log.d(c.LOG_TAG, "Device orientation is auto change by sensor.");
            } else {
                Log.w(c.LOG_TAG, "Orientation not specificed. It will be decided by user. ");
            }
            l.cl(this.ro);
        }
        if (properties.containsKey("widget.num")) {
            int parseInt = Integer.parseInt(properties.getProperty("widget.num"));
            a aVar = new a(properties);
            int i = 1;
            while (i <= parseInt) {
                try {
                    if (properties.containsKey("widget." + i + ".type")) {
                        String property2 = properties.getProperty("widget." + i + ".type");
                        try {
                            int parseInt2 = Integer.parseInt(property2);
                            if (parseInt2 < 0 || parseInt2 >= rn.length) {
                                Log.w(c.LOG_TAG, "Unknown widget type:" + parseInt2);
                                i++;
                            } else {
                                property2 = rn[parseInt2];
                                c.a bB = bB(property2);
                                bB.a(aVar, "widget." + i + ".");
                                a(bB);
                                i++;
                            }
                        } catch (Exception e) {
                            Log.d(c.LOG_TAG, "Type is not a number. Use a string type as widget name.");
                        }
                    } else {
                        Log.d(c.LOG_TAG, "Widget " + i + " not exist! Checkout it is not missing.");
                        i++;
                    }
                } catch (Exception e2) {
                    Log.w(c.LOG_TAG, "Init widget[" + i + "] error." + e2);
                    e2.printStackTrace();
                }
            }
        }
        properties.clear();
        System.gc();
    }

    public void a(ScreenWidget screenWidget) {
        if (this.rm != null) {
            b(this.rm);
        }
        this.rm = screenWidget;
    }

    public void b(c.a aVar) {
        this.rl.remove(aVar);
        e.b(aVar);
        f.b(aVar);
    }

    public c.a bB(String str) {
        if (str.equals(rn[2])) {
            Log.d(c.LOG_TAG, "Construct the device screen widget.");
            return (ScreenWidget) org.meteoroid.core.c.mN;
        }
        Log.d(c.LOG_TAG, "Construct a [" + str + "] widget.");
        return (c.a) Class.forName("org.meteoroid.plugin.vd." + str).newInstance();
    }

    public void bs(String str) {
        onDestroy();
        com.a.a.s.e.jy();
        if (str == null) {
            onCreate();
            return;
        }
        Properties properties = new Properties();
        try {
            properties.load(l.bl(str + File.separator + "res" + File.separator + "raw" + File.separator + "vd_" + jp() + ".properties"));
        } catch (IOException e) {
            Log.e(c.LOG_TAG, "Error in reloading vd:" + str);
        }
        com.a.a.s.e.bD(str + File.separator + "res" + File.separator + "drawable-nodpi");
        a(properties);
        com.a.a.s.e.bD(null);
    }

    public int getOrientation() {
        return this.ro;
    }

    public boolean isVisible() {
        return true;
    }

    public Properties jm() {
        Properties properties = new Properties();
        properties.load(l.getActivity().getResources().openRawResource(com.a.a.s.e.bG("vd_" + jp())));
        return properties;
    }

    public LinkedHashSet<c.a> jn() {
        return this.rl;
    }

    public ScreenWidget jo() {
        return this.rm;
    }

    public void onCreate() {
        h.h(VirtualKey.MSG_VIRTUAL_KEY_EVENT, "MSG_VIRTUAL_BUTTON_EVENT");
        try {
            a(jm());
        } catch (IOException e) {
            Log.e(c.LOG_TAG, "Oooooooops, Fail to load virtual device by resolution. Maybe the resolution is odd or missing." + e);
        }
    }

    public void onDestroy() {
        Iterator<c.a> it = this.rl.iterator();
        while (it.hasNext()) {
            c.a next = it.next();
            e.b(next);
            f.b(next);
        }
        this.rl.clear();
    }
}
