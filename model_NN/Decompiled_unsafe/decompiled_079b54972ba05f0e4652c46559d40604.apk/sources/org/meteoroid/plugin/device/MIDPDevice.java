package org.meteoroid.plugin.device;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.os.Message;
import android.os.StatFs;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import com.a.a.e.o;
import com.a.a.e.p;
import com.a.a.e.z;
import com.fasterxml.jackson.core.sym.CharsToNameCanonicalizer;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Properties;
import java.util.TimerTask;
import java.util.UUID;
import java.util.Vector;
import java.util.regex.Pattern;
import javax.microedition.media.Control;
import javax.microedition.media.MediaException;
import javax.microedition.media.Player;
import javax.microedition.media.PlayerListener;
import javax.microedition.media.control.VolumeControl;
import javax.microedition.midlet.MIDlet;
import org.meteoroid.core.f;
import org.meteoroid.core.g;
import org.meteoroid.core.h;
import org.meteoroid.core.n;
import org.meteoroid.plugin.vd.ScreenWidget;
import org.meteoroid.plugin.vd.VirtualKey;
import xsyjrxgdnxnw.zy_rlw.R;

public class MIDPDevice extends ScreenWidget implements com.a.a.r.a, f.a, f.C0005f, h.a {
    public static final int ASTERISK = 11;
    public static final int AUTO_DETECT = -1;
    public static final int DOWN = 1;
    public static final int FIRE = 4;
    public static final int GAME_A = 5;
    public static final int GAME_B = 6;
    public static final int GAME_C = 7;
    public static final int GAME_D = 8;
    public static final int LEFT = 2;
    public static final int MSG_MIDP_COMMAND_EVENT = 44034;
    public static final int MSG_MIDP_DISPLAY_CALL_SERIALLY = 44035;
    public static final int MSG_MIDP_MIDLET_NOTIFYDESTROYED = 44036;
    public static final int MUTE_SWITCH = 2;
    public static final int POUND = 12;
    public static final int POWER = 0;
    public static final int RIGHT = 3;
    public static final int SENSOR_SWITCH = 1;
    public static final int SOFT1 = 9;
    public static final int SOFT2 = 10;
    public static final int UP = 0;
    public static final int URL = 8;
    private static final Properties oB = new Properties();
    public static String oL = "";
    public static final String[] ol = {"UP", "DOWN", "LEFT", "RIGHT", "SELECT", "GAME_A", "GAME_B", "GAME_C", "GAME_D", "SOFT1", "SOFT2", "ASTERISK", "POUND", "UP_LEFT", "UP_RIGHT", "DOWN_LEFT", "DOWN_RIGHT"};
    private static final HashMap<String, Integer> ot = new HashMap<>(ol.length);
    public UUID fc;
    private int height = -1;
    private e oA;
    /* access modifiers changed from: private */
    public volatile int oC = -1;
    /* access modifiers changed from: private */
    public volatile int oD = -1;
    private boolean oE = false;
    private int oF;
    private int oG;
    private int oH;
    private int oI;
    private int oJ;
    public g oK;
    public h oM;
    public boolean oN = false;
    public String oO = "";
    private boolean om;
    private boolean on;
    private boolean oo;
    private boolean op;
    private boolean oq;
    private boolean or;
    private boolean os;
    private Bitmap ou;
    private Bitmap ov;
    private Bitmap ow;
    private Canvas ox;
    private e oy;
    private e oz;
    private int width = -1;

    private final class a extends TimerTask {
        private a() {
        }

        public void run() {
            if (MIDPDevice.this.oC == -1 && MIDPDevice.this.oD != -1) {
                MIDPDevice.this.T(1, MIDPDevice.this.cm(MIDPDevice.this.oD));
                int unused = MIDPDevice.this.oD = -1;
            }
        }
    }

    public static final class b implements com.a.a.d.b {
        File file;
        String hK;
        DataInputStream oS;
        DataOutputStream oT;
        InputStream oU;
        OutputStream oV;

        private class a extends OutputStream {
            private RandomAccessFile oW;

            public a(RandomAccessFile randomAccessFile) {
                this.oW = randomAccessFile;
            }

            public void close() {
                this.oW.close();
            }

            public void flush() {
            }

            public void write(int i) {
                this.oW.writeByte(i);
            }

            public void write(byte[] bArr) {
                this.oW.write(bArr);
            }

            public void write(byte[] bArr, int i, int i2) {
                this.oW.write(bArr, i, i2);
            }
        }

        public b(String str, int i) {
            this.hK = str;
            String substring = str.substring("file://".length());
            if (Environment.getExternalStorageState().equals("mounted")) {
                this.file = new File(Environment.getExternalStorageDirectory() + substring);
                return;
            }
            throw new IOException("SD card not ready.");
        }

        public void P(String str) {
            this.file.renameTo(new File(getPath(), str));
        }

        public void Q(String str) {
        }

        public DataInputStream aQ() {
            this.oS = new DataInputStream(aR());
            return this.oS;
        }

        public InputStream aR() {
            this.oU = new FileInputStream(this.file);
            return this.oU;
        }

        public DataOutputStream aS() {
            this.oT = new DataOutputStream(aT());
            return this.oT;
        }

        public OutputStream aT() {
            this.oV = new FileOutputStream(this.file);
            return this.oV;
        }

        public Enumeration<String> b(String str, boolean z) {
            return null;
        }

        public long bk() {
            return 0;
        }

        public long bl() {
            StatFs statFs = new StatFs(getPath());
            return (long) (statFs.getBlockSize() * statFs.getAvailableBlocks());
        }

        public long bm() {
            return 0;
        }

        public long bn() {
            return this.file.length();
        }

        public Enumeration<String> bo() {
            String[] list = this.file.list();
            Vector vector = new Vector(list.length);
            for (String addElement : list) {
                vector.addElement(addElement);
            }
            return vector.elements();
        }

        public void bp() {
            this.file.mkdir();
        }

        public boolean canRead() {
            return this.file.canRead();
        }

        public boolean canWrite() {
            return this.file.canWrite();
        }

        public void close() {
            if (this.oT != null) {
                this.oT.close();
                this.oT = null;
            }
            if (this.oS != null) {
                this.oS.close();
                this.oS = null;
            }
            if (this.oV != null) {
                this.oV.close();
                this.oV = null;
            }
            if (this.oU != null) {
                this.oU.close();
                this.oU = null;
            }
        }

        public void create() {
            this.file.createNewFile();
        }

        public OutputStream d(long j) {
            RandomAccessFile randomAccessFile = new RandomAccessFile(this.file, "rw");
            randomAccessFile.seek(j);
            this.oV = new a(randomAccessFile);
            return this.oV;
        }

        public void delete() {
            this.file.delete();
        }

        public boolean exists() {
            return this.file.exists();
        }

        public long g(boolean z) {
            return 0;
        }

        public String getName() {
            String name = this.file.getName();
            int lastIndexOf = name.lastIndexOf(47);
            return lastIndexOf == -1 ? name : name.substring(lastIndexOf);
        }

        public String getPath() {
            String path = this.file.getPath();
            int lastIndexOf = path.lastIndexOf(47);
            return lastIndexOf == -1 ? path : path.substring(0, lastIndexOf);
        }

        public String getURL() {
            return this.hK;
        }

        public void h(boolean z) {
        }

        public void i(boolean z) {
        }

        public boolean isDirectory() {
            return this.file.isDirectory();
        }

        public boolean isHidden() {
            return isHidden();
        }

        public boolean isOpen() {
            return (this.oS == null && this.oT == null && this.oU == null && this.oV == null) ? false : true;
        }

        public void j(boolean z) {
        }

        public long lastModified() {
            return this.file.lastModified();
        }

        public void truncate(long j) {
        }
    }

    public static final class c {
        public final Paint fV = new Paint(1);
        public Paint.FontMetricsInt oY;

        public c(Typeface typeface, int i, boolean z) {
            this.fV.setTypeface(typeface);
            this.fV.setTextSize((float) i);
            this.fV.setUnderlineText(z);
            this.oY = this.fV.getFontMetricsInt();
        }

        public int R(String str) {
            return (int) this.fV.measureText(str);
        }

        public int a(char[] cArr, int i, int i2) {
            return (int) this.fV.measureText(cArr, i, i2);
        }

        public int b(char c) {
            return (int) this.fV.measureText(String.valueOf(c));
        }

        public int b(String str, int i, int i2) {
            return (int) this.fV.measureText(str, i, i + i2);
        }

        public int cn() {
            return -this.oY.ascent;
        }

        public int getHeight() {
            return this.fV.getFontMetricsInt(this.oY);
        }
    }

    public static final class d {
        public static int SIZE_LARGE = 16;
        public static int SIZE_MEDIUM = 14;
        public static int SIZE_SMALL = 12;
        private static final HashMap<com.a.a.e.l, c> oZ = new HashMap<>();

        public static int a(com.a.a.e.l lVar, char c) {
            return b(lVar).b(c);
        }

        public static int a(com.a.a.e.l lVar, String str) {
            return b(lVar).R(str);
        }

        public static int a(com.a.a.e.l lVar, String str, int i, int i2) {
            return b(lVar).b(str, i, i2);
        }

        public static int a(com.a.a.e.l lVar, char[] cArr, int i, int i2) {
            return b(lVar).a(cArr, i, i2);
        }

        public static c b(com.a.a.e.l lVar) {
            boolean z = true;
            int i = 0;
            c cVar = oZ.get(lVar);
            if (cVar != null) {
                return cVar;
            }
            Typeface typeface = Typeface.SANS_SERIF;
            if (lVar.ck() == 0) {
                typeface = Typeface.SANS_SERIF;
            } else if (lVar.ck() == 32) {
                typeface = Typeface.MONOSPACE;
            } else if (lVar.ck() == 64) {
                typeface = Typeface.SANS_SERIF;
            }
            if ((lVar.getStyle() & 0) != 0) {
            }
            int i2 = (lVar.getStyle() & 1) != 0 ? 1 : 0;
            if ((lVar.getStyle() & 2) != 0) {
                i2 |= 2;
            }
            if ((lVar.getStyle() & 4) == 0) {
                z = false;
            }
            if (lVar.getSize() == 8) {
                i = SIZE_SMALL;
            } else if (lVar.getSize() == 0) {
                i = SIZE_MEDIUM;
            } else if (lVar.getSize() == 16) {
                i = SIZE_LARGE;
            }
            c cVar2 = new c(Typeface.create(typeface, i2), i, z);
            oZ.put(lVar, cVar2);
            return cVar2;
        }

        public static int c(com.a.a.e.l lVar) {
            return b(lVar).cn();
        }

        public static int d(com.a.a.e.l lVar) {
            return b(lVar).getHeight();
        }

        public static com.a.a.e.l s(int i, int i2, int i3) {
            boolean z = true;
            com.a.a.e.l lVar = new com.a.a.e.l(i, i2, i3);
            Typeface typeface = Typeface.SANS_SERIF;
            if (lVar.ck() == 0) {
                typeface = Typeface.SANS_SERIF;
            } else if (lVar.ck() == 32) {
                typeface = Typeface.MONOSPACE;
            } else if (lVar.ck() == 64) {
                typeface = Typeface.SANS_SERIF;
            }
            if ((lVar.getStyle() & 0) != 0) {
            }
            int i4 = (lVar.getStyle() & 1) != 0 ? 1 : 0;
            if ((lVar.getStyle() & 2) != 0) {
                i4 |= 2;
            }
            if ((lVar.getStyle() & 4) == 0) {
                z = false;
            }
            oZ.put(lVar, new c(Typeface.create(typeface, i4), i3, z));
            return lVar;
        }

        public void init() {
            oZ.clear();
        }
    }

    public static class e extends o {
        private static final DashPathEffect pd = new DashPathEffect(new float[]{5.0f, 5.0f}, 0.0f);
        private com.a.a.e.l hJ;
        private Paint pa;
        private Paint pb;
        private c pc;
        private Canvas pe;
        private int pf;
        private Matrix pg;
        private Bitmap ph;
        private int pi;
        private int pj;
        private int pk;
        private int pl;
        private Bitmap pm;
        private int[] pn;

        private e() {
            this.pa = new Paint();
            this.pb = new Paint();
            this.pf = 0;
            this.pg = new Matrix();
            this.pi = 0;
            this.pj = 0;
            this.pk = 0;
            this.pl = 0;
            this.pa.setAntiAlias(false);
            this.pa.setStyle(Paint.Style.STROKE);
            this.pb.setAntiAlias(false);
            this.pb.setStyle(Paint.Style.FILL);
        }

        public e(Bitmap bitmap) {
            this();
            this.ph = bitmap;
            this.pe = new Canvas(bitmap);
            b(this.pe);
        }

        public e(Canvas canvas) {
            this();
            b(canvas);
        }

        public void a(com.a.a.e.l lVar) {
            if (lVar == null) {
                lVar = com.a.a.e.l.cj();
            }
            this.hJ = lVar;
            this.pc = d.b(lVar);
        }

        public void a(p pVar, int i, int i2, int i3) {
            if (pVar != null && this.pk > 0 && this.pl > 0 && pVar.bitmap != null) {
                if (i3 == 0) {
                    i3 = 20;
                }
                if ((i3 & 8) != 0) {
                    i -= pVar.width;
                } else if ((i3 & 1) != 0) {
                    i -= pVar.width / 2;
                }
                if ((i3 & 32) != 0) {
                    i2 -= pVar.height;
                } else if ((i3 & 2) != 0) {
                    i2 -= pVar.height / 2;
                }
                this.pe.drawBitmap(pVar.bitmap, (float) i, (float) i2, (Paint) null);
            }
        }

        public void a(p pVar, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            int i9;
            int i10;
            int i11;
            boolean z = true;
            if (pVar != null && pVar.bitmap != null && this.pk > 0 && this.pl > 0) {
                if (i < 0 || i2 < 0) {
                    throw new IllegalArgumentException("Area out of Image: x" + i + " y:" + i2);
                } else if (!pVar.isMutable() || pVar.cD() != this) {
                    if (i + i3 > pVar.getWidth()) {
                        i3 = pVar.getWidth() - i;
                    }
                    int height = i2 + i4 > pVar.getHeight() ? pVar.getHeight() - i2 : i4;
                    if (i3 > 0 && height > 0) {
                        this.pg.reset();
                        switch (i5) {
                            case 0:
                                i9 = i3;
                                i3 = height;
                                i10 = -i;
                                i11 = -i2;
                                break;
                            case 1:
                                this.pg.preScale(-1.0f, 1.0f);
                                this.pg.preRotate(-180.0f);
                                int i12 = height + i2;
                                i9 = i3;
                                i3 = height;
                                i10 = -i;
                                i11 = i12;
                                break;
                            case 2:
                                this.pg.preScale(-1.0f, 1.0f);
                                int i13 = i3 + i;
                                i9 = i3;
                                i3 = height;
                                i10 = i13;
                                i11 = -i2;
                                break;
                            case 3:
                                this.pg.preRotate(180.0f);
                                int i14 = i3 + i;
                                int i15 = height + i2;
                                i9 = i3;
                                i3 = height;
                                i10 = i14;
                                i11 = i15;
                                break;
                            case 4:
                                this.pg.preScale(-1.0f, 1.0f);
                                this.pg.preRotate(-270.0f);
                                i9 = height;
                                i10 = -i2;
                                i11 = -i;
                                break;
                            case 5:
                                this.pg.preRotate(90.0f);
                                i9 = height;
                                i10 = height + i2;
                                i11 = -i;
                                break;
                            case 6:
                                this.pg.preRotate(270.0f);
                                i9 = height;
                                i10 = -i2;
                                i11 = i3 + i;
                                break;
                            case 7:
                                this.pg.preScale(-1.0f, 1.0f);
                                this.pg.preRotate(-90.0f);
                                i9 = height;
                                i10 = height + i2;
                                i11 = i3 + i;
                                break;
                            default:
                                throw new IllegalArgumentException("Bad transform");
                        }
                        boolean z2 = false;
                        if (i8 == 0) {
                            i8 = 20;
                        }
                        if ((i8 & 64) != 0) {
                            z2 = true;
                        }
                        if ((i8 & 16) != 0) {
                            if ((i8 & 34) != 0) {
                                z2 = true;
                            }
                        } else if ((i8 & 32) != 0) {
                            if ((i8 & 2) != 0) {
                                z2 = true;
                            } else {
                                i7 -= i3;
                            }
                        } else if ((i8 & 2) != 0) {
                            i7 -= (i3 - 1) >>> 1;
                        } else {
                            z2 = true;
                        }
                        if ((i8 & 4) != 0) {
                            if ((i8 & 9) == 0) {
                                z = z2;
                            }
                        } else if ((i8 & 8) != 0) {
                            if ((i8 & 1) == 0) {
                                i6 -= i9;
                                z = z2;
                            }
                        } else if ((i8 & 1) != 0) {
                            i6 -= (i9 - 1) >>> 1;
                            z = z2;
                        }
                        if (z) {
                            throw new IllegalArgumentException("Bad Anchor");
                        }
                        this.pe.save();
                        this.pe.clipRect(i6, i7, i6 + i9, i7 + i3);
                        this.pe.translate((float) (i6 + i10), (float) (i11 + i7));
                        this.pe.drawBitmap(pVar.bitmap, this.pg, null);
                        this.pe.restore();
                    }
                } else {
                    throw new IllegalArgumentException("Image is source and target");
                }
            }
        }

        public void a(String str, int i, int i2, int i3) {
            if (this.pk > 0 && this.pl > 0) {
                if (i3 == 0) {
                    i3 = 20;
                }
                if ((i3 & 16) != 0) {
                    i2 -= this.pc.oY.top;
                } else if ((i3 & 32) != 0) {
                    i2 -= this.pc.oY.bottom;
                } else if ((i3 & 2) != 0) {
                    i2 += ((this.pc.oY.descent - this.pc.oY.ascent) / 2) - this.pc.oY.descent;
                }
                if ((i3 & 1) != 0) {
                    this.pc.fV.setTextAlign(Paint.Align.CENTER);
                } else if ((i3 & 8) != 0) {
                    this.pc.fV.setTextAlign(Paint.Align.RIGHT);
                } else if ((i3 & 4) != 0) {
                    this.pc.fV.setTextAlign(Paint.Align.LEFT);
                }
                this.pc.fV.setColor(this.pa.getColor());
                this.pe.drawText(str, (float) i, (float) i2, this.pc.fV);
            }
        }

        public void a(String str, int i, int i2, int i3, int i4, int i5) {
            if (this.pk > 0 && this.pl > 0) {
                if (i5 == 0) {
                    i5 = 20;
                }
                if ((i5 & 16) != 0) {
                    i4 -= this.pc.oY.top;
                } else if ((i5 & 32) != 0) {
                    i4 -= this.pc.oY.bottom;
                } else if ((i5 & 2) != 0) {
                    i4 += ((this.pc.oY.descent - this.pc.oY.ascent) / 2) - this.pc.oY.descent;
                }
                if ((i5 & 1) != 0) {
                    this.pc.fV.setTextAlign(Paint.Align.CENTER);
                } else if ((i5 & 8) != 0) {
                    this.pc.fV.setTextAlign(Paint.Align.RIGHT);
                } else if ((i5 & 4) != 0) {
                    this.pc.fV.setTextAlign(Paint.Align.LEFT);
                }
                this.pc.fV.setColor(this.pa.getColor());
                String str2 = str;
                int i6 = i;
                this.pe.drawText(str2, i6, i2 + i, (float) i3, (float) i4, this.pc.fV);
            }
        }

        public void a(int[] iArr, int i, int i2, int i3, int i4, int i5, int i6, boolean z) {
            int[] iArr2;
            if (iArr != null && this.pk > 0 && this.pl > 0 && i5 > 0 && i6 > 0) {
                int i7 = i2 < i5 ? i5 : i2;
                if (iArr.length - i < i5 * i6) {
                    iArr2 = new int[((i5 * i6) + i)];
                    int i8 = i;
                    while (i8 < iArr2.length) {
                        System.arraycopy(iArr, 0, iArr2, i8, i5);
                        i8 += i5;
                    }
                } else {
                    iArr2 = iArr;
                }
                this.pe.drawBitmap(iArr2, i, i7, i3, i4, i5, i6, z, (Paint) null);
            }
        }

        public void aI(int i) {
            if (i == 0 || i == 1) {
                this.pf = i;
                if (i == 0) {
                    this.pa.setPathEffect(null);
                    this.pb.setPathEffect(null);
                    return;
                }
                this.pa.setPathEffect(pd);
                this.pb.setPathEffect(pd);
                return;
            }
            throw new IllegalArgumentException();
        }

        public int aJ(int i) {
            return i;
        }

        public void b(int i, int i2, int i3, int i4, int i5, int i6) {
            if (this.pk > 0 && this.pl > 0 && i3 > 0 && i4 > 0) {
                this.pe.drawArc(new RectF((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4)), (float) i5, (float) i6, true, this.pa);
            }
        }

        public void b(Canvas canvas) {
            this.pe = canvas;
            a(com.a.a.e.l.cj());
            translate(-cA(), -cB());
            setColor(0);
            if (this.ph != null) {
                j(0, 0, this.ph.getWidth(), this.ph.getHeight());
            } else {
                j(0, 0, org.meteoroid.core.c.mN.getWidth(), org.meteoroid.core.c.mN.getHeight());
            }
        }

        public void c(int i, int i2, int i3, int i4, int i5, int i6) {
            if (this.pk > 0 && this.pl > 0 && i3 > 0 && i4 > 0) {
                this.pe.drawRoundRect(new RectF((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4)), (float) i5, (float) i6, this.pa);
            }
        }

        public void c(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
            boolean z = true;
            if (this.pk > 0 && this.pl > 0 && i3 > 0 && i4 > 0 && this.ph != null) {
                Log.d("copyArea", "src:" + i + "|" + i2 + "|" + i3 + "|" + i4 + " dest:" + i5 + "|" + i6);
                if (i7 == 0) {
                    i7 = 20;
                }
                boolean z2 = (i7 & 64) != 0;
                if ((i7 & 16) != 0) {
                    if ((i7 & 34) != 0) {
                        z2 = true;
                    }
                } else if ((i7 & 32) != 0) {
                    if ((i7 & 2) != 0) {
                        z2 = true;
                    } else {
                        i6 -= i4 - 1;
                    }
                } else if ((i7 & 2) != 0) {
                    i6 -= (i4 - 1) >>> 1;
                } else {
                    z2 = true;
                }
                if ((i7 & 4) != 0) {
                    if ((i7 & 9) == 0) {
                        z = z2;
                    }
                } else if ((i7 & 8) != 0) {
                    if ((i7 & 1) == 0) {
                        i5 -= i3 - 1;
                        z = z2;
                    }
                } else if ((i7 & 1) != 0) {
                    i5 -= (i3 - 1) >>> 1;
                    z = z2;
                }
                if (z) {
                    throw new IllegalArgumentException("Bad Anchor");
                }
                if (this.pm != null && this.pm.getWidth() == i3 && this.pm.getHeight() == i4) {
                    if (this.pn == null || !(this.pn == null || this.pn.length == i3 * i4)) {
                        this.pn = new int[(i3 * i4)];
                    }
                    this.ph.getPixels(this.pn, 0, i3, i, i2, i3, i4);
                    this.pm.setPixels(this.pn, 0, i3, 0, 0, i3, i4);
                } else {
                    this.pm = Bitmap.createBitmap(this.ph, i, i2, i3, i4);
                }
                this.pe.drawBitmap(this.pm, (float) i5, (float) i6, this.pa);
            }
        }

        public void c(Bitmap bitmap) {
            this.pe.drawBitmap(bitmap, 0.0f, 0.0f, (Paint) null);
        }

        public void cC() {
            b(this.pe);
        }

        public int cr() {
            return this.pl;
        }

        public int cs() {
            return this.pk;
        }

        public int ct() {
            return this.pi;
        }

        public int cu() {
            return this.pj;
        }

        public com.a.a.e.l cv() {
            return this.hJ;
        }

        public int cz() {
            return this.pf;
        }

        public void d(int i, int i2, int i3, int i4, int i5, int i6) {
            if (this.pk > 0 && this.pl > 0 && i3 > 0 && i4 > 0) {
                this.pe.drawArc(new RectF((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4)), (float) i5, (float) i6, true, this.pb);
            }
        }

        public void e(int i, int i2, int i3, int i4, int i5, int i6) {
            if (this.pk > 0 && this.pl > 0 && i3 > 0 && i4 > 0) {
                this.pe.drawRoundRect(new RectF((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4)), (float) i5, (float) i6, this.pb);
            }
        }

        public void f(int i, int i2, int i3, int i4) {
            if (i3 < 0 || i4 < 0) {
                i4 = 0;
                i3 = 0;
            }
            this.pe.clipRect((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4), Region.Op.INTERSECT);
            Rect clipBounds = this.pe.getClipBounds();
            this.pi = clipBounds.left;
            this.pj = clipBounds.top;
            this.pk = clipBounds.width();
            this.pl = clipBounds.height();
        }

        public void f(int i, int i2, int i3, int i4, int i5, int i6) {
            if (this.pk > 0 && this.pl > 0) {
                Path path = new Path();
                path.moveTo((float) i, (float) i2);
                path.lineTo((float) i3, (float) i4);
                path.lineTo((float) i5, (float) i6);
                path.lineTo((float) i, (float) i2);
                this.pe.drawPath(path, this.pb);
            }
        }

        public void g(int i, int i2, int i3, int i4) {
            if (this.pk > 0 && this.pl > 0) {
                if (i > i3) {
                    i++;
                } else {
                    i3++;
                }
                if (i2 > i4) {
                    i2++;
                } else {
                    i4++;
                }
                this.pe.drawLine((float) i, (float) i2, (float) i3, (float) i4, this.pa);
            }
        }

        public Bitmap getBitmap() {
            return this.ph;
        }

        public void h(int i, int i2, int i3, int i4) {
            if (this.pk > 0 && this.pl > 0) {
                if (i3 == 0 && i4 == 0) {
                    this.pe.drawPoint((float) i, (float) i2, this.pb);
                } else {
                    this.pe.drawRect((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4), this.pa);
                }
            }
        }

        public void i(int i, int i2, int i3, int i4) {
            if (this.pk > 0 && this.pl > 0 && i3 > 0 && i4 > 0) {
                this.pe.drawRect((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4), this.pb);
            }
        }

        public Canvas iB() {
            return this.pe;
        }

        public void j(int i, int i2, int i3, int i4) {
            this.pi = i;
            this.pj = i2;
            this.pk = i3;
            this.pl = i4;
            if (i3 > 0 && i4 > 0) {
                this.pe.clipRect((float) i, (float) i2, (float) (i + i3), (float) (i2 + i4), Region.Op.REPLACE);
            }
        }

        public void setColor(int i) {
            this.pa.setColor(-16777216 | i);
            this.pb.setColor(-16777216 | i);
            super.setColor(i);
        }

        public void translate(int i, int i2) {
            if (i != 0 || i2 != 0) {
                super.translate(i, i2);
                this.pe.translate((float) i, (float) i2);
                j(ct() - i, cu() - i2, cs(), cr());
            }
        }
    }

    public static final class f implements com.a.a.c.h {
        private HttpURLConnection po;
        private URL url;

        public f(String str, int i, boolean z) {
            try {
                if (!hC()) {
                    throw new IOException("No avaliable connection.");
                }
                this.url = new URL(str);
                this.po = (HttpURLConnection) this.url.openConnection();
                this.po.setDoInput(true);
                if (i != 1) {
                    this.po.setDoOutput(true);
                }
                if (z) {
                    this.po.setConnectTimeout(10000);
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }

        private final boolean hC() {
            NetworkInfo activeNetworkInfo;
            try {
                ConnectivityManager hP = org.meteoroid.core.l.hP();
                return hP != null && (activeNetworkInfo = hP.getActiveNetworkInfo()) != null && activeNetworkInfo.isConnected() && activeNetworkInfo.getState() == NetworkInfo.State.CONNECTED;
            } catch (Exception e) {
                Log.v(PlayerListener.ERROR, e.toString());
            }
        }

        public DataInputStream aQ() {
            return new DataInputStream(aR());
        }

        public InputStream aR() {
            return this.po.getInputStream();
        }

        public DataOutputStream aS() {
            return new DataOutputStream(aT());
        }

        public OutputStream aT() {
            return this.po.getOutputStream();
        }

        public void close() {
            Log.d("HTTPConnection", "Connection " + this.url.toString() + " disconnected.");
            this.po.disconnect();
        }

        public long getDate() {
            return this.po.getDate();
        }

        public String getEncoding() {
            return this.po.getContentEncoding();
        }

        public long getExpiration() {
            return this.po.getExpiration();
        }

        public String getFile() {
            return this.url.getFile();
        }

        public String getHeaderField(int i) {
            return this.po.getHeaderField(i);
        }

        public String getHeaderField(String str) {
            return this.po.getHeaderField(str);
        }

        public long getHeaderFieldDate(String str, long j) {
            return this.po.getHeaderFieldDate(str, j);
        }

        public int getHeaderFieldInt(String str, int i) {
            return this.po.getHeaderFieldInt(str, i);
        }

        public String getHeaderFieldKey(int i) {
            return this.po.getHeaderFieldKey(i);
        }

        public String getHost() {
            return this.url.getHost();
        }

        public long getLastModified() {
            return this.po.getLastModified();
        }

        public long getLength() {
            return (long) this.po.getContentLength();
        }

        public int getPort() {
            return this.url.getPort();
        }

        public String getProtocol() {
            return this.url.getProtocol();
        }

        public String getQuery() {
            return this.url.getQuery();
        }

        public String getRef() {
            return this.url.getRef();
        }

        public String getRequestMethod() {
            return this.po.getRequestMethod();
        }

        public String getRequestProperty(String str) {
            return this.po.getRequestProperty(str);
        }

        public int getResponseCode() {
            Log.d("HTTPConnection", "Connection " + this.url.toString() + " established.");
            String requestProperty = this.po.getRequestProperty("X-Online-Host");
            if (requestProperty != null) {
                Log.d("HTTPConnection", "Connection X-Online-Host is" + requestProperty + ".");
            }
            this.po.connect();
            int responseCode = this.po.getResponseCode();
            Log.d("HTTPConnection", "Connection " + this.url.toString() + " response " + responseCode);
            return responseCode;
        }

        public String getResponseMessage() {
            return this.po.getResponseMessage();
        }

        public String getType() {
            return this.po.getContentType();
        }

        public String getURL() {
            return this.url.getPath();
        }

        public void setRequestMethod(String str) {
            this.po.setRequestMethod(str);
        }

        public void setRequestProperty(String str, String str2) {
            this.po.setRequestProperty(str, str2);
        }
    }

    public static final class g implements com.a.a.b.g {
        public BluetoothDevice fK = null;
        BluetoothAdapter pp;
        BluetoothSocket pq;
        com.a.a.b.j pr;

        /* JADX WARNING: Removed duplicated region for block: B:15:0x009e A[LOOP:1: B:15:0x009e->B:34:0x0166, LOOP_START, PHI: r2 r4 
          PHI: (r2v15 int) = (r2v14 int), (r2v16 int) binds: [B:14:0x009c, B:34:0x0166] A[DONT_GENERATE, DONT_INLINE]
          PHI: (r4v4 android.bluetooth.BluetoothSocket) = (r4v3 android.bluetooth.BluetoothSocket), (r4v5 android.bluetooth.BluetoothSocket) binds: [B:14:0x009c, B:34:0x0166] A[DONT_GENERATE, DONT_INLINE]] */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x0131  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public g(java.lang.String r8) {
            /*
                r7 = this;
                r1 = 0
                r3 = 0
                r7.<init>()
                r7.fK = r1
                java.lang.String r0 = "MIDPDevice"
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r4 = "open StreamConnection   name = "
                java.lang.StringBuilder r2 = r2.append(r4)
                java.lang.StringBuilder r2 = r2.append(r8)
                java.lang.String r2 = r2.toString()
                android.util.Log.d(r0, r2)
                android.bluetooth.BluetoothAdapter r0 = android.bluetooth.BluetoothAdapter.getDefaultAdapter()
                r7.pp = r0
                java.lang.String r0 = "MACadd"
                int r0 = r8.lastIndexOf(r0)
                java.lang.String r2 = "MACadd"
                int r2 = r2.length()
                int r2 = r2 + r0
                java.lang.String r0 = ""
                r4 = r0
                r0 = r2
            L_0x0036:
                int r5 = r2 + 17
                if (r0 >= r5) goto L_0x0052
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.StringBuilder r4 = r5.append(r4)
                char r5 = r8.charAt(r0)
                java.lang.StringBuilder r4 = r4.append(r5)
                java.lang.String r4 = r4.toString()
                int r0 = r0 + 1
                goto L_0x0036
            L_0x0052:
                com.a.a.r.a r0 = org.meteoroid.core.c.mN
                org.meteoroid.plugin.device.MIDPDevice r0 = (org.meteoroid.plugin.device.MIDPDevice) r0
                com.a.a.b.e r2 = com.a.a.b.e.fb
                java.util.UUID r2 = r2.fc
                r0.fc = r2
                java.lang.String r0 = "MIDPDevice"
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x016e }
                r2.<init>()     // Catch:{ Exception -> 0x016e }
                java.lang.String r5 = "ADD = "
                java.lang.StringBuilder r2 = r2.append(r5)     // Catch:{ Exception -> 0x016e }
                java.lang.StringBuilder r2 = r2.append(r4)     // Catch:{ Exception -> 0x016e }
                java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x016e }
                android.util.Log.d(r0, r2)     // Catch:{ Exception -> 0x016e }
                java.lang.String r0 = "MIDPDevice"
                java.lang.String r2 = "先直接连获取到的远程设备.如果连不上再循环连已经搜索到的设备"
                android.util.Log.d(r0, r2)     // Catch:{ Exception -> 0x016e }
                android.bluetooth.BluetoothAdapter r0 = r7.pp     // Catch:{ Exception -> 0x016e }
                android.bluetooth.BluetoothDevice r2 = r0.getRemoteDevice(r4)     // Catch:{ Exception -> 0x016e }
                com.a.a.r.a r0 = org.meteoroid.core.c.mN     // Catch:{ Exception -> 0x016e }
                org.meteoroid.plugin.device.MIDPDevice r0 = (org.meteoroid.plugin.device.MIDPDevice) r0     // Catch:{ Exception -> 0x016e }
                java.util.UUID r0 = r0.fc     // Catch:{ Exception -> 0x016e }
                android.bluetooth.BluetoothSocket r1 = r2.createRfcommSocketToServiceRecord(r0)     // Catch:{ Exception -> 0x016e }
                if (r1 != 0) goto L_0x0148
                java.lang.String r0 = "MIDPDevice"
                java.lang.String r2 = "BluetoothSocket null"
                android.util.Log.d(r0, r2)     // Catch:{ Exception -> 0x009a }
                java.lang.NullPointerException r0 = new java.lang.NullPointerException     // Catch:{ Exception -> 0x009a }
                r0.<init>()     // Catch:{ Exception -> 0x009a }
                throw r0     // Catch:{ Exception -> 0x009a }
            L_0x009a:
                r0 = move-exception
                r0 = r1
            L_0x009c:
                r2 = r3
                r4 = r0
            L_0x009e:
                com.a.a.b.e r0 = com.a.a.b.e.fb
                java.util.Vector<android.bluetooth.BluetoothDevice> r0 = r0.fl
                int r0 = r0.size()
                if (r2 >= r0) goto L_0x0175
                com.a.a.r.a r0 = org.meteoroid.core.c.mN     // Catch:{ IOException -> 0x014c }
                org.meteoroid.plugin.device.MIDPDevice r0 = (org.meteoroid.plugin.device.MIDPDevice) r0     // Catch:{ IOException -> 0x014c }
                java.util.UUID r0 = r0.fc     // Catch:{ IOException -> 0x014c }
                if (r0 == 0) goto L_0x0172
                java.lang.String r0 = "MIDPDevice"
                java.lang.String r1 = "开始连接"
                android.util.Log.d(r0, r1)     // Catch:{ IOException -> 0x014c }
                java.lang.String r1 = "MIDPDevice"
                com.a.a.b.e r0 = com.a.a.b.e.fb     // Catch:{ IOException -> 0x014c }
                java.util.Vector<android.bluetooth.BluetoothDevice> r0 = r0.fl     // Catch:{ IOException -> 0x014c }
                java.lang.Object r0 = r0.elementAt(r2)     // Catch:{ IOException -> 0x014c }
                android.bluetooth.BluetoothDevice r0 = (android.bluetooth.BluetoothDevice) r0     // Catch:{ IOException -> 0x014c }
                java.lang.String r0 = r0.getName()     // Catch:{ IOException -> 0x014c }
                android.util.Log.d(r1, r0)     // Catch:{ IOException -> 0x014c }
                java.lang.String r1 = "MIDPDevice"
                com.a.a.b.e r0 = com.a.a.b.e.fb     // Catch:{ IOException -> 0x014c }
                java.util.Vector<android.bluetooth.BluetoothDevice> r0 = r0.fl     // Catch:{ IOException -> 0x014c }
                java.lang.Object r0 = r0.elementAt(r2)     // Catch:{ IOException -> 0x014c }
                android.bluetooth.BluetoothDevice r0 = (android.bluetooth.BluetoothDevice) r0     // Catch:{ IOException -> 0x014c }
                java.lang.String r0 = r0.getAddress()     // Catch:{ IOException -> 0x014c }
                android.util.Log.d(r1, r0)     // Catch:{ IOException -> 0x014c }
                com.a.a.b.e r0 = com.a.a.b.e.fb     // Catch:{ IOException -> 0x014c }
                java.util.Vector<android.bluetooth.BluetoothDevice> r0 = r0.fl     // Catch:{ IOException -> 0x014c }
                java.lang.Object r0 = r0.elementAt(r2)     // Catch:{ IOException -> 0x014c }
                android.bluetooth.BluetoothDevice r0 = (android.bluetooth.BluetoothDevice) r0     // Catch:{ IOException -> 0x014c }
                com.a.a.r.a r1 = org.meteoroid.core.c.mN     // Catch:{ IOException -> 0x014c }
                org.meteoroid.plugin.device.MIDPDevice r1 = (org.meteoroid.plugin.device.MIDPDevice) r1     // Catch:{ IOException -> 0x014c }
                java.util.UUID r1 = r1.fc     // Catch:{ IOException -> 0x014c }
                android.bluetooth.BluetoothSocket r1 = r0.createRfcommSocketToServiceRecord(r1)     // Catch:{ IOException -> 0x014c }
            L_0x00f1:
                if (r1 == 0) goto L_0x0166
                com.a.a.b.e r0 = com.a.a.b.e.fb     // Catch:{ IOException -> 0x016c }
                java.util.Vector<android.bluetooth.BluetoothDevice> r0 = r0.fl     // Catch:{ IOException -> 0x016c }
                java.lang.Object r0 = r0.elementAt(r2)     // Catch:{ IOException -> 0x016c }
                android.bluetooth.BluetoothDevice r0 = (android.bluetooth.BluetoothDevice) r0     // Catch:{ IOException -> 0x016c }
                r7.fK = r0     // Catch:{ IOException -> 0x016c }
                r1.connect()     // Catch:{ IOException -> 0x016c }
            L_0x0102:
                java.lang.String r0 = "MIDPDevice"
                java.lang.String r2 = "取消搜索"
                android.util.Log.d(r0, r2)
                r7.pq = r1
                java.lang.String r0 = "MIDPDevice"
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "远程设备描述内容 = "
                java.lang.StringBuilder r1 = r1.append(r2)
                android.bluetooth.BluetoothSocket r2 = r7.pq
                android.bluetooth.BluetoothDevice r2 = r2.getRemoteDevice()
                int r2 = r2.describeContents()
                java.lang.StringBuilder r1 = r1.append(r2)
                java.lang.String r1 = r1.toString()
                android.util.Log.d(r0, r1)
                com.a.a.b.j r0 = r7.pr
                if (r0 != 0) goto L_0x013a
                com.a.a.b.j r0 = new com.a.a.b.j
                android.bluetooth.BluetoothSocket r1 = r7.pq
                r0.<init>(r1, r3)
                r7.pr = r0
            L_0x013a:
                com.a.a.r.a r0 = org.meteoroid.core.c.mN
                org.meteoroid.plugin.device.MIDPDevice r0 = (org.meteoroid.plugin.device.MIDPDevice) r0
                r0.oK = r7
                java.lang.String r0 = "MIDPDevice"
                java.lang.String r1 = "连接成功"
                android.util.Log.d(r0, r1)
                return
            L_0x0148:
                r1.connect()     // Catch:{ Exception -> 0x009a }
                goto L_0x0102
            L_0x014c:
                r0 = move-exception
                r1 = r4
            L_0x014e:
                java.lang.String r4 = "MIDPDevice"
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r6 = "error = "
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.StringBuilder r0 = r5.append(r0)
                java.lang.String r0 = r0.toString()
                android.util.Log.e(r4, r0)
            L_0x0166:
                int r0 = r2 + 1
                r2 = r0
                r4 = r1
                goto L_0x009e
            L_0x016c:
                r0 = move-exception
                goto L_0x014e
            L_0x016e:
                r0 = move-exception
                r0 = r1
                goto L_0x009c
            L_0x0172:
                r1 = r4
                goto L_0x00f1
            L_0x0175:
                r1 = r4
                goto L_0x0102
            */
            throw new UnsupportedOperationException("Method not decompiled: org.meteoroid.plugin.device.MIDPDevice.g.<init>(java.lang.String):void");
        }

        public int aF() {
            return this.pr.aF();
        }

        public int aG() {
            return this.pr.aG();
        }

        public DataInputStream aQ() {
            return this.pr.aQ();
        }

        public InputStream aR() {
            return this.pr.aR();
        }

        public DataOutputStream aS() {
            return this.pr.aS();
        }

        public OutputStream aT() {
            return this.pr.aT();
        }

        public void close() {
        }

        public void q(byte[] bArr) {
            this.pr.q(bArr);
        }

        public int r(byte[] bArr) {
            return this.pr.r(bArr);
        }

        public boolean ready() {
            return this.pr.ready();
        }
    }

    public static final class h implements com.a.a.b.h {
        private static final String LOG_TAG = "L2CapConnectionNotifier";
        public static final int STATE_CONNECTED = 3;
        public static final int STATE_CONNECTING = 2;
        public static final int STATE_LISTEN = 1;
        public static final int STATE_NONE = 0;
        public static int pv;
        public BluetoothDevice fK;
        public BluetoothSocket ps;
        public final BluetoothServerSocket pt;
        BluetoothAdapter pu;
        public boolean pw = false;
        com.a.a.b.j px;

        public h(String str) {
            BluetoothServerSocket bluetoothServerSocket;
            Log.d(LOG_TAG, "open StreamConnectionNotifier");
            this.pu = BluetoothAdapter.getDefaultAdapter();
            ((MIDPDevice) org.meteoroid.core.c.mN).oO = this.pu.getName();
            int indexOf = str.indexOf(";name=");
            if (indexOf != -1) {
                int length = indexOf + ";name=".length();
                String substring = str.substring(length, str.length());
                int indexOf2 = substring.indexOf(59);
                Log.d(LOG_TAG, "nameindex2 = " + indexOf2);
                if (indexOf2 != -1) {
                    MIDPDevice.oL = substring.substring(0, indexOf2);
                } else {
                    MIDPDevice.oL = str.substring(length, str.length());
                }
            }
            Log.d("MIDPDevice", "ServerName = " + MIDPDevice.oL);
            try {
                ((MIDPDevice) org.meteoroid.core.c.mN).fc = UUID.fromString("11111111-2222-3333-4444-555555555555");
                if (MIDPDevice.oL != "") {
                    this.pu.setName(MIDPDevice.oL);
                }
                ((MIDPDevice) org.meteoroid.core.c.mN).oN = true;
                bluetoothServerSocket = this.pu.listenUsingRfcommWithServiceRecord("MIDPBlueTooth", ((MIDPDevice) org.meteoroid.core.c.mN).fc);
            } catch (IOException e) {
                Log.e(LOG_TAG, "listen() failed", e);
                bluetoothServerSocket = null;
            }
            this.pt = bluetoothServerSocket;
            pv = 1;
            ((MIDPDevice) org.meteoroid.core.c.mN).oM = this;
        }

        /* renamed from: aH */
        public com.a.a.b.g bj() {
            pv = 1;
            new Thread() {
                public void run() {
                    while (h.pv != 3) {
                        try {
                            Log.d(h.LOG_TAG, "等待连接  " + h.pv);
                            BluetoothSocket accept = h.this.pt.accept();
                            if (accept != null) {
                                synchronized (((MIDPDevice) org.meteoroid.core.c.mN).fc) {
                                    switch (h.pv) {
                                        case 0:
                                        case 3:
                                            try {
                                                Log.d(h.LOG_TAG, "STATE_CONNECTED");
                                                accept.close();
                                                h.pv = 1;
                                                break;
                                            } catch (IOException e) {
                                                Log.d(h.LOG_TAG, "Could not close unwanted socket");
                                                break;
                                            }
                                        case 1:
                                        case 2:
                                            try {
                                                h.this.ps = accept;
                                                h.this.fK = h.this.ps.getRemoteDevice();
                                                h.this.px = new com.a.a.b.j(h.this.ps, true);
                                                h.pv = 3;
                                                Log.d(h.LOG_TAG, "监听成功.连接成功");
                                                break;
                                            } catch (Exception e2) {
                                                Log.e(h.LOG_TAG, "服务器 连接失败 = " + e2);
                                                break;
                                            }
                                    }
                                    Log.d(h.LOG_TAG, "notify        ");
                                    ((MIDPDevice) org.meteoroid.core.c.mN).fc.notify();
                                }
                            }
                        } catch (Exception e3) {
                            Log.d(h.LOG_TAG, "accept() failed", e3);
                            return;
                        }
                    }
                    return;
                }
            }.start();
            synchronized (((MIDPDevice) org.meteoroid.core.c.mN).fc) {
                try {
                    Log.d(LOG_TAG, "wait");
                    ((MIDPDevice) org.meteoroid.core.c.mN).fc.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return this.px;
        }

        public void close() {
        }
    }

    public static final class i implements MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener, Player {
        private static final String LOG_TAG = "MIDP player";
        private HashSet<PlayerListener> pA;
        private int pB = 1;
        private int pC;
        private boolean pD;
        private m pE;
        private g.a pz;
        private int state = 100;

        public i(g.a aVar) {
            this.pz = aVar;
            aVar.nu = org.meteoroid.core.g.hp();
            aVar.nw.setOnCompletionListener(this);
            aVar.nw.setOnPreparedListener(this);
            this.pA = new HashSet<>();
        }

        private final void c(String str, Object obj) {
            Iterator<PlayerListener> it = this.pA.iterator();
            while (it.hasNext()) {
                PlayerListener next = it.next();
                if (next != null) {
                    next.a(this, str, obj);
                }
            }
        }

        public void a(PlayerListener playerListener) {
            Log.d(LOG_TAG, "Add a PlayerListener.");
            if (!this.pA.contains(playerListener)) {
                this.pA.add(playerListener);
            }
        }

        public Control ad(String str) {
            if (str.indexOf("VolumeControl") == -1) {
                return null;
            }
            if (this.pE == null) {
                this.pE = new m(this.pz);
            }
            return this.pE;
        }

        public void b(PlayerListener playerListener) {
            this.pA.remove(playerListener);
        }

        public void ba(int i) {
            Log.d(LOG_TAG, "setLoopCount " + i + ".");
            this.pB = i;
        }

        public void close() {
            dF();
            this.state = 0;
            this.pz.recycle();
            if (this.pz.ns != null) {
                File file = new File(this.pz.ns);
                if (!file.exists() || !file.delete()) {
                    Log.d(LOG_TAG, "Temp file not exist or could not delete.");
                } else {
                    Log.d(LOG_TAG, "Clean the temp file.");
                }
            }
            org.meteoroid.core.g.a(this.pz);
            c(PlayerListener.CLOSED, null);
            Log.d(LOG_TAG, "close.");
        }

        public Control[] dC() {
            if (this.pE == null) {
                this.pE = new m(this.pz);
            }
            return new Control[]{this.pE};
        }

        public void dD() {
            try {
                if (this.state == 100) {
                    Log.d(LOG_TAG, "realize.");
                    this.pz.nw.reset();
                    this.pz.nw.setDataSource(this.pz.ns);
                    this.state = 200;
                }
            } catch (Exception e) {
                Log.w(LOG_TAG, e);
                throw new MediaException();
            }
        }

        public void dE() {
            dD();
            if (this.state == 200) {
                Log.d(LOG_TAG, "prefetch.");
                try {
                    this.pz.nw.prepare();
                    this.state = 300;
                } catch (Exception e) {
                    c(PlayerListener.ERROR, e.getMessage());
                    throw new MediaException();
                }
            }
        }

        public void dF() {
            this.state = 100;
            Log.d(LOG_TAG, "deallocate.");
        }

        public long dG() {
            return (long) (this.pz.nw.getCurrentPosition() * 1000);
        }

        public long e(long j) {
            Log.d(LOG_TAG, "setMediaTime " + j + ".");
            int duration = this.pz.nw.getDuration();
            if (((long) duration) < j) {
                j = (long) duration;
            }
            try {
                this.pz.nw.seekTo((int) j);
                return j;
            } catch (IllegalStateException e) {
                throw new MediaException();
            }
        }

        public String getContentType() {
            return this.pz.type;
        }

        public long getDuration() {
            return (long) this.pz.nw.getDuration();
        }

        public int getState() {
            return this.state;
        }

        public void onCompletion(MediaPlayer mediaPlayer) {
            Log.d(LOG_TAG, "playedCount:" + this.pC + " loopCount:" + this.pB);
            if (mediaPlayer == this.pz.nw) {
                this.pC++;
                if (this.pC < this.pB) {
                    try {
                        this.state = 100;
                        start();
                    } catch (MediaException e) {
                        e.printStackTrace();
                    }
                } else if (this.pA.isEmpty()) {
                    try {
                        stop();
                    } catch (MediaException e2) {
                        e2.printStackTrace();
                    }
                } else {
                    c(PlayerListener.END_OF_MEDIA, null);
                }
            }
        }

        public void onPrepared(MediaPlayer mediaPlayer) {
            Log.d(LOG_TAG, "onPrepared:" + this.pC + " loopCount:" + this.pB);
        }

        public void setContentType(String str) {
            this.pz.type = str;
        }

        public void start() {
            dD();
            dE();
            if (this.state == 300) {
                try {
                    if (this.pD && !this.pz.nw.isPlaying()) {
                        this.pz.nw.prepare();
                        this.pD = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    this.state = 100;
                    dD();
                    dE();
                }
                try {
                    Log.d(LOG_TAG, "start.");
                    if (this.pB == -1) {
                        this.pz.nw.setLooping(true);
                    } else {
                        this.pz.nw.setLooping(false);
                    }
                    this.pz.nw.start();
                    this.pz.nt = true;
                    this.state = 400;
                    c(PlayerListener.STARTED, null);
                } catch (Exception e2) {
                    c(PlayerListener.ERROR, e2.getMessage());
                    throw new MediaException();
                }
            }
        }

        public void stop() {
            if (this.state == 400) {
                Log.d(LOG_TAG, "stop.");
                try {
                    if (this.pz.nw.isPlaying()) {
                        this.pz.nw.stop();
                    }
                    this.pz.nt = false;
                    this.pC = 0;
                    this.state = 300;
                    this.pD = true;
                    c(PlayerListener.STOPPED, null);
                } catch (Exception e) {
                    c(PlayerListener.ERROR, e.getMessage());
                    e.printStackTrace();
                    throw new MediaException();
                } catch (Throwable th) {
                    this.state = 300;
                    this.pD = true;
                    c(PlayerListener.STOPPED, null);
                    throw th;
                }
            }
        }
    }

    public static final class j implements com.a.a.q.c, h.a {
        public static final int MSG_GCF_SMS_CONNECTION_RECEIVE = 15391747;
        public static final int MSG_GCF_SMS_CONNECTION_SEND = 15391744;
        public static final int MSG_GCF_SMS_CONNECTION_SEND_COMPLETE = 15391745;
        public static final int MSG_GCF_SMS_CONNECTION_SEND_FAIL = 15391746;
        private static final int NOT_SENT = 0;
        private static final int SENT_FAIL = 2;
        private static final int SENT_OK = 1;
        private String address;
        private int pF = 0;
        private com.a.a.q.d pG;
        private com.a.a.q.b pH;

        public static final class a implements com.a.a.q.a {
            private byte[] ec;
            private String pI;

            public byte[] fI() {
                return this.ec;
            }

            public String getAddress() {
                return this.pI;
            }

            public Date getTimestamp() {
                return new Date();
            }

            public void setAddress(String str) {
                this.pI = str;
            }

            public void w(byte[] bArr) {
                this.ec = bArr;
            }
        }

        public static final class b implements com.a.a.q.h {
            private String address;
            private String hH;

            public void aE(String str) {
                this.hH = str;
            }

            public String fS() {
                return this.hH;
            }

            public String getAddress() {
                return this.address;
            }

            public Date getTimestamp() {
                return new Date();
            }

            public void setAddress(String str) {
                this.address = str;
            }
        }

        public j(String str) {
            org.meteoroid.core.h.h(MSG_GCF_SMS_CONNECTION_SEND, "MSG_GCF_SMS_CONNECTION_SEND");
            org.meteoroid.core.h.h(MSG_GCF_SMS_CONNECTION_SEND_COMPLETE, "MSG_GCF_SMS_CONNECTION_SEND_COMPLETE");
            org.meteoroid.core.h.h(MSG_GCF_SMS_CONNECTION_SEND_FAIL, "MSG_GCF_SMS_CONNECTION_SEND_FAIL");
            org.meteoroid.core.h.h(MSG_GCF_SMS_CONNECTION_RECEIVE, "MSG_GCF_SMS_CONNECTION_RECEIVE");
            this.address = str;
            org.meteoroid.core.h.a(this);
        }

        public void a(com.a.a.q.b bVar) {
            org.meteoroid.core.h.a(this);
            org.meteoroid.core.h.c(org.meteoroid.core.h.c(MSG_GCF_SMS_CONNECTION_SEND, bVar));
            synchronized (this) {
                try {
                    this.pF = 0;
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            org.meteoroid.core.h.b(this);
            if (this.pF != 1) {
                throw new InterruptedIOException("Fail to send.");
            }
        }

        public void a(com.a.a.q.d dVar) {
            this.pG = dVar;
        }

        public com.a.a.q.b aw(String str) {
            return m(str, null);
        }

        public int b(com.a.a.q.b bVar) {
            return 1;
        }

        public boolean b(Message message) {
            if (message.what == 15391745) {
                this.pF = 1;
                synchronized (this) {
                    notifyAll();
                }
                return true;
            } else if (message.what == 15391746) {
                this.pF = 2;
                synchronized (this) {
                    notifyAll();
                }
                return true;
            } else if (message.what != 15391747) {
                return false;
            } else {
                this.pH = (com.a.a.q.b) message.obj;
                if (this.pG != null) {
                    this.pG.a(this);
                }
                synchronized (this) {
                    notifyAll();
                }
                return true;
            }
        }

        public void close() {
            org.meteoroid.core.h.b(this);
        }

        public com.a.a.q.b fJ() {
            synchronized (this) {
                try {
                    wait(60000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (this.pH != null) {
                return this.pH;
            }
            throw new InterruptedIOException();
        }

        public com.a.a.q.b m(String str, String str2) {
            com.a.a.q.b kVar;
            if (str.equals(com.a.a.q.c.TEXT_MESSAGE)) {
                kVar = new b();
            } else if (str.equals(com.a.a.q.c.BINARY_MESSAGE)) {
                kVar = new a();
            } else if (str.equals(com.a.a.q.c.MULTIPART_MESSAGE)) {
                kVar = new k();
            } else {
                throw new IllegalArgumentException(str);
            }
            if (str2 != null) {
                kVar.setAddress(str2);
            }
            if (this.address != null) {
                kVar.setAddress(this.address);
            }
            return kVar;
        }
    }

    public static final class k implements com.a.a.q.f {
        private ArrayList<com.a.a.q.e> pJ = new ArrayList<>();
        private String pK;
        private ArrayList<String> pL = new ArrayList<>();
        private ArrayList<String> pM = new ArrayList<>();
        private ArrayList<String> pN = new ArrayList<>();
        private HashMap<String, String> pO = new HashMap<>();
        private String pP;
        private String pQ;

        public boolean a(com.a.a.q.e eVar) {
            return this.pJ.remove(eVar);
        }

        public String aA(String str) {
            return this.pO.get(str);
        }

        public com.a.a.q.e aB(String str) {
            Iterator<com.a.a.q.e> it = this.pJ.iterator();
            while (it.hasNext()) {
                com.a.a.q.e next = it.next();
                if (next.fM().equals(str)) {
                    return next;
                }
            }
            return null;
        }

        public void aC(String str) {
            if (str.equals("cc")) {
                this.pM.clear();
            } else if (str.equals("bcc")) {
                this.pN.clear();
            } else if (str.equals("to")) {
                this.pL.clear();
            } else {
                throw new IllegalArgumentException();
            }
        }

        public void aD(String str) {
            this.pP = str;
        }

        public String[] ax(String str) {
            if (str.equals("cc")) {
                return (String[]) this.pM.toArray();
            }
            if (str.equals("bcc")) {
                return (String[]) this.pN.toArray();
            }
            if (str.equals("to")) {
                return (String[]) this.pL.toArray();
            }
            throw new IllegalArgumentException();
        }

        public boolean ay(String str) {
            com.a.a.q.e eVar;
            Iterator<com.a.a.q.e> it = this.pJ.iterator();
            while (true) {
                if (!it.hasNext()) {
                    eVar = null;
                    break;
                }
                eVar = it.next();
                if (eVar.fM().equals(str)) {
                    break;
                }
            }
            if (eVar != null) {
                return a(eVar);
            }
            return false;
        }

        public boolean az(String str) {
            com.a.a.q.e eVar;
            Iterator<com.a.a.q.e> it = this.pJ.iterator();
            while (true) {
                if (!it.hasNext()) {
                    eVar = null;
                    break;
                }
                eVar = it.next();
                if (eVar.fN().equals(str)) {
                    break;
                }
            }
            if (eVar != null) {
                return a(eVar);
            }
            return false;
        }

        public void b(com.a.a.q.e eVar) {
            this.pJ.add(eVar);
        }

        public com.a.a.q.e[] fP() {
            return (com.a.a.q.e[]) this.pJ.toArray();
        }

        public String fQ() {
            return this.pP;
        }

        public void fR() {
            this.pM.clear();
            this.pN.clear();
            this.pL.clear();
        }

        public String getAddress() {
            return this.pK != null ? this.pK : this.pL.get(0);
        }

        public String getSubject() {
            return this.pQ;
        }

        public Date getTimestamp() {
            return new Date();
        }

        public boolean n(String str, String str2) {
            if (str.equals("cc")) {
                this.pM.add(str2);
            } else if (str.equals("bcc")) {
                this.pN.add(str2);
            } else if (str.equals("to")) {
                this.pL.add(str2);
            } else {
                throw new IllegalArgumentException();
            }
            return true;
        }

        public boolean o(String str, String str2) {
            if (str.equals("cc")) {
                return this.pM.remove(str2);
            }
            if (str.equals("bcc")) {
                return this.pN.remove(str2);
            }
            if (str.equals("to")) {
                return this.pL.remove(str2);
            }
            throw new IllegalArgumentException();
        }

        public void setAddress(String str) {
            this.pK = str;
        }

        public void setHeader(String str, String str2) {
            this.pO.put(str, str2);
        }

        public void setSubject(String str) {
            this.pQ = str;
        }
    }

    public static final class l implements com.a.a.c.p {
        private int mode;
        private Socket pR;

        public l(String str, int i, boolean z) {
            this.mode = i;
            try {
                if (!hC()) {
                    throw new IOException("No avaliable connection.");
                }
                URI uri = new URI(str);
                this.pR = new Socket(uri.getHost(), uri.getPort());
                Log.d("SocketConnection", "Connection " + uri.toString() + " established.");
            } catch (Exception e) {
                throw new IOException("URISyntaxException");
            }
        }

        private final boolean hC() {
            NetworkInfo activeNetworkInfo;
            try {
                ConnectivityManager hP = org.meteoroid.core.l.hP();
                return hP != null && (activeNetworkInfo = hP.getActiveNetworkInfo()) != null && activeNetworkInfo.isConnected() && activeNetworkInfo.getState() == NetworkInfo.State.CONNECTED;
            } catch (Exception e) {
                Log.v(PlayerListener.ERROR, e.toString());
            }
        }

        public void a(byte b, int i) {
            boolean z = false;
            switch (b) {
                case 0:
                    Socket socket = this.pR;
                    if (i != 0) {
                        z = true;
                    }
                    socket.setTcpNoDelay(z);
                    return;
                case 1:
                    Socket socket2 = this.pR;
                    if (i != 0) {
                        z = true;
                    }
                    socket2.setSoLinger(z, i);
                    return;
                case 2:
                    Socket socket3 = this.pR;
                    if (i != 0) {
                        z = true;
                    }
                    socket3.setKeepAlive(z);
                    return;
                case 3:
                    this.pR.setReceiveBufferSize(i);
                    return;
                case 4:
                    this.pR.setSendBufferSize(i);
                    return;
                default:
                    return;
            }
        }

        public DataInputStream aQ() {
            return new DataInputStream(aR());
        }

        public InputStream aR() {
            if (this.mode == 2) {
                throw new IOException("Connection is write only.");
            }
            Log.d("SocketConnection", "Connection " + this.pR.getInetAddress().getHostAddress() + " openInputStream.");
            return this.pR.getInputStream();
        }

        public DataOutputStream aS() {
            return new DataOutputStream(aT());
        }

        public OutputStream aT() {
            if (this.mode == 1) {
                throw new IOException("Connection is read only.");
            }
            Log.d("SocketConnection", "Connection " + this.pR.getInetAddress().getHostAddress() + " openOutputStream.");
            return this.pR.getOutputStream();
        }

        public String bi() {
            return this.pR.getLocalAddress().getHostAddress();
        }

        public void close() {
            this.pR.close();
            Log.d("SocketConnection", "Connection " + this.pR.getInetAddress().getHostAddress() + " close.");
            this.pR = null;
            System.gc();
        }

        public int d(byte b) {
            switch (b) {
                case 0:
                    return this.pR.getTcpNoDelay() ? 1 : 0;
                case 1:
                    return this.pR.getSoLinger();
                case 2:
                    return !this.pR.getKeepAlive() ? 0 : 1;
                case 3:
                    return this.pR.getReceiveBufferSize();
                case 4:
                    return this.pR.getSendBufferSize();
                default:
                    throw new IllegalArgumentException("Invalid socket option");
            }
        }

        public String getAddress() {
            return this.pR.getInetAddress().getHostAddress();
        }

        public int getLocalPort() {
            return this.pR.getLocalPort();
        }

        public int getPort() {
            return this.pR.getPort();
        }
    }

    public static final class m implements VolumeControl {
        private g.a pz;

        public m(g.a aVar) {
            this.pz = aVar;
        }

        public int bl(int i) {
            int i2 = 100;
            int i3 = i < 0 ? 0 : i;
            if (i3 <= 100) {
                i2 = i3;
            }
            this.pz.nu = i2;
            org.meteoroid.core.g.cc(i2);
            Log.d("MIDPVolume", "Volume is set to " + i2);
            return i2;
        }

        public int getLevel() {
            return this.pz.nu;
        }

        public boolean isMuted() {
            return this.pz.nv;
        }

        public void r(boolean z) {
            this.pz.nv = z;
            org.meteoroid.core.g.v(z);
            Log.d("MIDPVolume", "Volume mute is set to " + z);
        }
    }

    private boolean R(int i2, int i3) {
        if (!this.os) {
            return true;
        }
        if (this.oH == 0) {
            this.oH = getWidth() / 60;
        }
        if (this.oI == 0) {
            this.oI = getHeight() / 60;
        }
        return Math.abs(i2 - this.oF) >= this.oH || Math.abs(i3 - this.oG) >= this.oI;
    }

    private final void S(int i2, int i3) {
        int co = co(i3);
        if (co == -1) {
            return;
        }
        if (i2 == 0) {
            this.oJ = (1 << co) | this.oJ;
            return;
        }
        this.oJ = (1 << co) ^ this.oJ;
    }

    /* access modifiers changed from: private */
    public final int cm(int i2) {
        switch (i2) {
            case 4:
                if (org.meteoroid.core.l.gv() == null || !org.meteoroid.core.l.gv().startsWith("R800")) {
                    return 0;
                }
                return ot.get("NUM_5").intValue();
            case 7:
            case com.a.a.i.a.TITLE:
                return ot.get("NUM_0").intValue();
            case 8:
            case com.a.a.e.c.KEY_NUM3:
                return ot.get("NUM_1").intValue();
            case 9:
            case CharsToNameCanonicalizer.HASH_MULT:
                return ot.get("NUM_2").intValue();
            case 10:
            case 46:
                return ot.get("NUM_3").intValue();
            case 11:
            case 47:
                return ot.get("NUM_4").intValue();
            case 12:
            case 32:
                return ot.get("NUM_5").intValue();
            case 13:
            case 34:
                return ot.get("NUM_6").intValue();
            case 14:
            case com.a.a.e.c.KEY_NUM6:
            case 99:
                return ot.get("NUM_7").intValue();
            case 15:
            case com.a.a.e.c.KEY_NUM4:
                return ot.get("NUM_8").intValue();
            case 16:
            case 31:
            case 100:
                return ot.get("NUM_9").intValue();
            case 17:
            case 61:
            case 102:
                return ot.get(ol[11]).intValue();
            case 18:
            case com.a.a.e.c.KEY_NUM7:
            case 103:
                return ot.get(ol[12]).intValue();
            case 19:
                return ot.get(ol[0]).intValue();
            case com.a.a.b.c.INT_16:
                return ot.get(ol[1]).intValue();
            case 21:
                return ot.get(ol[2]).intValue();
            case 22:
                return ot.get(ol[3]).intValue();
            case 23:
                return ot.get(ol[4]).intValue();
            case 44:
            case 86:
            case 88:
            case 93:
            case 108:
            case 186:
                return cn(2);
            case 45:
            case 85:
            case 87:
            case 92:
            case com.a.a.i.a.ORG:
            case 183:
                return cn(1);
            case com.a.a.p.c.TYPE:
                return ot.get(ol[9]).intValue();
            default:
                return 0;
        }
    }

    private final int co(int i2) {
        if (i2 == cn(1)) {
            return 17;
        }
        if (i2 == cn(2)) {
            return 18;
        }
        switch (ap(i2)) {
            case 1:
                return 12;
            case 2:
                return 13;
            case 5:
                return 14;
            case 6:
                return 15;
            case 8:
                return 16;
            case com.a.a.e.c.KEY_POUND:
                return 11;
            case com.a.a.e.c.KEY_STAR:
                return 10;
            case 48:
                return 0;
            case com.a.a.e.c.KEY_NUM1:
                return 1;
            case 50:
                return 2;
            case com.a.a.e.c.KEY_NUM3:
                return 3;
            case com.a.a.e.c.KEY_NUM4:
                return 4;
            case com.a.a.e.c.KEY_NUM5:
                return 5;
            case com.a.a.e.c.KEY_NUM6:
                return 6;
            case com.a.a.e.c.KEY_NUM7:
                return 7;
            case 56:
                return 8;
            case com.a.a.e.c.KEY_NUM9:
                return 9;
            default:
                return -1;
        }
    }

    private final void iz() {
        final HashMap hashMap = new HashMap();
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(org.meteoroid.core.l.getActivity().getAssets().open(com.a.a.s.e.bE(new String(com.a.a.s.a.decode("TUVUQS1JTkYvTUFOSUZFU1QuTUY=")))), com.umeng.common.b.e.f));
            Log.d(getName(), "Start to analyze META-INF/MANIFEST.MF.");
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                readLine.trim();
                int indexOf = readLine.indexOf(58);
                if (indexOf > -1) {
                    String trim = readLine.substring(0, indexOf).trim();
                    String trim2 = readLine.substring(indexOf + 1).trim();
                    if (Pattern.compile("\\bMIDlet-\\d").matcher(readLine).find()) {
                        int indexOf2 = trim2.indexOf(44);
                        int lastIndexOf = trim2.lastIndexOf(44);
                        if (indexOf2 <= -1 || lastIndexOf <= -1) {
                            Log.w(getName(), "The midlet " + trim + ":" + trim2 + " where p1=" + indexOf2 + " p2=" + lastIndexOf);
                        } else {
                            String trim3 = trim2.substring(0, indexOf2).trim();
                            trim2.substring(indexOf2 + 1, lastIndexOf).trim();
                            hashMap.put(trim3, trim2.substring(lastIndexOf + 1).trim());
                            Log.d(getName(), "The midlet " + readLine + " has added.");
                        }
                    } else {
                        org.meteoroid.core.a.x(trim, trim2);
                    }
                }
            }
            if (hashMap.isEmpty()) {
                Log.w(getName(), "No midlets found in MANIFEST.MF.");
                org.meteoroid.core.l.j(org.meteoroid.core.l.getString(R.string.no_midlet_found), 1);
            } else if (hashMap.size() == 1) {
                org.meteoroid.core.a.ba((String) hashMap.values().toArray()[0]);
            } else {
                final String[] strArr = new String[hashMap.keySet().size()];
                hashMap.keySet().toArray(strArr);
                final AlertDialog.Builder builder = new AlertDialog.Builder(org.meteoroid.core.l.getActivity());
                builder.setItems(strArr, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        org.meteoroid.core.a.ba((String) hashMap.get(strArr[i]));
                    }
                });
                builder.setCancelable(false);
                org.meteoroid.core.l.getHandler().post(new Runnable() {
                    public void run() {
                        builder.create().show();
                    }
                });
            }
        } catch (IOException e2) {
            Log.w(getName(), "MANIFEST.MF may not exist or invalid.");
            org.meteoroid.core.l.j(org.meteoroid.core.l.getString(R.string.no_midlet_found), 1);
        }
    }

    public final boolean T(int i2, int i3) {
        if (i3 == 0) {
            return false;
        }
        switch (i2) {
            case 0:
                if (!cp(i3)) {
                    com.a.a.e.j.a((MIDlet) null).af(i3);
                    Log.d(getName(), "Dispatch key event type: DOWN [" + i3 + "]");
                } else if (this.on) {
                    com.a.a.e.j.a((MIDlet) null).ay(i3);
                    Log.d(getName(), "Dispatch key event type: REPEAT [" + i3 + "]");
                }
                S(i2, i3);
                return true;
            case 1:
                if (cp(i3)) {
                    com.a.a.e.j.a((MIDlet) null).ag(i3);
                    Log.d(getName(), "Dispatch key event type: UP [" + i3 + "]");
                    S(i2, i3);
                }
                return true;
            default:
                Log.d(getName(), "Unkown key event type:" + i2 + "[" + i3 + "]");
                return false;
        }
    }

    public final Matrix a(int i2, Matrix matrix) {
        switch (i2) {
            case 0:
                break;
            case 1:
                matrix.preScale(-1.0f, 1.0f);
                matrix.preRotate(-180.0f);
                break;
            case 2:
                matrix.preScale(-1.0f, 1.0f);
                break;
            case 3:
                matrix.preRotate(180.0f);
                break;
            case 4:
                matrix.preScale(-1.0f, 1.0f);
                matrix.preRotate(-270.0f);
                break;
            case 5:
                matrix.preRotate(90.0f);
                break;
            case 6:
                matrix.preRotate(270.0f);
                break;
            case 7:
                matrix.preScale(-1.0f, 1.0f);
                matrix.preRotate(-90.0f);
                break;
            default:
                throw new IllegalArgumentException("Bad transform");
        }
        return matrix;
    }

    public com.a.a.c.b a(String str, int i2, boolean z) {
        Log.d(getName(), "Connector open " + str + ".");
        String trim = str.trim();
        if (trim.startsWith("http:")) {
            return new f(trim, i2, z);
        }
        if (trim.startsWith("sms:")) {
            return new j(trim);
        }
        if (trim.startsWith("socket:")) {
            return new l(trim, i2, z);
        }
        if (trim.startsWith("file:")) {
            return new b(trim, i2);
        }
        if (trim.indexOf("MACadd") != -1) {
            return new g(trim);
        }
        if (trim.startsWith("btl2cap://") || trim.startsWith("btspp://")) {
            return new h(trim);
        }
        throw new IOException("Unkown protocol:" + trim);
    }

    public void a(int i2, float f2, float f3, int i3) {
        if (this.or || i3 == 0) {
            org.meteoroid.core.h.c(org.meteoroid.core.h.c(com.a.a.r.a.MSG_DEVICE_TOUCH_EVENT, new int[]{i2, (int) f2, (int) f3, i3}));
        }
    }

    public void a(AttributeSet attributeSet, String str) {
        setTouchable(attributeSet.getAttributeBooleanValue(str, "touchable", false));
        this.oo = isTouchable();
        C(attributeSet.getAttributeBooleanValue(str, "filter", true));
        String attributeValue = attributeSet.getAttributeValue(str, "origrect");
        if (attributeValue != null) {
            this.ry = com.a.a.s.e.bH(attributeValue);
        }
        String attributeValue2 = attributeSet.getAttributeValue(str, "rect");
        if (attributeValue2 != null) {
            a(com.a.a.s.e.bH(attributeValue2));
        }
    }

    public void a(com.a.a.r.c cVar) {
        if (!n.of) {
            if (this.oq) {
                this.ou = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
                this.ov = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
                this.oz = b(this.ou);
                this.oA = b(this.ov);
                this.oy = this.oz;
                this.ow = this.ov;
            } else {
                this.ou = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
                this.ov = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.RGB_565);
                this.ow = this.ov;
                this.ox = new Canvas(this.ow);
                this.oy = b(this.ou);
            }
        }
        jt();
        if (this.om) {
            n.is();
        }
        Log.d(getName(), "Buffer and graphics are ready .");
        com.a.a.e.j.a((MIDlet) null).p(getWidth(), getHeight());
    }

    public void a(Properties properties) {
        try {
            if (properties.containsKey("screen.width")) {
                this.width = Integer.parseInt(properties.getProperty("screen.width"));
                Log.d(getName(), "Set screen width " + this.width);
            }
            if (properties.containsKey("screen.height")) {
                this.height = Integer.parseInt(properties.getProperty("screen.height"));
                Log.d(getName(), "Set screen height " + this.height);
            }
            if (properties.containsKey("font.size.large")) {
                d.SIZE_LARGE = Integer.parseInt(properties.getProperty("font.size.large"));
                Log.d(getName(), "Set font.size.large " + d.SIZE_LARGE);
            }
            if (properties.containsKey("font.size.medium")) {
                d.SIZE_MEDIUM = Integer.parseInt(properties.getProperty("font.size.medium"));
                Log.d(getName(), "Set font.size.medium " + d.SIZE_MEDIUM);
            }
            if (properties.containsKey("font.size.small")) {
                d.SIZE_SMALL = Integer.parseInt(properties.getProperty("font.size.small"));
                Log.d(getName(), "Set font.size.small " + d.SIZE_SMALL);
            }
            for (int i2 = 0; i2 < 10; i2++) {
                if (properties.containsKey("key." + i2)) {
                    ot.put("NUM_" + i2, Integer.valueOf(Integer.parseInt(properties.getProperty("key." + i2).trim())));
                }
            }
            for (int i3 = 0; i3 < ol.length; i3++) {
                if (properties.containsKey("key." + ol[i3])) {
                    ot.put(ol[i3], Integer.valueOf(Integer.parseInt(properties.getProperty("key." + ol[i3]).trim())));
                }
            }
            for (Object next : properties.keySet()) {
                System.setProperty((String) next, properties.getProperty((String) next));
            }
        } catch (Exception e2) {
            Log.w(getName(), "Device property exception. " + e2);
        }
        this.on = f("hasRepeatEvents", false);
        this.op = f("hasPointerMotionEvents", true);
        this.oq = f("isDoubleBuffered", true);
        this.om = f("isPositiveUpdate", false);
        this.or = f("enableMultiTouch", false);
        this.os = f("fixTouchPosition", false);
    }

    public final int ap(int i2) {
        if (ot.containsKey(ol[0]) && i2 == ot.get(ol[0]).intValue()) {
            return 1;
        }
        if (ot.containsKey(ol[1]) && i2 == ot.get(ol[1]).intValue()) {
            return 6;
        }
        if (ot.containsKey(ol[2]) && i2 == ot.get(ol[2]).intValue()) {
            return 2;
        }
        if (ot.containsKey(ol[3]) && i2 == ot.get(ol[3]).intValue()) {
            return 5;
        }
        if (ot.containsKey(ol[4]) && i2 == ot.get(ol[4]).intValue()) {
            return 8;
        }
        if (ot.containsKey(ol[5]) && i2 == ot.get(ol[5]).intValue()) {
            return 9;
        }
        if (ot.containsKey(ol[6]) && i2 == ot.get(ol[6]).intValue()) {
            return 10;
        }
        if (ot.containsKey(ol[7]) && i2 == ot.get(ol[7]).intValue()) {
            return 11;
        }
        if (ot.containsKey(ol[12]) && i2 == ot.get(ol[12]).intValue()) {
            return 35;
        }
        if (ot.containsKey(ol[11]) && i2 == ot.get(ol[11]).intValue()) {
            return 42;
        }
        if (ot.containsKey("NUM_0") && i2 == ot.get("NUM_0").intValue()) {
            return 48;
        }
        if (ot.containsKey("NUM_1") && i2 == ot.get("NUM_1").intValue()) {
            return 49;
        }
        if (ot.containsKey("NUM_2") && i2 == ot.get("NUM_2").intValue()) {
            return 50;
        }
        if (ot.containsKey("NUM_3") && i2 == ot.get("NUM_3").intValue()) {
            return 51;
        }
        if (ot.containsKey("NUM_4") && i2 == ot.get("NUM_4").intValue()) {
            return 52;
        }
        if (ot.containsKey("NUM_5") && i2 == ot.get("NUM_5").intValue()) {
            return 53;
        }
        if (ot.containsKey("NUM_6") && i2 == ot.get("NUM_6").intValue()) {
            return 54;
        }
        if (ot.containsKey("NUM_7") && i2 == ot.get("NUM_7").intValue()) {
            return 55;
        }
        if (!ot.containsKey("NUM_8") || i2 != ot.get("NUM_8").intValue()) {
            return (!ot.containsKey("NUM_9") || i2 != ot.get("NUM_9").intValue()) ? 0 : 57;
        }
        return 56;
    }

    public final int aq(int i2) {
        return (i2 < 0 || i2 > 8) ? i2 : i2 == 1 ? ot.get(ol[0]).intValue() : i2 == 6 ? ot.get(ol[1]).intValue() : i2 == 2 ? ot.get(ol[2]).intValue() : i2 == 5 ? ot.get(ol[3]).intValue() : i2 == 8 ? ot.get(ol[4]).intValue() : i2 == 9 ? ot.get(ol[5]).intValue() : i2 == 10 ? ot.get(ol[6]).intValue() : i2 == 11 ? ot.get(ol[7]).intValue() : i2 == 12 ? ot.get(ol[8]).intValue() : i2;
    }

    public final String ar(int i2) {
        if (i2 == ot.get(ol[0]).intValue()) {
            return ol[0];
        }
        if (i2 == ot.get(ol[1]).intValue()) {
            return ol[1];
        }
        if (i2 == ot.get(ol[2]).intValue()) {
            return ol[2];
        }
        if (i2 == ot.get(ol[3]).intValue()) {
            return ol[3];
        }
        if (i2 == ot.get(ol[4]).intValue()) {
            return ol[4];
        }
        switch (i2) {
            case com.a.a.e.c.KEY_POUND:
                return "KEY_POUND";
            case 36:
            case 37:
            case 38:
            case 39:
            case com.a.a.b.c.BOOL:
            case 41:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            default:
                return "";
            case com.a.a.e.c.KEY_STAR:
                return "KEY_STAR";
            case 48:
                return "KEY_NUM0";
            case com.a.a.e.c.KEY_NUM1:
                return "KEY_NUM1";
            case 50:
                return "KEY_NUM2";
            case com.a.a.e.c.KEY_NUM3:
                return "KEY_NUM3";
            case com.a.a.e.c.KEY_NUM4:
                return "KEY_NUM4";
            case com.a.a.e.c.KEY_NUM5:
                return "KEY_NUM5";
            case com.a.a.e.c.KEY_NUM6:
                return "KEY_NUM6";
            case com.a.a.e.c.KEY_NUM7:
                return "KEY_NUM7";
            case 56:
                return "KEY_NUM8";
            case com.a.a.e.c.KEY_NUM9:
                return "KEY_NUM9";
        }
    }

    public e b(Bitmap bitmap) {
        return new e(new Canvas(bitmap));
    }

    public boolean b(Message message) {
        if (message.what == 47872) {
            iz();
            return true;
        } else if (message.what == 44036) {
            org.meteoroid.core.h.ci(org.meteoroid.core.l.MSG_SYSTEM_EXIT);
            return true;
        } else if (message.what != -2023686143) {
            return f(message);
        } else {
            org.meteoroid.core.l.bp((String) message.obj);
            org.meteoroid.core.h.e(MIDlet.MIDLET_PLATFORM_REQUEST_FINISH, Boolean.FALSE);
            return true;
        }
    }

    public boolean b(KeyEvent keyEvent) {
        if (!this.oE) {
            org.meteoroid.core.l.hS().schedule(new a(), 500, 700);
            this.oE = true;
        }
        Log.d(getName(), "Native key event:" + keyEvent.getAction() + "[" + keyEvent.getKeyCode() + "]");
        if (keyEvent.getAction() == 1) {
            this.oD = keyEvent.getKeyCode();
            this.oC = -1;
        } else if (keyEvent.getAction() == 0) {
            if (this.oC == -1 && this.oD == -1) {
                T(0, cm(keyEvent.getKeyCode()));
            }
            this.oC = keyEvent.getKeyCode();
        }
        return false;
    }

    public boolean bA() {
        return this.on;
    }

    public boolean bB() {
        return this.oq;
    }

    public String bt(String str) {
        Log.d(getName(), "Get device property:" + str);
        return oB.getProperty(str);
    }

    public int bv(String str) {
        return ot.containsKey(str) ? ot.get(str).intValue() : z.CONSTRAINT_MASK;
    }

    public boolean by() {
        return this.oo;
    }

    public boolean bz() {
        return this.op;
    }

    public final o cD() {
        return this.oy;
    }

    public int cn(int i2) {
        if (i2 == 1) {
            return ot.get(ol[9]).intValue();
        }
        if (i2 == 2) {
            return ot.get(ol[10]).intValue();
        }
        return 0;
    }

    public final boolean cp(int i2) {
        return ((1 << co(i2)) & this.oJ) != 0;
    }

    public boolean f(Message message) {
        switch (message.what) {
            case MSG_MIDP_COMMAND_EVENT /*44034*/:
                return f((com.a.a.e.f) message.obj);
            case com.a.a.r.a.MSG_DEVICE_TOUCH_EVENT:
                return j((int[]) message.obj);
            case VirtualKey.MSG_VIRTUAL_KEY_EVENT /*7833601*/:
                return T(((VirtualKey) message.obj).getKeyState(), bv(((VirtualKey) message.obj).jv()));
            default:
                return false;
        }
    }

    public boolean f(com.a.a.e.f fVar) {
        com.a.a.e.j.a((MIDlet) null).a(fVar);
        return true;
    }

    public boolean f(String str, boolean z) {
        String bt = bt(str);
        return bt == null ? z : Boolean.parseBoolean(bt);
    }

    public int getHeight() {
        return this.height == -1 ? jq().height() : this.height;
    }

    public String getName() {
        return "MIDPDevice";
    }

    public int getWidth() {
        return this.width == -1 ? jq().width() : this.width;
    }

    public boolean hj() {
        return true;
    }

    public final int iA() {
        return this.oJ;
    }

    public void ix() {
        if (this.oq) {
            if (this.ow == this.ou) {
                this.ow = this.ov;
                this.oy = this.oz;
            } else {
                this.ow = this.ou;
                this.oy = this.oA;
            }
        } else if (this.ou != null && !this.ou.isRecycled()) {
            this.ox.drawBitmap(this.ou, 0.0f, 0.0f, (Paint) null);
        }
        if (this.oy != null) {
            this.oy.cC();
        }
        if (this.om) {
            unlock();
        } else {
            n.iu();
        }
    }

    public final Bitmap iy() {
        return this.ow;
    }

    public boolean j(int[] iArr) {
        Log.d(getName(), "action[" + iArr[0] + "]" + "x:" + iArr[1] + " y:" + iArr[2]);
        switch (iArr[0]) {
            case 0:
                if (!this.oo) {
                    return true;
                }
                com.a.a.e.j.a((MIDlet) null).l(iArr[1], iArr[2]);
                this.oF = iArr[1];
                this.oG = iArr[2];
                return true;
            case 1:
                if (!this.oo) {
                    return true;
                }
                com.a.a.e.j.a((MIDlet) null).m(iArr[1], iArr[2]);
                return true;
            case 2:
                if (!this.oo || !this.op || !R(iArr[1], iArr[2])) {
                    return true;
                }
                com.a.a.e.j.a((MIDlet) null).o(iArr[1], iArr[2]);
                return true;
            default:
                return false;
        }
    }

    public int k(String str, int i2) {
        String bt = bt(str);
        return bt == null ? i2 : Integer.parseInt(bt);
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void lock() {
        /*
            r2 = this;
            monitor-enter(r2)
            r0 = 100
            r2.wait(r0)     // Catch:{ InterruptedException -> 0x0008, all -> 0x000a }
        L_0x0006:
            monitor-exit(r2)
            return
        L_0x0008:
            r0 = move-exception
            goto L_0x0006
        L_0x000a:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.meteoroid.plugin.device.MIDPDevice.lock():void");
    }

    public void onCreate() {
        org.meteoroid.core.h.h(com.a.a.r.a.MSG_DEVICE_TOUCH_EVENT, "MSG_DEVICE_TOUCH_EVENT");
        org.meteoroid.core.h.h(MSG_MIDP_COMMAND_EVENT, "MSG_MIDP_COMMAND_EVENT");
        org.meteoroid.core.h.h(MSG_MIDP_DISPLAY_CALL_SERIALLY, "MSG_MIDP_DISPLAY_CALL_SERIALLY");
        org.meteoroid.core.h.h(com.a.a.r.a.MSG_DEVICE_REQUEST_REFRESH, "MSG_DEVICE_REQUEST_REFRESH");
        org.meteoroid.core.h.a(this);
        oB.clear();
        try {
            oB.load(org.meteoroid.core.l.getActivity().getResources().openRawResource(com.a.a.s.e.bG(com.a.a.m.m.CONTEXT_TYPE_DEVICE)));
        } catch (IOException e2) {
            Log.e(getName(), "device.properties not exist or valid." + e2);
        }
        a(oB);
        org.meteoroid.core.f.a((f.a) this);
        org.meteoroid.core.f.a((f.C0005f) this);
        System.gc();
    }

    public void onDestroy() {
        if (this.ou != null) {
            this.ou.recycle();
        }
        this.ou = null;
        if (this.ov != null) {
            this.ov.recycle();
        }
        this.ov = null;
        if (this.ow != null) {
            this.ow.recycle();
        }
        this.ow = null;
        this.oy = null;
        if (this.oN) {
        }
    }

    public void onDraw(Canvas canvas) {
        if (this.ow != null && !this.ow.isRecycled()) {
            canvas.drawBitmap(this.ow, this.ry, jq(), this.rx);
        }
        if (this.om) {
            lock();
        }
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        Log.d(getName(), "Trackball event.");
        return false;
    }

    public void setVisible(boolean z) {
    }

    public synchronized void unlock() {
        notify();
    }
}
