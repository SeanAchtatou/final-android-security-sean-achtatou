package org.meteoroid.core;

import android.app.Activity;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.SystemClock;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import java.util.Iterator;
import java.util.LinkedHashSet;

public final class f extends GestureDetector.SimpleOnGestureListener implements View.OnKeyListener, View.OnTouchListener {
    public static final String LOG_TAG = "InputManager";
    private static final f mX = new f();
    private static final LinkedHashSet<a> mZ = new LinkedHashSet<>();
    private static final LinkedHashSet<b> na = new LinkedHashSet<>();
    private static final LinkedHashSet<C0005f> nb = new LinkedHashSet<>();
    /* access modifiers changed from: private */
    public static final LinkedHashSet<e> nc = new LinkedHashSet<>();
    /* access modifiers changed from: private */
    public static final LinkedHashSet<d> nd = new LinkedHashSet<>();
    private SensorManager lo;
    /* access modifiers changed from: private */
    public GestureDetector mY;

    public interface a {
        boolean b(KeyEvent keyEvent);
    }

    public interface b {
        boolean o(int i, int i2, int i3, int i4);
    }

    private final class c implements View.OnTouchListener {
        private c() {
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            SystemClock.sleep(34);
            if (!f.nc.isEmpty()) {
                f.this.mY.onTouchEvent(motionEvent);
            }
            if (!f.nd.isEmpty()) {
                Iterator it = f.nd.iterator();
                while (it.hasNext()) {
                    ((d) it.next()).a(view, motionEvent);
                }
            }
            int action = motionEvent.getAction();
            int i = action & 255;
            int i2 = action >> 8;
            if (i == 6) {
                return f.o(1, (int) motionEvent.getX(i2), (int) motionEvent.getY(i2), i2);
            }
            if (i == 5) {
                return f.o(0, (int) motionEvent.getX(i2), (int) motionEvent.getY(i2), i2);
            }
            int pointerCount = motionEvent.getPointerCount();
            boolean z = false;
            for (int i3 = 0; i3 < pointerCount; i3++) {
                if (f.o(action, (int) motionEvent.getX(i3), (int) motionEvent.getY(i3), i3)) {
                    z = true;
                }
            }
            return z;
        }
    }

    public interface d {
        void a(View view, MotionEvent motionEvent);
    }

    public interface e {
        public static final int GESTURE_SLIDE_DOWN = 2;
        public static final int GESTURE_SLIDE_LEFT = 3;
        public static final int GESTURE_SLIDE_RIGHT = 4;
        public static final int GESTURE_SLIDE_UP = 1;

        boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2);
    }

    /* renamed from: org.meteoroid.core.f$f  reason: collision with other inner class name */
    public interface C0005f {
        boolean onTrackballEvent(MotionEvent motionEvent);
    }

    public static final void a(SensorEventListener sensorEventListener) {
        mX.lo.registerListener(sensorEventListener, mX.lo.getDefaultSensor(1), 3);
    }

    /* JADX WARNING: Removed duplicated region for block: B:5:0x0015  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void a(android.view.KeyEvent r2) {
        /*
            java.util.LinkedHashSet<org.meteoroid.core.f$a> r0 = org.meteoroid.core.f.mZ
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x0009
        L_0x0008:
            return
        L_0x0009:
            java.util.LinkedHashSet<org.meteoroid.core.f$a> r0 = org.meteoroid.core.f.mZ
            java.util.Iterator r1 = r0.iterator()
        L_0x000f:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x0008
            java.lang.Object r0 = r1.next()
            org.meteoroid.core.f$a r0 = (org.meteoroid.core.f.a) r0
            boolean r0 = r0.b(r2)
            if (r0 == 0) goto L_0x000f
            goto L_0x0008
        */
        throw new UnsupportedOperationException("Method not decompiled: org.meteoroid.core.f.a(android.view.KeyEvent):void");
    }

    protected static void a(View view) {
        View.OnTouchListener onTouchListener;
        if (!m.nV) {
            view.setOnKeyListener(mX);
        }
        if (l.hU() >= 5) {
            f fVar = mX;
            fVar.getClass();
            onTouchListener = new c();
        } else {
            onTouchListener = mX;
        }
        view.setOnTouchListener(onTouchListener);
    }

    public static final void a(a aVar) {
        mZ.add(aVar);
    }

    public static final void a(b bVar) {
        na.add(bVar);
    }

    public static final void a(d dVar) {
        nd.add(dVar);
    }

    public static final void a(e eVar) {
        nc.add(eVar);
    }

    public static final void a(C0005f fVar) {
        nb.add(fVar);
    }

    public static final void b(SensorEventListener sensorEventListener) {
        mX.lo.unregisterListener(sensorEventListener);
    }

    protected static void b(View view) {
        view.setOnKeyListener(null);
        view.setOnTouchListener(null);
    }

    public static final void b(a aVar) {
        mZ.remove(aVar);
    }

    public static final void b(b bVar) {
        na.remove(bVar);
    }

    public static final void b(d dVar) {
        nd.remove(dVar);
    }

    public static final void b(e eVar) {
        nc.remove(eVar);
    }

    public static final void b(C0005f fVar) {
        nb.remove(fVar);
    }

    protected static void d(Activity activity) {
        mX.mY = new GestureDetector(activity, mX);
        mX.lo = (SensorManager) activity.getSystemService("sensor");
    }

    public static final boolean o(int i, int i2, int i3, int i4) {
        Iterator<b> it = na.iterator();
        while (it.hasNext()) {
            if (it.next().o(i, i2, i3, i4)) {
                return true;
            }
        }
        return false;
    }

    protected static void onDestroy() {
        nc.clear();
        na.clear();
        nb.clear();
        mZ.clear();
    }

    public static boolean onTrackballEvent(MotionEvent motionEvent) {
        if (nb.isEmpty()) {
            return false;
        }
        Iterator<C0005f> it = nb.iterator();
        while (it.hasNext()) {
            if (it.next().onTrackballEvent(motionEvent)) {
                return true;
            }
        }
        return false;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        Iterator<e> it = nc.iterator();
        while (it.hasNext()) {
            if (it.next().onFling(motionEvent, motionEvent2, f, f2)) {
                return true;
            }
        }
        return false;
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (mZ.isEmpty()) {
            return false;
        }
        Iterator<a> it = mZ.iterator();
        while (it.hasNext()) {
            if (it.next().b(keyEvent)) {
                return true;
            }
        }
        return false;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        SystemClock.sleep(34);
        if (!nc.isEmpty()) {
            this.mY.onTouchEvent(motionEvent);
        }
        if (!nd.isEmpty()) {
            Iterator<d> it = nd.iterator();
            while (it.hasNext()) {
                it.next().a(view, motionEvent);
            }
        }
        return o(motionEvent.getAction(), (int) motionEvent.getX(), (int) motionEvent.getY(), 0);
    }
}
