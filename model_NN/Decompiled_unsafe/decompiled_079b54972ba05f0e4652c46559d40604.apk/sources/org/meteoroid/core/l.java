package org.meteoroid.core;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.os.Vibrator;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import com.a.a.i.k;
import com.a.a.m.m;
import com.a.a.s.e;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Properties;
import java.util.Timer;
import org.meteoroid.core.h;
import xsyjrxgdnxnw.zy_rlw.R;

public final class l {
    public static final String LOG_TAG = "SystemManager";
    public static final int MSG_SYSTEM_ACTIVITY_RESULT = 47880;
    public static final int MSG_SYSTEM_DEVICE_INIT_COMPLETE = 47878;
    public static final int MSG_SYSTEM_EXIT = 47875;
    public static final int MSG_SYSTEM_FEATURE_ADDED_COMPLETE = 47876;
    public static final int MSG_SYSTEM_FUNCTION_REQUEST = 47882;
    public static final int MSG_SYSTEM_GRAPHICS_INIT_COMPLETE = 47877;
    public static final int MSG_SYSTEM_INIT_COMPLETE = 47872;
    public static final int MSG_SYSTEM_LOG_APPENDER = 47886;
    public static final int MSG_SYSTEM_LOG_EVENT = 47887;
    public static final int MSG_SYSTEM_LOG_EVENT_BY_APPENDER = 47902;
    public static final int MSG_SYSTEM_NOTIFY_EXIT = 47881;
    public static final int MSG_SYSTEM_ONLINE_PARAM = 47885;
    public static final int MSG_SYSTEM_ON_PAUSE = 47873;
    public static final int MSG_SYSTEM_ON_RESUME = 47874;
    public static final int MSG_SYSTEM_VD_INIT_COMPLETE = 47879;
    private static Handler handler;
    /* access modifiers changed from: private */
    public static Activity nK;
    private static int nL = 0;
    public static boolean nM = false;
    /* access modifiers changed from: private */
    public static boolean nN = false;
    private static final Timer nO = new Timer();

    public static boolean aI(String str) {
        try {
            return nK.getPackageManager().getApplicationInfo(str, 0) != null;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static boolean aJ(String str) {
        PackageManager packageManager = nK.getPackageManager();
        try {
            PackageInfo packageInfo = packageManager.getPackageInfo(str, 0);
            Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
            intent.addCategory("android.intent.category.LAUNCHER");
            intent.setPackage(packageInfo.packageName);
            ResolveInfo next = packageManager.queryIntentActivities(intent, 0).iterator().next();
            if (next != null) {
                String str2 = next.activityInfo.name;
                Intent intent2 = new Intent("android.intent.action.MAIN");
                intent2.addCategory("android.intent.category.LAUNCHER");
                intent2.setComponent(new ComponentName(str, str2));
                nK.startActivity(intent2);
            }
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static final InputStream bl(String str) {
        InputStream inputStream = null;
        try {
            inputStream = nK.getAssets().open(str);
        } catch (Exception e) {
            Log.w(LOG_TAG, "Can't load resource:" + str + " is not exist.");
            if (nM) {
                throw new IOException();
            }
        }
        Log.d(LOG_TAG, "Load assert " + str + (inputStream != null ? " success." : " failed."));
        return inputStream;
    }

    public static final byte[] bm(String str) {
        InputStream bl = bl(str);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[1024];
        while (true) {
            int read = bl.read(bArr);
            if (read == -1) {
                return byteArrayOutputStream.toByteArray();
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    public static String bn(String str) {
        try {
            return nK.getPackageManager().getApplicationInfo(nK.getPackageName(), 128).metaData.getString(str);
        } catch (PackageManager.NameNotFoundException e) {
            Log.w(LOG_TAG, e.getMessage() + " unknown meta key or not exist:" + str);
            return null;
        }
    }

    public static int bo(String str) {
        String str2 = null;
        try {
            str2 = bn(str);
            return Integer.parseInt(str2);
        } catch (Exception e) {
            Log.w(LOG_TAG, e.getMessage() + " wrong trans:" + str + " with " + str2);
            return 0;
        }
    }

    public static boolean bp(String str) {
        h.c(h.c(MSG_SYSTEM_FUNCTION_REQUEST, str));
        return true;
    }

    public static boolean bq(final String str) {
        if (str.startsWith("http://") || str.startsWith("market://")) {
            getHandler().post(new Runnable() {
                public void run() {
                    l.nK.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                }
            });
            return true;
        } else if (str.startsWith("tel:")) {
            getHandler().post(new Runnable() {
                public void run() {
                    l.nK.startActivity(new Intent("android.intent.action.DIAL", Uri.parse(str)));
                }
            });
            return true;
        } else {
            Log.w(LOG_TAG, "Not supported " + str);
            return false;
        }
    }

    public static void ck(int i) {
        ((Vibrator) nK.getSystemService("vibrator")).vibrate((long) i);
    }

    public static void cl(int i) {
        nK.setRequestedOrientation(i);
    }

    protected static void d(Activity activity) {
        nK = activity;
        h.h(MSG_SYSTEM_INIT_COMPLETE, "MSG_SYSTEM_INIT_COMPLETE");
        h.h(MSG_SYSTEM_ON_PAUSE, "MSG_SYSTEM_ON_PAUSE");
        h.h(MSG_SYSTEM_FEATURE_ADDED_COMPLETE, "MSG_SYSTEM_FEATURE_ADDED_COMPLETE");
        h.h(MSG_SYSTEM_GRAPHICS_INIT_COMPLETE, "MSG_SYSTEM_GRAPHICS_INIT_COMPLETE");
        h.h(MSG_SYSTEM_DEVICE_INIT_COMPLETE, "MSG_SYSTEM_DEVICE_INIT_COMPLETE");
        h.h(MSG_SYSTEM_VD_INIT_COMPLETE, "MSG_SYSTEM_VD_INIT_COMPLETE");
        h.h(MSG_SYSTEM_ON_PAUSE, "MSG_SYSTEM_ON_PAUSE");
        h.h(MSG_SYSTEM_ON_RESUME, "MSG_SYSTEM_ON_RESUME");
        h.h(MSG_SYSTEM_FUNCTION_REQUEST, "MSG_SYSTEM_FUNCTION_REQUEST");
        h.h(MSG_SYSTEM_EXIT, "MSG_SYSTEM_EXIT");
        h.h(MSG_SYSTEM_ACTIVITY_RESULT, "MSG_SYSTEM_ACTIVITY_RESULT");
        h.h(MSG_SYSTEM_LOG_EVENT, "MSG_SYSTEM_LOG_EVENT");
        h.h(MSG_SYSTEM_NOTIFY_EXIT, "MSG_SYSTEM_NOTIFY_EXIT");
        Properties properties = new Properties();
        try {
            properties.load(activity.getResources().openRawResource(e.bG("globe")));
            handler = new Handler();
            hQ();
            if (properties.containsKey("ThrowIOExceptions")) {
                nM = Boolean.parseBoolean(properties.getProperty("ThrowIOExceptions"));
            }
            if (properties.containsKey("DontQuit")) {
                m.nV = Boolean.parseBoolean(properties.getProperty("DontQuit"));
            }
            if (!properties.containsKey("DisableWakeLock")) {
                activity.getWindow().setFlags(128, 128);
            }
            h.d(activity);
            e.d(activity);
            d.d(activity);
            if (properties.containsKey("feature")) {
                String[] split = properties.getProperty("feature").split("\\}");
                for (int i = 0; i < split.length; i++) {
                    int indexOf = split[i].indexOf("{");
                    if (indexOf != -1) {
                        d.y(split[i].trim().substring(0, indexOf), split[i].trim().substring(indexOf + 1));
                        Log.d(LOG_TAG, split[i] + " has been added.");
                    } else {
                        Log.w(LOG_TAG, "Failed to create feature:" + split[i]);
                    }
                }
            }
            h.ci(MSG_SYSTEM_FEATURE_ADDED_COMPLETE);
            a.d(activity);
            f.d(activity);
            if (properties.containsKey("VolumeMode")) {
                g.cb(Integer.parseInt(properties.getProperty("VolumeMode")));
            }
            g.d(activity);
            m.d(activity);
            i.d(activity);
            c.bi(properties.getProperty(m.CONTEXT_TYPE_DEVICE));
            h.ci(MSG_SYSTEM_DEVICE_INIT_COMPLETE);
            h.ci(MSG_SYSTEM_VD_INIT_COMPLETE);
            if (properties.containsKey("AdaptiveVirtualDevice")) {
                n.oe = Boolean.parseBoolean(properties.getProperty("AdaptiveVirtualDevice"));
            }
            n.br(properties.getProperty("virtualdevice"));
            properties.clear();
            System.gc();
            h.a(new h.a() {
                public boolean b(Message message) {
                    if (message.what == 47875) {
                        l.hz();
                        return true;
                    } else if (message.what == 47881) {
                        if (l.nN) {
                            return true;
                        }
                        m.a(l.getString(R.string.alert), l.getString(R.string.exit_dialog), l.getString(R.string.yes), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                l.hA();
                                boolean unused = l.nN = false;
                            }
                        }, l.getString(R.string.no), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                l.resume();
                                boolean unused = l.nN = false;
                            }
                        }, true, new DialogInterface.OnCancelListener() {
                            public void onCancel(DialogInterface dialogInterface) {
                                dialogInterface.dismiss();
                                l.resume();
                                boolean unused = l.nN = false;
                            }
                        });
                        boolean unused = l.nN = true;
                        l.pause();
                        return true;
                    } else if (message.what == 47882) {
                        return l.bq((String) message.obj);
                    } else {
                        return false;
                    }
                }
            });
            h.c(h.c(MSG_SYSTEM_LOG_EVENT, new String[]{"Launch", gb()}));
        } catch (Exception e) {
            Log.e(LOG_TAG, "Load globe.properties error." + e);
        }
    }

    public static String gB() {
        String str = null;
        if (nK.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            try {
                str = hJ().getDeviceId();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d(LOG_TAG, "imei=" + str);
        }
        return str;
    }

    public static String gG() {
        return Build.MANUFACTURER;
    }

    public static String gb() {
        return getString(R.string.app_name);
    }

    public static Activity getActivity() {
        return nK;
    }

    public static Handler getHandler() {
        return handler;
    }

    public static String getMacAddress() {
        String str = null;
        if (nK.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE") == 0) {
            try {
                str = hK().getConnectionInfo().getMacAddress();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d(LOG_TAG, "mac=" + str);
        }
        return str;
    }

    public static String getString(int i) {
        return nK.getString(i);
    }

    public static String gv() {
        return Build.MODEL;
    }

    public static int gy() {
        return nK.getWindowManager().getDefaultDisplay().getWidth();
    }

    public static int gz() {
        return nK.getWindowManager().getDefaultDisplay().getHeight();
    }

    public static void hA() {
        h.c(h.c(MSG_SYSTEM_EXIT, null));
    }

    public static String hB() {
        TelephonyManager hJ = hJ();
        if (nK.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") != 0) {
            Log.e(LOG_TAG, "Get CarrierIMSI failed for permission not properly config.");
            return null;
        } else if (hJ.getSimState() == 5) {
            Log.d(LOG_TAG, "Sim state ready.");
            return hJ.getSubscriberId();
        } else {
            throw new Exception("Sim card is not ready yet.");
        }
    }

    public static final boolean hC() {
        return hD() || hE();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001a, code lost:
        if (hP().getNetworkInfo(1).isConnected() == true) goto L_0x001c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final boolean hD() {
        /*
            r0 = 1
            android.app.Activity r1 = org.meteoroid.core.l.nK
            java.lang.String r2 = "android.permission.ACCESS_NETWORK_STATE"
            int r1 = r1.checkCallingOrSelfPermission(r2)
            if (r1 == 0) goto L_0x000c
        L_0x000b:
            return r0
        L_0x000c:
            r1 = 0
            android.net.ConnectivityManager r2 = hP()     // Catch:{ Exception -> 0x0035 }
            r3 = 1
            android.net.NetworkInfo r2 = r2.getNetworkInfo(r3)     // Catch:{ Exception -> 0x0035 }
            boolean r2 = r2.isConnected()     // Catch:{ Exception -> 0x0035 }
            if (r2 != r0) goto L_0x003b
        L_0x001c:
            java.lang.String r1 = "SystemManager"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Wifi state: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            android.util.Log.d(r1, r2)
            goto L_0x000b
        L_0x0035:
            r0 = move-exception
            java.lang.String r2 = "SystemManager"
            android.util.Log.w(r2, r0)
        L_0x003b:
            r0 = r1
            goto L_0x001c
        */
        throw new UnsupportedOperationException("Method not decompiled: org.meteoroid.core.l.hD():boolean");
    }

    public static final boolean hE() {
        boolean z = true;
        if (nK.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == 0) {
            try {
                if (!hP().getNetworkInfo(0).isConnectedOrConnecting()) {
                    if (hJ().getDataState() != 2) {
                        z = false;
                    }
                }
            } catch (Exception e) {
                Log.w(LOG_TAG, e);
                z = false;
            }
            Log.d(LOG_TAG, "DataConnect state: " + z);
        }
        return z;
    }

    public static void hF() {
        h.ci(MSG_SYSTEM_NOTIFY_EXIT);
    }

    public static AudioManager hG() {
        return (AudioManager) nK.getSystemService("audio");
    }

    public static AudioManager hH() {
        return (AudioManager) nK.getSystemService("alarm");
    }

    public static ActivityManager hI() {
        return (ActivityManager) nK.getSystemService("activity");
    }

    public static TelephonyManager hJ() {
        return (TelephonyManager) nK.getSystemService("phone");
    }

    public static WifiManager hK() {
        return (WifiManager) nK.getSystemService("wifi");
    }

    public static NotificationManager hL() {
        return (NotificationManager) nK.getSystemService("notification");
    }

    public static InputMethodManager hM() {
        return (InputMethodManager) nK.getSystemService("input_method");
    }

    public static void hN() {
        Intent launchIntentForPackage = nK.getBaseContext().getPackageManager().getLaunchIntentForPackage(nK.getBaseContext().getPackageName());
        launchIntentForPackage.addFlags(k.OCTOBER);
        nK.startActivity(launchIntentForPackage);
    }

    public static LocationManager hO() {
        return (LocationManager) nK.getSystemService(m.PROP_LOCATION);
    }

    public static ConnectivityManager hP() {
        return (ConnectivityManager) nK.getSystemService("connectivity");
    }

    public static void hQ() {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) nK.getSystemService("activity")).getRunningAppProcesses();
        int myPid = Process.myPid();
        for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
            if (next.pid != myPid && next.importance > 300) {
                Process.killProcess(next.pid);
                Log.d(LOG_TAG, "Kill background process:" + next.processName + " pid:" + next.pid);
            }
        }
    }

    public static int hR() {
        return nK.getResources().getConfiguration().orientation;
    }

    public static Timer hS() {
        return nO;
    }

    public static String hT() {
        if (nK.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0) {
            Log.d(LOG_TAG, "ACCESS_NETWORK_STATE permission denied.");
            return null;
        }
        try {
            return hP().getNetworkInfo(0).getExtraInfo();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int hU() {
        try {
            return Integer.valueOf(Build.VERSION.SDK).intValue();
        } catch (NumberFormatException e) {
            return 4;
        }
    }

    public static String hV() {
        String hW = hW();
        if (hW == null || hW.trim().equals("")) {
            Log.d(LOG_TAG, "phoneNumber is empty.Try imei.");
            hW = gB();
        }
        if (hW == null || hW.trim().equals("")) {
            Log.d(LOG_TAG, "imei is empty. Try iccid.");
            hW = hX();
        }
        if (hW == null || hW.trim().equals("")) {
            Log.d(LOG_TAG, "iccid is empty.Try mac.");
            hW = getMacAddress();
        }
        if (hW == null || hW.trim().equals("")) {
            Log.d(LOG_TAG, "mac is empty.Try android_id.");
            hW = hY();
        }
        Log.d(LOG_TAG, "uniqueID is " + hW);
        return hW;
    }

    public static String hW() {
        String str = null;
        if (nK.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            try {
                str = hJ().getLine1Number();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d(LOG_TAG, "phoneNumber=" + str);
        }
        return str;
    }

    public static String hX() {
        String str = null;
        if (nK.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            try {
                str = hJ().getSimSerialNumber();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.d(LOG_TAG, "iccid=" + str);
        }
        return str;
    }

    public static String hY() {
        String str;
        String string = Settings.Secure.getString(nK.getContentResolver(), "android_id");
        if (!"9774d56d682e549c".equals(string)) {
            try {
                Class<?> cls = Class.forName("android.os.SystemProperties");
                str = (String) cls.getMethod("get", String.class).invoke(cls, "ro.serialno");
            } catch (Exception e) {
                str = string;
            }
        } else {
            str = string;
        }
        Log.d(LOG_TAG, "androidId=" + str);
        return str;
    }

    public static Location hZ() {
        if (nK.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
            return ((LocationManager) nK.getSystemService(m.PROP_LOCATION)).getLastKnownLocation("gps");
        }
        if (nK.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
            return ((LocationManager) nK.getSystemService(m.PROP_LOCATION)).getLastKnownLocation("network");
        }
        Log.e(LOG_TAG, "Get Location failed for permission not properly config.");
        return null;
    }

    public static void hy() {
        nL = 0;
        h.c(h.c(MSG_SYSTEM_ON_RESUME, null));
    }

    protected static void hz() {
        h.hv();
        onDestroy();
        nK.finish();
        System.gc();
        System.exit(0);
        Process.killProcess(Process.myPid());
    }

    public static void ia() {
        nK.startActivity(new Intent("android.settings.WIFI_SETTINGS"));
    }

    public static void ib() {
        nK.startActivity(new Intent("android.settings.WIRELESS_SETTINGS"));
    }

    public static int ic() {
        return R.drawable.icon;
    }

    public static void j(final String str, final int i) {
        handler.post(new Runnable() {
            public void run() {
                Toast.makeText(l.nK, str, i).show();
            }
        });
    }

    protected static void onDestroy() {
        nO.cancel();
        nO.purge();
        a.onDestroy();
        i.onDestroy();
        e.onDestroy();
        f.onDestroy();
        g.onDestroy();
        m.onDestroy();
        c.onDestroy();
        n.onDestroy();
        d.onDestroy();
        h.onDestroy();
    }

    public static void pause() {
        nL++;
        if (nL == 1) {
            h.c(h.c(MSG_SYSTEM_ON_PAUSE, null));
        } else {
            Log.w(LOG_TAG, "The system has already paused." + nL);
        }
    }

    public static void resume() {
        nL--;
        if (nL == 0) {
            h.c(h.c(MSG_SYSTEM_ON_RESUME, null));
        } else {
            Log.w(LOG_TAG, "The system do not need resumed." + nL);
        }
        if (nL <= 0) {
            nL = 0;
        }
    }

    public static void w(boolean z) {
        if (nK.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE") == 0 && nK.checkCallingOrSelfPermission("android.permission.CHANGE_WIFI_STATE") == 0) {
            hK().setWifiEnabled(z);
        } else {
            Log.w(LOG_TAG, "Not enough permission in change wifi.");
        }
    }

    public static void x(boolean z) {
        Method declaredMethod;
        if ((nK.checkCallingOrSelfPermission("android.permission.MODIFY_PHONE_STATE") == 0 || nK.checkCallingOrSelfPermission("android.permission.CHANGE_NETWORK_STATE") == 0) && nK.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0) {
            try {
                if (Build.VERSION.SDK_INT >= 9) {
                    ConnectivityManager hP = hP();
                    Field declaredField = Class.forName(hP.getClass().getName()).getDeclaredField("mService");
                    declaredField.setAccessible(true);
                    Object obj = declaredField.get(hP);
                    Method declaredMethod2 = Class.forName(obj.getClass().getName()).getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
                    declaredMethod2.setAccessible(true);
                    declaredMethod2.invoke(obj, Boolean.valueOf(z));
                    return;
                }
                TelephonyManager hJ = hJ();
                if (hJ.getDataState() == 2 && z) {
                    Log.d(LOG_TAG, "Already enable. No need to turn on data connection.");
                } else if (hJ.getDataState() != 2 && !z) {
                    Log.d(LOG_TAG, "Already disable. No need to turn off data connection.");
                }
                Method declaredMethod3 = Class.forName(hJ.getClass().getName()).getDeclaredMethod("getITelephony", new Class[0]);
                declaredMethod3.setAccessible(true);
                Object invoke = declaredMethod3.invoke(hJ, new Object[0]);
                Class<?> cls = Class.forName(invoke.getClass().getName());
                if (!z) {
                    declaredMethod = cls.getDeclaredMethod("disableDataConnectivity", new Class[0]);
                    Log.d(LOG_TAG, "Ready to disable data connection state.");
                } else {
                    declaredMethod = cls.getDeclaredMethod("enableDataConnectivity", new Class[0]);
                    Log.d(LOG_TAG, "Ready to enable data connection state.");
                }
                declaredMethod.setAccessible(true);
                declaredMethod.invoke(invoke, new Object[0]);
                Log.d(LOG_TAG, "Success change data connection state.");
            } catch (Exception e) {
                Log.w(LOG_TAG, "Error in change data connection:" + e);
            }
        } else {
            Log.w(LOG_TAG, "Not enough permission in change data connection.");
        }
    }
}
