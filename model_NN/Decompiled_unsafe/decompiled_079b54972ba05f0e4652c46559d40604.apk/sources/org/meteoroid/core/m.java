package org.meteoroid.core;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import org.meteoroid.core.h;

public final class m {
    public static final String LOG_TAG = "ViewManager";
    public static final int MSG_VIEW_CHANGED = 23041;
    private static final int ROOT_VIEW_ID = 792998026;
    /* access modifiers changed from: private */
    public static ProgressDialog lT;
    /* access modifiers changed from: private */
    public static a nS;
    /* access modifiers changed from: private */
    public static a nT;
    /* access modifiers changed from: private */
    public static FrameLayout nU;
    public static boolean nV = false;
    /* access modifiers changed from: private */
    public static AlertDialog nW;

    public interface a {
        boolean bE();

        void br();

        void bs();

        View getView();
    }

    private static void a(final AlertDialog.Builder builder) {
        if (builder != null) {
            l.getHandler().post(new Runnable() {
                public void run() {
                    AlertDialog unused = m.nW = builder.create();
                    m.nW.show();
                }
            });
        }
    }

    public static void a(String str, String str2, View view, boolean z, DialogInterface.OnCancelListener onCancelListener) {
        a(str, str2, view, null, null, null, null, null, null, null, null, z, onCancelListener);
    }

    private static void a(String str, String str2, View view, String[] strArr, DialogInterface.OnClickListener onClickListener, String str3, DialogInterface.OnClickListener onClickListener2, String str4, DialogInterface.OnClickListener onClickListener3, String str5, DialogInterface.OnClickListener onClickListener4, boolean z, DialogInterface.OnCancelListener onCancelListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(l.getActivity());
        if (str != null) {
            builder.setTitle(str);
        }
        if (str2 != null) {
            builder.setMessage(str2);
        }
        if (!(strArr == null || onClickListener == null)) {
            builder.setItems(strArr, onClickListener);
        }
        if (view != null) {
            builder.setView(view);
        }
        if (str3 != null) {
            builder.setPositiveButton(str3, onClickListener2);
        }
        if (str4 != null) {
            builder.setNegativeButton(str4, onClickListener3);
        }
        if (str5 != null) {
            builder.setNeutralButton(str5, onClickListener4);
        }
        builder.setCancelable(z);
        if (onCancelListener != null) {
            builder.setOnCancelListener(onCancelListener);
        }
        a(builder);
    }

    public static void a(String str, String str2, String str3, DialogInterface.OnClickListener onClickListener) {
        a(str, str2, str3, onClickListener, null, null, null, null, false, null);
    }

    public static void a(String str, String str2, String str3, DialogInterface.OnClickListener onClickListener, String str4, DialogInterface.OnClickListener onClickListener2) {
        a(str, str2, str3, onClickListener, str4, onClickListener2, null, null, false, null);
    }

    @Deprecated
    public static void a(String str, String str2, String str3, DialogInterface.OnClickListener onClickListener, String str4, DialogInterface.OnClickListener onClickListener2, String str5, DialogInterface.OnClickListener onClickListener3, boolean z) {
        a(str, str2, str3, onClickListener, str4, onClickListener2, str5, onClickListener3, z, null);
    }

    public static void a(String str, String str2, String str3, DialogInterface.OnClickListener onClickListener, String str4, DialogInterface.OnClickListener onClickListener2, String str5, DialogInterface.OnClickListener onClickListener3, boolean z, DialogInterface.OnCancelListener onCancelListener) {
        a(str, str2, null, null, null, str3, onClickListener, str4, onClickListener2, str5, onClickListener3, z, onCancelListener);
    }

    @Deprecated
    public static void a(String str, String str2, String str3, DialogInterface.OnClickListener onClickListener, String str4, DialogInterface.OnClickListener onClickListener2, boolean z) {
        a(str, str2, str3, onClickListener, str4, onClickListener2, null, null, z, null);
    }

    public static void a(String str, String str2, String str3, DialogInterface.OnClickListener onClickListener, String str4, DialogInterface.OnClickListener onClickListener2, boolean z, DialogInterface.OnCancelListener onCancelListener) {
        a(str, str2, str3, onClickListener, str4, onClickListener2, null, null, z, onCancelListener);
    }

    public static void a(final String str, final String str2, final boolean z, final boolean z2) {
        l.getHandler().post(new Runnable() {
            public void run() {
                if (m.lT == null) {
                    ProgressDialog unused = m.lT = new ProgressDialog(l.getActivity());
                }
                if (str != null) {
                    m.lT.setTitle(str);
                }
                if (str2 != null) {
                    m.lT.setMessage(str2);
                }
                m.lT.setCancelable(z);
                m.lT.setIndeterminate(z2);
                if (!m.lT.isShowing()) {
                    m.lT.show();
                }
            }
        });
    }

    public static void a(String str, String str2, String[] strArr, DialogInterface.OnClickListener onClickListener, boolean z) {
        a(str, str2, null, strArr, onClickListener, null, null, null, null, null, null, z, null);
    }

    public static void a(final a aVar) {
        if (aVar == null || aVar == nS || aVar.getView() == null) {
            Log.d(LOG_TAG, "Change view not effected.");
        } else {
            l.getHandler().post(new Runnable() {
                public void run() {
                    Log.d(m.LOG_TAG, "Change view start.");
                    if (m.nU.getParent() == null) {
                        l.getActivity().setContentView(m.nU);
                    }
                    if (!(m.nS == null || m.nS.getView() == null)) {
                        m.nU.removeView(m.nS.getView());
                    }
                    if (m.nS != null) {
                        m.nS.bs();
                        a unused = m.nT = m.nS;
                    }
                    m.nU.addView(aVar.getView());
                    if (aVar != null) {
                        aVar.br();
                    }
                    a unused2 = m.nS = aVar;
                    m.nU.invalidate();
                    m.nS.getView().requestFocus();
                    h.d(m.MSG_VIEW_CHANGED, m.nS);
                    Log.d(m.LOG_TAG, "Change view end.");
                }
            });
        }
    }

    public static void b(a aVar) {
        nT = nS;
        nS = aVar;
    }

    protected static boolean bE() {
        if (nS != null) {
            return nS.bE() || nV;
        }
        return false;
    }

    protected static void d(Activity activity) {
        h.h(MSG_VIEW_CHANGED, "MSG_VIEW_CHANGED");
        nU = new FrameLayout(activity);
        nU.setBackgroundColor(-16777216);
        nU.setId(ROOT_VIEW_ID);
        nU.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        nU.requestFocus();
        h.a(new h.a() {
            public boolean b(Message message) {
                if (message.what == 47873) {
                    if (m.nS == null) {
                        return false;
                    }
                    m.nS.bs();
                    return false;
                } else if (message.what != 47874 || m.nS == null) {
                    return false;
                } else {
                    m.nS.br();
                    return false;
                }
            }
        });
    }

    public static void ig() {
        l.getHandler().post(new Runnable() {
            public void run() {
                if (m.lT != null && m.lT.isShowing()) {
                    m.lT.dismiss();
                }
            }
        });
    }

    public static final void ih() {
        l.getHandler().post(new Runnable() {
            public void run() {
                l.getActivity().setContentView(m.nU);
                m.nU.invalidate();
                Log.d(m.LOG_TAG, "restore root view end.");
            }
        });
    }

    public static final void ii() {
        a(nT);
    }

    public static a ij() {
        return nS;
    }

    public static void ik() {
        l.getHandler().post(new Runnable() {
            public void run() {
                if (m.nW != null && m.nW.isShowing()) {
                    m.nW.dismiss();
                }
            }
        });
    }

    public static FrameLayout il() {
        return nU;
    }

    public static void l(long j) {
        l.getHandler().postDelayed(new Runnable() {
            public void run() {
                if (m.lT != null && m.lT.isShowing()) {
                    m.lT.dismiss();
                }
            }
        }, j);
    }

    public static void m(long j) {
        l.getHandler().postDelayed(new Runnable() {
            public void run() {
                if (m.nW != null && m.nW.isShowing()) {
                    m.nW.dismiss();
                }
            }
        }, j);
    }

    protected static void onDestroy() {
        nT = null;
        nS = null;
    }
}
