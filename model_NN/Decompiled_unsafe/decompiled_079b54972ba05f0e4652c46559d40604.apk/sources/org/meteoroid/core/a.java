package org.meteoroid.core;

import android.app.Activity;
import android.os.Message;
import android.util.Log;
import java.util.HashMap;
import org.meteoroid.core.h;

public final class a {
    public static final String LOG_TAG = "ApplicationManager";
    public static final int MSG_APPLICATION_LAUNCH = 47617;
    public static final int MSG_APPLICATION_NEED_PAUSE = 47623;
    public static final int MSG_APPLICATION_NEED_START = 47622;
    public static final int MSG_APPLICATION_PAUSE = 47619;
    public static final int MSG_APPLICATION_QUIT = 47621;
    public static final int MSG_APPLICATION_RESUME = 47620;
    public static final int MSG_APPLICATION_START = 47618;
    public static C0004a mL;
    private static final HashMap<String, String> mM = new HashMap<>();

    /* renamed from: org.meteoroid.core.a$a  reason: collision with other inner class name */
    public interface C0004a {
        public static final int EXIT = 3;
        public static final int INIT = 0;
        public static final int PAUSE = 2;
        public static final int RUN = 1;

        void eg();

        int getState();

        void onDestroy();

        void onPause();

        void onResume();

        void onStart();
    }

    public static void a(C0004a aVar) {
        if (aVar == null) {
            Log.e(LOG_TAG, "Launch a null application.");
            return;
        }
        mL = aVar;
        try {
            mL.eg();
            Log.d(LOG_TAG, "The application has successfully launched.");
        } catch (Exception e) {
            Log.w(LOG_TAG, "The application failed to launch.");
            e.printStackTrace();
        }
    }

    public static void ba(String str) {
        if (str != null) {
            try {
                mL = (C0004a) Class.forName(str).newInstance();
                h.c(h.c(MSG_APPLICATION_LAUNCH, mL));
            } catch (Exception e) {
                e.printStackTrace();
                Log.w(LOG_TAG, e.toString());
            }
            if (mL != null) {
                mL.eg();
                Log.d(LOG_TAG, "The application has successfully launched.");
                return;
            }
            Log.w(LOG_TAG, "The application failed to launch.");
            return;
        }
        Log.w(LOG_TAG, "No available application could launch.");
    }

    public static String bb(String str) {
        Log.d(LOG_TAG, "Get Application Property:" + str);
        return mM.get(str);
    }

    public static boolean ca() {
        boolean z = true;
        if (mL == null) {
            return false;
        }
        if (mL.getState() != 1) {
            z = false;
        }
        return z;
    }

    protected static void d(Activity activity) {
        h.h(MSG_APPLICATION_LAUNCH, "MSG_APPLICATION_LAUNCH");
        h.h(MSG_APPLICATION_START, "MSG_APPLICATION_START");
        h.h(MSG_APPLICATION_PAUSE, "MSG_APPLICATION_PAUSE");
        h.h(MSG_APPLICATION_RESUME, "MSG_APPLICATION_RESUME");
        h.h(MSG_APPLICATION_QUIT, "MSG_APPLICATION_QUIT");
        h.h(MSG_APPLICATION_NEED_START, "MSG_APPLICATION_NEED_START");
        h.h(MSG_APPLICATION_NEED_PAUSE, "MSG_APPLICATION_NEED_PAUSE");
        h.a(new h.a() {
            public boolean b(Message message) {
                if (message.what == 47873) {
                    a.pause();
                    return false;
                } else if (message.what != 47874) {
                    return false;
                } else {
                    a.resume();
                    return false;
                }
            }
        });
    }

    protected static void onDestroy() {
        quit();
    }

    public static void pause() {
        if (mL == null) {
            return;
        }
        if (mL.getState() == 1) {
            mL.onPause();
            h.c(h.c(MSG_APPLICATION_PAUSE, mL));
            return;
        }
        Log.w(LOG_TAG, "Incorrect application state." + mL.getState());
    }

    public static void quit() {
        if (mL != null && mL.getState() != 3) {
            mL.onDestroy();
            h.c(h.c(MSG_APPLICATION_QUIT, mL));
        }
    }

    public static void resume() {
        if (mL == null) {
            return;
        }
        if (mL.getState() == 2) {
            mL.onResume();
            h.c(h.c(MSG_APPLICATION_RESUME, mL));
            return;
        }
        Log.w(LOG_TAG, "Incorrect application state." + mL.getState());
    }

    public static void start() {
        if (mL == null) {
            return;
        }
        if (mL.getState() == 0) {
            mL.onStart();
            h.c(h.c(MSG_APPLICATION_START, mL));
            Log.d(LOG_TAG, "The application has successfully started.");
            return;
        }
        Log.w(LOG_TAG, "Incorrect application state." + mL.getState());
    }

    public static void x(String str, String str2) {
        mM.put(str, str2);
    }
}
