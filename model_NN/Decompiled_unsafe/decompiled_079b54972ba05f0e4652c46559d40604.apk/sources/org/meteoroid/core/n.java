package org.meteoroid.core;

import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import com.a.a.r.c;
import org.meteoroid.core.h;

public final class n {
    public static final String LOG_TAG = "VDManager";
    public static final int MSG_SYSTEM_VD_RELOAD = 6913;
    public static final int MSG_SYSTEM_VD_RELOAD_COMPLETE = 6914;
    public static c od;
    public static boolean oe = false;
    public static boolean of;
    private static int og;
    private static int oh;
    /* access modifiers changed from: private */
    public static boolean oi;
    /* access modifiers changed from: private */
    public static boolean oj;
    private static final Thread ok = new Thread() {
        public final void run() {
            while (n.oi) {
                n.iu();
                SystemClock.sleep(200);
            }
        }
    };
    private static int orientation;

    protected static c br(String str) {
        try {
            od = (c) Class.forName(str).newInstance();
            od.onCreate();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Failed to create virtual device. " + e);
            e.printStackTrace();
        }
        h.h(MSG_SYSTEM_VD_RELOAD, "MSG_SYSTEM_VD_RELOAD");
        h.h(MSG_SYSTEM_VD_RELOAD_COMPLETE, "MSG_SYSTEM_VD_RELOAD_COMPLETE");
        h.a(new h.a() {
            public boolean b(Message message) {
                if (message.what == 40965 && n.oe && n.ir()) {
                    Log.d(n.LOG_TAG, "The vd need auto reload !");
                    l.pause();
                    h.ci(n.MSG_SYSTEM_VD_RELOAD);
                } else if (message.what == 6913) {
                    n.of = true;
                    SystemClock.sleep(100);
                    while (n.oj) {
                        Thread.yield();
                    }
                    n.bs((String) message.obj);
                    h.d(l.MSG_SYSTEM_LOG_EVENT, new String[]{"ReloadSkin", l.gb() + "=" + ((String) message.obj)});
                    h.ci(n.MSG_SYSTEM_VD_RELOAD_COMPLETE);
                    return true;
                } else if (message.what == 6914) {
                    SystemClock.sleep(100);
                    l.resume();
                    n.of = false;
                }
                return false;
            }
        });
        iq();
        return od;
    }

    protected static void bs(String str) {
        od.bs(str);
        iq();
    }

    private static final void iq() {
        og = l.gy();
        oh = l.gz();
        orientation = l.hR();
    }

    public static final boolean ir() {
        return (og == l.gy() && oh == l.gz() && orientation == l.hR()) ? false : true;
    }

    public static void is() {
        if (!oi && !ok.isAlive()) {
            oi = true;
            ok.start();
        }
    }

    public static void it() {
        oi = false;
    }

    public static void iu() {
        if (!of) {
            oj = true;
            e.hi();
            oj = false;
        }
    }

    protected static void onDestroy() {
        if (od != null) {
            od.onDestroy();
        }
    }
}
