package org.meteoroid.core;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Message;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.meteoroid.core.h;

public final class g {
    public static final String LOG_TAG = "MediaManager";
    public static final String MEDIA_TEMP_FILE = "temp";
    public static final int VOLUME_CONTROL_BY_CLIP = -1;
    public static final int VOLUME_CONTROL_BY_DEVICE = -2;
    private static final g nf = new g();
    private static int ng = -1;
    private static String nh;
    private static int ni = 15;
    /* access modifiers changed from: private */
    public static int nj;
    private static float nk;
    private static AudioManager nl;
    /* access modifiers changed from: private */
    public static final ConcurrentLinkedQueue<a> nm = new ConcurrentLinkedQueue<>();
    private static int nn = 0;
    private static int np;
    private static int nq = 0;
    /* access modifiers changed from: private */
    public static int nr = 0;

    public class a {
        public String name;
        public String ns;
        public boolean nt;
        public int nu;
        public boolean nv;
        public MediaPlayer nw;
        public String type;

        public a() {
        }

        public void recycle() {
            if (this.name != null) {
                g.nm.remove(this.name);
            }
            if (this.nw != null) {
                this.nw.release();
            }
            this.nw = null;
        }
    }

    public static a a(String str, InputStream inputStream, String str2) {
        if (inputStream == null) {
            return null;
        }
        nn++;
        g gVar = nf;
        gVar.getClass();
        a aVar = new a();
        String str3 = "";
        if (str2.indexOf("mid") != -1) {
            str3 = ".mid";
        } else if (str2.indexOf("mpeg") != -1) {
            str3 = ".mp3";
        } else if (str2.indexOf("amr") != -1) {
            str3 = ".amr";
        }
        aVar.name = str;
        aVar.type = str2;
        aVar.ns = nn + str3;
        FileOutputStream openFileOutput = l.getActivity().openFileOutput(aVar.ns, 1);
        byte[] bArr = new byte[256];
        while (true) {
            int read = inputStream.read(bArr);
            if (read <= 0) {
                openFileOutput.flush();
                openFileOutput.close();
                aVar.nw = new MediaPlayer();
                aVar.ns = nh + aVar.ns;
                nm.add(aVar);
                Log.d(LOG_TAG, "Create a media clip " + str + " [" + str2 + "].");
                return aVar;
            }
            openFileOutput.write(bArr, 0, read);
        }
    }

    public static void a(a aVar) {
        if (nm.contains(aVar)) {
            nm.remove(aVar.name);
        }
    }

    public static void cb(int i) {
        ng = i;
    }

    public static void cc(int i) {
        if (hm() == -1) {
            ce(i);
        } else if (hm() == -2) {
            if (i == 0) {
                np = hp();
                ce(0);
            } else if (np != 0) {
                ce(np);
                np = 0;
            }
            Log.d(LOG_TAG, "Failed to set volume because the globe volume mode is control by device." + np);
        }
    }

    /* access modifiers changed from: private */
    public static void cd(int i) {
        nl.setStreamVolume(3, i, 16);
    }

    /* access modifiers changed from: private */
    public static void ce(int i) {
        Log.d(LOG_TAG, "Set device volume to " + i);
        nl.setStreamVolume(3, (int) (((float) i) * nk), 16);
    }

    protected static void d(Activity activity) {
        nh = activity.getFilesDir().getAbsolutePath() + File.separator;
        nl = (AudioManager) activity.getSystemService("audio");
        ni = nl.getStreamMaxVolume(3);
        Log.d(LOG_TAG, "Max volume is" + ni);
        nk = ((float) ni) / 100.0f;
        Log.d(LOG_TAG, "VOLUME_TRANS_RATIO is" + nk);
        nj = hp();
        Log.d(LOG_TAG, "Init volume is" + nj);
        h.a(new h.a() {
            public boolean b(Message message) {
                if (message.what == 47873) {
                    Iterator it = g.nm.iterator();
                    while (it.hasNext()) {
                        a aVar = (a) it.next();
                        try {
                            if (aVar.nw != null && aVar.nw.isPlaying() && aVar.nt) {
                                aVar.nw.pause();
                                Log.d(g.LOG_TAG, "force to pause:" + aVar.name);
                            }
                        } catch (IllegalStateException e) {
                            Log.w(g.LOG_TAG, "error to pause:" + aVar.name);
                        }
                    }
                    int unused = g.nr = g.ho();
                    Log.d(g.LOG_TAG, "Pause volume is" + g.nr);
                    g.ce(g.nj);
                    return false;
                } else if (message.what != 47874) {
                    return false;
                } else {
                    g.cd(g.nr);
                    Iterator it2 = g.nm.iterator();
                    while (it2.hasNext()) {
                        a aVar2 = (a) it2.next();
                        try {
                            if (aVar2.nw != null && aVar2.nt && !aVar2.nw.isPlaying()) {
                                aVar2.nw.start();
                                Log.d(g.LOG_TAG, "force to resume:" + aVar2.name);
                            }
                        } catch (IllegalStateException e2) {
                            Log.w(g.LOG_TAG, "error to resume:" + aVar2.name);
                        }
                    }
                    return false;
                }
            }
        });
        if (hm() == -2) {
            ce(70);
        }
    }

    public static a e(String str, boolean z) {
        if (str == null) {
            return null;
        }
        String str2 = new String(str);
        if (!z) {
            if (str2.startsWith("/")) {
                str2 = str.substring(1);
            }
            str2 = "file:///android_asset/abresource" + str2;
        }
        g gVar = nf;
        gVar.getClass();
        a aVar = new a();
        String str3 = "";
        if (str.endsWith(".mid") || str.endsWith(".midi")) {
            str3 = "audio/midi";
        } else if (str.endsWith(".wav")) {
            str3 = "audio/x-wav";
        } else if (str.endsWith(".mpeg") || str.endsWith(".mp3")) {
            str3 = "audio/mpeg";
        } else if (str.endsWith(".amr")) {
            str3 = "audio/amr";
        }
        aVar.name = str;
        aVar.type = str3;
        aVar.ns = str2;
        aVar.nw = new MediaPlayer();
        Log.d(LOG_TAG, "Create a media clip " + str + " [" + str3 + "].");
        nm.add(aVar);
        return aVar;
    }

    public static int hm() {
        return ng;
    }

    public static a hn() {
        g gVar = nf;
        gVar.getClass();
        a aVar = new a();
        aVar.nw = new MediaPlayer();
        return aVar;
    }

    /* access modifiers changed from: private */
    public static int ho() {
        return nl.getStreamVolume(3);
    }

    public static int hp() {
        Log.d(LOG_TAG, "Current volume is:" + nl.getStreamVolume(3));
        return (int) (((float) nl.getStreamVolume(3)) / nk);
    }

    public static boolean hq() {
        return hp() == 0;
    }

    protected static void onDestroy() {
        nm.clear();
        ce(nj);
    }

    public static void v(boolean z) {
        Log.d(LOG_TAG, "Mute works:" + z);
        if (z) {
            nq = hp();
            cc(0);
        } else if (nq != 0) {
            cc(nq);
        }
    }
}
