package org.meteoroid.core;

import android.content.SharedPreferences;
import android.util.Log;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public final class k {

    public static final class a {
        public a(String str) {
        }
    }

    public static final class b {
        private static final String DATA_STORE_FILE = ".dat";
        private static final String LOG_TAG = "FilePersistence";
        private boolean nI;

        public b(boolean z) {
            this.nI = z;
        }

        private String bd(String str) {
            return str.replace(File.separator, "_");
        }

        public List<String> bc(String str) {
            String bd = bd(str);
            String[] fileList = l.getActivity().fileList();
            ArrayList arrayList = new ArrayList();
            if (fileList != null && fileList.length > 0) {
                for (int i = 0; i < fileList.length; i++) {
                    if (fileList[i].endsWith(DATA_STORE_FILE) && (!(bd == null || fileList[i].indexOf(bd) == -1) || bd == null)) {
                        arrayList.add(fileList[i].substring(0, fileList[i].length() - DATA_STORE_FILE.length()));
                        Log.d(LOG_TAG, "Find data file:" + fileList[i]);
                    }
                }
            }
            return arrayList;
        }

        public boolean be(String str) {
            String bd = bd(str);
            for (String equals : bc(bd)) {
                if (equals.equals(bd)) {
                    return true;
                }
            }
            return false;
        }

        public OutputStream bf(String str) {
            String bd = bd(str);
            Log.d(LOG_TAG, "Update data:" + bd);
            return l.getActivity().openFileOutput(bd + DATA_STORE_FILE, 1);
        }

        public InputStream bg(String str) {
            String bd = bd(str);
            Log.d(LOG_TAG, "Read data:" + bd);
            return l.getActivity().openFileInput(bd + DATA_STORE_FILE);
        }

        public boolean bh(String str) {
            String bd = bd(str);
            Log.d(LOG_TAG, "Delete data:" + bd);
            return l.getActivity().deleteFile(bd + DATA_STORE_FILE);
        }

        public String bk(String str) {
            return l.getActivity().getFilesDir().getAbsolutePath() + File.separator + bd(str) + DATA_STORE_FILE;
        }
    }

    public static final class c {
        private SharedPreferences nJ;

        private c(int i) {
            this.nJ = l.getActivity().getPreferences(i);
        }

        public SharedPreferences.Editor getEditor() {
            return this.nJ.edit();
        }

        public SharedPreferences getSharedPreferences() {
            return this.nJ;
        }
    }

    public static a bj(String str) {
        return new a(str);
    }

    public static c cj(int i) {
        return new c(i);
    }

    public static b hw() {
        return new b(true);
    }

    public static b hx() {
        return new b(false);
    }
}
