package org.meteoroid.core;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;

public class MeteoroidActivity extends Activity {
    public static final String LOG_TAG = "Meteoroid";
    public static final int MSG_ACTIVITY_CONF_CHANGE = 40965;
    public static final int MSG_ACTIVITY_CREATE = 40967;
    public static final int MSG_ACTIVITY_DESTROY = 40968;
    public static final int MSG_ACTIVITY_INTENT = 40966;
    public static final int MSG_ACTIVITY_PAUSE = 40960;
    public static final int MSG_ACTIVITY_RESTART = 40963;
    public static final int MSG_ACTIVITY_RESUME = 40961;
    public static final int MSG_ACTIVITY_START = 40962;
    public static final int MSG_ACTIVITY_STOP = 40964;
    public static final int MSG_ACTIVITY_WINDOWS_FOCUS_CHANGE = 40969;
    private boolean nE = true;

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        Log.d(LOG_TAG, "onActivityResult:" + i + "|" + i2);
        h.c(h.c(l.MSG_SYSTEM_ACTIVITY_RESULT, new Object[]{new int[]{i, i2}, intent}));
        super.onActivityResult(i, i2, intent);
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        Log.d(LOG_TAG, "onConfigurationChanged");
        h.c(h.c(MSG_ACTIVITY_CONF_CHANGE, this));
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        getWindow().setBackgroundDrawable(null);
        getWindow().requestFeature(1);
        getWindow().setFlags(1024, 1024);
        h.h(MSG_ACTIVITY_PAUSE, "MSG_ACTIVITY_PAUSE");
        h.h(MSG_ACTIVITY_RESUME, "MSG_ACTIVITY_RESUME");
        h.h(MSG_ACTIVITY_START, "MSG_ACTIVITY_START");
        h.h(MSG_ACTIVITY_RESTART, "MSG_ACTIVITY_RESTART");
        h.h(MSG_ACTIVITY_STOP, "MSG_ACTIVITY_STOP");
        h.h(MSG_ACTIVITY_CONF_CHANGE, "MSG_ACTIVITY_CONF_CHANGE");
        h.h(MSG_ACTIVITY_INTENT, "MSG_ACTIVITY_INTENT");
        h.h(MSG_ACTIVITY_CREATE, "MSG_ACTIVITY_CREATE");
        h.h(MSG_ACTIVITY_DESTROY, "MSG_ACTIVITY_DESTROY");
        l.d(this);
        super.onCreate(bundle);
        h.d(MSG_ACTIVITY_INTENT, getIntent());
        h.d(l.MSG_SYSTEM_INIT_COMPLETE, bundle);
        Log.d(LOG_TAG, "OnCreate.");
        h.ci(MSG_ACTIVITY_CREATE);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        h.ci(MSG_ACTIVITY_DESTROY);
        SystemClock.sleep(200);
        Log.d(LOG_TAG, "OnDestroy.");
        l.hA();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return super.onKeyDown(i, keyEvent);
        }
        if (!m.bE()) {
            l.hF();
        }
        return true;
    }

    public void onLowMemory() {
        l.hQ();
        super.onLowMemory();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        i.a(menuItem);
        return super.onOptionsItemSelected(menuItem);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        h.c(h.c(MSG_ACTIVITY_PAUSE, this));
        l.pause();
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        i.onPrepareOptionsMenu(menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        h.c(h.c(MSG_ACTIVITY_RESTART, this));
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        h.c(h.c(MSG_ACTIVITY_RESUME, this));
        if (this.nE) {
            this.nE = false;
            Log.d(LOG_TAG, "Skip first resume.");
            return;
        }
        l.resume();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        h.c(h.c(MSG_ACTIVITY_START, this));
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        h.c(h.c(MSG_ACTIVITY_STOP, this));
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        return f.onTrackballEvent(motionEvent);
    }

    public void onWindowFocusChanged(boolean z) {
        Log.d(LOG_TAG, "onWindowFocusChanged:" + z);
        h.c(h.c(MSG_ACTIVITY_WINDOWS_FOCUS_CHANGE, Boolean.valueOf(z)));
    }
}
