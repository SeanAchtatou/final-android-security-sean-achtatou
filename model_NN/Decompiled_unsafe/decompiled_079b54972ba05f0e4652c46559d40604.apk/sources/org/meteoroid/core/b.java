package org.meteoroid.core;

import android.app.Activity;
import android.util.Log;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public final class b {
    private static final String DATA_STORE_FILE = ".dat";
    private static final String LOG_TAG = "DataManager";

    @Deprecated
    public static List<String> bc(String str) {
        Log.w(LOG_TAG, "The DataManager is out of data. Use PersistenceManager instead.");
        String bd = bd(str);
        String[] fileList = l.getActivity().fileList();
        ArrayList arrayList = new ArrayList();
        if (fileList != null && fileList.length > 0) {
            for (int i = 0; i < fileList.length; i++) {
                if (fileList[i].endsWith(DATA_STORE_FILE) && (!(bd == null || fileList[i].indexOf(bd) == -1) || bd == null)) {
                    arrayList.add(fileList[i].substring(0, fileList[i].length() - DATA_STORE_FILE.length()));
                    Log.d(LOG_TAG, "Find data file:" + fileList[i]);
                }
            }
        }
        return arrayList;
    }

    private static String bd(String str) {
        return str.replace(File.separator, "_");
    }

    @Deprecated
    public static boolean be(String str) {
        Log.w(LOG_TAG, "The DataManager is out of data. Use PersistenceManager instead.");
        String bd = bd(str);
        for (String equals : bc(bd)) {
            if (equals.equals(bd)) {
                Log.d(LOG_TAG, "Find data:" + bd);
                return true;
            }
        }
        Log.d(LOG_TAG, "Find no data:" + bd);
        return false;
    }

    @Deprecated
    public static OutputStream bf(String str) {
        Log.w(LOG_TAG, "The DataManager is out of data. Use PersistenceManager instead.");
        String bd = bd(str);
        Log.d(LOG_TAG, "Update data:" + bd);
        return l.getActivity().openFileOutput(bd + DATA_STORE_FILE, 1);
    }

    @Deprecated
    public static InputStream bg(String str) {
        Log.w(LOG_TAG, "The DataManager is out of data. Use PersistenceManager instead.");
        String bd = bd(str);
        Log.d(LOG_TAG, "Read data:" + bd);
        return l.getActivity().openFileInput(bd + DATA_STORE_FILE);
    }

    @Deprecated
    public static boolean bh(String str) {
        Log.w(LOG_TAG, "The DataManager is out of data. Use PersistenceManager instead.");
        String bd = bd(str);
        Log.d(LOG_TAG, "Delete data:" + bd);
        return l.getActivity().deleteFile(bd + DATA_STORE_FILE);
    }

    protected static void d(Activity activity) {
    }

    protected static void onDestroy() {
    }
}
