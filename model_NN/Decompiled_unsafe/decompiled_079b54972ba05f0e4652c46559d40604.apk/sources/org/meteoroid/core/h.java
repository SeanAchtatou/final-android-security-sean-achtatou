package org.meteoroid.core;

import android.app.Activity;
import android.os.Message;
import android.util.Log;
import android.util.SparseArray;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.LinkedBlockingQueue;

public final class h implements Runnable {
    public static final String LOG_TAG = "MessageDispatchManager";
    public static final int MSG_ARG2_DONT_RECYCLE_ME = -13295;
    public static final int MSG_ARG_NONE = 0;
    private static final h nA = new h();
    private static final SparseArray<String> nB = new SparseArray<>();
    private static final ArrayList<a> nD = new ArrayList<>();
    private static final LinkedBlockingQueue<Message> ny = new LinkedBlockingQueue<>();
    private static final CopyOnWriteArraySet<a> nz = new CopyOnWriteArraySet<>();
    private boolean nC;

    public interface a {
        boolean b(Message message);
    }

    public static Message a(int i, Object obj, int i2, int i3) {
        Message obtain = Message.obtain();
        obtain.what = i;
        obtain.obj = obj;
        obtain.arg1 = i2;
        obtain.arg2 = i3;
        return obtain;
    }

    public static Message a(int i, Object obj, boolean z) {
        return a(i, obj, 0, z ? MSG_ARG2_DONT_RECYCLE_ME : 0);
    }

    public static synchronized void a(Message message, String str) {
        synchronized (h.class) {
            Iterator<a> it = nz.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                a next = it.next();
                if (next.getClass().getSimpleName().equalsIgnoreCase(str)) {
                    if (next.b(message)) {
                        Log.d(LOG_TAG, "The message [" + nB.get(message.what) + "] has been direct consumed by [" + next.getClass().getName() + "].");
                        break;
                    }
                    Log.d(LOG_TAG, "The message [" + nB.get(message.what) + "] has been skipped by [" + next.getClass().getName() + "].");
                }
            }
        }
    }

    public static void a(a aVar) {
        if (!nz.contains(aVar)) {
            nz.add(aVar);
        }
    }

    public static void b(a aVar) {
        nD.add(aVar);
    }

    public static Message c(int i, Object obj) {
        return a(i, obj, 0, 0);
    }

    public static void c(Message message) {
        ny.add(message);
    }

    public static void ci(int i) {
        d(i, null);
    }

    public static void d(int i, Object obj) {
        c(c(i, obj));
    }

    protected static void d(Activity activity) {
        new Thread(nA).start();
    }

    public static boolean d(Message message) {
        return ny.contains(message);
    }

    public static synchronized void e(int i, Object obj) {
        synchronized (h.class) {
            e(c(i, obj));
        }
    }

    private static final void e(Message message) {
        boolean z;
        Iterator<a> it = nz.iterator();
        while (true) {
            if (!it.hasNext()) {
                z = false;
                break;
            }
            a next = it.next();
            if (next.b(message)) {
                Log.d(LOG_TAG, "The message [" + nB.get(message.what) + "] has been consumed by [" + next.getClass().getName() + "]. And " + ny.size() + " messages are waiting.");
                z = true;
                break;
            }
        }
        if (!z) {
            Log.d(LOG_TAG, "There is a message [" + nB.get(message.what, Integer.toHexString(message.what)) + "] which no consumer could deal with it. " + ny.size() + " messages left.");
        }
        if (message.arg2 != -13295) {
            message.recycle();
        }
    }

    public static void h(int i, String str) {
        nB.append(i, str);
    }

    public static final void hv() {
        ny.clear();
    }

    protected static void onDestroy() {
        nA.nC = true;
        ny.clear();
    }

    public void run() {
        while (!this.nC) {
            while (true) {
                Message poll = ny.poll();
                if (poll == null) {
                    break;
                }
                e(poll);
            }
            if (!nD.isEmpty()) {
                Iterator<a> it = nD.iterator();
                while (it.hasNext()) {
                    nz.remove(it.next());
                }
                nD.clear();
            }
            Thread.yield();
        }
    }
}
