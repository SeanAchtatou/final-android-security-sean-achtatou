package org.meteoroid.core;

import android.app.Activity;
import android.util.Log;
import com.a.a.r.b;
import java.util.Iterator;
import java.util.LinkedHashSet;

public final class d {
    public static final String LOG_TAG = "FeatureManager";
    private static LinkedHashSet<b> mO = new LinkedHashSet<>();

    public static boolean a(b bVar) {
        return mO.remove(bVar);
    }

    protected static void d(Activity activity) {
    }

    public static LinkedHashSet<b> hg() {
        return mO;
    }

    protected static void onDestroy() {
        if (!mO.isEmpty()) {
            Iterator<b> it = mO.iterator();
            while (it.hasNext()) {
                b next = it.next();
                next.onDestroy();
                Log.d(LOG_TAG, next.getName() + " has destroyed.");
            }
        }
    }

    public static b y(String str, String str2) {
        Exception e;
        b bVar;
        try {
            bVar = (b) Class.forName(str).newInstance();
            try {
                bVar.bu(str2);
                mO.add(bVar);
                Log.d(LOG_TAG, bVar.getName() + " has added.");
            } catch (Exception e2) {
                e = e2;
                Log.w(LOG_TAG, e);
                return bVar;
            }
        } catch (Exception e3) {
            Exception exc = e3;
            bVar = null;
            e = exc;
            Log.w(LOG_TAG, e);
            return bVar;
        }
        return bVar;
    }
}
