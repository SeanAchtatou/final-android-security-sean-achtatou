package org.meteoroid.core;

import android.util.Log;
import com.a.a.r.a;

public final class c {
    private static final String LOG_TAG = "DeviceManager";
    public static a mN;

    protected static a bi(String str) {
        try {
            mN = (a) Class.forName(str).newInstance();
            mN.onCreate();
        } catch (Exception e) {
            Log.e(LOG_TAG, "Failed to create device. " + e);
            e.printStackTrace();
        }
        return mN;
    }

    protected static final void onDestroy() {
        if (mN != null) {
            mN.onDestroy();
        }
    }
}
