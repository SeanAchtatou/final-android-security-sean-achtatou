package org.opensocial.providers;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Provider implements Serializable {
    private static final long serialVersionUID = 1;
    private String ji;
    private String name;
    private String tp;
    private String tq;
    private String tr;
    private String ts;
    private String tt;
    private Map<String, String> tu;
    private boolean tv = true;
    private String version;

    public void B(String str, String str2) {
        if (this.tu == null) {
            this.tu = new HashMap();
        }
        this.tu.put(str, str2);
    }

    public void G(boolean z) {
        this.tv = z;
    }

    public void cn(String str) {
        this.version = str;
    }

    public void co(String str) {
        this.tp = str;
    }

    public void cp(String str) {
        this.tq = str;
    }

    public void cq(String str) {
        this.tr = str;
    }

    public void cr(String str) {
        this.ts = str;
    }

    public void cs(String str) {
        this.tt = str;
    }

    public void d(Map<String, String> map) {
        this.tu = map;
    }

    public String getContentType() {
        return this.ji == null ? "application/json" : this.ji;
    }

    public String getName() {
        return this.name;
    }

    public String getVersion() {
        return this.version == null ? "0.8" : this.version;
    }

    public String kK() {
        return this.tp;
    }

    public String kL() {
        return this.tq;
    }

    public String kM() {
        return this.tr;
    }

    public String kN() {
        return this.ts;
    }

    public String kO() {
        return this.tt;
    }

    public Map<String, String> kP() {
        return this.tu;
    }

    public boolean kQ() {
        return this.tv;
    }

    public void setContentType(String str) {
        this.ji = str;
    }

    public void setName(String str) {
        this.name = str;
    }
}
