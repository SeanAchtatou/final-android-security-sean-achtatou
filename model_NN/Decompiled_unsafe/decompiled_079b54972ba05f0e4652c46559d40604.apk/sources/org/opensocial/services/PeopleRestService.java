package org.opensocial.services;

import org.opensocial.models.Person;
import org.opensocial.providers.Provider;

public abstract class PeopleRestService extends RestService {
    public PeopleRestService(Provider provider) {
        super(provider);
        cw("people/{id}/{groupId}");
    }

    public abstract Person E(String str, String str2);

    public abstract Person[] F(String str, String str2);

    public Person cu(String str) {
        return E(str, Service.SELF);
    }

    public Person[] cv(String str) {
        return F(str, Service.FRIENDS);
    }
}
