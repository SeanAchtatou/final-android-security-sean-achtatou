package org.opensocial.services;

import org.opensocial.models.AppData;
import org.opensocial.providers.Provider;

public abstract class AppDataRestService extends RestService {
    public AppDataRestService(Provider provider) {
        super(provider);
        cw("appdata/{id}/{groupId}/{appId}");
    }

    public abstract AppData a(String str, String str2, String str3, AppData appData);

    public abstract AppData g(String str, String str2, String str3);

    public abstract boolean h(String str, String str2, String str3);
}
