package org.opensocial.services;

import org.opensocial.models.Group;
import org.opensocial.providers.Provider;

public abstract class GroupsRestService extends RestService {
    public GroupsRestService(Provider provider) {
        super(provider);
        cw("groups/{id}");
    }

    public abstract Group C(String str, String str2);

    public abstract boolean D(String str, String str2);

    public abstract Group a(String str, String str2, Group group);

    public abstract Group[] ct(String str);
}
