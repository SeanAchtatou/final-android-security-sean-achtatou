package org.opensocial.services;

import org.opensocial.providers.Provider;

public interface Service {
    public static final String APP = "@app";
    public static final String FRIENDS = "@friends";
    public static final String ME = "@me";
    public static final String SELF = "@self";
    public static final String VIEWER = "@viewer";

    Provider kS();
}
