package org.opensocial.models;

import java.util.HashMap;

public class AppData extends Model {
    private HashMap<String, String> sb = new HashMap<>();

    public String getString(String str) {
        return this.sb.get(str);
    }

    public void setString(String str, String str2) {
        this.sb.put(str, str2);
    }
}
