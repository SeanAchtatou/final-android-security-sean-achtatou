package org.opensocial.models;

import java.util.Date;

public class Person extends Model {
    public static final String GENDER_FEMALE = "female";
    public static final String GENDER_MALE = "male";
    public static final String GENDER_UNDISCLOSED = "undisclosed";
    public static final String NETWORK_PRESENCE_AWAY = "AWAY";
    public static final String NETWORK_PRESENCE_CHAT = "CHAT";
    public static final String NETWORK_PRESENCE_DND = "DND";
    public static final String NETWORK_PRESENCE_OFFLINE = "_OFFLINE";
    public static final String NETWORK_PRESENCE_ONLINE = "ONLINE";
    public static final String NETWORK_PRESENCE_XA = "XA";
    private boolean connected;
    private String ns;
    private String[] sA;
    private String sB;
    private String[] sC;
    private String sD;
    private Date sE;
    private String[] sF;
    private DateUTCOffset sG;
    private String se;
    private String[] sf;
    private String sg = GENDER_UNDISCLOSED;
    private Date sh;
    private String[] si;
    private String sj;
    private String sk;
    private Account[] sl;
    private Address[] sm;
    private Name[] sn;
    private AppData[] so;
    private String sp;
    private String[] sq;
    private Name sr;
    private Name ss;
    private String st;
    private Organization[] su;
    private String[] sv;
    private Name sw;
    private String sx;
    private String sy;
    private Date sz;

    public void D(boolean z) {
        this.connected = z;
    }

    public void a(DateUTCOffset dateUTCOffset) {
        this.sG = dateUTCOffset;
    }

    public void a(Name name) {
        this.sr = name;
    }

    public void a(Account[] accountArr) {
        this.sl = accountArr;
    }

    public void a(Address[] addressArr) {
        this.sm = addressArr;
    }

    public void a(AppData[] appDataArr) {
        this.so = appDataArr;
    }

    public void a(Name[] nameArr) {
        this.sn = nameArr;
    }

    public void a(Organization[] organizationArr) {
        this.su = organizationArr;
    }

    public void b(Date date) {
        this.sh = date;
    }

    public void b(Name name) {
        this.ss = name;
    }

    public void bM(String str) {
        this.se = str;
    }

    public void bN(String str) {
        this.sg = str;
    }

    public void bO(String str) {
        this.sj = str;
    }

    public void bP(String str) {
        this.sk = str;
    }

    public void bQ(String str) {
        this.sp = str;
    }

    public void bR(String str) {
        this.ns = str;
    }

    public void bS(String str) {
        this.st = str;
    }

    public void bT(String str) {
        this.sx = str;
    }

    public void bU(String str) {
        this.sy = str;
    }

    public void bV(String str) {
        this.sB = str;
    }

    public void bW(String str) {
        this.sD = str;
    }

    public void c(Date date) {
        this.sz = date;
    }

    public void c(Name name) {
        this.sw = name;
    }

    public void c(String[] strArr) {
        this.sf = strArr;
    }

    public void d(Date date) {
        this.sE = date;
    }

    public void d(String[] strArr) {
        this.si = strArr;
    }

    public void e(String[] strArr) {
        this.sq = strArr;
    }

    public void f(String[] strArr) {
        this.sv = strArr;
    }

    public void g(String[] strArr) {
        this.sA = strArr;
    }

    public String getDisplayName() {
        return this.se;
    }

    public String getLocation() {
        return this.ns;
    }

    public void h(String[] strArr) {
        this.sC = strArr;
    }

    public void i(String[] strArr) {
        this.sF = strArr;
    }

    public boolean isConnected() {
        return this.connected;
    }

    public String[] jB() {
        return this.sf;
    }

    public String jC() {
        return this.sg;
    }

    public Date jD() {
        return this.sh;
    }

    public String[] jE() {
        return this.si;
    }

    public String jF() {
        return this.sj;
    }

    public String jG() {
        return this.sk;
    }

    public Account[] jH() {
        return this.sl;
    }

    public Address[] jI() {
        return this.sm;
    }

    public Name[] jJ() {
        return this.sn;
    }

    public AppData[] jK() {
        return this.so;
    }

    public String jL() {
        return this.sp;
    }

    public String[] jM() {
        return this.sq;
    }

    public Name jN() {
        return this.sr;
    }

    public Name jO() {
        return this.ss;
    }

    public String jP() {
        return this.st;
    }

    public Organization[] jQ() {
        return this.su;
    }

    public String[] jR() {
        return this.sv;
    }

    public Name jS() {
        return this.sw;
    }

    public String jT() {
        return this.sx;
    }

    public String jU() {
        return this.sy;
    }

    public Date jV() {
        return this.sz;
    }

    public String[] jW() {
        return this.sA;
    }

    public String jX() {
        return this.sB;
    }

    public String[] jY() {
        return this.sC;
    }

    public String jZ() {
        return this.sD;
    }

    public Date ka() {
        return this.sE;
    }

    public String[] kb() {
        return this.sF;
    }

    public DateUTCOffset kc() {
        return this.sG;
    }
}
