package org.opensocial.models.skuld;

import org.opensocial.models.Model;

public final class Achievement extends Model {
    public boolean sJ;
    public boolean sK;
    public int sL;

    public void E(boolean z) {
        this.sJ = z;
    }

    public void F(boolean z) {
        this.sK = z;
    }

    public void cx(int i) {
        this.sL = i;
    }

    public boolean kf() {
        return this.sJ;
    }

    public boolean kg() {
        return this.sK;
    }

    public int kh() {
        return this.sL;
    }
}
