package org.opensocial.models.skuld;

import org.opensocial.models.Model;

public final class Score extends Model {
    public int score;
    private String ta;
    public int tn;
    public int to;

    public void cG(int i) {
        this.score = i;
    }

    public void cH(int i) {
        this.tn = i;
    }

    public void cI(int i) {
        this.to = i;
    }

    public void ci(String str) {
        this.ta = str;
    }

    public int kH() {
        return this.score;
    }

    public int kI() {
        return this.tn;
    }

    public int kJ() {
        return this.to;
    }

    public String kw() {
        return this.ta;
    }
}
