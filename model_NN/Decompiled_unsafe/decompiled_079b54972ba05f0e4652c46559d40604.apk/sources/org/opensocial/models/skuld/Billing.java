package org.opensocial.models.skuld;

import org.opensocial.models.Model;

public final class Billing extends Model {
    public static final int BILLING_TYPE_DEFAULT = 0;
    public static final int BILLING_TYPE_OTHER = 4;
    public static final int BILLING_TYPE_SMS = 1;
    public static final int BILLING_TYPE_WAP = 2;
    public static final int BILLING_TYPE_WEB = 3;
    public float sZ;
    private String ta;
    public int type;

    public void ci(String str) {
        this.ta = str;
    }

    public int getType() {
        return this.type;
    }

    public void k(float f) {
        this.sZ = f;
    }

    public float kv() {
        return this.sZ;
    }

    public String kw() {
        return this.ta;
    }

    public void setType(int i) {
        this.type = i;
    }
}
