package org.opensocial.models.skuld;

import org.opensocial.models.Model;

public class Application extends Model {
    private String name;
    private String sM;
    private String sN;
    private String sO;
    private long sP;
    private String sQ;
    private int sR;
    private int sS;
    private String sT;
    private String sU;
    private String sV;
    private String sW;
    private String sX;
    private long sY;
    private int versionCode;
    private String versionName;

    public void bY(String str) {
        this.sM = str;
    }

    public void bZ(String str) {
        this.sN = str;
    }

    public void cA(int i) {
        this.sS = i;
    }

    public void ca(String str) {
        this.sO = str;
    }

    public void cb(String str) {
        this.sQ = str;
    }

    public void cc(String str) {
        this.versionName = str;
    }

    public void cd(String str) {
        this.sT = str;
    }

    public void ce(String str) {
        this.sU = str;
    }

    public void cf(String str) {
        this.sV = str;
    }

    public void cg(String str) {
        this.sW = str;
    }

    public void ch(String str) {
        this.sX = str;
    }

    public void cy(int i) {
        this.versionCode = i;
    }

    public void cz(int i) {
        this.sR = i;
    }

    public String getName() {
        return this.name;
    }

    public int gq() {
        return this.versionCode;
    }

    public String gr() {
        return this.versionName;
    }

    public String ki() {
        return this.sM;
    }

    public String kj() {
        return this.sN;
    }

    public String kk() {
        return this.sO;
    }

    public long kl() {
        return this.sP;
    }

    public String km() {
        return this.sQ;
    }

    public int kn() {
        return this.sR;
    }

    public int ko() {
        return this.sS;
    }

    public String kp() {
        return this.sT;
    }

    public String kq() {
        return this.sU;
    }

    public String kr() {
        return this.sV;
    }

    public String ks() {
        return this.sW;
    }

    public String kt() {
        return this.sX;
    }

    public long ku() {
        return this.sY;
    }

    public void q(long j) {
        this.sP = j;
    }

    public void r(long j) {
        this.sY = j;
    }

    public void setName(String str) {
        this.name = str;
    }
}
