package org.opensocial.models.skuld;

import org.opensocial.models.Model;

public final class Challenge extends Model {
    public String tb;
    public long tc;
    public String td;

    public void cj(String str) {
        this.tb = str;
    }

    public void ck(String str) {
        this.td = str;
    }

    public long getScore() {
        return this.tc;
    }

    public String kx() {
        return this.tb;
    }

    public String ky() {
        return this.td;
    }

    public void s(long j) {
        this.tc = j;
    }
}
