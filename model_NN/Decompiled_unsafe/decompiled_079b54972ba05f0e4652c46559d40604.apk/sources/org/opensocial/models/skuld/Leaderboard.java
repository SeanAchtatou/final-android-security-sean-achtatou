package org.opensocial.models.skuld;

import org.opensocial.models.Model;

public final class Leaderboard extends Model {
    public int te;
    public int tf;
    public String tg;
    public int th;

    public void cB(int i) {
        this.te = i;
    }

    public void cC(int i) {
        this.tf = i;
    }

    public void cD(int i) {
        this.th = i;
    }

    public void cl(String str) {
        this.tg = str;
    }

    public int kA() {
        return this.tf;
    }

    public String kB() {
        return this.tg;
    }

    public int kC() {
        return this.th;
    }

    public int kz() {
        return this.te;
    }
}
