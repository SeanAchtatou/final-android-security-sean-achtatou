package defpackage;

/* renamed from: a  reason: default package */
public final class a {
    public static boolean ad;
    public int C;
    public int E;
    public int F;
    public int G;
    public int H;
    public int I;
    public int J;
    public int K;
    public String R;
    public a ab;
    public a ac;
    public boolean ae;
    public int[] af;
    public int[] ag;
    public int[] ah;
    public f ai;
    public byte[][] aj;
    public byte[][] al;
    public int[] am;
    public int[] an;
    public int ao;
    public int ap;
    public int aq;
    public int ar;
    public boolean as;
    public boolean at;
    public boolean au;
    public boolean av;
    public short aw;
    public short ax;
    public int ay;
    public int g;
    public int h;
    public int i;
    public int j;
    public boolean k;
    public String m;
    public int n;
    public int o;
    public int p;
    public int q;
    public int r;
    public int s;
    public int u;
    public int v;
    public int x;
    public int y;
    public int z;

    public a() {
        this.g = -1;
        this.h = 1;
        this.o = 1;
        this.r = 1;
        this.s = 1;
        this.G = 5;
        this.an = new int[4];
        this.ap = -1;
        this.aq = this.an[0];
        this.ar = -1;
        this.ay = 2;
    }

    private a(int[] iArr) {
        this.g = -1;
        this.h = 1;
        this.o = 1;
        this.r = 1;
        this.s = 1;
        this.G = 5;
        this.an = new int[4];
        this.ap = -1;
        this.aq = this.an[0];
        this.ar = -1;
        this.ay = 2;
        this.am = iArr;
    }

    private a(int[] iArr, f fVar) {
        this(iArr);
        this.ai = fVar;
    }

    private int b() {
        return this.an[0];
    }

    public static int h(int i2) {
        return i2 - c.bl;
    }

    public final void a(int i2) {
        this.x = c.bl + i2;
    }

    public final void a(a aVar) {
        this.x = aVar.x;
        this.y = aVar.y;
        this.z = aVar.z;
    }

    public final void a(int[] iArr, f fVar) {
        this.ac = new a(iArr, fVar);
        this.ac.av = true;
    }

    public final boolean a(a aVar, int i2, int i3) {
        return Math.abs(aVar.z - this.z) <= i3 && Math.abs(aVar.x - this.x) <= i2;
    }

    public final void b(int i2) {
        this.y = c.bm + i2;
    }

    public final int c() {
        return this.an[1];
    }

    public final int d() {
        return this.an[3];
    }

    public final void d(int i2) {
        this.ay = i2;
        if (this.ai == null) {
            return;
        }
        if ((i2 & 1) != 0) {
            this.aj = this.ai.eO;
            this.al = this.ai.eP;
            return;
        }
        this.aj = this.ai.eQ;
        this.al = this.ai.eR;
    }

    public final int i(int i2) {
        return this.aj[b()][i2];
    }

    public final int j(int i2) {
        return this.al[b()][i2];
    }
}
