package defpackage;

/* renamed from: e  reason: default package */
public final class e implements Runnable {
    public int aA;
    public Thread eI;
    public final c eJ;

    public e(c cVar, int i, int i2, boolean z) {
        this.eJ = cVar;
        cVar.n(21);
        cVar.k();
        c.G(c.cv);
        this.aA = i;
        cVar.aY = true;
        cVar.ba = i2;
        cVar.aZ = z;
        cVar.K = 0;
    }

    public e(c cVar, String str, int i, boolean z) {
        this.eJ = cVar;
        cVar.n(21);
        cVar.k();
        c.G(c.cv);
        cVar.aP = str;
        this.aA = -1;
        cVar.aY = true;
        cVar.ba = i;
        cVar.aZ = z;
        cVar.K = 0;
    }

    public final void a() {
        this.eI = new Thread(this);
        this.eI.start();
    }

    public final void run() {
        try {
            Thread.sleep(20);
        } catch (Exception e) {
        }
        if (this.aA != -1) {
            this.eJ.a(this.aA);
        } else {
            c.cb = 5;
            this.eJ.f(this.eJ.n(this.eJ.aP));
            this.eJ.C(3);
            while (c.cb != 5) {
                this.eJ.X();
                this.eJ.k();
            }
        }
        this.eJ.aY = false;
        this.eJ.K = 120;
    }
}
