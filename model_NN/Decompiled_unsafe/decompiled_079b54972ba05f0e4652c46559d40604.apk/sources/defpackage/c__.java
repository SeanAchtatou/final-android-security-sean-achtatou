package defpackage;

import c__om.a.a.e.c;
import c__om.a.a.e.l;
import c__om.a.a.e.o;
import c__om.a.a.e.p;
import c__om.a.a.l.f;
import com.a.a.e.z;
import com.a.a.i.a;
import com.fasterxml.jackson.core.sym.CharsToNameCanonicalizer;
import com.fasterxml.jackson.core.util.BufferRecycler;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.lang.reflect.Array;
import java.util.Hashtable;
import java.util.Random;
import javax.microedition.enhance.MIDPHelper;
import javax.microedition.media.Manager;
import javax.microedition.media.Player;
import javax.microedition.media.control.MIDIControl;
import javax.microedition.media.control.ToneControl;
import javax.microedition.media.control.VolumeControl;
import me.gall.sgp.sdk.service.BossService;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    */
/* renamed from: c__  reason: default package */
public abstract class c__ extends c implements Runnable {
    public static byte aB;
    public static byte aC;
    public static final int[] aD = {1, 1, 2, 2, 3, 4, 3, 4, 55, 57};
    public static int[] aE = new int[aD.length];
    public static final int[] aF;
    public static l aH = l.i(0, 0, 8);
    public static int aL = -1;
    public static boolean aM;
    public static final int aT = (bj * 16);
    public static final int aU = (bk * 11);
    public static final int ak = a("中");
    public static int ay;
    public static final int bg = (160 - (bk << 1));
    public static int bi;
    public static int bj = 16;
    public static int bk = 16;
    public static int bl;
    public static int bm;
    public static int bn;
    private static int bo;
    private static int bp;
    private static int bq;
    private static int br;
    public static p bs;
    public static o bt;
    public static int bz = 120;
    public static String[] cG;
    public static String[] cH = {"logo.png", "sign.png", "rim.png", "num.png", "head.png", "body.png", "hand.png", "crura.png", "player.ani", "title/012.png", "lei.png", "lei.ani", "eRed.png", "eRed.ani", "eBlue.png", "eBlue.ani", "eSword.ani", "sL.png", "sL.ani", "pUi.png", "num1.png", "goods.png", "goods.ani", "door.png", "door.ani", "fire.png", "fire.ani", "ja.png", "ja.ani", "eAi.png", "eAi1.png", "eAi.ani", "headSml.png", "title/006.png", "title/005.png", "title/007.png", "title/009.png", "title/003.png", "eCai.pnc", "eCai.ani", "eJin.png", "eJin.ani", "end.png", "over.png", "newsHead.png", "newsTv.png", "news.txt", "hiNum.png", "e.png", "top.png", "mm.png", "mmhead.png", "mmProp.png", "mm.ani", "mmhead2.png", "mm2.ani", "eAi2.png", "eAi2.ani", "last1.png", "last2.png", "down1.png", "top1.png", "soundAsk2.png", "hand3.png", "sp4.png"};
    public static Object[] cI = new Object[cH.length];
    public static int cb;
    public static int cc;
    private static b cd = new b();
    private static Hashtable ce = new Hashtable();
    public static b cf = new b();
    private static long co = 16777216;
    private static long[] cp = {co, 45605201, 123967790, 336979391, 916004956};
    public static Random cq = new Random(System.currentTimeMillis());
    public static final String[] cr = {"bgm.mid"};
    public static int cs = 0;
    public static Player[] ct = null;
    public static Player cu = null;
    public static int cv = 0;
    public static int[] cw = {5000, 4000, 3000, BufferRecycler.DEFAULT_WRITE_CONCAT_BUFFER_LEN, 1000};
    public static String[] cx = {"ABC", "ABC", "ABC", "ABC", "ABC"};
    public static char[] cy = {'A', 'A', 'A'};
    public static final int n = aH.getHeight();
    public static int u;
    public static int v = u;
    public int F;
    public int I;
    public int J;
    public int K;
    public int aG;
    public boolean aI;
    public boolean aJ;
    public int aK;
    public String aN;
    public byte aO;
    public String aP;
    public a aQ;
    public byte aR;
    public boolean aS;
    public byte aV;
    public a aW;
    private b aX;
    public boolean aY;
    public boolean aZ;
    public int ar;
    public int bA;
    public int bB;
    public int bC;
    public a bD;
    public boolean bE;
    public byte bF;
    public byte bG;
    public boolean bH;
    public int bI;
    public int bJ;
    public String bK;
    public b bL;
    public boolean bM;
    public int bN;
    public int bO;
    private b bP;
    private int bQ;
    public byte bR;
    public byte bS;
    public byte bT;
    public short bU;
    public byte bV;
    public byte bW;
    public boolean bX;
    public int bY;
    public int bZ;
    public int ba;
    public p bb;
    public e bc;
    public String be;
    public int bf;
    public byte bh;
    public short[][] bu;
    public byte[][] bv;
    public p bw;
    public int bx;
    public int by;
    public int cA;
    public boolean cB;
    public int cC;
    public int cD;
    public int cE;
    private String cF;
    public int cJ;
    public int cK;
    public int cL;
    public int cM;
    public b ca;
    public int cg;
    public boolean ch;
    public boolean ci;
    public b cj;
    public String ck;
    public int cl;
    public b cm;
    public int cn;
    public int cz;
    public int j;
    public int o;
    public int p;
    public int q;
    public int x;

    static {
        byte[] bArr = {50, 56, 52, 54, 53, 49, 51, 48};
        int[] iArr = new int[4];
        iArr[1] = 30888;
        iArr[3] = 88701;
        aF = iArr;
        new StringBuffer();
    }

    public static void A(String str) {
        for (int i = 0; i < cH.length; i++) {
            if (cH[i].compareTo(str) == 0) {
                Q(i);
                return;
            }
        }
    }

    public static void B(String str) {
        for (int i = 0; i < cH.length; i++) {
            if (cH[i].toLowerCase().compareTo(str) == 0) {
                S(i);
                return;
            }
        }
    }

    public static void F(int i) {
        if (ct == null) {
            ct = new Player[cr.length];
        }
        if (ct[i] == null) {
            try {
                DataInputStream z = z(new StringBuffer("/res/").append(cr[i]).toString());
                new StringBuffer("/res/").append(cr[i]).append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR).append(y(cr[i])).toString();
                ct[i] = Manager.a(z, y(cr[i]));
                ct[i].dD();
                ((VolumeControl) ct[i].ad("VolumeControl")).bl(100);
            } catch (Exception e) {
                a(e, "load");
            }
        }
    }

    public static void G(int i) {
        try {
            if (ct != null && ct[i] != null && ct[i].getState() == 400) {
                ct[i].stop();
            }
        } catch (Exception e) {
            a(e, "stop");
        }
    }

    public static final boolean I(int i) {
        return d(i, 5) || d(i, 53) || d(i, 6);
    }

    public static final boolean J(int i) {
        return d(i, 7) || d(i, 35);
    }

    public static final boolean K(int i) {
        return d(i, 6);
    }

    public static final boolean L(int i) {
        return d(i, 1) || d(i, 50);
    }

    public static final boolean M(int i) {
        return d(i, 2) || d(i, 56);
    }

    public static int N(int i) {
        return ((p) Q(i)).getWidth();
    }

    public static int O(int i) {
        return ((p) Q(i)).getHeight();
    }

    public static boolean O() {
        return aC != 0;
    }

    public static p P(int i) {
        return (p) Q(i);
    }

    public static Object Q(int i) {
        String str = cH[i];
        if (cI[i] == null) {
            if (str.toLowerCase().endsWith("png")) {
                cI[i] = m(str);
            } else if (str.toLowerCase().endsWith("ani")) {
                cI[i] = l(str);
            } else if (str.toLowerCase().endsWith("pnc")) {
                String[] d = d("/res/".concat(str), 2);
                String str2 = d[d.length - 1];
                int[] iArr = new int[(d.length - 1)];
                for (int i2 = 0; i2 < iArr.length; i2++) {
                    iArr[i2] = Integer.parseInt(d[i2].trim(), 16);
                }
                cI[i] = a(str2, iArr);
            } else if (str.toLowerCase().endsWith("txt")) {
                cI[i] = d("/res/".concat(str), 2);
            } else {
                str.toLowerCase().endsWith("pak");
            }
        }
        return cI[i];
    }

    public static void R(int i) {
        Q(i);
    }

    public static void S(int i) {
        cI[i] = null;
        System.gc();
    }

    public static boolean T(int i) {
        return i == 1;
    }

    public static void V() {
        cb = 5;
        cd.f();
        ce.clear();
        cc = 0;
    }

    public static byte a(short s) {
        return (byte) ((s >> 8) & -1);
    }

    public static int a(int i, p pVar, int i2, int i3, int i4, o oVar, int i5, int i6) {
        if (i == -1) {
            return -1;
        }
        int width = pVar.getWidth() / i5;
        a(oVar, pVar, (i % width) * i5, (i / width) * i6, i5, i6, 0, i2, i3, i4);
        return width;
    }

    public static int a(f fVar, int i, int i2) {
        return fVar.eM[i][i2 << 1] & 255;
    }

    public static final int a(String str) {
        return aH.R(str);
    }

    public static int a(byte[] bArr, int i) {
        return ((bArr[i] & ToneControl.SILENCE) << 24) | ((bArr[i + 1] & ToneControl.SILENCE) << 16) | ((bArr[i + 2] & ToneControl.SILENCE) << 8) | (bArr[i + 3] & ToneControl.SILENCE);
    }

    public static int a(int[] iArr, char c) {
        if (c != '+') {
            return 0;
        }
        int i = 0;
        for (int i2 : iArr) {
            i += i2;
        }
        return i;
    }

    private int a(int[] iArr, int[] iArr2, int i, int i2, int i3) {
        while (true) {
            i++;
            if (iArr[i] >= i3) {
                while (i2 != 0) {
                    i2--;
                    if (iArr[i2] <= i3) {
                        break;
                    }
                }
                a(iArr, i, i2);
                a(iArr2, i, i2);
                if (i >= i2) {
                    a(iArr2, i, i2);
                    a(iArr, i, i2);
                    return i;
                }
            }
        }
    }

    public static b a(String str, l lVar, int i, char c) {
        b bVar = new b();
        try {
            String stringBuffer = new StringBuffer(String.valueOf(str)).append(c).toString();
            int i2 = 0;
            int i3 = 0;
            for (int i4 = 0; i4 < stringBuffer.length(); i4++) {
                char charAt = stringBuffer.charAt(i4);
                boolean z = charAt == c;
                if (((charAt == ' ' || charAt == ',') && a(charAt)) || z || !a(charAt)) {
                    i3 = i4;
                }
                boolean z2 = lVar.R(stringBuffer.substring(i2, i4 + 1)) > i;
                if (z2 || z) {
                    if (z2) {
                        i3 = i4;
                    }
                    bVar.c(stringBuffer.substring(i2, i3));
                    i2 = z ? i3 + 1 : i3;
                }
            }
        } catch (Exception e) {
            a(e, "subS");
        }
        return bVar;
    }

    public static p a(String str, int[] iArr) {
        byte[] x2 = x(str);
        a(x2, iArr);
        return p.e(x2, 0, x2.length);
    }

    public static f a(int[] iArr) {
        return (f) cI[(short) iArr[iArr.length - 1]];
    }

    public static void a(a aVar, int i, int i2, int i3, int i4) {
        if (a.ad) {
            i >>= 1;
            i2 >>= 1;
            i3 >>= 1;
            i4 >>= 1;
        }
        aVar.I = i;
        aVar.H = i2;
        aVar.J = i3;
        aVar.K = i4;
    }

    public static void a(a aVar, String str) {
        aVar.R = str;
    }

    public static void a(o oVar, int i) {
        oVar.setColor(i);
        oVar.i(0, 0, 240, 340);
    }

    private void a(o oVar, int i, int i2, int i3, int i4, int i5, int i6) {
        if (oVar != null) {
            int i7 = this.bY + i5;
            int i8 = this.bZ + i6;
            oVar.setColor(0);
            oVar.j(i7, i8, i3, i4);
            oVar.a(bs, i7 - i, i8 - i2, 20);
        }
    }

    public static void a(o oVar, int i, int i2, int i3, int i4, int i5, boolean z, int i6) {
        oVar.setColor(i5);
        int[] b = b(i6, i3, i4);
        if (z) {
            oVar.i(b[0] + i, b[1] + i2, i3, i4);
        } else {
            oVar.h(b[0] + i, b[1] + i2, i3, i4);
        }
    }

    public static void a(o oVar, int i, int i2, int i3, int i4, p pVar, int[] iArr, boolean z, int i5, int i6, int i7) {
        int[] b = b(i7, i3, i4);
        if (z) {
            oVar.setColor(iArr[iArr.length - 1]);
            oVar.i(i + 2 + b[0], i2 + 2 + b[1], (i3 - 2) - 1, (i4 - 2) - 1);
        }
        if (pVar == null) {
            int i8 = 0;
            while (true) {
                int i9 = i8;
                if (i9 < iArr.length - 1) {
                    oVar.setColor(iArr[i9]);
                    oVar.c(b[0] + i + i9, b[1] + i2 + i9, i3 - ((i9 * 2) + 1), i4 - ((i9 * 2) + 1), 5, 5);
                    i8 = i9 + 1;
                } else {
                    return;
                }
            }
        } else {
            int i10 = 0;
            while (true) {
                int i11 = i10;
                if (i11 >= iArr.length - 1) {
                    a(oVar, pVar, 0, 0, i5, i6, 0, i + b[0], i2 + b[1], 0);
                    a(oVar, pVar, i5, 0, i5, i6, 0, ((i + i3) - i5) + b[0], i2 + b[1], 0);
                    o oVar2 = oVar;
                    p pVar2 = pVar;
                    a(oVar2, pVar2, i5 * 2, 0, i5, i6, 0, i + b[0], ((i2 + i4) - i5) + b[1], 0);
                    o oVar3 = oVar;
                    p pVar3 = pVar;
                    a(oVar3, pVar3, i5 * 3, 0, i5, i6, 0, ((i + i3) - i5) + b[0], ((i2 + i4) - i6) + b[1], 0);
                    return;
                }
                oVar.setColor(iArr[i11]);
                oVar.c(b[0] + i + i11, b[1] + i2 + i11, i3 - ((i11 * 2) + 1), i4 - ((i11 * 2) + 1), 5, 5);
                i10 = i11 + 1;
            }
        }
    }

    public static void a(o oVar, p pVar, int i, int i2, int i3, int i4) {
        a(oVar, pVar, 0, 0, pVar.getWidth(), pVar.getHeight(), i3, i, i2, i4);
    }

    public static void a(o oVar, p pVar, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        try {
            oVar.a(pVar, i, i2, i3, i4, i5 == 2 ? 1 : i5 == 1 ? 2 : i5, i6, i7, i8);
        } catch (Exception e) {
        }
    }

    public static void a(o oVar, String str, int i, int i2, int i3) {
        int[] b = b(i3, a(str), n);
        oVar.a(str, b[0] + i, b[1] + i2, 0);
    }

    public static void a(p pVar, String str, int i, int i2, int i3, o oVar, int i4, int i5, int i6, int i7) {
        int i8 = i4 + i6;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(str);
        if (stringBuffer.length() < i7) {
            for (int length = stringBuffer.length(); length < i7; length++) {
                stringBuffer.insert(0, "0");
            }
        }
        int[] b = b(i3, stringBuffer.length() * i8, i5);
        if (pVar == null) {
            oVar.setColor(16711680);
            a(oVar, str, b[0] + i, b[1] + i2, 0);
            return;
        }
        int i9 = 0;
        while (true) {
            int i10 = i9;
            if (i10 < stringBuffer.length()) {
                int charAt = stringBuffer.charAt(i10);
                if ((charAt < 58) && (charAt > 47)) {
                    a(oVar, pVar, i4 * (charAt - 48), 0, i4, i5, 0, (i10 * i8) + i + b[0], i2 + b[1], 0);
                } else {
                    if (((charAt < 123) & (charAt > 96)) || ((charAt > 64) & (charAt < 91))) {
                        int i11 = (charAt < 123) & (charAt > 96) ? charAt - 97 : charAt - 65;
                        a(oVar, pVar, i4 * i11, 0, i4, i5, 0, (i10 * i8) + i + b[0], i2 + b[1], 0);
                        charAt = i11;
                    }
                    if (charAt == 46) {
                        a(oVar, pVar, i4 * 36, 0, i4, i5, 0, (i10 * i8) + i + b[0], i2 + b[1], 0);
                        charAt = 36;
                    }
                    if (charAt == 58) {
                        a(oVar, pVar, i4 * 11, 0, i4, i5, 0, (i10 * i8) + i + b[0], i2 + b[1], 0);
                        charAt = 11;
                    }
                    if (charAt == 47) {
                        a(oVar, pVar, i4 * 10, 0, i4, i5, 0, (i10 * i8) + i + b[0], i2 + b[1], 0);
                    }
                }
                i9 = i10 + 1;
            } else {
                return;
            }
        }
    }

    public static void a(DataInputStream dataInputStream) {
        try {
            dataInputStream.close();
            System.gc();
        } catch (Exception e) {
        }
    }

    public static void a(Exception exc, String str) {
        exc.printStackTrace();
        System.out.println(new StringBuffer("!-------------\n").append(str).toString());
        System.out.println(str);
        System.out.println("--------------");
    }

    public static void a(byte[] bArr, int[] iArr) {
        if (bArr != null && iArr != null) {
            int i = 8;
            while (true) {
                if (bArr[i + 4] == 80 && bArr[i + 5] == 76 && bArr[i + 6] == 84 && bArr[i + 7] == 69) {
                    break;
                }
                i += a(bArr, i) + 8 + 4;
            }
            int a = a(bArr, i);
            int i2 = a / 3;
            int i3 = i + 8;
            int i4 = i + 8 + a;
            int i5 = i3;
            int i6 = 0;
            while (i6 < i2) {
                int i7 = 0;
                while (true) {
                    if (i7 < (iArr.length >> 1)) {
                        int i8 = i7 << 1;
                        byte b = (byte) ((iArr[i8] >> 16) & 255);
                        byte b2 = (byte) ((iArr[i8] >> 8) & 255);
                        byte b3 = (byte) (iArr[i8] & 255);
                        byte b4 = bArr[i5];
                        byte b5 = bArr[i5 + 1];
                        byte b6 = bArr[i5 + 2];
                        if (b == b4 && b2 == b5 && b3 == b6) {
                            int i9 = i8 + 1;
                            bArr[i5] = (byte) ((iArr[i9] >> 16) & 255);
                            bArr[i5 + 1] = (byte) ((iArr[i9] >> 8) & 255);
                            bArr[i5 + 2] = (byte) (iArr[i9] & 255);
                            break;
                        }
                        i7++;
                    } else {
                        break;
                    }
                }
                i6++;
                i5 += 3;
            }
            int i10 = i3 - 4;
            int i11 = (i2 * 3) + 4;
            int[] iArr2 = new int[256];
            for (int i12 = 0; i12 < iArr2.length; i12++) {
                int i13 = i12;
                for (int i14 = 0; i14 < 8; i14++) {
                    i13 = (i13 & 1) == 1 ? (i13 >>> 1) ^ -306674912 : i13 >>> 1;
                }
                iArr2[i12] = i13;
            }
            byte b7 = -1;
            for (int i15 = i10; i15 < i11 + i10; i15++) {
                b7 = (b7 >>> 8) ^ iArr2[(bArr[i15] ^ b7) & ToneControl.SILENCE];
            }
            byte b8 = b7 ^ -1;
            bArr[i4] = (byte) ((b8 >> 24) & 255);
            bArr[i4 + 1] = (byte) ((b8 >> 16) & 255);
            bArr[i4 + 2] = (byte) ((b8 >> 8) & 255);
            bArr[i4 + 3] = (byte) (b8 & ToneControl.SILENCE);
        }
    }

    private static void a(int[] iArr, int i, int i2) {
        int i3 = iArr[i];
        iArr[i] = iArr[i2];
        iArr[i2] = i3;
    }

    public static void a(int[] iArr, o oVar, int i, int i2, int i3, boolean z, boolean z2) {
        f a = a(iArr);
        short[] sArr = a.eL[i3];
        short[] sArr2 = a.eK;
        int length = sArr.length;
        for (int i4 = 0; i4 < length; i4 += 4) {
            int i5 = (sArr[i4] & 65535) * 5;
            short s = sArr[i4 + 1] & 255;
            int i6 = iArr[sArr2[i5]];
            short s2 = sArr2[i5 + 1];
            short s3 = sArr2[i5 + 2];
            short s4 = sArr2[i5 + 3];
            short s5 = sArr2[i5 + 4];
            boolean z3 = z ^ ((s & 1) != 0);
            boolean z4 = z2 ^ ((s & 2) != 0);
            a(oVar, (p) cI[i6], s2, s3, s4, s5, (z3 || z4) ? (!z3 || z4) ? (z3 || !z4) ? 3 : 2 : 1 : 0, (z ? i - sArr[i4 + 2] : sArr[i4 + 2] + i) - (z3 ? s4 : 0), (z2 ? i2 - sArr[i4 + 3] : sArr[i4 + 3] + i2) - (z4 ? s5 : 0), 0);
        }
    }

    private void a(int[] iArr, int[] iArr2, int i, int i2) {
        int i3 = (i + i2) / 2;
        a(iArr, i3, i2);
        a(iArr2, i3, i2);
        int a = a(iArr, iArr2, i - 1, i2, iArr[i2]);
        a(iArr, a, i2);
        a(iArr2, a, i2);
        if (a - i > 1) {
            a(iArr, iArr2, i, a - 1);
        }
        if (i2 - a > 1) {
            a(iArr, iArr2, a + 1, i2);
        }
    }

    public static boolean a(char c) {
        return ((short) c) < 255;
    }

    public static boolean a(int i, int i2, int i3, int i4, int i5, int i6) {
        return !((i3 > i + 0) | (((i4 + i6 < i2) | (i4 > i2 + 0)) | (i3 + i5 < i)));
    }

    public static boolean a(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        return !((i5 > i + i3) | (((i6 + i8 < i2) | (i6 > i2 + i4)) | (i5 + i7 < i)));
    }

    public static boolean a(a aVar, a aVar2) {
        if (aVar.av || aVar2.av || aVar.aj == null || aVar2.al == null || aVar.au || aVar.au) {
            return false;
        }
        return Math.abs(aVar.z - aVar2.z) <= aVar2.G && b(aVar.i(0) + aVar.x, aVar.i(1) + aVar.y, aVar.i(2) + aVar.x, aVar.i(3) + aVar.y, aVar2.j(0) + aVar2.x, aVar2.j(1) + aVar2.y, aVar2.j(2) + aVar2.x, aVar2.j(3) + aVar2.y);
    }

    public static void ad() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
        try {
            f d = f.d("fb".concat("Top"), true);
            for (int writeInt : cw) {
                dataOutputStream.writeInt(writeInt);
            }
            for (String writeUTF : cx) {
                dataOutputStream.writeUTF(writeUTF);
            }
            if (d.eS() > 0) {
                d.a(1, byteArrayOutputStream.toByteArray(), 0, byteArrayOutputStream.toByteArray().length);
            } else {
                d.g(byteArrayOutputStream.toByteArray(), 0, byteArrayOutputStream.toByteArray().length);
            }
            d.eR();
        } catch (Exception e) {
        }
    }

    public static void ae() {
        try {
            DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(f.d("fb".concat("Top"), true).bI(1)));
            for (int i = 0; i < cw.length; i++) {
                cw[i] = dataInputStream.readInt();
            }
            for (int i2 = 0; i2 < cx.length; i2++) {
                cx[i2] = dataInputStream.readUTF();
            }
        } catch (Exception e) {
        }
    }

    public static void af() {
        for (int i = 0; i < cI.length; i++) {
            cI[i] = null;
        }
        System.gc();
    }

    public static int b(int i, int i2) {
        return Math.abs(i - i2);
    }

    public static void b(int[] iArr) {
        for (int Q : iArr) {
            Q(Q);
        }
    }

    public static boolean b(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        if (i5 + 1 > i3 - 1 || i + 1 > i7 - 1 || i6 + 1 > i4 - 1 || i2 + 1 > i8 - 1) {
            return false;
        }
        if (i == i3 && i2 == i4) {
            return false;
        }
        return (i5 == i7 && i6 == i8) ? false : true;
    }

    public static int[] b(int i, int i2, int i3) {
        switch (i) {
            case 3:
                i2 >>= 1;
                i3 >>= 1;
                break;
            case 6:
                i3 >>= 1;
                i2 = 0;
                break;
            case 10:
                i3 >>= 1;
                break;
            case 17:
                i2 >>= 1;
                i3 = 0;
                break;
            case com.a.a.b.c.UUID:
                i3 = 0;
                break;
            case CharsToNameCanonicalizer.HASH_MULT /*33*/:
                i2 >>= 1;
                break;
            case 36:
                i2 = 0;
                break;
            case com.a.a.b.c.BOOL:
                break;
            default:
                i3 = 0;
                i2 = 0;
                break;
        }
        return new int[]{-i2, -i3};
    }

    public static void c(int i, int i2) {
        if (i <= cr.length - 1) {
            if (!(ct == null || ct[i] == null)) {
                cu = ct[i];
            }
            if (cs != 0 && ct != null && cu != null && cu.getState() != 400) {
                try {
                    cu = ct[i];
                    cu.ba(i2);
                    cu.start();
                    cv = i;
                } catch (Exception e) {
                    a(e, "pl");
                }
            }
        }
    }

    public static void c(int[] iArr) {
        for (int i = 0; i < iArr.length - 1; i++) {
            int i2 = iArr[i];
            int i3 = i;
            for (int i4 = i + 1; i4 < iArr.length; i4++) {
                if (iArr[i4] < i2) {
                    i2 = iArr[i4];
                    i3 = i4;
                }
            }
            iArr[i3] = iArr[i];
            iArr[i] = i2;
        }
    }

    public static int d(int i, int i2, int i3) {
        int i4 = i + i3;
        if (i3 < 0) {
            if (i4 < 0) {
                return i2;
            }
        } else if (i4 > i2) {
            return 0;
        }
        return i4;
    }

    public static int d(a aVar, int i, int i2) {
        int i3 = i - aVar.x;
        int i4 = i2 - aVar.z;
        int i5 = aVar.ay;
        if (i4 > 0) {
            if (i3 > 0) {
                return 10;
            }
            if (i3 < 0) {
                return 9;
            }
            return i5;
        } else if (i4 >= 0) {
            return i3 > 0 ? 2 : 1;
        } else {
            if (i3 > 0) {
                return 6;
            }
            if (i3 < 0) {
                return 5;
            }
            return i5;
        }
    }

    public static boolean d(int i, int i2) {
        return i == i2;
    }

    public static String[] d(String str, int i) {
        StringBuffer stringBuffer = new StringBuffer();
        b bVar = new b();
        DataInputStream z = z(str);
        try {
            z.skip((long) i);
            while (true) {
                char readChar = z.readChar();
                if (readChar == 65535) {
                    break;
                } else if (readChar == 10) {
                    stringBuffer.deleteCharAt(stringBuffer.length() - 1);
                    bVar.c(stringBuffer.toString());
                    stringBuffer.delete(0, stringBuffer.length());
                } else {
                    stringBuffer.append(readChar);
                }
            }
        } catch (Exception e) {
        } catch (Throwable th) {
            a(z);
            throw th;
        }
        a(z);
        if (stringBuffer.length() > 0) {
            bVar.c(stringBuffer.toString());
        }
        String[] strArr = new String[bVar.aA];
        bVar.b(strArr);
        return strArr;
    }

    public static int e(int i, int i2, int i3) {
        boolean z = true;
        boolean z2 = i3 <= 0;
        if (i3 > 0) {
            z = false;
        }
        if (z && z2) {
            return 0;
        }
        int nextInt = cq.nextInt() >>> 1;
        switch (i) {
            case 0:
                return (nextInt % (i3 - i2)) + i2;
            case 1:
                return ((-nextInt) % (i3 - i2)) - i2;
            case 2:
                int nextInt2 = cq.nextInt();
                return nextInt2 < 0 ? (nextInt2 % (i3 - i2)) - i2 : (nextInt2 % (i3 - i2)) + i2;
            case 3:
                return cq.nextInt() < 0 ? (cq.nextInt() % (i3 - i2)) - i2 : (cq.nextInt() % (i3 - i2)) + i2;
            default:
                return 0;
        }
    }

    public static int f(int i, int i2, int i3) {
        return (i - i2) - i3;
    }

    public static int[] f(int i, int i2) {
        return new int[]{((bj >> 1) + i) >> 4, ((bk >> 1) + i2) >> 4};
    }

    public static int i(String str, String str2) {
        int i = 0;
        if (!str.substring(str.length() - 1).equals(str2)) {
            str = new StringBuffer(String.valueOf(str)).append(str2).toString();
        }
        int i2 = 0;
        while (i != -1) {
            i = str.indexOf(str2, i + 1);
            if (i != -1) {
                i2++;
            }
        }
        return i2;
    }

    public static f l(String str) {
        try {
            f fVar = new f();
            DataInputStream z = z(new StringBuffer("/res/").append(str).toString());
            byte readByte = z.readByte();
            z.readByte();
            int readUnsignedShort = z.readUnsignedShort();
            fVar.eK = new short[(readUnsignedShort * 5)];
            for (int i = 0; i < readUnsignedShort; i++) {
                fVar.eK[i * 5] = (short) (z.readUnsignedShort() & z.CONSTRAINT_MASK);
                fVar.eK[(i * 5) + 1] = (short) (z.readUnsignedShort() & z.CONSTRAINT_MASK);
                fVar.eK[(i * 5) + 2] = (short) (z.readUnsignedShort() & z.CONSTRAINT_MASK);
                fVar.eK[(i * 5) + 3] = (short) (z.readUnsignedByte() & 255);
                fVar.eK[(i * 5) + 4] = (short) (z.readUnsignedByte() & 255);
            }
            int readUnsignedShort2 = z.readUnsignedShort();
            fVar.eL = (short[][]) Array.newInstance(short[].class, readUnsignedShort2);
            switch (readByte) {
                case 1:
                    fVar.eQ = (byte[][]) Array.newInstance(byte[].class, readUnsignedShort2);
                    fVar.eO = (byte[][]) Array.newInstance(byte[].class, readUnsignedShort2);
                    break;
                case 2:
                    fVar.eR = (byte[][]) Array.newInstance(byte[].class, readUnsignedShort2);
                    fVar.eP = (byte[][]) Array.newInstance(byte[].class, readUnsignedShort2);
                    break;
                case 3:
                    fVar.eQ = (byte[][]) Array.newInstance(byte[].class, readUnsignedShort2);
                    fVar.eR = (byte[][]) Array.newInstance(byte[].class, readUnsignedShort2);
                    fVar.eO = (byte[][]) Array.newInstance(byte[].class, readUnsignedShort2);
                    fVar.eP = (byte[][]) Array.newInstance(byte[].class, readUnsignedShort2);
                    break;
            }
            for (int i2 = 0; i2 < readUnsignedShort2; i2++) {
                switch (readByte) {
                    case 1:
                        fVar.eQ[i2] = new byte[4];
                        fVar.eO[i2] = new byte[4];
                        z.read(fVar.eQ[i2], 0, 4);
                        if (i2 == readUnsignedShort2 - 1) {
                            for (int i3 = 0; i3 < fVar.eQ.length; i3++) {
                                fVar.eO[i3][0] = (byte) (-fVar.eQ[i3][2]);
                                fVar.eO[i3][2] = (byte) (-fVar.eQ[i3][0]);
                                fVar.eO[i3][1] = fVar.eQ[i3][1];
                                fVar.eO[i3][3] = fVar.eQ[i3][3];
                            }
                            break;
                        }
                        break;
                    case 2:
                        fVar.eR[i2] = new byte[4];
                        fVar.eP[i2] = new byte[4];
                        z.read(fVar.eR[i2], 0, 4);
                        if (i2 == readUnsignedShort2 - 1) {
                            for (int i4 = 0; i4 < fVar.eR.length; i4++) {
                                fVar.eP[i4][0] = (byte) (-fVar.eR[i4][2]);
                                fVar.eP[i4][2] = (byte) (-fVar.eR[i4][0]);
                                fVar.eP[i4][1] = fVar.eR[i4][1];
                                fVar.eP[i4][3] = fVar.eR[i4][3];
                            }
                            break;
                        }
                        break;
                    case 3:
                        fVar.eQ[i2] = new byte[4];
                        fVar.eO[i2] = new byte[4];
                        z.read(fVar.eQ[i2], 0, 4);
                        fVar.eR[i2] = new byte[4];
                        fVar.eP[i2] = new byte[4];
                        z.read(fVar.eR[i2], 0, 4);
                        if (i2 == readUnsignedShort2 - 1) {
                            for (int i5 = 0; i5 < fVar.eQ.length; i5++) {
                                fVar.eO[i5][0] = (byte) (-fVar.eQ[i5][2]);
                                fVar.eO[i5][2] = (byte) (-fVar.eQ[i5][0]);
                                fVar.eO[i5][1] = fVar.eQ[i5][1];
                                fVar.eO[i5][3] = fVar.eQ[i5][3];
                                fVar.eP[i5][0] = (byte) (-fVar.eR[i5][2]);
                                fVar.eP[i5][2] = (byte) (-fVar.eR[i5][0]);
                                fVar.eP[i5][1] = fVar.eR[i5][1];
                                fVar.eP[i5][3] = fVar.eR[i5][3];
                            }
                            break;
                        }
                        break;
                }
                int readUnsignedByte = z.readUnsignedByte();
                fVar.eL[i2] = new short[(readUnsignedByte * 4)];
                for (int i6 = 0; i6 < readUnsignedByte * 4; i6++) {
                    if (i6 % 4 == 0) {
                        fVar.eL[i2][i6] = z.readShort();
                    } else {
                        fVar.eL[i2][i6] = z.readByte();
                    }
                }
            }
            int readUnsignedShort3 = z.readUnsignedShort();
            fVar.eN = new byte[readUnsignedShort3];
            fVar.eM = (short[][]) Array.newInstance(short[].class, readUnsignedShort3);
            for (int i7 = 0; i7 < readUnsignedShort3; i7++) {
                int readUnsignedByte2 = z.readUnsignedByte();
                fVar.eN[i7] = (byte) readUnsignedByte2;
                fVar.eM[i7] = new short[(readUnsignedByte2 * 2)];
                for (int i8 = 0; i8 < readUnsignedByte2 * 2; i8++) {
                    if (i8 % 2 == 0) {
                        fVar.eM[i7][i8] = z.readShort();
                    } else {
                        fVar.eM[i7][i8] = z.readByte();
                    }
                }
            }
            a(z);
            new StringBuffer(String.valueOf(str)).append("   ok").toString();
            return fVar;
        } catch (Exception e) {
            new StringBuffer("no ").append(str).toString();
            a(e, "");
            return null;
        }
    }

    public static p m(String str) {
        try {
            new StringBuffer(String.valueOf(str)).append(" open").toString();
            return p.T(new StringBuffer("/res/").append(str).toString());
        } catch (Exception e) {
            a(e, new StringBuffer("fileName ").append(str).toString());
            return null;
        }
    }

    public static b o(String str) {
        try {
            return (b) ce.get(new StringBuffer("[").append(str).append("]").toString());
        } catch (Exception e) {
            a(e, "");
            return null;
        }
    }

    public static int p(String str) {
        return str.toLowerCase().compareTo("l") == 0 ? 1 : 2;
    }

    public static boolean q(int i) {
        try {
            return f.d("fb", false).eS() > i + -1;
        } catch (Exception e) {
            return false;
        }
    }

    public static void r(String str) {
        for (int i = 0; i < cd.aA; i++) {
            String lowerCase = ((b) cd.k(i)).l(0).toLowerCase();
            str = str.toLowerCase();
            if (lowerCase.equals(new StringBuffer(String.valueOf(str)).append(":").toString())) {
                new StringBuffer("falg=").append(str).append(":").append(i).toString();
                cc = i;
                return;
            }
        }
    }

    public static int t(String str) {
        String lowerCase = str.toLowerCase();
        for (int i = 0; i < cf.aA; i++) {
            if (((String[]) cf.k(i))[0].toLowerCase().equals(lowerCase)) {
                return i;
            }
        }
        return -1;
    }

    public static final boolean t(int i) {
        return i >= 0 && i <= 19;
    }

    public static final boolean u(int i) {
        return i >= 20 && i <= 29;
    }

    public static int v(String str) {
        try {
            return Integer.parseInt(str);
        } catch (Exception e) {
            return 0;
        }
    }

    public static final boolean v(int i) {
        return i >= 30 && i <= 39;
    }

    public static StringBuffer w(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        try {
            DataInputStream z = z(new StringBuffer("/res/").append(str).toString());
            new StringBuffer("/res/").append(str).toString();
            short readShort = z.readShort();
            for (int i = 0; i < readShort; i++) {
                stringBuffer.append((char) z.readShort());
            }
            z.close();
            new StringBuffer().append((Object) stringBuffer).toString();
            return stringBuffer;
        } catch (Exception e) {
            a(e, "");
            return null;
        }
    }

    public static byte[] x(String str) {
        try {
            DataInputStream z = z(new StringBuffer("/res/").append(str).toString());
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            while (true) {
                int read = z.read();
                if (read == -1) {
                    z.close();
                    return byteArrayOutputStream.toByteArray();
                }
                byteArrayOutputStream.write(read);
            }
        } catch (Exception e) {
            a(e, "");
            a(e, new StringBuffer("no").append(str).toString());
            return null;
        }
    }

    private static final String y(String str) {
        return str.toLowerCase().endsWith("wav") ? "audio/x-wav" : str.toLowerCase().endsWith("mp3") ? "audio/mpeg" : "audio/midi";
    }

    public static DataInputStream z(String str) {
        new StringBuffer("openStream ").append(str).toString();
        return new DataInputStream(MIDPHelper.b("QN".getClass(), str));
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:14558, length=5899 in method: c__.A(int):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:14558, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void A(int r1) {
        /*
        // Can't load method instructions: Load method exception: index:14558, length=5899 in method: c__.A(int):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.A(int):void");
    }

    public final void B(int i) {
        this.bV = (byte) i;
        this.aI = true;
        if (this.bV == 1 || this.bV == 3) {
            this.bW = 6;
            if (this.bU > 0 && cb == 4) {
                this.bV = 0;
                this.bW = 0;
                U();
                this.aI = false;
            }
        } else if (this.bV == 2 || this.bV == 4) {
            this.bW = -6;
        }
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:25040, length=5899 in method: c__.C(int):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:25040, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void C(int r1) {
        /*
        // Can't load method instructions: Load method exception: index:25040, length=5899 in method: c__.C(int):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.C(int):void");
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:43987, length=5899 in method: c__.D(int):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:43987, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void D(int r1) {
        /*
        // Can't load method instructions: Load method exception: index:43987, length=5899 in method: c__.D(int):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.D(int):void");
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:51197, length=5899 in method: c__.E(int):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:51197, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void E(int r1) {
        /*
        // Can't load method instructions: Load method exception: index:51197, length=5899 in method: c__.E(int):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.E(int):void");
    }

    public final void H(int i) {
        switch (i) {
            case 1:
            case 50:
                if (cy[this.cz] == 'A') {
                    cy[this.cz] = 'Z';
                    return;
                }
                char[] cArr = cy;
                int i2 = this.cz;
                cArr[i2] = (char) (cArr[i2] - 1);
                return;
            case 2:
            case 56:
                if (cy[this.cz] == 'Z') {
                    cy[this.cz] = 'A';
                    return;
                }
                char[] cArr2 = cy;
                int i3 = this.cz;
                cArr2[i3] = (char) (cArr2[i3] + 1);
                return;
            case 3:
            case com.a.a.e.c.KEY_NUM4 /*52*/:
                if (this.cz > 0) {
                    this.cz--;
                    return;
                }
                return;
            case 4:
            case com.a.a.e.c.KEY_NUM6 /*54*/:
                if (this.cz < cy.length - 1) {
                    this.cz++;
                    return;
                }
                return;
            case 5:
            case 6:
            case 7:
            case com.a.a.e.c.KEY_NUM5 /*53*/:
                if (this.cB) {
                    StringBuffer stringBuffer = new StringBuffer();
                    for (char append : cy) {
                        stringBuffer.append(append);
                    }
                    cx[this.cA] = new String(stringBuffer);
                    cw[this.cA] = this.cC;
                    this.cC = 0;
                    ad();
                    this.cB = false;
                } else {
                    this.cB = false;
                    this.cC = 0;
                }
                n(2);
                ab();
                return;
            default:
                return;
        }
    }

    public final boolean N() {
        return a(this.j, this.aG, 0, 290, 30, 30);
    }

    public final boolean P() {
        return a(this.j, this.aG, 210, 290, 30, 30);
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:14558, length=5899 in method: c__.Q():int, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:14558, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final int Q() {
        /*
        // Can't load method instructions: Load method exception: index:14558, length=5899 in method: c__.Q():int, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.Q():int");
    }

    public final void R() {
        this.bU = 0;
        this.bV = 0;
        this.bW = 0;
        this.aI = false;
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:22200, length=5899 in method: c__.S():void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:22200, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:461)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void S() {
        /*
        // Can't load method instructions: Load method exception: index:22200, length=5899 in method: c__.S():void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.S():void");
    }

    public final void T() {
        C(3);
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:25040, length=5899 in method: c__.U():void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:25040, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void U() {
        /*
        // Can't load method instructions: Load method exception: index:25040, length=5899 in method: c__.U():void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.U():void");
    }

    public final boolean U(int i) {
        return T(i);
    }

    public final byte V(int i) {
        try {
            int length = this.bv[0].length;
            return this.bv[i / length][i % length];
        } catch (Exception e) {
            return -1;
        }
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:20957, length=5899 in method: c__.W():void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:20957, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void W() {
        /*
        // Can't load method instructions: Load method exception: index:20957, length=5899 in method: c__.W():void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.W():void");
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:20957, length=5899 in method: c__.W(int):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:20957, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void W(int r1) {
        /*
        // Can't load method instructions: Load method exception: index:20957, length=5899 in method: c__.W(int):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.W(int):void");
    }

    public final void X() {
        if (cb != 3) {
            return;
        }
        if (cc < cd.aA) {
            a((b) cd.k(cc));
            cc++;
            return;
        }
        cb = 5;
        if (!cd.N()) {
            cd.f();
        }
    }

    public final void Y() {
        if (this.ch) {
            int i = this.cg;
            this.cg = i - 1;
            if (i < 1) {
                U();
                this.ch = false;
            }
        }
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:51197, length=5899 in method: c__.Z():boolean, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:51197, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final boolean Z() {
        /*
        // Can't load method instructions: Load method exception: index:51197, length=5899 in method: c__.Z():boolean, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.Z():boolean");
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final int a(a aVar, int i, int i2, int i3, int i4, int i5, int i6) {
        int i7 = i - aVar.x;
        int i8 = i2 - aVar.z;
        int d = d(aVar, i, i2);
        switch (d) {
            case 1:
                aVar.I = -i3;
                break;
            case 2:
                aVar.I = i3;
                break;
            case 4:
                aVar.H = -i4;
                break;
            case 5:
                aVar.I = -i3;
                aVar.H = -i4;
                break;
            case 6:
                aVar.I = i3;
                aVar.H = -i4;
                break;
            case 8:
                aVar.H = i4;
                break;
            case 9:
                aVar.I = -i3;
                aVar.H = i4;
                break;
            case 10:
                aVar.I = i3;
                aVar.H = i4;
                break;
        }
        if (Math.abs(i7) <= i5) {
            aVar.I = 0;
        }
        if (Math.abs(i8) <= i6) {
            aVar.H = 0;
        }
        return d;
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:14558, length=5899 in method: c__.a(int, int, int, int, int, java.lang.String):b, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:14558, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final defpackage.b a(int r1, int r2, int r3, int r4, int r5, java.lang.String r6) {
        /*
        // Can't load method instructions: Load method exception: index:14558, length=5899 in method: c__.a(int, int, int, int, int, java.lang.String):b, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.a(int, int, int, int, int, java.lang.String):b");
    }

    public final b a(int i, int i2, int i3, int i4, String str) {
        return a(i, i2, i3, i4, 1, str);
    }

    public abstract void a(int i);

    public final void a(int i, int i2, int i3) {
        if (L(i)) {
            this.o = d(this.o, i2, -i3);
        } else if (M(i)) {
            this.o = d(this.o, i2, i3);
        }
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:49732, length=5899 in method: c__.a(int, int, int, int, boolean):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:49732, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:461)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void a(int r1, int r2, int r3, int r4, boolean r5) {
        /*
        // Can't load method instructions: Load method exception: index:49732, length=5899 in method: c__.a(int, int, int, int, boolean):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.a(int, int, int, int, boolean):void");
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:22655, length=5899 in method: c__.a(int, int, boolean):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:22655, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:461)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void a(int r1, int r2, boolean r3) {
        /*
        // Can't load method instructions: Load method exception: index:22655, length=5899 in method: c__.a(int, int, boolean):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.a(int, int, boolean):void");
    }

    public final void a(int i, String str, String str2, String str3, String str4) {
        int v2;
        a aVar;
        int v3;
        a aVar2;
        a w = w(i);
        w.o = str.toLowerCase().compareTo("cur") == 0 ? w.o : v(str);
        if (str2.toLowerCase().compareTo("max") == 0) {
            v2 = w.r;
            aVar = w;
        } else if (str2.toLowerCase().compareTo("cur") == 0) {
            v2 = w.p;
            aVar = w;
        } else {
            v2 = v(str2);
            aVar = w;
        }
        aVar.p = v2;
        new StringBuffer("spt.hp ").append(w.p).toString();
        if (str3.toLowerCase().compareTo("max") == 0) {
            v3 = w.s;
            aVar2 = w;
        } else if (str3.toLowerCase().compareTo("cur") == 0) {
            v3 = w.q;
            aVar2 = w;
        } else {
            v3 = v(str3);
            aVar2 = w;
        }
        aVar2.q = v3;
        if (str4.toLowerCase().compareTo("cur") == 0) {
            str4 = w.m;
        }
        w.m = str4;
    }

    public final void a(int i, boolean z) {
        a(i, i, z);
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:49732, length=5899 in method: c__.a(a):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:49732, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void a(defpackage.a r1) {
        /*
        // Can't load method instructions: Load method exception: index:49732, length=5899 in method: c__.a(a):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.a(a):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c__.a(a, short):void
     arg types: [a, int]
     candidates:
      c__.a(byte[], int):int
      c__.a(int[], char):int
      c__.a(java.lang.String, int[]):c__om.a.a.e.p
      c__.a(a, java.lang.String):void
      c__.a(c__om.a.a.e.o, int):void
      c__.a(java.lang.Exception, java.lang.String):void
      c__.a(byte[], int[]):void
      c__.a(a, a):boolean
      c__.a(int, boolean):void
      c__.a(a, int):void
      c__.a(java.lang.String, int):void
      c__.a(java.lang.String, java.lang.String):void
      c__.a(java.lang.String, boolean):void
      c__.a(int[], int[]):void
      c__.a(a, short):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c__.a(a, int, int, boolean):void
     arg types: [a, int, int, int]
     candidates:
      c__.a(java.lang.String, c__om.a.a.e.l, int, char):b
      c__.a(int[], int[], int, int):void
      c__.a(c__om.a.a.e.o, int, c__om.a.a.e.p, int):void
      c__.a(c__om.a.a.e.o, a, int, int):void
      c__.a(java.lang.String, java.lang.String, int, int):void
      c__.a(int, int, int, int):boolean
      c__.a(a, int, int, boolean):void */
    public final void a(a aVar, int i) {
        switch (i) {
            case 0:
            case 258:
            default:
                return;
            case 256:
                a(aVar, -8, 0, -e(0, 5, 8), 3);
                aVar.z += e(0, 2, 8) + 32;
                a(aVar, (short) 258);
                return;
            case 259:
                aVar.I = 0;
                aVar.K = 0;
                aVar.J = 0;
                aVar.H = 0;
                a(aVar, 1, aVar.ay, true);
                return;
        }
    }

    public final void a(a aVar, int i, int i2, boolean z) {
        a(aVar, i, -1, i2, z);
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:20957, length=5899 in method: c__.a(a, a, int):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:20957, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void a(defpackage.a r1, defpackage.a r2, int r3) {
        /*
        // Can't load method instructions: Load method exception: index:20957, length=5899 in method: c__.a(a, a, int):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.a(a, a, int):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c__.a(a, a, int):void
     arg types: [a, a, short]
     candidates:
      c__.a(f, int, int):int
      c__.a(int[], int, int):void
      c__.a(int, int, int):void
      c__.a(int, int, boolean):void
      c__.a(a, a, short):void
      c__.a(a, boolean, int):void
      c__.a(c__om.a.a.e.o, int, int):void
      c__.a(java.lang.String, java.lang.String, java.lang.String):void
      c__.a(a, a, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c__.b(a, a, int):void
     arg types: [a, a, short]
     candidates:
      c__.b(int, int, int):int[]
      c__.b(int, int, boolean):void
      c__.b(a, int, int):void
      c__.b(a, a, short):void
      c__.b(a, a, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c__.b(a, int):void
     arg types: [a, short]
     candidates:
      c__.b(int, int):int
      c__.b(int, boolean):void
      c__.b(a, short):void
      c__.b(java.lang.String, int):void
      c__.b(java.lang.String, java.lang.String):void
      c__.b(a, a):boolean
      c__.b(a, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c__.a(a, int):void
     arg types: [a, short]
     candidates:
      c__.a(byte[], int):int
      c__.a(int[], char):int
      c__.a(java.lang.String, int[]):c__om.a.a.e.p
      c__.a(a, java.lang.String):void
      c__.a(c__om.a.a.e.o, int):void
      c__.a(java.lang.Exception, java.lang.String):void
      c__.a(byte[], int[]):void
      c__.a(a, a):boolean
      c__.a(int, boolean):void
      c__.a(a, short):void
      c__.a(java.lang.String, int):void
      c__.a(java.lang.String, java.lang.String):void
      c__.a(java.lang.String, boolean):void
      c__.a(int[], int[]):void
      c__.a(a, int):void */
    public final void a(a aVar, a aVar2, short s) {
        if (aVar.aw != s) {
            aVar.ax = aVar.aw;
            aVar.aw = s;
            aVar.u = 0;
            switch (aVar.ao) {
                case 0:
                    b(aVar, aVar2, s);
                    return;
                case 1:
                    c(aVar, aVar2, s);
                    return;
                case 2:
                case 3:
                case 14:
                case 15:
                case 16:
                case 17:
                case 18:
                case 19:
                case 23:
                case com.a.a.b.c.UUID:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case CharsToNameCanonicalizer.HASH_MULT /*33*/:
                case 34:
                case com.a.a.e.c.KEY_POUND /*35*/:
                case 36:
                case 37:
                case 38:
                case 39:
                default:
                    return;
                case 4:
                case 11:
                case 12:
                    e(aVar, aVar2, s);
                    return;
                case 5:
                    e(aVar, aVar2, s);
                    return;
                case 6:
                    e(aVar, aVar2, s);
                    return;
                case 7:
                    d(aVar, aVar2, s);
                    return;
                case 8:
                case 13:
                    a(aVar, aVar2, (int) s);
                    return;
                case 9:
                    c(aVar, aVar2, s);
                    return;
                case 10:
                    b(aVar, aVar2, (int) s);
                    return;
                case com.a.a.b.c.INT_16:
                    b(aVar, (int) s);
                    return;
                case 21:
                    b(aVar, s);
                    return;
                case 22:
                    a(aVar, (int) s);
                    return;
                case 30:
                case 31:
                case 32:
                    d(aVar, s);
                    return;
                case com.a.a.b.c.BOOL:
                    c(aVar, s);
                    return;
            }
        }
    }

    public final void a(a aVar, short s) {
        a(aVar, (a) null, s);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c__.a(a, int, int, boolean):void
     arg types: [a, int, int, int]
     candidates:
      c__.a(java.lang.String, c__om.a.a.e.l, int, char):b
      c__.a(int[], int[], int, int):void
      c__.a(c__om.a.a.e.o, int, c__om.a.a.e.p, int):void
      c__.a(c__om.a.a.e.o, a, int, int):void
      c__.a(java.lang.String, java.lang.String, int, int):void
      c__.a(int, int, int, int):boolean
      c__.a(a, int, int, boolean):void */
    public final void a(a aVar, boolean z, int i) {
        aVar.ac.av = false;
        if (z) {
            aVar.ac.y = aVar.y - aVar.F;
        } else {
            aVar.ac.y = aVar.y - (aVar.F >> 1);
        }
        a(aVar.ac, i, aVar.ay, true);
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:43026, length=5899 in method: c__.a(b):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:43026, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:461)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void a(defpackage.b r1) {
        /*
        // Can't load method instructions: Load method exception: index:43026, length=5899 in method: c__.a(b):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.a(b):void");
    }

    public abstract void a(o oVar);

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:38839, length=5899 in method: c__.a(c__om.a.a.e.o, int, int):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:38839, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void a(c__om.a.a.e.o r1, int r2, int r3) {
        /*
        // Can't load method instructions: Load method exception: index:38839, length=5899 in method: c__.a(c__om.a.a.e.o, int, int):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.a(c__om.a.a.e.o, int, int):void");
    }

    public final void a(o oVar, int i, p pVar, int i2) {
        int i3;
        int height = pVar.getHeight();
        int height2 = pVar.getHeight();
        int i4 = (240 - height) - 1;
        int i5 = (i2 - height2) - 2;
        switch (i) {
            case 0:
            case 1:
                i3 = 131071;
                break;
            case 2:
                i3 = -65534;
                break;
            case 3:
                i3 = 65538;
                break;
            case 4:
                i3 = 262143;
                break;
            case 5:
            case 6:
            case 7:
            case 10:
            case 11:
            default:
                i3 = -1;
                break;
            case 8:
                i3 = -65532;
                break;
            case 9:
                i3 = 65540;
                break;
            case 12:
                i3 = 196612;
                break;
        }
        a(i3 >> 16, pVar, 1, i5, 0, oVar, height, height2);
        a((short) (65535 & i3), pVar, i4, i5, 0, oVar, height, height2);
    }

    public final void a(o oVar, a aVar, int i, int i2) {
        if (aVar.ao == 0) {
            a(oVar, P(19), i, i2, 0, 0);
            a(P(20), new StringBuffer(String.valueOf(aVar.n)).toString(), i + 66, i2 + 38, 0, oVar, P(20).getHeight(), P(20).getHeight(), 0, 0);
            oVar.setColor(16711680);
            oVar.i(i + a.PHOTO, i2 + 36, (aVar.p * a.TEL) / aVar.r, 9);
        } else if (t(aVar.ao)) {
            oVar.setColor(16777215);
            oVar.i(i - 1, i2 - 1, 46, 7);
            int i3 = (aVar.p / 44) + 1;
            int i4 = (255 - (i3 * 20)) % 255;
            int i5 = (i3 * 20) % 255;
            int i6 = ((aVar.p << 10) * 44) / aVar.r;
            oVar.setColor(0);
            oVar.i(i, i2, 44, 5);
            if (i3 > 1) {
                oVar.j(0, Math.abs(i5), Math.abs(i4));
                oVar.i(i, i2, i6 >> 10, 5);
                return;
            }
            oVar.setColor(16711680);
            oVar.i(i, i2, i6 >> 10, 5);
        }
    }

    public final void a(o oVar, a aVar, int i, int i2, boolean z, boolean z2, boolean z3) {
        if (!aVar.av) {
            a(aVar.am, oVar, i, i2, aVar.ar != -1 ? aVar.ar : aVar.an[0], z2, z3);
            if (!z && aVar.ar == -1 && !aVar.at) {
                aVar.as = b(aVar);
            }
        }
    }

    public final void a(o oVar, p pVar, int i, int i2, int i3) {
        int height = pVar.getHeight();
        int height2 = pVar.getHeight();
        int[] b = b(i3, height, height2);
        int i4 = (i2 - (height2 >> 3)) + b[1];
        a(0, pVar, b[0] + i, i4, 0, oVar, height, height2);
    }

    public final void a(o oVar, p pVar, p pVar2, p pVar3, int i) {
        oVar.a(aH);
        oVar.setColor(i);
        if (pVar != null) {
            a(oVar, pVar, 120, pVar.getHeight(), 0, 3);
        } else {
            a(oVar, cG[1], 120, 4, 17);
        }
        int b = 60 - (aH.b('A') * cy.length);
        int length = 168 / cx.length;
        int b2 = 180 - ((aH.b('0') * 6) / 2);
        byte b3 = 0;
        while (true) {
            byte b4 = b3;
            if (b4 < cx.length) {
                if (b4 != this.cA) {
                    byte b5 = 0;
                    while (true) {
                        byte b6 = b5;
                        if (b6 >= cx[b4].length()) {
                            break;
                        }
                        if (pVar2 != null) {
                            int width = pVar2.getWidth() / 26;
                            int height = pVar2.getHeight();
                            a(pVar2, new StringBuffer().append(cx[b4].charAt(b6)).toString(), b + (height * b6), (length * b4) + 40, 0, oVar, width, height, 2, -1);
                        } else {
                            oVar.a(cx[b4].charAt(b6), (aH.b('A') * b6 * 2) + b, (length * b4) + 40, 20);
                        }
                        b5 = (byte) (b6 + 1);
                    }
                } else {
                    byte b7 = 0;
                    while (true) {
                        byte b8 = b7;
                        if (b8 >= cx[b4].length()) {
                            break;
                        }
                        if (pVar2 != null) {
                            int width2 = pVar2.getWidth() / 26;
                            int height2 = pVar2.getHeight();
                            int i2 = b + (b8 * width2);
                            int i3 = (length * b4) + 40;
                            if (this.cz == b8) {
                                byte b9 = this.aO;
                                this.aO = (byte) (b9 + 1);
                                if (b9 % 3 == 0) {
                                }
                            }
                            a(pVar2, new StringBuffer().append(cy[b8]).toString(), i2, i3, 0, oVar, width2, height2, 1, -1);
                        } else {
                            if (this.cz == b8) {
                                byte b10 = this.aO;
                                this.aO = (byte) (b10 + 1);
                                if (b10 % 3 == 0) {
                                }
                            }
                            oVar.a(cy[b8], (aH.b('A') * b8 * 2) + b, (length * b4) + 40, 20);
                        }
                        b7 = (byte) (b8 + 1);
                    }
                }
                if (pVar3 != null) {
                    int width3 = pVar3.getWidth() / 10;
                    int height3 = pVar3.getHeight();
                    a(pVar3, new StringBuffer().append(cw[b4]).toString(), b2, (length * b4) + 40, 0, oVar, width3, height3, 1, -1);
                } else {
                    a(oVar, new StringBuffer().append(cw[b4]).toString(), b2, (length * b4) + 40, 20);
                }
                b3 = (byte) (b4 + 1);
            } else {
                return;
            }
        }
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:51197, length=5899 in method: c__.a(java.lang.String, int):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:51197, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void a(java.lang.String r1, int r2) {
        /*
        // Can't load method instructions: Load method exception: index:51197, length=5899 in method: c__.a(java.lang.String, int):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.a(java.lang.String, int):void");
    }

    public final void a(String str, String str2) {
        a(str, str2, this.bO - 10, 3);
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:38839, length=5899 in method: c__.a(java.lang.String, java.lang.String, int, int):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:38839, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:461)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void a(java.lang.String r1, java.lang.String r2, int r3, int r4) {
        /*
        // Can't load method instructions: Load method exception: index:38839, length=5899 in method: c__.a(java.lang.String, java.lang.String, int, int):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.a(java.lang.String, java.lang.String, int, int):void");
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:51197, length=5899 in method: c__.a(java.lang.String, java.lang.String, java.lang.String):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:51197, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void a(java.lang.String r1, java.lang.String r2, java.lang.String r3) {
        /*
        // Can't load method instructions: Load method exception: index:51197, length=5899 in method: c__.a(java.lang.String, java.lang.String, java.lang.String):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.a(java.lang.String, java.lang.String, java.lang.String):void");
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:6324, length=5899 in method: c__.a(java.lang.String, boolean):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:6324, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:461)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void a(java.lang.String r1, boolean r2) {
        /*
        // Can't load method instructions: Load method exception: index:6324, length=5899 in method: c__.a(java.lang.String, boolean):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.a(java.lang.String, boolean):void");
    }

    public final void a(int[] iArr, int[] iArr2) {
        a(iArr, iArr2, 0, iArr.length - 1);
    }

    public final boolean a(int i, int i2, int i3, int i4) {
        return a(this.j, this.aG, i, i2, i3, i4);
    }

    public final boolean a(a aVar, int i, int i2, int i3, boolean z) {
        f a = a(aVar.am);
        if ((i != aVar.an[1]) || z) {
            aVar.an[1] = i;
            aVar.an[2] = 0;
            aVar.an[3] = 0;
            aVar.an[0] = a(a, aVar.an[1], aVar.an[2]);
            aVar.as = false;
            aVar.ar = -1;
            aVar.aq = aVar.an[1];
            aVar.d(i3);
            aVar.aq = (byte) i2;
        }
        return aVar.as;
    }

    public final void aa() {
        ct = new Player[cr.length];
        for (int i = 0; i < ct.length; i++) {
            F(i);
        }
    }

    public final void ab() {
        if (cu != null) {
            if (cs == 0) {
                G(cv);
            } else if (cu != null && cv == 0) {
                c(cv, -1);
            }
        }
    }

    public final void ac() {
        this.cA = -1;
        ae();
        int i = 0;
        while (true) {
            if (i >= cw.length) {
                break;
            } else if (this.cC > cw[i]) {
                this.cB = true;
                this.cA = i;
                break;
            } else {
                i++;
            }
        }
        if (this.cB) {
            this.cz = 0;
            int length = cw.length;
            while (true) {
                length--;
                if (length <= this.cA) {
                    break;
                }
                int i2 = cw[length];
                cw[length] = cw[length - 1];
                cw[length - 1] = i2;
                String str = cx[length];
                cx[length] = cx[length - 1];
                cx[length - 1] = str;
            }
            cw[this.cA] = this.cC;
        }
        n(6);
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:20957, length=5899 in method: c__.ag():void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:20957, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void ag() {
        /*
        // Can't load method instructions: Load method exception: index:20957, length=5899 in method: c__.ag():void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.ag():void");
    }

    public final void b(int i) {
        if (i != 0 && !O()) {
            int[] iArr = aE;
            byte b = aB;
            aB = (byte) (b + 1);
            iArr[b] = i;
            byte b2 = 0;
            for (byte b3 = 0; b3 < aE.length; b3 = (byte) (b3 + 1)) {
                if (aE[b3] == aD[b3]) {
                    b2 = (byte) (b2 + 1);
                }
            }
            if (b2 > aE.length - 1 && aC != 2) {
                aC = 2;
                this.aS = true;
                this.F = 50;
            } else if (b2 <= aE.length - 3 || aC == 1) {
                aC = 0;
            } else {
                aC = 1;
                this.aS = true;
                this.F = 50;
            }
            if (aB + 1 > aE.length || i == 48) {
                aB = 0;
                aE = new int[aD.length];
            }
        }
    }

    public final void b(int i, int i2, int i3, int i4) {
        bt.setColor(0);
        while (i2 <= i4 && i2 < this.bu.length) {
            int i5 = i;
            while (i5 <= i3 && i5 < this.bu[0].length) {
                short s = this.bu[i2][i5];
                int i6 = (i5 % 16) << 4;
                int i7 = (i2 % 11) << 4;
                short s2 = s & 255;
                int i8 = (s2 % bi) << 4;
                int i9 = (s2 / bi) << 4;
                bt.i(i6, i7, bj, bk);
                a(bt, this.bw, i8, i9, bj, bk, (s >> 12) & 255, i6, i7, 0);
                i5++;
            }
            i2++;
        }
    }

    public final void b(int i, int i2, boolean z) {
        if (i2 >= -1) {
            if (this.bS != i || z) {
                this.bS = (byte) i;
                this.bT = (byte) i2;
                this.bR = 0;
            }
        }
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:49732, length=5899 in method: c__.b(int, boolean):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:49732, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:461)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void b(int r1, boolean r2) {
        /*
        // Can't load method instructions: Load method exception: index:49732, length=5899 in method: c__.b(int, boolean):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.b(int, boolean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c__.a(a, int, int, boolean):void
     arg types: [a, int, int, int]
     candidates:
      c__.a(java.lang.String, c__om.a.a.e.l, int, char):b
      c__.a(int[], int[], int, int):void
      c__.a(c__om.a.a.e.o, int, c__om.a.a.e.p, int):void
      c__.a(c__om.a.a.e.o, a, int, int):void
      c__.a(java.lang.String, java.lang.String, int, int):void
      c__.a(int, int, int, int):boolean
      c__.a(a, int, int, boolean):void */
    public final void b(a aVar, int i) {
        switch (i) {
            case 0:
            default:
                return;
            case 768:
                a(aVar, 1, aVar.ay, true);
                c(4, 1);
                return;
            case 3840:
                a(aVar, 2, aVar.ay, true);
                return;
        }
    }

    public final void b(a aVar, int i, int i2) {
        if (i != 999) {
            aVar.x = e(i, i2)[0];
        }
        if (i2 != 999) {
            aVar.y = e(i, i2)[1];
        }
        aVar.z = aVar.y;
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:20957, length=5899 in method: c__.b(a, a, int):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:20957, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void b(defpackage.a r1, defpackage.a r2, int r3) {
        /*
        // Can't load method instructions: Load method exception: index:20957, length=5899 in method: c__.b(a, a, int):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.b(a, a, int):void");
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:20957, length=5899 in method: c__.b(a, a, short):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:20957, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void b(defpackage.a r1, defpackage.a r2, short r3) {
        /*
        // Can't load method instructions: Load method exception: index:20957, length=5899 in method: c__.b(a, a, short):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.b(a, a, short):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c__.a(a, int, int, boolean):void
     arg types: [a, int, int, int]
     candidates:
      c__.a(java.lang.String, c__om.a.a.e.l, int, char):b
      c__.a(int[], int[], int, int):void
      c__.a(c__om.a.a.e.o, int, c__om.a.a.e.p, int):void
      c__.a(c__om.a.a.e.o, a, int, int):void
      c__.a(java.lang.String, java.lang.String, int, int):void
      c__.a(int, int, int, int):boolean
      c__.a(a, int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c__.a(a, boolean, int):void
     arg types: [a, int, int]
     candidates:
      c__.a(f, int, int):int
      c__.a(int[], int, int):void
      c__.a(int, int, int):void
      c__.a(int, int, boolean):void
      c__.a(a, a, int):void
      c__.a(a, a, short):void
      c__.a(c__om.a.a.e.o, int, int):void
      c__.a(java.lang.String, java.lang.String, java.lang.String):void
      c__.a(a, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c__.b(int, int, boolean):void
     arg types: [int, int, int]
     candidates:
      c__.b(int, int, int):int[]
      c__.b(a, int, int):void
      c__.b(a, a, int):void
      c__.b(a, a, short):void
      c__.b(int, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c__.a(a, short):void
     arg types: [a, int]
     candidates:
      c__.a(byte[], int):int
      c__.a(int[], char):int
      c__.a(java.lang.String, int[]):c__om.a.a.e.p
      c__.a(a, java.lang.String):void
      c__.a(c__om.a.a.e.o, int):void
      c__.a(java.lang.Exception, java.lang.String):void
      c__.a(byte[], int[]):void
      c__.a(a, a):boolean
      c__.a(int, boolean):void
      c__.a(a, int):void
      c__.a(java.lang.String, int):void
      c__.a(java.lang.String, java.lang.String):void
      c__.a(java.lang.String, boolean):void
      c__.a(int[], int[]):void
      c__.a(a, short):void */
    public final void b(a aVar, short s) {
        switch (s) {
            case 0:
            default:
                return;
            case 533:
                a(aVar, -15, 0, -16, 3);
                a(aVar, 4, aVar.ay, true);
                c(2, 1);
                b(1, 1, true);
                return;
            case 768:
                if (aVar.c() == 0) {
                    a(aVar, 1, aVar.ay, true);
                } else if (aVar.c() == 1) {
                    a(aVar, 2, aVar.ay, true);
                } else if (aVar.c() == 2) {
                    a(aVar, (short) 3840);
                }
                a(aVar, true, 0);
                c(2, 1);
                b(1, 1, true);
                return;
            case 3840:
                a(aVar, 3, aVar.ay, true);
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c__.a(c__om.a.a.e.o, int, int, int, int, int, boolean, int):void
     arg types: [c__om.a.a.e.o, int, int, int, int, int, int, int]
     candidates:
      c__.a(int, c__om.a.a.e.p, int, int, int, c__om.a.a.e.o, int, int):int
      c__.a(int, int, int, int, int, int, int, int):boolean
      c__.a(c__om.a.a.e.o, int, int, int, int, int, boolean, int):void */
    public final void b(o oVar) {
        if (this.aY) {
            if (this.K > 120) {
                this.K = 120;
            }
            this.K++;
        }
        if (!this.aZ) {
            a(oVar, 0);
            a(oVar, this.bb, 120, 196 - this.bb.getHeight(), 0, 17);
            a(oVar, 59, 195, 121, 11, 30888, false, 0);
            a(oVar, 60, 196, (this.K * 120) / 120, 10, 1930135, true, 0);
        }
    }

    public final void b(String str, int i) {
        ((Object[]) cf.k(t(str)))[1] = String.valueOf(i);
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:20957, length=5899 in method: c__.b(java.lang.String, java.lang.String):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:20957, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void b(java.lang.String r1, java.lang.String r2) {
        /*
        // Can't load method instructions: Load method exception: index:20957, length=5899 in method: c__.b(java.lang.String, java.lang.String):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.b(java.lang.String, java.lang.String):void");
    }

    public final boolean b(a aVar) {
        boolean z;
        f a = a(aVar.am);
        int[] iArr = aVar.an;
        iArr[3] = iArr[3] + 1;
        if (aVar.an[3] < a.eM[aVar.an[1]][(aVar.an[2] << 1) + 1]) {
            return false;
        }
        int[] iArr2 = aVar.an;
        iArr2[2] = iArr2[2] + 1;
        if (aVar.an[2] > a.eN[aVar.an[1]] - 1) {
            aVar.an[2] = aVar.aq != -1 ? aVar.aq : 0;
            z = true;
        } else {
            z = false;
        }
        aVar.an[0] = a(a, aVar.an[1], aVar.an[2]);
        aVar.an[3] = 0;
        return z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c__.a(a, a, short):void
     arg types: [a, a, int]
     candidates:
      c__.a(f, int, int):int
      c__.a(int[], int, int):void
      c__.a(int, int, int):void
      c__.a(int, int, boolean):void
      c__.a(a, a, int):void
      c__.a(a, boolean, int):void
      c__.a(c__om.a.a.e.o, int, int):void
      c__.a(java.lang.String, java.lang.String, java.lang.String):void
      c__.a(a, a, short):void */
    public final boolean b(a aVar, a aVar2) {
        int i = 20;
        if (aVar.p > 0) {
            if (aVar2.ao == 0) {
                if (aVar2.aw != 531) {
                    if (aVar2.aw == 1542) {
                        i = 15;
                    } else if (!(aVar2.c() == 3 || aVar2.c() == 9)) {
                        i = aVar2.ah[aVar2.C];
                    }
                }
                aVar.p -= i;
                this.cC = i + this.cC;
            } else {
                aVar.p -= aVar2.ah[aVar2.C];
                if (this.cC > 0) {
                    this.cC -= aVar2.ah[aVar2.C];
                } else {
                    this.cC = 0;
                }
            }
        }
        if (aVar.p > 0 || aVar.aw == 1538 || a(aVar.aw) == 15 || aVar.aw == 770) {
            return false;
        }
        a(aVar, aVar2, (short) 770);
        aVar.p = 0;
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002a  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0036 A[LOOP:1: B:13:0x0028->B:19:0x0036, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0034 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int c(int r6, int r7, int r8, int r9) {
        /*
            r5 = this;
            r2 = 16
            r1 = 1
            int r0 = r6 + r7
            int r0 = java.lang.Math.abs(r0)
            if (r0 >= r2) goto L_0x0018
            int r0 = r8 + r9
            int r0 = java.lang.Math.abs(r0)
            if (r0 >= r2) goto L_0x0018
            int r0 = r5.h(r7, r9)
        L_0x0017:
            return r0
        L_0x0018:
            int r3 = r5.h(r6, r8)
            if (r6 > r7) goto L_0x0020
            if (r8 <= r9) goto L_0x0040
        L_0x0020:
            r0 = -1
        L_0x0021:
            if (r0 != r1) goto L_0x003d
            if (r8 <= r9) goto L_0x0027
        L_0x0025:
            r0 = r3
            goto L_0x0017
        L_0x0027:
            r4 = r6
        L_0x0028:
            if (r0 != r1) goto L_0x003a
            if (r4 <= r7) goto L_0x002e
        L_0x002c:
            int r8 = r8 + r0
            goto L_0x0021
        L_0x002e:
            int r2 = r5.h(r4, r8)
            if (r3 == r2) goto L_0x0036
            r0 = r2
            goto L_0x0017
        L_0x0036:
            int r2 = r4 + r0
            r4 = r2
            goto L_0x0028
        L_0x003a:
            if (r4 >= r7) goto L_0x002e
            goto L_0x002c
        L_0x003d:
            if (r8 >= r9) goto L_0x0027
            goto L_0x0025
        L_0x0040:
            r0 = r1
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.c(int, int, int, int):int");
    }

    public final int c(a aVar) {
        return g(aVar.x + aVar.I, aVar.z + aVar.H);
    }

    public final int c(String str) {
        if (str.equals("-1")) {
            return -1;
        }
        if (str.equals("-2")) {
            return -2;
        }
        if (str.equals("玩家")) {
            return 0;
        }
        for (int i = 0; i < Q(); i++) {
            if (str.equals(w(i).R)) {
                return i;
            }
        }
        return -1;
    }

    public final a c(a aVar, int i, int i2) {
        aVar.x += i;
        aVar.z += i2;
        int i3 = 0;
        while (i3 < Q()) {
            a w = w(i3);
            if (w == aVar || !w.a(aVar, w.E >> 1, w.G >> 1)) {
                i3++;
            } else {
                aVar.x -= i;
                aVar.z -= i2;
                return w;
            }
        }
        aVar.x -= i;
        aVar.z -= i2;
        return null;
    }

    public final void c(int i, int i2, int i3) {
        b(w(i), i2, i3);
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:14558, length=5899 in method: c__.c(int, boolean):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:14558, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void c(int r1, boolean r2) {
        /*
        // Can't load method instructions: Load method exception: index:14558, length=5899 in method: c__.c(int, boolean):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.c(int, boolean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c__.a(a, short):void
     arg types: [a, int]
     candidates:
      c__.a(byte[], int):int
      c__.a(int[], char):int
      c__.a(java.lang.String, int[]):c__om.a.a.e.p
      c__.a(a, java.lang.String):void
      c__.a(c__om.a.a.e.o, int):void
      c__.a(java.lang.Exception, java.lang.String):void
      c__.a(byte[], int[]):void
      c__.a(a, a):boolean
      c__.a(int, boolean):void
      c__.a(a, int):void
      c__.a(java.lang.String, int):void
      c__.a(java.lang.String, java.lang.String):void
      c__.a(java.lang.String, boolean):void
      c__.a(int[], int[]):void
      c__.a(a, short):void */
    public final void c(a aVar, int i) {
        a(i, 0, 0, 2, (String) null);
        a w = w(Q() - 1);
        w.a(aVar);
        if (a.h(w.x) <= w.E) {
            w.a(w.E);
        } else if (a.h(w.x) >= 240) {
            w.a(240 - w.E);
        }
        a(w, (short) 257);
    }

    public final void c(a aVar, a aVar2) {
        if (b(aVar.x, aVar2.x) < 30) {
            aVar2.J = 0;
            if ((aVar.ay & 2) != 0) {
                aVar2.I = 4;
            } else if ((aVar.ay & 1) != 0) {
                aVar2.I = -4;
            }
            if (U(c(aVar2.x, aVar2.x + aVar2.I, aVar2.z, aVar2.z + aVar2.H))) {
                aVar2.H = 0;
                aVar2.I = 0;
                return;
            }
            int d = d(aVar2);
            short s = (short) (d >> 16);
            short s2 = (short) (d & z.CONSTRAINT_MASK);
            if (s > -1) {
                int length = (s2 % this.bv[0].length) << 4;
                int length2 = (s2 / this.bv[0].length) << 4;
                int i = s2 == -1 ? aVar2.x : this.bx + length + 1;
                int i2 = s2 == -1 ? aVar2.x : length - 2;
                int i3 = s2 == -1 ? aVar2.y : this.by + length2 + 1;
                int i4 = s2 == -1 ? aVar2.y : length2 - 2;
                int length3 = this.bv[0].length * this.bx;
                switch (s) {
                    case 1:
                        aVar2.x = aVar2.E >> 1;
                        aVar2.I = 0;
                        break;
                    case 2:
                        aVar2.I = 0;
                        aVar2.x = (length3 - 2) - (aVar2.E >> 1);
                        break;
                    case 4:
                        aVar2.a(aVar2.E >> 1);
                        aVar2.I = 0;
                        break;
                    case 8:
                        aVar2.I = 0;
                        aVar2.a(240 - (aVar2.E >> 1));
                        break;
                    case 16:
                        aVar2.I = 0;
                        aVar2.H = 0;
                        aVar2.x = i;
                        break;
                    case 32:
                        aVar2.I = 0;
                        aVar2.H = 0;
                        aVar2.x = i2;
                        break;
                    case 64:
                        aVar2.I = 0;
                        aVar2.H = 0;
                        aVar2.z = i3;
                        break;
                    case 65:
                        aVar2.x = aVar2.E >> 1;
                        aVar2.I = 0;
                        aVar2.H = 0;
                        break;
                    case com.a.a.p.c.TYPE /*66*/:
                        aVar2.I = 0;
                        aVar2.H = 0;
                        aVar2.x = (length3 - 2) - (aVar2.E >> 1);
                        break;
                    case 80:
                        aVar2.I = 0;
                        aVar2.H = 0;
                        aVar2.x = i;
                        aVar2.z = i3;
                        break;
                    case 96:
                        aVar2.I = 0;
                        aVar2.H = 0;
                        aVar2.x = i2;
                        aVar2.z = i3;
                        break;
                    case 128:
                        aVar2.I = 0;
                        aVar2.H = 0;
                        aVar2.z = i4;
                        break;
                    case 129:
                        aVar2.x = aVar2.E >> 1;
                        aVar2.I = 0;
                        aVar2.H = 0;
                        break;
                    case 130:
                        aVar2.I = 0;
                        aVar2.H = 0;
                        aVar2.x = (length3 - 2) - (aVar2.E >> 1);
                        break;
                    case MIDIControl.NOTE_ON /*144*/:
                        aVar2.I = 0;
                        aVar2.H = 0;
                        aVar2.x = i;
                        aVar2.z = i4;
                        break;
                    case com.a.a.p.f.OBEX_HTTP_OK /*160*/:
                        aVar2.I = 0;
                        aVar2.H = 0;
                        aVar2.x = i2;
                        aVar2.z = i4;
                        break;
                }
            }
            a(aVar2);
        }
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:20957, length=5899 in method: c__.c(a, a, int):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:20957, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void c(defpackage.a r1, defpackage.a r2, int r3) {
        /*
        // Can't load method instructions: Load method exception: index:20957, length=5899 in method: c__.c(a, a, int):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.c(a, a, int):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c__.a(a, int, int, boolean):void
     arg types: [a, int, int, int]
     candidates:
      c__.a(java.lang.String, c__om.a.a.e.l, int, char):b
      c__.a(int[], int[], int, int):void
      c__.a(c__om.a.a.e.o, int, c__om.a.a.e.p, int):void
      c__.a(c__om.a.a.e.o, a, int, int):void
      c__.a(java.lang.String, java.lang.String, int, int):void
      c__.a(int, int, int, int):boolean
      c__.a(a, int, int, boolean):void */
    public final void c(a aVar, short s) {
        switch (s) {
            case 0:
                aVar.I = 0;
                aVar.K = 0;
                aVar.J = 0;
                aVar.H = 0;
                if ((aVar.ay & 2) != 0) {
                    aVar.d(2);
                } else {
                    aVar.d(1);
                }
                a(aVar, 0, aVar.ay, true);
                return;
            case 1:
            default:
                return;
            case 2:
                aVar.I = -8;
                if (aVar.ay == 2) {
                    aVar.I = Math.abs(aVar.I);
                }
                a(aVar, 0, aVar.ay, true);
                return;
        }
    }

    public void c(o oVar) {
        if (!this.aY) {
            this.aO = (byte) (this.aO + 1);
            m();
        }
        d(oVar);
        a(oVar);
        if (aB < 40 && O() && aC > 0) {
            short[] sArr = {72, 105, 81, 78};
            oVar.setColor(65280);
            for (int i = 0; i < sArr.length; i++) {
                oVar.a((char) sArr[i], ((ak * i) + 240) >> 1, 104, 17);
            }
            aB = (byte) (aB + 1);
        }
        this.q = 0;
        this.aG = 0;
        this.j = 0;
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:6494, length=5899 in method: c__.c(java.lang.String, int):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:6494, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void c(java.lang.String r1, int r2) {
        /*
        // Can't load method instructions: Load method exception: index:6494, length=5899 in method: c__.c(java.lang.String, int):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.c(java.lang.String, int):void");
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:43987, length=5899 in method: c__.c(java.lang.String, java.lang.String):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:43987, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:461)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void c(java.lang.String r1, java.lang.String r2) {
        /*
        // Can't load method instructions: Load method exception: index:43987, length=5899 in method: c__.c(java.lang.String, java.lang.String):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.c(java.lang.String, java.lang.String):void");
    }

    public final int d(a aVar) {
        int i = aVar.I;
        int i2 = aVar.H;
        int i3 = aVar.x;
        int i4 = aVar.z;
        int i5 = i3 + i;
        int i6 = i4 + i2;
        if (i == 0 || i2 == 0) {
            if (i != 0) {
                int h = h(i3, i4);
                boolean z = i > 0;
                int c = c(i3, i5, i4, i6);
                int c2 = c(aVar);
                if (i5 <= (aVar.E >> 1) && !T(c) && i < 0) {
                    return 65536 | (65535 & c2);
                }
                if (i5 >= (this.bv[0].length * this.bx) - (aVar.E >> 1) && !T(c) && i > 0) {
                    return 131072 | (65535 & c2);
                }
                if (T(c) && h < c && c > -1) {
                    return z ? 2097152 | (65535 & c2) : 1048576 | (65535 & c2);
                }
                if (a.h(i5) <= (aVar.E >> 1) && !T(c)) {
                    return 262144 | (65535 & c2);
                }
                if (a.h(i5) >= 240 - (aVar.E >> 1) && !T(c)) {
                    return 524288 | (65535 & c2);
                }
            } else if (i2 != 0) {
                boolean z2 = i2 > 0;
                int h2 = h(i5, i6);
                int c3 = c(aVar);
                if (h2 == -1 || T(h2)) {
                    if (!z2) {
                        return 4194304 | (65535 & c3);
                    }
                    aVar.H = 0;
                    return 8388608 | (65535 & c3);
                }
            }
            return -1;
        }
        boolean z3 = i > 0;
        boolean z4 = i2 > 0;
        int c4 = c(i3, i5, i4, i4);
        int c5 = c(i3, i3, i4, i6);
        int g = g(i5, i4);
        int g2 = g(i3, i6);
        int c6 = c(aVar);
        int i7 = 0;
        if (i5 <= (aVar.E >> 1) && i < 0) {
            if (c5 == -1) {
                aVar.H = 0;
            }
            return 65536 | (65535 & c6);
        } else if (i5 < (this.bv[0].length * this.bx) - (aVar.E >> 1) || i <= 0) {
            if (c4 == -1) {
                i7 = z3 ? 32 : 16;
            }
            if (c5 == -1 || g2 == -1 || i6 > this.bv.length * bk) {
                i7 = z4 ? i7 | 128 : i7 | 64;
            }
            if ((T(c4) && T(c5)) || (g == -1 && g2 == -1)) {
                int i8 = z3 ? 32 : 16;
                i7 = z4 ? i8 | 128 : i8 | 64;
            } else if (T(c4) && (!T(c5) || c5 != -1)) {
                i7 = z3 ? 32 : 16;
            } else if ((!T(c4) || c4 != -1) && T(c5)) {
                i7 = z4 ? i7 | 128 : i7 | 64;
            } else if (a.h(i5) <= (aVar.E >> 1)) {
                if (c5 == -1) {
                    aVar.H = 0;
                }
                return 262144 | (65535 & c6);
            } else if (a.h(i5) >= 240 - (aVar.E >> 1)) {
                if (c5 == -1) {
                    aVar.H = 0;
                }
                return 524288 | (65535 & c6);
            }
            return (i7 << 16) | (65535 & c6);
        } else {
            if (c5 == -1) {
                aVar.H = 0;
            }
            return 131072 | (65535 & c6);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c__.a(int[], char):int
     arg types: [int[], int]
     candidates:
      c__.a(byte[], int):int
      c__.a(java.lang.String, int[]):c__om.a.a.e.p
      c__.a(a, java.lang.String):void
      c__.a(c__om.a.a.e.o, int):void
      c__.a(java.lang.Exception, java.lang.String):void
      c__.a(byte[], int[]):void
      c__.a(a, a):boolean
      c__.a(int, boolean):void
      c__.a(a, int):void
      c__.a(a, short):void
      c__.a(java.lang.String, int):void
      c__.a(java.lang.String, java.lang.String):void
      c__.a(java.lang.String, boolean):void
      c__.a(int[], int[]):void
      c__.a(int[], char):int */
    public final int d(int[] iArr) {
        c(iArr);
        int e = e(0, 0, a(iArr, '+'));
        int i = iArr[0];
        int i2 = 0;
        for (int i3 = 0; i3 < iArr.length - 1; i3++) {
            i2 += iArr[i3];
            if (e >= 0 && e < iArr[0]) {
                return i2;
            }
            if (e >= i2 && e < iArr[i3 + 1] + i2) {
                return iArr[i3 + 1];
            }
            if (e < iArr[i3 + 1] + i2) {
                return iArr[i3 + 1];
            }
        }
        return i;
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:48983, length=5899 in method: c__.d(int):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:48983, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void d(int r1) {
        /*
        // Can't load method instructions: Load method exception: index:48983, length=5899 in method: c__.d(int):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.d(int):void");
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:14558, length=5899 in method: c__.d(int, boolean):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:14558, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void d(int r1, boolean r2) {
        /*
        // Can't load method instructions: Load method exception: index:14558, length=5899 in method: c__.d(int, boolean):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.d(int, boolean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c__.a(a, int, int, boolean):void
     arg types: [a, int, int, int]
     candidates:
      c__.a(java.lang.String, c__om.a.a.e.l, int, char):b
      c__.a(int[], int[], int, int):void
      c__.a(c__om.a.a.e.o, int, c__om.a.a.e.p, int):void
      c__.a(c__om.a.a.e.o, a, int, int):void
      c__.a(java.lang.String, java.lang.String, int, int):void
      c__.a(int, int, int, int):boolean
      c__.a(a, int, int, boolean):void */
    public final void d(a aVar, int i) {
        switch (i) {
            case 0:
            default:
                return;
            case 257:
                aVar.J = -16;
                aVar.K = 3;
                break;
            case 258:
                aVar.K = 3;
                break;
            case 259:
                aVar.I = 0;
                aVar.K = 0;
                aVar.J = 0;
                aVar.H = 0;
                break;
            case 1024:
                break;
        }
        a(aVar, aVar.an[0], aVar.ay, true);
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:20957, length=5899 in method: c__.d(a, a, int):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:20957, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void d(defpackage.a r1, defpackage.a r2, int r3) {
        /*
        // Can't load method instructions: Load method exception: index:20957, length=5899 in method: c__.d(a, a, int):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.d(a, a, int):void");
    }

    public final void d(o oVar) {
        byte b;
        byte b2;
        if (this.bT != 0) {
            byte[] bArr = {-4, 4, -2, 2, -1, 1};
            switch (this.bS) {
                case -1:
                case 0:
                    b = 0;
                    b2 = 0;
                    break;
                case 1:
                    b2 = bArr[this.bR];
                    b = 0;
                    break;
                case 2:
                    bArr = new byte[]{-4, 4, -2, 2, -1, 1};
                    b = bArr[this.bR];
                    b2 = 0;
                    break;
                case 3:
                    bArr = new byte[6];
                    bArr[0] = 1;
                    bArr[1] = -1;
                    bArr[2] = -1;
                    b2 = bArr[this.bR];
                    b = bArr[this.bR];
                    break;
                case 4:
                    bArr = new byte[]{1, -1};
                    this.bY = bArr[this.bR];
                    b = 0;
                    b2 = 0;
                    break;
                case 5:
                    bArr = new byte[2];
                    bArr[0] = 1;
                    this.bZ = bArr[this.bR];
                default:
                    b = 0;
                    b2 = 0;
                    break;
            }
            oVar.translate(b2, b);
            byte b3 = this.bR;
            this.bR = (byte) (b3 + 1);
            if (b3 > bArr.length - 2) {
                this.bR = 0;
                if (this.bT != -1) {
                    this.bT = (byte) (this.bT - 1);
                    if (this.bT == 0) {
                        this.bT = 0;
                    }
                }
                oVar.translate(0, 0);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c__.a(a, a, short):void
     arg types: [a, a, int]
     candidates:
      c__.a(f, int, int):int
      c__.a(int[], int, int):void
      c__.a(int, int, int):void
      c__.a(int, int, boolean):void
      c__.a(a, a, int):void
      c__.a(a, boolean, int):void
      c__.a(c__om.a.a.e.o, int, int):void
      c__.a(java.lang.String, java.lang.String, java.lang.String):void
      c__.a(a, a, short):void */
    public final boolean d(a aVar, a aVar2) {
        boolean z = false;
        if (aVar == aVar2 || !a(aVar, aVar2) || a(aVar2.aw) == 3 || aVar2.aw == 7) {
            return false;
        }
        switch (aVar2.ao) {
            case 0:
                if (this.F == 0 || aVar.aw == 533) {
                    aVar2.C = 0;
                    if (aVar2.J != 0) {
                        z = true;
                    }
                    if (!z) {
                        if (aVar.ao == 7) {
                            if (aVar.c() != 2) {
                                a(aVar2, aVar, (short) 770);
                                break;
                            } else {
                                a(aVar2, aVar, (short) 769);
                                break;
                            }
                        } else {
                            switch (aVar.aw) {
                                case 512:
                                    if (aVar.ao != 8) {
                                        if (aVar.ao != 4 && aVar.ao != 5 && (aVar.ao != 6 || aVar.c() != 3)) {
                                            a(aVar2, aVar, (short) 769);
                                            break;
                                        } else {
                                            a(aVar2, aVar, (short) 770);
                                            break;
                                        }
                                    }
                                    break;
                                case 531:
                                    a(aVar2, aVar, (short) 770);
                                    break;
                                case 533:
                                    a(aVar2, aVar, (short) 773);
                                    break;
                                default:
                                    a(aVar2, aVar, (short) 769);
                                    break;
                            }
                        }
                    }
                    a(aVar2, aVar, (short) 770);
                    break;
                } else {
                    return false;
                }
                break;
            case 1:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
                if (aVar.ao == 0) {
                    switch (aVar.c()) {
                        case 3:
                        case 9:
                            if (aVar2.aw == 531) {
                                a(aVar, aVar2, (short) 770);
                            }
                            a(aVar2, aVar, (short) 770);
                            break;
                        case 13:
                            if (aVar2.aw == 531) {
                                a(aVar, aVar2, (short) 770);
                            }
                            a(aVar2, aVar, (short) 769);
                        case 14:
                            if (aVar2.aw == 531) {
                                a(aVar, aVar2, (short) 770);
                            }
                            a(aVar2, aVar, (short) 768);
                            break;
                        case 15:
                            if (aVar2.aw == 531) {
                                a(aVar, aVar2, (short) 770);
                            }
                            a(aVar2, aVar, (short) 770);
                            break;
                    }
                }
                break;
            case com.a.a.b.c.INT_16:
            case 21:
                a(aVar2, aVar, (short) 768);
                break;
        }
        return true;
    }

    public final void e(a aVar) {
        switch (aVar.ao) {
            case 0:
                f(aVar);
                return;
            case 1:
                l(aVar);
                return;
            case 2:
            case 3:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 23:
            case com.a.a.b.c.UUID:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
            case CharsToNameCanonicalizer.HASH_MULT /*33*/:
            case 34:
            case com.a.a.e.c.KEY_POUND /*35*/:
            case 36:
            case 37:
            case 38:
            case 39:
            default:
                return;
            case 4:
            case 11:
            case 12:
                p(aVar);
                return;
            case 5:
                p(aVar);
                return;
            case 6:
                p(aVar);
                return;
            case 7:
                p(aVar);
                return;
            case 8:
            case 13:
                p(aVar);
                return;
            case 9:
                l(aVar);
                return;
            case 10:
                l(aVar);
                return;
            case com.a.a.b.c.INT_16:
                n(aVar);
                return;
            case 21:
                g(aVar);
                return;
            case 22:
                h(aVar);
                return;
            case 30:
            case 31:
            case 32:
                o(aVar);
                return;
            case com.a.a.b.c.BOOL:
                i(aVar);
                return;
        }
    }

    public final void e(a aVar, a aVar2) {
        switch (aVar2.ao) {
            case 30:
            case 31:
                aVar.p += aVar2.p;
                if (aVar.p > aVar.r) {
                    aVar.p = aVar.r;
                    return;
                }
                return;
            default:
                this.cC += 1000;
                return;
        }
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:20957, length=5899 in method: c__.e(a, a, int):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:20957, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void e(defpackage.a r1, defpackage.a r2, int r3) {
        /*
        // Can't load method instructions: Load method exception: index:20957, length=5899 in method: c__.e(a, a, int):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.e(a, a, int):void");
    }

    public final void e(o oVar) {
        if (this.bV > 0) {
            this.bU = (short) (this.bU + this.bW);
            if (this.bW > 0) {
                if (this.bU > (this.bV <= 2 ? (short) 80 : 160)) {
                    this.bV = 0;
                    this.bW = 0;
                    U();
                    this.aI = false;
                }
            } else if (this.bU < this.bW) {
                this.bV = 0;
                this.bW = 0;
                U();
                this.aI = false;
            }
        }
        if (this.bU > 0) {
            int color = oVar.getColor();
            oVar.setColor(0);
            oVar.i(0, 0, 240, this.bU);
            oVar.i(0, 320 - this.bU, 240, this.bU);
            oVar.setColor(color);
        }
    }

    public final void e(String str, String str2) {
        f(str, str2);
        if (t(str2) >= 0) {
            b(str, u(str2));
        } else {
            b(str, v(str2));
        }
    }

    public final int[] e(int i, int i2) {
        return new int[]{(i << 4) + (this.bx >> 1), (i2 << 4) + (this.by >> 1)};
    }

    public final int f(a aVar, a aVar2) {
        if (aVar == aVar2) {
            return aVar.ay;
        }
        return a(aVar, aVar2.x, aVar2.z, 2, 2, aVar2.E, aVar2.G);
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:20957, length=5899 in method: c__.f(a):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:20957, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void f(defpackage.a r1) {
        /*
        // Can't load method instructions: Load method exception: index:20957, length=5899 in method: c__.f(a):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.f(a):void");
    }

    public final void f(o oVar) {
        int i = bl >> 4;
        int i2 = bm >> 4;
        int i3 = (bl + 240) >> 4;
        int i4 = (bm + com.a.a.p.f.OBEX_HTTP_OK) >> 4;
        if (!(i == bo && i3 == bq)) {
            if (i3 > bq) {
                b(bq + 1, i2, i3, i4);
            } else if (i < bo) {
                b(i, i2, bo - 1, i4);
            }
            bo = i;
            bq = i3;
        }
        if (!(i2 == bp && i4 == br)) {
            if (i4 > br) {
                b(i, br + 1, i3, i4);
            } else if (i2 < bp) {
                b(i, i2, i3, bp - 1);
            }
            bp = i2;
            br = i4;
        }
        int i5 = bl % aT;
        int i6 = bm % aU;
        int i7 = (bl + 240) % aT;
        int i8 = (bm + com.a.a.p.f.OBEX_HTTP_OK) % aU;
        boolean z = i5 < i7;
        boolean z2 = i6 < i8;
        if (z && z2) {
            a(oVar, i5, i6, 240, (int) com.a.a.p.f.OBEX_HTTP_OK, 0, 0);
        } else if (z2) {
            int i9 = aT - i5;
            a(oVar, i5, i6, i9, (int) com.a.a.p.f.OBEX_HTTP_OK, 0, 0);
            a(oVar, 0, i6, i7, (int) com.a.a.p.f.OBEX_HTTP_OK, i9, 0);
        } else if (z) {
            int i10 = aU - i6;
            a(oVar, i5, i6, 240, i10, 0, 0);
            a(oVar, i5, 0, 240, i8, 0, i10);
        } else {
            int i11 = aT - i5;
            int i12 = aU - i6;
            a(oVar, i5, i6, i11, i12, 0, 0);
            a(oVar, 0, i6, i7, i12, i11, 0);
            a(oVar, i5, 0, i11, i8, 0, i12);
            a(oVar, 0, 0, i7, i8, i11, i12);
        }
        oVar.j(0, 0, 240, 340);
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:25040, length=5899 in method: c__.f(java.lang.String):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:25040, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void f(java.lang.String r1) {
        /*
        // Can't load method instructions: Load method exception: index:25040, length=5899 in method: c__.f(java.lang.String):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.f(java.lang.String):void");
    }

    public final boolean f(String str, String str2) {
        if (t(str) != -1) {
            return false;
        }
        String[] strArr = {str, String.valueOf(str2)};
        new StringBuffer(String.valueOf(str)).append(" = ").append(str2).toString();
        cf.c(strArr);
        return true;
    }

    public final int g(int i, int i2) {
        if (i < 0 || i2 < 0 || i > this.bv[0].length * this.bx || i2 > this.bv.length * this.by) {
            return -1;
        }
        return (i >> 4) + ((i2 >> 4) * this.bv[0].length);
    }

    public final void g(int i, int i2, int i3) {
        if (!this.bH) {
            if (i3 != -1) {
                this.cM = i3;
                this.cL = i2;
            }
            this.bH = true;
            this.cJ = i;
            if (i == -1) {
                this.cJ = 4;
                return;
            }
            return;
        }
        this.bH = false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c__.a(a, short):void
     arg types: [a, int]
     candidates:
      c__.a(byte[], int):int
      c__.a(int[], char):int
      c__.a(java.lang.String, int[]):c__om.a.a.e.p
      c__.a(a, java.lang.String):void
      c__.a(c__om.a.a.e.o, int):void
      c__.a(java.lang.Exception, java.lang.String):void
      c__.a(byte[], int[]):void
      c__.a(a, a):boolean
      c__.a(int, boolean):void
      c__.a(a, int):void
      c__.a(java.lang.String, int):void
      c__.a(java.lang.String, java.lang.String):void
      c__.a(java.lang.String, boolean):void
      c__.a(int[], int[]):void
      c__.a(a, short):void */
    public final void g(a aVar) {
        switch (aVar.aw) {
            case 0:
            default:
                return;
            case 533:
            case 3840:
                a(aVar);
                if (aVar.J >= 0 && aVar.y >= aVar.z) {
                    y(x(aVar.ap));
                    return;
                }
                return;
            case 768:
                if (aVar.as) {
                    a(aVar, (short) 0);
                    return;
                }
                return;
        }
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:43987, length=5899 in method: c__.g(c__om.a.a.e.o):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:43987, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void g(c__om.a.a.e.o r1) {
        /*
        // Can't load method instructions: Load method exception: index:43987, length=5899 in method: c__.g(c__om.a.a.e.o):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.g(c__om.a.a.e.o):void");
    }

    public final boolean g(String str, String str2) {
        int indexOf = str.indexOf("&&");
        int indexOf2 = str.indexOf("||");
        String str3 = "";
        String str4 = "";
        if (indexOf > 0 || indexOf2 > 0) {
            str3 = str.substring(0, indexOf - 1).trim();
            str4 = str.substring(indexOf + 2, str.length()).trim();
        }
        if (indexOf > 0 ? q(str3) && q(str4) : indexOf2 > 0 ? q(str3) || q(str4) : q(str)) {
            r(str2);
        } else if (str2.length() <= 0) {
            int i = cc;
            while (true) {
                int i2 = i;
                if (i2 >= cd.aA) {
                    break;
                } else if (((b) cd.k(i2)).h(0) == 9) {
                    return true;
                } else {
                    cc++;
                    i = i2 + 1;
                }
            }
        } else {
            return true;
        }
        return false;
    }

    public final int h(int i, int i2) {
        return V(g(i, i2));
    }

    public final b h(String str, String str2) {
        int indexOf;
        String stringBuffer = new StringBuffer(String.valueOf(str)).append(str2).toString();
        b bVar = new b();
        if (stringBuffer.length() < 2) {
            bVar.c("");
        } else {
            int i = 0;
            int i2 = 0;
            while (i < stringBuffer.length() && (indexOf = stringBuffer.indexOf(str2, i2)) >= 0) {
                String substring = stringBuffer.substring(i2, indexOf);
                if (substring.charAt(0) == "\"".charAt(0)) {
                    int indexOf2 = stringBuffer.indexOf("\"", i2 + 1);
                    substring = stringBuffer.substring(i2 + 1, indexOf2);
                    indexOf = indexOf2 + 1;
                }
                if (substring.charAt(0) == '$') {
                    substring = String.valueOf(u(substring.substring(1)));
                }
                bVar.c(substring);
                i++;
                i2 = indexOf + 1;
            }
        }
        return bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c__.a(a, short):void
     arg types: [a, int]
     candidates:
      c__.a(byte[], int):int
      c__.a(int[], char):int
      c__.a(java.lang.String, int[]):c__om.a.a.e.p
      c__.a(a, java.lang.String):void
      c__.a(c__om.a.a.e.o, int):void
      c__.a(java.lang.Exception, java.lang.String):void
      c__.a(byte[], int[]):void
      c__.a(a, a):boolean
      c__.a(int, boolean):void
      c__.a(a, int):void
      c__.a(java.lang.String, int):void
      c__.a(java.lang.String, java.lang.String):void
      c__.a(java.lang.String, boolean):void
      c__.a(int[], int[]):void
      c__.a(a, short):void */
    public final void h(a aVar) {
        switch (aVar.aw) {
            case 0:
                a(aVar, (short) 256);
                return;
            case 258:
                if (aVar.J >= 0 && aVar.y >= aVar.z) {
                    a(aVar, (short) 259);
                }
                a(aVar);
                return;
            default:
                return;
        }
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:6494, length=5899 in method: c__.h(c__om.a.a.e.o):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:6494, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void h(c__om.a.a.e.o r1) {
        /*
        // Can't load method instructions: Load method exception: index:6494, length=5899 in method: c__.h(c__om.a.a.e.o):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.h(c__om.a.a.e.o):void");
    }

    public final void h(String str) {
        int i;
        String str2;
        if (q(str)) {
            int i2 = 0;
            while (true) {
                if (i2 >= 6) {
                    i = 0;
                    str2 = null;
                    break;
                }
                int indexOf = str.indexOf("=!<>[]".charAt(i2));
                if (indexOf != -1) {
                    String substring = str.substring(0, indexOf);
                    String substring2 = str.substring(indexOf + 1, str.length());
                    if (t(substring2) != -1) {
                        str2 = substring;
                        i = u(substring2);
                    } else {
                        str2 = substring;
                        i = v(substring2);
                    }
                } else {
                    i2++;
                }
            }
            int i3 = cc + 1;
            while (true) {
                int i4 = i3;
                if (u(str2) >= i) {
                    cc = i4 - 1;
                    return;
                }
                b bVar = (b) cd.k(i4);
                int h = bVar.h(0);
                a(bVar);
                i3 = h == 13 ? cc : i4 + 1;
            }
        }
    }

    public final void i(a aVar) {
        switch (a(aVar.aw)) {
            case 0:
                aVar.y = aVar.z;
                switch (aVar.aw) {
                    case 2:
                        aVar.I++;
                        if ((aVar.x - bl) - (aVar.E >> 1) > 240 || aVar.x - bl <= 0) {
                            y(x(aVar.ap));
                            return;
                        } else {
                            a(aVar);
                            return;
                        }
                    default:
                        return;
                }
            default:
                return;
        }
    }

    public final void i(String str) {
        int t = t(str);
        if (t >= 0) {
            cf.a(t);
        }
    }

    public final void j() {
        this.q = 0;
        this.aK = 0;
        this.p = 0;
        this.aG = -1;
        this.j = -1;
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:20957, length=5899 in method: c__.j(a):boolean, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:20957, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final boolean j(defpackage.a r1) {
        /*
        // Can't load method instructions: Load method exception: index:20957, length=5899 in method: c__.j(a):boolean, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.j(a):boolean");
    }

    public final void k() {
        bD();
    }

    public final boolean k(a aVar) {
        int d = d(aVar);
        short s = (short) (d >> 16);
        short s2 = (short) (d & z.CONSTRAINT_MASK);
        if (s > -1) {
            int length = (s2 % this.bv[0].length) << 4;
            int length2 = (s2 / this.bv[0].length) << 4;
            int i = s2 == -1 ? aVar.x : this.bx + length + 1;
            int i2 = s2 == -1 ? aVar.x : length - 2;
            int i3 = s2 == -1 ? aVar.y : this.by + length2 + 1;
            int i4 = s2 == -1 ? aVar.y : length2 - 2;
            int length3 = this.bv[0].length * this.bx;
            switch (s) {
                case 1:
                    aVar.x = aVar.E >> 1;
                    aVar.I = 0;
                    break;
                case 2:
                    aVar.I = 0;
                    aVar.x = (length3 - 2) - (aVar.E >> 1);
                    break;
                case 4:
                    if (aVar.ao == 0) {
                        aVar.a(aVar.E >> 1);
                        aVar.I = 0;
                        break;
                    }
                    break;
                case 8:
                    if (aVar.ao == 0) {
                        aVar.I = 0;
                        aVar.a(240 - (aVar.E >> 1));
                        break;
                    }
                    break;
                case 16:
                    aVar.I = 0;
                    aVar.x = i;
                    break;
                case 32:
                    aVar.I = 0;
                    aVar.x = i2;
                    break;
                case 64:
                    aVar.H = 0;
                    aVar.z = i3;
                    break;
                case 65:
                    aVar.x = aVar.E >> 1;
                    aVar.I = 0;
                    aVar.H = 0;
                    break;
                case com.a.a.p.c.TYPE /*66*/:
                    aVar.I = 0;
                    aVar.H = 0;
                    aVar.x = (length3 - 2) - (aVar.E >> 1);
                    break;
                case 80:
                    aVar.I = 0;
                    aVar.H = 0;
                    aVar.x = i;
                    aVar.z = i3;
                    break;
                case 96:
                    aVar.I = 0;
                    aVar.H = 0;
                    aVar.x = i2;
                    aVar.z = i3;
                    break;
                case 128:
                    aVar.H = 0;
                    aVar.z = i4;
                    break;
                case 129:
                    aVar.x = aVar.E >> 1;
                    aVar.I = 0;
                    aVar.H = 0;
                    break;
                case 130:
                    aVar.I = 0;
                    aVar.H = 0;
                    aVar.x = (length3 - 2) - (aVar.E >> 1);
                    break;
                case MIDIControl.NOTE_ON /*144*/:
                    aVar.I = 0;
                    aVar.H = 0;
                    aVar.x = i;
                    aVar.z = i4;
                    break;
                case com.a.a.p.f.OBEX_HTTP_OK /*160*/:
                    aVar.I = 0;
                    aVar.H = 0;
                    aVar.x = i2;
                    aVar.z = i4;
                    break;
            }
        }
        return false;
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:53972, length=5899 in method: c__.l():void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:53972, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:461)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void l() {
        /*
        // Can't load method instructions: Load method exception: index:53972, length=5899 in method: c__.l():void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.l():void");
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:20957, length=5899 in method: c__.l(a):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:20957, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void l(defpackage.a r1) {
        /*
        // Can't load method instructions: Load method exception: index:20957, length=5899 in method: c__.l(a):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.l(a):void");
    }

    public final void m() {
        boolean z = true;
        boolean z2 = this.ar != 0;
        byte b = this.bh;
        this.bh = (byte) (b + 1);
        if (b < 4) {
            z = false;
        }
        if (z2 && z) {
            n();
        }
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:20957, length=5899 in method: c__.m(a):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:20957, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void m(defpackage.a r1) {
        /*
        // Can't load method instructions: Load method exception: index:20957, length=5899 in method: c__.m(a):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.m(a):void");
    }

    public final String n(String str) {
        String str2 = null;
        cb = 5;
        cd.f();
        ce.clear();
        System.gc();
        cc = 0;
        String stringBuffer = w(str).toString();
        b bVar = new b();
        int i = 0;
        String str3 = null;
        int i2 = 0;
        while (i < stringBuffer.length()) {
            if (stringBuffer.charAt(i) == ']') {
                str3 = new String(stringBuffer.substring(i2, i + 1).trim());
                i2 = i + 1;
                i = stringBuffer.indexOf(10, i);
            }
            if (stringBuffer.charAt(i) == 10) {
                String str4 = new String(stringBuffer.substring(i2, i).trim());
                if (str4.trim().length() > 0) {
                    b h = h(str4.substring(1), BossService.ID_SEPARATOR);
                    h.a(new StringBuffer(String.valueOf(str4.charAt(0) - '0')).toString(), 0);
                    bVar.c(h);
                }
                i2 = i;
            }
            if ((stringBuffer.charAt(i) == '[' && i > 0) || i == stringBuffer.length() - 1) {
                if (str2 == null) {
                    str2 = str3.substring(1, str3.length() - 1);
                }
                ce.put(str3, bVar.M());
                bVar.f();
            }
            i++;
        }
        return str2;
    }

    public final void n() {
        this.ar = 0;
        this.bh = 0;
        this.aK = 0;
    }

    public final void n(int i) {
        if (u != v) {
            v = u;
        }
        u = i;
        this.o = 0;
        u();
        j();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c__.a(a, short):void
     arg types: [a, int]
     candidates:
      c__.a(byte[], int):int
      c__.a(int[], char):int
      c__.a(java.lang.String, int[]):c__om.a.a.e.p
      c__.a(a, java.lang.String):void
      c__.a(c__om.a.a.e.o, int):void
      c__.a(java.lang.Exception, java.lang.String):void
      c__.a(byte[], int[]):void
      c__.a(a, a):boolean
      c__.a(int, boolean):void
      c__.a(a, int):void
      c__.a(java.lang.String, int):void
      c__.a(java.lang.String, java.lang.String):void
      c__.a(java.lang.String, boolean):void
      c__.a(int[], int[]):void
      c__.a(a, short):void */
    public final void n(a aVar) {
        boolean z = false;
        switch (a(aVar.aw)) {
            case 0:
                aVar.y = aVar.z;
                for (int i = 0; i < Q(); i++) {
                    a w = w(i);
                    byte a = a(w.aw);
                    if (aVar != w && a(w, aVar)) {
                        if (a == 6 || w.aw == 771 || w.aw == 770) {
                            a(aVar, (short) 768);
                            return;
                        } else if (a == 2) {
                            a(aVar, (short) 768);
                            return;
                        }
                    }
                }
                a c = c(aVar, 0, 0);
                if (c == null) {
                    return;
                }
                if (!c.k) {
                    int f = f(c.z, c.y, c.J);
                    if (c.J > 0) {
                        if (c.y >= aVar.y - aVar.F && c.y <= aVar.y - (aVar.F / 3)) {
                            aVar.ab = c;
                            a(c, (short) 0);
                            c.y = aVar.y - aVar.F;
                            c.k = true;
                            return;
                        }
                        return;
                    } else if (c.J < 0 && f < aVar.F && c.J != 0) {
                        c.I = 0;
                        return;
                    } else {
                        return;
                    }
                } else if (c.J != 0 || c.K != 0) {
                    c.k = false;
                    aVar.ab = null;
                    return;
                } else {
                    return;
                }
            case 3:
                if (aVar.ab != null && aVar.ab.k) {
                    aVar.ab.I = 0;
                    a(aVar.ab, (short) 258);
                    aVar.ab.d(aVar.ab.ay | 8);
                    a aVar2 = aVar.ab;
                    if (!aVar.ab.k) {
                        z = true;
                    }
                    aVar2.k = z;
                    aVar.ab = null;
                }
                if (aVar.as) {
                    a(aVar, (short) 3840);
                    return;
                }
                return;
            case 15:
                if (aVar.as) {
                    y(x(aVar.ap));
                    return;
                }
                return;
            default:
                return;
        }
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:49732, length=5899 in method: c__.o():void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:49732, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:461)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void o() {
        /*
        // Can't load method instructions: Load method exception: index:49732, length=5899 in method: c__.o():void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.o():void");
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:48983, length=5899 in method: c__.o(int):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:48983, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void o(int r1) {
        /*
        // Can't load method instructions: Load method exception: index:48983, length=5899 in method: c__.o(int):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.o(int):void");
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:20957, length=5899 in method: c__.o(a):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:20957, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void o(defpackage.a r1) {
        /*
        // Can't load method instructions: Load method exception: index:20957, length=5899 in method: c__.o(a):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.o(a):void");
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:53972, length=5899 in method: c__.p(int):b, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:53972, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:461)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final defpackage.b p(int r1) {
        /*
        // Can't load method instructions: Load method exception: index:53972, length=5899 in method: c__.p(int):b, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.p(int):b");
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:49732, length=5899 in method: c__.p():void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:49732, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void p() {
        /*
        // Can't load method instructions: Load method exception: index:49732, length=5899 in method: c__.p():void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.p():void");
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:20957, length=5899 in method: c__.p(a):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:20957, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void p(defpackage.a r1) {
        /*
        // Can't load method instructions: Load method exception: index:20957, length=5899 in method: c__.p(a):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.p(a):void");
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:58435, length=5899 in method: c__.q():void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:58435, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:461)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void q() {
        /*
        // Can't load method instructions: Load method exception: index:58435, length=5899 in method: c__.q():void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.q():void");
    }

    public final boolean q(String str) {
        int i = 0;
        while (i < 6) {
            int indexOf = str.indexOf("=!<>[]".charAt(i));
            if (indexOf != -1) {
                int u2 = u(str.substring(0, indexOf));
                int v2 = v(str.substring(indexOf + 1, str.length()));
                switch (i) {
                    case 0:
                        return u2 == v2;
                    case 1:
                        return u2 != v2;
                    case 2:
                        return u2 < v2;
                    case 3:
                        return u2 > v2;
                    case 4:
                        return u2 <= v2;
                    case 5:
                        return u2 >= v2;
                    default:
                        return false;
                }
            } else {
                i++;
            }
        }
        return false;
    }

    public final void r() {
        int i = 0;
        for (int i2 = 1; i2 < Q(); i2++) {
            a w = w(i2);
            if (t(w.ao) && i < this.cJ) {
                w.p = -1;
                b(w, (a) null);
                i++;
            }
        }
    }

    public final void r(int i) {
        boolean z = true;
        if (this.ar == 0) {
            this.ar = i;
            this.bh = 0;
            return;
        }
        if ((this.aK == 0) && (this.ar == i)) {
            this.ar = 0;
            this.aK = i;
            return;
        }
        boolean z2 = (this.aK != 0) & (this.ar != 0);
        if (this.ar == this.aK) {
            z = false;
        }
        if (z2 && z) {
            n();
        }
    }

    public void run() {
        while (true) {
            try {
                long currentTimeMillis = System.currentTimeMillis();
                bC();
                k();
                Thread.yield();
                long currentTimeMillis2 = System.currentTimeMillis();
                if (currentTimeMillis2 - currentTimeMillis < 50) {
                    long j2 = 50 - (currentTimeMillis2 - currentTimeMillis);
                    if (this.ci) {
                        j2 <<= 1;
                    }
                    Thread.sleep(j2);
                }
            } catch (Exception e) {
                a(e, "");
            }
        }
    }

    public final void s(String str) {
        int i;
        String[] strArr = (String[]) cf.k(t(h(str, "=").l(0).trim()));
        for (int i2 = 0; i2 < 5; i2++) {
            int indexOf = str.indexOf("+-*/%".charAt(i2));
            if (indexOf >= 0) {
                String trim = str.substring(str.indexOf(61) + 1, indexOf).trim();
                String trim2 = str.substring(indexOf + 1, str.length()).trim();
                int u2 = u(trim);
                if (u2 == 0) {
                    u2 = v(trim);
                }
                int u3 = u(trim2);
                if (u3 == 0) {
                    u3 = v(trim2);
                }
                switch (i2) {
                    case 0:
                        i = u2 + u3;
                        break;
                    case 1:
                        i = u2 - u3;
                        break;
                    case 2:
                        i = u2 * u3;
                        break;
                    case 3:
                        if (u3 == 0) {
                            u3 = 1;
                        }
                        i = u2 / u3;
                        break;
                    case 4:
                        i = u2 % u3;
                        break;
                    default:
                        i = 0;
                        break;
                }
                new StringBuffer(String.valueOf(strArr[0])).append("=").append(i).toString();
                b(strArr[0], i);
                return;
            }
        }
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:58435, length=5899 in method: c__.s(int):boolean, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:58435, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final boolean s(int r1) {
        /*
        // Can't load method instructions: Load method exception: index:58435, length=5899 in method: c__.s(int):boolean, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.s(int):boolean");
    }

    public final void t() {
        this.bT = 0;
        this.bR = 0;
    }

    public final int u(String str) {
        String lowerCase = str.toLowerCase();
        String substring = lowerCase.charAt(0) == '$' ? lowerCase.substring(1) : lowerCase;
        for (int i = 0; i < cf.aA; i++) {
            String[] strArr = (String[]) cf.k(i);
            if (strArr[0].toLowerCase().compareTo(substring) == 0) {
                return v(strArr[1]);
            }
        }
        return 0;
    }

    public final void u() {
        t();
        this.bS = -1;
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:14558, length=5899 in method: c__.w(int):a, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:14558, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final defpackage.a w(int r1) {
        /*
        // Can't load method instructions: Load method exception: index:14558, length=5899 in method: c__.w(int):a, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.w(int):a");
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:14558, length=5899 in method: c__.x(int):int, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:14558, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final int x(int r1) {
        /*
        // Can't load method instructions: Load method exception: index:14558, length=5899 in method: c__.x(int):int, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.x(int):int");
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:14558, length=5899 in method: c__.y(int):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:14558, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void y(int r1) {
        /*
        // Can't load method instructions: Load method exception: index:14558, length=5899 in method: c__.y(int):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.y(int):void");
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:14558, length=5899 in method: c__.z(int):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:14558, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void z(int r1) {
        /*
        // Can't load method instructions: Load method exception: index:14558, length=5899 in method: c__.z(int):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.c__.z(int):void");
    }
}
