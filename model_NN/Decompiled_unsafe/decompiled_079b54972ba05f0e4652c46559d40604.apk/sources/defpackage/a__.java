package defpackage;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    */
/* renamed from: a__  reason: default package */
public final class a__ {
    public static boolean ad;
    public int C;
    public int E;
    public int F;
    public int G;
    public int H;
    public int I;
    public int J;
    public int K;
    public String R;
    public a__ ab;
    public a__ ac;
    public boolean ae;
    public int[] af;
    public int[] ag;
    public int[] ah;
    public f ai;
    public byte[][] aj;
    public byte[][] al;
    public int[] am;
    public int[] an;
    public int ao;
    public int ap;
    public int aq;
    public int ar;
    public boolean as;
    public boolean at;
    public boolean au;
    public boolean av;
    public short aw;
    public short ax;
    public int ay;
    public int g;
    public int h;
    public int i;
    public int j;
    public boolean k;
    public String m;
    public int n;
    public int o;
    public int p;
    public int q;
    public int r;
    public int s;
    public int u;
    public int v;
    public int x;
    public int y;
    public int z;

    public a__() {
        this.g = -1;
        this.h = 1;
        this.o = 1;
        this.r = 1;
        this.s = 1;
        this.G = 5;
        this.an = new int[4];
        this.ap = -1;
        this.aq = this.an[0];
        this.ar = -1;
        this.ay = 2;
    }

    private a__(int[] iArr) {
        this.g = -1;
        this.h = 1;
        this.o = 1;
        this.r = 1;
        this.s = 1;
        this.G = 5;
        this.an = new int[4];
        this.ap = -1;
        this.aq = this.an[0];
        this.ar = -1;
        this.ay = 2;
        this.am = iArr;
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:34378, length=5899 in method: a__.<init>(int[], f):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:34378, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:461)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    private a__(int[] r1, defpackage.f r2) {
        /*
        // Can't load method instructions: Load method exception: index:34378, length=5899 in method: a__.<init>(int[], f):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.a__.<init>(int[], f):void");
    }

    private int b() {
        return this.an[0];
    }

    public static int h(int i2) {
        return i2 - c.bl;
    }

    public final void a(int i2) {
        this.x = c.bl + i2;
    }

    public final void a(a__ a__) {
        this.x = a__.x;
        this.y = a__.y;
        this.z = a__.z;
    }

    public final void a(int[] iArr, f fVar) {
        this.ac = new a__(iArr, fVar);
        this.ac.av = true;
    }

    public final boolean a(a__ a__, int i2, int i3) {
        return Math.abs(a__.z - this.z) <= i3 && Math.abs(a__.x - this.x) <= i2;
    }

    public final void b(int i2) {
        this.y = c.bm + i2;
    }

    public final int c() {
        return this.an[1];
    }

    public final int d() {
        return this.an[3];
    }

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: index:34378, length=5899 in method: a__.d(int):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: java.lang.IndexOutOfBoundsException: index:34378, length=5899
        	at com.android.dex.Dex.checkBounds(Dex.java:146)
        	at com.android.dex.Dex.access$1100(Dex.java:51)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:766)
        	at com.android.dex.Dex$FieldIdTable.get(Dex.java:763)
        	at jadx.core.dex.nodes.DexNode.getFieldId(DexNode.java:255)
        	at jadx.core.dex.info.FieldInfo.fromDex(FieldInfo.java:31)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:448)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public final void d(int r1) {
        /*
        // Can't load method instructions: Load method exception: index:34378, length=5899 in method: a__.d(int):void, dex: classes.dex
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.a__.d(int):void");
    }

    public final int i(int i2) {
        return this.aj[b()][i2];
    }

    public final int j(int i2) {
        return this.al[b()][i2];
    }
}
