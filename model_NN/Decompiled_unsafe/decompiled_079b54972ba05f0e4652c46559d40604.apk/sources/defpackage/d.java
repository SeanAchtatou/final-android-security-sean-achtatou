package defpackage;

import com.a.a.c.c;
import com.a.a.e.o;
import com.a.a.e.p;
import com.a.a.i.a;

/* renamed from: d  reason: default package */
public final class d extends c {
    public int bQ;
    public int bo;
    public int eA;
    public int eB;
    public p[] eC;
    public String[] eD;
    public p eE;
    public p eF;
    public int eG = 2;
    public boolean eH;
    public int eu;
    public String[] ev;
    public String[] ew;
    public String[] ex;
    public int ey;
    public int ez;

    public d() {
        k(true);
        c.R(0);
        c.R(60);
        c.R(61);
        c.R(62);
        c.R(63);
        c.R(64);
        aa();
        c.cG = c.d("/res/l_ch.txt", 2);
        String[] d = c.d("/res/help.txt", 2);
        for (String append : d) {
            String[] strArr = c.cG;
            strArr[25] = new StringBuffer(String.valueOf(strArr[25])).append(append).toString();
        }
        String[] d2 = c.d("/res/about.txt", 2);
        for (String append2 : d2) {
            String[] strArr2 = c.cG;
            strArr2[26] = new StringBuffer(String.valueOf(strArr2[26])).append(append2).toString();
        }
        f("GAME_SAVE", "20");
        f("GAME_END", "19");
        f("joinL", "2312");
        f("joinR", "2313");
        f("自动触发", "0");
        f("坐标触发", "1");
        f("行列触发", "2");
        f("时间触发", "3");
        f("精灵触发", "4");
        f("人数触发", "5");
        f("ROLL", "7");
        f("DEAD", "3840");
        f("STAND", "0");
        f("红兵", "4");
        f("蓝兵", "5");
        f("刀兵", "6");
        f("门", "21");
        f("门", "21");
        f("门", "21");
        f("JA", "7");
        f("MM", "11");
        f("MM1", "12");
        f("AI1", "13");
        f("火", "22");
        f("雷", "1");
        f("AI", "8");
        f("CAI", "9");
        f("JIN", "10");
        f("LEI", "1");
        f("艾特殊撞击", "533");
        f("艾特殊跳", "534");
        f("门被撞击", "533");
        f("躲闪", "2560");
        f("ATT", "512");
        f("ACT_FOOT0", "15");
        f("ACT_FOOT2", "15");
        f("ACT_艾撞击", "2");
        f("SPT_LIE_STILL", "1794");
        f("ALL_DEAD", new StringBuffer().append(this.x).toString());
    }

    private void aA() {
        k();
        this.cC = 0;
        c.R(34);
        c.R(9);
        c.R(35);
        c.R(36);
        c.R(37);
        k();
        c.N(35);
        c.N(36);
        k();
    }

    private void aw() {
        this.ev = null;
        this.ew = null;
        this.bo = 0;
    }

    private void ax() {
        B(1);
        this.ey = 0;
        this.ez = 0;
        this.eC = new p[12];
        this.eD = c.d("/res/title/dialog.txt", 2);
        for (int i = 0; i < this.eC.length; i++) {
            String stringBuffer = new StringBuffer("0").append(i + 1).toString();
            if (i < 9) {
                stringBuffer = new StringBuffer("00").append(i + 1).toString();
            }
            this.eC[i] = c.m(new StringBuffer("title/").append(stringBuffer.concat(".png")).toString());
        }
    }

    private void ay() {
        this.eD = null;
        this.eC = null;
    }

    private static void az() {
        try {
            Mi.Y.al("http://go.i139.cn/gcomm1/portal/spchannel.do?url=http://gamepie.i139.cn/wap/s.do?j=3channel");
        } catch (c e) {
            e.printStackTrace();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c.a(int, boolean):void
     arg types: [int, int]
     candidates:
      c.a(byte[], int):int
      c.a(int[], char):int
      c.a(java.lang.String, int[]):com.a.a.e.p
      c.a(a, java.lang.String):void
      c.a(com.a.a.e.o, int):void
      c.a(java.lang.Exception, java.lang.String):void
      c.a(byte[], int[]):void
      c.a(a, a):boolean
      c.a(a, int):void
      c.a(a, short):void
      c.a(java.lang.String, int):void
      c.a(java.lang.String, java.lang.String):void
      c.a(java.lang.String, boolean):void
      c.a(int[], int[]):void
      c.a(int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c.a(int, int, boolean):void
     arg types: [int, int, int]
     candidates:
      c.a(f, int, int):int
      c.a(int[], int, int):void
      c.a(int, int, int):void
      c.a(a, a, int):void
      c.a(a, a, short):void
      c.a(a, boolean, int):void
      c.a(com.a.a.e.o, int, int):void
      c.a(java.lang.String, java.lang.String, java.lang.String):void
      c.a(int, int, boolean):void */
    private void j(o oVar) {
        int N = c.N(9);
        int O = c.O(9) / 7;
        c.a(oVar, c.P(34), 0, 0, 0, 0);
        c.a(oVar, c.P(33), 0, 0, 0, 0);
        c.a(oVar, c.P(35), 0, (c.P(33).getHeight() / 2) + 5, 0, 0);
        c.a(oVar, c.P(37), 240, 320, 0, 40);
        c.a(oVar, c.P(36), 120, (O / 2) + (265 - (c.O(36) / 2)), 0, 17);
        c.a(this.o, c.P(9), 120, 265, 17, oVar, N, O);
        if (a((int) a.PHOTO, 240, 20, 20)) {
            this.q = 50;
        } else if (a((int) a.PHOTO, 282, 20, 20)) {
            this.q = 56;
        } else if (a((int) a.PHOTO, 260, 20, 25)) {
            this.q = 53;
        }
        a(this.q, 6, 1);
        if (c.I(this.q) || P()) {
            switch (this.o) {
                case 0:
                    a(3, false);
                    return;
                case 1:
                    try {
                        if (c.q(1)) {
                            a(4, 3, false);
                            return;
                        } else {
                            a(3, false);
                            return;
                        }
                    } catch (Exception e) {
                        c.a(e, "load");
                        a(3, false);
                        return;
                    }
                case 2:
                    a(6, true);
                    return;
                case 3:
                    a(5, true);
                    return;
                case 4:
                    a(7, true);
                    return;
                case 5:
                    a(8, true);
                    return;
                case 6:
                    Mi.Y.a();
                    return;
                default:
                    return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c.a(java.lang.String, com.a.a.e.l, int, char):b
     arg types: [java.lang.String, com.a.a.e.l, int, int]
     candidates:
      c.a(int[], int[], int, int):void
      c.a(a, int, int, boolean):void
      c.a(com.a.a.e.o, int, com.a.a.e.p, int):void
      c.a(com.a.a.e.o, a, int, int):void
      c.a(java.lang.String, java.lang.String, int, int):void
      c.a(int, int, int, int):boolean
      c.a(java.lang.String, com.a.a.e.l, int, char):b */
    public final void a(int i) {
        switch (i) {
            case 1:
                c.af();
                return;
            case 2:
                ay();
                k();
                this.bX = true;
                this.aQ = null;
                c.af();
                k();
                c.V();
                k();
                aA();
                this.o = 0;
                return;
            case 3:
                c.af();
                this.bX = true;
                this.cC = 0;
                f(n("s/school.s"));
                T();
                while (c.cb != 5) {
                    X();
                }
                return;
            case 4:
                c.V();
                c.af();
                b p = p(1);
                this.aN = p.l(0);
                this.cC = p.h(1);
                k();
                f(n(this.aN));
                T();
                while (c.cb != 5) {
                    X();
                }
                k();
                this.aQ.n = p.h(2);
                this.aN = null;
                return;
            case 5:
            case 9:
            case 10:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case com.a.a.b.c.INT_16:
            case 21:
            default:
                return;
            case 6:
                c.af();
                c.R(47);
                c.R(48);
                k();
                c.R(49);
                ac();
                k();
                return;
            case 7:
                b a = c.a(c.cG[25], c.aH, 235, '^');
                this.ev = new String[a.aA];
                a.b(this.ev);
                this.bo = 0;
                return;
            case 8:
                b a2 = c.a(c.cG[26], c.aH, 235, '^');
                this.ew = new String[a2.aA];
                a2.b(this.ew);
                this.bo = 0;
                return;
            case 11:
                c.R(44);
                c.R(45);
                c.R(46);
                k();
                this.ex = (String[]) c.Q(46);
                k();
                a("", this.ex[0], 236, 3);
                return;
            case 19:
                c.af();
                c.R(42);
                return;
            case 22:
                ax();
                return;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c.a(int, boolean):void
     arg types: [int, int]
     candidates:
      c.a(byte[], int):int
      c.a(int[], char):int
      c.a(java.lang.String, int[]):com.a.a.e.p
      c.a(a, java.lang.String):void
      c.a(com.a.a.e.o, int):void
      c.a(java.lang.Exception, java.lang.String):void
      c.a(byte[], int[]):void
      c.a(a, a):boolean
      c.a(a, int):void
      c.a(a, short):void
      c.a(java.lang.String, int):void
      c.a(java.lang.String, java.lang.String):void
      c.a(java.lang.String, boolean):void
      c.a(int[], int[]):void
      c.a(int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c.a(com.a.a.e.o, int, int, int, int, int, boolean, int):void
     arg types: [com.a.a.e.o, int, int, int, int, int, int, int]
     candidates:
      c.a(int, com.a.a.e.p, int, int, int, com.a.a.e.o, int, int):int
      c.a(int, int, int, int, int, int, int, int):boolean
      c.a(com.a.a.e.o, int, int, int, int, int, boolean, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c.a(com.a.a.e.o, a, int, int, boolean, boolean, boolean):void
     arg types: [com.a.a.e.o, a, int, int, int, boolean, int]
     candidates:
      c.a(com.a.a.e.o, int, int, int, int, int, int):void
      c.a(int[], com.a.a.e.o, int, int, int, boolean, boolean):void
      c.a(a, int, int, int, int, int, int):int
      c.a(com.a.a.e.o, a, int, int, boolean, boolean, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:256:0x0ef1  */
    /* JADX WARNING: Removed duplicated region for block: B:301:0x0e63 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(com.a.a.e.o r19) {
        /*
            r18 = this;
            com.a.a.e.l r2 = defpackage.c.aH
            r0 = r19
            r0.a(r2)
            int r2 = defpackage.c.u
            switch(r2) {
                case 0: goto L_0x02a0;
                case 1: goto L_0x02a9;
                case 2: goto L_0x073a;
                case 3: goto L_0x0cb7;
                case 4: goto L_0x000c;
                case 5: goto L_0x0792;
                case 6: goto L_0x082c;
                case 7: goto L_0x0879;
                case 8: goto L_0x0879;
                case 9: goto L_0x0a08;
                case 10: goto L_0x0a53;
                case 11: goto L_0x000c;
                case 12: goto L_0x000c;
                case 13: goto L_0x000c;
                case 14: goto L_0x000c;
                case 15: goto L_0x000c;
                case 16: goto L_0x000c;
                case 17: goto L_0x000c;
                case 18: goto L_0x0c77;
                case 19: goto L_0x0c24;
                case 20: goto L_0x0991;
                case 21: goto L_0x0759;
                case 22: goto L_0x032f;
                case 23: goto L_0x004e;
                case 24: goto L_0x00e1;
                default: goto L_0x000c;
            }
        L_0x000c:
            r2 = 16711680(0xff0000, float:2.3418052E-38)
            r0 = r19
            r0.setColor(r2)
            r0 = r18
            boolean r2 = r0.eH
            if (r2 == 0) goto L_0x004d
            r0 = r18
            com.a.a.e.p r2 = r0.eF
            if (r2 != 0) goto L_0x0029
            java.lang.String r2 = "bb.png"
            com.a.a.e.p r2 = defpackage.c.m(r2)
            r0 = r18
            r0.eF = r2
        L_0x0029:
            r2 = 0
            r0 = r18
            com.a.a.e.p r3 = r0.eF
            int r3 = r3.getHeight()
            int r3 = -r3
            r0 = r19
            r0.translate(r2, r3)
            r0 = r18
            com.a.a.e.p r2 = r0.eF
            r3 = 0
            r0 = r18
            com.a.a.e.p r4 = r0.eF
            int r4 = r4.getHeight()
            int r4 = 320 - r4
            r5 = 0
            r0 = r19
            r0.a(r2, r3, r4, r5)
        L_0x004d:
            return
        L_0x004e:
            r2 = 0
            r0 = r19
            r0.setColor(r2)
            r2 = 0
            r3 = 0
            int r4 = r18.getWidth()
            int r5 = r18.getHeight()
            r0 = r19
            r0.i(r2, r3, r4, r5)
            r2 = 16777215(0xffffff, float:2.3509886E-38)
            r0 = r19
            r0.setColor(r2)
            java.lang.String r2 = "是否退出"
            int r3 = r18.getWidth()
            com.a.a.e.l r4 = r19.cv()
            java.lang.String r5 = "是否退出"
            int r4 = r4.R(r5)
            int r3 = r3 - r4
            int r3 = r3 / 2
            int r4 = r18.getHeight()
            com.a.a.e.l r5 = r19.cv()
            int r5 = r5.getHeight()
            int r4 = r4 - r5
            int r4 = r4 / 2
            r5 = 0
            r0 = r19
            r0.a(r2, r3, r4, r5)
            java.lang.String r2 = "是"
            r3 = 0
            int r4 = r18.getHeight()
            com.a.a.e.l r5 = r19.cv()
            int r5 = r5.getHeight()
            int r4 = r4 - r5
            int r4 = r4 + -10
            r5 = 0
            r0 = r19
            r0.a(r2, r3, r4, r5)
            java.lang.String r2 = "否"
            int r3 = r18.getWidth()
            com.a.a.e.l r4 = r19.cv()
            java.lang.String r5 = "否"
            int r4 = r4.R(r5)
            int r3 = r3 - r4
            int r4 = r18.getHeight()
            com.a.a.e.l r5 = r19.cv()
            int r5 = r5.getHeight()
            int r4 = r4 - r5
            int r4 = r4 + -10
            r5 = 0
            r0 = r19
            r0.a(r2, r3, r4, r5)
            r2 = 58
            com.a.a.e.p r2 = defpackage.c.P(r2)
            r3 = 0
            r4 = 0
            r5 = 0
            r0 = r19
            r0.a(r2, r3, r4, r5)
            goto L_0x000c
        L_0x00e1:
            r2 = 0
            r0 = r19
            r0.setColor(r2)
            r2 = 0
            r3 = 0
            int r4 = r18.getWidth()
            int r5 = r18.getHeight()
            r0 = r19
            r0.i(r2, r3, r4, r5)
            r2 = 16777215(0xffffff, float:2.3509886E-38)
            r0 = r19
            r0.setColor(r2)
            r0 = r18
            int r2 = r0.o
            r3 = 3
            if (r2 != r3) goto L_0x01e8
            java.lang.String r2 = "你确定要访问"
            int r3 = r18.getWidth()
            com.a.a.e.l r4 = r19.cv()
            java.lang.String r5 = "你确定要访问"
            int r4 = r4.R(r5)
            int r3 = r3 - r4
            int r3 = r3 / 2
            int r4 = r18.getHeight()
            com.a.a.e.l r5 = r19.cv()
            int r5 = r5.getHeight()
            int r4 = r4 - r5
            int r4 = r4 / 2
            com.a.a.e.l r5 = r19.cv()
            int r5 = r5.getHeight()
            int r4 = r4 - r5
            int r4 = r4 + -5
            r5 = 0
            r0 = r19
            r0.a(r2, r3, r4, r5)
            java.lang.String r2 = "游戏频道并"
            int r3 = r18.getWidth()
            com.a.a.e.l r4 = r19.cv()
            java.lang.String r5 = "游戏频道并"
            int r4 = r4.R(r5)
            int r3 = r3 - r4
            int r3 = r3 / 2
            int r4 = r18.getHeight()
            com.a.a.e.l r5 = r19.cv()
            int r5 = r5.getHeight()
            int r4 = r4 - r5
            int r4 = r4 / 2
            r5 = 0
            r0 = r19
            r0.a(r2, r3, r4, r5)
            java.lang.String r2 = "退出游戏吗？"
            int r3 = r18.getWidth()
            com.a.a.e.l r4 = r19.cv()
            java.lang.String r5 = "退出游戏吗？"
            int r4 = r4.R(r5)
            int r3 = r3 - r4
            int r3 = r3 / 2
            int r4 = r18.getHeight()
            com.a.a.e.l r5 = r19.cv()
            int r5 = r5.getHeight()
            int r4 = r4 - r5
            int r4 = r4 / 2
            com.a.a.e.l r5 = r19.cv()
            int r5 = r5.getHeight()
            int r4 = r4 + r5
            int r4 = r4 + 5
            r5 = 0
            r0 = r19
            r0.a(r2, r3, r4, r5)
        L_0x0193:
            java.lang.String r2 = "确定"
            r3 = 0
            int r4 = r18.getHeight()
            com.a.a.e.l r5 = r19.cv()
            int r5 = r5.getHeight()
            int r4 = r4 - r5
            int r4 = r4 + -10
            r5 = 0
            r0 = r19
            r0.a(r2, r3, r4, r5)
            r0 = r18
            int r2 = r0.o
            r3 = 3
            if (r2 != r3) goto L_0x0278
            java.lang.String r2 = "返回"
            int r3 = r18.getWidth()
            com.a.a.e.l r4 = r19.cv()
            java.lang.String r5 = "返回"
            int r4 = r4.R(r5)
            int r3 = r3 - r4
            int r4 = r18.getHeight()
            com.a.a.e.l r5 = r19.cv()
            int r5 = r5.getHeight()
            int r4 = r4 - r5
            int r4 = r4 + -10
            r5 = 0
            r0 = r19
            r0.a(r2, r3, r4, r5)
        L_0x01d8:
            r2 = 59
            com.a.a.e.p r2 = defpackage.c.P(r2)
            r3 = 0
            r4 = 0
            r5 = 0
            r0 = r19
            r0.a(r2, r3, r4, r5)
            goto L_0x000c
        L_0x01e8:
            java.lang.String r2 = "更多精彩游戏，"
            int r3 = r18.getWidth()
            com.a.a.e.l r4 = r19.cv()
            java.lang.String r5 = "更多精彩游戏，"
            int r4 = r4.R(r5)
            int r3 = r3 - r4
            int r3 = r3 / 2
            int r4 = r18.getHeight()
            com.a.a.e.l r5 = r19.cv()
            int r5 = r5.getHeight()
            int r4 = r4 - r5
            int r4 = r4 / 2
            com.a.a.e.l r5 = r19.cv()
            int r5 = r5.getHeight()
            int r4 = r4 - r5
            int r4 = r4 + -5
            r5 = 0
            r0 = r19
            r0.a(r2, r3, r4, r5)
            java.lang.String r2 = "尽在游戏频道。"
            int r3 = r18.getWidth()
            com.a.a.e.l r4 = r19.cv()
            java.lang.String r5 = "尽在游戏频道。"
            int r4 = r4.R(r5)
            int r3 = r3 - r4
            int r3 = r3 / 2
            int r4 = r18.getHeight()
            com.a.a.e.l r5 = r19.cv()
            int r5 = r5.getHeight()
            int r4 = r4 - r5
            int r4 = r4 / 2
            r5 = 0
            r0 = r19
            r0.a(r2, r3, r4, r5)
            java.lang.String r2 = "wap.xjoys.com"
            int r3 = r18.getWidth()
            com.a.a.e.l r4 = r19.cv()
            java.lang.String r5 = "wap.xjoys.com"
            int r4 = r4.R(r5)
            int r3 = r3 - r4
            int r3 = r3 / 2
            int r4 = r18.getHeight()
            com.a.a.e.l r5 = r19.cv()
            int r5 = r5.getHeight()
            int r4 = r4 - r5
            int r4 = r4 / 2
            com.a.a.e.l r5 = r19.cv()
            int r5 = r5.getHeight()
            int r4 = r4 + r5
            int r4 = r4 + 5
            r5 = 0
            r0 = r19
            r0.a(r2, r3, r4, r5)
            goto L_0x0193
        L_0x0278:
            java.lang.String r2 = "退出"
            int r3 = r18.getWidth()
            com.a.a.e.l r4 = r19.cv()
            java.lang.String r5 = "退出"
            int r4 = r4.R(r5)
            int r3 = r3 - r4
            int r4 = r18.getHeight()
            com.a.a.e.l r5 = r19.cv()
            int r5 = r5.getHeight()
            int r4 = r4 - r5
            int r4 = r4 + -10
            r5 = 0
            r0 = r19
            r0.a(r2, r3, r4, r5)
            goto L_0x01d8
        L_0x02a0:
            r2 = 1
            r3 = 1
            r0 = r18
            r0.a(r2, r3)
            goto L_0x000c
        L_0x02a9:
            r2 = 0
            r0 = r19
            defpackage.c.a(r0, r2)
            r3 = 120(0x78, float:1.68E-43)
            r4 = 104(0x68, float:1.46E-43)
            r5 = 240(0xf0, float:3.36E-43)
            int r2 = defpackage.c.n
            int r6 = r2 + 4
            r7 = 0
            int[] r8 = defpackage.c.aF
            r9 = 1
            r10 = 8
            r11 = 8
            r12 = 3
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
            r2 = 16777215(0xffffff, float:2.3509886E-38)
            r0 = r19
            r0.setColor(r2)
            java.lang.String[] r2 = defpackage.c.cG
            r3 = 2
            r2 = r2[r3]
            r3 = 120(0x78, float:1.68E-43)
            r4 = 104(0x68, float:1.46E-43)
            r5 = 3
            r0 = r19
            defpackage.c.a(r0, r2, r3, r4, r5)
            r2 = 12
            r3 = 1
            com.a.a.e.p r3 = defpackage.c.P(r3)
            r4 = 320(0x140, float:4.48E-43)
            r0 = r18
            r1 = r19
            r0.a(r1, r2, r3, r4)
            r0 = r18
            int r2 = r0.q
            r0 = r18
            r0.b(r2)
            r0 = r18
            int r2 = r0.q
            boolean r2 = defpackage.c.I(r2)
            if (r2 != 0) goto L_0x0307
            boolean r2 = r18.P()
            if (r2 == 0) goto L_0x0313
        L_0x0307:
            r2 = 1
            defpackage.c.cs = r2
            r2 = 2
            r3 = 0
            r0 = r18
            r0.a(r2, r3)
            goto L_0x000c
        L_0x0313:
            r0 = r18
            int r2 = r0.q
            boolean r2 = defpackage.c.J(r2)
            if (r2 != 0) goto L_0x0323
            boolean r2 = r18.N()
            if (r2 == 0) goto L_0x000c
        L_0x0323:
            r2 = 0
            defpackage.c.cs = r2
            r2 = 2
            r3 = 0
            r0 = r18
            r0.a(r2, r3)
            goto L_0x000c
        L_0x032f:
            r2 = 0
            r0 = r18
            r0.eH = r2
            r2 = 0
            r0 = r19
            defpackage.c.a(r0, r2)
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 0
            r3 = r2[r3]
            r4 = 120(0x78, float:1.68E-43)
            r5 = 160(0xa0, float:2.24E-43)
            r6 = 0
            r7 = 3
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r0 = r18
            int r2 = r0.ey
            switch(r2) {
                case 100: goto L_0x03f0;
                case 101: goto L_0x0402;
                case 102: goto L_0x0414;
                case 103: goto L_0x0477;
                case 104: goto L_0x04ba;
                case 105: goto L_0x04d9;
                case 106: goto L_0x0508;
                case 107: goto L_0x0537;
                case 108: goto L_0x0566;
                case 109: goto L_0x0595;
                case 110: goto L_0x05c4;
                case 111: goto L_0x065b;
                case 112: goto L_0x06c8;
                default: goto L_0x0353;
            }
        L_0x0353:
            r0 = r18
            int r2 = r0.ey
            r3 = 104(0x68, float:1.46E-43)
            if (r2 >= r3) goto L_0x0378
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 1
            r3 = r2[r3]
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r4 = 1
            r2 = r2[r4]
            int r2 = r2.getWidth()
            int r4 = 240 - r2
            r5 = 160(0xa0, float:2.24E-43)
            r6 = 0
            r7 = 0
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
        L_0x0378:
            r18.e(r19)
            r2 = 0
            int r3 = defpackage.c.n
            int r3 = r3 * 2
            int r3 = 320 - r3
            int r3 = r3 + -2
            r0 = r18
            r1 = r19
            r0.a(r1, r2, r3)
            r2 = 0
            r3 = 0
            r4 = 240(0xf0, float:3.36E-43)
            r5 = 320(0x140, float:4.48E-43)
            r0 = r18
            boolean r2 = r0.a(r2, r3, r4, r5)
            if (r2 == 0) goto L_0x03aa
            r0 = r18
            int r2 = r0.j
            r0 = r18
            int r3 = r0.aG
            int r2 = r2 + r3
            if (r2 <= 0) goto L_0x03aa
            r2 = 53
            r0 = r18
            r0.q = r2
        L_0x03aa:
            r0 = r18
            int r2 = r0.q
            r0 = r18
            r0.s(r2)
            r0 = r18
            boolean r2 = r0.bM
            if (r2 != 0) goto L_0x000c
            r0 = r18
            int r2 = r0.ey
            r3 = 100
            if (r2 >= r3) goto L_0x000c
            r0 = r18
            int r2 = r0.ey
            int r2 = r2 + 1
            r0 = r18
            r0.ey = r2
            r0 = r18
            int r2 = r0.ey
            int r2 = r2 + -1
            r0 = r18
            java.lang.String[] r3 = r0.eD
            int r3 = r3.length
            if (r2 >= r3) goto L_0x0732
            java.lang.String r2 = ""
            r0 = r18
            java.lang.String[] r3 = r0.eD
            r0 = r18
            int r4 = r0.ey
            int r4 = r4 + -1
            r3 = r3[r4]
            r4 = 238(0xee, float:3.34E-43)
            r5 = 2
            r0 = r18
            r0.a(r2, r3, r4, r5)
            goto L_0x000c
        L_0x03f0:
            r2 = 2
            r0 = r18
            r0.B(r2)
            r0 = r18
            int r2 = r0.ey
            int r2 = r2 + 1
            r0 = r18
            r0.ey = r2
            goto L_0x0353
        L_0x0402:
            r0 = r18
            byte r2 = r0.bV
            if (r2 != 0) goto L_0x0353
            r0 = r18
            int r2 = r0.ey
            int r2 = r2 + 1
            r0 = r18
            r0.ey = r2
            goto L_0x0353
        L_0x0414:
            r0 = r18
            int r2 = r0.ez
            int r2 = r2 / 3
            r0 = r18
            com.a.a.e.p[] r3 = r0.eC
            r4 = 10
            r3 = r3[r4]
            r0 = r18
            int r4 = r0.eA
            int r4 = 240 - r4
            r5 = 140(0x8c, float:1.96E-43)
            r6 = 0
            r8 = 32
            r0 = r18
            com.a.a.e.p[] r7 = r0.eC
            r9 = 10
            r7 = r7[r9]
            int r9 = r7.getHeight()
            r7 = r19
            int r2 = defpackage.c.a(r2, r3, r4, r5, r6, r7, r8, r9)
            r0 = r18
            int r3 = r0.ez
            int r3 = r3 + 1
            r0 = r18
            r0.ez = r3
            r0 = r18
            int r3 = r0.ez
            int r3 = r3 / 3
            r4 = 4
            if (r3 >= r4) goto L_0x045c
            r0 = r18
            int r3 = r0.eA
            int r3 = r3 + 6
            r0 = r18
            r0.eA = r3
        L_0x045c:
            r0 = r18
            int r3 = r0.ez
            int r3 = r3 / 3
            r4 = 5
            if (r3 <= r4) goto L_0x0353
            r0 = r18
            int r3 = r0.ey
            int r3 = r3 + 1
            r0 = r18
            r0.ey = r3
            int r2 = r2 + -1
            r0 = r18
            r0.ez = r2
            goto L_0x0353
        L_0x0477:
            r2 = 6
            r0 = r18
            com.a.a.e.p[] r3 = r0.eC
            r4 = 10
            r3 = r3[r4]
            r0 = r18
            int r4 = r0.eA
            int r4 = 240 - r4
            r5 = 140(0x8c, float:1.96E-43)
            r6 = 0
            r8 = 32
            r0 = r18
            com.a.a.e.p[] r7 = r0.eC
            r9 = 10
            r7 = r7[r9]
            int r9 = r7.getHeight()
            r7 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7, r8, r9)
            r0 = r18
            int r2 = r0.bQ
            int r3 = r2 + 1
            r0 = r18
            r0.bQ = r3
            r3 = 5
            if (r2 <= r3) goto L_0x0353
            r0 = r18
            int r2 = r0.ey
            int r2 = r2 + 1
            r0 = r18
            r0.ey = r2
            r2 = 0
            r0 = r18
            r0.bQ = r2
            goto L_0x0353
        L_0x04ba:
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 2
            r3 = r2[r3]
            r4 = 240(0xf0, float:3.36E-43)
            r5 = 320(0x140, float:4.48E-43)
            r6 = 0
            r7 = 40
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r0 = r18
            int r2 = r0.ey
            int r2 = r2 + 1
            r0 = r18
            r0.ey = r2
            goto L_0x0353
        L_0x04d9:
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 3
            r3 = r2[r3]
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 2
            r3 = r2[r3]
            r4 = 240(0xf0, float:3.36E-43)
            r5 = 320(0x140, float:4.48E-43)
            r6 = 0
            r7 = 40
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r0 = r18
            int r2 = r0.ey
            int r2 = r2 + 1
            r0 = r18
            r0.ey = r2
            goto L_0x0353
        L_0x0508:
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 4
            r3 = r2[r3]
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 2
            r3 = r2[r3]
            r4 = 240(0xf0, float:3.36E-43)
            r5 = 320(0x140, float:4.48E-43)
            r6 = 0
            r7 = 40
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r0 = r18
            int r2 = r0.ey
            int r2 = r2 + 1
            r0 = r18
            r0.ey = r2
            goto L_0x0353
        L_0x0537:
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 3
            r3 = r2[r3]
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 2
            r3 = r2[r3]
            r4 = 240(0xf0, float:3.36E-43)
            r5 = 320(0x140, float:4.48E-43)
            r6 = 0
            r7 = 40
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r0 = r18
            int r2 = r0.ey
            int r2 = r2 + 1
            r0 = r18
            r0.ey = r2
            goto L_0x0353
        L_0x0566:
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 4
            r3 = r2[r3]
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 2
            r3 = r2[r3]
            r4 = 240(0xf0, float:3.36E-43)
            r5 = 320(0x140, float:4.48E-43)
            r6 = 0
            r7 = 40
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r0 = r18
            int r2 = r0.ey
            int r2 = r2 + 1
            r0 = r18
            r0.ey = r2
            goto L_0x0353
        L_0x0595:
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 3
            r3 = r2[r3]
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 2
            r3 = r2[r3]
            r4 = 240(0xf0, float:3.36E-43)
            r5 = 320(0x140, float:4.48E-43)
            r6 = 0
            r7 = 40
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r0 = r18
            int r2 = r0.ey
            int r2 = r2 + 1
            r0 = r18
            r0.ey = r2
            goto L_0x0353
        L_0x05c4:
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 4
            r3 = r2[r3]
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 5
            r3 = r2[r3]
            r0 = r18
            int r2 = r0.eB
            r0 = r18
            com.a.a.e.p[] r4 = r0.eC
            r5 = 5
            r4 = r4[r5]
            int r4 = r4.getWidth()
            int r4 = r2 - r4
            r5 = 0
            r6 = 0
            r7 = 0
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 2
            r3 = r2[r3]
            r4 = 240(0xf0, float:3.36E-43)
            r5 = 320(0x140, float:4.48E-43)
            r6 = 0
            r7 = 40
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r0 = r18
            int r2 = r0.eB
            int r2 = r2 + 10
            r0 = r18
            r0.eB = r2
            r0 = r18
            int r2 = r0.eB
            r0 = r18
            com.a.a.e.p[] r3 = r0.eC
            r4 = 5
            r3 = r3[r4]
            int r3 = r3.getWidth()
            if (r2 <= r3) goto L_0x0353
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 5
            r2 = r2[r3]
            int r2 = r2.getWidth()
            r0 = r18
            r0.eB = r2
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 6
            r3 = r2[r3]
            r4 = 0
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r5 = 5
            r2 = r2[r5]
            int r2 = r2.getHeight()
            int r2 = r2 / 2
            int r5 = r2 + 5
            r6 = 0
            r7 = 0
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r0 = r18
            int r2 = r0.ey
            int r2 = r2 + 1
            r0 = r18
            r0.ey = r2
            goto L_0x0353
        L_0x065b:
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 4
            r3 = r2[r3]
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 5
            r3 = r2[r3]
            r0 = r18
            int r2 = r0.eB
            r0 = r18
            com.a.a.e.p[] r4 = r0.eC
            r5 = 5
            r4 = r4[r5]
            int r4 = r4.getWidth()
            int r4 = r2 - r4
            r5 = 0
            r6 = 0
            r7 = 0
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 2
            r3 = r2[r3]
            r4 = 240(0xf0, float:3.36E-43)
            r5 = 320(0x140, float:4.48E-43)
            r6 = 0
            r7 = 40
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 7
            r3 = r2[r3]
            r4 = 0
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r5 = 5
            r2 = r2[r5]
            int r2 = r2.getHeight()
            int r2 = r2 / 2
            int r5 = r2 + 5
            r6 = 0
            r7 = 0
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r0 = r18
            int r2 = r0.ey
            int r2 = r2 + 1
            r0 = r18
            r0.ey = r2
            goto L_0x0353
        L_0x06c8:
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 4
            r3 = r2[r3]
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 5
            r3 = r2[r3]
            r0 = r18
            int r2 = r0.eB
            r0 = r18
            com.a.a.e.p[] r4 = r0.eC
            r5 = 5
            r4 = r4[r5]
            int r4 = r4.getWidth()
            int r4 = r2 - r4
            r5 = 0
            r6 = 0
            r7 = 0
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 2
            r3 = r2[r3]
            r4 = 240(0xf0, float:3.36E-43)
            r5 = 320(0x140, float:4.48E-43)
            r6 = 0
            r7 = 40
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r3 = 6
            r3 = r2[r3]
            r4 = 0
            r0 = r18
            com.a.a.e.p[] r2 = r0.eC
            r5 = 5
            r2 = r2[r5]
            int r2 = r2.getHeight()
            int r2 = r2 / 2
            int r5 = r2 + 5
            r6 = 0
            r7 = 0
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r2 = 2
            r3 = 1
            r0 = r18
            r0.a(r2, r3)
            goto L_0x0353
        L_0x0732:
            r2 = 100
            r0 = r18
            r0.ey = r2
            goto L_0x000c
        L_0x073a:
            r2 = 0
            r0 = r18
            r0.eH = r2
            r2 = 0
            r0 = r19
            defpackage.c.a(r0, r2)
            r18.j(r19)
            r2 = 1
            r3 = 1
            com.a.a.e.p r3 = defpackage.c.P(r3)
            r4 = 320(0x140, float:4.48E-43)
            r0 = r18
            r1 = r19
            r0.a(r1, r2, r3, r4)
            goto L_0x000c
        L_0x0759:
            r2 = 0
            r0 = r18
            r0.eH = r2
            r0 = r18
            e r2 = r0.bc
            java.lang.Thread r2 = r2.eI
            if (r2 != 0) goto L_0x076d
            r0 = r18
            e r2 = r0.bc
            r2.a()
        L_0x076d:
            r18.b(r19)
            r0 = r18
            boolean r2 = r0.aY
            if (r2 != 0) goto L_0x000c
            r0 = r18
            int r2 = r0.ba
            r0 = r18
            r0.n(r2)
            int r3 = defpackage.c.cv
            int r2 = defpackage.c.cv
            if (r2 != 0) goto L_0x0790
            r2 = -1
        L_0x0786:
            defpackage.c.c(r3, r2)
            r2 = 0
            r0 = r18
            r0.bc = r2
            goto L_0x000c
        L_0x0790:
            r2 = 1
            goto L_0x0786
        L_0x0792:
            r2 = 0
            r0 = r18
            r0.eH = r2
            r2 = 0
            r0 = r19
            defpackage.c.a(r0, r2)
            r3 = 120(0x78, float:1.68E-43)
            r4 = 104(0x68, float:1.46E-43)
            r5 = 240(0xf0, float:3.36E-43)
            int r2 = defpackage.c.n
            int r6 = r2 + 4
            r7 = 0
            int[] r8 = defpackage.c.aF
            r9 = 1
            r10 = 8
            r11 = 8
            r12 = 3
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
            r2 = 10066329(0x999999, float:1.4105931E-38)
            r0 = r19
            r0.setColor(r2)
            java.lang.String[] r2 = defpackage.c.cG
            r3 = 3
            r2 = r2[r3]
            int r3 = defpackage.c.cs
            r4 = 1
            if (r3 != r4) goto L_0x07d4
            r2 = 16777215(0xffffff, float:2.3509886E-38)
            r0 = r19
            r0.setColor(r2)
            java.lang.String[] r2 = defpackage.c.cG
            r3 = 2
            r2 = r2[r3]
        L_0x07d4:
            r3 = 120(0x78, float:1.68E-43)
            r4 = 104(0x68, float:1.46E-43)
            r5 = 3
            r0 = r19
            defpackage.c.a(r0, r2, r3, r4, r5)
            r2 = 3
            r3 = 1
            com.a.a.e.p r3 = defpackage.c.P(r3)
            r4 = 320(0x140, float:4.48E-43)
            r0 = r18
            r1 = r19
            r0.a(r1, r2, r3, r4)
            r0 = r18
            int r2 = r0.q
            boolean r2 = defpackage.c.I(r2)
            if (r2 != 0) goto L_0x0804
            r0 = r18
            int r2 = r0.q
            r3 = 6
            if (r2 == r3) goto L_0x0804
            boolean r2 = r18.P()
            if (r2 == 0) goto L_0x080f
        L_0x0804:
            int r2 = defpackage.c.cs
            r2 = r2 ^ 1
            defpackage.c.cs = r2
            r18.ab()
            goto L_0x000c
        L_0x080f:
            r0 = r18
            int r2 = r0.q
            r3 = 7
            if (r2 == r3) goto L_0x081c
            boolean r2 = r18.N()
            if (r2 == 0) goto L_0x000c
        L_0x081c:
            r2 = 0
            r0 = r18
            r0.o = r2
            r2 = 2
            r0 = r18
            r0.n(r2)
            r18.ab()
            goto L_0x000c
        L_0x082c:
            r2 = 0
            r0 = r18
            r0.eH = r2
            r2 = 1464650(0x16594a, float:2.052412E-39)
            r0 = r19
            defpackage.c.a(r0, r2)
            r2 = 49
            com.a.a.e.p r4 = defpackage.c.P(r2)
            r2 = 48
            com.a.a.e.p r5 = defpackage.c.P(r2)
            r2 = 47
            com.a.a.e.p r6 = defpackage.c.P(r2)
            r7 = 16711680(0xff0000, float:2.3418052E-38)
            r2 = r18
            r3 = r19
            r2.a(r3, r4, r5, r6, r7)
            r2 = 2
            r3 = 1
            com.a.a.e.p r3 = defpackage.c.P(r3)
            r4 = 320(0x140, float:4.48E-43)
            r0 = r18
            r1 = r19
            r0.a(r1, r2, r3, r4)
            boolean r2 = r18.N()
            if (r2 == 0) goto L_0x086e
            r2 = 7
            r0 = r18
            r0.q = r2
        L_0x086e:
            r0 = r18
            int r2 = r0.q
            r0 = r18
            r0.H(r2)
            goto L_0x000c
        L_0x0879:
            r2 = 0
            r0 = r18
            r0.eH = r2
            r0 = r18
            java.lang.String[] r2 = r0.ev
            int r3 = defpackage.c.u
            r4 = 8
            if (r3 != r4) goto L_0x088c
            r0 = r18
            java.lang.String[] r2 = r0.ew
        L_0x088c:
            r3 = 0
            r4 = 0
            r5 = 240(0xf0, float:3.36E-43)
            r6 = 320(0x140, float:4.48E-43)
            r0 = r19
            r0.j(r3, r4, r5, r6)
            int r3 = r2.length
            int r3 = r3 + 1
            int r4 = defpackage.c.n
            int r4 = r4 * r3
            r3 = 102080(0x18ec0, float:1.43045E-40)
            int r5 = r3 / r4
            int r6 = defpackage.c.n
            r3 = 0
            r0 = r19
            defpackage.c.a(r0, r3)
            r3 = 16777215(0xffffff, float:2.3509886E-38)
            r0 = r19
            r0.setColor(r3)
            r3 = 0
        L_0x08b3:
            int r7 = r2.length
            if (r3 < r7) goto L_0x094e
            int r2 = r2.length
            int r3 = defpackage.c.n
            int r2 = r2 * r3
            r3 = 319(0x13f, float:4.47E-43)
            if (r2 <= r3) goto L_0x0906
            r2 = 16711680(0xff0000, float:2.3418052E-38)
            r0 = r19
            r0.setColor(r2)
            r2 = 238(0xee, float:3.34E-43)
            r3 = 0
            r7 = 1
            r8 = 320(0x140, float:4.48E-43)
            r0 = r19
            r0.i(r2, r3, r7, r8)
            r2 = 255(0xff, float:3.57E-43)
            r0 = r19
            r0.setColor(r2)
            r2 = 238(0xee, float:3.34E-43)
            r0 = r18
            int r3 = r0.bo
            int r3 = -r3
            int r3 = r3 * 319
            int r3 = r3 / r4
            int r3 = r3 + 0
            int r7 = r5 >> 1
            int r3 = r3 + r7
            r7 = 1
            r0 = r19
            r0.i(r2, r3, r7, r5)
            r2 = 16776960(0xffff00, float:2.3509528E-38)
            r0 = r19
            r0.setColor(r2)
            r2 = 238(0xee, float:3.34E-43)
            r0 = r18
            int r3 = r0.bo
            int r3 = -r3
            int r3 = r3 * 319
            int r3 = r3 / r4
            int r3 = r3 + 0
            r7 = 1
            r0 = r19
            r0.i(r2, r3, r7, r5)
        L_0x0906:
            r2 = 0
            r3 = 0
            r5 = 240(0xf0, float:3.36E-43)
            r7 = 340(0x154, float:4.76E-43)
            r0 = r19
            r0.j(r2, r3, r5, r7)
            r2 = 2
            r3 = 1
            com.a.a.e.p r3 = defpackage.c.P(r3)
            r5 = 320(0x140, float:4.48E-43)
            r0 = r18
            r1 = r19
            r0.a(r1, r2, r3, r5)
            boolean r2 = r18.N()
            if (r2 == 0) goto L_0x092b
            r2 = 7
            r0 = r18
            r0.q = r2
        L_0x092b:
            r0 = r18
            int r2 = r0.q
            switch(r2) {
                case 1: goto L_0x0934;
                case 2: goto L_0x0971;
                case 7: goto L_0x0963;
                case 50: goto L_0x0934;
                case 56: goto L_0x0971;
                default: goto L_0x0932;
            }
        L_0x0932:
            goto L_0x000c
        L_0x0934:
            r0 = r18
            int r2 = r0.bo
            int r2 = r2 + r6
            r0 = r18
            r0.bo = r2
            r0 = r18
            int r2 = r0.bo
            if (r2 <= 0) goto L_0x000c
            r0 = r18
            int r2 = r0.bo
            int r2 = r2 - r6
            r0 = r18
            r0.bo = r2
            goto L_0x000c
        L_0x094e:
            r7 = r2[r3]
            r8 = 0
            r0 = r18
            int r9 = r0.bo
            int r10 = defpackage.c.n
            int r10 = r10 * r3
            int r9 = r9 + r10
            r10 = 0
            r0 = r19
            r0.a(r7, r8, r9, r10)
            int r3 = r3 + 1
            goto L_0x08b3
        L_0x0963:
            r18.aw()
            r2 = 2
            r0 = r18
            r0.n(r2)
            r18.ab()
            goto L_0x000c
        L_0x0971:
            r0 = r18
            int r2 = r0.bo
            int r2 = r2 - r6
            r0 = r18
            r0.bo = r2
            r0 = r18
            int r2 = r0.bo
            int r2 = java.lang.Math.abs(r2)
            int r3 = r4 + -319
            if (r2 <= r3) goto L_0x000c
            r0 = r18
            int r2 = r0.bo
            int r2 = r2 + r6
            r0 = r18
            r0.bo = r2
            goto L_0x000c
        L_0x0991:
            r2 = 0
            r0 = r18
            r0.eH = r2
            r0 = r18
            int r2 = r0.bf
            if (r2 != 0) goto L_0x09e0
            java.lang.String[] r2 = defpackage.c.cG
            r3 = 19
            r2 = r2[r3]
            r0 = r18
            r0.be = r2
        L_0x09a6:
            r0 = r18
            int r2 = r0.bf
            int r2 = r2 + 1
            r0 = r18
            r0.bf = r2
            r3 = 120(0x78, float:1.68E-43)
            r4 = 104(0x68, float:1.46E-43)
            r5 = 240(0xf0, float:3.36E-43)
            int r2 = defpackage.c.n
            int r6 = r2 + 4
            r7 = 0
            int[] r8 = defpackage.c.aF
            r9 = 1
            r10 = 8
            r11 = 8
            r12 = 3
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
            r2 = 16777215(0xffffff, float:2.3509886E-38)
            r0 = r19
            r0.setColor(r2)
            r0 = r18
            java.lang.String r2 = r0.be
            r3 = 120(0x78, float:1.68E-43)
            r4 = 104(0x68, float:1.46E-43)
            r5 = 3
            r0 = r19
            defpackage.c.a(r0, r2, r3, r4, r5)
            goto L_0x000c
        L_0x09e0:
            r0 = r18
            int r2 = r0.bf
            r3 = 2
            if (r2 != r3) goto L_0x09eb
            r18.l()
            goto L_0x09a6
        L_0x09eb:
            r0 = r18
            int r2 = r0.bf
            r3 = 30
            if (r2 <= r3) goto L_0x09a6
            r2 = 0
            r0 = r19
            defpackage.c.a(r0, r2)
            r2 = 0
            r0 = r18
            r0.bf = r2
            r2 = 3
            r0 = r18
            r0.n(r2)
            r18.U()
            goto L_0x09a6
        L_0x0a08:
            r2 = 0
            r0 = r18
            r0.eH = r2
            r2 = 0
            r0 = r19
            defpackage.c.a(r0, r2)
            r3 = 120(0x78, float:1.68E-43)
            r4 = 104(0x68, float:1.46E-43)
            r5 = 240(0xf0, float:3.36E-43)
            int r2 = defpackage.c.n
            int r6 = r2 + 4
            r7 = 0
            int[] r8 = defpackage.c.aF
            r9 = 1
            r10 = 8
            r11 = 8
            r12 = 3
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
            r2 = 16777215(0xffffff, float:2.3509886E-38)
            r0 = r19
            r0.setColor(r2)
            java.lang.String[] r2 = defpackage.c.cG
            r3 = 7
            r2 = r2[r3]
            r3 = 120(0x78, float:1.68E-43)
            r4 = 104(0x68, float:1.46E-43)
            r5 = 3
            r0 = r19
            defpackage.c.a(r0, r2, r3, r4, r5)
            r2 = 3
            r3 = 1
            com.a.a.e.p r3 = defpackage.c.P(r3)
            r4 = 320(0x140, float:4.48E-43)
            r0 = r18
            r1 = r19
            r0.a(r1, r2, r3, r4)
            goto L_0x000c
        L_0x0a53:
            r2 = 0
            r0 = r18
            r0.eH = r2
            r2 = 0
            r0 = r19
            defpackage.c.a(r0, r2)
            boolean r2 = defpackage.c.aM
            if (r2 != 0) goto L_0x0bae
            java.lang.String[] r13 = defpackage.c.cG
            r2 = 4
            java.lang.String[] r14 = new java.lang.String[r2]
            r2 = 0
            r3 = 13
            r3 = r13[r3]
            r14[r2] = r3
            r3 = 1
            int r2 = defpackage.c.cs
            r4 = 1
            if (r2 != r4) goto L_0x0ae8
            r2 = 2
            r2 = r13[r2]
        L_0x0a77:
            r14[r3] = r2
            r2 = 2
            r3 = 14
            r3 = r13[r3]
            r14[r2] = r3
            r2 = 3
            r3 = 15
            r3 = r13[r3]
            r14[r2] = r3
            int r2 = r14.length
            int r3 = defpackage.c.n
            int r2 = r2 * r3
            int r2 = 320 - r2
            int r4 = r2 >> 1
            r3 = 120(0x78, float:1.68E-43)
            r5 = 240(0xf0, float:3.36E-43)
            int r2 = r14.length
            int r6 = defpackage.c.n
            int r2 = r2 * r6
            int r6 = r2 + 8
            r7 = 0
            int[] r8 = defpackage.c.aF
            r9 = 1
            r10 = 8
            r11 = 8
            r12 = 17
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
            r2 = 16777215(0xffffff, float:2.3509886E-38)
            r0 = r19
            r0.setColor(r2)
            r2 = 0
        L_0x0ab1:
            int r3 = r14.length
            if (r2 < r3) goto L_0x0aec
            r2 = 3
            r3 = 1
            com.a.a.e.p r3 = defpackage.c.P(r3)
            r4 = 320(0x140, float:4.48E-43)
            r0 = r18
            r1 = r19
            r0.a(r1, r2, r3, r4)
            r0 = r18
            int r2 = r0.q
            int r3 = r14.length
            int r3 = r3 + -1
            r4 = 1
            r0 = r18
            r0.a(r2, r3, r4)
            r0 = r18
            int r2 = r0.q
            boolean r2 = defpackage.c.J(r2)
            if (r2 != 0) goto L_0x0ae0
            boolean r2 = r18.N()
            if (r2 == 0) goto L_0x0b6c
        L_0x0ae0:
            r2 = 3
            defpackage.c.u = r2
            r18.ab()
            goto L_0x000c
        L_0x0ae8:
            r2 = 3
            r2 = r13[r2]
            goto L_0x0a77
        L_0x0aec:
            r3 = r14[r2]
            r5 = 3
            r5 = r13[r5]
            if (r3 != r5) goto L_0x0b63
            r3 = 10066329(0x999999, float:1.4105931E-38)
            r0 = r19
            r0.setColor(r3)
        L_0x0afb:
            r3 = r14[r2]
            int r3 = defpackage.c.a(r3)
            int r3 = 240 - r3
            int r3 = r3 >> 1
            int r5 = defpackage.c.n
            int r5 = r5 * r2
            int r5 = r5 + r4
            int r5 = r5 + 4
            r6 = r14[r2]
            r7 = 0
            r0 = r19
            defpackage.c.a(r0, r6, r3, r5, r7)
            r6 = 240(0xf0, float:3.36E-43)
            int r7 = defpackage.c.n
            r0 = r18
            boolean r5 = r0.a(r3, r5, r6, r7)
            if (r5 == 0) goto L_0x0b28
            r0 = r18
            r0.o = r2
            r5 = 5
            r0 = r18
            r0.q = r5
        L_0x0b28:
            r0 = r18
            int r5 = r0.o
            if (r2 != r5) goto L_0x0b5f
            r5 = 1
            com.a.a.e.p r7 = defpackage.c.P(r5)
            r5 = 1
            com.a.a.e.p r5 = defpackage.c.P(r5)
            int r5 = r5.getHeight()
            int r3 = r3 - r5
            int r3 = r3 + 2
            r0 = r18
            byte r5 = r0.aO
            int r5 = r5 % 4
            int r8 = r3 - r5
            int r3 = r4 + -4
            r0 = r18
            int r5 = r0.o
            int r6 = defpackage.c.n
            int r5 = r5 * r6
            int r3 = r3 + r5
            int r5 = defpackage.c.n
            int r5 = r5 >> 1
            int r9 = r3 + r5
            r10 = 0
            r5 = r18
            r6 = r19
            r5.a(r6, r7, r8, r9, r10)
        L_0x0b5f:
            int r2 = r2 + 1
            goto L_0x0ab1
        L_0x0b63:
            r3 = 16777215(0xffffff, float:2.3509886E-38)
            r0 = r19
            r0.setColor(r3)
            goto L_0x0afb
        L_0x0b6c:
            r0 = r18
            int r2 = r0.q
            boolean r2 = defpackage.c.I(r2)
            if (r2 != 0) goto L_0x0b7c
            boolean r2 = r18.P()
            if (r2 == 0) goto L_0x000c
        L_0x0b7c:
            r0 = r18
            int r2 = r0.o
            switch(r2) {
                case 0: goto L_0x0b8a;
                case 1: goto L_0x0b91;
                case 2: goto L_0x0ba2;
                case 3: goto L_0x0baa;
                default: goto L_0x0b83;
            }
        L_0x0b83:
            r2 = 0
            r0 = r18
            r0.q = r2
            goto L_0x000c
        L_0x0b8a:
            r2 = 3
            defpackage.c.u = r2
            r18.ab()
            goto L_0x0b83
        L_0x0b91:
            int r2 = defpackage.c.cs
            r3 = 1
            if (r2 != r3) goto L_0x0b9a
            r2 = 0
            defpackage.c.cs = r2
            goto L_0x0b83
        L_0x0b9a:
            int r2 = defpackage.c.cs
            if (r2 != 0) goto L_0x0b83
            r2 = 1
            defpackage.c.cs = r2
            goto L_0x0b83
        L_0x0ba2:
            r2 = 2
            r3 = 0
            r0 = r18
            r0.a(r2, r3)
            goto L_0x0b83
        L_0x0baa:
            r2 = 1
            defpackage.c.aM = r2
            goto L_0x0b83
        L_0x0bae:
            r2 = 0
            r0 = r19
            defpackage.c.a(r0, r2)
            r3 = 120(0x78, float:1.68E-43)
            r4 = 104(0x68, float:1.46E-43)
            r5 = 240(0xf0, float:3.36E-43)
            int r2 = defpackage.c.n
            int r6 = r2 + 4
            r7 = 0
            int[] r8 = defpackage.c.aF
            r9 = 1
            r10 = 8
            r11 = 8
            r12 = 3
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
            r2 = 16777215(0xffffff, float:2.3509886E-38)
            r0 = r19
            r0.setColor(r2)
            java.lang.String[] r2 = defpackage.c.cG
            r3 = 15
            r2 = r2[r3]
            r3 = 120(0x78, float:1.68E-43)
            r4 = 104(0x68, float:1.46E-43)
            r5 = 3
            r0 = r19
            defpackage.c.a(r0, r2, r3, r4, r5)
            r2 = 9
            r3 = 1
            com.a.a.e.p r3 = defpackage.c.P(r3)
            r4 = 320(0x140, float:4.48E-43)
            r0 = r18
            r1 = r19
            r0.a(r1, r2, r3, r4)
            r0 = r18
            int r2 = r0.q
            boolean r2 = defpackage.c.I(r2)
            if (r2 != 0) goto L_0x0c04
            boolean r2 = r18.P()
            if (r2 == 0) goto L_0x0c13
        L_0x0c04:
            r2 = 0
            defpackage.c.aM = r2
            r2 = 0
            r0 = r18
            r0.q = r2
            Mi r2 = defpackage.Mi.Y
            r2.a()
            goto L_0x000c
        L_0x0c13:
            r0 = r18
            int r2 = r0.q
            if (r2 != 0) goto L_0x0c1f
            boolean r2 = r18.N()
            if (r2 == 0) goto L_0x000c
        L_0x0c1f:
            r2 = 0
            defpackage.c.aM = r2
            goto L_0x000c
        L_0x0c24:
            r2 = 0
            r0 = r18
            r0.eH = r2
            r2 = 0
            r0 = r19
            defpackage.c.a(r0, r2)
            r2 = 42
            com.a.a.e.p r3 = defpackage.c.P(r2)
            r4 = 120(0x78, float:1.68E-43)
            r5 = 104(0x68, float:1.46E-43)
            r6 = 0
            r7 = 3
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r2 = 1
            r3 = 1
            com.a.a.e.p r3 = defpackage.c.P(r3)
            r4 = 320(0x140, float:4.48E-43)
            r0 = r18
            r1 = r19
            r0.a(r1, r2, r3, r4)
            r0 = r18
            int r2 = r0.q
            boolean r2 = defpackage.c.K(r2)
            if (r2 != 0) goto L_0x0c5f
            boolean r2 = r18.P()
            if (r2 == 0) goto L_0x0c68
        L_0x0c5f:
            r2 = 6
            r3 = 1
            r0 = r18
            r0.a(r2, r3)
            goto L_0x000c
        L_0x0c68:
            r0 = r18
            int r2 = r0.q
            if (r2 == 0) goto L_0x000c
            r2 = 6
            r3 = 1
            r0 = r18
            r0.a(r2, r3)
            goto L_0x000c
        L_0x0c77:
            r2 = 0
            r0 = r18
            r0.eH = r2
            r2 = 0
            r0 = r19
            defpackage.c.a(r0, r2)
            r2 = 43
            com.a.a.e.p r3 = defpackage.c.P(r2)
            r4 = 120(0x78, float:1.68E-43)
            r5 = 104(0x68, float:1.46E-43)
            r6 = 0
            r7 = 3
            r2 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7)
            r2 = 1
            r3 = 1
            com.a.a.e.p r3 = defpackage.c.P(r3)
            r4 = 320(0x140, float:4.48E-43)
            r0 = r18
            r1 = r19
            r0.a(r1, r2, r3, r4)
            r0 = r18
            int r2 = r0.q
            if (r2 != 0) goto L_0x0cae
            boolean r2 = r18.P()
            if (r2 == 0) goto L_0x000c
        L_0x0cae:
            r2 = 2
            r3 = 0
            r0 = r18
            r0.a(r2, r3)
            goto L_0x000c
        L_0x0cb7:
            r2 = 1
            r0 = r18
            r0.eH = r2
            com.a.a.e.l r2 = defpackage.c.aH
            r0 = r19
            r0.a(r2)
            r2 = 0
            r3 = 0
            r4 = 240(0xf0, float:3.36E-43)
            r5 = 320(0x140, float:4.48E-43)
            r0 = r19
            r0.j(r2, r3, r4, r5)
            r0 = r18
            boolean r2 = r0.eH
            if (r2 == 0) goto L_0x0cfe
            r0 = r18
            com.a.a.e.p r2 = r0.eE
            if (r2 != 0) goto L_0x0ce4
            java.lang.String r2 = "bt.png"
            com.a.a.e.p r2 = defpackage.c.m(r2)
            r0 = r18
            r0.eE = r2
        L_0x0ce4:
            r0 = r18
            com.a.a.e.p r2 = r0.eE
            r3 = 0
            r4 = 0
            r5 = 0
            r0 = r19
            r0.a(r2, r3, r4, r5)
            r2 = 0
            r0 = r18
            com.a.a.e.p r3 = r0.eE
            int r3 = r3.getHeight()
            r0 = r19
            r0.translate(r2, r3)
        L_0x0cfe:
            r18.X()
            r18.Z()
            r18.W()
            r18.Y()
            r0 = r18
            a r2 = r0.aQ
            r0 = r18
            r0.e(r2)
            r3 = 0
            b r14 = new b
            r14.<init>()
            r0 = r18
            a r2 = r0.aQ
            r14.c(r2)
            r2 = 1
        L_0x0d21:
            int r4 = r18.Q()
            if (r2 < r4) goto L_0x0e14
            r18.p()
            r18.f(r19)
            int r15 = r14.aA
            int[] r4 = new int[r15]
            int[] r0 = new int[r15]
            r16 = r0
            r2 = 0
            r3 = r2
        L_0x0d37:
            if (r3 < r15) goto L_0x0e3f
            r2 = 0
            r3 = r4[r2]
            int r3 = r3 + 1
            r4[r2] = r3
            r0 = r18
            r1 = r16
            r0.a(r4, r1)
            r2 = 0
            r13 = r2
        L_0x0d49:
            if (r13 < r15) goto L_0x0e54
            r0 = r18
            a r2 = r0.aQ
            r3 = 0
            r4 = 19
            com.a.a.e.p r4 = defpackage.c.P(r4)
            int r4 = r4.getHeight()
            int r4 = 208 - r4
            r0 = r18
            r1 = r19
            r0.a(r1, r2, r3, r4)
            r2 = 3
            com.a.a.e.p r2 = defpackage.c.P(r2)
            r0 = r18
            int r3 = r0.cC
            java.lang.String r3 = java.lang.Integer.toString(r3)
            r4 = 10
            r5 = 10
            r6 = 0
            r8 = 10
            r9 = 10
            r10 = 1
            r11 = 6
            r7 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11)
            r0 = r18
            a r2 = r0.aW
            if (r2 == 0) goto L_0x0dfb
            r2 = 32
            int r8 = defpackage.c.O(r2)
            r2 = 32
            int r9 = defpackage.c.O(r2)
            r0 = r18
            a r2 = r0.aW
            short r2 = r2.aw
            byte r2 = defpackage.c.a(r2)
            r3 = 15
            if (r2 != r3) goto L_0x0da8
            r0 = r18
            int r2 = r0.I
            int r2 = r2 % 3
            if (r2 == 0) goto L_0x0de3
        L_0x0da8:
            r0 = r18
            a r2 = r0.aW
            int r2 = r2.g
            r3 = 32
            com.a.a.e.p r3 = defpackage.c.P(r3)
            r4 = 174(0xae, float:2.44E-43)
            r5 = 5
            r6 = 0
            r7 = r19
            defpackage.c.a(r2, r3, r4, r5, r6, r7, r8, r9)
            r11 = 173(0xad, float:2.42E-43)
            r12 = 4
            int r13 = r8 + 2
            int r14 = r9 + 2
            r15 = 16777215(0xffffff, float:2.3509886E-38)
            r16 = 0
            r17 = 0
            r10 = r19
            defpackage.c.a(r10, r11, r12, r13, r14, r15, r16, r17)
            r0 = r18
            a r2 = r0.aW
            int r3 = r8 + 174
            int r3 = r3 + 4
            int r4 = r9 + 5
            int r4 = r4 + -5
            r0 = r18
            r1 = r19
            r0.a(r1, r2, r3, r4)
        L_0x0de3:
            r0 = r18
            int r2 = r0.I
            int r3 = r2 + 1
            r0 = r18
            r0.I = r3
            r3 = 30
            if (r2 <= r3) goto L_0x0dfb
            r2 = 0
            r0 = r18
            r0.aW = r2
            r2 = 0
            r0 = r18
            r0.I = r2
        L_0x0dfb:
            r18.h(r19)
            r18.e(r19)
            r2 = 0
            int r3 = defpackage.c.n
            int r3 = r3 * 3
            int r3 = 208 - r3
            r0 = r18
            r1 = r19
            r0.a(r1, r2, r3)
            r18.g(r19)
            goto L_0x000c
        L_0x0e14:
            r0 = r18
            a r4 = r0.w(r2)
            int r5 = r4.ao
            boolean r5 = defpackage.c.t(r5)
            if (r5 == 0) goto L_0x0e36
            r0 = r18
            int r5 = r0.cJ
            if (r3 >= r5) goto L_0x0e32
            r0 = r18
            r0.e(r4)
            r14.c(r4)
            int r3 = r3 + 1
        L_0x0e32:
            int r2 = r2 + 1
            goto L_0x0d21
        L_0x0e36:
            r0 = r18
            r0.e(r4)
            r14.c(r4)
            goto L_0x0e32
        L_0x0e3f:
            java.lang.Object r2 = r14.k(r3)
            a r2 = (defpackage.a) r2
            int r5 = r2.z
            int r2 = r2.h
            int r2 = r5 << r2
            r4[r3] = r2
            r16[r3] = r3
            int r2 = r3 + 1
            r3 = r2
            goto L_0x0d37
        L_0x0e54:
            r2 = r16[r13]
            java.lang.Object r4 = r14.k(r2)
            a r4 = (defpackage.a) r4
            int r2 = r4.ao
            switch(r2) {
                case 0: goto L_0x0e8e;
                case 40: goto L_0x0e68;
                default: goto L_0x0e61;
            }
        L_0x0e61:
            if (r4 != 0) goto L_0x0f4e
        L_0x0e63:
            int r2 = r13 + 1
            r13 = r2
            goto L_0x0d49
        L_0x0e68:
            int r2 = r4.x
            int r3 = defpackage.c.bl
            int r5 = r2 - r3
            int r2 = r4.y
            a r3 = r4.ab
            int r3 = r3.F
            int r3 = r3 >> 1
            int r2 = r2 - r3
            int r3 = defpackage.c.bm
            int r6 = r2 - r3
            r7 = 0
            int r2 = r4.ay
            r2 = r2 & 1
            if (r2 == 0) goto L_0x0e8c
            r8 = 1
        L_0x0e83:
            r9 = 0
            r2 = r18
            r3 = r19
            r2.a(r3, r4, r5, r6, r7, r8, r9)
            goto L_0x0e63
        L_0x0e8c:
            r8 = 0
            goto L_0x0e83
        L_0x0e8e:
            boolean r2 = r4.k
            if (r2 != 0) goto L_0x0ebd
            r2 = 3026478(0x2e2e2e, float:4.240999E-39)
            r0 = r19
            r0.setColor(r2)
            int r2 = r4.x
            int r3 = r4.E
            int r3 = r3 >> 1
            int r2 = r2 - r3
            int r3 = defpackage.c.bl
            int r6 = r2 - r3
            int r2 = r4.z
            int r3 = r4.G
            int r3 = r3 >> 1
            int r2 = r2 - r3
            int r3 = defpackage.c.bm
            int r7 = r2 - r3
            int r8 = r4.E
            int r9 = r4.G
            r10 = 8
            r11 = 8
            r5 = r19
            r5.e(r6, r7, r8, r9, r10, r11)
        L_0x0ebd:
            r0 = r18
            boolean r2 = r0.aS
            if (r2 == 0) goto L_0x0f28
            r0 = r18
            int r2 = r0.F
            int r3 = r2 + -1
            r0 = r18
            r0.F = r3
            int r2 = r2 % 3
            if (r2 != 0) goto L_0x0f28
            boolean r2 = defpackage.c.O()
            if (r2 != 0) goto L_0x0eed
            r0 = r18
            int r2 = r0.F
            if (r2 > 0) goto L_0x0eed
            r0 = r18
            boolean r2 = r0.aS
            if (r2 == 0) goto L_0x0f26
            r2 = 0
        L_0x0ee4:
            r0 = r18
            r0.aS = r2
            r2 = 0
            r0 = r18
            r0.F = r2
        L_0x0eed:
            a r2 = r4.ac
            if (r2 == 0) goto L_0x0e63
            a r7 = r4.ac
            int r2 = r4.x
            int r3 = defpackage.c.bl
            int r8 = r2 - r3
            a r2 = r4.ac
            int r2 = r2.y
            int r2 = r2 + 15
            int r3 = defpackage.c.bm
            int r9 = r2 - r3
            r10 = 0
            int r2 = r4.ay
            r2 = r2 & 1
            if (r2 == 0) goto L_0x0f4c
            r11 = 1
        L_0x0f0b:
            r12 = 0
            r5 = r18
            r6 = r19
            r5.a(r6, r7, r8, r9, r10, r11, r12)
            a r2 = r4.ac
            boolean r2 = r2.as
            if (r2 == 0) goto L_0x0e63
            a r2 = r4.ac
            boolean r2 = r2.av
            if (r2 != 0) goto L_0x0e63
            a r2 = r4.ac
            r3 = 1
            r2.av = r3
            goto L_0x0e63
        L_0x0f26:
            r2 = 1
            goto L_0x0ee4
        L_0x0f28:
            int r2 = r4.x
            int r3 = defpackage.c.bl
            int r5 = r2 - r3
            int r2 = r4.y
            int r3 = r4.G
            int r3 = r3 >> 1
            int r2 = r2 + r3
            int r3 = defpackage.c.bm
            int r6 = r2 - r3
            r7 = 0
            int r2 = r4.ay
            r2 = r2 & 1
            if (r2 == 0) goto L_0x0f4a
            r8 = 1
        L_0x0f41:
            r9 = 0
            r2 = r18
            r3 = r19
            r2.a(r3, r4, r5, r6, r7, r8, r9)
            goto L_0x0eed
        L_0x0f4a:
            r8 = 0
            goto L_0x0f41
        L_0x0f4c:
            r11 = 0
            goto L_0x0f0b
        L_0x0f4e:
            boolean r2 = r4.k
            if (r2 != 0) goto L_0x0f7d
            r2 = 3026478(0x2e2e2e, float:4.240999E-39)
            r0 = r19
            r0.setColor(r2)
            int r2 = r4.x
            int r3 = r4.E
            int r3 = r3 >> 1
            int r2 = r2 - r3
            int r3 = defpackage.c.bl
            int r6 = r2 - r3
            int r2 = r4.z
            int r3 = r4.G
            int r3 = r3 >> 1
            int r2 = r2 - r3
            int r3 = defpackage.c.bm
            int r7 = r2 - r3
            int r8 = r4.E
            int r9 = r4.G
            r10 = 8
            r11 = 8
            r5 = r19
            r5.e(r6, r7, r8, r9, r10, r11)
        L_0x0f7d:
            int r2 = r4.x
            int r3 = defpackage.c.bl
            int r5 = r2 - r3
            int r2 = r4.y
            int r3 = r4.G
            int r3 = r3 >> 1
            int r2 = r2 + r3
            int r3 = defpackage.c.bm
            int r6 = r2 - r3
            r7 = 0
            int r2 = r4.ay
            r2 = r2 & 1
            if (r2 == 0) goto L_0x0fd7
            r8 = 1
        L_0x0f96:
            r9 = 0
            r2 = r18
            r3 = r19
            r2.a(r3, r4, r5, r6, r7, r8, r9)
            a r2 = r4.ac
            if (r2 == 0) goto L_0x0e63
            a r7 = r4.ac
            int r2 = r4.x
            int r3 = defpackage.c.bl
            int r8 = r2 - r3
            a r2 = r4.ac
            int r2 = r2.y
            int r2 = r2 + 15
            int r3 = defpackage.c.bm
            int r9 = r2 - r3
            r10 = 0
            int r2 = r4.ay
            r2 = r2 & 1
            if (r2 == 0) goto L_0x0fd9
            r11 = 1
        L_0x0fbc:
            r12 = 0
            r5 = r18
            r6 = r19
            r5.a(r6, r7, r8, r9, r10, r11, r12)
            a r2 = r4.ac
            boolean r2 = r2.as
            if (r2 == 0) goto L_0x0e63
            a r2 = r4.ac
            boolean r2 = r2.av
            if (r2 != 0) goto L_0x0e63
            a r2 = r4.ac
            r3 = 1
            r2.av = r3
            goto L_0x0e63
        L_0x0fd7:
            r8 = 0
            goto L_0x0f96
        L_0x0fd9:
            r11 = 0
            goto L_0x0fbc
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.d.a(com.a.a.e.o):void");
    }

    public final void aB() {
        if (c.ct != null) {
            c.G(c.cv);
        }
        if (c.u != 9 && c.u != 23 && c.u != 24) {
            c.aL = c.u;
            c.u = 9;
        }
    }

    public final void af(int i) {
        this.p = Math.abs(i);
        int i2 = this.p;
        try {
            switch (ap(i)) {
                case 1:
                    this.p = 1;
                    break;
                case 2:
                    this.p = 3;
                    break;
                case 5:
                    this.p = 4;
                    break;
                case 6:
                    this.p = 2;
                    break;
            }
        } catch (Exception e) {
        }
        switch (this.p) {
            case 5:
            case com.a.a.b.c.INT_16:
                this.p = 5;
                break;
            case 6:
            case 22:
                this.p = 6;
                break;
            case 7:
            case 21:
                this.p = 7;
                break;
        }
        if (c.u == 0) {
            if (this.p == 6) {
                this.eu += 30;
                c.cs = 1;
            } else if (this.p == 7) {
                this.eu += 30;
                c.cs = 0;
            }
        }
        if (c.u == 23) {
            if (this.p == 6) {
                c.R(59);
                c.u = 24;
                this.o = 0;
            } else if (this.p == 7) {
                c.S(58);
                c.u = this.eG;
            }
        } else if (c.u == 24) {
            if (this.p == 6) {
                az();
            } else if (this.p == 7) {
                if (this.o == 3) {
                    c.u = 2;
                }
            }
            Mi.Y.a();
        }
        if (c.u == 9) {
            if (c.K(this.p) || c.J(this.p) || P() || N()) {
                if (c.cv == 0 && c.aL != 21) {
                    c.c(c.cv, -1);
                }
                n(c.aL);
                if (c.aL == 21) {
                    k();
                }
            }
        } else if (this.aI || this.aY) {
            this.p = 0;
        } else {
            switch (i2) {
                case 50:
                    this.p = 50;
                    break;
                case com.a.a.e.c.KEY_NUM4:
                    this.p = 52;
                    break;
                case com.a.a.e.c.KEY_NUM6:
                    this.p = 54;
                    break;
                case 56:
                    this.p = 56;
                    break;
            }
            this.q = this.p;
            if (c.u == 3) {
                r(this.p);
                this.q = this.p;
                if (this.bM) {
                    s(this.q);
                } else if (this.cj != null) {
                    D(this.q);
                } else if (this.aQ != null) {
                    if (c.K(this.q)) {
                        n(10);
                        c.G(0);
                        this.aG = 0;
                        this.j = 0;
                    } else if (c.J(this.q)) {
                        n(9);
                        c.aL = 3;
                        c.G(0);
                        this.aG = 0;
                        this.j = 0;
                    }
                    W(this.q);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: c.a(a, short):void
     arg types: [a, int]
     candidates:
      c.a(byte[], int):int
      c.a(int[], char):int
      c.a(java.lang.String, int[]):com.a.a.e.p
      c.a(a, java.lang.String):void
      c.a(com.a.a.e.o, int):void
      c.a(java.lang.Exception, java.lang.String):void
      c.a(byte[], int[]):void
      c.a(a, a):boolean
      c.a(int, boolean):void
      c.a(a, int):void
      c.a(java.lang.String, int):void
      c.a(java.lang.String, java.lang.String):void
      c.a(java.lang.String, boolean):void
      c.a(int[], int[]):void
      c.a(a, short):void */
    public final void ag(int i) {
        this.p = 0;
        if (this.aK != 0) {
            n();
        }
        switch (c.u) {
            case 3:
                if (this.aQ == null) {
                    return;
                }
                if (this.aQ.aw == 1 || this.aQ.aw == 2) {
                    a(this.aQ, (short) 0);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public final void l(int i, int i2) {
        this.j = i;
        this.aG = i2;
        if (this.eH) {
            for (int i3 = 0; i3 < 12; i3++) {
                if (c.a(i, i2, 1, 1, (i3 % 3) * 80, ((i3 / 3) * 28) + 208 + 6, 80, 28)) {
                    int i4 = i3 + 49;
                    if (i3 == 9) {
                        i4 = 7;
                    } else if (i3 == 11) {
                        i4 = 6;
                    } else if (i3 == 10) {
                        i4 = 48;
                    }
                    af(i4);
                    return;
                }
            }
        }
        af(0);
        System.out.println(new StringBuffer("x ").append(this.j).append(" y ").append(this.aG).toString());
    }

    public final void m(int i, int i2) {
        if (this.eH) {
            int i3 = 0;
            while (true) {
                int i4 = i3;
                if (i4 < 12) {
                    if (c.a(i, i2, 1, 1, (i4 % 3) * 80, ((i4 / 3) * 28) + 208 + 6, 80, 28)) {
                        int i5 = i4 + 49;
                        if (i4 == 9) {
                            i5 = 7;
                        } else if (i4 == 11) {
                            i5 = 6;
                        } else if (i4 == 10) {
                            i5 = 48;
                        }
                        ag(i5);
                        return;
                    }
                    i3 = i4 + 1;
                } else {
                    return;
                }
            }
        }
    }
}
