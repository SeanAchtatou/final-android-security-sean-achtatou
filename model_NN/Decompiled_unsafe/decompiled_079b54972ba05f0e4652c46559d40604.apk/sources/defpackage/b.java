package defpackage;

/* renamed from: b  reason: default package */
public final class b {
    public int aA;
    private Object[] az;

    public b() {
        this(10);
    }

    private b(int i) {
        this.az = new Object[i];
    }

    private void g(int i) {
        int length = this.az.length;
        if (i > length) {
            Object[] objArr = this.az;
            int i2 = length * 2;
            if (i2 >= i) {
                i = i2;
            }
            this.az = new Object[i];
            System.arraycopy(objArr, 0, this.az, 0, this.aA);
        }
    }

    public final b M() {
        b bVar = new b();
        bVar.aA = this.aA;
        bVar.az = new Object[this.aA];
        b(bVar.az);
        return bVar;
    }

    public final boolean N() {
        return this.aA == 0;
    }

    public final void a(int i) {
        if (i < 0 || i > this.aA) {
            throw new ArrayIndexOutOfBoundsException(new StringBuffer(String.valueOf(i)).append(" index <> ").append(this.aA).toString());
        }
        System.arraycopy(this.az, i + 1, this.az, i, (this.aA - 1) - i);
        Object[] objArr = this.az;
        int i2 = this.aA - 1;
        this.aA = i2;
        objArr[i2] = null;
    }

    public final void a(Object obj, int i) {
        if (i > this.aA) {
            throw new ArrayIndexOutOfBoundsException(new StringBuffer(String.valueOf(i)).append(" > ").append(this.aA).toString());
        }
        g(this.aA + 1);
        System.arraycopy(this.az, i, this.az, i + 1, this.aA - i);
        this.az[i] = obj;
        this.aA++;
    }

    public final void b(Object[] objArr) {
        System.arraycopy(this.az, 0, objArr, 0, this.aA);
    }

    public final void c(Object obj) {
        if (obj != null) {
            g(this.aA + 1);
            Object[] objArr = this.az;
            int i = this.aA;
            this.aA = i + 1;
            objArr[i] = obj;
        }
    }

    public final void f() {
        for (int i = 0; i < this.az.length; i++) {
            this.az[i] = null;
        }
        this.az = new Object[10];
        System.gc();
        this.aA = 0;
    }

    public final int h(int i) {
        try {
            return Integer.parseInt(l(i));
        } catch (ClassCastException e) {
            try {
                return ((Integer) k(i)).intValue();
            } catch (NumberFormatException e2) {
                throw new NumberFormatException("NumberIllegalFormat");
            }
        }
    }

    public final Object k(int i) {
        if (i < 0 || i > this.aA) {
            return null;
        }
        return this.az[i];
    }

    public final String l(int i) {
        try {
            return k(i).toString();
        } catch (Exception e) {
            return "";
        }
    }

    public final boolean m(int i) {
        try {
            return k(i).toString().toLowerCase().equals("true");
        } catch (Exception e) {
            return false;
        }
    }
}
