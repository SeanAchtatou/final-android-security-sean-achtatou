package javax.microedition.midlet;

import android.os.Message;
import android.util.Log;
import com.a.a.c.c;
import org.meteoroid.core.a;
import org.meteoroid.core.h;
import org.meteoroid.plugin.device.MIDPDevice;

public abstract class MIDlet implements a.C0004a, h.a {
    public static final int MIDLET_PLATFORM_REQUEST = -2023686143;
    public static final int MIDLET_PLATFORM_REQUEST_FINISH = -2023686142;
    private boolean jk;
    private int jl;

    protected MIDlet() {
    }

    /* access modifiers changed from: protected */
    public abstract void J();

    /* access modifiers changed from: protected */
    public abstract void K();

    public final String ak(String str) {
        return a.bb(str);
    }

    public final boolean al(String str) {
        h.h(MIDLET_PLATFORM_REQUEST, "MIDLET_PLATFORM_REQUEST");
        h.h(MIDLET_PLATFORM_REQUEST_FINISH, "MIDLET_PLATFORM_REQUEST_FINISH");
        h.e(MIDLET_PLATFORM_REQUEST, str);
        if (!this.jk) {
            return false;
        }
        throw new c();
    }

    public final int am(String str) {
        return -1;
    }

    public boolean b(Message message) {
        if (message.what == -2023686142) {
            if (message.obj != null) {
                this.jk = ((Boolean) message.obj).booleanValue();
            }
            return true;
        } else if (message.what == 47623) {
            onPause();
            K();
            return true;
        } else if (message.what != 47622) {
            return false;
        } else {
            onStart();
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public abstract void c(boolean z);

    public final void ed() {
        Log.d(getClass().getSimpleName(), "MIDlet is called to destroy.");
        h.ci(MIDPDevice.MSG_MIDP_MIDLET_NOTIFYDESTROYED);
    }

    public final void ee() {
        a.pause();
    }

    public final void ef() {
        a.resume();
    }

    public void eg() {
        this.jl = 0;
        h.a(this);
        a.start();
    }

    public int getState() {
        return this.jl;
    }

    public void onDestroy() {
        this.jl = 3;
        try {
            c(true);
        } catch (Exception e) {
            Log.w(getClass().getSimpleName(), e + " in MIDlet destroyApp");
        }
    }

    public void onPause() {
        this.jl = 2;
    }

    public void onResume() {
        this.jl = 1;
    }

    public void onStart() {
        this.jl = 1;
        try {
            J();
        } catch (Exception e) {
            Log.w(getClass().getSimpleName(), e + " restart in MIDlet startApp");
            e.printStackTrace();
        }
    }
}
