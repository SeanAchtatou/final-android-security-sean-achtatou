package javax.microedition.enhance;

import android.app.ActivityManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import com.a.a.e.o;
import com.a.a.e.p;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import org.meteoroid.core.e;
import org.meteoroid.core.l;
import org.meteoroid.plugin.device.MIDPDevice;

public final class MIDPHelper {
    public static final int HORIZONTAL = 1;
    private static final String LOG_TAG = "MIDPHelper";
    public static final int MOVEDOWN = 2;
    public static final int MOVELEFT = 3;
    public static final int MOVERIGHT = 4;
    public static final int MOVEUP = 0;
    public static final int NOMOVE = -1;
    public static final int VERTICAL = 0;
    private static final String[] fU = {".png", ".mp3", ".jpg", ".jpeg", ".mpeg", ".bmp"};
    public static Paint fV = new Paint();
    public static Rect rect = new Rect();

    public static p a(InputStream inputStream, int i) {
        return new p(e.b(inputStream, i));
    }

    public static void a(o oVar, p pVar, int i, int i2) {
        Bitmap b = b(pVar);
        Canvas canvas = new Canvas(Bitmap.createBitmap(b.getWidth(), b.getHeight(), Bitmap.Config.RGB_565));
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0.0f);
        fV.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        canvas.drawBitmap(b, 0.0f, 0.0f, fV);
        k(oVar).drawBitmap(b, (float) i, (float) i2, fV);
        fV.reset();
    }

    public static void a(o oVar, p pVar, int i, int i2, int i3, int i4, boolean z) {
        Bitmap bitmap = pVar.getBitmap();
        rect.set(i, i2, i + i3, i2 + i4);
        k(oVar).drawBitmap(bitmap, (Rect) null, rect, (Paint) null);
    }

    public static void a(p pVar) {
        if (pVar != null && pVar.bitmap != null) {
            pVar.bitmap.recycle();
        }
    }

    private static Bitmap b(p pVar) {
        return pVar.getBitmap();
    }

    public static InputStream b(Class cls, String str) {
        String str2;
        if (str == null) {
            throw new IOException("Can't load resource noname.");
        }
        Log.d(LOG_TAG, "Load assert " + str + " .");
        while (str.startsWith(File.separator)) {
            str = str.substring(1);
        }
        String[] split = str.split("\\.");
        String bK = com.a.a.s.e.bK(split[0]);
        String bL = com.a.a.s.e.bL(split[0]);
        if (split.length == 1) {
            str2 = "a_b";
        } else if (split.length == 2) {
            str2 = split[1];
        } else {
            str2 = "";
            for (int i = 1; i < split.length; i++) {
                str2 = str2 + split[i];
            }
        }
        String str3 = (bL.length() == 0 || bL.charAt(0) == '_') ? "b_a" + bL : bL;
        int i2 = 0;
        while (true) {
            if (i2 >= fU.length) {
                break;
            } else if (str2.equalsIgnoreCase(fU[i2])) {
                str2 = str2.toLowerCase();
                break;
            } else {
                i2++;
            }
        }
        String str4 = bK + str3 + "." + str2;
        Log.d(LOG_TAG, "Load assert " + str4 + " .");
        return l.bl(com.a.a.s.e.bE(str4));
    }

    public static void b(o oVar, p pVar, int i, int i2, int i3) {
        Bitmap b = b(pVar);
        fV.setAlpha(i3);
        k(oVar).drawBitmap(b, (float) i, (float) i2, fV);
        fV.reset();
    }

    public static void b(o oVar, p pVar, int i, int i2, int i3, int i4) {
        Bitmap b = b(pVar);
        fV.reset();
        fV.setColor(i3);
        k(oVar).drawRect((float) i, (float) i2, (float) (b.getWidth() + i), (float) (b.getHeight() + i2), fV);
        fV.setAlpha(i4);
        k(oVar).drawBitmap(b, (float) i, (float) i2, fV);
        fV.reset();
    }

    public static void ba() {
    }

    public static void c(o oVar, p pVar, int i, int i2, int i3) {
        int i4;
        int i5 = -1;
        Bitmap bitmap = pVar.getBitmap();
        int save = k(oVar).save(1);
        if (i3 == 1) {
            i = (-i) - bitmap.getWidth();
            i4 = -1;
        } else {
            i4 = 1;
        }
        if (i3 == 0) {
            i2 = (-i2) - bitmap.getHeight();
        } else {
            i5 = 1;
        }
        k(oVar).scale((float) i4, (float) i5);
        k(oVar).drawBitmap(bitmap, (float) i, (float) i2, (Paint) null);
        k(oVar).restoreToCount(save);
    }

    public static int d(int i, int i2, int i3, int i4) {
        if (i > i3) {
            return 3;
        }
        if (i < i3) {
            return 4;
        }
        if (i2 > i4) {
            return 0;
        }
        return i2 < i4 ? 2 : -1;
    }

    public static long freeMemory() {
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        l.hI().getMemoryInfo(memoryInfo);
        return memoryInfo.availMem;
    }

    private static Canvas k(o oVar) {
        return ((MIDPDevice.e) oVar).iB();
    }

    public static void println(String str) {
        if (str != null) {
            Log.d(LOG_TAG, "System.out.println:" + str);
        } else {
            Log.w(LOG_TAG, "System.out.println null");
        }
    }

    public static void sleep(long j) {
        try {
            Thread.sleep(17);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static long totalMemory() {
        return 6291456;
    }
}
