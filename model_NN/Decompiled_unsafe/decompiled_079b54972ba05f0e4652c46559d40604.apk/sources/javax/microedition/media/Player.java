package javax.microedition.media;

public interface Player extends Controllable {
    public static final int CLOSED = 0;
    public static final int PREFETCHED = 300;
    public static final int REALIZED = 200;
    public static final int STARTED = 400;
    public static final long TIME_UNKNOWN = -1;
    public static final int UNREALIZED = 100;

    void a(PlayerListener playerListener);

    void b(PlayerListener playerListener);

    void ba(int i);

    void close();

    void dD();

    void dE();

    void dF();

    long dG();

    long e(long j);

    String getContentType();

    long getDuration();

    int getState();

    void start();

    void stop();
}
