package javax.microedition.media.control;

import javax.microedition.media.Control;

public interface PitchControl extends Control {
    int bh(int i);

    int dJ();

    int dK();

    int getPitch();
}
