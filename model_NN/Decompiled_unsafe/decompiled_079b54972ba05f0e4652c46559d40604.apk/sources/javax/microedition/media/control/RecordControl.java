package javax.microedition.media.control;

import java.io.OutputStream;
import javax.microedition.media.Control;

public interface RecordControl extends Control {
    void a(OutputStream outputStream);

    void ai(String str);

    int bj(int i);

    void commit();

    void dO();

    void dP();

    String getContentType();

    void reset();
}
