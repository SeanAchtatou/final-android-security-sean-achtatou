package javax.microedition.media.control;

import javax.microedition.media.Control;

public interface VolumeControl extends Control {
    int bl(int i);

    int getLevel();

    boolean isMuted();

    void r(boolean z);
}
