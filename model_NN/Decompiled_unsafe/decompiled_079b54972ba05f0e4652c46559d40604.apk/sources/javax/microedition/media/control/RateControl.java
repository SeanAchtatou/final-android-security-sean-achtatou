package javax.microedition.media.control;

import javax.microedition.media.Control;

public interface RateControl extends Control {
    int bi(int i);

    int dL();

    int dM();

    int dN();
}
