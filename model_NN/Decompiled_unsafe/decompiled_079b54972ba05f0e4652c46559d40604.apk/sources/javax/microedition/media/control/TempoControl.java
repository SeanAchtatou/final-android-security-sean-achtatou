package javax.microedition.media.control;

public interface TempoControl extends RateControl {
    int bk(int i);

    int dR();
}
