package javax.microedition.media.control;

public interface VideoControl extends GUIControl {
    public static final int USE_DIRECT_VIDEO = 1;

    void D(int i, int i2);

    void E(int i, int i2);

    Object a(int i, Object obj);

    byte[] aj(String str);

    int dS();

    int dT();

    int dU();

    int dV();

    int dW();

    int dX();

    void q(boolean z);

    void setVisible(boolean z);
}
