package javax.microedition.media.control;

import javax.microedition.media.Control;

public interface MIDIControl extends Control {
    public static final int CONTROL_CHANGE = 176;
    public static final int NOTE_ON = 144;

    void B(int i, int i2);

    String C(int i, int i2);

    int[] be(int i);

    int bf(int i);

    int[] bg(int i);

    boolean dH();

    int f(byte[] bArr, int i, int i2);

    void m(int i, int i2, int i3);

    String n(int i, int i2, int i3);

    void o(int i, int i2, int i3);

    int[] p(boolean z);
}
