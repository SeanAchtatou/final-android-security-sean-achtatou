package javax.microedition.media.control;

import javax.microedition.media.Control;

public interface FramePositioningControl extends Control {
    int bb(int i);

    int bc(int i);

    long bd(int i);

    int f(long j);
}
