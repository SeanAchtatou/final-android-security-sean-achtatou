package javax.microedition.media;

import java.io.InputStream;
import javax.microedition.media.protocol.DataSource;
import org.meteoroid.core.g;
import org.meteoroid.plugin.device.MIDPDevice;

public class Manager {
    public static final String TONE_DEVICE_LOCATOR = "device://tone";
    public static final String[] jg = {"audio/x-wav", "audio/basic", "audio/mpeg", "audio/midi", "audio/x-tone-seq", "audio/amr"};
    public static final String[] jh = {"http://"};

    public static Player a(InputStream inputStream, String str) {
        return new MIDPDevice.i(g.a(null, inputStream, str));
    }

    public static Player a(DataSource dataSource) {
        return new MIDPDevice.i(g.hn());
    }

    public static String[] ae(String str) {
        return jg;
    }

    public static String[] af(String str) {
        return jh;
    }

    public static Player ag(String str) {
        return new MIDPDevice.i(g.e(str, false));
    }

    public static void l(int i, int i2, int i3) {
        throw new MediaException();
    }
}
