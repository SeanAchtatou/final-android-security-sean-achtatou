package javax.microedition.media.protocol;

import javax.microedition.media.Controllable;

public interface SourceStream extends Controllable {
    public static final int NOT_SEEKABLE = 0;
    public static final int RANDOM_ACCESSIBLE = 2;
    public static final int SEEKABLE_TO_START = 1;

    ContentDescriptor ea();

    long eb();

    int ec();

    long getContentLength();

    int getTransferSize();

    long h(long j);

    int read(byte[] bArr, int i, int i2);
}
