package javax.microedition.media.protocol;

import javax.microedition.media.Controllable;

public abstract class DataSource implements Controllable {
    private String jj;

    public DataSource(String str) {
    }

    public abstract void connect();

    public String dY() {
        return this.jj;
    }

    public abstract SourceStream[] dZ();

    public abstract void disconnect();

    public abstract String getContentType();

    public abstract void start();

    public abstract void stop();
}
