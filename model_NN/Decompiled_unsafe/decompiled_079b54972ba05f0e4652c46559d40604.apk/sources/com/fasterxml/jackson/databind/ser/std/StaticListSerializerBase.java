package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.lang.reflect.Type;
import java.util.Collection;

public abstract class StaticListSerializerBase<T extends Collection<?>> extends StdSerializer<T> {
    protected StaticListSerializerBase(Class<?> cls) {
        super(cls, false);
    }

    /* access modifiers changed from: protected */
    public abstract JsonNode contentSchema();

    public JsonNode getSchema(SerializerProvider serializerProvider, Type type) {
        ObjectNode createSchemaNode = createSchemaNode("array", true);
        createSchemaNode.put("items", contentSchema());
        return createSchemaNode;
    }

    public boolean isEmpty(T t) {
        return t == null || t.size() == 0;
    }
}
