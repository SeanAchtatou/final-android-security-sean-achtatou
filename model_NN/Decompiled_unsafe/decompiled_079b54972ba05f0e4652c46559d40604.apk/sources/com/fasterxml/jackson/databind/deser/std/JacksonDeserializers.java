package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.core.JsonLocation;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import com.fasterxml.jackson.databind.deser.CreatorProperty;
import com.fasterxml.jackson.databind.deser.ValueInstantiator;
import com.fasterxml.jackson.databind.util.TokenBuffer;

public class JacksonDeserializers {

    public static class JavaTypeDeserializer extends StdScalarDeserializer<JavaType> {
        public JavaTypeDeserializer() {
            super(JavaType.class);
        }

        public JavaType deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
            JsonToken currentToken = jsonParser.getCurrentToken();
            if (currentToken == JsonToken.VALUE_STRING) {
                String trim = jsonParser.getText().trim();
                return trim.length() == 0 ? (JavaType) getEmptyValue() : deserializationContext.getTypeFactory().constructFromCanonical(trim);
            } else if (currentToken == JsonToken.VALUE_EMBEDDED_OBJECT) {
                return (JavaType) jsonParser.getEmbeddedObject();
            } else {
                throw deserializationContext.mappingException(this._valueClass);
            }
        }
    }

    public static class JsonLocationInstantiator extends ValueInstantiator {
        private static final int _int(Object obj) {
            if (obj == null) {
                return 0;
            }
            return ((Number) obj).intValue();
        }

        private static final long _long(Object obj) {
            if (obj == null) {
                return 0;
            }
            return ((Number) obj).longValue();
        }

        public boolean canCreateFromObjectWith() {
            return true;
        }

        public Object createFromObjectWith(DeserializationContext deserializationContext, Object[] objArr) {
            return new JsonLocation(objArr[0], _long(objArr[1]), _long(objArr[2]), _int(objArr[3]), _int(objArr[4]));
        }

        public CreatorProperty[] getFromObjectArguments(DeserializationConfig deserializationConfig) {
            JavaType constructType = deserializationConfig.constructType(Integer.TYPE);
            JavaType constructType2 = deserializationConfig.constructType(Long.TYPE);
            return new CreatorProperty[]{new CreatorProperty("sourceRef", deserializationConfig.constructType(Object.class), null, null, null, 0, null), new CreatorProperty("byteOffset", constructType2, null, null, null, 1, null), new CreatorProperty("charOffset", constructType2, null, null, null, 2, null), new CreatorProperty("lineNr", constructType, null, null, null, 3, null), new CreatorProperty("columnNr", constructType, null, null, null, 4, null)};
        }

        public String getValueTypeDesc() {
            return JsonLocation.class.getName();
        }
    }

    @JacksonStdImpl
    public static class TokenBufferDeserializer extends StdScalarDeserializer<TokenBuffer> {
        public TokenBufferDeserializer() {
            super(TokenBuffer.class);
        }

        public TokenBuffer deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
            TokenBuffer tokenBuffer = new TokenBuffer(jsonParser.getCodec());
            tokenBuffer.copyCurrentStructure(jsonParser);
            return tokenBuffer;
        }
    }

    public static StdDeserializer<?>[] all() {
        return new StdDeserializer[]{new JavaTypeDeserializer(), new TokenBufferDeserializer()};
    }

    public static ValueInstantiator findValueInstantiator(DeserializationConfig deserializationConfig, BeanDescription beanDescription) {
        if (beanDescription.getBeanClass() == JsonLocation.class) {
            return new JsonLocationInstantiator();
        }
        return null;
    }
}
