package com.fasterxml.jackson.databind.ser.impl;

import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;

public final class WritableObjectId {
    public final ObjectIdGenerator<?> generator;
    public Object id;
    public JsonSerializer<Object> serializer;

    public WritableObjectId(ObjectIdGenerator<?> objectIdGenerator) {
        this.generator = objectIdGenerator;
    }
}
