package com.fasterxml.jackson.databind.type;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.util.ArrayBuilders;
import com.fasterxml.jackson.databind.util.LRUMap;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class TypeFactory {
    protected static final SimpleType CORE_TYPE_BOOL = new SimpleType(Boolean.TYPE);
    protected static final SimpleType CORE_TYPE_INT = new SimpleType(Integer.TYPE);
    protected static final SimpleType CORE_TYPE_LONG = new SimpleType(Long.TYPE);
    protected static final SimpleType CORE_TYPE_STRING = new SimpleType(String.class);
    private static final JavaType[] NO_TYPES = new JavaType[0];
    protected static final TypeFactory instance = new TypeFactory();
    protected HierarchicType _cachedArrayListType;
    protected HierarchicType _cachedHashMapType;
    protected final TypeModifier[] _modifiers;
    protected final TypeParser _parser;
    protected final LRUMap<ClassKey, JavaType> _typeCache;

    private TypeFactory() {
        this._typeCache = new LRUMap<>(16, 100);
        this._parser = new TypeParser(this);
        this._modifiers = null;
    }

    protected TypeFactory(TypeParser typeParser, TypeModifier[] typeModifierArr) {
        this._typeCache = new LRUMap<>(16, 100);
        this._parser = typeParser;
        this._modifiers = typeModifierArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.type.CollectionType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.CollectionType
     arg types: [java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType]
     candidates:
      com.fasterxml.jackson.databind.type.CollectionLikeType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.CollectionLikeType
      com.fasterxml.jackson.databind.type.CollectionType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.CollectionType */
    private JavaType _collectionType(Class<?> cls) {
        JavaType[] findTypeParameters = findTypeParameters(cls, Collection.class);
        if (findTypeParameters == null) {
            return CollectionType.construct(cls, _unknownType());
        }
        if (findTypeParameters.length == 1) {
            return CollectionType.construct(cls, findTypeParameters[0]);
        }
        throw new IllegalArgumentException("Strange Collection type " + cls.getName() + ": can not determine type parameters");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.type.MapType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.MapType
     arg types: [java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType]
     candidates:
      com.fasterxml.jackson.databind.type.MapLikeType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.MapLikeType
      com.fasterxml.jackson.databind.type.MapType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.MapType */
    private JavaType _mapType(Class<?> cls) {
        JavaType[] findTypeParameters = findTypeParameters(cls, Map.class);
        if (findTypeParameters == null) {
            return MapType.construct(cls, _unknownType(), _unknownType());
        }
        if (findTypeParameters.length == 2) {
            return MapType.construct(cls, findTypeParameters[0], findTypeParameters[1]);
        }
        throw new IllegalArgumentException("Strange Map type " + cls.getName() + ": can not determine type parameters");
    }

    public static TypeFactory defaultInstance() {
        return instance;
    }

    public static Class<?> rawClass(Type type) {
        return type instanceof Class ? (Class) type : defaultInstance().constructType(type).getRawClass();
    }

    public static JavaType unknownType() {
        return defaultInstance()._unknownType();
    }

    /* access modifiers changed from: protected */
    public synchronized HierarchicType _arrayListSuperInterfaceChain(HierarchicType hierarchicType) {
        if (this._cachedArrayListType == null) {
            HierarchicType deepCloneWithoutSubtype = hierarchicType.deepCloneWithoutSubtype();
            _doFindSuperInterfaceChain(deepCloneWithoutSubtype, List.class);
            this._cachedArrayListType = deepCloneWithoutSubtype.getSuperType();
        }
        HierarchicType deepCloneWithoutSubtype2 = this._cachedArrayListType.deepCloneWithoutSubtype();
        hierarchicType.setSuperType(deepCloneWithoutSubtype2);
        deepCloneWithoutSubtype2.setSubType(hierarchicType);
        return hierarchicType;
    }

    public JavaType _constructType(Type type, TypeBindings typeBindings) {
        JavaType _fromWildcard;
        if (type instanceof Class) {
            _fromWildcard = _fromClass((Class) type, typeBindings);
        } else if (type instanceof ParameterizedType) {
            _fromWildcard = _fromParamType((ParameterizedType) type, typeBindings);
        } else if (type instanceof GenericArrayType) {
            _fromWildcard = _fromArrayType((GenericArrayType) type, typeBindings);
        } else if (type instanceof TypeVariable) {
            _fromWildcard = _fromVariable((TypeVariable) type, typeBindings);
        } else if (type instanceof WildcardType) {
            _fromWildcard = _fromWildcard((WildcardType) type, typeBindings);
        } else {
            throw new IllegalArgumentException("Unrecognized Type: " + (type == null ? "[null]" : type.toString()));
        }
        if (this._modifiers != null && !_fromWildcard.isContainerType()) {
            TypeModifier[] typeModifierArr = this._modifiers;
            int length = typeModifierArr.length;
            int i = 0;
            while (i < length) {
                JavaType modifyType = typeModifierArr[i].modifyType(_fromWildcard, type, typeBindings, this);
                i++;
                _fromWildcard = modifyType;
            }
        }
        return _fromWildcard;
    }

    /* access modifiers changed from: protected */
    public HierarchicType _doFindSuperInterfaceChain(HierarchicType hierarchicType, Class<?> cls) {
        HierarchicType _findSuperInterfaceChain;
        Class<?> rawClass = hierarchicType.getRawClass();
        Type[] genericInterfaces = rawClass.getGenericInterfaces();
        if (genericInterfaces != null) {
            for (Type _findSuperInterfaceChain2 : genericInterfaces) {
                HierarchicType _findSuperInterfaceChain3 = _findSuperInterfaceChain(_findSuperInterfaceChain2, cls);
                if (_findSuperInterfaceChain3 != null) {
                    _findSuperInterfaceChain3.setSubType(hierarchicType);
                    hierarchicType.setSuperType(_findSuperInterfaceChain3);
                    return hierarchicType;
                }
            }
        }
        Type genericSuperclass = rawClass.getGenericSuperclass();
        if (genericSuperclass == null || (_findSuperInterfaceChain = _findSuperInterfaceChain(genericSuperclass, cls)) == null) {
            return null;
        }
        _findSuperInterfaceChain.setSubType(hierarchicType);
        hierarchicType.setSuperType(_findSuperInterfaceChain);
        return hierarchicType;
    }

    /* access modifiers changed from: protected */
    public HierarchicType _findSuperClassChain(Type type, Class<?> cls) {
        HierarchicType _findSuperClassChain;
        HierarchicType hierarchicType = new HierarchicType(type);
        Class<?> rawClass = hierarchicType.getRawClass();
        if (rawClass == cls) {
            return hierarchicType;
        }
        Type genericSuperclass = rawClass.getGenericSuperclass();
        if (genericSuperclass == null || (_findSuperClassChain = _findSuperClassChain(genericSuperclass, cls)) == null) {
            return null;
        }
        _findSuperClassChain.setSubType(hierarchicType);
        hierarchicType.setSuperType(_findSuperClassChain);
        return hierarchicType;
    }

    /* access modifiers changed from: protected */
    public HierarchicType _findSuperInterfaceChain(Type type, Class<?> cls) {
        HierarchicType hierarchicType = new HierarchicType(type);
        Class<?> rawClass = hierarchicType.getRawClass();
        return rawClass == cls ? new HierarchicType(type) : (rawClass == HashMap.class && cls == Map.class) ? _hashMapSuperInterfaceChain(hierarchicType) : (rawClass == ArrayList.class && cls == List.class) ? _arrayListSuperInterfaceChain(hierarchicType) : _doFindSuperInterfaceChain(hierarchicType, cls);
    }

    /* access modifiers changed from: protected */
    public HierarchicType _findSuperTypeChain(Class<?> cls, Class<?> cls2) {
        return cls2.isInterface() ? _findSuperInterfaceChain(cls, cls2) : _findSuperClassChain(cls, cls2);
    }

    /* access modifiers changed from: protected */
    public JavaType _fromArrayType(GenericArrayType genericArrayType, TypeBindings typeBindings) {
        return ArrayType.construct(_constructType(genericArrayType.getGenericComponentType(), typeBindings), null, null);
    }

    /* access modifiers changed from: protected */
    public JavaType _fromClass(Class<?> cls, TypeBindings typeBindings) {
        JavaType javaType;
        if (cls == String.class) {
            return CORE_TYPE_STRING;
        }
        if (cls == Boolean.TYPE) {
            return CORE_TYPE_BOOL;
        }
        if (cls == Integer.TYPE) {
            return CORE_TYPE_INT;
        }
        if (cls == Long.TYPE) {
            return CORE_TYPE_LONG;
        }
        ClassKey classKey = new ClassKey(cls);
        synchronized (this._typeCache) {
            javaType = this._typeCache.get(classKey);
        }
        if (javaType != null) {
            return javaType;
        }
        JavaType construct = cls.isArray() ? ArrayType.construct(_constructType(cls.getComponentType(), null), null, null) : cls.isEnum() ? new SimpleType(cls) : Map.class.isAssignableFrom(cls) ? _mapType(cls) : Collection.class.isAssignableFrom(cls) ? _collectionType(cls) : new SimpleType(cls);
        synchronized (this._typeCache) {
            this._typeCache.put(classKey, construct);
        }
        return construct;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.type.MapType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.MapType
     arg types: [java.lang.Class, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType]
     candidates:
      com.fasterxml.jackson.databind.type.MapLikeType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.MapLikeType
      com.fasterxml.jackson.databind.type.MapType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.MapType */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.type.CollectionType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.CollectionType
     arg types: [java.lang.Class, com.fasterxml.jackson.databind.JavaType]
     candidates:
      com.fasterxml.jackson.databind.type.CollectionLikeType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.CollectionLikeType
      com.fasterxml.jackson.databind.type.CollectionType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.CollectionType */
    /* access modifiers changed from: protected */
    public JavaType _fromParamType(ParameterizedType parameterizedType, TypeBindings typeBindings) {
        JavaType[] javaTypeArr;
        Class cls = (Class) parameterizedType.getRawType();
        Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        int length = actualTypeArguments == null ? 0 : actualTypeArguments.length;
        if (length == 0) {
            javaTypeArr = NO_TYPES;
        } else {
            javaTypeArr = new JavaType[length];
            for (int i = 0; i < length; i++) {
                javaTypeArr[i] = _constructType(actualTypeArguments[i], typeBindings);
            }
        }
        if (Map.class.isAssignableFrom(cls)) {
            JavaType[] findTypeParameters = findTypeParameters(constructSimpleType(cls, javaTypeArr), Map.class);
            if (findTypeParameters.length == 2) {
                return MapType.construct((Class<?>) cls, findTypeParameters[0], findTypeParameters[1]);
            }
            throw new IllegalArgumentException("Could not find 2 type parameters for Map class " + cls.getName() + " (found " + findTypeParameters.length + ")");
        } else if (!Collection.class.isAssignableFrom(cls)) {
            return length == 0 ? new SimpleType(cls) : constructSimpleType(cls, javaTypeArr);
        } else {
            JavaType[] findTypeParameters2 = findTypeParameters(constructSimpleType(cls, javaTypeArr), Collection.class);
            if (findTypeParameters2.length == 1) {
                return CollectionType.construct((Class<?>) cls, findTypeParameters2[0]);
            }
            throw new IllegalArgumentException("Could not find 1 type parameter for Collection class " + cls.getName() + " (found " + findTypeParameters2.length + ")");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.type.CollectionType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.CollectionType
     arg types: [java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType]
     candidates:
      com.fasterxml.jackson.databind.type.CollectionLikeType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.CollectionLikeType
      com.fasterxml.jackson.databind.type.CollectionType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.CollectionType */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.type.MapType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.MapType
     arg types: [java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType]
     candidates:
      com.fasterxml.jackson.databind.type.MapLikeType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.MapLikeType
      com.fasterxml.jackson.databind.type.MapType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.MapType */
    /* access modifiers changed from: protected */
    public JavaType _fromParameterizedClass(Class<?> cls, List<JavaType> list) {
        if (cls.isArray()) {
            return ArrayType.construct(_constructType(cls.getComponentType(), null), null, null);
        }
        if (cls.isEnum()) {
            return new SimpleType(cls);
        }
        if (!Map.class.isAssignableFrom(cls)) {
            return Collection.class.isAssignableFrom(cls) ? list.size() >= 1 ? CollectionType.construct(cls, list.get(0)) : _collectionType(cls) : list.size() == 0 ? new SimpleType(cls) : constructSimpleType(cls, (JavaType[]) list.toArray(new JavaType[list.size()]));
        }
        if (list.size() <= 0) {
            return _mapType(cls);
        }
        return MapType.construct(cls, list.get(0), list.size() >= 2 ? list.get(1) : _unknownType());
    }

    /* access modifiers changed from: protected */
    public JavaType _fromVariable(TypeVariable<?> typeVariable, TypeBindings typeBindings) {
        if (typeBindings == null) {
            return _unknownType();
        }
        String name = typeVariable.getName();
        JavaType findType = typeBindings.findType(name);
        if (findType != null) {
            return findType;
        }
        Type[] bounds = typeVariable.getBounds();
        typeBindings._addPlaceholder(name);
        return _constructType(bounds[0], typeBindings);
    }

    /* access modifiers changed from: protected */
    public JavaType _fromWildcard(WildcardType wildcardType, TypeBindings typeBindings) {
        return _constructType(wildcardType.getUpperBounds()[0], typeBindings);
    }

    /* access modifiers changed from: protected */
    public synchronized HierarchicType _hashMapSuperInterfaceChain(HierarchicType hierarchicType) {
        if (this._cachedHashMapType == null) {
            HierarchicType deepCloneWithoutSubtype = hierarchicType.deepCloneWithoutSubtype();
            _doFindSuperInterfaceChain(deepCloneWithoutSubtype, Map.class);
            this._cachedHashMapType = deepCloneWithoutSubtype.getSuperType();
        }
        HierarchicType deepCloneWithoutSubtype2 = this._cachedHashMapType.deepCloneWithoutSubtype();
        hierarchicType.setSuperType(deepCloneWithoutSubtype2);
        deepCloneWithoutSubtype2.setSubType(hierarchicType);
        return hierarchicType;
    }

    /* access modifiers changed from: protected */
    public JavaType _resolveVariableViaSubTypes(HierarchicType hierarchicType, String str, TypeBindings typeBindings) {
        if (hierarchicType != null && hierarchicType.isGeneric()) {
            TypeVariable[] typeParameters = hierarchicType.getRawClass().getTypeParameters();
            int length = typeParameters.length;
            for (int i = 0; i < length; i++) {
                if (str.equals(typeParameters[i].getName())) {
                    Type type = hierarchicType.asGeneric().getActualTypeArguments()[i];
                    return type instanceof TypeVariable ? _resolveVariableViaSubTypes(hierarchicType.getSubType(), ((TypeVariable) type).getName(), typeBindings) : _constructType(type, typeBindings);
                }
            }
        }
        return _unknownType();
    }

    /* access modifiers changed from: protected */
    public JavaType _unknownType() {
        return new SimpleType(Object.class);
    }

    public ArrayType constructArrayType(JavaType javaType) {
        return ArrayType.construct(javaType, null, null);
    }

    public ArrayType constructArrayType(Class<?> cls) {
        return ArrayType.construct(_constructType(cls, null), null, null);
    }

    public CollectionLikeType constructCollectionLikeType(Class<?> cls, JavaType javaType) {
        return CollectionLikeType.construct(cls, javaType);
    }

    public CollectionLikeType constructCollectionLikeType(Class<?> cls, Class<?> cls2) {
        return CollectionLikeType.construct(cls, constructType(cls2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.type.CollectionType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.CollectionType
     arg types: [java.lang.Class<? extends java.util.Collection>, com.fasterxml.jackson.databind.JavaType]
     candidates:
      com.fasterxml.jackson.databind.type.CollectionLikeType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.CollectionLikeType
      com.fasterxml.jackson.databind.type.CollectionType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.CollectionType */
    public CollectionType constructCollectionType(Class<? extends Collection> cls, JavaType javaType) {
        return CollectionType.construct((Class<?>) cls, javaType);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.type.CollectionType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.CollectionType
     arg types: [java.lang.Class<? extends java.util.Collection>, com.fasterxml.jackson.databind.JavaType]
     candidates:
      com.fasterxml.jackson.databind.type.CollectionLikeType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.CollectionLikeType
      com.fasterxml.jackson.databind.type.CollectionType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.CollectionType */
    public CollectionType constructCollectionType(Class<? extends Collection> cls, Class<?> cls2) {
        return CollectionType.construct((Class<?>) cls, constructType(cls2));
    }

    public JavaType constructFromCanonical(String str) {
        return this._parser.parse(str);
    }

    public MapLikeType constructMapLikeType(Class<?> cls, JavaType javaType, JavaType javaType2) {
        return MapLikeType.construct(cls, javaType, javaType2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.type.MapType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.MapType
     arg types: [java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType]
     candidates:
      com.fasterxml.jackson.databind.type.MapLikeType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.MapLikeType
      com.fasterxml.jackson.databind.type.MapType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.MapType */
    public MapLikeType constructMapLikeType(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        return MapType.construct(cls, constructType(cls2), constructType(cls3));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.type.MapType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.MapType
     arg types: [java.lang.Class<? extends java.util.Map>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType]
     candidates:
      com.fasterxml.jackson.databind.type.MapLikeType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.MapLikeType
      com.fasterxml.jackson.databind.type.MapType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.MapType */
    public MapType constructMapType(Class<? extends Map> cls, JavaType javaType, JavaType javaType2) {
        return MapType.construct((Class<?>) cls, javaType, javaType2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.type.MapType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.MapType
     arg types: [java.lang.Class<? extends java.util.Map>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType]
     candidates:
      com.fasterxml.jackson.databind.type.MapLikeType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.MapLikeType
      com.fasterxml.jackson.databind.type.MapType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.MapType */
    public MapType constructMapType(Class<? extends Map> cls, Class<?> cls2, Class<?> cls3) {
        return MapType.construct((Class<?>) cls, constructType(cls2), constructType(cls3));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.type.TypeFactory.constructMapType(java.lang.Class<? extends java.util.Map>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.MapType
     arg types: [java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType]
     candidates:
      com.fasterxml.jackson.databind.type.TypeFactory.constructMapType(java.lang.Class<? extends java.util.Map>, java.lang.Class<?>, java.lang.Class<?>):com.fasterxml.jackson.databind.type.MapType
      com.fasterxml.jackson.databind.type.TypeFactory.constructMapType(java.lang.Class<? extends java.util.Map>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.MapType */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.type.TypeFactory.constructCollectionType(java.lang.Class<? extends java.util.Collection>, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.CollectionType
     arg types: [java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType]
     candidates:
      com.fasterxml.jackson.databind.type.TypeFactory.constructCollectionType(java.lang.Class<? extends java.util.Collection>, java.lang.Class<?>):com.fasterxml.jackson.databind.type.CollectionType
      com.fasterxml.jackson.databind.type.TypeFactory.constructCollectionType(java.lang.Class<? extends java.util.Collection>, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.CollectionType */
    public JavaType constructParametricType(Class<?> cls, JavaType... javaTypeArr) {
        if (cls.isArray()) {
            if (javaTypeArr.length == 1) {
                return constructArrayType(javaTypeArr[0]);
            }
            throw new IllegalArgumentException("Need exactly 1 parameter type for arrays (" + cls.getName() + ")");
        } else if (Map.class.isAssignableFrom(cls)) {
            if (javaTypeArr.length == 2) {
                return constructMapType((Class<? extends Map>) cls, javaTypeArr[0], javaTypeArr[1]);
            }
            throw new IllegalArgumentException("Need exactly 2 parameter types for Map types (" + cls.getName() + ")");
        } else if (!Collection.class.isAssignableFrom(cls)) {
            return constructSimpleType(cls, javaTypeArr);
        } else {
            if (javaTypeArr.length == 1) {
                return constructCollectionType((Class<? extends Collection>) cls, javaTypeArr[0]);
            }
            throw new IllegalArgumentException("Need exactly 1 parameter type for Collection types (" + cls.getName() + ")");
        }
    }

    public JavaType constructParametricType(Class<?> cls, Class<?>... clsArr) {
        int length = clsArr.length;
        JavaType[] javaTypeArr = new JavaType[length];
        for (int i = 0; i < length; i++) {
            javaTypeArr[i] = _fromClass(clsArr[i], null);
        }
        return constructParametricType(cls, javaTypeArr);
    }

    public CollectionLikeType constructRawCollectionLikeType(Class<?> cls) {
        return CollectionLikeType.construct(cls, unknownType());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.type.CollectionType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.CollectionType
     arg types: [java.lang.Class<? extends java.util.Collection>, com.fasterxml.jackson.databind.JavaType]
     candidates:
      com.fasterxml.jackson.databind.type.CollectionLikeType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.CollectionLikeType
      com.fasterxml.jackson.databind.type.CollectionType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.CollectionType */
    public CollectionType constructRawCollectionType(Class<? extends Collection> cls) {
        return CollectionType.construct((Class<?>) cls, unknownType());
    }

    public MapLikeType constructRawMapLikeType(Class<?> cls) {
        return MapLikeType.construct(cls, unknownType(), unknownType());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.type.MapType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.MapType
     arg types: [java.lang.Class<? extends java.util.Map>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType]
     candidates:
      com.fasterxml.jackson.databind.type.MapLikeType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.MapLikeType
      com.fasterxml.jackson.databind.type.MapType.construct(java.lang.Class<?>, com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.databind.JavaType):com.fasterxml.jackson.databind.type.MapType */
    public MapType constructRawMapType(Class<? extends Map> cls) {
        return MapType.construct((Class<?>) cls, unknownType(), unknownType());
    }

    public JavaType constructSimpleType(Class<?> cls, JavaType[] javaTypeArr) {
        TypeVariable[] typeParameters = cls.getTypeParameters();
        if (typeParameters.length != javaTypeArr.length) {
            throw new IllegalArgumentException("Parameter type mismatch for " + cls.getName() + ": expected " + typeParameters.length + " parameters, was given " + javaTypeArr.length);
        }
        String[] strArr = new String[typeParameters.length];
        int length = typeParameters.length;
        for (int i = 0; i < length; i++) {
            strArr[i] = typeParameters[i].getName();
        }
        return new SimpleType(cls, strArr, javaTypeArr, null, null);
    }

    public JavaType constructSpecializedType(JavaType javaType, Class<?> cls) {
        if (!(javaType instanceof SimpleType) || (!cls.isArray() && !Map.class.isAssignableFrom(cls) && !Collection.class.isAssignableFrom(cls))) {
            return javaType.narrowBy(cls);
        }
        if (!javaType.getRawClass().isAssignableFrom(cls)) {
            throw new IllegalArgumentException("Class " + cls.getClass().getName() + " not subtype of " + javaType);
        }
        JavaType _fromClass = _fromClass(cls, new TypeBindings(this, javaType.getRawClass()));
        Object valueHandler = javaType.getValueHandler();
        if (valueHandler != null) {
            _fromClass = _fromClass.withValueHandler(valueHandler);
        }
        Object typeHandler = javaType.getTypeHandler();
        return typeHandler != null ? _fromClass.withTypeHandler(typeHandler) : _fromClass;
    }

    public JavaType constructType(TypeReference<?> typeReference) {
        return _constructType(typeReference.getType(), null);
    }

    public JavaType constructType(Type type) {
        return _constructType(type, null);
    }

    public JavaType constructType(Type type, JavaType javaType) {
        return _constructType(type, javaType == null ? null : new TypeBindings(this, javaType));
    }

    public JavaType constructType(Type type, TypeBindings typeBindings) {
        return _constructType(type, typeBindings);
    }

    public JavaType constructType(Type type, Class<?> cls) {
        return _constructType(type, cls == null ? null : new TypeBindings(this, cls));
    }

    public JavaType[] findTypeParameters(JavaType javaType, Class<?> cls) {
        Class<?> rawClass = javaType.getRawClass();
        if (rawClass != cls) {
            return findTypeParameters(rawClass, cls, new TypeBindings(this, javaType));
        }
        int containedTypeCount = javaType.containedTypeCount();
        if (containedTypeCount == 0) {
            return null;
        }
        JavaType[] javaTypeArr = new JavaType[containedTypeCount];
        for (int i = 0; i < containedTypeCount; i++) {
            javaTypeArr[i] = javaType.containedType(i);
        }
        return javaTypeArr;
    }

    public JavaType[] findTypeParameters(Class<?> cls, Class<?> cls2) {
        return findTypeParameters(cls, cls2, new TypeBindings(this, cls));
    }

    public JavaType[] findTypeParameters(Class<?> cls, Class<?> cls2, TypeBindings typeBindings) {
        HierarchicType _findSuperTypeChain = _findSuperTypeChain(cls, cls2);
        if (_findSuperTypeChain == null) {
            throw new IllegalArgumentException("Class " + cls.getName() + " is not a subtype of " + cls2.getName());
        }
        while (_findSuperTypeChain.getSuperType() != null) {
            _findSuperTypeChain = _findSuperTypeChain.getSuperType();
            Class<?> rawClass = _findSuperTypeChain.getRawClass();
            TypeBindings typeBindings2 = new TypeBindings(this, rawClass);
            if (_findSuperTypeChain.isGeneric()) {
                Type[] actualTypeArguments = _findSuperTypeChain.asGeneric().getActualTypeArguments();
                TypeVariable[] typeParameters = rawClass.getTypeParameters();
                int length = actualTypeArguments.length;
                for (int i = 0; i < length; i++) {
                    typeBindings2.addBinding(typeParameters[i].getName(), instance._constructType(actualTypeArguments[i], typeBindings));
                }
            }
            typeBindings = typeBindings2;
        }
        if (!_findSuperTypeChain.isGeneric()) {
            return null;
        }
        return typeBindings.typesAsArray();
    }

    public JavaType uncheckedSimpleType(Class<?> cls) {
        return new SimpleType(cls);
    }

    public TypeFactory withModifier(TypeModifier typeModifier) {
        if (this._modifiers != null) {
            return new TypeFactory(this._parser, (TypeModifier[]) ArrayBuilders.insertInListNoDup(this._modifiers, typeModifier));
        }
        return new TypeFactory(this._parser, new TypeModifier[]{typeModifier});
    }
}
