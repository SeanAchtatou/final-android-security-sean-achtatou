package com.fasterxml.jackson.databind;

import com.fasterxml.jackson.core.FormatSchema;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.core.Versioned;
import com.fasterxml.jackson.core.type.ResolvedType;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.cfg.DatabindVersion;
import com.fasterxml.jackson.databind.deser.DefaultDeserializationContext;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.NullNode;
import com.fasterxml.jackson.databind.node.TreeTraversingParser;
import com.fasterxml.jackson.databind.type.SimpleType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.databind.util.RootNameLookup;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.Iterator;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;

public class ObjectReader extends ObjectCodec implements Versioned {
    private static final JavaType JSON_NODE_TYPE = SimpleType.constructUnsafe(JsonNode.class);
    protected final DeserializationConfig _config;
    protected final DefaultDeserializationContext _context;
    protected final InjectableValues _injectableValues;
    protected final JsonFactory _jsonFactory;
    protected final ConcurrentHashMap<JavaType, JsonDeserializer<Object>> _rootDeserializers;
    protected final RootNameLookup _rootNames;
    protected final FormatSchema _schema;
    protected final boolean _unwrapRoot;
    protected final Object _valueToUpdate;
    protected final JavaType _valueType;

    protected ObjectReader(ObjectMapper objectMapper, DeserializationConfig deserializationConfig) {
        this(objectMapper, deserializationConfig, (JavaType) null, (Object) null, (FormatSchema) null, (InjectableValues) null);
    }

    protected ObjectReader(ObjectMapper objectMapper, DeserializationConfig deserializationConfig, JavaType javaType, Object obj, FormatSchema formatSchema, InjectableValues injectableValues) {
        this._config = deserializationConfig;
        this._context = objectMapper._deserializationContext;
        this._rootDeserializers = objectMapper._rootDeserializers;
        this._jsonFactory = objectMapper._jsonFactory;
        this._rootNames = objectMapper._rootNames;
        this._valueType = javaType;
        this._valueToUpdate = obj;
        if (obj == null || !javaType.isArrayType()) {
            this._schema = formatSchema;
            this._injectableValues = injectableValues;
            this._unwrapRoot = deserializationConfig.useRootWrapping();
            return;
        }
        throw new IllegalArgumentException("Can not update an array value");
    }

    protected ObjectReader(ObjectReader objectReader, DeserializationConfig deserializationConfig) {
        this._config = deserializationConfig;
        this._context = objectReader._context;
        this._rootDeserializers = objectReader._rootDeserializers;
        this._jsonFactory = objectReader._jsonFactory;
        this._rootNames = objectReader._rootNames;
        this._valueType = objectReader._valueType;
        this._valueToUpdate = objectReader._valueToUpdate;
        this._schema = objectReader._schema;
        this._injectableValues = objectReader._injectableValues;
        this._unwrapRoot = deserializationConfig.useRootWrapping();
    }

    protected ObjectReader(ObjectReader objectReader, DeserializationConfig deserializationConfig, JavaType javaType, Object obj, FormatSchema formatSchema, InjectableValues injectableValues) {
        this._config = deserializationConfig;
        this._context = objectReader._context;
        this._rootDeserializers = objectReader._rootDeserializers;
        this._jsonFactory = objectReader._jsonFactory;
        this._rootNames = objectReader._rootNames;
        this._valueType = javaType;
        this._valueToUpdate = obj;
        if (obj == null || !javaType.isArrayType()) {
            this._schema = formatSchema;
            this._injectableValues = injectableValues;
            this._unwrapRoot = deserializationConfig.useRootWrapping();
            return;
        }
        throw new IllegalArgumentException("Can not update an array value");
    }

    protected static JsonToken _initForReading(JsonParser jsonParser) {
        JsonToken currentToken = jsonParser.getCurrentToken();
        if (currentToken != null || (currentToken = jsonParser.nextToken()) != null) {
            return currentToken;
        }
        throw JsonMappingException.from(jsonParser, "No content to map due to end-of-input");
    }

    /* access modifiers changed from: protected */
    public Object _bind(JsonParser jsonParser) {
        Object obj;
        JsonToken _initForReading = _initForReading(jsonParser);
        if (_initForReading == JsonToken.VALUE_NULL) {
            obj = this._valueToUpdate == null ? _findRootDeserializer(createDeserializationContext(jsonParser, this._config), this._valueType).getNullValue() : this._valueToUpdate;
        } else if (_initForReading == JsonToken.END_ARRAY || _initForReading == JsonToken.END_OBJECT) {
            obj = this._valueToUpdate;
        } else {
            DefaultDeserializationContext createDeserializationContext = createDeserializationContext(jsonParser, this._config);
            JsonDeserializer<Object> _findRootDeserializer = _findRootDeserializer(createDeserializationContext, this._valueType);
            if (this._unwrapRoot) {
                obj = _unwrapAndDeserialize(jsonParser, createDeserializationContext, this._valueType, _findRootDeserializer);
            } else if (this._valueToUpdate == null) {
                obj = _findRootDeserializer.deserialize(jsonParser, createDeserializationContext);
            } else {
                _findRootDeserializer.deserialize(jsonParser, createDeserializationContext, this._valueToUpdate);
                obj = this._valueToUpdate;
            }
        }
        jsonParser.clearCurrentToken();
        return obj;
    }

    /* access modifiers changed from: protected */
    public Object _bindAndClose(JsonParser jsonParser) {
        Object obj;
        if (this._schema != null) {
            jsonParser.setSchema(this._schema);
        }
        try {
            JsonToken _initForReading = _initForReading(jsonParser);
            if (_initForReading == JsonToken.VALUE_NULL) {
                obj = this._valueToUpdate == null ? _findRootDeserializer(createDeserializationContext(jsonParser, this._config), this._valueType).getNullValue() : this._valueToUpdate;
            } else if (_initForReading == JsonToken.END_ARRAY || _initForReading == JsonToken.END_OBJECT) {
                obj = this._valueToUpdate;
            } else {
                DefaultDeserializationContext createDeserializationContext = createDeserializationContext(jsonParser, this._config);
                JsonDeserializer<Object> _findRootDeserializer = _findRootDeserializer(createDeserializationContext, this._valueType);
                if (this._unwrapRoot) {
                    obj = _unwrapAndDeserialize(jsonParser, createDeserializationContext, this._valueType, _findRootDeserializer);
                } else if (this._valueToUpdate == null) {
                    obj = _findRootDeserializer.deserialize(jsonParser, createDeserializationContext);
                } else {
                    _findRootDeserializer.deserialize(jsonParser, createDeserializationContext, this._valueToUpdate);
                    obj = this._valueToUpdate;
                }
            }
            return obj;
        } finally {
            try {
                jsonParser.close();
            } catch (IOException e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public JsonNode _bindAndCloseAsTree(JsonParser jsonParser) {
        if (this._schema != null) {
            jsonParser.setSchema(this._schema);
        }
        try {
            return _bindAsTree(jsonParser);
        } finally {
            try {
                jsonParser.close();
            } catch (IOException e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public JsonNode _bindAsTree(JsonParser jsonParser) {
        JsonNode jsonNode;
        JsonToken _initForReading = _initForReading(jsonParser);
        if (_initForReading == JsonToken.VALUE_NULL || _initForReading == JsonToken.END_ARRAY || _initForReading == JsonToken.END_OBJECT) {
            jsonNode = NullNode.instance;
        } else {
            DefaultDeserializationContext createDeserializationContext = createDeserializationContext(jsonParser, this._config);
            JsonDeserializer<Object> _findRootDeserializer = _findRootDeserializer(createDeserializationContext, JSON_NODE_TYPE);
            jsonNode = this._unwrapRoot ? (JsonNode) _unwrapAndDeserialize(jsonParser, createDeserializationContext, JSON_NODE_TYPE, _findRootDeserializer) : (JsonNode) _findRootDeserializer.deserialize(jsonParser, createDeserializationContext);
        }
        jsonParser.clearCurrentToken();
        return jsonNode;
    }

    /* access modifiers changed from: protected */
    public JsonDeserializer<Object> _findRootDeserializer(DeserializationContext deserializationContext, JavaType javaType) {
        if (javaType == null) {
            throw new JsonMappingException("No value type configured for ObjectReader");
        }
        JsonDeserializer<Object> jsonDeserializer = this._rootDeserializers.get(javaType);
        if (jsonDeserializer == null) {
            jsonDeserializer = deserializationContext.findRootValueDeserializer(javaType);
            if (jsonDeserializer == null) {
                throw new JsonMappingException("Can not find a deserializer for type " + javaType);
            }
            this._rootDeserializers.put(javaType, jsonDeserializer);
        }
        return jsonDeserializer;
    }

    /* access modifiers changed from: protected */
    public Object _unwrapAndDeserialize(JsonParser jsonParser, DeserializationContext deserializationContext, JavaType javaType, JsonDeserializer<Object> jsonDeserializer) {
        Object obj;
        String rootName = this._config.getRootName();
        if (rootName == null) {
            rootName = this._rootNames.findRootName(javaType, this._config).getValue();
        }
        if (jsonParser.getCurrentToken() != JsonToken.START_OBJECT) {
            throw JsonMappingException.from(jsonParser, "Current token not START_OBJECT (needed to unwrap root name '" + rootName + "'), but " + jsonParser.getCurrentToken());
        } else if (jsonParser.nextToken() != JsonToken.FIELD_NAME) {
            throw JsonMappingException.from(jsonParser, "Current token not FIELD_NAME (to contain expected root name '" + rootName + "'), but " + jsonParser.getCurrentToken());
        } else {
            String currentName = jsonParser.getCurrentName();
            if (!rootName.equals(currentName)) {
                throw JsonMappingException.from(jsonParser, "Root name '" + currentName + "' does not match expected ('" + rootName + "') for type " + javaType);
            }
            jsonParser.nextToken();
            if (this._valueToUpdate == null) {
                obj = jsonDeserializer.deserialize(jsonParser, deserializationContext);
            } else {
                jsonDeserializer.deserialize(jsonParser, deserializationContext, this._valueToUpdate);
                obj = this._valueToUpdate;
            }
            if (jsonParser.nextToken() == JsonToken.END_OBJECT) {
                return obj;
            }
            throw JsonMappingException.from(jsonParser, "Current token not END_OBJECT (to match wrapper object with root name '" + rootName + "'), but " + jsonParser.getCurrentToken());
        }
    }

    public JsonNode createArrayNode() {
        return this._config.getNodeFactory().arrayNode();
    }

    /* access modifiers changed from: protected */
    public final DefaultDeserializationContext createDeserializationContext(JsonParser jsonParser, DeserializationConfig deserializationConfig) {
        return this._context.createInstance(deserializationConfig, jsonParser, this._injectableValues);
    }

    public JsonNode createObjectNode() {
        return this._config.getNodeFactory().objectNode();
    }

    public JsonFactory getJsonFactory() {
        return this._jsonFactory;
    }

    public TypeFactory getTypeFactory() {
        return this._config.getTypeFactory();
    }

    public boolean isEnabled(JsonParser.Feature feature) {
        return this._jsonFactory.isEnabled(feature);
    }

    public boolean isEnabled(DeserializationFeature deserializationFeature) {
        return this._config.isEnabled(deserializationFeature);
    }

    public boolean isEnabled(MapperFeature mapperFeature) {
        return this._config.isEnabled(mapperFeature);
    }

    public <T extends TreeNode> T readTree(JsonParser jsonParser) {
        return _bindAsTree(jsonParser);
    }

    public JsonNode readTree(InputStream inputStream) {
        return _bindAndCloseAsTree(this._jsonFactory.createJsonParser(inputStream));
    }

    public JsonNode readTree(Reader reader) {
        return _bindAndCloseAsTree(this._jsonFactory.createJsonParser(reader));
    }

    public JsonNode readTree(String str) {
        return _bindAndCloseAsTree(this._jsonFactory.createJsonParser(str));
    }

    public <T> T readValue(JsonParser jsonParser) {
        return _bind(jsonParser);
    }

    public <T> T readValue(JsonParser jsonParser, ResolvedType resolvedType) {
        return withType((JavaType) resolvedType).readValue(jsonParser);
    }

    public <T> T readValue(JsonParser jsonParser, TypeReference<?> typeReference) {
        return withType(typeReference).readValue(jsonParser);
    }

    public <T> T readValue(JsonParser jsonParser, JavaType javaType) {
        return withType(javaType).readValue(jsonParser);
    }

    public <T> T readValue(JsonParser jsonParser, Class<T> cls) {
        return withType((Class<?>) cls).readValue(jsonParser);
    }

    public <T> T readValue(JsonNode jsonNode) {
        return _bindAndClose(treeAsTokens(jsonNode));
    }

    public <T> T readValue(File file) {
        return _bindAndClose(this._jsonFactory.createJsonParser(file));
    }

    public <T> T readValue(InputStream inputStream) {
        return _bindAndClose(this._jsonFactory.createJsonParser(inputStream));
    }

    public <T> T readValue(Reader reader) {
        return _bindAndClose(this._jsonFactory.createJsonParser(reader));
    }

    public <T> T readValue(String str) {
        return _bindAndClose(this._jsonFactory.createJsonParser(str));
    }

    public <T> T readValue(URL url) {
        return _bindAndClose(this._jsonFactory.createJsonParser(url));
    }

    public <T> T readValue(byte[] bArr) {
        return _bindAndClose(this._jsonFactory.createJsonParser(bArr));
    }

    public <T> T readValue(byte[] bArr, int i, int i2) {
        return _bindAndClose(this._jsonFactory.createJsonParser(bArr, i, i2));
    }

    public <T> MappingIterator<T> readValues(JsonParser jsonParser) {
        DefaultDeserializationContext createDeserializationContext = createDeserializationContext(jsonParser, this._config);
        return new MappingIterator<>(this._valueType, jsonParser, createDeserializationContext, _findRootDeserializer(createDeserializationContext, this._valueType), false, this._valueToUpdate);
    }

    public <T> MappingIterator<T> readValues(File file) {
        JsonParser createJsonParser = this._jsonFactory.createJsonParser(file);
        if (this._schema != null) {
            createJsonParser.setSchema(this._schema);
        }
        DefaultDeserializationContext createDeserializationContext = createDeserializationContext(createJsonParser, this._config);
        return new MappingIterator<>(this._valueType, createJsonParser, createDeserializationContext, _findRootDeserializer(createDeserializationContext, this._valueType), true, this._valueToUpdate);
    }

    public <T> MappingIterator<T> readValues(InputStream inputStream) {
        JsonParser createJsonParser = this._jsonFactory.createJsonParser(inputStream);
        if (this._schema != null) {
            createJsonParser.setSchema(this._schema);
        }
        DefaultDeserializationContext createDeserializationContext = createDeserializationContext(createJsonParser, this._config);
        return new MappingIterator<>(this._valueType, createJsonParser, createDeserializationContext, _findRootDeserializer(createDeserializationContext, this._valueType), true, this._valueToUpdate);
    }

    public <T> MappingIterator<T> readValues(Reader reader) {
        JsonParser createJsonParser = this._jsonFactory.createJsonParser(reader);
        if (this._schema != null) {
            createJsonParser.setSchema(this._schema);
        }
        DefaultDeserializationContext createDeserializationContext = createDeserializationContext(createJsonParser, this._config);
        return new MappingIterator<>(this._valueType, createJsonParser, createDeserializationContext, _findRootDeserializer(createDeserializationContext, this._valueType), true, this._valueToUpdate);
    }

    public <T> MappingIterator<T> readValues(String str) {
        JsonParser createJsonParser = this._jsonFactory.createJsonParser(str);
        if (this._schema != null) {
            createJsonParser.setSchema(this._schema);
        }
        DefaultDeserializationContext createDeserializationContext = createDeserializationContext(createJsonParser, this._config);
        return new MappingIterator<>(this._valueType, createJsonParser, createDeserializationContext, _findRootDeserializer(createDeserializationContext, this._valueType), true, this._valueToUpdate);
    }

    public <T> MappingIterator<T> readValues(URL url) {
        JsonParser createJsonParser = this._jsonFactory.createJsonParser(url);
        if (this._schema != null) {
            createJsonParser.setSchema(this._schema);
        }
        DefaultDeserializationContext createDeserializationContext = createDeserializationContext(createJsonParser, this._config);
        return new MappingIterator<>(this._valueType, createJsonParser, createDeserializationContext, _findRootDeserializer(createDeserializationContext, this._valueType), true, this._valueToUpdate);
    }

    public final <T> MappingIterator<T> readValues(byte[] bArr) {
        return readValues(bArr, 0, bArr.length);
    }

    public <T> MappingIterator<T> readValues(byte[] bArr, int i, int i2) {
        JsonParser createJsonParser = this._jsonFactory.createJsonParser(bArr, i, i2);
        if (this._schema != null) {
            createJsonParser.setSchema(this._schema);
        }
        DefaultDeserializationContext createDeserializationContext = createDeserializationContext(createJsonParser, this._config);
        return new MappingIterator<>(this._valueType, createJsonParser, createDeserializationContext, _findRootDeserializer(createDeserializationContext, this._valueType), true, this._valueToUpdate);
    }

    public <T> Iterator<T> readValues(JsonParser jsonParser, ResolvedType resolvedType) {
        return readValues(jsonParser, (JavaType) resolvedType);
    }

    public <T> Iterator<T> readValues(JsonParser jsonParser, TypeReference<?> typeReference) {
        return withType(typeReference).readValues(jsonParser);
    }

    public <T> Iterator<T> readValues(JsonParser jsonParser, JavaType javaType) {
        return withType(javaType).readValues(jsonParser);
    }

    public <T> Iterator<T> readValues(JsonParser jsonParser, Class<T> cls) {
        return withType((Class<?>) cls).readValues(jsonParser);
    }

    public JsonParser treeAsTokens(TreeNode treeNode) {
        return new TreeTraversingParser((JsonNode) treeNode, this);
    }

    public <T> T treeToValue(TreeNode treeNode, Class<T> cls) {
        try {
            return readValue(treeAsTokens(treeNode), cls);
        } catch (JsonProcessingException e) {
            throw e;
        } catch (IOException e2) {
            throw new IllegalArgumentException(e2.getMessage(), e2);
        }
    }

    public Version version() {
        return DatabindVersion.instance.version();
    }

    public ObjectReader with(FormatSchema formatSchema) {
        if (this._schema == formatSchema) {
            return this;
        }
        return new ObjectReader(this, this._config, this._valueType, this._valueToUpdate, formatSchema, this._injectableValues);
    }

    public ObjectReader with(DeserializationFeature deserializationFeature) {
        DeserializationConfig with = this._config.with(deserializationFeature);
        return with == this._config ? this : new ObjectReader(this, with);
    }

    public ObjectReader with(DeserializationFeature deserializationFeature, DeserializationFeature... deserializationFeatureArr) {
        DeserializationConfig with = this._config.with(deserializationFeature, deserializationFeatureArr);
        return with == this._config ? this : new ObjectReader(this, with);
    }

    public ObjectReader with(InjectableValues injectableValues) {
        return this._injectableValues == injectableValues ? this : new ObjectReader(this, this._config, this._valueType, this._valueToUpdate, this._schema, injectableValues);
    }

    public ObjectReader with(JsonNodeFactory jsonNodeFactory) {
        DeserializationConfig with = this._config.with(jsonNodeFactory);
        return with == this._config ? this : new ObjectReader(this, with);
    }

    public ObjectReader with(Locale locale) {
        DeserializationConfig with = this._config.with(locale);
        return with == this._config ? this : new ObjectReader(this, with);
    }

    public ObjectReader with(TimeZone timeZone) {
        DeserializationConfig with = this._config.with(timeZone);
        return with == this._config ? this : new ObjectReader(this, with);
    }

    public ObjectReader withFeatures(DeserializationFeature... deserializationFeatureArr) {
        DeserializationConfig withFeatures = this._config.withFeatures(deserializationFeatureArr);
        return withFeatures == this._config ? this : new ObjectReader(this, withFeatures);
    }

    public ObjectReader withRootName(String str) {
        DeserializationConfig withRootName = this._config.withRootName(str);
        return withRootName == this._config ? this : new ObjectReader(this, withRootName);
    }

    public ObjectReader withType(TypeReference<?> typeReference) {
        return withType(this._config.getTypeFactory().constructType(typeReference.getType()));
    }

    public ObjectReader withType(JavaType javaType) {
        if (javaType != null && javaType.equals(this._valueType)) {
            return this;
        }
        return new ObjectReader(this, this._config, javaType, this._valueToUpdate, this._schema, this._injectableValues);
    }

    public ObjectReader withType(Class<?> cls) {
        return withType(this._config.constructType(cls));
    }

    public ObjectReader withType(Type type) {
        return withType(this._config.getTypeFactory().constructType(type));
    }

    public ObjectReader withValueToUpdate(Object obj) {
        if (obj == this._valueToUpdate) {
            return this;
        }
        if (obj == null) {
            throw new IllegalArgumentException("cat not update null value");
        }
        return new ObjectReader(this, this._config, this._valueType == null ? this._config.constructType(obj.getClass()) : this._valueType, obj, this._schema, this._injectableValues);
    }

    public ObjectReader withView(Class<?> cls) {
        DeserializationConfig withView = this._config.withView(cls);
        return withView == this._config ? this : new ObjectReader(this, withView);
    }

    public ObjectReader without(DeserializationFeature deserializationFeature) {
        DeserializationConfig without = this._config.without(deserializationFeature);
        return without == this._config ? this : new ObjectReader(this, without);
    }

    public ObjectReader without(DeserializationFeature deserializationFeature, DeserializationFeature... deserializationFeatureArr) {
        DeserializationConfig without = this._config.without(deserializationFeature, deserializationFeatureArr);
        return without == this._config ? this : new ObjectReader(this, without);
    }

    public ObjectReader withoutFeatures(DeserializationFeature... deserializationFeatureArr) {
        DeserializationConfig withoutFeatures = this._config.withoutFeatures(deserializationFeatureArr);
        return withoutFeatures == this._config ? this : new ObjectReader(this, withoutFeatures);
    }

    public void writeValue(JsonGenerator jsonGenerator, Object obj) {
        throw new UnsupportedOperationException("Not implemented for ObjectReader");
    }
}
