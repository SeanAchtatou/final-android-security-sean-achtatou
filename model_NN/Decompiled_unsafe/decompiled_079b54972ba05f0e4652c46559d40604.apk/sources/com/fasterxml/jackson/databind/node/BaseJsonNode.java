package com.fasterxml.jackson.databind.node;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializable;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import java.util.List;

public abstract class BaseJsonNode extends JsonNode implements JsonSerializable {
    protected BaseJsonNode() {
    }

    public abstract JsonToken asToken();

    public ObjectNode findParent(String str) {
        return null;
    }

    public List<JsonNode> findParents(String str, List<JsonNode> list) {
        return list;
    }

    public final JsonNode findPath(String str) {
        JsonNode findValue = findValue(str);
        return findValue == null ? MissingNode.getInstance() : findValue;
    }

    public JsonNode findValue(String str) {
        return null;
    }

    public List<JsonNode> findValues(String str, List<JsonNode> list) {
        return list;
    }

    public List<String> findValuesAsText(String str, List<String> list) {
        return list;
    }

    public JsonParser.NumberType numberType() {
        return null;
    }

    public abstract void serialize(JsonGenerator jsonGenerator, SerializerProvider serializerProvider);

    public abstract void serializeWithType(JsonGenerator jsonGenerator, SerializerProvider serializerProvider, TypeSerializer typeSerializer);

    public JsonParser traverse() {
        return new TreeTraversingParser(this);
    }
}
