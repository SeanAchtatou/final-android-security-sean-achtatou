package com.fasterxml.jackson.databind.ser;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.impl.ReadOnlyClassToSerializerMap;
import java.util.HashMap;

public final class SerializerCache {
    private ReadOnlyClassToSerializerMap _readOnlyMap = null;
    private HashMap<TypeKey, JsonSerializer<Object>> _sharedMap = new HashMap<>(64);

    public static final class TypeKey {
        protected Class<?> _class;
        protected int _hashCode;
        protected boolean _isTyped;
        protected JavaType _type;

        public TypeKey(JavaType javaType, boolean z) {
            this._type = javaType;
            this._class = null;
            this._isTyped = z;
            this._hashCode = hash(javaType, z);
        }

        public TypeKey(Class<?> cls, boolean z) {
            this._class = cls;
            this._type = null;
            this._isTyped = z;
            this._hashCode = hash(cls, z);
        }

        private static final int hash(JavaType javaType, boolean z) {
            int hashCode = javaType.hashCode() - 1;
            return z ? hashCode - 1 : hashCode;
        }

        private static final int hash(Class<?> cls, boolean z) {
            int hashCode = cls.getName().hashCode();
            return z ? hashCode + 1 : hashCode;
        }

        public final boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            TypeKey typeKey = (TypeKey) obj;
            if (typeKey._isTyped == this._isTyped) {
                return this._class != null ? typeKey._class == this._class : this._type.equals(typeKey._type);
            }
            return false;
        }

        public final int hashCode() {
            return this._hashCode;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.hash(com.fasterxml.jackson.databind.JavaType, boolean):int
         arg types: [com.fasterxml.jackson.databind.JavaType, int]
         candidates:
          com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.hash(java.lang.Class<?>, boolean):int
          com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.hash(com.fasterxml.jackson.databind.JavaType, boolean):int */
        public void resetTyped(JavaType javaType) {
            this._type = javaType;
            this._class = null;
            this._isTyped = true;
            this._hashCode = hash(javaType, true);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.hash(java.lang.Class<?>, boolean):int
         arg types: [java.lang.Class<?>, int]
         candidates:
          com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.hash(com.fasterxml.jackson.databind.JavaType, boolean):int
          com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.hash(java.lang.Class<?>, boolean):int */
        public void resetTyped(Class<?> cls) {
            this._type = null;
            this._class = cls;
            this._isTyped = true;
            this._hashCode = hash(cls, true);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.hash(com.fasterxml.jackson.databind.JavaType, boolean):int
         arg types: [com.fasterxml.jackson.databind.JavaType, int]
         candidates:
          com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.hash(java.lang.Class<?>, boolean):int
          com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.hash(com.fasterxml.jackson.databind.JavaType, boolean):int */
        public void resetUntyped(JavaType javaType) {
            this._type = javaType;
            this._class = null;
            this._isTyped = false;
            this._hashCode = hash(javaType, false);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.hash(java.lang.Class<?>, boolean):int
         arg types: [java.lang.Class<?>, int]
         candidates:
          com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.hash(com.fasterxml.jackson.databind.JavaType, boolean):int
          com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.hash(java.lang.Class<?>, boolean):int */
        public void resetUntyped(Class<?> cls) {
            this._type = null;
            this._class = cls;
            this._isTyped = false;
            this._hashCode = hash(cls, false);
        }

        public final String toString() {
            return this._class != null ? "{class: " + this._class.getName() + ", typed? " + this._isTyped + "}" : "{type: " + this._type + ", typed? " + this._isTyped + "}";
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(com.fasterxml.jackson.databind.JavaType, boolean):void
     arg types: [com.fasterxml.jackson.databind.JavaType, int]
     candidates:
      com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void
      com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(com.fasterxml.jackson.databind.JavaType, boolean):void */
    public void addAndResolveNonTypedSerializer(JavaType javaType, JsonSerializer<Object> jsonSerializer, SerializerProvider serializerProvider) {
        synchronized (this) {
            if (this._sharedMap.put(new TypeKey(javaType, false), jsonSerializer) == null) {
                this._readOnlyMap = null;
            }
            if (jsonSerializer instanceof ResolvableSerializer) {
                ((ResolvableSerializer) jsonSerializer).resolve(serializerProvider);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void
     arg types: [java.lang.Class<?>, int]
     candidates:
      com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(com.fasterxml.jackson.databind.JavaType, boolean):void
      com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void */
    public void addAndResolveNonTypedSerializer(Class<?> cls, JsonSerializer<Object> jsonSerializer, SerializerProvider serializerProvider) {
        synchronized (this) {
            if (this._sharedMap.put(new TypeKey(cls, false), jsonSerializer) == null) {
                this._readOnlyMap = null;
            }
            if (jsonSerializer instanceof ResolvableSerializer) {
                ((ResolvableSerializer) jsonSerializer).resolve(serializerProvider);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(com.fasterxml.jackson.databind.JavaType, boolean):void
     arg types: [com.fasterxml.jackson.databind.JavaType, int]
     candidates:
      com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void
      com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(com.fasterxml.jackson.databind.JavaType, boolean):void */
    public void addTypedSerializer(JavaType javaType, JsonSerializer<Object> jsonSerializer) {
        synchronized (this) {
            if (this._sharedMap.put(new TypeKey(javaType, true), jsonSerializer) == null) {
                this._readOnlyMap = null;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void
     arg types: [java.lang.Class<?>, int]
     candidates:
      com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(com.fasterxml.jackson.databind.JavaType, boolean):void
      com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void */
    public void addTypedSerializer(Class<?> cls, JsonSerializer<Object> jsonSerializer) {
        synchronized (this) {
            if (this._sharedMap.put(new TypeKey(cls, true), jsonSerializer) == null) {
                this._readOnlyMap = null;
            }
        }
    }

    public synchronized void flush() {
        this._sharedMap.clear();
    }

    public ReadOnlyClassToSerializerMap getReadOnlyLookupMap() {
        ReadOnlyClassToSerializerMap readOnlyClassToSerializerMap;
        synchronized (this) {
            readOnlyClassToSerializerMap = this._readOnlyMap;
            if (readOnlyClassToSerializerMap == null) {
                readOnlyClassToSerializerMap = ReadOnlyClassToSerializerMap.from(this._sharedMap);
                this._readOnlyMap = readOnlyClassToSerializerMap;
            }
        }
        return readOnlyClassToSerializerMap.instance();
    }

    public synchronized int size() {
        return this._sharedMap.size();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(com.fasterxml.jackson.databind.JavaType, boolean):void
     arg types: [com.fasterxml.jackson.databind.JavaType, int]
     candidates:
      com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void
      com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(com.fasterxml.jackson.databind.JavaType, boolean):void */
    public JsonSerializer<Object> typedValueSerializer(JavaType javaType) {
        JsonSerializer<Object> jsonSerializer;
        synchronized (this) {
            jsonSerializer = this._sharedMap.get(new TypeKey(javaType, true));
        }
        return jsonSerializer;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void
     arg types: [java.lang.Class<?>, int]
     candidates:
      com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(com.fasterxml.jackson.databind.JavaType, boolean):void
      com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void */
    public JsonSerializer<Object> typedValueSerializer(Class<?> cls) {
        JsonSerializer<Object> jsonSerializer;
        synchronized (this) {
            jsonSerializer = this._sharedMap.get(new TypeKey(cls, true));
        }
        return jsonSerializer;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(com.fasterxml.jackson.databind.JavaType, boolean):void
     arg types: [com.fasterxml.jackson.databind.JavaType, int]
     candidates:
      com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void
      com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(com.fasterxml.jackson.databind.JavaType, boolean):void */
    public JsonSerializer<Object> untypedValueSerializer(JavaType javaType) {
        JsonSerializer<Object> jsonSerializer;
        synchronized (this) {
            jsonSerializer = this._sharedMap.get(new TypeKey(javaType, false));
        }
        return jsonSerializer;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void
     arg types: [java.lang.Class<?>, int]
     candidates:
      com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(com.fasterxml.jackson.databind.JavaType, boolean):void
      com.fasterxml.jackson.databind.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void */
    public JsonSerializer<Object> untypedValueSerializer(Class<?> cls) {
        JsonSerializer<Object> jsonSerializer;
        synchronized (this) {
            jsonSerializer = this._sharedMap.get(new TypeKey(cls, false));
        }
        return jsonSerializer;
    }
}
