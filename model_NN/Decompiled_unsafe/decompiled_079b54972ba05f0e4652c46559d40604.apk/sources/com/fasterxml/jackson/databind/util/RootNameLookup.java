package com.fasterxml.jackson.databind.util;

import com.fasterxml.jackson.core.io.SerializedString;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.type.ClassKey;

public class RootNameLookup {
    protected LRUMap<ClassKey, SerializedString> _rootNames;

    public SerializedString findRootName(JavaType javaType, MapperConfig<?> mapperConfig) {
        return findRootName(javaType.getRawClass(), mapperConfig);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0046, code lost:
        if (r0 != null) goto L_0x003c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.fasterxml.jackson.core.io.SerializedString findRootName(java.lang.Class<?> r5, com.fasterxml.jackson.databind.cfg.MapperConfig<?> r6) {
        /*
            r4 = this;
            monitor-enter(r4)
            com.fasterxml.jackson.databind.type.ClassKey r2 = new com.fasterxml.jackson.databind.type.ClassKey     // Catch:{ all -> 0x0049 }
            r2.<init>(r5)     // Catch:{ all -> 0x0049 }
            com.fasterxml.jackson.databind.util.LRUMap<com.fasterxml.jackson.databind.type.ClassKey, com.fasterxml.jackson.core.io.SerializedString> r0 = r4._rootNames     // Catch:{ all -> 0x0049 }
            if (r0 != 0) goto L_0x003e
            com.fasterxml.jackson.databind.util.LRUMap r0 = new com.fasterxml.jackson.databind.util.LRUMap     // Catch:{ all -> 0x0049 }
            r1 = 20
            r3 = 200(0xc8, float:2.8E-43)
            r0.<init>(r1, r3)     // Catch:{ all -> 0x0049 }
            r4._rootNames = r0     // Catch:{ all -> 0x0049 }
        L_0x0015:
            com.fasterxml.jackson.databind.BeanDescription r0 = r6.introspectClassAnnotations(r5)     // Catch:{ all -> 0x0049 }
            com.fasterxml.jackson.databind.AnnotationIntrospector r1 = r6.getAnnotationIntrospector()     // Catch:{ all -> 0x0049 }
            com.fasterxml.jackson.databind.introspect.AnnotatedClass r0 = r0.getClassInfo()     // Catch:{ all -> 0x0049 }
            java.lang.String r0 = r1.findRootName(r0)     // Catch:{ all -> 0x0049 }
            if (r0 == 0) goto L_0x002d
            int r1 = r0.length()     // Catch:{ all -> 0x0049 }
            if (r1 != 0) goto L_0x004c
        L_0x002d:
            java.lang.String r0 = r5.getSimpleName()     // Catch:{ all -> 0x0049 }
            r1 = r0
        L_0x0032:
            com.fasterxml.jackson.core.io.SerializedString r0 = new com.fasterxml.jackson.core.io.SerializedString     // Catch:{ all -> 0x0049 }
            r0.<init>(r1)     // Catch:{ all -> 0x0049 }
            com.fasterxml.jackson.databind.util.LRUMap<com.fasterxml.jackson.databind.type.ClassKey, com.fasterxml.jackson.core.io.SerializedString> r1 = r4._rootNames     // Catch:{ all -> 0x0049 }
            r1.put(r2, r0)     // Catch:{ all -> 0x0049 }
        L_0x003c:
            monitor-exit(r4)
            return r0
        L_0x003e:
            com.fasterxml.jackson.databind.util.LRUMap<com.fasterxml.jackson.databind.type.ClassKey, com.fasterxml.jackson.core.io.SerializedString> r0 = r4._rootNames     // Catch:{ all -> 0x0049 }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ all -> 0x0049 }
            com.fasterxml.jackson.core.io.SerializedString r0 = (com.fasterxml.jackson.core.io.SerializedString) r0     // Catch:{ all -> 0x0049 }
            if (r0 == 0) goto L_0x0015
            goto L_0x003c
        L_0x0049:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x004c:
            r1 = r0
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.util.RootNameLookup.findRootName(java.lang.Class, com.fasterxml.jackson.databind.cfg.MapperConfig):com.fasterxml.jackson.core.io.SerializedString");
    }
}
