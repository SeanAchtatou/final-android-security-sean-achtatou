package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.core.Base64Variants;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.annotation.JacksonStdImpl;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import com.fasterxml.jackson.databind.jsontype.TypeDeserializer;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.net.InetAddress;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Currency;
import java.util.Locale;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;

public class JdkDeserializers {

    public static class AtomicBooleanDeserializer extends StdScalarDeserializer<AtomicBoolean> {
        public AtomicBooleanDeserializer() {
            super(AtomicBoolean.class);
        }

        public AtomicBoolean deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
            return new AtomicBoolean(_parseBooleanPrimitive(jsonParser, deserializationContext));
        }
    }

    public static class AtomicReferenceDeserializer extends StdScalarDeserializer<AtomicReference<?>> implements ContextualDeserializer {
        protected final JavaType _referencedType;
        protected final JsonDeserializer<?> _valueDeserializer;

        public AtomicReferenceDeserializer(JavaType javaType) {
            this(javaType, null);
        }

        public AtomicReferenceDeserializer(JavaType javaType, JsonDeserializer<?> jsonDeserializer) {
            super(AtomicReference.class);
            this._referencedType = javaType;
            this._valueDeserializer = jsonDeserializer;
        }

        public JsonDeserializer<?> createContextual(DeserializationContext deserializationContext, BeanProperty beanProperty) {
            return this._valueDeserializer != null ? this : new AtomicReferenceDeserializer(this._referencedType, deserializationContext.findContextualValueDeserializer(this._referencedType, beanProperty));
        }

        public AtomicReference<?> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
            return new AtomicReference<>(this._valueDeserializer.deserialize(jsonParser, deserializationContext));
        }
    }

    protected static class CharsetDeserializer extends FromStringDeserializer<Charset> {
        public CharsetDeserializer() {
            super(Charset.class);
        }

        /* access modifiers changed from: protected */
        public Charset _deserialize(String str, DeserializationContext deserializationContext) {
            return Charset.forName(str);
        }
    }

    public static class CurrencyDeserializer extends FromStringDeserializer<Currency> {
        public CurrencyDeserializer() {
            super(Currency.class);
        }

        /* access modifiers changed from: protected */
        public Currency _deserialize(String str, DeserializationContext deserializationContext) {
            return Currency.getInstance(str);
        }
    }

    protected static class InetAddressDeserializer extends FromStringDeserializer<InetAddress> {
        public InetAddressDeserializer() {
            super(InetAddress.class);
        }

        /* access modifiers changed from: protected */
        public InetAddress _deserialize(String str, DeserializationContext deserializationContext) {
            return InetAddress.getByName(str);
        }
    }

    protected static class LocaleDeserializer extends FromStringDeserializer<Locale> {
        public LocaleDeserializer() {
            super(Locale.class);
        }

        /* access modifiers changed from: protected */
        public Locale _deserialize(String str, DeserializationContext deserializationContext) {
            int indexOf = str.indexOf(95);
            if (indexOf < 0) {
                return new Locale(str);
            }
            String substring = str.substring(0, indexOf);
            String substring2 = str.substring(indexOf + 1);
            int indexOf2 = substring2.indexOf(95);
            return indexOf2 < 0 ? new Locale(substring, substring2) : new Locale(substring, substring2.substring(0, indexOf2), substring2.substring(indexOf2 + 1));
        }
    }

    public static class PatternDeserializer extends FromStringDeserializer<Pattern> {
        public PatternDeserializer() {
            super(Pattern.class);
        }

        /* access modifiers changed from: protected */
        public Pattern _deserialize(String str, DeserializationContext deserializationContext) {
            return Pattern.compile(str);
        }
    }

    public static class StackTraceElementDeserializer extends StdScalarDeserializer<StackTraceElement> {
        public StackTraceElementDeserializer() {
            super(StackTraceElement.class);
        }

        public StackTraceElement deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
            JsonToken currentToken = jsonParser.getCurrentToken();
            if (currentToken == JsonToken.START_OBJECT) {
                String str = "";
                String str2 = "";
                String str3 = "";
                int i = -1;
                while (true) {
                    JsonToken nextValue = jsonParser.nextValue();
                    if (nextValue == JsonToken.END_OBJECT) {
                        return new StackTraceElement(str, str2, str3, i);
                    }
                    String currentName = jsonParser.getCurrentName();
                    if ("className".equals(currentName)) {
                        str = jsonParser.getText();
                    } else if ("fileName".equals(currentName)) {
                        str3 = jsonParser.getText();
                    } else if ("lineNumber".equals(currentName)) {
                        if (nextValue.isNumeric()) {
                            i = jsonParser.getIntValue();
                        } else {
                            throw JsonMappingException.from(jsonParser, "Non-numeric token (" + nextValue + ") for property 'lineNumber'");
                        }
                    } else if ("methodName".equals(currentName)) {
                        str2 = jsonParser.getText();
                    } else if (!"nativeMethod".equals(currentName)) {
                        handleUnknownProperty(jsonParser, deserializationContext, this._valueClass, currentName);
                    }
                }
            } else {
                throw deserializationContext.mappingException(this._valueClass, currentToken);
            }
        }
    }

    @JacksonStdImpl
    public static final class StringDeserializer extends StdScalarDeserializer<String> {
        public StringDeserializer() {
            super(String.class);
        }

        public String deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
            JsonToken currentToken = jsonParser.getCurrentToken();
            if (currentToken == JsonToken.VALUE_STRING) {
                return jsonParser.getText();
            }
            if (currentToken == JsonToken.VALUE_EMBEDDED_OBJECT) {
                Object embeddedObject = jsonParser.getEmbeddedObject();
                if (embeddedObject == null) {
                    return null;
                }
                return embeddedObject instanceof byte[] ? Base64Variants.getDefaultVariant().encode((byte[]) embeddedObject, false) : embeddedObject.toString();
            } else if (currentToken.isScalarValue()) {
                return jsonParser.getText();
            } else {
                throw deserializationContext.mappingException(this._valueClass, currentToken);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.fasterxml.jackson.databind.deser.std.JdkDeserializers.StringDeserializer.deserialize(com.fasterxml.jackson.core.JsonParser, com.fasterxml.jackson.databind.DeserializationContext):java.lang.String
         arg types: [com.fasterxml.jackson.core.JsonParser, com.fasterxml.jackson.databind.DeserializationContext]
         candidates:
          com.fasterxml.jackson.databind.deser.std.JdkDeserializers.StringDeserializer.deserialize(com.fasterxml.jackson.core.JsonParser, com.fasterxml.jackson.databind.DeserializationContext):java.lang.Object
          com.fasterxml.jackson.databind.JsonDeserializer.deserialize(com.fasterxml.jackson.core.JsonParser, com.fasterxml.jackson.databind.DeserializationContext):T
          com.fasterxml.jackson.databind.deser.std.JdkDeserializers.StringDeserializer.deserialize(com.fasterxml.jackson.core.JsonParser, com.fasterxml.jackson.databind.DeserializationContext):java.lang.String */
        public String deserializeWithType(JsonParser jsonParser, DeserializationContext deserializationContext, TypeDeserializer typeDeserializer) {
            return deserialize(jsonParser, deserializationContext);
        }
    }

    public static class URIDeserializer extends FromStringDeserializer<URI> {
        public URIDeserializer() {
            super(URI.class);
        }

        /* access modifiers changed from: protected */
        public URI _deserialize(String str, DeserializationContext deserializationContext) {
            return URI.create(str);
        }
    }

    public static class URLDeserializer extends FromStringDeserializer<URL> {
        public URLDeserializer() {
            super(URL.class);
        }

        /* access modifiers changed from: protected */
        public URL _deserialize(String str, DeserializationContext deserializationContext) {
            return new URL(str);
        }
    }

    public static class UUIDDeserializer extends FromStringDeserializer<UUID> {
        public UUIDDeserializer() {
            super(UUID.class);
        }

        /* access modifiers changed from: protected */
        public UUID _deserialize(String str, DeserializationContext deserializationContext) {
            return UUID.fromString(str);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.fasterxml.jackson.databind.deser.std.FromStringDeserializer._deserializeEmbedded(java.lang.Object, com.fasterxml.jackson.databind.DeserializationContext):T
         arg types: [java.lang.Object, com.fasterxml.jackson.databind.DeserializationContext]
         candidates:
          com.fasterxml.jackson.databind.deser.std.JdkDeserializers.UUIDDeserializer._deserializeEmbedded(java.lang.Object, com.fasterxml.jackson.databind.DeserializationContext):java.util.UUID
          com.fasterxml.jackson.databind.deser.std.FromStringDeserializer._deserializeEmbedded(java.lang.Object, com.fasterxml.jackson.databind.DeserializationContext):T */
        /* access modifiers changed from: protected */
        public UUID _deserializeEmbedded(Object obj, DeserializationContext deserializationContext) {
            if (obj instanceof byte[]) {
                byte[] bArr = (byte[]) obj;
                if (bArr.length != 16) {
                    deserializationContext.mappingException("Can only construct UUIDs from 16 byte arrays; got " + bArr.length + " bytes");
                }
                DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(bArr));
                return new UUID(dataInputStream.readLong(), dataInputStream.readLong());
            }
            super._deserializeEmbedded(obj, deserializationContext);
            return null;
        }
    }

    public static StdDeserializer<?>[] all() {
        return new StdDeserializer[]{new StringDeserializer(), new UUIDDeserializer(), new URLDeserializer(), new URIDeserializer(), new CurrencyDeserializer(), new PatternDeserializer(), new LocaleDeserializer(), new InetAddressDeserializer(), new CharsetDeserializer(), new AtomicBooleanDeserializer(), new ClassDeserializer(), new StackTraceElementDeserializer()};
    }
}
