package com.fasterxml.jackson.databind.node;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.SerializerProvider;

public final class BooleanNode extends ValueNode {
    public static final BooleanNode FALSE = new BooleanNode();
    public static final BooleanNode TRUE = new BooleanNode();

    private BooleanNode() {
    }

    public static BooleanNode getFalse() {
        return FALSE;
    }

    public static BooleanNode getTrue() {
        return TRUE;
    }

    public static BooleanNode valueOf(boolean z) {
        return z ? TRUE : FALSE;
    }

    public boolean asBoolean() {
        return this == TRUE;
    }

    public boolean asBoolean(boolean z) {
        return this == TRUE;
    }

    public double asDouble(double d) {
        return this == TRUE ? 1.0d : 0.0d;
    }

    public int asInt(int i) {
        return this == TRUE ? 1 : 0;
    }

    public long asLong(long j) {
        return this == TRUE ? 1 : 0;
    }

    public String asText() {
        return this == TRUE ? "true" : "false";
    }

    public JsonToken asToken() {
        return this == TRUE ? JsonToken.VALUE_TRUE : JsonToken.VALUE_FALSE;
    }

    public boolean booleanValue() {
        return this == TRUE;
    }

    public boolean equals(Object obj) {
        return obj == this;
    }

    public boolean isBoolean() {
        return true;
    }

    public final void serialize(JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        jsonGenerator.writeBoolean(this == TRUE);
    }
}
