package com.fasterxml.jackson.databind.ser;

import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.NoClass;
import com.fasterxml.jackson.databind.cfg.HandlerInstantiator;
import com.fasterxml.jackson.databind.introspect.Annotated;
import com.fasterxml.jackson.databind.introspect.ObjectIdInfo;
import com.fasterxml.jackson.databind.jsonschema.JsonSchema;
import com.fasterxml.jackson.databind.jsonschema.SchemaAware;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.ser.impl.WritableObjectId;
import com.fasterxml.jackson.databind.util.ClassUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.IdentityHashMap;

public abstract class DefaultSerializerProvider extends SerializerProvider {
    protected ArrayList<ObjectIdGenerator<?>> _objectIdGenerators;
    protected IdentityHashMap<Object, WritableObjectId> _seenObjectIds;

    public static final class Impl extends DefaultSerializerProvider {
        public Impl() {
        }

        protected Impl(SerializerProvider serializerProvider, SerializationConfig serializationConfig, SerializerFactory serializerFactory) {
            super(serializerProvider, serializationConfig, serializerFactory);
        }

        public Impl createInstance(SerializationConfig serializationConfig, SerializerFactory serializerFactory) {
            return new Impl(this, serializationConfig, serializerFactory);
        }
    }

    protected DefaultSerializerProvider() {
    }

    protected DefaultSerializerProvider(SerializerProvider serializerProvider, SerializationConfig serializationConfig, SerializerFactory serializerFactory) {
        super(serializerProvider, serializationConfig, serializerFactory);
    }

    public int cachedSerializersCount() {
        return this._serializerCache.size();
    }

    public abstract DefaultSerializerProvider createInstance(SerializationConfig serializationConfig, SerializerFactory serializerFactory);

    public WritableObjectId findObjectId(Object obj, ObjectIdGenerator<?> objectIdGenerator) {
        ObjectIdGenerator<?> objectIdGenerator2;
        if (this._seenObjectIds == null) {
            this._seenObjectIds = new IdentityHashMap<>();
        } else {
            WritableObjectId writableObjectId = this._seenObjectIds.get(obj);
            if (writableObjectId != null) {
                return writableObjectId;
            }
        }
        if (this._objectIdGenerators != null) {
            int size = this._objectIdGenerators.size();
            int i = 0;
            while (true) {
                if (i >= size) {
                    objectIdGenerator2 = null;
                    break;
                }
                objectIdGenerator2 = this._objectIdGenerators.get(i);
                if (objectIdGenerator2.canUseFor(objectIdGenerator)) {
                    break;
                }
                i++;
            }
        } else {
            this._objectIdGenerators = new ArrayList<>(8);
            objectIdGenerator2 = null;
        }
        if (objectIdGenerator2 == null) {
            objectIdGenerator2 = objectIdGenerator.newForSerialization(this);
            this._objectIdGenerators.add(objectIdGenerator2);
        }
        WritableObjectId writableObjectId2 = new WritableObjectId(objectIdGenerator2);
        this._seenObjectIds.put(obj, writableObjectId2);
        return writableObjectId2;
    }

    public void flushCachedSerializers() {
        this._serializerCache.flush();
    }

    public JsonSchema generateJsonSchema(Class<?> cls) {
        if (cls == null) {
            throw new IllegalArgumentException("A class must be provided");
        }
        JsonSerializer<Object> findValueSerializer = findValueSerializer(cls, (BeanProperty) null);
        JsonNode schema = findValueSerializer instanceof SchemaAware ? ((SchemaAware) findValueSerializer).getSchema(this, null) : JsonSchema.getDefaultSchemaNode();
        if (schema instanceof ObjectNode) {
            return new JsonSchema((ObjectNode) schema);
        }
        throw new IllegalArgumentException("Class " + cls.getName() + " would not be serialized as a JSON object and therefore has no schema");
    }

    public boolean hasSerializerFor(Class<?> cls) {
        return _findExplicitUntypedSerializer(cls, null) != null;
    }

    public ObjectIdGenerator<?> objectIdGeneratorInstance(Annotated annotated, ObjectIdInfo objectIdInfo) {
        Class<? extends ObjectIdGenerator<?>> generatorType = objectIdInfo.getGeneratorType();
        HandlerInstantiator handlerInstantiator = this._config.getHandlerInstantiator();
        return (handlerInstantiator != null ? handlerInstantiator.objectIdGeneratorInstance(this._config, annotated, generatorType) : (ObjectIdGenerator) ClassUtil.createInstance(generatorType, this._config.canOverrideAccessModifiers())).forScope(objectIdInfo.getScope());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.SerializerProvider.findTypedValueSerializer(java.lang.Class<?>, boolean, com.fasterxml.jackson.databind.BeanProperty):com.fasterxml.jackson.databind.JsonSerializer<java.lang.Object>
     arg types: [java.lang.Class<?>, int, ?[OBJECT, ARRAY]]
     candidates:
      com.fasterxml.jackson.databind.SerializerProvider.findTypedValueSerializer(com.fasterxml.jackson.databind.JavaType, boolean, com.fasterxml.jackson.databind.BeanProperty):com.fasterxml.jackson.databind.JsonSerializer<java.lang.Object>
      com.fasterxml.jackson.databind.SerializerProvider.findTypedValueSerializer(java.lang.Class<?>, boolean, com.fasterxml.jackson.databind.BeanProperty):com.fasterxml.jackson.databind.JsonSerializer<java.lang.Object> */
    public void serializeValue(JsonGenerator jsonGenerator, Object obj) {
        JsonSerializer<Object> jsonSerializer;
        boolean z = false;
        if (obj == null) {
            jsonSerializer = getDefaultNullValueSerializer();
        } else {
            JsonSerializer<Object> findTypedValueSerializer = findTypedValueSerializer(obj.getClass(), true, (BeanProperty) null);
            String rootName = this._config.getRootName();
            if (rootName == null) {
                z = this._config.isEnabled(SerializationFeature.WRAP_ROOT_VALUE);
                if (z) {
                    jsonGenerator.writeStartObject();
                    jsonGenerator.writeFieldName(this._rootNames.findRootName(obj.getClass(), this._config));
                    jsonSerializer = findTypedValueSerializer;
                } else {
                    jsonSerializer = findTypedValueSerializer;
                }
            } else if (rootName.length() == 0) {
                jsonSerializer = findTypedValueSerializer;
            } else {
                jsonGenerator.writeStartObject();
                jsonGenerator.writeFieldName(rootName);
                z = true;
                jsonSerializer = findTypedValueSerializer;
            }
        }
        try {
            jsonSerializer.serialize(obj, jsonGenerator, this);
            if (z) {
                jsonGenerator.writeEndObject();
            }
        } catch (IOException e) {
            throw e;
        } catch (Exception e2) {
            Exception exc = e2;
            String message = exc.getMessage();
            if (message == null) {
                message = "[no message for " + exc.getClass().getName() + "]";
            }
            throw new JsonMappingException(message, exc);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.databind.SerializerProvider.findTypedValueSerializer(com.fasterxml.jackson.databind.JavaType, boolean, com.fasterxml.jackson.databind.BeanProperty):com.fasterxml.jackson.databind.JsonSerializer<java.lang.Object>
     arg types: [com.fasterxml.jackson.databind.JavaType, int, ?[OBJECT, ARRAY]]
     candidates:
      com.fasterxml.jackson.databind.SerializerProvider.findTypedValueSerializer(java.lang.Class<?>, boolean, com.fasterxml.jackson.databind.BeanProperty):com.fasterxml.jackson.databind.JsonSerializer<java.lang.Object>
      com.fasterxml.jackson.databind.SerializerProvider.findTypedValueSerializer(com.fasterxml.jackson.databind.JavaType, boolean, com.fasterxml.jackson.databind.BeanProperty):com.fasterxml.jackson.databind.JsonSerializer<java.lang.Object> */
    public void serializeValue(JsonGenerator jsonGenerator, Object obj, JavaType javaType) {
        JsonSerializer<Object> findTypedValueSerializer;
        boolean isEnabled;
        if (obj == null) {
            findTypedValueSerializer = getDefaultNullValueSerializer();
            isEnabled = false;
        } else {
            if (!javaType.getRawClass().isAssignableFrom(obj.getClass())) {
                _reportIncompatibleRootType(obj, javaType);
            }
            findTypedValueSerializer = findTypedValueSerializer(javaType, true, (BeanProperty) null);
            isEnabled = this._config.isEnabled(SerializationFeature.WRAP_ROOT_VALUE);
            if (isEnabled) {
                jsonGenerator.writeStartObject();
                jsonGenerator.writeFieldName(this._rootNames.findRootName(javaType, this._config));
            }
        }
        try {
            findTypedValueSerializer.serialize(obj, jsonGenerator, this);
            if (isEnabled) {
                jsonGenerator.writeEndObject();
            }
        } catch (IOException e) {
            throw e;
        } catch (Exception e2) {
            Exception exc = e2;
            String message = exc.getMessage();
            if (message == null) {
                message = "[no message for " + exc.getClass().getName() + "]";
            }
            throw new JsonMappingException(message, exc);
        }
    }

    public JsonSerializer<Object> serializerInstance(Annotated annotated, Object obj) {
        JsonSerializer<?> serializerInstance;
        if (obj == null) {
            return null;
        }
        if (obj instanceof JsonSerializer) {
            serializerInstance = (JsonSerializer) obj;
        } else if (!(obj instanceof Class)) {
            throw new IllegalStateException("AnnotationIntrospector returned serializer definition of type " + obj.getClass().getName() + "; expected type JsonSerializer or Class<JsonSerializer> instead");
        } else {
            Class<NoClass> cls = (Class) obj;
            if (cls == JsonSerializer.None.class || cls == NoClass.class) {
                return null;
            }
            if (!JsonSerializer.class.isAssignableFrom(cls)) {
                throw new IllegalStateException("AnnotationIntrospector returned Class " + cls.getName() + "; expected Class<JsonSerializer>");
            }
            HandlerInstantiator handlerInstantiator = this._config.getHandlerInstantiator();
            serializerInstance = handlerInstantiator != null ? handlerInstantiator.serializerInstance(this._config, annotated, cls) : (JsonSerializer) ClassUtil.createInstance(cls, this._config.canOverrideAccessModifiers());
        }
        return _handleResolvable(serializerInstance);
    }
}
