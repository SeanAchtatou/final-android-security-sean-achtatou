package com.fasterxml.jackson.databind.ser.std;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

public abstract class DateTimeSerializerBase<T> extends StdScalarSerializer<T> implements ContextualSerializer {
    protected final DateFormat _customFormat;
    protected final boolean _useTimestamp;

    protected DateTimeSerializerBase(Class<T> cls, boolean z, DateFormat dateFormat) {
        super(cls);
        this._useTimestamp = z;
        this._customFormat = dateFormat;
    }

    /* access modifiers changed from: protected */
    public abstract long _timestamp(Object obj);

    public JsonSerializer<?> createContextual(SerializerProvider serializerProvider, BeanProperty beanProperty) {
        JsonFormat.Value findFormat;
        DateFormat dateFormat;
        if (beanProperty == null || (findFormat = serializerProvider.getAnnotationIntrospector().findFormat(beanProperty.getMember())) == null) {
            return this;
        }
        if (findFormat.getShape().isNumeric()) {
            return withFormat(true, null);
        }
        TimeZone timeZone = findFormat.getTimeZone();
        String pattern = findFormat.getPattern();
        if (pattern.length() > 0) {
            Locale locale = findFormat.getLocale();
            if (locale == null) {
                locale = serializerProvider.getLocale();
            }
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, locale);
            simpleDateFormat.setTimeZone(timeZone == null ? serializerProvider.getTimeZone() : timeZone);
            return withFormat(false, simpleDateFormat);
        } else if (timeZone == null) {
            return this;
        } else {
            DateFormat dateFormat2 = serializerProvider.getConfig().getDateFormat();
            if (dateFormat2.getClass() == StdDateFormat.class) {
                dateFormat = StdDateFormat.getISO8601Format(timeZone);
            } else {
                dateFormat = (DateFormat) dateFormat2.clone();
                dateFormat.setTimeZone(timeZone);
            }
            return withFormat(false, dateFormat);
        }
    }

    public JsonNode getSchema(SerializerProvider serializerProvider, Type type) {
        boolean z = this._useTimestamp;
        if (!z && this._customFormat == null) {
            z = serializerProvider.isEnabled(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        }
        return createSchemaNode(z ? "number" : "string", true);
    }

    public boolean isEmpty(T t) {
        return t == null || _timestamp(t) == 0;
    }

    public abstract void serialize(Object obj, JsonGenerator jsonGenerator, SerializerProvider serializerProvider);

    public abstract DateTimeSerializerBase<T> withFormat(boolean z, DateFormat dateFormat);
}
