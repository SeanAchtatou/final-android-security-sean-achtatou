package com.fasterxml.jackson.databind.util;

import java.lang.reflect.Array;

public class Comparators {
    public static Object getArrayComparator(final Object obj) {
        final int length = Array.getLength(obj);
        return new Object() {
            public boolean equals(Object obj) {
                if (obj == this) {
                    return true;
                }
                if (obj == null || obj.getClass() != obj.getClass() || Array.getLength(obj) != length) {
                    return false;
                }
                for (int i = 0; i < length; i++) {
                    Object obj2 = Array.get(obj, i);
                    Object obj3 = Array.get(obj, i);
                    if (obj2 != obj3 && obj2 != null && !obj2.equals(obj3)) {
                        return false;
                    }
                }
                return true;
            }
        };
    }
}
