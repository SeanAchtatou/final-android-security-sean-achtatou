package com.fasterxml.jackson.databind.node;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import com.fasterxml.jackson.databind.node.ContainerNode;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import me.gall.sgp.sdk.service.BossService;

public class ObjectNode extends ContainerNode<ObjectNode> {
    protected LinkedHashMap<String, JsonNode> _children = null;

    protected static class NoFieldsIterator implements Iterator<Map.Entry<String, JsonNode>> {
        static final NoFieldsIterator instance = new NoFieldsIterator();

        private NoFieldsIterator() {
        }

        public boolean hasNext() {
            return false;
        }

        public Map.Entry<String, JsonNode> next() {
            throw new NoSuchElementException();
        }

        public void remove() {
            throw new IllegalStateException();
        }
    }

    public ObjectNode(JsonNodeFactory jsonNodeFactory) {
        super(jsonNodeFactory);
    }

    protected ObjectNode(JsonNodeFactory jsonNodeFactory, LinkedHashMap<String, JsonNode> linkedHashMap) {
        super(jsonNodeFactory);
        this._children = linkedHashMap;
    }

    private final JsonNode _put(String str, JsonNode jsonNode) {
        if (this._children == null) {
            this._children = new LinkedHashMap<>();
        }
        return this._children.put(str, jsonNode);
    }

    public JsonToken asToken() {
        return JsonToken.START_OBJECT;
    }

    public ObjectNode deepCopy() {
        if (this._children == null) {
            return new ObjectNode(this._nodeFactory);
        }
        LinkedHashMap linkedHashMap = new LinkedHashMap(Math.max(4, this._children.size()));
        for (Map.Entry next : this._children.entrySet()) {
            linkedHashMap.put(next.getKey(), ((JsonNode) next.getValue()).deepCopy());
        }
        return new ObjectNode(this._nodeFactory, linkedHashMap);
    }

    public Iterator<JsonNode> elements() {
        return this._children == null ? ContainerNode.NoNodesIterator.instance() : this._children.values().iterator();
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0038  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r6) {
        /*
            r5 = this;
            r2 = 1
            r3 = 0
            if (r6 != r5) goto L_0x0006
            r0 = r2
        L_0x0005:
            return r0
        L_0x0006:
            if (r6 != 0) goto L_0x000a
            r0 = r3
            goto L_0x0005
        L_0x000a:
            java.lang.Class r0 = r6.getClass()
            java.lang.Class r1 = r5.getClass()
            if (r0 == r1) goto L_0x0016
            r0 = r3
            goto L_0x0005
        L_0x0016:
            com.fasterxml.jackson.databind.node.ObjectNode r6 = (com.fasterxml.jackson.databind.node.ObjectNode) r6
            int r0 = r6.size()
            int r1 = r5.size()
            if (r0 == r1) goto L_0x0024
            r0 = r3
            goto L_0x0005
        L_0x0024:
            java.util.LinkedHashMap<java.lang.String, com.fasterxml.jackson.databind.JsonNode> r0 = r5._children
            if (r0 == 0) goto L_0x0058
            java.util.LinkedHashMap<java.lang.String, com.fasterxml.jackson.databind.JsonNode> r0 = r5._children
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r4 = r0.iterator()
        L_0x0032:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x0058
            java.lang.Object r0 = r4.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r1 = r0.getKey()
            java.lang.String r1 = (java.lang.String) r1
            java.lang.Object r0 = r0.getValue()
            com.fasterxml.jackson.databind.JsonNode r0 = (com.fasterxml.jackson.databind.JsonNode) r0
            com.fasterxml.jackson.databind.JsonNode r1 = r6.get(r1)
            if (r1 == 0) goto L_0x0056
            boolean r0 = r1.equals(r0)
            if (r0 != 0) goto L_0x0032
        L_0x0056:
            r0 = r3
            goto L_0x0005
        L_0x0058:
            r0 = r2
            goto L_0x0005
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.node.ObjectNode.equals(java.lang.Object):boolean");
    }

    public Iterator<String> fieldNames() {
        return this._children == null ? ContainerNode.NoStringsIterator.instance() : this._children.keySet().iterator();
    }

    public Iterator<Map.Entry<String, JsonNode>> fields() {
        return this._children == null ? NoFieldsIterator.instance : this._children.entrySet().iterator();
    }

    public ObjectNode findParent(String str) {
        if (this._children != null) {
            for (Map.Entry next : this._children.entrySet()) {
                if (str.equals(next.getKey())) {
                    return this;
                }
                JsonNode findParent = ((JsonNode) next.getValue()).findParent(str);
                if (findParent != null) {
                    return (ObjectNode) findParent;
                }
            }
        }
        return null;
    }

    public List<JsonNode> findParents(String str, List<JsonNode> list) {
        List<JsonNode> findParents;
        if (this._children == null) {
            return list;
        }
        List<JsonNode> list2 = list;
        for (Map.Entry next : this._children.entrySet()) {
            if (str.equals(next.getKey())) {
                findParents = list2 == null ? new ArrayList<>() : list2;
                findParents.add(this);
            } else {
                findParents = ((JsonNode) next.getValue()).findParents(str, list2);
            }
            list2 = findParents;
        }
        return list2;
    }

    public JsonNode findValue(String str) {
        if (this._children != null) {
            for (Map.Entry next : this._children.entrySet()) {
                if (str.equals(next.getKey())) {
                    return (JsonNode) next.getValue();
                }
                JsonNode findValue = ((JsonNode) next.getValue()).findValue(str);
                if (findValue != null) {
                    return findValue;
                }
            }
        }
        return null;
    }

    public List<JsonNode> findValues(String str, List<JsonNode> list) {
        if (this._children == null) {
            return list;
        }
        List<JsonNode> list2 = list;
        for (Map.Entry next : this._children.entrySet()) {
            if (str.equals(next.getKey())) {
                if (list2 == null) {
                    list2 = new ArrayList<>();
                }
                list2.add(next.getValue());
            } else {
                list2 = ((JsonNode) next.getValue()).findValues(str, list2);
            }
        }
        return list2;
    }

    public List<String> findValuesAsText(String str, List<String> list) {
        if (this._children == null) {
            return list;
        }
        List<String> list2 = list;
        for (Map.Entry next : this._children.entrySet()) {
            if (str.equals(next.getKey())) {
                if (list2 == null) {
                    list2 = new ArrayList<>();
                }
                list2.add(((JsonNode) next.getValue()).asText());
            } else {
                list2 = ((JsonNode) next.getValue()).findValuesAsText(str, list2);
            }
        }
        return list2;
    }

    public JsonNode get(int i) {
        return null;
    }

    public JsonNode get(String str) {
        if (this._children != null) {
            return this._children.get(str);
        }
        return null;
    }

    public int hashCode() {
        if (this._children == null) {
            return -1;
        }
        return this._children.hashCode();
    }

    public boolean isObject() {
        return true;
    }

    public JsonNode path(int i) {
        return MissingNode.getInstance();
    }

    public JsonNode path(String str) {
        JsonNode jsonNode;
        return (this._children == null || (jsonNode = this._children.get(str)) == null) ? MissingNode.getInstance() : jsonNode;
    }

    public JsonNode put(String str, JsonNode jsonNode) {
        if (jsonNode == null) {
            jsonNode = nullNode();
        }
        return _put(str, jsonNode);
    }

    public ObjectNode put(String str, double d) {
        _put(str, numberNode(d));
        return this;
    }

    public ObjectNode put(String str, float f) {
        _put(str, numberNode(f));
        return this;
    }

    public ObjectNode put(String str, int i) {
        _put(str, numberNode(i));
        return this;
    }

    public ObjectNode put(String str, long j) {
        _put(str, numberNode(j));
        return this;
    }

    public ObjectNode put(String str, Boolean bool) {
        if (bool == null) {
            _put(str, nullNode());
        } else {
            _put(str, booleanNode(bool.booleanValue()));
        }
        return this;
    }

    public ObjectNode put(String str, Double d) {
        if (d == null) {
            _put(str, nullNode());
        } else {
            _put(str, numberNode(d.doubleValue()));
        }
        return this;
    }

    public ObjectNode put(String str, Float f) {
        if (f == null) {
            _put(str, nullNode());
        } else {
            _put(str, numberNode(f.floatValue()));
        }
        return this;
    }

    public ObjectNode put(String str, Integer num) {
        if (num == null) {
            _put(str, nullNode());
        } else {
            _put(str, numberNode(num.intValue()));
        }
        return this;
    }

    public ObjectNode put(String str, Long l) {
        if (l == null) {
            _put(str, nullNode());
        } else {
            _put(str, numberNode(l.longValue()));
        }
        return this;
    }

    public ObjectNode put(String str, String str2) {
        if (str2 == null) {
            putNull(str);
        } else {
            _put(str, textNode(str2));
        }
        return this;
    }

    public ObjectNode put(String str, BigDecimal bigDecimal) {
        if (bigDecimal == null) {
            putNull(str);
        } else {
            _put(str, numberNode(bigDecimal));
        }
        return this;
    }

    public ObjectNode put(String str, boolean z) {
        _put(str, booleanNode(z));
        return this;
    }

    public ObjectNode put(String str, byte[] bArr) {
        if (bArr == null) {
            _put(str, nullNode());
        } else {
            _put(str, binaryNode(bArr));
        }
        return this;
    }

    public JsonNode putAll(ObjectNode objectNode) {
        int size = objectNode.size();
        if (size > 0) {
            if (this._children == null) {
                this._children = new LinkedHashMap<>(size);
            }
            objectNode.putContentsTo(this._children);
        }
        return this;
    }

    public JsonNode putAll(Map<String, JsonNode> map) {
        if (this._children == null) {
            this._children = new LinkedHashMap<>(map);
        } else {
            for (Map.Entry next : map.entrySet()) {
                Object obj = (JsonNode) next.getValue();
                if (obj == null) {
                    obj = nullNode();
                }
                this._children.put(next.getKey(), obj);
            }
        }
        return this;
    }

    public ArrayNode putArray(String str) {
        ArrayNode arrayNode = arrayNode();
        _put(str, arrayNode);
        return arrayNode;
    }

    /* access modifiers changed from: protected */
    public void putContentsTo(Map<String, JsonNode> map) {
        if (this._children != null) {
            for (Map.Entry next : this._children.entrySet()) {
                map.put(next.getKey(), next.getValue());
            }
        }
    }

    public ObjectNode putNull(String str) {
        _put(str, nullNode());
        return this;
    }

    public ObjectNode putObject(String str) {
        ObjectNode objectNode = objectNode();
        _put(str, objectNode);
        return objectNode;
    }

    public ObjectNode putPOJO(String str, Object obj) {
        _put(str, POJONode(obj));
        return this;
    }

    public JsonNode remove(String str) {
        if (this._children != null) {
            return this._children.remove(str);
        }
        return null;
    }

    public ObjectNode remove(Collection<String> collection) {
        if (this._children != null) {
            for (String remove : collection) {
                this._children.remove(remove);
            }
        }
        return this;
    }

    public ObjectNode removeAll() {
        this._children = null;
        return this;
    }

    public ObjectNode retain(Collection<String> collection) {
        if (this._children != null) {
            Iterator<Map.Entry<String, JsonNode>> it = this._children.entrySet().iterator();
            while (it.hasNext()) {
                if (!collection.contains(it.next().getKey())) {
                    it.remove();
                }
            }
        }
        return this;
    }

    public ObjectNode retain(String... strArr) {
        return retain(Arrays.asList(strArr));
    }

    public final void serialize(JsonGenerator jsonGenerator, SerializerProvider serializerProvider) {
        jsonGenerator.writeStartObject();
        if (this._children != null) {
            for (Map.Entry next : this._children.entrySet()) {
                jsonGenerator.writeFieldName((String) next.getKey());
                ((BaseJsonNode) next.getValue()).serialize(jsonGenerator, serializerProvider);
            }
        }
        jsonGenerator.writeEndObject();
    }

    public void serializeWithType(JsonGenerator jsonGenerator, SerializerProvider serializerProvider, TypeSerializer typeSerializer) {
        typeSerializer.writeTypePrefixForObject(this, jsonGenerator);
        if (this._children != null) {
            for (Map.Entry next : this._children.entrySet()) {
                jsonGenerator.writeFieldName((String) next.getKey());
                ((BaseJsonNode) next.getValue()).serialize(jsonGenerator, serializerProvider);
            }
        }
        typeSerializer.writeTypeSuffixForObject(this, jsonGenerator);
    }

    public int size() {
        if (this._children == null) {
            return 0;
        }
        return this._children.size();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder((size() << 4) + 32);
        sb.append("{");
        if (this._children != null) {
            int i = 0;
            for (Map.Entry next : this._children.entrySet()) {
                if (i > 0) {
                    sb.append(BossService.ID_SEPARATOR);
                }
                TextNode.appendQuoted(sb, (String) next.getKey());
                sb.append(':');
                sb.append(((JsonNode) next.getValue()).toString());
                i++;
            }
        }
        sb.append("}");
        return sb.toString();
    }

    public ObjectNode with(String str) {
        if (this._children == null) {
            this._children = new LinkedHashMap<>();
        } else {
            JsonNode jsonNode = this._children.get(str);
            if (jsonNode != null) {
                if (jsonNode instanceof ObjectNode) {
                    return (ObjectNode) jsonNode;
                }
                throw new UnsupportedOperationException("Property '" + str + "' has value that is not of type ObjectNode (but " + jsonNode.getClass().getName() + ")");
            }
        }
        ObjectNode objectNode = objectNode();
        this._children.put(str, objectNode);
        return objectNode;
    }

    public ArrayNode withArray(String str) {
        if (this._children == null) {
            this._children = new LinkedHashMap<>();
        } else {
            JsonNode jsonNode = this._children.get(str);
            if (jsonNode != null) {
                if (jsonNode instanceof ArrayNode) {
                    return (ArrayNode) jsonNode;
                }
                throw new UnsupportedOperationException("Property '" + str + "' has value that is not of type ArrayNode (but " + jsonNode.getClass().getName() + ")");
            }
        }
        ArrayNode arrayNode = arrayNode();
        this._children.put(str, arrayNode);
        return arrayNode;
    }
}
