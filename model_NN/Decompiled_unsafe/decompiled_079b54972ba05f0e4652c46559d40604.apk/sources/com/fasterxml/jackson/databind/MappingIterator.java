package com.fasterxml.jackson.databind;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class MappingIterator<T> implements Iterator<T> {
    protected static final MappingIterator<?> EMPTY_ITERATOR = new MappingIterator<>(null, null, null, null, false, null);
    protected final boolean _closeParser;
    protected final DeserializationContext _context;
    protected final JsonDeserializer<T> _deserializer;
    protected boolean _hasNextChecked;
    protected JsonParser _parser;
    protected final JavaType _type;
    protected final T _updatedValue;

    protected MappingIterator(JavaType javaType, JsonParser jsonParser, DeserializationContext deserializationContext, JsonDeserializer<?> jsonDeserializer) {
        this(javaType, jsonParser, deserializationContext, jsonDeserializer, true, null);
    }

    /* JADX WARN: Type inference failed for: r6v0, types: [com.fasterxml.jackson.databind.JsonDeserializer<T>, com.fasterxml.jackson.databind.JsonDeserializer<?>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected MappingIterator(com.fasterxml.jackson.databind.JavaType r3, com.fasterxml.jackson.core.JsonParser r4, com.fasterxml.jackson.databind.DeserializationContext r5, com.fasterxml.jackson.databind.JsonDeserializer<?> r6, boolean r7, java.lang.Object r8) {
        /*
            r2 = this;
            r2.<init>()
            r2._type = r3
            r2._parser = r4
            r2._context = r5
            r2._deserializer = r6
            if (r4 == 0) goto L_0x0022
            com.fasterxml.jackson.core.JsonToken r0 = r4.getCurrentToken()
            com.fasterxml.jackson.core.JsonToken r1 = com.fasterxml.jackson.core.JsonToken.START_ARRAY
            if (r0 != r1) goto L_0x0022
            com.fasterxml.jackson.core.JsonStreamContext r0 = r4.getParsingContext()
            boolean r0 = r0.inRoot()
            if (r0 != 0) goto L_0x0022
            r4.clearCurrentToken()
        L_0x0022:
            r2._closeParser = r7
            if (r8 != 0) goto L_0x002a
            r0 = 0
            r2._updatedValue = r0
        L_0x0029:
            return
        L_0x002a:
            r2._updatedValue = r8
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.databind.MappingIterator.<init>(com.fasterxml.jackson.databind.JavaType, com.fasterxml.jackson.core.JsonParser, com.fasterxml.jackson.databind.DeserializationContext, com.fasterxml.jackson.databind.JsonDeserializer, boolean, java.lang.Object):void");
    }

    protected static <T> MappingIterator<T> emptyIterator() {
        return EMPTY_ITERATOR;
    }

    public boolean hasNext() {
        try {
            return hasNextValue();
        } catch (JsonMappingException e) {
            throw new RuntimeJsonMappingException(e.getMessage(), e);
        } catch (IOException e2) {
            throw new RuntimeException(e2.getMessage(), e2);
        }
    }

    public boolean hasNextValue() {
        if (this._parser == null) {
            return false;
        }
        if (!this._hasNextChecked) {
            JsonToken currentToken = this._parser.getCurrentToken();
            this._hasNextChecked = true;
            if (currentToken == null) {
                JsonToken nextToken = this._parser.nextToken();
                if (nextToken == null) {
                    JsonParser jsonParser = this._parser;
                    this._parser = null;
                    if (!this._closeParser) {
                        return false;
                    }
                    jsonParser.close();
                    return false;
                } else if (nextToken == JsonToken.END_ARRAY) {
                    return false;
                }
            }
        }
        return true;
    }

    public T next() {
        try {
            return nextValue();
        } catch (JsonMappingException e) {
            throw new RuntimeJsonMappingException(e.getMessage(), e);
        } catch (IOException e2) {
            throw new RuntimeException(e2.getMessage(), e2);
        }
    }

    public T nextValue() {
        T t;
        if (!this._hasNextChecked && !hasNextValue()) {
            throw new NoSuchElementException();
        } else if (this._parser == null) {
            throw new NoSuchElementException();
        } else {
            this._hasNextChecked = false;
            if (this._updatedValue == null) {
                t = this._deserializer.deserialize(this._parser, this._context);
            } else {
                this._deserializer.deserialize(this._parser, this._context, this._updatedValue);
                t = this._updatedValue;
            }
            this._parser.clearCurrentToken();
            return t;
        }
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }
}
