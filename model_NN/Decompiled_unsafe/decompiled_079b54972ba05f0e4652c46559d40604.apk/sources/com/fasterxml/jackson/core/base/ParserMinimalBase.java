package com.fasterxml.jackson.core.base;

import com.fasterxml.jackson.core.Base64Variant;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonStreamContext;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.core.io.NumberInput;
import com.fasterxml.jackson.core.util.VersionUtil;

public abstract class ParserMinimalBase extends JsonParser {
    protected static final int INT_APOSTROPHE = 39;
    protected static final int INT_ASTERISK = 42;
    protected static final int INT_BACKSLASH = 92;
    protected static final int INT_COLON = 58;
    protected static final int INT_COMMA = 44;
    protected static final int INT_CR = 13;
    protected static final int INT_LBRACKET = 91;
    protected static final int INT_LCURLY = 123;
    protected static final int INT_LF = 10;
    protected static final int INT_QUOTE = 34;
    protected static final int INT_RBRACKET = 93;
    protected static final int INT_RCURLY = 125;
    protected static final int INT_SLASH = 47;
    protected static final int INT_SPACE = 32;
    protected static final int INT_TAB = 9;
    protected static final int INT_b = 98;
    protected static final int INT_f = 102;
    protected static final int INT_n = 110;
    protected static final int INT_r = 114;
    protected static final int INT_t = 116;
    protected static final int INT_u = 117;
    protected JsonToken _currToken;
    protected JsonToken _lastClearedToken;

    protected ParserMinimalBase() {
    }

    protected ParserMinimalBase(int i) {
        super(i);
    }

    protected static final String _getCharDesc(int i) {
        char c = (char) i;
        return Character.isISOControl(c) ? "(CTRL-CHAR, code " + i + ")" : i > 255 ? "'" + c + "' (code " + i + " / 0x" + Integer.toHexString(i) + ")" : "'" + c + "' (code " + i + ")";
    }

    /* access modifiers changed from: protected */
    public final JsonParseException _constructError(String str, Throwable th) {
        return new JsonParseException(str, getCurrentLocation(), th);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0023, code lost:
        _reportBase64EOF();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0026, code lost:
        r0 = r1 + 1;
        r1 = r11.charAt(r1);
        r5 = r13.decodeBase64Char(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0030, code lost:
        if (r5 >= 0) goto L_0x0036;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0032, code lost:
        _reportInvalidBase64(r13, r1, 1, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0036, code lost:
        r1 = (r4 << 6) | r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0039, code lost:
        if (r0 < r3) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003f, code lost:
        if (r13.usesPadding() != false) goto L_0x0047;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0041, code lost:
        r12.append(r1 >> 4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0047, code lost:
        _reportBase64EOF();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004a, code lost:
        r4 = r0 + 1;
        r0 = r11.charAt(r0);
        r5 = r13.decodeBase64Char(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0054, code lost:
        if (r5 >= 0) goto L_0x0094;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0056, code lost:
        if (r5 == -2) goto L_0x005c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0058, code lost:
        _reportInvalidBase64(r13, r0, 2, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005c, code lost:
        if (r4 < r3) goto L_0x0061;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x005e, code lost:
        _reportBase64EOF();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0061, code lost:
        r0 = r4 + 1;
        r4 = r11.charAt(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x006b, code lost:
        if (r13.usesPaddingChar(r4) != false) goto L_0x008d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x006d, code lost:
        _reportInvalidBase64(r13, r4, 3, "expected padding character '" + r13.getPaddingChar() + "'");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x008d, code lost:
        r12.append(r1 >> 4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0094, code lost:
        r1 = (r1 << 6) | r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0098, code lost:
        if (r4 < r3) goto L_0x00aa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x009e, code lost:
        if (r13.usesPadding() != false) goto L_0x00a7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00a0, code lost:
        r12.appendTwoBytes(r1 >> 2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00a7, code lost:
        _reportBase64EOF();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00aa, code lost:
        r0 = r4 + 1;
        r4 = r11.charAt(r4);
        r5 = r13.decodeBase64Char(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00b4, code lost:
        if (r5 >= 0) goto L_0x00c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00b6, code lost:
        if (r5 == -2) goto L_0x00bb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00b8, code lost:
        _reportInvalidBase64(r13, r4, 3, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00bb, code lost:
        r12.appendTwoBytes(r1 >> 2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00c2, code lost:
        r12.appendThreeBytes((r1 << 6) | r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0018, code lost:
        r4 = r13.decodeBase64Char(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x001c, code lost:
        if (r4 >= 0) goto L_0x0021;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001e, code lost:
        _reportInvalidBase64(r13, r0, 0, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0021, code lost:
        if (r1 < r3) goto L_0x0026;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void _decodeBase64(java.lang.String r11, com.fasterxml.jackson.core.util.ByteArrayBuilder r12, com.fasterxml.jackson.core.Base64Variant r13) {
        /*
            r10 = this;
            r9 = 3
            r2 = 0
            r8 = -2
            r7 = 0
            int r3 = r11.length()
            r0 = r2
        L_0x0009:
            if (r0 >= r3) goto L_0x0013
        L_0x000b:
            int r1 = r0 + 1
            char r0 = r11.charAt(r0)
            if (r1 < r3) goto L_0x0014
        L_0x0013:
            return
        L_0x0014:
            r4 = 32
            if (r0 <= r4) goto L_0x00ca
            int r4 = r13.decodeBase64Char(r0)
            if (r4 >= 0) goto L_0x0021
            r10._reportInvalidBase64(r13, r0, r2, r7)
        L_0x0021:
            if (r1 < r3) goto L_0x0026
            r10._reportBase64EOF()
        L_0x0026:
            int r0 = r1 + 1
            char r1 = r11.charAt(r1)
            int r5 = r13.decodeBase64Char(r1)
            if (r5 >= 0) goto L_0x0036
            r6 = 1
            r10._reportInvalidBase64(r13, r1, r6, r7)
        L_0x0036:
            int r1 = r4 << 6
            r1 = r1 | r5
            if (r0 < r3) goto L_0x004a
            boolean r4 = r13.usesPadding()
            if (r4 != 0) goto L_0x0047
            int r0 = r1 >> 4
            r12.append(r0)
            goto L_0x0013
        L_0x0047:
            r10._reportBase64EOF()
        L_0x004a:
            int r4 = r0 + 1
            char r0 = r11.charAt(r0)
            int r5 = r13.decodeBase64Char(r0)
            if (r5 >= 0) goto L_0x0094
            if (r5 == r8) goto L_0x005c
            r5 = 2
            r10._reportInvalidBase64(r13, r0, r5, r7)
        L_0x005c:
            if (r4 < r3) goto L_0x0061
            r10._reportBase64EOF()
        L_0x0061:
            int r0 = r4 + 1
            char r4 = r11.charAt(r4)
            boolean r5 = r13.usesPaddingChar(r4)
            if (r5 != 0) goto L_0x008d
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r6 = "expected padding character '"
            java.lang.StringBuilder r5 = r5.append(r6)
            char r6 = r13.getPaddingChar()
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r6 = "'"
            java.lang.StringBuilder r5 = r5.append(r6)
            java.lang.String r5 = r5.toString()
            r10._reportInvalidBase64(r13, r4, r9, r5)
        L_0x008d:
            int r1 = r1 >> 4
            r12.append(r1)
            goto L_0x0009
        L_0x0094:
            int r0 = r1 << 6
            r1 = r0 | r5
            if (r4 < r3) goto L_0x00aa
            boolean r0 = r13.usesPadding()
            if (r0 != 0) goto L_0x00a7
            int r0 = r1 >> 2
            r12.appendTwoBytes(r0)
            goto L_0x0013
        L_0x00a7:
            r10._reportBase64EOF()
        L_0x00aa:
            int r0 = r4 + 1
            char r4 = r11.charAt(r4)
            int r5 = r13.decodeBase64Char(r4)
            if (r5 >= 0) goto L_0x00c2
            if (r5 == r8) goto L_0x00bb
            r10._reportInvalidBase64(r13, r4, r9, r7)
        L_0x00bb:
            int r1 = r1 >> 2
            r12.appendTwoBytes(r1)
            goto L_0x0009
        L_0x00c2:
            int r1 = r1 << 6
            r1 = r1 | r5
            r12.appendThreeBytes(r1)
            goto L_0x0009
        L_0x00ca:
            r0 = r1
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.core.base.ParserMinimalBase._decodeBase64(java.lang.String, com.fasterxml.jackson.core.util.ByteArrayBuilder, com.fasterxml.jackson.core.Base64Variant):void");
    }

    /* access modifiers changed from: protected */
    public abstract void _handleEOF();

    /* access modifiers changed from: protected */
    public char _handleUnrecognizedCharacterEscape(char c) {
        if (!isEnabled(JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER) && (c != INT_APOSTROPHE || !isEnabled(JsonParser.Feature.ALLOW_SINGLE_QUOTES))) {
            _reportError("Unrecognized character escape " + _getCharDesc(c));
        }
        return c;
    }

    /* access modifiers changed from: protected */
    public void _reportBase64EOF() {
        throw _constructError("Unexpected end-of-String in base64 content");
    }

    /* access modifiers changed from: protected */
    public final void _reportError(String str) {
        throw _constructError(str);
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidBase64(Base64Variant base64Variant, char c, int i, String str) {
        String str2 = c <= ' ' ? "Illegal white space character (code 0x" + Integer.toHexString(c) + ") as character #" + (i + 1) + " of 4-char base64 unit: can only used between units" : base64Variant.usesPaddingChar(c) ? "Unexpected padding character ('" + base64Variant.getPaddingChar() + "') as character #" + (i + 1) + " of 4-char base64 unit: padding only legal as 3rd or 4th character" : (!Character.isDefined(c) || Character.isISOControl(c)) ? "Illegal character (code 0x" + Integer.toHexString(c) + ") in base64 content" : "Illegal character '" + c + "' (code 0x" + Integer.toHexString(c) + ") in base64 content";
        if (str != null) {
            str2 = str2 + ": " + str;
        }
        throw _constructError(str2);
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidEOF() {
        _reportInvalidEOF(" in " + this._currToken);
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidEOF(String str) {
        _reportError("Unexpected end-of-input" + str);
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidEOFInValue() {
        _reportInvalidEOF(" in a value");
    }

    /* access modifiers changed from: protected */
    public void _reportUnexpectedChar(int i, String str) {
        String str2 = "Unexpected character (" + _getCharDesc(i) + ")";
        if (str != null) {
            str2 = str2 + ": " + str;
        }
        _reportError(str2);
    }

    /* access modifiers changed from: protected */
    public final void _throwInternal() {
        throw new RuntimeException("Internal error: this code path should never get executed");
    }

    /* access modifiers changed from: protected */
    public void _throwInvalidSpace(int i) {
        _reportError("Illegal character (" + _getCharDesc((char) i) + "): only regular white space (\\r, \\n, \\t) is allowed between tokens");
    }

    /* access modifiers changed from: protected */
    public void _throwUnquotedSpace(int i, String str) {
        if (!isEnabled(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS) || i >= 32) {
            _reportError("Illegal unquoted character (" + _getCharDesc((char) i) + "): has to be escaped using backslash to be included in " + str);
        }
    }

    /* access modifiers changed from: protected */
    public final void _wrapError(String str, Throwable th) {
        throw _constructError(str, th);
    }

    public void clearCurrentToken() {
        if (this._currToken != null) {
            this._lastClearedToken = this._currToken;
            this._currToken = null;
        }
    }

    public abstract void close();

    public abstract byte[] getBinaryValue(Base64Variant base64Variant);

    public abstract String getCurrentName();

    public JsonToken getCurrentToken() {
        return this._currToken;
    }

    public JsonToken getLastClearedToken() {
        return this._lastClearedToken;
    }

    public abstract JsonStreamContext getParsingContext();

    public abstract String getText();

    public abstract char[] getTextCharacters();

    public abstract int getTextLength();

    public abstract int getTextOffset();

    /* JADX WARNING: Removed duplicated region for block: B:14:0x003e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean getValueAsBoolean(boolean r5) {
        /*
            r4 = this;
            r0 = 0
            r1 = 1
            com.fasterxml.jackson.core.JsonToken r2 = r4._currToken
            if (r2 == 0) goto L_0x0013
            int[] r2 = com.fasterxml.jackson.core.base.ParserMinimalBase.AnonymousClass1.$SwitchMap$com$fasterxml$jackson$core$JsonToken
            com.fasterxml.jackson.core.JsonToken r3 = r4._currToken
            int r3 = r3.ordinal()
            r2 = r2[r3]
            switch(r2) {
                case 5: goto L_0x0015;
                case 6: goto L_0x001d;
                case 7: goto L_0x0014;
                case 8: goto L_0x0014;
                case 9: goto L_0x001f;
                case 10: goto L_0x002e;
                default: goto L_0x0013;
            }
        L_0x0013:
            r0 = r5
        L_0x0014:
            return r0
        L_0x0015:
            int r2 = r4.getIntValue()
            if (r2 == 0) goto L_0x0014
            r0 = r1
            goto L_0x0014
        L_0x001d:
            r0 = r1
            goto L_0x0014
        L_0x001f:
            java.lang.Object r0 = r4.getEmbeddedObject()
            boolean r2 = r0 instanceof java.lang.Boolean
            if (r2 == 0) goto L_0x002e
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            goto L_0x0014
        L_0x002e:
            java.lang.String r0 = r4.getText()
            java.lang.String r0 = r0.trim()
            java.lang.String r2 = "true"
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x0013
            r0 = r1
            goto L_0x0014
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.core.base.ParserMinimalBase.getValueAsBoolean(boolean):boolean");
    }

    public double getValueAsDouble(double d) {
        if (this._currToken == null) {
            return d;
        }
        switch (this._currToken) {
            case VALUE_NUMBER_INT:
            case VALUE_NUMBER_FLOAT:
                return getDoubleValue();
            case VALUE_TRUE:
                return 1.0d;
            case VALUE_FALSE:
            case VALUE_NULL:
                return 0.0d;
            case VALUE_EMBEDDED_OBJECT:
                Object embeddedObject = getEmbeddedObject();
                return embeddedObject instanceof Number ? ((Number) embeddedObject).doubleValue() : d;
            case VALUE_STRING:
                return NumberInput.parseAsDouble(getText(), d);
            default:
                return d;
        }
    }

    public int getValueAsInt(int i) {
        if (this._currToken == null) {
            return i;
        }
        switch (this._currToken) {
            case VALUE_NUMBER_INT:
            case VALUE_NUMBER_FLOAT:
                return getIntValue();
            case VALUE_TRUE:
                return 1;
            case VALUE_FALSE:
            case VALUE_NULL:
                return 0;
            case VALUE_EMBEDDED_OBJECT:
                Object embeddedObject = getEmbeddedObject();
                return embeddedObject instanceof Number ? ((Number) embeddedObject).intValue() : i;
            case VALUE_STRING:
                return NumberInput.parseAsInt(getText(), i);
            default:
                return i;
        }
    }

    public long getValueAsLong(long j) {
        if (this._currToken == null) {
            return j;
        }
        switch (this._currToken) {
            case VALUE_NUMBER_INT:
            case VALUE_NUMBER_FLOAT:
                return getLongValue();
            case VALUE_TRUE:
                return 1;
            case VALUE_FALSE:
            case VALUE_NULL:
                return 0;
            case VALUE_EMBEDDED_OBJECT:
                Object embeddedObject = getEmbeddedObject();
                return embeddedObject instanceof Number ? ((Number) embeddedObject).longValue() : j;
            case VALUE_STRING:
                return NumberInput.parseAsLong(getText(), j);
            default:
                return j;
        }
    }

    public boolean hasCurrentToken() {
        return this._currToken != null;
    }

    public abstract boolean hasTextCharacters();

    public abstract boolean isClosed();

    public abstract JsonToken nextToken();

    public JsonToken nextValue() {
        JsonToken nextToken = nextToken();
        return nextToken == JsonToken.FIELD_NAME ? nextToken() : nextToken;
    }

    public abstract void overrideCurrentName(String str);

    public JsonParser skipChildren() {
        if (this._currToken == JsonToken.START_OBJECT || this._currToken == JsonToken.START_ARRAY) {
            int i = 1;
            while (true) {
                JsonToken nextToken = nextToken();
                if (nextToken != null) {
                    switch (nextToken) {
                        case START_OBJECT:
                        case START_ARRAY:
                            i++;
                            break;
                        case END_OBJECT:
                        case END_ARRAY:
                            i--;
                            if (i != 0) {
                                break;
                            } else {
                                break;
                            }
                    }
                } else {
                    _handleEOF();
                }
            }
        }
        return this;
    }

    public Version version() {
        return VersionUtil.versionFor(getClass());
    }
}
