package com.fasterxml.jackson.core.json;

import com.a.a.c.p;
import com.a.a.e.c;
import com.a.a.i.a;
import com.a.a.p.f;
import com.fasterxml.jackson.core.Base64Variant;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.core.SerializableString;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.core.base.ParserBase;
import com.fasterxml.jackson.core.io.CharTypes;
import com.fasterxml.jackson.core.io.IOContext;
import com.fasterxml.jackson.core.sym.BytesToNameCanonicalizer;
import com.fasterxml.jackson.core.sym.Name;
import com.fasterxml.jackson.core.util.ByteArrayBuilder;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.microedition.media.control.ToneControl;

public final class UTF8StreamJsonParser extends ParserBase {
    static final byte BYTE_LF = 10;
    private static final int[] sInputCodesLatin1 = CharTypes.getInputCodeLatin1();
    private static final int[] sInputCodesUtf8 = CharTypes.getInputCodeUtf8();
    protected boolean _bufferRecyclable;
    protected byte[] _inputBuffer;
    protected InputStream _inputStream;
    protected ObjectCodec _objectCodec;
    private int _quad1;
    protected int[] _quadBuffer = new int[16];
    protected final BytesToNameCanonicalizer _symbols;
    protected boolean _tokenIncomplete = false;

    public UTF8StreamJsonParser(IOContext iOContext, int i, InputStream inputStream, ObjectCodec objectCodec, BytesToNameCanonicalizer bytesToNameCanonicalizer, byte[] bArr, int i2, int i3, boolean z) {
        super(iOContext, i);
        this._inputStream = inputStream;
        this._objectCodec = objectCodec;
        this._symbols = bytesToNameCanonicalizer;
        this._inputBuffer = bArr;
        this._inputPtr = i2;
        this._inputEnd = i3;
        this._bufferRecyclable = z;
    }

    private final int _decodeUtf8_2(int i) {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        byte b = bArr[i2];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & ToneControl.SILENCE, this._inputPtr);
        }
        return (b & 63) | ((i & 31) << 6);
    }

    private final int _decodeUtf8_3(int i) {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        int i2 = i & 15;
        byte[] bArr = this._inputBuffer;
        int i3 = this._inputPtr;
        this._inputPtr = i3 + 1;
        byte b = bArr[i3];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & ToneControl.SILENCE, this._inputPtr);
        }
        byte b2 = (i2 << 6) | (b & 63);
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr2 = this._inputBuffer;
        int i4 = this._inputPtr;
        this._inputPtr = i4 + 1;
        byte b3 = bArr2[i4];
        if ((b3 & 192) != 128) {
            _reportInvalidOther(b3 & ToneControl.SILENCE, this._inputPtr);
        }
        return (b2 << 6) | (b3 & 63);
    }

    private final int _decodeUtf8_3fast(int i) {
        int i2 = i & 15;
        byte[] bArr = this._inputBuffer;
        int i3 = this._inputPtr;
        this._inputPtr = i3 + 1;
        byte b = bArr[i3];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & ToneControl.SILENCE, this._inputPtr);
        }
        byte b2 = (i2 << 6) | (b & 63);
        byte[] bArr2 = this._inputBuffer;
        int i4 = this._inputPtr;
        this._inputPtr = i4 + 1;
        byte b3 = bArr2[i4];
        if ((b3 & 192) != 128) {
            _reportInvalidOther(b3 & ToneControl.SILENCE, this._inputPtr);
        }
        return (b2 << 6) | (b3 & 63);
    }

    private final int _decodeUtf8_4(int i) {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        byte b = bArr[i2];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & ToneControl.SILENCE, this._inputPtr);
        }
        byte b2 = (b & 63) | ((i & 7) << 6);
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr2 = this._inputBuffer;
        int i3 = this._inputPtr;
        this._inputPtr = i3 + 1;
        byte b3 = bArr2[i3];
        if ((b3 & 192) != 128) {
            _reportInvalidOther(b3 & ToneControl.SILENCE, this._inputPtr);
        }
        byte b4 = (b2 << 6) | (b3 & 63);
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr3 = this._inputBuffer;
        int i4 = this._inputPtr;
        this._inputPtr = i4 + 1;
        byte b5 = bArr3[i4];
        if ((b5 & 192) != 128) {
            _reportInvalidOther(b5 & ToneControl.SILENCE, this._inputPtr);
        }
        return ((b4 << 6) | (b5 & 63)) - p.DELAY;
    }

    private final void _finishString2(char[] cArr, int i) {
        int i2;
        int[] iArr = sInputCodesUtf8;
        byte[] bArr = this._inputBuffer;
        while (true) {
            int i3 = this._inputPtr;
            if (i3 >= this._inputEnd) {
                loadMoreGuaranteed();
                i3 = this._inputPtr;
            }
            if (i >= cArr.length) {
                cArr = this._textBuffer.finishCurrentSegment();
                i = 0;
            }
            int min = Math.min(this._inputEnd, (cArr.length - i) + i3);
            while (true) {
                if (i3 < min) {
                    int i4 = i3 + 1;
                    int i5 = bArr[i3] & ToneControl.SILENCE;
                    if (iArr[i5] != 0) {
                        this._inputPtr = i4;
                        if (i5 == 34) {
                            this._textBuffer.setCurrentLength(i);
                            return;
                        }
                        switch (iArr[i5]) {
                            case 1:
                                i5 = _decodeEscaped();
                                break;
                            case 2:
                                i5 = _decodeUtf8_2(i5);
                                break;
                            case 3:
                                if (this._inputEnd - this._inputPtr < 2) {
                                    i5 = _decodeUtf8_3(i5);
                                    break;
                                } else {
                                    i5 = _decodeUtf8_3fast(i5);
                                    break;
                                }
                            case 4:
                                int _decodeUtf8_4 = _decodeUtf8_4(i5);
                                int i6 = i + 1;
                                cArr[i] = (char) (55296 | (_decodeUtf8_4 >> 10));
                                if (i6 >= cArr.length) {
                                    cArr = this._textBuffer.finishCurrentSegment();
                                    i6 = 0;
                                }
                                i = i6;
                                i5 = (_decodeUtf8_4 & 1023) | 56320;
                                break;
                            default:
                                if (i5 >= 32) {
                                    _reportInvalidChar(i5);
                                    break;
                                } else {
                                    _throwUnquotedSpace(i5, "string value");
                                    break;
                                }
                        }
                        if (i >= cArr.length) {
                            cArr = this._textBuffer.finishCurrentSegment();
                            i2 = 0;
                        } else {
                            i2 = i;
                        }
                        i = i2 + 1;
                        cArr[i2] = (char) i5;
                    } else {
                        cArr[i] = (char) i5;
                        i3 = i4;
                        i++;
                    }
                } else {
                    this._inputPtr = i3;
                }
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private final void _isNextTokenNameNo(int i) {
        JsonToken parseNumberText;
        this._parsingContext.setCurrentName(_parseFieldName(i).getName());
        this._currToken = JsonToken.FIELD_NAME;
        int _skipWS = _skipWS();
        if (_skipWS != 58) {
            _reportUnexpectedChar(_skipWS, "was expecting a colon to separate field name and value");
        }
        int _skipWS2 = _skipWS();
        if (_skipWS2 == 34) {
            this._tokenIncomplete = true;
            this._nextToken = JsonToken.VALUE_STRING;
            return;
        }
        switch (_skipWS2) {
            case 45:
            case 48:
            case c.KEY_NUM1:
            case 50:
            case c.KEY_NUM3:
            case c.KEY_NUM4:
            case c.KEY_NUM5:
            case c.KEY_NUM6:
            case c.KEY_NUM7:
            case 56:
            case c.KEY_NUM9:
                parseNumberText = parseNumberText(_skipWS2);
                break;
            case 91:
                parseNumberText = JsonToken.START_ARRAY;
                break;
            case 93:
            case 125:
                _reportUnexpectedChar(_skipWS2, "expected a value");
                _matchToken("true", 1);
                parseNumberText = JsonToken.VALUE_TRUE;
                break;
            case 102:
                _matchToken("false", 1);
                parseNumberText = JsonToken.VALUE_FALSE;
                break;
            case a.PHOTO:
                _matchToken("null", 1);
                parseNumberText = JsonToken.VALUE_NULL;
                break;
            case a.TITLE:
                _matchToken("true", 1);
                parseNumberText = JsonToken.VALUE_TRUE;
                break;
            case 123:
                parseNumberText = JsonToken.START_OBJECT;
                break;
            default:
                parseNumberText = _handleUnexpectedValue(_skipWS2);
                break;
        }
        this._nextToken = parseNumberText;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private final void _isNextTokenNameYes() {
        int _skipColon;
        if (this._inputPtr >= this._inputEnd || this._inputBuffer[this._inputPtr] != 58) {
            _skipColon = _skipColon();
        } else {
            this._inputPtr++;
            byte[] bArr = this._inputBuffer;
            int i = this._inputPtr;
            this._inputPtr = i + 1;
            byte b = bArr[i];
            if (b == 34) {
                this._tokenIncomplete = true;
                this._nextToken = JsonToken.VALUE_STRING;
                return;
            } else if (b == 123) {
                this._nextToken = JsonToken.START_OBJECT;
                return;
            } else if (b == 91) {
                this._nextToken = JsonToken.START_ARRAY;
                return;
            } else {
                _skipColon = b & ToneControl.SILENCE;
                if (_skipColon <= 32 || _skipColon == 47) {
                    this._inputPtr--;
                    _skipColon = _skipWS();
                }
            }
        }
        switch (_skipColon) {
            case 34:
                this._tokenIncomplete = true;
                this._nextToken = JsonToken.VALUE_STRING;
                return;
            case 45:
            case 48:
            case c.KEY_NUM1:
            case 50:
            case c.KEY_NUM3:
            case c.KEY_NUM4:
            case c.KEY_NUM5:
            case c.KEY_NUM6:
            case c.KEY_NUM7:
            case 56:
            case c.KEY_NUM9:
                this._nextToken = parseNumberText(_skipColon);
                return;
            case 91:
                this._nextToken = JsonToken.START_ARRAY;
                return;
            case 93:
            case 125:
                _reportUnexpectedChar(_skipColon, "expected a value");
                break;
            case 102:
                _matchToken("false", 1);
                this._nextToken = JsonToken.VALUE_FALSE;
                return;
            case a.PHOTO:
                _matchToken("null", 1);
                this._nextToken = JsonToken.VALUE_NULL;
                return;
            case a.TITLE:
                break;
            case 123:
                this._nextToken = JsonToken.START_OBJECT;
                return;
            default:
                this._nextToken = _handleUnexpectedValue(_skipColon);
                return;
        }
        _matchToken("true", 1);
        this._nextToken = JsonToken.VALUE_TRUE;
    }

    private final JsonToken _nextAfterName() {
        this._nameCopied = false;
        JsonToken jsonToken = this._nextToken;
        this._nextToken = null;
        if (jsonToken == JsonToken.START_ARRAY) {
            this._parsingContext = this._parsingContext.createChildArrayContext(this._tokenInputRow, this._tokenInputCol);
        } else if (jsonToken == JsonToken.START_OBJECT) {
            this._parsingContext = this._parsingContext.createChildObjectContext(this._tokenInputRow, this._tokenInputCol);
        }
        this._currToken = jsonToken;
        return jsonToken;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private final JsonToken _nextTokenNotInObject(int i) {
        if (i == 34) {
            this._tokenIncomplete = true;
            JsonToken jsonToken = JsonToken.VALUE_STRING;
            this._currToken = jsonToken;
            return jsonToken;
        }
        switch (i) {
            case 45:
            case 48:
            case c.KEY_NUM1:
            case 50:
            case c.KEY_NUM3:
            case c.KEY_NUM4:
            case c.KEY_NUM5:
            case c.KEY_NUM6:
            case c.KEY_NUM7:
            case 56:
            case c.KEY_NUM9:
                JsonToken parseNumberText = parseNumberText(i);
                this._currToken = parseNumberText;
                return parseNumberText;
            case 91:
                this._parsingContext = this._parsingContext.createChildArrayContext(this._tokenInputRow, this._tokenInputCol);
                JsonToken jsonToken2 = JsonToken.START_ARRAY;
                this._currToken = jsonToken2;
                return jsonToken2;
            case 93:
            case 125:
                _reportUnexpectedChar(i, "expected a value");
                break;
            case 102:
                _matchToken("false", 1);
                JsonToken jsonToken3 = JsonToken.VALUE_FALSE;
                this._currToken = jsonToken3;
                return jsonToken3;
            case a.PHOTO:
                _matchToken("null", 1);
                JsonToken jsonToken4 = JsonToken.VALUE_NULL;
                this._currToken = jsonToken4;
                return jsonToken4;
            case a.TITLE:
                break;
            case 123:
                this._parsingContext = this._parsingContext.createChildObjectContext(this._tokenInputRow, this._tokenInputCol);
                JsonToken jsonToken5 = JsonToken.START_OBJECT;
                this._currToken = jsonToken5;
                return jsonToken5;
            default:
                JsonToken _handleUnexpectedValue = _handleUnexpectedValue(i);
                this._currToken = _handleUnexpectedValue;
                return _handleUnexpectedValue;
        }
        _matchToken("true", 1);
        JsonToken jsonToken6 = JsonToken.VALUE_TRUE;
        this._currToken = jsonToken6;
        return jsonToken6;
    }

    private final JsonToken _parseFloatText(char[] cArr, int i, int i2, boolean z, int i3) {
        int i4;
        char[] cArr2;
        int i5;
        int i6;
        boolean z2;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11 = 0;
        boolean z3 = false;
        if (i2 == 46) {
            int i12 = i + 1;
            cArr[i] = (char) i2;
            while (true) {
                if (this._inputPtr >= this._inputEnd && !loadMore()) {
                    z3 = true;
                    break;
                }
                byte[] bArr = this._inputBuffer;
                int i13 = this._inputPtr;
                this._inputPtr = i13 + 1;
                i2 = bArr[i13] & ToneControl.SILENCE;
                if (i2 < 48 || i2 > 57) {
                    break;
                }
                i11++;
                if (i12 >= cArr.length) {
                    cArr = this._textBuffer.finishCurrentSegment();
                    i12 = 0;
                }
                int i14 = i12;
                i12 = i14 + 1;
                cArr[i14] = (char) i2;
            }
            if (i11 == 0) {
                reportUnexpectedNumberChar(i2, "Decimal point not followed by a digit");
            }
            i4 = i11;
            i5 = i12;
            cArr2 = cArr;
        } else {
            i4 = 0;
            cArr2 = cArr;
            i5 = i;
        }
        if (i2 == 101 || i2 == 69) {
            if (i5 >= cArr2.length) {
                cArr2 = this._textBuffer.finishCurrentSegment();
                i5 = 0;
            }
            int i15 = i5 + 1;
            cArr2[i5] = (char) i2;
            if (this._inputPtr >= this._inputEnd) {
                loadMoreGuaranteed();
            }
            byte[] bArr2 = this._inputBuffer;
            int i16 = this._inputPtr;
            this._inputPtr = i16 + 1;
            byte b = bArr2[i16] & ToneControl.SILENCE;
            if (b == 45 || b == 43) {
                if (i15 >= cArr2.length) {
                    cArr2 = this._textBuffer.finishCurrentSegment();
                    i10 = 0;
                } else {
                    i10 = i15;
                }
                int i17 = i10 + 1;
                cArr2[i10] = (char) b;
                if (this._inputPtr >= this._inputEnd) {
                    loadMoreGuaranteed();
                }
                byte[] bArr3 = this._inputBuffer;
                int i18 = this._inputPtr;
                this._inputPtr = i18 + 1;
                b = bArr3[i18] & ToneControl.SILENCE;
                i9 = i17;
                i8 = 0;
            } else {
                i9 = i15;
                i8 = 0;
            }
            while (true) {
                if (b <= 57 && b >= 48) {
                    i8++;
                    if (i9 >= cArr2.length) {
                        cArr2 = this._textBuffer.finishCurrentSegment();
                        i9 = 0;
                    }
                    int i19 = i9 + 1;
                    cArr2[i9] = (char) b;
                    if (this._inputPtr >= this._inputEnd && !loadMore()) {
                        i7 = i8;
                        z2 = true;
                        i6 = i19;
                        break;
                    }
                    byte[] bArr4 = this._inputBuffer;
                    int i20 = this._inputPtr;
                    this._inputPtr = i20 + 1;
                    b = bArr4[i20] & ToneControl.SILENCE;
                    i9 = i19;
                } else {
                    z2 = z3;
                    int i21 = i8;
                    i6 = i9;
                    i7 = i21;
                }
            }
            z2 = z3;
            int i212 = i8;
            i6 = i9;
            i7 = i212;
            if (i7 == 0) {
                reportUnexpectedNumberChar(b, "Exponent indicator not followed by a digit");
            }
        } else {
            z2 = z3;
            i6 = i5;
            i7 = 0;
        }
        if (!z2) {
            this._inputPtr--;
        }
        this._textBuffer.setCurrentLength(i6);
        return resetFloat(z, i3, i4, i7);
    }

    private final JsonToken _parserNumber2(char[] cArr, int i, boolean z, int i2) {
        byte b;
        int i3 = i2;
        int i4 = i;
        char[] cArr2 = cArr;
        while (true) {
            if (this._inputPtr < this._inputEnd || loadMore()) {
                byte[] bArr = this._inputBuffer;
                int i5 = this._inputPtr;
                this._inputPtr = i5 + 1;
                b = bArr[i5] & ToneControl.SILENCE;
                if (b <= 57 && b >= 48) {
                    if (i4 >= cArr2.length) {
                        cArr2 = this._textBuffer.finishCurrentSegment();
                        i4 = 0;
                    }
                    int i6 = i4;
                    i4 = i6 + 1;
                    cArr2[i6] = (char) b;
                    i3++;
                }
            } else {
                this._textBuffer.setCurrentLength(i4);
                return resetInt(z, i3);
            }
        }
        if (b == 46 || b == 101 || b == 69) {
            return _parseFloatText(cArr2, i4, b, z, i3);
        }
        this._inputPtr--;
        this._textBuffer.setCurrentLength(i4);
        return resetInt(z, i3);
    }

    private final void _skipCComment() {
        int[] inputCodeComment = CharTypes.getInputCodeComment();
        while (true) {
            if (this._inputPtr < this._inputEnd || loadMore()) {
                byte[] bArr = this._inputBuffer;
                int i = this._inputPtr;
                this._inputPtr = i + 1;
                byte b = bArr[i] & ToneControl.SILENCE;
                int i2 = inputCodeComment[b];
                if (i2 != 0) {
                    switch (i2) {
                        case 2:
                            _skipUtf8_2(b);
                            continue;
                        case 3:
                            _skipUtf8_3(b);
                            continue;
                        case 4:
                            _skipUtf8_4(b);
                            continue;
                        case 10:
                            _skipLF();
                            continue;
                        case 13:
                            _skipCR();
                            continue;
                        case c.KEY_STAR:
                            if (this._inputPtr >= this._inputEnd && !loadMore()) {
                                break;
                            } else if (this._inputBuffer[this._inputPtr] == 47) {
                                this._inputPtr++;
                                return;
                            } else {
                                continue;
                            }
                        default:
                            _reportInvalidChar(b);
                            continue;
                    }
                }
            }
        }
        _reportInvalidEOF(" in a comment");
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0086  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final int _skipColon() {
        /*
            r6 = this;
            r5 = 58
            r4 = 47
            r3 = 32
            int r0 = r6._inputPtr
            int r1 = r6._inputEnd
            if (r0 < r1) goto L_0x000f
            r6.loadMoreGuaranteed()
        L_0x000f:
            byte[] r0 = r6._inputBuffer
            int r1 = r6._inputPtr
            int r2 = r1 + 1
            r6._inputPtr = r2
            byte r0 = r0[r1]
            if (r0 != r5) goto L_0x0034
            int r0 = r6._inputPtr
            int r1 = r6._inputEnd
            if (r0 >= r1) goto L_0x005a
            byte[] r0 = r6._inputBuffer
            int r1 = r6._inputPtr
            byte r0 = r0[r1]
            r0 = r0 & 255(0xff, float:3.57E-43)
            if (r0 <= r3) goto L_0x005a
            if (r0 == r4) goto L_0x005a
            int r1 = r6._inputPtr
            int r1 = r1 + 1
            r6._inputPtr = r1
        L_0x0033:
            return r0
        L_0x0034:
            r0 = r0 & 255(0xff, float:3.57E-43)
        L_0x0036:
            switch(r0) {
                case 9: goto L_0x007a;
                case 10: goto L_0x007e;
                case 13: goto L_0x007a;
                case 32: goto L_0x007a;
                case 47: goto L_0x0082;
                default: goto L_0x0039;
            }
        L_0x0039:
            if (r0 >= r3) goto L_0x003e
            r6._throwInvalidSpace(r0)
        L_0x003e:
            int r0 = r6._inputPtr
            int r1 = r6._inputEnd
            if (r0 < r1) goto L_0x0047
            r6.loadMoreGuaranteed()
        L_0x0047:
            byte[] r0 = r6._inputBuffer
            int r1 = r6._inputPtr
            int r2 = r1 + 1
            r6._inputPtr = r2
            byte r0 = r0[r1]
            r0 = r0 & 255(0xff, float:3.57E-43)
            if (r0 == r5) goto L_0x005a
            java.lang.String r1 = "was expecting a colon to separate field name and value"
            r6._reportUnexpectedChar(r0, r1)
        L_0x005a:
            int r0 = r6._inputPtr
            int r1 = r6._inputEnd
            if (r0 < r1) goto L_0x0066
            boolean r0 = r6.loadMore()
            if (r0 == 0) goto L_0x00a0
        L_0x0066:
            byte[] r0 = r6._inputBuffer
            int r1 = r6._inputPtr
            int r2 = r1 + 1
            r6._inputPtr = r2
            byte r0 = r0[r1]
            r0 = r0 & 255(0xff, float:3.57E-43)
            if (r0 <= r3) goto L_0x0086
            if (r0 != r4) goto L_0x0033
            r6._skipComment()
            goto L_0x005a
        L_0x007a:
            r6._skipCR()
            goto L_0x0036
        L_0x007e:
            r6._skipLF()
            goto L_0x0036
        L_0x0082:
            r6._skipComment()
            goto L_0x0036
        L_0x0086:
            if (r0 == r3) goto L_0x005a
            r1 = 10
            if (r0 != r1) goto L_0x0090
            r6._skipLF()
            goto L_0x005a
        L_0x0090:
            r1 = 13
            if (r0 != r1) goto L_0x0098
            r6._skipCR()
            goto L_0x005a
        L_0x0098:
            r1 = 9
            if (r0 == r1) goto L_0x005a
            r6._throwInvalidSpace(r0)
            goto L_0x005a
        L_0x00a0:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Unexpected end-of-input within/between "
            java.lang.StringBuilder r0 = r0.append(r1)
            com.fasterxml.jackson.core.json.JsonReadContext r1 = r6._parsingContext
            java.lang.String r1 = r1.getTypeDesc()
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = " entries"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.fasterxml.jackson.core.JsonParseException r0 = r6._constructError(r0)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.core.json.UTF8StreamJsonParser._skipColon():int");
    }

    private final void _skipComment() {
        if (!isEnabled(JsonParser.Feature.ALLOW_COMMENTS)) {
            _reportUnexpectedChar(47, "maybe a (non-standard) comment? (not recognized as one since Feature 'ALLOW_COMMENTS' not enabled for parser)");
        }
        if (this._inputPtr >= this._inputEnd && !loadMore()) {
            _reportInvalidEOF(" in a comment");
        }
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        byte b = bArr[i] & ToneControl.SILENCE;
        if (b == 47) {
            _skipCppComment();
        } else if (b == 42) {
            _skipCComment();
        } else {
            _reportUnexpectedChar(b, "was expecting either '*' or '/' for a comment");
        }
    }

    private final void _skipCppComment() {
        int[] inputCodeComment = CharTypes.getInputCodeComment();
        while (true) {
            if (this._inputPtr < this._inputEnd || loadMore()) {
                byte[] bArr = this._inputBuffer;
                int i = this._inputPtr;
                this._inputPtr = i + 1;
                byte b = bArr[i] & ToneControl.SILENCE;
                int i2 = inputCodeComment[b];
                if (i2 != 0) {
                    switch (i2) {
                        case 2:
                            _skipUtf8_2(b);
                            continue;
                        case 3:
                            _skipUtf8_3(b);
                            continue;
                        case 4:
                            _skipUtf8_4(b);
                            continue;
                        case 10:
                            _skipLF();
                            return;
                        case 13:
                            _skipCR();
                            return;
                        case c.KEY_STAR:
                            break;
                        default:
                            _reportInvalidChar(b);
                            continue;
                    }
                }
            } else {
                return;
            }
        }
    }

    private final void _skipUtf8_2(int i) {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        byte b = bArr[i2];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & ToneControl.SILENCE, this._inputPtr);
        }
    }

    private final void _skipUtf8_3(int i) {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        byte b = bArr[i2];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & ToneControl.SILENCE, this._inputPtr);
        }
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr2 = this._inputBuffer;
        int i3 = this._inputPtr;
        this._inputPtr = i3 + 1;
        byte b2 = bArr2[i3];
        if ((b2 & 192) != 128) {
            _reportInvalidOther(b2 & ToneControl.SILENCE, this._inputPtr);
        }
    }

    private final void _skipUtf8_4(int i) {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        byte b = bArr[i2];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & ToneControl.SILENCE, this._inputPtr);
        }
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr2 = this._inputBuffer;
        int i3 = this._inputPtr;
        this._inputPtr = i3 + 1;
        byte b2 = bArr2[i3];
        if ((b2 & 192) != 128) {
            _reportInvalidOther(b2 & ToneControl.SILENCE, this._inputPtr);
        }
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr3 = this._inputBuffer;
        int i4 = this._inputPtr;
        this._inputPtr = i4 + 1;
        byte b3 = bArr3[i4];
        if ((b3 & 192) != 128) {
            _reportInvalidOther(b3 & ToneControl.SILENCE, this._inputPtr);
        }
    }

    private final int _skipWS() {
        while (true) {
            if (this._inputPtr < this._inputEnd || loadMore()) {
                byte[] bArr = this._inputBuffer;
                int i = this._inputPtr;
                this._inputPtr = i + 1;
                byte b = bArr[i] & ToneControl.SILENCE;
                if (b > 32) {
                    if (b != 47) {
                        return b;
                    }
                    _skipComment();
                } else if (b != 32) {
                    if (b == 10) {
                        _skipLF();
                    } else if (b == 13) {
                        _skipCR();
                    } else if (b != 9) {
                        _throwInvalidSpace(b);
                    }
                }
            } else {
                throw _constructError("Unexpected end-of-input within/between " + this._parsingContext.getTypeDesc() + " entries");
            }
        }
    }

    private final int _skipWSOrEnd() {
        while (true) {
            if (this._inputPtr < this._inputEnd || loadMore()) {
                byte[] bArr = this._inputBuffer;
                int i = this._inputPtr;
                this._inputPtr = i + 1;
                byte b = bArr[i] & ToneControl.SILENCE;
                if (b > 32) {
                    if (b != 47) {
                        return b;
                    }
                    _skipComment();
                } else if (b != 32) {
                    if (b == 10) {
                        _skipLF();
                    } else if (b == 13) {
                        _skipCR();
                    } else if (b != 9) {
                        _throwInvalidSpace(b);
                    }
                }
            } else {
                _handleEOF();
                return -1;
            }
        }
    }

    private final int _verifyNoLeadingZeroes() {
        if (this._inputPtr >= this._inputEnd && !loadMore()) {
            return 48;
        }
        byte b = this._inputBuffer[this._inputPtr] & ToneControl.SILENCE;
        if (b < 48 || b > 57) {
            return 48;
        }
        if (!isEnabled(JsonParser.Feature.ALLOW_NUMERIC_LEADING_ZEROS)) {
            reportInvalidNumber("Leading zeroes not allowed");
        }
        this._inputPtr++;
        if (b != 48) {
            return b;
        }
        do {
            if (this._inputPtr >= this._inputEnd && !loadMore()) {
                return b;
            }
            b = this._inputBuffer[this._inputPtr] & ToneControl.SILENCE;
            if (b < 48 || b > 57) {
                return 48;
            }
            this._inputPtr++;
        } while (b == 48);
        return b;
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00d1 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final com.fasterxml.jackson.core.sym.Name addName(int[] r12, int r13, int r14) {
        /*
            r11 = this;
            int r0 = r13 << 2
            int r0 = r0 + -4
            int r6 = r0 + r14
            r0 = 4
            if (r14 >= r0) goto L_0x00da
            int r0 = r13 + -1
            r0 = r12[r0]
            int r1 = r13 + -1
            int r2 = 4 - r14
            int r2 = r2 << 3
            int r2 = r0 << r2
            r12[r1] = r2
        L_0x0017:
            com.fasterxml.jackson.core.util.TextBuffer r1 = r11._textBuffer
            char[] r1 = r1.emptyAndGetCurrentSegment()
            r5 = 0
            r2 = 0
            r3 = r2
        L_0x0020:
            if (r3 >= r6) goto L_0x0100
            int r2 = r3 >> 2
            r2 = r12[r2]
            r4 = r3 & 3
            int r4 = 3 - r4
            int r4 = r4 << 3
            int r2 = r2 >> r4
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r3 = r3 + 1
            r4 = 127(0x7f, float:1.78E-43)
            if (r2 <= r4) goto L_0x0114
            r4 = r2 & 224(0xe0, float:3.14E-43)
            r7 = 192(0xc0, float:2.69E-43)
            if (r4 != r7) goto L_0x00dd
            r4 = r2 & 31
            r2 = 1
            r10 = r2
            r2 = r4
            r4 = r10
        L_0x0041:
            int r7 = r3 + r4
            if (r7 <= r6) goto L_0x004a
            java.lang.String r7 = " in field name"
            r11._reportInvalidEOF(r7)
        L_0x004a:
            int r7 = r3 >> 2
            r7 = r12[r7]
            r8 = r3 & 3
            int r8 = 3 - r8
            int r8 = r8 << 3
            int r7 = r7 >> r8
            int r3 = r3 + 1
            r8 = r7 & 192(0xc0, float:2.69E-43)
            r9 = 128(0x80, float:1.794E-43)
            if (r8 == r9) goto L_0x0060
            r11._reportInvalidOther(r7)
        L_0x0060:
            int r2 = r2 << 6
            r7 = r7 & 63
            r2 = r2 | r7
            r7 = 1
            if (r4 <= r7) goto L_0x00a3
            int r7 = r3 >> 2
            r7 = r12[r7]
            r8 = r3 & 3
            int r8 = 3 - r8
            int r8 = r8 << 3
            int r7 = r7 >> r8
            int r3 = r3 + 1
            r8 = r7 & 192(0xc0, float:2.69E-43)
            r9 = 128(0x80, float:1.794E-43)
            if (r8 == r9) goto L_0x007e
            r11._reportInvalidOther(r7)
        L_0x007e:
            int r2 = r2 << 6
            r7 = r7 & 63
            r2 = r2 | r7
            r7 = 2
            if (r4 <= r7) goto L_0x00a3
            int r7 = r3 >> 2
            r7 = r12[r7]
            r8 = r3 & 3
            int r8 = 3 - r8
            int r8 = r8 << 3
            int r7 = r7 >> r8
            int r3 = r3 + 1
            r8 = r7 & 192(0xc0, float:2.69E-43)
            r9 = 128(0x80, float:1.794E-43)
            if (r8 == r9) goto L_0x009e
            r8 = r7 & 255(0xff, float:3.57E-43)
            r11._reportInvalidOther(r8)
        L_0x009e:
            int r2 = r2 << 6
            r7 = r7 & 63
            r2 = r2 | r7
        L_0x00a3:
            r7 = 2
            if (r4 <= r7) goto L_0x0114
            r4 = 65536(0x10000, float:9.18355E-41)
            int r2 = r2 - r4
            int r4 = r1.length
            if (r5 < r4) goto L_0x00b2
            com.fasterxml.jackson.core.util.TextBuffer r1 = r11._textBuffer
            char[] r1 = r1.expandCurrentSegment()
        L_0x00b2:
            int r4 = r5 + 1
            r7 = 55296(0xd800, float:7.7486E-41)
            int r8 = r2 >> 10
            int r7 = r7 + r8
            char r7 = (char) r7
            r1[r5] = r7
            r5 = 56320(0xdc00, float:7.8921E-41)
            r2 = r2 & 1023(0x3ff, float:1.434E-42)
            r2 = r2 | r5
            r10 = r2
            r2 = r3
            r3 = r4
            r4 = r1
            r1 = r10
        L_0x00c8:
            int r5 = r4.length
            if (r3 < r5) goto L_0x00d1
            com.fasterxml.jackson.core.util.TextBuffer r4 = r11._textBuffer
            char[] r4 = r4.expandCurrentSegment()
        L_0x00d1:
            int r5 = r3 + 1
            char r1 = (char) r1
            r4[r3] = r1
            r3 = r2
            r1 = r4
            goto L_0x0020
        L_0x00da:
            r0 = 0
            goto L_0x0017
        L_0x00dd:
            r4 = r2 & 240(0xf0, float:3.36E-43)
            r7 = 224(0xe0, float:3.14E-43)
            if (r4 != r7) goto L_0x00eb
            r4 = r2 & 15
            r2 = 2
            r10 = r2
            r2 = r4
            r4 = r10
            goto L_0x0041
        L_0x00eb:
            r4 = r2 & 248(0xf8, float:3.48E-43)
            r7 = 240(0xf0, float:3.36E-43)
            if (r4 != r7) goto L_0x00f9
            r4 = r2 & 7
            r2 = 3
            r10 = r2
            r2 = r4
            r4 = r10
            goto L_0x0041
        L_0x00f9:
            r11._reportInvalidInitial(r2)
            r2 = 1
            r4 = r2
            goto L_0x0041
        L_0x0100:
            java.lang.String r2 = new java.lang.String
            r3 = 0
            r2.<init>(r1, r3, r5)
            r1 = 4
            if (r14 >= r1) goto L_0x010d
            int r1 = r13 + -1
            r12[r1] = r0
        L_0x010d:
            com.fasterxml.jackson.core.sym.BytesToNameCanonicalizer r0 = r11._symbols
            com.fasterxml.jackson.core.sym.Name r0 = r0.addName(r2, r12, r13)
            return r0
        L_0x0114:
            r4 = r1
            r1 = r2
            r2 = r3
            r3 = r5
            goto L_0x00c8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.core.json.UTF8StreamJsonParser.addName(int[], int, int):com.fasterxml.jackson.core.sym.Name");
    }

    private final Name findName(int i, int i2) {
        Name findName = this._symbols.findName(i);
        if (findName != null) {
            return findName;
        }
        this._quadBuffer[0] = i;
        return addName(this._quadBuffer, 1, i2);
    }

    private final Name findName(int i, int i2, int i3) {
        Name findName = this._symbols.findName(i, i2);
        if (findName != null) {
            return findName;
        }
        this._quadBuffer[0] = i;
        this._quadBuffer[1] = i2;
        return addName(this._quadBuffer, 2, i3);
    }

    private final Name findName(int[] iArr, int i, int i2, int i3) {
        if (i >= iArr.length) {
            iArr = growArrayBy(iArr, iArr.length);
            this._quadBuffer = iArr;
        }
        int i4 = i + 1;
        iArr[i] = i2;
        Name findName = this._symbols.findName(iArr, i4);
        return findName == null ? addName(iArr, i4, i3) : findName;
    }

    public static int[] growArrayBy(int[] iArr, int i) {
        if (iArr == null) {
            return new int[i];
        }
        int length = iArr.length;
        int[] iArr2 = new int[(length + i)];
        System.arraycopy(iArr, 0, iArr2, 0, length);
        return iArr2;
    }

    private int nextByte() {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        return bArr[i] & ToneControl.SILENCE;
    }

    private final Name parseFieldName(int i, int i2, int i3) {
        return parseEscapedFieldName(this._quadBuffer, 0, i, i2, i3);
    }

    private final Name parseFieldName(int i, int i2, int i3, int i4) {
        this._quadBuffer[0] = i;
        return parseEscapedFieldName(this._quadBuffer, 1, i2, i3, i4);
    }

    /* access modifiers changed from: protected */
    public void _closeInput() {
        if (this._inputStream != null) {
            if (this._ioContext.isResourceManaged() || isEnabled(JsonParser.Feature.AUTO_CLOSE_SOURCE)) {
                this._inputStream.close();
            }
            this._inputStream = null;
        }
    }

    /* access modifiers changed from: protected */
    public byte[] _decodeBase64(Base64Variant base64Variant) {
        ByteArrayBuilder _getByteArrayBuilder = _getByteArrayBuilder();
        while (true) {
            if (this._inputPtr >= this._inputEnd) {
                loadMoreGuaranteed();
            }
            byte[] bArr = this._inputBuffer;
            int i = this._inputPtr;
            this._inputPtr = i + 1;
            byte b = bArr[i] & ToneControl.SILENCE;
            if (b > 32) {
                int decodeBase64Char = base64Variant.decodeBase64Char(b);
                if (decodeBase64Char < 0) {
                    if (b == 34) {
                        return _getByteArrayBuilder.toByteArray();
                    }
                    decodeBase64Char = _decodeBase64Escape(base64Variant, b, 0);
                    if (decodeBase64Char < 0) {
                        continue;
                    }
                }
                if (this._inputPtr >= this._inputEnd) {
                    loadMoreGuaranteed();
                }
                byte[] bArr2 = this._inputBuffer;
                int i2 = this._inputPtr;
                this._inputPtr = i2 + 1;
                byte b2 = bArr2[i2] & ToneControl.SILENCE;
                int decodeBase64Char2 = base64Variant.decodeBase64Char(b2);
                if (decodeBase64Char2 < 0) {
                    decodeBase64Char2 = _decodeBase64Escape(base64Variant, b2, 1);
                }
                int i3 = decodeBase64Char2 | (decodeBase64Char << 6);
                if (this._inputPtr >= this._inputEnd) {
                    loadMoreGuaranteed();
                }
                byte[] bArr3 = this._inputBuffer;
                int i4 = this._inputPtr;
                this._inputPtr = i4 + 1;
                byte b3 = bArr3[i4] & ToneControl.SILENCE;
                int decodeBase64Char3 = base64Variant.decodeBase64Char(b3);
                if (decodeBase64Char3 < 0) {
                    if (decodeBase64Char3 != -2) {
                        if (b3 != 34 || base64Variant.usesPadding()) {
                            decodeBase64Char3 = _decodeBase64Escape(base64Variant, b3, 2);
                        } else {
                            _getByteArrayBuilder.append(i3 >> 4);
                            return _getByteArrayBuilder.toByteArray();
                        }
                    }
                    if (decodeBase64Char3 == -2) {
                        if (this._inputPtr >= this._inputEnd) {
                            loadMoreGuaranteed();
                        }
                        byte[] bArr4 = this._inputBuffer;
                        int i5 = this._inputPtr;
                        this._inputPtr = i5 + 1;
                        byte b4 = bArr4[i5] & ToneControl.SILENCE;
                        if (!base64Variant.usesPaddingChar(b4)) {
                            throw reportInvalidBase64Char(base64Variant, b4, 3, "expected padding character '" + base64Variant.getPaddingChar() + "'");
                        }
                        _getByteArrayBuilder.append(i3 >> 4);
                    }
                }
                int i6 = (i3 << 6) | decodeBase64Char3;
                if (this._inputPtr >= this._inputEnd) {
                    loadMoreGuaranteed();
                }
                byte[] bArr5 = this._inputBuffer;
                int i7 = this._inputPtr;
                this._inputPtr = i7 + 1;
                byte b5 = bArr5[i7] & ToneControl.SILENCE;
                int decodeBase64Char4 = base64Variant.decodeBase64Char(b5);
                if (decodeBase64Char4 < 0) {
                    if (decodeBase64Char4 != -2) {
                        if (b5 != 34 || base64Variant.usesPadding()) {
                            decodeBase64Char4 = _decodeBase64Escape(base64Variant, b5, 3);
                        } else {
                            _getByteArrayBuilder.appendTwoBytes(i6 >> 2);
                            return _getByteArrayBuilder.toByteArray();
                        }
                    }
                    if (decodeBase64Char4 == -2) {
                        _getByteArrayBuilder.appendTwoBytes(i6 >> 2);
                    }
                }
                _getByteArrayBuilder.appendThreeBytes(decodeBase64Char4 | (i6 << 6));
            }
        }
    }

    /* access modifiers changed from: protected */
    public int _decodeCharForError(int i) {
        char c;
        if (i >= 0) {
            return i;
        }
        if ((i & f.OBEX_DATABASE_FULL) == 192) {
            i &= 31;
            c = 1;
        } else if ((i & 240) == 224) {
            i &= 15;
            c = 2;
        } else if ((i & 248) == 240) {
            i &= 7;
            c = 3;
        } else {
            _reportInvalidInitial(i & 255);
            c = 1;
        }
        int nextByte = nextByte();
        if ((nextByte & 192) != 128) {
            _reportInvalidOther(nextByte & 255);
        }
        int i2 = (i << 6) | (nextByte & 63);
        if (c <= 1) {
            return i2;
        }
        int nextByte2 = nextByte();
        if ((nextByte2 & 192) != 128) {
            _reportInvalidOther(nextByte2 & 255);
        }
        int i3 = (i2 << 6) | (nextByte2 & 63);
        if (c <= 2) {
            return i3;
        }
        int nextByte3 = nextByte();
        if ((nextByte3 & 192) != 128) {
            _reportInvalidOther(nextByte3 & 255);
        }
        return (i3 << 6) | (nextByte3 & 63);
    }

    /* access modifiers changed from: protected */
    public final char _decodeEscaped() {
        if (this._inputPtr >= this._inputEnd && !loadMore()) {
            _reportInvalidEOF(" in character escape sequence");
        }
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        byte b = bArr[i];
        switch (b) {
            case 34:
            case 47:
            case 92:
                return (char) b;
            case 98:
                return 8;
            case 102:
                return 12;
            case a.PHOTO:
                return 10;
            case a.REVISION:
                return 13;
            case a.TITLE:
                return 9;
            case a.UID:
                int i2 = 0;
                for (int i3 = 0; i3 < 4; i3++) {
                    if (this._inputPtr >= this._inputEnd && !loadMore()) {
                        _reportInvalidEOF(" in character escape sequence");
                    }
                    byte[] bArr2 = this._inputBuffer;
                    int i4 = this._inputPtr;
                    this._inputPtr = i4 + 1;
                    byte b2 = bArr2[i4];
                    int charToHex = CharTypes.charToHex(b2);
                    if (charToHex < 0) {
                        _reportUnexpectedChar(b2, "expected a hex-digit for character escape sequence");
                    }
                    i2 = (i2 << 4) | charToHex;
                }
                return (char) i2;
            default:
                return _handleUnrecognizedCharacterEscape((char) _decodeCharForError(b));
        }
    }

    /* access modifiers changed from: protected */
    public void _finishString() {
        int i = this._inputPtr;
        if (i >= this._inputEnd) {
            loadMoreGuaranteed();
            i = this._inputPtr;
        }
        char[] emptyAndGetCurrentSegment = this._textBuffer.emptyAndGetCurrentSegment();
        int[] iArr = sInputCodesUtf8;
        int min = Math.min(this._inputEnd, emptyAndGetCurrentSegment.length + i);
        byte[] bArr = this._inputBuffer;
        int i2 = i;
        int i3 = 0;
        while (true) {
            if (i2 >= min) {
                break;
            }
            byte b = bArr[i2] & ToneControl.SILENCE;
            if (iArr[b] == 0) {
                emptyAndGetCurrentSegment[i3] = (char) b;
                i3++;
                i2++;
            } else if (b == 34) {
                this._inputPtr = i2 + 1;
                this._textBuffer.setCurrentLength(i3);
                return;
            }
        }
        this._inputPtr = i2;
        _finishString2(emptyAndGetCurrentSegment, i3);
    }

    /* access modifiers changed from: protected */
    public final String _getText2(JsonToken jsonToken) {
        if (jsonToken == null) {
            return null;
        }
        switch (jsonToken) {
            case FIELD_NAME:
                return this._parsingContext.getCurrentName();
            case VALUE_STRING:
            case VALUE_NUMBER_INT:
            case VALUE_NUMBER_FLOAT:
                return this._textBuffer.contentsAsString();
            default:
                return jsonToken.asString();
        }
    }

    /* access modifiers changed from: protected */
    public JsonToken _handleApostropheValue() {
        int i;
        int i2;
        char[] emptyAndGetCurrentSegment = this._textBuffer.emptyAndGetCurrentSegment();
        int[] iArr = sInputCodesUtf8;
        byte[] bArr = this._inputBuffer;
        int i3 = 0;
        while (true) {
            if (this._inputPtr >= this._inputEnd) {
                loadMoreGuaranteed();
            }
            if (i3 >= emptyAndGetCurrentSegment.length) {
                emptyAndGetCurrentSegment = this._textBuffer.finishCurrentSegment();
                i3 = 0;
            }
            int i4 = this._inputEnd;
            int length = this._inputPtr + (emptyAndGetCurrentSegment.length - i3);
            if (length >= i4) {
                length = i4;
            }
            while (true) {
                if (this._inputPtr < length) {
                    int i5 = this._inputPtr;
                    this._inputPtr = i5 + 1;
                    byte b = bArr[i5] & ToneControl.SILENCE;
                    if (b != 39 && iArr[b] == 0) {
                        emptyAndGetCurrentSegment[i3] = (char) b;
                        i3++;
                    } else if (b == 39) {
                        this._textBuffer.setCurrentLength(i3);
                        return JsonToken.VALUE_STRING;
                    } else {
                        switch (iArr[b]) {
                            case 1:
                                if (b != 34) {
                                    i = _decodeEscaped();
                                    break;
                                }
                                i = b;
                                break;
                            case 2:
                                i = _decodeUtf8_2(b);
                                break;
                            case 3:
                                if (this._inputEnd - this._inputPtr < 2) {
                                    i = _decodeUtf8_3(b);
                                    break;
                                } else {
                                    i = _decodeUtf8_3fast(b);
                                    break;
                                }
                            case 4:
                                int _decodeUtf8_4 = _decodeUtf8_4(b);
                                int i6 = i3 + 1;
                                emptyAndGetCurrentSegment[i3] = (char) (55296 | (_decodeUtf8_4 >> 10));
                                if (i6 >= emptyAndGetCurrentSegment.length) {
                                    emptyAndGetCurrentSegment = this._textBuffer.finishCurrentSegment();
                                    i3 = 0;
                                } else {
                                    i3 = i6;
                                }
                                i = 56320 | (_decodeUtf8_4 & 1023);
                                break;
                            default:
                                if (b < 32) {
                                    _throwUnquotedSpace(b, "string value");
                                }
                                _reportInvalidChar(b);
                                i = b;
                                break;
                        }
                        if (i3 >= emptyAndGetCurrentSegment.length) {
                            emptyAndGetCurrentSegment = this._textBuffer.finishCurrentSegment();
                            i2 = 0;
                        } else {
                            i2 = i3;
                        }
                        i3 = i2 + 1;
                        emptyAndGetCurrentSegment[i2] = (char) i;
                    }
                }
            }
        }
    }

    /*  JADX ERROR: JadxRuntimeException in pass: InitCodeVariables
        jadx.core.utils.exceptions.JadxRuntimeException: Several immutable types in one variable: [int, byte], vars: [r9v0 ?, r9v1 ?, r9v2 ?]
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVarType(InitCodeVariables.java:102)
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVar(InitCodeVariables.java:78)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVar(InitCodeVariables.java:69)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVars(InitCodeVariables.java:48)
        	at jadx.core.dex.visitors.InitCodeVariables.visit(InitCodeVariables.java:32)
        */
    protected com.fasterxml.jackson.core.JsonToken _handleInvalidNumberStart(
/*
Method generation error in method: com.fasterxml.jackson.core.json.UTF8StreamJsonParser._handleInvalidNumberStart(int, boolean):com.fasterxml.jackson.core.JsonToken, dex: classes.dex
    jadx.core.utils.exceptions.JadxRuntimeException: Code variable not set in r9v0 ?
    	at jadx.core.dex.instructions.args.SSAVar.getCodeVar(SSAVar.java:189)
    	at jadx.core.codegen.MethodGen.addMethodArguments(MethodGen.java:161)
    	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:133)
    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:317)
    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:275)
    	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$3(ClassGen.java:244)
    	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
    	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
    	at java.base/java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
    	at java.base/java.util.stream.Sink$ChainedReference.end(Sink.java:258)
    	at java.base/java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:485)
    	at java.base/java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:474)
    	at java.base/java.util.stream.ForEachOps$ForEachOp.evaluateSequential(ForEachOps.java:150)
    	at java.base/java.util.stream.ForEachOps$ForEachOp$OfRef.evaluateSequential(ForEachOps.java:173)
    	at java.base/java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)
    	at java.base/java.util.stream.ReferencePipeline.forEach(ReferencePipeline.java:497)
    	at jadx.core.codegen.ClassGen.addInnerClsAndMethods(ClassGen.java:240)
    	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:231)
    	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:115)
    	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:81)
    	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:45)
    	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:34)
    	at jadx.core.codegen.CodeGen.generate(CodeGen.java:22)
    	at jadx.core.ProcessClass.generateCode(ProcessClass.java:61)
    	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
    	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
    
*/

    /* access modifiers changed from: protected */
    public JsonToken _handleUnexpectedValue(int i) {
        switch (i) {
            case 39:
                if (isEnabled(JsonParser.Feature.ALLOW_SINGLE_QUOTES)) {
                    return _handleApostropheValue();
                }
                break;
            case 43:
                if (this._inputPtr >= this._inputEnd && !loadMore()) {
                    _reportInvalidEOFInValue();
                }
                byte[] bArr = this._inputBuffer;
                int i2 = this._inputPtr;
                this._inputPtr = i2 + 1;
                return _handleInvalidNumberStart(bArr[i2] & ToneControl.SILENCE, false);
            case 78:
                _matchToken("NaN", 1);
                if (!isEnabled(JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS)) {
                    _reportError("Non-standard token 'NaN': enable JsonParser.Feature.ALLOW_NON_NUMERIC_NUMBERS to allow");
                    break;
                } else {
                    return resetAsNaN("NaN", Double.NaN);
                }
        }
        _reportUnexpectedChar(i, "expected a valid value (number, String, array, object, 'true', 'false' or 'null')");
        return null;
    }

    /* access modifiers changed from: protected */
    public final Name _handleUnusualFieldName(int i) {
        int[] iArr;
        int i2;
        int i3;
        int i4;
        if (i == 39 && isEnabled(JsonParser.Feature.ALLOW_SINGLE_QUOTES)) {
            return _parseApostropheFieldName();
        }
        if (!isEnabled(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES)) {
            _reportUnexpectedChar(i, "was expecting double-quote to start field name");
        }
        int[] inputCodeUtf8JsNames = CharTypes.getInputCodeUtf8JsNames();
        if (inputCodeUtf8JsNames[i] != 0) {
            _reportUnexpectedChar(i, "was expecting either valid name character (for unquoted name) or double-quote (for quoted) to start field name");
        }
        int i5 = 0;
        int i6 = 0;
        byte b = i;
        int i7 = 0;
        int[] iArr2 = this._quadBuffer;
        while (true) {
            if (i5 < 4) {
                int i8 = i5 + 1;
                i3 = b | (i6 << 8);
                i4 = i7;
                iArr = iArr2;
                i2 = i8;
            } else {
                if (i7 >= iArr2.length) {
                    iArr2 = growArrayBy(iArr2, iArr2.length);
                    this._quadBuffer = iArr2;
                }
                int i9 = i7 + 1;
                iArr2[i7] = i6;
                iArr = iArr2;
                i2 = 1;
                i3 = b;
                i4 = i9;
            }
            if (this._inputPtr >= this._inputEnd && !loadMore()) {
                _reportInvalidEOF(" in field name");
            }
            byte b2 = this._inputBuffer[this._inputPtr] & ToneControl.SILENCE;
            if (inputCodeUtf8JsNames[b2] != 0) {
                break;
            }
            this._inputPtr++;
            i6 = i3;
            i5 = i2;
            iArr2 = iArr;
            i7 = i4;
            b = b2;
        }
        if (i2 > 0) {
            if (i4 >= iArr.length) {
                iArr = growArrayBy(iArr, iArr.length);
                this._quadBuffer = iArr;
            }
            iArr[i4] = i3;
            i4++;
        }
        Name findName = this._symbols.findName(iArr, i4);
        return findName == null ? addName(iArr, i4, i2) : findName;
    }

    /* access modifiers changed from: protected */
    public final boolean _loadToHaveAtLeast(int i) {
        if (this._inputStream == null) {
            return false;
        }
        int i2 = this._inputEnd - this._inputPtr;
        if (i2 <= 0 || this._inputPtr <= 0) {
            this._inputEnd = 0;
        } else {
            this._currInputProcessed += (long) this._inputPtr;
            this._currInputRowStart -= this._inputPtr;
            System.arraycopy(this._inputBuffer, this._inputPtr, this._inputBuffer, 0, i2);
            this._inputEnd = i2;
        }
        this._inputPtr = 0;
        while (this._inputEnd < i) {
            int read = this._inputStream.read(this._inputBuffer, this._inputEnd, this._inputBuffer.length - this._inputEnd);
            if (read < 1) {
                _closeInput();
                if (read != 0) {
                    return false;
                }
                throw new IOException("InputStream.read() returned 0 characters when trying to read " + i2 + " bytes");
            }
            this._inputEnd = read + this._inputEnd;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final void _matchToken(String str, int i) {
        byte b;
        int length = str.length();
        do {
            if (this._inputPtr >= this._inputEnd && !loadMore()) {
                _reportInvalidEOF(" in a value");
            }
            if (this._inputBuffer[this._inputPtr] != str.charAt(i)) {
                _reportInvalidToken(str.substring(0, i), "'null', 'true', 'false' or NaN");
            }
            this._inputPtr++;
            i++;
        } while (i < length);
        if ((this._inputPtr < this._inputEnd || loadMore()) && (b = this._inputBuffer[this._inputPtr] & ToneControl.SILENCE) >= 48 && b != 93 && b != 125 && Character.isJavaIdentifierPart((char) _decodeCharForError(b))) {
            this._inputPtr++;
            _reportInvalidToken(str.substring(0, i), "'null', 'true', 'false' or NaN");
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00f3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.fasterxml.jackson.core.sym.Name _parseApostropheFieldName() {
        /*
            r12 = this;
            r10 = 39
            r9 = 4
            r1 = 0
            int r0 = r12._inputPtr
            int r2 = r12._inputEnd
            if (r0 < r2) goto L_0x0015
            boolean r0 = r12.loadMore()
            if (r0 != 0) goto L_0x0015
            java.lang.String r0 = ": was expecting closing ''' for name"
            r12._reportInvalidEOF(r0)
        L_0x0015:
            byte[] r0 = r12._inputBuffer
            int r2 = r12._inputPtr
            int r3 = r2 + 1
            r12._inputPtr = r3
            byte r0 = r0[r2]
            r5 = r0 & 255(0xff, float:3.57E-43)
            if (r5 != r10) goto L_0x0028
            com.fasterxml.jackson.core.sym.Name r0 = com.fasterxml.jackson.core.sym.BytesToNameCanonicalizer.getEmptyName()
        L_0x0027:
            return r0
        L_0x0028:
            int[] r0 = r12._quadBuffer
            int[] r7 = com.fasterxml.jackson.core.json.UTF8StreamJsonParser.sInputCodesLatin1
            r3 = r1
            r4 = r1
            r2 = r1
        L_0x002f:
            if (r5 != r10) goto L_0x0051
            if (r3 <= 0) goto L_0x010a
            int r1 = r0.length
            if (r2 < r1) goto L_0x003d
            int r1 = r0.length
            int[] r0 = growArrayBy(r0, r1)
            r12._quadBuffer = r0
        L_0x003d:
            int r1 = r2 + 1
            r0[r2] = r4
            r11 = r1
            r1 = r0
            r0 = r11
        L_0x0044:
            com.fasterxml.jackson.core.sym.BytesToNameCanonicalizer r2 = r12._symbols
            com.fasterxml.jackson.core.sym.Name r2 = r2.findName(r1, r0)
            if (r2 != 0) goto L_0x0107
            com.fasterxml.jackson.core.sym.Name r0 = r12.addName(r1, r0, r3)
            goto L_0x0027
        L_0x0051:
            r6 = 34
            if (r5 == r6) goto L_0x011a
            r6 = r7[r5]
            if (r6 == 0) goto L_0x011a
            r6 = 92
            if (r5 == r6) goto L_0x00c2
            java.lang.String r6 = "name"
            r12._throwUnquotedSpace(r5, r6)
        L_0x0062:
            r6 = 127(0x7f, float:1.78E-43)
            if (r5 <= r6) goto L_0x011a
            if (r3 < r9) goto L_0x0114
            int r3 = r0.length
            if (r2 < r3) goto L_0x0072
            int r3 = r0.length
            int[] r0 = growArrayBy(r0, r3)
            r12._quadBuffer = r0
        L_0x0072:
            int r3 = r2 + 1
            r0[r2] = r4
            r2 = r1
            r4 = r3
            r3 = r1
        L_0x0079:
            r6 = 2048(0x800, float:2.87E-42)
            if (r5 >= r6) goto L_0x00c7
            int r3 = r3 << 8
            int r6 = r5 >> 6
            r6 = r6 | 192(0xc0, float:2.69E-43)
            r3 = r3 | r6
            int r2 = r2 + 1
            r11 = r2
            r2 = r3
            r3 = r0
            r0 = r11
        L_0x008a:
            r5 = r5 & 63
            r5 = r5 | 128(0x80, float:1.794E-43)
            r6 = r2
            r2 = r0
            r0 = r3
            r3 = r5
        L_0x0092:
            if (r2 >= r9) goto L_0x00f3
            int r2 = r2 + 1
            int r5 = r6 << 8
            r3 = r3 | r5
            r11 = r2
            r2 = r3
            r3 = r4
            r4 = r0
            r0 = r11
        L_0x009e:
            int r5 = r12._inputPtr
            int r6 = r12._inputEnd
            if (r5 < r6) goto L_0x00af
            boolean r5 = r12.loadMore()
            if (r5 != 0) goto L_0x00af
            java.lang.String r5 = " in field name"
            r12._reportInvalidEOF(r5)
        L_0x00af:
            byte[] r5 = r12._inputBuffer
            int r6 = r12._inputPtr
            int r8 = r6 + 1
            r12._inputPtr = r8
            byte r5 = r5[r6]
            r5 = r5 & 255(0xff, float:3.57E-43)
            r11 = r0
            r0 = r4
            r4 = r2
            r2 = r3
            r3 = r11
            goto L_0x002f
        L_0x00c2:
            char r5 = r12._decodeEscaped()
            goto L_0x0062
        L_0x00c7:
            int r3 = r3 << 8
            int r6 = r5 >> 12
            r6 = r6 | 224(0xe0, float:3.14E-43)
            r3 = r3 | r6
            int r2 = r2 + 1
            if (r2 < r9) goto L_0x010e
            int r2 = r0.length
            if (r4 < r2) goto L_0x00dc
            int r2 = r0.length
            int[] r0 = growArrayBy(r0, r2)
            r12._quadBuffer = r0
        L_0x00dc:
            int r2 = r4 + 1
            r0[r4] = r3
            r3 = r2
            r4 = r0
            r0 = r1
            r2 = r1
        L_0x00e4:
            int r2 = r2 << 8
            int r6 = r5 >> 6
            r6 = r6 & 63
            r6 = r6 | 128(0x80, float:1.794E-43)
            r2 = r2 | r6
            int r0 = r0 + 1
            r11 = r3
            r3 = r4
            r4 = r11
            goto L_0x008a
        L_0x00f3:
            int r2 = r0.length
            if (r4 < r2) goto L_0x00fd
            int r2 = r0.length
            int[] r0 = growArrayBy(r0, r2)
            r12._quadBuffer = r0
        L_0x00fd:
            int r5 = r4 + 1
            r0[r4] = r6
            r2 = 1
            r4 = r0
            r0 = r2
            r2 = r3
            r3 = r5
            goto L_0x009e
        L_0x0107:
            r0 = r2
            goto L_0x0027
        L_0x010a:
            r1 = r0
            r0 = r2
            goto L_0x0044
        L_0x010e:
            r11 = r2
            r2 = r3
            r3 = r4
            r4 = r0
            r0 = r11
            goto L_0x00e4
        L_0x0114:
            r11 = r3
            r3 = r4
            r4 = r2
            r2 = r11
            goto L_0x0079
        L_0x011a:
            r6 = r4
            r4 = r2
            r2 = r3
            r3 = r5
            goto L_0x0092
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.core.json.UTF8StreamJsonParser._parseApostropheFieldName():com.fasterxml.jackson.core.sym.Name");
    }

    /* access modifiers changed from: protected */
    public final Name _parseFieldName(int i) {
        if (i != 34) {
            return _handleUnusualFieldName(i);
        }
        if (this._inputPtr + 9 > this._inputEnd) {
            return slowParseFieldName();
        }
        byte[] bArr = this._inputBuffer;
        int[] iArr = sInputCodesLatin1;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        byte b = bArr[i2] & ToneControl.SILENCE;
        if (iArr[b] != 0) {
            return b == 34 ? BytesToNameCanonicalizer.getEmptyName() : parseFieldName(0, b, 0);
        }
        int i3 = this._inputPtr;
        this._inputPtr = i3 + 1;
        byte b2 = bArr[i3] & ToneControl.SILENCE;
        if (iArr[b2] != 0) {
            return b2 == 34 ? findName(b, 1) : parseFieldName(b, b2, 1);
        }
        byte b3 = (b << 8) | b2;
        int i4 = this._inputPtr;
        this._inputPtr = i4 + 1;
        byte b4 = bArr[i4] & ToneControl.SILENCE;
        if (iArr[b4] != 0) {
            return b4 == 34 ? findName(b3, 2) : parseFieldName(b3, b4, 2);
        }
        byte b5 = (b3 << 8) | b4;
        int i5 = this._inputPtr;
        this._inputPtr = i5 + 1;
        byte b6 = bArr[i5] & ToneControl.SILENCE;
        if (iArr[b6] != 0) {
            return b6 == 34 ? findName(b5, 3) : parseFieldName(b5, b6, 3);
        }
        byte b7 = (b5 << 8) | b6;
        int i6 = this._inputPtr;
        this._inputPtr = i6 + 1;
        byte b8 = bArr[i6] & ToneControl.SILENCE;
        if (iArr[b8] != 0) {
            return b8 == 34 ? findName(b7, 4) : parseFieldName(b7, b8, 4);
        }
        this._quad1 = b7;
        return parseMediumFieldName(b8, iArr);
    }

    /* access modifiers changed from: protected */
    public void _releaseBuffers() {
        byte[] bArr;
        super._releaseBuffers();
        if (this._bufferRecyclable && (bArr = this._inputBuffer) != null) {
            this._inputBuffer = null;
            this._ioContext.releaseReadIOBuffer(bArr);
        }
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidChar(int i) {
        if (i < 32) {
            _throwInvalidSpace(i);
        }
        _reportInvalidInitial(i);
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidInitial(int i) {
        _reportError("Invalid UTF-8 start byte 0x" + Integer.toHexString(i));
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidOther(int i) {
        _reportError("Invalid UTF-8 middle byte 0x" + Integer.toHexString(i));
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidOther(int i, int i2) {
        this._inputPtr = i2;
        _reportInvalidOther(i);
    }

    /* access modifiers changed from: protected */
    public void _reportInvalidToken(String str, String str2) {
        StringBuilder sb = new StringBuilder(str);
        while (true) {
            if (this._inputPtr >= this._inputEnd && !loadMore()) {
                break;
            }
            byte[] bArr = this._inputBuffer;
            int i = this._inputPtr;
            this._inputPtr = i + 1;
            char _decodeCharForError = (char) _decodeCharForError(bArr[i]);
            if (!Character.isJavaIdentifierPart(_decodeCharForError)) {
                break;
            }
            sb.append(_decodeCharForError);
        }
        _reportError("Unrecognized token '" + sb.toString() + "': was expecting " + str2);
    }

    /* access modifiers changed from: protected */
    public final void _skipCR() {
        if ((this._inputPtr < this._inputEnd || loadMore()) && this._inputBuffer[this._inputPtr] == 10) {
            this._inputPtr++;
        }
        this._currInputRow++;
        this._currInputRowStart = this._inputPtr;
    }

    /* access modifiers changed from: protected */
    public final void _skipLF() {
        this._currInputRow++;
        this._currInputRowStart = this._inputPtr;
    }

    /* access modifiers changed from: protected */
    public void _skipString() {
        this._tokenIncomplete = false;
        int[] iArr = sInputCodesUtf8;
        byte[] bArr = this._inputBuffer;
        while (true) {
            int i = this._inputPtr;
            int i2 = this._inputEnd;
            if (i >= i2) {
                loadMoreGuaranteed();
                i = this._inputPtr;
                i2 = this._inputEnd;
            }
            while (true) {
                if (i < i2) {
                    int i3 = i + 1;
                    byte b = bArr[i] & ToneControl.SILENCE;
                    if (iArr[b] != 0) {
                        this._inputPtr = i3;
                        if (b != 34) {
                            switch (iArr[b]) {
                                case 1:
                                    _decodeEscaped();
                                    continue;
                                case 2:
                                    _skipUtf8_2(b);
                                    continue;
                                case 3:
                                    _skipUtf8_3(b);
                                    continue;
                                case 4:
                                    _skipUtf8_4(b);
                                    continue;
                                default:
                                    if (b >= 32) {
                                        _reportInvalidChar(b);
                                        break;
                                    } else {
                                        _throwUnquotedSpace(b, "string value");
                                        continue;
                                    }
                            }
                        } else {
                            return;
                        }
                    } else {
                        i = i3;
                    }
                } else {
                    this._inputPtr = i;
                }
            }
        }
    }

    public void close() {
        super.close();
        this._symbols.release();
    }

    public byte[] getBinaryValue(Base64Variant base64Variant) {
        if (this._currToken != JsonToken.VALUE_STRING && (this._currToken != JsonToken.VALUE_EMBEDDED_OBJECT || this._binaryValue == null)) {
            _reportError("Current token (" + this._currToken + ") not VALUE_STRING or VALUE_EMBEDDED_OBJECT, can not access as binary");
        }
        if (this._tokenIncomplete) {
            try {
                this._binaryValue = _decodeBase64(base64Variant);
                this._tokenIncomplete = false;
            } catch (IllegalArgumentException e) {
                throw _constructError("Failed to decode VALUE_STRING as base64 (" + base64Variant + "): " + e.getMessage());
            }
        } else if (this._binaryValue == null) {
            ByteArrayBuilder _getByteArrayBuilder = _getByteArrayBuilder();
            _decodeBase64(getText(), _getByteArrayBuilder, base64Variant);
            this._binaryValue = _getByteArrayBuilder.toByteArray();
        }
        return this._binaryValue;
    }

    public ObjectCodec getCodec() {
        return this._objectCodec;
    }

    public Object getEmbeddedObject() {
        return null;
    }

    public Object getInputSource() {
        return this._inputStream;
    }

    public String getText() {
        JsonToken jsonToken = this._currToken;
        if (jsonToken != JsonToken.VALUE_STRING) {
            return _getText2(jsonToken);
        }
        if (this._tokenIncomplete) {
            this._tokenIncomplete = false;
            _finishString();
        }
        return this._textBuffer.contentsAsString();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public char[] getTextCharacters() {
        if (this._currToken == null) {
            return null;
        }
        switch (this._currToken) {
            case FIELD_NAME:
                if (!this._nameCopied) {
                    String currentName = this._parsingContext.getCurrentName();
                    int length = currentName.length();
                    if (this._nameCopyBuffer == null) {
                        this._nameCopyBuffer = this._ioContext.allocNameCopyBuffer(length);
                    } else if (this._nameCopyBuffer.length < length) {
                        this._nameCopyBuffer = new char[length];
                    }
                    currentName.getChars(0, length, this._nameCopyBuffer, 0);
                    this._nameCopied = true;
                }
                return this._nameCopyBuffer;
            case VALUE_STRING:
                if (this._tokenIncomplete) {
                    this._tokenIncomplete = false;
                    _finishString();
                    break;
                }
                break;
            case VALUE_NUMBER_INT:
            case VALUE_NUMBER_FLOAT:
                break;
            default:
                return this._currToken.asCharArray();
        }
        return this._textBuffer.getTextBuffer();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public int getTextLength() {
        if (this._currToken == null) {
            return 0;
        }
        switch (this._currToken) {
            case FIELD_NAME:
                return this._parsingContext.getCurrentName().length();
            case VALUE_STRING:
                if (this._tokenIncomplete) {
                    this._tokenIncomplete = false;
                    _finishString();
                    break;
                }
                break;
            case VALUE_NUMBER_INT:
            case VALUE_NUMBER_FLOAT:
                break;
            default:
                return this._currToken.asCharArray().length;
        }
        return this._textBuffer.size();
    }

    public int getTextOffset() {
        if (this._currToken == null) {
            return 0;
        }
        switch (this._currToken) {
            case FIELD_NAME:
            default:
                return 0;
            case VALUE_STRING:
                if (this._tokenIncomplete) {
                    this._tokenIncomplete = false;
                    _finishString();
                    break;
                }
                break;
            case VALUE_NUMBER_INT:
            case VALUE_NUMBER_FLOAT:
                break;
        }
        return this._textBuffer.getTextOffset();
    }

    /* access modifiers changed from: protected */
    public final boolean loadMore() {
        this._currInputProcessed += (long) this._inputEnd;
        this._currInputRowStart -= this._inputEnd;
        if (this._inputStream == null) {
            return false;
        }
        int read = this._inputStream.read(this._inputBuffer, 0, this._inputBuffer.length);
        if (read > 0) {
            this._inputPtr = 0;
            this._inputEnd = read;
            return true;
        }
        _closeInput();
        if (read != 0) {
            return false;
        }
        throw new IOException("InputStream.read() returned 0 characters when trying to read " + this._inputBuffer.length + " bytes");
    }

    public Boolean nextBooleanValue() {
        if (this._currToken == JsonToken.FIELD_NAME) {
            this._nameCopied = false;
            JsonToken jsonToken = this._nextToken;
            this._nextToken = null;
            this._currToken = jsonToken;
            if (jsonToken == JsonToken.VALUE_TRUE) {
                return Boolean.TRUE;
            }
            if (jsonToken == JsonToken.VALUE_FALSE) {
                return Boolean.FALSE;
            }
            if (jsonToken == JsonToken.START_ARRAY) {
                this._parsingContext = this._parsingContext.createChildArrayContext(this._tokenInputRow, this._tokenInputCol);
                return null;
            } else if (jsonToken != JsonToken.START_OBJECT) {
                return null;
            } else {
                this._parsingContext = this._parsingContext.createChildObjectContext(this._tokenInputRow, this._tokenInputCol);
                return null;
            }
        } else {
            switch (nextToken()) {
                case VALUE_TRUE:
                    return Boolean.TRUE;
                case VALUE_FALSE:
                    return Boolean.FALSE;
                default:
                    return null;
            }
        }
    }

    public boolean nextFieldName(SerializableString serializableString) {
        this._numTypesValid = 0;
        if (this._currToken == JsonToken.FIELD_NAME) {
            _nextAfterName();
            return false;
        }
        if (this._tokenIncomplete) {
            _skipString();
        }
        int _skipWSOrEnd = _skipWSOrEnd();
        if (_skipWSOrEnd < 0) {
            close();
            this._currToken = null;
            return false;
        }
        this._tokenInputTotal = (this._currInputProcessed + ((long) this._inputPtr)) - 1;
        this._tokenInputRow = this._currInputRow;
        this._tokenInputCol = (this._inputPtr - this._currInputRowStart) - 1;
        this._binaryValue = null;
        if (_skipWSOrEnd == 93) {
            if (!this._parsingContext.inArray()) {
                _reportMismatchedEndMarker(_skipWSOrEnd, '}');
            }
            this._parsingContext = this._parsingContext.getParent();
            this._currToken = JsonToken.END_ARRAY;
            return false;
        } else if (_skipWSOrEnd == 125) {
            if (!this._parsingContext.inObject()) {
                _reportMismatchedEndMarker(_skipWSOrEnd, ']');
            }
            this._parsingContext = this._parsingContext.getParent();
            this._currToken = JsonToken.END_OBJECT;
            return false;
        } else {
            if (this._parsingContext.expectComma()) {
                if (_skipWSOrEnd != 44) {
                    _reportUnexpectedChar(_skipWSOrEnd, "was expecting comma to separate " + this._parsingContext.getTypeDesc() + " entries");
                }
                _skipWSOrEnd = _skipWS();
            }
            if (!this._parsingContext.inObject()) {
                _nextTokenNotInObject(_skipWSOrEnd);
                return false;
            }
            if (_skipWSOrEnd == 34) {
                byte[] asQuotedUTF8 = serializableString.asQuotedUTF8();
                int length = asQuotedUTF8.length;
                if (this._inputPtr + length < this._inputEnd) {
                    int i = this._inputPtr + length;
                    if (this._inputBuffer[i] == 34) {
                        int i2 = this._inputPtr;
                        int i3 = 0;
                        while (i3 != length) {
                            if (asQuotedUTF8[i3] == this._inputBuffer[i2 + i3]) {
                                i3++;
                            }
                        }
                        this._inputPtr = i + 1;
                        this._parsingContext.setCurrentName(serializableString.getValue());
                        this._currToken = JsonToken.FIELD_NAME;
                        _isNextTokenNameYes();
                        return true;
                    }
                }
            }
            _isNextTokenNameNo(_skipWSOrEnd);
            return false;
        }
    }

    public int nextIntValue(int i) {
        if (this._currToken != JsonToken.FIELD_NAME) {
            return nextToken() == JsonToken.VALUE_NUMBER_INT ? getIntValue() : i;
        }
        this._nameCopied = false;
        JsonToken jsonToken = this._nextToken;
        this._nextToken = null;
        this._currToken = jsonToken;
        if (jsonToken == JsonToken.VALUE_NUMBER_INT) {
            return getIntValue();
        }
        if (jsonToken == JsonToken.START_ARRAY) {
            this._parsingContext = this._parsingContext.createChildArrayContext(this._tokenInputRow, this._tokenInputCol);
            return i;
        } else if (jsonToken != JsonToken.START_OBJECT) {
            return i;
        } else {
            this._parsingContext = this._parsingContext.createChildObjectContext(this._tokenInputRow, this._tokenInputCol);
            return i;
        }
    }

    public long nextLongValue(long j) {
        if (this._currToken != JsonToken.FIELD_NAME) {
            return nextToken() == JsonToken.VALUE_NUMBER_INT ? getLongValue() : j;
        }
        this._nameCopied = false;
        JsonToken jsonToken = this._nextToken;
        this._nextToken = null;
        this._currToken = jsonToken;
        if (jsonToken == JsonToken.VALUE_NUMBER_INT) {
            return getLongValue();
        }
        if (jsonToken == JsonToken.START_ARRAY) {
            this._parsingContext = this._parsingContext.createChildArrayContext(this._tokenInputRow, this._tokenInputCol);
            return j;
        } else if (jsonToken != JsonToken.START_OBJECT) {
            return j;
        } else {
            this._parsingContext = this._parsingContext.createChildObjectContext(this._tokenInputRow, this._tokenInputCol);
            return j;
        }
    }

    public String nextTextValue() {
        if (this._currToken == JsonToken.FIELD_NAME) {
            this._nameCopied = false;
            JsonToken jsonToken = this._nextToken;
            this._nextToken = null;
            this._currToken = jsonToken;
            if (jsonToken == JsonToken.VALUE_STRING) {
                if (this._tokenIncomplete) {
                    this._tokenIncomplete = false;
                    _finishString();
                }
                return this._textBuffer.contentsAsString();
            } else if (jsonToken == JsonToken.START_ARRAY) {
                this._parsingContext = this._parsingContext.createChildArrayContext(this._tokenInputRow, this._tokenInputCol);
                return null;
            } else if (jsonToken != JsonToken.START_OBJECT) {
                return null;
            } else {
                this._parsingContext = this._parsingContext.createChildObjectContext(this._tokenInputRow, this._tokenInputCol);
                return null;
            }
        } else if (nextToken() == JsonToken.VALUE_STRING) {
            return getText();
        } else {
            return null;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public JsonToken nextToken() {
        JsonToken parseNumberText;
        this._numTypesValid = 0;
        if (this._currToken == JsonToken.FIELD_NAME) {
            return _nextAfterName();
        }
        if (this._tokenIncomplete) {
            _skipString();
        }
        int _skipWSOrEnd = _skipWSOrEnd();
        if (_skipWSOrEnd < 0) {
            close();
            this._currToken = null;
            return null;
        }
        this._tokenInputTotal = (this._currInputProcessed + ((long) this._inputPtr)) - 1;
        this._tokenInputRow = this._currInputRow;
        this._tokenInputCol = (this._inputPtr - this._currInputRowStart) - 1;
        this._binaryValue = null;
        if (_skipWSOrEnd == 93) {
            if (!this._parsingContext.inArray()) {
                _reportMismatchedEndMarker(_skipWSOrEnd, '}');
            }
            this._parsingContext = this._parsingContext.getParent();
            JsonToken jsonToken = JsonToken.END_ARRAY;
            this._currToken = jsonToken;
            return jsonToken;
        } else if (_skipWSOrEnd == 125) {
            if (!this._parsingContext.inObject()) {
                _reportMismatchedEndMarker(_skipWSOrEnd, ']');
            }
            this._parsingContext = this._parsingContext.getParent();
            JsonToken jsonToken2 = JsonToken.END_OBJECT;
            this._currToken = jsonToken2;
            return jsonToken2;
        } else {
            if (this._parsingContext.expectComma()) {
                if (_skipWSOrEnd != 44) {
                    _reportUnexpectedChar(_skipWSOrEnd, "was expecting comma to separate " + this._parsingContext.getTypeDesc() + " entries");
                }
                _skipWSOrEnd = _skipWS();
            }
            if (!this._parsingContext.inObject()) {
                return _nextTokenNotInObject(_skipWSOrEnd);
            }
            this._parsingContext.setCurrentName(_parseFieldName(_skipWSOrEnd).getName());
            this._currToken = JsonToken.FIELD_NAME;
            int _skipWS = _skipWS();
            if (_skipWS != 58) {
                _reportUnexpectedChar(_skipWS, "was expecting a colon to separate field name and value");
            }
            int _skipWS2 = _skipWS();
            if (_skipWS2 == 34) {
                this._tokenIncomplete = true;
                this._nextToken = JsonToken.VALUE_STRING;
                return this._currToken;
            }
            switch (_skipWS2) {
                case 45:
                case 48:
                case c.KEY_NUM1:
                case 50:
                case c.KEY_NUM3:
                case c.KEY_NUM4:
                case c.KEY_NUM5:
                case c.KEY_NUM6:
                case c.KEY_NUM7:
                case 56:
                case c.KEY_NUM9:
                    parseNumberText = parseNumberText(_skipWS2);
                    break;
                case 91:
                    parseNumberText = JsonToken.START_ARRAY;
                    break;
                case 93:
                case 125:
                    _reportUnexpectedChar(_skipWS2, "expected a value");
                    _matchToken("true", 1);
                    parseNumberText = JsonToken.VALUE_TRUE;
                    break;
                case 102:
                    _matchToken("false", 1);
                    parseNumberText = JsonToken.VALUE_FALSE;
                    break;
                case a.PHOTO:
                    _matchToken("null", 1);
                    parseNumberText = JsonToken.VALUE_NULL;
                    break;
                case a.TITLE:
                    _matchToken("true", 1);
                    parseNumberText = JsonToken.VALUE_TRUE;
                    break;
                case 123:
                    parseNumberText = JsonToken.START_OBJECT;
                    break;
                default:
                    parseNumberText = _handleUnexpectedValue(_skipWS2);
                    break;
            }
            this._nextToken = parseNumberText;
            return this._currToken;
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00bb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.fasterxml.jackson.core.sym.Name parseEscapedFieldName(int[] r10, int r11, int r12, int r13, int r14) {
        /*
            r9 = this;
            r7 = 4
            r1 = 0
            int[] r5 = com.fasterxml.jackson.core.json.UTF8StreamJsonParser.sInputCodesLatin1
        L_0x0004:
            r0 = r5[r13]
            if (r0 == 0) goto L_0x00d7
            r0 = 34
            if (r13 != r0) goto L_0x002a
            if (r14 <= 0) goto L_0x001d
            int r0 = r10.length
            if (r11 < r0) goto L_0x0018
            int r0 = r10.length
            int[] r10 = growArrayBy(r10, r0)
            r9._quadBuffer = r10
        L_0x0018:
            int r0 = r11 + 1
            r10[r11] = r12
            r11 = r0
        L_0x001d:
            com.fasterxml.jackson.core.sym.BytesToNameCanonicalizer r0 = r9._symbols
            com.fasterxml.jackson.core.sym.Name r0 = r0.findName(r10, r11)
            if (r0 != 0) goto L_0x0029
            com.fasterxml.jackson.core.sym.Name r0 = r9.addName(r10, r11, r14)
        L_0x0029:
            return r0
        L_0x002a:
            r0 = 92
            if (r13 == r0) goto L_0x008a
            java.lang.String r0 = "name"
            r9._throwUnquotedSpace(r13, r0)
        L_0x0033:
            r0 = 127(0x7f, float:1.78E-43)
            if (r13 <= r0) goto L_0x00d7
            if (r14 < r7) goto L_0x00d3
            int r0 = r10.length
            if (r11 < r0) goto L_0x0043
            int r0 = r10.length
            int[] r10 = growArrayBy(r10, r0)
            r9._quadBuffer = r10
        L_0x0043:
            int r4 = r11 + 1
            r10[r11] = r12
            r14 = r1
            r12 = r1
            r0 = r10
        L_0x004a:
            r2 = 2048(0x800, float:2.87E-42)
            if (r13 >= r2) goto L_0x008f
            int r2 = r12 << 8
            int r3 = r13 >> 6
            r3 = r3 | 192(0xc0, float:2.69E-43)
            r3 = r3 | r2
            int r2 = r14 + 1
            r8 = r2
            r2 = r3
            r3 = r0
            r0 = r8
        L_0x005b:
            r6 = r13 & 63
            r12 = r6 | 128(0x80, float:1.794E-43)
            r14 = r0
            r11 = r4
            r0 = r3
            r3 = r2
        L_0x0063:
            if (r14 >= r7) goto L_0x00bb
            int r14 = r14 + 1
            int r2 = r3 << 8
            r12 = r12 | r2
            r10 = r0
        L_0x006b:
            int r0 = r9._inputPtr
            int r2 = r9._inputEnd
            if (r0 < r2) goto L_0x007c
            boolean r0 = r9.loadMore()
            if (r0 != 0) goto L_0x007c
            java.lang.String r0 = " in field name"
            r9._reportInvalidEOF(r0)
        L_0x007c:
            byte[] r0 = r9._inputBuffer
            int r2 = r9._inputPtr
            int r3 = r2 + 1
            r9._inputPtr = r3
            byte r0 = r0[r2]
            r13 = r0 & 255(0xff, float:3.57E-43)
            goto L_0x0004
        L_0x008a:
            char r13 = r9._decodeEscaped()
            goto L_0x0033
        L_0x008f:
            int r2 = r12 << 8
            int r3 = r13 >> 12
            r3 = r3 | 224(0xe0, float:3.14E-43)
            r3 = r3 | r2
            int r2 = r14 + 1
            if (r2 < r7) goto L_0x00cd
            int r2 = r0.length
            if (r4 < r2) goto L_0x00a4
            int r2 = r0.length
            int[] r0 = growArrayBy(r0, r2)
            r9._quadBuffer = r0
        L_0x00a4:
            int r2 = r4 + 1
            r0[r4] = r3
            r3 = r2
            r4 = r0
            r0 = r1
            r2 = r1
        L_0x00ac:
            int r2 = r2 << 8
            int r6 = r13 >> 6
            r6 = r6 & 63
            r6 = r6 | 128(0x80, float:1.794E-43)
            r2 = r2 | r6
            int r0 = r0 + 1
            r8 = r3
            r3 = r4
            r4 = r8
            goto L_0x005b
        L_0x00bb:
            int r2 = r0.length
            if (r11 < r2) goto L_0x00c5
            int r2 = r0.length
            int[] r0 = growArrayBy(r0, r2)
            r9._quadBuffer = r0
        L_0x00c5:
            int r2 = r11 + 1
            r0[r11] = r3
            r14 = 1
            r11 = r2
            r10 = r0
            goto L_0x006b
        L_0x00cd:
            r8 = r2
            r2 = r3
            r3 = r4
            r4 = r0
            r0 = r8
            goto L_0x00ac
        L_0x00d3:
            r4 = r11
            r0 = r10
            goto L_0x004a
        L_0x00d7:
            r3 = r12
            r0 = r10
            r12 = r13
            goto L_0x0063
        */
        throw new UnsupportedOperationException("Method not decompiled: com.fasterxml.jackson.core.json.UTF8StreamJsonParser.parseEscapedFieldName(int[], int, int, int, int):com.fasterxml.jackson.core.sym.Name");
    }

    /* access modifiers changed from: protected */
    public Name parseLongFieldName(int i) {
        int[] iArr = sInputCodesLatin1;
        int i2 = 2;
        byte b = i;
        while (this._inputEnd - this._inputPtr >= 4) {
            byte[] bArr = this._inputBuffer;
            int i3 = this._inputPtr;
            this._inputPtr = i3 + 1;
            byte b2 = bArr[i3] & ToneControl.SILENCE;
            if (iArr[b2] != 0) {
                return b2 == 34 ? findName(this._quadBuffer, i2, b, 1) : parseEscapedFieldName(this._quadBuffer, i2, b, b2, 1);
            }
            byte b3 = (b << 8) | b2;
            byte[] bArr2 = this._inputBuffer;
            int i4 = this._inputPtr;
            this._inputPtr = i4 + 1;
            byte b4 = bArr2[i4] & ToneControl.SILENCE;
            if (iArr[b4] != 0) {
                return b4 == 34 ? findName(this._quadBuffer, i2, b3, 2) : parseEscapedFieldName(this._quadBuffer, i2, b3, b4, 2);
            }
            byte b5 = (b3 << 8) | b4;
            byte[] bArr3 = this._inputBuffer;
            int i5 = this._inputPtr;
            this._inputPtr = i5 + 1;
            byte b6 = bArr3[i5] & ToneControl.SILENCE;
            if (iArr[b6] != 0) {
                return b6 == 34 ? findName(this._quadBuffer, i2, b5, 3) : parseEscapedFieldName(this._quadBuffer, i2, b5, b6, 3);
            }
            int i6 = (b5 << 8) | b6;
            byte[] bArr4 = this._inputBuffer;
            int i7 = this._inputPtr;
            this._inputPtr = i7 + 1;
            b = bArr4[i7] & ToneControl.SILENCE;
            if (iArr[b] != 0) {
                return b == 34 ? findName(this._quadBuffer, i2, i6, 4) : parseEscapedFieldName(this._quadBuffer, i2, i6, b, 4);
            }
            if (i2 >= this._quadBuffer.length) {
                this._quadBuffer = growArrayBy(this._quadBuffer, i2);
            }
            this._quadBuffer[i2] = i6;
            i2++;
        }
        return parseEscapedFieldName(this._quadBuffer, i2, 0, b, 0);
    }

    /* access modifiers changed from: protected */
    public final Name parseMediumFieldName(int i, int[] iArr) {
        byte[] bArr = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        byte b = bArr[i2] & ToneControl.SILENCE;
        if (iArr[b] != 0) {
            return b == 34 ? findName(this._quad1, i, 1) : parseFieldName(this._quad1, i, b, 1);
        }
        byte b2 = b | (i << 8);
        byte[] bArr2 = this._inputBuffer;
        int i3 = this._inputPtr;
        this._inputPtr = i3 + 1;
        byte b3 = bArr2[i3] & ToneControl.SILENCE;
        if (iArr[b3] != 0) {
            return b3 == 34 ? findName(this._quad1, b2, 2) : parseFieldName(this._quad1, b2, b3, 2);
        }
        byte b4 = (b2 << 8) | b3;
        byte[] bArr3 = this._inputBuffer;
        int i4 = this._inputPtr;
        this._inputPtr = i4 + 1;
        byte b5 = bArr3[i4] & ToneControl.SILENCE;
        if (iArr[b5] != 0) {
            return b5 == 34 ? findName(this._quad1, b4, 3) : parseFieldName(this._quad1, b4, b5, 3);
        }
        int i5 = (b4 << 8) | b5;
        byte[] bArr4 = this._inputBuffer;
        int i6 = this._inputPtr;
        this._inputPtr = i6 + 1;
        byte b6 = bArr4[i6] & ToneControl.SILENCE;
        if (iArr[b6] != 0) {
            return b6 == 34 ? findName(this._quad1, i5, 4) : parseFieldName(this._quad1, i5, b6, 4);
        }
        this._quadBuffer[0] = this._quad1;
        this._quadBuffer[1] = i5;
        return parseLongFieldName(b6);
    }

    /* access modifiers changed from: protected */
    public final JsonToken parseNumberText(int i) {
        int i2;
        int i3;
        int i4 = 1;
        char[] emptyAndGetCurrentSegment = this._textBuffer.emptyAndGetCurrentSegment();
        boolean z = i == 45;
        if (z) {
            emptyAndGetCurrentSegment[0] = '-';
            if (this._inputPtr >= this._inputEnd) {
                loadMoreGuaranteed();
            }
            byte[] bArr = this._inputBuffer;
            int i5 = this._inputPtr;
            this._inputPtr = i5 + 1;
            i3 = bArr[i5] & ToneControl.SILENCE;
            if (i3 < 48 || i3 > 57) {
                return _handleInvalidNumberStart(i3, true);
            }
            i2 = 1;
        } else {
            i2 = 0;
            i3 = i;
        }
        if (i3 == 48) {
            i3 = _verifyNoLeadingZeroes();
        }
        int i6 = i2 + 1;
        emptyAndGetCurrentSegment[i2] = (char) i3;
        int length = this._inputPtr + emptyAndGetCurrentSegment.length;
        if (length > this._inputEnd) {
            length = this._inputEnd;
        }
        while (this._inputPtr < length) {
            byte[] bArr2 = this._inputBuffer;
            int i7 = this._inputPtr;
            this._inputPtr = i7 + 1;
            byte b = bArr2[i7] & ToneControl.SILENCE;
            if (b >= 48 && b <= 57) {
                i4++;
                emptyAndGetCurrentSegment[i6] = (char) b;
                i6++;
            } else if (b == 46 || b == 101 || b == 69) {
                return _parseFloatText(emptyAndGetCurrentSegment, i6, b, z, i4);
            } else {
                this._inputPtr--;
                this._textBuffer.setCurrentLength(i6);
                return resetInt(z, i4);
            }
        }
        return _parserNumber2(emptyAndGetCurrentSegment, i6, z, i4);
    }

    public int releaseBuffered(OutputStream outputStream) {
        int i = this._inputEnd - this._inputPtr;
        if (i < 1) {
            return 0;
        }
        outputStream.write(this._inputBuffer, this._inputPtr, i);
        return i;
    }

    public void setCodec(ObjectCodec objectCodec) {
        this._objectCodec = objectCodec;
    }

    /* access modifiers changed from: protected */
    public Name slowParseFieldName() {
        if (this._inputPtr >= this._inputEnd && !loadMore()) {
            _reportInvalidEOF(": was expecting closing '\"' for name");
        }
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        byte b = bArr[i] & ToneControl.SILENCE;
        return b == 34 ? BytesToNameCanonicalizer.getEmptyName() : parseEscapedFieldName(this._quadBuffer, 0, 0, b, 0);
    }

    public Version version() {
        return CoreVersion.instance.version();
    }
}
