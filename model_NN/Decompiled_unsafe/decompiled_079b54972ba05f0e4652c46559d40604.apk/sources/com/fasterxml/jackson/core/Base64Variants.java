package com.fasterxml.jackson.core;

import com.a.a.e.r;

public final class Base64Variants {
    public static final Base64Variant MIME = new Base64Variant("MIME", STD_BASE64_ALPHABET, true, '=', 76);
    public static final Base64Variant MIME_NO_LINEFEEDS = new Base64Variant(MIME, "MIME-NO-LINEFEEDS", r.OUTOFITEM);
    public static final Base64Variant MODIFIED_FOR_URL;
    public static final Base64Variant PEM = new Base64Variant(MIME, "PEM", true, '=', 64);
    static final String STD_BASE64_ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.core.Base64Variant.<init>(java.lang.String, java.lang.String, boolean, char, int):void
     arg types: [java.lang.String, java.lang.String, int, int, int]
     candidates:
      com.fasterxml.jackson.core.Base64Variant.<init>(com.fasterxml.jackson.core.Base64Variant, java.lang.String, boolean, char, int):void
      com.fasterxml.jackson.core.Base64Variant.<init>(java.lang.String, java.lang.String, boolean, char, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.core.Base64Variant.<init>(com.fasterxml.jackson.core.Base64Variant, java.lang.String, boolean, char, int):void
     arg types: [com.fasterxml.jackson.core.Base64Variant, java.lang.String, int, int, int]
     candidates:
      com.fasterxml.jackson.core.Base64Variant.<init>(java.lang.String, java.lang.String, boolean, char, int):void
      com.fasterxml.jackson.core.Base64Variant.<init>(com.fasterxml.jackson.core.Base64Variant, java.lang.String, boolean, char, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fasterxml.jackson.core.Base64Variant.<init>(java.lang.String, java.lang.String, boolean, char, int):void
     arg types: [java.lang.String, java.lang.String, int, int, ?]
     candidates:
      com.fasterxml.jackson.core.Base64Variant.<init>(com.fasterxml.jackson.core.Base64Variant, java.lang.String, boolean, char, int):void
      com.fasterxml.jackson.core.Base64Variant.<init>(java.lang.String, java.lang.String, boolean, char, int):void */
    static {
        StringBuffer stringBuffer = new StringBuffer(STD_BASE64_ALPHABET);
        stringBuffer.setCharAt(stringBuffer.indexOf("+"), '-');
        stringBuffer.setCharAt(stringBuffer.indexOf("/"), '_');
        MODIFIED_FOR_URL = new Base64Variant("MODIFIED-FOR-URL", stringBuffer.toString(), false, 0, (int) r.OUTOFITEM);
    }

    public static Base64Variant getDefaultVariant() {
        return MIME_NO_LINEFEEDS;
    }
}
