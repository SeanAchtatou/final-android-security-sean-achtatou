package com.fasterxml.jackson.core;

import com.umeng.common.b.e;

public enum JsonEncoding {
    UTF8(e.f, false),
    UTF16_BE(e.d, true),
    UTF16_LE(e.e, false),
    UTF32_BE("UTF-32BE", true),
    UTF32_LE("UTF-32LE", false);
    
    protected final boolean _bigEndian;
    protected final String _javaName;

    private JsonEncoding(String str, boolean z) {
        this._javaName = str;
        this._bigEndian = z;
    }

    public String getJavaName() {
        return this._javaName;
    }

    public boolean isBigEndian() {
        return this._bigEndian;
    }
}
