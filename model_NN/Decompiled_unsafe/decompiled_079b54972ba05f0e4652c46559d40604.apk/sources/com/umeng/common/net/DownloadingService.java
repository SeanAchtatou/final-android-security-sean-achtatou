package com.umeng.common.net;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Debug;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.widget.RemoteViews;
import android.widget.Toast;
import com.a.a.i.k;
import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import com.umeng.common.Log;
import com.umeng.common.b.g;
import com.umeng.common.net.a;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class DownloadingService extends Service {
    static final int a = 3;
    static final int b = 4;
    static final int c = 5;
    static final int d = 0;
    static final int e = 1;
    static final int f = 100;
    static final String g = "filename";
    public static boolean h = false;
    /* access modifiers changed from: private */
    public static final String j = DownloadingService.class.getName();
    private static final int l = 3;
    private static final long p = 8000;
    /* access modifiers changed from: private */
    public static Map<a.C0003a, Messenger> q = new HashMap();
    /* access modifiers changed from: private */
    public static Map<Integer, d> r = new HashMap();
    /* access modifiers changed from: private */
    public static Boolean t = false;
    final Messenger i = new Messenger(new c());
    /* access modifiers changed from: private */
    public NotificationManager k;
    /* access modifiers changed from: private */
    public Context m;
    /* access modifiers changed from: private */
    public Handler n;
    private a o;
    private BroadcastReceiver s;

    private interface a {
        void a(int i);

        void a(int i, int i2);

        void a(int i, Exception exc);

        void a(int i, String str);
    }

    class b extends Thread {
        /* access modifiers changed from: private */
        public Context b;
        private String c;
        private int d = 0;
        private long e = -1;
        private long f = -1;
        private int g = -1;
        private int h;
        private a i;
        private a.C0003a j;

        public b(Context context, a.C0003a aVar, int i2, int i3, a aVar2) {
            long[] jArr;
            try {
                this.b = context;
                this.j = aVar;
                this.d = i3;
                if (DownloadingService.r.containsKey(Integer.valueOf(i2)) && (jArr = ((d) DownloadingService.r.get(Integer.valueOf(i2))).f) != null && jArr.length > 1) {
                    this.e = jArr[0];
                    this.f = jArr[1];
                }
                this.i = aVar2;
                this.h = i2;
                if (com.umeng.common.b.b()) {
                    this.c = Environment.getExternalStorageDirectory().getCanonicalPath();
                    new File(this.c).mkdirs();
                } else {
                    this.c = this.b.getFilesDir().getAbsolutePath();
                }
                this.c = String.valueOf(this.c) + "/download/.um/apk";
                new File(this.c).mkdirs();
            } catch (Exception e2) {
                Log.c(DownloadingService.j, e2.getMessage(), e2);
                this.i.a(this.h, e2);
            }
        }

        private void a(Exception exc) {
            Log.b(DownloadingService.j, "can not install. " + exc.getMessage());
            if (this.i != null) {
                this.i.a(this.h, exc);
            }
            DownloadingService.this.a(this.j, this.e, this.f, (long) this.d);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
         arg types: [java.io.File, int]
         candidates:
          ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
          ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.umeng.common.net.DownloadingService.a(java.util.Map, boolean, java.lang.String[]):void
         arg types: [java.util.HashMap, int, java.lang.String[]]
         candidates:
          com.umeng.common.net.DownloadingService.a(com.umeng.common.net.a$a, int, int):android.app.Notification
          com.umeng.common.net.DownloadingService.a(com.umeng.common.net.DownloadingService, android.content.Context, android.content.Intent):boolean
          com.umeng.common.net.DownloadingService.a(java.util.Map, boolean, java.lang.String[]):void */
        /* JADX WARNING: Removed duplicated region for block: B:173:0x03ee A[SYNTHETIC, Splitter:B:173:0x03ee] */
        /* JADX WARNING: Removed duplicated region for block: B:176:0x03f3 A[SYNTHETIC, Splitter:B:176:0x03f3] */
        /* JADX WARNING: Removed duplicated region for block: B:187:0x042f A[Catch:{ InterruptedException -> 0x0442 }] */
        /* JADX WARNING: Removed duplicated region for block: B:196:0x0455 A[Catch:{ InterruptedException -> 0x0442 }] */
        /* JADX WARNING: Removed duplicated region for block: B:217:0x04e6 A[SYNTHETIC, Splitter:B:217:0x04e6] */
        /* JADX WARNING: Removed duplicated region for block: B:220:0x04eb A[SYNTHETIC, Splitter:B:220:0x04eb] */
        /* JADX WARNING: Removed duplicated region for block: B:297:? A[RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:314:? A[RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:64:0x0224 A[Catch:{ RemoteException -> 0x03cb, all -> 0x03f7, all -> 0x03e9 }] */
        /* JADX WARNING: Removed duplicated region for block: B:71:0x0264 A[SYNTHETIC, Splitter:B:71:0x0264] */
        /* JADX WARNING: Removed duplicated region for block: B:74:0x0269 A[SYNTHETIC, Splitter:B:74:0x0269] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void a(boolean r14) {
            /*
                r13 = this;
                r2 = 0
                r1 = 0
                java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0570, RemoteException -> 0x04d7, all -> 0x0553 }
                com.umeng.common.net.a$a r3 = r13.j     // Catch:{ IOException -> 0x0570, RemoteException -> 0x04d7, all -> 0x0553 }
                java.lang.String r3 = r3.c     // Catch:{ IOException -> 0x0570, RemoteException -> 0x04d7, all -> 0x0553 }
                java.lang.String r3 = com.umeng.common.b.g.a(r3)     // Catch:{ IOException -> 0x0570, RemoteException -> 0x04d7, all -> 0x0553 }
                java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ IOException -> 0x0570, RemoteException -> 0x04d7, all -> 0x0553 }
                r0.<init>(r3)     // Catch:{ IOException -> 0x0570, RemoteException -> 0x04d7, all -> 0x0553 }
                java.lang.String r3 = ".apk.tmp"
                java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ IOException -> 0x0570, RemoteException -> 0x04d7, all -> 0x0553 }
                java.lang.String r3 = r0.toString()     // Catch:{ IOException -> 0x0570, RemoteException -> 0x04d7, all -> 0x0553 }
                boolean r0 = com.umeng.common.b.b()     // Catch:{ IOException -> 0x0570, RemoteException -> 0x04d7, all -> 0x0553 }
                if (r0 == 0) goto L_0x015e
                java.io.File r0 = new java.io.File     // Catch:{ IOException -> 0x0570, RemoteException -> 0x04d7, all -> 0x0553 }
                java.lang.String r4 = r13.c     // Catch:{ IOException -> 0x0570, RemoteException -> 0x04d7, all -> 0x0553 }
                r0.<init>(r4, r3)     // Catch:{ IOException -> 0x0570, RemoteException -> 0x04d7, all -> 0x0553 }
                java.io.FileOutputStream r8 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0570, RemoteException -> 0x04d7, all -> 0x0553 }
                r3 = 1
                r8.<init>(r0, r3)     // Catch:{ IOException -> 0x0570, RemoteException -> 0x04d7, all -> 0x0553 }
                r4 = r0
            L_0x0031:
                java.lang.String r0 = com.umeng.common.net.DownloadingService.j     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                java.lang.String r1 = "saveAPK: url = %1$15s\t|\tfilename = %2$15s"
                r3 = 2
                java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                r5 = 0
                com.umeng.common.net.a$a r6 = r13.j     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                java.lang.String r6 = r6.c     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                r3[r5] = r6     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                r5 = 1
                java.lang.String r6 = r4.getAbsolutePath()     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                r3[r5] = r6     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                java.lang.String r1 = java.lang.String.format(r1, r3)     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                com.umeng.common.Log.c(r0, r1)     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                java.net.URL r0 = new java.net.URL     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                com.umeng.common.net.a$a r1 = r13.j     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                java.lang.String r1 = r1.c     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                r0.<init>(r1)     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                java.net.URLConnection r0 = r0.openConnection()     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                java.lang.String r1 = "GET"
                r0.setRequestMethod(r1)     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                java.lang.String r1 = "Accept-Encoding"
                java.lang.String r3 = "identity"
                r0.setRequestProperty(r1, r3)     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                java.lang.String r1 = "Connection"
                java.lang.String r3 = "keep-alive"
                r0.addRequestProperty(r1, r3)     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                r1 = 5000(0x1388, float:7.006E-42)
                r0.setConnectTimeout(r1)     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                r1 = 10000(0x2710, float:1.4013E-41)
                r0.setReadTimeout(r1)     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                boolean r1 = r4.exists()     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                if (r1 == 0) goto L_0x00a9
                long r5 = r4.length()     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                r9 = 0
                int r1 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
                if (r1 <= 0) goto L_0x00a9
                java.lang.String r1 = "Range"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                java.lang.String r5 = "bytes="
                r3.<init>(r5)     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                long r5 = r4.length()     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                java.lang.String r5 = "-"
                java.lang.StringBuilder r3 = r3.append(r5)     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                r0.setRequestProperty(r1, r3)     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
            L_0x00a9:
                r0.connect()     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                java.io.InputStream r9 = r0.getInputStream()     // Catch:{ IOException -> 0x057c, RemoteException -> 0x0569, all -> 0x055d }
                if (r14 != 0) goto L_0x00d6
                r1 = 0
                r13.e = r1     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                int r0 = r0.getContentLength()     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                long r0 = (long) r0     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r13.f = r0     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r0 = com.umeng.common.net.DownloadingService.j     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r1 = "getContentLength: %1$15s"
                r2 = 1
                java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r3 = 0
                long r5 = r13.f     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r2[r3] = r5     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r1 = java.lang.String.format(r1, r2)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                com.umeng.common.Log.c(r0, r1)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
            L_0x00d6:
                r0 = 4096(0x1000, float:5.74E-42)
                byte[] r5 = new byte[r0]     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r0 = 0
                r6 = 50
                r1 = 1
                java.lang.String r2 = com.umeng.common.net.DownloadingService.j     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                com.umeng.common.net.a$a r7 = r13.j     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r7 = r7.b     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r7 = java.lang.String.valueOf(r7)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r3.<init>(r7)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r7 = "saveAPK getContentLength "
                java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                long r10 = r13.f     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r7 = java.lang.String.valueOf(r10)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.StringBuilder r3 = r3.append(r7)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                com.umeng.common.Log.c(r2, r3)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                android.content.Context r2 = r13.b     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                com.umeng.common.net.c r2 = com.umeng.common.net.c.a(r2)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                com.umeng.common.net.a$a r3 = r13.j     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r3 = r3.a     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                com.umeng.common.net.a$a r7 = r13.j     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r7 = r7.c     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r2.a(r3, r7)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
            L_0x0117:
                int r2 = r13.g     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                if (r2 >= 0) goto L_0x0121
                int r2 = r9.read(r5)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                if (r2 > 0) goto L_0x017d
            L_0x0121:
                r0 = r1
            L_0x0122:
                r9.close()     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r8.close()     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                int r1 = r13.g     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r2 = 1
                if (r1 != r2) goto L_0x0297
                java.util.Map r0 = com.umeng.common.net.DownloadingService.r     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                int r1 = r13.h     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.Object r0 = r0.get(r1)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                com.umeng.common.net.DownloadingService$d r0 = (com.umeng.common.net.DownloadingService.d) r0     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                long[] r1 = r0.f     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r2 = 0
                long r3 = r13.e     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r1[r2] = r3     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                long[] r1 = r0.f     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r2 = 1
                long r3 = r13.f     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r1[r2] = r3     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                long[] r0 = r0.f     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r1 = 2
                int r2 = r13.d     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                long r2 = (long) r2     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r0[r1] = r2     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                if (r9 == 0) goto L_0x0158
                r9.close()     // Catch:{ IOException -> 0x0274 }
            L_0x0158:
                if (r8 == 0) goto L_0x015d
                r8.close()     // Catch:{ IOException -> 0x0291 }
            L_0x015d:
                return
            L_0x015e:
                android.content.Context r0 = r13.b     // Catch:{ IOException -> 0x0570, RemoteException -> 0x04d7, all -> 0x0553 }
                java.io.File r0 = r0.getFilesDir()     // Catch:{ IOException -> 0x0570, RemoteException -> 0x04d7, all -> 0x0553 }
                java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ IOException -> 0x0570, RemoteException -> 0x04d7, all -> 0x0553 }
                r13.c = r0     // Catch:{ IOException -> 0x0570, RemoteException -> 0x04d7, all -> 0x0553 }
                android.content.Context r0 = r13.b     // Catch:{ IOException -> 0x0570, RemoteException -> 0x04d7, all -> 0x0553 }
                r4 = 32771(0x8003, float:4.5922E-41)
                java.io.FileOutputStream r1 = r0.openFileOutput(r3, r4)     // Catch:{ IOException -> 0x0570, RemoteException -> 0x04d7, all -> 0x0553 }
                android.content.Context r0 = r13.b     // Catch:{ IOException -> 0x0576, RemoteException -> 0x0564, all -> 0x0558 }
                java.io.File r0 = r0.getFileStreamPath(r3)     // Catch:{ IOException -> 0x0576, RemoteException -> 0x0564, all -> 0x0558 }
                r4 = r0
                r8 = r1
                goto L_0x0031
            L_0x017d:
                r3 = 0
                r8.write(r5, r3, r2)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                long r10 = r13.e     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                long r2 = (long) r2     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                long r2 = r2 + r10
                r13.e = r2     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                int r3 = r0 + 1
                int r0 = r0 % r6
                if (r0 != 0) goto L_0x01e6
                android.content.Context r0 = r13.b     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                boolean r0 = com.umeng.common.b.m(r0)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                if (r0 != 0) goto L_0x0196
                r0 = 0
                goto L_0x0122
            L_0x0196:
                long r10 = r13.e     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                float r0 = (float) r10     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r2 = 1120403456(0x42c80000, float:100.0)
                float r0 = r0 * r2
                long r10 = r13.f     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                float r2 = (float) r10     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                float r0 = r0 / r2
                int r0 = (int) r0     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r2 = 100
                if (r0 <= r2) goto L_0x0582
                r0 = 99
                r2 = r0
            L_0x01a8:
                com.umeng.common.net.DownloadingService$a r0 = r13.i     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                if (r0 == 0) goto L_0x01b3
                com.umeng.common.net.DownloadingService$a r0 = r13.i     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                int r7 = r13.h     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r0.a(r7, r2)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
            L_0x01b3:
                java.util.Map r0 = com.umeng.common.net.DownloadingService.q     // Catch:{ DeadObjectException -> 0x01e9 }
                com.umeng.common.net.a$a r7 = r13.j     // Catch:{ DeadObjectException -> 0x01e9 }
                java.lang.Object r0 = r0.get(r7)     // Catch:{ DeadObjectException -> 0x01e9 }
                if (r0 == 0) goto L_0x01d5
                java.util.Map r0 = com.umeng.common.net.DownloadingService.q     // Catch:{ DeadObjectException -> 0x01e9 }
                com.umeng.common.net.a$a r7 = r13.j     // Catch:{ DeadObjectException -> 0x01e9 }
                java.lang.Object r0 = r0.get(r7)     // Catch:{ DeadObjectException -> 0x01e9 }
                android.os.Messenger r0 = (android.os.Messenger) r0     // Catch:{ DeadObjectException -> 0x01e9 }
                r7 = 0
                r10 = 3
                r11 = 0
                android.os.Message r7 = android.os.Message.obtain(r7, r10, r2, r11)     // Catch:{ DeadObjectException -> 0x01e9 }
                r0.send(r7)     // Catch:{ DeadObjectException -> 0x01e9 }
            L_0x01d5:
                android.content.Context r0 = r13.b     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                com.umeng.common.net.c r0 = com.umeng.common.net.c.a(r0)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                com.umeng.common.net.a$a r7 = r13.j     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r7 = r7.a     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                com.umeng.common.net.a$a r10 = r13.j     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r10 = r10.c     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r0.a(r7, r10, r2)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
            L_0x01e6:
                r0 = r3
                goto L_0x0117
            L_0x01e9:
                r0 = move-exception
                java.lang.String r0 = com.umeng.common.net.DownloadingService.j     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r7 = "Service Client for downloading %1$15s is dead. Removing messenger from the service"
                r10 = 1
                java.lang.Object[] r10 = new java.lang.Object[r10]     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r11 = 0
                com.umeng.common.net.a$a r12 = r13.j     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r12 = r12.b     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r10[r11] = r12     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r7 = java.lang.String.format(r7, r10)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                com.umeng.common.Log.b(r0, r7)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.util.Map r0 = com.umeng.common.net.DownloadingService.q     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                com.umeng.common.net.a$a r7 = r13.j     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r10 = 0
                r0.put(r7, r10)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                goto L_0x01d5
            L_0x020c:
                r0 = move-exception
                r1 = r0
                r2 = r8
                r3 = r9
            L_0x0210:
                java.lang.String r0 = com.umeng.common.net.DownloadingService.j     // Catch:{ all -> 0x03e9 }
                java.lang.String r4 = r1.getMessage()     // Catch:{ all -> 0x03e9 }
                com.umeng.common.Log.c(r0, r4, r1)     // Catch:{ all -> 0x03e9 }
                int r0 = r13.d     // Catch:{ all -> 0x03e9 }
                int r0 = r0 + 1
                r13.d = r0     // Catch:{ all -> 0x03e9 }
                r4 = 3
                if (r0 <= r4) goto L_0x0411
                com.umeng.common.net.a$a r0 = r13.j     // Catch:{ all -> 0x03e9 }
                boolean r0 = r0.e     // Catch:{ all -> 0x03e9 }
                if (r0 != 0) goto L_0x0411
                java.lang.String r0 = com.umeng.common.net.DownloadingService.j     // Catch:{ RemoteException -> 0x03cb }
                java.lang.String r4 = "Download Fail out of max repeat count"
                com.umeng.common.Log.b(r0, r4)     // Catch:{ RemoteException -> 0x03cb }
                java.util.Map r0 = com.umeng.common.net.DownloadingService.q     // Catch:{ RemoteException -> 0x03cb }
                com.umeng.common.net.a$a r4 = r13.j     // Catch:{ RemoteException -> 0x03cb }
                java.lang.Object r0 = r0.get(r4)     // Catch:{ RemoteException -> 0x03cb }
                android.os.Messenger r0 = (android.os.Messenger) r0     // Catch:{ RemoteException -> 0x03cb }
                r4 = 0
                r5 = 5
                r6 = 0
                r7 = 0
                android.os.Message r4 = android.os.Message.obtain(r4, r5, r6, r7)     // Catch:{ RemoteException -> 0x03cb }
                r0.send(r4)     // Catch:{ RemoteException -> 0x03cb }
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ all -> 0x03e9 }
                int r4 = r13.h     // Catch:{ all -> 0x03e9 }
                r0.a(r4)     // Catch:{ all -> 0x03e9 }
                r13.a(r1)     // Catch:{ all -> 0x03e9 }
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ all -> 0x03e9 }
                android.os.Handler r0 = r0.n     // Catch:{ all -> 0x03e9 }
                com.umeng.common.net.j r1 = new com.umeng.common.net.j     // Catch:{ all -> 0x03e9 }
                r1.<init>(r13)     // Catch:{ all -> 0x03e9 }
                r0.post(r1)     // Catch:{ all -> 0x03e9 }
            L_0x0262:
                if (r3 == 0) goto L_0x0267
                r3.close()     // Catch:{ IOException -> 0x04ba }
            L_0x0267:
                if (r2 == 0) goto L_0x015d
                r2.close()     // Catch:{ IOException -> 0x026e }
                goto L_0x015d
            L_0x026e:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x015d
            L_0x0274:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x0285 }
                if (r8 == 0) goto L_0x015d
                r8.close()     // Catch:{ IOException -> 0x027f }
                goto L_0x015d
            L_0x027f:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x015d
            L_0x0285:
                r0 = move-exception
                if (r8 == 0) goto L_0x028b
                r8.close()     // Catch:{ IOException -> 0x028c }
            L_0x028b:
                throw r0
            L_0x028c:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x028b
            L_0x0291:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x015d
            L_0x0297:
                int r1 = r13.g     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r2 = 2
                if (r1 != r2) goto L_0x02e4
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                com.umeng.common.net.a$a r1 = r13.j     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                long r2 = r13.e     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                long r4 = r13.f     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                int r6 = r13.d     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                long r6 = (long) r6     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r0.a(r1, r2, r4, r6)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                android.app.NotificationManager r0 = r0.k     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                int r1 = r13.h     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r0.cancel(r1)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                if (r9 == 0) goto L_0x02ba
                r9.close()     // Catch:{ IOException -> 0x02c7 }
            L_0x02ba:
                if (r8 == 0) goto L_0x015d
                r8.close()     // Catch:{ IOException -> 0x02c1 }
                goto L_0x015d
            L_0x02c1:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x015d
            L_0x02c7:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x02d8 }
                if (r8 == 0) goto L_0x015d
                r8.close()     // Catch:{ IOException -> 0x02d2 }
                goto L_0x015d
            L_0x02d2:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x015d
            L_0x02d8:
                r0 = move-exception
                if (r8 == 0) goto L_0x02de
                r8.close()     // Catch:{ IOException -> 0x02df }
            L_0x02de:
                throw r0
            L_0x02df:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x02de
            L_0x02e4:
                if (r0 != 0) goto L_0x0357
                java.lang.String r0 = com.umeng.common.net.DownloadingService.j     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r2 = "Download Fail repeat count="
                r1.<init>(r2)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                int r2 = r13.d     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                com.umeng.common.Log.b(r0, r1)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.util.Map r0 = com.umeng.common.net.DownloadingService.q     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                com.umeng.common.net.a$a r1 = r13.j     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.Object r0 = r0.get(r1)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                android.os.Messenger r0 = (android.os.Messenger) r0     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r1 = 0
                r2 = 5
                r3 = 0
                r4 = 0
                android.os.Message r1 = android.os.Message.obtain(r1, r2, r3, r4)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r0.send(r1)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                int r1 = r13.h     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r0.a(r1)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                com.umeng.common.net.DownloadingService$a r0 = r13.i     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                if (r0 == 0) goto L_0x0328
                com.umeng.common.net.DownloadingService$a r0 = r13.i     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                int r1 = r13.h     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r2 = 0
                r0.a(r1, r2)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
            L_0x0328:
                if (r9 == 0) goto L_0x032d
                r9.close()     // Catch:{ IOException -> 0x033a }
            L_0x032d:
                if (r8 == 0) goto L_0x015d
                r8.close()     // Catch:{ IOException -> 0x0334 }
                goto L_0x015d
            L_0x0334:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x015d
            L_0x033a:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x034b }
                if (r8 == 0) goto L_0x015d
                r8.close()     // Catch:{ IOException -> 0x0345 }
                goto L_0x015d
            L_0x0345:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x015d
            L_0x034b:
                r0 = move-exception
                if (r8 == 0) goto L_0x0351
                r8.close()     // Catch:{ IOException -> 0x0352 }
            L_0x0351:
                throw r0
            L_0x0352:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x0351
            L_0x0357:
                com.umeng.common.net.a$a r0 = r13.j     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String[] r0 = r0.d     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                if (r0 == 0) goto L_0x0392
                java.util.HashMap r0 = new java.util.HashMap     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r0.<init>()     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r1 = "dsize"
                long r2 = r13.f     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r0.put(r1, r2)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r1 = com.umeng.common.b.g.a()     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r2 = " "
                java.lang.String[] r1 = r1.split(r2)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r2 = 1
                r1 = r1[r2]     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r2 = "dtime"
                r0.put(r2, r1)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r1 = "ptimes"
                int r2 = r13.d     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r0.put(r1, r2)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r1 = 1
                com.umeng.common.net.a$a r2 = r13.j     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String[] r2 = r2.d     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                com.umeng.common.net.DownloadingService.b(r0, r1, r2)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
            L_0x0392:
                java.io.File r0 = new java.io.File     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r1 = r4.getParent()     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r2 = r4.getName()     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r3 = ".tmp"
                java.lang.String r5 = ""
                java.lang.String r2 = r2.replace(r3, r5)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r0.<init>(r1, r2)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r4.renameTo(r0)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                java.lang.String r0 = r0.getAbsolutePath()     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                com.umeng.common.net.DownloadingService$a r1 = r13.i     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                if (r1 == 0) goto L_0x03b9
                com.umeng.common.net.DownloadingService$a r1 = r13.i     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                int r2 = r13.h     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
                r1.a(r2, r0)     // Catch:{ IOException -> 0x020c, RemoteException -> 0x056d }
            L_0x03b9:
                if (r9 == 0) goto L_0x03be
                r9.close()     // Catch:{ IOException -> 0x0536 }
            L_0x03be:
                if (r8 == 0) goto L_0x015d
                r8.close()     // Catch:{ IOException -> 0x03c5 }
                goto L_0x015d
            L_0x03c5:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x015d
            L_0x03cb:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x03f7 }
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ all -> 0x03e9 }
                int r4 = r13.h     // Catch:{ all -> 0x03e9 }
                r0.a(r4)     // Catch:{ all -> 0x03e9 }
                r13.a(r1)     // Catch:{ all -> 0x03e9 }
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ all -> 0x03e9 }
                android.os.Handler r0 = r0.n     // Catch:{ all -> 0x03e9 }
                com.umeng.common.net.j r1 = new com.umeng.common.net.j     // Catch:{ all -> 0x03e9 }
                r1.<init>(r13)     // Catch:{ all -> 0x03e9 }
                r0.post(r1)     // Catch:{ all -> 0x03e9 }
                goto L_0x0262
            L_0x03e9:
                r0 = move-exception
                r8 = r2
                r9 = r3
            L_0x03ec:
                if (r9 == 0) goto L_0x03f1
                r9.close()     // Catch:{ IOException -> 0x0513 }
            L_0x03f1:
                if (r8 == 0) goto L_0x03f6
                r8.close()     // Catch:{ IOException -> 0x0530 }
            L_0x03f6:
                throw r0
            L_0x03f7:
                r0 = move-exception
                com.umeng.common.net.DownloadingService r4 = com.umeng.common.net.DownloadingService.this     // Catch:{ all -> 0x03e9 }
                int r5 = r13.h     // Catch:{ all -> 0x03e9 }
                r4.a(r5)     // Catch:{ all -> 0x03e9 }
                r13.a(r1)     // Catch:{ all -> 0x03e9 }
                com.umeng.common.net.DownloadingService r1 = com.umeng.common.net.DownloadingService.this     // Catch:{ all -> 0x03e9 }
                android.os.Handler r1 = r1.n     // Catch:{ all -> 0x03e9 }
                com.umeng.common.net.j r4 = new com.umeng.common.net.j     // Catch:{ all -> 0x03e9 }
                r4.<init>(r13)     // Catch:{ all -> 0x03e9 }
                r1.post(r4)     // Catch:{ all -> 0x03e9 }
                throw r0     // Catch:{ all -> 0x03e9 }
            L_0x0411:
                java.lang.String r0 = com.umeng.common.net.DownloadingService.j     // Catch:{ all -> 0x03e9 }
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x03e9 }
                java.lang.String r4 = "wait for repeating Test network repeat count="
                r1.<init>(r4)     // Catch:{ all -> 0x03e9 }
                int r4 = r13.d     // Catch:{ all -> 0x03e9 }
                java.lang.StringBuilder r1 = r1.append(r4)     // Catch:{ all -> 0x03e9 }
                java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x03e9 }
                com.umeng.common.Log.c(r0, r1)     // Catch:{ all -> 0x03e9 }
                com.umeng.common.net.a$a r0 = r13.j     // Catch:{ InterruptedException -> 0x0442 }
                boolean r0 = r0.e     // Catch:{ InterruptedException -> 0x0442 }
                if (r0 != 0) goto L_0x0455
                r0 = 8000(0x1f40, double:3.9525E-320)
                java.lang.Thread.sleep(r0)     // Catch:{ InterruptedException -> 0x0442 }
                long r0 = r13.f     // Catch:{ InterruptedException -> 0x0442 }
                r4 = 1
                int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
                if (r0 >= 0) goto L_0x044f
                r0 = 0
                r13.a(r0)     // Catch:{ InterruptedException -> 0x0442 }
                goto L_0x0262
            L_0x0442:
                r0 = move-exception
                r13.a(r0)     // Catch:{ all -> 0x03e9 }
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ all -> 0x03e9 }
                int r1 = r13.h     // Catch:{ all -> 0x03e9 }
                r0.a(r1)     // Catch:{ all -> 0x03e9 }
                goto L_0x0262
            L_0x044f:
                r0 = 1
                r13.a(r0)     // Catch:{ InterruptedException -> 0x0442 }
                goto L_0x0262
            L_0x0455:
                java.util.Map r0 = com.umeng.common.net.DownloadingService.r     // Catch:{ InterruptedException -> 0x0442 }
                int r1 = r13.h     // Catch:{ InterruptedException -> 0x0442 }
                java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ InterruptedException -> 0x0442 }
                java.lang.Object r0 = r0.get(r1)     // Catch:{ InterruptedException -> 0x0442 }
                com.umeng.common.net.DownloadingService$d r0 = (com.umeng.common.net.DownloadingService.d) r0     // Catch:{ InterruptedException -> 0x0442 }
                long[] r1 = r0.f     // Catch:{ InterruptedException -> 0x0442 }
                r4 = 0
                long r5 = r13.e     // Catch:{ InterruptedException -> 0x0442 }
                r1[r4] = r5     // Catch:{ InterruptedException -> 0x0442 }
                long[] r1 = r0.f     // Catch:{ InterruptedException -> 0x0442 }
                r4 = 1
                long r5 = r13.f     // Catch:{ InterruptedException -> 0x0442 }
                r1[r4] = r5     // Catch:{ InterruptedException -> 0x0442 }
                long[] r0 = r0.f     // Catch:{ InterruptedException -> 0x0442 }
                r1 = 2
                int r4 = r13.d     // Catch:{ InterruptedException -> 0x0442 }
                long r4 = (long) r4     // Catch:{ InterruptedException -> 0x0442 }
                r0[r1] = r4     // Catch:{ InterruptedException -> 0x0442 }
                int r0 = r13.h     // Catch:{ InterruptedException -> 0x0442 }
                java.lang.String r1 = "continue"
                java.lang.String r0 = com.umeng.common.net.l.a(r0, r1)     // Catch:{ InterruptedException -> 0x0442 }
                android.content.Intent r1 = new android.content.Intent     // Catch:{ InterruptedException -> 0x0442 }
                android.content.Context r4 = r13.b     // Catch:{ InterruptedException -> 0x0442 }
                java.lang.Class<com.umeng.common.net.DownloadingService> r5 = com.umeng.common.net.DownloadingService.class
                r1.<init>(r4, r5)     // Catch:{ InterruptedException -> 0x0442 }
                java.lang.String r4 = "com.umeng.broadcast.download.msg"
                r1.putExtra(r4, r0)     // Catch:{ InterruptedException -> 0x0442 }
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ InterruptedException -> 0x0442 }
                android.content.Context r4 = r13.b     // Catch:{ InterruptedException -> 0x0442 }
                boolean unused = r0.a(r4, r1)     // Catch:{ InterruptedException -> 0x0442 }
                com.umeng.common.net.DownloadingService r0 = com.umeng.common.net.DownloadingService.this     // Catch:{ InterruptedException -> 0x0442 }
                android.content.Context r1 = r13.b     // Catch:{ InterruptedException -> 0x0442 }
                android.content.Context r4 = r13.b     // Catch:{ InterruptedException -> 0x0442 }
                com.umeng.common.c r4 = com.umeng.common.c.a(r4)     // Catch:{ InterruptedException -> 0x0442 }
                java.lang.String r5 = "Download_info_interrupt"
                int r4 = r4.f(r5)     // Catch:{ InterruptedException -> 0x0442 }
                java.lang.String r1 = r1.getString(r4)     // Catch:{ InterruptedException -> 0x0442 }
                r0.a(r1)     // Catch:{ InterruptedException -> 0x0442 }
                java.lang.String r0 = com.umeng.common.net.DownloadingService.j     // Catch:{ InterruptedException -> 0x0442 }
                java.lang.String r1 = "changed play state button on op-notification."
                com.umeng.common.Log.c(r0, r1)     // Catch:{ InterruptedException -> 0x0442 }
                goto L_0x0262
            L_0x04ba:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x04cb }
                if (r2 == 0) goto L_0x015d
                r2.close()     // Catch:{ IOException -> 0x04c5 }
                goto L_0x015d
            L_0x04c5:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x015d
            L_0x04cb:
                r0 = move-exception
                if (r2 == 0) goto L_0x04d1
                r2.close()     // Catch:{ IOException -> 0x04d2 }
            L_0x04d1:
                throw r0
            L_0x04d2:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x04d1
            L_0x04d7:
                r0 = move-exception
                r8 = r1
                r9 = r2
            L_0x04da:
                com.umeng.common.net.DownloadingService r1 = com.umeng.common.net.DownloadingService.this     // Catch:{ all -> 0x0561 }
                int r2 = r13.h     // Catch:{ all -> 0x0561 }
                r1.a(r2)     // Catch:{ all -> 0x0561 }
                r0.printStackTrace()     // Catch:{ all -> 0x0561 }
                if (r9 == 0) goto L_0x04e9
                r9.close()     // Catch:{ IOException -> 0x04f6 }
            L_0x04e9:
                if (r8 == 0) goto L_0x015d
                r8.close()     // Catch:{ IOException -> 0x04f0 }
                goto L_0x015d
            L_0x04f0:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x015d
            L_0x04f6:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x0507 }
                if (r8 == 0) goto L_0x015d
                r8.close()     // Catch:{ IOException -> 0x0501 }
                goto L_0x015d
            L_0x0501:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x015d
            L_0x0507:
                r0 = move-exception
                if (r8 == 0) goto L_0x050d
                r8.close()     // Catch:{ IOException -> 0x050e }
            L_0x050d:
                throw r0
            L_0x050e:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x050d
            L_0x0513:
                r1 = move-exception
                r1.printStackTrace()     // Catch:{ all -> 0x0524 }
                if (r8 == 0) goto L_0x03f6
                r8.close()     // Catch:{ IOException -> 0x051e }
                goto L_0x03f6
            L_0x051e:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x03f6
            L_0x0524:
                r0 = move-exception
                if (r8 == 0) goto L_0x052a
                r8.close()     // Catch:{ IOException -> 0x052b }
            L_0x052a:
                throw r0
            L_0x052b:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x052a
            L_0x0530:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x03f6
            L_0x0536:
                r0 = move-exception
                r0.printStackTrace()     // Catch:{ all -> 0x0547 }
                if (r8 == 0) goto L_0x015d
                r8.close()     // Catch:{ IOException -> 0x0541 }
                goto L_0x015d
            L_0x0541:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x015d
            L_0x0547:
                r0 = move-exception
                if (r8 == 0) goto L_0x054d
                r8.close()     // Catch:{ IOException -> 0x054e }
            L_0x054d:
                throw r0
            L_0x054e:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x054d
            L_0x0553:
                r0 = move-exception
                r8 = r1
                r9 = r2
                goto L_0x03ec
            L_0x0558:
                r0 = move-exception
                r8 = r1
                r9 = r2
                goto L_0x03ec
            L_0x055d:
                r0 = move-exception
                r9 = r2
                goto L_0x03ec
            L_0x0561:
                r0 = move-exception
                goto L_0x03ec
            L_0x0564:
                r0 = move-exception
                r8 = r1
                r9 = r2
                goto L_0x04da
            L_0x0569:
                r0 = move-exception
                r9 = r2
                goto L_0x04da
            L_0x056d:
                r0 = move-exception
                goto L_0x04da
            L_0x0570:
                r0 = move-exception
                r3 = r2
                r2 = r1
                r1 = r0
                goto L_0x0210
            L_0x0576:
                r0 = move-exception
                r3 = r2
                r2 = r1
                r1 = r0
                goto L_0x0210
            L_0x057c:
                r0 = move-exception
                r1 = r0
                r3 = r2
                r2 = r8
                goto L_0x0210
            L_0x0582:
                r2 = r0
                goto L_0x01a8
            */
            throw new UnsupportedOperationException("Method not decompiled: com.umeng.common.net.DownloadingService.b.a(boolean):void");
        }

        public void a(int i2) {
            this.g = i2;
        }

        public void run() {
            boolean z = false;
            this.d = 0;
            try {
                if (this.i != null) {
                    this.i.a(this.h);
                }
                if (this.e > 0) {
                    z = true;
                }
                a(z);
                if (DownloadingService.q.size() <= 0) {
                    DownloadingService.this.stopSelf();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    class c extends Handler {
        c() {
        }

        public void handleMessage(Message message) {
            Log.c(DownloadingService.j, "IncomingHandler(msg.what:" + message.what + " msg.arg1:" + message.arg1 + " msg.arg2:" + message.arg2 + " msg.replyTo:" + message.replyTo);
            switch (message.what) {
                case 4:
                    Bundle data = message.getData();
                    Log.c(DownloadingService.j, "IncomingHandler(msg.getData():" + data);
                    a.C0003a a2 = a.C0003a.a(data);
                    if (DownloadingService.d(a2)) {
                        Log.a(DownloadingService.j, String.valueOf(a2.b) + " is already in downloading list. ");
                        Toast.makeText(DownloadingService.this.m, com.umeng.common.c.a(DownloadingService.this.m).f("umeng_common_action_info_exist"), 0).show();
                        return;
                    }
                    DownloadingService.q.put(a2, message.replyTo);
                    DownloadingService.this.c(a2);
                    return;
                default:
                    super.handleMessage(message);
                    return;
            }
        }
    }

    private static class d {
        b a;
        Notification b;
        int c;
        int d;
        a.C0003a e;
        long[] f = new long[3];

        public d(a.C0003a aVar, int i) {
            this.c = i;
            this.e = aVar;
        }

        public void a() {
            DownloadingService.r.put(Integer.valueOf(this.c), this);
        }

        public void b() {
            if (DownloadingService.r.containsKey(Integer.valueOf(this.c))) {
                DownloadingService.r.remove(Integer.valueOf(this.c));
            }
        }
    }

    public static int a(a.C0003a aVar) {
        return Math.abs((int) (((long) ((aVar.b.hashCode() >> 2) + (aVar.c.hashCode() >> 3))) + System.currentTimeMillis()));
    }

    /* access modifiers changed from: private */
    public Notification a(a.C0003a aVar, int i2, int i3) {
        Context applicationContext = getApplicationContext();
        Notification notification = new Notification(17301633, com.umeng.common.a.j, 1);
        RemoteViews remoteViews = new RemoteViews(applicationContext.getPackageName(), com.umeng.common.a.b.a(applicationContext));
        remoteViews.setProgressBar(com.umeng.common.a.a.c(applicationContext), 100, i3, false);
        remoteViews.setTextViewText(com.umeng.common.a.a.b(applicationContext), String.valueOf(i3) + "%");
        remoteViews.setTextViewText(com.umeng.common.a.a.d(applicationContext), String.valueOf(applicationContext.getResources().getString(com.umeng.common.a.c.g(applicationContext.getApplicationContext()))) + aVar.b);
        remoteViews.setTextViewText(com.umeng.common.a.a.a(applicationContext), "");
        remoteViews.setImageViewResource(com.umeng.common.a.a.e(applicationContext), 17301633);
        notification.contentView = remoteViews;
        notification.contentIntent = PendingIntent.getActivity(applicationContext, 0, new Intent(), k.NOVEMBER);
        if (aVar.e) {
            notification.flags = 2;
            remoteViews.setOnClickPendingIntent(com.umeng.common.a.a.f(applicationContext), l.b(getApplicationContext(), l.a(i2, l.b)));
            remoteViews.setViewVisibility(com.umeng.common.a.a.f(applicationContext), 0);
            b(notification, i2);
            PendingIntent b2 = l.b(getApplicationContext(), l.a(i2, l.c));
            remoteViews.setViewVisibility(com.umeng.common.a.a.h(applicationContext), 0);
            remoteViews.setOnClickPendingIntent(com.umeng.common.a.a.h(applicationContext), b2);
        } else {
            notification.flags = 16;
            remoteViews.setViewVisibility(com.umeng.common.a.a.f(applicationContext), 8);
            remoteViews.setViewVisibility(com.umeng.common.a.a.h(applicationContext), 8);
        }
        return notification;
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        d dVar = r.get(Integer.valueOf(i2));
        Log.c(j, "download service clear cache " + dVar.e.b);
        if (dVar.a != null) {
            dVar.a.a(2);
        }
        this.k.cancel(dVar.c);
        if (q.containsKey(dVar.e)) {
            q.remove(dVar.e);
        }
        dVar.b();
        e();
    }

    private void a(Notification notification, int i2) {
        int f2 = com.umeng.common.a.a.f(this.m);
        notification.contentView.setTextViewText(f2, this.m.getResources().getString(com.umeng.common.a.c.e(this.m.getApplicationContext())));
        notification.contentView.setInt(f2, "setBackgroundResource", com.umeng.common.c.a(this.m).c("umeng_common_gradient_green"));
        this.k.notify(i2, notification);
    }

    /* access modifiers changed from: private */
    public void a(a.C0003a aVar, long j2, long j3, long j4) {
        if (aVar.d != null) {
            HashMap hashMap = new HashMap();
            hashMap.put("dsize", String.valueOf(j2));
            hashMap.put("dtime", g.a().split(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR)[1]);
            float f2 = 0.0f;
            if (j3 > 0) {
                f2 = ((float) j2) / ((float) j3);
            }
            hashMap.put("dpcent", String.valueOf((int) (f2 * 100.0f)));
            hashMap.put("ptimes", String.valueOf(j4));
            b(hashMap, false, aVar.d);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(android.content.Context r16, android.content.Intent r17) {
        /*
            r15 = this;
            android.os.Bundle r1 = r17.getExtras()     // Catch:{ Exception -> 0x00e3 }
            java.lang.String r2 = "com.umeng.broadcast.download.msg"
            java.lang.String r1 = r1.getString(r2)     // Catch:{ Exception -> 0x00e3 }
            java.lang.String r2 = ":"
            java.lang.String[] r1 = r1.split(r2)     // Catch:{ Exception -> 0x00e3 }
            r2 = 0
            r2 = r1[r2]     // Catch:{ Exception -> 0x00e3 }
            int r5 = java.lang.Integer.parseInt(r2)     // Catch:{ Exception -> 0x00e3 }
            r2 = 1
            r1 = r1[r2]     // Catch:{ Exception -> 0x00e3 }
            java.lang.String r2 = r1.trim()     // Catch:{ Exception -> 0x00e3 }
            if (r5 == 0) goto L_0x00e7
            boolean r1 = android.text.TextUtils.isEmpty(r2)     // Catch:{ Exception -> 0x00e3 }
            if (r1 != 0) goto L_0x00e7
            java.util.Map<java.lang.Integer, com.umeng.common.net.DownloadingService$d> r1 = com.umeng.common.net.DownloadingService.r     // Catch:{ Exception -> 0x00e3 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x00e3 }
            boolean r1 = r1.containsKey(r3)     // Catch:{ Exception -> 0x00e3 }
            if (r1 == 0) goto L_0x00e7
            java.util.Map<java.lang.Integer, com.umeng.common.net.DownloadingService$d> r1 = com.umeng.common.net.DownloadingService.r     // Catch:{ Exception -> 0x00e3 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x00e3 }
            java.lang.Object r1 = r1.get(r3)     // Catch:{ Exception -> 0x00e3 }
            r0 = r1
            com.umeng.common.net.DownloadingService$d r0 = (com.umeng.common.net.DownloadingService.d) r0     // Catch:{ Exception -> 0x00e3 }
            r14 = r0
            com.umeng.common.net.DownloadingService$b r1 = r14.a     // Catch:{ Exception -> 0x00e3 }
            java.lang.String r3 = "continue"
            boolean r3 = r3.equals(r2)     // Catch:{ Exception -> 0x00e3 }
            if (r3 == 0) goto L_0x00ae
            if (r1 != 0) goto L_0x0099
            java.lang.String r1 = com.umeng.common.net.DownloadingService.j     // Catch:{ Exception -> 0x00e3 }
            java.lang.String r2 = "Receive action do play click."
            com.umeng.common.Log.c(r1, r2)     // Catch:{ Exception -> 0x00e3 }
            java.lang.String r1 = "android.permission.ACCESS_NETWORK_STATE"
            r0 = r16
            boolean r1 = com.umeng.common.b.a(r0, r1)     // Catch:{ Exception -> 0x00e3 }
            if (r1 == 0) goto L_0x007f
            boolean r1 = com.umeng.common.b.m(r16)     // Catch:{ Exception -> 0x00e3 }
            if (r1 != 0) goto L_0x007f
            android.content.res.Resources r1 = r16.getResources()     // Catch:{ Exception -> 0x00e3 }
            android.content.Context r2 = r16.getApplicationContext()     // Catch:{ Exception -> 0x00e3 }
            int r2 = com.umeng.common.a.c.a(r2)     // Catch:{ Exception -> 0x00e3 }
            java.lang.String r1 = r1.getString(r2)     // Catch:{ Exception -> 0x00e3 }
            r2 = 1
            r0 = r16
            android.widget.Toast r1 = android.widget.Toast.makeText(r0, r1, r2)     // Catch:{ Exception -> 0x00e3 }
            r1.show()     // Catch:{ Exception -> 0x00e3 }
            r1 = 0
        L_0x007e:
            return r1
        L_0x007f:
            com.umeng.common.net.DownloadingService$b r1 = new com.umeng.common.net.DownloadingService$b     // Catch:{ Exception -> 0x00e3 }
            com.umeng.common.net.a$a r4 = r14.e     // Catch:{ Exception -> 0x00e3 }
            int r6 = r14.d     // Catch:{ Exception -> 0x00e3 }
            com.umeng.common.net.DownloadingService$a r7 = r15.o     // Catch:{ Exception -> 0x00e3 }
            r2 = r15
            r3 = r16
            r1.<init>(r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x00e3 }
            r14.a = r1     // Catch:{ Exception -> 0x00e3 }
            r1.start()     // Catch:{ Exception -> 0x00e3 }
            android.app.Notification r1 = r14.b     // Catch:{ Exception -> 0x00e3 }
            r15.b(r1, r5)     // Catch:{ Exception -> 0x00e3 }
            r1 = 1
            goto L_0x007e
        L_0x0099:
            java.lang.String r2 = com.umeng.common.net.DownloadingService.j     // Catch:{ Exception -> 0x00e3 }
            java.lang.String r3 = "Receive action do play click."
            com.umeng.common.Log.c(r2, r3)     // Catch:{ Exception -> 0x00e3 }
            r2 = 1
            r1.a(r2)     // Catch:{ Exception -> 0x00e3 }
            r1 = 0
            r14.a = r1     // Catch:{ Exception -> 0x00e3 }
            android.app.Notification r1 = r14.b     // Catch:{ Exception -> 0x00e3 }
            r15.a(r1, r5)     // Catch:{ Exception -> 0x00e3 }
            r1 = 1
            goto L_0x007e
        L_0x00ae:
            java.lang.String r3 = "cancel"
            boolean r2 = r3.equals(r2)     // Catch:{ Exception -> 0x00e3 }
            if (r2 == 0) goto L_0x00e7
            java.lang.String r2 = com.umeng.common.net.DownloadingService.j     // Catch:{ Exception -> 0x00e3 }
            java.lang.String r3 = "Receive action do stop click."
            com.umeng.common.Log.c(r2, r3)     // Catch:{ Exception -> 0x00e3 }
            if (r1 == 0) goto L_0x00c8
            r2 = 2
            r1.a(r2)     // Catch:{ Exception -> 0x00de, all -> 0x00e9 }
        L_0x00c3:
            r15.a(r5)     // Catch:{ Exception -> 0x00e3 }
        L_0x00c6:
            r1 = 1
            goto L_0x007e
        L_0x00c8:
            long[] r1 = r14.f     // Catch:{ Exception -> 0x00de, all -> 0x00e9 }
            r2 = 0
            r8 = r1[r2]     // Catch:{ Exception -> 0x00de, all -> 0x00e9 }
            long[] r1 = r14.f     // Catch:{ Exception -> 0x00de, all -> 0x00e9 }
            r2 = 1
            r10 = r1[r2]     // Catch:{ Exception -> 0x00de, all -> 0x00e9 }
            long[] r1 = r14.f     // Catch:{ Exception -> 0x00de, all -> 0x00e9 }
            r2 = 2
            r12 = r1[r2]     // Catch:{ Exception -> 0x00de, all -> 0x00e9 }
            com.umeng.common.net.a$a r7 = r14.e     // Catch:{ Exception -> 0x00de, all -> 0x00e9 }
            r6 = r15
            r6.a(r7, r8, r10, r12)     // Catch:{ Exception -> 0x00de, all -> 0x00e9 }
            goto L_0x00c3
        L_0x00de:
            r1 = move-exception
            r15.a(r5)     // Catch:{ Exception -> 0x00e3 }
            goto L_0x00c6
        L_0x00e3:
            r1 = move-exception
            r1.printStackTrace()
        L_0x00e7:
            r1 = 0
            goto L_0x007e
        L_0x00e9:
            r1 = move-exception
            r15.a(r5)     // Catch:{ Exception -> 0x00e3 }
            throw r1     // Catch:{ Exception -> 0x00e3 }
        */
        throw new UnsupportedOperationException("Method not decompiled: com.umeng.common.net.DownloadingService.a(android.content.Context, android.content.Intent):boolean");
    }

    private void b(Notification notification, int i2) {
        int f2 = com.umeng.common.a.a.f(this.m);
        notification.contentView.setTextViewText(f2, this.m.getResources().getString(com.umeng.common.a.c.d(this.m.getApplicationContext())));
        notification.contentView.setInt(f2, "setBackgroundResource", com.umeng.common.c.a(this.m).c("umeng_common_gradient_orange"));
        this.k.notify(i2, notification);
    }

    /* access modifiers changed from: private */
    public static final void b(Map<String, String> map, boolean z, String[] strArr) {
        new Thread(new i(strArr, z, map)).start();
    }

    /* access modifiers changed from: private */
    public static boolean b(Context context) {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses == null) {
            return false;
        }
        String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
            if (next.importance == 100 && next.processName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void c(a.C0003a aVar) {
        Log.c(j, "startDownload([mComponentName:" + aVar.a + " mTitle:" + aVar.b + " mUrl:" + aVar.c + "])");
        int a2 = a(aVar);
        b bVar = new b(getApplicationContext(), aVar, a2, 0, this.o);
        d dVar = new d(aVar, a2);
        dVar.a();
        dVar.a = bVar;
        bVar.start();
        e();
        if (h) {
            for (Integer num : r.keySet()) {
                Log.c(j, "Running task " + r.get(num).e.b);
            }
        }
    }

    private void d() {
        IntentFilter intentFilter = new IntentFilter(l.d);
        this.s = new f(this);
        registerReceiver(this.s, intentFilter);
    }

    /* access modifiers changed from: private */
    public static boolean d(a.C0003a aVar) {
        if (h) {
            int nextInt = new Random().nextInt(1000);
            if (q != null) {
                for (a.C0003a next : q.keySet()) {
                    Log.c(j, "_" + nextInt + " downling  " + next.b + "   " + next.c);
                }
            } else {
                Log.c(j, "_" + nextInt + "downling  null");
            }
        }
        if (q == null) {
            return false;
        }
        for (a.C0003a aVar2 : q.keySet()) {
            if (aVar2.c.equals(aVar.c)) {
                return true;
            }
        }
        return false;
    }

    private void e() {
        if (h) {
            int size = q.size();
            int size2 = r.size();
            Log.a(j, "Client size =" + size + "   cacheSize = " + size2);
            if (size != size2) {
                throw new RuntimeException("Client size =" + size + "   cacheSize = " + size2);
            }
        }
    }

    public void a(String str) {
        synchronized (t) {
            if (!t.booleanValue()) {
                Log.c(j, "show single toast.[" + str + "]");
                t = true;
                this.n.post(new g(this, str));
                this.n.postDelayed(new h(this), 1200);
            }
        }
    }

    public IBinder onBind(Intent intent) {
        Log.c(j, "onBind ");
        return this.i.getBinder();
    }

    public void onCreate() {
        super.onCreate();
        if (h) {
            Log.LOG = true;
            Debug.waitForDebugger();
        }
        Log.c(j, "onCreate ");
        this.k = (NotificationManager) getSystemService("notification");
        this.m = this;
        this.n = new d(this);
        this.o = new e(this);
    }

    public void onDestroy() {
        try {
            c.a(getApplicationContext()).a(259200);
            c.a(getApplicationContext()).finalize();
            if (this.s != null) {
                unregisterReceiver(this.s);
            }
        } catch (Exception e2) {
            Log.b(j, e2.getMessage());
        }
        super.onDestroy();
    }

    public void onStart(Intent intent, int i2) {
        Log.c(j, "onStart ");
        a(getApplicationContext(), intent);
        super.onStart(intent, i2);
    }
}
