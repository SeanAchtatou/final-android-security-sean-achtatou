package com.umeng.common.net;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.a.a.i.k;
import com.umeng.common.net.a;

public class l {
    public static final String a = "pause";
    public static final String b = "continue";
    public static final String c = "cancel";
    public static final String d = "com.umeng.intent.DOWNLOAD";
    public static final String e = "com.umeng.broadcast.download.msg";

    public static int a(a.C0003a aVar) {
        int currentTimeMillis = (int) System.currentTimeMillis();
        return currentTimeMillis < 0 ? -currentTimeMillis : currentTimeMillis;
    }

    public static PendingIntent a(Context context, String str) {
        Intent intent = new Intent(d);
        intent.addFlags(1073741824);
        intent.putExtra(e, str);
        return PendingIntent.getBroadcast(context, str.hashCode(), intent, k.NOVEMBER);
    }

    public static String a(int i, String str) {
        if (i == 0) {
            return null;
        }
        return new StringBuilder(String.valueOf(i)).toString() + ":" + str;
    }

    public static PendingIntent b(Context context, String str) {
        Intent intent = new Intent(context, DownloadingService.class);
        intent.putExtra(e, str);
        return PendingIntent.getService(context, str.hashCode(), intent, k.NOVEMBER);
    }
}
