package com.umeng.common.b;

import java.net.URLEncoder;
import java.util.Map;
import java.util.Set;

public class h {
    public static String a(Map<String, Object> map, String str) {
        if (map == null || map.isEmpty()) {
            return str;
        }
        StringBuilder sb = new StringBuilder(str);
        Set<String> keySet = map.keySet();
        if (!str.endsWith("?")) {
            sb.append("?");
        }
        for (String next : keySet) {
            sb.append(String.valueOf(URLEncoder.encode(next)) + "=" + URLEncoder.encode(map.get(next) == null ? "" : map.get(next).toString()) + "&");
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }
}
