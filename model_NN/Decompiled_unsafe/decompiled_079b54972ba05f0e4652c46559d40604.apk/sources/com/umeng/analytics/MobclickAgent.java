package com.umeng.analytics;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.umeng.common.Log;
import com.umeng.common.b;
import java.util.Map;
import javax.microedition.khronos.opengles.GL10;

public class MobclickAgent {
    private static final b a = new b();
    private static /* synthetic */ int[] b;

    static b a() {
        return a;
    }

    static /* synthetic */ int[] b() {
        int[] iArr = b;
        if (iArr == null) {
            iArr = new int[Gender.values().length];
            try {
                iArr[Gender.Female.ordinal()] = 2;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[Gender.Male.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[Gender.Unknown.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            b = iArr;
        }
        return iArr;
    }

    public static void flush(Context context) {
        a.d(context);
    }

    public static String getConfigParams(Context context, String str) {
        return h.b(context).getString(str, "");
    }

    public static void onError(Context context) {
        a.b(context);
    }

    public static void onError(Context context, String str) {
        if (str == null || str.length() == 0) {
            Log.b("MobclickAgent", "unexpected empty appkey in onError");
            return;
        }
        a.b = str;
        onError(context);
    }

    public static void onEvent(Context context, String str) {
        a.a(context, str, (String) null, -1, 1);
    }

    public static void onEvent(Context context, String str, int i) {
        a.a(context, str, (String) null, -1, i);
    }

    public static void onEvent(Context context, String str, String str2) {
        if (TextUtils.isEmpty(str2)) {
            Log.a("MobclickAgent", "label is null or empty");
        } else {
            a.a(context, str, str2, -1, 1);
        }
    }

    public static void onEvent(Context context, String str, String str2, int i) {
        if (TextUtils.isEmpty(str2)) {
            Log.a("MobclickAgent", "label is null or empty");
        } else {
            a.a(context, str, str2, -1, i);
        }
    }

    public static void onEvent(Context context, String str, Map<String, String> map) {
        a.a(context, str, map, -1);
    }

    public static void onEventBegin(Context context, String str) {
        a.b(context, str);
    }

    public static void onEventBegin(Context context, String str, String str2) {
        a.a(context, str, str2);
    }

    public static void onEventDuration(Context context, String str, long j) {
        if (j <= 0) {
            Log.a("MobclickAgent", "duration is not valid in onEventDuration");
        } else {
            a.a(context, str, (String) null, j, 1);
        }
    }

    public static void onEventDuration(Context context, String str, String str2, long j) {
        if (TextUtils.isEmpty(str2)) {
            Log.a("MobclickAgent", "label is null or empty");
        } else if (j <= 0) {
            Log.a("MobclickAgent", "duration is not valid in onEventDuration");
        } else {
            a.a(context, str, str2, j, 1);
        }
    }

    public static void onEventDuration(Context context, String str, Map<String, String> map, long j) {
        if (j <= 0) {
            Log.a("MobclickAgent", "duration is not valid in onEventDuration");
        } else {
            a.a(context, str, map, j);
        }
    }

    public static void onEventEnd(Context context, String str) {
        a.c(context, str);
    }

    public static void onEventEnd(Context context, String str, String str2) {
        a.b(context, str, str2);
    }

    public static void onKVEventBegin(Context context, String str, Map<String, String> map, String str2) {
        a.a(context, str, map, str2);
    }

    public static void onKVEventEnd(Context context, String str, String str2) {
        a.c(context, str, str2);
    }

    public static void onPause(Context context) {
        a.a(context);
    }

    public static void onResume(Context context) {
        a.c(context);
    }

    public static void onResume(Context context, String str, String str2) {
        if (str == null || str.length() == 0) {
            Log.b("MobclickAgent", "unexpected empty appkey in onResume");
            return;
        }
        a.b = str;
        a.a = str2;
        a.c(context);
    }

    public static void openActivityDurationTrack(boolean z) {
        e.h = z;
    }

    public static void reportError(Context context, String str) {
        a.a(context, str);
    }

    public static void setAutoLocation(boolean z) {
        e.g = z;
    }

    public static void setDebugMode(boolean z) {
        Log.LOG = z;
    }

    public static void setDefaultReportPolicy(Context context, int i) {
        Log.e("MobclickAgent", "此方法不再使用，请使用在线参数配置，发送策略");
    }

    public static void setEnableEventBuffer(boolean z) {
        e.k = z;
    }

    public static void setOnlineConfigureListener(UmengOnlineConfigureListener umengOnlineConfigureListener) {
        a.c = umengOnlineConfigureListener;
    }

    public static void setOpenGLContext(GL10 gl10) {
        if (gl10 != null) {
            String[] a2 = b.a(gl10);
            if (a2.length == 2) {
                a.d = a2[0];
                a.e = a2[1];
            }
        }
    }

    public static void setSessionContinueMillis(long j) {
        e.d = j;
    }

    public static void updateOnlineConfig(Context context) {
        a.e(context);
    }

    public static void updateOnlineConfig(Context context, String str, String str2) {
        if (str == null || str.length() == 0) {
            Log.b("MobclickAgent", "unexpected empty appkey in onResume");
            return;
        }
        a.b = str;
        a.a = str2;
        a.e(context);
    }

    public void setAge(Context context, int i) {
        SharedPreferences a2 = h.a(context);
        if (i < 0 || i > 200) {
            Log.a("MobclickAgent", "not a valid age!");
        } else {
            a2.edit().putInt("age", i).commit();
        }
    }

    public void setGender(Context context, Gender gender) {
        int i = 0;
        SharedPreferences a2 = h.a(context);
        switch (b()[gender.ordinal()]) {
            case 1:
                i = 1;
                break;
            case 2:
                i = 2;
                break;
        }
        a2.edit().putInt("gender", i).commit();
    }

    public void setUserID(Context context, String str, String str2) {
        SharedPreferences a2 = h.a(context);
        if (TextUtils.isEmpty(str)) {
            Log.a("MobclickAgent", "userID is null or empty");
            return;
        }
        a2.edit().putString("user_id", str).commit();
        if (TextUtils.isEmpty(str2)) {
            Log.a("MobclickAgent", "id source is null or empty");
        } else {
            a2.edit().putString("id_source", str2).commit();
        }
    }
}
