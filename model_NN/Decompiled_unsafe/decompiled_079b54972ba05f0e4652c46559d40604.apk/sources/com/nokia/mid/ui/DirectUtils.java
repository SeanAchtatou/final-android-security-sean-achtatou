package com.nokia.mid.ui;

import android.util.Log;
import com.a.a.e.j;
import com.a.a.e.o;
import com.a.a.e.p;
import java.lang.reflect.Array;
import java.util.HashMap;
import javax.microedition.midlet.MIDlet;
import org.meteoroid.core.e;

public class DirectUtils {
    private static final HashMap<o, DirectGraphics> dY = new HashMap<>();

    private static final class a implements DirectGraphics {
        private o dZ;
        private final int[] ea = new int[2];
        private int[] eb;

        public a(o oVar) {
            this.dZ = oVar;
        }

        private void a(int i, int i2, int i3, int i4, int i5) {
            if (i5 == 0) {
                i5 = 20;
            }
            boolean z = (i5 & 64) != 0;
            if ((i5 & 16) != 0) {
                if ((i5 & 34) != 0) {
                    z = true;
                }
            } else if ((i5 & 32) != 0) {
                if ((i5 & 2) != 0) {
                    z = true;
                } else {
                    i2 -= i4;
                }
            } else if ((i5 & 2) != 0) {
                i2 -= i4 >>> 1;
            } else {
                z = true;
            }
            if ((i5 & 4) != 0) {
                if ((i5 & 9) != 0) {
                    z = true;
                }
            } else if ((i5 & 8) != 0) {
                if ((i5 & 1) != 0) {
                    z = true;
                } else {
                    i -= i3;
                }
            } else if ((i5 & 1) != 0) {
                i -= i3 >>> 1;
            } else {
                z = true;
            }
            if (z) {
                throw new IllegalArgumentException("Bad Anchor");
            }
            this.ea[0] = i;
            this.ea[1] = i2;
        }

        private static int[][] a(int[] iArr, int[] iArr2, int i) {
            int[] iArr3 = new int[(iArr.length - 1)];
            int[] iArr4 = new int[(iArr2.length - 1)];
            int[][] iArr5 = {iArr3, iArr4};
            int i2 = 0;
            for (int i3 = 0; i3 < iArr.length; i3++) {
                if (i3 != i) {
                    iArr3[i2] = iArr[i3];
                    iArr4[i2] = iArr2[i3];
                    i2++;
                }
            }
            return iArr5;
        }

        private int ab(int i) {
            switch (i) {
                case DirectGraphics.ROTATE_90:
                    return 6;
                case 180:
                    return 3;
                case DirectGraphics.ROTATE_270:
                    return 5;
                case 8192:
                case 16564:
                    return 2;
                case 8282:
                    return 7;
                case 8372:
                    return 1;
                case 8462:
                    return 4;
                case 16384:
                    return 1;
                case 16474:
                    return 4;
                case 16654:
                    return 7;
                case 24576:
                    return 3;
                default:
                    return 0;
            }
        }

        private static int[][][] b(int[] iArr, int[] iArr2, int i, int i2) {
            int length = i2 < i ? (iArr.length - i) + i2 + 1 : (i2 - i) + 1;
            int length2 = (iArr.length - length) + 2;
            int[][] iArr3 = (int[][]) Array.newInstance(Integer.TYPE, 2, length);
            int[][] iArr4 = (int[][]) Array.newInstance(Integer.TYPE, 2, length2);
            for (int i3 = 0; i3 < length; i3++) {
                int length3 = (i + i3) % iArr.length;
                iArr3[0][i3] = iArr[length3];
                iArr3[1][i3] = iArr2[length3];
            }
            for (int i4 = 0; i4 < length2; i4++) {
                int length4 = (i2 + i4) % iArr.length;
                iArr4[0][i4] = iArr[length4];
                iArr4[1][i4] = iArr2[length4];
            }
            return new int[][][]{iArr3, iArr4};
        }

        private static boolean c(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            if (i < Math.min(i3, Math.min(i5, i7)) || i > Math.max(i3, Math.max(i5, i7)) || i2 < Math.min(i4, Math.min(i6, i8)) || i2 > Math.max(i4, Math.max(i6, i8))) {
                return false;
            }
            return d(i, i2, i3, i4, i5, i6, i7, i8) && d(i, i2, i5, i6, i3, i4, i7, i8) && d(i, i2, i7, i8, i3, i4, i5, i6);
        }

        private static boolean d(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            return ((long) (((i - i5) * (i8 - i6)) - ((i7 - i5) * (i2 - i6)))) * ((long) (((i3 - i5) * (i8 - i6)) - ((i7 - i5) * (i4 - i6)))) >= 0;
        }

        static int e(int[] iArr) {
            int i = 0;
            int i2 = iArr[0];
            for (int i3 = 1; i3 < iArr.length; i3++) {
                if (iArr[i3] < i2) {
                    i2 = iArr[i3];
                    i = i3;
                }
            }
            return i;
        }

        public void a(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
            this.dZ.g(i, i2, i3, i4);
            this.dZ.g(i3, i4, i5, i6);
            this.dZ.g(i5, i6, i, i2);
        }

        public void a(p pVar, int i, int i2, int i3, int i4) {
            this.dZ.a(pVar, 0, 0, pVar.getWidth(), pVar.getHeight(), ab(i4), i, i2, i3);
        }

        public void a(byte[] bArr, byte[] bArr2, int i, int i2, int i3, int i4, int i5, int i6, int i7) {
            Log.d("DirectGraphics", "getPixels byte[]");
        }

        public void a(byte[] bArr, byte[] bArr2, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            Log.d("DirectGraphics", "drawPixels byte[]");
        }

        public void a(int[] iArr, int i, int i2, int i3, int i4, int i5, int i6, int i7) {
            if (iArr == null) {
                throw new NullPointerException("array_int getPixels");
            } else if (i < 0 || iArr.length < i5 * i6) {
                throw new ArrayIndexOutOfBoundsException("array_int getPixels");
            } else {
                switch (i7) {
                    case DirectGraphics.TYPE_INT_888_RGB:
                        for (int i8 = i3; i8 < i3 + i5; i8++) {
                            for (int i9 = i4; i9 < i4 + i6; i9++) {
                                iArr[i + i8 + (i9 * i2)] = iArr[i + i8 + (i9 * i2)] & 16777215;
                            }
                        }
                        return;
                    case DirectGraphics.TYPE_INT_8888_ARGB:
                        return;
                    default:
                        throw new IllegalArgumentException("array_int getPixels");
                }
            }
        }

        public void a(int[] iArr, int i, int[] iArr2, int i2, int i3, int i4) {
            if (iArr == null || iArr == null) {
                throw new NullPointerException("drawPolygon");
            }
            int length = iArr.length - 1;
            int length2 = iArr2.length - 1;
            this.dZ.setColor(i4);
            for (int i5 = 0; i5 < length; i5++) {
                if (i5 + i >= length || i5 + i2 >= length2) {
                    throw new ArrayIndexOutOfBoundsException("drawPolygon");
                }
                this.dZ.g(iArr[i5 + i], iArr2[i5 + i2], iArr[i5 + i + 1], iArr2[i5 + i2 + 1]);
            }
            this.dZ.g(iArr[length], iArr2[length], iArr[0], iArr2[0]);
        }

        public void a(int[] iArr, boolean z, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            this.dZ.a(iArr, i, i2, i3, i4, i5, i6, z);
        }

        public void a(short[] sArr, int i, int i2, int i3, int i4, int i5, int i6, int i7) {
            Log.d("DirectGraphics", "getPixels short[]");
        }

        public void a(short[] sArr, boolean z, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            boolean z2;
            if (sArr == null) {
                throw new NullPointerException("drawPixels short");
            } else if (i < 0 || sArr.length < i5 * i6) {
                throw new ArrayIndexOutOfBoundsException("drawPixels short");
            } else {
                switch (i8) {
                    case DirectGraphics.TYPE_USHORT_444_RGB:
                    case DirectGraphics.TYPE_USHORT_555_RGB:
                    case DirectGraphics.TYPE_USHORT_565_RGB:
                        z2 = false;
                        break;
                    case DirectGraphics.TYPE_USHORT_1555_ARGB:
                    case DirectGraphics.TYPE_USHORT_4444_ARGB:
                        z2 = true;
                        break;
                    default:
                        throw new IllegalArgumentException("Unknown short pixel format in drawPixels.");
                }
                if (this.eb == null || this.eb.length != sArr.length) {
                    this.eb = new int[sArr.length];
                }
                for (int i9 = 0; i9 < sArr.length; i9++) {
                    switch (i8) {
                        case DirectGraphics.TYPE_USHORT_444_RGB:
                            this.eb[i9] = (((sArr[i9] & 3840) << 12) & 16711680) | (((sArr[i9] & 240) << 8) & 65280) | (((sArr[i9] & 15) << 4) & 255);
                            break;
                        case DirectGraphics.TYPE_USHORT_555_RGB:
                            this.eb[i9] = (((sArr[i9] & 31744) << 9) & 16711680) | (((sArr[i9] & 992) << 6) & 65280) | (((sArr[i9] & 31) << 3) & 255);
                            break;
                        case DirectGraphics.TYPE_USHORT_565_RGB:
                            this.eb[i9] = (((sArr[i9] & 63488) << 8) & 16711680) | (((sArr[i9] & 2016) << 5) & 65280) | (((sArr[i9] & 31) << 3) & 255);
                            break;
                        case DirectGraphics.TYPE_USHORT_1555_ARGB:
                            this.eb[i9] = (((sArr[i9] & 32768) << 16) & -16777216) | (((sArr[i9] & 31744) << 9) & 16711680) | (((sArr[i9] & 992) << 6) & 65280) | (((sArr[i9] & 31) << 3) & 255);
                            break;
                        case DirectGraphics.TYPE_USHORT_4444_ARGB:
                            this.eb[i9] = (((sArr[i9] & 61440) << 16) & -16777216) | (((sArr[i9] & 3840) << 12) & 16711680) | (((sArr[i9] & 240) << 8) & 65280) | (((sArr[i9] & 15) << 4) & 255);
                            break;
                        default:
                            throw new IllegalArgumentException("Unknown short pixel format in drawPixels.");
                    }
                }
                if (i2 < i5) {
                    i2 = i5;
                }
                a(i3, i4, i5, i6, ab(i7));
                this.dZ.a(this.eb, i, i2, this.ea[0], this.ea[1], i5, i6, z2);
            }
        }

        public void aa(int i) {
            this.dZ.setColor(i);
        }

        public int ao() {
            return DirectGraphics.TYPE_INT_8888_ARGB;
        }

        public int ap() {
            return j.a((MIDlet) null).bV();
        }

        public void b(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
            int color = this.dZ.getColor();
            this.dZ.setColor(i7);
            this.dZ.f(i, i2, i3, i4, i5, i6);
            this.dZ.setColor(color);
        }

        public void b(int[] iArr, int i, int[] iArr2, int i2, int i3, int i4) {
            boolean z;
            int i5;
            while (iArr.length > 2) {
                int e = e(iArr);
                int length = (e + 1) % iArr.length;
                int length2 = e > 0 ? e - 1 : iArr.length - 1;
                int i6 = -1;
                boolean z2 = false;
                if (iArr.length > 3) {
                    int i7 = 0;
                    while (true) {
                        int i8 = i7;
                        z = z2;
                        i5 = i6;
                        if (i8 >= iArr.length) {
                            break;
                        }
                        if (i8 == e || i8 == length || i8 == length2 || !c(iArr[i8], iArr2[i8], iArr[e], iArr2[e], iArr[length], iArr2[length], iArr[length2], iArr2[length2]) || (z && iArr[i8] >= iArr[i5])) {
                            z2 = z;
                            i6 = i5;
                        } else {
                            z2 = true;
                            i6 = i8;
                        }
                        i7 = i8 + 1;
                    }
                } else {
                    z = false;
                    i5 = -1;
                }
                if (!z) {
                    this.dZ.setColor(i4);
                    this.dZ.f(iArr[e], iArr2[e], iArr[length], iArr2[length], iArr[length2], iArr2[length2]);
                    int[][] a = a(iArr, iArr2, e);
                    iArr = a[0];
                    iArr2 = a[1];
                } else {
                    int[][][] b = b(iArr, iArr2, e, i5);
                    int[][] iArr3 = b[0];
                    int[][] iArr4 = b[1];
                    b(iArr3[0], 0, iArr3[1], 0, iArr3[0].length, i4);
                    b(iArr4[0], 0, iArr4[1], 0, iArr4[0].length, i4);
                    return;
                }
            }
        }
    }

    public static p e(byte[] bArr, int i, int i2) {
        return p.e(bArr, i, i2);
    }

    public static p h(int i, int i2, int i3) {
        p pVar = new p(e.a(i, i2, true, i3));
        pVar.hs = true;
        return pVar;
    }

    public static DirectGraphics i(o oVar) {
        if (dY.containsKey(oVar)) {
            return dY.get(oVar);
        }
        a aVar = new a(oVar);
        dY.put(oVar, aVar);
        Log.d("DirectUtils", "Create a new Nokia DirectUtils.");
        return aVar;
    }
}
