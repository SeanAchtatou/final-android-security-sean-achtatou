package com.nokia.mid.sound;

import javax.microedition.enhance.MIDPHelper;
import javax.microedition.media.Manager;
import javax.microedition.media.MediaException;
import javax.microedition.media.Player;

public class Sound {
    public static final int FORMAT_TONE = 1;
    public static final int FORMAT_WAV = 5;
    public static final int SOUND_PLAYING = 0;
    public static final int SOUND_STOPPED = 1;
    public static final int SOUND_UNINITIALIZED = 3;
    private int dV = 100;
    private SoundListener dW;
    Player dX;
    private int state = 3;

    public Sound(int i, long j) {
    }

    public Sound(byte[] bArr, int i) {
        b(bArr, i);
    }

    public static int Z(int i) {
        return 1;
    }

    public static int[] am() {
        return new int[]{1, 5};
    }

    public void X(int i) {
        if (i == 0) {
            this.dX.ba(-1);
        } else {
            this.dX.ba(i);
        }
        try {
            this.dX.start();
        } catch (MediaException e) {
            e.printStackTrace();
        }
        this.state = 0;
        if (this.dW != null) {
            this.dW.a(this, this.state);
        }
    }

    public void Y(int i) {
        this.dV = i;
    }

    public void a(SoundListener soundListener) {
        this.dW = soundListener;
    }

    public int al() {
        return this.dV;
    }

    public void b(byte[] bArr, int i) {
        try {
            if (this.dX != null) {
                this.dX.stop();
            }
            this.dX = null;
            this.dX = Manager.a(bArr.length < 3 ? MIDPHelper.b(getClass(), "/short.mp3") : MIDPHelper.b(getClass(), "/long.mp3"), "audio/mpeg");
            this.dX.dD();
            this.dX.dE();
            this.state = 1;
            if (this.dW != null) {
                this.dW.a(this, this.state);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getState() {
        return this.state;
    }

    public void release() {
        try {
            if (this.dX != null) {
                this.dX.stop();
            }
            this.dX = null;
            this.state = 3;
            if (this.dW != null) {
                this.dW.a(this, this.state);
            }
        } catch (MediaException e) {
            e.printStackTrace();
        }
    }

    public void resume() {
        try {
            this.dX.stop();
            this.dX.start();
        } catch (MediaException e) {
            e.printStackTrace();
        }
        this.state = 0;
        if (this.dW != null) {
            this.dW.a(this, this.state);
        }
    }

    public void stop() {
        try {
            if (this.dX != null) {
                this.dX.stop();
            }
            this.state = 1;
            if (this.dW != null) {
                this.dW.a(this, this.state);
            }
        } catch (MediaException e) {
            e.printStackTrace();
        }
    }
}
