package com.sms.purchasesdk.view;

import android.view.MotionEvent;
import android.view.View;

class b implements View.OnTouchListener {
    final /* synthetic */ a eh;

    b(a aVar) {
        this.eh = aVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            view.setBackgroundDrawable(this.eh.es);
            return false;
        }
        view.setBackgroundDrawable(this.eh.er);
        return false;
    }
}
