package com.sms.purchasesdk.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import mm.sms.purchasesdk.e.d;
import mm.sms.purchasesdk.e.r;

public class c extends l {
    private ImageView ds = new ImageView(this.mContext);
    private View.OnTouchListener ei;

    public c(d dVar, Context context) {
        super(dVar, context);
        this.ds.setClickable(true);
        this.ei = new d(this);
    }

    public View a() {
        this.ds = new ImageView(this.mContext);
        this.ds.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(this.f3a.getWidth(), this.f3a.getHeight());
        layoutParams.gravity = f(this.f3a.m18a());
        layoutParams.setMargins(this.f3a.g(), this.f3a.i(), this.f3a.h(), this.f3a.j());
        if (this.f3a.m23g() != null) {
            this.ds.setImageBitmap(r.c(this.mContext, this.f3a.m23g()));
        }
        this.ds.setLayoutParams(layoutParams);
        if (this.f3a.m25i() != null) {
            this.ds.setBackgroundDrawable(new BitmapDrawable(r.c(this.mContext, this.f3a.m25i())));
        }
        if (this.f3a.m14a().booleanValue()) {
            this.ds.setOnTouchListener(this.ei);
            this.ds.setClickable(true);
        }
        return this.ds;
    }

    public Bitmap o(Context context, String str) {
        return a(r.b, r.b, r.c(context, str));
    }
}
