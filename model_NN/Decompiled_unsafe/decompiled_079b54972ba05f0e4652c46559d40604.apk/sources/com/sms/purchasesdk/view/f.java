package com.sms.purchasesdk.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.LinearLayout;
import mm.sms.purchasesdk.e.d;
import mm.sms.purchasesdk.e.r;

public class f extends e {
    private LinearLayout ek = new LinearLayout(this.mContext);

    public f(d dVar, Context context) {
        super(dVar, context);
    }

    public View a() {
        if (this.ek == null) {
            this.ek = new LinearLayout(this.mContext);
        }
        this.f3a.getWidth();
        this.f3a.getHeight();
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(this.f3a.getWidth(), this.f3a.getHeight());
        this.ek.setPadding(this.f3a.k(), this.f3a.getTopPadding(), this.f3a.l(), this.f3a.getBottomPadding());
        layoutParams.setMargins(this.f3a.g(), this.f3a.i(), this.f3a.h(), this.f3a.j());
        this.ek.setOrientation(this.f3a.getOrientation());
        layoutParams.gravity = f(this.f3a.m18a());
        if (this.f3a.f() != 0) {
            layoutParams.weight = (float) this.f3a.f();
        }
        this.ek.setLayoutParams(layoutParams);
        if (this.er != null) {
            this.ek.setBackgroundDrawable(this.er);
        }
        return this.ek;
    }

    public Bitmap o(Context context, String str) {
        return a(r.b, r.b, r.c(context, str));
    }
}
