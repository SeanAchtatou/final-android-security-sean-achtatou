package com.sms.purchasesdk.view;

import android.view.MotionEvent;
import android.view.View;

class d implements View.OnTouchListener {
    final /* synthetic */ c ej;

    d(c cVar) {
        this.ej = cVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            if (this.ej.es == null) {
                return false;
            }
            view.setBackgroundDrawable(this.ej.es);
            return false;
        } else if (motionEvent.getAction() != 1 || this.ej.er == null) {
            return false;
        } else {
            view.setBackgroundDrawable(this.ej.er);
            return false;
        }
    }
}
