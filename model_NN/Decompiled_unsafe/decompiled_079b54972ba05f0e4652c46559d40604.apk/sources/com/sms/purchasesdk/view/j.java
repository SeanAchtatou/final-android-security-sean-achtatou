package com.sms.purchasesdk.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import mm.sms.purchasesdk.e.d;
import mm.sms.purchasesdk.e.r;

public class j extends e {
    private ScrollView ep;

    public j(d dVar, Context context) {
        super(dVar, context);
        this.ep = new ScrollView(context);
    }

    public View a() {
        this.ep.setLayoutParams(new LinearLayout.LayoutParams(this.f3a.f(), this.f3a.getHeight()));
        this.ep.setFillViewport(true);
        if (this.er != null) {
            this.ep.setBackgroundDrawable(this.er);
        }
        return this.ep;
    }

    public Bitmap o(Context context, String str) {
        return r.c(context, str);
    }
}
