package com.sms.purchasesdk.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import mm.sms.purchasesdk.e.d;
import mm.sms.purchasesdk.e.r;

public class a extends l {
    private Button eg = new Button(this.mContext);

    public a(d dVar, Context context) {
        super(dVar, context);
    }

    public View a() {
        this.eg.setGravity(17);
        this.eg.setOnTouchListener(new b(this));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(this.f3a.getWidth(), this.f3a.getHeight());
        layoutParams.gravity = f(this.f3a.m18a());
        layoutParams.weight = (float) this.f3a.f();
        layoutParams.setMargins(this.e, this.g, this.f, this.h);
        this.eg.setGravity(f(this.f3a.m18a()));
        this.eg.setPadding(this.a, this.c, this.b, this.d);
        this.eg.setText(this.f2a);
        this.eg.setTextSize(1, (float) this.i);
        this.eg.setTextColor(-1);
        this.eg.setLayoutParams(layoutParams);
        this.eg.setBackgroundDrawable(this.er);
        return this.eg;
    }

    public Bitmap o(Context context, String str) {
        return a(r.b, r.b, r.c(context, str));
    }
}
