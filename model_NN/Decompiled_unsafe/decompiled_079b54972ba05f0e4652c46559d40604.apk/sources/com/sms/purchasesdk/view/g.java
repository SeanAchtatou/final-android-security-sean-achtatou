package com.sms.purchasesdk.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import mm.sms.purchasesdk.e.d;
import mm.sms.purchasesdk.e.e;
import mm.sms.purchasesdk.e.r;

public class g extends l {
    private ProductItemView el = new ProductItemView(this.mContext);
    private e em;

    public g(d dVar, Context context) {
        super(dVar, context);
    }

    public View a() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(this.f3a.getWidth(), this.f3a.getHeight());
        layoutParams.setMargins(this.f3a.g(), this.f3a.i(), this.f3a.h(), this.f3a.j());
        layoutParams.gravity = 17;
        if (f(this.f3a.m18a()) != 0) {
            this.el.setGravity(f(this.f3a.m18a()));
        }
        this.el.setTextColor(this.f3a.m());
        this.el.setLayoutParams(layoutParams);
        this.el.setTextSize((float) this.f3a.getTextSize());
        this.el.setSingleLine(this.f3a.b().booleanValue());
        this.el.setSingleLine();
        this.el.setEllipsize(TextUtils.TruncateAt.valueOf("MARQUEE"));
        this.el.setMarqueeRepeatLimit(-1);
        this.el.setTextColor(this.f3a.m());
        if (this.em != null) {
            this.el.setText(this.em.mValue);
        } else {
            this.el.setText(this.f3a.getText());
        }
        if (this.f3a.m25i() != null) {
            this.el.setBackgroundDrawable(new BitmapDrawable(r.c(this.mContext, this.f3a.m25i())));
        }
        return this.el;
    }

    public void a(e eVar) {
        this.em = eVar;
    }

    public Bitmap o(Context context, String str) {
        return a(r.b, r.b, r.c(context, str));
    }
}
