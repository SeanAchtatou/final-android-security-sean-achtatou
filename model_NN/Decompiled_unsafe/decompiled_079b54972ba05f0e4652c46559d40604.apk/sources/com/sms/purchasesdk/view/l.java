package com.sms.purchasesdk.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.NinePatch;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import mm.sms.purchasesdk.e.d;
import mm.sms.purchasesdk.e.r;

public abstract class l {
    public int a;

    /* renamed from: a  reason: collision with other field name */
    public String f2a;

    /* renamed from: a  reason: collision with other field name */
    public d f3a;
    public int b;
    public int c;
    public int d;
    public int e;
    public Drawable er;
    public Drawable es;
    public Drawable et;
    public int f;
    public int g;
    public int h;
    public int i;
    public int j = -16777216;
    public Context mContext;

    public l(d dVar, Context context) {
        this.f3a = dVar;
        this.mContext = context;
        a(dVar, context);
    }

    private void b(d dVar, Context context) {
        String i2 = dVar.m25i();
        String e2 = dVar.e();
        String f2 = dVar.m22f();
        if (i2 != null && i2.trim().length() > 0) {
            if (i2.endsWith(".9.png")) {
                this.er = p(context, i2);
            } else {
                this.er = new BitmapDrawable(o(context, i2));
            }
        }
        if (e2 != null && e2.trim().length() > 0) {
            if (e2.endsWith(".9.png")) {
                this.es = p(context, e2);
            } else {
                this.es = new BitmapDrawable(o(context, e2));
            }
        }
        if (f2 != null && f2.trim().length() > 0) {
            if (e2.endsWith(".9.png")) {
                this.et = p(context, f2);
            } else {
                this.et = new BitmapDrawable(o(context, f2));
            }
        }
    }

    private void i(d dVar) {
        this.d = dVar.getBottomPadding();
        this.a = dVar.k();
        this.b = dVar.l();
        this.c = dVar.getTopPadding();
    }

    private void j(d dVar) {
        this.g = dVar.i();
        this.h = dVar.j();
        this.e = dVar.g();
        this.f = dVar.h();
    }

    private void k(d dVar) {
        this.f2a = dVar.getText();
        this.j = dVar.m();
        this.i = dVar.getTextSize();
    }

    private Drawable p(Context context, String str) {
        Bitmap b2 = r.b(context, str);
        if (b2 == null) {
            return null;
        }
        byte[] ninePatchChunk = b2.getNinePatchChunk();
        NinePatch.isNinePatchChunk(ninePatchChunk);
        return new NinePatchDrawable(b2, ninePatchChunk, new Rect(), null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public Bitmap a(float f2, float f3, Bitmap bitmap) {
        Matrix matrix = new Matrix();
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        if (height > 1 && width > 1) {
            matrix.postScale(f2, f3);
            return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        } else if (height <= 1) {
            matrix.postScale(f2, 1.0f);
            return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        } else if (width > 1) {
            return bitmap;
        } else {
            matrix.postScale(1.0f, f3);
            return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        }
    }

    public void a(d dVar, Context context) {
        b(dVar, context);
        i(dVar);
        j(dVar);
        k(dVar);
    }

    public int f(int[] iArr) {
        int i2 = iArr[0];
        for (int i3 = 1; i3 < iArr.length; i3++) {
            i2 |= iArr[i3];
        }
        return i2;
    }

    public abstract Bitmap o(Context context, String str);
}
