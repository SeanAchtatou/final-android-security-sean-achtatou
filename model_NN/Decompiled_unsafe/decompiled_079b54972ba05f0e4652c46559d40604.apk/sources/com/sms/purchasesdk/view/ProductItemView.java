package com.sms.purchasesdk.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

public class ProductItemView extends TextView {
    public ProductItemView(Context context) {
        super(context);
    }

    public ProductItemView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ProductItemView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public boolean isFocused() {
        return true;
    }
}
