package com.sms.purchasesdk.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ProgressBar;
import mm.sms.purchasesdk.e.d;
import mm.sms.purchasesdk.e.r;

public class h extends l {
    private ProgressBar en;

    public h(d dVar, Context context) {
        super(dVar, context);
        this.en = new ProgressBar(context);
    }

    public View a() {
        return this.en;
    }

    public Bitmap o(Context context, String str) {
        return a(r.b, r.b, r.c(context, str));
    }
}
