package com.sms.purchasesdk.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import mm.sms.purchasesdk.e.d;
import mm.sms.purchasesdk.e.r;

public class k extends l {
    private TextView eq = new ProductItemView(this.mContext);

    public k(d dVar, Context context) {
        super(dVar, context);
    }

    public View a() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(this.f3a.getWidth(), this.f3a.getHeight());
        layoutParams.setMargins(this.f3a.g(), this.f3a.i(), this.f3a.h(), this.f3a.j());
        layoutParams.gravity = 17;
        layoutParams.setMargins(this.e, this.g, this.f, this.h);
        if (f(this.f3a.m18a()) != 0) {
            this.eq.setGravity(f(this.f3a.m18a()));
        }
        this.eq.setPadding(this.a, this.c, this.b, this.d);
        this.eq.setLayoutParams(layoutParams);
        this.eq.setTextSize((float) this.i);
        this.eq.setTextColor(this.j);
        this.eq.setText(this.f2a);
        if (this.er != null) {
            this.eq.setBackgroundDrawable(this.er);
        }
        return this.eq;
    }

    public Bitmap o(Context context, String str) {
        return a(r.b, r.b, r.c(context, str));
    }
}
