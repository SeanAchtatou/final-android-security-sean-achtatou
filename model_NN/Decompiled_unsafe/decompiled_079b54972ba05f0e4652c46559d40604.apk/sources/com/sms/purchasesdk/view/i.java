package com.sms.purchasesdk.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.RelativeLayout;
import mm.sms.purchasesdk.e.d;
import mm.sms.purchasesdk.e.r;

public class i extends e {
    private RelativeLayout eo;

    public i(d dVar, Context context) {
        super(dVar, context);
        this.eo = new RelativeLayout(context);
    }

    public View a() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(this.f3a.getWidth(), this.f3a.getHeight());
        this.eo.setPadding(this.a, this.c, this.b, this.d);
        layoutParams.setMargins(this.e, this.g, this.f, this.h);
        this.eo.setLayoutParams(layoutParams);
        if (this.er != null) {
            this.eo.setBackgroundDrawable(this.er);
        }
        return this.eo;
    }

    public Bitmap o(Context context, String str) {
        return a(r.b, r.b, r.c(context, str));
    }
}
