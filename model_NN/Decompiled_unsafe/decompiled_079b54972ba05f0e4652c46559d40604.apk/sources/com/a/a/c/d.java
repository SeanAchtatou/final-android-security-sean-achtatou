package com.a.a.c;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import org.meteoroid.core.c;
import org.meteoroid.plugin.device.MIDPDevice;

public class d {
    public static final int READ = 1;
    public static final int READ_WRITE = 3;
    public static final int WRITE = 2;

    public static b H(String str) {
        return a(str, 3, true);
    }

    public static DataInputStream I(String str) {
        return ((j) H(str)).aQ();
    }

    public static DataOutputStream J(String str) {
        return ((k) H(str)).aS();
    }

    public static InputStream K(String str) {
        return ((j) H(str)).aR();
    }

    public static OutputStream L(String str) {
        return ((k) H(str)).aT();
    }

    public static b a(String str, int i, boolean z) {
        return ((MIDPDevice) c.mN).a(str, i, z);
    }

    public static b f(String str, int i) {
        return a(str, i, true);
    }
}
