package com.a.a.c;

import java.io.DataInput;
import java.io.DataOutput;

public interface f extends DataInput, DataOutput {
    void a(f fVar);

    String getAddress();

    byte[] getData();

    int getLength();

    int getOffset();

    void reset();

    void setAddress(String str);

    void setData(byte[] bArr, int i, int i2);

    void setLength(int i);
}
