package com.a.a.s;

import java.util.Vector;

public abstract class c<T> {
    private int rT = 0;
    private T rU;
    private Vector<T> rV = new Vector<>();
    private int rW;
    private int rX;

    public c(int i, int i2) {
        this.rW = i;
        this.rX = i2;
    }

    public T getObject() {
        if (this.rV.size() > this.rW) {
            this.rU = this.rV.firstElement();
        } else if (this.rT <= this.rX) {
            this.rU = jw();
            this.rT++;
        } else {
            synchronized (this) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                }
                this.rU = this.rV.firstElement();
            }
        }
        return this.rU;
    }

    public void j(T t) {
        this.rV.addElement(t);
        synchronized (this) {
            notifyAll();
        }
    }

    public abstract T jw();
}
