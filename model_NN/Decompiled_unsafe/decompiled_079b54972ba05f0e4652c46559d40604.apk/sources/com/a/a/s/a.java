package com.a.a.s;

import com.a.a.c.p;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import javax.microedition.media.control.ToneControl;

public class a {
    static final /* synthetic */ boolean $assertionsDisabled = (!a.class.desiredAssertionStatus() ? true : $assertionsDisabled);
    public static final int DECODE = 0;
    public static final int DONT_GUNZIP = 4;
    public static final int DO_BREAK_LINES = 8;
    public static final int ENCODE = 1;
    private static final byte EQUALS_SIGN = 61;
    private static final byte EQUALS_SIGN_ENC = -1;
    public static final int GZIP = 2;
    private static final int MAX_LINE_LENGTH = 76;
    private static final byte NEW_LINE = 10;
    public static final int NO_OPTIONS = 0;
    public static final int ORDERED = 32;
    private static final String PREFERRED_ENCODING = "US-ASCII";
    public static final int URL_SAFE = 16;
    private static final byte WHITE_SPACE_ENC = -5;
    private static final byte[] _ORDERED_ALPHABET = {45, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 95, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122};
    private static final byte[] _ORDERED_DECODABET = {-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 0, -9, -9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, -9, -9, -9, -1, -9, -9, -9, com.a.a.k.b.BROKEN_CHAIN, com.a.a.k.b.ROOT_CA_EXPIRED, com.a.a.k.b.UNSUPPORTED_PUBLIC_KEY_TYPE, com.a.a.k.b.VERIFICATION_FAILED, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, -9, -9, -9, -9, 37, -9, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, ToneControl.C4, EQUALS_SIGN, 62, 63, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9};
    private static final byte[] _STANDARD_ALPHABET = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] _STANDARD_DECODABET = {-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 62, -9, -9, -9, 63, 52, 53, 54, 55, 56, 57, 58, 59, ToneControl.C4, EQUALS_SIGN, -9, -9, -9, -1, -9, -9, -9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, com.a.a.k.b.BROKEN_CHAIN, com.a.a.k.b.ROOT_CA_EXPIRED, com.a.a.k.b.UNSUPPORTED_PUBLIC_KEY_TYPE, com.a.a.k.b.VERIFICATION_FAILED, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -9, -9, -9, -9, -9, -9, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9};
    private static final byte[] _URL_SAFE_ALPHABET = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
    private static final byte[] _URL_SAFE_DECODABET = {-9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -5, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -5, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, 62, -9, -9, 52, 53, 54, 55, 56, 57, 58, 59, ToneControl.C4, EQUALS_SIGN, -9, -9, -9, -1, -9, -9, -9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, com.a.a.k.b.BROKEN_CHAIN, com.a.a.k.b.ROOT_CA_EXPIRED, com.a.a.k.b.UNSUPPORTED_PUBLIC_KEY_TYPE, com.a.a.k.b.VERIFICATION_FAILED, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -9, -9, -9, -9, 63, -9, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9, -9};

    /* renamed from: com.a.a.s.a$a  reason: collision with other inner class name */
    public static class C0000a extends FilterInputStream {
        private boolean breakLines;
        private byte[] buffer;
        private int bufferLength;
        private byte[] decodabet;
        private boolean encode;
        private int lineLength;
        private int numSigBytes;
        private int options;
        private int position;

        public C0000a(InputStream inputStream) {
            this(inputStream, 0);
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public C0000a(InputStream inputStream, int i) {
            super(inputStream);
            boolean z = true;
            this.options = i;
            this.breakLines = (i & 8) > 0;
            this.encode = (i & 1) <= 0 ? false : z;
            this.bufferLength = this.encode ? 4 : 3;
            this.buffer = new byte[this.bufferLength];
            this.position = -1;
            this.lineLength = 0;
            this.decodabet = a.getDecodabet(i);
        }

        public int read() {
            int read;
            if (this.position < 0) {
                if (this.encode) {
                    byte[] bArr = new byte[3];
                    int i = 0;
                    int i2 = 0;
                    while (i < 3) {
                        int read2 = this.in.read();
                        if (read2 < 0) {
                            break;
                        }
                        bArr[i] = (byte) read2;
                        i++;
                        i2++;
                    }
                    if (i2 <= 0) {
                        return -1;
                    }
                    byte[] unused = a.encode3to4(bArr, 0, i2, this.buffer, 0, this.options);
                    this.position = 0;
                    this.numSigBytes = 4;
                } else {
                    byte[] bArr2 = new byte[4];
                    int i3 = 0;
                    while (i3 < 4) {
                        do {
                            read = this.in.read();
                            if (read < 0) {
                                break;
                            }
                        } while (this.decodabet[read & 127] <= -5);
                        if (read < 0) {
                            break;
                        }
                        bArr2[i3] = (byte) read;
                        i3++;
                    }
                    if (i3 == 4) {
                        this.numSigBytes = a.decode4to3(bArr2, 0, this.buffer, 0, this.options);
                        this.position = 0;
                    } else if (i3 == 0) {
                        return -1;
                    } else {
                        throw new IOException("Improperly padded Base64 input.");
                    }
                }
            }
            if (this.position < 0) {
                throw new IOException("Error in Base64 code reading stream.");
            } else if (this.position >= this.numSigBytes) {
                return -1;
            } else {
                if (!this.encode || !this.breakLines || this.lineLength < 76) {
                    this.lineLength++;
                    byte[] bArr3 = this.buffer;
                    int i4 = this.position;
                    this.position = i4 + 1;
                    byte b = bArr3[i4];
                    if (this.position >= this.bufferLength) {
                        this.position = -1;
                    }
                    return b & 255;
                }
                this.lineLength = 0;
                return 10;
            }
        }

        public int read(byte[] bArr, int i, int i2) {
            int i3 = 0;
            while (i3 < i2) {
                int read = read();
                if (read >= 0) {
                    bArr[i + i3] = (byte) read;
                    i3++;
                } else if (i3 == 0) {
                    return -1;
                } else {
                    return i3;
                }
            }
            return i3;
        }
    }

    public static class b extends FilterOutputStream {
        private byte[] b4;
        private boolean breakLines;
        private byte[] buffer;
        private int bufferLength;
        private byte[] decodabet;
        private boolean encode;
        private int lineLength;
        private int options;
        private int position;
        private boolean suspendEncoding;

        public b(OutputStream outputStream) {
            this(outputStream, 1);
        }

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(OutputStream outputStream, int i) {
            super(outputStream);
            boolean z = true;
            this.breakLines = (i & 8) != 0;
            this.encode = (i & 1) == 0 ? false : z;
            this.bufferLength = this.encode ? 3 : 4;
            this.buffer = new byte[this.bufferLength];
            this.position = 0;
            this.lineLength = 0;
            this.suspendEncoding = a.$assertionsDisabled;
            this.b4 = new byte[4];
            this.options = i;
            this.decodabet = a.getDecodabet(i);
        }

        public void close() {
            flushBase64();
            super.close();
            this.buffer = null;
            this.out = null;
        }

        public void flushBase64() {
            if (this.position <= 0) {
                return;
            }
            if (this.encode) {
                this.out.write(a.encode3to4(this.b4, this.buffer, this.position, this.options));
                this.position = 0;
                return;
            }
            throw new IOException("Base64 input not properly padded.");
        }

        public void resumeEncoding() {
            this.suspendEncoding = a.$assertionsDisabled;
        }

        public void suspendEncoding() {
            flushBase64();
            this.suspendEncoding = true;
        }

        public void write(int i) {
            if (this.suspendEncoding) {
                this.out.write(i);
            } else if (this.encode) {
                byte[] bArr = this.buffer;
                int i2 = this.position;
                this.position = i2 + 1;
                bArr[i2] = (byte) i;
                if (this.position >= this.bufferLength) {
                    this.out.write(a.encode3to4(this.b4, this.buffer, this.bufferLength, this.options));
                    this.lineLength += 4;
                    if (this.breakLines && this.lineLength >= 76) {
                        this.out.write(10);
                        this.lineLength = 0;
                    }
                    this.position = 0;
                }
            } else if (this.decodabet[i & 127] > -5) {
                byte[] bArr2 = this.buffer;
                int i3 = this.position;
                this.position = i3 + 1;
                bArr2[i3] = (byte) i;
                if (this.position >= this.bufferLength) {
                    this.out.write(this.b4, 0, a.decode4to3(this.buffer, 0, this.b4, 0, this.options));
                    this.position = 0;
                }
            } else if (this.decodabet[i & 127] != -5) {
                throw new IOException("Invalid character in Base64 data.");
            }
        }

        public void write(byte[] bArr, int i, int i2) {
            if (this.suspendEncoding) {
                this.out.write(bArr, i, i2);
                return;
            }
            for (int i3 = 0; i3 < i2; i3++) {
                write(bArr[i + i3]);
            }
        }
    }

    private a() {
    }

    public static byte[] decode(String str) {
        return decode(str, 0);
    }

    public static byte[] decode(String str, int i) {
        byte[] bytes;
        ByteArrayOutputStream byteArrayOutputStream;
        ByteArrayInputStream byteArrayInputStream;
        ByteArrayInputStream byteArrayInputStream2;
        GZIPInputStream gZIPInputStream = null;
        if (str == null) {
            throw new NullPointerException("Input string was null.");
        }
        try {
            bytes = str.getBytes("US-ASCII");
        } catch (UnsupportedEncodingException e) {
            bytes = str.getBytes();
        }
        byte[] decode = decode(bytes, 0, bytes.length, i);
        boolean z = (i & 4) != 0;
        if (decode != null && decode.length >= 4 && !z && 35615 == ((decode[0] & 255) | ((decode[1] << 8) & p.DELAY))) {
            byte[] bArr = new byte[2048];
            try {
                byteArrayOutputStream = new ByteArrayOutputStream();
                try {
                    byteArrayInputStream2 = new ByteArrayInputStream(decode);
                    try {
                        GZIPInputStream gZIPInputStream2 = new GZIPInputStream(byteArrayInputStream2);
                        while (true) {
                            try {
                                int read = gZIPInputStream2.read(bArr);
                                if (read < 0) {
                                    break;
                                }
                                byteArrayOutputStream.write(bArr, 0, read);
                            } catch (IOException e2) {
                                e = e2;
                                gZIPInputStream = gZIPInputStream2;
                                byteArrayInputStream = byteArrayInputStream2;
                            } catch (Throwable th) {
                                th = th;
                                gZIPInputStream = gZIPInputStream2;
                                try {
                                    byteArrayOutputStream.close();
                                } catch (Exception e3) {
                                }
                                try {
                                    gZIPInputStream.close();
                                } catch (Exception e4) {
                                }
                                try {
                                    byteArrayInputStream2.close();
                                } catch (Exception e5) {
                                }
                                throw th;
                            }
                        }
                        decode = byteArrayOutputStream.toByteArray();
                        try {
                            byteArrayOutputStream.close();
                        } catch (Exception e6) {
                        }
                        try {
                            gZIPInputStream2.close();
                        } catch (Exception e7) {
                        }
                        try {
                            byteArrayInputStream2.close();
                        } catch (Exception e8) {
                        }
                    } catch (IOException e9) {
                        e = e9;
                        byteArrayInputStream = byteArrayInputStream2;
                        try {
                            e.printStackTrace();
                            try {
                                byteArrayOutputStream.close();
                            } catch (Exception e10) {
                            }
                            try {
                                gZIPInputStream.close();
                            } catch (Exception e11) {
                            }
                            try {
                                byteArrayInputStream.close();
                            } catch (Exception e12) {
                            }
                            return decode;
                        } catch (Throwable th2) {
                            th = th2;
                            byteArrayInputStream2 = byteArrayInputStream;
                            byteArrayOutputStream.close();
                            gZIPInputStream.close();
                            byteArrayInputStream2.close();
                            throw th;
                        }
                    } catch (Throwable th3) {
                        th = th3;
                        byteArrayOutputStream.close();
                        gZIPInputStream.close();
                        byteArrayInputStream2.close();
                        throw th;
                    }
                } catch (IOException e13) {
                    e = e13;
                    byteArrayInputStream = null;
                    e.printStackTrace();
                    byteArrayOutputStream.close();
                    gZIPInputStream.close();
                    byteArrayInputStream.close();
                    return decode;
                } catch (Throwable th4) {
                    th = th4;
                    byteArrayInputStream2 = null;
                    byteArrayOutputStream.close();
                    gZIPInputStream.close();
                    byteArrayInputStream2.close();
                    throw th;
                }
            } catch (IOException e14) {
                e = e14;
                byteArrayOutputStream = null;
                byteArrayInputStream = null;
                e.printStackTrace();
                byteArrayOutputStream.close();
                gZIPInputStream.close();
                byteArrayInputStream.close();
                return decode;
            } catch (Throwable th5) {
                th = th5;
                byteArrayOutputStream = null;
                byteArrayInputStream2 = null;
                byteArrayOutputStream.close();
                gZIPInputStream.close();
                byteArrayInputStream2.close();
                throw th;
            }
        }
        return decode;
    }

    public static byte[] decode(byte[] bArr) {
        return decode(bArr, 0, bArr.length, 0);
    }

    public static byte[] decode(byte[] bArr, int i, int i2, int i3) {
        int i4;
        int i5;
        int i6;
        if (bArr == null) {
            throw new NullPointerException("Cannot decode null source array.");
        } else if (i < 0 || i + i2 > bArr.length) {
            throw new IllegalArgumentException(String.format("Source array with length %d cannot have offset of %d and process %d bytes.", Integer.valueOf(bArr.length), Integer.valueOf(i), Integer.valueOf(i2)));
        } else if (i2 == 0) {
            return new byte[0];
        } else {
            if (i2 < 4) {
                throw new IllegalArgumentException("Base64-encoded string must have at least four characters, but length specified was " + i2);
            }
            byte[] decodabet = getDecodabet(i3);
            byte[] bArr2 = new byte[((i2 * 3) / 4)];
            byte[] bArr3 = new byte[4];
            int i7 = i;
            int i8 = 0;
            int i9 = 0;
            while (true) {
                if (i7 >= i + i2) {
                    i4 = i9;
                    break;
                }
                byte b2 = decodabet[bArr[i7] & 255];
                if (b2 >= -5) {
                    if (b2 >= -1) {
                        i5 = i8 + 1;
                        bArr3[i8] = bArr[i7];
                        if (i5 > 3) {
                            i4 = decode4to3(bArr3, 0, bArr2, i9, i3) + i9;
                            if (bArr[i7] == 61) {
                                break;
                            }
                            i6 = i4;
                            i5 = 0;
                        } else {
                            i6 = i9;
                        }
                    } else {
                        i5 = i8;
                        i6 = i9;
                    }
                    i7++;
                    i9 = i6;
                    i8 = i5;
                } else {
                    throw new IOException(String.format("Bad Base64 input character decimal %d in array position %d", Integer.valueOf(bArr[i7] & 255), Integer.valueOf(i7)));
                }
            }
            byte[] bArr4 = new byte[i4];
            System.arraycopy(bArr2, 0, bArr4, 0, i4);
            return bArr4;
        }
    }

    /* access modifiers changed from: private */
    public static int decode4to3(byte[] bArr, int i, byte[] bArr2, int i2, int i3) {
        if (bArr == null) {
            throw new NullPointerException("Source array was null.");
        } else if (bArr2 == null) {
            throw new NullPointerException("Destination array was null.");
        } else if (i < 0 || i + 3 >= bArr.length) {
            throw new IllegalArgumentException(String.format("Source array with length %d cannot have offset of %d and still process four bytes.", Integer.valueOf(bArr.length), Integer.valueOf(i)));
        } else if (i2 < 0 || i2 + 2 >= bArr2.length) {
            throw new IllegalArgumentException(String.format("Destination array with length %d cannot have offset of %d and still store three bytes.", Integer.valueOf(bArr2.length), Integer.valueOf(i2)));
        } else {
            byte[] decodabet = getDecodabet(i3);
            if (bArr[i + 2] == 61) {
                bArr2[i2] = (byte) ((((decodabet[bArr[i]] & 255) << 18) | ((decodabet[bArr[i + 1]] & 255) << com.a.a.k.b.ROOT_CA_EXPIRED)) >>> 16);
                return 1;
            } else if (bArr[i + 3] == 61) {
                int i4 = ((decodabet[bArr[i]] & 255) << 18) | ((decodabet[bArr[i + 1]] & 255) << com.a.a.k.b.ROOT_CA_EXPIRED) | ((decodabet[bArr[i + 2]] & 255) << 6);
                bArr2[i2] = (byte) (i4 >>> 16);
                bArr2[i2 + 1] = (byte) (i4 >>> 8);
                return 2;
            } else {
                byte b2 = ((decodabet[bArr[i]] & 255) << 18) | ((decodabet[bArr[i + 1]] & 255) << com.a.a.k.b.ROOT_CA_EXPIRED) | ((decodabet[bArr[i + 2]] & 255) << 6) | (decodabet[bArr[i + 3]] & 255);
                bArr2[i2] = (byte) (b2 >> 16);
                bArr2[i2 + 1] = (byte) (b2 >> 8);
                bArr2[i2 + 2] = (byte) b2;
                return 3;
            }
        }
    }

    public static void decodeFileToFile(String str, String str2) {
        BufferedOutputStream bufferedOutputStream;
        byte[] decodeFromFile = decodeFromFile(str);
        BufferedOutputStream bufferedOutputStream2 = null;
        try {
            bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(str2));
            try {
                bufferedOutputStream.write(decodeFromFile);
                try {
                    bufferedOutputStream.close();
                } catch (Exception e) {
                }
            } catch (IOException e2) {
                e = e2;
                try {
                    throw e;
                } catch (Throwable th) {
                    th = th;
                    bufferedOutputStream2 = bufferedOutputStream;
                }
            }
        } catch (IOException e3) {
            e = e3;
            bufferedOutputStream = null;
            throw e;
        } catch (Throwable th2) {
            th = th2;
            try {
                bufferedOutputStream2.close();
            } catch (Exception e4) {
            }
            throw th;
        }
    }

    public static byte[] decodeFromFile(String str) {
        int i = 0;
        C0000a aVar = null;
        try {
            File file = new File(str);
            if (file.length() > 2147483647L) {
                throw new IOException("File is too big for this convenience method (" + file.length() + " bytes).");
            }
            byte[] bArr = new byte[((int) file.length())];
            C0000a aVar2 = new C0000a(new BufferedInputStream(new FileInputStream(file)), 0);
            while (true) {
                try {
                    int read = aVar2.read(bArr, i, 4096);
                    if (read < 0) {
                        break;
                    }
                    i += read;
                } catch (IOException e) {
                    e = e;
                    aVar = aVar2;
                    try {
                        throw e;
                    } catch (Throwable th) {
                        th = th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    aVar = aVar2;
                    try {
                        aVar.close();
                    } catch (Exception e2) {
                    }
                    throw th;
                }
            }
            byte[] bArr2 = new byte[i];
            System.arraycopy(bArr, 0, bArr2, 0, i);
            try {
                aVar2.close();
            } catch (Exception e3) {
            }
            return bArr2;
        } catch (IOException e4) {
            e = e4;
            throw e;
        }
    }

    public static void decodeToFile(String str, String str2) {
        b bVar;
        try {
            bVar = new b(new FileOutputStream(str2), 0);
            try {
                bVar.write(str.getBytes("US-ASCII"));
                try {
                    bVar.close();
                } catch (Exception e) {
                }
            } catch (IOException e2) {
                e = e2;
                try {
                    throw e;
                } catch (Throwable th) {
                    th = th;
                }
            }
        } catch (IOException e3) {
            e = e3;
            bVar = null;
            throw e;
        } catch (Throwable th2) {
            th = th2;
            bVar = null;
            try {
                bVar.close();
            } catch (Exception e4) {
            }
            throw th;
        }
    }

    public static Object decodeToObject(String str) {
        return decodeToObject(str, 0, null);
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:27:0x0031 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:17:0x0026 */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: java.io.ObjectInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: java.io.ByteArrayInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: java.io.ObjectInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: java.io.ObjectInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: java.io.ByteArrayInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v5, resolved type: java.io.ObjectInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v6, resolved type: java.io.ObjectInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v7, resolved type: java.io.ObjectInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v8, resolved type: java.io.ByteArrayInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v10, resolved type: java.io.ByteArrayInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v11, resolved type: java.io.ObjectInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v12, resolved type: java.io.ObjectInputStream} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:27:0x0031=Splitter:B:27:0x0031, B:17:0x0026=Splitter:B:17:0x0026} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Object decodeToObject(java.lang.String r3, int r4, final java.lang.ClassLoader r5) {
        /*
            r1 = 0
            byte[] r0 = decode(r3, r4)
            java.io.ByteArrayInputStream r2 = new java.io.ByteArrayInputStream     // Catch:{ IOException -> 0x0024, ClassNotFoundException -> 0x002f, all -> 0x003a }
            r2.<init>(r0)     // Catch:{ IOException -> 0x0024, ClassNotFoundException -> 0x002f, all -> 0x003a }
            if (r5 != 0) goto L_0x001d
            java.io.ObjectInputStream r0 = new java.io.ObjectInputStream     // Catch:{ IOException -> 0x003f, ClassNotFoundException -> 0x003d }
            r0.<init>(r2)     // Catch:{ IOException -> 0x003f, ClassNotFoundException -> 0x003d }
            r1 = r0
        L_0x0012:
            java.lang.Object r0 = r1.readObject()     // Catch:{ IOException -> 0x003f, ClassNotFoundException -> 0x003d }
            r2.close()     // Catch:{ Exception -> 0x0032 }
        L_0x0019:
            r1.close()     // Catch:{ Exception -> 0x0034 }
        L_0x001c:
            return r0
        L_0x001d:
            com.a.a.s.a$1 r0 = new com.a.a.s.a$1     // Catch:{ IOException -> 0x003f, ClassNotFoundException -> 0x003d }
            r0.<init>(r2, r5)     // Catch:{ IOException -> 0x003f, ClassNotFoundException -> 0x003d }
            r1 = r0
            goto L_0x0012
        L_0x0024:
            r0 = move-exception
            r2 = r1
        L_0x0026:
            throw r0     // Catch:{ all -> 0x0027 }
        L_0x0027:
            r0 = move-exception
        L_0x0028:
            r2.close()     // Catch:{ Exception -> 0x0036 }
        L_0x002b:
            r1.close()     // Catch:{ Exception -> 0x0038 }
        L_0x002e:
            throw r0
        L_0x002f:
            r0 = move-exception
            r2 = r1
        L_0x0031:
            throw r0     // Catch:{ all -> 0x0027 }
        L_0x0032:
            r2 = move-exception
            goto L_0x0019
        L_0x0034:
            r1 = move-exception
            goto L_0x001c
        L_0x0036:
            r2 = move-exception
            goto L_0x002b
        L_0x0038:
            r1 = move-exception
            goto L_0x002e
        L_0x003a:
            r0 = move-exception
            r2 = r1
            goto L_0x0028
        L_0x003d:
            r0 = move-exception
            goto L_0x0031
        L_0x003f:
            r0 = move-exception
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.s.a.decodeToObject(java.lang.String, int, java.lang.ClassLoader):java.lang.Object");
    }

    public static void encode(ByteBuffer byteBuffer, ByteBuffer byteBuffer2) {
        byte[] bArr = new byte[3];
        byte[] bArr2 = new byte[4];
        while (byteBuffer.hasRemaining()) {
            int min = Math.min(3, byteBuffer.remaining());
            byteBuffer.get(bArr, 0, min);
            encode3to4(bArr2, bArr, min, 0);
            byteBuffer2.put(bArr2);
        }
    }

    public static void encode(ByteBuffer byteBuffer, CharBuffer charBuffer) {
        byte[] bArr = new byte[3];
        byte[] bArr2 = new byte[4];
        while (byteBuffer.hasRemaining()) {
            int min = Math.min(3, byteBuffer.remaining());
            byteBuffer.get(bArr, 0, min);
            encode3to4(bArr2, bArr, min, 0);
            for (int i = 0; i < 4; i++) {
                charBuffer.put((char) (bArr2[i] & 255));
            }
        }
    }

    /* access modifiers changed from: private */
    public static byte[] encode3to4(byte[] bArr, int i, int i2, byte[] bArr2, int i3, int i4) {
        int i5 = 0;
        byte[] alphabet = getAlphabet(i4);
        int i6 = (i2 > 1 ? (bArr[i + 1] << 24) >>> 16 : 0) | (i2 > 0 ? (bArr[i] << 24) >>> 8 : 0);
        if (i2 > 2) {
            i5 = (bArr[i + 2] << 24) >>> 24;
        }
        int i7 = i5 | i6;
        switch (i2) {
            case 1:
                bArr2[i3] = alphabet[i7 >>> 18];
                bArr2[i3 + 1] = alphabet[(i7 >>> 12) & 63];
                bArr2[i3 + 2] = EQUALS_SIGN;
                bArr2[i3 + 3] = EQUALS_SIGN;
                break;
            case 2:
                bArr2[i3] = alphabet[i7 >>> 18];
                bArr2[i3 + 1] = alphabet[(i7 >>> 12) & 63];
                bArr2[i3 + 2] = alphabet[(i7 >>> 6) & 63];
                bArr2[i3 + 3] = EQUALS_SIGN;
                break;
            case 3:
                bArr2[i3] = alphabet[i7 >>> 18];
                bArr2[i3 + 1] = alphabet[(i7 >>> 12) & 63];
                bArr2[i3 + 2] = alphabet[(i7 >>> 6) & 63];
                bArr2[i3 + 3] = alphabet[i7 & 63];
                break;
        }
        return bArr2;
    }

    /* access modifiers changed from: private */
    public static byte[] encode3to4(byte[] bArr, byte[] bArr2, int i, int i2) {
        encode3to4(bArr2, 0, i, bArr, 0, i2);
        return bArr;
    }

    public static String encodeBytes(byte[] bArr) {
        String str = null;
        try {
            str = encodeBytes(bArr, 0, bArr.length, 0);
        } catch (IOException e) {
            if (!$assertionsDisabled) {
                throw new AssertionError(e.getMessage());
            }
        }
        if ($assertionsDisabled || str != null) {
            return str;
        }
        throw new AssertionError();
    }

    public static String encodeBytes(byte[] bArr, int i) {
        return encodeBytes(bArr, 0, bArr.length, i);
    }

    public static String encodeBytes(byte[] bArr, int i, int i2) {
        String str = null;
        try {
            str = encodeBytes(bArr, i, i2, 0);
        } catch (IOException e) {
            if (!$assertionsDisabled) {
                throw new AssertionError(e.getMessage());
            }
        }
        if ($assertionsDisabled || str != null) {
            return str;
        }
        throw new AssertionError();
    }

    public static String encodeBytes(byte[] bArr, int i, int i2, int i3) {
        byte[] encodeBytesToBytes = encodeBytesToBytes(bArr, i, i2, i3);
        try {
            return new String(encodeBytesToBytes, "US-ASCII");
        } catch (UnsupportedEncodingException e) {
            return new String(encodeBytesToBytes);
        }
    }

    public static byte[] encodeBytesToBytes(byte[] bArr) {
        try {
            return encodeBytesToBytes(bArr, 0, bArr.length, 0);
        } catch (IOException e) {
            if ($assertionsDisabled) {
                return null;
            }
            throw new AssertionError("IOExceptions only come from GZipping, which is turned off: " + e.getMessage());
        }
    }

    public static byte[] encodeBytesToBytes(byte[] bArr, int i, int i2, int i3) {
        b bVar;
        ByteArrayOutputStream byteArrayOutputStream;
        ByteArrayOutputStream byteArrayOutputStream2;
        GZIPOutputStream gZIPOutputStream;
        GZIPOutputStream gZIPOutputStream2 = null;
        if (bArr == null) {
            throw new NullPointerException("Cannot serialize a null array.");
        } else if (i < 0) {
            throw new IllegalArgumentException("Cannot have negative offset: " + i);
        } else if (i2 < 0) {
            throw new IllegalArgumentException("Cannot have length offset: " + i2);
        } else if (i + i2 > bArr.length) {
            throw new IllegalArgumentException(String.format("Cannot have offset of %d and length of %d with array of length %d", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(bArr.length)));
        } else if ((i3 & 2) != 0) {
            try {
                byteArrayOutputStream = new ByteArrayOutputStream();
                try {
                    bVar = new b(byteArrayOutputStream, i3 | 1);
                    try {
                        gZIPOutputStream = new GZIPOutputStream(bVar);
                    } catch (IOException e) {
                        e = e;
                        byteArrayOutputStream2 = byteArrayOutputStream;
                        try {
                            throw e;
                        } catch (Throwable th) {
                            th = th;
                            byteArrayOutputStream = byteArrayOutputStream2;
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        try {
                            gZIPOutputStream2.close();
                        } catch (Exception e2) {
                        }
                        try {
                            bVar.close();
                        } catch (Exception e3) {
                        }
                        try {
                            byteArrayOutputStream.close();
                        } catch (Exception e4) {
                        }
                        throw th;
                    }
                } catch (IOException e5) {
                    e = e5;
                    bVar = null;
                    byteArrayOutputStream2 = byteArrayOutputStream;
                    throw e;
                } catch (Throwable th3) {
                    th = th3;
                    bVar = null;
                    gZIPOutputStream2.close();
                    bVar.close();
                    byteArrayOutputStream.close();
                    throw th;
                }
                try {
                    gZIPOutputStream.write(bArr, i, i2);
                    gZIPOutputStream.close();
                    try {
                        gZIPOutputStream.close();
                    } catch (Exception e6) {
                    }
                    try {
                        bVar.close();
                    } catch (Exception e7) {
                    }
                    try {
                        byteArrayOutputStream.close();
                    } catch (Exception e8) {
                    }
                    return byteArrayOutputStream.toByteArray();
                } catch (IOException e9) {
                    e = e9;
                    gZIPOutputStream2 = gZIPOutputStream;
                    byteArrayOutputStream2 = byteArrayOutputStream;
                    throw e;
                } catch (Throwable th4) {
                    th = th4;
                    gZIPOutputStream2 = gZIPOutputStream;
                    gZIPOutputStream2.close();
                    bVar.close();
                    byteArrayOutputStream.close();
                    throw th;
                }
            } catch (IOException e10) {
                e = e10;
                bVar = null;
                byteArrayOutputStream2 = null;
                throw e;
            } catch (Throwable th5) {
                th = th5;
                bVar = null;
                byteArrayOutputStream = null;
                gZIPOutputStream2.close();
                bVar.close();
                byteArrayOutputStream.close();
                throw th;
            }
        } else {
            boolean z = (i3 & 8) != 0;
            int i4 = (i2 % 3 > 0 ? 4 : 0) + ((i2 / 3) * 4);
            if (z) {
                i4 += i4 / 76;
            }
            byte[] bArr2 = new byte[i4];
            int i5 = i2 - 2;
            int i6 = 0;
            int i7 = 0;
            int i8 = 0;
            while (i8 < i5) {
                encode3to4(bArr, i8 + i, 3, bArr2, i7, i3);
                int i9 = i6 + 4;
                if (z && i9 >= 76) {
                    bArr2[i7 + 4] = 10;
                    i7++;
                    i9 = 0;
                }
                i7 += 4;
                i6 = i9;
                i8 += 3;
            }
            if (i8 < i2) {
                encode3to4(bArr, i8 + i, i2 - i8, bArr2, i7, i3);
                i7 += 4;
            }
            if (i7 > bArr2.length - 1) {
                return bArr2;
            }
            byte[] bArr3 = new byte[i7];
            System.arraycopy(bArr2, 0, bArr3, 0, i7);
            return bArr3;
        }
    }

    public static void encodeFileToFile(String str, String str2) {
        BufferedOutputStream bufferedOutputStream;
        String encodeFromFile = encodeFromFile(str);
        try {
            bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(str2));
            try {
                bufferedOutputStream.write(encodeFromFile.getBytes("US-ASCII"));
                try {
                    bufferedOutputStream.close();
                } catch (Exception e) {
                }
            } catch (IOException e2) {
                e = e2;
                try {
                    throw e;
                } catch (Throwable th) {
                    th = th;
                }
            }
        } catch (IOException e3) {
            e = e3;
            bufferedOutputStream = null;
            throw e;
        } catch (Throwable th2) {
            th = th2;
            bufferedOutputStream = null;
            try {
                bufferedOutputStream.close();
            } catch (Exception e4) {
            }
            throw th;
        }
    }

    public static String encodeFromFile(String str) {
        C0000a aVar;
        int i = 0;
        C0000a aVar2 = null;
        try {
            File file = new File(str);
            byte[] bArr = new byte[Math.max((int) ((((double) file.length()) * 1.4d) + 1.0d), 40)];
            aVar = new C0000a(new BufferedInputStream(new FileInputStream(file)), 1);
            while (true) {
                try {
                    int read = aVar.read(bArr, i, 4096);
                    if (read < 0) {
                        break;
                    }
                    i += read;
                } catch (IOException e) {
                    e = e;
                    try {
                        throw e;
                    } catch (Throwable th) {
                        th = th;
                        aVar2 = aVar;
                    }
                }
            }
            String str2 = new String(bArr, 0, i, "US-ASCII");
            try {
                aVar.close();
            } catch (Exception e2) {
            }
            return str2;
        } catch (IOException e3) {
            e = e3;
            aVar = null;
            throw e;
        } catch (Throwable th2) {
            th = th2;
            try {
                aVar2.close();
            } catch (Exception e4) {
            }
            throw th;
        }
    }

    public static String encodeObject(Serializable serializable) {
        return encodeObject(serializable, 0);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (5) to help type inference */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v0, resolved type: java.io.ObjectOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: java.util.zip.GZIPOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v0, resolved type: java.io.ByteArrayOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v1, resolved type: java.io.ByteArrayOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: java.util.zip.GZIPOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v2, resolved type: java.io.ObjectOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: java.io.OutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: java.io.OutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v3, resolved type: java.io.ObjectOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v4, resolved type: java.io.ByteArrayOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v4, resolved type: java.io.OutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v5, resolved type: java.util.zip.GZIPOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v6, resolved type: java.io.OutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v7, resolved type: java.util.zip.GZIPOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v8, resolved type: java.util.zip.GZIPOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v9, resolved type: java.util.zip.GZIPOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v11, resolved type: java.io.OutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v5, resolved type: java.io.ByteArrayOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v12, resolved type: java.util.zip.GZIPOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v6, resolved type: java.io.ObjectOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v7, resolved type: java.io.ByteArrayOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v8, resolved type: java.io.ObjectOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v9, resolved type: java.io.ByteArrayOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v10, resolved type: java.io.ObjectOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v13, resolved type: java.io.OutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v14, resolved type: java.util.zip.GZIPOutputStream} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String encodeObject(java.io.Serializable r5, int r6) {
        /*
            r1 = 0
            if (r5 != 0) goto L_0x000b
            java.lang.NullPointerException r0 = new java.lang.NullPointerException
            java.lang.String r1 = "Cannot serialize a null object."
            r0.<init>(r1)
            throw r0
        L_0x000b:
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x0049, all -> 0x0077 }
            r4.<init>()     // Catch:{ IOException -> 0x0049, all -> 0x0077 }
            com.a.a.s.a$b r3 = new com.a.a.s.a$b     // Catch:{ IOException -> 0x0083, all -> 0x007c }
            r0 = r6 | 1
            r3.<init>(r4, r0)     // Catch:{ IOException -> 0x0083, all -> 0x007c }
            r0 = r6 & 2
            if (r0 == 0) goto L_0x0041
            java.util.zip.GZIPOutputStream r2 = new java.util.zip.GZIPOutputStream     // Catch:{ IOException -> 0x0087, all -> 0x0080 }
            r2.<init>(r3)     // Catch:{ IOException -> 0x0087, all -> 0x0080 }
            java.io.ObjectOutputStream r0 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x008a }
            r0.<init>(r2)     // Catch:{ IOException -> 0x008a }
            r1 = r0
        L_0x0026:
            r1.writeObject(r5)     // Catch:{ IOException -> 0x008a }
            r1.close()     // Catch:{ Exception -> 0x0067 }
        L_0x002c:
            r2.close()     // Catch:{ Exception -> 0x0069 }
        L_0x002f:
            r3.close()     // Catch:{ Exception -> 0x006b }
        L_0x0032:
            r4.close()     // Catch:{ Exception -> 0x006d }
        L_0x0035:
            java.lang.String r0 = new java.lang.String     // Catch:{ UnsupportedEncodingException -> 0x005c }
            byte[] r1 = r4.toByteArray()     // Catch:{ UnsupportedEncodingException -> 0x005c }
            java.lang.String r2 = "US-ASCII"
            r0.<init>(r1, r2)     // Catch:{ UnsupportedEncodingException -> 0x005c }
        L_0x0040:
            return r0
        L_0x0041:
            java.io.ObjectOutputStream r0 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x0087, all -> 0x0080 }
            r0.<init>(r3)     // Catch:{ IOException -> 0x0087, all -> 0x0080 }
            r2 = r1
            r1 = r0
            goto L_0x0026
        L_0x0049:
            r0 = move-exception
            r2 = r1
            r3 = r1
            r4 = r1
        L_0x004d:
            throw r0     // Catch:{ all -> 0x004e }
        L_0x004e:
            r0 = move-exception
        L_0x004f:
            r1.close()     // Catch:{ Exception -> 0x006f }
        L_0x0052:
            r2.close()     // Catch:{ Exception -> 0x0071 }
        L_0x0055:
            r3.close()     // Catch:{ Exception -> 0x0073 }
        L_0x0058:
            r4.close()     // Catch:{ Exception -> 0x0075 }
        L_0x005b:
            throw r0
        L_0x005c:
            r0 = move-exception
            java.lang.String r0 = new java.lang.String
            byte[] r1 = r4.toByteArray()
            r0.<init>(r1)
            goto L_0x0040
        L_0x0067:
            r0 = move-exception
            goto L_0x002c
        L_0x0069:
            r0 = move-exception
            goto L_0x002f
        L_0x006b:
            r0 = move-exception
            goto L_0x0032
        L_0x006d:
            r0 = move-exception
            goto L_0x0035
        L_0x006f:
            r1 = move-exception
            goto L_0x0052
        L_0x0071:
            r1 = move-exception
            goto L_0x0055
        L_0x0073:
            r1 = move-exception
            goto L_0x0058
        L_0x0075:
            r1 = move-exception
            goto L_0x005b
        L_0x0077:
            r0 = move-exception
            r2 = r1
            r3 = r1
            r4 = r1
            goto L_0x004f
        L_0x007c:
            r0 = move-exception
            r2 = r1
            r3 = r1
            goto L_0x004f
        L_0x0080:
            r0 = move-exception
            r2 = r1
            goto L_0x004f
        L_0x0083:
            r0 = move-exception
            r2 = r1
            r3 = r1
            goto L_0x004d
        L_0x0087:
            r0 = move-exception
            r2 = r1
            goto L_0x004d
        L_0x008a:
            r0 = move-exception
            goto L_0x004d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.a.a.s.a.encodeObject(java.io.Serializable, int):java.lang.String");
    }

    public static void encodeToFile(byte[] bArr, String str) {
        b bVar;
        if (bArr == null) {
            throw new NullPointerException("Data to encode was null.");
        }
        b bVar2 = null;
        try {
            bVar = new b(new FileOutputStream(str), 1);
            try {
                bVar.write(bArr);
                try {
                    bVar.close();
                } catch (Exception e) {
                }
            } catch (IOException e2) {
                e = e2;
                try {
                    throw e;
                } catch (Throwable th) {
                    th = th;
                    bVar2 = bVar;
                }
            }
        } catch (IOException e3) {
            e = e3;
            bVar = null;
            throw e;
        } catch (Throwable th2) {
            th = th2;
            try {
                bVar2.close();
            } catch (Exception e4) {
            }
            throw th;
        }
    }

    private static final byte[] getAlphabet(int i) {
        return (i & 16) == 16 ? _URL_SAFE_ALPHABET : (i & 32) == 32 ? _ORDERED_ALPHABET : _STANDARD_ALPHABET;
    }

    /* access modifiers changed from: private */
    public static final byte[] getDecodabet(int i) {
        return (i & 16) == 16 ? _URL_SAFE_DECODABET : (i & 32) == 32 ? _ORDERED_DECODABET : _STANDARD_DECODABET;
    }
}
