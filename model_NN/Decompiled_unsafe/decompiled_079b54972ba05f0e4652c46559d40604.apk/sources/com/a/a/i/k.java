package com.a.a.i;

import java.util.Date;
import java.util.Enumeration;

public class k {
    public static final int APRIL = 1048576;
    public static final int AUGUST = 16777216;
    public static final int COUNT = 32;
    public static final int DAILY = 16;
    public static final int DAY_IN_MONTH = 1;
    public static final int DAY_IN_WEEK = 2;
    public static final int DAY_IN_YEAR = 4;
    public static final int DECEMBER = 268435456;
    public static final int END = 64;
    public static final int FEBRUARY = 262144;
    public static final int FIFTH = 5;
    public static final int FIFTHLAST = 512;
    public static final int FIRST = 1;
    public static final int FOURTH = 4;
    public static final int FOURTHLAST = 256;
    public static final int FREQUENCY = 0;
    public static final int FRIDAY = 2048;
    public static final int INTERVAL = 128;
    public static final int JANUARY = 131072;
    public static final int JULY = 8388608;
    public static final int JUNE = 4194304;
    public static final int LAST = 32;
    public static final int MARCH = 524288;
    public static final int MAY = 2097152;
    public static final int MONDAY = 32768;
    public static final int MONTHLY = 18;
    public static final int MONTH_IN_YEAR = 8;
    public static final int NOVEMBER = 134217728;
    public static final int OCTOBER = 67108864;
    public static final int SATURDAY = 1024;
    public static final int SECOND = 2;
    public static final int SECONDLAST = 64;
    public static final int SEPTEMBER = 33554432;
    public static final int SUNDAY = 65536;
    public static final int THIRD = 3;
    public static final int THIRDLAST = 128;
    public static final int THURSDAY = 4096;
    public static final int TUESDAY = 16384;
    public static final int WEDNESDAY = 8192;
    public static final int WEEKLY = 17;
    public static final int WEEK_IN_MONTH = 16;
    public static final int YEARLY = 19;
    private int[] fields;
    private Enumeration<Date> jp;

    public Enumeration<Date> a(long j, long j2, long j3) {
        return null;
    }

    public void b(int i, long j) {
    }

    public long bz(int i) {
        return 0;
    }

    public int[] eo() {
        return this.fields;
    }

    public boolean equals(Object obj) {
        return false;
    }

    public Enumeration<Date> et() {
        return this.jp;
    }

    public int getInt(int i) {
        return 0;
    }

    public void i(long j) {
    }

    public void j(long j) {
    }

    public void setInt(int i, int i2) {
    }
}
