package com.a.a.i;

import java.util.Enumeration;

public interface j {
    public static final String UNCATEGORIZED = null;

    boolean M(int i, int i2);

    boolean N(int i, int i2);

    String O(int i, int i2);

    Enumeration<i> a(i iVar);

    void addCategory(String str);

    Enumeration<i> ap(String str);

    Enumeration<i> aq(String str);

    boolean ar(String str);

    boolean br(int i);

    int[] bs(int i);

    int[] bt(int i);

    int bu(int i);

    String bv(int i);

    String bw(int i);

    int bx(int i);

    int by(int i);

    void c(String str, boolean z);

    void close();

    String[] ep();

    int eq();

    Enumeration<i> er();

    int[] es();

    String getName();

    void k(String str, String str2);
}
