package com.a.a.i;

import com.a.a.j.b;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class g {
    public static final int CONTACT_LIST = 1;
    public static final int EVENT_LIST = 2;
    public static final int READ_ONLY = 1;
    public static final int READ_WRITE = 3;
    public static final int TODO_LIST = 3;
    public static final int WRITE_ONLY = 2;
    private static g jn;

    protected g() {
    }

    public static g ek() {
        if (jn == null) {
            jn = new b();
        }
        return jn;
    }

    public abstract j F(int i, int i2);

    public abstract j a(int i, int i2, String str);

    public abstract void a(i iVar, OutputStream outputStream, String str, String str2);

    public abstract i[] b(InputStream inputStream, String str);

    public abstract String[] bo(int i);

    public abstract String[] bp(int i);
}
