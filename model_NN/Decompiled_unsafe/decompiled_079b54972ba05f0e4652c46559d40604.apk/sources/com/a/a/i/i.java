package com.a.a.i;

public interface i {
    public static final int ATTR_NONE = 0;
    public static final int BINARY = 0;
    public static final int BOOLEAN = 1;
    public static final int DATE = 2;
    public static final int EXTENDED_ATTRIBUTE_MIN_VALUE = 16777216;
    public static final int EXTENDED_FIELD_MIN_VALUE = 16777216;
    public static final int INT = 3;
    public static final int STRING = 4;
    public static final int STRING_ARRAY = 5;

    byte[] G(int i, int i2);

    long H(int i, int i2);

    boolean I(int i, int i2);

    String[] J(int i, int i2);

    void K(int i, int i2);

    int L(int i, int i2);

    void a(int i, int i2, int i3, long j);

    void a(int i, int i2, int i3, String str);

    void a(int i, int i2, int i3, boolean z);

    void a(int i, int i2, int i3, byte[] bArr, int i4, int i5);

    void a(int i, int i2, int i3, String[] strArr);

    void a(int i, int i2, long j);

    void a(int i, int i2, byte[] bArr, int i3, int i4);

    void a(int i, int i2, String[] strArr);

    void an(String str);

    void ao(String str);

    void b(int i, int i2, String str);

    int bq(int i);

    void c(int i, int i2, boolean z);

    void commit();

    j em();

    boolean en();

    int[] eo();

    String[] ep();

    int eq();

    int getInt(int i, int i2);

    String getString(int i, int i2);

    void n(int i, int i2, int i3, int i4);

    void p(int i, int i2, int i3);
}
