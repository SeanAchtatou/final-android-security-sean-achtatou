package com.a.a.e;

import android.app.Activity;
import android.graphics.Typeface;
import android.text.InputFilter;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.meteoroid.core.l;

public class z extends r {
    public static final int ANY = 0;
    public static final int CONSTRAINT_MASK = 65535;
    public static final int DECIMAL = 5;
    public static final int EMAILADDR = 1;
    public static final int INITIAL_CAPS_SENTENCE = 2097152;
    public static final int INITIAL_CAPS_WORD = 1048576;
    public static final int NON_PREDICTIVE = 524288;
    public static final int NUMERIC = 2;
    public static final int PASSWORD = 65536;
    public static final int PHONENUMBER = 3;
    public static final int SENSITIVE = 262144;
    public static final int UNEDITABLE = 131072;
    public static final int URL = 4;
    /* access modifiers changed from: private */
    public TextView gc;
    /* access modifiers changed from: private */
    public LinearLayout gd;
    private int hN;
    private int hO;
    /* access modifiers changed from: private */
    public EditText hP;
    private int hU;

    public z(final String str, final String str2, int i, final int i2) {
        super(str);
        this.hN = i;
        this.hO = i2;
        l.getHandler().post(new Runnable() {
            public void run() {
                Activity activity = l.getActivity();
                LinearLayout unused = z.this.gd = new LinearLayout(activity);
                z.this.gd.setOrientation(1);
                z.this.gd.setBackgroundColor(-16777216);
                TextView unused2 = z.this.gc = new TextView(activity);
                z.this.V(str);
                z.this.gd.addView(z.this.gc, new ViewGroup.LayoutParams(-1, -2));
                EditText unused3 = z.this.hP = new EditText(activity);
                z.this.setString(str2);
                switch (i2) {
                    case 0:
                        z.this.hP.setSingleLine(true);
                        z.this.hP.setInputType(1);
                        break;
                    case 1:
                        z.this.hP.setSingleLine(true);
                        z.this.hP.setInputType(48);
                        break;
                    case 2:
                        z.this.hP.setSingleLine(true);
                        z.this.hP.setInputType(2);
                        break;
                    case 3:
                        z.this.hP.setSingleLine(true);
                        z.this.hP.setInputType(3);
                        break;
                    case 4:
                        z.this.hP.setSingleLine(true);
                        z.this.hP.setInputType(16);
                        break;
                    case 5:
                        z.this.hP.setSingleLine(true);
                        z.this.hP.setInputType(12290);
                        break;
                    case 65536:
                        z.this.hP.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        z.this.hP.setTypeface(Typeface.MONOSPACE);
                        break;
                }
                z.this.gd.addView(z.this.hP, new ViewGroup.LayoutParams(-1, -2));
                synchronized (z.this) {
                    z.this.notify();
                }
            }
        });
        synchronized (this) {
            try {
                wait(1000);
            } catch (InterruptedException e) {
            }
        }
    }

    public void V(final String str) {
        super.V(str);
        l.getHandler().post(new Runnable() {
            public void run() {
                z.this.gc.setText(str);
            }
        });
    }

    public void W(String str) {
    }

    public int a(char[] cArr) {
        return getString().toCharArray().length;
    }

    public void a(char[] cArr, int i, int i2, int i3) {
        h(new String(cArr, i, i2), i3);
    }

    /* access modifiers changed from: protected */
    public void aB() {
        l.hM().hideSoftInputFromWindow(this.hP.getWindowToken(), 2);
        Log.d("TextBox", "Force close soft input.");
    }

    public int aN(int i) {
        this.hP.setFilters(new InputFilter[]{new InputFilter.LengthFilter(i)});
        this.hN = i;
        return i;
    }

    public void aO(int i) {
        this.hO = i;
    }

    public void b(char[] cArr, int i, int i2) {
        setString(new String(cArr, i, i2));
    }

    /* renamed from: bT */
    public LinearLayout getView() {
        return this.gd;
    }

    public int cP() {
        return this.hO;
    }

    public int cQ() {
        return this.hU;
    }

    /* access modifiers changed from: protected */
    public void ch() {
        this.hP.clearFocus();
    }

    public int getMaxSize() {
        return this.hN;
    }

    public String getString() {
        return this.hP.getText().toString();
    }

    public void h(String str, int i) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(getString().substring(0, i));
        stringBuffer.append(str);
        stringBuffer.append(getString().substring(i + 1));
        setString(stringBuffer.toString());
    }

    public void setString(final String str) {
        l.getHandler().post(new Runnable() {
            public void run() {
                z.this.hP.setText(str);
            }
        });
    }

    public int size() {
        return getString().length();
    }

    public void t(int i, int i2) {
        StringBuffer stringBuffer = new StringBuffer(getString());
        stringBuffer.delete(i, i + i2);
        setString(stringBuffer.toString());
    }
}
