package com.a.a.e;

import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import org.meteoroid.core.c;
import org.meteoroid.core.i;
import org.meteoroid.core.l;
import org.meteoroid.core.m;

public abstract class k implements m.a {
    public static final int ALERT = 5;
    public static final int CANVAS = 0;
    public static final int FORM = 2;
    public static final int GAMECANVAS = 1;
    public static final int LIST = 3;
    public static final int TEXTBOX = 4;
    protected j gR = null;
    public boolean gS = false;
    private aa gT;
    private ArrayList<f> gU = new ArrayList<>();
    private g gV = null;
    private int height = -1;
    private boolean isVisible;
    private String title;
    private int width = -1;

    k() {
    }

    private void cd() {
        if (this.isVisible) {
            l.getHandler().post(new Runnable() {
                public void run() {
                    k.this.bs();
                    k.this.br();
                }
            });
        }
    }

    private void ci() {
        this.width = c.mN.getWidth();
        this.height = c.mN.getHeight();
    }

    public void a(aa aaVar) {
        this.gT = aaVar;
    }

    public void a(g gVar) {
        this.gV = gVar;
    }

    /* access modifiers changed from: protected */
    public void aB() {
    }

    /* access modifiers changed from: protected */
    public void af(int i) {
    }

    /* access modifiers changed from: protected */
    public void ag(int i) {
    }

    /* access modifiers changed from: protected */
    public void ay(int i) {
    }

    public void b(f fVar) {
        boolean z = false;
        Log.d("Displayable", fVar.bK() + " has added.");
        int priority = fVar.getPriority();
        int i = 0;
        while (true) {
            if (i >= this.gU.size()) {
                break;
            } else if (priority <= this.gU.get(i).getPriority()) {
                this.gU.add(i, fVar);
                cd();
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (!z) {
            this.gU.add(fVar);
            cd();
        }
    }

    public boolean bE() {
        return false;
    }

    public void br() {
        Iterator<f> it = this.gU.iterator();
        while (it.hasNext()) {
            i.a(it.next());
        }
        if (!this.isVisible) {
            try {
                ch();
                Log.d("Displayable", getClass().getSimpleName() + " shownotify called.");
            } catch (Exception e) {
                Log.w("Displayable", e);
            }
            this.isVisible = true;
        }
    }

    public void bs() {
        Iterator<f> it = this.gU.iterator();
        while (it.hasNext()) {
            i.b(it.next());
        }
        if (this.isVisible) {
            try {
                aB();
                Log.d("Displayable", getClass().getSimpleName() + " hidenotify called.");
            } catch (Exception e) {
                Log.w("Displayable", e);
            }
            this.isVisible = false;
        }
    }

    public abstract int bx();

    public void c(f fVar) {
        this.gU.remove(fVar);
        cd();
    }

    public ArrayList<f> ce() {
        return this.gU;
    }

    public aa cf() {
        return this.gT;
    }

    public g cg() {
        return this.gV;
    }

    /* access modifiers changed from: protected */
    public void ch() {
    }

    public int getHeight() {
        if (this.height == -1) {
            ci();
        }
        return this.height;
    }

    public String getTitle() {
        return this.title;
    }

    public int getWidth() {
        if (this.width == -1) {
            ci();
        }
        return this.width;
    }

    public boolean isShown() {
        return this.isVisible;
    }

    /* access modifiers changed from: protected */
    public void l(int i, int i2) {
    }

    /* access modifiers changed from: protected */
    public void m(int i, int i2) {
    }

    /* access modifiers changed from: protected */
    public void o(int i, int i2) {
    }

    /* access modifiers changed from: protected */
    public void p(int i, int i2) {
    }

    public void setTitle(String str) {
        this.title = str;
    }
}
