package com.a.a.e;

import android.app.Activity;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import org.meteoroid.core.l;

public class i extends r {
    public static final int DATE = 1;
    public static final int DATE_TIME = 3;
    public static final int TIME = 2;
    private Calendar calendar;
    /* access modifiers changed from: private */
    public int gA;
    /* access modifiers changed from: private */
    public int gB;
    /* access modifiers changed from: private */
    public int gC;
    /* access modifiers changed from: private */
    public int gD;
    /* access modifiers changed from: private */
    public int gE;
    DatePicker.OnDateChangedListener gF = new DatePicker.OnDateChangedListener() {
        public void onDateChanged(DatePicker datePicker, int i, int i2, int i3) {
            int unused = i.this.gA = i;
            int unused2 = i.this.gB = i2;
            int unused3 = i.this.gC = i3;
            i.this.gc.setText(i.this.label + i.this.gA + "/" + (i.this.gB + 1) + "/" + i.this.gC);
        }
    };
    TimePicker.OnTimeChangedListener gG = new TimePicker.OnTimeChangedListener() {
        public void onTimeChanged(TimePicker timePicker, int i, int i2) {
            int unused = i.this.gD = i;
            int unused2 = i.this.gE = i2;
            i.this.gc.setText(i.this.label + i.this.gD + ":" + i.this.gE);
        }
    };
    /* access modifiers changed from: private */
    public TextView gc;
    private LinearLayout gd;
    private Date gx;
    private DatePicker gy;
    private TimePicker gz;
    /* access modifiers changed from: private */
    public String label;
    private int mode;

    public i(String str, int i) {
        super(str);
        g(str, i);
    }

    public i(String str, int i, TimeZone timeZone) {
        super(str);
        g(str, i);
    }

    public void aB(int i) {
        this.mode = i;
    }

    /* renamed from: bT */
    public LinearLayout getView() {
        return this.gd;
    }

    public int bU() {
        return this.mode;
    }

    public void g(String str, int i) {
        this.label = str;
        this.mode = i;
        Activity activity = l.getActivity();
        this.calendar = Calendar.getInstance();
        this.gA = this.calendar.get(1);
        this.gB = this.calendar.get(2);
        this.gC = this.calendar.get(5);
        this.gD = this.calendar.get(10);
        this.gE = this.calendar.get(12);
        this.gd = new LinearLayout(activity);
        this.gd.setBackgroundColor(-16777216);
        this.gd.setOrientation(1);
        this.gc = new TextView(activity);
        this.gc.setText(str);
        this.gd.addView(this.gc, new ViewGroup.LayoutParams(-2, -2));
        this.gy = new DatePicker(activity);
        this.gz = new TimePicker(activity);
        this.gz.setOnTimeChangedListener(this.gG);
        this.gy.init(this.gA, this.gB, this.gC, this.gF);
        switch (i) {
            case 1:
                this.gd.addView(this.gy, new ViewGroup.LayoutParams(-2, -2));
                return;
            case 2:
                this.gd.addView(this.gz, new ViewGroup.LayoutParams(-2, -2));
                return;
            case 3:
                this.gd.addView(this.gy, new ViewGroup.LayoutParams(-2, -2));
                this.gd.addView(this.gz, new ViewGroup.LayoutParams(-2, -2));
                return;
            default:
                return;
        }
    }

    public Date getDate() {
        return this.gx;
    }

    public void setDate(Date date) {
        this.gx = date;
    }
}
