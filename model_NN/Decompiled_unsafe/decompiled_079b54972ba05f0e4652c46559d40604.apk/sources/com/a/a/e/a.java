package com.a.a.e;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.fasterxml.jackson.core.util.BufferRecycler;
import java.util.Iterator;
import org.meteoroid.core.l;

public class a extends v {
    public static final int FOREVER = -2;
    protected static Context context = null;
    public static final f fW = new f("", 4, 0);
    private String fX;
    private p fY;
    private b fZ;
    private int ga;
    private n gb;
    private TextView gc;
    private LinearLayout gd;
    private ImageView ge;
    private Activity gf;

    public a(String str) {
        this(str, null, null, b.gh);
    }

    public a(String str, String str2, p pVar, b bVar) {
        super(str);
        this.fX = null;
        this.fY = null;
        this.gf = l.getActivity();
        this.gd = new LinearLayout(this.gf);
        this.gd.setOrientation(1);
        this.gc = new TextView(this.gf);
        this.ge = new ImageView(this.gf);
        if (str2 != null) {
            setString(str2);
        }
        if (pVar != null) {
            c(pVar);
        }
        if (bVar != null) {
            a(bVar);
        }
        this.gd.addView(this.gc, new ViewGroup.LayoutParams(-2, -2));
        this.gd.addView(this.ge, new ViewGroup.LayoutParams(-2, -2));
    }

    public void a(b bVar) {
        this.fZ = bVar;
    }

    public void a(n nVar) {
        if (nVar == null && this.gb != null) {
            this.gd.removeView(this.gb.getView());
        }
        this.gb = nVar;
        if (nVar != null) {
            this.gd.addView(nVar.getView(), new ViewGroup.LayoutParams(-2, -2));
        }
    }

    public void br() {
        super.br();
        l.getHandler().post(new Runnable() {
            public void run() {
                Iterator<f> it = a.this.ce().iterator();
                while (it.hasNext()) {
                    f next = it.next();
                    Log.d("Screen", "Add command " + next.bK());
                    ((ViewGroup) a.this.getView()).addView(next.bM());
                }
                a.this.getView().requestLayout();
            }
        });
    }

    public void bs() {
        super.bs();
        l.getHandler().post(new Runnable() {
            public void run() {
                Iterator<f> it = a.this.ce().iterator();
                while (it.hasNext()) {
                    f next = it.next();
                    Log.d("Screen", "Remove command " + next.bK());
                    ((ViewGroup) a.this.getView()).removeView(next.bM());
                }
                a.this.getView().requestLayout();
            }
        });
    }

    public p bt() {
        return this.fY;
    }

    public b bu() {
        return this.fZ;
    }

    public int bv() {
        return BufferRecycler.DEFAULT_WRITE_CONCAT_BUFFER_LEN;
    }

    public n bw() {
        return this.gb;
    }

    public int bx() {
        return 5;
    }

    public void c(p pVar) {
        this.fY = pVar;
        this.ge.setImageBitmap(pVar.getBitmap());
    }

    public String getString() {
        return this.fX;
    }

    public int getTimeout() {
        return this.ga;
    }

    public View getView() {
        return this.gd;
    }

    public void setString(String str) {
        this.fX = str;
        this.gc.setText(str);
    }

    public void setTimeout(int i) {
        this.ga = i;
    }
}
