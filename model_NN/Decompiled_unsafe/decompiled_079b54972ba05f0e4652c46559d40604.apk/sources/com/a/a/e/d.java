package com.a.a.e;

public interface d {
    public static final int EXCLUSIVE = 1;
    public static final int IMPLICIT = 3;
    public static final int MULTIPLE = 2;
    public static final int POPUP = 4;
    public static final int TEXT_WRAP_DEFAULT = 0;
    public static final int TEXT_WRAP_OFF = 2;
    public static final int TEXT_WRAP_ON = 1;

    int a(String str, p pVar);

    int a(boolean[] zArr);

    void a(int i, l lVar);

    void a(int i, String str, p pVar);

    l as(int i);

    p at(int i);

    boolean au(int i);

    void av(int i);

    void b(int i, String str, p pVar);

    void b(boolean[] zArr);

    void bF();

    int bG();

    int bH();

    void delete(int i);

    void f(int i, boolean z);

    String getString(int i);

    int size();
}
