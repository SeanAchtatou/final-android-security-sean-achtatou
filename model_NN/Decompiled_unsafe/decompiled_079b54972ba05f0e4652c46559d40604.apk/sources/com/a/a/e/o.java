package com.a.a.e;

public class o {
    public static final int BASELINE = 64;
    public static final int BOTTOM = 32;
    public static final int DOTTED = 1;
    public static final int HCENTER = 1;
    public static final int LEFT = 4;
    public static final int RIGHT = 8;
    public static final int SOLID = 0;
    public static final int TOP = 16;
    public static final int VCENTER = 2;
    private static final StringBuffer hr = new StringBuffer();
    private int color;
    private int ho = 0;
    private int hq = 0;

    public void a(char c, int i, int i2, int i3) {
        hr.delete(0, hr.length());
        hr.append(c);
        a(hr.toString(), i, i2, i3);
    }

    public void a(l lVar) {
    }

    public void a(p pVar, int i, int i2, int i3) {
    }

    public void a(p pVar, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
    }

    public void a(String str, int i, int i2, int i3) {
    }

    public void a(String str, int i, int i2, int i3, int i4, int i5) {
    }

    public void a(char[] cArr, int i, int i2, int i3, int i4, int i5) {
        hr.delete(0, hr.length());
        hr.append(cArr, i, i2);
        a(hr.toString(), i3, i4, i5);
    }

    public void a(int[] iArr, int i, int i2, int i3, int i4, int i5, int i6, boolean z) {
    }

    public void aH(int i) {
        if (i < 0 || i > 255) {
            throw new IllegalArgumentException();
        }
        j(i, i, i);
    }

    public void aI(int i) {
    }

    public int aJ(int i) {
        return -1;
    }

    public void b(int i, int i2, int i3, int i4, int i5, int i6) {
    }

    public void c(int i, int i2, int i3, int i4, int i5, int i6) {
    }

    public void c(int i, int i2, int i3, int i4, int i5, int i6, int i7) {
    }

    public int cA() {
        return this.ho;
    }

    public int cB() {
        return this.hq;
    }

    public void cC() {
    }

    public int cq() {
        return getColor() & 255;
    }

    public int cr() {
        return -1;
    }

    public int cs() {
        return -1;
    }

    public int ct() {
        return -1;
    }

    public int cu() {
        return -1;
    }

    public l cv() {
        return null;
    }

    public int cw() {
        return ((cy() + cx()) + cq()) / 3;
    }

    public int cx() {
        return (getColor() >> 8) & 255;
    }

    public int cy() {
        return (getColor() >> 16) & 255;
    }

    public int cz() {
        return -1;
    }

    public void d(int i, int i2, int i3, int i4, int i5, int i6) {
    }

    public void e(int i, int i2, int i3, int i4, int i5, int i6) {
    }

    public void f(int i, int i2, int i3, int i4) {
    }

    public void f(int i, int i2, int i3, int i4, int i5, int i6) {
    }

    public void g(int i, int i2, int i3, int i4) {
    }

    public int getColor() {
        return this.color;
    }

    public void h(int i, int i2, int i3, int i4) {
    }

    public void i(int i, int i2, int i3, int i4) {
    }

    public void j(int i, int i2, int i3) {
        setColor((i2 << 8) + i3 + (i << 16));
    }

    public void j(int i, int i2, int i3, int i4) {
    }

    public void setColor(int i) {
        this.color = i;
    }

    public void translate(int i, int i2) {
        this.ho += i;
        this.hq += i2;
    }
}
