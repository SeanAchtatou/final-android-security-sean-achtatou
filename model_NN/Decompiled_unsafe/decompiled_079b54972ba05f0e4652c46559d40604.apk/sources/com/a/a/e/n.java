package com.a.a.e;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import org.meteoroid.core.l;

public class n extends r {
    public static final int CONTINUOUS_IDLE = 0;
    public static final int CONTINUOUS_RUNNING = 2;
    public static final int INCREMENTAL_IDLE = 1;
    public static final int INCREMENTAL_UPDATING = 3;
    public static final int INDEFINITE = -1;
    private TextView gc;
    private LinearLayout gd;
    private boolean hj;
    private int hk;
    private ProgressBar hl;
    private SeekBar hm;
    private int value;

    public n(String str, boolean z, int i, int i2) {
        super(str);
        this.hj = z;
        this.hk = i;
        Activity activity = l.getActivity();
        this.gd = new LinearLayout(activity);
        this.gd.setOrientation(1);
        this.gc = new TextView(activity);
        this.gc.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        this.gc.setTextAppearance(this.gc.getContext(), 16973890);
        if (z) {
            this.hm = new SeekBar(activity, null, 16842875);
            if (i == 0) {
                this.hm.setMax(getView().getWidth());
            } else {
                this.hm.setMax(i);
            }
            this.hm.setProgress(i2);
            this.gd.addView(this.hm, new ViewGroup.LayoutParams(-1, -2));
        } else if (i != -1) {
            this.hl = new ProgressBar(activity, null, 16842872);
            this.hl.setMinimumWidth(1);
            if (i == 0) {
                this.hl.setMax(getView().getWidth());
            } else {
                this.hl.setMax(i);
            }
            this.hl.incrementProgressBy(1);
            this.hl.setProgress(i2);
            this.hl.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (n.this.cN() != null && n.this.hx != null) {
                        n.this.cN().a(n.this.hx, n.this);
                    }
                }
            });
            this.gd.addView(this.hl, new ViewGroup.LayoutParams(-1, -2));
        } else if (i2 == 1) {
            this.hl = new ProgressBar(activity, null, 16842874);
            this.hl.setIndeterminate(true);
            this.gd.addView(this.hl, new ViewGroup.LayoutParams(-2, -2));
        } else if (i2 == 2) {
            this.hl = new ProgressBar(activity);
            this.gd.addView(this.hl, new ViewGroup.LayoutParams(-2, -2));
        }
    }

    /* renamed from: bT */
    public LinearLayout getView() {
        return this.gd;
    }

    public boolean cp() {
        return this.hj;
    }

    public int getMaxValue() {
        return this.hk;
    }

    public int getValue() {
        return this.value;
    }

    public void setMaxValue(int i) {
        this.hk = i;
    }

    public void setValue(int i) {
        this.value = i;
        if (this.hl != null) {
            this.hl.setProgress(i);
        }
        if (this.hm != null) {
            this.hm.setProgress(i);
        }
        this.gd.postInvalidate();
    }
}
