package com.a.a.e;

import android.app.Activity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import org.meteoroid.core.l;

public class u extends v implements AdapterView.OnItemClickListener, d {
    public static f hz;
    private LinearLayout gd;
    /* access modifiers changed from: private */
    public ArrayAdapter<String> gm;
    /* access modifiers changed from: private */
    public ArrayAdapter<p> gn;
    /* access modifiers changed from: private */
    public ListView gp;
    private int gr;
    private int hA;

    public u(String str, int i) {
        this(str, i, null, null);
    }

    public u(String str, int i, String[] strArr, p[] pVarArr) {
        super(str);
        this.gr = i;
        strArr = strArr == null ? new String[0] : strArr;
        pVarArr = pVarArr == null ? new p[0] : pVarArr;
        hz = new f("", 1, 0);
        Activity activity = l.getActivity();
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(Arrays.asList(strArr));
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(Arrays.asList(pVarArr));
        this.gm = new ArrayAdapter<>(activity, aw(i), arrayList);
        this.gm.setNotifyOnChange(true);
        this.gn = new ArrayAdapter<>(activity, aw(i), arrayList2);
        this.gp = new ListView(activity);
        this.gp.setAdapter((ListAdapter) this.gm);
        this.gp.setBackgroundColor(-16777216);
        this.gp.setChoiceMode(ax(i));
        this.gp.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                return keyEvent.getAction() == 0 && i == 4;
            }
        });
        this.gp.setOnItemClickListener(this);
        this.gd = new LinearLayout(l.getActivity());
        this.gd.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.gd.setOrientation(1);
        this.gd.addView(this.gp);
    }

    private int aw(int i) {
        switch (i) {
            case 1:
                return 17367055;
            case 2:
                return 17367056;
            case 3:
            default:
                return 17367043;
        }
    }

    private int ax(int i) {
        switch (i) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 0;
            default:
                throw new IllegalArgumentException("Invalid listType.");
        }
    }

    public int a(final String str, final p pVar) {
        l.getHandler().post(new Runnable() {
            public void run() {
                if (str != null) {
                    u.this.gm.add(str);
                }
                if (pVar != null) {
                    u.this.gn.add(pVar);
                }
            }
        });
        return this.gm.getCount() + 1;
    }

    public int a(boolean[] zArr) {
        if (zArr.length > this.gm.getCount()) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < zArr.length; i++) {
            zArr[i] = this.gp.isItemChecked(i);
        }
        return zArr.length;
    }

    public void a(int i, l lVar) {
    }

    public void a(final int i, final String str, final p pVar) {
        l.getHandler().post(new Runnable() {
            public void run() {
                u.this.gm.insert(str, i);
                u.this.gn.insert(pVar, i);
            }
        });
    }

    public l as(int i) {
        return l.cj();
    }

    public p at(int i) {
        return this.gn.getItem(i);
    }

    public boolean au(int i) {
        if (this.gr == 3 && i == this.hA) {
            return true;
        }
        return this.gp.isItemChecked(i);
    }

    public void av(int i) {
    }

    public void b(final int i, final String str, final p pVar) {
        l.getHandler().post(new Runnable() {
            public void run() {
                u.this.a(i, str, pVar);
                u.this.delete(i + 1);
            }
        });
    }

    public void b(final boolean[] zArr) {
        if (zArr.length > this.gm.getCount()) {
            throw new IllegalArgumentException();
        }
        l.getHandler().post(new Runnable() {
            public void run() {
                for (int i = 0; i < zArr.length; i++) {
                    u.this.gp.setItemChecked(i, zArr[i]);
                }
            }
        });
    }

    public void bF() {
        l.getHandler().post(new Runnable() {
            public void run() {
                u.this.gm.clear();
                u.this.gn.clear();
            }
        });
    }

    public int bG() {
        return 0;
    }

    public int bH() {
        if (this.gr == 2) {
            return -1;
        }
        if (this.gr == 3) {
            return this.hA;
        }
        for (int i = 0; i < this.gp.getCount(); i++) {
            if (this.gp.isItemChecked(i)) {
                return i;
            }
        }
        return -1;
    }

    /* renamed from: bT */
    public LinearLayout getView() {
        return this.gd;
    }

    public void br() {
        super.br();
        l.getHandler().post(new Runnable() {
            public void run() {
                Iterator<f> it = u.this.ce().iterator();
                while (it.hasNext()) {
                    f next = it.next();
                    Log.d("Screen", "Add command " + next.bK());
                    u.this.getView().addView(next.bM());
                }
                u.this.getView().requestLayout();
            }
        });
    }

    public void bs() {
        super.bs();
        l.getHandler().post(new Runnable() {
            public void run() {
                Iterator<f> it = u.this.ce().iterator();
                while (it.hasNext()) {
                    f next = it.next();
                    Log.d("Screen", "Remove command " + next.bK());
                    u.this.getView().removeView(next.bM());
                }
                u.this.getView().requestLayout();
            }
        });
    }

    public int bx() {
        return 3;
    }

    public void delete(final int i) {
        l.getHandler().post(new Runnable() {
            public void run() {
                u.this.gm.remove(u.this.gm.getItem(i));
                u.this.gn.remove(u.this.gn.getItem(i));
            }
        });
    }

    public void e(f fVar) {
        hz = fVar;
    }

    public void f(final int i, final boolean z) {
        l.getHandler().post(new Runnable() {
            public void run() {
                u.this.gp.setItemChecked(i, z);
            }
        });
    }

    public String getString(int i) {
        return this.gm.getItem(i);
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        if (this.gr == 3) {
            this.hA = i;
            cg().a(hz, this);
        }
    }

    public int size() {
        return this.gm.getCount();
    }
}
