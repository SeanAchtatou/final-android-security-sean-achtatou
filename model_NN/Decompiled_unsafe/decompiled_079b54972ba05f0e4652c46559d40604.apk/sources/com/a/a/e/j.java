package com.a.a.e;

import android.content.DialogInterface;
import android.os.Message;
import android.util.Log;
import com.a.a.r.a;
import javax.microedition.midlet.MIDlet;
import org.meteoroid.core.c;
import org.meteoroid.core.h;
import org.meteoroid.core.l;
import org.meteoroid.core.m;
import org.meteoroid.plugin.device.MIDPDevice;

public final class j implements h.a {
    public static final int ALERT = 3;
    public static final int CHOICE_GROUP_ELEMENT = 2;
    public static final int COLOR_BACKGROUND = 0;
    public static final int COLOR_BORDER = 4;
    public static final int COLOR_FOREGROUND = 1;
    public static final int COLOR_HIGHLIGHTED_BACKGROUND = 2;
    public static final int COLOR_HIGHLIGHTED_BORDER = 5;
    public static final int COLOR_HIGHLIGHTED_FOREGROUND = 3;
    public static final int LIST_ELEMENT = 1;
    public static final String LOG_TAG = "Display";
    private static k gI = null;
    public static final j gJ = new j();
    private static MIDlet gK;
    private static volatile int gO;
    private Message gL = h.a(MIDPDevice.MSG_MIDP_DISPLAY_CALL_SERIALLY, null, 0, h.MSG_ARG2_DONT_RECYCLE_ME);
    private boolean gM;
    private volatile boolean gN = true;

    private j() {
        h.a(this);
    }

    public static j a(MIDlet mIDlet) {
        if (mIDlet != null) {
            gK = mIDlet;
        }
        return gJ;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.meteoroid.core.m.a(java.lang.String, java.lang.String, android.view.View, boolean, android.content.DialogInterface$OnCancelListener):void
     arg types: [java.lang.String, ?[OBJECT, ARRAY], android.view.View, int, ?[OBJECT, ARRAY]]
     candidates:
      org.meteoroid.core.m.a(java.lang.String, java.lang.String, java.lang.String[], android.content.DialogInterface$OnClickListener, boolean):void
      org.meteoroid.core.m.a(java.lang.String, java.lang.String, android.view.View, boolean, android.content.DialogInterface$OnCancelListener):void */
    private final void a(final a aVar) {
        int timeout = aVar.getTimeout();
        if (!aVar.ce().isEmpty() || aVar.cg() == null) {
            m.a(aVar.getTitle(), (String) null, aVar.getView(), false, (DialogInterface.OnCancelListener) null);
        } else {
            m.a(aVar.getTitle(), aVar.getString(), "关闭", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    aVar.cg().a(a.fW, aVar);
                }
            });
        }
        if (timeout != -2) {
            if (timeout <= 0) {
                timeout = aVar.bv();
            }
            m.m((long) timeout);
        }
    }

    private final void a(c cVar) {
        this.gN = false;
        try {
            cVar.c(bZ().cD());
        } catch (Exception e) {
            Log.w(LOG_TAG, "Exception in paint!" + e);
            e.printStackTrace();
        }
        cb();
        this.gN = true;
    }

    private final void b(c cVar) {
        h.ci(a.MSG_DEVICE_REQUEST_REFRESH);
    }

    private final void b(Runnable runnable) {
        this.gL.obj = runnable;
        if (!h.d(this.gL)) {
            h.c(this.gL);
        }
    }

    private static final MIDPDevice bZ() {
        return (MIDPDevice) c.mN;
    }

    private final boolean ca() {
        return org.meteoroid.core.a.ca();
    }

    private final boolean cc() {
        return gI != null && (gI.bx() == 0 || gI.bx() == 1);
    }

    public void a(a aVar, k kVar) {
        a(kVar);
        a(aVar);
    }

    public void a(f fVar) {
        if (gI != null) {
            gI.cg().a(fVar, gI);
        }
    }

    public void a(k kVar) {
        if (kVar != gI && kVar != null) {
            if (kVar.bx() != 5) {
                if (gI != null && gI.bx() == 5) {
                    m.ik();
                }
                if (gI != null && gI.isShown()) {
                    gI.gR = null;
                }
                if (gI == null || !((gI.bx() == 0 || gI.bx() == 1) && (kVar.bx() == 0 || kVar.bx() == 1))) {
                    m.a(kVar);
                    return;
                }
                gI.bs();
                gI.gR = gJ;
                m.b(kVar);
                gI = kVar;
                gI.br();
                h.d(m.MSG_VIEW_CHANGED, kVar);
                Log.d(LOG_TAG, "Fast switch canvas end.");
                return;
            }
            a((a) kVar);
        }
    }

    public void a(r rVar) {
        a(rVar.cK());
    }

    public void a(Runnable runnable) {
        b(runnable);
    }

    public boolean aC(int i) {
        return false;
    }

    public int aD(int i) {
        return c.mN.k("BestImageWidth", 15);
    }

    public int aE(int i) {
        return c.mN.k("BestImageHeight", 15);
    }

    public final boolean aF(int i) {
        l.ck(i);
        return true;
    }

    public final void af(int i) {
        if (this.gM) {
            this.gM = false;
        }
        if (gI != null && ca()) {
            if (!gI.ce().isEmpty()) {
                if (i == ((MIDPDevice) c.mN).cn(1)) {
                    a(gI.ce().get(0));
                } else if (gI.ce().size() > 1 && i == ((MIDPDevice) c.mN).cn(2)) {
                    a(gI.ce().get(1));
                }
            }
            if (gI.bx() == 1) {
                ((com.a.a.f.a) gI).u(0, i);
            }
            gI.af(i);
        }
    }

    public final void ag(int i) {
        if (!this.gM && gI != null && ca()) {
            if (gI.bx() == 1) {
                ((com.a.a.f.a) gI).u(1, i);
            }
            gI.ag(i);
        }
    }

    public final void ay(int i) {
        if (!this.gM && gI != null && ca()) {
            gI.ay(i);
        }
    }

    public boolean b(Message message) {
        if ((message.what == 44287 || message.what == 40965) && cc()) {
            if (this.gN) {
                Log.d(LOG_TAG, "Repaint later now.");
                a((c) gI);
            } else {
                if (gO > 0) {
                    gO--;
                }
                Log.d(LOG_TAG, "Still not ready for repaint. Try later for " + gO);
            }
        } else if (message.what == 23041) {
            if (message.obj != null && (message.obj instanceof k)) {
                gI = (k) message.obj;
                gI.gR = this;
                this.gM = true;
                if (gI.bx() == 0) {
                    c((c) gI);
                }
            }
        } else if (message.what == 44035) {
            Log.d(LOG_TAG, "MIDP_DISPLAY_CALL_SERIALLY");
            if (message.obj != null) {
                ((Runnable) message.obj).run();
            }
            return true;
        }
        return false;
    }

    public final void bD() {
        Thread.yield();
    }

    public int bV() {
        return c.mN.k("alphaLevelCount", 255);
    }

    public int bW() {
        return c.mN.k("colorCount", z.CONSTRAINT_MASK);
    }

    public k bX() {
        return gI;
    }

    public boolean bY() {
        return c.mN.f("isColor", true);
    }

    public final void c(c cVar) {
        if (!cc() || cVar != gI) {
            Log.d(LOG_TAG, "Not a valid canvas. Skip repaint.");
        } else if (this.gN) {
            gO = 0;
            a(cVar);
        } else if (gO <= 1) {
            b(cVar);
            gO++;
        }
    }

    public final void cb() {
        bZ().ix();
    }

    public int getColor(int i) {
        switch (i) {
            case 0:
            case 3:
            case 5:
                return 16777215;
            case 1:
            case 2:
            case 4:
            default:
                return 0;
        }
    }

    public int l(boolean z) {
        return z ? 1 : 0;
    }

    public final synchronized void l(int i, int i2) {
        if (this.gM) {
            this.gM = false;
        }
        if (gI != null && ca()) {
            try {
                gI.l(i, i2);
            } catch (Exception e) {
            }
        }
    }

    public final void l(o oVar) {
        if (cc()) {
            ((c) gI).c(oVar);
        }
    }

    public final synchronized void m(int i, int i2) {
        if (!this.gM) {
            if (gI != null && ca()) {
                try {
                    gI.m(i, i2);
                } catch (Exception e) {
                }
            }
        }
    }

    public final synchronized void o(int i, int i2) {
        if (!this.gM) {
            if (gI != null && ca()) {
                try {
                    gI.o(i, i2);
                } catch (Exception e) {
                }
            }
        }
    }

    public void p(int i, int i2) {
        if (gI != null) {
            gI.p(i, i2);
        }
    }
}
