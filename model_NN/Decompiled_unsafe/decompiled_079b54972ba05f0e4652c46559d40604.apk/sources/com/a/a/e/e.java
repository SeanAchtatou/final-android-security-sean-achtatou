package com.a.a.e;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Arrays;
import org.meteoroid.core.l;

public class e extends r implements d {
    /* access modifiers changed from: private */
    public ArrayAdapter<String> gm;
    private ArrayAdapter<p> gn;
    private LinearLayout go;
    /* access modifiers changed from: private */
    public ListView gp;
    private Spinner gq;
    private int gr;

    public e(String str, int i) {
        this(str, i, null, null);
    }

    public e(String str, int i, String[] strArr, p[] pVarArr) {
        super(str);
        this.gr = i;
        Activity activity = l.getActivity();
        this.go = new LinearLayout(activity);
        this.go.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        this.go.setOrientation(1);
        strArr = strArr == null ? new String[0] : strArr;
        pVarArr = pVarArr == null ? new p[0] : pVarArr;
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(Arrays.asList(strArr));
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(Arrays.asList(pVarArr));
        this.gm = new ArrayAdapter<>(activity, aw(i), arrayList);
        this.gm.setNotifyOnChange(true);
        this.gn = new ArrayAdapter<>(activity, aw(i), arrayList2);
        TextView textView = new TextView(activity);
        textView.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        textView.setText(str);
        this.go.addView(textView);
        if (i == 4) {
            this.gq = new Spinner(activity);
            this.gq.setAdapter((SpinnerAdapter) this.gm);
            this.go.addView(this.gq);
            return;
        }
        this.gp = new ListView(activity);
        this.gp.setAdapter((ListAdapter) this.gm);
        this.gp.setBackgroundColor(-16777216);
        this.gp.setChoiceMode(ax(i));
        this.go.addView(this.gp);
        bI();
    }

    private int aw(int i) {
        switch (i) {
            case 1:
                return 17367055;
            case 2:
                return 17367056;
            case 3:
            default:
                return 17367043;
            case 4:
                return 17367048;
        }
    }

    private int ax(int i) {
        switch (i) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
            default:
                return 0;
        }
    }

    private void bI() {
        if (this.gp != null && this.gm != null) {
            l.getHandler().post(new Runnable() {
                public void run() {
                    try {
                        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(e.this.gp.getWidth(), Integer.MIN_VALUE);
                        int i = 0;
                        for (int i2 = 0; i2 < e.this.gm.getCount(); i2++) {
                            View view = e.this.gm.getView(i2, null, e.this.gp);
                            view.measure(makeMeasureSpec, 0);
                            i += view.getMeasuredHeight();
                        }
                        ViewGroup.LayoutParams layoutParams = e.this.gp.getLayoutParams();
                        layoutParams.height = i + (e.this.gp.getDividerHeight() * (e.this.gm.getCount() - 1));
                        e.this.gp.setLayoutParams(layoutParams);
                        e.this.gp.requestLayout();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    }

    public int a(String str, p pVar) {
        if (str != null) {
            this.gm.add(str);
        }
        if (pVar != null) {
            this.gn.add(pVar);
        }
        if (this.gr != 4) {
            bI();
        }
        return this.gm.getCount();
    }

    public int a(boolean[] zArr) {
        if (zArr.length > this.gm.getCount()) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < zArr.length; i++) {
            zArr[i] = this.gp.isItemChecked(i);
        }
        return zArr.length;
    }

    public void a(int i, l lVar) {
    }

    public void a(int i, String str, p pVar) {
        this.gm.insert(str, i);
        this.gn.insert(pVar, i);
        if (this.gr != 4) {
            bI();
        }
    }

    public l as(int i) {
        return l.cj();
    }

    public p at(int i) {
        return this.gn.getItem(i);
    }

    public boolean au(int i) {
        return this.gr == 4 ? i == this.gq.getSelectedItemPosition() : this.gp.isItemChecked(i);
    }

    public void av(int i) {
    }

    public void b(int i, String str, p pVar) {
        a(i, str, pVar);
        delete(i + 1);
    }

    public void b(boolean[] zArr) {
        if (zArr.length > this.gm.getCount()) {
            throw new IllegalArgumentException();
        }
        for (int i = 0; i < zArr.length; i++) {
            this.gp.setItemChecked(i, zArr[i]);
        }
    }

    public void bF() {
        this.gm.clear();
        this.gn.clear();
        if (this.gr != 4) {
            bI();
        }
    }

    public int bG() {
        return 0;
    }

    public int bH() {
        if (this.gr == 2) {
            return -1;
        }
        if (this.gr == 4) {
            return this.gq.getSelectedItemPosition();
        }
        for (int i = 0; i < this.gp.getCount(); i++) {
            if (this.gp.isItemChecked(i)) {
                return i;
            }
        }
        return -1;
    }

    public void delete(int i) {
        this.gm.remove(this.gm.getItem(i));
        this.gn.remove(this.gn.getItem(i));
        if (this.gr != 4) {
            bI();
        }
    }

    public void f(int i, boolean z) {
        if (this.gr == 4) {
            this.gq.setSelection(i);
        } else {
            this.gp.setItemChecked(i, z);
        }
    }

    public String getString(int i) {
        return this.gm.getItem(i);
    }

    public View getView() {
        return this.go;
    }

    public int size() {
        return this.gm.getCount();
    }
}
