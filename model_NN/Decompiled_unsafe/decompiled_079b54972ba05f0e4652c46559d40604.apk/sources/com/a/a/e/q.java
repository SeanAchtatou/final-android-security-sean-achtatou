package com.a.a.e;

import android.app.Activity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.meteoroid.core.l;

public class q extends r {
    private p fY;
    private TextView gc;
    private LinearLayout gd;
    private ImageView ge;
    private String hu;
    private int hv;
    private int layout;

    public q(String str, p pVar, int i, String str2) {
        this(str, pVar, i, str2, 0);
    }

    public q(String str, p pVar, int i, String str2, int i2) {
        super(str);
        Activity activity = l.getActivity();
        this.gd = new LinearLayout(activity);
        this.gd.setOrientation(1);
        this.gc = new TextView(activity);
        this.gd.addView(this.gc, new ViewGroup.LayoutParams(-1, -2));
        this.ge = new ImageView(activity);
        this.gd.addView(this.ge, new ViewGroup.LayoutParams(-1, -2));
        if (pVar != null) {
            c(pVar);
        }
        aL(i);
        if (str2 != null) {
            U(str2);
        }
        aK(i2);
    }

    public void U(String str) {
        this.hu = str;
        this.gc.setText(str);
        this.gc.postInvalidate();
    }

    public void aK(int i) {
        this.hv = i;
    }

    public void aL(int i) {
        this.layout = i;
    }

    /* renamed from: bT */
    public LinearLayout getView() {
        return this.gd;
    }

    public p bt() {
        return this.fY;
    }

    public void c(p pVar) {
        this.fY = pVar;
        this.ge.setImageBitmap(pVar.bitmap);
        this.ge.postInvalidate();
    }

    public String cE() {
        return this.hu;
    }

    public int cF() {
        return this.hv;
    }

    public int cG() {
        return this.layout;
    }
}
