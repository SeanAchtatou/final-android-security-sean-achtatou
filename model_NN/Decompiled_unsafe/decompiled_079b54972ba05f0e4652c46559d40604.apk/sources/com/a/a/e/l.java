package com.a.a.e;

import java.util.Hashtable;
import org.meteoroid.plugin.device.MIDPDevice;

public final class l {
    public static final int FACE_MONOSPACE = 32;
    public static final int FACE_PROPORTIONAL = 64;
    public static final int FACE_SYSTEM = 0;
    public static final int FONT_INPUT_TEXT = 1;
    public static final int FONT_STATIC_TEXT = 0;
    public static final int SIZE_LARGE = 16;
    public static final int SIZE_MEDIUM = 0;
    public static final int SIZE_SMALL = 8;
    public static final int STYLE_BOLD = 1;
    public static final int STYLE_ITALIC = 2;
    public static final int STYLE_PLAIN = 0;
    public static final int STYLE_UNDERLINED = 4;
    private static final l gX = new l(0, 0, 0);
    private static l[] gY = {gX, gX};
    private static final Hashtable<Integer, l> gZ = new Hashtable<>();
    private int ha;
    private int hb;
    private int size;

    public l(int i, int i2, int i3) {
        this.ha = i;
        this.hb = i2;
        this.size = i3;
    }

    public static l as(int i) {
        if (i == 1 || i == 0) {
            return gY[i];
        }
        throw new IllegalArgumentException("Bad specifier");
    }

    public static l cj() {
        return gX;
    }

    public static l i(int i, int i2, int i3) {
        Integer num = new Integer(i2 + i3 + i);
        l lVar = gZ.get(num);
        if (lVar != null) {
            return lVar;
        }
        l lVar2 = new l(i, i2, i3);
        gZ.put(num, lVar2);
        return lVar2;
    }

    public int R(String str) {
        return MIDPDevice.d.a(this, str);
    }

    public int a(char[] cArr, int i, int i2) {
        return MIDPDevice.d.a(this, cArr, i, i2);
    }

    public int b(char c) {
        return MIDPDevice.d.a(this, c);
    }

    public int b(String str, int i, int i2) {
        return MIDPDevice.d.a(this, str, i, i2);
    }

    public int ck() {
        return this.ha;
    }

    public boolean cl() {
        return this.hb == 0;
    }

    public boolean cm() {
        return (this.hb & 4) != 0;
    }

    public int cn() {
        return MIDPDevice.d.c(this);
    }

    public int getHeight() {
        return MIDPDevice.d.d(this);
    }

    public int getSize() {
        return this.size;
    }

    public int getStyle() {
        return this.hb;
    }

    public int hashCode() {
        return this.hb + this.size + this.ha;
    }

    public boolean isBold() {
        return (this.hb & 1) != 0;
    }

    public boolean isItalic() {
        return (this.hb & 2) != 0;
    }
}
