package com.a.a.e;

import android.graphics.Typeface;
import android.text.InputFilter;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.Iterator;
import org.meteoroid.core.l;

public class y extends v {
    /* access modifiers changed from: private */
    public TextView gc;
    /* access modifiers changed from: private */
    public LinearLayout gd;
    private int hN;
    private int hO;
    /* access modifiers changed from: private */
    public EditText hP;
    private v hw = null;

    public y(String str, final String str2, final int i, final int i2) {
        super(str);
        l.getHandler().post(new Runnable() {
            public void run() {
                LinearLayout unused = y.this.gd = new LinearLayout(l.getActivity());
                y.this.gd.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
                y.this.gd.setOrientation(1);
                TextView unused2 = y.this.gc = new TextView(l.getActivity());
                y.this.gc.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
                EditText unused3 = y.this.hP = new EditText(l.getActivity());
                y.this.hP.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
                y.this.gd.addView(y.this.gc);
                y.this.gd.addView(y.this.hP);
                switch (i2) {
                    case 0:
                        y.this.hP.setSingleLine(true);
                        y.this.hP.setInputType(1);
                        break;
                    case 1:
                        y.this.hP.setSingleLine(true);
                        y.this.hP.setInputType(48);
                        break;
                    case 2:
                        y.this.hP.setSingleLine(true);
                        y.this.hP.setInputType(2);
                        break;
                    case 3:
                        y.this.hP.setSingleLine(true);
                        y.this.hP.setInputType(3);
                        break;
                    case 4:
                        y.this.hP.setSingleLine(true);
                        y.this.hP.setInputType(16);
                        break;
                    case 5:
                        y.this.hP.setSingleLine(true);
                        y.this.hP.setInputType(12290);
                        break;
                    case 65536:
                        y.this.hP.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        y.this.hP.setTypeface(Typeface.MONOSPACE);
                        break;
                }
                y.this.aN(i);
                y.this.hP.setText(str2 == null ? "" : str2);
                synchronized (y.this) {
                    y.this.notify();
                }
            }
        });
        synchronized (this) {
            try {
                wait(1000);
            } catch (InterruptedException e) {
            }
        }
    }

    public void W(String str) {
    }

    public int a(char[] cArr) {
        return getString().toCharArray().length;
    }

    public void a(char[] cArr, int i, int i2, int i3) {
        h(new String(cArr, i, i2), i3);
    }

    /* access modifiers changed from: protected */
    public void aB() {
        l.hM().hideSoftInputFromWindow(this.hP.getWindowToken(), 2);
        Log.d("TextBox", "Force close soft input.");
    }

    public int aN(int i) {
        this.hP.setFilters(new InputFilter[]{new InputFilter.LengthFilter(i)});
        this.hN = i;
        return i;
    }

    public void aO(int i) {
        this.hO = i;
    }

    public void b(char[] cArr, int i, int i2) {
        setString(new String(cArr, i, i2));
    }

    /* renamed from: bT */
    public LinearLayout getView() {
        return this.gd;
    }

    public void br() {
        super.br();
        l.getHandler().post(new Runnable() {
            public void run() {
                Iterator<f> it = y.this.ce().iterator();
                while (it.hasNext()) {
                    f next = it.next();
                    Log.d("Screen", "Add command " + next.bK());
                    y.this.getView().addView(next.bM());
                }
                y.this.getView().requestLayout();
            }
        });
    }

    public void bs() {
        super.bs();
        l.getHandler().post(new Runnable() {
            public void run() {
                Iterator<f> it = y.this.ce().iterator();
                while (it.hasNext()) {
                    f next = it.next();
                    Log.d("Screen", "Remove command " + next.bK());
                    y.this.getView().removeView(next.bM());
                }
                y.this.getView().requestLayout();
            }
        });
    }

    public int bx() {
        return 4;
    }

    public int cP() {
        return this.hO;
    }

    public int cQ() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public void ch() {
        this.hP.clearFocus();
    }

    public int getMaxSize() {
        return this.hN;
    }

    public String getString() {
        return this.hP.getText().toString();
    }

    public void h(String str, int i) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(getString().substring(0, i));
        stringBuffer.append(str);
        stringBuffer.append(getString().substring(i + 1));
        setString(stringBuffer.toString());
    }

    public void invalidate() {
        if (this.hw != null) {
            getView().postInvalidate();
        }
    }

    public void setString(final String str) {
        l.getHandler().post(new Runnable() {
            public void run() {
                y.this.hP.setText(str);
            }
        });
    }

    public int size() {
        return getString().length();
    }

    public void t(int i, int i2) {
        StringBuffer stringBuffer = new StringBuffer(getString());
        stringBuffer.delete(i, i + i2);
        setString(stringBuffer.toString());
    }
}
