package com.a.a.e;

import android.content.Context;
import android.graphics.Canvas;
import android.view.View;
import android.view.ViewGroup;
import org.meteoroid.core.c;
import org.meteoroid.core.l;
import org.meteoroid.plugin.device.MIDPDevice;

public abstract class h extends r implements View.OnFocusChangeListener {
    protected static final int KEY_PRESS = 4;
    protected static final int KEY_RELEASE = 8;
    protected static final int KEY_REPEAT = 16;
    protected static final int NONE = 0;
    protected static final int POINTER_DRAG = 128;
    protected static final int POINTER_PRESS = 32;
    protected static final int POINTER_RELEASE = 64;
    protected static final int TRAVERSE_HORIZONTAL = 1;
    protected static final int TRAVERSE_VERTICAL = 2;
    private o dZ;
    private a gv = new a(l.getActivity());

    class a extends View {
        public a(Context context) {
            super(context);
        }

        /* access modifiers changed from: protected */
        public void onDraw(Canvas canvas) {
            h.this.a(canvas);
        }
    }

    public h(String str) {
        super(str);
        this.gv.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.gv.setOnFocusChangeListener(this);
    }

    public void a(Canvas canvas) {
        if (this.dZ == null) {
            this.dZ = new MIDPDevice.e(canvas);
        }
        b(this.dZ, canvas.getWidth(), canvas.getHeight());
    }

    /* access modifiers changed from: protected */
    public boolean a(int i, int i2, int i3, int[] iArr) {
        return true;
    }

    /* access modifiers changed from: protected */
    public abstract int aA(int i);

    /* access modifiers changed from: protected */
    public void af(int i) {
    }

    /* access modifiers changed from: protected */
    public void ag(int i) {
    }

    public int ap(int i) {
        if (i > 0) {
            return ((MIDPDevice) c.mN).ap(i);
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public void ay(int i) {
    }

    /* access modifiers changed from: protected */
    public abstract int az(int i);

    /* access modifiers changed from: protected */
    public abstract void b(o oVar, int i, int i2);

    /* access modifiers changed from: protected */
    public final void bC() {
        this.gv.postInvalidate();
    }

    /* renamed from: bO */
    public a getView() {
        this.gv.setMinimumHeight(bS());
        this.gv.setMinimumWidth(bR());
        return this.gv;
    }

    /* access modifiers changed from: protected */
    public final int bP() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public void bQ() {
    }

    /* access modifiers changed from: protected */
    public abstract int bR();

    /* access modifiers changed from: protected */
    public abstract int bS();

    /* access modifiers changed from: protected */
    public final void e(int i, int i2, int i3, int i4) {
        this.gv.postInvalidate(i, i2, i + i3, i2 + i4);
    }

    /* access modifiers changed from: protected */
    public void l(int i, int i2) {
    }

    /* access modifiers changed from: protected */
    public void m(int i, int i2) {
    }

    /* access modifiers changed from: protected */
    public void o(int i, int i2) {
    }

    public void onFocusChange(View view, boolean z) {
        if (view == this.gv && !z) {
            bQ();
        }
    }
}
