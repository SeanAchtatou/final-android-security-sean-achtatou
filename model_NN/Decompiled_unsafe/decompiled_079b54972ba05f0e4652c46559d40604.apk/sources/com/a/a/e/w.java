package com.a.a.e;

import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import org.meteoroid.core.l;

public class w extends r {
    private LinearLayout gd;

    public w(int i, int i2) {
        super("");
        this.gd = new LinearLayout(l.getActivity());
        s(i, i2);
    }

    public w(String str) {
        this(10, 10);
    }

    public View getView() {
        return this.gd;
    }

    public void s(int i, int i2) {
        if (i < 0 || i2 < 0) {
            throw new IllegalArgumentException();
        }
        this.gd.setLayoutParams(new ViewGroup.LayoutParams(i, i2));
    }
}
