package com.a.a.e;

import android.app.Activity;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.meteoroid.core.l;

public class x extends r {
    private TextView gc;
    private LinearLayout gd;
    private final Activity gf;
    private Button gu;
    private String hH;
    private int hI;
    private l hJ;
    private String hK;
    private TextView hL;
    /* access modifiers changed from: private */
    public f hx;

    public x(String str, String str2) {
        this(str, str2, 0);
    }

    public x(String str, String str2, int i) {
        super(str);
        this.gf = l.getActivity();
        this.hx = new f("", 8, 0);
        this.gd = new LinearLayout(this.gf);
        this.gd.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.gd.setOrientation(1);
        this.gc = new TextView(this.gf);
        this.gu = new Button(this.gf);
        this.hL = new TextView(this.gf);
        if (str != null) {
            this.hL.setText(str);
            this.hL.setTextSize(20.0f);
            this.gd.addView(this.hL, new ViewGroup.LayoutParams(-2, -2));
        }
        switch (i) {
            case 0:
                if (str2 != null) {
                    this.gc.setText(str2);
                }
                this.gc.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (x.this.hx != null && x.this.cN() != null) {
                            x.this.cN().a(x.this.hx, x.this);
                        }
                    }
                });
                this.gd.addView(this.gc, new ViewGroup.LayoutParams(-1, -2));
                this.gd.postInvalidate();
                break;
            case 1:
                if (str2 != null) {
                    this.hK = "<a href=tel:" + str2 + ">" + str2 + "</a>";
                }
                this.gc.setText(Html.fromHtml(this.hK));
                this.gc.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (x.this.hx != null && x.this.cN() != null) {
                            x.this.cN().a(x.this.hx, x.this);
                        }
                    }
                });
                this.gd.addView(this.gc, new ViewGroup.LayoutParams(-1, -2));
                this.gd.postInvalidate();
                break;
            case 2:
                this.gu.setText(str2);
                this.gd.addView(this.gu, new ViewGroup.LayoutParams(-2, -2));
                this.gu.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (x.this.hx != null && x.this.cN() != null) {
                            x.this.cN().a(x.this.hx, x.this);
                        }
                    }
                });
                this.gd.postInvalidate();
                break;
        }
        aM(i);
    }

    public void a(l lVar) {
        this.hJ = lVar;
    }

    public void aM(int i) {
        this.hI = i;
    }

    /* renamed from: bT */
    public LinearLayout getView() {
        return this.gd;
    }

    public int cO() {
        return this.hI;
    }

    public l cv() {
        return this.hJ;
    }

    public void d(f fVar) {
        this.hx = fVar;
    }

    public String getText() {
        return this.hH;
    }

    public void setText(String str) {
        this.hH = str;
        this.gc.setText(str);
        this.gc.postInvalidate();
    }
}
