package com.a.a.e;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.meteoroid.core.l;

public class m extends v {
    /* access modifiers changed from: private */
    public List<r> hc;
    private t hd;
    private ScrollView he;
    /* access modifiers changed from: private */
    public LinearLayout hf;

    public m(String str) {
        this(str, null);
    }

    public m(String str, r[] rVarArr) {
        super(str);
        this.hf = new LinearLayout(l.getActivity());
        this.hf.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        this.hf.setOrientation(1);
        this.hc = new ArrayList();
        if (rVarArr != null) {
            for (r b : rVarArr) {
                b(b);
            }
        }
        this.he = new ScrollView(l.getActivity());
        this.he.addView(this.hf);
        this.he.setVerticalScrollBarEnabled(true);
        this.he.setVerticalFadingEdgeEnabled(false);
        this.he.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
    }

    public int S(String str) {
        if (str != null) {
            return b(new x("", str));
        }
        throw new NullPointerException();
    }

    public void a(final int i, final r rVar) {
        this.hc.add(i, rVar);
        l.getHandler().post(new Runnable() {
            public void run() {
                m.this.hf.addView(rVar.getView(), i, new ViewGroup.LayoutParams(-1, -2));
            }
        });
    }

    public void a(t tVar) {
        this.hd = tVar;
    }

    /* access modifiers changed from: protected */
    public void aB() {
        l.getHandler().post(new Runnable() {
            public void run() {
                for (r aB : m.this.hc) {
                    aB.aB();
                }
                m.this.hf.clearFocus();
            }
        });
    }

    public r aG(int i) {
        return this.hc.get(i);
    }

    public int b(final r rVar) {
        if (this.hc.add(rVar)) {
            l.getHandler().post(new Runnable() {
                public void run() {
                    m.this.hf.addView(rVar.getView(), new ViewGroup.LayoutParams(-1, -2));
                }
            });
            return this.hc.size() - 1;
        }
        throw new IllegalStateException();
    }

    public void b(final int i, final r rVar) {
        this.hc.set(i, rVar);
        l.getHandler().post(new Runnable() {
            public void run() {
                m.this.hf.addView(rVar.getView(), i, new ViewGroup.LayoutParams(-1, -2));
            }
        });
        this.hf.removeViewAt(i + 1);
    }

    public void bF() {
        this.hc.clear();
        l.getHandler().post(new Runnable() {
            public void run() {
                m.this.hf.removeAllViews();
            }
        });
    }

    public void br() {
        super.br();
        l.getHandler().post(new Runnable() {
            public void run() {
                Iterator<f> it = m.this.ce().iterator();
                while (it.hasNext()) {
                    f next = it.next();
                    Log.d("Screen", "Add command " + next.bK());
                    m.this.hf.addView(next.bM());
                }
                m.this.getView().requestLayout();
            }
        });
    }

    public void bs() {
        super.bs();
        l.getHandler().post(new Runnable() {
            public void run() {
                Iterator<f> it = m.this.ce().iterator();
                while (it.hasNext()) {
                    f next = it.next();
                    Log.d("Screen", "Remove command " + next.bK());
                    m.this.hf.removeView(next.bM());
                }
                m.this.getView().requestLayout();
            }
        });
    }

    public int bx() {
        return 2;
    }

    /* access modifiers changed from: protected */
    public void ch() {
        l.getHandler().post(new Runnable() {
            public void run() {
                for (r ch : m.this.hc) {
                    ch.ch();
                }
                m.this.hf.clearFocus();
            }
        });
    }

    public t co() {
        return this.hd;
    }

    public int d(p pVar) {
        if (pVar != null) {
            return b(new q("", pVar, 0, null));
        }
        throw new NullPointerException();
    }

    public void delete(final int i) {
        this.hc.remove(i);
        l.getHandler().post(new Runnable() {
            public void run() {
                m.this.hf.removeViewAt(i);
            }
        });
    }

    public View getView() {
        return this.he;
    }

    public int size() {
        return this.hc.size();
    }
}
