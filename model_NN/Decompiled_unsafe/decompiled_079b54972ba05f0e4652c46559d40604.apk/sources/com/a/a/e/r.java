package com.a.a.e;

import android.view.View;
import java.util.ArrayList;

public abstract class r {
    public static final int BUTTON = 2;
    public static final int HYPERLINK = 1;
    public static final int LAYOUT_2 = 16384;
    public static final int LAYOUT_BOTTOM = 32;
    public static final int LAYOUT_CENTER = 3;
    public static final int LAYOUT_DEFAULT = 0;
    public static final int LAYOUT_EXPAND = 2048;
    public static final int LAYOUT_LEFT = 1;
    public static final int LAYOUT_NEWLINE_AFTER = 512;
    public static final int LAYOUT_NEWLINE_BEFORE = 256;
    public static final int LAYOUT_RIGHT = 2;
    public static final int LAYOUT_SHRINK = 1024;
    public static final int LAYOUT_TOP = 16;
    public static final int LAYOUT_VCENTER = 48;
    public static final int LAYOUT_VEXPAND = 8192;
    public static final int LAYOUT_VSHRINK = 4096;
    public static final int OUTOFITEM = Integer.MAX_VALUE;
    public static final int PLAIN = 0;
    private ArrayList<f> gU = new ArrayList<>();
    private v hw = null;
    protected f hx;
    private s hy;
    private String label;
    private int layout;

    public r(String str) {
        r(-1, -1);
        this.label = str;
    }

    public void V(String str) {
        this.label = str;
    }

    public void a(s sVar) {
        this.hy = sVar;
    }

    /* access modifiers changed from: protected */
    public void a(v vVar) {
        this.hw = vVar;
    }

    /* access modifiers changed from: protected */
    public void aB() {
    }

    public void aL(int i) {
        if (((i & 1024) == 0 || (i & 2048) == 0) && ((i & 4096) == 0 || (i & 8192) == 0)) {
            this.layout = i;
            invalidate();
            return;
        }
        throw new IllegalArgumentException("Bad combination of layout policies");
    }

    public void b(f fVar) {
        boolean z = false;
        if (fVar == null) {
            throw new NullPointerException();
        } else if (!this.gU.contains(fVar)) {
            int i = 0;
            while (true) {
                if (i >= this.gU.size()) {
                    break;
                } else if (fVar.getPriority() < this.gU.get(i).getPriority()) {
                    this.gU.add(i, fVar);
                    z = true;
                    break;
                } else {
                    i++;
                }
            }
            if (!z) {
                this.gU.add(fVar);
            }
            invalidate();
        }
    }

    public String bK() {
        return this.label;
    }

    public void c(f fVar) {
        this.gU.remove(fVar);
        if (this.hx == fVar) {
            this.hx = null;
        }
        invalidate();
    }

    public int cG() {
        return this.layout;
    }

    public int cH() {
        return getHeight();
    }

    public int cI() {
        return getWidth();
    }

    public void cJ() {
        v cK = cK();
        if (cK != null && (cK instanceof m)) {
            ((m) cK).co().c(this);
        }
    }

    /* access modifiers changed from: protected */
    public v cK() {
        return this.hw;
    }

    public int cL() {
        return getHeight();
    }

    public int cM() {
        return getWidth();
    }

    public s cN() {
        return this.hy;
    }

    /* access modifiers changed from: protected */
    public void ch() {
    }

    public void d(f fVar) {
        this.hx = fVar;
        if (fVar == null) {
            invalidate();
        } else if (this.gU.contains(fVar)) {
            b(fVar);
        } else {
            invalidate();
        }
    }

    public int getHeight() {
        if (getView() != null) {
            return getView().getHeight();
        }
        return 0;
    }

    public int getMinimumHeight() {
        return getHeight();
    }

    public int getMinimumWidth() {
        return getWidth();
    }

    public abstract View getView();

    public int getWidth() {
        if (getView() != null) {
            return getView().getWidth();
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public final void invalidate() {
        if (this.hw != null) {
            this.hw.getView().postInvalidate();
        }
    }

    public boolean isFocusable() {
        return true;
    }

    public void r(int i, int i2) {
        if (i < -1 || i2 < -1) {
            throw new IllegalArgumentException();
        }
        invalidate();
    }
}
