package com.a.a.e;

import android.util.Log;
import android.view.View;
import org.meteoroid.core.e;
import org.meteoroid.plugin.device.MIDPDevice;

public abstract class c extends k {
    public static final int DOWN = 6;
    public static final int FIRE = 8;
    public static final int GAME_A = 9;
    public static final int GAME_B = 10;
    public static final int GAME_C = 11;
    public static final int GAME_D = 12;
    public static final int KEY_NUM0 = 48;
    public static final int KEY_NUM1 = 49;
    public static final int KEY_NUM2 = 50;
    public static final int KEY_NUM3 = 51;
    public static final int KEY_NUM4 = 52;
    public static final int KEY_NUM5 = 53;
    public static final int KEY_NUM6 = 54;
    public static final int KEY_NUM7 = 55;
    public static final int KEY_NUM8 = 56;
    public static final int KEY_NUM9 = 57;
    public static final int KEY_POUND = 35;
    public static final int KEY_STAR = 42;
    public static final int LEFT = 2;
    public static final int RIGHT = 5;
    public static final int UP = 1;

    public int ap(int i) {
        return ((MIDPDevice) org.meteoroid.core.c.mN).ap(i);
    }

    public int aq(int i) {
        return ((MIDPDevice) org.meteoroid.core.c.mN).aq(i);
    }

    public String ar(int i) {
        return ((MIDPDevice) org.meteoroid.core.c.mN).ar(i);
    }

    public boolean bA() {
        return ((MIDPDevice) org.meteoroid.core.c.mN).bA();
    }

    public boolean bB() {
        return ((MIDPDevice) org.meteoroid.core.c.mN).bB();
    }

    public final void bC() {
        if (this.gR != null) {
            this.gR.c(this);
        }
    }

    public final void bD() {
        if (this.gR != null) {
            this.gR.bD();
        }
    }

    public boolean bE() {
        return false;
    }

    public void br() {
        super.br();
        if (bx() == 0) {
            bC();
        }
    }

    public int bx() {
        return 0;
    }

    public boolean by() {
        return ((MIDPDevice) org.meteoroid.core.c.mN).by();
    }

    public boolean bz() {
        return ((MIDPDevice) org.meteoroid.core.c.mN).bz();
    }

    /* access modifiers changed from: protected */
    public abstract void c(o oVar);

    public final void e(int i, int i2, int i3, int i4) {
        Log.d("midpcanvas", "repaint x:" + i + " y:" + i2 + " w:" + i3 + " h:" + i4);
        bC();
    }

    public View getView() {
        return e.mP;
    }

    public void k(boolean z) {
    }
}
