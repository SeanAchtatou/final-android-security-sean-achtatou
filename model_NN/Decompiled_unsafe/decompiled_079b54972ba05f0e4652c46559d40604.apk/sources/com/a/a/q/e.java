package com.a.a.q;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class e {
    private byte[] lF;
    private String lG;
    private String lH;
    private String lI;
    private String mimeType;

    public e(InputStream inputStream, String str, String str2, String str3, String str4) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = inputStream.read();
            if (read != -1) {
                byteArrayOutputStream.write(read);
            } else {
                byteArrayOutputStream.flush();
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                h(byteArray, 0, byteArray.length);
                this.lG = str2;
                this.mimeType = str;
                this.lH = str3;
                this.lI = str4;
                return;
            }
        }
    }

    public e(byte[] bArr, int i, int i2, String str, String str2, String str3, String str4) {
        h(bArr, i, i2);
        this.lG = str2;
        this.mimeType = str;
        this.lH = str3;
        this.lI = str4;
    }

    public e(byte[] bArr, String str, String str2, String str3, String str4) {
        this(bArr, 0, bArr.length, str, str2, str3, str4);
    }

    private void h(byte[] bArr, int i, int i2) {
        this.lF = new byte[i2];
        System.arraycopy(this.lF, i, this.lF, 0, i2);
    }

    public byte[] fK() {
        return this.lF;
    }

    public InputStream fL() {
        return new ByteArrayInputStream(this.lF);
    }

    public String fM() {
        return this.lG;
    }

    public String fN() {
        return this.lH;
    }

    public String fO() {
        return this.mimeType;
    }

    public String getEncoding() {
        return this.lI;
    }

    public int getLength() {
        return this.lF.length;
    }
}
