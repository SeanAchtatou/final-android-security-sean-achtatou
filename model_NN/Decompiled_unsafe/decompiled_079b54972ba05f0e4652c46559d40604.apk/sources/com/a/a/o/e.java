package com.a.a.o;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import com.a.a.p.f;
import com.nokia.mid.ui.DirectGraphics;

public class e implements SensorEventListener {
    int accuracy;
    private float[] kT = new float[3];
    int kU;
    double[] kV = {-1.0d, -0.9d, -0.8d, -0.7d, -0.6d, -0.5d, -0.4d, -0.3d, -0.2d, -0.1d, 0.0d, 0.1d, 0.2d, 0.3d, 0.4d, 0.5d, 0.6d, 0.7d, 0.8d, 0.9d, 1.0d};
    int[] kW = {-60, -50, -40, -30, -20, -10, 0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100};
    int[] kX = {-359, -315, -270, -225, -180, -135, -90, -45, 0, 45, 90, 135, 180, f.OBEX_DATABASE_LOCKED, DirectGraphics.ROTATE_270, 315, 359};
    Object[] kY = new Object[0];
    String[] strings = new String[3];
    long timestamp;

    public e() {
        org.meteoroid.core.f.a(this);
        this.kU = 2;
    }

    public void init() {
    }

    public void onAccuracyChanged(Sensor sensor, int i) {
        this.accuracy = i;
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        if (this.kU == 1) {
            this.kT[0] = sensorEvent.values[0];
            this.kT[1] = sensorEvent.values[1];
            this.kT[2] = sensorEvent.values[2];
            this.strings[0] = "yaw";
            this.strings[1] = "pitch";
            this.strings[2] = "roll";
            this.accuracy = sensorEvent.accuracy;
            this.timestamp = sensorEvent.timestamp;
        }
        if (this.kU == 2) {
            this.kT[0] = sensorEvent.values[0];
            this.kT[1] = sensorEvent.values[1];
            this.kT[2] = sensorEvent.values[2];
            this.strings[0] = "axisX";
            this.strings[1] = "axisY";
            this.strings[2] = "axisZ";
            this.accuracy = sensorEvent.accuracy;
            this.timestamp = sensorEvent.timestamp;
        }
        if (this.kU == 7) {
        }
    }
}
