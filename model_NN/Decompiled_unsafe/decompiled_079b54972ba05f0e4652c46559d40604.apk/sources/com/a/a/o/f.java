package com.a.a.o;

import android.util.Log;
import com.a.a.c.b;
import com.a.a.m.a;
import com.a.a.m.e;
import com.a.a.m.g;
import com.a.a.m.l;
import com.a.a.m.m;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

public class f implements l {
    /* access modifiers changed from: private */
    public e kI = new e();
    private e[] kZ;
    private g kr = new g();
    private c kw = new c();
    private a kx;
    private g la;
    private m lb;
    private b lc;
    private a ld = new a();
    private BufferedReader le = new BufferedReader(this.lf);
    private Reader lf = new Reader() {
        public void close() {
        }

        public int read(char[] cArr, int i, int i2) {
            String[] strArr = f.this.kI.strings;
            StringBuffer stringBuffer = new StringBuffer();
            for (int i3 = 0; i3 < f.this.kI.strings.length; i3++) {
                stringBuffer = stringBuffer.append(strArr[i3]);
                i2 = stringBuffer.length();
                for (int i4 = 0; i4 < i2; i4++) {
                    cArr[i4] = stringBuffer.charAt(i4);
                }
            }
            return i2;
        }
    };
    private int[] lg;
    private String lh;
    private int li;
    private long lj;
    private boolean lk;
    private boolean ll;
    private boolean lm;
    private int state;

    public f() {
        try {
            this.li = this.le.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public a a(com.a.a.m.b bVar) {
        this.kx = this.ld;
        return this.kx;
    }

    public void a(g gVar, int i) {
        this.la = gVar;
        this.li = i;
        this.la.a(this, this.kZ, true);
    }

    public void a(g gVar, int i, long j, boolean z, boolean z2, boolean z3) {
        this.la = gVar;
        this.li = i;
        this.lj = j;
        this.lk = z;
        this.ll = z2;
        this.lm = z3;
        this.la.a(this, this.kZ, true);
    }

    public e[] a(int i, long j, boolean z, boolean z2, boolean z3) {
        this.li = i;
        this.lj = j;
        this.lk = z;
        this.ll = z2;
        this.lm = z3;
        for (int i2 = 0; i2 < this.kZ.length; i2++) {
            this.kZ[i2] = this.kw;
            System.currentTimeMillis();
        }
        return this.kZ;
    }

    public e[] bL(int i) {
        this.li = i;
        for (int i2 = 0; i2 < this.kZ.length; i2++) {
            this.kZ[i2] = this.kw;
        }
        return this.kZ;
    }

    public String bM(int i) {
        for (int i2 : this.lg) {
            this.lh = m.PROP_IS_REPORTING_ERRORS;
            Log.i("Get Error Text", this.lh + "**********");
        }
        return this.lh;
    }

    public void close() {
        this.lc.close();
    }

    public int[] fo() {
        return this.lg;
    }

    public m fp() {
        this.lb = this.kr;
        return this.lb;
    }

    public void fq() {
        this.la.a(this, this.kZ, false);
    }

    public int getState() {
        switch (this.state) {
            case 1:
                a(this.la, this.li);
                a(this.la, this.li, this.lj, this.lk, this.ll, this.lm);
                break;
            case 2:
                fq();
                break;
            case 4:
                try {
                    close();
                    break;
                } catch (IOException e) {
                    e.printStackTrace();
                    break;
                }
        }
        return this.state;
    }
}
