package com.a.a.o;

import android.hardware.SensorManager;
import com.a.a.m.b;
import com.a.a.m.m;
import java.util.HashMap;
import org.meteoroid.core.l;

public class g implements m {
    private String hK;
    private int hN;
    private e kI = new e();
    private String kJ;
    private String[] kN = new String[4];
    private int kU = this.kI.kU;
    private c kw = new c();
    private int[] lA = {1, 7, 3};
    private String[] lB = {"acc01", "tem01", "ori01"};
    private SensorManager lo = ((SensorManager) l.getActivity().getSystemService("sensor"));
    private b[] lp = new b[3];
    private b lq = new b();
    private int lr;
    private String ls;
    private String lt;
    private String lu;
    private HashMap<String, String> lv = new HashMap<>();
    private HashMap<String, String> lw = new HashMap<>();
    private HashMap<String, String> lx = new HashMap<>();
    private String[] ly = {m.CONTEXT_TYPE_USER, m.CONTEXT_TYPE_DEVICE, m.CONTEXT_TYPE_DEVICE};
    private String[] lz = {"Accelerometer", "Thermometer", "Orientation"};
    private String[] strings = new String[3];

    public g() {
        this.lo.getDefaultSensor(this.kU);
        this.strings = this.kI.strings;
        this.kN = this.lq.kN;
    }

    public b[] fr() {
        for (int i = 0; i < this.lp.length; i++) {
            this.lp[i] = this.lq;
        }
        return this.lp;
    }

    public int fs() {
        this.ls = ft();
        if (this.ls.equals(m.CONTEXT_TYPE_AMBIENT)) {
            this.lr = 2;
        }
        if (this.ls.equals(m.CONTEXT_TYPE_DEVICE)) {
            this.lr = 4;
        }
        if (this.ls.equals(m.CONTEXT_TYPE_USER)) {
            this.lr = 1;
        }
        if (this.ls.equals(m.CONTEXT_TYPE_VEHICLE)) {
            this.lr = 8;
        }
        return this.lr;
    }

    public String ft() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.kN.length) {
                return this.ls;
            }
            this.lv.put(this.kN[i2], this.ly[i2]);
            if (this.kJ == fv() && this.lv.containsKey(this.kJ)) {
                this.ls = this.lv.get(this.kJ);
            }
            i = i2 + 1;
        }
    }

    public String[] fu() {
        return this.strings;
    }

    public String fv() {
        for (int i = 0; i < this.lA.length; i++) {
            if (this.kU == this.lA[i]) {
                this.kJ = this.kN[i];
                this.lq.kJ = this.kJ;
            }
        }
        return this.kJ;
    }

    public boolean fw() {
        this.lr = fs();
        return this.lr != 1;
    }

    public boolean fx() {
        this.lr = fs();
        return this.lr != 1;
    }

    public String getDescription() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.kN.length) {
                return "Simple Gui " + this.lt + " Sensor";
            }
            this.lw.put(this.kN[i2], this.lz[i2]);
            if (this.kJ == fv() && this.lw.containsKey(this.kJ)) {
                this.lt = this.lw.get(this.kJ);
            }
            i = i2 + 1;
        }
    }

    public int getMaxBufferSize() {
        this.hN = this.kw.toString().length();
        for (int i = 0; i < this.hN; i++) {
            this.kw.isValid(i);
            this.kw.bJ(i);
            this.kw.bK(i);
        }
        return this.hN;
    }

    public String getModel() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.kN.length) {
                return this.lu;
            }
            this.lx.put(this.kN[i2], this.lB[i2]);
            if (this.kJ == fv() && this.lx.containsKey(this.kJ)) {
                this.lu = this.lx.get(this.kJ);
            }
            i = i2 + 1;
        }
    }

    public Object getProperty(String str) {
        String str2 = null;
        for (int i = 0; i < this.strings.length; i++) {
            str2 = this.strings[i];
        }
        return str2;
    }

    public String getUrl() {
        this.hK = "sensor:" + fv() + ";contextType=" + ft() + ";model=" + getModel();
        return this.hK;
    }

    public boolean isAvailable() {
        return true;
    }
}
