package com.a.a.o;

import android.util.Log;
import com.a.a.m.i;
import com.a.a.m.p;
import java.util.HashMap;

public class b implements com.a.a.m.b {
    private int accuracy;
    private int eS;
    private p kE;
    private i[] kF;
    private i kG;
    private d kH;
    private e kI;
    public String kJ;
    private int kK;
    private double kL;
    private HashMap<String, String> kM;
    public String[] kN;
    private int[] kO;
    private double kh;
    private double ki;
    private double kj;
    private String ks;
    private String[] ku;
    private c kw;
    private String name;
    private String[] strings;

    public b() {
        this.kF = new i[1];
        this.strings = new String[3];
        this.kM = new HashMap<>();
        this.kN = new String[]{"acceleration", "temperature", "orientation"};
        this.ku = new String[]{"m/s^2", "Celsius", "degree"};
        this.kO = new int[]{0, -1, -2, -3};
        this.kH = new d();
        this.kh = this.kH.kh;
        this.ki = this.kH.ki;
        this.kG = new i(this.kh, this.ki, fB());
        this.kI = new e();
        this.strings = this.kI.strings;
        this.kw = new c();
    }

    public i[] eY() {
        for (int i = 0; i < this.kF.length; i++) {
            this.kF[i] = this.kG;
        }
        return this.kF;
    }

    public int eZ() {
        if (getAccuracy() == -1.0f) {
            this.kw.kS = 0.0f;
        } else if (this.kw.kS > 0.0f) {
            this.kK = this.kO[0];
        } else if (this.kw.kS == ((float) (((double) this.kw.kS) * 0.1d))) {
            this.kK = this.kO[1];
        } else if (this.kw.kS == ((float) (((double) this.kw.kS) * 0.01d))) {
            this.kK = this.kO[2];
        } else if (this.kw.kS == ((float) (((double) this.kw.kS) * 0.001d))) {
            this.kK = this.kO[3];
        }
        return this.kK;
    }

    public double fB() {
        for (int i = 0; i < this.kN.length; i++) {
            this.kM.put(this.ku[i], this.kN[i]);
            if (this.kM.containsKey(this.ku[i])) {
                String str = this.kM.get(this.ku[i]);
                if (str == this.kN[0]) {
                    if (this.kL >= -19.61d && this.kL <= 19.61d) {
                        this.kj = 0.01d;
                    } else if (this.kL >= -58.84d && this.kL <= 58.84d) {
                        this.kj = 0.03d;
                    }
                } else if (str == this.kN[1]) {
                    this.kj = 1.0d;
                } else if (str == this.kN[2]) {
                    this.kj = 1.0d;
                }
            }
        }
        return this.kj;
    }

    public p fa() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.ku.length) {
                return this.kE;
            }
            this.kM.put(this.kN[i2], this.ku[i2]);
            if (this.kM.containsKey(this.kN[i2]) && this.kJ == this.kN[i2]) {
                this.ks = this.kM.get(this.kN[i2]);
                try {
                    this.kE = p.au(this.ks);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InstantiationException e2) {
                    e2.printStackTrace();
                }
            }
            i = i2 + 1;
        }
    }

    public float getAccuracy() {
        this.accuracy = this.kI.accuracy;
        Log.i("getAccuracy", this.accuracy + "******");
        return (float) this.accuracy;
    }

    public int getDataType() {
        if (this.kh == this.kw.fb()[0]) {
            this.eS = 1;
        } else if (this.kh == ((double) this.kw.fc()[0])) {
            this.eS = 2;
        } else {
            this.eS = 4;
        }
        return this.eS;
    }

    public String getName() {
        for (String str : this.strings) {
            this.name = str;
            Log.i("Get Name", this.name + "******");
        }
        return this.name;
    }
}
