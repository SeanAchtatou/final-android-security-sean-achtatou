package com.a.a.j;

import java.util.ArrayList;
import java.util.Arrays;

public class h {
    protected final i jG;
    protected ArrayList<Object> jH = new ArrayList<>();
    protected ArrayList<Integer> jI = new ArrayList<>();

    public h(i iVar) {
        this.jG = iVar;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.jG);
        stringBuffer.append("\n");
        int size = this.jH.size();
        for (int i = 0; i < size; i++) {
            Object obj = this.jH.get(i);
            if (obj instanceof Object[]) {
                obj = Arrays.toString((Object[]) obj);
            }
            stringBuffer.append("Value[" + i + "]:" + obj + ".");
            stringBuffer.append("Attr[" + i + "]:" + this.jI.get(i) + ".");
            stringBuffer.append("\n");
        }
        stringBuffer.append("\n");
        return stringBuffer.toString();
    }
}
