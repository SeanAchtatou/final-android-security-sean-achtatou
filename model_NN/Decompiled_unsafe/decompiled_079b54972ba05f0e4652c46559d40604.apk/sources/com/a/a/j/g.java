package com.a.a.j;

import com.a.a.i.a;
import com.a.a.i.b;
import com.a.a.i.h;
import com.a.a.i.i;
import com.a.a.i.n;
import java.util.Enumeration;

public class g extends a implements b {
    public static final i jA = new i(106, "Name", 5, 2, new int[]{2}, new int[]{0});
    public static final i jB = new i(a.TEL, 4, "Phone", new int[]{16, 512, 8, 32, 64, 4, 0});
    public static final i jC = new i(103, 4, "Email", new int[]{16, 512, 8, 32, 0});
    public static final i jD = new i(108, 4, "Note", new int[]{0});
    public static final i jE = new i(105, 4, "Formatted Name", new int[]{0});
    public static final i jF = new i(a.UID, 4, "Unique identifier", new int[]{0});
    public static final i jz = new i(100, "Address", 7, 1, new int[]{1}, new int[]{8, 512, 32, 0});
    private c jv = new c(this);

    g(String str, int i) {
        super(str, i);
        a(new i[]{jz, jC, jA, jB, jD, jE});
    }

    public String O(int i, int i2) {
        switch (i) {
            case 100:
                switch (i2) {
                    case 0:
                        return "POBox";
                    case 1:
                        return "Extra";
                    case 2:
                        return "Street";
                    case 3:
                        return "Locality";
                    case 4:
                        return "Region";
                    case 5:
                        return "Postalcode";
                    case 6:
                        return "Country";
                    default:
                        throw new IllegalArgumentException("Array element '" + i2 + "' is not valid for field 'Contact.ADDR'");
                }
            case 106:
                switch (i2) {
                    case 0:
                        return "Family";
                    case 1:
                        return "Given";
                    case 2:
                        return "Other";
                    case 3:
                        return "Prefix";
                    case 4:
                        return "Suffix";
                    default:
                        throw new IllegalArgumentException("Array element '" + i2 + "' is not valid for field 'Contact.NAME'");
                }
            default:
                throw new n("No field with id '" + i + "' present for ContactList.");
        }
    }

    public a a(a aVar) {
        if (aVar == null) {
            throw new NullPointerException("Parameter 'contact' must not be null.");
        }
        ew();
        return this.jv.c((f) aVar);
    }

    public Enumeration<i> a(i iVar) {
        ev();
        try {
            return this.jv.d((f) iVar);
        } catch (Exception e) {
            throw new h(e.getMessage());
        }
    }

    /* access modifiers changed from: package-private */
    public void a(f fVar) {
        this.jv.a(fVar);
    }

    public void addCategory(String str) {
        if (str == null) {
            throw new NullPointerException("Parameter 'category' must not be null.");
        }
        ew();
        throw new UnsupportedOperationException();
    }

    public Enumeration<i> ap(String str) {
        ev();
        try {
            return this.jv.ap(str);
        } catch (Exception e) {
            throw new h(e.getMessage());
        }
    }

    public Enumeration<i> aq(String str) {
        ev();
        try {
            return this.jv.aq(str);
        } catch (Exception e) {
            throw new h(e.getMessage());
        }
    }

    public boolean ar(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        throw new UnsupportedOperationException();
    }

    public void b(a aVar) {
        if (aVar == null) {
            throw new NullPointerException("Parameter 'contact' must not be null.");
        }
        ew();
        try {
            this.jv.b((f) aVar);
        } catch (Exception e) {
            throw new h(e.getMessage(), 1);
        }
    }

    public String bw(int i) {
        switch (i) {
            case 1:
                return "Assistant";
            case 2:
                return "Auto";
            case 4:
                return "Fax";
            case 8:
                return "Home";
            case 16:
                return "Mobile";
            case 32:
                return "Other";
            case 64:
                return "Pager";
            case 128:
                return "Preferred";
            case 256:
                return "SMS";
            case 512:
                return "Work";
            default:
                return super.bw(i);
        }
    }

    public void c(String str, boolean z) {
        throw new UnsupportedOperationException();
    }

    public void close() {
    }

    public a eh() {
        ew();
        return new f(this);
    }

    public String[] ep() {
        return new String[0];
    }

    public int eq() {
        return 0;
    }

    public Enumeration<i> er() {
        ev();
        try {
            return this.jv.er();
        } catch (Exception e) {
            throw new h(e.getMessage());
        }
    }

    public void k(String str, String str2) {
        ew();
        throw new UnsupportedOperationException();
    }
}
