package com.a.a.j;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.Contacts;
import com.a.a.i.i;
import java.util.Enumeration;
import org.meteoroid.core.l;

public class d implements Enumeration<i> {
    private final int count = this.ju.getCount();
    private ContentResolver jt = l.getActivity().getContentResolver();
    private Cursor ju = this.jt.query(Contacts.People.CONTENT_URI, null, null, null, null);
    private final c jv;
    private int position = -1;

    public d(c cVar) {
        this.jv = cVar;
    }

    /* renamed from: ex */
    public i nextElement() {
        if (this.ju.isClosed()) {
            return null;
        }
        this.position++;
        this.ju.moveToPosition(this.position);
        return this.jv.a(this.ju);
    }

    public boolean hasMoreElements() {
        return this.position < this.count + -1;
    }
}
