package com.a.a.j;

class i {
    public static final int DEFAULT_NUMBER_OF_ARRAYELEMENTS = 0;
    public static final int DEFAULT_PREFERRED_INDEX = -1;
    protected final int jJ;
    protected final int jK;
    protected final int jL;
    protected final int[] jM;
    protected final int[] jN;
    protected final String label;
    protected final int type;

    public i(int i, int i2, String str, int[] iArr) {
        this.jK = i;
        this.type = i2;
        this.label = str;
        this.jN = iArr;
        this.jJ = 0;
        this.jL = -1;
        this.jM = new int[0];
    }

    public i(int i, String str, int i2, int i3, int[] iArr, int[] iArr2) {
        this.jK = i;
        this.type = 5;
        this.label = str;
        this.jJ = i2;
        this.jL = i3;
        this.jM = iArr;
        this.jN = iArr2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return this.jK == ((i) obj).jK;
    }

    public int hashCode() {
        return this.jK + 31;
    }

    public String toString() {
        return "FieldInfo:Id:" + this.jK + ".Type:" + this.type + ".Label:" + this.label + ".ArrayElements:" + this.jJ + ".";
    }
}
