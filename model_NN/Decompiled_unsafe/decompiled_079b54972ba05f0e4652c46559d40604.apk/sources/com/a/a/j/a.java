package com.a.a.j;

import com.a.a.i.j;
import com.a.a.i.n;

public abstract class a implements j {
    private i[] jq;
    private final int mode;
    private final String name;

    public a(String str, int i) {
        this.name = str;
        if (i == 1 || i == 2 || i == 3) {
            this.mode = i;
            return;
        }
        throw new IllegalArgumentException("The mode '" + i + "' is not supported.");
    }

    private i g(int i, boolean z) {
        for (i iVar : this.jq) {
            if (i == iVar.jK) {
                return iVar;
            }
        }
        if (!z) {
            return null;
        }
        throw new n("The field with id '" + i + "' is not supported.");
    }

    public boolean M(int i, int i2) {
        i bA = bA(i);
        for (int i3 : bA.jN) {
            if (i2 == i3) {
                return true;
            }
        }
        return false;
    }

    public boolean N(int i, int i2) {
        i bA = bA(i);
        if (bA.type != 5) {
            throw new IllegalArgumentException("The field with id '" + i + "' is not of type 'PIMItem.STRING_ARRAY'.");
        }
        for (int i3 : bA.jM) {
            if (i2 == i3) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void a(i[] iVarArr) {
        this.jq = iVarArr;
    }

    public i bA(int i) {
        return g(i, true);
    }

    public boolean br(int i) {
        return g(i, false) != null;
    }

    public int[] bs(int i) {
        return bA(i).jN;
    }

    public int[] bt(int i) {
        return bA(i).jM;
    }

    public int bu(int i) {
        return bA(i).type;
    }

    public String bv(int i) {
        return bA(i).label;
    }

    public String bw(int i) {
        switch (i) {
            case 0:
                break;
            default:
                throw new IllegalArgumentException("Attribute '" + i + "' is not valid.");
        }
        return "None";
    }

    public int bx(int i) {
        return -1;
    }

    public int by(int i) {
        return bA(i).jJ;
    }

    public int[] es() {
        int[] iArr = new int[this.jq.length];
        for (int i = 0; i < this.jq.length; i++) {
            iArr[i] = this.jq[i].jK;
        }
        return iArr;
    }

    /* access modifiers changed from: protected */
    public void ev() {
        if (this.mode == 2) {
            throw new SecurityException("The list is only writeable.");
        }
    }

    /* access modifiers changed from: protected */
    public void ew() {
        if (this.mode == 1) {
            throw new SecurityException("The list is only readable.");
        }
    }

    public String getName() {
        return this.name;
    }
}
