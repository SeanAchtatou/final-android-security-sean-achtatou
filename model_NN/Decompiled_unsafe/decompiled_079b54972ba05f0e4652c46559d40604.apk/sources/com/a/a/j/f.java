package com.a.a.j;

import com.a.a.i.a;
import com.a.a.i.j;
import com.a.a.i.n;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class f implements a {
    private final long id;
    private final g jw;
    private final ArrayList<h> jx;
    private boolean jy;

    f(long j, g gVar) {
        this.id = j;
        this.jw = gVar;
        this.jx = new ArrayList<>();
    }

    f(g gVar) {
        this(-1, gVar);
    }

    private h P(int i, int i2) {
        i bA = this.jw.bA(i);
        if (bA == null) {
            throw new n("The field with id '" + i + "' is not supported.");
        } else if (bA.type != i2) {
            throw new IllegalArgumentException("The field with metadata '" + bA + "' is not of type '" + i2 + "'.");
        } else {
            Iterator<h> it = this.jx.iterator();
            while (it.hasNext()) {
                h next = it.next();
                if (next.jG.equals(bA)) {
                    return next;
                }
            }
            h hVar = new h(bA);
            this.jx.add(hVar);
            return hVar;
        }
    }

    private h bF(int i) {
        i bA = this.jw.bA(i);
        if (bA == null) {
            throw new n("The field with id '" + i + "' is not supported.");
        }
        Iterator<h> it = this.jx.iterator();
        while (it.hasNext()) {
            h next = it.next();
            if (next.jG.equals(bA)) {
                return next;
            }
        }
        h hVar = new h(bA);
        this.jx.add(hVar);
        return hVar;
    }

    public byte[] G(int i, int i2) {
        h P = P(i, 0);
        if (i2 < P.jH.size()) {
            return (byte[]) P.jH.get(i2);
        }
        throw new IndexOutOfBoundsException("The parameter 'index' violates constraint 'index < numberOfValues'.");
    }

    public long H(int i, int i2) {
        h P = P(i, 2);
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("The parameter 'index' violates constraint 'index >= 0'");
        } else if (i2 < P.jH.size()) {
            return ((Date) P.jH.get(i2)).getTime();
        } else {
            throw new IndexOutOfBoundsException("The parameter 'index' violates constraint 'index < numberOfValues'.");
        }
    }

    public boolean I(int i, int i2) {
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("The parameter 'index' violates constraint 'index >= 0'");
        }
        h P = P(i, 0);
        if (i2 < P.jH.size()) {
            return ((Boolean) P.jH.get(i2)).booleanValue();
        }
        throw new IndexOutOfBoundsException("The parameter 'index' violates constraint 'index < numberOfValues'.");
    }

    public String[] J(int i, int i2) {
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("The parameter 'index' violates contraint 'index >= 0'");
        }
        h P = P(i, 5);
        if (i2 < P.jH.size()) {
            return (String[]) P.jH.get(i2);
        }
        throw new IndexOutOfBoundsException("The parameter 'index' violates contraint 'index < numberOfValuesInField'");
    }

    public void K(int i, int i2) {
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index >= 0'");
        }
        h bF = bF(i);
        if (i2 >= bF.jH.size()) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index < numberOfValues'");
        }
        bF.jH.remove(i2);
        bF.jI.remove(i2);
    }

    public int L(int i, int i2) {
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("The index '" + i2 + "' must not be < 0.");
        }
        h bF = bF(i);
        int size = bF.jI.size() - 1;
        if (i2 <= size) {
            return bF.jI.get(i2).intValue();
        }
        throw new IndexOutOfBoundsException("The index '" + i2 + "' is larger then the last valid index of '" + size + "'");
    }

    public void a(int i, int i2, int i3, long j) {
        h P = P(i, 2);
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index >= 0'");
        } else if (i2 >= P.jH.size()) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index < numberOfValues'");
        } else {
            P.jH.set(i2, new Date(j));
            P.jI.set(i2, new Integer(i3));
            this.jy = true;
        }
    }

    public void a(int i, int i2, int i3, String str) {
        h P = P(i, 4);
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index >= 0'");
        } else if (i2 >= P.jH.size()) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index < numberOfValues'");
        } else {
            P.jH.set(i2, str);
            P.jI.set(i2, new Integer(i3));
            this.jy = true;
        }
    }

    public void a(int i, int i2, int i3, boolean z) {
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index >= 0'");
        }
        h P = P(i, 1);
        if (i2 >= P.jH.size()) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index < numberOfValues'");
        }
        P.jH.set(i2, new Boolean(z));
        P.jI.set(i2, new Integer(i3));
        this.jy = true;
    }

    public void a(int i, int i2, int i3, byte[] bArr, int i4, int i5) {
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index >= 0'");
        }
        h P = P(i, 0);
        int size = P.jH.size();
        if (i2 >= size) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index < numberOfValues'");
        } else if (bArr == null) {
            throw new NullPointerException("Parameter 'value' must not be null.");
        } else if (bArr.length == 0) {
            throw new IllegalArgumentException("Array value of parameter 'value' must not have zero elements.");
        } else if (i4 < 0) {
            throw new IllegalArgumentException("Parameter 'offset' must not have a negative value.");
        } else if (i4 >= size) {
            throw new IllegalArgumentException("Parameter 'offset' must not be larger then number of values which is '" + size + "'");
        } else if (i5 <= 0) {
            throw new IllegalArgumentException("Parameter 'length' must not have a negative value.");
        } else if (i5 > size) {
            throw new IllegalArgumentException("Parameter 'length' must not have a value which exceeds the number of values in the field which is '" + size + "'");
        } else if (i4 + i5 > size) {
            throw new IllegalArgumentException("The sum of 'offset' and 'length' must not exceed the number of values whish is '" + size + "'");
        } else {
            byte[] bArr2 = new byte[i5];
            System.arraycopy(bArr, i4, bArr2, 0, i5);
            P.jH.set(i2, bArr2);
            P.jI.set(i2, new Integer(i3));
            this.jy = true;
        }
    }

    public void a(int i, int i2, int i3, String[] strArr) {
        h P = P(i, 5);
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index >= 0'");
        } else if (i2 >= P.jH.size()) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index < numberOfValues'");
        } else {
            P.jH.set(i2, strArr);
            P.jI.set(i2, new Integer(i3));
            this.jy = true;
        }
    }

    public void a(int i, int i2, long j) {
        h P = P(i, 2);
        P.jH.add(new Date(j));
        P.jI.add(new Integer(i2));
        this.jy = true;
    }

    public void a(int i, int i2, byte[] bArr, int i3, int i4) {
        if (bArr == null) {
            throw new NullPointerException("The parameter value is null.");
        } else if (i4 < 1) {
            throw new IllegalArgumentException("The parameter 'length' violates contraint 'length > 0'");
        } else if (i4 > bArr.length) {
            throw new IllegalArgumentException("The parameter 'length' violates contraint 'length <= value.length'");
        } else if (i3 < 0) {
            throw new IllegalArgumentException("The parameter 'offset' violates contraint 'offset > 0'");
        } else if (i3 >= bArr.length) {
            throw new IllegalArgumentException("The parameter 'offset' violates contraint 'offset < value.length'");
        } else {
            h P = P(i, 0);
            byte[] bArr2 = new byte[i4];
            System.arraycopy(bArr, i3, bArr2, 0, i4);
            P.jH.add(bArr2);
            P.jI.add(new Integer(i2));
            this.jy = true;
        }
    }

    public void a(int i, int i2, String[] strArr) {
        if (strArr == null) {
            throw new NullPointerException("Parameter 'value' must not be null.");
        }
        h P = P(i, 5);
        P.jH.add(strArr);
        P.jI.add(new Integer(i2));
        this.jy = true;
    }

    public void an(String str) {
        throw new UnsupportedOperationException();
    }

    public void ao(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
    }

    public void b(int i, int i2, String str) {
        if (str == null) {
            throw new NullPointerException("Parameter 'value' must not be null.");
        }
        h P = P(i, 4);
        P.jH.add(str);
        P.jI.add(new Integer(i2));
        this.jy = true;
    }

    public int bm(int i) {
        return this.jw.bA(i).jL;
    }

    public int bq(int i) {
        return bF(i).jH.size();
    }

    public void c(int i, int i2, boolean z) {
        h P = P(i, 1);
        P.jH.add(new Boolean(z));
        P.jI.add(new Integer(i2));
        this.jy = true;
    }

    public void commit() {
        if (this.jy) {
            this.jw.a(this);
        }
    }

    public j em() {
        return this.jw;
    }

    public boolean en() {
        return this.jy;
    }

    public int[] eo() {
        int i;
        int[] iArr = new int[this.jx.size()];
        int i2 = 0;
        Iterator<h> it = this.jx.iterator();
        while (true) {
            i = i2;
            if (!it.hasNext()) {
                break;
            }
            iArr[i] = it.next().jG.jK;
            i2 = i + 1;
        }
        int[] iArr2 = new int[i];
        while (true) {
            i--;
            if (i < 0) {
                return iArr2;
            }
            iArr2[i] = iArr[i];
        }
    }

    public String[] ep() {
        return new String[0];
    }

    public int eq() {
        return 0;
    }

    /* access modifiers changed from: package-private */
    public boolean ey() {
        return this.id == -1;
    }

    /* access modifiers changed from: package-private */
    public long getId() {
        return this.id;
    }

    public int getInt(int i, int i2) {
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("The parameter 'index' violates constraint 'index >= 0'");
        }
        h P = P(i, 3);
        if (i2 < P.jH.size()) {
            return ((Integer) P.jH.get(i2)).intValue();
        }
        throw new IndexOutOfBoundsException("The parameter 'index' violates constraint 'index < numberOfValues'.");
    }

    public String getString(int i, int i2) {
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("The parameter 'index' violates constraint 'index >= 0'");
        }
        h P = P(i, 4);
        if (i2 < P.jH.size()) {
            return (String) P.jH.get(i2);
        }
        throw new IndexOutOfBoundsException("The parameter 'index' violates constraint 'index < numberOfValues'.");
    }

    public void n(int i, int i2, int i3, int i4) {
        h P = P(i, 3);
        if (i2 < 0) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index >= 0'");
        } else if (i2 >= P.jH.size()) {
            throw new IndexOutOfBoundsException("Parameter 'index' violates constraint 'index < numberOfValues'");
        } else {
            P.jH.set(i2, new Integer(i4));
            P.jI.set(i2, new Integer(i3));
            this.jy = true;
        }
    }

    public void p(int i, int i2, int i3) {
        h P = P(i, 3);
        P.jH.add(new Integer(i3));
        P.jI.add(new Integer(i2));
        this.jy = true;
    }

    /* access modifiers changed from: package-private */
    public void s(boolean z) {
        this.jy = z;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Contact (" + this.id + ")");
        stringBuffer.append("\n");
        Iterator<h> it = this.jx.iterator();
        while (it.hasNext()) {
            stringBuffer.append(it.next());
            stringBuffer.append("\n");
        }
        stringBuffer.append("\n");
        return stringBuffer.toString();
    }
}
