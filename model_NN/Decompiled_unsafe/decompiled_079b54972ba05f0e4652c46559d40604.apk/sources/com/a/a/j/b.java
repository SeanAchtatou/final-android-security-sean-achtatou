package com.a.a.j;

import com.a.a.i.g;
import com.a.a.i.h;
import com.a.a.i.i;
import com.a.a.i.j;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

public class b extends g {
    public static final String DEFAULT_PIMLIST_NAME_CONTACTS = "contacts";
    private static g jr;

    public j F(int i, int i2) {
        switch (i) {
            case 1:
                break;
            default:
                throw new h("The pimListType '" + i + "' is not supported.");
        }
        return a(i, i2, DEFAULT_PIMLIST_NAME_CONTACTS);
    }

    public j a(int i, int i2, String str) {
        switch (i) {
            case 1:
                break;
            default:
                throw new h("The pimListType '" + i + "' is not supported.");
        }
        if (!DEFAULT_PIMLIST_NAME_CONTACTS.equals(str)) {
            throw new h("A PIMList with name '" + str + "' and type '" + i + "' does not exist.");
        }
        if (jr == null) {
            jr = new g(str, i2);
        }
        return jr;
    }

    public void a(i iVar, OutputStream outputStream, String str, String str2) {
        throw new UnsupportedEncodingException("At the moment no encoding is supported.");
    }

    public i[] b(InputStream inputStream, String str) {
        throw new UnsupportedEncodingException("At the moment no encoding is supported.");
    }

    public String[] bo(int i) {
        switch (i) {
            case 1:
                return new String[]{DEFAULT_PIMLIST_NAME_CONTACTS};
            default:
                return new String[0];
        }
    }

    public String[] bp(int i) {
        return new String[0];
    }
}
