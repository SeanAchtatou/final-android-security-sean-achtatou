package com.a.a.r;

public interface a {
    public static final int MSG_DEVICE_KEY_EVENT = 44285;
    public static final int MSG_DEVICE_REQUEST_REFRESH = 44287;
    public static final int MSG_DEVICE_TOUCH_EVENT = 44286;

    String bt(String str);

    boolean f(String str, boolean z);

    int getHeight();

    String getName();

    int getWidth();

    int k(String str, int i);

    void onCreate();

    void onDestroy();
}
