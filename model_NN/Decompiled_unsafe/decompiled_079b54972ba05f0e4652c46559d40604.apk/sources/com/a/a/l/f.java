package com.a.a.l;

import android.util.Log;
import com.a.a.i.k;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.microedition.media.control.ToneControl;
import org.meteoroid.core.b;

public final class f {
    public static final int AUTHMODE_ANY = 1;
    public static final int AUTHMODE_PRIVATE = 0;
    public static final String LOG_TAG = "RMS";
    private static final String PREFIX = "rms_";
    /* access modifiers changed from: private */
    public HashMap<Integer, byte[]> jQ = new HashMap<>();
    private int jR;
    private long jS;
    private HashSet<e> jT = new HashSet<>();
    private int jU;
    private boolean jV;
    private boolean jW;
    private String name;
    private int size;
    private int version;

    private static final class a implements c {
        private int index;
        private f jX;
        private d jY;
        /* access modifiers changed from: private */
        public b jZ;
        private boolean ka;
        private ArrayList<Integer> kb;
        private final Comparator<Map.Entry<Integer, byte[]>> kc = new Comparator<Map.Entry<Integer, byte[]>>() {
            /* renamed from: a */
            public int compare(Map.Entry<Integer, byte[]> entry, Map.Entry<Integer, byte[]> entry2) {
                int a = a.this.jZ.a(entry.getValue(), entry2.getValue());
                if (a == 0) {
                    return 0;
                }
                return a == 1 ? 1 : -1;
            }
        };
        private e kd = new e() {
            public void a(f fVar, int i) {
                a.this.eL();
            }

            public void b(f fVar, int i) {
                a.this.eL();
            }

            public void c(f fVar, int i) {
                a.this.eL();
            }
        };

        public a(f fVar, d dVar, b bVar, boolean z) {
            this.jX = fVar;
            this.jY = dVar;
            this.jZ = bVar;
            this.ka = z;
            this.kb = new ArrayList<>();
            t(z);
            eL();
            reset();
        }

        public void destroy() {
            Log.d("RecordEnumerationImpl", "destroy");
            this.kb.clear();
            this.kb = null;
        }

        public int eE() {
            Log.d("RecordEnumerationImpl", "numRecords:" + this.kb.size());
            return this.kb.size();
        }

        public byte[] eF() {
            Log.d("RecordEnumerationImpl", "nextRecord");
            boolean unused = this.jX.isOpen();
            try {
                byte[] bI = this.jX.bI(this.kb.get(this.index).intValue());
                this.index++;
                return bI;
            } catch (Exception e) {
                throw new a();
            }
        }

        public int eG() {
            try {
                int intValue = this.kb.get(this.index).intValue();
                this.index++;
                Log.d("RecordEnumerationImpl", "nextRecordId:" + intValue);
                return intValue;
            } catch (Exception e) {
                throw new a();
            }
        }

        public byte[] eH() {
            Log.d("RecordEnumerationImpl", "previousRecord");
            boolean unused = this.jX.isOpen();
            try {
                this.index--;
                return this.jX.bI(this.kb.get(this.index).intValue());
            } catch (Exception e) {
                throw new a();
            }
        }

        public int eI() {
            Log.d("RecordEnumerationImpl", "previousRecordId");
            try {
                this.index--;
                return this.kb.get(this.index).intValue();
            } catch (Exception e) {
                throw new a();
            }
        }

        public boolean eJ() {
            Log.d("RecordEnumerationImpl", "hasNextElement:" + this.index + "[" + this.kb.size() + "]");
            try {
                boolean unused = this.jX.isOpen();
                return this.index <= this.kb.size() + -1;
            } catch (j e) {
                return false;
            }
        }

        public boolean eK() {
            Log.d("RecordEnumerationImpl", "hasPreviousElement" + this.index + "[" + this.kb.size() + "]");
            try {
                boolean unused = this.jX.isOpen();
                return this.index > 0;
            } catch (j e) {
                return false;
            }
        }

        public void eL() {
            Log.d("RecordEnumerationImpl", "rebuild");
            synchronized (this) {
                this.kb.clear();
                ArrayList arrayList = new ArrayList();
                for (Map.Entry entry : this.jX.jQ.entrySet()) {
                    if (this.jY == null || (this.jY != null && this.jY.t((byte[]) entry.getValue()))) {
                        arrayList.add(entry);
                    }
                }
                if (this.jZ != null) {
                    Collections.sort(arrayList, this.kc);
                }
                Iterator it = arrayList.iterator();
                while (it.hasNext()) {
                    Map.Entry entry2 = (Map.Entry) it.next();
                    this.kb.add(entry2.getKey());
                    Log.d("RecordEnumerationImpl", "add:" + entry2.getKey());
                }
            }
        }

        public boolean eM() {
            Log.d("RecordEnumerationImpl", "isKeptUpdated");
            return this.ka;
        }

        public void reset() {
            Log.d("RecordEnumerationImpl", "reset");
            this.index = 0;
        }

        public void t(boolean z) {
            Log.d("RecordEnumerationImpl", "keepUpdated" + z);
            if (z) {
                this.jX.a(this.kd);
            } else {
                this.jX.b(this.kd);
            }
            this.ka = z;
        }
    }

    private f(String str) {
        this.name = str;
    }

    public static f a(String str, boolean z, int i, boolean z2) {
        return d(str, z);
    }

    private synchronized void a(DataOutputStream dataOutputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream2 = new DataOutputStream(byteArrayOutputStream);
        this.version++;
        dataOutputStream2.writeInt(this.version);
        dataOutputStream2.writeLong(System.currentTimeMillis());
        dataOutputStream2.writeInt(this.jR);
        dataOutputStream2.writeInt(this.jQ.size());
        for (Map.Entry next : this.jQ.entrySet()) {
            dataOutputStream2.writeInt(((Integer) next.getKey()).intValue());
            byte[] bArr = (byte[]) next.getValue();
            dataOutputStream2.writeInt(bArr.length);
            dataOutputStream2.write(bArr);
        }
        byteArrayOutputStream.flush();
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        String b = b(byteArray, "MD5");
        Log.d(LOG_TAG, "Generate sign:" + b);
        dataOutputStream.write(byteArray);
        dataOutputStream.writeUTF(b);
        dataOutputStream.flush();
    }

    public static void as(String str) {
        Log.d(LOG_TAG, "Del " + str);
        if (!b.be(PREFIX + str)) {
            throw new i();
        } else if (!b.bh(PREFIX + str)) {
            throw new g();
        }
    }

    private String b(byte[] bArr, String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance(str.toUpperCase());
            instance.reset();
            instance.update(bArr);
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < digest.length; i++) {
                if (Integer.toHexString(digest[i] & ToneControl.SILENCE).length() == 1) {
                    stringBuffer.append("0").append(Integer.toHexString(digest[i] & ToneControl.SILENCE));
                } else {
                    stringBuffer.append(Integer.toHexString(digest[i] & ToneControl.SILENCE));
                }
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    private synchronized void b(DataInputStream dataInputStream) {
        synchronized (this) {
            DataInputStream dataInputStream2 = new DataInputStream(dataInputStream);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr = new byte[1024];
            while (true) {
                int read = dataInputStream2.read(bArr);
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            DataInputStream dataInputStream3 = new DataInputStream(new ByteArrayInputStream(byteArray));
            this.version = dataInputStream3.readInt();
            this.jS = dataInputStream3.readLong();
            this.jR = dataInputStream3.readInt();
            int readInt = dataInputStream3.readInt();
            for (int i = 0; i < readInt; i++) {
                int readInt2 = dataInputStream3.readInt();
                byte[] bArr2 = new byte[dataInputStream3.readInt()];
                dataInputStream3.read(bArr2);
                this.jQ.put(Integer.valueOf(readInt2), bArr2);
            }
            try {
                String readUTF = dataInputStream3.readUTF();
                dataInputStream3.close();
                byte[] bArr3 = new byte[((byteArray.length - readUTF.getBytes().length) - 2)];
                System.arraycopy(byteArray, 0, bArr3, 0, bArr3.length);
                if (!readUTF.equals(b(bArr3, "MD5"))) {
                    Log.w(LOG_TAG, "Verify sign fail:" + readUTF);
                    throw new IllegalArgumentException("Invalid sign. The file has been modified.");
                }
            } catch (IOException e) {
                Log.d(LOG_TAG, "No need to verify the rms.");
            } catch (Exception e2) {
                throw new IOException(e2.getMessage());
            }
            eN();
        }
    }

    public static f c(String str, String str2, String str3) {
        return d(str, false);
    }

    public static f d(String str, boolean z) {
        Log.d(LOG_TAG, "openRecordStore:" + str);
        f fVar = new f(str);
        if (b.be(PREFIX + str)) {
            Log.d(LOG_TAG, str + " exist.");
            try {
                DataInputStream dataInputStream = new DataInputStream(b.bg(PREFIX + str));
                fVar.b(dataInputStream);
                dataInputStream.close();
            } catch (Exception e) {
                Log.e(LOG_TAG, e.getMessage());
                throw new g();
            }
        } else if (!z) {
            Log.d(LOG_TAG, str + " not exist and not necessary to create.");
            throw new i();
        } else {
            Log.d(LOG_TAG, str + " not exist and necessary to create.");
            try {
                fVar.eQ();
            } catch (Exception e2) {
                Log.e(LOG_TAG, e2.getMessage());
                throw new g();
            }
        }
        fVar.jW = true;
        return fVar;
    }

    private void eN() {
        this.size = 0;
        for (byte[] length : this.jQ.values()) {
            this.size = length.length + this.size;
        }
        Log.d(LOG_TAG, "calcTheSizeOfRecords:" + this.size);
    }

    private synchronized void eO() {
        if (b.be(PREFIX + this.name)) {
            try {
                DataInputStream dataInputStream = new DataInputStream(b.bg(PREFIX + this.name));
                int readInt = dataInputStream.readInt();
                Log.d(LOG_TAG, "Current version:" + this.version + " newest version:" + readInt);
                dataInputStream.close();
                if (readInt > this.version) {
                    Log.d(LOG_TAG, "Records are out of date.Refereshing...");
                    b(new DataInputStream(b.bg(PREFIX + this.name)));
                }
            } catch (Exception e) {
                Log.e(LOG_TAG, e.getMessage());
            }
        }
        return;
    }

    public static String[] eP() {
        Log.d(LOG_TAG, "listRecordStores");
        List<String> bc = b.bc(PREFIX);
        if (bc.isEmpty()) {
            return null;
        }
        String[] strArr = new String[bc.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < strArr.length) {
                strArr[i2] = bc.get(i2).substring(PREFIX.length());
                i = i2 + 1;
            } else {
                bc.clear();
                return strArr;
            }
        }
    }

    private synchronized void eQ() {
        DataOutputStream dataOutputStream = new DataOutputStream(b.bf(PREFIX + this.name));
        a(dataOutputStream);
        dataOutputStream.flush();
        dataOutputStream.close();
        eN();
    }

    /* access modifiers changed from: private */
    public boolean isOpen() {
        if (this.jW) {
            return this.jW;
        }
        Log.e(LOG_TAG, "RecordStoreNotOpenException");
        throw new j();
    }

    public int a(int i, byte[] bArr, int i2) {
        isOpen();
        eO();
        byte[] bI = bI(i);
        int min = Math.min(bI.length, bArr.length - i2);
        System.arraycopy(bI, 0, bArr, i2, min);
        Log.d(LOG_TAG, "getRecordwithoffset" + i + " offset:" + i2 + " length:" + min);
        return min;
    }

    public c a(d dVar, b bVar, boolean z) {
        boolean z2 = true;
        isOpen();
        StringBuilder append = new StringBuilder().append("enumerateRecords: filter:").append(dVar != null).append(" comparator:");
        if (bVar == null) {
            z2 = false;
        }
        Log.d(LOG_TAG, append.append(z2).append(" keepUpdated:").append(z).toString());
        return new a(this, dVar, bVar, z);
    }

    public void a(int i, byte[] bArr, int i2, int i3) {
        isOpen();
        Log.d(LOG_TAG, "setRecord:" + i + " data:" + bArr.length);
        if (this.jQ.containsKey(Integer.valueOf(i))) {
            this.jQ.remove(Integer.valueOf(i));
            byte[] bArr2 = new byte[i3];
            System.arraycopy(bArr, i2, bArr2, 0, i3);
            this.jQ.put(Integer.valueOf(i), bArr2);
            try {
                eQ();
            } catch (Exception e) {
            }
            Iterator<e> it = this.jT.iterator();
            while (it.hasNext()) {
                it.next().b(this, i);
            }
            return;
        }
        throw new a();
    }

    public void a(e eVar) {
        if (!this.jT.contains(eVar)) {
            this.jT.add(eVar);
            Log.d(LOG_TAG, "addRecordListener");
        }
    }

    public void b(e eVar) {
        if (this.jT.remove(eVar)) {
            Log.d(LOG_TAG, "removeRecordListener");
        }
    }

    public void bG(int i) {
        isOpen();
        Log.d(LOG_TAG, "deleteRecord" + i);
        if (this.jQ.containsKey(Integer.valueOf(i))) {
            this.jQ.remove(Integer.valueOf(i));
            try {
                eQ();
                Iterator<e> it = this.jT.iterator();
                while (it.hasNext()) {
                    it.next().c(this, i);
                }
            } catch (Exception e) {
                throw new g();
            }
        } else {
            throw new a();
        }
    }

    public int bH(int i) {
        isOpen();
        eO();
        Log.d(LOG_TAG, "getRecordSize:" + i);
        if (this.jQ.containsKey(Integer.valueOf(i))) {
            return this.jQ.get(Integer.valueOf(i)).length;
        }
        throw new a();
    }

    public byte[] bI(int i) {
        isOpen();
        eO();
        if (this.jQ.containsKey(Integer.valueOf(i))) {
            Log.d(LOG_TAG, "getRecord" + i + " length:" + this.jQ.get(Integer.valueOf(i)).length);
            return this.jQ.get(Integer.valueOf(i));
        }
        throw new a("recordId=" + i);
    }

    public void eR() {
        isOpen();
        eO();
        Log.d(LOG_TAG, "closeRecordStore" + this.name + " at version:" + this.version);
        this.jT.clear();
        this.jT = null;
        this.jQ.clear();
        this.jW = false;
        this.jQ = null;
        this.name = null;
        System.gc();
    }

    public int eS() {
        isOpen();
        eO();
        Log.d(LOG_TAG, "getNumRecords" + this.jQ.size());
        return this.jQ.size();
    }

    public int eT() {
        isOpen();
        return k.JULY - this.size;
    }

    public int eU() {
        isOpen();
        eO();
        Log.d(LOG_TAG, "getNextRecordID");
        return this.jR + 1;
    }

    public int g(byte[] bArr, int i, int i2) {
        isOpen();
        this.jR++;
        Log.d(LOG_TAG, "addRecord:" + this.jR + " offset:" + i + " numBytes:" + i2);
        byte[] bArr2 = new byte[i2];
        if (i2 != 0) {
            System.arraycopy(bArr, i, bArr2, 0, i2);
        }
        this.jQ.put(Integer.valueOf(this.jR), bArr2);
        try {
            eQ();
            Iterator<e> it = this.jT.iterator();
            while (it.hasNext()) {
                it.next().a(this, this.jR);
            }
            return this.jR;
        } catch (Exception e) {
            throw new g();
        }
    }

    public long getLastModified() {
        isOpen();
        eO();
        return this.jS;
    }

    public String getName() {
        isOpen();
        return this.name;
    }

    public int getSize() {
        isOpen();
        eO();
        return this.size;
    }

    public int getVersion() {
        isOpen();
        eO();
        return this.version;
    }

    public void h(int i, boolean z) {
        this.jU = i;
        this.jV = z;
    }
}
