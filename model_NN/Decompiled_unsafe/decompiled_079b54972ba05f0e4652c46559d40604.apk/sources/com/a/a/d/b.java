package com.a.a.d;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;

public interface b extends com.a.a.c.b {
    void P(String str);

    void Q(String str);

    DataInputStream aQ();

    InputStream aR();

    DataOutputStream aS();

    OutputStream aT();

    Enumeration<String> b(String str, boolean z);

    long bk();

    long bl();

    long bm();

    long bn();

    Enumeration<String> bo();

    void bp();

    boolean canRead();

    boolean canWrite();

    void create();

    OutputStream d(long j);

    void delete();

    boolean exists();

    long g(boolean z);

    String getName();

    String getPath();

    String getURL();

    void h(boolean z);

    void i(boolean z);

    boolean isDirectory();

    boolean isHidden();

    boolean isOpen();

    void j(boolean z);

    long lastModified();

    void truncate(long j);
}
