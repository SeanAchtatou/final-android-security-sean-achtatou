package com.a.a.f;

import com.a.a.e.c;
import com.a.a.e.o;
import org.meteoroid.plugin.device.MIDPDevice;

public abstract class a extends c {
    public static final int DOWN_PRESSED = 64;
    public static final int FIRE_PRESSED = 256;
    public static final int GAME_A_PRESSED = 512;
    public static final int GAME_B_PRESSED = 1024;
    public static final int GAME_C_PRESSED = 2048;
    public static final int GAME_D_PRESSED = 4096;
    public static final int LEFT_PRESSED = 4;
    public static final int RIGHT_PRESSED = 32;
    public static final int UP_PRESSED = 2;
    private int hY;

    protected a(boolean z) {
    }

    private static final boolean aP(int i) {
        return i == 6 || i == 1 || i == 2 || i == 5 || i == 8 || i == 9 || i == 10 || i == 11 || i == 12;
    }

    public int bx() {
        return 1;
    }

    public void c(o oVar) {
    }

    /* access modifiers changed from: protected */
    public o cD() {
        return ((MIDPDevice) org.meteoroid.core.c.mN).cD();
    }

    public int cR() {
        return this.hY;
    }

    public void cb() {
        if (this.gR != null) {
            this.gR.cb();
        }
    }

    public void k(int i, int i2, int i3, int i4) {
        if (this.gR != null) {
            this.gR.cb();
        }
    }

    public void u(int i, int i2) {
        int ap = ap(i2);
        if (i == 0 && aP(ap)) {
            this.hY = (1 << ap) | this.hY;
        } else if (i == 1 && aP(ap)) {
            this.hY = (1 << ap) ^ this.hY;
        }
    }
}
