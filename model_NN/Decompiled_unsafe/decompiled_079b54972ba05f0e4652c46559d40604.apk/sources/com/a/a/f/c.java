package com.a.a.f;

import android.util.Log;
import com.a.a.e.o;
import com.a.a.e.r;
import java.util.Vector;

public class c {
    private Vector<b> ia = new Vector<>();
    private int ib = 0;
    private int ic = 0;
    private int ie = r.OUTOFITEM;

    /* renamed from: if  reason: not valid java name */
    private int f0if = r.OUTOFITEM;

    public c() {
        Log.d("LayerManager", "LayerManager is created.");
    }

    public void a(b bVar) {
        this.ia.add(bVar);
    }

    public void a(b bVar, int i) {
        this.ia.insertElementAt(bVar, i);
    }

    public b aQ(int i) {
        return this.ia.get(i);
    }

    public void b(o oVar, int i, int i2) {
        int ct = oVar.ct();
        int cu = oVar.cu();
        int cs = oVar.cs();
        int cr = oVar.cr();
        oVar.translate(i - this.ib, i2 - this.ic);
        oVar.f(this.ib, this.ic, this.ie, this.f0if);
        int size = getSize();
        while (true) {
            size--;
            if (size >= 0) {
                b aQ = aQ(size);
                if (aQ.isVisible()) {
                    aQ.c(oVar);
                }
            } else {
                oVar.translate((-i) + this.ib, (-i2) + this.ic);
                oVar.j(ct, cu, cs, cr);
                return;
            }
        }
    }

    public void b(b bVar) {
        this.ia.remove(bVar);
    }

    public int getSize() {
        return this.ia.size();
    }

    public void l(int i, int i2, int i3, int i4) {
        if (i3 < 0 || i4 < 0) {
            throw new IllegalArgumentException();
        }
        this.ib = i;
        this.ic = i2;
        this.ie = i3;
        this.f0if = i4;
    }
}
