package com.a.a.f;

import com.a.a.e.o;
import com.a.a.e.p;
import java.lang.reflect.Array;

public class e extends b {
    private final int ik;
    private final int il;
    p in;
    private int iu;
    private int iv;
    private int iw;
    private int[][] ix;
    int[] iy;
    int iz;

    public e(int i, int i2, p pVar, int i3, int i4) {
        super(0, 0, i * i3, i2 * i4, true);
        if (pVar == null) {
            throw new NullPointerException();
        } else if (i <= 0 || i2 <= 0 || i4 <= 0 || i3 <= 0) {
            throw new IllegalArgumentException();
        } else if (pVar.getWidth() % i3 == 0 && pVar.getHeight() % i4 == 0) {
            this.in = pVar;
            this.ik = i;
            this.il = i2;
            this.iv = i3;
            this.iu = i4;
            this.iw = (pVar.getWidth() / i3) * (pVar.getHeight() / i4);
            this.ix = (int[][]) Array.newInstance(Integer.TYPE, i2, i);
            this.iy = new int[5];
            this.iz = 0;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public int A(int i, int i2) {
        return this.ix[i2][i];
    }

    public int aT(int i) {
        int i2;
        synchronized (this) {
            if (i >= 0) {
                if (i <= this.iw) {
                    if (this.iz == this.iy.length) {
                        int[] iArr = new int[(this.iz + 6)];
                        System.arraycopy(this.iy, 0, iArr, 0, this.iz);
                        this.iy = iArr;
                    }
                    this.iy[this.iz] = i;
                    this.iz++;
                    i2 = -this.iz;
                }
            }
            throw new IndexOutOfBoundsException();
        }
        return i2;
    }

    public int aU(int i) {
        int i2;
        synchronized (this) {
            int i3 = (-i) - 1;
            if (i3 >= 0) {
                if (i3 < this.iz) {
                    i2 = this.iy[i3];
                }
            }
            throw new IndexOutOfBoundsException();
        }
        return i2;
    }

    public void b(int i, int i2, int i3, int i4, int i5) {
        synchronized (this) {
            if (i3 < 0 || i4 < 0) {
                throw new IllegalArgumentException();
            }
            if (i2 >= 0 && i >= 0) {
                if (i + i3 <= this.ik && i2 + i4 <= this.il) {
                    if ((-i5) - 1 >= this.iz || i5 > this.iw) {
                        throw new IndexOutOfBoundsException();
                    }
                    int i6 = i2 + i4;
                    int i7 = i + i3;
                    while (i2 < i6) {
                        for (int i8 = i; i8 < i7; i8++) {
                            this.ix[i2][i8] = i5;
                        }
                        i2++;
                    }
                }
            }
            throw new IndexOutOfBoundsException();
        }
    }

    public void b(p pVar, int i, int i2) {
        synchronized (this) {
            if (this.in == null) {
                throw new NullPointerException();
            } else if (i2 <= 0 || i <= 0) {
                throw new IllegalArgumentException();
            } else if (this.in.getWidth() % i == 0 && this.in.getHeight() % i2 == 0) {
                int width = (this.in.getWidth() / db()) * (this.in.getHeight() / dc());
                setSize(this.ik * i, this.il * i2);
                this.iv = i;
                this.iu = i2;
                if (width >= this.iw) {
                    this.iw = width;
                    return;
                }
                this.iw = width;
                this.iy = new int[5];
                this.iz = 0;
                b(0, 0, cZ(), da(), 0);
            } else {
                throw new IllegalArgumentException();
            }
        }
    }

    public final void c(o oVar) {
        synchronized (this) {
            if (isVisible()) {
                int x = getX();
                int y = getY();
                int i = 0;
                int cZ = cZ();
                int da = da();
                int db = db();
                int dc = dc();
                int width = this.in.getWidth() / db;
                while (true) {
                    int i2 = i;
                    if (i2 < da) {
                        int i3 = x;
                        for (int i4 = 0; i4 < cZ; i4++) {
                            int A = A(i4, i2);
                            if (A < 0) {
                                A = aU(A);
                            }
                            if (A != 0) {
                                int i5 = A - 1;
                                oVar.a(this.in, db * (i5 % width), (i5 / width) * dc, db, dc, 0, i3, y, 20);
                            }
                            i3 += db;
                        }
                        i = i2 + 1;
                        y += dc;
                    } else {
                        return;
                    }
                }
            }
        }
    }

    public final int cZ() {
        return this.ik;
    }

    public final int da() {
        return this.il;
    }

    public final int db() {
        return this.iv;
    }

    public final int dc() {
        return this.iu;
    }

    public void k(int i, int i2, int i3) {
        synchronized (this) {
            if ((-i3) - 1 >= this.iz || i3 > this.iw) {
                throw new IndexOutOfBoundsException();
            }
            this.ix[i2][i] = i3;
        }
    }

    public void z(int i, int i2) {
        synchronized (this) {
            int i3 = (-i) - 1;
            if (i3 >= 0) {
                if (i3 < this.iz) {
                    if (i2 >= 0) {
                        if (i2 <= this.iw) {
                            this.iy[i3] = i2;
                        }
                    }
                    throw new IndexOutOfBoundsException();
                }
            }
            throw new IndexOutOfBoundsException();
        }
    }
}
