package com.a.a.k;

import java.io.IOException;

public class b extends IOException {
    public static final byte BAD_EXTENSIONS = 1;
    public static final byte BROKEN_CHAIN = 11;
    public static final byte CERTIFICATE_CHAIN_TOO_LONG = 2;
    public static final byte EXPIRED = 3;
    public static final byte INAPPROPRIATE_KEY_USAGE = 10;
    public static final byte MISSING_SIGNATURE = 5;
    public static final byte NOT_YET_VALID = 6;
    public static final byte ROOT_CA_EXPIRED = 12;
    public static final byte SITENAME_MISMATCH = 7;
    public static final byte UNAUTHORIZED_INTERMEDIATE_CA = 4;
    public static final byte UNRECOGNIZED_ISSUER = 8;
    public static final byte UNSUPPORTED_PUBLIC_KEY_TYPE = 13;
    public static final byte UNSUPPORTED_SIGALG = 9;
    public static final byte VERIFICATION_FAILED = 14;
    private static final long serialVersionUID = 1;
    private a jO;
    private byte jP;

    public b(a aVar, byte b) {
        this.jO = aVar;
        this.jP = b;
    }

    public b(String str, a aVar, byte b) {
        super(str);
        this.jO = aVar;
        this.jP = b;
    }

    public a eC() {
        return this.jO;
    }

    public byte eD() {
        return this.jP;
    }
}
