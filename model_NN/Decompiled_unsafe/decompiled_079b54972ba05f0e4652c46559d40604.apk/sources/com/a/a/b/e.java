package com.a.a.b;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import com.a.a.b.i;
import com.chinaMobile.MobileAgent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;
import java.util.Vector;
import org.meteoroid.core.l;

public class e {
    public static final int CACHED = 0;
    public static final int GIAC = 10390323;
    public static final int LIAC = 10390272;
    public static final int NOT_DISCOVERABLE = 0;
    public static final int PREKNOWN = 1;
    public static final e fb = new e();
    protected static f fk;
    public UUID fc;
    protected final BroadcastReceiver fd = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if ("android.bluetooth.device.action.FOUND".equals(intent.getAction())) {
                Log.d("DiscoveryAgent", "发现设备");
                BluetoothDevice bluetoothDevice = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                l lVar = new l(bluetoothDevice);
                d dVar = new d(bluetoothDevice.getBluetoothClass());
                Log.d("DiscoveryAgent", "name = " + bluetoothDevice.getName());
                Log.d("DiscoveryAgent", "add = " + bluetoothDevice.getAddress());
                if (bluetoothDevice.getBluetoothClass().getMajorDeviceClass() == 512 && bluetoothDevice.getBluetoothClass().getDeviceClass() == 524) {
                    Log.d("DiscoveryAgent", "这是一个android手机设备.添加到vector");
                    e.this.fl.add(bluetoothDevice);
                }
                e.this.ff.a(lVar, dVar);
            }
        }
    };
    protected final BroadcastReceiver fe = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if ("android.bluetooth.adapter.action.DISCOVERY_FINISHED".equals(intent.getAction())) {
                e.this.ff.aj(0);
                l.getActivity().unregisterReceiver(e.this.fd);
                l.getActivity().unregisterReceiver(e.this.fe);
                Log.d("DiscoveryAgent", "设备搜索结束");
            }
        }
    };
    f ff;
    boolean fg = false;
    int fh;
    ArrayList<a> fi = new ArrayList<>();
    public Thread fj = new Thread() {
        public void run() {
            int i = 0;
            Log.d("DISCOVERYAGENT", MobileAgent.USER_STATUS_START);
            while (true) {
                int i2 = i;
                if (i2 < e.this.fi.size()) {
                    try {
                        BluetoothDevice bluetoothDevice = e.this.fi.get(i2).fn.fK;
                        Log.d("DiscoveryAgent", "server name = " + bluetoothDevice.getName());
                        e.fk.a(e.this.fi.get(i2).index, new k[]{new k(bluetoothDevice, o.fc.toString())});
                    } catch (Exception e) {
                        Log.d("DISCOVERYAGENT", "搜索服务器error = ", e);
                    }
                    e.fk.n(e.this.fi.get(i2).index, 1);
                    i = i2 + 1;
                } else {
                    Log.d("DiscoveryAgent", "搜索服务完成");
                    return;
                }
            }
        }
    };
    public Vector<BluetoothDevice> fl = new Vector<>();
    int index = 0;

    protected class a {
        l fn;
        int index;

        protected a() {
        }
    }

    public synchronized int a(int[] iArr, o[] oVarArr, l lVar, f fVar) {
        a aVar;
        Log.d("DiscoveryAgent", "开始搜索服务器");
        if (this.fc == null) {
            for (int i = 0; i < oVarArr.length; i++) {
                o oVar = oVarArr[i];
                if (o.fc != null) {
                    o oVar2 = oVarArr[i];
                    this.fc = o.fc;
                }
            }
        }
        aVar = new a();
        int i2 = this.index;
        this.index = i2 + 1;
        aVar.index = i2;
        aVar.fn = lVar;
        fk = fVar;
        this.fi.add(aVar);
        Log.d("DiscoveryAgent", "add server = " + this.index);
        new Thread() {
            public void run() {
                Log.d("DISCOVERYAGENT", MobileAgent.USER_STATUS_START);
                try {
                    BluetoothDevice bluetoothDevice = e.this.fi.get(0).fn.fK;
                    Log.d("DiscoveryAgent", "server name = " + bluetoothDevice.getName());
                    e.fk.a(e.this.fi.get(0).index, new k[]{new k(bluetoothDevice, o.fc.toString())});
                } catch (Exception e) {
                    Log.d("DISCOVERYAGENT", "搜索服务器error = ", e);
                }
                e.fk.n(e.this.fi.get(0).index, 1);
                e.this.fi.remove(0);
                Log.d("DiscoveryAgent", "搜索服务完成");
            }
        }.start();
        return aVar.index;
    }

    public String a(UUID uuid, int i, boolean z) {
        Log.d("DiscoveryAgent", "!!!!!!!!!!!!!!!!!!!!!!!1selectService");
        return null;
    }

    public boolean a(int i, f fVar) {
        this.ff = fVar;
        this.fh = i;
        Log.d("DiscoveryAgent", "开始搜索设备");
        i.a aVar = i.fq;
        this.fg = i.a.ft.startDiscovery();
        l.getActivity().registerReceiver(this.fd, new IntentFilter("android.bluetooth.device.action.FOUND"));
        l.getActivity().registerReceiver(this.fe, new IntentFilter("android.bluetooth.adapter.action.DISCOVERY_FINISHED"));
        return this.fg;
    }

    public boolean a(f fVar) {
        fVar.aj(2);
        i.a aVar = i.fq;
        return i.a.ft.cancelDiscovery();
    }

    public l[] ah(int i) {
        Log.d("DiscoveryAgent", "retrieveDevices");
        Set<BluetoothDevice> bondedDevices = BluetoothAdapter.getDefaultAdapter().getBondedDevices();
        l[] lVarArr = new l[bondedDevices.size()];
        int i2 = 0;
        Iterator<BluetoothDevice> it = bondedDevices.iterator();
        while (true) {
            int i3 = i2;
            if (it.hasNext()) {
                lVarArr[i3] = new l(it.next());
                i2 = i3 + 1;
            } else {
                Log.d("DiscoveryAgent", "devicies.size = " + lVarArr.length);
                return lVarArr;
            }
        }
    }

    public boolean ai(int i) {
        i.a aVar = i.fq;
        return i.a.ft.cancelDiscovery();
    }
}
