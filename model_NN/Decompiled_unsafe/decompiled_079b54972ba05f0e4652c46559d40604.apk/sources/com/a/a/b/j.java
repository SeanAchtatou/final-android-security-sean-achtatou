package com.a.a.b;

import android.bluetooth.BluetoothSocket;
import android.util.Log;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;

public class j implements g {
    byte[] ec;
    protected b fA;
    protected a fB;
    public BluetoothSocket fC;
    protected boolean fD;
    protected boolean fE = false;
    boolean fF;
    Vector<byte[]> fG = new Vector<>();
    boolean fH = false;
    protected c fy;
    protected d fz;

    public static class a extends DataInputStream {
        public a(InputStream inputStream) {
            super(inputStream);
        }

        public void close() {
        }
    }

    public static class b extends DataOutputStream {
        public b(OutputStream outputStream) {
            super(outputStream);
        }

        public void close() {
        }
    }

    public static class c extends InputStream {
        InputStream fI;

        public c(InputStream inputStream) {
            this.fI = inputStream;
        }

        public void close() {
        }

        public int read() {
            return this.fI.read();
        }
    }

    public static class d extends OutputStream {
        OutputStream fJ;

        public d(OutputStream outputStream) {
            this.fJ = outputStream;
        }

        public void close() {
        }

        public void write(int i) {
            this.fJ.write(i);
        }
    }

    public j(BluetoothSocket bluetoothSocket, boolean z) {
        this.fC = bluetoothSocket;
        this.fD = z;
        Log.d("MyL2CAPConnection", "socket Create Completed  is server = " + z);
    }

    public int aF() {
        Log.d("MyL2CAPConnection", "getTransmitMTU");
        return 48;
    }

    public int aG() {
        Log.d("MyL2CAPConnection", "getReceiveMTU");
        return 48;
    }

    public DataInputStream aQ() {
        Log.d("MyL2CAPConnection", "openDataInputStream   " + this.fD);
        if (this.fB == null) {
            this.fB = new a(this.fC.getInputStream());
        }
        return this.fB;
    }

    public InputStream aR() {
        Log.d("MyL2CAPConnection", "openInputStream   " + this.fD);
        if (this.fy == null) {
            this.fy = new c(this.fC.getInputStream());
        }
        return this.fy;
    }

    public DataOutputStream aS() {
        if (this.fA == null) {
            this.fA = new b(this.fC.getOutputStream());
        }
        Log.d("MyL2CAPConnection", "openDataOutputStream   " + this.fD);
        return this.fA;
    }

    public OutputStream aT() {
        Log.d("MyL2CAPConnection", "openOutputStream   " + this.fD);
        if (this.fz == null) {
            this.fz = new d(this.fC.getOutputStream());
        }
        return this.fz;
    }

    public void close() {
        Log.d("MyL2CAP", "close");
    }

    public void q(byte[] bArr) {
        if (this.fz == null) {
            this.fz = new d(this.fC.getOutputStream());
        }
        this.fz.write(bArr.length);
        this.fz.write(bArr);
        this.fz.flush();
    }

    public int r(byte[] bArr) {
        if (this.fy == null) {
            this.fy = new c(this.fC.getInputStream());
        }
        int read = this.fy.read();
        if (read == 0) {
            this.ec = new byte[256];
        } else {
            this.ec = new byte[read];
        }
        int read2 = this.fy.read(this.ec) + 0;
        if (this.ec.length > bArr.length) {
            System.arraycopy(this.ec, 0, bArr, 0, bArr.length);
        } else {
            System.arraycopy(this.ec, 0, bArr, 0, this.ec.length);
        }
        return read;
    }

    public boolean ready() {
        return true;
    }
}
