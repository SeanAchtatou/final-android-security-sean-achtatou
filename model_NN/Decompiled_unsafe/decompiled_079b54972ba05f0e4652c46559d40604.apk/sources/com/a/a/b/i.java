package com.a.a.b;

import android.bluetooth.BluetoothAdapter;
import android.os.Message;
import android.util.Log;
import com.a.a.c.b;
import org.meteoroid.core.c;
import org.meteoroid.core.h;
import org.meteoroid.core.l;
import org.meteoroid.plugin.device.MIDPDevice;

public class i {
    private static final int REQUEST_DISCOVERABLE = 1;
    private static final int REQUEST_ENABLE = 2;
    public static final a fq = new a();
    private d fo;
    private int fp;
    String fr;

    public static class a extends i implements h.a {
        public static final int MSG_MYLOCALDEVICE_GETDEVICE = 65025;
        public static BluetoothAdapter ft = null;
        boolean fu = false;
        boolean fv = false;
        k fw;

        public a() {
            h.a(this);
            h.h(MSG_MYLOCALDEVICE_GETDEVICE, "MSG_MYLOCALDEVICE_GETDEVICE");
            aP();
        }

        public m a(b bVar) {
            Log.e("LOcalDevice", "getRecord");
            if (this.fw == null) {
                if (o.fc != null) {
                    this.fw = new k(o.fc.toString());
                } else {
                    this.fw = new k("11111111-2222-3333-4444-555555555555");
                }
            }
            return this.fw;
        }

        public void a(m mVar) {
            this.fw = (k) mVar;
        }

        /* JADX WARNING: Removed duplicated region for block: B:5:0x001b  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public com.a.a.b.e aJ() {
            /*
                r3 = this;
                android.bluetooth.BluetoothAdapter r0 = com.a.a.b.i.a.ft
                boolean r0 = r0.isEnabled()
                if (r0 != 0) goto L_0x002c
                android.content.Intent r0 = new android.content.Intent
                java.lang.String r1 = "android.bluetooth.adapter.action.REQUEST_ENABLE"
                r0.<init>(r1)
                android.app.Activity r1 = org.meteoroid.core.l.getActivity()
                r2 = 2
                r1.startActivityForResult(r0, r2)
            L_0x0017:
                boolean r0 = r3.fu
                if (r0 != 0) goto L_0x001f
                boolean r0 = r3.fv
                if (r0 == 0) goto L_0x0017
            L_0x001f:
                boolean r0 = r3.fu
                if (r0 == 0) goto L_0x0026
                com.a.a.b.e r0 = com.a.a.b.e.fb
            L_0x0025:
                return r0
            L_0x0026:
                java.lang.NullPointerException r0 = new java.lang.NullPointerException
                r0.<init>()
                throw r0
            L_0x002c:
                com.a.a.b.e r0 = com.a.a.b.e.fb
                goto L_0x0025
            */
            throw new UnsupportedOperationException("Method not decompiled: com.a.a.b.i.a.aJ():com.a.a.b.e");
        }

        public void aP() {
            try {
                ft = BluetoothAdapter.getDefaultAdapter();
            } catch (RuntimeException e) {
                l.getHandler().post(new Runnable() {
                    public void run() {
                        a.ft = BluetoothAdapter.getDefaultAdapter();
                    }
                });
                do {
                } while (ft == null);
            } catch (Exception e2) {
                e2.printStackTrace();
                Log.e("LOcalDevice", "setDevice ERROR");
            }
        }

        /* JADX WARNING: Removed duplicated region for block: B:6:0x003a  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean ak(int r5) {
            /*
                r4 = this;
                r0 = 1
                java.lang.String r1 = "LocalDevice"
                java.lang.String r2 = "SetDiscoverable"
                android.util.Log.e(r1, r2)
                android.bluetooth.BluetoothAdapter r1 = com.a.a.b.i.a.ft
                int r1 = r1.getScanMode()
                r2 = 23
                if (r1 != r2) goto L_0x001a
                java.lang.String r1 = "LocalDevice"
                java.lang.String r2 = "getScanMode"
                android.util.Log.e(r1, r2)
            L_0x0019:
                return r0
            L_0x001a:
                java.lang.String r1 = "LocalDevice"
                java.lang.String r2 = "NOT getScanMode"
                android.util.Log.e(r1, r2)
                android.content.Intent r1 = new android.content.Intent
                java.lang.String r2 = "android.bluetooth.adapter.action.REQUEST_DISCOVERABLE"
                r1.<init>(r2)
                java.lang.String r2 = "android.bluetooth.adapter.extra.DISCOVERABLE_DURATION"
                r3 = 180(0xb4, float:2.52E-43)
                r1.putExtra(r2, r3)
                android.app.Activity r2 = org.meteoroid.core.l.getActivity()
                r2.startActivityForResult(r1, r0)
            L_0x0036:
                boolean r0 = r4.fu
                if (r0 != 0) goto L_0x003e
                boolean r0 = r4.fv
                if (r0 == 0) goto L_0x0036
            L_0x003e:
                java.lang.String r0 = "LocalDevice"
                java.lang.String r1 = "设置可查找 180秒"
                android.util.Log.e(r0, r1)
                boolean r0 = r4.fu
                goto L_0x0019
            */
            throw new UnsupportedOperationException("Method not decompiled: com.a.a.b.i.a.ak(int):boolean");
        }

        public boolean b(Message message) {
            switch (message.what) {
                case l.MSG_SYSTEM_ACTIVITY_RESULT /*47880*/:
                    Object[] objArr = (Object[]) message.obj;
                    int i = ((int[]) objArr[0])[0];
                    int i2 = ((int[]) objArr[0])[1];
                    if (i != 2) {
                        if (i == 1) {
                            if (i2 != 0) {
                                this.fu = true;
                                break;
                            } else {
                                this.fv = true;
                                break;
                            }
                        }
                    } else if (i2 != -1) {
                        this.fv = true;
                        break;
                    } else {
                        this.fu = true;
                        break;
                    }
                    break;
            }
            return false;
        }
    }

    public static i aI() {
        a aVar;
        synchronized (fq) {
            Log.e("LocalDevice", "获取本地蓝牙");
            aVar = fq;
        }
        return aVar;
    }

    public static boolean aO() {
        Log.e("LocalDevice", "isPowerOn");
        return true;
    }

    public static String getProperty(String str) {
        if (str.equals("bluetooth.connected.devices.max")) {
            return "7";
        }
        Log.e("LocalDevice", "getProperty   = " + str);
        return "";
    }

    public m a(b bVar) {
        return fq.a(bVar);
    }

    public void a(m mVar) {
        Log.e("LocalDevice", "updateRecord");
        fq.a(mVar);
    }

    public e aJ() {
        return fq.aJ();
    }

    public String aK() {
        if (((MIDPDevice) c.mN).oO != "") {
            return ((MIDPDevice) c.mN).oO;
        }
        a aVar = fq;
        return a.ft.getName();
    }

    public d aL() {
        return this.fo;
    }

    public int aM() {
        return e.GIAC;
    }

    public String aN() {
        try {
            a aVar = fq;
            this.fr = a.ft.getAddress();
        } catch (RuntimeException e) {
            l.getHandler().post(new Runnable() {
                public void run() {
                    i iVar = i.this;
                    a aVar = i.fq;
                    iVar.fr = a.ft.getAddress();
                }
            });
        } catch (Exception e2) {
            e2.printStackTrace();
            Log.e("LOcalDevice", "setDevice ERROR");
        }
        return this.fr;
    }

    public boolean ak(int i) {
        Log.e("LocalDevice", "setDiscoverable" + fq);
        return fq.ak(i);
    }
}
