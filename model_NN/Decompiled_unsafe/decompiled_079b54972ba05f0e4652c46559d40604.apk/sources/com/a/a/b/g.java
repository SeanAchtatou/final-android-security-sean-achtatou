package com.a.a.b;

import com.a.a.c.q;

public interface g extends q {
    public static final int DEFAULT_MTU = 672;
    public static final int MINIMUM_MTU = 48;

    int aF();

    int aG();

    void q(byte[] bArr);

    int r(byte[] bArr);

    boolean ready();
}
