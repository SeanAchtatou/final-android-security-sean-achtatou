package com.a.a.b;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.util.Log;
import com.a.a.c.b;
import org.meteoroid.core.c;
import org.meteoroid.plugin.device.MIDPDevice;

public class l {
    protected BluetoothDevice fK;
    protected boolean fQ;

    protected l(BluetoothDevice bluetoothDevice) {
        this.fK = bluetoothDevice;
        Log.d("RemoteDevice", "新建远程设备");
    }

    protected l(String str) {
        Log.d("RemoteDevice", "address");
        this.fK = BluetoothAdapter.getDefaultAdapter().getRemoteDevice(str);
    }

    public static l b(b bVar) {
        Log.d("RemoteDevice", "getRemoteDevice");
        if (((MIDPDevice) c.mN).oK != null) {
            return new l(((MIDPDevice) c.mN).oK.fK);
        }
        if (((MIDPDevice) c.mN).oM != null) {
            return new l(((MIDPDevice) c.mN).oM.fK);
        }
        Log.d("RemoteDevice", "getRemoteDevice    device  null");
        return null;
    }

    public boolean a(b bVar, boolean z) {
        Log.d("RemoteDevice", "encrypt");
        return false;
    }

    public final String aN() {
        Log.d("RemoteDevice", "getBluetoothAddress");
        return this.fK.getAddress();
    }

    public boolean aW() {
        Log.d("RemoteDevice", "isTrustedDevice");
        return false;
    }

    public boolean aX() {
        Log.d("RemoteDevice", "authenticate");
        return false;
    }

    public boolean aY() {
        Log.d("RemoteDevice", "isAuthenticated");
        return false;
    }

    public boolean aZ() {
        Log.d("RemoteDevice", "isEncrypted");
        return false;
    }

    public boolean c(b bVar) {
        Log.d("RemoteDevice", "authorize");
        return false;
    }

    public boolean d(b bVar) {
        Log.d("RemoteDevice", "isAuthorized");
        return false;
    }

    public String e(boolean z) {
        Log.d("RemoteDevice", "getFriendlyName" + this.fK.getName());
        return this.fK.getName();
    }

    public boolean equals(Object obj) {
        Log.d("RemoteDevice", "equals");
        return false;
    }

    /* access modifiers changed from: protected */
    public int getBondState() {
        return this.fK.getBondState();
    }

    public int hashCode() {
        Log.d("RemoteDevice", "hashCode");
        return 0;
    }
}
