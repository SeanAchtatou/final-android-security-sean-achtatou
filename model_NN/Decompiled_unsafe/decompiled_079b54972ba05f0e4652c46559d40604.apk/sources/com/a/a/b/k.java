package com.a.a.b;

import android.bluetooth.BluetoothDevice;
import android.util.Log;
import java.util.Hashtable;

public class k implements m {
    BluetoothDevice fK;
    String fL;
    c fM;
    Hashtable<Integer, c> fN = new Hashtable<>();
    public int fO = 0;
    public c fP;

    public k(BluetoothDevice bluetoothDevice, String str) {
        Log.d("MyServiceRecord", "new MyServiceRecord");
        this.fK = bluetoothDevice;
        this.fL = str;
    }

    public k(String str) {
        this.fL = str;
        Log.d("MyServiceRecord", "new MyServiceRecord 2");
    }

    public boolean a(int i, c cVar) {
        Log.d("MyServiceRecord", "setAttributeValue");
        this.fO = i;
        this.fP = cVar;
        return true;
    }

    public int[] aU() {
        Log.d("MyServiceRecord", "getAttributeIDs");
        return new int[]{this.fO};
    }

    public l aV() {
        Log.d("MyServiceRecord", "getHostDevice");
        return new l(this.fK);
    }

    public c al(int i) {
        Log.d("MyServiceRecord", "getAttributeValue ID = " + i);
        if (i == this.fO) {
            return this.fP;
        }
        if (i != 256) {
            return new c(32, this.fK);
        }
        if (this.fM != null) {
            return this.fM;
        }
        Log.d("MyServiceRecord", "getAttributeValue ID = 256  " + this.fK.getName());
        return new c(this.fK.getName(), this.fK);
    }

    public void am(int i) {
        Log.d("MyServiceRecord", "setDeviceServiceClasses");
    }

    public String e(int i, boolean z) {
        Log.d("MyServiceRecord", this.fL);
        String str = this.fL.startsWith("btspp://") ? "btspp://" : this.fL.startsWith("btl2cap://") ? "btl2cap://" : "btl2cap://";
        if (this.fK != null) {
            Log.d("MyServiceRecord", "getConnectionURL=" + str + "0000000AAAAA:1master=false;encrypt=false;authenticate=false" + "MACadd" + this.fK.getAddress());
            return str + "0000000AAAAA:1master=false;encrypt=false;authenticate=false" + "MACadd" + this.fK.getAddress();
        }
        Log.d("MyServiceRecord", "Device Null");
        return str + "0000000AAAAA:1master=false;encrypt=false;authenticate=false";
    }

    public boolean g(int[] iArr) {
        Log.d("MyServiceRecord", "populateRecord");
        return false;
    }
}
