package com.a.a.b;

import android.bluetooth.BluetoothClass;
import android.util.Log;

public class d {
    private int eX;
    private int eY;
    private int eZ;
    BluetoothClass fa;

    public d(int i) {
        Log.e("DeviceClass", "record = " + i);
    }

    protected d(BluetoothClass bluetoothClass) {
        this.fa = bluetoothClass;
    }

    public int aD() {
        Log.e("DeviceClass", "getServiceClasses");
        return this.eX;
    }

    public int aE() {
        Log.e("DeviceClass", "getMinorDeviceClass = " + this.fa.getDeviceClass());
        if (this.fa.getDeviceClass() == 516 || this.fa.getDeviceClass() == 524) {
            Log.e("DeviceClass", "getMinorDeviceClass return 4");
            return 4;
        } else if (this.fa.getDeviceClass() == 268) {
            return 12;
        } else {
            Log.e("DeviceClass", "getMinorDeviceClass 不是已知类型,实际类型是" + this.fa.getDeviceClass());
            return 4;
        }
    }

    public int getMajorDeviceClass() {
        Log.e("DeviceClass", "getMajorDeviceClass = " + this.fa.getMajorDeviceClass());
        return this.fa.getMajorDeviceClass();
    }
}
