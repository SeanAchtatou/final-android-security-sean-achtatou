package com.a.a.g;

public class b {
    public static final int DD_MM = 2;
    public static final int DD_MM_SS = 1;
    protected double iA;
    protected double iB;
    protected float iC;

    public b(double d, double d2, float f) {
        this.iA = d;
        this.iB = d2;
        this.iC = f;
    }

    public static double convert(String str) {
        return 0.0d;
    }

    public static String convert(double d, int i) {
        return null;
    }

    public float a(b bVar) {
        return 0.0f;
    }

    public float b(b bVar) {
        return 0.0f;
    }

    public float dd() {
        return this.iC;
    }

    public void e(float f) {
        this.iC = f;
    }

    public double getLatitude() {
        return this.iA;
    }

    public double getLongitude() {
        return this.iB;
    }

    public void setLatitude(double d) {
        this.iA = d;
    }

    public void setLongitude(double d) {
        this.iB = d;
    }
}
