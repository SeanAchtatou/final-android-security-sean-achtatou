package com.a.a.g;

import android.location.Location;

public class g {
    public static final int MTA_ASSISTED = 262144;
    public static final int MTA_UNASSISTED = 524288;
    public static final int MTE_ANGLEOFARRIVAL = 32;
    public static final int MTE_CELLID = 8;
    public static final int MTE_SATELLITE = 1;
    public static final int MTE_SHORTRANGE = 16;
    public static final int MTE_TIMEDIFFERENCE = 2;
    public static final int MTE_TIMEOFARRIVAL = 4;
    public static final int MTY_NETWORKBASED = 131072;
    public static final int MTY_TERMINALBASED = 65536;
    private m iM;
    private float iQ;
    private float iR;
    private long timestamp;

    public g(Location location) {
        float accuracy = location.getAccuracy();
        this.iQ = location.getSpeed();
        this.iR = location.getBearing();
        this.timestamp = location.getTime();
        this.iM = new m(location.getLatitude(), location.getLongitude(), (float) location.getAltitude(), accuracy, accuracy);
    }

    public String ac(String str) {
        return null;
    }

    public m dk() {
        return this.iM;
    }

    public a dl() {
        return null;
    }

    public float dp() {
        return this.iR;
    }

    public int dq() {
        return 0;
    }

    public float getSpeed() {
        return this.iQ;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public boolean isValid() {
        return true;
    }

    public String toString() {
        return this.iM.toString() + ".Speed:" + this.iQ + ".Time:" + this.timestamp + ".Course:" + this.iR + ".";
    }
}
