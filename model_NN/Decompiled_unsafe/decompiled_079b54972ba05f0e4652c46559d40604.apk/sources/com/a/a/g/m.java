package com.a.a.g;

public class m extends b {
    private float iX;
    private float iY;

    public m(double d, double d2, float f, float f2, float f3) {
        super(d, d2, f);
        this.iX = f2;
        this.iY = f3;
    }

    public float dx() {
        return this.iX;
    }

    public float dy() {
        return this.iY;
    }

    public void f(float f) {
        this.iX = f;
    }

    public void g(float f) {
        this.iY = f;
    }

    public String toString() {
        return "Lat:" + this.iA + ".Log:" + this.iB + ".Alt:" + this.iC + ".HAcc:" + this.iX + ".VAcc:" + this.iY + ".";
    }
}
