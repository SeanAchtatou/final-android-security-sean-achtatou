package com.a.a.g;

public class c {
    public static final int NO_REQUIREMENT = 0;
    public static final int POWER_USAGE_HIGH = 3;
    public static final int POWER_USAGE_LOW = 1;
    public static final int POWER_USAGE_MEDIUM = 2;
    private int iD = 2;
    private int iE = 1;
    private int iF = 1;
    private int iG;
    private boolean iH = true;
    private boolean iI = true;
    private boolean iJ;
    private boolean iK;
    private String iL;

    public void X(String str) {
        this.iL = str;
    }

    public void aW(int i) {
        this.iG = i;
    }

    public void aX(int i) {
        this.iD = i;
    }

    public int de() {
        return this.iD;
    }

    public boolean df() {
        return this.iH;
    }

    public int dg() {
        return this.iG;
    }

    public boolean dh() {
        return this.iI;
    }

    public boolean di() {
        return this.iK;
    }

    public String dj() {
        return this.iL;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        c cVar = (c) obj;
        if (this.iK != cVar.iK) {
            return false;
        }
        if (this.iJ != cVar.iJ) {
            return false;
        }
        if (this.iH != cVar.iH) {
            return false;
        }
        if (this.iF != cVar.iF) {
            return false;
        }
        if (this.iD != cVar.iD) {
            return false;
        }
        if (this.iG != cVar.iG) {
            return false;
        }
        if (this.iI != cVar.iI) {
            return false;
        }
        return this.iE == cVar.iE;
    }

    public int getHorizontalAccuracy() {
        return this.iF;
    }

    public int getVerticalAccuracy() {
        return this.iE;
    }

    public int hashCode() {
        int i = 1231;
        int i2 = ((((((((this.iH ? 1231 : 1237) + (((this.iJ ? 1231 : 1237) + (((this.iK ? 1231 : 1237) + 31) * 31)) * 31)) * 31) + this.iF) * 31) + this.iD) * 31) + this.iG) * 31;
        if (!this.iI) {
            i = 1237;
        }
        return ((i2 + i) * 31) + this.iE;
    }

    public boolean isAltitudeRequired() {
        return this.iJ;
    }

    public void m(boolean z) {
        this.iI = z;
    }

    public void n(boolean z) {
        this.iK = z;
    }

    public void setAltitudeRequired(boolean z) {
        this.iJ = z;
    }

    public void setCostAllowed(boolean z) {
        this.iH = z;
    }

    public void setHorizontalAccuracy(int i) {
        this.iF = i;
    }

    public void setVerticalAccuracy(int i) {
        this.iE = i;
    }
}
