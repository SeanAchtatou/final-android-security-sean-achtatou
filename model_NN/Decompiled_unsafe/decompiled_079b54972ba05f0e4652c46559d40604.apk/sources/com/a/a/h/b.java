package com.a.a.h;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Looper;
import com.a.a.g.c;
import com.a.a.g.g;
import com.a.a.g.h;
import com.a.a.g.i;
import com.a.a.g.j;
import org.meteoroid.core.l;

public class b extends j {
    public static final int ACCURACY_THRESHOLD = 50;
    public static final float DEFAULT_MINIMAL_LOCATION_DISTANCE = 1.0f;
    public static final int DEFAULT_MINIMAL_LOCATION_UPDATES = 1000;
    public static final int DEFAULT_POWER_REQIREMENT = 2;
    private static LocationManager jb;
    private String iL;
    private a jc;
    private a jd;
    private int state = 3;

    private final class a extends Thread {
        private Looper je;

        public a(String str) {
            super(str);
        }

        public Looper getLooper() {
            while (this.je == null) {
                try {
                    Thread.sleep(30);
                    System.out.println("Waited for 30ms for the looper to prepare.");
                } catch (InterruptedException e) {
                }
            }
            return this.je;
        }

        public void run() {
            Looper.prepare();
            this.je = Looper.myLooper();
            Looper.loop();
        }
    }

    private b(String str) {
        this.iL = str;
        this.jd = new a("LocationUpdateThread");
        this.jd.start();
    }

    private String aZ(int i) {
        switch (i) {
            case 1:
                return "Available";
            case 2:
                return "Temporarily Unavailable";
            case 3:
                return "Out of Service";
            default:
                return "Unknown State '" + i + "'";
        }
    }

    public static b b(c cVar) {
        int i;
        int i2 = 2;
        if (jb == null) {
            jb = l.hO();
        }
        if (cVar == null) {
            cVar = new c();
        }
        String dj = cVar.dj();
        if (dj == null) {
            switch (cVar.de()) {
                case 0:
                    i = 2;
                    break;
                case 1:
                    i = 1;
                    break;
                case 2:
                    i = 2;
                    break;
                case 3:
                    i = 3;
                    break;
                default:
                    throw new IllegalArgumentException("The power consumption must be one of Critiera.NO_REQUIREMENT, Criteria.POWER_USAGE_HIGH, Criteria.POWER_USAGE_MEDIUM or Criteria.POWER_USAGE_LOW.");
            }
            boolean isAltitudeRequired = cVar.isAltitudeRequired();
            boolean dh = cVar.dh();
            boolean dh2 = cVar.dh();
            boolean df = cVar.df();
            if (cVar.getHorizontalAccuracy() < 50) {
                i2 = 1;
            }
            Criteria criteria = new Criteria();
            criteria.setAccuracy(i2);
            criteria.setSpeedRequired(dh2);
            criteria.setAltitudeRequired(isAltitudeRequired);
            criteria.setBearingRequired(dh);
            criteria.setCostAllowed(df);
            criteria.setPowerRequirement(i);
            dj = jb.getBestProvider(criteria, true);
        }
        System.out.println("getAndroidLocationProvider: Best provider for criteria is '" + dj + "'");
        if (dj != null) {
            return new b(dj);
        }
        if (!jb.getProviders(true).isEmpty()) {
            return null;
        }
        throw new h("No enabled LocationProvider found. Enable an Location Provider and try again.");
    }

    private void dA() {
        jb.requestLocationUpdates(this.iL, 0, 0.0f, this.jc, this.jd.getLooper());
    }

    public void a(i iVar, int i, int i2, int i3) {
        System.out.println("Setting a location listener:" + iVar);
        if (this.jc == null) {
            this.jc = new a(this);
        }
        if (iVar == null) {
            jb.removeUpdates(this.jc);
            return;
        }
        this.jc.a(iVar);
        dA();
    }

    public g aY(int i) {
        Location lastKnownLocation = jb.getLastKnownLocation(this.iL);
        if (lastKnownLocation == null) {
            System.out.println("getLocation():null received from LocationManager.getLastKnownLocation.");
            return null;
        }
        g gVar = new g(lastKnownLocation);
        j.a(gVar);
        System.out.println("getLocation():" + gVar);
        return gVar;
    }

    public String dB() {
        return this.iL;
    }

    public int getState() {
        return this.state;
    }

    public void reset() {
        if (this.jc != null) {
            jb.removeUpdates(this.jc);
        }
    }

    /* access modifiers changed from: package-private */
    public void setState(int i) {
        this.state = i;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("AndroidLocationProvider{provider='");
        stringBuffer.append(this.iL);
        stringBuffer.append("',state='");
        stringBuffer.append(aZ(getState()));
        stringBuffer.append("'}");
        return stringBuffer.toString();
    }
}
