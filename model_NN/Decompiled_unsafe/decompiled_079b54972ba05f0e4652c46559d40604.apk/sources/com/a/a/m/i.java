package com.a.a.m;

public class i {
    private double kh;
    private double ki;
    private double kj;

    public i(double d, double d2, double d3) {
        this.kh = d;
        this.ki = d2;
        this.kj = d3;
    }

    public double fg() {
        return this.ki;
    }

    public double fh() {
        return this.kj;
    }

    public double fi() {
        return this.kh;
    }
}
