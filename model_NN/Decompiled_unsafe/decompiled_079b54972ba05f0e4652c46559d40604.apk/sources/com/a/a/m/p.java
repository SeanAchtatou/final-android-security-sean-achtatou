package com.a.a.m;

import java.util.HashMap;

public class p {
    public static final HashMap<String, p> kt = new HashMap<>();
    private static String[] ku = {"m/s^2", "Celsius", "degree"};
    private String ks;

    private p() {
    }

    public static p au(String str) {
        if (kt.isEmpty()) {
            for (int i = 0; i < ku.length; i++) {
                p pVar = new p();
                pVar.ks = ku[i];
                kt.put(ku[i], pVar);
            }
        }
        return kt.get(str);
    }

    public String toString() {
        return this.ks;
    }
}
