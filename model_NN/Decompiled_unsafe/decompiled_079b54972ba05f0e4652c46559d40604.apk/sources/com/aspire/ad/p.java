package com.aspire.ad;

import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;

class p extends Handler {
    final /* synthetic */ c dE;

    p(c cVar) {
        this.dE = cVar;
    }

    public void handleMessage(Message message) {
        j jVar = (j) message.obj;
        ImageView imageView = (ImageView) this.dE.dq.findViewWithTag(jVar.c);
        if (imageView != null) {
            imageView.setImageBitmap(jVar.a(null, this.dE.b));
        }
    }
}
