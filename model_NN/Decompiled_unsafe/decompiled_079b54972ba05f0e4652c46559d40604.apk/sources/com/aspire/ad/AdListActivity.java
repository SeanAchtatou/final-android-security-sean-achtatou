package com.aspire.ad;

import android.app.Activity;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.List;

public class AdListActivity extends Activity {
    final Handler a = new b(this);
    /* access modifiers changed from: private */
    public int d = 1;
    /* access modifiers changed from: private */
    public List db;
    /* access modifiers changed from: private */
    public c dg;
    private Button dh;
    private Button di;
    private int e = 1;

    /* access modifiers changed from: private */
    public void a() {
        this.e = ((this.db.size() + 10) - 1) / 10;
        if (this.d > 1) {
            this.dh.setVisibility(0);
        } else {
            this.dh.setVisibility(8);
        }
        if (this.d < this.e) {
            this.di.setVisibility(0);
        } else {
            this.di.setVisibility(8);
        }
    }

    static /* synthetic */ int c(AdListActivity adListActivity) {
        int i = adListActivity.d;
        adListActivity.d = i - 1;
        return i;
    }

    static /* synthetic */ int e(AdListActivity adListActivity) {
        int i = adListActivity.d;
        adListActivity.d = i + 1;
        return i;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(1);
        linearLayout.setBackgroundColor(-1577985);
        TextView textView = new TextView(this);
        textView.setText("精彩应用推荐");
        textView.setTextSize(20.0f);
        textView.setTextColor(-1);
        textView.setSingleLine(true);
        Button button = new Button(this);
        button.setText("返回");
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(9);
        layoutParams.addRule(15);
        layoutParams.setMargins(3, 0, 3, 0);
        textView.setLayoutParams(layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(11);
        layoutParams2.addRule(15);
        layoutParams2.setMargins(0, 3, 3, 0);
        button.setLayoutParams(layoutParams2);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[]{-12541718, -15701056, -12734488});
        gradientDrawable.setGradientType(0);
        relativeLayout.setBackgroundDrawable(gradientDrawable);
        relativeLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        relativeLayout.addView(textView);
        relativeLayout.addView(button);
        ListView listView = new ListView(this);
        listView.setDivider(null);
        listView.setLayoutParams(new LinearLayout.LayoutParams(-1, -1, 1.0f));
        listView.setCacheColorHint(0);
        this.dh = new Button(this);
        this.dh.setText("上一页");
        this.dh.setOnClickListener(new a(this));
        this.di = new Button(this);
        this.di.setText("下一页");
        this.di.setOnClickListener(new i(this));
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(9);
        layoutParams3.addRule(15);
        layoutParams3.setMargins(3, 0, 3, 0);
        this.dh.setLayoutParams(layoutParams3);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams4.addRule(11);
        layoutParams4.addRule(15);
        layoutParams4.setMargins(0, 3, 3, 0);
        this.di.setLayoutParams(layoutParams4);
        RelativeLayout relativeLayout2 = new RelativeLayout(this);
        relativeLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        relativeLayout2.addView(this.dh);
        relativeLayout2.addView(this.di);
        linearLayout.addView(relativeLayout);
        linearLayout.addView(listView);
        linearLayout.addView(relativeLayout2);
        this.db = AdManager.z(this).dl.d(this.a);
        a();
        this.dg = new c(this, this.db, listView);
        this.dg.b(40);
        listView.setAdapter((ListAdapter) this.dg);
        button.setOnClickListener(new h(this));
        listView.setOnItemClickListener(new g(this));
        setContentView(linearLayout);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.dg.a();
        AdManager.z(this).dl.b(this.a);
        super.onDestroy();
    }
}
