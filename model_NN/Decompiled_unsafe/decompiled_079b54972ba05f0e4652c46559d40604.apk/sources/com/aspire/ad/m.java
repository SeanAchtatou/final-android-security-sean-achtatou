package com.aspire.ad;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import com.aspire.a.a;
import com.aspire.a.c;
import com.aspire.a.e;
import com.aspire.a.h;
import com.umeng.common.a;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.util.Calendar;
import java.util.List;
import java.util.Vector;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.entity.StringEntity;

public class m {
    private static m dz = null;
    final Handler a = new l(this);
    private Context c;
    /* access modifiers changed from: private */
    public List cS = null;
    /* access modifiers changed from: private */
    public List dA = null;
    /* access modifiers changed from: private */
    public List dr;

    private m(Context context) {
        this.c = context;
    }

    public static m A(Context context) {
        if (dz == null) {
            dz = new m(context.getApplicationContext());
            dz.b();
        }
        return dz;
    }

    private String a(String str) {
        StringBuilder sb = new StringBuilder("AbilityApiName");
        sb.append("&").append(AdManager.z(this.c).a).append("&").append(AdManager.z(this.c).b).append("&").append(str);
        Log.d("AdData", "plain text: " + sb.toString());
        byte[] digest = MessageDigest.getInstance("MD5").digest(sb.toString().getBytes());
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        char[] cArr2 = new char[(digest.length * 2)];
        int i = 0;
        for (byte b : digest) {
            int i2 = i + 1;
            cArr2[i] = cArr[(b >>> 4) & 15];
            i = i2 + 1;
            cArr2[i2] = cArr[b & 15];
        }
        return c.a(new String(cArr2));
    }

    public static void a() {
        if (dz != null) {
            dz.c();
            dz = null;
        }
    }

    private void b() {
        this.cS = new Vector();
        this.dA = new Vector();
        this.dr = new Vector();
        d();
    }

    private void c() {
        e eVar = new e();
        e eVar2 = new e();
        eVar.a("root", eVar2);
        for (j jVar : this.cS) {
            e eVar3 = new e();
            eVar3.a(a.b, Integer.valueOf(jVar.a));
            eVar3.a("id", Long.valueOf(jVar.b));
            if (jVar.c != null) {
                eVar3.a("logo", jVar.c);
            }
            if (jVar.d != null) {
                eVar3.a("logoPath", jVar.d);
            }
            eVar3.a("name", jVar.e);
            if (jVar.f != null) {
                eVar3.a("desc", jVar.f);
            }
            if (jVar.g != null) {
                eVar3.a("url", jVar.g);
            }
            eVar2.a("item", eVar3);
        }
        StringBuilder sb = new StringBuilder();
        e.a(eVar, sb);
        h.a(AdManager.c + "ads.xml", sb.toString());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.aspire.a.a.a(java.lang.String, org.apache.http.HttpHost, java.util.List, org.apache.http.HttpEntity, com.aspire.a.a$b):void
     arg types: [java.lang.String, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], org.apache.http.entity.StringEntity, com.aspire.ad.k]
     candidates:
      com.aspire.a.a.a(com.aspire.a.a$b, java.lang.String, long, org.apache.http.HttpResponse, java.io.InputStream):void
      com.aspire.a.a.a(java.lang.String, org.apache.http.HttpHost, java.util.List, org.apache.http.HttpEntity, com.aspire.a.a$b):void */
    /* access modifiers changed from: private */
    public void d() {
        k kVar = new k(this);
        e eVar = new e();
        e eVar2 = new e();
        long timeInMillis = Calendar.getInstance().getTimeInMillis();
        eVar.a("MsgType", "AppInfoReq");
        eVar.a("Version", "1.0.0");
        eVar.a("RandNumb", String.valueOf(timeInMillis));
        eVar.a("AppID", Long.valueOf(AdManager.z(this.c).a));
        eVar.a("Timestamp", String.valueOf(timeInMillis / 1000));
        try {
            eVar.a("Signature", a(String.valueOf(timeInMillis / 1000)));
            eVar2.a("InfoType", "0");
            eVar2.a("StartNum", Integer.valueOf(this.cS.size() + 1));
            eVar2.a("MaxNum", "10");
            eVar2.a("SysCode", "5610005254");
            e eVar3 = new e();
            e eVar4 = new e();
            eVar3.a("EmbeddedInfoReq", eVar4);
            eVar4.a("Head", eVar);
            eVar4.a("Body", eVar2);
            StringBuilder sb = new StringBuilder();
            e.a(eVar3, sb);
            try {
                String sb2 = sb.toString();
                Log.i("AdData", "register xmldata=" + sb2);
                com.aspire.a.a.x(this.c).a("http://recommend.mmarket.com:8082/api/Recommend", (HttpHost) null, (List) null, (HttpEntity) new StringEntity(sb2, com.umeng.common.b.e.f), (a.b) kVar);
            } catch (UnsupportedEncodingException e) {
                Log.e("AdData", "reqBody UnsupportedEncodingException: " + e.getMessage());
            }
        } catch (Exception e2) {
            Log.e("AdData", "Signature Exception: " + e2.getMessage());
        }
    }

    public void b(Handler handler) {
        this.dr.remove(handler);
    }

    public List d(Handler handler) {
        if (!this.dr.contains(handler)) {
            this.dr.add(handler);
        }
        return this.cS;
    }
}
