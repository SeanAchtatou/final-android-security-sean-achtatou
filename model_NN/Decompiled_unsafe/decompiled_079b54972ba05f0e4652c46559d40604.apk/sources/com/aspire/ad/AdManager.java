package com.aspire.ad;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import com.a.a.i.k;
import com.aspire.a.h;
import java.util.Timer;
import java.util.TimerTask;

public class AdManager {
    public static String c = null;
    private static AdManager dj = null;
    public long a = 300001948320L;
    public String b = "E69041425C759E5A";
    private Context dk = null;
    public m dl = null;
    private Timer dm;
    private TimerTask dn;
    /* access modifiers changed from: private */

    /* renamed from: do  reason: not valid java name */
    public Handler f1do;

    private AdManager(Context context) {
        this.dk = context;
    }

    private void b() {
        if (h.a()) {
            c = h.b() + "aspiread/";
        } else {
            c = h.a(this.dk);
        }
        h.a(c);
        this.dl = m.A(this.dk);
    }

    public static AdManager z(Context context) {
        if (dj == null) {
            dj = new AdManager(context.getApplicationContext());
        }
        return dj;
    }

    public void a() {
        Intent intent = new Intent(this.dk, AdListActivity.class);
        intent.addFlags(k.DECEMBER);
        this.dk.startActivity(intent);
    }

    public void a(long j, String str) {
        this.a = j;
        this.b = str;
        b();
    }

    public void a(Handler handler) {
        if (handler != null) {
            this.f1do = handler;
            if (this.dn == null) {
                this.dn = new n(this);
            }
            if (this.dm == null) {
                this.dm = new Timer();
                this.dm.schedule(this.dn, 10000, 10000);
            }
        }
    }

    public void a(j jVar) {
        if (jVar != null) {
            String str = (jVar.a != 0 || TextUtils.isEmpty(jVar.g)) ? "http://recommend.mmarket.com:8082/ad.do?adCode=5610005254&cntid=" + jVar.b : jVar.g;
            Log.d("zyg", "detail GET : " + str);
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            intent.addFlags(k.DECEMBER);
            this.dk.startActivity(intent);
        }
    }

    public void finalize() {
        if (this.dm != null) {
            this.dm.cancel();
            this.dm = null;
        }
        m.a();
        dj = null;
    }
}
