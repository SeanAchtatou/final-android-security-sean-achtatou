package com.aspire.ad;

import android.util.Log;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class o extends DefaultHandler {
    private String a = null;
    private String b = null;
    private List dC;
    private j dD;

    public o(List list) {
        this.dC = list;
    }

    public void characters(char[] cArr, int i, int i2) {
        if (this.a != null) {
            String trim = new String(cArr, i, i2).trim();
            if (this.b == null) {
                this.b = trim;
            } else {
                this.b += trim;
            }
        }
    }

    public void endElement(String str, String str2, String str3) {
        if (str2.equals("InfoItem")) {
            this.dC.add(this.dD);
            this.dD = null;
        } else if (str2.equals("MsgType")) {
            if (!this.b.equals("AppInfoResp")) {
                Log.e("AdSaxHandler", "MsgType != AppInfoResp (" + this.b + ")");
            }
        } else if (str2.equals("ReturnCode")) {
            if (!this.b.equals("0")) {
                Log.e("AdSaxHandler", "ReturnCode != 0 (" + this.b + ")");
            }
        } else if (!str2.equals("TotalNum") && !str2.equals("BatchNum")) {
            if (str2.equals("InfoType")) {
                if (this.dD != null) {
                    this.dD.a = Integer.parseInt(this.b);
                }
            } else if (str2.equals("InfoName")) {
                if (this.dD != null) {
                    this.dD.e = this.b;
                }
            } else if (str2.equals("InfoID")) {
                if (this.dD != null) {
                    this.dD.b = Long.parseLong(this.b);
                }
            } else if (str2.equals("InfoLogo")) {
                if (this.dD != null) {
                    this.dD.c = this.b;
                }
            } else if (str2.equals("DirectUrl")) {
                if (this.dD != null) {
                    this.dD.g = this.b;
                }
            } else if (str2.equals("AppcateName") && this.dD != null) {
                this.dD.f = this.b;
            }
        }
        this.a = null;
    }

    public void startElement(String str, String str2, String str3, Attributes attributes) {
        if (str2.equals("InfoItem")) {
            this.dD = new j();
        }
        this.a = str2;
        this.b = null;
    }
}
