package com.aspire.ad;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import java.util.List;

public class c extends BaseAdapter {
    final Handler a = new p(this);
    /* access modifiers changed from: private */
    public Context b = null;
    private int d = 14;
    /* access modifiers changed from: private */
    public ListView dq;
    private List dr;
    private int e = 10;
    private int g = 1;

    private final class a {
        public ImageView ds;
        public TextView dt;
        public TextView du;

        private a() {
        }

        /* synthetic */ a(c cVar, p pVar) {
            this();
        }
    }

    public c(Context context, List list, ListView listView) {
        this.b = context;
        this.dr = list;
        this.dq = listView;
    }

    private LinearLayout ai() {
        a aVar = new a(this, null);
        aVar.ds = new ImageView(this.b);
        aVar.dt = new TextView(this.b);
        aVar.dt.setTextSize((float) this.d);
        aVar.dt.setSingleLine(true);
        aVar.dt.setPadding(0, 0, 0, 0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        layoutParams.setMargins(0, 0, 0, 0);
        aVar.dt.setLayoutParams(layoutParams);
        aVar.du = new TextView(this.b);
        aVar.du.setTextSize((float) this.e);
        aVar.du.setSingleLine(true);
        aVar.du.setPadding(0, 0, 0, 0);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams2.setMargins(0, 0, 0, 0);
        aVar.du.setLayoutParams(layoutParams2);
        LinearLayout linearLayout = new LinearLayout(this.b);
        linearLayout.setOrientation(1);
        linearLayout.setPadding(0, 0, 0, 0);
        linearLayout.addView(aVar.dt);
        linearLayout.addView(aVar.du);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(40, 40);
        layoutParams3.gravity = 16;
        layoutParams3.setMargins(3, 0, 3, 0);
        aVar.ds.setLayoutParams(layoutParams3);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-2, -2);
        layoutParams4.gravity = 16;
        layoutParams4.setMargins(0, 0, 0, 0);
        linearLayout.setLayoutParams(layoutParams4);
        LinearLayout linearLayout2 = new LinearLayout(this.b);
        linearLayout2.setOrientation(0);
        linearLayout2.setPadding(0, 0, 0, 0);
        linearLayout2.addView(aVar.ds);
        linearLayout2.addView(linearLayout);
        linearLayout2.setTag(aVar);
        return linearLayout2;
    }

    public void a() {
        for (j a2 : this.dr) {
            a2.a(this.a);
        }
    }

    public void a(int i) {
        if (i <= ((this.dr.size() + 10) - 1) / 10) {
            this.g = i;
            notifyDataSetInvalidated();
        }
    }

    public void b(int i) {
        this.d = (i + 4) / 2;
        this.e = (i - 4) / 2;
    }

    public int getCount() {
        int size = this.dr.size() - ((this.g - 1) * 10);
        if (size > 10) {
            return 10;
        }
        return size;
    }

    public Object getItem(int i) {
        return null;
    }

    public long getItemId(int i) {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        j jVar = (j) this.dr.get(((this.g - 1) * 10) + i);
        if (view == null) {
            view = ai();
        }
        a aVar = (a) view.getTag();
        Bitmap a2 = jVar.a(this.a, this.b);
        if (a2 != null) {
            aVar.ds.setImageBitmap(a2);
        }
        aVar.ds.setTag(jVar.c);
        aVar.dt.setText(jVar.e);
        aVar.du.setText(jVar.f);
        if (i % 2 == 0) {
            view.setBackgroundColor(-3154182);
            aVar.du.setTextColor(-16298875);
        } else {
            view.setBackgroundColor(-1577985);
            aVar.du.setTextColor(-16298875);
        }
        aVar.dt.setTextColor(-16298875);
        return view;
    }
}
