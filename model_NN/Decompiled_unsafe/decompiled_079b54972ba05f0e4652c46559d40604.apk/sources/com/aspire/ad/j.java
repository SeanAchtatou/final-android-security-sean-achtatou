package com.aspire.ad;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import com.aspire.a.a;
import com.aspire.a.b;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;

public class j {
    public int a;
    public long b;
    public String c;
    public String d;
    /* access modifiers changed from: private */
    public List dx;
    public String e = "";
    public String f = "应用";
    public String g;

    private void a(Context context) {
        q qVar = new q(this);
        if (this.c != null && this.c.length() > 0) {
            a.x(context).a(this.c, (HttpHost) null, (List) null, (HttpEntity) null, qVar);
        }
    }

    public Bitmap a(Handler handler, Context context) {
        if (this.d != null && this.d.length() > 0) {
            return b.a(this.d, 40, 40);
        }
        if (handler != null) {
            if (this.dx == null) {
                this.dx = new ArrayList();
            }
            if (!this.dx.contains(handler)) {
                this.dx.add(handler);
            }
        }
        a(context);
        return b.a(context, "adicon.png", 40, 40);
    }

    public void a(Handler handler) {
        if (this.dx != null) {
            this.dx.remove(handler);
        }
    }
}
