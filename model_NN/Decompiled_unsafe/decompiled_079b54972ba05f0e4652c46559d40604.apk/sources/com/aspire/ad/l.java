package com.aspire.ad;

import android.os.Handler;
import android.os.Message;

class l extends Handler {
    final /* synthetic */ m dy;

    l(m mVar) {
        this.dy = mVar;
    }

    public void handleMessage(Message message) {
        if (this.dy.dA.size() == 0) {
            for (Handler handler : this.dy.dr) {
                Message obtainMessage = handler.obtainMessage(0, null);
                obtainMessage.what = 1;
                handler.sendMessage(obtainMessage);
            }
            this.dy.dr.clear();
            return;
        }
        this.dy.cS.addAll(this.dy.dA);
        this.dy.dA.clear();
        this.dy.d();
    }
}
