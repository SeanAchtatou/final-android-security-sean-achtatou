package com.aspire.ad;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.PowerManager;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.List;

public class AdBannar extends LinearLayout {
    Handler a = new e(this);
    /* access modifiers changed from: private */
    public Context cO;
    private int d = 0;
    private List db;
    /* access modifiers changed from: private */
    public j dc;
    /* access modifiers changed from: private */
    public ImageView dd;
    private TextView de;
    private TextView df;

    public AdBannar(Context context) {
        super(context);
        this.cO = context;
        c();
    }

    public AdBannar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.cO = context;
        c();
    }

    /* access modifiers changed from: private */
    public void a() {
        try {
            this.dc = (j) this.db.get(this.d);
            Bitmap a2 = this.dc.a(this.a, this.cO);
            if (a2 != null) {
                this.dd.setImageBitmap(a2);
            }
            this.de.setText(this.dc.e);
            this.df.setText(this.dc.f);
            this.d++;
            if (this.d >= this.db.size()) {
                this.d = 0;
            }
        } catch (Exception e) {
            this.d = 0;
            this.dc = null;
            this.dd.setImageBitmap(null);
            this.de.setText((CharSequence) null);
            this.df.setText((CharSequence) null);
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        try {
            List<ActivityManager.RunningTaskInfo> runningTasks = ((ActivityManager) this.cO.getSystemService("activity")).getRunningTasks(2);
            if (runningTasks != null && !runningTasks.isEmpty()) {
                if (this.cO.getClass().toString().contains(runningTasks.get(0).topActivity.getShortClassName())) {
                    if (Build.VERSION.SDK_INT >= 7) {
                        PowerManager powerManager = (PowerManager) this.cO.getSystemService("power");
                        if (!((Boolean) powerManager.getClass().getDeclaredMethod("isScreenOn", new Class[0]).invoke(powerManager, new Object[0])).booleanValue()) {
                            return;
                        }
                    }
                    a();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void c() {
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[]{-12541718, -15701056, -12734488});
        gradientDrawable.setGradientType(0);
        setBackgroundDrawable(gradientDrawable);
        this.dd = new ImageView(this.cO);
        this.de = new TextView(this.cO);
        this.de.setTextSize(20.0f);
        this.de.setTextColor(-1);
        this.de.setSingleLine(true);
        this.df = new TextView(this.cO);
        this.df.setTextSize(16.0f);
        this.df.setTextColor(-16777216);
        this.df.setSingleLine(true);
        LinearLayout linearLayout = new LinearLayout(this.cO);
        linearLayout.setOrientation(1);
        linearLayout.addView(this.de);
        linearLayout.addView(this.df);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(40, 40);
        layoutParams.gravity = 16;
        layoutParams.setMargins(3, 0, 3, 0);
        this.dd.setLayoutParams(layoutParams);
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams2.gravity = 16;
        linearLayout.setLayoutParams(layoutParams2);
        LinearLayout linearLayout2 = new LinearLayout(this.cO);
        linearLayout2.setOrientation(0);
        linearLayout2.addView(this.dd);
        linearLayout2.addView(linearLayout);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams3.addRule(9);
        layoutParams3.addRule(15);
        linearLayout2.setLayoutParams(layoutParams3);
        Button button = new Button(this.cO);
        button.setText("更多");
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams4.addRule(11);
        layoutParams4.addRule(15);
        button.setLayoutParams(layoutParams4);
        RelativeLayout relativeLayout = new RelativeLayout(this.cO);
        relativeLayout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        relativeLayout.addView(linearLayout2);
        relativeLayout.addView(button);
        addView(relativeLayout);
        setOnClickListener(new f(this));
        button.setOnClickListener(new d(this));
        this.db = AdManager.z(this.cO).dl.d(this.a);
        a();
    }
}
