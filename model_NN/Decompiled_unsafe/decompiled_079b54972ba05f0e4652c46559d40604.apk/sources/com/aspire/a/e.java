package com.aspire.a;

import java.util.ArrayList;
import java.util.Iterator;

public class e {
    private static int b = 0;
    private ArrayList a = new ArrayList();

    public final class a {
        private String b;
        private Object cZ;

        public a(String str, Object obj) {
            this.b = str;
            this.cZ = obj;
        }

        public String a() {
            return this.b;
        }

        public Object ah() {
            return this.cZ;
        }
    }

    public static void a(e eVar, StringBuilder sb) {
        if (eVar != null && sb != null) {
            Iterator it = eVar.a().iterator();
            while (it.hasNext()) {
                a aVar = (a) it.next();
                String a2 = aVar.a();
                Object ah = aVar.ah();
                if (ah != null) {
                    a(sb);
                    sb.append("<" + a2 + ">");
                    if (ah instanceof e) {
                        b++;
                        sb.append("\n");
                        a((e) ah, sb);
                        b--;
                        a(sb);
                        sb.append("</" + a2 + ">\n");
                    } else {
                        sb.append(ah);
                        sb.append("</" + a2 + ">\n");
                    }
                } else {
                    a(sb);
                    sb.append("<" + a2 + "/>\n");
                }
            }
        }
    }

    private static void a(StringBuilder sb) {
        for (int i = 0; i < b; i++) {
            sb.append("\t");
        }
    }

    public ArrayList a() {
        return this.a;
    }

    public void a(String str, Object obj) {
        this.a.add(new a(str, obj));
    }
}
