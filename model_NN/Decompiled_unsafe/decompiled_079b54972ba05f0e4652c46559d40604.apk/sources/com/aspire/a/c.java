package com.aspire.a;

import javax.microedition.media.control.ToneControl;

public class c {
    private static final String a = System.getProperty("line.separator");
    private static char[] cX = new char[64];
    private static byte[] cY = new byte[128];

    static {
        char c = 'A';
        int i = 0;
        while (c <= 'Z') {
            cX[i] = c;
            c = (char) (c + 1);
            i++;
        }
        char c2 = 'a';
        while (c2 <= 'z') {
            cX[i] = c2;
            c2 = (char) (c2 + 1);
            i++;
        }
        char c3 = '0';
        while (c3 <= '9') {
            cX[i] = c3;
            c3 = (char) (c3 + 1);
            i++;
        }
        int i2 = i + 1;
        cX[i] = '+';
        int i3 = i2 + 1;
        cX[i2] = '/';
        for (int i4 = 0; i4 < cY.length; i4++) {
            cY[i4] = -1;
        }
        for (int i5 = 0; i5 < 64; i5++) {
            cY[cX[i5]] = (byte) i5;
        }
    }

    private c() {
    }

    public static String a(String str) {
        return new String(p(str.getBytes()));
    }

    public static char[] d(byte[] bArr, int i, int i2) {
        byte b;
        int i3;
        byte b2;
        int i4 = ((i2 * 4) + 2) / 3;
        char[] cArr = new char[(((i2 + 2) / 3) * 4)];
        int i5 = i + i2;
        int i6 = 0;
        while (i < i5) {
            int i7 = i + 1;
            byte b3 = bArr[i] & ToneControl.SILENCE;
            if (i7 < i5) {
                b = bArr[i7] & ToneControl.SILENCE;
                i7++;
            } else {
                b = 0;
            }
            if (i7 < i5) {
                i3 = i7 + 1;
                b2 = bArr[i7] & ToneControl.SILENCE;
            } else {
                i3 = i7;
                b2 = 0;
            }
            int i8 = b3 >>> 2;
            int i9 = ((b3 & 3) << 4) | (b >>> 4);
            int i10 = ((b & 15) << 2) | (b2 >>> 6);
            byte b4 = b2 & 63;
            int i11 = i6 + 1;
            cArr[i6] = cX[i8];
            int i12 = i11 + 1;
            cArr[i11] = cX[i9];
            cArr[i12] = i12 < i4 ? cX[i10] : '=';
            int i13 = i12 + 1;
            cArr[i13] = i13 < i4 ? cX[b4] : '=';
            i6 = i13 + 1;
            i = i3;
        }
        return cArr;
    }

    public static char[] p(byte[] bArr) {
        return d(bArr, 0, bArr.length);
    }
}
