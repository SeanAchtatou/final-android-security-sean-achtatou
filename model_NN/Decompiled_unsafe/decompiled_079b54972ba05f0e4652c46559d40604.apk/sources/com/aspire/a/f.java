package com.aspire.a;

import android.util.Log;

public class f {
    private static void a(int i, String str, String str2, Exception exc) {
        if (i >= 0) {
            if (str == null || str.length() == 0) {
                str = "LogAdapter";
            }
            switch (i) {
                case 0:
                    Log.d(str, str2);
                    return;
                case 1:
                default:
                    Log.i(str, str2);
                    return;
                case 2:
                    Log.w(str, str2);
                    return;
                case 3:
                    Log.e(str, str2, exc);
                    return;
            }
        }
    }

    public static void a(String str, String str2) {
        a(3, str, str2, null);
    }

    public static void a(String str, String str2, Exception exc) {
        a(3, str, str2, exc);
    }

    public static void b(String str, String str2) {
        a(0, str, str2, null);
    }
}
