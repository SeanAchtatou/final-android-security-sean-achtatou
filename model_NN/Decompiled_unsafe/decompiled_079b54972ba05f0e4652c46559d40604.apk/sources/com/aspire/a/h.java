package com.aspire.a;

import android.content.Context;
import android.os.Environment;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

public class h {
    public static InputStream C(String str) {
        try {
            return new FileInputStream(new File(str));
        } catch (Exception e) {
            return null;
        }
    }

    public static String a(Context context) {
        return context.getFilesDir().getPath() + "/";
    }

    public static boolean a() {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static boolean a(String str) {
        return new File(str).mkdir();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    public static boolean a(String str, InputStream inputStream) {
        BufferedOutputStream bufferedOutputStream;
        FileOutputStream fileOutputStream;
        FileOutputStream fileOutputStream2 = null;
        boolean z = false;
        try {
            fileOutputStream = new FileOutputStream(new File(str), false);
            try {
                bufferedOutputStream = new BufferedOutputStream(fileOutputStream);
            } catch (Exception e) {
                e = e;
                bufferedOutputStream = null;
                fileOutputStream2 = fileOutputStream;
                try {
                    e.printStackTrace();
                    try {
                        bufferedOutputStream.close();
                        fileOutputStream2.close();
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    return z;
                } catch (Throwable th) {
                    th = th;
                    fileOutputStream = fileOutputStream2;
                    try {
                        bufferedOutputStream.close();
                        fileOutputStream.close();
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                bufferedOutputStream = null;
                bufferedOutputStream.close();
                fileOutputStream.close();
                throw th;
            }
            try {
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = inputStream.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    bufferedOutputStream.write(bArr, 0, read);
                }
                bufferedOutputStream.flush();
                bufferedOutputStream.close();
                fileOutputStream.close();
                z = true;
                try {
                    bufferedOutputStream.close();
                    fileOutputStream.close();
                } catch (Exception e4) {
                    e4.printStackTrace();
                }
            } catch (Exception e5) {
                e = e5;
                fileOutputStream2 = fileOutputStream;
                e.printStackTrace();
                bufferedOutputStream.close();
                fileOutputStream2.close();
                return z;
            } catch (Throwable th3) {
                th = th3;
                bufferedOutputStream.close();
                fileOutputStream.close();
                throw th;
            }
        } catch (Exception e6) {
            e = e6;
            bufferedOutputStream = null;
            e.printStackTrace();
            bufferedOutputStream.close();
            fileOutputStream2.close();
            return z;
        } catch (Throwable th4) {
            th = th4;
            bufferedOutputStream = null;
            fileOutputStream = null;
            bufferedOutputStream.close();
            fileOutputStream.close();
            throw th;
        }
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0026 A[SYNTHETIC, Splitter:B:17:0x0026] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0032 A[SYNTHETIC, Splitter:B:23:0x0032] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(java.lang.String r4, java.lang.String r5) {
        /*
            r3 = 0
            r0 = 0
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x001f, all -> 0x002f }
            r1.<init>(r4)     // Catch:{ Exception -> 0x001f, all -> 0x002f }
            java.io.FileWriter r2 = new java.io.FileWriter     // Catch:{ Exception -> 0x001f, all -> 0x002f }
            r2.<init>(r1)     // Catch:{ Exception -> 0x001f, all -> 0x002f }
            r2.write(r5)     // Catch:{ Exception -> 0x003e }
            r2.close()     // Catch:{ Exception -> 0x003e }
            r1 = 0
            r0 = 1
            if (r3 == 0) goto L_0x0019
            r1.close()     // Catch:{ Exception -> 0x001a }
        L_0x0019:
            return r0
        L_0x001a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0019
        L_0x001f:
            r1 = move-exception
            r2 = r3
        L_0x0021:
            r1.printStackTrace()     // Catch:{ all -> 0x003b }
            if (r2 == 0) goto L_0x0019
            r2.close()     // Catch:{ Exception -> 0x002a }
            goto L_0x0019
        L_0x002a:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0019
        L_0x002f:
            r0 = move-exception
        L_0x0030:
            if (r3 == 0) goto L_0x0035
            r3.close()     // Catch:{ Exception -> 0x0036 }
        L_0x0035:
            throw r0
        L_0x0036:
            r1 = move-exception
            r1.printStackTrace()
            goto L_0x0035
        L_0x003b:
            r0 = move-exception
            r3 = r2
            goto L_0x0030
        L_0x003e:
            r1 = move-exception
            goto L_0x0021
        */
        throw new UnsupportedOperationException("Method not decompiled: com.aspire.a.h.a(java.lang.String, java.lang.String):boolean");
    }

    public static String b() {
        return Environment.getExternalStorageDirectory().getPath() + "/";
    }
}
