package com.aspire.a;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class b {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap a(Context context, String str, int i, int i2) {
        try {
            InputStream open = context.getResources().getAssets().open(str);
            Bitmap decodeStream = BitmapFactory.decodeStream(open);
            open.close();
            if (i <= 0 || i2 <= 0) {
                return decodeStream;
            }
            int width = decodeStream.getWidth();
            int height = decodeStream.getHeight();
            Matrix matrix = new Matrix();
            matrix.postScale(((float) i) / ((float) width), ((float) i2) / ((float) height));
            return Bitmap.createBitmap(decodeStream, 0, 0, width, height, matrix, true);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e2) {
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap a(String str, int i, int i2) {
        try {
            FileInputStream fileInputStream = new FileInputStream(str);
            Bitmap decodeStream = BitmapFactory.decodeStream(fileInputStream);
            fileInputStream.close();
            if (i <= 0 || i2 <= 0) {
                return decodeStream;
            }
            int width = decodeStream.getWidth();
            int height = decodeStream.getHeight();
            Matrix matrix = new Matrix();
            matrix.postScale(((float) i) / ((float) width), ((float) i2) / ((float) height));
            return Bitmap.createBitmap(decodeStream, 0, 0, width, height, matrix, true);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (Exception e2) {
            return null;
        }
    }
}
