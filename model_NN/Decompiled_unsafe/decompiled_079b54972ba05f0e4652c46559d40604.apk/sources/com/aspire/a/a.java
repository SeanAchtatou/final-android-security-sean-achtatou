package com.aspire.a;

import android.content.Context;
import android.os.Process;
import com.umeng.common.b.e;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpProtocolParams;

public class a {
    public static int a = 15000;
    public static int b = -1;
    private static a cN = null;
    /* access modifiers changed from: private */
    public Context cO;
    /* access modifiers changed from: private */
    public DefaultHttpClient cP;
    /* access modifiers changed from: private */
    public List cQ = Collections.synchronizedList(new ArrayList());
    private int d;
    /* access modifiers changed from: private */
    public String h = null;

    /* renamed from: com.aspire.a.a$a  reason: collision with other inner class name */
    private final class C0001a extends Thread {
        private String b;
        private HttpHost cR;
        private List cS;
        private HttpEntity cT;
        private b cU;
        private HttpRequestBase cV;
        private int h;
        private int i;

        public C0001a(String str, HttpHost httpHost, List list, HttpEntity httpEntity, b bVar) {
            this.b = str;
            this.cR = httpHost;
            this.cS = list;
            this.cT = httpEntity;
            this.cU = bVar;
            this.h = a.this.c();
            this.i = a.this.b();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:117:0x0348, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:118:0x0349, code lost:
            r1.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:151:0x0418, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:154:?, code lost:
            com.aspire.a.a.a(r0.cW, r0.cU, r0.b, "HttpEntity getContent IllegalStateException: " + r1.getMessage());
            r1 = r7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:155:0x0441, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:156:0x0442, code lost:
            com.aspire.a.a.a(r0.cW, r0.cU, r0.b, "HttpEntity getContent IOException: " + r1.getMessage());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:157:0x0468, code lost:
            r1 = r7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:171:?, code lost:
            monitor-enter(r16);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:175:0x04b6, code lost:
            if (r0.cV != null) goto L_0x04b8;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:176:0x04b8, code lost:
            r0.cV.abort();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:183:0x04c5, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:184:0x04c6, code lost:
            r15 = r2;
            r2 = r1;
            r15.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:185:0x04cc, code lost:
            r1 = r2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x0114, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x0115, code lost:
            monitor-enter(r16);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:38:0x011a, code lost:
            if (r0.cV != null) goto L_0x011c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x011c, code lost:
            r0.cV.abort();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:0x0124, code lost:
            com.aspire.a.f.a("HttpAdapter", "request IllegalArgumentException: " + r1.getMessage());
            com.aspire.a.a.a(r0.cW, r0.cU, r0.b, "request IllegalArgumentException: " + r1.getMessage());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:48:0x017f, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:49:0x0180, code lost:
            monitor-enter(r16);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:53:0x0185, code lost:
            if (r0.cV != null) goto L_0x0187;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:54:0x0187, code lost:
            r0.cV.abort();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:56:0x018f, code lost:
            com.aspire.a.f.a("HttpAdapter", "request ClientProtocolException: " + r1.getMessage());
            com.aspire.a.a.a(r0.cW, r0.cU, r0.b, "request ClientProtocolException: " + r1.getMessage());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:69:0x0206, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:70:0x0207, code lost:
            monitor-enter(r16);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:74:0x020c, code lost:
            if (r0.cV != null) goto L_0x020e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:75:0x020e, code lost:
            r0.cV.abort();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:77:0x0216, code lost:
            com.aspire.a.f.a("HttpAdapter", "request IOException: " + r1.getMessage());
            com.aspire.a.a.a(r0.cW, r0.cU, r0.b, "request IOException: " + r1.getMessage());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:85:0x026f, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:86:0x0270, code lost:
            monitor-enter(r16);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:90:0x0275, code lost:
            if (r0.cV != null) goto L_0x0277;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:91:0x0277, code lost:
            r0.cV.abort();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:93:0x027f, code lost:
            com.aspire.a.f.a("HttpAdapter", "request Exception: " + r1.getMessage());
            com.aspire.a.a.a(r0.cW, r0.cU, r0.b, "request Exception: " + r1.getMessage());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:95:0x02c4, code lost:
            r1 = null;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x0114 A[ExcHandler: IllegalArgumentException (r1v20 'e' java.lang.IllegalArgumentException A[CUSTOM_DECLARE]), Splitter:B:139:0x03cc] */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x017f A[ExcHandler: ClientProtocolException (r1v12 'e' org.apache.http.client.ClientProtocolException A[CUSTOM_DECLARE]), Splitter:B:4:0x001e] */
        /* JADX WARNING: Removed duplicated region for block: B:69:0x0206 A[ExcHandler: IOException (r1v7 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:4:0x001e] */
        /* JADX WARNING: Removed duplicated region for block: B:85:0x026f A[ExcHandler: Exception (r1v2 'e' java.lang.Exception A[CUSTOM_DECLARE]), Splitter:B:4:0x001e] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void a() {
            /*
                r16 = this;
                r13 = 102400(0x19000, double:5.05923E-319)
                r11 = 0
                r7 = 0
                r8 = 0
                java.lang.String r1 = "HttpAdapter"
                java.lang.String r2 = ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
                com.aspire.a.f.b(r1, r2)
                r0 = r16
                com.aspire.a.a$b r1 = r0.cU
                if (r1 != 0) goto L_0x001c
                java.lang.String r1 = "HttpAdapter"
                java.lang.String r2 = "OnHttpResponse is null"
                com.aspire.a.f.a(r1, r2)
            L_0x001b:
                return
            L_0x001c:
                r0 = r16
                org.apache.http.HttpEntity r1 = r0.cT     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                if (r1 != 0) goto L_0x01d2
                java.lang.String r1 = "HttpAdapter"
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r2.<init>()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r3 = "GET : "
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r0 = r16
                java.lang.String r3 = r0.b     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r2 = r2.toString()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                com.aspire.a.f.b(r1, r2)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                monitor-enter(r16)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                org.apache.http.client.methods.HttpGet r1 = new org.apache.http.client.methods.HttpGet     // Catch:{ all -> 0x017c }
                r0 = r16
                java.lang.String r2 = r0.b     // Catch:{ all -> 0x017c }
                r1.<init>(r2)     // Catch:{ all -> 0x017c }
                r0 = r16
                r0.cV = r1     // Catch:{ all -> 0x017c }
                r0 = r16
                org.apache.http.client.methods.HttpRequestBase r2 = r0.cV     // Catch:{ all -> 0x017c }
                monitor-exit(r16)     // Catch:{ all -> 0x017c }
            L_0x0051:
                r0 = r16
                com.aspire.a.a r1 = com.aspire.a.a.this     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                org.apache.http.impl.client.DefaultHttpClient r1 = r1.cP     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                org.apache.http.params.HttpParams r3 = r1.getParams()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r0 = r16
                org.apache.http.HttpHost r1 = r0.cR     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                if (r1 == 0) goto L_0x025a
                java.lang.String r1 = "HttpAdapter"
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r4.<init>()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r5 = "proxy host: "
                java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r0 = r16
                org.apache.http.HttpHost r5 = r0.cR     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r5 = r5.getHostName()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r5 = ", port: "
                java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r0 = r16
                org.apache.http.HttpHost r5 = r0.cR     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                int r5 = r5.getPort()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r4 = r4.toString()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                com.aspire.a.f.b(r1, r4)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r1 = "http.route.default-proxy"
                r0 = r16
                org.apache.http.HttpHost r4 = r0.cR     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r3.setParameter(r1, r4)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
            L_0x009e:
                r0 = r16
                int r1 = r0.i     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                if (r1 <= 0) goto L_0x02cd
                r0 = r16
                int r1 = r0.i     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r3, r1)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
            L_0x00ab:
                r0 = r16
                int r1 = r0.h     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                if (r1 <= 0) goto L_0x02d3
                r0 = r16
                int r1 = r0.h     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                org.apache.http.params.HttpConnectionParams.setSoTimeout(r3, r1)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
            L_0x00b8:
                r0 = r16
                java.util.List r1 = r0.cS     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                if (r1 == 0) goto L_0x02d9
                r0 = r16
                java.util.List r1 = r0.cS     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.util.Iterator r3 = r1.iterator()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
            L_0x00c6:
                boolean r1 = r3.hasNext()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                if (r1 == 0) goto L_0x02d9
                java.lang.Object r1 = r3.next()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                org.apache.http.NameValuePair r1 = (org.apache.http.NameValuePair) r1     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r4 = r1.getName()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                if (r4 == 0) goto L_0x00c6
                java.lang.String r4 = r1.getValue()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                if (r4 == 0) goto L_0x00c6
                java.lang.String r4 = "HttpAdapter"
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r5.<init>()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r6 = "httpheader name: "
                java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r6 = r1.getName()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r6 = ", value: "
                java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r6 = r1.getValue()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r5 = r5.toString()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                com.aspire.a.f.b(r4, r5)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r4 = r1.getName()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r1 = r1.getValue()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r2.addHeader(r4, r1)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                goto L_0x00c6
            L_0x0114:
                r1 = move-exception
                monitor-enter(r16)
                r0 = r16
                org.apache.http.client.methods.HttpRequestBase r2 = r0.cV     // Catch:{ all -> 0x04ce }
                if (r2 == 0) goto L_0x0123
                r0 = r16
                org.apache.http.client.methods.HttpRequestBase r2 = r0.cV     // Catch:{ all -> 0x04ce }
                r2.abort()     // Catch:{ all -> 0x04ce }
            L_0x0123:
                monitor-exit(r16)     // Catch:{ all -> 0x04ce }
                java.lang.String r2 = "HttpAdapter"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "request IllegalArgumentException: "
                java.lang.StringBuilder r3 = r3.append(r4)
                java.lang.String r4 = r1.getMessage()
                java.lang.StringBuilder r3 = r3.append(r4)
                java.lang.String r3 = r3.toString()
                com.aspire.a.f.a(r2, r3)
                r0 = r16
                com.aspire.a.a r2 = com.aspire.a.a.this
                r0 = r16
                com.aspire.a.a$b r3 = r0.cU
                r0 = r16
                java.lang.String r4 = r0.b
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r6 = "request IllegalArgumentException: "
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.String r1 = r1.getMessage()
                java.lang.StringBuilder r1 = r5.append(r1)
                java.lang.String r1 = r1.toString()
                r2.a(r3, r4, r1)
            L_0x0166:
                java.lang.String r1 = "HttpAdapter"
                java.lang.String r2 = "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
                com.aspire.a.f.b(r1, r2)
                r0 = r16
                com.aspire.a.a r1 = com.aspire.a.a.this
                java.util.List r1 = r1.cQ
                r0 = r16
                r1.remove(r0)
                goto L_0x001b
            L_0x017c:
                r1 = move-exception
                monitor-exit(r16)     // Catch:{ all -> 0x017c }
                throw r1     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
            L_0x017f:
                r1 = move-exception
                monitor-enter(r16)
                r0 = r16
                org.apache.http.client.methods.HttpRequestBase r2 = r0.cV     // Catch:{ all -> 0x04d1 }
                if (r2 == 0) goto L_0x018e
                r0 = r16
                org.apache.http.client.methods.HttpRequestBase r2 = r0.cV     // Catch:{ all -> 0x04d1 }
                r2.abort()     // Catch:{ all -> 0x04d1 }
            L_0x018e:
                monitor-exit(r16)     // Catch:{ all -> 0x04d1 }
                java.lang.String r2 = "HttpAdapter"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "request ClientProtocolException: "
                java.lang.StringBuilder r3 = r3.append(r4)
                java.lang.String r4 = r1.getMessage()
                java.lang.StringBuilder r3 = r3.append(r4)
                java.lang.String r3 = r3.toString()
                com.aspire.a.f.a(r2, r3)
                r0 = r16
                com.aspire.a.a r2 = com.aspire.a.a.this
                r0 = r16
                com.aspire.a.a$b r3 = r0.cU
                r0 = r16
                java.lang.String r4 = r0.b
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r6 = "request ClientProtocolException: "
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.String r1 = r1.getMessage()
                java.lang.StringBuilder r1 = r5.append(r1)
                java.lang.String r1 = r1.toString()
                r2.a(r3, r4, r1)
                goto L_0x0166
            L_0x01d2:
                java.lang.String r1 = "HttpAdapter"
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r2.<init>()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r3 = "POST : "
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r0 = r16
                java.lang.String r3 = r0.b     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r2 = r2.toString()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                com.aspire.a.f.b(r1, r2)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                monitor-enter(r16)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                org.apache.http.client.methods.HttpPost r1 = new org.apache.http.client.methods.HttpPost     // Catch:{ all -> 0x0203 }
                r0 = r16
                java.lang.String r2 = r0.b     // Catch:{ all -> 0x0203 }
                r1.<init>(r2)     // Catch:{ all -> 0x0203 }
                r0 = r16
                r0.cV = r1     // Catch:{ all -> 0x0203 }
                r0 = r16
                org.apache.http.client.methods.HttpRequestBase r2 = r0.cV     // Catch:{ all -> 0x0203 }
                monitor-exit(r16)     // Catch:{ all -> 0x0203 }
                goto L_0x0051
            L_0x0203:
                r1 = move-exception
                monitor-exit(r16)     // Catch:{ all -> 0x0203 }
                throw r1     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
            L_0x0206:
                r1 = move-exception
                monitor-enter(r16)
                r0 = r16
                org.apache.http.client.methods.HttpRequestBase r2 = r0.cV     // Catch:{ all -> 0x04d4 }
                if (r2 == 0) goto L_0x0215
                r0 = r16
                org.apache.http.client.methods.HttpRequestBase r2 = r0.cV     // Catch:{ all -> 0x04d4 }
                r2.abort()     // Catch:{ all -> 0x04d4 }
            L_0x0215:
                monitor-exit(r16)     // Catch:{ all -> 0x04d4 }
                java.lang.String r2 = "HttpAdapter"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "request IOException: "
                java.lang.StringBuilder r3 = r3.append(r4)
                java.lang.String r4 = r1.getMessage()
                java.lang.StringBuilder r3 = r3.append(r4)
                java.lang.String r3 = r3.toString()
                com.aspire.a.f.a(r2, r3)
                r0 = r16
                com.aspire.a.a r2 = com.aspire.a.a.this
                r0 = r16
                com.aspire.a.a$b r3 = r0.cU
                r0 = r16
                java.lang.String r4 = r0.b
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r6 = "request IOException: "
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.String r1 = r1.getMessage()
                java.lang.StringBuilder r1 = r5.append(r1)
                java.lang.String r1 = r1.toString()
                r2.a(r3, r4, r1)
                goto L_0x0166
            L_0x025a:
                r0 = r16
                com.aspire.a.a r1 = com.aspire.a.a.this     // Catch:{ Exception -> 0x02c3, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
                android.content.Context r1 = r1.cO     // Catch:{ Exception -> 0x02c3, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
                org.apache.http.HttpHost r1 = com.aspire.a.g.y(r1)     // Catch:{ Exception -> 0x02c3, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
            L_0x0266:
                if (r1 != 0) goto L_0x02c6
                java.lang.String r1 = "http.route.default-proxy"
                r3.removeParameter(r1)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                goto L_0x009e
            L_0x026f:
                r1 = move-exception
                monitor-enter(r16)
                r0 = r16
                org.apache.http.client.methods.HttpRequestBase r2 = r0.cV     // Catch:{ all -> 0x04d7 }
                if (r2 == 0) goto L_0x027e
                r0 = r16
                org.apache.http.client.methods.HttpRequestBase r2 = r0.cV     // Catch:{ all -> 0x04d7 }
                r2.abort()     // Catch:{ all -> 0x04d7 }
            L_0x027e:
                monitor-exit(r16)     // Catch:{ all -> 0x04d7 }
                java.lang.String r2 = "HttpAdapter"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = "request Exception: "
                java.lang.StringBuilder r3 = r3.append(r4)
                java.lang.String r4 = r1.getMessage()
                java.lang.StringBuilder r3 = r3.append(r4)
                java.lang.String r3 = r3.toString()
                com.aspire.a.f.a(r2, r3)
                r0 = r16
                com.aspire.a.a r2 = com.aspire.a.a.this
                r0 = r16
                com.aspire.a.a$b r3 = r0.cU
                r0 = r16
                java.lang.String r4 = r0.b
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>()
                java.lang.String r6 = "request Exception: "
                java.lang.StringBuilder r5 = r5.append(r6)
                java.lang.String r1 = r1.getMessage()
                java.lang.StringBuilder r1 = r5.append(r1)
                java.lang.String r1 = r1.toString()
                r2.a(r3, r4, r1)
                goto L_0x0166
            L_0x02c3:
                r1 = move-exception
                r1 = r7
                goto L_0x0266
            L_0x02c6:
                java.lang.String r4 = "http.route.default-proxy"
                r3.setParameter(r4, r1)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                goto L_0x009e
            L_0x02cd:
                r1 = 0
                org.apache.http.params.HttpConnectionParams.setConnectionTimeout(r3, r1)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                goto L_0x00ab
            L_0x02d3:
                r1 = 0
                org.apache.http.params.HttpConnectionParams.setSoTimeout(r3, r1)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                goto L_0x00b8
            L_0x02d9:
                r0 = r16
                org.apache.http.HttpEntity r1 = r0.cT     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                if (r1 == 0) goto L_0x030a
                java.lang.String r1 = "HttpAdapter"
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r3.<init>()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r4 = "HttpEntity: "
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r0 = r16
                org.apache.http.HttpEntity r4 = r0.cT     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r4 = r4.toString()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r3 = r3.toString()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                com.aspire.a.f.b(r1, r3)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r0 = r2
                org.apache.http.client.methods.HttpPost r0 = (org.apache.http.client.methods.HttpPost) r0     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r1 = r0
                r0 = r16
                org.apache.http.HttpEntity r3 = r0.cT     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r1.setEntity(r3)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
            L_0x030a:
                r5 = 3
                r1 = r8
                r3 = r7
            L_0x030d:
                if (r1 >= r5) goto L_0x04dc
                int r4 = r1 + 1
                r0 = r16
                com.aspire.a.a r1 = com.aspire.a.a.this     // Catch:{ Exception -> 0x0348, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
                org.apache.http.impl.client.DefaultHttpClient r1 = r1.cP     // Catch:{ Exception -> 0x0348, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
                org.apache.http.HttpResponse r3 = r1.execute(r2)     // Catch:{ Exception -> 0x0348, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
                if (r3 == 0) goto L_0x034c
                org.apache.http.StatusLine r1 = r3.getStatusLine()     // Catch:{ Exception -> 0x0348, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
                if (r1 == 0) goto L_0x034c
                r1 = r4
                r6 = r3
            L_0x0327:
                if (r1 != r5) goto L_0x0367
                java.net.SocketTimeoutException r2 = new java.net.SocketTimeoutException     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r3.<init>()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r4 = "网络或服务器无响应(重试"
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r3 = "次)"
                java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r1 = r1.toString()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r2.<init>(r1)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                throw r2     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
            L_0x0348:
                r1 = move-exception
                r1.printStackTrace()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
            L_0x034c:
                r0 = r16
                com.aspire.a.a r1 = com.aspire.a.a.this     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                org.apache.http.impl.client.DefaultHttpClient r1 = r1.cP     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                org.apache.http.conn.ClientConnectionManager r1 = r1.getConnectionManager()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r9 = 1
                java.util.concurrent.TimeUnit r6 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r1.closeIdleConnections(r9, r6)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                int r1 = r4 * 250
                long r9 = (long) r1     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                sleep(r9)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r1 = r4
                goto L_0x030d
            L_0x0367:
                org.apache.http.StatusLine r1 = r6.getStatusLine()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                if (r1 != 0) goto L_0x0375
                java.lang.Exception r1 = new java.lang.Exception     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r2 = "Http StatusLine is null"
                r1.<init>(r2)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                throw r1     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
            L_0x0375:
                int r2 = r1.getStatusCode()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r3 = 200(0xc8, float:2.8E-43)
                if (r2 == r3) goto L_0x03a6
                r3 = 206(0xce, float:2.89E-43)
                if (r2 == r3) goto L_0x03a6
                java.lang.Exception r2 = new java.lang.Exception     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r3.<init>()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r4 = "Http Status text: "
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                int r4 = r1.getStatusCode()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r1 = r1.getReasonPhrase()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r1 = r1.toString()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r2.<init>(r1)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                throw r2     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
            L_0x03a6:
                org.apache.http.HttpEntity r9 = r6.getEntity()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                if (r9 != 0) goto L_0x03b4
                java.lang.Exception r1 = new java.lang.Exception     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r2 = "Http Entity is null"
                r1.<init>(r2)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                throw r1     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
            L_0x03b4:
                java.io.InputStream r7 = r9.getContent()     // Catch:{ IllegalStateException -> 0x0418, IOException -> 0x0441, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, Exception -> 0x026f }
                if (r7 != 0) goto L_0x03e2
                r0 = r16
                com.aspire.a.a r1 = com.aspire.a.a.this     // Catch:{ IllegalStateException -> 0x0418, IOException -> 0x0441, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, Exception -> 0x026f }
                r0 = r16
                com.aspire.a.a$b r2 = r0.cU     // Catch:{ IllegalStateException -> 0x0418, IOException -> 0x0441, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, Exception -> 0x026f }
                r0 = r16
                java.lang.String r3 = r0.b     // Catch:{ IllegalStateException -> 0x0418, IOException -> 0x0441, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, Exception -> 0x026f }
                java.lang.String r4 = "HttpEntity getContent is null"
                r1.a(r2, r3, r4)     // Catch:{ IllegalStateException -> 0x0418, IOException -> 0x0441, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, Exception -> 0x026f }
            L_0x03cb:
                r1 = r7
            L_0x03cc:
                org.apache.http.Header[] r3 = r6.getAllHeaders()     // Catch:{ Exception -> 0x04c5, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
                int r4 = r3.length     // Catch:{ Exception -> 0x04c5, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
                r2 = r8
            L_0x03d2:
                if (r2 >= r4) goto L_0x046b
                r5 = r3[r2]     // Catch:{ Exception -> 0x04c5, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
                java.lang.String r6 = "HttpAdapter"
                java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x04c5, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
                com.aspire.a.f.b(r6, r5)     // Catch:{ Exception -> 0x04c5, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
                int r2 = r2 + 1
                goto L_0x03d2
            L_0x03e2:
                long r4 = r9.getContentLength()     // Catch:{ IllegalStateException -> 0x0418, IOException -> 0x0441, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, Exception -> 0x026f }
                int r1 = (r4 > r11 ? 1 : (r4 == r11 ? 0 : -1))
                if (r1 <= 0) goto L_0x0408
                int r1 = (r4 > r13 ? 1 : (r4 == r13 ? 0 : -1))
                if (r1 >= 0) goto L_0x0408
                r0 = r16
                com.aspire.a.a r1 = com.aspire.a.a.this     // Catch:{ IllegalStateException -> 0x0418, IOException -> 0x0441, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, Exception -> 0x026f }
                java.lang.String r1 = r1.h     // Catch:{ IllegalStateException -> 0x0418, IOException -> 0x0441, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, Exception -> 0x026f }
                com.aspire.a.h.a(r1, r7)     // Catch:{ IllegalStateException -> 0x0418, IOException -> 0x0441, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, Exception -> 0x026f }
                r7.close()     // Catch:{ IllegalStateException -> 0x0418, IOException -> 0x0441, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, Exception -> 0x026f }
                r0 = r16
                com.aspire.a.a r1 = com.aspire.a.a.this     // Catch:{ IllegalStateException -> 0x0418, IOException -> 0x0441, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, Exception -> 0x026f }
                java.lang.String r1 = r1.h     // Catch:{ IllegalStateException -> 0x0418, IOException -> 0x0441, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, Exception -> 0x026f }
                java.io.InputStream r7 = com.aspire.a.h.C(r1)     // Catch:{ IllegalStateException -> 0x0418, IOException -> 0x0441, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, Exception -> 0x026f }
            L_0x0408:
                r0 = r16
                com.aspire.a.a r1 = com.aspire.a.a.this     // Catch:{ IllegalStateException -> 0x0418, IOException -> 0x0441, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, Exception -> 0x026f }
                r0 = r16
                com.aspire.a.a$b r2 = r0.cU     // Catch:{ IllegalStateException -> 0x0418, IOException -> 0x0441, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, Exception -> 0x026f }
                r0 = r16
                java.lang.String r3 = r0.b     // Catch:{ IllegalStateException -> 0x0418, IOException -> 0x0441, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, Exception -> 0x026f }
                r1.a(r2, r3, r4, r6, r7)     // Catch:{ IllegalStateException -> 0x0418, IOException -> 0x0441, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, Exception -> 0x026f }
                goto L_0x03cb
            L_0x0418:
                r1 = move-exception
                r0 = r16
                com.aspire.a.a r2 = com.aspire.a.a.this     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r0 = r16
                com.aspire.a.a$b r3 = r0.cU     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r0 = r16
                java.lang.String r4 = r0.b     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r5.<init>()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r10 = "HttpEntity getContent IllegalStateException: "
                java.lang.StringBuilder r5 = r5.append(r10)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r1 = r1.getMessage()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.StringBuilder r1 = r5.append(r1)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r1 = r1.toString()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r2.a(r3, r4, r1)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r1 = r7
                goto L_0x03cc
            L_0x0441:
                r1 = move-exception
                r0 = r16
                com.aspire.a.a r2 = com.aspire.a.a.this     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r0 = r16
                com.aspire.a.a$b r3 = r0.cU     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r0 = r16
                java.lang.String r4 = r0.b     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r5.<init>()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r10 = "HttpEntity getContent IOException: "
                java.lang.StringBuilder r5 = r5.append(r10)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r1 = r1.getMessage()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.StringBuilder r1 = r5.append(r1)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                java.lang.String r1 = r1.toString()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r2.a(r3, r4, r1)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r1 = r7
                goto L_0x03cc
            L_0x046b:
                long r2 = r9.getContentLength()     // Catch:{ Exception -> 0x04c5, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
                int r4 = (r2 > r11 ? 1 : (r2 == r11 ? 0 : -1))
                if (r4 <= 0) goto L_0x04a6
                int r2 = (r2 > r13 ? 1 : (r2 == r13 ? 0 : -1))
                if (r2 >= 0) goto L_0x04a6
                r1.close()     // Catch:{ Exception -> 0x04c5, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
                r0 = r16
                com.aspire.a.a r2 = com.aspire.a.a.this     // Catch:{ Exception -> 0x04c5, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
                java.lang.String r2 = r2.h     // Catch:{ Exception -> 0x04c5, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
                java.io.InputStream r1 = com.aspire.a.h.C(r2)     // Catch:{ Exception -> 0x04c5, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
                r0 = r16
                com.aspire.a.a r2 = com.aspire.a.a.this     // Catch:{ Exception -> 0x04c5, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
                java.lang.String r2 = r2.a(r1)     // Catch:{ Exception -> 0x04c5, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
                java.lang.String r3 = "HttpAdapter"
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x04c5, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
                r4.<init>()     // Catch:{ Exception -> 0x04c5, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
                java.lang.String r5 = "Entity "
                java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x04c5, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
                java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ Exception -> 0x04c5, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
                java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x04c5, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
                com.aspire.a.f.b(r3, r2)     // Catch:{ Exception -> 0x04c5, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206 }
            L_0x04a6:
                if (r1 == 0) goto L_0x04ab
                r1.close()     // Catch:{ IOException -> 0x04da, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, Exception -> 0x026f }
            L_0x04ab:
                r9.consumeContent()     // Catch:{ IOException -> 0x04b0, IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, Exception -> 0x026f }
                goto L_0x0166
            L_0x04b0:
                r1 = move-exception
                monitor-enter(r16)     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r0 = r16
                org.apache.http.client.methods.HttpRequestBase r1 = r0.cV     // Catch:{ all -> 0x04c2 }
                if (r1 == 0) goto L_0x04bf
                r0 = r16
                org.apache.http.client.methods.HttpRequestBase r1 = r0.cV     // Catch:{ all -> 0x04c2 }
                r1.abort()     // Catch:{ all -> 0x04c2 }
            L_0x04bf:
                monitor-exit(r16)     // Catch:{ all -> 0x04c2 }
                goto L_0x0166
            L_0x04c2:
                r1 = move-exception
                monitor-exit(r16)     // Catch:{ all -> 0x04c2 }
                throw r1     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
            L_0x04c5:
                r2 = move-exception
                r15 = r2
                r2 = r1
                r1 = r15
                r1.printStackTrace()     // Catch:{ IllegalArgumentException -> 0x0114, ClientProtocolException -> 0x017f, IOException -> 0x0206, Exception -> 0x026f }
                r1 = r2
                goto L_0x04a6
            L_0x04ce:
                r1 = move-exception
                monitor-exit(r16)     // Catch:{ all -> 0x04ce }
                throw r1
            L_0x04d1:
                r1 = move-exception
                monitor-exit(r16)     // Catch:{ all -> 0x04d1 }
                throw r1
            L_0x04d4:
                r1 = move-exception
                monitor-exit(r16)     // Catch:{ all -> 0x04d4 }
                throw r1
            L_0x04d7:
                r1 = move-exception
                monitor-exit(r16)     // Catch:{ all -> 0x04d7 }
                throw r1
            L_0x04da:
                r1 = move-exception
                goto L_0x04ab
            L_0x04dc:
                r6 = r3
                goto L_0x0327
            */
            throw new UnsupportedOperationException("Method not decompiled: com.aspire.a.a.C0001a.a():void");
        }

        public void run() {
            a();
        }
    }

    public interface b {
        void a(String str, boolean z, String str2, long j, HttpResponse httpResponse, InputStream inputStream);
    }

    private a(Context context) {
        this.cO = context;
        this.d = Process.myPid();
        if (h.a()) {
            this.h = h.b();
        } else {
            this.h = h.a(this.cO);
        }
        this.h += "HttpRespLog.xml";
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpProtocolParams.setVersion(basicHttpParams, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(basicHttpParams, e.f);
        ConnManagerParams.setMaxTotalConnections(basicHttpParams, 60);
        ConnManagerParams.setMaxConnectionsPerRoute(basicHttpParams, new ConnPerRouteBean(30));
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        this.cP = new DefaultHttpClient(new ThreadSafeClientConnManager(basicHttpParams, schemeRegistry), basicHttpParams);
    }

    /* access modifiers changed from: private */
    public String a(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine + "\n");
                } else {
                    try {
                        break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e2) {
                e2.printStackTrace();
                try {
                    inputStream.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            } catch (Throwable th) {
                try {
                    inputStream.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                throw th;
            }
        }
        inputStream.close();
        return sb.toString();
    }

    private void a() {
        synchronized (a.class) {
            a = 15000;
            b = -1;
        }
    }

    /* access modifiers changed from: private */
    public void a(b bVar, String str, long j, HttpResponse httpResponse, InputStream inputStream) {
        try {
            bVar.a(str, true, null, j, httpResponse, inputStream);
        } catch (Exception e) {
            e.printStackTrace();
            f.a("HttpAdapter", "onResponse exception", e);
        }
    }

    /* access modifiers changed from: private */
    public void a(b bVar, String str, String str2) {
        try {
            bVar.a(str, false, str2, 0, null, null);
        } catch (Exception e) {
            e.printStackTrace();
            f.a("HttpAdapter", "onResponse exception", e);
        }
    }

    /* access modifiers changed from: private */
    public int b() {
        int i;
        synchronized (a.class) {
            i = a;
        }
        return i;
    }

    /* access modifiers changed from: private */
    public int c() {
        int i;
        synchronized (a.class) {
            i = b;
        }
        return i;
    }

    public static a x(Context context) {
        int myPid = Process.myPid();
        if (cN == null || myPid != cN.d) {
            cN = new a(context);
        }
        return cN;
    }

    public void a(String str, HttpHost httpHost, List list, HttpEntity httpEntity, b bVar) {
        C0001a aVar = new C0001a(str, httpHost, list, httpEntity, bVar);
        this.cQ.add(aVar);
        try {
            aVar.start();
        } catch (Exception e) {
            e.printStackTrace();
            f.a("HttpAdapter", "requestAsync exception", e);
        }
        a();
    }
}
