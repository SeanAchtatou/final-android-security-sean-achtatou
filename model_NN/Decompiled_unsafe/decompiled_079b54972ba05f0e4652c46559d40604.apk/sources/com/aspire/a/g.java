package com.aspire.a;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import android.util.Log;
import org.apache.http.HttpHost;

public class g {
    private static boolean a = false;
    private static boolean b = false;
    private static boolean c = false;
    private static boolean d = false;
    private static boolean e = false;
    private static boolean f = false;

    private static boolean a(String str) {
        String str2 = "";
        String str3 = "";
        if (str != null && str.length() > 4) {
            str2 = str.substring(0, 3);
            str3 = str.substring(3, 5);
        }
        return str2.equals("460") && (str3.equals("00") || str3.equals("02") || str3.equals("07"));
    }

    public static boolean b(Context context) {
        if (e) {
            return f;
        }
        e = true;
        if (a(d(true))) {
            f = true;
            return true;
        } else if (a(d(false))) {
            f = true;
            return true;
        } else if (!a(((TelephonyManager) context.getSystemService("phone")).getSimOperator())) {
            return false;
        } else {
            f = true;
            return true;
        }
    }

    public static int c(Context context) {
        NetworkInfo networkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        int networkPreference = connectivityManager.getNetworkPreference();
        NetworkInfo[] allNetworkInfo = connectivityManager.getAllNetworkInfo();
        int length = allNetworkInfo.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                networkInfo = null;
                break;
            }
            networkInfo = allNetworkInfo[i];
            if (networkInfo.getType() == networkPreference && networkInfo.isConnected()) {
                break;
            }
            i++;
        }
        NetworkInfo activeNetworkInfo = networkInfo == null ? connectivityManager.getActiveNetworkInfo() : networkInfo;
        if (activeNetworkInfo == null) {
            return 6;
        }
        if (!b(context)) {
            return 5;
        }
        if (activeNetworkInfo.getType() == 1) {
            return 4;
        }
        String extraInfo = activeNetworkInfo.getExtraInfo();
        if (extraInfo == null) {
            return 6;
        }
        if (extraInfo.equals("cmmm") || extraInfo.startsWith("cmmm:")) {
            return 1;
        }
        if (extraInfo.equals("cmwap") || extraInfo.startsWith("cmwap:")) {
            return 3;
        }
        if (extraInfo.equals("cmnet") || extraInfo.startsWith("cmnet:")) {
            return 2;
        }
        return (extraInfo.contains("internet") || extraInfo.equalsIgnoreCase("internet")) ? 1 : 6;
    }

    private static String d(boolean z) {
        Object a2;
        Class[] clsArr = {String.class};
        Object[] objArr = new Object[1];
        objArr[0] = z ? "iphonesubinfo2" : "iphonesubinfo";
        Object a3 = d.a("android.os.ServiceManager", "getService", clsArr, objArr);
        if (a3 == null && z) {
            return d(false);
        }
        if (a3 == null || (a2 = d.a("com.android.internal.telephony.IPhoneSubInfo$Stub", "asInterface", new Class[]{IBinder.class}, new Object[]{a3})) == null) {
            return "";
        }
        String str = (String) d.a(a2, "getSubscriberId", (Class[]) null, (Object[]) null);
        Log.i("NetAdaptor", "getSubscriberId(sim" + (z ? "2" : "1") + ")=" + str);
        return str;
    }

    public static HttpHost y(Context context) {
        int i;
        String str;
        if (c(context) != 4) {
            str = Proxy.getHost(context);
            i = Proxy.getPort(context);
        } else {
            i = -1;
            str = null;
        }
        if (str == null || i == -1) {
            return null;
        }
        return new HttpHost(str, i);
    }
}
