package com.aspire.a;

import android.util.Log;
import java.lang.reflect.Method;

public class d {
    public static Object a(Class cls, String str, Class[] clsArr, Object[] objArr) {
        try {
            Method declaredMethod = cls.getDeclaredMethod(str, clsArr);
            declaredMethod.setAccessible(true);
            return declaredMethod.invoke(null, objArr);
        } catch (NoSuchMethodException e) {
            Log.e("ReflectHelper", e.getMessage());
            return null;
        } catch (Exception e2) {
            Log.e("ReflectHelper", e2.getMessage());
            return null;
        }
    }

    public static Object a(Object obj, String str, Class[] clsArr, Object[] objArr) {
        try {
            return obj.getClass().getMethod(str, clsArr).invoke(obj, objArr);
        } catch (NoSuchMethodException e) {
            Log.e("ReflectHelper", e.getMessage());
            return null;
        } catch (Exception e2) {
            Log.e("ReflectHelper", e2.getMessage());
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.aspire.a.d.a(java.lang.Class, java.lang.String, java.lang.Class[], java.lang.Object[]):java.lang.Object
     arg types: [java.lang.Class<?>, java.lang.String, java.lang.Class[], java.lang.Object[]]
     candidates:
      com.aspire.a.d.a(java.lang.Object, java.lang.String, java.lang.Class[], java.lang.Object[]):java.lang.Object
      com.aspire.a.d.a(java.lang.String, java.lang.String, java.lang.Class[], java.lang.Object[]):java.lang.Object
      com.aspire.a.d.a(java.lang.Class, java.lang.String, java.lang.Class[], java.lang.Object[]):java.lang.Object */
    public static Object a(String str, String str2, Class[] clsArr, Object[] objArr) {
        try {
            return a((Class) Class.forName(str), str2, clsArr, objArr);
        } catch (ClassNotFoundException e) {
            Log.e("ReflectHelper", e.getMessage());
            return null;
        }
    }
}
