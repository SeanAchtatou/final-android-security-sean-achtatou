package com.chinaMobile;

import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

final class a implements DialogInterface.OnClickListener {
    private final /* synthetic */ Context b;
    private /* synthetic */ p dM;

    a(p pVar, Context context) {
        this.dM = pVar;
        this.b = context;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        if (this.dM.dS.getText() == null || this.dM.dS.getText().toString().equals("")) {
            Toast.makeText(this.b, "请输入反馈信息", 1).show();
            return;
        }
        String editable = this.dM.dS.getText().toString();
        String obj = this.dM.dT.getSelectedItem().toString();
        String obj2 = this.dM.dU.getSelectedItem().toString();
        if (obj == null || obj.equals("年龄")) {
            obj = "";
        }
        if (obj2 == null || obj2.equals("性别")) {
            obj2 = "";
        }
        MobileAgent.a(this.b, obj, obj2, editable);
    }
}
