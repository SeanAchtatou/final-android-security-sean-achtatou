package com.chinaMobile;

import android.content.Context;
import android.util.Log;

public final class k extends Thread {
    private Context a;
    private String b = "";
    private int c;
    private String d = "";
    private String e = "";

    k(Context context, int i) {
        this.a = context;
        this.c = i;
    }

    k(Context context, int i, String str, String str2) {
        this.a = context;
        this.c = i;
        this.d = str;
        this.e = str2;
    }

    k(Context context, String str) {
        this.a = context;
        this.c = 2;
        this.d = str;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void run() {
        try {
            switch (this.c) {
                case 1:
                    MobileAgent.h(this.a, this.b);
                    return;
                case 2:
                    MobileAgent.m(this.a, this.b, this.d);
                    return;
                case 3:
                    MobileAgent.i(this.a, this.b);
                    return;
                case 4:
                    MobileAgent.j(this.a, null);
                    return;
                case 5:
                case 7:
                default:
                    return;
                case 6:
                    MobileAgent.O(this.a);
                    return;
                case 8:
                    MobileAgent.K(this.a);
                    return;
                case 9:
                    MobileAgent.e(this.a, this.d, this.e);
                    return;
                case 10:
                    MobileAgent.e(this.a, null, null);
                    return;
                case 11:
                    MobileAgent.g(this.a, this.d, this.e);
                    return;
            }
        } catch (Exception e2) {
            Log.e("MobileAgent", "Exception occurred when recording usage.");
            e2.printStackTrace();
        }
        Log.e("MobileAgent", "Exception occurred when recording usage.");
        e2.printStackTrace();
    }
}
