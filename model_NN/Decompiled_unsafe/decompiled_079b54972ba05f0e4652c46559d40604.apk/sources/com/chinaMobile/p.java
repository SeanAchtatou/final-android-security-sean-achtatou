package com.chinaMobile;

import android.app.AlertDialog;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import java.util.ArrayList;

public final class p extends AlertDialog {
    EditText dS;
    Spinner dT;
    Spinner dU;

    protected p(Context context) {
        super(context);
        setTitle("反馈意见");
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setPadding(5, 5, 5, 5);
        linearLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
        linearLayout.setMinimumWidth(((int) displayMetrics.density) * 200);
        linearLayout.setOrientation(1);
        this.dS = new EditText(context);
        this.dS.setMaxLines(3);
        this.dS.setMinLines(2);
        this.dS.setHint("请输入您的反馈意见");
        this.dS.setGravity(48);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
        layoutParams.weight = 1.0f;
        linearLayout.addView(this.dS, layoutParams);
        LinearLayout linearLayout2 = new LinearLayout(context);
        this.dU = new Spinner(context);
        ArrayList arrayList = new ArrayList();
        arrayList.add("性别");
        arrayList.add("男");
        arrayList.add("女");
        this.dU.setAdapter((SpinnerAdapter) new e(context, arrayList));
        this.dU.setOnItemSelectedListener(new o());
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams2.weight = 1.0f;
        layoutParams3.weight = 1.0f;
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add("年龄");
        arrayList2.add("18岁以下");
        arrayList2.add("18-24岁");
        arrayList2.add("25-30岁");
        arrayList2.add("31-35岁");
        arrayList2.add("36-40岁");
        arrayList2.add("41-50岁");
        arrayList2.add("50-59岁");
        arrayList2.add("60岁及以上");
        this.dT = new Spinner(context);
        this.dT.setAdapter((SpinnerAdapter) new e(context, arrayList2));
        this.dT.setOnItemSelectedListener(new n());
        linearLayout2.addView(this.dU, layoutParams2);
        linearLayout2.addView(this.dT, layoutParams2);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(-1, -2);
        layoutParams4.weight = 1.0f;
        linearLayout.addView(linearLayout2, layoutParams4);
        setView(linearLayout);
        setButton("提交", new a(this, context));
        setButton2("取消", new b(this));
    }

    public final void setView(View view) {
        super.setView(view);
    }
}
