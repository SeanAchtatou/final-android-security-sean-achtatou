package com.chinaMobile;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import java.util.ArrayList;

final class e implements SpinnerAdapter {
    private ArrayList a;
    private Context b;

    public e(Context context, ArrayList arrayList) {
        this.a = arrayList;
        this.b = context;
    }

    public final int getCount() {
        return this.a.size();
    }

    public final View getDropDownView(int i, View view, ViewGroup viewGroup) {
        CheckedTextView checkedTextView = (CheckedTextView) LayoutInflater.from(this.b).inflate(17367049, (ViewGroup) null);
        checkedTextView.setText((CharSequence) this.a.get(i));
        return checkedTextView;
    }

    public final Object getItem(int i) {
        return this.a.get(i);
    }

    public final long getItemId(int i) {
        return (long) i;
    }

    public final int getItemViewType(int i) {
        return 0;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        TextView textView = (TextView) LayoutInflater.from(this.b).inflate(17367048, (ViewGroup) null);
        textView.setText((CharSequence) this.a.get(i));
        return textView;
    }

    public final int getViewTypeCount() {
        return 0;
    }

    public final boolean hasStableIds() {
        return false;
    }

    public final boolean isEmpty() {
        return false;
    }

    public final void registerDataSetObserver(DataSetObserver dataSetObserver) {
    }

    public final void unregisterDataSetObserver(DataSetObserver dataSetObserver) {
    }
}
