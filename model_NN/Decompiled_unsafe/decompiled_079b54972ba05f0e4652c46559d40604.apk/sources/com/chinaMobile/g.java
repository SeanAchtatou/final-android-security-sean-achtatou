package com.chinaMobile;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import com.a.a.c.h;
import com.umeng.common.b.e;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.List;
import javax.microedition.media.control.ToneControl;

public final class g {
    public static String R(Context context) {
        String a = a(context);
        if (a != null && a.length() > 6) {
            return String.valueOf(a(a).toCharArray(), 7, 18);
        }
        String f = f(context);
        if (f != null && f.length() >= 9) {
            return String.valueOf(a(f).toCharArray(), 7, 18);
        }
        String S = S(context);
        return (S == null || S.length() == 0) ? String.valueOf(a(b(Build.MODEL)).toCharArray(), 7, 18) : String.valueOf(a(S).toCharArray(), 7, 18);
    }

    public static String S(Context context) {
        String str;
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager == null) {
            return "";
        }
        try {
            String subscriberId = telephonyManager.getSubscriberId();
            str = subscriberId != null ? subscriberId.trim() : "";
            try {
                Log.i("MobileUtils", str);
                return str;
            } catch (Exception e) {
            }
        } catch (Exception e2) {
            str = "";
            Log.i("MobileUtils", "can't not read imsi");
            return str;
        }
    }

    public static String T(Context context) {
        if (!a(context, "android.permission.ACCESS_NETWORK_STATE")) {
            return "unknown";
        }
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null) {
                return "unknown";
            }
            if (activeNetworkInfo.getType() == 1) {
                return "wifi";
            }
            String extraInfo = activeNetworkInfo.getExtraInfo();
            if (extraInfo == null) {
                return "unknown";
            }
            Log.i("MobileUtils", "net type:" + extraInfo);
            return extraInfo.trim();
        } catch (Exception e) {
            Log.w("MobileUtils", "Could not read ACCESSPOINT, forget to include ACCESS_NETSTATE_STATE permission?", e);
            return "unknown";
        }
    }

    public static int U(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static String V(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static long[] W(Context context) {
        long j;
        long j2 = -1;
        int X = X(context);
        String str = "proc/uid_stat/" + X + "/tcp_snd";
        try {
            j = Long.parseLong(new BufferedReader(new FileReader("proc/uid_stat/" + X + "/tcp_rcv"), h.HTTP_INTERNAL_ERROR).readLine().toString().trim());
        } catch (Exception e) {
            j = -1;
        }
        try {
            j2 = Long.parseLong(new BufferedReader(new FileReader(str), h.HTTP_INTERNAL_ERROR).readLine().toString().trim());
        } catch (Exception e2) {
        }
        Log.i("MobileUtils", "indicate flow: " + j + "  &  " + j2);
        return new long[]{j, j2};
    }

    private static int X(Context context) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 1).uid;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static String a() {
        return b("android " + Build.VERSION.RELEASE);
    }

    public static String a(Context context) {
        return b(((TelephonyManager) context.getSystemService("phone")).getDeviceId());
    }

    public static String a(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            char[] charArray = str.toCharArray();
            byte[] bArr = new byte[charArray.length];
            for (int i = 0; i < charArray.length; i++) {
                bArr[i] = (byte) charArray[i];
            }
            byte[] digest = instance.digest(bArr);
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b : digest) {
                byte b2 = b & ToneControl.SILENCE;
                if (b2 < 16) {
                    stringBuffer.append("0");
                }
                stringBuffer.append(Integer.toHexString(b2));
            }
            return stringBuffer.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private static boolean a(Context context, String str) {
        return context.getPackageManager().checkPermission(str, context.getPackageName()) == 0;
    }

    public static String b() {
        String str = "";
        try {
            InputStream inputStream = new ProcessBuilder("/system/bin/cat", "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq").start().getInputStream();
            byte[] bArr = new byte[24];
            while (inputStream.read(bArr) != -1) {
                str = String.valueOf(str) + new String(bArr);
            }
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
            str = "N/A";
        }
        return str.trim();
    }

    public static String b(String str) {
        if (str == null) {
            return "";
        }
        if (str.length() > 30) {
            str = str.substring(0, 29);
        }
        return str.replace("\\", "").replace("|", "");
    }

    public static String c() {
        try {
            return new BufferedReader(new FileReader("/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq")).readLine().trim();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return "";
    }

    public static int d(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    public static String d() {
        try {
            String readLine = new BufferedReader(new FileReader("/proc/meminfo"), 8192).readLine();
            if (readLine != null) {
                String trim = readLine.substring(readLine.indexOf(":") + 2, readLine.indexOf("k") - 1).trim();
                Log.i("MobileUtils", trim);
                return trim;
            }
        } catch (IOException e) {
        }
        return "";
    }

    public static int e(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    public static String f(Context context) {
        try {
            if (a(context, "android.permission.ACCESS_WIFI_STATE")) {
                String macAddress = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
                return (macAddress == null || macAddress.equals("")) ? "unknown" : macAddress;
            }
            Log.w("MobileUtils", "Could not read MAC, forget to include ACCESS_WIFI_STATE permission?");
            return "unknown";
        } catch (Exception e) {
            Log.w("MobileUtils", "Could not read MAC, forget to include ACCESS_WIFI_STATE permission?", e);
            return "unknown";
        }
    }

    public static String g(Context context) {
        String string = context.getSharedPreferences("MoblieAgent_sys_config", 0).getString("MOBILE_APPKEY", "");
        if (!string.equals("")) {
            return string;
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            return applicationInfo != null ? applicationInfo.metaData.getString("MOBILE_APPKEY") : string;
        } catch (Exception e) {
            e.printStackTrace();
            return string;
        }
    }

    public static String h(Context context) {
        String string = context.getSharedPreferences("MoblieAgent_sys_config", 0).getString("MOBILE_CHANNEL", "");
        if (!string.equals("")) {
            return b(string);
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo != null && (string = applicationInfo.metaData.getString("MOBILE_CHANNEL")) == null) {
                Log.w("MobileUtils", "Could not read MOBILE_CHANNEL meta-data from AndroidManifest.xml.");
                string = "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return b(string);
    }

    public static String i(Context context) {
        try {
            return ((TelephonyManager) context.getSystemService("phone")).getNetworkOperatorName();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String n(Context context) {
        int i = 0;
        StringBuffer stringBuffer = new StringBuffer();
        List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(0);
        while (true) {
            int i2 = i;
            if (i2 >= installedPackages.size()) {
                return stringBuffer.toString();
            }
            PackageInfo packageInfo = installedPackages.get(i2);
            if ((packageInfo.applicationInfo.flags & 1) == 0) {
                try {
                    stringBuffer.append(URLEncoder.encode(packageInfo.applicationInfo.loadLabel(context.getPackageManager()).toString(), e.f));
                    stringBuffer.append("@@");
                    stringBuffer.append(packageInfo.packageName);
                    stringBuffer.append("@@");
                    stringBuffer.append(packageInfo.versionName);
                    stringBuffer.append("@@");
                    stringBuffer.append("##");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
            i = i2 + 1;
        }
    }
}
