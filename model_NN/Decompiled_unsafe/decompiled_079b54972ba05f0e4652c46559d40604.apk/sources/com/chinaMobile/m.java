package com.chinaMobile;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import java.util.Locale;

public final class m {
    private static String a;

    static String a() {
        if (a == null) {
            try {
                Locale locale = Locale.getDefault();
                String format = String.format("%s-%s", locale.getLanguage(), locale.getCountry());
                StringBuilder sb = new StringBuilder(256);
                sb.append("Mozilla/5.0 (Linux; U; Android ");
                sb.append(Build.VERSION.RELEASE);
                sb.append("; ");
                sb.append(format);
                sb.append("; ");
                sb.append(Build.MODEL);
                sb.append(" Build/");
                sb.append(Build.ID);
                sb.append(") AppleWebkit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1");
                a = sb.toString();
            } catch (Exception e) {
                return "";
            }
        }
        return a;
    }

    static String a(Context context) {
        NetworkInfo activeNetworkInfo;
        try {
            if (j.a(context, "android.permission.ACCESS_NETWORK_STATE") && (activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo()) != null && activeNetworkInfo.isAvailable()) {
                if (activeNetworkInfo.getType() != 0) {
                    return "wifi";
                }
                String extraInfo = activeNetworkInfo.getExtraInfo();
                if (extraInfo == null) {
                    return "";
                }
                String lowerCase = extraInfo.trim().toLowerCase();
                return lowerCase.length() > 10 ? lowerCase.substring(0, 10) : lowerCase;
            }
        } catch (Exception e) {
        }
        return "";
    }
}
