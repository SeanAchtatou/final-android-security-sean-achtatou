package com.chinaMobile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

final class d {
    private static d dO = new d();
    private SecretKeySpec dP;

    private d() {
    }

    public static d ak() {
        return dO;
    }

    private static byte[] b(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            gZIPOutputStream.write(str.getBytes("utf-8"));
            gZIPOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return byteArrayOutputStream.toByteArray();
    }

    public final byte[] a(String str) {
        if (this.dP == null) {
            byte[] bytes = "134e3265829ff82daf16e7b740a600b5".getBytes();
            byte[] bArr = new byte[16];
            int i = 0;
            while (i < bytes.length && i < bArr.length) {
                bArr[i] = bytes[i];
                i++;
            }
            this.dP = new SecretKeySpec(bArr, "AES");
        }
        Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
        instance.init(1, this.dP, new IvParameterSpec("0102030405060708".getBytes()));
        return b(new h().a(instance.doFinal(str.getBytes())));
    }
}
