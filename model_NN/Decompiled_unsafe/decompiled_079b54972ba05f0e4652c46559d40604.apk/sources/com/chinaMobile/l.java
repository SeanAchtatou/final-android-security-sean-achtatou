package com.chinaMobile;

import com.a.a.m.m;
import java.net.URI;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.RedirectHandler;
import org.apache.http.protocol.HttpContext;

final class l implements RedirectHandler {
    l() {
    }

    public final URI getLocationURI(HttpResponse httpResponse, HttpContext httpContext) {
        String value;
        Header firstHeader = httpResponse.containsHeader(m.PROP_LOCATION) ? httpResponse.getFirstHeader(m.PROP_LOCATION) : httpResponse.containsHeader("Location") ? httpResponse.getFirstHeader("Location") : httpResponse.containsHeader("LOCATION") ? httpResponse.getFirstHeader("LOCATION") : null;
        if (firstHeader == null || (value = firstHeader.getValue()) == null) {
            return null;
        }
        return URI.create(value);
    }

    public final boolean isRedirectRequested(HttpResponse httpResponse, HttpContext httpContext) {
        int statusCode = httpResponse.getStatusLine().getStatusCode();
        return statusCode == 301 || statusCode == 302 || statusCode == 303 || statusCode == 307;
    }
}
