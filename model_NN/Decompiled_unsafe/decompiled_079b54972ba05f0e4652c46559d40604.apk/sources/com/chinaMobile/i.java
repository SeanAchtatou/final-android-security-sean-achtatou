package com.chinaMobile;

import android.content.Context;
import com.umeng.common.b.e;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.json.JSONException;
import org.json.JSONObject;

final class i implements Runnable {
    private final /* synthetic */ Context a;
    private final /* synthetic */ String b;
    private final /* synthetic */ String c;
    private final /* synthetic */ String d;

    i(Context context, String str, String str2, String str3) {
        this.a = context;
        this.b = str;
        this.c = str2;
        this.d = str3;
    }

    public final void run() {
        String R = g.R(this.a);
        JSONObject jSONObject = new JSONObject();
        String replace = this.b.replace("\n", "");
        try {
            jSONObject.put("pid", 1);
            jSONObject.put("protocolVersion", "3.1.0");
            jSONObject.put("sdkVersion", MobileAgent.SDK_VERSION);
            jSONObject.put("cid", R);
            jSONObject.put("deviceId", g.a(this.a));
            jSONObject.put("appKey", g.g(this.a));
            jSONObject.put("packageName", this.a.getPackageName());
            jSONObject.put("versionCode", g.U(this.a));
            jSONObject.put("versionName", g.V(this.a));
            jSONObject.put("sendTime", System.currentTimeMillis());
            jSONObject.put("userAge", URLEncoder.encode(this.c, e.f));
            jSONObject.put("userSex", URLEncoder.encode(this.d, e.f));
            jSONObject.put("userFeedback", URLEncoder.encode(replace, e.f));
            if (MobileAgent.a(this.a, "http://da.mmarket.com/mmsdk/mmsdk?func=mmsdk:feedback", jSONObject) == 1) {
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
        }
    }
}
