package com.chinaMobile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

public abstract class f {
    private PrintStream dQ;

    private static int a(InputStream inputStream, byte[] bArr) {
        for (int i = 0; i < bArr.length; i++) {
            int read = inputStream.read();
            if (read == -1) {
                return i;
            }
            bArr[i] = (byte) read;
        }
        return bArr.length;
    }

    private void a(InputStream inputStream, OutputStream outputStream) {
        byte[] bArr = new byte[57];
        this.dQ = new PrintStream(outputStream);
        while (true) {
            int a = a(inputStream, bArr);
            if (a != 0) {
                for (int i = 0; i < a; i += 3) {
                    if (i + 3 <= a) {
                        a(outputStream, bArr, i, 3);
                    } else {
                        a(outputStream, bArr, i, a - i);
                    }
                }
                if (a >= 57) {
                    this.dQ.println();
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    public final String a(byte[] bArr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            a(new ByteArrayInputStream(bArr), byteArrayOutputStream);
            return byteArrayOutputStream.toString("8859_1");
        } catch (Exception e) {
            throw new Error("CharacterEncoder.encode internal error");
        }
    }

    /* access modifiers changed from: protected */
    public abstract void a(OutputStream outputStream, byte[] bArr, int i, int i2);
}
