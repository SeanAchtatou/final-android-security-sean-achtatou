package com.chinaMobile;

import android.content.Context;
import android.util.Log;
import java.lang.Thread;

public final class c implements Thread.UncaughtExceptionHandler {
    private static c dN = new c();
    private Context b;

    private c() {
    }

    public static c aj() {
        return dN;
    }

    public final void a(Context context) {
        this.b = context;
        Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
    }

    public final void uncaughtException(Thread thread, Throwable th) {
        StackTraceElement[] stackTrace = th.getStackTrace();
        String str = "";
        for (int i = 0; i < stackTrace.length; i++) {
            str = String.valueOf(str) + stackTrace[i].toString() + "\n";
        }
        String str2 = String.valueOf(th.toString()) + "\n" + str;
        Log.i("CrashHandler", str2);
        MobileAgent.n(this.b, str2);
        System.exit(10);
    }
}
