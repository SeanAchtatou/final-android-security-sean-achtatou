package com.chinaMobile;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import com.umeng.common.a;
import com.umeng.common.b.e;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Random;
import org.apache.http.HttpHost;
import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MobileAgent {
    public static final String SDK_VERSION = "3.1.1";
    public static final String USER_STATUS_ACTIVATE = "activate";
    public static final String USER_STATUS_CUSTOM = "custom";
    public static final String USER_STATUS_LOGIN = "login";
    public static final String USER_STATUS_REGIST = "regist";
    public static final String USER_STATUS_START = "start";
    private static boolean b = true;
    private static boolean c = false;
    private static boolean d = true;
    private static JSONObject dG;
    private static MMOnlineConfigureListener dH;
    private static long dI = 0;
    private static Object dJ = new Object();
    private static boolean dK = false;
    private static boolean dL = false;
    private static int e = 1;
    private static int f = 0;
    private static String h = "";
    private static String i = "";
    private static String k = "";

    private static void B(Context context) {
        if (!"onPause".equals(i) || !context.getClass().getName().equals(h)) {
            "do pausep start: " + context.getClass().getName();
            SharedPreferences F = F(context);
            String string = F.getString("activities", null);
            SharedPreferences.Editor edit = F.edit();
            edit.putLong("endTime", System.currentTimeMillis());
            edit.putString("activities", l(context, "onPause", string));
            edit.commit();
            "do pauseP" + context.getClass().getName();
        }
    }

    public static void C(Context context) {
        c.aj().a(context);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.chinaMobile.MobileAgent.a(android.content.Context, android.content.SharedPreferences, boolean):boolean
     arg types: [android.content.Context, android.content.SharedPreferences, int]
     candidates:
      com.chinaMobile.MobileAgent.a(android.content.Context, java.lang.String, org.json.JSONObject):int
      com.chinaMobile.MobileAgent.a(android.content.Context, java.lang.String, int):boolean
      com.chinaMobile.MobileAgent.a(android.content.Context, android.content.SharedPreferences, boolean):boolean */
    private static boolean D(Context context) {
        String g = g.g(context);
        SharedPreferences F = F(context);
        String string = F.getString("sessionId", null);
        if (b(F)) {
            if (string != null) {
                f(context, F);
                a(context, F, true);
                SharedPreferences.Editor edit = F.edit();
                Long valueOf = Long.valueOf(F.getLong("readFlowRev", 0));
                Long valueOf2 = Long.valueOf(F.getLong("readFlowSnd", 0));
                edit.clear();
                edit.putLong("readFlowRev", valueOf.longValue());
                edit.putLong("readFlowSnd", valueOf2.longValue());
                edit.commit();
            }
            b(context, g, F);
            a(context, F, false);
            if (!b(context, 1)) {
                return true;
            }
            J(context);
            h(context, "#applist", g.n(context));
            return true;
        }
        String string2 = F.getString("activities", null);
        SharedPreferences.Editor edit2 = F.edit();
        edit2.putString("activities", l(context, "onResume", string2));
        edit2.putLong("lastResumeTime", System.currentTimeMillis());
        edit2.commit();
        if (e == 1 || string2.getBytes().length > 10000) {
            a(context, F, false);
        }
        return false;
    }

    private static SharedPreferences E(Context context) {
        return context.getSharedPreferences("MoblieAgent_config_" + context.getPackageName(), 0);
    }

    private static SharedPreferences F(Context context) {
        return context.getSharedPreferences("MoblieAgent_state_" + context.getPackageName(), 0);
    }

    protected static SharedPreferences G(Context context) {
        return context.getSharedPreferences("MoblieAgent_event_" + context.getPackageName(), 0);
    }

    protected static SharedPreferences H(Context context) {
        return context.getSharedPreferences("MoblieAgent_upload_" + context.getPackageName(), 0);
    }

    protected static JSONObject I(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("pid", 1);
            jSONObject.put("protocolVersion", "3.1.0");
            jSONObject.put("sdkVersion", SDK_VERSION);
            jSONObject.put("cid", g.R(context));
            jSONObject.put("appKey", g.g(context));
            jSONObject.put("packageName", context.getPackageName());
            jSONObject.put("versionCode", g.U(context));
            jSONObject.put("versionName", g.V(context));
            jSONObject.put("sendTime", System.currentTimeMillis());
            jSONObject.put("deviceId", g.a(context));
            jSONObject.put(a.d, g.h(context));
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return jSONObject;
    }

    protected static void J(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("pid", 1);
            jSONObject.put("protocolVersion", "3.1.0");
            jSONObject.put("sdkVersion", SDK_VERSION);
            jSONObject.put("cid", g.R(context));
            jSONObject.put("deviceId", g.a(context));
            jSONObject.put("appKey", g.g(context));
            jSONObject.put("packageName", context.getPackageName());
            jSONObject.put("versionCode", g.U(context));
            jSONObject.put("versionName", g.V(context));
            jSONObject.put("sendTime", System.currentTimeMillis());
            jSONObject.put("imsi", g.S(context));
            jSONObject.put("mac", g.f(context));
            jSONObject.put("deviceDetail", URLEncoder.encode(g.b(Build.MODEL), e.f));
            jSONObject.put("manufacturer", URLEncoder.encode(g.b(Build.MANUFACTURER), e.f));
            jSONObject.put("phoneOS", URLEncoder.encode(g.a(), e.f));
            jSONObject.put("screenWidth", g.d(context));
            jSONObject.put("screenHeight", g.e(context));
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
            jSONObject.put("screenDensity", displayMetrics.densityDpi);
            jSONObject.put("carrierName", URLEncoder.encode(g.i(context), e.f));
            jSONObject.put("accessPoint", g.T(context));
            jSONObject.put("countryCode", Locale.getDefault().getCountry());
            jSONObject.put("languageCode", Locale.getDefault().getLanguage());
            jSONObject.put(a.d, URLEncoder.encode(g.h(context), e.f));
            if (a(context, jSONObject.toString(), 1)) {
                c(context, 1);
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        } catch (UnsupportedEncodingException e3) {
            e3.printStackTrace();
        }
    }

    protected static void K(Context context) {
        HttpPost httpPost = new HttpPost("http://da.mmarket.com/mmsdk/mmsdk?func=mmsdk:getappparameter&appkey=" + g.g(context));
        try {
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpClientParams.setRedirecting(basicHttpParams, true);
            HttpProtocolParams.setUserAgent(basicHttpParams, m.a());
            HttpProtocolParams.setContentCharset(basicHttpParams, "utf-8");
            HttpProtocolParams.setHttpElementCharset(basicHttpParams, "utf-8");
            if (m.a(context).equals("cmwap")) {
                basicHttpParams.setParameter("http.route.default-proxy", new HttpHost("10.0.0.172", 80, (String) null));
            }
            DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
            defaultHttpClient.setRedirectHandler(new l());
            JSONObject jSONObject = new JSONObject(EntityUtils.toString(defaultHttpClient.execute(httpPost).getEntity()));
            SharedPreferences.Editor edit = E(context).edit();
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                edit.putString(next, jSONObject.getString(next));
            }
            edit.commit();
            if (jSONObject.length() > 0 && dH != null) {
                dH.a(jSONObject);
            }
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        } catch (JSONException e3) {
            e3.printStackTrace();
        } catch (ParseException e4) {
            e4.printStackTrace();
        } catch (IOException e5) {
            e5.printStackTrace();
        }
    }

    protected static void L(Context context) {
        if (d) {
            if (getConfigParams(context, "updateonlyonwifi").equals("1")) {
                c = true;
            } else {
                c = false;
            }
            String configParams = getConfigParams(context, "updatedelay");
            if (!configParams.equals("0")) {
                f = Integer.parseInt(configParams) * 1000;
            }
            int parseInt = Integer.parseInt(getConfigParams(context, "send_policy"));
            e = parseInt;
            if (parseInt == 0) {
                e = 1;
            }
        }
    }

    private static synchronized long M(Context context) {
        long j;
        synchronized (MobileAgent.class) {
            j = H(context).getLong("uploadpopindex", 0);
        }
        return j;
    }

    private static String N(Context context) {
        String str;
        synchronized (dJ) {
            str = "";
            String string = H(context).getString("uploadList", "");
            if (!string.equals("")) {
                str = string.substring(0, string.indexOf("|"));
            }
        }
        return str;
    }

    static synchronized void O(Context context) {
        String str;
        long j = 0;
        synchronized (MobileAgent.class) {
            if (e == 2) {
                try {
                    Thread.sleep((long) f);
                } catch (InterruptedException e2) {
                    e2.printStackTrace();
                }
            }
            if (!dL) {
                j = g.W(context)[0] + g.W(context)[1];
            }
            long currentTimeMillis = System.currentTimeMillis();
            boolean z = true;
            while (true) {
                String N = N(context);
                if (!N.equals("") && z) {
                    if (N.substring(0, 3).equals("act")) {
                        z = h(context, N);
                    } else if (N.substring(0, 3).equals("evn")) {
                        z = m(context, N, null);
                    } else if (N.substring(0, 3).equals("err")) {
                        z = j(context, N);
                    } else if (N.substring(0, 3).equals("sys")) {
                        z = i(context, N);
                    }
                    if (z) {
                        try {
                            Thread.sleep(0);
                        } catch (InterruptedException e3) {
                            e3.printStackTrace();
                        }
                    }
                }
            }
            if (!dL) {
                long currentTimeMillis2 = System.currentTimeMillis();
                float f2 = (float) ((g.W(context)[0] + g.W(context)[1]) - j);
                StringBuffer stringBuffer = new StringBuffer();
                stringBuffer.append(k);
                stringBuffer.append("@@");
                stringBuffer.append(f2);
                stringBuffer.append("@@");
                stringBuffer.append((float) (currentTimeMillis2 - currentTimeMillis));
                stringBuffer.append("@@");
                NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
                if (!(activeNetworkInfo == null || activeNetworkInfo.getType() == 1)) {
                    String extraInfo = activeNetworkInfo.getExtraInfo();
                    Log.i("MobileUtils", "net type:" + extraInfo);
                    if (extraInfo != null && (extraInfo.equals("cmwap") || extraInfo.equals("3gwap") || extraInfo.equals("uniwap"))) {
                        str = "10.0.0.172";
                        stringBuffer.append(str);
                        stringBuffer.append("@@");
                        stringBuffer.append("23");
                        h(context, "#netStatus", stringBuffer.toString());
                        dL = true;
                    }
                }
                str = null;
                stringBuffer.append(str);
                stringBuffer.append("@@");
                try {
                    stringBuffer.append("23");
                } catch (Exception e4) {
                    stringBuffer.append("0");
                }
                h(context, "#netStatus", stringBuffer.toString());
                dL = true;
            }
        }
        return;
    }

    public static void P(Context context) {
        new p(context).show();
    }

    private static boolean Q(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.getType() == 1;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00dc, code lost:
        return 3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00de, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x00df, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        return 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:?, code lost:
        return 2;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x00d3 A[ExcHandler: ClientProtocolException (e org.apache.http.client.ClientProtocolException), Splitter:B:1:0x0002] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x00d8 A[ExcHandler: IOException (e java.io.IOException), Splitter:B:1:0x0002] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00db A[ExcHandler: JSONException (e org.json.JSONException), Splitter:B:1:0x0002] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(android.content.Context r7, java.lang.String r8, org.json.JSONObject r9) {
        /*
            r1 = 1
            r2 = 2
            java.lang.String r0 = com.chinaMobile.g.g(r7)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.lang.String r3 = com.chinaMobile.g.h(r7)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.lang.String r4 = "UTF-8"
            java.lang.String r3 = java.net.URLEncoder.encode(r3, r4)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.lang.String r5 = java.lang.String.valueOf(r8)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            r4.<init>(r5)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.lang.String r5 = "&appkey="
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.lang.StringBuilder r0 = r4.append(r0)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.lang.String r4 = "&channel="
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.lang.String r3 = "&code="
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            r3 = 105(0x69, float:1.47E-43)
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.lang.String r0 = r0.toString()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.net.URL r3 = new java.net.URL     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            r3.<init>(r0)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.net.URLConnection r0 = r3.openConnection()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            r3 = 1
            r0.setDoOutput(r3)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            r3 = 1
            r0.setDoInput(r3)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            r3 = 0
            r0.setUseCaches(r3)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.lang.String r3 = "POST"
            r0.setRequestMethod(r3)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            r9.toString()     // Catch:{ Exception -> 0x00e4, ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db }
        L_0x005c:
            com.chinaMobile.d r3 = com.chinaMobile.d.ak()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.lang.String r4 = r9.toString()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            byte[] r3 = r3.a(r4)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.lang.String r4 = "Content-length"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            r5.<init>()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            int r6 = r3.length     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.lang.String r5 = r5.toString()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            r0.setRequestProperty(r4, r5)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.lang.String r4 = "Content-Type"
            java.lang.String r5 = "application/octet-stream"
            r0.setRequestProperty(r4, r5)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.lang.String r4 = "Charset"
            java.lang.String r5 = "UTF-8"
            r0.setRequestProperty(r4, r5)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.io.OutputStream r4 = r0.getOutputStream()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            r4.write(r3)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            r4.close()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            int r3 = r0.getResponseCode()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            r4 = 200(0xc8, float:2.8E-43)
            if (r4 != r3) goto L_0x00d6
            java.lang.StringBuffer r3 = new java.lang.StringBuffer     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            r3.<init>()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.lang.String r6 = "utf-8"
            r5.<init>(r0, r6)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            r4.<init>(r5)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
        L_0x00b0:
            java.lang.String r0 = r4.readLine()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            if (r0 != 0) goto L_0x00c9
            r4.close()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.lang.String r0 = r3.toString()     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            r3.<init>(r0)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.lang.String r0 = "resultcode"
            r3.getInt(r0)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            r0 = r1
        L_0x00c8:
            return r0
        L_0x00c9:
            java.lang.StringBuffer r0 = r3.append(r0)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            java.lang.String r5 = "\n"
            r0.append(r5)     // Catch:{ ClientProtocolException -> 0x00d3, IOException -> 0x00d8, JSONException -> 0x00db, Exception -> 0x00de }
            goto L_0x00b0
        L_0x00d3:
            r0 = move-exception
            r0 = r2
            goto L_0x00c8
        L_0x00d6:
            r0 = r2
            goto L_0x00c8
        L_0x00d8:
            r0 = move-exception
            r0 = r2
            goto L_0x00c8
        L_0x00db:
            r0 = move-exception
            r0 = 3
            goto L_0x00c8
        L_0x00de:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r2
            goto L_0x00c8
        L_0x00e4:
            r3 = move-exception
            goto L_0x005c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.chinaMobile.MobileAgent.a(android.content.Context, java.lang.String, org.json.JSONObject):int");
    }

    protected static void a(Context context, String str, String str2, String str3) {
        new Thread(new i(context, str3, str, str2)).start();
    }

    protected static void a(Context context, boolean z) {
        L(context);
        if (!c || (c && Q(context))) {
            switch (e) {
                case 1:
                    new k(context, 6).start();
                    break;
                case 2:
                    if (z) {
                        new k(context, 6).start();
                        d = false;
                        break;
                    }
                    break;
                case 3:
                    if (b(context, 3)) {
                        new k(context, 6).start();
                        break;
                    }
                    break;
            }
        }
        d = false;
    }

    public static void a(MMOnlineConfigureListener mMOnlineConfigureListener) {
        dH = mMOnlineConfigureListener;
    }

    private static boolean a(Context context, SharedPreferences sharedPreferences, boolean z) {
        String string = sharedPreferences.getString("sessionId", null);
        String string2 = sharedPreferences.getString("activities", null);
        dG = new JSONObject();
        try {
            dG.put("sid", string);
            dG.put("logs", string2);
            if (z) {
                dG.put("flowConsumpRev", sharedPreferences.getLong("consumeFlowRev", 0));
                dG.put("flowConsumpSnd", sharedPreferences.getLong("consumeFlowSnd", 0));
            } else {
                dG.put("flowConsumpRev", 0);
                dG.put("flowConsumpSnd", 0);
            }
            if (!a(context, dG.toString(), 3)) {
                return true;
            }
            sharedPreferences.edit().putString("activities", "").commit();
            return true;
        } catch (JSONException e2) {
            e2.printStackTrace();
            return true;
        }
    }

    private static synchronized boolean a(Context context, String str, int i2) {
        String str2;
        boolean z = false;
        synchronized (MobileAgent.class) {
            if (i2 == 3) {
                str2 = "act";
            } else if (i2 == 2) {
                str2 = "evn";
            } else if (i2 == 4) {
                str2 = "err";
            } else if (i2 == 1) {
                str2 = "sys";
            }
            if (!str.equals("")) {
                long M = M(context);
                long j = 1 + M;
                String str3 = String.valueOf(str2) + M;
                try {
                    FileOutputStream openFileOutput = context.openFileOutput(str3, 1);
                    b(context, str3, j);
                    openFileOutput.write(str.getBytes());
                    openFileOutput.close();
                } catch (FileNotFoundException e2) {
                } catch (IOException e3) {
                }
                z = true;
            }
        }
        return z;
    }

    private static String b(Context context, String str, SharedPreferences sharedPreferences) {
        h = "";
        String valueOf = String.valueOf(g.a(String.valueOf(System.currentTimeMillis()) + str + g.a(context) + g.f(context) + new Random().nextInt(10)).toCharArray(), 8, 16);
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("appKey", str);
        edit.putString("sessionId", valueOf);
        edit.putLong("lastResumeTime", System.currentTimeMillis());
        edit.putString("activities", l(context, "onResume", null));
        long j = sharedPreferences.getLong("readFlowRev", 0);
        long[] W = g.W(context);
        edit.putLong("readFlowRev", W[0]);
        edit.putLong("consumeFlowRev", W[0] - j);
        long j2 = sharedPreferences.getLong("readFlowSnd", 0);
        edit.putLong("readFlowSnd", W[1]);
        edit.putLong("consumeFlowSnd", W[1] - j2);
        edit.commit();
        k = valueOf;
        return valueOf;
    }

    private static void b(Context context, String str, long j) {
        synchronized (dJ) {
            SharedPreferences H = H(context);
            int i2 = H.getInt("uploadcount", 0);
            String str2 = String.valueOf(H.getString("uploadList", "")) + str + "|";
            if (j > 10000) {
                j = 1;
            }
            H.edit().putString("uploadList", str2).commit();
            H.edit().putLong("uploadpopindex", j).commit();
            if (str2.split("\\|").length > 200) {
                String N = N(context);
                context.deleteFile(N);
                m(context, N);
            } else {
                H.edit().putInt("uploadcount", i2 + 1).commit();
            }
        }
    }

    protected static boolean b(Context context, int i2) {
        int i3;
        int i4;
        SharedPreferences E = E(context);
        if (i2 == 3) {
            i3 = E.getInt("actionmonth", 0);
            i4 = E.getInt("actionday", 0);
        } else if (i2 == 2) {
            i3 = E.getInt("eventmonth", 0);
            i4 = E.getInt("eventday", 0);
        } else {
            i3 = E.getInt("sysmonth", 0);
            i4 = E.getInt("sysday", 0);
        }
        Date date = new Date();
        return (Integer.valueOf(new SimpleDateFormat("M").format(date)).intValue() == i3 && Integer.valueOf(new SimpleDateFormat("dd").format(date)).intValue() == i4) ? false : true;
    }

    private static boolean b(SharedPreferences sharedPreferences) {
        return System.currentTimeMillis() - sharedPreferences.getLong("endTime", -1) > 30000;
    }

    private static String c(Context context, String str, SharedPreferences sharedPreferences) {
        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString("appKey", str);
        edit.putString("sessionId", k);
        edit.putLong("lastResumeTime", System.currentTimeMillis());
        edit.putString("activities", "");
        edit.commit();
        return k;
    }

    protected static void c(Context context, int i2) {
        Date date = new Date();
        int parseInt = Integer.parseInt(new SimpleDateFormat("dd").format(date));
        int parseInt2 = Integer.parseInt(new SimpleDateFormat("M").format(date));
        SharedPreferences.Editor edit = E(context).edit();
        if (i2 == 3) {
            edit.putInt("actionmonth", parseInt2);
            edit.putInt("actionday", parseInt);
        } else if (i2 == 2) {
            edit.putInt("eventmonth", parseInt2);
            edit.putInt("eventday", parseInt);
        } else {
            edit.putInt("sysmonth", parseInt2);
            edit.putInt("sysday", parseInt);
        }
        edit.commit();
    }

    protected static synchronized void e(Context context, String str, String str2) {
        synchronized (MobileAgent.class) {
            if (str != null) {
                f(context, str, str2);
            } else {
                B(context);
            }
        }
    }

    public static void e(Boolean bool) {
        b = bool.booleanValue();
    }

    private static void f(Context context, String str, String str2) {
        try {
            if (!"onResume".equals(i) || !context.getClass().getName().equals(h)) {
                "do resume start: " + context.getClass().getName();
                a(context, D(context));
                "do resume:" + context.getClass().getName();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private static boolean f(Context context, SharedPreferences sharedPreferences) {
        SharedPreferences G = G(context);
        String string = G.getString("eventlogs", "");
        if (string.equals("")) {
            return false;
        }
        String string2 = F(context).getString("sessionId", null);
        dG = new JSONObject();
        try {
            dG.put("sid", string2);
            dG.put("logJsonAry", string);
            if (a(context, dG.toString(), 2)) {
                G.edit().putString("eventlogs", "").commit();
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return true;
    }

    protected static void g(Context context, String str, String str2) {
        if (str.equals("userID")) {
            StringBuffer stringBuffer = new StringBuffer();
            try {
                stringBuffer.append(URLEncoder.encode(str, e.f));
                stringBuffer.append("|");
                stringBuffer.append(URLEncoder.encode(str2, e.f));
                stringBuffer.append("|");
                stringBuffer.append(0);
                stringBuffer.append("|");
                stringBuffer.append(System.currentTimeMillis());
                stringBuffer.append("\n");
                JSONObject jSONObject = new JSONObject();
                dG = jSONObject;
                jSONObject.put("sid", k);
                dG.put("logJsonAry", stringBuffer.toString());
                new k(context, dG.toString()).start();
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
            } catch (JSONException e3) {
                e3.printStackTrace();
            }
        } else {
            String b2 = g.b(str);
            String b3 = g.b(str2);
            SharedPreferences G = G(context);
            StringBuffer stringBuffer2 = new StringBuffer();
            stringBuffer2.append(G.getString("eventlogs", ""));
            try {
                stringBuffer2.append(URLEncoder.encode(b2, e.f));
                stringBuffer2.append("|");
                stringBuffer2.append(URLEncoder.encode(b3, e.f));
                stringBuffer2.append("|");
                stringBuffer2.append(1);
                stringBuffer2.append("|");
                stringBuffer2.append(System.currentTimeMillis());
                stringBuffer2.append("\n");
                G.edit().putString("eventlogs", stringBuffer2.toString()).commit();
                if (e == 1 || stringBuffer2.toString().getBytes().length > 10000) {
                    f(context, null);
                }
            } catch (UnsupportedEncodingException e4) {
            }
        }
    }

    public static String getConfigParams(Context context, String str) {
        return E(context).getString(str, "0");
    }

    private static void h(Context context, String str, String str2) {
        SharedPreferences G = G(context);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(G.getString("eventlogs", ""));
        try {
            stringBuffer.append(URLEncoder.encode(str, e.f));
            stringBuffer.append("|");
            stringBuffer.append(URLEncoder.encode(str2, e.f));
            stringBuffer.append("|");
            stringBuffer.append(0);
            stringBuffer.append("|");
            stringBuffer.append(System.currentTimeMillis());
            stringBuffer.append("\n");
            G.edit().putString("eventlogs", stringBuffer.toString()).commit();
            if (e == 1 || stringBuffer.toString().getBytes().length > 10000) {
                f(context, null);
            }
        } catch (UnsupportedEncodingException e2) {
        }
    }

    protected static boolean h(Context context, String str) {
        int i2;
        String k2 = k(context, str);
        if (!k2.equals("")) {
            JSONObject I = I(context);
            try {
                I.put("sid", new JSONObject(k2).get("sid"));
                I.put("mac", g.f(context));
                try {
                    I.put("deviceDetail", URLEncoder.encode(g.b(Build.MODEL), e.f));
                } catch (UnsupportedEncodingException e2) {
                    I.put("deviceDetail", "");
                }
                try {
                    I.put("manufacturer", URLEncoder.encode(g.b(Build.MANUFACTURER), e.f));
                } catch (UnsupportedEncodingException e3) {
                    I.put("manufacturer", "");
                }
                try {
                    I.put("phoneOs", URLEncoder.encode(g.a(), e.f));
                } catch (UnsupportedEncodingException e4) {
                    I.put("phoneOs", "");
                }
                I.put("accessPoint", g.T(context));
                I.put("deviceId", g.a(context));
                I.put("cpuRatioMax", g.b());
                I.put("cpuRatioCur", g.c());
                I.put("menoryRatio", g.d());
                I.put("logJsonAry", new JSONArray("[" + k2 + "]"));
                i2 = a(context, "http://da.mmarket.com/mmsdk/mmsdk?func=mmsdk:postactlog", I);
            } catch (JSONException e5) {
                e5.printStackTrace();
                i2 = 3;
            }
            if (i2 == 1 || i2 == 3) {
                c(context, 3);
                l(context, str);
                Log.i("MobileAgent", "act log sd");
                return true;
            }
            if (i2 == 2) {
            }
            return false;
        }
        c(context, 3);
        l(context, str);
        return true;
    }

    public static void i(Context context, String str, String str2) {
        StringBuffer stringBuffer = new StringBuffer();
        try {
            stringBuffer.append(URLEncoder.encode(g.h(context), e.f));
        } catch (UnsupportedEncodingException e2) {
            stringBuffer.append("");
        }
        stringBuffer.append("@@");
        stringBuffer.append(g.S(context));
        stringBuffer.append("@@");
        stringBuffer.append(g.b(Build.MANUFACTURER));
        stringBuffer.append("@@");
        stringBuffer.append(g.b(Build.MODEL));
        stringBuffer.append("@@");
        try {
            stringBuffer.append(URLEncoder.encode(g.a(), e.f));
        } catch (UnsupportedEncodingException e3) {
            stringBuffer.append("");
        }
        stringBuffer.append("@@");
        stringBuffer.append(g.f(context));
        stringBuffer.append("@@");
        stringBuffer.append(g.T(context));
        stringBuffer.append("@@");
        stringBuffer.append(String.valueOf(g.d(context)) + "*" + g.e(context));
        stringBuffer.append("@@");
        try {
            stringBuffer.append(URLEncoder.encode(str2, e.f));
        } catch (UnsupportedEncodingException e4) {
            stringBuffer.append("");
        }
        StringBuffer stringBuffer2 = new StringBuffer();
        try {
            stringBuffer2.append(URLEncoder.encode(str, e.f));
            stringBuffer2.append("|");
            stringBuffer2.append(URLEncoder.encode(stringBuffer.toString(), e.f));
            stringBuffer2.append("|");
            stringBuffer2.append(0);
            stringBuffer2.append("|");
            stringBuffer2.append(System.currentTimeMillis());
            stringBuffer2.append("\n");
            JSONObject jSONObject = new JSONObject();
            dG = jSONObject;
            jSONObject.put("sid", k);
            dG.put("logJsonAry", stringBuffer2.toString());
            new k(context, dG.toString()).start();
        } catch (UnsupportedEncodingException e5) {
            e5.printStackTrace();
        } catch (JSONException e6) {
            e6.printStackTrace();
        }
    }

    protected static boolean i(Context context, String str) {
        try {
            if (a(context, "http://da.mmarket.com/mmsdk/mmsdk?func=mmsdk:postsyslog", new JSONObject(k(context, str))) == 1) {
                l(context, str);
                Log.i("MobileAgent", "send syslog success");
                return true;
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return false;
    }

    public static void j(Context context, String str, String str2) {
        StringBuffer stringBuffer = new StringBuffer();
        if (str == null || str.equals("")) {
            stringBuffer.append("guest");
        } else {
            stringBuffer.append(str);
        }
        stringBuffer.append("@@");
        if (str2 == null) {
            stringBuffer.append("other");
        } else {
            stringBuffer.append(str2);
        }
        stringBuffer.append("@@");
        try {
            stringBuffer.append(URLEncoder.encode(g.h(context), e.f));
        } catch (UnsupportedEncodingException e2) {
            stringBuffer.append("");
        }
        stringBuffer.append("@@");
        stringBuffer.append(g.S(context));
        stringBuffer.append("@@");
        stringBuffer.append(g.b(Build.MODEL));
        stringBuffer.append("@@");
        try {
            stringBuffer.append(URLEncoder.encode(g.a(), e.f));
        } catch (UnsupportedEncodingException e3) {
            stringBuffer.append("");
        }
        stringBuffer.append("@@");
        stringBuffer.append(g.f(context));
        stringBuffer.append("@@");
        stringBuffer.append(g.T(context));
        onEvent(context, "userID", stringBuffer.toString());
    }

    protected static boolean j(Context context, String str) {
        try {
            JSONObject jSONObject = new JSONObject(k(context, str));
            jSONObject.put("pid", 1);
            jSONObject.put("protocolVersion", "3.1.0");
            jSONObject.put("sdkVersion", SDK_VERSION);
            jSONObject.put("cid", g.R(context));
            jSONObject.put("deviceId", g.a(context));
            jSONObject.put("appKey", g.g(context));
            jSONObject.put("packageName", context.getPackageName());
            jSONObject.put("versionCode", g.U(context));
            jSONObject.put("versionName", g.V(context));
            jSONObject.put("sendTime", System.currentTimeMillis());
            int a = a(context, "http://da.mmarket.com/mmsdk/mmsdk?func=mmsdk:posterrlog", jSONObject);
            if (a == 1 || a == 3) {
                c(context, 3);
                l(context, str);
                "send errlog success" + str;
                return true;
            }
            if (a == 2) {
            }
            return false;
        } catch (JSONException e2) {
            e2.printStackTrace();
            return false;
        }
    }

    private static synchronized String k(Context context, String str) {
        String str2;
        synchronized (MobileAgent.class) {
            str2 = "";
            try {
                FileInputStream openFileInput = context.openFileInput(str);
                byte[] bArr = new byte[10000];
                while (true) {
                    int read = openFileInput.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    str2 = String.valueOf(str2) + new String(bArr, 0, read);
                }
                openFileInput.close();
            } catch (FileNotFoundException e2) {
                str2 = "";
            } catch (IOException e3) {
            }
        }
        return str2;
    }

    public static void k(Context context, String str, String str2) {
        if (!dK) {
            SharedPreferences sharedPreferences = context.getSharedPreferences("MoblieAgent_sys_config", 0);
            sharedPreferences.edit().putString("MOBILE_APPKEY", str).commit();
            sharedPreferences.edit().putString("MOBILE_CHANNEL", str2).commit();
            onResume(context);
            if (str.length() > 7) {
                Log.i("MobileAgent", "init: " + str.substring(6));
            }
            dK = true;
        }
    }

    private static String l(Context context, String str, String str2) {
        long j = 0;
        long currentTimeMillis = System.currentTimeMillis();
        if (str.equals("onResume")) {
            dI = currentTimeMillis;
        } else if (str.equals("onPause") && h.equals(context.getClass().getName())) {
            j = currentTimeMillis - dI;
            if (j > 6000000) {
                j = 300000;
            }
        }
        if (str2 == null) {
            str2 = "";
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(str2);
        stringBuffer.append(str);
        stringBuffer.append("|");
        stringBuffer.append(context.getClass().getName());
        stringBuffer.append("|");
        stringBuffer.append(currentTimeMillis);
        stringBuffer.append("|");
        stringBuffer.append(j);
        stringBuffer.append("|");
        stringBuffer.append(h);
        stringBuffer.append("\n");
        h = context.getClass().getName();
        i = str;
        return stringBuffer.toString();
    }

    private static void l(Context context, String str) {
        context.deleteFile(str);
        m(context, str);
    }

    private static void m(Context context, String str) {
        synchronized (dJ) {
            SharedPreferences H = H(context);
            H.edit().putString("uploadList", H.getString("uploadList", "").replace(String.valueOf(str) + "|", "")).commit();
        }
    }

    protected static boolean m(Context context, String str, String str2) {
        String k2 = str2 == null ? k(context, str) : str2;
        if (!k2.equals("")) {
            try {
                JSONObject jSONObject = new JSONObject(k2);
                String R = g.R(context);
                jSONObject.put("pid", 1);
                jSONObject.put("protocolVersion", "3.1.0");
                jSONObject.put("sdkVersion", SDK_VERSION);
                jSONObject.put("cid", R);
                jSONObject.put("appKey", g.g(context));
                jSONObject.put("packageName", context.getPackageName());
                jSONObject.put("versionCode", g.U(context));
                jSONObject.put("versionName", g.V(context));
                jSONObject.put("sendTime", System.currentTimeMillis());
                jSONObject.put("mac", g.f(context));
                try {
                    jSONObject.put("deviceDetail", URLEncoder.encode(g.b(Build.MODEL), e.f));
                } catch (UnsupportedEncodingException e2) {
                    jSONObject.put("deviceDetail", "");
                }
                try {
                    jSONObject.put("manufacturer", URLEncoder.encode(g.b(Build.MANUFACTURER), e.f));
                } catch (UnsupportedEncodingException e3) {
                    jSONObject.put("manufacturer", "");
                }
                try {
                    jSONObject.put("phoneOs", URLEncoder.encode(g.a(), e.f));
                } catch (UnsupportedEncodingException e4) {
                    jSONObject.put("phoneOs", "");
                }
                jSONObject.put("accessPoint", g.T(context));
                jSONObject.put("deviceId", g.a(context));
                jSONObject.put("deviceId", g.a(context));
                try {
                    jSONObject.put(a.d, URLEncoder.encode(g.h(context), e.f));
                } catch (UnsupportedEncodingException e5) {
                    jSONObject.put(a.d, "");
                }
                int a = a(context, "http://da.mmarket.com/mmsdk/mmsdk?func=mmsdk:posteventlog", jSONObject);
                if (str2 != null || (a != 1 && a != 3)) {
                    return a == 2 ? false : false;
                }
                c(context, 3);
                l(context, str);
                Log.i("MobileAgent", "evn log sd");
                return true;
            } catch (JSONException e6) {
                e6.printStackTrace();
                return true;
            }
        } else if (str2 != null) {
            return false;
        } else {
            c(context, 3);
            l(context, str);
            return true;
        }
    }

    protected static void n(Context context, String str) {
        try {
            JSONObject jSONObject = new JSONObject();
            JSONArray jSONArray = new JSONArray();
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("occurtime", System.currentTimeMillis());
            jSONObject2.put("errmsg", URLEncoder.encode(str, e.f));
            jSONArray.put(jSONObject2);
            jSONObject.put("sid", k);
            jSONObject.put("errjsonary", jSONArray);
            a(context, jSONObject.toString(), 4);
        } catch (UnsupportedEncodingException | JSONException e2) {
        }
    }

    private static boolean n(Context context, String str, String str2) {
        dG = new JSONObject();
        try {
            dG.put("sid", str2);
            dG.put("logs", str);
            a(context, dG.toString(), 3);
            return true;
        } catch (JSONException e2) {
            e2.printStackTrace();
            return true;
        }
    }

    public static void onEvent(Context context, String str) {
        onEvent(context, str, str, 1);
    }

    public static void onEvent(Context context, String str, int i2) {
        onEvent(context, str, str, i2);
    }

    public static void onEvent(Context context, String str, String str2) {
        onEvent(context, str, str2, 1);
    }

    public static void onEvent(Context context, String str, String str2, int i2) {
        if (context == null) {
            Log.e("MobileAgent", "onEvent context is null");
        } else if (str == null || str.equals("")) {
            Log.e("MobileAgent", "onEvent eventId is null or empty ");
        } else if (str2 != null && !str2.equals("") && i2 > 0) {
            new k(context, 11, str, str2).start();
        }
    }

    public static void onPause(Context context) {
        new k(context, 10).start();
    }

    public static void onResume(Context context) {
        onResume(context, g.g(context), g.h(context));
    }

    private static void onResume(Context context, String str, String str2) {
        try {
            if (b) {
                c.aj().a(context);
            }
            if (context == null) {
                Log.e("MobileAgent", "unexpected null context");
            } else if (str == null || str.length() == 0) {
                Log.e("MobileAgent", "unexpected empty appkey");
            } else {
                new k(context, 9, str, str2).start();
            }
        } catch (Exception e2) {
            Log.e("MobileAgent", "Exception occurred in Mobclick.onResume(). ");
            e2.printStackTrace();
        }
    }

    public static void updateOnlineConfig(Context context) {
        new k(context, 8).start();
    }
}
