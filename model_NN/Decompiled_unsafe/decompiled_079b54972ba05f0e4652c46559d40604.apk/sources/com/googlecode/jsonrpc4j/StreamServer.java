package com.googlecode.jsonrpc4j;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLException;

public class StreamServer {
    /* access modifiers changed from: private */
    public static final Logger LOGGER = Logger.getLogger(StreamServer.class.getName());
    private static final long SERVER_SOCKET_SO_TIMEOUT = 5000;
    /* access modifiers changed from: private */
    public ThreadPoolExecutor executor;
    private AtomicBoolean isStarted;
    /* access modifiers changed from: private */
    public JsonRpcServer jsonRpcServer;
    /* access modifiers changed from: private */
    public AtomicBoolean keepRunning;
    /* access modifiers changed from: private */
    public int maxClientErrors;
    /* access modifiers changed from: private */
    public ServerSocket serverSocket;

    private class Server implements Runnable {
        private Server() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
         arg types: [java.util.logging.Level, java.lang.String, javax.net.ssl.SSLException]
         candidates:
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
         arg types: [java.util.logging.Level, java.lang.String, java.io.IOException]
         candidates:
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
          ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
        public void run() {
            Socket socket;
            ServerSocket access$100 = StreamServer.this.serverSocket;
            Socket socket2 = null;
            while (true) {
                if (!StreamServer.this.keepRunning.get()) {
                    socket = socket2;
                    break;
                }
                try {
                    access$100.setSoTimeout(5000);
                    socket2 = access$100.accept();
                    StreamServer.LOGGER.log(Level.INFO, "Connection from " + socket2.getInetAddress() + ":" + socket2.getPort());
                    StreamServer.this.executor.submit(new Server());
                    socket = socket2;
                    break;
                } catch (SocketTimeoutException e) {
                } catch (SSLException e2) {
                    SSLException sSLException = e2;
                    StreamServer.LOGGER.log(Level.SEVERE, "SSLException while listening for clients, terminating", (Throwable) sSLException);
                    socket = socket2;
                } catch (IOException e3) {
                    if (SocketException.class.isInstance(e3) && !StreamServer.this.keepRunning.get()) {
                        socket = socket2;
                        break;
                    }
                    StreamServer.LOGGER.log(Level.SEVERE, "Exception while listening for clients", (Throwable) e3);
                }
            }
            try {
                InputStream inputStream = socket.getInputStream();
                OutputStream outputStream = socket.getOutputStream();
                int i = 0;
                while (true) {
                    if (StreamServer.this.keepRunning.get()) {
                        try {
                            StreamServer.this.jsonRpcServer.handle(inputStream, outputStream);
                        } catch (Throwable th) {
                            i++;
                            if (i >= StreamServer.this.maxClientErrors) {
                                StreamServer.LOGGER.log(Level.SEVERE, "Closing client connection due to repeated errors", th);
                                break;
                            }
                            StreamServer.LOGGER.log(Level.SEVERE, "Exception while handling request", th);
                        }
                    }
                }
                try {
                    socket.close();
                    inputStream.close();
                    outputStream.close();
                } catch (IOException e4) {
                }
            } catch (IOException e5) {
                StreamServer.LOGGER.log(Level.SEVERE, "Client socket failed", (Throwable) e5);
            }
        }
    }

    public StreamServer(JsonRpcServer jsonRpcServer2, int i, int i2, int i3, InetAddress inetAddress) {
        this(jsonRpcServer2, i, ServerSocketFactory.getDefault().createServerSocket(i2, i3, inetAddress));
    }

    public StreamServer(JsonRpcServer jsonRpcServer2, int i, ServerSocket serverSocket2) {
        this.maxClientErrors = 5;
        this.isStarted = new AtomicBoolean(false);
        this.keepRunning = new AtomicBoolean(false);
        this.jsonRpcServer = jsonRpcServer2;
        this.serverSocket = serverSocket2;
        this.executor = new ThreadPoolExecutor(i + 1, i + 1, 0, TimeUnit.MILLISECONDS, new LinkedBlockingQueue());
        this.executor.setRejectedExecutionHandler(new ThreadPoolExecutor.AbortPolicy());
        jsonRpcServer2.setRethrowExceptions(false);
    }

    public int getMaxClientErrors() {
        return this.maxClientErrors;
    }

    public boolean isStarted() {
        return this.isStarted.get();
    }

    public void setMaxClientErrors(int i) {
        this.maxClientErrors = i;
    }

    public void start() {
        if (!this.isStarted.compareAndSet(false, true)) {
            throw new IllegalStateException("The StreamServer is already started");
        }
        LOGGER.log(Level.INFO, "StreamServer starting " + this.serverSocket.getInetAddress() + ":" + this.serverSocket.getLocalPort());
        this.keepRunning.set(true);
        this.executor.submit(new Server());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.InterruptedException]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public void stop() {
        if (!this.isStarted.get()) {
            throw new IllegalStateException("The StreamServer is not started");
        }
        this.keepRunning.set(false);
        this.executor.shutdownNow();
        try {
            this.serverSocket.close();
        } catch (IOException e) {
        }
        try {
            if (!this.executor.isTerminated()) {
                this.executor.awaitTermination(7000, TimeUnit.MILLISECONDS);
            }
            this.isStarted.set(false);
            this.keepRunning.set(false);
        } catch (InterruptedException e2) {
            LOGGER.log(Level.SEVERE, "InterruptedException while waiting for termination", (Throwable) e2);
            throw e2;
        }
    }
}
