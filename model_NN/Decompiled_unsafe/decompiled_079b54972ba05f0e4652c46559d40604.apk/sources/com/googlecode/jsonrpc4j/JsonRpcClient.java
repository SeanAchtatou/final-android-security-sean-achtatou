package com.googlecode.jsonrpc4j;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.microedition.media.PlayerListener;

public class JsonRpcClient {
    private static final String JSON_RPC_VERSION = "2.0";
    private static final Logger LOGGER = Logger.getLogger(JsonRpcClient.class.getName());
    private ExceptionResolver exceptionResolver;
    private ObjectMapper mapper;
    private Random random;
    private RequestListener requestListener;

    public interface RequestListener {
        void onBeforeRequestSent(JsonRpcClient jsonRpcClient, ObjectNode objectNode);

        void onBeforeResponseProcessed(JsonRpcClient jsonRpcClient, ObjectNode objectNode);
    }

    public JsonRpcClient() {
        this(new ObjectMapper());
    }

    public JsonRpcClient(ObjectMapper objectMapper) {
        this.exceptionResolver = DefaultExceptionResolver.INSTANCE;
        this.mapper = objectMapper;
        this.random = new Random(System.currentTimeMillis());
    }

    private void internalWriteRequest(String str, Object obj, OutputStream outputStream, String str2) {
        ObjectNode createObjectNode = this.mapper.createObjectNode();
        if (str2 != null) {
            createObjectNode.put("id", str2);
        }
        createObjectNode.put("jsonrpc", JSON_RPC_VERSION);
        createObjectNode.put("method", str);
        if (obj != null && obj.getClass().isArray()) {
            Object[] cast = Object[].class.cast(obj);
            if (cast.length > 0) {
                ArrayNode arrayNode = new ArrayNode(this.mapper.getNodeFactory());
                for (Object valueToTree : cast) {
                    arrayNode.add(this.mapper.valueToTree(valueToTree));
                }
                createObjectNode.put("params", arrayNode);
            }
        } else if (obj != null && Collection.class.isInstance(obj)) {
            Collection<Object> cast2 = Collection.class.cast(obj);
            if (!cast2.isEmpty()) {
                ArrayNode arrayNode2 = new ArrayNode(this.mapper.getNodeFactory());
                for (Object valueToTree2 : cast2) {
                    arrayNode2.add(this.mapper.valueToTree(valueToTree2));
                }
                createObjectNode.put("params", arrayNode2);
            }
        } else if (obj == null || !Map.class.isInstance(obj)) {
            if (obj != null) {
                createObjectNode.put("params", this.mapper.valueToTree(obj));
            }
        } else if (!Map.class.cast(obj).isEmpty()) {
            createObjectNode.put("params", this.mapper.valueToTree(obj));
        }
        if (this.requestListener != null) {
            this.requestListener.onBeforeRequestSent(this, createObjectNode);
        }
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "JSON-PRC Request: " + createObjectNode.toString());
        }
        writeAndFlushValue(outputStream, createObjectNode);
    }

    private void writeAndFlushValue(OutputStream outputStream, Object obj) {
        this.mapper.writeValue(new NoCloseOutputStream(outputStream), obj);
        outputStream.flush();
    }

    public ObjectMapper getObjectMapper() {
        return this.mapper;
    }

    public void invoke(String str, Object obj, OutputStream outputStream) {
        invoke(str, obj, outputStream, this.random.nextLong() + "");
    }

    public void invoke(String str, Object obj, OutputStream outputStream, String str2) {
        writeRequest(str, obj, outputStream, str2);
        outputStream.flush();
    }

    public <T> T invokeAndReadResponse(String str, Object obj, Class<T> cls, OutputStream outputStream, InputStream inputStream) {
        return invokeAndReadResponse(str, obj, (Type) Type.class.cast(cls), outputStream, inputStream);
    }

    public <T> T invokeAndReadResponse(String str, Object obj, Class<T> cls, OutputStream outputStream, InputStream inputStream, String str2) {
        return invokeAndReadResponse(str, obj, (Type) Type.class.cast(cls), outputStream, inputStream, str2);
    }

    public Object invokeAndReadResponse(String str, Object obj, Type type, OutputStream outputStream, InputStream inputStream) {
        return invokeAndReadResponse(str, obj, type, outputStream, inputStream, this.random.nextLong() + "");
    }

    public Object invokeAndReadResponse(String str, Object obj, Type type, OutputStream outputStream, InputStream inputStream, String str2) {
        invoke(str, obj, outputStream, str2);
        return readResponse(type, inputStream);
    }

    public void invokeNotification(String str, Object obj, OutputStream outputStream) {
        writeRequest(str, obj, outputStream, null);
        outputStream.flush();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.googlecode.jsonrpc4j.JsonRpcClient.readResponse(java.lang.reflect.Type, java.io.InputStream):java.lang.Object
     arg types: [java.lang.Class<T>, java.io.InputStream]
     candidates:
      com.googlecode.jsonrpc4j.JsonRpcClient.readResponse(java.lang.Class, java.io.InputStream):T
      com.googlecode.jsonrpc4j.JsonRpcClient.readResponse(java.lang.reflect.Type, java.io.InputStream):java.lang.Object */
    public <T> T readResponse(Class<T> cls, InputStream inputStream) {
        return readResponse((Type) cls, inputStream);
    }

    public Object readResponse(Type type, InputStream inputStream) {
        JsonNode readTree = this.mapper.readTree(new NoCloseInputStream(inputStream));
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "JSON-PRC Response: " + readTree.toString());
        }
        if (!readTree.isObject()) {
            throw new JsonRpcClientException(0, "Invalid JSON-RPC response", readTree);
        }
        ObjectNode cast = ObjectNode.class.cast(readTree);
        if (this.requestListener != null) {
            this.requestListener.onBeforeResponseProcessed(this, cast);
        }
        if (!cast.has(PlayerListener.ERROR) || cast.get(PlayerListener.ERROR) == null || cast.get(PlayerListener.ERROR).isNull()) {
            if (!cast.has("result") || cast.get("result").isNull() || cast.get("result") == null) {
                return null;
            }
            if (type == null) {
                LOGGER.warning("Server returned result but returnType is null");
                return null;
            }
            return this.mapper.readValue(this.mapper.treeAsTokens(cast.get("result")), TypeFactory.defaultInstance().constructType(type));
        } else if (this.exceptionResolver == null) {
            throw DefaultExceptionResolver.INSTANCE.resolveException(cast);
        } else {
            throw this.exceptionResolver.resolveException(cast);
        }
    }

    public void setExceptionResolver(ExceptionResolver exceptionResolver2) {
        this.exceptionResolver = exceptionResolver2;
    }

    public void setRequestListener(RequestListener requestListener2) {
        this.requestListener = requestListener2;
    }

    public void writeNotification(String str, Object obj, OutputStream outputStream) {
        internalWriteRequest(str, obj, outputStream, null);
    }

    public void writeRequest(String str, Object obj, OutputStream outputStream, String str2) {
        internalWriteRequest(str, obj, outputStream, str2);
    }
}
