package com.googlecode.jsonrpc4j;

import com.a.a.c.h;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.microedition.media.PlayerListener;
import javax.net.ssl.SSLContext;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.concurrent.FutureCallback;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.DefaultConnectionReuseStrategy;
import org.apache.http.impl.nio.DefaultHttpClientIODispatch;
import org.apache.http.impl.nio.pool.BasicNIOConnFactory;
import org.apache.http.impl.nio.pool.BasicNIOConnPool;
import org.apache.http.impl.nio.reactor.DefaultConnectingIOReactor;
import org.apache.http.impl.nio.reactor.IOReactorConfig;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;
import org.apache.http.nio.protocol.BasicAsyncRequestProducer;
import org.apache.http.nio.protocol.BasicAsyncResponseConsumer;
import org.apache.http.nio.protocol.HttpAsyncRequestExecutor;
import org.apache.http.nio.protocol.HttpAsyncRequester;
import org.apache.http.nio.reactor.IOReactorException;
import org.apache.http.nio.reactor.ssl.SSLSetupHandler;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.ImmutableHttpProcessor;
import org.apache.http.protocol.RequestConnControl;
import org.apache.http.protocol.RequestContent;
import org.apache.http.protocol.RequestExpectContinue;
import org.apache.http.protocol.RequestTargetHost;
import org.apache.http.protocol.RequestUserAgent;

public class JsonRpcHttpAsyncClient {
    private static final String JSON_RPC_VERSION = "2.0";
    private static final Logger LOGGER = Logger.getLogger(JsonRpcHttpAsyncClient.class.getName());
    private static AtomicBoolean initialized = new AtomicBoolean();
    private static AtomicLong nextId = new AtomicLong();
    private static BasicNIOConnPool pool;
    private static HttpAsyncRequester requester;
    /* access modifiers changed from: private */
    public static SSLContext sslContext;
    private ExceptionResolver exceptionResolver;
    private Map<String, String> headers;
    private ObjectMapper mapper;
    private URL serviceUrl;

    private class JsonRpcFuture<T> implements JsonRpcCallback<T>, Future<T> {
        private boolean done;
        private ExecutionException exception;
        private T object;

        private JsonRpcFuture() {
        }

        public boolean cancel(boolean z) {
            return false;
        }

        public synchronized T get() {
            while (!this.done) {
                wait();
            }
            if (this.exception != null) {
                throw this.exception;
            }
            return this.object;
        }

        public synchronized T get(long j, TimeUnit timeUnit) {
            while (!this.done) {
                wait(timeUnit.toMillis(j));
            }
            if (this.exception != null) {
                throw this.exception;
            }
            return this.object;
        }

        public boolean isCancelled() {
            return false;
        }

        public synchronized boolean isDone() {
            return this.done;
        }

        public synchronized void onComplete(T t) {
            this.object = t;
            this.done = true;
            notify();
        }

        public synchronized void onError(Throwable th) {
            this.exception = new ExecutionException(th);
            this.done = true;
            notify();
        }
    }

    private class RequestAsyncFuture<T> implements FutureCallback<HttpResponse> {
        private JsonRpcCallback<T> callBack;
        private Class<T> type;

        RequestAsyncFuture(Class<T> cls, JsonRpcCallback<T> jsonRpcCallback) {
            this.type = cls;
            this.callBack = jsonRpcCallback;
        }

        public void cancelled() {
            this.callBack.onError(new RuntimeException("HTTP Request was cancelled"));
        }

        /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void completed(org.apache.http.HttpResponse r6) {
            /*
                r5 = this;
                org.apache.http.StatusLine r0 = r6.getStatusLine()     // Catch:{ Throwable -> 0x002d }
                int r0 = r0.getStatusCode()     // Catch:{ Throwable -> 0x002d }
                r1 = 200(0xc8, float:2.8E-43)
                if (r0 != r1) goto L_0x0034
                org.apache.http.HttpEntity r0 = r6.getEntity()     // Catch:{ Throwable -> 0x002d }
                java.io.InputStream r0 = r0.getContent()     // Catch:{ Exception -> 0x0028 }
                com.googlecode.jsonrpc4j.JsonRpcCallback<T> r1 = r5.callBack     // Catch:{ Throwable -> 0x002d }
                java.lang.Class<T> r2 = r5.type     // Catch:{ Throwable -> 0x002d }
                com.googlecode.jsonrpc4j.JsonRpcHttpAsyncClient r3 = com.googlecode.jsonrpc4j.JsonRpcHttpAsyncClient.this     // Catch:{ Throwable -> 0x002d }
                java.lang.Class<T> r4 = r5.type     // Catch:{ Throwable -> 0x002d }
                java.lang.Object r0 = r3.readResponse(r4, r0)     // Catch:{ Throwable -> 0x002d }
                java.lang.Object r0 = r2.cast(r0)     // Catch:{ Throwable -> 0x002d }
                r1.onComplete(r0)     // Catch:{ Throwable -> 0x002d }
            L_0x0027:
                return
            L_0x0028:
                r0 = move-exception
                r5.failed(r0)     // Catch:{ Throwable -> 0x002d }
                goto L_0x0027
            L_0x002d:
                r0 = move-exception
                com.googlecode.jsonrpc4j.JsonRpcCallback<T> r1 = r5.callBack
                r1.onError(r0)
                goto L_0x0027
            L_0x0034:
                com.googlecode.jsonrpc4j.JsonRpcCallback<T> r1 = r5.callBack     // Catch:{ Throwable -> 0x002d }
                java.lang.RuntimeException r2 = new java.lang.RuntimeException     // Catch:{ Throwable -> 0x002d }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Throwable -> 0x002d }
                r3.<init>()     // Catch:{ Throwable -> 0x002d }
                java.lang.String r4 = "Unexpected response code: "
                java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Throwable -> 0x002d }
                java.lang.StringBuilder r0 = r3.append(r0)     // Catch:{ Throwable -> 0x002d }
                java.lang.String r0 = r0.toString()     // Catch:{ Throwable -> 0x002d }
                r2.<init>(r0)     // Catch:{ Throwable -> 0x002d }
                r1.onError(r2)     // Catch:{ Throwable -> 0x002d }
                goto L_0x0027
            */
            throw new UnsupportedOperationException("Method not decompiled: com.googlecode.jsonrpc4j.JsonRpcHttpAsyncClient.RequestAsyncFuture.completed(org.apache.http.HttpResponse):void");
        }

        public void failed(Exception exc) {
            this.callBack.onError(exc);
        }
    }

    public JsonRpcHttpAsyncClient(ObjectMapper objectMapper, URL url, Map<String, String> map) {
        this.exceptionResolver = DefaultExceptionResolver.INSTANCE;
        this.headers = new HashMap();
        initialize();
        this.mapper = objectMapper;
        this.serviceUrl = url;
        this.headers.putAll(map);
    }

    public JsonRpcHttpAsyncClient(URL url) {
        this(new ObjectMapper(), url, new HashMap());
    }

    public JsonRpcHttpAsyncClient(URL url, Map<String, String> map) {
        this(new ObjectMapper(), url, map);
    }

    private void addHeaders(HttpRequest httpRequest, Map<String, String> map) {
        for (String next : map.keySet()) {
            httpRequest.addHeader(next, map.get(next));
        }
    }

    private <T> Future<T> doInvoke(String str, Object obj, Class<T> cls, Map<String, String> map, JsonRpcCallback<T> jsonRpcCallback) {
        String str2 = this.serviceUrl.getPath() + (this.serviceUrl.getQuery() != null ? "?" + this.serviceUrl.getQuery() : "");
        int port = this.serviceUrl.getPort() != -1 ? this.serviceUrl.getPort() : this.serviceUrl.getDefaultPort();
        BasicHttpEntityEnclosingRequest basicHttpEntityEnclosingRequest = new BasicHttpEntityEnclosingRequest(h.POST, str2);
        addHeaders(basicHttpEntityEnclosingRequest, this.headers);
        addHeaders(basicHttpEntityEnclosingRequest, map);
        try {
            writeRequest(str, obj, basicHttpEntityEnclosingRequest);
        } catch (IOException e) {
            jsonRpcCallback.onError(e);
        }
        requester.execute(new BasicAsyncRequestProducer(new HttpHost(this.serviceUrl.getHost(), port, this.serviceUrl.getProtocol()), basicHttpEntityEnclosingRequest), new BasicAsyncResponseConsumer(), pool, new BasicHttpContext(), new RequestAsyncFuture(cls, jsonRpcCallback));
        if (jsonRpcCallback instanceof JsonRpcFuture) {
            return (Future) jsonRpcCallback;
        }
        return null;
    }

    private void initialize() {
        if (!initialized.getAndSet(true)) {
            final BasicHttpParams basicHttpParams = new BasicHttpParams();
            basicHttpParams.setIntParameter("http.socket.timeout", Integer.getInteger("com.googlecode.jsonrpc4j.async.socket.timeout", 30000).intValue());
            basicHttpParams.setIntParameter("http.connection.timeout", Integer.getInteger("com.googlecode.jsonrpc4j.async.connect.timeout", 30000).intValue());
            basicHttpParams.setIntParameter("http.socket.buffer-size", Integer.getInteger("com.googlecode.jsonrpc4j.async.socket.buffer", 8192).intValue());
            basicHttpParams.setBooleanParameter("http.tcp.nodelay", Boolean.valueOf(System.getProperty("com.googlecode.jsonrpc4j.async.tcp.nodelay", "true")).booleanValue());
            basicHttpParams.setParameter("http.useragent", "jsonrpc4j/1.0");
            try {
                IOReactorConfig iOReactorConfig = new IOReactorConfig();
                iOReactorConfig.setIoThreadCount(Integer.getInteger("com.googlecode.jsonrpc4j.async.reactor.threads", 1).intValue());
                final DefaultConnectingIOReactor defaultConnectingIOReactor = new DefaultConnectingIOReactor(iOReactorConfig);
                if (sslContext == null) {
                    try {
                        sslContext = SSLContext.getDefault();
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
                pool = new BasicNIOConnPool(defaultConnectingIOReactor, new BasicNIOConnFactory(sslContext, (SSLSetupHandler) null, basicHttpParams), basicHttpParams);
                pool.setDefaultMaxPerRoute(Integer.getInteger("com.googlecode.jsonrpc4j.async.max.inflight.route", (int) h.HTTP_INTERNAL_ERROR).intValue());
                pool.setMaxTotal(Integer.getInteger("com.googlecode.jsonrpc4j.async.max.inflight.total", (int) h.HTTP_INTERNAL_ERROR).intValue());
                Thread thread = new Thread(new Runnable() {
                    public void run() {
                        try {
                            defaultConnectingIOReactor.execute(new DefaultHttpClientIODispatch(new HttpAsyncRequestExecutor(), JsonRpcHttpAsyncClient.sslContext, basicHttpParams));
                        } catch (InterruptedIOException e) {
                            System.err.println("Interrupted");
                        } catch (IOException e2) {
                            System.err.println("I/O error: " + e2.getMessage());
                        }
                    }
                }, "jsonrpc4j HTTP IOReactor");
                thread.setDaemon(true);
                thread.start();
                requester = new HttpAsyncRequester(new ImmutableHttpProcessor(new HttpRequestInterceptor[]{new RequestContent(), new RequestTargetHost(), new RequestConnControl(), new RequestUserAgent(), new RequestExpectContinue()}), new DefaultConnectionReuseStrategy(), basicHttpParams);
            } catch (IOReactorException e2) {
                throw new RuntimeException("Exception initializing asynchronous Apache HTTP Client", e2);
            }
        }
    }

    private <T> void invoke(String str, Object obj, Class<T> cls, Map<String, String> map, JsonRpcCallback<T> jsonRpcCallback) {
        doInvoke(str, obj, cls, map, jsonRpcCallback);
    }

    /* access modifiers changed from: private */
    public <T> T readResponse(Type type, InputStream inputStream) {
        JsonNode readTree = this.mapper.readTree(new NoCloseInputStream(inputStream));
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "JSON-PRC Response: " + readTree.toString());
        }
        if (!readTree.isObject()) {
            throw new JsonRpcClientException(0, "Invalid JSON-RPC response", readTree);
        }
        ObjectNode cast = ObjectNode.class.cast(readTree);
        if (!cast.has(PlayerListener.ERROR) || cast.get(PlayerListener.ERROR) == null || cast.get(PlayerListener.ERROR).isNull()) {
            if (!cast.has("result") || cast.get("result").isNull() || cast.get("result") == null) {
                return null;
            }
            return this.mapper.readValue(this.mapper.treeAsTokens(cast.get("result")), TypeFactory.defaultInstance().constructType(type));
        } else if (this.exceptionResolver == null) {
            throw DefaultExceptionResolver.INSTANCE.resolveException(cast);
        } else {
            throw this.exceptionResolver.resolveException(cast);
        }
    }

    public static void setSSLContext(SSLContext sSLContext) {
        sslContext = sSLContext;
    }

    private void writeRequest(String str, Object obj, HttpRequest httpRequest) {
        ObjectNode createObjectNode = this.mapper.createObjectNode();
        createObjectNode.put("id", nextId.getAndIncrement());
        createObjectNode.put("jsonrpc", JSON_RPC_VERSION);
        createObjectNode.put("method", str);
        if (obj == null || !obj.getClass().isArray()) {
            if (obj == null || !Collection.class.isInstance(obj)) {
                if (obj == null || !Map.class.isInstance(obj)) {
                    if (obj != null) {
                        createObjectNode.put("params", this.mapper.valueToTree(obj));
                    }
                } else if (!Map.class.cast(obj).isEmpty()) {
                    createObjectNode.put("params", this.mapper.valueToTree(obj));
                }
            } else if (!Collection.class.cast(obj).isEmpty()) {
                createObjectNode.put("params", this.mapper.valueToTree(obj));
            }
        } else if (Object[].class.cast(obj).length > 0) {
            createObjectNode.put("params", this.mapper.valueToTree(Object[].class.cast(obj)));
        }
        if (LOGGER.isLoggable(Level.FINE)) {
            LOGGER.log(Level.FINE, "JSON-PRC Request: " + createObjectNode.toString());
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(512);
        this.mapper.writeValue(byteArrayOutputStream, createObjectNode);
        HttpEntityEnclosingRequest httpEntityEnclosingRequest = (HttpEntityEnclosingRequest) httpRequest;
        httpEntityEnclosingRequest.setEntity(httpEntityEnclosingRequest.getFirstHeader("Content-Type") == null ? new ByteArrayEntity(byteArrayOutputStream.toByteArray(), ContentType.APPLICATION_JSON) : new ByteArrayEntity(byteArrayOutputStream.toByteArray()));
    }

    public Future<Object> invoke(String str, Object obj) {
        return invoke(str, obj, Object.class, new HashMap());
    }

    public <T> Future<T> invoke(String str, Object obj, Class<T> cls) {
        return invoke(str, obj, cls, new HashMap());
    }

    public <T> Future<T> invoke(String str, Object obj, Class<T> cls, Map<String, String> map) {
        return doInvoke(str, obj, cls, map, new JsonRpcFuture());
    }

    public void invoke(String str, Object obj, JsonRpcCallback<Object> jsonRpcCallback) {
        invoke(str, obj, Object.class, new HashMap(), jsonRpcCallback);
    }

    public <T> void invoke(String str, Object obj, Class<T> cls, JsonRpcCallback<T> jsonRpcCallback) {
        invoke(str, obj, cls, new HashMap(), jsonRpcCallback);
    }
}
