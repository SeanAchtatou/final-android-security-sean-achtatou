package com.googlecode.jsonrpc4j.spring;

import com.googlecode.jsonrpc4j.JsonRpcServer;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.HttpRequestHandler;

public class JsonServiceExporter extends AbstractJsonServiceExporter implements HttpRequestHandler {
    private JsonRpcServer jsonRpcServer;

    /* access modifiers changed from: protected */
    public void exportService() {
        this.jsonRpcServer = getJsonRpcServer();
    }

    public void handleRequest(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        this.jsonRpcServer.handle(httpServletRequest, httpServletResponse);
        httpServletResponse.getOutputStream().flush();
    }
}
