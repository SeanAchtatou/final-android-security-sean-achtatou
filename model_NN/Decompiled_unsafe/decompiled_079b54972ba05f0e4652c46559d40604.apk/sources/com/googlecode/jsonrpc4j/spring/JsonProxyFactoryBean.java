package com.googlecode.jsonrpc4j.spring;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.googlecode.jsonrpc4j.JsonRpcClient;
import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import com.googlecode.jsonrpc4j.ReflectionUtil;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.remoting.support.UrlBasedRemoteAccessor;

public class JsonProxyFactoryBean extends UrlBasedRemoteAccessor implements MethodInterceptor, FactoryBean<Object>, InitializingBean, ApplicationContextAware {
    private ApplicationContext applicationContext;
    private Map<String, String> extraHttpHeaders = new HashMap();
    private JsonRpcHttpClient jsonRpcHttpClient = null;
    private ObjectMapper objectMapper = null;
    private Object proxyObject = null;
    private JsonRpcClient.RequestListener requestListener = null;
    private boolean useNamedParams = false;

    public void afterPropertiesSet() {
        JsonProxyFactoryBean.super.afterPropertiesSet();
        this.proxyObject = ProxyFactory.getProxy(getServiceInterface(), this);
        if (this.objectMapper == null && this.applicationContext != null && this.applicationContext.containsBean("objectMapper")) {
            this.objectMapper = (ObjectMapper) this.applicationContext.getBean("objectMapper");
        }
        if (this.objectMapper == null && this.applicationContext != null) {
            try {
                this.objectMapper = (ObjectMapper) BeanFactoryUtils.beanOfTypeIncludingAncestors(this.applicationContext, ObjectMapper.class);
            } catch (Exception e) {
            }
        }
        if (this.objectMapper == null) {
            this.objectMapper = new ObjectMapper();
        }
        try {
            this.jsonRpcHttpClient = new JsonRpcHttpClient(this.objectMapper, new URL(getServiceUrl()), this.extraHttpHeaders);
            this.jsonRpcHttpClient.setRequestListener(this.requestListener);
        } catch (MalformedURLException e2) {
            throw new RuntimeException(e2);
        }
    }

    public Object getObject() {
        return this.proxyObject;
    }

    public Class<?> getObjectType() {
        return getServiceInterface();
    }

    public Object invoke(MethodInvocation methodInvocation) {
        Method method = methodInvocation.getMethod();
        if (method.getDeclaringClass() == Object.class && method.getName().equals("toString")) {
            return this.proxyObject.getClass().getName() + "@" + System.identityHashCode(this.proxyObject);
        }
        return this.jsonRpcHttpClient.invoke(methodInvocation.getMethod().getName(), ReflectionUtil.parseArguments(methodInvocation.getMethod(), methodInvocation.getArguments(), this.useNamedParams), methodInvocation.getMethod().getGenericReturnType() != null ? methodInvocation.getMethod().getGenericReturnType() : methodInvocation.getMethod().getReturnType(), this.extraHttpHeaders);
    }

    public boolean isSingleton() {
        return true;
    }

    public void setApplicationContext(ApplicationContext applicationContext2) {
        this.applicationContext = applicationContext2;
    }

    public void setExtraHttpHeaders(Map<String, String> map) {
        this.extraHttpHeaders = map;
    }

    public void setObjectMapper(ObjectMapper objectMapper2) {
        this.objectMapper = objectMapper2;
    }

    public void setRequestListener(JsonRpcClient.RequestListener requestListener2) {
        this.requestListener = requestListener2;
    }

    public void setUseNamedParams(boolean z) {
        this.useNamedParams = z;
    }
}
