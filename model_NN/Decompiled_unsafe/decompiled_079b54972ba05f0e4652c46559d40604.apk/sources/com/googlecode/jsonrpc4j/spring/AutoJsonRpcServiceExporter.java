package com.googlecode.jsonrpc4j.spring;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.googlecode.jsonrpc4j.ErrorResolver;
import com.googlecode.jsonrpc4j.JsonRpcService;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.util.ClassUtils;

public class AutoJsonRpcServiceExporter implements BeanFactoryPostProcessor {
    private static final Logger LOG = Logger.getLogger(AutoJsonRpcServiceExporter.class.getName());
    private static final String PATH_PREFIX = "/";
    private boolean allowExtraParams = false;
    private boolean allowLessParams = false;
    private boolean backwardsComaptible = true;
    private ErrorResolver errorResolver = null;
    private ObjectMapper objectMapper;
    private boolean rethrowExceptions = false;
    private Map<String, String> serviceBeanNames = new HashMap();

    private BeanDefinition findBeanDefintion(ConfigurableListableBeanFactory configurableListableBeanFactory, String str) {
        if (configurableListableBeanFactory.containsLocalBean(str)) {
            return configurableListableBeanFactory.getBeanDefinition(str);
        }
        ConfigurableListableBeanFactory parentBeanFactory = configurableListableBeanFactory.getParentBeanFactory();
        if (parentBeanFactory != null && ConfigurableListableBeanFactory.class.isInstance(parentBeanFactory)) {
            return findBeanDefintion(parentBeanFactory, str);
        }
        throw new RuntimeException(String.format("Bean with name '%s' can no longer be found.", str));
    }

    private void findServiceBeanDefinitions(ConfigurableListableBeanFactory configurableListableBeanFactory) {
        for (String str : configurableListableBeanFactory.getBeanDefinitionNames()) {
            JsonRpcService jsonRpcService = (JsonRpcService) configurableListableBeanFactory.findAnnotationOnBean(str, JsonRpcService.class);
            if (jsonRpcService != null) {
                String value = jsonRpcService.value();
                LOG.fine(String.format("Found JSON-RPC path '%s' for bean [%s].", value, str));
                if (this.serviceBeanNames.containsKey(value)) {
                    LOG.warning(String.format("Duplicate JSON-RPC path specification: found %s on both [%s] and [%s].", value, str, this.serviceBeanNames.get(value)));
                }
                this.serviceBeanNames.put(value, str);
            }
        }
        ConfigurableListableBeanFactory parentBeanFactory = configurableListableBeanFactory.getParentBeanFactory();
        if (parentBeanFactory != null && ConfigurableListableBeanFactory.class.isInstance(parentBeanFactory)) {
            findServiceBeanDefinitions(parentBeanFactory);
        }
    }

    private Class<?>[] getBeanInterfaces(BeanDefinition beanDefinition, ClassLoader classLoader) {
        String beanClassName = beanDefinition.getBeanClassName();
        try {
            return ClassUtils.getAllInterfacesForClass(ClassUtils.forName(beanClassName, classLoader), classLoader);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(String.format("Cannot find bean class '%s'.", beanClassName), e);
        } catch (LinkageError e2) {
            throw new RuntimeException(String.format("Cannot find bean class '%s'.", beanClassName), e2);
        }
    }

    private String makeUrlPath(String str) {
        return PATH_PREFIX.concat(str);
    }

    private void registerServiceProxy(DefaultListableBeanFactory defaultListableBeanFactory, String str, String str2) {
        BeanDefinitionBuilder addPropertyReference = BeanDefinitionBuilder.rootBeanDefinition(JsonServiceExporter.class).addPropertyReference("service", str2);
        Class<?>[] beanInterfaces = getBeanInterfaces(findBeanDefintion(defaultListableBeanFactory, str2), defaultListableBeanFactory.getBeanClassLoader());
        int length = beanInterfaces.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            Class<?> cls = beanInterfaces[i];
            if (cls.isAnnotationPresent(JsonRpcService.class)) {
                String name = cls.getName();
                LOG.fine(String.format("Registering interface '%s' for JSON-RPC bean [%s].", name, str2));
                addPropertyReference.addPropertyValue("serviceInterface", name);
                break;
            }
            i++;
        }
        if (this.objectMapper != null) {
            addPropertyReference.addPropertyValue("objectMapper", this.objectMapper);
        }
        if (this.errorResolver != null) {
            addPropertyReference.addPropertyValue("errorResolver", this.errorResolver);
        }
        addPropertyReference.addPropertyValue("backwardsComaptible", Boolean.valueOf(this.backwardsComaptible));
        addPropertyReference.addPropertyValue("rethrowExceptions", Boolean.valueOf(this.rethrowExceptions));
        addPropertyReference.addPropertyValue("allowExtraParams", Boolean.valueOf(this.allowExtraParams));
        addPropertyReference.addPropertyValue("allowLessParams", Boolean.valueOf(this.allowLessParams));
        defaultListableBeanFactory.registerBeanDefinition(str, addPropertyReference.getBeanDefinition());
    }

    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) {
        DefaultListableBeanFactory defaultListableBeanFactory = (DefaultListableBeanFactory) configurableListableBeanFactory;
        findServiceBeanDefinitions(defaultListableBeanFactory);
        for (Map.Entry next : this.serviceBeanNames.entrySet()) {
            registerServiceProxy(defaultListableBeanFactory, makeUrlPath((String) next.getKey()), (String) next.getValue());
        }
    }

    public void setAllowExtraParams(boolean z) {
        this.allowExtraParams = z;
    }

    public void setAllowLessParams(boolean z) {
        this.allowLessParams = z;
    }

    public void setBackwardsComaptible(boolean z) {
        this.backwardsComaptible = z;
    }

    public void setErrorResolver(ErrorResolver errorResolver2) {
        this.errorResolver = errorResolver2;
    }

    public void setObjectMapper(ObjectMapper objectMapper2) {
        this.objectMapper = objectMapper2;
    }

    public void setRethrowExceptions(boolean z) {
        this.rethrowExceptions = z;
    }
}
