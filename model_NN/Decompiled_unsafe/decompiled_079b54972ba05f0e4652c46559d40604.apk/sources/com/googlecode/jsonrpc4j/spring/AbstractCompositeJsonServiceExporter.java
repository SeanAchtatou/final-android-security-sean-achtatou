package com.googlecode.jsonrpc4j.spring;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.googlecode.jsonrpc4j.ErrorResolver;
import com.googlecode.jsonrpc4j.JsonRpcServer;
import com.googlecode.jsonrpc4j.ProxyUtil;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public abstract class AbstractCompositeJsonServiceExporter implements InitializingBean, ApplicationContextAware {
    private boolean allowExtraParams = false;
    private boolean allowLessParams = false;
    private boolean allowMultipleInheritance = false;
    private ApplicationContext applicationContext;
    private boolean backwardsComaptible = true;
    private ErrorResolver errorResolver = null;
    private JsonRpcServer jsonRpcServer;
    private ObjectMapper objectMapper;
    private boolean rethrowExceptions = false;
    private Class<?>[] serviceInterfaces = null;
    private Object[] services = new Object[0];

    public final void afterPropertiesSet() {
        if (this.objectMapper == null && this.applicationContext != null && this.applicationContext.containsBean("objectMapper")) {
            this.objectMapper = (ObjectMapper) this.applicationContext.getBean("objectMapper");
        }
        if (this.objectMapper == null && this.applicationContext != null) {
            try {
                this.objectMapper = (ObjectMapper) BeanFactoryUtils.beanOfTypeIncludingAncestors(this.applicationContext, ObjectMapper.class);
            } catch (Exception e) {
            }
        }
        if (this.objectMapper == null) {
            this.objectMapper = new ObjectMapper();
        }
        this.jsonRpcServer = new JsonRpcServer(this.objectMapper, ProxyUtil.createCompositeServiceProxy(getClass().getClassLoader(), this.services, this.serviceInterfaces, this.allowMultipleInheritance));
        this.jsonRpcServer.setErrorResolver(this.errorResolver);
        this.jsonRpcServer.setBackwardsComaptible(this.backwardsComaptible);
        this.jsonRpcServer.setRethrowExceptions(this.rethrowExceptions);
        this.jsonRpcServer.setAllowExtraParams(this.allowExtraParams);
        this.jsonRpcServer.setAllowLessParams(this.allowLessParams);
        exportService();
    }

    /* access modifiers changed from: protected */
    public void exportService() {
    }

    /* access modifiers changed from: protected */
    public JsonRpcServer getJsonRpcServer() {
        return this.jsonRpcServer;
    }

    public void setAllowExtraParams(boolean z) {
        this.allowExtraParams = z;
    }

    public void setAllowLessParams(boolean z) {
        this.allowLessParams = z;
    }

    public void setAllowMultipleInheritance(boolean z) {
        this.allowMultipleInheritance = z;
    }

    public void setApplicationContext(ApplicationContext applicationContext2) {
        this.applicationContext = applicationContext2;
    }

    public void setBackwardsComaptible(boolean z) {
        this.backwardsComaptible = z;
    }

    public void setErrorResolver(ErrorResolver errorResolver2) {
        this.errorResolver = errorResolver2;
    }

    public void setObjectMapper(ObjectMapper objectMapper2) {
        this.objectMapper = objectMapper2;
    }

    public void setServiceInterfaces(Class<?>[] clsArr) {
        this.serviceInterfaces = clsArr;
    }

    public void setServices(Object[] objArr) {
        this.services = objArr;
    }
}
