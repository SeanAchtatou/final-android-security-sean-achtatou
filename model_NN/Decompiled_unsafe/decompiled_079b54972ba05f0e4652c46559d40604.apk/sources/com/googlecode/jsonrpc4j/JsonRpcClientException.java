package com.googlecode.jsonrpc4j;

import com.fasterxml.jackson.databind.JsonNode;

public class JsonRpcClientException extends RuntimeException {
    private int code;
    private JsonNode data;

    public JsonRpcClientException(int i, String str, JsonNode jsonNode) {
        super(str);
        this.code = i;
        this.data = jsonNode;
    }

    public int getCode() {
        return this.code;
    }

    public JsonNode getData() {
        return this.data;
    }
}
