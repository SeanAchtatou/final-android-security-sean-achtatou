package com.googlecode.jsonrpc4j;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JsonRpcMultiServer extends JsonRpcServer {
    private static final Logger LOGGER = Logger.getLogger(JsonRpcMultiServer.class.getName());
    private Map<String, Object> handlerMap;
    private Map<String, Class<?>> interfaceMap;

    public JsonRpcMultiServer() {
        this(new ObjectMapper());
    }

    public JsonRpcMultiServer(ObjectMapper objectMapper) {
        super(objectMapper, (Object) null);
        this.handlerMap = new HashMap();
        this.interfaceMap = new HashMap();
    }

    public JsonRpcMultiServer addService(String str, Object obj) {
        return addService(str, obj, null);
    }

    public JsonRpcMultiServer addService(String str, Object obj, Class<?> cls) {
        this.handlerMap.put(str, obj);
        if (cls != null) {
            this.interfaceMap.put(str, cls);
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public Object getHandler(String str) {
        Object obj = this.handlerMap.get(str);
        if (obj != null) {
            return obj;
        }
        LOGGER.log(Level.SEVERE, "Service '" + str + "' is not registered in this multi-server");
        throw new RuntimeException("Service '" + str + "' does not exist");
    }

    /* access modifiers changed from: protected */
    public Class<?>[] getHandlerInterfaces(String str) {
        Class<?> cls = this.interfaceMap.get(str);
        if (cls != null) {
            return new Class[]{cls};
        } else if (Proxy.isProxyClass(getHandler(str).getClass())) {
            return getHandler(str).getClass().getInterfaces();
        } else {
            return new Class[]{getHandler(str).getClass()};
        }
    }

    /* access modifiers changed from: protected */
    public String getMethodName(JsonNode jsonNode) {
        int indexOf;
        String asText = (jsonNode == null || jsonNode.isNull()) ? null : jsonNode.asText();
        return (asText == null || (indexOf = asText.indexOf(46)) <= 0) ? asText : asText.substring(indexOf + 1);
    }

    /* access modifiers changed from: protected */
    public String getServiceName(JsonNode jsonNode) {
        int indexOf;
        String asText = (jsonNode == null || jsonNode.isNull()) ? null : jsonNode.asText();
        return (asText == null || (indexOf = asText.indexOf(46)) <= 0) ? asText : asText.substring(0, indexOf);
    }
}
