package com.googlecode.jsonrpc4j;

import com.fasterxml.jackson.databind.JsonNode;
import com.googlecode.jsonrpc4j.ErrorResolver;
import java.lang.reflect.Method;
import java.util.List;

public class DefaultErrorResolver implements ErrorResolver {
    public static final DefaultErrorResolver INSTANCE = new DefaultErrorResolver();

    public static class ErrorData {
        private String exceptionTypeName;
        private String message;

        public ErrorData(String str, String str2) {
            this.exceptionTypeName = str;
            this.message = str2;
        }

        public String getExceptionTypeName() {
            return this.exceptionTypeName;
        }

        public String getMessage() {
            return this.message;
        }
    }

    public ErrorResolver.JsonError resolveError(Throwable th, Method method, List<JsonNode> list) {
        return new ErrorResolver.JsonError(0, th.getMessage(), new ErrorData(th.getClass().getName(), th.getMessage()));
    }
}
