package com.googlecode.jsonrpc4j;

import java.io.InputStream;

public class NoCloseInputStream extends InputStream {
    private boolean closeAttempted = false;
    private InputStream ips;

    public NoCloseInputStream(InputStream inputStream) {
        this.ips = inputStream;
    }

    public int available() {
        return this.ips.available();
    }

    public void close() {
        this.closeAttempted = true;
    }

    public synchronized void mark(int i) {
        this.ips.mark(i);
    }

    public boolean markSupported() {
        return this.ips.markSupported();
    }

    public int read() {
        return this.ips.read();
    }

    public int read(byte[] bArr) {
        return this.ips.read(bArr);
    }

    public int read(byte[] bArr, int i, int i2) {
        return this.ips.read(bArr, i, i2);
    }

    public synchronized void reset() {
        this.ips.reset();
    }

    public long skip(long j) {
        return this.ips.skip(j);
    }

    public boolean wasCloseAttempted() {
        return this.closeAttempted;
    }
}
