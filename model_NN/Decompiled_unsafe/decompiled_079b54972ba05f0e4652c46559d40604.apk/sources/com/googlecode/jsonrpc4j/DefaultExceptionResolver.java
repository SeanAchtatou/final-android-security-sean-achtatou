package com.googlecode.jsonrpc4j;

import com.fasterxml.jackson.databind.node.ObjectNode;
import java.lang.reflect.Constructor;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.microedition.media.PlayerListener;

public class DefaultExceptionResolver implements ExceptionResolver {
    public static final DefaultExceptionResolver INSTANCE = new DefaultExceptionResolver();
    private static final Logger LOGGER = Logger.getLogger(DefaultExceptionResolver.class.getName());

    private JsonRpcClientException createJsonRpcClientException(ObjectNode objectNode) {
        return new JsonRpcClientException(objectNode.get("code").asInt(), objectNode.get("message").asText(), objectNode.get("data"));
    }

    private Throwable createThrowable(String str, String str2) {
        Constructor<?> constructor;
        Constructor<?> constructor2;
        try {
            Class<?> cls = Class.forName(str);
            if (!Throwable.class.isAssignableFrom(cls)) {
                LOGGER.warning("Type does not inherit from Throwable" + cls.getName());
                return null;
            }
            try {
                constructor = cls.getConstructor(new Class[0]);
            } catch (Throwable th) {
                constructor = null;
            }
            try {
                constructor2 = cls.getConstructor(String.class);
            } catch (Throwable th2) {
                constructor2 = null;
            }
            if (str2 != null && constructor2 != null) {
                return (Throwable) constructor2.newInstance(str2);
            } else if (str2 != null && constructor != null) {
                LOGGER.warning("Unable to invoke message constructor for " + cls.getName());
                return (Throwable) constructor.newInstance(new Object[0]);
            } else if (str2 == null && constructor != null) {
                return (Throwable) constructor.newInstance(new Object[0]);
            } else {
                if (str2 != null || constructor2 == null) {
                    LOGGER.warning("Unable to find a suitable constructor for " + cls.getName());
                    return null;
                }
                LOGGER.warning("Passing null message to message constructor for " + cls.getName());
                return (Throwable) constructor2.newInstance(null);
            }
        } catch (Exception e) {
            LOGGER.warning("Unable to load Throwable class " + str);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public Throwable resolveException(ObjectNode objectNode) {
        Throwable th;
        ObjectNode cast = ObjectNode.class.cast(objectNode.get(PlayerListener.ERROR));
        if (!cast.has("data") || cast.get("data").isNull() || !cast.get("data").isObject()) {
            return createJsonRpcClientException(cast);
        }
        ObjectNode cast2 = ObjectNode.class.cast(cast.get("data"));
        if (!cast2.has("exceptionTypeName") || cast2.get("exceptionTypeName") == null || cast2.get("exceptionTypeName").isNull() || !cast2.get("exceptionTypeName").isTextual()) {
            return createJsonRpcClientException(cast);
        }
        try {
            th = createThrowable(cast2.get("exceptionTypeName").asText(), (!cast2.has("message") || !cast2.get("message").isTextual()) ? null : cast2.get("message").asText());
        } catch (Exception e) {
            LOGGER.log(Level.WARNING, "Unable to create throwable", (Throwable) e);
            th = null;
        }
        return th == null ? createJsonRpcClientException(cast) : th;
    }
}
