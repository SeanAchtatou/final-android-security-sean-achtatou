package com.googlecode.jsonrpc4j;

import com.fasterxml.jackson.databind.JsonNode;
import com.googlecode.jsonrpc4j.DefaultErrorResolver;
import com.googlecode.jsonrpc4j.ErrorResolver;
import java.lang.reflect.Method;
import java.util.List;

public class AnnotationsErrorResolver implements ErrorResolver {
    public static final AnnotationsErrorResolver INSTANCE = new AnnotationsErrorResolver();

    public ErrorResolver.JsonError resolveError(Throwable th, Method method, List<JsonNode> list) {
        JsonRpcErrors jsonRpcErrors = (JsonRpcErrors) ReflectionUtil.getAnnotation(method, JsonRpcErrors.class);
        if (jsonRpcErrors != null) {
            for (JsonRpcError jsonRpcError : jsonRpcErrors.value()) {
                if (jsonRpcError.exception().isInstance(th)) {
                    String message = (jsonRpcError.message() == null || jsonRpcError.message().trim().length() <= 0) ? th.getMessage() : jsonRpcError.message();
                    return new ErrorResolver.JsonError(jsonRpcError.code(), message, new DefaultErrorResolver.ErrorData(jsonRpcError.exception().getName(), message));
                }
            }
        }
        return null;
    }
}
