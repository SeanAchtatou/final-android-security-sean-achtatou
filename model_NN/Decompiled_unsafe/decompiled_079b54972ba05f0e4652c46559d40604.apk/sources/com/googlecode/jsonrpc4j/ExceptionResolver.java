package com.googlecode.jsonrpc4j;

import com.fasterxml.jackson.databind.node.ObjectNode;

public interface ExceptionResolver {
    Throwable resolveException(ObjectNode objectNode);
}
