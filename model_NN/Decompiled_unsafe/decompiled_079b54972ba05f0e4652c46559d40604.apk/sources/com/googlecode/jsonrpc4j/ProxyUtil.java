package com.googlecode.jsonrpc4j;

import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.Socket;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class ProxyUtil {
    private static final Logger LOGGER = Logger.getLogger(ProxyUtil.class.getName());

    public static <T> T createClientProxy(ClassLoader classLoader, Class<T> cls, JsonRpcClient jsonRpcClient, Socket socket) {
        return createClientProxy(classLoader, cls, false, jsonRpcClient, socket.getInputStream(), socket.getOutputStream());
    }

    public static <T> T createClientProxy(ClassLoader classLoader, Class<T> cls, JsonRpcHttpClient jsonRpcHttpClient) {
        return createClientProxy(classLoader, cls, false, jsonRpcHttpClient, new HashMap());
    }

    public static <T> T createClientProxy(ClassLoader classLoader, Class<T> cls, final boolean z, final JsonRpcClient jsonRpcClient, final InputStream inputStream, final OutputStream outputStream) {
        return Proxy.newProxyInstance(classLoader, new Class[]{cls}, new InvocationHandler() {
            public Object invoke(Object obj, Method method, Object[] objArr) {
                if (method.getDeclaringClass() == Object.class) {
                    return ProxyUtil.proxyObjectMethods(method, obj, objArr);
                }
                return jsonRpcClient.invokeAndReadResponse(method.getName(), ReflectionUtil.parseArguments(method, objArr, z), method.getGenericReturnType(), outputStream, inputStream);
            }
        });
    }

    public static <T> T createClientProxy(ClassLoader classLoader, Class<T> cls, final boolean z, final JsonRpcHttpClient jsonRpcHttpClient, final Map<String, String> map) {
        return Proxy.newProxyInstance(classLoader, new Class[]{cls}, new InvocationHandler() {
            public Object invoke(Object obj, Method method, Object[] objArr) {
                if (method.getDeclaringClass() == Object.class) {
                    return ProxyUtil.proxyObjectMethods(method, obj, objArr);
                }
                return jsonRpcHttpClient.invoke(method.getName(), ReflectionUtil.parseArguments(method, objArr, z), method.getGenericReturnType(), map);
            }
        });
    }

    public static Object createCompositeServiceProxy(ClassLoader classLoader, Object[] objArr, boolean z) {
        return createCompositeServiceProxy(classLoader, objArr, null, z);
    }

    public static Object createCompositeServiceProxy(ClassLoader classLoader, Object[] objArr, Class<?>[] clsArr, boolean z) {
        HashSet<Class> hashSet = new HashSet<>();
        if (clsArr != null) {
            hashSet.addAll(Arrays.asList(clsArr));
        } else {
            for (Object obj : objArr) {
                hashSet.addAll(Arrays.asList(obj.getClass().getInterfaces()));
            }
        }
        final HashMap hashMap = new HashMap();
        for (Class cls : hashSet) {
            if (!hashMap.containsKey(cls) || !z) {
                if (hashMap.containsKey(cls)) {
                    throw new IllegalArgumentException("Multiple inheritance not allowed " + cls.getName());
                }
                int length = objArr.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    }
                    Object obj2 = objArr[i];
                    if (cls.isInstance(obj2)) {
                        if (LOGGER.isLoggable(Level.FINE)) {
                            LOGGER.fine("Using " + obj2.getClass().getName() + " for " + cls.getName());
                        }
                        hashMap.put(cls, obj2);
                    } else {
                        i++;
                    }
                }
                if (!hashMap.containsKey(cls)) {
                    throw new IllegalArgumentException("None of the provided services implement " + cls.getName());
                }
            }
        }
        return Proxy.newProxyInstance(classLoader, (Class[]) hashSet.toArray(new Class[0]), new InvocationHandler() {
            public Object invoke(Object obj, Method method, Object[] objArr) {
                Class<?> declaringClass = method.getDeclaringClass();
                return declaringClass == Object.class ? ProxyUtil.proxyObjectMethods(method, obj, objArr) : method.invoke(hashMap.get(declaringClass), objArr);
            }
        });
    }

    /* access modifiers changed from: private */
    public static Object proxyObjectMethods(Method method, Object obj, Object[] objArr) {
        boolean z = false;
        String name = method.getName();
        if (name.equals("toString")) {
            return obj.getClass().getName() + "@" + System.identityHashCode(obj);
        }
        if (name.equals("hashCode")) {
            return Integer.valueOf(System.identityHashCode(obj));
        }
        if (name.equals("equals")) {
            if (obj == objArr[0]) {
                z = true;
            }
            return Boolean.valueOf(z);
        }
        throw new RuntimeException(method.getName() + " is not a member of java.lang.Object");
    }
}
