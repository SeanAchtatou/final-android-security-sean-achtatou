package com.googlecode.jsonrpc4j;

public interface JsonRpcCallback<T> {
    void onComplete(T t);

    void onError(Throwable th);
}
