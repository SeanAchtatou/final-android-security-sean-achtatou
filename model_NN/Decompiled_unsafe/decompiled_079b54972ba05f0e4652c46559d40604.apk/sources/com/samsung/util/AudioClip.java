package com.samsung.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class AudioClip {
    public static final int TYPE_MIDI = 3;
    public static final int TYPE_MMF = 1;
    public static final int TYPE_MP3 = 2;
    private byte[] ec;
    private int offset;
    private int type;

    private AudioClip() {
    }

    public AudioClip(int i, String str) {
        if (!ar()) {
            throw new IllegalStateException("device does not support AudioClip");
        } else if (!ad(i)) {
            throw new IllegalArgumentException("type is not supported");
        } else {
            byte[] e = e(str, i);
            if (e == null) {
                throw new IOException("resource " + str + " could not be opened");
            }
            this.ec = e;
            this.type = i;
        }
    }

    public AudioClip(int i, byte[] bArr, int i2, int i3) {
        if (!ar()) {
            throw new IllegalStateException("device does not support AudioClip");
        } else if (bArr == null) {
            throw new NullPointerException("null audio data");
        } else if (i2 < 0 || i3 <= 0 || i2 + i3 > bArr.length) {
            throw new ArrayIndexOutOfBoundsException("illegal offset or length values");
        } else if (!ad(i)) {
            throw new IllegalArgumentException("type is not supported");
        } else {
            byte[] bArr2 = new byte[i3];
            System.arraycopy(bArr, i2, bArr2, 0, i3);
            this.ec = bArr2;
            this.type = i;
        }
    }

    private boolean ad(int i) {
        return i == 1 || i == 2 || i == 3;
    }

    public static boolean ar() {
        return true;
    }

    private byte[] e(String str, int i) {
        byte[] bArr = null;
        try {
            InputStream resourceAsStream = getClass().getResourceAsStream(str);
            if (resourceAsStream == null) {
                return bArr;
            }
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] bArr2 = new byte[1024];
            for (int read = resourceAsStream.read(bArr2); read > 0; read = resourceAsStream.read(bArr2)) {
                byteArrayOutputStream.write(bArr2, 0, read);
            }
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            System.out.println("error opening resource " + str);
            return bArr;
        }
    }

    public void j(int i, int i2) {
        if (i < 0 || i > 255 || i2 < 0 || i2 > 5) {
            throw new IllegalArgumentException("Illegal value: loop value must be 0-255, volume value must be 0-5");
        }
    }

    public void pause() {
    }

    public void resume() {
    }

    public void stop() {
    }
}
