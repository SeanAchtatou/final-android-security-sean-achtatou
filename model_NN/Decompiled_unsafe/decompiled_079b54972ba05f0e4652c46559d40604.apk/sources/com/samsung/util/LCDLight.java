package com.samsung.util;

public final class LCDLight {
    public static void ae(int i) {
        if (!ar()) {
            throw new IllegalStateException("on: device does not support LCDLight");
        } else if (i < 0) {
            throw new IllegalArgumentException("invalid duration");
        }
    }

    public static boolean ar() {
        return true;
    }

    public static void at() {
        if (!ar()) {
            throw new IllegalStateException("off: device does not support LCDLight");
        }
    }
}
