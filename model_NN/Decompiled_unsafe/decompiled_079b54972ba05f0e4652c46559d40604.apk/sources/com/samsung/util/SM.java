package com.samsung.util;

public class SM {
    private String ed;
    private String ee;
    private final int ef;
    private String message;

    public SM() {
        this.ef = 80;
        this.ed = "";
        this.ee = "";
        this.message = "";
    }

    public SM(String str, String str2, String str3) {
        this.ef = 80;
        E(str);
        F(str2);
        setData(str3);
    }

    private boolean G(String str) {
        int length;
        if (str == null || (length = str.length()) > 20) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if ((charAt < '0' || charAt > '9') && (charAt != '+' || i != 0)) {
                return false;
            }
        }
        return true;
    }

    public void E(String str) {
        if (!G(str)) {
            throw new IllegalArgumentException("Invalid DestAddress");
        }
        this.ed = str;
    }

    public void F(String str) {
        this.ee = str;
    }

    public String au() {
        return this.ed;
    }

    public String av() {
        return this.ee;
    }

    public String getData() {
        return this.message;
    }

    public void setData(String str) {
        if (str == null || str.length() <= 80) {
            this.message = str;
            return;
        }
        throw new IllegalArgumentException("Invalid textMessage size");
    }
}
