package com.samsung.util;

public final class Vibration {
    public static boolean ar() {
        return true;
    }

    public static void k(int i, int i2) {
        if (!ar()) {
            throw new IllegalStateException("start: device does not support Vibration");
        } else if (i < 0 || i2 < 1 || i2 > 5) {
            throw new IllegalArgumentException("illegal parameter");
        }
    }

    public static void stop() {
        if (!ar()) {
            throw new IllegalStateException("stop: device does not support Vibration");
        }
    }
}
