package com.samsung.util;

public class SMS {
    public static void a(SM sm) {
        if (sm == null) {
            throw new NullPointerException("SM is null");
        } else if (!ar()) {
            throw new IllegalStateException("send: device does not support SMS");
        } else if (sm.getData() == null) {
            throw new NullPointerException("SM data is null");
        }
    }

    public static boolean ar() {
        return true;
    }
}
