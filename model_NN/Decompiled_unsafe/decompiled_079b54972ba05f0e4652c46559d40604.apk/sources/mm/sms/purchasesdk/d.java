package mm.sms.purchasesdk;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import mm.sms.purchasesdk.f.c;
import mm.sms.purchasesdk.sms.SMSReceiver;

class d extends Handler {
    final /* synthetic */ SMSPurchase b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    d(SMSPurchase sMSPurchase, Looper looper) {
        super(looper);
        this.b = sMSPurchase;
    }

    public void handleMessage(Message message) {
        int i = message.arg1;
        b bVar = (b) message.obj;
        switch (message.what) {
            case 0:
                c.a(this.b.f4a, this.b.b, bVar, i);
                break;
            case 2:
                SMSReceiver.d = false;
                c.b(this.b.f4a, this.b.b, bVar, i);
                break;
            case 5:
                this.b.b(c.getContext());
                System.out.println("SMSPurchase() code =" + bVar.a());
                bVar.m2a();
                break;
        }
        super.handleMessage(message);
    }
}
