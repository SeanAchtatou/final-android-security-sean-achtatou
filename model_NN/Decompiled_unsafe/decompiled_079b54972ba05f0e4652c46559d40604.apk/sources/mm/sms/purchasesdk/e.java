package mm.sms.purchasesdk;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import com.chinaMobile.MobileAgent;
import mm.sms.purchasesdk.a.a;
import mm.sms.purchasesdk.a.b;
import mm.sms.purchasesdk.f.c;
import mm.sms.purchasesdk.f.d;

public class e extends HandlerThread {
    private static a a;
    /* access modifiers changed from: private */
    public static boolean b;

    /* renamed from: a  reason: collision with other field name */
    Handler f12a;

    /* renamed from: b  reason: collision with other field name */
    Handler f13b;

    public e(String str) {
        super(str);
    }

    public static a a() {
        return a;
    }

    /* access modifiers changed from: private */
    public void a(b bVar) {
        d.c("TaskThread", "init() called");
        MobileAgent.k(c.getContext(), c.m29o(), c.v());
        mm.sms.purchasesdk.c.a.n = 1;
        mm.sms.purchasesdk.c.a.m5b();
        c.f(-1);
        Message obtainMessage = this.f13b.obtainMessage();
        obtainMessage.what = 0;
        obtainMessage.obj = bVar;
        obtainMessage.arg1 = 1000;
        obtainMessage.sendToTarget();
    }

    /* access modifiers changed from: private */
    public void a(b bVar, int i) {
        Message obtainMessage = this.f13b.obtainMessage();
        obtainMessage.what = 0;
        obtainMessage.arg1 = i;
        obtainMessage.obj = bVar;
        obtainMessage.sendToTarget();
    }

    /* renamed from: a  reason: collision with other method in class */
    public static boolean m6a() {
        return b;
    }

    /* access modifiers changed from: private */
    public void b(b bVar) {
        d.c("TaskThread", " enter auth  ");
        if (a == null) {
            a = new a();
        }
        mm.sms.purchasesdk.c.a.n = 2;
        mm.sms.purchasesdk.c.a.m5b();
        if (mm.sms.purchasesdk.b.a.a(a).booleanValue()) {
            PurchaseCode.setStatusCode(PurchaseCode.BILL_NOORDER);
            Message obtainMessage = this.f13b.obtainMessage();
            obtainMessage.what = 2;
            obtainMessage.arg1 = PurchaseCode.getStatusCode();
            obtainMessage.obj = bVar;
            obtainMessage.sendToTarget();
            return;
        }
        d.d("TaskThread", "AuthManager checkAuth ret = null.code=" + PurchaseCode.getStatusCode());
        Message obtainMessage2 = this.f13b.obtainMessage();
        obtainMessage2.what = 2;
        obtainMessage2.arg1 = PurchaseCode.getStatusCode();
        obtainMessage2.obj = bVar;
        obtainMessage2.sendToTarget();
    }

    /* access modifiers changed from: private */
    public void c(b bVar) {
        d.c("TaskThread", " enter order  ");
        c.d((Boolean) false);
        Message obtainMessage = this.f13b.obtainMessage();
        obtainMessage.what = 2;
        obtainMessage.arg1 = PurchaseCode.getStatusCode();
        obtainMessage.obj = bVar;
        b.a(a, obtainMessage);
    }

    /* renamed from: a  reason: collision with other method in class */
    public Handler m7a() {
        if (this.f12a == null) {
            init();
        }
        return this.f12a;
    }

    public void a(Handler handler) {
        this.f13b = handler;
    }

    public void init() {
        this.f12a = new f(this, getLooper());
    }
}
