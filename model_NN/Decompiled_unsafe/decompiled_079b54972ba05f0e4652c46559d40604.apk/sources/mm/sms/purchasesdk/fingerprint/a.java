package mm.sms.purchasesdk.fingerprint;

import android.content.ContextWrapper;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import mm.sms.purchasesdk.f.c;
import mm.sms.purchasesdk.f.d;

public class a {
    public static Boolean c = true;
    private static int status = 0;

    public a() {
        try {
            IdentifyApp.init(c.getContext(), d(), d.M);
            status = IdentifyApp.getLastError();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String a(String str, String str2) {
        try {
            try {
                return (String) Class.forName("android.os.SystemProperties").getMethod("get", String.class, String.class).invoke(null, str, str2);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                return null;
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
                return null;
            } catch (InvocationTargetException e3) {
                e3.printStackTrace();
                return null;
            }
        } catch (SecurityException e4) {
            e4.printStackTrace();
            return null;
        } catch (NoSuchMethodException e5) {
            e5.printStackTrace();
            return null;
        } catch (ClassNotFoundException e6) {
            e6.printStackTrace();
            return null;
        }
    }

    private static String d() {
        String a = a("ro.product.cpu.abi", null);
        String a2 = a("ro.product.cpu.abi2", null);
        System.currentTimeMillis();
        if (a == null) {
            return null;
        }
        ZipFile zipFile = new ZipFile(((ContextWrapper) c.getContext()).getPackageCodePath());
        boolean z = false;
        Enumeration<? extends ZipEntry> entries = zipFile.entries();
        while (true) {
            boolean z2 = z;
            if (entries.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry) entries.nextElement();
                if (zipEntry.getName().startsWith("lib/" + a + "/")) {
                    System.currentTimeMillis();
                    return a;
                }
                z = zipEntry.getName().startsWith(new StringBuilder().append("lib/").append(a2).append("/").toString()) ? true : z2;
            } else {
                zipFile.close();
                return z2 ? a2 : null;
            }
        }
    }

    public static void init() {
        try {
            IdentifyApp.init(c.getContext(), d(), d.M);
            status = IdentifyApp.getLastError();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
