package mm.sms.purchasesdk.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Message;
import mm.sms.purchasesdk.PurchaseCode;
import mm.sms.purchasesdk.f.c;
import mm.sms.purchasesdk.f.d;

public class SMSReceiver extends BroadcastReceiver {
    private static Message a;
    public static Boolean d = false;
    public static String i = "aspire.iap.SMS_SEND_ACTIOIN";
    public static String j = "aspire.iap.SMS_DELIVERED_ACTION";
    private final String TAG = "SMSReceiver";

    public static void a(Message message) {
        a = message;
    }

    private int d() {
        switch (c.F) {
            case 0:
                return 1000;
            case 1:
            default:
                return -1;
            case 2:
                return PurchaseCode.ORDER_OK;
        }
    }

    private int e() {
        switch (c.F) {
            case 0:
                return PurchaseCode.INIT_SMS_ERR;
            case 1:
            default:
                return -1;
            case 2:
                return PurchaseCode.BILL_SMS_ERR;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void onReceive(Context context, Intent intent) {
        d.c("SMSReceiver", "is receiver finished：" + d);
        if (!d.booleanValue() && intent.getAction().equals(i)) {
            try {
                switch (getResultCode()) {
                    case -1:
                        b.a(true);
                        a.arg1 = d();
                        a.sendToTarget();
                        return;
                    case 0:
                    default:
                        return;
                    case 1:
                        b.a(true);
                        a.arg1 = e();
                        a.sendToTarget();
                        return;
                    case 2:
                        return;
                }
            } catch (Exception e) {
                e.getStackTrace();
            }
            e.getStackTrace();
        }
    }
}
