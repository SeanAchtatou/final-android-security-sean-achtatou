package mm.sms.purchasesdk.sms;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Message;
import android.telephony.gsm.SmsManager;
import java.io.File;
import mm.sms.purchasesdk.PurchaseCode;
import mm.sms.purchasesdk.f.c;
import mm.sms.purchasesdk.f.d;

public class a {
    private String TAG = "SMSManager";

    public void a(String str, Message message) {
        String w = c.w();
        d.c(this.TAG, "send message address:" + w);
        d.c(this.TAG, "send message:" + str);
        SmsManager smsManager = SmsManager.getDefault();
        try {
            Intent intent = new Intent(SMSReceiver.i);
            Intent intent2 = new Intent(SMSReceiver.j);
            smsManager.sendTextMessage(w, null, str, PendingIntent.getBroadcast(c.getContext().getApplicationContext(), 0, intent, 0), PendingIntent.getBroadcast(c.getContext().getApplicationContext(), 0, intent2, 0));
            b bVar = new b(c.o(), message);
            b.a(false);
            bVar.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int c() {
        if (new File(c.A()).exists()) {
            return 1000;
        }
        return PurchaseCode.INIT_NO_ACTIVE;
    }
}
