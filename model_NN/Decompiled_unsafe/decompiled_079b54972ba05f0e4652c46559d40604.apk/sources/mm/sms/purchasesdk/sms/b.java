package mm.sms.purchasesdk.sms;

import android.os.Message;
import mm.sms.purchasesdk.PurchaseCode;
import mm.sms.purchasesdk.f.c;
import mm.sms.purchasesdk.f.d;

public class b extends Thread {
    private static Boolean e = false;
    private final String TAG = "Timer";
    private Message a;
    private int o;

    public b(int i, Message message) {
        this.o = i;
        this.a = message;
    }

    public static void a(Boolean bool) {
        e = bool;
    }

    private int d() {
        switch (c.F) {
            case 0:
                return PurchaseCode.INIT_TIMEOUT;
            case 1:
            default:
                return -1;
            case 2:
                return PurchaseCode.ORDER_OK_TIMEOUT;
        }
    }

    public void run() {
        super.run();
        while (!e.booleanValue() && this.o > 0) {
            SMSReceiver.d = false;
            try {
                sleep(1000);
                this.o--;
            } catch (InterruptedException e2) {
                e2.printStackTrace();
            }
        }
        if (this.o == 0) {
            d.c("Timer", "send message failed,is timeout ");
            SMSReceiver.d = true;
            this.a.arg1 = d();
            this.a.sendToTarget();
        }
    }
}
