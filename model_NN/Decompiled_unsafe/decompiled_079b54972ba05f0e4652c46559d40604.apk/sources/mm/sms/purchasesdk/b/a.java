package mm.sms.purchasesdk.b;

import com.fasterxml.jackson.core.util.MinimalPrettyPrinter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import me.gall.sgp.sdk.entity.app.StructuredData;
import mm.sms.purchasesdk.PurchaseCode;
import mm.sms.purchasesdk.e.e;
import mm.sms.purchasesdk.f.c;
import mm.sms.purchasesdk.f.d;
import mm.sms.purchasesdk.fingerprint.IdentifyApp;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class a {
    private static final String TAG = a.class.getSimpleName();

    private static int a(String str, String str2) {
        new ArrayList();
        try {
            XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
            newPullParser.setInput(new ByteArrayInputStream(str.getBytes()), "utf-8");
            String str3 = null;
            for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
                switch (eventType) {
                    case 2:
                        String name = newPullParser.getName();
                        if (!"ProgramId".equals(name)) {
                            if (!"Provider".equals(name)) {
                                if (!"Mark".equals(name)) {
                                    if (!com.umeng.common.a.d.equals(name)) {
                                        break;
                                    } else {
                                        String nextText = newPullParser.nextText();
                                        d.c(TAG, "achannel = " + nextText);
                                        c.e(nextText);
                                        break;
                                    }
                                } else {
                                    str3 = newPullParser.nextText();
                                    d.c(TAG, "aMark = " + str3);
                                    break;
                                }
                            } else {
                                d.c(TAG, "aProvider = " + newPullParser.nextText());
                                break;
                            }
                        } else {
                            d.c(TAG, "aProgramId = " + newPullParser.nextText());
                            break;
                        }
                }
            }
            if (str3.equals(str2)) {
                return PurchaseCode.BILL_NOORDER;
            }
            d.c(TAG, "md5data error: ");
            return PurchaseCode.BILL_INVALID_APP;
        } catch (XmlPullParserException e) {
            d.a(TAG, "failed to read mmiap.xml excepiton. ", e);
            return PurchaseCode.BILL_XML_PARSE_ERR;
        } catch (IOException e2) {
            d.a(TAG, "failed to read mmiap.xml. io excetion ", e2);
            return PurchaseCode.BILL_XML_PARSE_ERR;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: mm.sms.purchasesdk.b.a.a(java.lang.String, java.lang.String):int
     arg types: [java.lang.String, java.lang.String]
     candidates:
      mm.sms.purchasesdk.b.a.a(java.lang.String, java.lang.String):mm.sms.purchasesdk.a.c
      mm.sms.purchasesdk.b.a.a(mm.sms.purchasesdk.a.c, float):void
      mm.sms.purchasesdk.b.a.a(java.lang.String, java.lang.String):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: mm.sms.purchasesdk.b.a.a(java.lang.String, java.lang.String):mm.sms.purchasesdk.a.c
     arg types: [java.lang.String, java.lang.String]
     candidates:
      mm.sms.purchasesdk.b.a.a(java.lang.String, java.lang.String):int
      mm.sms.purchasesdk.b.a.a(mm.sms.purchasesdk.a.c, float):void
      mm.sms.purchasesdk.b.a.a(java.lang.String, java.lang.String):mm.sms.purchasesdk.a.c */
    public static Boolean a(mm.sms.purchasesdk.a.a aVar) {
        PurchaseCode.setStatusCode(PurchaseCode.ORDER_OK);
        aVar.b(c.m31q());
        aVar.a(c.m27c());
        if (c.c().booleanValue()) {
            aVar.a(b());
            aVar.a(c.m28n());
            return true;
        }
        new mm.sms.purchasesdk.d.a();
        String c = mm.sms.purchasesdk.d.a.c();
        String str = null;
        if (c == null) {
            d.c(TAG, "read mmiap.xml false ");
            PurchaseCode.setStatusCode(PurchaseCode.INIT_TEST_APK);
            return false;
        } else if (c.c().booleanValue() || ((str = a(c)) != null && str.trim().length() > 0)) {
            if (!c.c().booleanValue()) {
                d.c(TAG, "md5data signContent MD5 is: " + IdentifyApp.md5(str.getBytes()));
                int a = a(c, IdentifyApp.md5(str.getBytes()));
                if (1202 != a) {
                    d.c(TAG, "MD5 error: ");
                    PurchaseCode.setStatusCode(a);
                    return false;
                }
            }
            mm.sms.purchasesdk.a.c a2 = m3a(c.m31q(), c);
            if (a2 == null) {
                PurchaseCode.setStatusCode(PurchaseCode.BILL_PAYCODE_ERR);
                return false;
            }
            aVar.a(a2);
            aVar.a(c.m28n());
            return true;
        } else {
            PurchaseCode.setStatusCode(PurchaseCode.INIT_TEST_APK);
            return false;
        }
    }

    public static String a(String str) {
        int indexOf = str.indexOf("<ProgramId>");
        int indexOf2 = str.indexOf("<Mark>");
        if (indexOf < 0 || indexOf2 < 0) {
            return null;
        }
        String substring = str.substring(indexOf, indexOf2);
        d.c(TAG, "md5data signContent is: " + substring);
        d.c(TAG, "md5data signContentlength  is: " + substring.length());
        substring.trim();
        substring.replace(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, "");
        return substring;
    }

    /* renamed from: a  reason: collision with other method in class */
    private static mm.sms.purchasesdk.a.c m3a(String str, String str2) {
        new ArrayList().add(new mm.sms.purchasesdk.d.a());
        try {
            return b(str, str2);
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            return null;
        } catch (SAXException e2) {
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            e3.printStackTrace();
            return null;
        }
    }

    private static e a(Element element, String str, int i) {
        Element element2 = (Element) element.getElementsByTagName(str).item(i);
        String decryptPapaya = IdentifyApp.decryptPapaya(element2.getFirstChild() != null ? element2.getFirstChild().getNodeValue() : null);
        e eVar = new e();
        if (str.equals("Provider")) {
            eVar.s = "provider";
            eVar.r = "提供商:";
            eVar.mValue = decryptPapaya;
        } else if (str.equals("AppName")) {
            eVar.s = "appname";
            eVar.r = "应用名称:";
            eVar.mValue = decryptPapaya;
        }
        return eVar;
    }

    private static void a(mm.sms.purchasesdk.a.c cVar, float f) {
        e eVar = new e();
        eVar.s = "itemcount";
        eVar.r = "数量：";
        eVar.mValue = "" + c.b();
        eVar.D = -39424;
        cVar.c(eVar.s);
        cVar.a(eVar.s, eVar);
        e eVar2 = new e();
        eVar2.s = "totalprice";
        eVar2.r = "支付金额:";
        eVar2.mValue = (((float) c.b()) * f) + "元";
        eVar2.D = -39424;
        cVar.c(eVar2.s);
        cVar.a(eVar2.s, eVar2);
    }

    private static mm.sms.purchasesdk.a.c b() {
        mm.sms.purchasesdk.a.c cVar = new mm.sms.purchasesdk.a.c();
        e eVar = new e();
        eVar.s = "appname";
        eVar.r = "应用名称：";
        eVar.mValue = "" + c.m29o();
        eVar.D = -16777216;
        cVar.c(eVar.s);
        cVar.a(eVar.s, eVar);
        e eVar2 = new e();
        eVar2.s = "provider";
        eVar2.r = "提供商：";
        eVar2.mValue = "" + c.m29o();
        eVar2.D = -16777216;
        cVar.c(eVar2.s);
        cVar.a(eVar2.s, eVar2);
        e eVar3 = new e();
        eVar3.s = "itemname";
        eVar3.r = "商品：";
        eVar3.mValue = "" + c.m31q();
        eVar3.D = -16777216;
        cVar.c(eVar3.s);
        cVar.a(eVar3.s, eVar3);
        e eVar4 = new e();
        eVar4.s = "itemprice";
        eVar4.r = "单价：";
        eVar4.mValue = "0.1元";
        eVar4.D = -39424;
        cVar.c(eVar4.s);
        cVar.a(eVar4.s, eVar4);
        e eVar5 = new e();
        eVar5.s = "itemcount";
        eVar5.r = "数量：";
        eVar5.mValue = "" + c.b();
        eVar5.D = -39424;
        cVar.c(eVar5.s);
        cVar.a(eVar5.s, eVar5);
        e eVar6 = new e();
        eVar6.s = "totalprice";
        eVar6.r = "支付金额:";
        eVar6.mValue = (((double) c.b()) * 0.1d) + "元";
        eVar6.D = -39424;
        cVar.c(eVar6.s);
        cVar.a(eVar6.s, eVar6);
        return cVar;
    }

    private static mm.sms.purchasesdk.a.c b(String str, String str2) {
        float f;
        int i = 0;
        d.c(TAG, "enter parseProductInfo");
        mm.sms.purchasesdk.a.c cVar = new mm.sms.purchasesdk.a.c();
        Element documentElement = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new ByteArrayInputStream(str2.getBytes())).getDocumentElement();
        cVar.a("appname", a(documentElement, "AppName", 0));
        cVar.c("appname");
        cVar.a("provider", a(documentElement, "Provider", 0));
        cVar.c("provider");
        NodeList elementsByTagName = documentElement.getElementsByTagName("ProductInfo");
        for (int i2 = 0; i2 < elementsByTagName.getLength(); i2++) {
            Element element = (Element) documentElement.getElementsByTagName("paycode").item(i2);
            String decryptPapaya = IdentifyApp.decryptPapaya(element.getFirstChild() != null ? element.getFirstChild().getNodeValue() : null);
            d.c(TAG, "paycode =" + decryptPapaya);
            d.c(TAG, "apaycode =" + str);
            if (decryptPapaya.equals(str)) {
                NodeList elementsByTagName2 = ((Element) documentElement.getElementsByTagName("ProductInfo").item(i2)).getElementsByTagName("Item");
                d.c(TAG, "nodes.getLength() = " + elementsByTagName2.getLength());
                float f2 = 0.0f;
                while (i < elementsByTagName2.getLength()) {
                    Element element2 = (Element) elementsByTagName2.item(i);
                    String attribute = element2.getAttribute("id");
                    String attribute2 = element2.getAttribute("name");
                    String attribute3 = element2.getAttribute(StructuredData.TYPE_OF_VALUE);
                    d.c(TAG, "id = " + attribute);
                    d.c(TAG, "name = " + attribute2);
                    d.c(TAG, "value = " + attribute3);
                    e eVar = new e();
                    eVar.s = IdentifyApp.decryptPapaya(attribute);
                    eVar.r = IdentifyApp.decryptPapaya(attribute2) + ":";
                    eVar.mValue = IdentifyApp.decryptPapaya(attribute3);
                    d.c(TAG, "info.mID = " + eVar.s);
                    d.c(TAG, "info.mKey = " + eVar.r);
                    d.c(TAG, "info.mValue = " + eVar.mValue);
                    if (eVar.s.equals("itemprice")) {
                        Integer num = new Integer(eVar.mValue.replace("分", ""));
                        eVar.mValue = (num.floatValue() / 100.0f) + "元";
                        f = num.floatValue() / 100.0f;
                        d.c(TAG, "Price = " + eVar.mValue);
                    } else {
                        f = f2;
                    }
                    if (eVar.s.equals("totalprice") || eVar.s.equals("itemcount") || eVar.s.equals("itemprice") || eVar.s.equals("renttime")) {
                        eVar.D = -39424;
                    } else {
                        eVar.D = -16777216;
                    }
                    cVar.c(eVar.s);
                    cVar.a(eVar.s, eVar);
                    i++;
                    f2 = f;
                }
                a(cVar, f2);
                return cVar;
            }
        }
        PurchaseCode.setStatusCode(PurchaseCode.BILL_PAYCODE_NULL);
        return null;
    }
}
