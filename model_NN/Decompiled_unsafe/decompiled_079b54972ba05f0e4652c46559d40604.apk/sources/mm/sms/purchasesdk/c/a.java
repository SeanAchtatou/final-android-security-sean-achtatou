package mm.sms.purchasesdk.c;

import android.os.Build;
import android.util.Log;
import com.chinaMobile.MobileAgent;
import java.util.Calendar;
import mm.sms.purchasesdk.f.c;

public class a {
    private static long a = 0;
    private static long b = 0;
    private static long c = 0;
    public static int n = -1;

    public static long a() {
        return b;
    }

    /* renamed from: a  reason: collision with other method in class */
    private static String m4a() {
        switch (n) {
            case 1:
                return "_pay_init";
            case 2:
                return "_pay_auth";
            case 3:
                return "_pay_pay";
            case 4:
                return "_pay_remain";
            default:
                return "unknown";
        }
    }

    public static String b() {
        String o = c.m29o();
        String q = c.m31q();
        String y = c.y();
        String valueOf = String.valueOf(c.p());
        String packageName = c.getPackageName();
        String C = c.C();
        String r = c.r();
        String u = c.u();
        String s = c.s();
        String t = c.t();
        String str = Build.MODEL;
        String str2 = Build.VERSION.RELEASE;
        String str3 = c.a().widthPixels + "*" + c.a().heightPixels;
        String E = c.E();
        String D = c.D();
        String F = c.F();
        String valueOf2 = String.valueOf(getStartTime());
        String valueOf3 = String.valueOf(a());
        String I = c.I();
        String H = c.H();
        String str4 = "unknown";
        String str5 = "" + c.n();
        switch (n) {
            case 1:
                str4 = o + "@@" + packageName + "@@" + C + "@@" + r + "@@" + u + "@@" + s + "@@" + t + "@@" + str + "@@" + str2 + "@@" + str3 + "@@" + D + "@@" + E + "@@" + "" + "@@" + F + "@@" + valueOf2 + "@@" + valueOf3 + "@@" + str5;
                break;
            case 2:
                str4 = o + "@@" + q + "@@" + "1" + "@@" + y + "@@" + valueOf + "@@" + packageName + "@@" + C + "@@" + r + "@@" + u + "@@" + s + "@@" + t + "@@" + D + "@@" + E + "@@" + "" + "@@" + F + "@@" + valueOf2 + "@@" + valueOf3 + "@@" + str5;
                break;
            case 3:
                str4 = o + "@@" + q + "@@" + "1" + "@@" + y + "@@" + valueOf + "@@" + packageName + "@@" + C + "@@" + r + "@@" + u + "@@" + s + "@@" + t + "@@" + D + "@@" + E + "@@" + "" + "@@" + F + "@@" + valueOf2 + "@@" + valueOf3 + "@@" + str5;
                break;
            case 4:
                str4 = o + "@@" + packageName + "@@" + C + "@@" + r + "@@" + u + "@@" + s + "@@" + t + "@@" + F + "@@" + I + "@@" + H + "@@" + str5;
                break;
        }
        Log.d("DAHelper", str4);
        return str4;
    }

    /* renamed from: b  reason: collision with other method in class */
    public static void m5b() {
        a = Calendar.getInstance().getTimeInMillis() / 1000;
    }

    public static void c() {
        b = Calendar.getInstance().getTimeInMillis() / 1000;
    }

    public static void d() {
        MobileAgent.i(c.getContext(), m4a(), b());
    }

    public static long getStartTime() {
        return a;
    }
}
