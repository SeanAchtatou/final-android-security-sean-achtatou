package mm.sms.purchasesdk;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;
import mm.sms.purchasesdk.e.j;
import mm.sms.purchasesdk.f.b;
import mm.sms.purchasesdk.f.d;
import mm.sms.purchasesdk.f.e;
import mm.sms.purchasesdk.f.f;
import mm.sms.purchasesdk.fingerprint.IdentifyApp;
import mm.sms.purchasesdk.fingerprint.a;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class c {
    private static final String TAG = c.class.getSimpleName();
    private static boolean a = false;

    public static int a(Context context) {
        if (a) {
            return 0;
        }
        e.k(context.getPackageName());
        mm.sms.purchasesdk.f.c.b(b.m26a(context));
        mm.sms.purchasesdk.f.c.c(Boolean.valueOf(f.b(context)));
        a.init();
        String c = mm.sms.purchasesdk.d.a.c();
        if (c == null) {
            mm.sms.purchasesdk.f.c.b((Boolean) true);
        } else {
            String a2 = mm.sms.purchasesdk.b.a.a(c);
            if (a2 == null || a2.trim().length() <= 0) {
                mm.sms.purchasesdk.f.c.b((Boolean) true);
                a = true;
                return 0;
            }
            mm.sms.purchasesdk.f.c.b((Boolean) false);
            try {
                XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
                newPullParser.setInput(new ByteArrayInputStream(c.getBytes()), "utf-8");
                int i = 0;
                for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
                    switch (eventType) {
                        case 2:
                            String name = newPullParser.getName();
                            if (!"ProgramId".equals(name)) {
                                if (!com.umeng.common.a.d.equals(name)) {
                                    if (!"Channelcode".equals(name)) {
                                        break;
                                    } else {
                                        String nextText = newPullParser.nextText();
                                        d.c(TAG, "aChannelcode = " + nextText);
                                        mm.sms.purchasesdk.f.c.g(IdentifyApp.decryptPapaya(nextText));
                                        d.c(TAG, "aProgramId = " + IdentifyApp.decryptPapaya(nextText));
                                        break;
                                    }
                                } else {
                                    String nextText2 = newPullParser.nextText();
                                    d.c(TAG, "achannel = " + nextText2);
                                    mm.sms.purchasesdk.f.c.e(nextText2);
                                    i += 2;
                                    break;
                                }
                            } else {
                                String nextText3 = newPullParser.nextText();
                                d.c(TAG, "aProgramId = " + nextText3);
                                mm.sms.purchasesdk.f.c.f(IdentifyApp.decryptPapaya(nextText3));
                                d.c(TAG, "aProgramId = " + IdentifyApp.decryptPapaya(nextText3));
                                i++;
                                break;
                            }
                    }
                }
                if (i != 3) {
                    return PurchaseCode.BILL_XML_PARSE_ERR;
                }
            } catch (XmlPullParserException e) {
                d.a(TAG, "failed to read mmiap.xml excepiton. ", e);
                return PurchaseCode.BILL_XML_PARSE_ERR;
            } catch (IOException e2) {
                d.a(TAG, "failed to read mmiap.xml. io excetion ", e2);
                return PurchaseCode.BILL_XML_PARSE_ERR;
            }
        }
        if (!mm.sms.purchasesdk.f.c.d().booleanValue()) {
            return PurchaseCode.INIT_NOT_CMCC;
        }
        a = true;
        return 0;
    }

    public static void a(Handler handler, Handler handler2, b bVar, int i) {
        mm.sms.purchasesdk.c.a.c();
        mm.sms.purchasesdk.c.a.d();
        switch (mm.sms.purchasesdk.f.c.F) {
            case 0:
                if (mm.sms.purchasesdk.f.c.d().booleanValue() || !mm.sms.purchasesdk.f.c.m27c()) {
                    bVar.onInitFinish(i);
                    return;
                } else {
                    bVar.onInitFinish(1000);
                    return;
                }
            case 1:
            default:
                return;
            case 2:
                if (i == 1000) {
                    Message obtainMessage = handler.obtainMessage();
                    obtainMessage.what = 2;
                    obtainMessage.obj = bVar;
                    obtainMessage.arg1 = 0;
                    obtainMessage.sendToTarget();
                    return;
                }
                Message obtainMessage2 = handler.obtainMessage();
                obtainMessage2.what = 2;
                obtainMessage2.obj = bVar;
                obtainMessage2.arg1 = 0;
                obtainMessage2.sendToTarget();
                return;
        }
    }

    public static int b(Context context) {
        String o = mm.sms.purchasesdk.f.c.m29o();
        String p = mm.sms.purchasesdk.f.c.m30p();
        if (o == null || o.trim().length() == 0) {
            d.d("PurchaseInternal", "appid is null");
            return PurchaseCode.PARAMETER_ERR;
        } else if (p == null || p.trim().length() == 0) {
            d.d("PurchaseInternal", "appkey is null");
            return PurchaseCode.PARAMETER_ERR;
        } else {
            mm.sms.purchasesdk.f.c.b(o, p);
            mm.sms.purchasesdk.f.c.c(Boolean.valueOf(f.b(context)));
            return 1000;
        }
    }

    public static void b(Handler handler, Handler handler2, b bVar, int i) {
        mm.sms.purchasesdk.f.c.f(i);
        if (1001 == 1001 || 1214 == 1001) {
            HashMap hashMap = new HashMap();
            hashMap.put(OnSMSPurchaseListener.PAYCODE, mm.sms.purchasesdk.f.c.m31q());
            hashMap.put(OnSMSPurchaseListener.TRADEID, mm.sms.purchasesdk.f.c.y());
            bVar.a(PurchaseCode.ORDER_OK, hashMap);
        } else if (1001 == 1202) {
            j.a().a(handler, handler2, bVar, e.a());
        } else {
            HashMap hashMap2 = null;
            if (e.m6a()) {
                hashMap2 = new HashMap();
            }
            d.d("PurchaseInternal", " order fail =" + ((int) PurchaseCode.ORDER_OK));
            bVar.a(PurchaseCode.ORDER_OK, hashMap2);
        }
        mm.sms.purchasesdk.c.a.c();
        mm.sms.purchasesdk.c.a.d();
    }
}
