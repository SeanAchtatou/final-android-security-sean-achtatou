package mm.sms.purchasesdk.f;

import android.content.Context;
import android.telephony.TelephonyManager;
import java.lang.reflect.InvocationTargetException;

public class b {
    public static Boolean a(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getSimState() == 5;
    }

    /* renamed from: a  reason: collision with other method in class */
    public static boolean m26a(Context context) {
        String l = l();
        String k = k();
        Boolean a = a(context);
        if (l == null || l.equals("unknown") || !a.booleanValue()) {
            return k != null && !k.equals("unknown") && a.booleanValue();
        }
        return true;
    }

    private static String k() {
        try {
            try {
                return (String) Class.forName("android.os.SystemProperties").getMethod("get", String.class, String.class).invoke(null, "gsm.version.ril-impl", "unknown");
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                return null;
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
                return null;
            } catch (InvocationTargetException e3) {
                e3.printStackTrace();
                return null;
            }
        } catch (SecurityException e4) {
            e4.printStackTrace();
            return null;
        } catch (NoSuchMethodException e5) {
            e5.printStackTrace();
            return null;
        } catch (ClassNotFoundException e6) {
            e6.printStackTrace();
            return null;
        }
    }

    private static String l() {
        try {
            try {
                return (String) Class.forName("android.os.SystemProperties").getMethod("get", String.class, String.class).invoke(null, "gsm.version.baseband", "unknown");
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
                return null;
            } catch (IllegalAccessException e2) {
                e2.printStackTrace();
                return null;
            } catch (InvocationTargetException e3) {
                e3.printStackTrace();
                return null;
            }
        } catch (SecurityException e4) {
            e4.printStackTrace();
            return null;
        } catch (NoSuchMethodException e5) {
            e5.printStackTrace();
            return null;
        } catch (ClassNotFoundException e6) {
            e6.printStackTrace();
            return null;
        }
    }
}
