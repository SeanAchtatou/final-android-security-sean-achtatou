package mm.sms.purchasesdk.f;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import java.io.ByteArrayInputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPublicKey;

public class a {
    private static String a(byte[] bArr) {
        try {
            String bigInteger = ((RSAPublicKey) ((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(bArr))).getPublicKey()).getModulus().toString(16);
            if (bigInteger == null || !bigInteger.contains("modulus:")) {
                return bigInteger;
            }
            String substring = bigInteger.substring(bigInteger.indexOf("modulus: ") + 9, bigInteger.indexOf("\n", bigInteger.indexOf("modulus:")));
            substring.trim();
            d.c("CommUtil", "public key:" + substring);
            return substring;
        } catch (CertificateException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static byte[] a() {
        Context context = c.getContext();
        String str = context.getApplicationInfo().packageName;
        d.c("CommUtil", "package name:" + str);
        for (PackageInfo next : ((Activity) context).getPackageManager().getInstalledPackages(64)) {
            if (next.packageName.equals(str)) {
                return next.signatures[0].toByteArray();
            }
        }
        return null;
    }

    public static String j() {
        return a(a());
    }
}
