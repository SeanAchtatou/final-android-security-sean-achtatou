package mm.sms.purchasesdk.f;

import android.os.Environment;
import android.util.Log;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import mm.sms.purchasesdk.PurchaseCode;

public class d implements InvocationHandler {
    public static int M = 1;
    private static final String TAG = d.class.getSimpleName();
    private Object a;

    public static void a(int i, String str, String str2) {
        if (i >= M) {
            switch (i) {
                case 0:
                    Log.d(str, str2);
                    e.e(str, str2);
                    return;
                case 1:
                    Log.w(str, str2);
                    e.e(str, str2);
                    return;
                case 2:
                    Log.e(str, str2);
                    e.e(str, str2);
                    return;
                default:
                    Log.i(str, str2);
                    e.e(str, str2);
                    return;
            }
        }
    }

    public static void a(int i, String str, String str2, Exception exc) {
        if (i >= M) {
            switch (i) {
                case 0:
                    Log.d(str, str2);
                    e.e(str, str2);
                    return;
                case 1:
                    Log.w(str, str2);
                    e.e(str, str2);
                    return;
                case 2:
                    Log.e(str, str2, exc);
                    e.e(str, str2);
                    return;
                default:
                    Log.i(str, str2);
                    e.e(str, str2);
                    return;
            }
        }
    }

    public static void a(String str, String str2, Exception exc) {
        a(2, str, str2, exc);
    }

    public static void c(String str, String str2) {
        a(0, str, str2);
    }

    public static void d(String str, String str2) {
        a(2, str, str2);
    }

    public static void u() {
        File file = new File(Environment.getExternalStorageDirectory() + "/InAppBillingLibrary/IAPConfig");
        String str = "";
        if (file.exists()) {
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                if (fileInputStream != null) {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
                    while (true) {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break;
                        }
                        str = str + readLine;
                    }
                    fileInputStream.close();
                }
                switch (new Integer(str).intValue()) {
                    case 0:
                        M = 0;
                        return;
                    case 1:
                        M = 1;
                        return;
                    case 2:
                        M = 2;
                        return;
                    default:
                        M = 1;
                        return;
                }
            } catch (FileNotFoundException e) {
                M = 1;
            } catch (IOException e2) {
                M = 1;
            }
        } else {
            M = 1;
        }
    }

    public Object invoke(Object obj, Method method, Object[] objArr) {
        String name = method.getName();
        a(2, "PurchaseListener", name);
        if (name.compareTo("onQueryFinish") == 0) {
            d("PurchaseListener", "query statuscode = " + objArr[0] + " (" + PurchaseCode.getReason(((Integer) objArr[0]).intValue()) + ")");
        } else if (name.compareTo("onInitFinish") == 0) {
            d("PurchaseListener", "Init statuscode = " + objArr[0] + " (" + PurchaseCode.getReason(((Integer) objArr[0]).intValue()) + ")");
        } else if (name.compareTo("onUnsubscribeFinish") == 0) {
            d("PurchaseListener", "Unsubscribe statuscode = " + objArr[0] + " (" + PurchaseCode.getReason(((Integer) objArr[0]).intValue()) + ")");
        } else if (name.compareTo("onBillingFinish") == 0) {
            d("PurchaseListener", "Billing statuscode = " + objArr[0] + " (" + PurchaseCode.getReason(((Integer) objArr[0]).intValue()) + ")");
        }
        return method.invoke(this.a, objArr);
    }
}
