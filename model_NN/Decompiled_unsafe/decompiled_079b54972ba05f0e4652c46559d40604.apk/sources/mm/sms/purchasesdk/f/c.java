package mm.sms.purchasesdk.f;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import mm.sms.purchasesdk.e.r;
import mm.sms.purchasesdk.fingerprint.IdentifyApp;

public class c {
    private static String B = "";
    private static String C = "";
    private static String D = "";
    private static String E = "1.2.2";
    public static int F = -1;

    /* renamed from: F  reason: collision with other field name */
    private static String f37F = "15985";
    private static int G = 1;

    /* renamed from: G  reason: collision with other field name */
    private static String f38G = null;
    private static int H = 30;

    /* renamed from: H  reason: collision with other field name */
    private static String f39H = "";
    private static int I = 3;

    /* renamed from: I  reason: collision with other field name */
    private static String f40I = "";
    public static int J;

    /* renamed from: J  reason: collision with other field name */
    private static String f41J = "";
    public static int K;

    /* renamed from: K  reason: collision with other field name */
    private static String f42K = "0000000000";
    private static int L = -1;

    /* renamed from: L  reason: collision with other field name */
    private static String f43L = "";
    private static String M = "";
    private static String N = "";
    private static String O = "";
    public static String P = Build.VERSION.SDK;
    public static String Q = Build.MODEL;
    public static float c;
    private static boolean e = false;
    private static boolean f = false;
    private static boolean g = true;
    public static Boolean h = false;

    /* renamed from: h  reason: collision with other field name */
    public static boolean f44h = false;
    private static Boolean i = true;
    private static Boolean j = false;
    private static Boolean k = false;
    private static Boolean l = false;
    private static Context mContext;
    private static int n = 0;

    public static String A() {
        String str = null;
        try {
            str = mContext.getPackageManager().getApplicationInfo(mContext.getPackageName(), 0).dataDir;
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
        }
        String str2 = str + "/Files";
        File file = new File(str2);
        if (!file.exists()) {
            file.mkdir();
        }
        return str2 + "/mobile.so";
    }

    public static String B() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        try {
            date = simpleDateFormat.parse("2010-01-01 0:0:0");
        } catch (ParseException e2) {
            e2.printStackTrace();
        }
        return a((new Date().getTime() - date.getTime()) / 1000);
    }

    public static String C() {
        PackageInfo packageInfo = null;
        try {
            packageInfo = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
        }
        return packageInfo.versionName;
    }

    public static String D() {
        return "SMS";
    }

    public static String E() {
        return "";
    }

    public static String F() {
        return E;
    }

    public static String G() {
        return f39H;
    }

    public static String H() {
        return f40I;
    }

    public static String I() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        try {
            date = simpleDateFormat.parse("2010-01-01 0:0:0");
        } catch (ParseException e2) {
            e2.printStackTrace();
        }
        return Long.toString((new Date().getTime() - date.getTime()) / 1000);
    }

    public static DisplayMetrics a() {
        new DisplayMetrics();
        return getContext().getResources().getDisplayMetrics();
    }

    public static String a(long j2) {
        d.c("MMBillingSDk", "t10=  " + j2);
        d.c("MMBillingSDk", "tt=  " + (j2 + ""));
        String str = "" + b(j2 % 36);
        for (long j3 = j2 / 36; j3 > 0; j3 /= 36) {
            str = str + b(j3 % 36);
        }
        StringBuffer stringBuffer = new StringBuffer(str);
        stringBuffer.reverse().toString();
        d.c("MMBillingSDk", "result=  " + str);
        d.c("MMBillingSDk", "reresult=  " + ((Object) stringBuffer));
        d.c("MMBillingSDk", "result.length()=  " + str.length());
        return stringBuffer.toString();
    }

    public static synchronized boolean a(int i2) {
        boolean z = true;
        synchronized (c.class) {
            d.c("MMBillingSDk", " lock =  " + i2);
            d.c("MMBillingSDk", " mLocked =  " + e);
            if (e) {
                d.c("MMBillingSDk", " lock true");
            } else {
                e = true;
                F = i2;
                d.c("MMBillingSDk", " lock false");
                z = false;
            }
        }
        return z;
    }

    public static int b() {
        return G;
    }

    public static String b(long j2) {
        return j2 < 10 ? j2 + "" : String.valueOf((char) ((int) (55 + j2)));
    }

    public static String b(String str) {
        f41J = str;
        return str;
    }

    public static void b(Boolean bool) {
        k = bool;
    }

    public static void b(String str, String str2) {
        B = str;
        C = str2;
    }

    public static void b(boolean z) {
        g = z;
    }

    public static Boolean c() {
        return k;
    }

    public static String c(String str) {
        return str.replaceAll("[a-zA-Z`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]", "");
    }

    public static void c(Boolean bool) {
        j = bool;
    }

    /* renamed from: c  reason: collision with other method in class */
    public static boolean m27c() {
        return g;
    }

    public static Boolean d() {
        return j;
    }

    public static void d(int i2) {
        I = i2;
    }

    public static void d(Boolean bool) {
        i = bool;
    }

    public static void d(String str) {
        if (str.trim().length() == 0) {
            f38G = null;
        } else {
            f38G = str;
        }
    }

    public static Boolean e() {
        return i;
    }

    public static void e(int i2) {
        G = i2;
    }

    public static void e(String str) {
        f42K = str;
    }

    public static void f(int i2) {
        L = i2;
    }

    public static void f(String str) {
        N = str;
    }

    public static void g(int i2) {
        n = i2;
    }

    public static void g(String str) {
        O = str;
    }

    public static Context getContext() {
        return mContext;
    }

    public static String getPackageName() {
        PackageInfo packageInfo = null;
        try {
            packageInfo = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
        }
        return packageInfo.packageName;
    }

    public static void h(String str) {
        D = str;
    }

    public static void i(String str) {
        f39H = str;
    }

    public static void j(String str) {
        f40I = str;
    }

    public static String m() {
        return f38G;
    }

    public static int n() {
        return I;
    }

    /* renamed from: n  reason: collision with other method in class */
    public static String m28n() {
        return "尊敬的用户，欢迎使用中国移动手机话费支付，您的支付请求将通过短信加密方式发送给中国移动支付系统。支付成功后，您可登陆mm.10086.cn或拨打10086查询购买记录。 ";
    }

    public static int o() {
        return H;
    }

    /* renamed from: o  reason: collision with other method in class */
    public static String m29o() {
        return B;
    }

    public static int p() {
        return L;
    }

    /* renamed from: p  reason: collision with other method in class */
    public static String m30p() {
        return C;
    }

    public static int q() {
        return n;
    }

    /* renamed from: q  reason: collision with other method in class */
    public static String m31q() {
        return f41J;
    }

    public static String r() {
        return f42K;
    }

    private static void reset() {
        r.b = 1.0f;
    }

    public static String s() {
        f43L = ((TelephonyManager) mContext.getSystemService("phone")).getSubscriberId();
        if (f43L == null || M.equals("")) {
            f43L = "10086";
        }
        d.a(0, "MMBillingSDk", "Imsi-->" + f43L);
        return f43L;
    }

    /* renamed from: s  reason: collision with other method in class */
    public static void m32s() {
        mContext = null;
        D = "";
        G = 1;
        f41J = "";
        j = true;
        l = false;
        reset();
    }

    public static void setContext(Context context) {
        mContext = context;
    }

    public static String t() {
        M = ((TelephonyManager) mContext.getSystemService("phone")).getDeviceId();
        d.a(0, "MMBillingSDk", "mImei-->" + M);
        if (M == null || M.equals("")) {
            M = "10086";
        }
        return M;
    }

    /* renamed from: t  reason: collision with other method in class */
    public static void m33t() {
        float min;
        reset();
        r.r();
        int n2 = n();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) mContext.getSystemService("window")).getDefaultDisplay().getMetrics(displayMetrics);
        Configuration configuration = mContext.getResources().getConfiguration();
        K = displayMetrics.heightPixels;
        J = displayMetrics.widthPixels;
        r.b = displayMetrics.density / 1.5f;
        if (configuration.orientation == 2) {
            min = Math.min(((float) K) / 480.0f, ((float) J) / 800.0f);
            if (n2 == 1) {
                r.t = "mmiap/01/";
            } else if (n2 == 2) {
                r.t = "mmiap/02/";
            } else if (n2 == 3) {
                r.t = "mmiap/03/";
            } else {
                r.t = "mmiap/03/";
            }
            r.u += "horizontal/";
            r.t += "horizontal/";
        } else {
            min = Math.min(((float) K) / 800.0f, ((float) J) / 480.0f);
            if (n2 == 1) {
                r.t = "mmiap/01/";
            } else if (n2 == 2) {
                r.t = "mmiap/02/";
            } else if (n2 == 3) {
                r.t = "mmiap/03/";
            } else {
                r.t = "mmiap/03/";
            }
            r.u += "vertical/";
            r.t += "vertical/";
        }
        r.a(r.t, r.u);
        r.b = Math.min(min, r.b);
    }

    public static String u() {
        if (c().booleanValue()) {
            N = "99999999";
        }
        return N;
    }

    public static synchronized void unlock() {
        synchronized (c.class) {
            e = false;
            F = -1;
            m32s();
        }
    }

    public static String v() {
        if (c().booleanValue()) {
            O = "99999999";
        }
        return O;
    }

    public static String w() {
        if (!f44h) {
            return "1065842410";
        }
        d.d("MMBillingSDk", " getDestAddress ");
        return "1252013632580951";
    }

    public static String x() {
        String u = u();
        i(B());
        return IdentifyApp.generateTransactionID(G(), a(Long.parseLong(c(u))), a(Long.parseLong(c(r()))), a(Long.parseLong(c(m31q()))), a(Long.parseLong(c(t()))), a(Long.parseLong(c(s()))), m30p());
    }

    public static String y() {
        return D;
    }

    public static String z() {
        return f37F;
    }
}
