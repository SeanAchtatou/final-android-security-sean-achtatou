package mm.sms.purchasesdk;

import android.os.Handler;
import java.util.HashMap;
import mm.sms.purchasesdk.e.j;
import mm.sms.purchasesdk.e.r;
import mm.sms.purchasesdk.f.c;
import mm.sms.purchasesdk.f.d;

public class b {
    private static int l = 0;
    private Handler a;

    /* renamed from: a  reason: collision with other field name */
    private HashMap f10a = null;

    /* renamed from: a  reason: collision with other field name */
    private OnSMSPurchaseListener f11a;
    private Handler b;
    private int k = 0;

    public b(OnSMSPurchaseListener onSMSPurchaseListener, Handler handler, Handler handler2) {
        this.f11a = onSMSPurchaseListener;
        this.a = handler;
        this.b = handler2;
    }

    public int a() {
        return this.k;
    }

    /* renamed from: a  reason: collision with other method in class */
    public void m2a() {
        d.d("onBillingfinish", "code=" + this.k + "." + PurchaseCode.getReason(this.k));
        j.a().p();
        c.d((Boolean) true);
        c.unlock();
        r.q();
        c.f(this.k);
        mm.sms.purchasesdk.sms.b.a(true);
        this.f11a.onBillingFinish(this.k, this.f10a);
    }

    public void a(int i) {
        this.k = i;
    }

    public void a(int i, HashMap hashMap) {
        d.d("onDialogShow", "code=" + i);
        this.f10a = hashMap;
        j.a().a(i, this, this.a, this.b, hashMap);
        c.f(i);
    }

    public void a(HashMap hashMap) {
        this.f10a = hashMap;
    }

    public void onInitFinish(int i) {
        d.d("onInitFinish", "code=" + i + "." + PurchaseCode.getReason(i));
        c.unlock();
        c.f(this.k);
        this.f11a.onInitFinish(i);
    }
}
