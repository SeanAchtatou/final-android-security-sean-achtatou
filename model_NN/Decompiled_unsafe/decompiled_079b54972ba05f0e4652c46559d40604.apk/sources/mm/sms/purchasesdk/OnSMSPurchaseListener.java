package mm.sms.purchasesdk;

import java.util.HashMap;

public interface OnSMSPurchaseListener {
    public static final String PAYCODE = "Paycode";
    public static final String TRADEID = "TradeID";

    void onBillingFinish(int i, HashMap hashMap);

    void onInitFinish(int i);
}
