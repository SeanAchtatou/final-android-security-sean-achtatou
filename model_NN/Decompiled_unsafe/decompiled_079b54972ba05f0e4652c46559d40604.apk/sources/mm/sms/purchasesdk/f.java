package mm.sms.purchasesdk;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import mm.sms.purchasesdk.f.c;
import mm.sms.purchasesdk.f.d;

class f extends Handler {
    final /* synthetic */ e b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    f(e eVar, Looper looper) {
        super(looper);
        this.b = eVar;
    }

    public void handleMessage(Message message) {
        b bVar = (b) message.obj;
        d.c("TaskThread", "ReqHandler Handler id:" + Thread.currentThread().getId());
        d.c("TaskThread", "ReqHandler Handler name:" + Thread.currentThread().getName());
        boolean unused = e.b = false;
        switch (message.what) {
            case 0:
                this.b.a(bVar);
                break;
            case 2:
                d.c("TaskThread", "ORDER_REQUEST ");
                int i = message.arg1;
                message.getData();
                c.f(-1);
                if (i != 0) {
                    boolean unused2 = e.b = true;
                    this.b.c(bVar);
                    break;
                } else {
                    this.b.b(bVar);
                    break;
                }
            case 7:
                this.b.a(bVar, 1000);
                break;
        }
        super.handleMessage(message);
    }
}
