package mm.sms.purchasesdk.e;

import android.app.Activity;
import android.view.View;
import mm.sms.purchasesdk.c.a;
import mm.sms.purchasesdk.e;
import mm.sms.purchasesdk.f.c;
import mm.sms.purchasesdk.f.d;

class p implements View.OnClickListener {
    final /* synthetic */ n b;

    p(n nVar) {
        this.b = nVar;
    }

    public void onClick(View view) {
        if (((Activity) c.getContext()).isFinishing()) {
            d.d("SavingDialog", "Activity is finished!");
            return;
        }
        c.j("3");
        a.d();
        a.n = c.q();
        j.a().a(this.b.a, this.b.b, this.b.f24a, e.a());
        this.b.dismiss();
        this.b.f25a.k();
    }
}
