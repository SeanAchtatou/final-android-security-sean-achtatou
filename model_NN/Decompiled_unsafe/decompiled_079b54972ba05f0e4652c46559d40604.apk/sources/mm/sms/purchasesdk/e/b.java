package mm.sms.purchasesdk.e;

import android.content.Context;
import android.os.Handler;
import mm.sms.purchasesdk.PurchaseCode;
import mm.sms.purchasesdk.a.a;
import mm.sms.purchasesdk.a.c;

public class b {
    private final String TAG = "DialogManager";
    private Handler a;

    /* renamed from: a  reason: collision with other field name */
    private c f14a;

    /* renamed from: a  reason: collision with other field name */
    private mm.sms.purchasesdk.b f15a;

    /* renamed from: a  reason: collision with other field name */
    private a f16a;

    /* renamed from: a  reason: collision with other field name */
    private f f17a;

    /* renamed from: a  reason: collision with other field name */
    private g f18a;

    /* renamed from: a  reason: collision with other field name */
    private k f19a;

    /* renamed from: a  reason: collision with other field name */
    private n f20a;

    /* renamed from: a  reason: collision with other field name */
    private q f21a;
    private Handler b;

    /* renamed from: b  reason: collision with other field name */
    private a f22b;
    private final String k = "";
    private Context mContext;

    public b(Context context) {
        this.f16a = new a(context);
        this.mContext = context;
        b(1);
    }

    public Handler a() {
        return this.a;
    }

    /* renamed from: a  reason: collision with other method in class */
    public mm.sms.purchasesdk.b m9a() {
        return this.f15a;
    }

    public void a(a aVar) {
        this.f22b = aVar;
    }

    public Handler b() {
        return this.b;
    }

    /* renamed from: b  reason: collision with other method in class */
    public a m10b() {
        return this.f22b;
    }

    public void b(int i) {
        r.a(r.t, r.u);
    }

    public void b(Handler handler) {
        this.b = handler;
    }

    public void c(int i) {
        d dVar;
        c cVar = new c();
        e eVar = new e();
        eVar.s = "Result";
        eVar.r = "结果：";
        e eVar2 = new e();
        eVar2.s = "ResultMSG";
        eVar2.r = "结果信息：";
        if (1001 == 1001 || 1001 == 1214 || 1001 == 1000) {
            dVar = this.f16a.m8a(r.y);
            eVar.mValue = "支付请求发送成功";
            eVar2.mValue = "" + PurchaseCode.getDescription(PurchaseCode.ORDER_OK);
        } else {
            dVar = this.f16a.m8a(r.z);
            eVar.mValue = "" + mm.sms.purchasesdk.f.c.m29o();
            eVar2.mValue = "" + PurchaseCode.getDescription(PurchaseCode.ORDER_OK);
        }
        this.mContext = mm.sms.purchasesdk.f.c.getContext();
        if (dVar.getHeight() == -1 || dVar.getWidth() == -1) {
            this.f19a = new k(this.mContext, dVar, 16973829, this);
        } else {
            this.f19a = new k(this.mContext, dVar, this);
        }
        cVar.c(eVar.s);
        cVar.a(eVar.s, eVar);
        cVar.c(eVar2.s);
        cVar.a(eVar2.s, eVar2);
        this.f19a.b(cVar.a());
        this.f19a.show();
    }

    public void c(Handler handler) {
        this.a = handler;
    }

    public void d(mm.sms.purchasesdk.b bVar) {
        this.f15a = bVar;
    }

    public void e() {
        d a2 = this.f16a.m8a(r.w);
        if (this.f17a == null) {
            if (a2.getHeight() == -1 || a2.getWidth() == -1) {
                this.f17a = new f(this.mContext, a2, 16973829, this);
            } else {
                this.f17a = new f(this.mContext, a2, this);
            }
            if (!(this.f22b == null || this.f22b.a() == null)) {
                this.f14a = this.f22b.a();
                this.f17a.b(this.f14a.a());
            }
        } else if (this.f17a.isShowing()) {
            if (this.f22b != null && this.f22b.a() != null) {
                this.f14a = this.f22b.a();
                this.f17a.b(this.f14a.a());
                return;
            }
            return;
        }
        this.f17a.show();
    }

    public void f() {
        d a2 = this.f16a.m8a(r.A);
        this.mContext = mm.sms.purchasesdk.f.c.getContext();
        if (a2.getHeight() == -1 || a2.getWidth() == -1) {
            this.f21a = new q(this.mContext, a2, 16973829, this);
        } else {
            this.f21a = new q(this.mContext, a2, this);
        }
        this.f21a.show();
    }

    public void g() {
        this.mContext = mm.sms.purchasesdk.f.c.getContext();
        d a2 = this.f16a.m8a(r.x);
        if (a2.getHeight() == -1 || a2.getWidth() == -1) {
            this.f20a = new n(this.mContext, a2, 16973829, this);
        } else {
            this.f20a = new n(this.mContext, a2, this);
        }
        if (!(this.f22b == null || this.f22b.a() == null)) {
            this.f14a = this.f22b.a();
            this.f20a.b(this.f14a.a());
        }
        this.f20a.show();
    }

    public void h() {
        this.mContext = mm.sms.purchasesdk.f.c.getContext();
        d a2 = this.f16a.m8a(r.v);
        if (a2.getHeight() == -1 || a2.getWidth() == -1) {
            this.f18a = new g(this.mContext, a2, 16973829, this);
        } else {
            this.f18a = new g(this.mContext, a2, this);
        }
        this.f14a = this.f22b.a();
        this.f18a.b(this.f14a.a());
        this.f18a.show();
    }

    public void i() {
        if (this.f17a != null && this.f17a.isShowing()) {
            this.f17a.dismiss();
        }
    }

    public void j() {
        if (this.f18a != null && this.f18a.isShowing()) {
            this.f18a.dismiss();
        }
    }

    public void k() {
        if (this.f20a != null && this.f20a.isShowing()) {
            this.f20a.dismiss();
        }
    }

    public void l() {
        if (this.f21a != null && this.f21a.isShowing()) {
            this.f21a.dismiss();
        }
    }

    public void m() {
        if (this.f17a != null && this.f17a.isShowing()) {
            this.f17a.dismiss();
        }
        if (this.f20a != null && this.f20a.isShowing()) {
            this.f20a.dismiss();
        }
        if (this.f19a != null && this.f19a.isShowing()) {
            this.f19a.dismiss();
        }
        if (this.f18a != null && this.f18a.isShowing()) {
            this.f18a.dismiss();
        }
        if (this.f21a != null && this.f21a.isShowing()) {
            this.f21a.dismiss();
        }
        this.f17a = null;
        this.f20a = null;
        this.f19a = null;
        this.f18a = null;
        this.f21a = null;
    }
}
