package mm.sms.purchasesdk.e;

import android.content.Context;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class a {
    private d b;
    private d c;
    private d d;
    private d e;
    private Context mContext;

    public a(Context context) {
        this.mContext = context;
    }

    private InputStream a(String str) {
        try {
            return this.mContext.getResources().getAssets().open(str);
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private HashMap a(Element element) {
        NamedNodeMap attributes = element.getAttributes();
        int length = attributes.getLength();
        HashMap hashMap = new HashMap();
        for (int i = 0; i < length; i++) {
            Node item = attributes.item(i);
            hashMap.put(item.getNodeName(), item.getNodeValue());
        }
        return hashMap;
    }

    private d a(InputStream inputStream, d dVar) {
        if (inputStream != null) {
            Element documentElement = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(inputStream).getDocumentElement();
            if (this.b == null) {
                this.b = new d();
            }
            this.b.c(a(documentElement));
            a(documentElement, this.b);
        }
        return this.b;
    }

    private void a(Element element, d dVar) {
        HashMap hashMap = new HashMap();
        ArrayList arrayList = new ArrayList();
        NodeList childNodes = element.getChildNodes();
        int length = childNodes.getLength();
        for (int i = 0; i < length; i++) {
            Node item = childNodes.item(i);
            if (item instanceof Element) {
                Element element2 = (Element) item;
                d dVar2 = new d();
                dVar2.c(a(element2));
                arrayList.add(dVar2.m24h());
                hashMap.put(dVar2.m24h(), dVar2);
                dVar.d(hashMap);
                dVar.a(arrayList);
                if (element2.getChildNodes().getLength() > 0) {
                    a(element2, dVar2);
                }
            }
        }
    }

    private d b(String str) {
        if ("progressdialog.xml".equals(str)) {
            return this.c;
        }
        if ("sucresultdialog.xml".equals(str)) {
            return this.d;
        }
        if ("purchasedialog.xml".equals(str)) {
            return this.e;
        }
        return null;
    }

    /* renamed from: a  reason: collision with other method in class */
    public d m8a(String str) {
        this.b = b(str);
        if (this.b == null) {
            try {
                a(a(str), this.b);
            } catch (SAXException e2) {
                e2.printStackTrace();
            } catch (IOException e3) {
                e3.printStackTrace();
            } catch (ParserConfigurationException e4) {
                e4.printStackTrace();
            }
        }
        return this.b;
    }
}
