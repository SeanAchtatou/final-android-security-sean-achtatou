package mm.sms.purchasesdk.e;

import android.app.Activity;
import android.os.Handler;
import java.util.HashMap;
import mm.sms.purchasesdk.a.a;
import mm.sms.purchasesdk.b;
import mm.sms.purchasesdk.f.c;

public class j {
    private static j a = null;

    /* renamed from: a  reason: collision with other field name */
    private b f36a = new b(c.getContext());

    private j() {
    }

    public static j a() {
        if (a == null) {
            a = new j();
        }
        return a;
    }

    public void a(int i, b bVar, Handler handler, Handler handler2, HashMap hashMap) {
        if (!((Activity) c.getContext()).isFinishing()) {
            this.f36a.i();
            bVar.a(i);
            this.f36a.c(handler);
            this.f36a.b(handler2);
            this.f36a.d(bVar);
            this.f36a.c(i);
        }
    }

    public void a(Handler handler, Handler handler2, b bVar) {
        if (!((Activity) c.getContext()).isFinishing()) {
            this.f36a.c(handler);
            this.f36a.b(handler2);
            this.f36a.d(bVar);
            this.f36a.f();
        }
    }

    public void a(Handler handler, Handler handler2, b bVar, a aVar) {
        if (!((Activity) c.getContext()).isFinishing()) {
            this.f36a.c(handler);
            this.f36a.b(handler2);
            this.f36a.d(bVar);
            this.f36a.a(aVar);
            this.f36a.h();
        }
    }

    public void b(a aVar) {
        if (!((Activity) c.getContext()).isFinishing()) {
            this.f36a.a(aVar);
            this.f36a.e();
        }
    }

    public void o() {
        this.f36a.i();
    }

    public void p() {
        this.f36a.m();
    }
}
