package mm.sms.purchasesdk.e;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.View;

public class q extends c {
    private final String TAG = "SplashDialog";
    private Drawable d = null;
    private Drawable e = null;

    class a implements Runnable {
        a() {
        }

        public void run() {
            q.this.f25a.l();
        }
    }

    public q(Context context, d dVar, int i, b bVar) {
        super(context, dVar, i, bVar);
    }

    public q(Context context, d dVar, b bVar) {
        super(context, dVar, bVar);
    }

    /* access modifiers changed from: protected */
    public View a() {
        this.p = this.f26a.getWidth();
        this.q = this.f26a.getHeight();
        return b((d) this.f26a.m21b().get(this.f26a.m17a().get(0)));
    }

    public void show() {
        new Handler().postDelayed(new a(), 3000);
        super.show();
    }
}
