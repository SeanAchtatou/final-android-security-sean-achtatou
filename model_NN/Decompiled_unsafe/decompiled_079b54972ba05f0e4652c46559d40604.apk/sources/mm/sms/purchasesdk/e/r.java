package mm.sms.purchasesdk.e;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import mm.sms.purchasesdk.f.c;

public class r {
    public static String A = (u + "splashdialog.xml");
    public static float b = 1.0f;
    private static ArrayList c = new ArrayList();
    private static HashMap d = new HashMap();
    public static String t = "mmiap/smsimage/";
    public static String u = "mmiap/splash/";
    public static String v = (t + "purchasedialog.xml");
    public static String w = (t + "progressdialog.xml");
    public static String x = (t + "savingdialog.xml");
    public static String y = (t + "sucresultdialog.xml");
    public static String z = (t + "failresultdialog.xml");

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public static Bitmap a(float f, float f2, Bitmap bitmap) {
        Matrix matrix = new Matrix();
        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        if (height > 1 && width > 1) {
            matrix.postScale(f, f2);
            return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        } else if (height <= 1) {
            matrix.postScale(f, 1.0f);
            return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        } else if (width > 1) {
            return bitmap;
        } else {
            matrix.postScale(1.0f, f2);
            return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
        }
    }

    public static Bitmap a(Bitmap bitmap) {
        float f;
        if (c.c > 1.0f) {
            f = b;
        } else if (c.c >= 1.0f) {
            return bitmap;
        } else {
            f = b;
        }
        return a(f, f, bitmap);
    }

    public static void a(String str, String str2) {
        v = str + "purchasedialog.xml";
        w = str + "progressdialog.xml";
        x = str + "savingdialog.xml";
        y = str + "sucresultdialog.xml";
        z = str + "failresultdialog.xml";
        A = str2 + "splashdialog.xml";
    }

    private static boolean a(String str) {
        return c.contains(str);
    }

    public static Bitmap b(Context context, String str) {
        return c(context, str);
    }

    public static Bitmap c(Context context, String str) {
        if (d == null) {
            d = new HashMap();
        }
        Bitmap bitmap = (Bitmap) d.get(str);
        if (bitmap != null) {
            return bitmap;
        }
        try {
            InputStream open = context.getAssets().open(str);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            Bitmap decodeStream = BitmapFactory.decodeStream(open, null, options);
            if (a(str)) {
                Bitmap a = a(decodeStream);
                if (a != decodeStream) {
                    decodeStream.recycle();
                    System.gc();
                }
                d.put(str, a);
                return a;
            }
            d.put(str, decodeStream);
            return decodeStream;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void q() {
    }

    public static void r() {
        if (c.size() != 0) {
            c.clear();
        }
    }
}
