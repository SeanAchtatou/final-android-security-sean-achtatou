package mm.sms.purchasesdk.e;

import android.os.Bundle;
import android.os.Message;
import android.view.View;
import mm.sms.purchasesdk.c.a;

class h implements View.OnClickListener {
    final /* synthetic */ g b;

    h(g gVar) {
        this.b = gVar;
    }

    public void onClick(View view) {
        a.m5b();
        a.n = 3;
        Bundle bundle = new Bundle();
        bundle.putInt("OrderCount", this.b.f29b.b());
        bundle.putBoolean("multiSubs", this.b.f29b.m1b());
        Message obtainMessage = this.b.a.obtainMessage();
        obtainMessage.what = 2;
        obtainMessage.obj = this.b.f24a;
        obtainMessage.arg1 = 1;
        obtainMessage.setData(bundle);
        obtainMessage.sendToTarget();
        j.a().b(this.b.f29b);
        this.b.dismiss();
    }
}
