package mm.sms.purchasesdk.e;

import com.a.a.q.c;
import java.util.ArrayList;
import java.util.HashMap;

public class d {
    private int A;
    private final int B;
    private float a;

    /* renamed from: a  reason: collision with other field name */
    private String f30a;

    /* renamed from: a  reason: collision with other field name */
    private int[] f31a;
    private ArrayList b;
    private HashMap c;
    private int e;
    private int f;

    /* renamed from: f  reason: collision with other field name */
    private Boolean f32f;
    private int g;

    /* renamed from: g  reason: collision with other field name */
    private Boolean f33g;
    private int h;
    private int i;
    private int j;
    private String l;
    private String m;
    private String n;
    private String o;
    private int p;

    /* renamed from: p  reason: collision with other field name */
    private String f34p;
    private int q;

    /* renamed from: q  reason: collision with other field name */
    private String f35q;
    private int r;
    private int s;
    private int t;
    private int u;
    private int v;
    private int w;
    private int x;
    private int y;
    private int z;

    public d() {
        this.f32f = false;
        this.c = null;
        this.b = null;
        this.f33g = false;
        this.a = 1.0f;
        this.B = 64;
        this.c = new HashMap();
        this.b = new ArrayList();
    }

    private int b(String str) {
        if (str.equals("center")) {
            return 17;
        }
        if (str.equals("top")) {
            return 48;
        }
        if (str.equals("bottom")) {
            return 80;
        }
        if (str.equals("left")) {
            return 3;
        }
        if (str.equals("right")) {
            return 5;
        }
        if (str.equals("center_horizontal")) {
            return 1;
        }
        if (str.equals("center_vertical")) {
            return 16;
        }
        if (str.equals("r_center_vertical")) {
            return 15;
        }
        if (str.equals("center_in_parent")) {
            return 13;
        }
        if (str.equals("align_parent_left")) {
            return 9;
        }
        if (str.equals("align_parent_right")) {
            return 11;
        }
        if (str.equals("align_parent_bottom")) {
            return 12;
        }
        return str.equals("r_center_horizontal") ? 14 : -1;
    }

    private int d(String str) {
        if (str != null) {
            return (int) (((float) new Integer(str).intValue()) * r.b);
        }
        return 0;
    }

    public float a() {
        return this.a;
    }

    public float a(String str) {
        if (str == null || str.trim().length() <= 0) {
            return 1.0f;
        }
        return new Float(str).floatValue();
    }

    /* renamed from: a  reason: collision with other method in class */
    public int m13a(String str) {
        if (str != null) {
            return new Integer(str).intValue();
        }
        return 0;
    }

    /* renamed from: a  reason: collision with other method in class */
    public Boolean m14a() {
        return this.f32f;
    }

    /* renamed from: a  reason: collision with other method in class */
    public Boolean m15a(String str) {
        return str != null && str.equals("true");
    }

    /* renamed from: a  reason: collision with other method in class */
    public String m16a() {
        return this.l;
    }

    /* renamed from: a  reason: collision with other method in class */
    public ArrayList m17a() {
        return this.b;
    }

    public void a(ArrayList arrayList) {
        this.b = arrayList;
    }

    /* renamed from: a  reason: collision with other method in class */
    public int[] m18a() {
        return this.f31a;
    }

    /* renamed from: a  reason: collision with other method in class */
    public int[] m19a(String str) {
        if (str == null) {
            this.f31a = new int[1];
            this.f31a[0] = 17;
            return this.f31a;
        } else if (str.contains("|")) {
            String[] split = str.split("\\|");
            b(split[0]);
            int length = split.length;
            this.f31a = new int[length];
            for (int i2 = 0; i2 < length; i2++) {
                if (i2 < 64) {
                    this.f31a[i2] = b(split[i2]);
                }
            }
            return this.f31a;
        } else {
            this.f31a = new int[1];
            this.f31a[0] = b(str);
            return this.f31a;
        }
    }

    public Boolean b() {
        return this.f33g;
    }

    /* renamed from: b  reason: collision with other method in class */
    public Boolean m20b(String str) {
        if (str == null || str.trim().length() <= 0) {
            return false;
        }
        return str.equals("true");
    }

    /* renamed from: b  reason: collision with other method in class */
    public HashMap m21b() {
        return this.c;
    }

    public int c(String str) {
        if (str == null) {
            this.z = 0;
        } else if (str.equals("vertical")) {
            this.z = 1;
        } else {
            this.z = 0;
        }
        return this.z;
    }

    public void c(HashMap hashMap) {
        this.m = (String) hashMap.get("background");
        this.n = (String) hashMap.get("pressedBG");
        this.o = (String) hashMap.get("focusedBG");
        this.f35q = (String) hashMap.get("bitmap");
        this.f34p = (String) hashMap.get("content");
        this.q = e((String) hashMap.get("height"));
        this.f32f = m15a((String) hashMap.get("clickable"));
        this.p = f((String) hashMap.get("width"));
        this.w = d((String) hashMap.get("margin"));
        this.h = d((String) hashMap.get("margingBottom"));
        this.e = d((String) hashMap.get("margingLeft"));
        this.f = d((String) hashMap.get("margingRight"));
        this.g = d((String) hashMap.get("margingTop"));
        this.z = c((String) hashMap.get("orientation"));
        this.r = d((String) hashMap.get("padding"));
        this.v = d((String) hashMap.get("paddingBottom"));
        this.s = d((String) hashMap.get("paddingLeft"));
        this.t = d((String) hashMap.get("paddingRight"));
        this.u = d((String) hashMap.get("paddingTop"));
        this.l = (String) hashMap.get("stytle");
        this.f30a = (String) hashMap.get(c.TEXT_MESSAGE);
        this.f31a = m19a((String) hashMap.get("gravity"));
        this.i = g((String) hashMap.get("textsize"));
        this.j = h((String) hashMap.get("textcolor"));
        this.x = d((String) hashMap.get("x"));
        this.y = d((String) hashMap.get("y"));
        this.A = m13a((String) hashMap.get("weight"));
        this.f33g = m20b((String) hashMap.get("singleline"));
        this.a = a((String) hashMap.get("alpha"));
    }

    public void d(HashMap hashMap) {
        this.c = hashMap;
    }

    public int e(String str) {
        if (str == null) {
            return 0;
        }
        if (str.trim().equals("fill_parent")) {
            this.q = -1;
        } else if (str.trim().equals("wrap_content")) {
            this.q = -2;
        } else {
            this.q = d(str.trim());
        }
        return this.q;
    }

    public String e() {
        return this.n;
    }

    public int f() {
        return this.A;
    }

    public int f(String str) {
        if (str == null) {
            return 0;
        }
        if (str.trim().equals("fill_parent")) {
            this.p = -1;
        } else if (str.trim().equals("wrap_content")) {
            this.p = -2;
        } else {
            this.p = d(str.trim());
        }
        return this.p;
    }

    /* renamed from: f  reason: collision with other method in class */
    public String m22f() {
        return this.o;
    }

    public int g() {
        return this.e;
    }

    public int g(String str) {
        if (str != null) {
            return new Integer(str).intValue();
        }
        return 0;
    }

    /* renamed from: g  reason: collision with other method in class */
    public String m23g() {
        if (this.f35q == null || this.f35q.trim().length() <= 0) {
            return null;
        }
        return this.f35q;
    }

    public int getBottomPadding() {
        return this.v;
    }

    public int getHeight() {
        return this.q;
    }

    public int getOrientation() {
        return this.z;
    }

    public String getText() {
        return this.f30a;
    }

    public int getTextSize() {
        return this.i;
    }

    public int getTopPadding() {
        return this.u;
    }

    public int getWidth() {
        return this.p;
    }

    public int h() {
        return this.f;
    }

    public int h(String str) {
        if (str == null || str.trim().length() <= 0) {
            return -16777216;
        }
        return Long.valueOf(Long.parseLong(str.substring(str.indexOf("#") + 1), 16)).intValue();
    }

    /* renamed from: h  reason: collision with other method in class */
    public String m24h() {
        return this.f34p;
    }

    public int i() {
        return this.g;
    }

    /* renamed from: i  reason: collision with other method in class */
    public String m25i() {
        return this.m;
    }

    public int j() {
        return this.h;
    }

    public int k() {
        return this.s;
    }

    public int l() {
        return this.t;
    }

    public int m() {
        return this.j;
    }
}
