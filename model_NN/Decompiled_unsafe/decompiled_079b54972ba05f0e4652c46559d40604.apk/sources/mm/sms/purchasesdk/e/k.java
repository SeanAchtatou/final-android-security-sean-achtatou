package mm.sms.purchasesdk.e;

import android.content.Context;
import android.view.View;

public class k extends c {
    /* access modifiers changed from: private */
    public int E;

    public k(Context context, d dVar, int i, b bVar) {
        super(context, dVar, i, bVar);
        n();
    }

    public k(Context context, d dVar, b bVar) {
        super(context, dVar, bVar);
        n();
    }

    private void n() {
        this.f23a = new l(this);
        this.f27b = new m(this);
    }

    /* access modifiers changed from: protected */
    public View a() {
        this.p = this.f26a.getWidth();
        this.q = this.f26a.getHeight();
        return b((d) this.f26a.m21b().get(this.f26a.m17a().get(0)));
    }
}
