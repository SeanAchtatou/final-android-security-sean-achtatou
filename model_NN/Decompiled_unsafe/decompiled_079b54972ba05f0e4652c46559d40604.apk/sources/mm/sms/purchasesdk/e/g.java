package mm.sms.purchasesdk.e;

import android.content.Context;
import android.view.View;

public class g extends c {
    private final String TAG = "PurchaseDialog";

    public g(Context context, d dVar, int i, b bVar) {
        super(context, dVar, i, bVar);
        n();
    }

    public g(Context context, d dVar, b bVar) {
        super(context, dVar, bVar);
        n();
    }

    private void n() {
        this.f23a = new h(this);
        this.f27b = new i(this);
    }

    /* access modifiers changed from: protected */
    public View a() {
        this.p = this.f26a.getWidth();
        this.q = this.f26a.getHeight();
        return b((d) this.f26a.m21b().get(this.f26a.m17a().get(0)));
    }

    public void show() {
        super.show();
        j.a().o();
    }
}
