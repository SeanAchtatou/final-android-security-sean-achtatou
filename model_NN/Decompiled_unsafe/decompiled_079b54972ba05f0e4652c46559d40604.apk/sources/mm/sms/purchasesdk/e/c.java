package mm.sms.purchasesdk.e;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import com.sms.purchasesdk.view.f;
import com.sms.purchasesdk.view.g;
import com.sms.purchasesdk.view.h;
import com.sms.purchasesdk.view.i;
import com.sms.purchasesdk.view.j;
import com.sms.purchasesdk.view.k;
import java.util.HashMap;
import mm.sms.purchasesdk.a.a;
import mm.sms.purchasesdk.b;
import mm.sms.purchasesdk.f.d;

public abstract class c extends Dialog {
    private final String TAG = "IAPDialog";
    public Handler a;

    /* renamed from: a  reason: collision with other field name */
    public View.OnClickListener f23a;

    /* renamed from: a  reason: collision with other field name */
    public b f24a;

    /* renamed from: a  reason: collision with other field name */
    public b f25a;

    /* renamed from: a  reason: collision with other field name */
    public d f26a;
    public Handler b;

    /* renamed from: b  reason: collision with other field name */
    public View.OnClickListener f27b;

    /* renamed from: b  reason: collision with other field name */
    public HashMap f28b;

    /* renamed from: b  reason: collision with other field name */
    public a f29b;
    public Boolean f = true;
    public Context mContext;
    public int p = 450;
    public int q = 280;

    public c(Context context, int i) {
        super(context, i);
        getWindow().requestFeature(1);
        this.mContext = context;
    }

    public c(Context context, d dVar, int i, b bVar) {
        super(context, i);
        getWindow().requestFeature(1);
        this.f26a = dVar;
        if (this.f26a.a() != 1.0f) {
            WindowManager.LayoutParams attributes = getWindow().getAttributes();
            attributes.alpha = this.f26a.a();
            getWindow().setAttributes(attributes);
        }
        this.mContext = context;
        this.f25a = bVar;
        this.a = bVar.a();
        this.b = bVar.b();
        this.f24a = bVar.m9a();
        this.f29b = bVar.m10b();
    }

    public c(Context context, d dVar, b bVar) {
        super(context);
        getWindow().requestFeature(1);
        this.f26a = dVar;
        this.mContext = context;
        this.f25a = bVar;
        this.a = bVar.a();
        this.b = bVar.b();
        this.f24a = bVar.m9a();
        this.f29b = bVar.m10b();
    }

    private e a(String str) {
        d.c("IAPDialog", "content =  " + str);
        if (this.f28b != null) {
            return (e) this.f28b.get(str);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public abstract View a();

    /* access modifiers changed from: protected */
    public View a(d dVar) {
        return b(dVar);
    }

    /* renamed from: a  reason: collision with other method in class */
    public LinearLayout m11a(d dVar) {
        LinearLayout linearLayout = (LinearLayout) new f(dVar, this.mContext).a();
        int size = dVar.m21b().size();
        for (int i = 0; i < size; i++) {
            d dVar2 = (d) dVar.m21b().get((String) dVar.m17a().get(i));
            String h = dVar2.m24h();
            View a2 = a(dVar2);
            if (h != null) {
                if (!h.equals("backbutton") || this.f27b == null) {
                    if (h.equals("confirmButton") && this.f23a != null) {
                        if (this.f.booleanValue()) {
                            a2.setOnClickListener(this.f23a);
                        } else {
                            a2.setClickable(false);
                        }
                    }
                } else if (this.f.booleanValue()) {
                    a2.setOnClickListener(this.f27b);
                } else {
                    a2.setClickable(false);
                }
                if (a2 != null) {
                    linearLayout.addView(a2);
                }
            }
        }
        return linearLayout;
    }

    /* renamed from: a  reason: collision with other method in class */
    public RelativeLayout m12a(d dVar) {
        RelativeLayout relativeLayout = (RelativeLayout) new i(dVar, this.mContext).a();
        int size = dVar.m21b().size();
        for (int i = 0; i < size; i++) {
            d dVar2 = (d) dVar.m21b().get((String) dVar.m17a().get(i));
            String h = dVar2.m24h();
            View a2 = a(dVar2);
            if (!h.equals("backbutton") || this.f27b == null) {
                if (h.equals("confirmButton") && this.f23a != null) {
                    if (this.f.booleanValue()) {
                        a2.setOnClickListener(this.f23a);
                    } else {
                        a2.setClickable(this.f.booleanValue());
                    }
                }
            } else if (this.f.booleanValue()) {
                a2.setOnClickListener(this.f27b);
            } else {
                a2.setClickable(this.f.booleanValue());
            }
            if (a2 != null) {
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(dVar2.getWidth(), dVar2.getHeight());
                int[] a3 = dVar2.m18a();
                for (int addRule : a3) {
                    layoutParams.addRule(addRule, -1);
                }
                relativeLayout.addView(a2, layoutParams);
            } else {
                relativeLayout.addView(a2);
            }
        }
        return relativeLayout;
    }

    public View b(d dVar) {
        String a2 = dVar.m16a();
        if (a2.equals("LinearLayout")) {
            return m11a(dVar);
        }
        if (a2.equals("Button")) {
            return d(dVar);
        }
        if (a2.equals("TextView")) {
            return e(dVar);
        }
        if (a2.equals("ScrollView")) {
            return g(dVar);
        }
        if (a2.equals("ImageView")) {
            return h(dVar);
        }
        if (a2.equals("RelativeLayout")) {
            return m12a(dVar);
        }
        if (a2.equals("ProductItemView")) {
            return f(dVar);
        }
        if (a2.equals("ProgressBar")) {
            return c(dVar);
        }
        return null;
    }

    public void b(HashMap hashMap) {
        this.f28b = hashMap;
        setContentView(a());
    }

    public View c(d dVar) {
        return new h(dVar, this.mContext).a();
    }

    public View d(d dVar) {
        return new com.sms.purchasesdk.view.a(dVar, this.mContext).a();
    }

    public View e(d dVar) {
        k kVar = new k(dVar, this.mContext);
        String h = dVar.m24h();
        e a2 = a(dVar.m24h());
        if (a2 != null) {
            kVar.f2a = a2.mValue;
        }
        int size = dVar.m21b().size();
        if (kVar.f2a != null && kVar.f2a.trim().length() > 0) {
            StringBuilder sb = new StringBuilder(kVar.f2a);
            for (int i = 0; i < size; i++) {
                d dVar2 = (d) dVar.m21b().get((String) dVar.m17a().get(i));
                e a3 = a(dVar2.m24h());
                if (a3 != null) {
                    sb.append(a3.mValue);
                } else {
                    sb.append(dVar2.getText());
                }
            }
            kVar.f2a = sb.toString();
        }
        if (h != null && h.equals("SMALLTitle") && mm.sms.purchasesdk.f.c.c().booleanValue()) {
            kVar.f2a += "(自测试)";
        }
        return kVar.a();
    }

    public View f(d dVar) {
        g gVar = new g(dVar, this.mContext);
        gVar.a(a(dVar.m24h()));
        return gVar.a();
    }

    public View g(d dVar) {
        ScrollView scrollView = (ScrollView) new j(dVar, this.mContext).a();
        int size = dVar.m21b().size();
        for (int i = 0; i < size; i++) {
            d dVar2 = (d) dVar.m21b().get((String) dVar.m17a().get(i));
            String h = dVar2.m24h();
            View a2 = a(dVar2);
            if (!h.equals("backbutton") || this.f27b == null) {
                if (h.equals("confirmButton") && this.f23a != null) {
                    if (this.f.booleanValue()) {
                        a2.setOnClickListener(this.f23a);
                    } else {
                        a2.setClickable(false);
                    }
                }
            } else if (this.f.booleanValue()) {
                a2.setOnClickListener(this.f27b);
            } else {
                a2.setClickable(false);
            }
            if (a2 != null) {
                scrollView.addView(a2);
            }
        }
        return scrollView;
    }

    public View h(d dVar) {
        return new com.sms.purchasesdk.view.c(dVar, this.mContext).a();
    }

    public void show() {
        setContentView(a());
        getWindow().setLayout(this.p, this.q);
        setCancelable(false);
        super.show();
    }
}
