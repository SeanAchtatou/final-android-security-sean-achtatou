package mm.sms.purchasesdk.e;

import android.os.Message;
import android.view.View;
import java.util.HashMap;
import mm.sms.purchasesdk.PurchaseCode;
import mm.sms.purchasesdk.c.a;
import mm.sms.purchasesdk.f.c;

class o implements View.OnClickListener {
    final /* synthetic */ n b;

    o(n nVar) {
        this.b = nVar;
    }

    public void onClick(View view) {
        this.b.f25a.m();
        c.j("2");
        a.d();
        a.n = c.q();
        this.b.f24a.a((int) PurchaseCode.BILL_CANCEL_FAIL);
        this.b.f24a.a((HashMap) null);
        Message obtainMessage = this.b.b.obtainMessage();
        obtainMessage.what = 5;
        obtainMessage.obj = this.b.f24a;
        obtainMessage.sendToTarget();
    }
}
