package mm.sms.purchasesdk;

import android.app.Activity;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.widget.Toast;
import mm.sms.purchasesdk.e.j;
import mm.sms.purchasesdk.f.c;
import mm.sms.purchasesdk.f.d;
import mm.sms.purchasesdk.fingerprint.IdentifyApp;
import mm.sms.purchasesdk.sms.SMSReceiver;
import mm.sms.purchasesdk.sms.a;

public class SMSPurchase {
    public static final String TAG = SMSPurchase.class.getSimpleName();
    private static SMSPurchase a;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with other field name */
    public Handler f4a;

    /* renamed from: a  reason: collision with other field name */
    private HandlerThread f5a;

    /* renamed from: a  reason: collision with other field name */
    private e f6a = new e("purchase-task");

    /* renamed from: a  reason: collision with other field name */
    private SMSReceiver f7a = null;
    /* access modifiers changed from: private */
    public Handler b;

    private SMSPurchase() {
        this.f6a.start();
        this.f6a.init();
        this.f5a = new HandlerThread("Response-thread");
        this.f5a.start();
        this.f7a = new SMSReceiver();
        this.b = new d(this, this.f5a.getLooper());
        this.f6a.a(this.b);
        this.f4a = this.f6a.m7a();
    }

    private synchronized void a(Context context, String str, int i, boolean z, OnSMSPurchaseListener onSMSPurchaseListener, String str2) {
        d.c(TAG, "enter order ");
        if (context == null || !(context instanceof Activity)) {
            throw new Exception("Context Object is null or Context Object is not instance of Activity");
        }
        a(context);
        if (onSMSPurchaseListener == null) {
            throw new Exception("OnPurchaseListener Object is null");
        } else if (str == null || str.trim().length() == 0) {
            throw new Exception("Paycode is null");
        } else if (i <= 0) {
            throw new Exception("orderCout must be greater than 0 ");
        } else if (str2 == null || str2.trim().length() <= 16) {
            if (str2 == null) {
                c.d("");
            } else {
                c.d(str2);
            }
            if (c.a(2)) {
                Toast.makeText(context, "尊敬的用户，已有其他支付请求正在发送。", 0).show();
            } else {
                d.c(TAG, "enter order 1");
                c.setContext(context);
                c.b(str);
                c.e(i);
                c.m33t();
                b bVar = new b(onSMSPurchaseListener, this.f4a, this.b);
                if (this.f7a == null) {
                    this.f7a = new SMSReceiver();
                }
                int a2 = c.a(context);
                if (a2 != 0) {
                    bVar.a(a2, null);
                } else {
                    int b2 = c.b(context);
                    if (b2 != 1000) {
                        bVar.a(b2, null);
                    } else {
                        c.h(c.x());
                        Message obtainMessage = this.f4a.obtainMessage();
                        obtainMessage.what = 0;
                        obtainMessage.obj = bVar;
                        obtainMessage.sendToTarget();
                        d.c(TAG, "leave order ");
                    }
                }
            }
        } else {
            throw new Exception("UserData Error! UserData's length must be greater than 16.You have input " + str2.length() + " bytes");
        }
    }

    public static String getDescription(int i) {
        return PurchaseCode.getDescription(i);
    }

    public static SMSPurchase getInstance() {
        if (a == null) {
            a = new SMSPurchase();
        }
        return a;
    }

    public static String getReason(int i) {
        return PurchaseCode.getReason(i);
    }

    public void a(Context context) {
        IntentFilter intentFilter = new IntentFilter(SMSReceiver.i);
        intentFilter.addAction(SMSReceiver.j);
        context.registerReceiver(this.f7a, intentFilter);
    }

    public void b(Context context) {
        if (this.f7a == null) {
            this.f7a = new SMSReceiver();
        }
        context.unregisterReceiver(this.f7a);
    }

    public void setAppInfo(String str, String str2) {
        if (str == null || str2 == null || str.trim().length() == 0 || str2.trim().length() == 0) {
            throw new Exception("invalid app parameter, pls check it");
        }
        d.u();
        c.b(str.trim(), str2.trim());
    }

    public void setAppInfo(String str, String str2, int i) {
        if (str == null || str2 == null || str.trim().length() == 0 || str2.trim().length() == 0) {
            throw new Exception("invalid app parameter, pls check it");
        }
        d.u();
        c.b(str.trim(), str2.trim());
        c.d(i);
    }

    public synchronized void smsInit(Context context, OnSMSPurchaseListener onSMSPurchaseListener) {
        if (context != null) {
            if (context instanceof Activity) {
                if (onSMSPurchaseListener == null) {
                    throw new Exception("OnPurchaseListener Object is null");
                } else if (c.a(0)) {
                    Toast.makeText(context, "尊敬的用户，已有其他支付请求正在发送。", 0).show();
                } else {
                    c.setContext(context);
                    b bVar = new b(onSMSPurchaseListener, this.f4a, this.b);
                    if (this.f7a == null) {
                        this.f7a = new SMSReceiver();
                    }
                    c.m33t();
                    int a2 = c.a(context);
                    if (a2 != 0) {
                        bVar.onInitFinish(a2);
                    } else {
                        int b2 = c.b(context);
                        if (b2 != 1000) {
                            bVar.onInitFinish(b2);
                        } else {
                            if (new a().c() == 1104) {
                                j.a().a(this.f4a, this.b, bVar);
                                if (c.e().booleanValue()) {
                                    IdentifyApp.saveSMS(c.z(), c.t(), c.s(), c.m29o(), c.A());
                                }
                            }
                            Message obtainMessage = this.f4a.obtainMessage();
                            obtainMessage.what = 0;
                            obtainMessage.obj = bVar;
                            obtainMessage.sendToTarget();
                        }
                    }
                }
            }
        }
        throw new Exception("Context Object is null or Context Object is not instance of Activity");
    }

    public void smsOrder(Context context, String str, OnSMSPurchaseListener onSMSPurchaseListener) {
        a(context, str, 1, false, onSMSPurchaseListener, null);
    }

    public void smsOrder(Context context, String str, OnSMSPurchaseListener onSMSPurchaseListener, String str2) {
        a(context, str, 1, false, onSMSPurchaseListener, str2);
    }
}
