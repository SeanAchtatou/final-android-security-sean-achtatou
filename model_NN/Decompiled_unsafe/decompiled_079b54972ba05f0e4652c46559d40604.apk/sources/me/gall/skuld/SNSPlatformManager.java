package me.gall.skuld;

import android.app.Activity;
import android.util.Log;
import java.util.Map;
import me.gall.skuld.adapter.SNSPlatformAdapter;
import me.gall.skuld.util.Configuration;

public final class SNSPlatformManager {
    public static final String META_TAG_SKULD_ADAPTER_NAME = "SKULD_ADAPTER_NAME";
    private static final String TAG = "SNSPlatformManager";
    private static SNSPlatformAdapter lJ;
    private static Activity lK;
    private static String lL;
    private static boolean lM;

    public static void a(Activity activity, String str, Map<String, String> map) {
        lK = activity;
        lL = str;
        try {
            b(map);
            lM = true;
            Log.i(TAG, "Skuld is initilized. Everything has been prepared.");
        } catch (Exception e) {
            Log.e(TAG, "The SDK is not initilized because of " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static void b(Map<String, String> map) {
        Class<?> cls = Class.forName("me.gall.skuld.adapter." + Configuration.getManifestMetaValue(lK, META_TAG_SKULD_ADAPTER_NAME) + "Adapter");
        Log.i(META_TAG_SKULD_ADAPTER_NAME, cls.getName());
        lJ = (SNSPlatformAdapter) cls.newInstance();
        lJ.c(map);
        Log.i(TAG, "Skuld is created.");
    }

    public static SNSPlatformAdapter fT() {
        if (lM) {
            return lJ;
        }
        throw new NullPointerException("SNSPlatformAdapter has not initilized. May be SKULD_ADAPTER_NAME in the manifest is wrong spelled or check the log for detail.");
    }

    public static Activity fU() {
        return lK;
    }

    public static String fV() {
        return lL;
    }

    public static final void onDestroy() {
        if (lM) {
            lJ.onDestroy();
            Log.i(TAG, "Skuld is destroyed.");
            return;
        }
        Log.w(TAG, "Skuld is not initilized yet.");
    }

    public static final void onPause() {
        if (lM) {
            lJ.onPause();
            Log.i(TAG, "Skuld is paused.");
            return;
        }
        Log.w(TAG, "Skuld is not initilized yet.");
    }

    public static final void onResume() {
        if (lM) {
            lJ.onResume();
            Log.i(TAG, "Skuld is resumed.");
            return;
        }
        Log.w(TAG, "Skuld is not initilized yet.");
    }
}
