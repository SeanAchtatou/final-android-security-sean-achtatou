package me.gall.skuld.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.tencent.webnet.PreSMSReturn;
import com.tencent.webnet.WebNetEvent;
import com.tencent.webnet.WebNetInterface;
import java.io.FileNotFoundException;
import java.util.Map;
import me.gall.skuld.SNSPlatformManager;
import me.gall.skuld.adapter.SNSPlatformAdapter;
import me.gall.skuld.util.DataMapper;
import org.opensocial.models.AppData;
import org.opensocial.models.skuld.Achievement;
import org.opensocial.models.skuld.Application;
import org.opensocial.models.skuld.Billing;
import org.opensocial.models.skuld.Challenge;
import org.opensocial.models.skuld.Lobby;
import org.opensocial.models.skuld.Player;
import org.opensocial.models.skuld.Score;

public class QQGameCenterAdapter extends a implements WebNetEvent {
    /* access modifiers changed from: private */
    public static SNSPlatformAdapter.AsyncResultCallback<Billing> lO;
    private boolean lN;
    private Billing lP;

    public static class SMSPaymentActivity extends Activity implements Handler.Callback {
        private static final int HIDE_PROCESSING_DIALOG = 2;
        private static final String LOG = "SMSPaymentActivity";
        private static final int SHOW_PROCESSING_DIALOG = 1;
        /* access modifiers changed from: private */
        public Handler handler;
        /* access modifiers changed from: private */
        public String lS;
        private ProgressDialog lT;
        private ViewGroup lU;

        public static final View findViewByName(Context context, View view, String str) {
            return view.findViewById(getViewIdentifier(context, str));
        }

        public static final int getIdentifier(Context context, String str, String str2) {
            int identifier = context.getResources().getIdentifier(str, str2, context.getPackageName());
            if (identifier != 0) {
                return identifier;
            }
            throw new FileNotFoundException(String.valueOf(str) + " is not found in res/" + str2 + "/.");
        }

        public static final int getLayoutIdentifier(Context context, String str) {
            return getIdentifier(context, str, "layout");
        }

        public static final int getViewIdentifier(Context context, String str) {
            try {
                return getIdentifier(context, str, "id");
            } catch (FileNotFoundException e) {
                throw new NullPointerException(String.valueOf(str) + " is not found.");
            }
        }

        /* access modifiers changed from: private */
        public void gk() {
            this.handler.sendEmptyMessage(1);
        }

        private void gl() {
            this.handler.sendEmptyMessage(2);
        }

        /* access modifiers changed from: private */
        public void gm() {
            gl();
            finish();
        }

        public static final View inflateView(Context context, String str) {
            return ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(getLayoutIdentifier(context, str), (ViewGroup) null);
        }

        public boolean handleMessage(Message message) {
            switch (message.what) {
                case 1:
                    if (getWindow().isActive()) {
                        if (this.lT == null) {
                            this.lT = new ProgressDialog(this);
                            this.lT.setMessage("请稍候");
                            this.lT.setCancelable(false);
                        }
                        if (!this.lT.isShowing()) {
                            this.lT.show();
                            break;
                        }
                    }
                    break;
                case 2:
                    if (this.lT != null && this.lT.isShowing()) {
                        this.lT.hide();
                        break;
                    }
            }
            return false;
        }

        public void onBackPressed() {
            gm();
        }

        /* access modifiers changed from: protected */
        public void onCreate(Bundle bundle) {
            super.onCreate(bundle);
            getWindow().requestFeature(1);
            Intent intent = getIntent();
            this.handler = new Handler(this);
            String stringExtra = intent.hasExtra("TEXT") ? intent.getStringExtra("TEXT") : null;
            this.lS = intent.getStringExtra("BILLINGID");
            try {
                this.lU = (ViewGroup) inflateView(this, "skuld_layout_main");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            setContentView(this.lU);
            Log.d(LOG, "TEXT:" + stringExtra);
            ((TextView) findViewByName(this, this.lU, "skuld_view_content")).setText(stringExtra);
            ((Button) findViewByName(this, this.lU, "skuld_view_confirm")).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    SMSPaymentActivity.this.gk();
                    String str = "SMS_TECENT_BILLING_MARK:" + System.currentTimeMillis();
                    Log.d(SMSPaymentActivity.LOG, "Sending :" + SMSPaymentActivity.this.lS + " mark:" + str);
                    WebNetInterface.SMSBillingPoint(Integer.parseInt(SMSPaymentActivity.this.lS), str);
                    SMSPaymentActivity.this.handler.postDelayed(new Runnable() {
                        public void run() {
                            SMSPaymentActivity.this.gm();
                        }
                    }, 5000);
                }
            });
            ((Button) findViewByName(this, this.lU, "skuld_view_cancel")).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    if (QQGameCenterAdapter.lO != null) {
                        QQGameCenterAdapter.lO.aG("用户取消计费");
                    }
                    SMSPaymentActivity.this.gm();
                }
            });
        }

        public void onDestroy() {
            if (this.lT != null) {
                this.lT.dismiss();
            }
            this.lT = null;
            super.onDestroy();
        }
    }

    private String bQ(int i) {
        String str;
        switch (i) {
            case -107:
                str = "Error_Type_Sys";
                break;
            case -106:
                str = "Error_Type_Rec";
                break;
            case -105:
                str = "Error_Type_Send";
                break;
            case -104:
                str = "Error_Type_Connect";
                break;
            case -103:
                str = "FUNCTION_NA";
                break;
            case -102:
                str = "NOT_LOGIN";
                break;
            case -101:
                str = "NET_NOT_AVAILABLE";
                break;
            case 1031:
                str = "BillingPoint_Event_Error";
                break;
            case 1041:
                str = "SendSMS_Event_Error";
                break;
            case 1042:
                str = "SendSMS_Event_Generic_Failure";
                break;
            case 1043:
                str = "SendSMS_Event_Radio_Off";
                break;
            case 1044:
                str = "SendSMS_Event_NULL_PDU";
                break;
            case 1045:
                str = "SendSMS_Event_INIT_ERROR";
                break;
            default:
                str = "Unknown error";
                break;
        }
        Log.w(getName(), str);
        return str;
    }

    public /* bridge */ /* synthetic */ AppData a(Player player) {
        return super.a(player);
    }

    public /* bridge */ /* synthetic */ void a(String str, Bitmap bitmap) {
        super.a(str, bitmap);
    }

    public /* bridge */ /* synthetic */ void a(DataMapper dataMapper) {
        super.a(dataMapper);
    }

    public /* bridge */ /* synthetic */ void a(AppData appData) {
        super.a(appData);
    }

    public /* bridge */ /* synthetic */ void a(AppData appData, SNSPlatformAdapter.AsyncResultCallback asyncResultCallback) {
        super.a(appData, asyncResultCallback);
    }

    public /* bridge */ /* synthetic */ void a(Achievement achievement) {
        super.a(achievement);
    }

    public void a(Achievement achievement, SNSPlatformAdapter.AsyncResultCallback<Achievement> asyncResultCallback) {
        WebNetInterface.UpdateAchievement(Integer.parseInt(achievement.getId()));
    }

    public /* bridge */ /* synthetic */ void a(Billing billing) {
        super.a(billing);
    }

    public void a(Billing billing, SNSPlatformAdapter.AsyncResultCallback<Billing> asyncResultCallback) {
        Log.d(getName(), "=========Start Billing========");
        if (billing.type == 1) {
            Log.d(getName(), "=========It is a SMS Billing========");
            Log.d(getName(), "Get the sms desp.");
            lO = asyncResultCallback;
            this.lP = billing;
            PreSMSReturn PreSMSBillingPoint = WebNetInterface.PreSMSBillingPoint(Integer.parseInt(billing.getId()));
            if (PreSMSBillingPoint.m_bSuccess) {
                Log.d(getName(), "Pre SMS Billing:" + PreSMSBillingPoint.m_contents);
                Intent intent = new Intent();
                intent.setClass(SNSPlatformManager.fU(), SMSPaymentActivity.class);
                intent.putExtra("TEXT", PreSMSBillingPoint.m_contents);
                intent.putExtra("BILLINGID", billing.getId());
                SNSPlatformManager.fU().startActivity(intent);
                return;
            }
            Log.w(getName(), "Fail Pre SMS Billing." + PreSMSBillingPoint.m_contents);
            asyncResultCallback.aG("Could not get fee description.");
            return;
        }
        Log.d(getName(), "Not support update normal billing.");
        Log.d(getName(), "Normal Billing:" + billing.getId());
    }

    public /* bridge */ /* synthetic */ void a(Challenge challenge) {
        super.a(challenge);
    }

    public /* bridge */ /* synthetic */ void a(Challenge challenge, SNSPlatformAdapter.AsyncResultCallback asyncResultCallback) {
        super.a(challenge, asyncResultCallback);
    }

    public /* bridge */ /* synthetic */ void a(Score score) {
        super.a(score);
    }

    public void a(Score score, SNSPlatformAdapter.AsyncResultCallback<Score> asyncResultCallback) {
        WebNetInterface.UpdateScore(Integer.parseInt(score.getId()), score.score);
    }

    public /* bridge */ /* synthetic */ Player[] a(Lobby lobby, int i) {
        return super.a(lobby, i);
    }

    public /* bridge */ /* synthetic */ void aF(String str) {
        super.aF(str);
    }

    public /* bridge */ /* synthetic */ Player b(Player player) {
        return super.b(player);
    }

    public /* bridge */ /* synthetic */ void b(DataMapper dataMapper) {
        super.b(dataMapper);
    }

    public void bO(final int i) {
        SNSPlatformManager.fU().runOnUiThread(new Runnable() {
            public void run() {
                if (i == 0) {
                    WebNetInterface.StartWeb(SNSPlatformManager.fU());
                } else {
                    WebNetInterface.StartWeb(SNSPlatformManager.fU(), i);
                }
            }
        });
    }

    public void bP(int i) {
    }

    public void c(Map<String, String> map) {
        super.c(map);
        WebNetInterface.Init(SNSPlatformManager.fU(), this);
    }

    public /* bridge */ /* synthetic */ void c(DataMapper dataMapper) {
        super.c((DataMapper<String>) dataMapper);
    }

    public boolean e(int i, String str) {
        Log.d(getName(), "Result mark:" + str);
        if (i != 1040 || this.lP == null) {
            bQ(i);
            lO.aG(bQ(i));
            return false;
        }
        lO.g(this.lP);
        return false;
    }

    public boolean f(int i, String str) {
        return false;
    }

    public Bitmap fW() {
        return null;
    }

    public Bitmap fX() {
        return null;
    }

    public String fY() {
        return null;
    }

    public String fZ() {
        return null;
    }

    public void ga() {
    }

    public /* bridge */ /* synthetic */ String gb() {
        return super.gb();
    }

    public /* bridge */ /* synthetic */ DataMapper gc() {
        return super.gc();
    }

    public /* bridge */ /* synthetic */ DataMapper gd() {
        return super.gd();
    }

    public /* bridge */ /* synthetic */ DataMapper ge() {
        return super.ge();
    }

    public /* bridge */ /* synthetic */ String getId() {
        return super.getId();
    }

    public /* bridge */ /* synthetic */ String getKey() {
        return super.getKey();
    }

    public String getName() {
        return getClass().getName();
    }

    public /* bridge */ /* synthetic */ Application gf() {
        return super.gf();
    }

    public /* bridge */ /* synthetic */ Lobby gg() {
        return super.gg();
    }

    public /* bridge */ /* synthetic */ boolean gh() {
        return super.gh();
    }

    public /* bridge */ /* synthetic */ String gi() {
        return super.gi();
    }

    public void onDestroy() {
        if (this.lN) {
            WebNetInterface.Destroy();
        }
    }

    public void onPause() {
    }

    public void onResume() {
        WebNetInterface.SetCurActivity(SNSPlatformManager.fU());
    }

    public /* bridge */ /* synthetic */ void setId(String str) {
        super.setId(str);
    }

    public /* bridge */ /* synthetic */ void setKey(String str) {
        super.setKey(str);
    }

    public /* bridge */ /* synthetic */ Player u(boolean z) {
        return super.u(z);
    }
}
