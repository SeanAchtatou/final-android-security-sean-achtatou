package me.gall.skuld.adapter;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import java.util.Map;
import org.opensocial.models.AppData;
import org.opensocial.models.skuld.Achievement;
import org.opensocial.models.skuld.Application;
import org.opensocial.models.skuld.Billing;
import org.opensocial.models.skuld.Challenge;
import org.opensocial.models.skuld.Lobby;
import org.opensocial.models.skuld.Player;
import org.opensocial.models.skuld.Score;

public interface SNSPlatformAdapter {
    public static final int LAUNCH_DASHBOARD = 0;
    public static final int LAUNCH_GAME_ARCHIEVEMENT = 3;
    public static final int LAUNCH_GAME_DETAIL = 1;
    public static final int LAUNCH_GAME_LEADERBOARD = 2;
    public static final int LAUNCH_GAME_OTHER = 4;

    public interface AsyncResultCallback<R> {
        void aG(String str);

        void f(R r);

        void g(R r);
    }

    public interface AsyncWorkerAndResultCallback<R> extends AsyncResultCallback<R> {
        R gn();
    }

    public static class SkuldAsyncTask<R> extends AsyncTask<Void, Void, R> {
        private AsyncWorkerAndResultCallback<R> lX;
        private String lY;

        public SkuldAsyncTask(AsyncWorkerAndResultCallback<R> asyncWorkerAndResultCallback) {
            this.lX = asyncWorkerAndResultCallback;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public R doInBackground(Void... voidArr) {
            try {
                if (this.lX != null) {
                    return this.lX.gn();
                }
                throw new Exception("AsyncResultCallback could not be null");
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        public void a(AsyncWorkerAndResultCallback<R> asyncWorkerAndResultCallback) {
            this.lX = asyncWorkerAndResultCallback;
        }

        public void aH(String str) {
            this.lY = str;
        }

        public AsyncWorkerAndResultCallback<R> go() {
            return this.lX;
        }

        public String gp() {
            return this.lY;
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(R r) {
            if (r != null) {
                this.lX.g(r);
            } else {
                this.lX.aG(this.lY);
            }
        }

        /* access modifiers changed from: protected */
        public void onPreExecute() {
        }
    }

    AppData a(Player player);

    void a(String str, Bitmap bitmap);

    void a(AppData appData);

    void a(AppData appData, AsyncResultCallback<AppData> asyncResultCallback);

    void a(Achievement achievement);

    void a(Achievement achievement, AsyncResultCallback<Achievement> asyncResultCallback);

    void a(Billing billing);

    void a(Billing billing, AsyncResultCallback<Billing> asyncResultCallback);

    void a(Challenge challenge);

    void a(Challenge challenge, AsyncResultCallback<Challenge> asyncResultCallback);

    void a(Score score);

    void a(Score score, AsyncResultCallback<Score> asyncResultCallback);

    Player[] a(Lobby lobby, int i);

    Player b(Player player);

    void bO(int i);

    void bP(int i);

    void c(Map<String, String> map);

    Bitmap fW();

    Bitmap fX();

    String fY();

    String fZ();

    void ga();

    String getName();

    Application gf();

    Lobby gg();

    boolean gh();

    void onDestroy();

    void onPause();

    void onResume();

    Player u(boolean z);
}
