package me.gall.skuld.adapter;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.util.Log;
import cn.emagsoftware.gamecommunity.api.GameCommunity;
import java.util.Map;
import me.gall.skuld.SNSPlatformManager;
import me.gall.skuld.adapter.SNSPlatformAdapter;
import me.gall.skuld.util.DataMapper;
import org.opensocial.models.AppData;
import org.opensocial.models.skuld.Achievement;
import org.opensocial.models.skuld.Application;
import org.opensocial.models.skuld.Billing;
import org.opensocial.models.skuld.Challenge;
import org.opensocial.models.skuld.Lobby;
import org.opensocial.models.skuld.Player;
import org.opensocial.models.skuld.Score;

public class CMCCGameCommunityAdapter extends a implements DialogInterface.OnClickListener {
    public /* bridge */ /* synthetic */ AppData a(Player player) {
        return super.a(player);
    }

    public /* bridge */ /* synthetic */ void a(String str, Bitmap bitmap) {
        super.a(str, bitmap);
    }

    public /* bridge */ /* synthetic */ void a(DataMapper dataMapper) {
        super.a(dataMapper);
    }

    public /* bridge */ /* synthetic */ void a(AppData appData) {
        super.a(appData);
    }

    public /* bridge */ /* synthetic */ void a(AppData appData, SNSPlatformAdapter.AsyncResultCallback asyncResultCallback) {
        super.a(appData, asyncResultCallback);
    }

    public void a(Achievement achievement) {
        Log.d(getName(), "Update Achievement:" + achievement.getId());
        GameCommunity.openAchievement(achievement.getId());
    }

    public /* bridge */ /* synthetic */ void a(Achievement achievement, SNSPlatformAdapter.AsyncResultCallback asyncResultCallback) {
        super.a(achievement, asyncResultCallback);
    }

    public /* bridge */ /* synthetic */ void a(Billing billing) {
        super.a(billing);
    }

    public /* bridge */ /* synthetic */ void a(Billing billing, SNSPlatformAdapter.AsyncResultCallback asyncResultCallback) {
        super.a(billing, asyncResultCallback);
    }

    public void a(Challenge challenge) {
    }

    public /* bridge */ /* synthetic */ void a(Challenge challenge, SNSPlatformAdapter.AsyncResultCallback asyncResultCallback) {
        super.a(challenge, asyncResultCallback);
    }

    public void a(Score score) {
        Log.d(getName(), "Update Score:" + score.to + "|" + score.score);
        GameCommunity.commitScore(String.valueOf(score.to), (long) score.score);
    }

    public /* bridge */ /* synthetic */ void a(Score score, SNSPlatformAdapter.AsyncResultCallback asyncResultCallback) {
        super.a(score, asyncResultCallback);
    }

    public /* bridge */ /* synthetic */ Player[] a(Lobby lobby, int i) {
        return super.a(lobby, i);
    }

    public /* bridge */ /* synthetic */ void aF(String str) {
        super.aF(str);
    }

    public /* bridge */ /* synthetic */ Player b(Player player) {
        return super.b(player);
    }

    public /* bridge */ /* synthetic */ void b(DataMapper dataMapper) {
        super.b(dataMapper);
    }

    public void bO(int i) {
        if (i == 0) {
            GameCommunity.launchGameCommunity(SNSPlatformManager.fU());
        } else if (i == 1) {
            GameCommunity.launchGameRecommend(SNSPlatformManager.fU());
        } else {
            Log.e(getName(), "启动游戏平台失败, 没有这个参数:" + i);
        }
    }

    public void bP(int i) {
    }

    public void c(Map<String, String> map) {
        super.c(map);
        Log.d(getName(), "Key:" + getKey() + " Secret:" + gi() + " Id:" + getId());
        if (getKey().length() < 12) {
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < 12 - getKey().length(); i++) {
                stringBuffer.append('0');
            }
            setKey(stringBuffer.append(getKey()).toString());
            Log.d(getName(), "Fix key:" + getKey());
        }
        GameCommunity.initializeSDK(SNSPlatformManager.fU(), SNSPlatformManager.fV(), getKey(), gi(), getId(), SNSPlatformManager.fU().getPackageName());
    }

    public /* bridge */ /* synthetic */ void c(DataMapper dataMapper) {
        super.c((DataMapper<String>) dataMapper);
    }

    public Bitmap fW() {
        return null;
    }

    public Bitmap fX() {
        return null;
    }

    public String fY() {
        return GameCommunity.getCurrentChallengeId();
    }

    public String fZ() {
        return GameCommunity.getCurrentChallengeCrossId();
    }

    public void ga() {
    }

    public /* bridge */ /* synthetic */ String gb() {
        return super.gb();
    }

    public /* bridge */ /* synthetic */ DataMapper gc() {
        return super.gc();
    }

    public /* bridge */ /* synthetic */ DataMapper gd() {
        return super.gd();
    }

    public /* bridge */ /* synthetic */ DataMapper ge() {
        return super.ge();
    }

    public /* bridge */ /* synthetic */ String getId() {
        return super.getId();
    }

    public /* bridge */ /* synthetic */ String getKey() {
        return super.getKey();
    }

    public String getName() {
        return getClass().getSimpleName();
    }

    public /* bridge */ /* synthetic */ Application gf() {
        return super.gf();
    }

    public /* bridge */ /* synthetic */ Lobby gg() {
        return super.gg();
    }

    public /* bridge */ /* synthetic */ boolean gh() {
        return super.gh();
    }

    public /* bridge */ /* synthetic */ String gi() {
        return super.gi();
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (i == -1) {
            bO(0);
        } else {
            dialogInterface.dismiss();
        }
    }

    public void onDestroy() {
        GameCommunity.exit();
    }

    public void onPause() {
    }

    public void onResume() {
    }

    public /* bridge */ /* synthetic */ void setId(String str) {
        super.setId(str);
    }

    public /* bridge */ /* synthetic */ void setKey(String str) {
        super.setKey(str);
    }

    public /* bridge */ /* synthetic */ Player u(boolean z) {
        return super.u(z);
    }
}
