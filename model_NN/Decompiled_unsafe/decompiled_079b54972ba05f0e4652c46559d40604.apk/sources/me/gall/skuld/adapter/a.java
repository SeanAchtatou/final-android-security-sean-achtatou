package me.gall.skuld.adapter;

import android.graphics.Bitmap;
import java.util.Map;
import me.gall.skuld.SNSPlatformManager;
import me.gall.skuld.adapter.SNSPlatformAdapter;
import me.gall.skuld.util.Configuration;
import me.gall.skuld.util.DataMapper;
import org.opensocial.models.AppData;
import org.opensocial.models.skuld.Achievement;
import org.opensocial.models.skuld.Application;
import org.opensocial.models.skuld.Billing;
import org.opensocial.models.skuld.Challenge;
import org.opensocial.models.skuld.Lobby;
import org.opensocial.models.skuld.Player;
import org.opensocial.models.skuld.Score;

abstract class a implements SNSPlatformAdapter {
    public static final String META_TAG_SKULD_ADAPTER_APP_ID = "SKULD_ADAPTER_APP_ID";
    public static final String META_TAG_SKULD_ADAPTER_APP_KEY = "SKULD_ADAPTER_APP_KEY";
    public static final String META_TAG_SKULD_ADAPTER_APP_SECRET = "SKULD_ADAPTER_APP_SECRET";
    public static final String META_TAG_SKULD_ARCHIEVEMENT_ID_MAPPER = "SKULD_ARCHIEVEMENT_ID_MAPPER";
    public static final String META_TAG_SKULD_BILLING_ID_MAPPER = "SKULD_BILLING_ID_MAPPER";
    public static final String META_TAG_SKULD_LEADERBOARD_ID_MAPPER = "SKULD_LEADERBOARD_ID_MAPPER";
    private String id;
    private String key;
    private DataMapper<String> lZ;
    private DataMapper<String> ma;
    private DataMapper<String> mb;
    private String mc;

    a() {
    }

    public AppData a(Player player) {
        throw new FeatureNotSupportException("getAppData(Player player) is not supported yet.");
    }

    public void a(String str, Bitmap bitmap) {
        throw new FeatureNotSupportException("share(String msg, Bitmap bitmap) is not supported yet.");
    }

    public void a(DataMapper<String> dataMapper) {
        this.ma = dataMapper;
    }

    public void a(AppData appData) {
        a(appData, (SNSPlatformAdapter.AsyncResultCallback<AppData>) null);
    }

    public void a(AppData appData, SNSPlatformAdapter.AsyncResultCallback<AppData> asyncResultCallback) {
        throw new FeatureNotSupportException("update(AppData appData) is not supported yet.");
    }

    public void a(Achievement achievement) {
        a(achievement, (SNSPlatformAdapter.AsyncResultCallback<Achievement>) null);
    }

    public void a(Achievement achievement, SNSPlatformAdapter.AsyncResultCallback<Achievement> asyncResultCallback) {
        throw new FeatureNotSupportException("update(Achievement achievement) is not supported yet.");
    }

    public void a(Billing billing) {
        a(billing, (SNSPlatformAdapter.AsyncResultCallback<Billing>) null);
    }

    public void a(Billing billing, SNSPlatformAdapter.AsyncResultCallback<Billing> asyncResultCallback) {
        throw new FeatureNotSupportException("update(Billing billing) is not supported yet.");
    }

    public void a(Challenge challenge) {
        a(challenge, (SNSPlatformAdapter.AsyncResultCallback<Challenge>) null);
    }

    public void a(Challenge challenge, SNSPlatformAdapter.AsyncResultCallback<Challenge> asyncResultCallback) {
        throw new FeatureNotSupportException("update(Challenge challenge) is not supported yet.");
    }

    public void a(Score score) {
        a(score, (SNSPlatformAdapter.AsyncResultCallback<Score>) null);
    }

    public void a(Score score, SNSPlatformAdapter.AsyncResultCallback<Score> asyncResultCallback) {
        throw new FeatureNotSupportException("update(Score score) is not supported yet.");
    }

    public Player[] a(Lobby lobby, int i) {
        throw new FeatureNotSupportException("pickRandomPlayers(Lobby lobby) is not supported yet.");
    }

    public void aF(String str) {
        this.mc = str;
    }

    public Player b(Player player) {
        throw new FeatureNotSupportException("update(Player player) is not supported yet.");
    }

    public void b(DataMapper<String> dataMapper) {
        this.mb = dataMapper;
    }

    public void c(Map<String, String> map) {
        String manifestMetaValue = Configuration.getManifestMetaValue(SNSPlatformManager.fU(), META_TAG_SKULD_LEADERBOARD_ID_MAPPER);
        if (manifestMetaValue != null) {
            c((DataMapper<String>) new DataMapper().c(manifestMetaValue.split(";")));
        }
        String manifestMetaValue2 = Configuration.getManifestMetaValue(SNSPlatformManager.fU(), META_TAG_SKULD_ARCHIEVEMENT_ID_MAPPER);
        if (manifestMetaValue2 != null) {
            a(new DataMapper().c(manifestMetaValue2.split(";")));
        }
        String manifestMetaValue3 = Configuration.getManifestMetaValue(SNSPlatformManager.fU(), META_TAG_SKULD_BILLING_ID_MAPPER);
        if (manifestMetaValue3 != null) {
            b(new DataMapper().c(manifestMetaValue3.split(";")));
        }
        setKey(Configuration.getManifestMetaValue(SNSPlatformManager.fU(), META_TAG_SKULD_ADAPTER_APP_KEY));
        aF(Configuration.getManifestMetaValue(SNSPlatformManager.fU(), META_TAG_SKULD_ADAPTER_APP_SECRET));
        setId(Configuration.getManifestMetaValue(SNSPlatformManager.fU(), META_TAG_SKULD_ADAPTER_APP_ID));
    }

    public void c(DataMapper<String> dataMapper) {
        this.lZ = dataMapper;
    }

    public String gb() {
        return SNSPlatformManager.fV();
    }

    public DataMapper<String> gc() {
        return this.lZ;
    }

    public DataMapper<String> gd() {
        return this.ma;
    }

    public DataMapper<String> ge() {
        return this.mb;
    }

    public String getId() {
        return this.id;
    }

    public String getKey() {
        return this.key;
    }

    public Application gf() {
        throw new FeatureNotSupportException("getCurrentApplication() is not supported yet.");
    }

    public Lobby gg() {
        throw new FeatureNotSupportException("getCurrentLobby() is not supported yet.");
    }

    public boolean gh() {
        return false;
    }

    public String gi() {
        return this.mc;
    }

    public void setId(String str) {
        this.id = str;
    }

    public void setKey(String str) {
        this.key = str;
    }

    public Player u(boolean z) {
        throw new FeatureNotSupportException("getCurrentPlayer() is not supported yet.");
    }
}
