package me.gall.skuld.adapter;

public class FeatureNotSupportException extends Exception {
    private static final long serialVersionUID = 1;

    public FeatureNotSupportException(String str) {
        super(str);
    }
}
