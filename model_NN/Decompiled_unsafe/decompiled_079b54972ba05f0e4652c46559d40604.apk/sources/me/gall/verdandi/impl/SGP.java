package me.gall.verdandi.impl;

import android.util.Log;
import com.a.a.m.m;
import com.googlecode.jsonrpc4j.Base64;
import java.util.HashMap;
import java.util.List;
import me.gall.sgp.android.core.SGPManager;
import me.gall.sgp.sdk.entity.Server;
import me.gall.sgp.sdk.entity.User;
import me.gall.sgp.sdk.entity.app.Announcement;
import me.gall.sgp.sdk.entity.app.Boss;
import me.gall.sgp.sdk.entity.app.Campaign;
import me.gall.sgp.sdk.entity.app.CampaignDetail;
import me.gall.sgp.sdk.entity.app.CheckinBox;
import me.gall.sgp.sdk.entity.app.GachaBox;
import me.gall.sgp.sdk.entity.app.LeaderBoard;
import me.gall.sgp.sdk.entity.app.LeaderBoardScore;
import me.gall.sgp.sdk.entity.app.Lottery;
import me.gall.sgp.sdk.entity.app.Mail;
import me.gall.sgp.sdk.entity.app.Save;
import me.gall.sgp.sdk.entity.app.SgpPlayer;
import me.gall.sgp.sdk.entity.app.Ticket;
import me.gall.sgp.sdk.service.RouterService;
import me.gall.verdandi.APIBridge;
import me.gall.verdandi.ISGP;
import org.meteoroid.core.l;

public class SGP implements ISGP {
    private SGPManager mz;

    public int a(int i, String str, int i2) {
        try {
            return this.mz.getCampaignService().updateProgress(i, str, i2);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public Object a(Class cls) {
        return this.mz.getService(cls);
    }

    public Server a(String str, long j, String str2) {
        return a(str, j, str2, (String) null);
    }

    public Server a(String str, long j, String str2, String str3) {
        HashMap hashMap = new HashMap();
        if (str != null) {
            hashMap.put(RouterService.USER_ID, str);
        }
        if (j != 0) {
            hashMap.put(RouterService.CREATE_TIME, String.valueOf(j));
        }
        if (str2 != null) {
            hashMap.put(m.PROP_VERSION, str2);
        }
        if (str3 != null) {
            hashMap.put("channelId", str3);
        }
        try {
            hashMap.put(RouterService.APP_SIGN, APIBridge.getApplicationUtil().getAppSignature());
        } catch (Exception e) {
            Log.d("verdandi", "Do not have an app sign.");
        }
        return this.mz.updateRouting(hashMap);
    }

    public LeaderBoardScore a(String str, SgpPlayer sgpPlayer) {
        try {
            return this.mz.getLeaderBoardService().getLeaderBoardScoreByLeaderIdAndPlayerId(str, sgpPlayer.getId());
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public Save a(SgpPlayer sgpPlayer, Save save) {
        if (sgpPlayer == null || sgpPlayer.getId() == null) {
            throw new Exception("Call create/get to get a player at first.");
        }
        save.setPlayerId(sgpPlayer.getId());
        try {
            return this.mz.getSgpPlayerService().uploadSave(save);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public SgpPlayer a(SgpPlayer sgpPlayer) {
        if (this.mz.getCurrentServer() == null || this.mz.getCurrentUser() == null) {
            throw new Exception("Call route to get server instance or register/login to get a user at first.");
        }
        sgpPlayer.setUserId(this.mz.getCurrentUser().getUserid());
        sgpPlayer.setServerId(this.mz.getCurrentServer().getId());
        try {
            return this.mz.getSgpPlayerService().create(sgpPlayer);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public void a(String str, String str2, Mail mail) {
        try {
            this.mz.getFriendshipService().acceptInvite(str, str2, mail);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public void a(String str, SgpPlayer sgpPlayer, int i) {
        try {
            this.mz.getLeaderBoardService().submitLeaderBoardScore(str, sgpPlayer.getId(), i);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void a(String str, String[] strArr) {
        try {
            this.mz.getFriendshipService().acceptInvite(str, strArr);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public void a(String str, String[] strArr, Mail mail) {
        try {
            this.mz.getFriendshipService().acceptInvite(str, strArr, mail);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public void a(Mail mail) {
        if (mail.getFromId() == null || mail.getToId() == null) {
            throw new Exception("发送者和接受者不能为空");
        }
        try {
            this.mz.getMailService().send(mail);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void a(Ticket ticket) {
        try {
            this.mz.getTicketService().sendTicket(ticket);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void a(Mail[] mailArr) {
        int[] iArr = new int[mailArr.length];
        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = mailArr[i].getId().intValue();
        }
        try {
            this.mz.getMailService().read(iArr);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public Campaign[] a(long j, long j2) {
        try {
            return this.mz.getCampaignService().getByTimeZone(j, j2);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public Mail[] a(SgpPlayer sgpPlayer, int i, int i2, int i3) {
        try {
            return this.mz.getMailService().receive(i, i2, sgpPlayer.getId(), i3);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public Mail[] a(SgpPlayer sgpPlayer, long j) {
        try {
            return this.mz.getMailService().receiveUnread(j, sgpPlayer.getId());
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public SgpPlayer[] a(long j, int i, int i2) {
        try {
            return this.mz.getSgpPlayerService().getByLastLoginTime(j, i, i2);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public byte[] aR(String str) {
        return Base64.decode(str);
    }

    public SgpPlayer[] aS(String str) {
        try {
            return this.mz.getFriendshipService().getInvite(str);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public SgpPlayer[] aT(String str) {
        try {
            return this.mz.getFriendshipService().getNotConfirm(str);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public CheckinBox aU(String str) {
        try {
            return this.mz.getCheckinService().getCheckinboardByChekinboardId(str);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public String aV(String str) {
        try {
            return this.mz.getCheckinService().getRewardByChekinboardId(str);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public String[] aW(String str) {
        try {
            return (String[]) this.mz.getStructuredDataService().getSetValue(str).toArray();
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public SgpPlayer aX(String str) {
        try {
            return this.mz.getSgpPlayerService().getSgpPlayerByCustomId(str);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public Boss aY(String str) {
        return this.mz.getBossService().getByBossId(Integer.parseInt(str));
    }

    public LeaderBoard aZ(String str) {
        return this.mz.getLeaderBoardService().getLeaderBoardByLeaderId(str);
    }

    public void addListValue(String str, String str2) {
        try {
            this.mz.getStructuredDataService().addListValue(str, str2);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public boolean addPlayerIntoBlacklist(String str, String str2) {
        return this.mz.getBlacklistService().addPlayerIntoBlacklist(str, str2);
    }

    public void addSetValue(String str, String str2) {
        try {
            this.mz.getStructuredDataService().addSetValue(str, str2);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public int b(int i, String str, int i2) {
        return this.mz.getBossService().attack(i, i2, str);
    }

    public int b(String str, String str2, int i) {
        try {
            return this.mz.getCheckinService().setCheckinTimes(str, str2, i);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public Save b(SgpPlayer sgpPlayer) {
        if (sgpPlayer == null || sgpPlayer.getId() == null) {
            throw new Exception("Call create/get to get a player at first.");
        }
        try {
            return this.mz.getSgpPlayerService().downloadSave(sgpPlayer.getId());
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public void b(String str, SgpPlayer sgpPlayer, int i) {
        try {
            this.mz.getLeaderBoardService().addUpLeaderBoardScore(str, sgpPlayer.getId(), i);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void b(String str, String[] strArr) {
        try {
            this.mz.getFriendshipService().unfriend(str, strArr);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public void b(Mail mail) {
        try {
            this.mz.getMailService().read(mail.getId().intValue());
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void b(Mail[] mailArr) {
        int[] iArr = new int[mailArr.length];
        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = mailArr[i].getId().intValue();
        }
        try {
            this.mz.getMailService().delete(iArr);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public Ticket[] b(String str, int i, int i2, int i3) {
        try {
            return this.mz.getTicketService().getTicketsBySenderPlayerId(str, i, i2, i3);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public Announcement bX(int i) {
        return this.mz.getAnnouncementService().getAnnounceByType(i);
    }

    public CampaignDetail bY(int i) {
        try {
            return this.mz.getCampaignService().getCampaignDetaiByCId(i);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public SgpPlayer bZ(int i) {
        return this.mz.getBossService().getLastAttackPlayer(i);
    }

    public SgpPlayer c(SgpPlayer sgpPlayer) {
        try {
            return this.mz.getSgpPlayerService().update(sgpPlayer);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public void c(String str, boolean z, boolean z2) {
        this.mz = SGPManager.getInstance(l.getActivity(), str, z2);
    }

    public void c(Mail mail) {
        try {
            this.mz.getMailService().delete(mail.getId().intValue());
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public LeaderBoardScore[] c(String str, int i, int i2) {
        try {
            List<LeaderBoardScore> topLeaderBoardScoreByLeaderId = this.mz.getLeaderBoardService().getTopLeaderBoardScoreByLeaderId(str, i, i2);
            if (topLeaderBoardScoreByLeaderId == null || topLeaderBoardScoreByLeaderId.isEmpty()) {
                return null;
            }
            LeaderBoardScore[] leaderBoardScoreArr = new LeaderBoardScore[topLeaderBoardScoreByLeaderId.size()];
            int i3 = 0;
            while (true) {
                int i4 = i3;
                if (i4 >= leaderBoardScoreArr.length) {
                    return leaderBoardScoreArr;
                }
                leaderBoardScoreArr[i4] = topLeaderBoardScoreByLeaderId.get(i4);
                i3 = i4 + 1;
            }
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public SgpPlayer[] c(int i, int i2, String str) {
        try {
            return this.mz.getFriendshipService().getDenied(i, i2, str);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public Lottery[] ca(int i) {
        return this.mz.getGachaBoxService().getLotteriesByGachaBoxId(Integer.valueOf(i));
    }

    public SgpPlayer[] d(String str, int i, int i2) {
        try {
            return this.mz.getSgpPlayerService().getByName(str, i, i2);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public void deleteSgpPlayerByPlayerId(String str) {
        try {
            this.mz.getSgpPlayerService().deleteSgpPlayerByPlayerId(str);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public void destroy() {
        this.mz.destroy();
        this.mz = null;
    }

    public String draw(String str, int i, int i2) {
        try {
            return this.mz.getGachaBoxService().draw(str, i, i2);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public String[] draw(String str, int i, int[] iArr) {
        try {
            return this.mz.getGachaBoxService().draw(str, i, iArr);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public SgpPlayer[] e(String str, int i, int i2) {
        try {
            return this.mz.getFriendshipService().getMyFriends(i, i2, str);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public User f(String str, String str2, String str3) {
        User user = new User();
        user.setUserid(str);
        user.setUserName(str2);
        user.setPassword(str3);
        User updateUser = this.mz.getUserService().updateUser(user);
        this.mz.setCurrentUser(updateUser);
        return updateUser;
    }

    public int g(int i, String str) {
        return this.mz.getBossService().getCurrentHP(i, str);
    }

    public Server[] gW() {
        return this.mz.getRouterService().getServerList(this.mz.getAppId());
    }

    public User gX() {
        return this.mz.quickLogin();
    }

    public User gY() {
        return this.mz.getCurrentUser();
    }

    public Server gZ() {
        return this.mz.getCurrentServer();
    }

    public Campaign[] getAvailableCampaigns() {
        try {
            return this.mz.getCampaignService().getAvailableCampaigns();
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public GachaBox[] getAvailableGachaBox() {
        return this.mz.getGachaBoxService().getAvailableGachaBox();
    }

    public int getCampaignProgress(int i, String str) {
        try {
            return this.mz.getCampaignService().getCampaignProgress(i, str);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public long getCurrentTimestamp() {
        return this.mz.getRouterService().getCurrentTimestamp();
    }

    public int getFrindsCount(String str) {
        try {
            return this.mz.getFriendshipService().getFrindsCount(str);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public GachaBox getGachaBoxByName(String str) {
        return this.mz.getGachaBoxService().getGachaBoxByName(str);
    }

    public String getHashValue(String str, String str2) {
        try {
            return this.mz.getStructuredDataService().getHashValue(str, str2);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public long getLastCheckinTime(String str, String str2) {
        try {
            return this.mz.getCheckinService().getLastCheckinTime(str, str2);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public String[] getListByIndex(String str, long j, long j2) {
        try {
            return this.mz.getStructuredDataService().getListByIndex(str, j, j2);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public String[] getListValue(String str) {
        try {
            return this.mz.getStructuredDataService().getListValue(str);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public SgpPlayer getPlayer() {
        if (this.mz.getCurrentUser() == null) {
            throw new Exception("Call register/login to get a user at first.");
        }
        try {
            return this.mz.getSgpPlayerService().getOneByUserId(this.mz.getCurrentUser().getUserid());
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public SgpPlayer getSgpPlayerById(String str) {
        try {
            return this.mz.getSgpPlayerService().getSgpPlayerById(str);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public String getValue(String str) {
        try {
            return this.mz.getStructuredDataService().getValue(str);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public SgpPlayer[] ha() {
        if (this.mz.getCurrentUser() == null) {
            throw new Exception("Call register/login to get a user at first.");
        }
        try {
            return this.mz.getSgpPlayerService().getByUserId(this.mz.getCurrentUser().getUserid());
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public LeaderBoardScore[] i(String str, int i) {
        return c(str, 0, i);
    }

    public void invite(String str, String str2) {
        try {
            this.mz.getFriendshipService().invite(str, str2);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public void invite(String str, String[] strArr) {
        try {
            this.mz.getFriendshipService().invite(str, strArr);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public boolean isMyfriend(String str, String str2) {
        try {
            return this.mz.getFriendshipService().isMyfriend(str, str2);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public boolean[] isMyfriend(String str, String[] strArr) {
        try {
            return this.mz.getFriendshipService().isMyfriend(str, strArr);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public User login(String str, String str2) {
        return this.mz.login(str, str2);
    }

    public void p(String str, String str2) {
        try {
            this.mz.getFriendshipService().acceptInvite(str, str2);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public int q(String str, String str2) {
        try {
            return this.mz.getCheckinService().checkin(str, str2);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public int r(String str, String str2) {
        try {
            return this.mz.getCheckinService().accumlateCount(str, str2);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public void refuse(String str, String str2) {
        try {
            this.mz.getFriendshipService().refuse(str, str2);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public void refuse(String[] strArr, String str) {
        try {
            this.mz.getFriendshipService().refuse(strArr, str);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public User register(String str, String str2) {
        return this.mz.signup(str, str2);
    }

    public int s(String str, String str2) {
        try {
            return this.mz.getCheckinService().countinuousCount(str, str2);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public void saveOrUpdateHashValue(String str, String str2, String str3) {
        try {
            this.mz.getStructuredDataService().saveOrUpdateHashValue(str, str2, str3);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public void saveOrUpdateValue(String str, String str2) {
        try {
            this.mz.getStructuredDataService().saveOrUpdateValue(str, str2);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public SgpPlayer[] searchPlayersByLastLogin(int i) {
        return this.mz.getSgpPlayerService().searchPlayersByLastLogin(i);
    }

    public SgpPlayer[] searchPlayersByLastLogin(long j, int i, String[] strArr) {
        return this.mz.getSgpPlayerService().searchPlayersByLastLogin(j, i, strArr);
    }

    public boolean t(String str, String str2) {
        try {
            return this.mz.getStructuredDataService().containtSet(str, str2);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public void u(String str, String str2) {
        try {
            this.mz.getStructuredDataService().removeFromSet(str, str2);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public boolean v(String str, String str2) {
        return this.mz.getBlacklistService().isInBlacklist(str, str2);
    }

    public void w(String str, String str2) {
        try {
            this.mz.getFriendshipService().unfriend(str, str2);
        } catch (Throwable th) {
            th.printStackTrace();
            throw th;
        }
    }

    public String y(byte[] bArr) {
        return Base64.encodeBytes(bArr);
    }
}
