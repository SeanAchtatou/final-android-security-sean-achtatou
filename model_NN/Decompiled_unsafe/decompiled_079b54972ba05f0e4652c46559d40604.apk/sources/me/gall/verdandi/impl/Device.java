package me.gall.verdandi.impl;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import com.a.a.e.j;
import com.a.a.i.k;
import com.umeng.common.b.e;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import me.gall.verdandi.IDevice;
import org.meteoroid.core.h;
import org.meteoroid.core.i;
import org.meteoroid.core.l;

public class Device implements IDevice {
    /* access modifiers changed from: private */
    public String md;

    public static class IntentIntegrator {
        private static final String BSPLUS_PACKAGE = "com.srowen.bs.android";
        private static final String BS_PACKAGE = "com.google.zxing.client.android";
        public static final String DEFAULT_MESSAGE = "This application requires Barcode Scanner. Would you like to install it?";
        public static final String DEFAULT_NO = "No";
        public static final String DEFAULT_TITLE = "Install Barcode Scanner?";
        public static final String DEFAULT_YES = "Yes";
        private static final int INSTALL_PLUGIN_REQUEST_CODE = 54260;
        private static final String PLUGIN_APK_FILENAME = "zxing.apk";
        private static final String PLUGIN_PACKAGE_NAME = "com.google.zxing.client.android";
        public static final int REQUEST_CODE = 49374;
        /* access modifiers changed from: private */
        public static final String TAG = IntentIntegrator.class.getName();
        public static final Collection<String> mh = b("UPC_A", "UPC_E", "EAN_8", "EAN_13", "RSS_14");
        public static final Collection<String> mi = b("UPC_A", "UPC_E", "EAN_8", "EAN_13", "CODE_39", "CODE_93", "CODE_128", "ITF", "RSS_14", "RSS_EXPANDED");
        public static final Collection<String> mj = Collections.singleton("QR_CODE");
        public static final Collection<String> mk = Collections.singleton("DATA_MATRIX");
        public static final Collection<String> ml = null;

        /* renamed from: mm  reason: collision with root package name */
        public static final Collection<String> f49mm = Collections.singleton("com.google.zxing.client.android");
        public static final Collection<String> mn = b("com.google.zxing.client.android", BSPLUS_PACKAGE, "com.srowen.bs.android.simple");
        /* access modifiers changed from: private */
        public final Activity gf;
        private String message = DEFAULT_MESSAGE;
        private String mo = DEFAULT_YES;
        private String mp = DEFAULT_NO;
        private Collection<String> mq = mn;
        private final Map<String, Object> mr = new HashMap(3);
        private String title = DEFAULT_TITLE;

        public IntentIntegrator(Activity activity) {
            this.gf = activity;
        }

        /* access modifiers changed from: private */
        public static boolean Y(Context context) {
            try {
                return context.getPackageManager().getApplicationInfo("com.google.zxing.client.android", 0) != null;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                return false;
            }
        }

        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX WARN: Type inference failed for: r1v1 */
        /* JADX WARN: Type inference failed for: r2v0, types: [java.io.InputStream] */
        /* JADX WARN: Type inference failed for: r2v2 */
        /* JADX WARN: Type inference failed for: r1v8, types: [java.io.InputStream] */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x003b, code lost:
            r1 = r1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:38:?, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
            r2.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:44:0x0075, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:0x0076, code lost:
            r1.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:0x007a, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:47:0x007b, code lost:
            r1.printStackTrace();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:54:0x008f, code lost:
            r2 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:55:0x0090, code lost:
            r5 = r2;
            r2 = r1;
            r1 = r0;
            r0 = r5;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:22:0x003b A[ExcHandler: Exception (e java.lang.Exception), Splitter:B:4:0x000e] */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x0062 A[SYNTHETIC, Splitter:B:37:0x0062] */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x0067 A[SYNTHETIC, Splitter:B:40:0x0067] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private static void Z(android.content.Context r6) {
            /*
                r0 = 0
                android.content.res.AssetManager r1 = r6.getAssets()     // Catch:{ Exception -> 0x0095, all -> 0x005b }
                java.lang.String r2 = "zxing.apk"
                java.io.InputStream r1 = r1.open(r2)     // Catch:{ Exception -> 0x0095, all -> 0x005b }
                java.lang.String r2 = "zxing.apk"
                r3 = 1
                java.io.FileOutputStream r0 = r6.openFileOutput(r2, r3)     // Catch:{ Exception -> 0x003b, all -> 0x0089 }
                r2 = 102400(0x19000, float:1.43493E-40)
                byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x003b, all -> 0x008f }
            L_0x0017:
                int r3 = r1.read(r2)     // Catch:{ Exception -> 0x003b, all -> 0x008f }
                r4 = -1
                if (r3 != r4) goto L_0x0036
                r0.flush()     // Catch:{ Exception -> 0x003b, all -> 0x008f }
                if (r0 == 0) goto L_0x0026
                r0.close()     // Catch:{ IOException -> 0x007f }
            L_0x0026:
                if (r1 == 0) goto L_0x002b
                r1.close()     // Catch:{ IOException -> 0x0084 }
            L_0x002b:
                java.lang.System.gc()
                java.lang.String r0 = me.gall.verdandi.impl.Device.IntentIntegrator.TAG
                java.lang.String r1 = "Plugin file extract complete."
                android.util.Log.d(r0, r1)
            L_0x0035:
                return
            L_0x0036:
                r4 = 0
                r0.write(r2, r4, r3)     // Catch:{ Exception -> 0x003b, all -> 0x008f }
                goto L_0x0017
            L_0x003b:
                r2 = move-exception
            L_0x003c:
                if (r0 == 0) goto L_0x0041
                r0.close()     // Catch:{ IOException -> 0x0051 }
            L_0x0041:
                if (r1 == 0) goto L_0x0046
                r1.close()     // Catch:{ IOException -> 0x0056 }
            L_0x0046:
                java.lang.System.gc()
                java.lang.String r0 = me.gall.verdandi.impl.Device.IntentIntegrator.TAG
                java.lang.String r1 = "Plugin file extract complete."
                android.util.Log.d(r0, r1)
                goto L_0x0035
            L_0x0051:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0041
            L_0x0056:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0046
            L_0x005b:
                r1 = move-exception
                r2 = r0
                r5 = r0
                r0 = r1
                r1 = r5
            L_0x0060:
                if (r1 == 0) goto L_0x0065
                r1.close()     // Catch:{ IOException -> 0x0075 }
            L_0x0065:
                if (r2 == 0) goto L_0x006a
                r2.close()     // Catch:{ IOException -> 0x007a }
            L_0x006a:
                java.lang.System.gc()
                java.lang.String r1 = me.gall.verdandi.impl.Device.IntentIntegrator.TAG
                java.lang.String r2 = "Plugin file extract complete."
                android.util.Log.d(r1, r2)
                throw r0
            L_0x0075:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x0065
            L_0x007a:
                r1 = move-exception
                r1.printStackTrace()
                goto L_0x006a
            L_0x007f:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x0026
            L_0x0084:
                r0 = move-exception
                r0.printStackTrace()
                goto L_0x002b
            L_0x0089:
                r2 = move-exception
                r5 = r2
                r2 = r1
                r1 = r0
                r0 = r5
                goto L_0x0060
            L_0x008f:
                r2 = move-exception
                r5 = r2
                r2 = r1
                r1 = r0
                r0 = r5
                goto L_0x0060
            L_0x0095:
                r1 = move-exception
                r1 = r0
                goto L_0x003c
            */
            throw new UnsupportedOperationException("Method not decompiled: me.gall.verdandi.impl.Device.IntentIntegrator.Z(android.content.Context):void");
        }

        private String a(Intent intent) {
            List<ResolveInfo> queryIntentActivities = this.gf.getPackageManager().queryIntentActivities(intent, 65536);
            if (queryIntentActivities != null) {
                for (ResolveInfo resolveInfo : queryIntentActivities) {
                    String str = resolveInfo.activityInfo.packageName;
                    if (this.mq.contains(str)) {
                        return str;
                    }
                }
            }
            return null;
        }

        public static b a(int i, int i2, Intent intent) {
            Integer num = null;
            if (i != 49374) {
                return null;
            }
            if (i2 != -1) {
                return new b();
            }
            String stringExtra = intent.getStringExtra("SCAN_RESULT");
            String stringExtra2 = intent.getStringExtra("SCAN_RESULT_FORMAT");
            byte[] byteArrayExtra = intent.getByteArrayExtra("SCAN_RESULT_BYTES");
            int intExtra = intent.getIntExtra("SCAN_RESULT_ORIENTATION", Integer.MIN_VALUE);
            if (intExtra != Integer.MIN_VALUE) {
                num = Integer.valueOf(intExtra);
            }
            return new b(stringExtra, stringExtra2, byteArrayExtra, num, intent.getStringExtra("SCAN_RESULT_ERROR_CORRECTION_LEVEL"));
        }

        private static Collection<String> b(String... strArr) {
            return Collections.unmodifiableCollection(Arrays.asList(strArr));
        }

        /* access modifiers changed from: private */
        public static void b(Activity activity) {
            File file = new File(String.valueOf(activity.getFilesDir().getAbsolutePath()) + File.separator + PLUGIN_APK_FILENAME);
            if (!file.exists()) {
                Z(activity);
            }
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive");
            activity.startActivityForResult(intent, INSTALL_PLUGIN_REQUEST_CODE);
        }

        private void b(Intent intent) {
            for (Map.Entry next : this.mr.entrySet()) {
                String str = (String) next.getKey();
                Object value = next.getValue();
                if (value instanceof Integer) {
                    intent.putExtra(str, (Integer) value);
                } else if (value instanceof Long) {
                    intent.putExtra(str, (Long) value);
                } else if (value instanceof Boolean) {
                    intent.putExtra(str, (Boolean) value);
                } else if (value instanceof Double) {
                    intent.putExtra(str, (Double) value);
                } else if (value instanceof Float) {
                    intent.putExtra(str, (Float) value);
                } else if (value instanceof Bundle) {
                    intent.putExtra(str, (Bundle) value);
                } else {
                    intent.putExtra(str, value.toString());
                }
            }
        }

        private AlertDialog gQ() {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.gf);
            builder.setTitle(this.title);
            builder.setMessage(this.message);
            builder.setPositiveButton(this.mo, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    try {
                        IntentIntegrator.this.gf.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.google.zxing.client.android")));
                    } catch (ActivityNotFoundException e) {
                        Log.w(IntentIntegrator.TAG, "Android Market is not installed; cannot install Barcode Scanner");
                    }
                }
            });
            builder.setNegativeButton(this.mp, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
            return builder.show();
        }

        private AlertDialog i(final Object obj) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.gf);
            builder.setTitle("提醒");
            builder.setMessage("使用二维码功能，需要先安装条码扫描软件");
            builder.setPositiveButton(this.mo, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    IntentIntegrator.b(IntentIntegrator.this.gf);
                }
            });
            builder.setNegativeButton(this.mp, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    if (obj != null) {
                        synchronized (obj) {
                            obj.notify();
                        }
                    }
                }
            });
            return builder.create();
        }

        public AlertDialog a(CharSequence charSequence, CharSequence charSequence2) {
            Intent intent = new Intent();
            intent.addCategory("android.intent.category.DEFAULT");
            intent.setAction("com.google.zxing.client.android.ENCODE");
            intent.putExtra("ENCODE_TYPE", charSequence2);
            intent.putExtra("ENCODE_DATA", charSequence);
            String a = a(intent);
            if (a == null) {
                return i(null);
            }
            intent.setPackage(a);
            intent.addFlags(k.OCTOBER);
            intent.addFlags(524288);
            b(intent);
            this.gf.startActivity(intent);
            return null;
        }

        public AlertDialog a(Collection<String> collection, Object obj) {
            Intent intent = new Intent("com.google.zxing.client.android.SCAN");
            intent.addCategory("android.intent.category.DEFAULT");
            if (collection != null) {
                StringBuilder sb = new StringBuilder();
                for (String next : collection) {
                    if (sb.length() > 0) {
                        sb.append(',');
                    }
                    sb.append(next);
                }
                intent.putExtra("SCAN_FORMATS", sb.toString());
            }
            String a = a(intent);
            if (a == null) {
                return i(obj);
            }
            intent.setPackage(a);
            intent.addFlags(k.OCTOBER);
            intent.addFlags(524288);
            b(intent);
            startActivityForResult(intent, REQUEST_CODE);
            return null;
        }

        public void a(Collection<String> collection) {
            this.mq = collection;
        }

        public void aL(String str) {
            this.mo = str;
        }

        public void aM(String str) {
            this.mp = str;
        }

        public void aN(String str) {
            this.mq = Collections.singleton(str);
        }

        public AlertDialog aO(String str) {
            return a(str, "TEXT_TYPE");
        }

        public void b(String str, Object obj) {
            this.mr.put(str, obj);
        }

        public void bT(int i) {
            this.title = this.gf.getString(i);
        }

        public void bU(int i) {
            this.message = this.gf.getString(i);
        }

        public void bV(int i) {
            this.mo = this.gf.getString(i);
        }

        public void bW(int i) {
            this.mp = this.gf.getString(i);
        }

        public String gM() {
            return this.mo;
        }

        public String gN() {
            return this.mp;
        }

        public Collection<String> gO() {
            return this.mq;
        }

        public Map<String, ?> gP() {
            return this.mr;
        }

        public String getMessage() {
            return this.message;
        }

        public String getTitle() {
            return this.title;
        }

        public AlertDialog h(Object obj) {
            return a(ml, obj);
        }

        public void setMessage(String str) {
            this.message = str;
        }

        public void setTitle(String str) {
            this.title = str;
        }

        /* access modifiers changed from: protected */
        public void startActivityForResult(Intent intent, int i) {
            this.gf.startActivityForResult(intent, i);
        }
    }

    private static final class a implements Callable<Boolean> {
        private a() {
        }

        /* synthetic */ a(a aVar) {
            this();
        }

        /* JADX WARNING: Removed duplicated region for block: B:22:0x008a  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x0092  */
        /* renamed from: gL */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Boolean call() {
            /*
                r7 = this;
                r1 = 0
                r2 = 0
                java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x009a }
                java.lang.String r3 = "http://www.baidu.com"
                r0.<init>(r3)     // Catch:{ Exception -> 0x009a }
                java.io.PrintStream r3 = java.lang.System.out     // Catch:{ Exception -> 0x009a }
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x009a }
                java.lang.String r5 = "TEST:"
                r4.<init>(r5)     // Catch:{ Exception -> 0x009a }
                java.lang.String r5 = r0.toString()     // Catch:{ Exception -> 0x009a }
                java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ Exception -> 0x009a }
                java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x009a }
                r3.println(r4)     // Catch:{ Exception -> 0x009a }
                java.net.URLConnection r0 = r0.openConnection()     // Catch:{ Exception -> 0x009a }
                java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ Exception -> 0x009a }
                java.lang.String r2 = "GET"
                r0.setRequestMethod(r2)     // Catch:{ Exception -> 0x0081, all -> 0x0096 }
                r2 = 5000(0x1388, float:7.006E-42)
                r0.setConnectTimeout(r2)     // Catch:{ Exception -> 0x0081, all -> 0x0096 }
                r0.connect()     // Catch:{ Exception -> 0x0081, all -> 0x0096 }
                int r2 = r0.getResponseCode()     // Catch:{ Exception -> 0x0081, all -> 0x0096 }
                java.io.PrintStream r3 = java.lang.System.out     // Catch:{ Exception -> 0x0081, all -> 0x0096 }
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0081, all -> 0x0096 }
                java.lang.String r5 = "ResponseCode:"
                r4.<init>(r5)     // Catch:{ Exception -> 0x0081, all -> 0x0096 }
                java.lang.StringBuilder r4 = r4.append(r2)     // Catch:{ Exception -> 0x0081, all -> 0x0096 }
                java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0081, all -> 0x0096 }
                r3.println(r4)     // Catch:{ Exception -> 0x0081, all -> 0x0096 }
                r3 = 200(0xc8, float:2.8E-43)
                if (r2 != r3) goto L_0x006c
                java.io.InputStream r2 = r0.getInputStream()     // Catch:{ Exception -> 0x0081, all -> 0x0096 }
                java.lang.String r2 = me.gall.verdandi.impl.Device.c(r2)     // Catch:{ Exception -> 0x0081, all -> 0x0096 }
                java.lang.String r3 = "百度"
                boolean r2 = r2.contains(r3)     // Catch:{ Exception -> 0x0081, all -> 0x0096 }
                if (r2 == 0) goto L_0x0061
                r1 = 1
            L_0x0061:
                if (r0 == 0) goto L_0x009c
                r0.disconnect()
                r0 = r1
            L_0x0067:
                java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
                return r0
            L_0x006c:
                java.lang.Exception r3 = new java.lang.Exception     // Catch:{ Exception -> 0x0081, all -> 0x0096 }
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0081, all -> 0x0096 }
                java.lang.String r5 = "responseCode:"
                r4.<init>(r5)     // Catch:{ Exception -> 0x0081, all -> 0x0096 }
                java.lang.StringBuilder r2 = r4.append(r2)     // Catch:{ Exception -> 0x0081, all -> 0x0096 }
                java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0081, all -> 0x0096 }
                r3.<init>(r2)     // Catch:{ Exception -> 0x0081, all -> 0x0096 }
                throw r3     // Catch:{ Exception -> 0x0081, all -> 0x0096 }
            L_0x0081:
                r2 = move-exception
                r6 = r2
                r2 = r0
                r0 = r6
            L_0x0085:
                r0.printStackTrace()     // Catch:{ all -> 0x008f }
                if (r2 == 0) goto L_0x009c
                r2.disconnect()
                r0 = r1
                goto L_0x0067
            L_0x008f:
                r0 = move-exception
            L_0x0090:
                if (r2 == 0) goto L_0x0095
                r2.disconnect()
            L_0x0095:
                throw r0
            L_0x0096:
                r1 = move-exception
                r2 = r0
                r0 = r1
                goto L_0x0090
            L_0x009a:
                r0 = move-exception
                goto L_0x0085
            L_0x009c:
                r0 = r1
                goto L_0x0067
            */
            throw new UnsupportedOperationException("Method not decompiled: me.gall.verdandi.impl.Device.a.call():java.lang.Boolean");
        }
    }

    static final class b {
        private final String mu;
        private final String mv;
        private final byte[] mw;
        private final Integer mx;
        private final String my;

        public b() {
            this(null, null, null, null, null);
        }

        public b(String str, String str2, byte[] bArr, Integer num, String str3) {
            this.mu = str;
            this.mv = str2;
            this.mw = bArr;
            this.mx = num;
            this.my = str3;
        }

        public String gS() {
            return this.mu;
        }

        public byte[] gT() {
            return this.mw;
        }

        public Integer gU() {
            return this.mx;
        }

        public String gV() {
            return this.my;
        }

        public String getFormatName() {
            return this.mv;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder(100);
            sb.append("Format: ").append(this.mv).append(10);
            sb.append("Contents: ").append(this.mu).append(10);
            sb.append("Raw bytes: (").append(this.mw == null ? 0 : this.mw.length).append(" bytes)\n");
            sb.append("Orientation: ").append(this.mx).append(10);
            sb.append("EC level: ").append(this.my).append(10);
            return sb.toString();
        }
    }

    public static final String c(InputStream inputStream) {
        return c(inputStream, e.f);
    }

    public static final String c(InputStream inputStream, String str) {
        if (inputStream == null) {
            return "";
        }
        byte[] bArr = new byte[1024];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                byteArrayOutputStream.flush();
                byteArrayOutputStream.close();
                String str2 = new String(byteArrayOutputStream.toByteArray(), str);
                System.out.println("Data:" + str2);
                inputStream.close();
                return str2;
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    public void aK(final String str) {
        final IntentIntegrator intentIntegrator = new IntentIntegrator(l.getActivity());
        final AlertDialog aO = intentIntegrator.aO(str);
        Log.d(getClass().getName(), "ShareText:" + str);
        if (aO != null) {
            h.a(new h.a() {
                public boolean b(Message message) {
                    if (message.what != 40961) {
                        return false;
                    }
                    if (IntentIntegrator.Y(l.getActivity())) {
                        h.b(this);
                        intentIntegrator.aO(str);
                        return false;
                    }
                    intentIntegrator.aO(str).show();
                    return false;
                }
            });
            l.getHandler().post(new Runnable() {
                public void run() {
                    if (aO != null) {
                        aO.show();
                    }
                }
            });
        }
    }

    public void b(j jVar) {
        h.ci(i.MSG_OPTIONMENU_ABOUT);
    }

    public synchronized boolean bS(int i) {
        boolean z;
        if (gu()) {
            FutureTask futureTask = new FutureTask(new a(null));
            new Thread(futureTask).start();
            try {
                z = ((Boolean) futureTask.get((long) i, TimeUnit.SECONDS)).booleanValue();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        z = false;
        return z;
    }

    public int gA() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        l.getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.densityDpi;
    }

    public String gB() {
        return l.gB();
    }

    public String gC() {
        try {
            return l.hJ().getSubscriberId();
        } catch (Exception e) {
            return "";
        }
    }

    public String gD() {
        return l.hY();
    }

    public String gE() {
        try {
            return l.hK().getConnectionInfo().getMacAddress();
        } catch (Exception e) {
            return "";
        }
    }

    public String gF() {
        return l.hW();
    }

    public String gG() {
        return l.gG();
    }

    public String gH() {
        h.a(new h.a() {
            public boolean b(Message message) {
                if (message.what == 47880) {
                    Object[] objArr = (Object[]) message.obj;
                    b a = IntentIntegrator.a(((int[]) objArr[0])[0], ((int[]) objArr[0])[1], (Intent) objArr[1]);
                    if (a != null) {
                        Device.this.md = a.gS();
                    }
                    synchronized (Device.this) {
                        Device.this.notify();
                    }
                    h.b(this);
                }
                return false;
            }
        });
        l.getHandler().post(new Runnable() {
            public void run() {
                AlertDialog h = new IntentIntegrator(l.getActivity()).h(Device.this);
                if (h != null) {
                    h.show();
                }
            }
        });
        synchronized (this) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (this.md != null) {
            Log.d(getClass().getName(), "GainText:" + this.md);
        }
        return this.md;
    }

    public File gI() {
        Object invoke = Class.forName("org.meteroid.core.SystemManager").getMethod("getActivity", new Class[0]).invoke(null, new Object[0]);
        return (File) invoke.getClass().getMethod("getFilesDir", new Class[0]).invoke(invoke, new Object[0]);
    }

    public String gJ() {
        Class<?> cls = Class.forName("android.os.SystemProperties");
        return (String) cls.getMethod("get", String.class, String.class).invoke(cls, "ro.serialno", "unknown");
    }

    public String gK() {
        return l.hX();
    }

    public String getDeviceName() {
        return "android-" + l.hU();
    }

    public int gt() {
        return 1;
    }

    public boolean gu() {
        return l.hC();
    }

    public String gv() {
        return l.gv();
    }

    public String gw() {
        return String.valueOf(l.hU());
    }

    public String gx() {
        try {
            TelephonyManager hJ = l.hJ();
            if (hJ.getSimState() == 5) {
                String subscriberId = hJ.getSubscriberId();
                if (subscriberId != null) {
                    return (subscriberId.startsWith("46000") || subscriberId.startsWith("46002")) ? "中国移动" : subscriberId.startsWith("46001") ? "中国联通" : subscriberId.startsWith("46003") ? "中国电信" : "其他运营商";
                }
                return "未知运营商";
            }
            throw new Exception("Sim card is not ready yet.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int gy() {
        return l.gy();
    }

    public int gz() {
        return l.gz();
    }
}
