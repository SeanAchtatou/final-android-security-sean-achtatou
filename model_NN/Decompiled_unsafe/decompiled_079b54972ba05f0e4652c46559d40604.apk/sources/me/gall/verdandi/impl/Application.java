package me.gall.verdandi.impl;

import android.content.pm.PackageManager;
import com.a.a.s.a;
import java.security.MessageDigest;
import me.gall.verdandi.IApplication;
import org.meteoroid.core.l;

public class Application implements IApplication {
    public boolean aI(String str) {
        return l.aI(str);
    }

    public boolean aJ(String str) {
        return l.aJ(str);
    }

    public String gb() {
        return l.gb();
    }

    public String getPackageName() {
        return l.getActivity().getPackageName();
    }

    public int gq() {
        try {
            return l.getActivity().getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return 1;
        }
    }

    public String gr() {
        try {
            return l.getActivity().getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String gs() {
        return a.encodeBytes(MessageDigest.getInstance("SHA-1").digest(l.getActivity().getPackageManager().getPackageInfo(l.getActivity().getPackageName(), 64).signatures[0].toByteArray()));
    }
}
