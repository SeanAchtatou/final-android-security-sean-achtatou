package me.gall.sgp.android.common;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import java.io.IOException;
import java.io.InputStream;

public class DrawableRes {
    private static BitmapDrawable bd;
    private static Bitmap bitmap;

    public static BitmapDrawable getDrawableRes(Context context, String str) {
        try {
            InputStream open = context.getResources().getAssets().open(str);
            bitmap = BitmapFactory.decodeStream(open);
            bd = new BitmapDrawable(context.getResources(), bitmap);
            open.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bd;
    }
}
