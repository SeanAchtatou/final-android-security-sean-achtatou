package me.gall.sgp.android.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.io.FileNotFoundException;

public final class Configuration {
    public static final String LOG_TAG = Configuration.class.getSimpleName();
    private static final String SGP_SETTING = "SGP_SETTING";
    private static boolean testMode;

    public static boolean containsManifestMetaKey(Context context, String str) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.containsKey(str);
        } catch (Exception e) {
            Log.d(LOG_TAG, e.getMessage() + " unknown meta key or not exist:" + str);
            return false;
        }
    }

    public static String contructServiceEndpoint(String str, String str2, Class cls) {
        String simpleName = cls.getSimpleName();
        if (simpleName.endsWith("Service")) {
            return str + "/" + str2 + "/" + simpleName.substring(0, simpleName.length() - 7).toLowerCase() + ".do";
        }
        throw new IllegalAccessException("Not a valid service endpoint class:" + cls.getName());
    }

    public static final View findViewByName(Context context, View view, String str) {
        return view.findViewById(getViewIdentifier(context, str));
    }

    public static String getDefaultGatewayEndpoint() {
        return isTestMode() ? "http://test.savenumber.com/gateway/route" : "http://sgp.gallme.com.cn/gateway/route";
    }

    public static final int getDrawableIdentifier(Context context, String str) {
        return getIdentifier(context, str, "drawable");
    }

    public static final int getIdentifier(Context context, String str, String str2) {
        int identifier = context.getResources().getIdentifier(str, str2, context.getPackageName());
        if (identifier != 0) {
            return identifier;
        }
        throw new FileNotFoundException(str + " is not found in res/" + str2 + "/.");
    }

    public static final int getLayoutIdentifier(Context context, String str) {
        return getIdentifier(context, str, "layout");
    }

    public static boolean getManifestMetaBooleanValue(Context context, String str, boolean z) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getBoolean(str);
        } catch (Exception e) {
            Log.d(LOG_TAG, e.getMessage() + " unknown meta key or not exist:" + str);
            return z;
        }
    }

    public static int getManifestMetaIntValue(Context context, String str, int i) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getInt(str);
        } catch (Exception e) {
            Log.d(LOG_TAG, e.getMessage() + " unknown meta key or not exist:" + str);
            return i;
        }
    }

    public static String getManifestMetaValue(Context context, String str) {
        try {
            return context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString(str);
        } catch (Exception e) {
            Log.d(LOG_TAG, e.getMessage() + " unknown meta key or not exist:" + str);
            return null;
        }
    }

    public static SharedPreferences getSetting(Context context) {
        return context.getApplicationContext().getSharedPreferences(SGP_SETTING, 0);
    }

    public static final int getStringIdentifier(Context context, String str) {
        return getIdentifier(context, str, "string");
    }

    public static final int getViewIdentifier(Context context, String str) {
        try {
            return getIdentifier(context, str, "id");
        } catch (FileNotFoundException e) {
            throw new NullPointerException(str + " is not found.");
        }
    }

    public static final View inflateView(Context context, String str) {
        return ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(getLayoutIdentifier(context, str), (ViewGroup) null);
    }

    public static boolean isTestMode() {
        return testMode;
    }

    public static void setTestMode(boolean z) {
        testMode = z;
    }
}
