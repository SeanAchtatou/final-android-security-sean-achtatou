package me.gall.sgp.android.common;

import com.umeng.common.b.e;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.microedition.media.control.ToneControl;

public class PasswardMD5 {
    public static String getMD5Str(String str) {
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(str.getBytes(e.f));
        } catch (NoSuchAlgorithmException e) {
            System.out.println("NoSuchAlgorithmException caught!");
            System.exit(-1);
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        byte[] digest = messageDigest.digest();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < digest.length; i++) {
            if (Integer.toHexString(digest[i] & ToneControl.SILENCE).length() == 1) {
                stringBuffer.append("0").append(Integer.toHexString(digest[i] & ToneControl.SILENCE));
            } else {
                stringBuffer.append(Integer.toHexString(digest[i] & ToneControl.SILENCE));
            }
        }
        return stringBuffer.substring(8, 24).toString().toUpperCase();
    }
}
