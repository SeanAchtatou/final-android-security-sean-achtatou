package me.gall.sgp.android.common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.util.Log;

public class NetworkInfo {
    public static final String LOG_TAG = NetworkInfo.class.getSimpleName();

    public static String getDataConnectionNetworkInfo(Context context) {
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0) {
            Log.d(LOG_TAG, "ACCESS_NETWORK_STATE permission denied.");
            return null;
        }
        try {
            return ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(0).getExtraInfo();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private static TelephonyManager getTelephonyManager(Context context) {
        return (TelephonyManager) context.getSystemService("phone");
    }

    public static final boolean isConnectedOrConnecting(Context context) {
        return isWifiConnect(context) || isMobileConnect(context);
    }

    public static boolean isMobileConnect(Context context) {
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0) {
            Log.d(LOG_TAG, "ACCESS_NETWORK_STATE permission denied.");
            return true;
        }
        try {
            android.net.NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(0);
            if (networkInfo.isConnectedOrConnecting() && networkInfo.isAvailable() && getTelephonyManager(context).getDataState() == 2) {
                Log.d(LOG_TAG, "mobile is connected. Type:" + networkInfo.getTypeName() + " APN:" + networkInfo.getExtraInfo());
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean isWifiConnect(Context context) {
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE") != 0) {
            Log.d(LOG_TAG, "ACCESS_WIFI_STATE permission denied.");
            return true;
        }
        try {
            return ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(1).isConnectedOrConnecting();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void setWifiEnabled(Context context, boolean z) {
        ((WifiManager) context.getSystemService("wifi")).setWifiEnabled(z);
        SystemClock.sleep(2000);
    }
}
