package me.gall.sgp.android.common;

import me.gall.sgp.sdk.entity.User;

public interface UserLoginListener {
    void exception(String str);

    void login(User user);
}
