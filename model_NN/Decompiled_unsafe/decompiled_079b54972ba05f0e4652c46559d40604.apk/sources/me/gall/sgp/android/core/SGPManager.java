package me.gall.sgp.android.core;

import android.content.Context;
import android.util.Log;
import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import com.googlecode.jsonrpc4j.ProxyUtil;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import me.gall.sgp.android.common.Configuration;
import me.gall.sgp.android.common.DeviceInfo;
import me.gall.sgp.android.common.NetworkInfo;
import me.gall.sgp.sdk.entity.Server;
import me.gall.sgp.sdk.entity.User;
import me.gall.sgp.sdk.service.AnnouncementService;
import me.gall.sgp.sdk.service.BlacklistService;
import me.gall.sgp.sdk.service.BossService;
import me.gall.sgp.sdk.service.CampaignService;
import me.gall.sgp.sdk.service.CheckinService;
import me.gall.sgp.sdk.service.FriendshipService;
import me.gall.sgp.sdk.service.GachaBoxService;
import me.gall.sgp.sdk.service.LeaderBoardService;
import me.gall.sgp.sdk.service.MailService;
import me.gall.sgp.sdk.service.RouterService;
import me.gall.sgp.sdk.service.SgpPlayerService;
import me.gall.sgp.sdk.service.StructuredDataService;
import me.gall.sgp.sdk.service.TicketService;
import me.gall.sgp.sdk.service.UserService;

public final class SGPManager {
    public static final String LOG_TAG = SGPManager.class.getSimpleName();
    private static SGPManager sgpManager;
    private String appId;
    private Context context;
    private Server currentServer;
    private User currentUser;
    private boolean offlineMode;
    private int requestTimeout = 10;

    private SGPManager() {
    }

    public static SGPManager getInstance(Context context2, String str, boolean z) {
        return getInstance(context2, str, z, false);
    }

    public static SGPManager getInstance(Context context2, String str, boolean z, boolean z2) {
        if (sgpManager == null || (sgpManager != null && !sgpManager.getAppId().equals(str))) {
            Log.d(LOG_TAG, "Create a new instance.");
            Configuration.setTestMode(z);
            sgpManager = new SGPManager();
            sgpManager.setContext(context2);
            sgpManager.setAppId(str);
            sgpManager.setOfflineMode(z2);
            sgpManager.init();
        }
        return sgpManager;
    }

    public void destroy() {
    }

    public AnnouncementService getAnnouncementService() {
        return (AnnouncementService) getService(AnnouncementService.class);
    }

    public String getAppId() {
        return this.appId;
    }

    public BlacklistService getBlacklistService() {
        return (BlacklistService) getService(BlacklistService.class);
    }

    public BossService getBossService() {
        return (BossService) getService(BossService.class);
    }

    public CampaignService getCampaignService() {
        return (CampaignService) getService(CampaignService.class);
    }

    public CheckinService getCheckinService() {
        return (CheckinService) getService(CheckinService.class);
    }

    public Context getContext() {
        return this.context;
    }

    public Server getCurrentServer() {
        return this.currentServer;
    }

    public User getCurrentUser() {
        return this.currentUser;
    }

    public FriendshipService getFriendshipService() {
        return (FriendshipService) getService(FriendshipService.class);
    }

    public GachaBoxService getGachaBoxService() {
        return (GachaBoxService) getService(GachaBoxService.class);
    }

    public LeaderBoardService getLeaderBoardService() {
        return (LeaderBoardService) getService(LeaderBoardService.class);
    }

    public MailService getMailService() {
        return (MailService) getService(MailService.class);
    }

    public int getRequestTimeout() {
        return this.requestTimeout;
    }

    public RouterService getRouterService() {
        return (RouterService) getService(RouterService.class);
    }

    public <T> T getService(Class<T> cls) {
        if (getAppId() == null || cls == null) {
            throw new Exception("Not initilized complete.");
        }
        String defaultGatewayEndpoint = cls == RouterService.class ? Configuration.getDefaultGatewayEndpoint() : Configuration.contructServiceEndpoint(getCurrentServer().getAddress(), getAppId(), cls);
        Log.d(LOG_TAG, "Getting " + cls.getName() + " at " + defaultGatewayEndpoint);
        try {
            return ProxyUtil.createClientProxy(getClass().getClassLoader(), cls, new JsonRpcHttpClient(new URL(defaultGatewayEndpoint)));
        } catch (Exception e) {
            Log.e(LOG_TAG, e.toString());
            e.printStackTrace();
            return null;
        }
    }

    public SgpPlayerService getSgpPlayerService() {
        return (SgpPlayerService) getService(SgpPlayerService.class);
    }

    public StructuredDataService getStructuredDataService() {
        return (StructuredDataService) getService(StructuredDataService.class);
    }

    public TicketService getTicketService() {
        return (TicketService) getService(TicketService.class);
    }

    public UserService getUserService() {
        return (UserService) getService(UserService.class);
    }

    public void init() {
        Log.d(LOG_TAG, "Start initilizing.");
    }

    public Server initRoute() {
        if (NetworkInfo.isConnectedOrConnecting(this.context)) {
            Server registerServer = getRouterService().getRegisterServer(this.appId);
            setCurrentServer(registerServer);
            return registerServer;
        }
        throw new Exception("Network is unavailable.");
    }

    public boolean isOfflineMode() {
        return this.offlineMode;
    }

    public User login(String str, String str2) {
        if (NetworkInfo.isConnectedOrConnecting(this.context)) {
            initRoute();
            User login = getUserService().login(str, str2);
            setCurrentUser(login);
            return login;
        }
        throw new Exception("Network is unavailable.");
    }

    public User quickLogin() {
        User register;
        Log.d(LOG_TAG, "Start quick login.");
        if (NetworkInfo.isConnectedOrConnecting(this.context)) {
            initRoute();
            String deviceIMEI = DeviceInfo.getDeviceIMEI(this.context);
            String deviceICCID = DeviceInfo.getDeviceICCID(this.context);
            String macAddress = DeviceInfo.getMacAddress(this.context);
            Log.d(LOG_TAG, "imei" + deviceIMEI + "iccid" + deviceICCID + "mac" + macAddress);
            if (!deviceIMEI.equals("") || !deviceICCID.equals("")) {
                Log.d(LOG_TAG, "This is a normal device.");
                register = getUserService().register(deviceIMEI, deviceICCID, macAddress);
            } else {
                Log.d(LOG_TAG, "This is a tablet device without 3g module. Use serialno as iccid.");
                String serialNo = DeviceInfo.getSerialNo();
                Log.d(LOG_TAG, "serialno" + serialNo);
                register = getUserService().register(null, serialNo, macAddress);
            }
            setCurrentUser(register);
            return register;
        }
        throw new Exception("Network is unavailable.");
    }

    public Server routeByChannel(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("channelId", str);
        return updateRouting(hashMap);
    }

    public void setAppId(String str) {
        this.appId = str;
    }

    public void setContext(Context context2) {
        this.context = context2;
    }

    public void setCurrentServer(Server server) {
        if (server == null) {
            throw new Exception("Get none server for invalid appid or routers.");
        }
        Log.d(LOG_TAG, "Set current server as " + server.getName() + "[" + server.getAddress() + "]");
        this.currentServer = server;
    }

    public void setCurrentUser(User user) {
        this.currentUser = user;
    }

    public void setOfflineMode(boolean z) {
        this.offlineMode = z;
    }

    public void setRequestTimeout(int i) {
        this.requestTimeout = i;
    }

    public User signup(String str, String str2) {
        if (NetworkInfo.isConnectedOrConnecting(this.context)) {
            initRoute();
            User user = new User();
            user.setUserName(str);
            user.setPassword(str2);
            String deviceIMEI = DeviceInfo.getDeviceIMEI(this.context);
            String deviceICCID = DeviceInfo.getDeviceICCID(this.context);
            String macAddress = DeviceInfo.getMacAddress(this.context);
            Log.d(LOG_TAG, "imei" + deviceIMEI + "iccid" + deviceICCID + "mac" + macAddress);
            if (!deviceIMEI.equals("") || !deviceICCID.equals("")) {
                Log.d(LOG_TAG, "This is a normal device.");
                user.setImei(deviceIMEI);
                user.setIccid(deviceICCID);
                user.setMac(macAddress);
            } else {
                Log.d(LOG_TAG, "This is a tablet device without 3g module. Use serialno as iccid.");
                String serialNo = DeviceInfo.getSerialNo();
                Log.d(LOG_TAG, "serialno" + serialNo);
                user = getUserService().register(null, serialNo, macAddress);
                user.setIccid(serialNo);
                user.setMac(macAddress);
            }
            User register = getUserService().register(user);
            setCurrentUser(register);
            return register;
        }
        throw new Exception("Network is unavailable.");
    }

    public Server updateRouting(Map<String, String> map) {
        if (NetworkInfo.isConnectedOrConnecting(this.context)) {
            try {
                Server route = getRouterService().route(getAppId(), map);
                setCurrentServer(route);
                return route;
            } catch (Throwable th) {
                th.printStackTrace();
                throw th;
            }
        } else {
            throw new Exception("Network is unavailable.");
        }
    }
}
