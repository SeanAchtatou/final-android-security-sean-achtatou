package me.gall.sgp.sdk.service;

import java.util.Map;
import me.gall.sgp.sdk.entity.Server;

public interface RouterService {
    public static final String APP_SIGN = "AppSign";
    public static final String CREATE_TIME = "createTime";
    public static final String USER_ID = "userId";

    long getCurrentTimestamp();

    Server getRegisterServer(String str);

    Server[] getServerList(String str);

    Server route(String str, Map<String, String> map);
}
