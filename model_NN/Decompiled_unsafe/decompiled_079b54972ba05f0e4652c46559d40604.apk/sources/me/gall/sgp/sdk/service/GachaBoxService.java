package me.gall.sgp.sdk.service;

import me.gall.sgp.sdk.entity.app.GachaBox;
import me.gall.sgp.sdk.entity.app.Lottery;

public interface GachaBoxService {
    String draw(String str, int i, int i2);

    String[] draw(String str, int i, int[] iArr);

    GachaBox[] getAvailableGachaBox();

    GachaBox getGachaBoxByName(String str);

    Lottery[] getLotteriesByGachaBoxId(Integer num);
}
