package me.gall.sgp.sdk.entity.app;

import java.io.Serializable;

public class Mail implements Serializable {
    public static final int DELETED = -1;
    public static final int NORMAL = 0;
    public static final int READ = 1;
    public static final int SYSTEM = 1;
    public static final int UNREAD = 0;
    private static final long serialVersionUID = 7765994647828819791L;
    private String attachment;
    private String content;
    private String fromId;
    private String fromName;
    private Integer id;
    private Long sendTime;
    private Integer status;
    private String title;
    private String toId;
    private String toName;
    private Integer type;

    public String getAttachment() {
        return this.attachment;
    }

    public String getContent() {
        return this.content;
    }

    public String getFromId() {
        return this.fromId;
    }

    public String getFromName() {
        return this.fromName;
    }

    public Integer getId() {
        return this.id;
    }

    public Long getSendTime() {
        return this.sendTime;
    }

    public Integer getStatus() {
        return this.status;
    }

    public String getTitle() {
        return this.title;
    }

    public String getToId() {
        return this.toId;
    }

    public String getToName() {
        return this.toName;
    }

    public Integer getType() {
        return this.type;
    }

    public void setAttachment(String str) {
        this.attachment = str;
    }

    public void setContent(String str) {
        this.content = str;
    }

    public void setFromId(String str) {
        this.fromId = str;
    }

    public void setFromName(String str) {
        this.fromName = str;
    }

    public void setId(Integer num) {
        this.id = num;
    }

    public void setSendTime(Long l) {
        this.sendTime = l;
    }

    public void setStatus(Integer num) {
        this.status = num;
    }

    public void setTitle(String str) {
        this.title = str;
    }

    public void setToId(String str) {
        this.toId = str;
    }

    public void setToName(String str) {
        this.toName = str;
    }

    public void setType(Integer num) {
        this.type = num;
    }
}
