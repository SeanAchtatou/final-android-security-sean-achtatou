package me.gall.sgp.sdk.entity.app;

import java.io.Serializable;

public class Reward implements Serializable {
    private static final long serialVersionUID = -1241243920764437327L;
    private String content;
    private Integer id;

    public String getContent() {
        return this.content;
    }

    public Integer getId() {
        return this.id;
    }

    public void setContent(String str) {
        this.content = str;
    }

    public void setId(Integer num) {
        this.id = num;
    }
}
