package me.gall.sgp.sdk.service;

import me.gall.sgp.sdk.entity.User;

public interface UserService {
    User login(String str, String str2);

    @Deprecated
    User register(String str, String str2);

    User register(String str, String str2, String str3);

    User register(User user);

    User updateUser(User user);
}
