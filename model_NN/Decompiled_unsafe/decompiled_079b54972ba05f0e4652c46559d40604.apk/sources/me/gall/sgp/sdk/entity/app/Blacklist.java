package me.gall.sgp.sdk.entity.app;

import java.io.Serializable;

public class Blacklist implements Serializable {
    public static final int TYPE_OF_NORMAL = 1;
    private static final long serialVersionUID = -4528317412612512605L;
    private String blacklistId;
    private String description;
    private Integer id;
    private String name;
    private Integer type;

    public String getBlacklistId() {
        return this.blacklistId;
    }

    public String getDescription() {
        return this.description;
    }

    public Integer getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public Integer getType() {
        return this.type;
    }

    public void setBlacklistId(String str) {
        this.blacklistId = str;
    }

    public void setDescription(String str) {
        this.description = str;
    }

    public void setId(Integer num) {
        this.id = num;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setType(Integer num) {
        this.type = num;
    }
}
