package me.gall.sgp.sdk.entity.app;

import java.io.Serializable;

public class Ticket implements Serializable {
    public static final int STATUS_OF_PENDING = 1;
    public static final int STATUS_OF_PROCESSED = 2;
    public static final int TYPE_OF_NORMAL = 1;
    private static final long serialVersionUID = -3520286961330867161L;
    private String content;
    private Long createTime;
    private String feedbackInfo;
    private Integer id;
    private String senderPlayerId;
    private Integer status;
    private String targetPlayerId;
    private String title;
    private Integer type;

    public String getContent() {
        return this.content;
    }

    public Long getCreateTime() {
        return this.createTime;
    }

    public String getFeedbackInfo() {
        return this.feedbackInfo;
    }

    public Integer getId() {
        return this.id;
    }

    public String getSenderPlayerId() {
        return this.senderPlayerId;
    }

    public Integer getStatus() {
        return this.status;
    }

    public String getTargetPlayerId() {
        return this.targetPlayerId;
    }

    public String getTitle() {
        return this.title;
    }

    public Integer getType() {
        return this.type;
    }

    public void setContent(String str) {
        this.content = str;
    }

    public void setCreateTime(Long l) {
        this.createTime = l;
    }

    public void setFeedbackInfo(String str) {
        this.feedbackInfo = str;
    }

    public void setId(Integer num) {
        this.id = num;
    }

    public void setSenderPlayerId(String str) {
        this.senderPlayerId = str;
    }

    public void setStatus(Integer num) {
        this.status = num;
    }

    public void setTargetPlayerId(String str) {
        this.targetPlayerId = str;
    }

    public void setTitle(String str) {
        this.title = str;
    }

    public void setType(Integer num) {
        this.type = num;
    }
}
