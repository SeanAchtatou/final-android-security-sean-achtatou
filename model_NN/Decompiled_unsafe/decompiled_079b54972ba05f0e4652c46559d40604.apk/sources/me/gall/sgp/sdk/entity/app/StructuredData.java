package me.gall.sgp.sdk.entity.app;

import java.io.Serializable;

public class StructuredData implements Serializable {
    public static final String TYPE_OF_LIST = "list";
    public static final String TYPE_OF_MAP = "map";
    public static final String TYPE_OF_SET = "set";
    public static final String TYPE_OF_VALUE = "value";
    public static final String TYPE_OF_ZSET = "zset";
    private static final long serialVersionUID = -8770932367976268561L;
    private Integer id;
    private String key;
    private String name;
    private String type;

    public Integer getId() {
        return this.id;
    }

    public String getKey() {
        return this.key;
    }

    public String getName() {
        return this.name;
    }

    public String getType() {
        return this.type;
    }

    public void setId(Integer num) {
        this.id = num;
    }

    public void setKey(String str) {
        this.key = str;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setType(String str) {
        this.type = str;
    }
}
