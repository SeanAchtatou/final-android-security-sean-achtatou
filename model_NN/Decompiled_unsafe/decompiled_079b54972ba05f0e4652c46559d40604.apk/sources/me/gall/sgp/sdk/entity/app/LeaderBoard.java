package me.gall.sgp.sdk.entity.app;

import java.io.Serializable;

public class LeaderBoard implements Serializable {
    private static final long serialVersionUID = -8913118161308432392L;
    private String activityId;
    private String appId;
    private Integer id;
    private String leaderId;
    private String name;
    private String serverId;

    public String getActivityId() {
        return this.activityId;
    }

    public String getAppId() {
        return this.appId;
    }

    public Integer getId() {
        return this.id;
    }

    public String getLeaderId() {
        return this.leaderId;
    }

    public String getName() {
        return this.name;
    }

    public String getServerId() {
        return this.serverId;
    }

    public void setActivityId(String str) {
        this.activityId = str;
    }

    public void setAppId(String str) {
        this.appId = str;
    }

    public void setId(Integer num) {
        this.id = num;
    }

    public void setLeaderId(String str) {
        this.leaderId = str;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setServerId(String str) {
        this.serverId = str;
    }
}
