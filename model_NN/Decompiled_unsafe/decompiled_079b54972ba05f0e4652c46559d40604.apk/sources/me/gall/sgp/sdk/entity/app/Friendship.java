package me.gall.sgp.sdk.entity.app;

import java.io.Serializable;

public class Friendship implements Serializable {
    public static final int FRIEND = 1;
    public static final int REFUSE = 2;
    public static final int UN_CONFIRM = 0;
    private static final long serialVersionUID = -6597601271790579729L;
    private Integer id;
    private String receiveId;
    private String sendId;
    private Integer status;

    public Integer getId() {
        return this.id;
    }

    public String getReceiveId() {
        return this.receiveId;
    }

    public String getSendId() {
        return this.sendId;
    }

    public Integer getStatus() {
        return this.status;
    }

    public void setId(Integer num) {
        this.id = num;
    }

    public void setReceiveId(String str) {
        this.receiveId = str;
    }

    public void setSendId(String str) {
        this.sendId = str;
    }

    public void setStatus(Integer num) {
        this.status = num;
    }
}
