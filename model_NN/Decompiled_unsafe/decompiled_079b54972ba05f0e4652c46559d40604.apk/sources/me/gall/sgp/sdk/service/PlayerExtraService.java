package me.gall.sgp.sdk.service;

import java.util.Map;

public interface PlayerExtraService {
    void addPlayer(Object obj);

    void deletePlayerById(String str);

    Object findAll(int i, int i2);

    Object findAll(Map<String, String> map, int i, int i2);

    Object getPlayerById(String str);

    Object getPlayerList(Map<String, String> map);

    void updatePlayer(String str, Object obj);

    void updatePlayer(Map<String, Object> map);
}
