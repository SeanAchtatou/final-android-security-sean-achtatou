package me.gall.sgp.sdk.service;

import java.util.Map;
import java.util.Set;

public interface StructuredDataService {
    void addListValue(String str, String str2);

    void addSetValue(String str, String str2);

    Set<String> addZSetValue(String str, String str2, double d);

    boolean containsHash(String str, String str2);

    boolean containsKey(String str);

    boolean containtSet(String str, String str2);

    String getHashValue(String str, String str2);

    Map<String, String> getHashValue(String str);

    String[] getListByIndex(String str, long j, long j2);

    String[] getListValue(String str);

    Set<String> getSetValue(String str);

    String getValue(String str);

    Set<String> getZSetRangByScore(String str, double d, double d2);

    Set<String> getZSetValue(String str);

    void remove(String str);

    void removeFromHash(String str, String str2);

    void removeFromSet(String str, String str2);

    void saveOrUpdateHashValue(String str, String str2, String str3);

    void saveOrUpdateHashValue(String str, Map<String, String> map);

    void saveOrUpdateValue(String str, String str2);

    void updateListValue(String str, String str2, String str3);

    void updateSetValue(String str, String str2, String str3);

    Set<String> updateZSetValue(String str, String str2, double d, String str3);
}
