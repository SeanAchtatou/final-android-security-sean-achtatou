package me.gall.sgp.sdk.entity.app;

import java.io.Serializable;

public class LeaderBoardScore implements Serializable {
    private static final long serialVersionUID = -4149479347566612853L;
    private Integer index;
    private SgpPlayer player;
    private Integer score;

    public Integer getIndex() {
        return this.index;
    }

    public SgpPlayer getPlayer() {
        return this.player;
    }

    public Integer getScore() {
        return this.score;
    }

    public void setIndex(Integer num) {
        this.index = num;
    }

    public void setPlayer(SgpPlayer sgpPlayer) {
        this.player = sgpPlayer;
    }

    public void setScore(Integer num) {
        this.score = num;
    }
}
