package me.gall.sgp.sdk.entity.app;

import java.io.Serializable;

public class SgpPlayer implements Serializable {
    private static final long serialVersionUID = -6925849405165538230L;
    private String customId;
    private String equip;
    private String id;
    private Long lastLoginTime;
    private Integer level;
    private String name;
    private String serverId;
    private String type;
    private String userId;
    private Integer vip;

    public String getCustomId() {
        return this.customId;
    }

    public String getEquip() {
        return this.equip;
    }

    public String getId() {
        return this.id;
    }

    public Long getLastLoginTime() {
        return this.lastLoginTime;
    }

    public Integer getLevel() {
        return this.level;
    }

    public String getName() {
        return this.name;
    }

    public String getServerId() {
        return this.serverId;
    }

    public String getType() {
        return this.type;
    }

    public String getUserId() {
        return this.userId;
    }

    public Integer getVip() {
        return this.vip;
    }

    public void setCustomId(String str) {
        this.customId = str;
    }

    public void setEquip(String str) {
        this.equip = str;
    }

    public void setId(String str) {
        this.id = str;
    }

    public void setLastLoginTime(Long l) {
        this.lastLoginTime = l;
    }

    public void setLevel(Integer num) {
        this.level = num;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setServerId(String str) {
        this.serverId = str;
    }

    public void setType(String str) {
        this.type = str;
    }

    public void setUserId(String str) {
        this.userId = str;
    }

    public void setVip(Integer num) {
        this.vip = num;
    }
}
