package me.gall.sgp.sdk.entity.app;

import java.io.Serializable;

public class Boss implements Serializable {
    public static final int PRIVATE = 1;
    public static final int PUBLIC = 2;
    private static final long serialVersionUID = -6043201574149394617L;
    private Integer hp;
    private Integer id;
    private String intensity;
    private String name;
    private String rewards;
    private Integer type;

    public Integer getHp() {
        return this.hp;
    }

    public Integer getId() {
        return this.id;
    }

    public String getIntensity() {
        return this.intensity;
    }

    public String getName() {
        return this.name;
    }

    public String getRewards() {
        return this.rewards;
    }

    public Integer getType() {
        return this.type;
    }

    public void setHp(Integer num) {
        this.hp = num;
    }

    public void setId(Integer num) {
        this.id = num;
    }

    public void setIntensity(String str) {
        this.intensity = str;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setRewards(String str) {
        this.rewards = str;
    }

    public void setType(Integer num) {
        this.type = num;
    }
}
