package me.gall.sgp.sdk.entity.app;

import java.io.Serializable;

public class Campaign implements Serializable {
    private static final long serialVersionUID = -8038436247852684414L;
    private String bossIds;
    private String brief;
    private Long endTime;
    private String icon;
    private Integer id;
    private String leaderboardIds;
    private Integer progress;
    private String[] rewardContent;
    private String rewards;
    private Long startTime;
    private String title;
    private String type;

    public String getBossIds() {
        return this.bossIds;
    }

    public String getBrief() {
        return this.brief;
    }

    public Long getEndTime() {
        return this.endTime;
    }

    public String getIcon() {
        return this.icon;
    }

    public Integer getId() {
        return this.id;
    }

    public String getLeaderboardIds() {
        return this.leaderboardIds;
    }

    public Integer getProgress() {
        return this.progress;
    }

    public String[] getRewardContent() {
        return this.rewardContent;
    }

    public String getRewards() {
        return this.rewards;
    }

    public Long getStartTime() {
        return this.startTime;
    }

    public String getTitle() {
        return this.title;
    }

    public String getType() {
        return this.type;
    }

    public void setBossIds(String str) {
        this.bossIds = str;
    }

    public void setBrief(String str) {
        this.brief = str;
    }

    public void setEndTime(Long l) {
        this.endTime = l;
    }

    public void setIcon(String str) {
        this.icon = str;
    }

    public void setId(Integer num) {
        this.id = num;
    }

    public void setLeaderboardIds(String str) {
        this.leaderboardIds = str;
    }

    public void setProgress(Integer num) {
        this.progress = num;
    }

    public void setRewardContent(String[] strArr) {
        this.rewardContent = strArr;
    }

    public void setRewards(String str) {
        this.rewards = str;
    }

    public void setStartTime(Long l) {
        this.startTime = l;
    }

    public void setTitle(String str) {
        this.title = str;
    }

    public void setType(String str) {
        this.type = str;
    }
}
