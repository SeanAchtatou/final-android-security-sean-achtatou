package me.gall.sgp.sdk.service;

import java.util.List;
import me.gall.sgp.sdk.entity.app.LeaderBoard;
import me.gall.sgp.sdk.entity.app.LeaderBoardScore;
import me.gall.sgp.sdk.entity.app.SgpPlayer;

public interface LeaderBoardService {
    boolean addUpLeaderBoardScore(String str, String str2, int i);

    boolean[] addUpLeaderBoardScore(String str, String[] strArr, int[] iArr);

    LeaderBoard getLeaderBoardByLeaderId(String str);

    List<LeaderBoardScore> getLeaderBoardScoreByExample(String str, SgpPlayer sgpPlayer);

    LeaderBoardScore getLeaderBoardScoreByLeaderIdAndPlayerId(String str, String str2);

    List<LeaderBoardScore> getTopLeaderBoardScoreByLeaderId(String str, int i, int i2);

    boolean submitLeaderBoardScore(String str, String str2, int i);

    boolean[] submitLeaderBoardScore(String str, String[] strArr, int[] iArr);
}
