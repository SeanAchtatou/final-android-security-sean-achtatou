package com.admob.android.ads;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class AdManager {
    private static final long LOCATION_UPDATE_INTERVAL = 900000;
    static final String LOG = "AdMob SDK";
    static final String SDK_SITE_ID = "a1496ced2842262";
    public static final String SDK_VERSION = "20091123-ANDROID-3312276cc1406347";
    private static final String SDK_VERSION_CHECKSUM = "3312276cc1406347";
    static final String SDK_VERSION_DATE = "20091123";
    private static GregorianCalendar birthday;
    /* access modifiers changed from: private */
    public static Location coordinates;
    /* access modifiers changed from: private */
    public static long coordinatesTimestamp;
    private static Gender gender;
    private static String publisherId;
    private static boolean testMode;
    private static String userAgent;
    private static String userId;

    public enum Gender {
        MALE,
        FEMALE
    }

    static {
        Log.i(LOG, j.a("L\u0001@\noE^!FE{\u0000\u0016d\ncEd\u0016-W=U4T<W>HL+I7B,IH>V<W?R;\u0006nT9U;V9R"));
    }

    protected static void clientError(String str) {
        Log.e(LOG, str);
        throw new IllegalArgumentException(str);
    }

    public static GregorianCalendar getBirthday() {
        return birthday;
    }

    static String getBirthdayAsString() {
        GregorianCalendar birthday2 = getBirthday();
        if (birthday2 == null) {
            return null;
        }
        return String.format(d.a("\u0015J\u0004\u001e\u0015J\u0002\u001e\u0015J\u0002\u001e"), Integer.valueOf(birthday2.get(1)), Integer.valueOf(birthday2.get(2) + 1), Integer.valueOf(birthday2.get(5)));
    }

    public static Location getCoordinates(Context context) {
        String str;
        String str2;
        boolean z;
        final LocationManager locationManager;
        LocationManager locationManager2 = null;
        boolean z2 = false;
        if (context != null && (coordinates == null || System.currentTimeMillis() > coordinatesTimestamp + LOCATION_UPDATE_INTERVAL)) {
            synchronized (context) {
                if (coordinates == null || System.currentTimeMillis() > coordinatesTimestamp + LOCATION_UPDATE_INTERVAL) {
                    coordinatesTimestamp = System.currentTimeMillis();
                    if (context.checkCallingOrSelfPermission(j.a("\u0004c\u0001\nd\u0001#\u0015h\u0017`\f~\u0016d\ncKL&N ^6R&B$_6H:A*N$Y,B+")) == 0) {
                        if (Log.isLoggable(LOG, 3)) {
                            Log.d(LOG, d.a(".B\u0003Y\u0014WZD\u0015\u0010\u001dU\u000e\u0010\u0016_\u0019Q\u000eY\u0015^\t\u0010\u001cB\u0015]ZD\u0012UZ^\u001fD\r_\b[T"));
                        }
                        LocationManager locationManager3 = (LocationManager) context.getSystemService(j.a("a\nn\u0004y\fb\u000b"));
                        if (locationManager3 != null) {
                            Criteria criteria = new Criteria();
                            criteria.setAccuracy(2);
                            criteria.setCostAllowed(false);
                            str = locationManager3.getBestProvider(criteria, true);
                            locationManager2 = locationManager3;
                            z2 = true;
                        } else {
                            str = null;
                            locationManager2 = locationManager3;
                            z2 = true;
                        }
                    } else {
                        str = null;
                    }
                    if (str == null && context.checkCallingOrSelfPermission(d.a("\u001b^\u001eB\u0015Y\u001e\u001e\nU\b]\u0013C\tY\u0015^Tq9s?c)o<y4u%|5s;d34")) == 0) {
                        if (Log.isLoggable(LOG, 3)) {
                            Log.d(LOG, j.a("1\u001cd\u000bjEy\n-\u0002h\u0011-\tb\u0006l\u0011d\nc\u0016-\u0003\n`EJ5^K"));
                        }
                        locationManager = (LocationManager) context.getSystemService(d.a("\\\u0015S\u001bD\u0013_\u0014"));
                        if (locationManager != null) {
                            Criteria criteria2 = new Criteria();
                            criteria2.setAccuracy(1);
                            criteria2.setCostAllowed(false);
                            str2 = locationManager.getBestProvider(criteria2, true);
                            z = true;
                        } else {
                            str2 = str;
                            z = true;
                        }
                    } else {
                        str2 = str;
                        z = z2;
                        locationManager = locationManager2;
                    }
                    if (!z) {
                        if (Log.isLoggable(LOG, 3)) {
                            Log.d(LOG, j.a("N\u0004c\u000bb\u0011-\u0004n\u0006h\u0016~Ex\u0016h\u0017*\u0016-\tb\u0006l\u0011d\ncK-E]\u0000\bd\u0016~\fb\u000b~El\u0017hEc\nyE~\u0000yK"));
                        }
                    } else if (str2 != null) {
                        Log.i(LOG, j.a(")b\u0006l\u0011d\ncE}\u0017b\u0013d\u0001h\u0017-\u0016h\u0011x\u0015-\u0016x\u0006n\u0000~\u0016k\u0010a\ttK"));
                        locationManager.requestLocationUpdates(str2, 0, 0.0f, new LocationListener() {
                            public void onLocationChanged(Location location) {
                                Location unused = AdManager.coordinates = location;
                                long unused2 = AdManager.coordinatesTimestamp = System.currentTimeMillis();
                                locationManager.removeUpdates(this);
                                Log.i(AdManager.LOG, d.a(";A\u000fY\bU\u001e\u0010\u0016_\u0019Q\u000eY\u0015^Z") + AdManager.coordinates.getLatitude() + d.a("V") + AdManager.coordinates.getLongitude() + d.a("\u0010\u001bDZ") + new Date(AdManager.coordinatesTimestamp).toString() + d.a("T"));
                            }

                            public void onProviderDisabled(String str) {
                            }

                            public void onProviderEnabled(String str) {
                            }

                            public void onStatusChanged(String str, int i, Bundle bundle) {
                            }
                        }, context.getMainLooper());
                    } else if (Log.isLoggable(LOG, 3)) {
                        Log.d(LOG, d.a("~\u0015\u0010\u0016_\u0019Q\u000eY\u0015^Z@\b_\fY\u001eU\bCZQ\bUZQ\fQ\u0013\\\u001bR\u0016UT\u0010Zq\u001eCZG\u0013\\\u0016\u0010\u0014_\u000e\u0010\u0018UZW\u001f_\u000eQ\bW\u001fD\u001fTT"));
                    }
                }
            }
        }
        return coordinates;
    }

    static String getCoordinatesAsString(Context context) {
        String str = null;
        Location coordinates2 = getCoordinates(context);
        if (coordinates2 != null) {
            str = coordinates2.getLatitude() + d.a("V") + coordinates2.getLongitude();
        }
        if (Log.isLoggable(LOG, 3)) {
            Log.d(LOG, j.a("0~\u0000En\nb\u0017i\fc\u0004y\u0000~El\u0017hE") + str);
        }
        return str;
    }

    public static Gender getGender() {
        return gender;
    }

    static String getGenderAsString() {
        if (gender == Gender.MALE) {
            return d.a("\u0017");
        }
        if (gender == Gender.FEMALE) {
            return "f";
        }
        return null;
    }

    public static String getPublisherId(Context context) {
        if (publisherId == null) {
            try {
                ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
                if (applicationInfo != null) {
                    String string = applicationInfo.metaData.getString(j.a("L!@*O:]0O)D6E _:D!"));
                    Log.d(LOG, d.a("`\u000fR\u0016Y\tX\u001fBZy>\u0010\bU\u001bTZV\b_\u0017\u0010;^\u001eB\u0015Y\u001e}\u001b^\u0013V\u001fC\u000e\u001e\u0002]\u0016\u0010\u0013CZ") + string);
                    if (!string.equals(SDK_SITE_ID) || (!context.getPackageName().equals(j.a("n\n`Kl\u0001`\noKl\u000bi\u0017b\fiKy\u0000~\u0011")) && !context.getPackageName().equals(d.a("\u0019_\u0017\u001e\u001fH\u001b]\n\\\u001f\u001e\u001bT\u0017_\u0018\u001e\u0016E\u0014Q\b\\\u001b^\u001eU\b")))) {
                        setPublisherId(string);
                    } else {
                        Log.i(LOG, j.a("1e\f~Ed\u0016-\u0004-\u0016l\b}\thEl\u0015}\td\u0006l\u0011d\ncE~\n-\u0004a\tb\u0012d\u000bjE~\u0004`\u0015a\u0000-\u0015x\u0007a\f~\rh\u0017-,IK"));
                        publisherId = string;
                    }
                }
            } catch (Exception e) {
                Log.e(LOG, d.a("9_\u000f\\\u001e\u0010\u0014_\u000e\u0010\bU\u001bTZq>}5r%`/r6y)x?b%y>\u0010\u0017U\u000eQWT\u001bD\u001b\u0010\u001cB\u0015]Zq\u0014T\b_\u0013T7Q\u0014Y\u001cU\tDTH\u0017\\T"), e);
            }
        }
        return publisherId;
    }

    static String getUserAgent() {
        if (userAgent == null) {
            StringBuffer stringBuffer = new StringBuffer();
            String str = Build.VERSION.RELEASE;
            if (str.length() > 0) {
                stringBuffer.append(str);
            } else {
                stringBuffer.append(j.a("T#U"));
            }
            stringBuffer.append(j.a("6E"));
            Locale locale = Locale.getDefault();
            String language = locale.getLanguage();
            if (language != null) {
                stringBuffer.append(language.toLowerCase());
                String country = locale.getCountry();
                if (country != null) {
                    stringBuffer.append(d.a("W"));
                    stringBuffer.append(country.toLowerCase());
                }
            } else {
                stringBuffer.append("en");
            }
            String str2 = Build.MODEL;
            if (str2.length() > 0) {
                stringBuffer.append(j.a("6E"));
                stringBuffer.append(str2);
            }
            String str3 = Build.ID;
            if (str3.length() > 0) {
                stringBuffer.append(d.a("Zr\u000fY\u0016TU"));
                stringBuffer.append(str3);
            }
            userAgent = String.format(j.a("@\nw\fa\tlJ8K=E%)d\u000bx\u001d6EX^-$c\u0001\nd\u0001-@~L-$}\u0015a\u0000Z\u0000o.d\u0011\"P?P#T=N-MF-Y(AI-\td\u000ehEJ\u0000n\u000ebL-3h\u0017~\fb\u000b\"V#U#Q-(b\u0007d\thE^\u0004k\u0004\f\"P?V#T?K?E%$i(b\u0007 $C!_*D! @~L"), stringBuffer, SDK_VERSION_DATE);
            if (Log.isLoggable(LOG, 3)) {
                Log.d(LOG, d.a("`\u0012_\u0014U]CZE\tU\b\u001d\u001bW\u001f^\u000e\u0010\u0013C@\u0010Z") + userAgent);
            }
        }
        return userAgent;
    }

    public static String getUserId(Context context) {
        if (userId == null) {
            userId = Settings.System.getString(context.getContentResolver(), d.a("Q\u0014T\b_\u0013T%Y\u001e"));
            userId = md5(userId);
            Log.i(LOG, j.a("1e\u0000-\u0010~\u0000ED!-\f~E") + userId);
        }
        return userId;
    }

    public static boolean isInTestMode() {
        return testMode;
    }

    private static /* synthetic */ String md5(String str) {
        if (str == null || str.length() <= 0) {
            return null;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance(d.a("7tO"));
            instance.update(str.getBytes(), 0, str.length());
            return String.format(j.a("@=V?="), new BigInteger(1, instance.digest()));
        } catch (Exception e) {
            Log.d(LOG, d.a("9_\u000f\\\u001e\u0010\u0014_\u000e\u0010\u001dU\u0014U\bQ\u000eUZX\u001bC\u0012\u0010\u0015VZ") + str, e);
            userId = userId.substring(0, 32);
            return null;
        }
    }

    public static void setBirthday(int i, int i2, int i3) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(i, i2 - 1, i3);
        setBirthday(gregorianCalendar);
    }

    public static void setBirthday(GregorianCalendar gregorianCalendar) {
        birthday = gregorianCalendar;
    }

    public static void setGender(Gender gender2) {
        gender = gender2;
    }

    public static void setInTestMode(boolean z) {
        testMode = z;
    }

    public static void setPublisherId(String str) {
        if (str == null || str.length() != 15) {
            clientError(j.a("^ Y0]EH7_*__-ED\u000bn\n\u0017h\u0006yEL\u0001@\noE}\u0010o\td\u0016e\u0000ED!#E-6e\nx\tiE<P->lHkI=H48-\u0006e\u0004\u0004n\u0011h\u0017~_-E") + publisherId);
        }
        if (str.equalsIgnoreCase(SDK_SITE_ID)) {
            clientError(d.a(")u.e*\u0010?b((\nZ\u00109Q\u0014^\u0015DZE\tUZD\u0012UZC\u001b]\n\\\u001f\u0010\nE\u0018\\\u0013C\u0012U\b\u00103tZ\u0018\u001b\u0001N\tLS\u001fTH\bN\u0002H\u0006H\u0019T\u0010Zi\u0015E\bCZY\t\u0010\u001bF\u001bY\u0016Q\u0018\\\u001f\u0010\u0015^ZG\rGTQ\u001e]\u0015RTS\u0015]T"));
        }
        Log.i(LOG, j.a("]\u0010o\td\u0016e\u0000ED!-\u0016h\u0011-\u0011bE") + str);
        publisherId = str;
    }
}
