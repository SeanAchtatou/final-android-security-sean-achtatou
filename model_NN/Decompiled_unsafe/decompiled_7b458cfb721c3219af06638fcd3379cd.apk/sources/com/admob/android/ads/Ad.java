package com.admob.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Html;
import android.util.Log;

class Ad {
    /* access modifiers changed from: private */
    public static int CLICK_REQUEST_TIMEOUT = 5000;
    /* access modifiers changed from: private */
    public String clickURL;
    /* access modifiers changed from: private */
    public Context context;
    private String html;
    private Bitmap icon;
    private String iconURL;
    private Bitmap image;
    private int imageHeight;
    private String imageURL;
    private int imageWidth;
    /* access modifiers changed from: private */
    public NetworkListener networkListener;
    private String text;

    interface NetworkListener {
        void onNetworkActivityEnd();

        void onNetworkActivityStart();
    }

    private /* synthetic */ Ad() {
    }

    public static Ad createAd(Context context2, String str, String str2) {
        if (str == null || str.equals("")) {
            return null;
        }
        Ad ad = new Ad();
        ad.context = context2;
        ad.html = str;
        ad.iconURL = str2;
        try {
            int indexOf = str.indexOf(j.a("YlE"));
            if (indexOf >= 0) {
                int indexOf2 = str.indexOf(d.a("ZX\bU\u001c\rX"), indexOf) + 7;
                int indexOf3 = str.indexOf(j.a("G"), indexOf2);
                ad.clickURL = str.substring(indexOf2, indexOf3);
                indexOf = skipToNext(str, indexOf3 + 1);
                if (indexOf < 0) {
                    return null;
                }
            }
            if (indexOf >= 0 && str.indexOf(d.a("\f\u0013]\u001d"), indexOf) == indexOf) {
                int indexOf4 = str.indexOf(j.a("-\u0016\u00060G"), indexOf) + 6;
                ad.imageURL = str.substring(indexOf4, str.indexOf(d.a("X"), indexOf4));
                int indexOf5 = str.indexOf(j.a("Ee\u0000d\u0002e\u00110G"), indexOf) + 9;
                ad.imageHeight = Integer.valueOf(str.substring(indexOf5, str.indexOf(d.a("X"), indexOf5))).intValue();
                int indexOf6 = str.indexOf(j.a("-\u0012d\u0001y\r0G"), indexOf) + 8;
                int indexOf7 = str.indexOf(d.a("X"), indexOf6);
                ad.imageWidth = Integer.valueOf(str.substring(indexOf6, indexOf7)).intValue();
                indexOf = str.indexOf(j.a("1\u0004"), indexOf7 + 1);
                if (indexOf >= 0) {
                    indexOf = skipToNext(str, indexOf + 2);
                }
            }
            if (indexOf >= 0) {
                ad.text = str.substring(indexOf, str.indexOf(d.a("F"), indexOf)).trim();
                ad.text = Html.fromHtml(ad.text).toString();
            }
            if (ad.hasImage() && ad.getImage() == null) {
                return null;
            }
            if (ad.iconURL != null) {
                ad.getIcon();
            }
            return ad;
        } catch (Exception e) {
            Log.e("AdMob SDK", j.a("K\u0004d\th\u0001-\u0011bE}\u0004\u0016hEl\u0001-\u0017h\u0016}\nc\u0016h_-E") + str, e);
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0053 A[SYNTHETIC, Splitter:B:22:0x0053] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static /* synthetic */ android.graphics.Bitmap fetchImage(java.lang.String r6, boolean r7) {
        /*
            r0 = 0
            if (r6 == 0) goto L_0x0027
            java.net.URL r1 = new java.net.URL     // Catch:{ Throwable -> 0x0028, all -> 0x004e }
            r1.<init>(r6)     // Catch:{ Throwable -> 0x0028, all -> 0x004e }
            java.net.URLConnection r1 = r1.openConnection()     // Catch:{ Throwable -> 0x0028, all -> 0x004e }
            r2 = 0
            r1.setConnectTimeout(r2)     // Catch:{ Throwable -> 0x0028, all -> 0x004e }
            r2 = 0
            r1.setReadTimeout(r2)     // Catch:{ Throwable -> 0x0028, all -> 0x004e }
            r1.setUseCaches(r7)     // Catch:{ Throwable -> 0x0028, all -> 0x004e }
            r1.connect()     // Catch:{ Throwable -> 0x0028, all -> 0x004e }
            java.io.InputStream r2 = r1.getInputStream()     // Catch:{ Throwable -> 0x0028, all -> 0x004e }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r2)     // Catch:{ Throwable -> 0x005d }
            if (r2 == 0) goto L_0x0027
            r2.close()     // Catch:{ IOException -> 0x0057 }
        L_0x0027:
            return r0
        L_0x0028:
            r1 = move-exception
            r2 = r0
        L_0x002a:
            java.lang.String r3 = "AdMob SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x005b }
            r4.<init>()     // Catch:{ all -> 0x005b }
            java.lang.String r5 = "`\b_\u0018\\\u001f]ZW\u001fD\u000eY\u0014WZY\u0017Q\u001dU@\u0010Z"
            java.lang.String r5 = defpackage.d.a(r5)     // Catch:{ all -> 0x005b }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x005b }
            java.lang.StringBuilder r4 = r4.append(r6)     // Catch:{ all -> 0x005b }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x005b }
            android.util.Log.w(r3, r4, r1)     // Catch:{ all -> 0x005b }
            if (r2 == 0) goto L_0x0027
            r2.close()     // Catch:{ IOException -> 0x004c }
            goto L_0x0027
        L_0x004c:
            r1 = move-exception
            goto L_0x0027
        L_0x004e:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0051:
            if (r2 == 0) goto L_0x0056
            r2.close()     // Catch:{ IOException -> 0x0059 }
        L_0x0056:
            throw r0
        L_0x0057:
            r1 = move-exception
            goto L_0x0027
        L_0x0059:
            r1 = move-exception
            goto L_0x0056
        L_0x005b:
            r0 = move-exception
            goto L_0x0051
        L_0x005d:
            r1 = move-exception
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.Ad.fetchImage(java.lang.String, boolean):android.graphics.Bitmap");
    }

    private static /* synthetic */ int skipToNext(String str, int i) {
        int length = str.length();
        if (i < 0 || i >= length) {
            return -1;
        }
        char charAt = str.charAt(i);
        int i2 = i;
        while (charAt != '>' && charAt != '<') {
            i2++;
            if (i2 >= length) {
                return -1;
            }
            charAt = str.charAt(i2);
        }
        if (charAt != '>') {
            return i2;
        }
        int i3 = i2 + 1;
        char charAt2 = str.charAt(i3);
        while (Character.isWhitespace(charAt2)) {
            i3++;
            if (i3 >= length) {
                return -1;
            }
            charAt2 = str.charAt(charAt2);
        }
        return i3;
    }

    public void clicked() {
        Log.i("AdMob SDK", j.a("$iEn\td\u0006f\u0000iK"));
        if (this.clickURL != null) {
            new Thread() {
                /* JADX WARNING: Removed duplicated region for block: B:15:0x0087  */
                /* JADX WARNING: Removed duplicated region for block: B:23:0x00d4  */
                /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    /*
                        r6 = this;
                        r1 = 0
                        r5 = 3
                        com.admob.android.ads.Ad r0 = com.admob.android.ads.Ad.this     // Catch:{ MalformedURLException -> 0x00de, IOException -> 0x0102 }
                        com.admob.android.ads.Ad$NetworkListener r0 = r0.networkListener     // Catch:{ MalformedURLException -> 0x00de, IOException -> 0x0102 }
                        if (r0 == 0) goto L_0x0013
                        com.admob.android.ads.Ad r0 = com.admob.android.ads.Ad.this     // Catch:{ MalformedURLException -> 0x00de, IOException -> 0x0102 }
                        com.admob.android.ads.Ad$NetworkListener r0 = r0.networkListener     // Catch:{ MalformedURLException -> 0x00de, IOException -> 0x0102 }
                        r0.onNetworkActivityStart()     // Catch:{ MalformedURLException -> 0x00de, IOException -> 0x0102 }
                    L_0x0013:
                        java.net.URL r2 = new java.net.URL     // Catch:{ MalformedURLException -> 0x00de, IOException -> 0x0102 }
                        com.admob.android.ads.Ad r0 = com.admob.android.ads.Ad.this     // Catch:{ MalformedURLException -> 0x00de, IOException -> 0x0102 }
                        java.lang.String r0 = r0.clickURL     // Catch:{ MalformedURLException -> 0x00de, IOException -> 0x0102 }
                        r2.<init>(r0)     // Catch:{ MalformedURLException -> 0x00de, IOException -> 0x0102 }
                        r0 = 1
                        java.net.HttpURLConnection.setFollowRedirects(r0)     // Catch:{ MalformedURLException -> 0x0148, IOException -> 0x0145 }
                        java.net.URLConnection r0 = r2.openConnection()     // Catch:{ MalformedURLException -> 0x0148, IOException -> 0x0145 }
                        java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x0148, IOException -> 0x0145 }
                        int r1 = com.admob.android.ads.Ad.CLICK_REQUEST_TIMEOUT     // Catch:{ MalformedURLException -> 0x0148, IOException -> 0x0145 }
                        r0.setConnectTimeout(r1)     // Catch:{ MalformedURLException -> 0x0148, IOException -> 0x0145 }
                        int r1 = com.admob.android.ads.Ad.CLICK_REQUEST_TIMEOUT     // Catch:{ MalformedURLException -> 0x0148, IOException -> 0x0145 }
                        r0.setReadTimeout(r1)     // Catch:{ MalformedURLException -> 0x0148, IOException -> 0x0145 }
                        java.lang.String r1 = "R\u001cb\u001d*.`\ni\u001b"
                        java.lang.String r1 = defpackage.i.a(r1)     // Catch:{ MalformedURLException -> 0x0148, IOException -> 0x0145 }
                        java.lang.String r3 = com.admob.android.ads.AdManager.getUserAgent()     // Catch:{ MalformedURLException -> 0x0148, IOException -> 0x0145 }
                        r0.setRequestProperty(r1, r3)     // Catch:{ MalformedURLException -> 0x0148, IOException -> 0x0145 }
                        java.lang.String r1 = "1*(C$H+* T<"
                        java.lang.String r1 = com.iPhand.FirstAid.DBUtil.a(r1)     // Catch:{ MalformedURLException -> 0x0148, IOException -> 0x0145 }
                        com.admob.android.ads.Ad r3 = com.admob.android.ads.Ad.this     // Catch:{ MalformedURLException -> 0x0148, IOException -> 0x0145 }
                        android.content.Context r3 = r3.context     // Catch:{ MalformedURLException -> 0x0148, IOException -> 0x0145 }
                        java.lang.String r3 = com.admob.android.ads.AdManager.getUserId(r3)     // Catch:{ MalformedURLException -> 0x0148, IOException -> 0x0145 }
                        r0.setRequestProperty(r1, r3)     // Catch:{ MalformedURLException -> 0x0148, IOException -> 0x0145 }
                        r0.connect()     // Catch:{ MalformedURLException -> 0x0148, IOException -> 0x0145 }
                        r0.getResponseCode()     // Catch:{ MalformedURLException -> 0x0148, IOException -> 0x0145 }
                        java.net.URL r1 = r0.getURL()     // Catch:{ MalformedURLException -> 0x0148, IOException -> 0x0145 }
                        java.lang.String r0 = "AdMob SDK"
                        r2 = 3
                        boolean r0 = android.util.Log.isLoggable(r0, r2)     // Catch:{ MalformedURLException -> 0x00de, IOException -> 0x0102 }
                        if (r0 == 0) goto L_0x0085
                        java.lang.String r0 = "AdMob SDK"
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x00de, IOException -> 0x0102 }
                        r2.<init>()     // Catch:{ MalformedURLException -> 0x00de, IOException -> 0x0102 }
                        java.lang.String r3 = "A\u0006i\u000ekOd\u0003n\flOc\nt\u001bn\u0001f\u001bn\u0000iOR=KU'O"
                        java.lang.String r3 = defpackage.i.a(r3)     // Catch:{ MalformedURLException -> 0x00de, IOException -> 0x0102 }
                        java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ MalformedURLException -> 0x00de, IOException -> 0x0102 }
                        java.lang.StringBuilder r2 = r2.append(r1)     // Catch:{ MalformedURLException -> 0x00de, IOException -> 0x0102 }
                        java.lang.String r2 = r2.toString()     // Catch:{ MalformedURLException -> 0x00de, IOException -> 0x0102 }
                        android.util.Log.d(r0, r2)     // Catch:{ MalformedURLException -> 0x00de, IOException -> 0x0102 }
                    L_0x0085:
                        if (r1 == 0) goto L_0x00cc
                        java.lang.String r0 = "AdMob SDK"
                        boolean r0 = android.util.Log.isLoggable(r0, r5)
                        if (r0 == 0) goto L_0x00ab
                        java.lang.String r0 = "AdMob SDK"
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder
                        r2.<init>()
                        java.lang.String r3 = "H\u0019b\u0007n\u0007`I"
                        java.lang.String r3 = com.iPhand.FirstAid.DBUtil.a(r3)
                        java.lang.StringBuilder r2 = r2.append(r3)
                        java.lang.StringBuilder r2 = r2.append(r1)
                        java.lang.String r2 = r2.toString()
                        android.util.Log.d(r0, r2)
                    L_0x00ab:
                        android.content.Intent r0 = new android.content.Intent
                        java.lang.String r2 = "f\u0001c\u001dh\u0006cAn\u0001s\ni\u001b)\u000ed\u001bn\u0000iAQ&B8"
                        java.lang.String r2 = defpackage.i.a(r2)
                        java.lang.String r3 = r1.toString()
                        android.net.Uri r3 = android.net.Uri.parse(r3)
                        r0.<init>(r2, r3)
                        r2 = 268435456(0x10000000, float:2.5243549E-29)
                        r0.addFlags(r2)
                        com.admob.android.ads.Ad r2 = com.admob.android.ads.Ad.this     // Catch:{ Exception -> 0x0127 }
                        android.content.Context r2 = r2.context     // Catch:{ Exception -> 0x0127 }
                        r2.startActivity(r0)     // Catch:{ Exception -> 0x0127 }
                    L_0x00cc:
                        com.admob.android.ads.Ad r0 = com.admob.android.ads.Ad.this
                        com.admob.android.ads.Ad$NetworkListener r0 = r0.networkListener
                        if (r0 == 0) goto L_0x00dd
                        com.admob.android.ads.Ad r0 = com.admob.android.ads.Ad.this
                        com.admob.android.ads.Ad$NetworkListener r0 = r0.networkListener
                        r0.onNetworkActivityEnd()
                    L_0x00dd:
                        return
                    L_0x00de:
                        r0 = move-exception
                    L_0x00df:
                        java.lang.String r2 = "AdMob SDK"
                        java.lang.StringBuilder r3 = new java.lang.StringBuilder
                        r3.<init>()
                        java.lang.String r4 = "J\bk\u000fh\u001bj\fcId\u0005n\nlIR;KG'IP\u0000k\u0005'\u001du\u0010'\u001dhIa\u0006k\u0005h\u001e'\bi\u0010p\b~G'I"
                        java.lang.String r4 = com.iPhand.FirstAid.DBUtil.a(r4)
                        java.lang.StringBuilder r3 = r3.append(r4)
                        com.admob.android.ads.Ad r4 = com.admob.android.ads.Ad.this
                        java.lang.String r4 = r4.clickURL
                        java.lang.StringBuilder r3 = r3.append(r4)
                        java.lang.String r3 = r3.toString()
                        android.util.Log.w(r2, r3, r0)
                        goto L_0x0085
                    L_0x0102:
                        r0 = move-exception
                    L_0x0103:
                        java.lang.String r2 = "AdMob SDK"
                        java.lang.StringBuilder r3 = new java.lang.StringBuilder
                        r3.<init>()
                        java.lang.String r4 = "D\u0000r\u0003cOi\u0000sOc\ns\nu\u0002n\u0001bOa\u0006i\u000ekOd\u0003n\flOc\nt\u001bn\u0001f\u001bn\u0000iOR=KA'OP\u0006k\u0003'\u001bu\u0016'\u001bhOa\u0000k\u0003h\u0018'\u000ei\u0016p\u000e~A'O"
                        java.lang.String r4 = defpackage.i.a(r4)
                        java.lang.StringBuilder r3 = r3.append(r4)
                        com.admob.android.ads.Ad r4 = com.admob.android.ads.Ad.this
                        java.lang.String r4 = r4.clickURL
                        java.lang.StringBuilder r3 = r3.append(r4)
                        java.lang.String r3 = r3.toString()
                        android.util.Log.w(r2, r3, r0)
                        goto L_0x0085
                    L_0x0127:
                        r0 = move-exception
                        java.lang.String r2 = "AdMob SDK"
                        java.lang.StringBuilder r3 = new java.lang.StringBuilder
                        r3.<init>()
                        java.lang.String r4 = "D\u0006r\u0005cIi\u0006sIh\u0019b\u0007'\u000bu\u0006p\u001ab\u001b'\u0006iIf\r'\nk\u0000d\u0002'\u001dhI"
                        java.lang.String r4 = com.iPhand.FirstAid.DBUtil.a(r4)
                        java.lang.StringBuilder r3 = r3.append(r4)
                        java.lang.StringBuilder r1 = r3.append(r1)
                        java.lang.String r1 = r1.toString()
                        android.util.Log.e(r2, r1, r0)
                        goto L_0x00cc
                    L_0x0145:
                        r0 = move-exception
                        r1 = r2
                        goto L_0x0103
                    L_0x0148:
                        r0 = move-exception
                        r1 = r2
                        goto L_0x00df
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.Ad.AnonymousClass1.run():void");
                }
            }.start();
        }
    }

    public boolean equals(Object obj) {
        if (obj instanceof Ad) {
            return toString().equals(((Ad) obj).toString());
        }
        return false;
    }

    public String getClickURL() {
        return this.clickURL;
    }

    public String getHTML() {
        return this.html;
    }

    public Bitmap getIcon() {
        if (this.icon == null) {
            this.icon = fetchImage(this.iconURL, true);
            if (this.icon == null) {
                Log.w("AdMob SDK", d.a("9_\u000f\\\u001e\u0010\u0014_\u000e\u0010\u001dU\u000e\u0010\u0013S\u0015^ZV\u0015BZQ\u001e\u0010\u001cB\u0015]Z") + this.iconURL);
            }
        }
        return this.icon;
    }

    public Bitmap getImage() {
        if (this.image == null && this.imageURL != null) {
            this.image = fetchImage(this.imageURL, false);
        }
        return this.image;
    }

    public int getImageHeight() {
        return this.image != null ? this.image.getHeight() : this.imageHeight;
    }

    public String getImageURL() {
        return this.imageURL;
    }

    public int getImageWidth() {
        return this.image != null ? this.image.getWidth() : this.imageWidth;
    }

    public NetworkListener getNetworkListener() {
        return this.networkListener;
    }

    public String getText() {
        return this.text;
    }

    public boolean hasImage() {
        return this.imageURL != null;
    }

    public int hashCode() {
        return toString().hashCode();
    }

    public void setNetworkListener(NetworkListener networkListener2) {
        this.networkListener = networkListener2;
    }

    public String toString() {
        String text2 = getText();
        return text2 == null ? "" : text2;
    }
}
