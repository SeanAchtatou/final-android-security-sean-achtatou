package com.admob.android.ads;

import android.content.Context;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

class AdRequester {
    private static final String ADMOB_ENDPOINT = "http://r.admob.com/ad_source.php";
    private static int REQUEST_TIMEOUT = 3000;

    AdRequester() {
    }

    private static /* synthetic */ void appendParams(StringBuilder sb, String str, String str2) {
        if (str2 != null && str2.length() > 0) {
            try {
                sb.append(d.a("\\")).append(URLEncoder.encode(str, d.a("/d<\u001dB"))).append(d.a("G")).append(URLEncoder.encode(str2, d.a("/d<\u001dB")));
            } catch (UnsupportedEncodingException e) {
                Log.e("AdMob SDK", d.a("e.vW\bZU\u0014S\u0015T\u0013^\u001d\u0010\u0013CZ^\u0015DZC\u000f@\n_\bD\u001fTZ_\u0014\u0010\u000eX\u0013CZT\u001fF\u0013S\u001f\u001eZ\u0010;TZB\u001fA\u000fU\tD\t\u0010\u001bB\u001f\u0010\u0013]\n_\tC\u0013R\u0016UT"), e);
            }
        }
    }

    static String buildParamString(Context context, String str, String str2) {
        StringBuilder sb = new StringBuilder();
        long currentTimeMillis = System.currentTimeMillis();
        sb.append(d.a("\u0000")).append(d.a("G")).append(currentTimeMillis / 1000).append(d.a("T")).append(currentTimeMillis % 1000);
        appendParams(sb, d.a("B\u000e"), d.a("J"));
        String publisherId = AdManager.getPublisherId(context);
        if (publisherId == null) {
            throw new IllegalStateException(d.a("`\u000fR\u0016Y\tX\u001fBZy>\u0010\u0013CZ^\u0015DZC\u001fD[\u0010Zd\u0015\u0010\tU\bF\u001f\u0010\u001bT\t\u0010\u0003_\u000f\u0010\u0017E\tDZC\u001fDZI\u0015E\b\u0010\nE\u0018\\\u0013C\u0012U\b\u00103tZQ\tC\u0013W\u0014U\u001e\u0010\u001cB\u0015]ZG\rGTQ\u001e]\u0015RTS\u0015]T\u0010Zu\u0013D\u0012U\b\u0010\u001bT\u001e\u0010\u0013DZD\u0015\u0010;^\u001eB\u0015Y\u001e}\u001b^\u0013V\u001fC\u000e\u001e\u0002]\u0016\u0010\u000f^\u001eU\b\u0010\u000eX\u001f\u0010FQ\n@\u0016Y\u0019Q\u000eY\u0015^D\u0010\u000eQ\u001d\u0010\u0015BZS\u001b\\\u0016\u0010;T7Q\u0014Q\u001dU\b\u001e\tU\u000e`\u000fR\u0016Y\tX\u001fB3TR\u0019T"));
        }
        appendParams(sb, d.a("\t"), publisherId);
        appendParams(sb, "f", d.a("X\u000e]\u0016o\u0014_%Z\t"));
        appendParams(sb, d.a("\u0003"), d.a("D\u001fH\u000e"));
        appendParams(sb, d.a("S\u0016Y\u001f^\u000eo\tT\u0011"), d.a("K"));
        appendParams(sb, d.a("U\u0002"), d.a("K"));
        appendParams(sb, d.a("\f"), AdManager.SDK_VERSION);
        appendParams(sb, d.a("\u000e"), AdManager.getUserId(context));
        appendParams(sb, d.a("T!S\u0015_\bT'"), AdManager.getCoordinatesAsString(context));
        appendParams(sb, d.a("T!T\u0015R'"), AdManager.getBirthdayAsString());
        appendParams(sb, "k", str);
        appendParams(sb, d.a("C\u001fQ\bS\u0012"), str2);
        if (AdManager.isInTestMode()) {
            appendParams(sb, d.a("\u0017"), d.a("D\u001fC\u000e"));
        }
        return sb.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x00cf A[SYNTHETIC, Splitter:B:20:0x00cf] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00d4 A[Catch:{ Exception -> 0x017e }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00eb  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00f8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.admob.android.ads.Ad requestAd(android.content.Context r10, java.lang.String r11, java.lang.String r12) {
        /*
            r1 = 0
            java.lang.String r0 = "\u001b^\u001eB\u0015Y\u001e\u001e\nU\b]\u0013C\tY\u0015^Ty4d?b4u."
            java.lang.String r0 = defpackage.d.a(r0)
            int r0 = r10.checkCallingOrSelfPermission(r0)
            r2 = -1
            if (r0 != r2) goto L_0x0017
            java.lang.String r0 = "s\u001b^\u0014_\u000e\u0010\bU\u000bE\u001fC\u000e\u0010\u001b^ZQ\u001e\u0010\rY\u000eX\u0015E\u000e\u00103^\u000eU\b^\u001fDZ@\u001fB\u0017Y\tC\u0013_\u0014C[\u0010Z\nU\u0014\u0010\u0017Q\u0014Y\u001cU\tDTH\u0017\\ZQ\u0014TZZ\u000fC\u000e\u0010\u0018U\u001c_\bUZD\u0012UZV\u0013^\u001b\\Z\fU]\u001b^\u0013V\u001fC\u000e\u000eZD\u001bWZQ\u001eT@\u0010Z\f\u000fC\u001fCW@\u001fB\u0017Y\tC\u0013_\u0014\u0010\u001b^\u001eB\u0015Y\u001e\n\u0014Q\u0017UG\u0012\u001b^\u001eB\u0015Y\u001e\u001e\nU\b]\u0013C\tY\u0015^Ty4d?b4u.\u0012Z\u001fD"
            java.lang.String r0 = defpackage.d.a(r0)
            com.admob.android.ads.AdManager.clientError(r0)
        L_0x0017:
            com.admob.android.ads.AdMobLocalizer.init(r10)
            long r4 = java.lang.System.currentTimeMillis()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r2 = buildParamString(r10, r11, r12)     // Catch:{ Exception -> 0x017b }
            java.net.URL r0 = new java.net.URL     // Catch:{ all -> 0x0170 }
            java.lang.String r3 = "http://r.admob.com/ad_source.php"
            r0.<init>(r3)     // Catch:{ all -> 0x0170 }
            java.net.URLConnection r0 = r0.openConnection()     // Catch:{ all -> 0x0170 }
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ all -> 0x0170 }
            java.lang.String r3 = "`5c."
            java.lang.String r3 = defpackage.d.a(r3)     // Catch:{ all -> 0x0170 }
            r0.setRequestMethod(r3)     // Catch:{ all -> 0x0170 }
            r3 = 1
            r0.setDoOutput(r3)     // Catch:{ all -> 0x0170 }
            java.lang.String r3 = "e\tU\b\u001d;W\u001f^\u000e"
            java.lang.String r3 = defpackage.d.a(r3)     // Catch:{ all -> 0x0170 }
            java.lang.String r7 = com.admob.android.ads.AdManager.getUserAgent()     // Catch:{ all -> 0x0170 }
            r0.setRequestProperty(r3, r7)     // Catch:{ all -> 0x0170 }
            java.lang.String r3 = "s\u0015^\u000eU\u0014DWd\u0003@\u001f"
            java.lang.String r3 = defpackage.d.a(r3)     // Catch:{ all -> 0x0170 }
            java.lang.String r7 = "\u001b@\n\\\u0013S\u001bD\u0013_\u0014\u001f\u0002\u001d\rG\r\u001d\u001c_\b]WE\b\\\u001f^\u0019_\u001eU\u001e"
            java.lang.String r7 = defpackage.d.a(r7)     // Catch:{ all -> 0x0170 }
            r0.setRequestProperty(r3, r7)     // Catch:{ all -> 0x0170 }
            java.lang.String r3 = "s\u0015^\u000eU\u0014DW|\u001f^\u001dD\u0012"
            java.lang.String r3 = defpackage.d.a(r3)     // Catch:{ all -> 0x0170 }
            int r7 = r2.length()     // Catch:{ all -> 0x0170 }
            java.lang.String r7 = java.lang.Integer.toString(r7)     // Catch:{ all -> 0x0170 }
            r0.setRequestProperty(r3, r7)     // Catch:{ all -> 0x0170 }
            int r3 = com.admob.android.ads.AdRequester.REQUEST_TIMEOUT     // Catch:{ all -> 0x0170 }
            r0.setConnectTimeout(r3)     // Catch:{ all -> 0x0170 }
            int r3 = com.admob.android.ads.AdRequester.REQUEST_TIMEOUT     // Catch:{ all -> 0x0170 }
            r0.setReadTimeout(r3)     // Catch:{ all -> 0x0170 }
            java.lang.String r3 = "AdMob SDK"
            r7 = 3
            boolean r3 = android.util.Log.isLoggable(r3, r7)     // Catch:{ all -> 0x0170 }
            if (r3 == 0) goto L_0x009d
            java.lang.String r3 = "AdMob SDK"
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0170 }
            r7.<init>()     // Catch:{ all -> 0x0170 }
            java.lang.String r8 = "(U\u000bE\u001fC\u000eY\u0014WZQ\u0014\u0010\u001bTZG\u0013D\u0012\u0010*)dZ@\u001bB\u0017Q\u0017C@\u0010Z"
            java.lang.String r8 = defpackage.d.a(r8)     // Catch:{ all -> 0x0170 }
            java.lang.StringBuilder r7 = r7.append(r8)     // Catch:{ all -> 0x0170 }
            java.lang.StringBuilder r7 = r7.append(r2)     // Catch:{ all -> 0x0170 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x0170 }
            android.util.Log.d(r3, r7)     // Catch:{ all -> 0x0170 }
        L_0x009d:
            java.io.BufferedWriter r3 = new java.io.BufferedWriter     // Catch:{ all -> 0x0170 }
            java.io.OutputStreamWriter r7 = new java.io.OutputStreamWriter     // Catch:{ all -> 0x0170 }
            java.io.OutputStream r8 = r0.getOutputStream()     // Catch:{ all -> 0x0170 }
            r7.<init>(r8)     // Catch:{ all -> 0x0170 }
            r3.<init>(r7)     // Catch:{ all -> 0x0170 }
            r3.write(r2)     // Catch:{ all -> 0x0176 }
            r3.close()     // Catch:{ all -> 0x0176 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ all -> 0x0176 }
            java.io.InputStreamReader r7 = new java.io.InputStreamReader     // Catch:{ all -> 0x0176 }
            java.io.InputStream r8 = r0.getInputStream()     // Catch:{ all -> 0x0176 }
            r7.<init>(r8)     // Catch:{ all -> 0x0176 }
            r2.<init>(r7)     // Catch:{ all -> 0x0176 }
        L_0x00bf:
            java.lang.String r7 = r2.readLine()     // Catch:{ all -> 0x00c9 }
            if (r7 == 0) goto L_0x0126
            r6.append(r7)     // Catch:{ all -> 0x00c9 }
            goto L_0x00bf
        L_0x00c9:
            r0 = move-exception
            r9 = r2
            r2 = r0
            r0 = r9
        L_0x00cd:
            if (r3 == 0) goto L_0x00d2
            r3.close()     // Catch:{ Exception -> 0x017e }
        L_0x00d2:
            if (r0 == 0) goto L_0x00d7
            r0.close()     // Catch:{ Exception -> 0x017e }
        L_0x00d7:
            throw r2     // Catch:{ Exception -> 0x00d8 }
        L_0x00d8:
            r0 = move-exception
        L_0x00d9:
            java.lang.String r2 = "AdMob SDK"
            java.lang.String r3 = "s\u0015E\u0016TZ^\u0015DZW\u001fDZQ\u001e\u0010\u001cB\u0015]Zq\u001e}\u0015RZC\u001fB\fU\bCT"
            java.lang.String r3 = defpackage.d.a(r3)
            android.util.Log.w(r2, r3, r0)
            r0 = r1
        L_0x00e5:
            java.lang.String r2 = r6.toString()
            if (r2 == 0) goto L_0x00ef
            com.admob.android.ads.Ad r1 = com.admob.android.ads.Ad.createAd(r10, r2, r0)
        L_0x00ef:
            java.lang.String r0 = "AdMob SDK"
            r2 = 4
            boolean r0 = android.util.Log.isLoggable(r0, r2)
            if (r0 == 0) goto L_0x0125
            long r2 = java.lang.System.currentTimeMillis()
            long r2 = r2 - r4
            if (r1 != 0) goto L_0x0145
            java.lang.String r0 = "AdMob SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "c\u001fB\fU\b\u0010\bU\n\\\u0013U\u001e\u0010\u000eX\u001bDZ^\u0015\u0010\u001bT\t\u0010\u001bB\u001f\u0010\u001bF\u001bY\u0016Q\u0018\\\u001f\u0010R"
            java.lang.String r5 = defpackage.d.a(r5)
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r3 = "\u0017CS"
            java.lang.String r3 = defpackage.d.a(r3)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            android.util.Log.i(r0, r2)
        L_0x0125:
            return r1
        L_0x0126:
            java.lang.String r7 = "\"\u001d;T7_\u0018\u001d;^\u001eB\u0015Y\u001e\u001d9Q\u000eU\u001d_\bIWy\u0019_\u0014"
            java.lang.String r7 = defpackage.d.a(r7)     // Catch:{ all -> 0x00c9 }
            java.lang.String r0 = r0.getHeaderField(r7)     // Catch:{ all -> 0x00c9 }
            if (r0 != 0) goto L_0x0138
            java.lang.String r0 = "X\u000eD\n\nU\u001f\u0017]TQ\u001e]\u0015RTS\u0015]UC\u000eQ\u000eY\u0019\u001f\u001b^\u001eB\u0015Y\u001e\u001f\u000eY\u0016U\t\u001f\u001eU\u001cQ\u000f\\\u000e\u001e\n^\u001d"
            java.lang.String r0 = defpackage.d.a(r0)
        L_0x0138:
            if (r3 == 0) goto L_0x013d
            r3.close()     // Catch:{ Exception -> 0x0143 }
        L_0x013d:
            if (r2 == 0) goto L_0x00e5
            r2.close()     // Catch:{ Exception -> 0x0143 }
            goto L_0x00e5
        L_0x0143:
            r2 = move-exception
            goto L_0x00e5
        L_0x0145:
            java.lang.String r0 = "AdMob SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = ";TZB\u001fD\u000fB\u0014U\u001e\u0010\u0013^Z"
            java.lang.String r5 = defpackage.d.a(r5)
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r2 = r4.append(r2)
            java.lang.String r3 = "\u0017C@\u0010Z"
            java.lang.String r3 = defpackage.d.a(r3)
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.String r2 = r2.toString()
            android.util.Log.i(r0, r2)
            goto L_0x0125
        L_0x0170:
            r0 = move-exception
            r2 = r0
            r3 = r1
            r0 = r1
            goto L_0x00cd
        L_0x0176:
            r0 = move-exception
            r2 = r0
            r0 = r1
            goto L_0x00cd
        L_0x017b:
            r0 = move-exception
            goto L_0x00d9
        L_0x017e:
            r0 = move-exception
            goto L_0x00d7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.AdRequester.requestAd(android.content.Context, java.lang.String, java.lang.String):com.admob.android.ads.Ad");
    }
}
