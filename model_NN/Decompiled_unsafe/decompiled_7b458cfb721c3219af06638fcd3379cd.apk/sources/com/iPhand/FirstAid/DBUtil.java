package com.iPhand.FirstAid;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

public class DBUtil {
    private static final String DATABASE_FILENAME = "firstaid.sqlite";
    private static final String DATABASE_PATH = "/data/data/com.iPhand.FirstAid/databases";

    public static String a(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 'i');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ 7);
            i2 = i;
        }
        return new String(cArr);
    }

    public static SQLiteDatabase openDatabase(Context context) {
        try {
            File file = new File(DATABASE_PATH);
            File file2 = new File(DATABASE_PATH);
            if (!file2.exists()) {
                file2.mkdir();
            }
            if (!file.exists()) {
                file.mkdir();
            }
            if (!new File(j.a("\"\u0001l\u0011lJi\u0004y\u0004\"\u0006b\b#\f]\rl\u000biKK\f\u0016y$d\u0001\"\u0001l\u0011l\u0007l\u0016h\u0016\"\u0003d\u0017~\u0011l\fiK~\u0014a\fy\u0000")).exists()) {
                InputStream openRawResource = context.getResources().openRawResource(R.raw.firstaid);
                FileOutputStream fileOutputStream = new FileOutputStream(a("(\rf\u001dfFc\bs\b(\nh\u0004)\u0000W\u0001f\u0007cGA\u0000u\u001as(n\r(\rf\u001df\u000bf\u001ab\u001a(\u000fn\u001bt\u001df\u0000cGt\u0018k\u0000s\f"));
                byte[] bArr = new byte[8192];
                while (true) {
                    int read = openRawResource.read(bArr);
                    if (read <= 0) {
                        break;
                    }
                    fileOutputStream.write(bArr, 0, read);
                }
                fileOutputStream.close();
                openRawResource.close();
            }
            return SQLiteDatabase.openOrCreateDatabase(j.a("\"\u0001l\u0011lJi\u0004y\u0004\"\u0006b\b#\f]\rl\u000biKK\f\u0016y$d\u0001\"\u0001l\u0011l\u0007l\u0016h\u0016\"\u0003d\u0017~\u0011l\fiK~\u0014a\fy\u0000"), (SQLiteDatabase.CursorFactory) null);
        } catch (Exception e) {
            return null;
        }
    }
}
