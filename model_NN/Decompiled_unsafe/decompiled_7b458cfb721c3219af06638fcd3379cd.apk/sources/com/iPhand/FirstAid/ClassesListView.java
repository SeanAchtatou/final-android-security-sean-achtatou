package com.iPhand.FirstAid;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import java.util.ArrayList;
import java.util.HashMap;

public class ClassesListView extends Activity {
    int[] Icons = {R.drawable.icon_01, R.drawable.icon_02, R.drawable.icon_03, R.drawable.icon_04, R.drawable.icon_05, R.drawable.icon_06, R.drawable.icon_07, R.drawable.icon_08, R.drawable.icon_09, R.drawable.icon_10};
    int Num = 0;
    String Title = null;
    private SQLiteDatabase database;
    ListView mListView;
    String[] mQ = {j.a("n\n`\bb\u000b"), d.a("^\u001fS\u001fC\tQ\bI\u0011^\u0015G\u0012_\r"), j.a("\nx\u0011i\nb\u0017"), d.a("Y\u0014D\u001fB\u0014Q\u0016"), j.a("h\u001dy\u0000\u000bl\t"), d.a("V\u001bS\u001f"), j.a("j\u001cc\u0004h\u0006b\tb\u0002d\u0006l\t"), d.a("\nQ\u001fT\u0013Q\u000eB\u0013S\t"), j.a("~\u000ed\u000b"), d.a("\nC\u0003S\u0012_\u0016_\u001dY\u0019Q\u0016")};

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.dialogueclass_list);
        this.database = DBUtil.openDatabase(this);
        this.mListView = (ListView) findViewById(R.id.DialogueClassList);
        this.Num = getIntent().getExtras().getInt(j.a("+x\b"));
        this.Title = getIntent().getExtras().getString(d.a(".Y\u000e\\\u001f"));
        setTitle(this.Title);
        setListAdapter();
        this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                Intent intent = new Intent(ClassesListView.this, DetailView.class);
                Bundle bundle = new Bundle();
                bundle.putInt(d.a("4E\u0017"), ClassesListView.this.getIntent().getExtras().getInt(j.a("+x\b")));
                bundle.putInt(d.a("3D\u001f]4E\u0017"), i);
                intent.putExtras(bundle);
                ClassesListView.this.startActivity(intent);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.database.close();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        this.database = DBUtil.openDatabase(this);
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        this.database.close();
        super.onStop();
    }

    public void setListAdapter() {
        Cursor rawQuery = this.database.rawQuery(j.a("~\u0000a\u0000n\u0011-\u0011b\u0015d\u0006-\u0003\n`Ek\f\u0016y\u0004d\u0001-\u0012e\u0000\u0000-\u0006l\u0011h\u0002b\u0017tE0Z"), new String[]{this.mQ[this.Num]});
        if (rawQuery.getCount() > 0) {
            rawQuery.moveToFirst();
        }
        try {
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < rawQuery.getCount(); i++) {
                HashMap hashMap = new HashMap();
                hashMap.put(d.a("3]\u001bW\u001f"), Integer.valueOf(this.Icons[this.Num]));
                hashMap.put(j.a(",y\u0000`1d\u0011a\u0000"), rawQuery.getString(rawQuery.getColumnIndex(d.a("\u000e_\nY\u0019"))));
                arrayList.add(hashMap);
                rawQuery.moveToNext();
            }
            this.mListView.setAdapter((ListAdapter) new SimpleAdapter(this, arrayList, R.layout.dialogue_details_item, new String[]{d.a("3]\u001bW\u001f"), j.a(",y\u0000`1d\u0011a\u0000")}, new int[]{R.id.DialogueclassImage, R.id.DialogueclassTitle}));
            rawQuery.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
