package com.iPhand.FirstAid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import java.util.ArrayList;
import java.util.HashMap;

public class FirstAid extends Activity {
    private static final int MENU_ABOUT = 0;
    String[] Catalogs = {l.m1a("幓觹恎敩"), j.a("怨攴顶瞀"), l.m1a("扜央慤央恎敩"), j.a("冈禴"), l.m1a("好秩"), j.a("仱宕禴"), l.m1a("妿仌秩"), j.a("儲禴"), l.m1a("皖胏秩"), j.a("徦琋禴")};
    int[] Icons = {R.drawable.icon_01, R.drawable.icon_02, R.drawable.icon_03, R.drawable.icon_04, R.drawable.icon_05, R.drawable.icon_06, R.drawable.icon_07, R.drawable.icon_08, R.drawable.icon_09, R.drawable.icon_10};
    private ImageView ImgFlash = null;
    ListView mListView;
    private AlphaAnimation myAnimation_Alpha = null;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.flashscreen);
        this.ImgFlash = (ImageView) findViewById(R.id.ivStart);
        this.myAnimation_Alpha = new AlphaAnimation(1.0f, 0.9f);
        this.myAnimation_Alpha.setDuration(2000);
        this.myAnimation_Alpha.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                FirstAid.this.setContentView((int) R.layout.dialogueclass_list);
                FirstAid.this.mListView = (ListView) FirstAid.this.findViewById(R.id.DialogueClassList);
                FirstAid.this.setListAdapter(FirstAid.this.Catalogs);
                FirstAid.this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                        Intent intent = new Intent(FirstAid.this, ClassesListView.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt(i.a("!r\u0002"), i);
                        bundle.putString(DBUtil.a("=n\u001dk\f"), FirstAid.this.Catalogs[i]);
                        intent.putExtras(bundle);
                        FirstAid.this.startActivity(intent);
                    }
                });
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 0, 1, l.m1a("儘亶")).setIcon((int) R.drawable.about);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        switch (menuItem.getItemId()) {
            case R.styleable.com_admob_android_ads_AdView_testing /*0*/:
                startActivity(new Intent(this, about.class));
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.ImgFlash.startAnimation(this.myAnimation_Alpha);
    }

    public void setListAdapter(String[] strArr) {
        try {
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < strArr.length; i++) {
                HashMap hashMap = new HashMap();
                hashMap.put(j.a(",`\u0004j\u0000"), Integer.valueOf(this.Icons[i]));
                hashMap.put(l.m1a("q\u001f]\u0006l\u0002L\u0007]"), strArr[i]);
                arrayList.add(hashMap);
            }
            this.mListView.setAdapter((ListAdapter) new SimpleAdapter(this, arrayList, R.layout.dialogueclass_list_itme, new String[]{j.a(",`\u0004j\u0000"), l.m1a("q\u001f]\u0006l\u0002L\u0007]")}, new int[]{R.id.DialogueclassImage, R.id.DialogueclassTitle}));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
