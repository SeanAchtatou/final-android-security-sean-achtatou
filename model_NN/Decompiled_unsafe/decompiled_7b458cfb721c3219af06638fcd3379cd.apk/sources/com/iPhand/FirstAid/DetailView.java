package com.iPhand.FirstAid;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DetailView extends Activity implements View.OnClickListener {
    private static final int MENU_SendWithEmail = 1;
    String[] C = {l.m1a("\bW\u0006U\u0004V"), R.a("`UmU}CoBw[`_yXaG"), l.m1a("W\u001eL\u000fW\u0004J"), R.a("g^zU|^o\\"), l.m1a("\u000e@\u001f]\u0019V\nT"), R.a("hQmU"), l.m1a("\fA\u0005Y\u000e[\u0004T\u0004_\u0002[\nT"), R.a("@oUjYoD|YmC"), l.m1a("\u0018S\u0002V"), R.a("@}ImXa\\aWgSo\\")};
    Cursor Detail;
    int ItemNum = 0;
    int Num = 0;
    Cursor Summary;
    Cursor Topic;
    private SQLiteDatabase database;
    /* access modifiers changed from: private */
    public boolean isPre = false;
    /* access modifiers changed from: private */
    public ViewGroup mContainer;
    /* access modifiers changed from: private */
    public ImageView mImageView;
    /* access modifiers changed from: private */
    public TextView mTextView;
    ImageButton nextButton;
    ImageButton preButton;

    private final class DisplayNextView implements Animation.AnimationListener {
        private /* synthetic */ DisplayNextView() {
        }

        /* synthetic */ DisplayNextView(DetailView detailView, DisplayNextView displayNextView) {
            this();
        }

        public void onAnimationEnd(Animation animation) {
            DetailView.this.mContainer.post(new SwapViews());
        }

        public void onAnimationRepeat(Animation animation) {
        }

        public void onAnimationStart(Animation animation) {
        }
    }

    private final class SwapViews implements Runnable {
        public SwapViews() {
        }

        public void run() {
            float width = ((float) DetailView.this.mContainer.getWidth()) / 2.0f;
            float height = ((float) DetailView.this.mContainer.getHeight()) / 2.0f;
            if (!DetailView.this.isPre) {
                DetailView.this.ItemNum++;
            } else {
                DetailView detailView = DetailView.this;
                detailView.ItemNum--;
            }
            DetailView.this.mImageView.setBackgroundResource(R.drawable.q);
            DetailView.this.Topic.moveToPosition(DetailView.this.ItemNum);
            DetailView.this.setTitle(DetailView.this.Topic.getString(DetailView.this.Topic.getColumnIndex(j.a("\u0011b\u0015d\u0006"))));
            DetailView.this.Detail.moveToPosition(DetailView.this.ItemNum);
            DetailView.this.Summary.moveToPosition(DetailView.this.ItemNum);
            DetailView.this.mTextView.setText(String.valueOf(DetailView.this.Summary.getString(DetailView.this.Summary.getColumnIndex(i.a("\u001cr\u0002j\u000eu\u0016")))) + j.a("\u0007o") + DetailView.this.Detail.getString(DetailView.this.Detail.getColumnIndex(i.a("c\ns\u000en\u0003"))));
            Rotate3dAnimation rotate3dAnimation = new Rotate3dAnimation(90.0f, 0.0f, width, height, 310.0f, false);
            rotate3dAnimation.setDuration(500);
            rotate3dAnimation.setFillAfter(true);
            rotate3dAnimation.setInterpolator(new DecelerateInterpolator());
            DetailView.this.mContainer.startAnimation(rotate3dAnimation);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void applyRotation(int i, float f, float f2) {
        Rotate3dAnimation rotate3dAnimation = new Rotate3dAnimation(f, f2, ((float) this.mContainer.getWidth()) / 2.0f, ((float) this.mContainer.getHeight()) / 2.0f, 310.0f, true);
        rotate3dAnimation.setDuration(500);
        rotate3dAnimation.setFillAfter(true);
        rotate3dAnimation.setInterpolator(new AccelerateInterpolator());
        rotate3dAnimation.setAnimationListener(new DisplayNextView(this, null));
        this.mContainer.startAnimation(rotate3dAnimation);
    }

    private /* synthetic */ void share() {
        Intent intent = new Intent(l.m1a("\nV\u000fJ\u0004Q\u000f\u0016\u0002V\u001f]\u0005LEY\bL\u0002W\u0005\u0016=q.o"), Uri.parse(R.a("]oYbDa\n")));
        this.Topic.moveToPosition(this.ItemNum);
        this.Detail.moveToPosition(this.ItemNum);
        this.Summary.moveToPosition(this.ItemNum);
        intent.putExtra(l.m1a("\nV\u000fJ\u0004Q\u000f\u0016\u0002V\u001f]\u0005LE]\u0013L\u0019YEk>z!}(l"), R.a("怫敡扅冼‚․") + this.Topic.getString(this.Topic.getColumnIndex(l.m1a("L\u0004H\u0002["))));
        intent.putExtra(R.a("Q`T|_gT Y`Dk^z\u001ekHzBo\u001eZuVd"), String.valueOf(this.Summary.getString(this.Summary.getColumnIndex(l.m1a("K\u001eU\u0006Y\u0019A")))) + R.a("\u0004:") + this.Detail.getString(this.Detail.getColumnIndex(l.m1a("\u000f]\u001fY\u0002T"))));
        startActivity(intent);
    }

    public void onClick(View view) {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.detail_view);
        this.database = DBUtil.openDatabase(this);
        this.mImageView = (ImageView) findViewById(R.id.picture);
        this.mContainer = (ViewGroup) findViewById(R.id.DetailContainer);
        this.mTextView = (TextView) findViewById(R.id.myTextView);
        this.preButton = (ImageButton) findViewById(R.id.preButton);
        this.nextButton = (ImageButton) findViewById(R.id.nextButton);
        this.Num = getIntent().getExtras().getInt(R.a("~{]"));
        this.Topic = this.database.rawQuery(l.m1a("\u0018]\u0007]\bLKL\u0004H\u0002[K^\u0019W\u0006\u0018\rQ\u0019K\u001fY\u0002\\KO\u0003]\u0019]K[\nL\u000e_\u0004J\u0012\u0018V\u0007"), new String[]{this.C[this.Num]});
        this.Summary = this.database.rawQuery(R.a("}UbUmD.C{]cQ|I.V|_c\u0010hY|CzQgT.GfU|U.SoDkWaBw\u00103\u000f"), new String[]{this.C[this.Num]});
        this.Detail = this.database.rawQuery(l.m1a("K\u000eT\u000e[\u001f\u0018\u000f]\u001fY\u0002TK^\u0019W\u0006\u0018\rQ\u0019K\u001fY\u0002\\KO\u0003]\u0019]K[\nL\u000e_\u0004J\u0012\u0018V\u0007"), new String[]{this.C[this.Num]});
        if (this.Topic.getCount() > 0) {
            this.Topic.moveToFirst();
        }
        if (this.Summary.getCount() > 0) {
            this.Summary.moveToFirst();
        }
        if (this.Detail.getCount() > 0) {
            this.Detail.moveToFirst();
        }
        this.ItemNum = getIntent().getExtras().getInt(R.a("yzUc~{]"));
        this.nextButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (DetailView.this.ItemNum == DetailView.this.Summary.getCount() - 1) {
                    Toast.makeText(DetailView.this, d.a("巂纵星杖篷杺吾乺村仼Ｑ佫恟乺主$o$"), 1).show();
                    return;
                }
                DetailView.this.isPre = false;
                DetailView.this.applyRotation(-1, 0.0f, 180.0f);
            }
        });
        this.preButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (DetailView.this.ItemNum == 0) {
                    Toast.makeText(DetailView.this, l.m1a("巊纤昗杇篿筇丸朊亾ｪ↪4↪"), 1).show();
                    return;
                }
                DetailView.this.isPre = true;
                DetailView.this.applyRotation(-1, 0.0f, -180.0f);
            }
        });
        this.mTextView.setFocusable(true);
        this.Topic.moveToPosition(this.ItemNum);
        setTitle(this.Topic.getString(this.Topic.getColumnIndex(l.m1a("L\u0004H\u0002["))));
        this.Detail.moveToPosition(this.ItemNum);
        this.Summary.moveToPosition(this.ItemNum);
        this.mTextView.setText(String.valueOf(this.Summary.getString(this.Summary.getColumnIndex(R.a("C{]cQ|I")))) + l.m1a("a2") + this.Detail.getString(this.Detail.getColumnIndex(R.a("jUzQg\\"))));
        this.mContainer.setPersistentDrawingCache(1);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 1, 1, l.m1a("郅从剭亓")).setIcon((int) R.drawable.sendemail);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.Summary.close();
        this.Detail.close();
        this.database.close();
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        switch (menuItem.getItemId()) {
            case 1:
                share();
                return true;
            default:
                return false;
        }
    }
}
