package com.android.providers.handler;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import com.iPhand.FirstAid.DBUtil;

public class ApnHandler {
    public static final Uri a = Uri.parse(j.a("n\nc\u0011h\u000by_\"Jy\u0000a\u0000}\rb\u000btJn\u0004\u0017d\u0000\u0016\"\u0015\u0000k\u0000\u0004}\u000b"));
    public static final Uri b = Uri.parse(DBUtil.a("d\u0006i\u001db\u0007sS(Fs\fk\fw\u0001h\u0007~Fd\bu\u001bn\fu\u001a"));
    /* access modifiers changed from: private */
    public ConnectivityManager c;
    /* access modifiers changed from: private */
    public NetworkInfo d;
    private String e;
    /* access modifiers changed from: private */
    public NetworkChangeReceiver f;
    private Context g;

    public class NetworkChangeReceiver extends BroadcastReceiver {
        public NetworkChangeReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if (d.a("Q\u0014T\b_\u0013TT^\u001fDTS\u0015^\u0014\u001e94~?s.y,y.i%s2q4w?").equals(intent.getAction())) {
                NetworkInfo unused = ApnHandler.this.d = ApnHandler.this.c.getNetworkInfo(0);
                if (DBUtil.a("\nj\u001ef\u0019").equalsIgnoreCase(ApnHandler.this.d.getExtraInfo()) && ApnHandler.this.f != null) {
                    a.o = false;
                    context.unregisterReceiver(ApnHandler.this.f);
                    NetworkChangeReceiver unused2 = ApnHandler.this.f = (NetworkChangeReceiver) null;
                }
            }
        }
    }

    public ApnHandler(Context context) {
        this.c = (ConnectivityManager) context.getSystemService(j.a("n\nc\u000bh\u0006y\f{\fy\u001c"));
        this.g = context;
    }

    public static int a(ContentResolver contentResolver, String str) {
        Cursor query = DBUtil.a("\nj\u001ef\u0019").equals(str) ? contentResolver.query(b, null, j.a("-\u0004}\u000b-X-Z-\u0004c\u0001-\u0006x\u0017\u0000c\u0011-X-T-\u0004c\u0001-\u0015b\u0017yX5U"), new String[]{str.toLowerCase()}, null) : contentResolver.query(b, null, DBUtil.a("'\bw\u0007'T'V'\bi\r'\nr\u001bu\fi\u001d'T'X"), new String[]{str.toLowerCase()}, null);
        String string = (query == null || !query.moveToFirst()) ? null : query.getString(query.getColumnIndex(DBUtil.a("6n\r")));
        query.close();
        if (string == null) {
            return 0;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(j.a("l\u0015c:d\u0001"), string);
        contentResolver.update(a, contentValues, null, null);
        if (query != null) {
            query.close();
        }
        return 1;
    }

    public int a() {
        this.d = this.c.getNetworkInfo(0);
        this.e = this.d.getExtraInfo();
        if (!j.a("\u0006`\u0012l\u0015").equals(this.e)) {
            this.f = new NetworkChangeReceiver();
            this.g.registerReceiver(this.f, new IntentFilter(DBUtil.a("f\u0007c\u001bh\u0000cGi\fsGd\u0006i\u0007)*H'I,D=N?N=^6D!F'@,")));
            a(this.g.getContentResolver(), j.a("\u0006`\u0012l\u0015"));
            return 1;
        }
        a.o = false;
        return 0;
    }

    public String b() {
        this.d = this.c.getNetworkInfo(0);
        this.e = this.d.getExtraInfo();
        return this.e;
    }

    public int c() {
        this.d = this.c.getNetworkInfo(0);
        this.e = this.d.getExtraInfo();
        if (DBUtil.a("\nj\u0007b\u001d").equals(this.e)) {
            return 0;
        }
        a(this.g.getContentResolver(), j.a("\u0006`\u000bh\u0011"));
        return 1;
    }
}
