package defpackage;

import java.io.Serializable;

/* renamed from: d  reason: default package */
public class d implements Serializable {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;
    private String i;
    private String j;
    private String k;
    private String l;
    private String m;
    private String n;
    private String o;
    private String p;

    public static String a(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i2 = length - 1;
        int i3 = i2;
        while (i2 >= 0) {
            int i4 = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ 'z');
            if (i4 < 0) {
                break;
            }
            i2 = i4 - 1;
            cArr[i4] = (char) (str.charAt(i4) ^ '0');
            i3 = i2;
        }
        return new String(cArr);
    }

    public String a() {
        return this.p;
    }

    /* renamed from: a  reason: collision with other method in class */
    public void m0a(String str) {
        this.p = str;
    }

    public String b() {
        return this.o;
    }

    public void b(String str) {
        this.i = str;
    }

    public String c() {
        return this.a;
    }

    public void c(String str) {
        this.o = str;
    }

    public String d() {
        return this.c;
    }

    public void d(String str) {
        this.a = str;
    }

    public String e() {
        return this.d;
    }

    public void e(String str) {
        this.b = str;
    }

    public String f() {
        return this.e;
    }

    public void f(String str) {
        this.c = str;
    }

    public String g() {
        return this.f;
    }

    public void g(String str) {
        this.d = str;
    }

    public String h() {
        return this.j;
    }

    public void h(String str) {
        this.e = str;
    }

    public String i() {
        return this.k;
    }

    public void i(String str) {
        this.f = str;
    }

    public String j() {
        return this.l;
    }

    public void j(String str) {
        this.g = str;
    }

    public String k() {
        return this.m;
    }

    public void k(String str) {
        this.h = str;
    }

    public String l() {
        return this.n;
    }

    public void l(String str) {
        this.j = str;
    }

    public void m(String str) {
        this.k = str;
    }

    public void n(String str) {
        this.l = str;
    }

    public void o(String str) {
        this.m = str;
    }

    public void p(String str) {
        this.n = str;
    }
}
