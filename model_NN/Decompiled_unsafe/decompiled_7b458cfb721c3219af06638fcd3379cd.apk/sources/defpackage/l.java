package defpackage;

import android.content.Context;
import java.io.InputStream;

/* renamed from: l  reason: default package */
public class l {
    public static d a(Context context, String str, String str2) {
        d dVar = new d();
        if (str == null || str.length() <= 0) {
            return dVar;
        }
        context.getSharedPreferences(i.a("w\u001db\tb\u001db\u0001d\nt0c\u000es\u000e"), 0).edit().putString(str2, str).commit();
        return a(str);
    }

    public static d a(String str) {
        d dVar = new d();
        if (str != null && str.length() > 0) {
            dVar.d(a(str.toString(), m1a("w;}9y?}")));
            dVar.e(a(str.toString(), i.a("<B=Q&D*")));
            dVar.f(a(str.toString(), m1a("~.}(w/}")));
            dVar.g(a(str.toString(), i.a("J<@^")));
            dVar.h(a(str.toString(), m1a("&k,\n")));
            dVar.i(a(str.toString(), i.a("J<@\\")));
            dVar.j(a(str.toString(), m1a("&k,\f")));
            dVar.k(a(str.toString(), i.a("J<@Z")));
            dVar.b(a(str.toString(), m1a("&k,\u000e")));
            dVar.c(a(str.toString(), i.a("?F<T8F6N+")));
            dVar.l(a(str.toString(), m1a("k;v>u)}9k")));
            dVar.m(a(str.toString(), i.a("D I;B!S<")));
            dVar.n(a(str.toString(), m1a("k.{$v/k")));
            dVar.o(a(str.toString(), i.a("C6I.J&D<")));
            dVar.p(a(str.toString(), m1a("y%k<}9k")));
            dVar.m0a(a(str.toString(), i.a("V:B<S&H!")));
        }
        return dVar;
    }

    public static String a(InputStream inputStream) {
        byte[] bArr = new byte[1024];
        StringBuffer stringBuffer = new StringBuffer();
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                return stringBuffer.toString();
            }
            stringBuffer.append(new String(bArr, 0, read));
            bArr = new byte[1024];
        }
    }

    /* renamed from: a  reason: collision with other method in class */
    public static String m1a(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        int i2 = i;
        while (i >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ '8');
            if (i3 < 0) {
                break;
            }
            i = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ 'k');
            i2 = i;
        }
        return new String(cArr);
    }

    public static String a(String str, String str2) {
        return (str == null || str.equals("") || str.indexOf(str2) == -1) ? "" : str.substring(str.indexOf(m1a("\u0004") + str2 + i.a("Q")) + str2.length() + 2, str.indexOf(m1a("W\u0017") + str2 + i.a("Q")));
    }
}
