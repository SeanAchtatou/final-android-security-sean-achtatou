package defpackage;

import android.util.Xml;
import com.iPhand.FirstAid.R;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.xmlpull.v1.XmlPullParser;

/* renamed from: g  reason: default package */
public class g {
    public static boolean b = true;
    byte[] a = null;
    private String c = "";
    private String d = "";
    private String e = "";
    private int f = 0;
    private String g = "";
    private HttpURLConnection h = null;
    private OutputStream i;
    private InputStream j;
    private final String k = "10.0.0.172:80";
    private final String l = "http://";
    private String m;
    private String n;

    public g() {
    }

    public g(String str) {
        a(str, "", l.m1a(".l"), 0);
    }

    public g(String str, String str2) {
        a(str, "", str2, 0);
    }

    public g(String str, String str2, int i2) {
        a(str, "", str2, i2);
    }

    private /* synthetic */ void a(String str, String str2, String str3, int i2) {
        try {
            this.c = str;
            this.d = str3;
            this.e = str2;
            this.f = i2;
            if (str != null) {
                String[] d2 = d(str);
                this.m = d2[0];
                this.n = d2[1];
                if (b) {
                    this.h = (HttpURLConnection) new URL(l.m1a("\u0003L\u001fHQ\u0017D\t[\u0016[\u0016[\u0016Z\u000fY\u0002S\b") + this.n).openConnection();
                    this.h.setRequestProperty(l.m1a("`Fw\u0005T\u0002V\u000e\u0015#W\u0018L"), this.m);
                } else {
                    this.h = (HttpURLConnection) new URL(str).openConnection();
                }
                this.h.setConnectTimeout(50000);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private /* synthetic */ void b() {
        String headerField = this.h.getHeaderField(l.m1a("j\u000eK\u001eT\u001f\u0015(W\u000f]"));
        if (headerField == null) {
            headerField = "";
        }
        a.l = headerField;
        String headerField2 = this.h.getHeaderField(l.m1a("(W\u0005L\u000eV\u001f\u0015']\u0005_\u001fP"));
        if (headerField2 == null) {
            headerField2 = "";
        }
        a.m = headerField2;
        String headerField3 = this.h.getHeaderField(l.m1a("8]\u001f\u0015(W\u0004S\u0002]"));
        if (headerField3 == null) {
            headerField3 = "";
        }
        a.k = headerField3;
        a.j = new StringBuffer();
        for (Map.Entry next : this.h.getHeaderFields().entrySet()) {
            Object key = next.getKey();
            a.j.append(key + l.m1a("\u0002") + next.getValue() + l.m1a("DV"));
        }
    }

    private /* synthetic */ void d(byte[] bArr) {
        if (bArr != null && bArr.length != 0) {
            this.i = this.h.getOutputStream();
            this.i.write(bArr);
            this.i.flush();
            this.i.close();
        }
    }

    private /* synthetic */ String[] d(String str) {
        String[] strArr = new String[2];
        int length = "http://".length();
        if (str.toLowerCase().indexOf("http://") == -1) {
            return null;
        }
        int indexOf = str.indexOf(l.m1a("\u0017"), length);
        if (indexOf == -1) {
            strArr[0] = str.substring(length, str.length());
            strArr[1] = l.m1a("\u0017");
            return strArr;
        }
        strArr[0] = str.substring(length, indexOf);
        strArr[1] = str.substring(indexOf);
        return strArr;
    }

    private /* synthetic */ void e(byte[] bArr) {
        try {
            this.h.setDoOutput(true);
            this.h.setDoInput(true);
            this.h.setRequestMethod(this.d);
            HashMap hashMap = new HashMap();
            hashMap.put(l.m1a("*[\b]\u001bLF{\u0003Y\u0019K\u000eL"), l.m1a("\u001eL\r\u0015S\u0014KQ\u0018WF\u0000S\rR\u0015Z\u0014KM\u001f^F\t]\u0014K\u0012PIV\bE\u000f"));
            hashMap.put(l.m1a("*[\b]\u001bL"), l.m1a("\nH\u001bT\u0002[\nL\u0002W\u0005\u0017\u0013U\u0007\u0014\nH\u001bT\u0002[\nL\u0002W\u0005\u0017\u0013P\u001fU\u0007\u0013\u0013U\u0007\u0014\u001f]\u0013LDP\u001fU\u0007\u0003\u001a\u0005[\u0016R\u0014\u001f]\u0013LDH\u0007Y\u0002VPIV\bE\u0000GQ\u0006Y\f]DH\u0005_G\u0012D\u0012PIV\bE\r"));
            hashMap.put(l.m1a("y\b[\u000eH\u001f\u0015'Y\u0005_\u001eY\f]"), l.m1a("\u0011PF{%\u0014K]\u0005\u0015>k"));
            hashMap.put(l.m1a("{\n[\u0003]F{\u0004V\u001fJ\u0004T"), l.m1a("\u0005WFK\u001fW\u0019]"));
            hashMap.put(l.m1a("(W\u0005V\u000e[\u001fQ\u0004V"), l.m1a(" ]\u000eHFy\u0007Q\u001d]"));
            hashMap.put(l.m1a(";J\u0004@\u0012\u0015(W\u0005V\u000e[\u001fQ\u0004V"), l.m1a(" ]\u000eHFy\u0007Q\u001d]"));
            if (this.g != null && !this.g.equals("")) {
                if (this.g.contains(l.m1a("\u001b"))) {
                    for (String split : this.g.split(l.m1a("\u001b"))) {
                        String[] split2 = split.split(l.m1a("\u0002"));
                        hashMap.put(split2[0], split2[1]);
                    }
                } else if (this.g.contains(l.m1a("\u0002"))) {
                    String[] split3 = this.g.split(l.m1a("\u0002"));
                    hashMap.put(split3[0], split3[1]);
                }
            }
            for (Map.Entry entry : hashMap.entrySet()) {
                this.h.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
            }
            if (!a.i.equals("")) {
                this.h.setRequestProperty(l.m1a(">K\u000eJFy\f]\u0005L"), a.i);
            }
            if (a.k.equals("") || !this.c.contains(l.m1a("\u0004H\bW\u000f]"))) {
            }
            if (this.e != null && !this.e.equals("")) {
                this.h.setRequestProperty(l.m1a("*[\u001fQ\u0004V"), this.e);
                this.h.setRequestProperty(l.m1a("(T\u0002]\u0005L#Y\u0018P"), a.h);
                this.h.setRequestProperty(l.m1a("n\u000eJ\u0018Q\u0004V"), a.d);
                this.h.setRequestProperty(l.m1a("m\u0018]\u0019\u0015\"\\"), a.f != null ? a.f : "");
                this.h.setRequestProperty(l.m1a("*h\"n\u000eJ\u0018Q\u0004V"), l.m1a("\tE\tE\b"));
                this.h.setRequestProperty(l.m1a("\u0013\u0015\u001eHF[\nT\u0007Q\u0005_FT\u0002V\u000e\u0015\u0002\\"), l.m1a("\tX\u0000]\u000fY\nY\fY\f"));
            }
            if (this.d.equals(l.m1a(";w8l"))) {
                if (bArr == null || bArr.length == 0) {
                    this.h.setRequestProperty(l.m1a("(W\u0005L\u000eV\u001f\u0015\u0007]\u0005_\u001fP"), l.m1a("\b"));
                } else {
                    this.h.setRequestProperty(l.m1a("(W\u0005L\u000eV\u001f\u0015?A\u001b]"), l.m1a("Y\u001bH\u0007Q\bY\u001fQ\u0004VD@FO\u001cOF^\u0004J\u0006\u0015\u001eJ\u0007]\u0005[\u0004\\\u000e\\"));
                    this.h.setRequestProperty(l.m1a("(W\u0005L\u000eV\u001f\u0015\u0007]\u0005_\u001fP"), bArr.length + "");
                }
            }
            for (Map.Entry next : this.h.getRequestProperties().entrySet()) {
                next.getKey();
                next.getValue();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public InputStream a(byte[] bArr) {
        e(bArr);
        if (!(bArr == null || bArr.length == 0)) {
            d(bArr);
        }
        this.j = this.h.getInputStream();
        b();
        return this.j;
    }

    public void a() {
        if (this.h != null) {
            this.h.disconnect();
            this.h = null;
        }
        if (this.j != null) {
            this.j.close();
            this.j = null;
        }
        if (this.i != null) {
            this.i.close();
            this.i = null;
        }
    }

    public byte[] a(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (this.f <= 0) {
            byte[] bArr = new byte[1024];
            while (true) {
                int read = inputStream.read(bArr);
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(bArr, 0, read);
            }
        } else {
            byte[] bArr2 = new byte[1024];
            int i2 = 1;
            while (true) {
                int read2 = inputStream.read(bArr2);
                if (read2 == -1 || this.f < i2) {
                    break;
                }
                byteArrayOutputStream.write(bArr2, 0, read2);
                i2++;
            }
        }
        byteArrayOutputStream.flush();
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        byteArrayOutputStream.close();
        return byteArray;
    }

    public byte[] a(String str) {
        return b(str.getBytes());
    }

    public String b(String str) {
        return new String(b(str.getBytes()));
    }

    public byte[] b(byte[] bArr) {
        byte[] a2 = a(a(bArr));
        a();
        return a2;
    }

    public List c(byte[] bArr) {
        InputStream a2 = a(bArr);
        if (a.m == null || a.m.equals("") || a.m.equals(l.m1a("\b"))) {
            return null;
        }
        XmlPullParser newPullParser = Xml.newPullParser();
        newPullParser.setInput(a2, l.m1a("m?~F\u0000"));
        ArrayList arrayList = null;
        e eVar = null;
        for (int eventType = newPullParser.getEventType(); eventType != 1; eventType = newPullParser.next()) {
            switch (eventType) {
                case R.styleable.com_admob_android_ads_AdView_testing:
                    arrayList = new ArrayList();
                    break;
                case R.styleable.com_admob_android_ads_AdView_textColor:
                    if (l.m1a("J\u000eI\u001e]\u0018L").equalsIgnoreCase(newPullParser.getName())) {
                        eVar = new e();
                    }
                    if (eVar != null) {
                        if (l.m1a("M\u0019T").equalsIgnoreCase(newPullParser.getName())) {
                            eVar.b(newPullParser.nextText());
                        } else if (l.m1a("\u0006]\u001fP\u0004\\").equalsIgnoreCase(newPullParser.getName())) {
                            eVar.c(newPullParser.nextText());
                        } else if (l.m1a("\u000fY\u001fY").equalsIgnoreCase(newPullParser.getName())) {
                            eVar.d(newPullParser.nextText());
                        } else if (l.m1a("\u0002K9]\u001fM\u0019V").equalsIgnoreCase(newPullParser.getName())) {
                            eVar.e(newPullParser.nextText());
                        } else if (l.m1a("\u000fW\u001cV8Q\u0011]").equalsIgnoreCase(newPullParser.getName())) {
                            eVar.f(newPullParser.nextText());
                        } else if (l.m1a("\u0019]\u001aM\u000eK\u001fh\u0019W\u001b]\u0019L\u0012K").equalsIgnoreCase(newPullParser.getName())) {
                            eVar.a(newPullParser.nextText());
                        }
                    }
                    if (!l.m1a("\u0004H\bW\u000f]").equalsIgnoreCase(newPullParser.getName())) {
                        if (!l.m1a("\u0019]\u001fM\u0019V>J\u0007K").equalsIgnoreCase(newPullParser.getName())) {
                            if (!l.m1a("\u0019]\u001fM\u0019V&K\fK").equalsIgnoreCase(newPullParser.getName())) {
                                if (!l.m1a("\u0018]\u001aM\u000eV\b]").equalsIgnoreCase(newPullParser.getName())) {
                                    if (!l.m1a("\u0018A\u0005[").equalsIgnoreCase(newPullParser.getName())) {
                                        break;
                                    } else {
                                        e.e = newPullParser.nextText();
                                        break;
                                    }
                                } else {
                                    e.d = newPullParser.nextText();
                                    break;
                                }
                            } else {
                                e.c = newPullParser.nextText();
                                break;
                            }
                        } else {
                            e.b = newPullParser.nextText();
                            break;
                        }
                    } else {
                        e.a = newPullParser.nextText();
                        break;
                    }
                case R.styleable.com_admob_android_ads_AdView_keywords:
                    if (!l.m1a("J\u000eI\u001e]\u0018L").equalsIgnoreCase(newPullParser.getName())) {
                        break;
                    } else {
                        arrayList.add(eVar);
                        eVar = null;
                        break;
                    }
            }
        }
        return arrayList;
    }

    public void c(String str) {
        this.g = str;
    }
}
