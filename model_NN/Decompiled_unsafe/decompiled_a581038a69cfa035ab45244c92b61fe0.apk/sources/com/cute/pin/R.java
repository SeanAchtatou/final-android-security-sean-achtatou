package com.cute.pin;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int ic_launcher = 2130837504;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class raw {
        public static final int libc = 2131034112;
    }

    public static final class string {
        public static final int app_name = 2131099648;
        public static final int pin = 2131099649;
    }

    public static final class style {
        public static final int AppTheme = 2131165184;
    }

    public static final class xml {
        public static final int pin = 2130968576;
    }
}
