package a.a.a;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

class b implements InvocationHandler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f1a;
    final /* synthetic */ a b;

    b(a aVar, c cVar) {
        this.b = aVar;
        this.f1a = cVar;
    }

    public Object invoke(Object obj, Method method, Object[] objArr) {
        a.f0a.b("invoke: " + method.getName() + " listener: " + this.f1a);
        if ("onZoom".equals(method.getName())) {
            this.f1a.a(((Boolean) objArr[0]).booleanValue());
            return null;
        } else if ("onVisibilityChanged".equals(method.getName())) {
            this.f1a.b(((Boolean) objArr[0]).booleanValue());
            return null;
        } else {
            a.f0a.b("unhandled listener method: " + method);
            return null;
        }
    }
}
