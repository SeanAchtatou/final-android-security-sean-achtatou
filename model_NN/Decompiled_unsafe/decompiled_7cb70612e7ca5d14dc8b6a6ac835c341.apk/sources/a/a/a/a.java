package a.a.a;

import android.view.View;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import org.c.b;
import org.c.c;

public class a {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final b f0a = c.a(a.class);
    private static Class b;
    private static Class c;
    private static Method d;
    private static Method e;
    private static Method f;
    private static Method g;
    private Object h;

    static {
        try {
            b = Class.forName("android.widget.ZoomButtonsController");
            for (Class<?> cls : b.getDeclaredClasses()) {
                if ("OnZoomListener".equals(cls.getSimpleName())) {
                    c = cls;
                }
            }
            d = b.getMethod("setOnZoomListener", c);
            e = b.getMethod("setVisible", Boolean.TYPE);
            f = b.getMethod("setZoomInEnabled", Boolean.TYPE);
            g = b.getMethod("setZoomOutEnabled", Boolean.TYPE);
        } catch (Exception e2) {
            f0a.b("no zoom buttons: " + e2);
        }
    }

    public a(View view) {
        if (b != null) {
            try {
                this.h = b.getConstructor(View.class).newInstance(view);
            } catch (Exception e2) {
                f0a.d("exception instantiating: " + e2);
            }
        }
    }

    public void a(c cVar) {
        if (this.h != null) {
            try {
                b bVar = new b(this, cVar);
                Object newProxyInstance = Proxy.newProxyInstance(c.getClassLoader(), new Class[]{c}, bVar);
                d.invoke(this.h, newProxyInstance);
            } catch (Exception e2) {
                f0a.d("setOnZoomListener exception: " + e2);
            }
        }
    }

    public void a(boolean z) {
        if (this.h != null) {
            try {
                e.invoke(this.h, Boolean.valueOf(z));
            } catch (Exception e2) {
                f0a.d("setVisible exception: " + e2);
            }
        }
    }

    public void b(boolean z) {
        if (this.h != null) {
            try {
                f.invoke(this.h, Boolean.valueOf(z));
            } catch (Exception e2) {
                f0a.d("setZoomInEnabled exception: " + e2);
            }
        }
    }

    public void c(boolean z) {
        if (this.h != null) {
            try {
                g.invoke(this.h, Boolean.valueOf(z));
            } catch (Exception e2) {
                f0a.d("setZoomOutEnabled exception: " + e2);
            }
        }
    }
}
