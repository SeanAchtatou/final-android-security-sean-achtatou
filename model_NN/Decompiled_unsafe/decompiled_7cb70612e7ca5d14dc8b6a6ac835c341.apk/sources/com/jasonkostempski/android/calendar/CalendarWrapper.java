package com.jasonkostempski.android.calendar;

import android.text.format.DateFormat;
import android.text.format.DateUtils;
import java.util.Calendar;
import java.util.Date;

public class CalendarWrapper {

    /* renamed from: a  reason: collision with root package name */
    private Calendar f343a;
    private Date b;
    private Calendar c = Calendar.getInstance();
    private String[] d;
    private String[] e;
    private OnDateChangedListener f;
    private Calendar g;
    private Calendar h;

    public interface OnDateChangedListener {
        void onDateChanged(CalendarWrapper calendarWrapper);
    }

    public CalendarWrapper() {
        this.c.set(11, 12);
        this.c.set(12, 0);
        this.f343a = Calendar.getInstance();
        this.f343a.set(11, 23);
        this.f343a.set(12, 59);
        this.d = new String[this.c.getActualMaximum(7)];
        this.e = new String[(this.c.getActualMaximum(2) + 1)];
        for (int i = 0; i < this.d.length; i++) {
            this.d[i] = DateUtils.getDayOfWeekString(i + 1, 30);
        }
        for (int i2 = 0; i2 < this.e.length; i2++) {
            this.e[i2] = DateUtils.getMonthString(i2, 30);
        }
    }

    private void a() {
        if (this.f != null) {
            this.f.onDateChanged(this);
        }
    }

    public void addDay(int i) {
        if (i != 0) {
            this.c.add(5, i);
            a();
        }
    }

    public void addMonth(int i) {
        if (i != 0) {
            this.c.add(2, i);
            a();
        }
    }

    public void addMonthSetDay(int i, int i2) {
        this.c.add(2, i);
        this.c.set(5, i2);
        a();
    }

    public void addYear(int i) {
        if (i != 0) {
            this.c.add(1, i);
            a();
        }
    }

    public int[] get7x6DayArray() {
        int i;
        this.g = null;
        this.h = null;
        int[] iArr = new int[42];
        Calendar calendar = (Calendar) this.c.clone();
        calendar.set(5, 1);
        int i2 = calendar.get(7);
        int actualMaximum = calendar.getActualMaximum(5);
        int i3 = i2 - 1;
        if (i3 > 0) {
            calendar.set(5, -1);
            int actualMaximum2 = calendar.getActualMaximum(5);
            int i4 = 0;
            for (int i5 = i3; i5 > 0; i5--) {
                int i6 = (actualMaximum2 - i5) + 1;
                if (i4 == i3) {
                    this.g = (Calendar) calendar.clone();
                    this.g.set(5, i6);
                }
                iArr[i4] = i6;
                i4++;
            }
            i = i4;
        } else {
            i = 0;
        }
        int i7 = i;
        for (int i8 = 0; i8 < actualMaximum; i8++) {
            if (i8 == 0 && this.g == null) {
                this.g = (Calendar) calendar.clone();
            }
            iArr[i7] = i8 + 1;
            i7++;
        }
        int i9 = 1;
        for (int i10 = i7; i10 < iArr.length; i10++) {
            if (i10 == i7) {
                iArr[i7] = i9;
            }
            i9++;
            i7++;
        }
        this.h = (Calendar) this.c.clone();
        this.h.add(2, 1);
        this.h.set(5, iArr[41]);
        return iArr;
    }

    public int getDay() {
        return this.c.get(5);
    }

    public int getDayOfWeek() {
        return this.c.get(7);
    }

    public int getMonth() {
        return this.c.get(2);
    }

    public Calendar getSelectedDay() {
        return (Calendar) this.c.clone();
    }

    public String[] getShortDayNames() {
        return this.d;
    }

    public String[] getShortMonthNames() {
        return this.e;
    }

    public Calendar getVisibleEndDate() {
        return this.h;
    }

    public Calendar getVisibleStartDate() {
        return this.g;
    }

    public int getYear() {
        return this.c.get(1);
    }

    public boolean isValidDate(int i, int i2) {
        Calendar calendar = (Calendar) this.c.clone();
        calendar.add(2, i);
        calendar.set(5, i2);
        boolean z = calendar.before(this.f343a) && (this.b == null || calendar.getTimeInMillis() >= this.b.getTime());
        System.out.println("_firstValidDate:" + this.b);
        return z;
    }

    public void setDate(Calendar calendar) {
        this.c = calendar;
        a();
    }

    public void setDay(int i) {
        this.c.set(5, i);
        a();
    }

    public void setFirstValidDate(Date date) {
        System.out.println(this + " / setFirstValidDate: " + date);
        this.b = date;
    }

    public void setMonth(int i) {
        this.c.set(2, i);
        a();
    }

    public void setOnDateChangedListener(OnDateChangedListener onDateChangedListener) {
        this.f = onDateChangedListener;
    }

    public void setYear(int i) {
        this.c.set(1, i);
        a();
    }

    public void setYearAndMonth(int i, int i2) {
        this.c.set(1, i);
        this.c.set(2, i2);
        a();
    }

    public String toString(CharSequence charSequence) {
        return DateFormat.format(charSequence, this.c).toString();
    }
}
