package com.jasonkostempski.android.calendar;

import android.view.View;

final class e implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CalendarView f348a;

    e(CalendarView calendarView) {
        this.f348a = calendarView;
    }

    public void onClick(View view) {
        int i = view == this.f348a.s ? 1 : -1;
        if (this.f348a.v == 2) {
            this.f348a.m.addMonth(i);
        } else if (this.f348a.v == 1) {
            this.f348a.m.addDay(i);
            this.f348a.e();
        } else if (this.f348a.v == 3) {
            CalendarView.a(this.f348a, i);
            this.f348a.b();
        }
    }
}
