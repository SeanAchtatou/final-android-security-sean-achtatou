package com.jasonkostempski.android.calendar;

public class CalendarDayMarker {

    /* renamed from: a  reason: collision with root package name */
    private int f340a;
    private int b;
    private int c;
    private int d;

    public CalendarDayMarker(int i, int i2, int i3, int i4) {
        this.f340a = i;
        this.b = i2;
        this.c = i3;
        this.d = i4;
    }

    public int getColor() {
        return this.d;
    }

    public int getDay() {
        return this.c;
    }

    public int getMonth() {
        return this.b;
    }

    public int getYear() {
        return this.f340a;
    }

    public void setColor(int i) {
        this.d = i;
    }

    public void setDay(int i) {
        this.c = i;
    }

    public void setMonth(int i) {
        this.b = i;
    }

    public void setYear(int i) {
        this.f340a = i;
    }
}
