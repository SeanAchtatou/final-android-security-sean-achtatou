package com.jasonkostempski.android.calendar;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Button;
import com.agilebinary.mobilemonitor.client.android.d.g;
import com.agilebinary.phonebeagle.client.R;
import java.util.Calendar;
import java.util.Date;

public class CalendarDialog extends Dialog implements DialogInterface.OnCancelListener {

    /* renamed from: a  reason: collision with root package name */
    private OnDateSelectedListener f341a;
    /* access modifiers changed from: private */
    public CalendarView b;
    private Button c;
    /* access modifiers changed from: private */
    public Calendar d = Calendar.getInstance();

    public interface OnDateSelectedListener {
        void onDateSelected(Calendar calendar);
    }

    public CalendarDialog(Context context, OnDateSelectedListener onDateSelectedListener) {
        super(context);
        requestWindowFeature(1);
        this.f341a = onDateSelectedListener;
        setOnCancelListener(this);
        setContentView((int) R.layout.calendar_dialog);
        this.b = (CalendarView) findViewById(R.id.calendar_view);
        this.c = (Button) findViewById(R.id.calendar_today);
        this.c.setText(getContext().getString(R.string.label_calendar_today, g.a(getContext()).b(this.d.getTimeInMillis())));
        this.c.setOnClickListener(new a(this, onDateSelectedListener));
        this.b.setOnMonthChangedListener(new b(this));
        this.b.setOnSelectedDayChangedListener(new c(this, onDateSelectedListener));
        a();
    }

    /* access modifiers changed from: private */
    public void a() {
    }

    public void onCancel(DialogInterface dialogInterface) {
        dismiss();
        this.f341a.onDateSelected(null);
    }

    public void setCurrentDate(Calendar calendar, Date date) {
        this.b.setFirstValidDate(date);
        this.b.setDate(calendar);
    }
}
