package com.jasonkostempski.android.calendar;

import android.view.View;

final class f implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CalendarView f349a;

    f(CalendarView calendarView) {
        this.f349a = calendarView;
    }

    public void onClick(View view) {
        int[] iArr = (int[]) view.getTag();
        this.f349a.m.addMonthSetDay(iArr[0], iArr[1]);
        this.f349a.e();
        this.f349a.setView(1);
    }
}
