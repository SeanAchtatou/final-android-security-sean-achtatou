package com.jasonkostempski.android.calendar;

import com.jasonkostempski.android.calendar.CalendarWrapper;

final class d implements CalendarWrapper.OnDateChangedListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CalendarView f347a;

    d(CalendarView calendarView) {
        this.f347a = calendarView;
    }

    public void onDateChanged(CalendarWrapper calendarWrapper) {
        if (Boolean.valueOf((this.f347a.w == calendarWrapper.getYear() && this.f347a.x == calendarWrapper.getMonth()) ? false : true).booleanValue()) {
            this.f347a.a();
            this.f347a.d();
        }
        this.f347a.c();
        this.f347a.b();
    }
}
