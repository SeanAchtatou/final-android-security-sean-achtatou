package com.jasonkostempski.android.calendar;

import android.view.View;
import com.jasonkostempski.android.calendar.CalendarDialog;

final class a implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CalendarDialog.OnDateSelectedListener f344a;
    final /* synthetic */ CalendarDialog b;

    a(CalendarDialog calendarDialog, CalendarDialog.OnDateSelectedListener onDateSelectedListener) {
        this.b = calendarDialog;
        this.f344a = onDateSelectedListener;
    }

    public void onClick(View view) {
        this.b.dismiss();
        this.f344a.onDateSelected(this.b.d);
    }
}
