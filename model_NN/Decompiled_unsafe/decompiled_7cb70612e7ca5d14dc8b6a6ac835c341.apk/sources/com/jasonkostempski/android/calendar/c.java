package com.jasonkostempski.android.calendar;

import com.jasonkostempski.android.calendar.CalendarDialog;
import com.jasonkostempski.android.calendar.CalendarView;

final class c implements CalendarView.OnSelectedDayChangedListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CalendarDialog.OnDateSelectedListener f346a;
    final /* synthetic */ CalendarDialog b;

    c(CalendarDialog calendarDialog, CalendarDialog.OnDateSelectedListener onDateSelectedListener) {
        this.b = calendarDialog;
        this.f346a = onDateSelectedListener;
    }

    public void onSelectedDayChanged(CalendarView calendarView) {
        this.b.dismiss();
        this.f346a.onDateSelected(this.b.b.getSelectedDay());
    }
}
