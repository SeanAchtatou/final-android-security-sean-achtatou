package com.jasonkostempski.android.calendar;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.agilebinary.phonebeagle.client.R;
import com.jasonkostempski.android.calendar.CalendarWrapper;
import java.util.Calendar;
import java.util.Date;

public class CalendarView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private CalendarWrapper.OnDateChangedListener f342a = new d(this);
    private View.OnClickListener b = new e(this);
    private View.OnClickListener c = new f(this);
    private View.OnClickListener d = new g(this);
    private View.OnClickListener e = new h(this);
    private View.OnClickListener f = new i(this);
    private final int g = 5;
    private final int h = 4;
    private final int i = 3;
    private final int j = 2;
    private final int k = 1;
    private final int l = 0;
    /* access modifiers changed from: private */
    public CalendarWrapper m;
    private LinearLayout n;
    private TableLayout o;
    private TableLayout p;
    private Button q;
    private Button r;
    /* access modifiers changed from: private */
    public Button s;
    private OnMonthChangedListener t;
    private OnSelectedDayChangedListener u;
    /* access modifiers changed from: private */
    public int v;
    /* access modifiers changed from: private */
    public int w;
    /* access modifiers changed from: private */
    public int x;

    public interface OnMonthChangedListener {
        void onMonthChanged(CalendarView calendarView);
    }

    public interface OnSelectedDayChangedListener {
        void onSelectedDayChanged(CalendarView calendarView);
    }

    public CalendarView(Context context) {
        super(context);
        a(context);
    }

    public CalendarView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context);
    }

    static /* synthetic */ int a(CalendarView calendarView, int i2) {
        int i3 = calendarView.w + i2;
        calendarView.w = i3;
        return i3;
    }

    /* access modifiers changed from: private */
    public void a() {
        int[] iArr = this.m.get7x6DayArray();
        int i2 = -1;
        int i3 = 0;
        int i4 = 0;
        int i5 = 1;
        while (i3 < iArr.length) {
            int i6 = iArr[i3] == 1 ? i2 + 1 : i2;
            TextView textView = (TextView) ((ViewGroup) this.n.getChildAt(i5)).getChildAt(i4);
            textView.setText(iArr[i3] + "");
            boolean isValidDate = this.m.isValidDate(i6, iArr[i3]);
            textView.setEnabled(isValidDate);
            if (!isValidDate) {
                textView.setTextColor(getResources().getColor(R.color.text_invalid));
            } else if (i6 == 0) {
                textView.setTextColor(getResources().getColor(R.color.text));
            } else {
                textView.setTextColor(getResources().getColor(R.color.text_gray));
            }
            textView.setTag(new int[]{i6, iArr[i3]});
            i4++;
            if (i4 == 7) {
                i5++;
                i4 = 0;
            }
            i3++;
            i2 = i6;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.jasonkostempski.android.calendar.CalendarView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a(Context context) {
        View inflate = LayoutInflater.from(context).inflate((int) R.layout.calendar, (ViewGroup) this, true);
        this.m = new CalendarWrapper();
        this.n = (LinearLayout) inflate.findViewById(R.id.calendarpicker_days);
        this.o = (TableLayout) inflate.findViewById(R.id.calendarpicker_months);
        this.p = (TableLayout) inflate.findViewById(R.id.calendarpicker_years);
        this.q = (Button) inflate.findViewById(R.id.calendarpicker_up);
        this.r = (Button) inflate.findViewById(R.id.calendarpicker_previous);
        this.s = (Button) inflate.findViewById(R.id.calendarpicker_next);
        c();
        String[] shortDayNames = this.m.getShortDayNames();
        int i2 = 0;
        while (i2 < 7) {
            ViewGroup viewGroup = (ViewGroup) this.n.getChildAt(i2);
            for (int i3 = 0; i3 < 7; i3++) {
                if (Boolean.valueOf(i2 == 0).booleanValue()) {
                    ((TextView) viewGroup.getChildAt(i3)).setText(shortDayNames[i3]);
                } else {
                    ((Button) viewGroup.getChildAt(i3)).setOnClickListener(this.c);
                }
            }
            i2++;
        }
        a();
        String[] shortMonthNames = this.m.getShortMonthNames();
        int i4 = 0;
        int i5 = 0;
        while (i4 < 3) {
            TableRow tableRow = (TableRow) this.o.getChildAt(i4);
            int i6 = i5;
            for (int i7 = 0; i7 < 4; i7++) {
                TextView textView = (TextView) tableRow.getChildAt(i7);
                textView.setOnClickListener(this.d);
                textView.setText(shortMonthNames[i6]);
                textView.setTag(Integer.valueOf(i6));
                i6++;
            }
            i4++;
            i5 = i6;
        }
        for (int i8 = 0; i8 < 3; i8++) {
            TableRow tableRow2 = (TableRow) this.p.getChildAt(i8);
            for (int i9 = 0; i9 < 4; i9++) {
                ((TextView) tableRow2.getChildAt(i9)).setOnClickListener(this.e);
            }
        }
        this.m.setOnDateChangedListener(this.f342a);
        this.q.setOnClickListener(this.f);
        this.r.setOnClickListener(this.b);
        this.s.setOnClickListener(this.b);
        setView(2);
    }

    /* access modifiers changed from: private */
    public void b() {
        switch (this.v) {
            case 0:
                this.q.setText("ITEM_VIEW");
                return;
            case 1:
                this.q.setText(this.m.toString("EEEE, MMMM dd, yyyy"));
                return;
            case 2:
                this.q.setText(this.m.toString("MMMM yyyy"));
                return;
            case 3:
                this.q.setText(this.w + "");
                return;
            case 4:
                this.q.setText("DECADE_VIEW");
                return;
            case 5:
                this.q.setText("CENTURY_VIEW");
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        this.w = this.m.getYear();
        this.x = this.m.getMonth();
        this.m.getDay();
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.t != null) {
            this.t.onMonthChanged(this);
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.u != null) {
            this.u.onSelectedDayChanged(this);
        }
    }

    /* access modifiers changed from: private */
    public void setView(int i2) {
        int i3 = 8;
        boolean z = false;
        if (this.v != i2) {
            this.v = i2;
            this.p.setVisibility(this.v == 4 ? 0 : 8);
            this.o.setVisibility(this.v == 3 ? 0 : 8);
            LinearLayout linearLayout = this.n;
            if (this.v == 2) {
                i3 = 0;
            }
            linearLayout.setVisibility(i3);
            Button button = this.q;
            if (this.v != 3) {
                z = true;
            }
            button.setEnabled(z);
            b();
        }
    }

    public Calendar getSelectedDay() {
        return this.m.getSelectedDay();
    }

    public Calendar getVisibleEndDate() {
        return this.m.getVisibleEndDate();
    }

    public Calendar getVisibleStartDate() {
        return this.m.getVisibleStartDate();
    }

    public void setDate(Calendar calendar) {
        this.m.setDate(calendar);
    }

    public void setDaysWithEvents(CalendarDayMarker[] calendarDayMarkerArr) {
        int i2;
        for (CalendarDayMarker calendarDayMarker : calendarDayMarkerArr) {
            int i3 = 0;
            int i4 = 0;
            int i5 = 1;
            while (i3 < 42) {
                TextView textView = (TextView) ((TableRow) this.n.getChildAt(i5)).getChildAt(i4);
                int[] iArr = (int[]) textView.getTag();
                int i6 = iArr[0];
                int i7 = iArr[1];
                if (i6 == 0 && this.m.getYear() == calendarDayMarker.getYear() && this.m.getMonth() == calendarDayMarker.getMonth() && i7 == calendarDayMarker.getDay()) {
                    textView.setBackgroundColor(calendarDayMarker.getColor());
                } else if (i6 == -1) {
                    textView.setBackgroundDrawable(null);
                } else if (i6 == 1) {
                    textView.setBackgroundDrawable(null);
                } else {
                    textView.setBackgroundDrawable(null);
                }
                int i8 = i4 + 1;
                if (i8 == 7) {
                    i2 = i5 + 1;
                    i8 = 0;
                } else {
                    i2 = i5;
                }
                i3++;
                i4 = i8;
                i5 = i2;
            }
        }
    }

    public void setFirstValidDate(Date date) {
        this.m.setFirstValidDate(date);
        a();
    }

    public void setOnMonthChangedListener(OnMonthChangedListener onMonthChangedListener) {
        this.t = onMonthChangedListener;
    }

    public void setOnSelectedDayChangedListener(OnSelectedDayChangedListener onSelectedDayChangedListener) {
        this.u = onSelectedDayChangedListener;
    }
}
