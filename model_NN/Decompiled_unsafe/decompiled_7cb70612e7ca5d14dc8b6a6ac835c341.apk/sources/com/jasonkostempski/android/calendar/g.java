package com.jasonkostempski.android.calendar;

import android.view.View;

final class g implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ CalendarView f350a;

    g(CalendarView calendarView) {
        this.f350a = calendarView;
    }

    public void onClick(View view) {
        this.f350a.m.setYearAndMonth(this.f350a.w, ((Integer) view.getTag()).intValue());
        this.f350a.setView(2);
    }
}
