package com.agilebinary.a.a.b.c.b;

import com.agilebinary.a.a.b.l;
import java.net.InetAddress;

public final class b implements e, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private static final l[] f15a = new l[0];
    private final l b;
    private final InetAddress c;
    private final l[] d;
    private final g e;
    private final f f;
    private final boolean g;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.b.c.b.b.<init>(java.net.InetAddress, com.agilebinary.a.a.b.l, com.agilebinary.a.a.b.l[], boolean, com.agilebinary.a.a.b.c.b.g, com.agilebinary.a.a.b.c.b.f):void
     arg types: [?[OBJECT, ARRAY], com.agilebinary.a.a.b.l, com.agilebinary.a.a.b.l[], int, com.agilebinary.a.a.b.c.b.g, com.agilebinary.a.a.b.c.b.f]
     candidates:
      com.agilebinary.a.a.b.c.b.b.<init>(com.agilebinary.a.a.b.l, java.net.InetAddress, com.agilebinary.a.a.b.l[], boolean, com.agilebinary.a.a.b.c.b.g, com.agilebinary.a.a.b.c.b.f):void
      com.agilebinary.a.a.b.c.b.b.<init>(java.net.InetAddress, com.agilebinary.a.a.b.l, com.agilebinary.a.a.b.l[], boolean, com.agilebinary.a.a.b.c.b.g, com.agilebinary.a.a.b.c.b.f):void */
    public b(l lVar) {
        this((InetAddress) null, lVar, f15a, false, g.PLAIN, f.PLAIN);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public b(l lVar, InetAddress inetAddress, l lVar2, boolean z) {
        this(inetAddress, lVar, a(lVar2), z, z ? g.TUNNELLED : g.PLAIN, z ? f.LAYERED : f.PLAIN);
        if (lVar2 == null) {
            throw new IllegalArgumentException("Proxy host may not be null.");
        }
    }

    public b(l lVar, InetAddress inetAddress, boolean z) {
        this(inetAddress, lVar, f15a, z, g.PLAIN, f.PLAIN);
    }

    public b(l lVar, InetAddress inetAddress, l[] lVarArr, boolean z, g gVar, f fVar) {
        this(inetAddress, lVar, a(lVarArr), z, gVar, fVar);
    }

    private b(InetAddress inetAddress, l lVar, l[] lVarArr, boolean z, g gVar, f fVar) {
        if (lVar == null) {
            throw new IllegalArgumentException("Target host may not be null.");
        } else if (lVarArr == null) {
            throw new IllegalArgumentException("Proxies may not be null.");
        } else if (gVar == g.TUNNELLED && lVarArr.length == 0) {
            throw new IllegalArgumentException("Proxy required if tunnelled.");
        } else {
            gVar = gVar == null ? g.PLAIN : gVar;
            fVar = fVar == null ? f.PLAIN : fVar;
            this.b = lVar;
            this.c = inetAddress;
            this.d = lVarArr;
            this.g = z;
            this.e = gVar;
            this.f = fVar;
        }
    }

    private static l[] a(l lVar) {
        if (lVar == null) {
            return f15a;
        }
        return new l[]{lVar};
    }

    private static l[] a(l[] lVarArr) {
        if (lVarArr == null || lVarArr.length < 1) {
            return f15a;
        }
        for (l lVar : lVarArr) {
            if (lVar == null) {
                throw new IllegalArgumentException("Proxy chain may not contain null elements.");
            }
        }
        l[] lVarArr2 = new l[lVarArr.length];
        System.arraycopy(lVarArr, 0, lVarArr2, 0, lVarArr.length);
        return lVarArr2;
    }

    public final l a() {
        return this.b;
    }

    public final l a(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Hop index must not be negative: " + i);
        }
        int c2 = c();
        if (i < c2) {
            return i < c2 + -1 ? this.d[i] : this.b;
        }
        throw new IllegalArgumentException("Hop index " + i + " exceeds route length " + c2);
    }

    public final InetAddress b() {
        return this.c;
    }

    public final int c() {
        return this.d.length + 1;
    }

    public Object clone() {
        return super.clone();
    }

    public final l d() {
        if (this.d.length == 0) {
            return null;
        }
        return this.d[0];
    }

    public final boolean e() {
        return this.e == g.TUNNELLED;
    }

    public final boolean equals(Object obj) {
        boolean z = true;
        int i = 0;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        boolean equals = (this.d == bVar.d || this.d.length == bVar.d.length) & this.b.equals(bVar.b) & (this.c == bVar.c || (this.c != null && this.c.equals(bVar.c)));
        if (!(this.g == bVar.g && this.e == bVar.e && this.f == bVar.f)) {
            z = false;
        }
        boolean z2 = equals & z;
        if (z2 && this.d != null) {
            while (z2 && i < this.d.length) {
                z2 = this.d[i].equals(bVar.d[i]);
                i++;
            }
        }
        return z2;
    }

    public final boolean f() {
        return this.f == f.LAYERED;
    }

    public final boolean g() {
        return this.g;
    }

    public final int hashCode() {
        int hashCode = this.b.hashCode();
        if (this.c != null) {
            hashCode ^= this.c.hashCode();
        }
        l[] lVarArr = this.d;
        int length = lVarArr.length;
        int length2 = this.d.length ^ hashCode;
        int i = 0;
        while (i < length) {
            int hashCode2 = lVarArr[i].hashCode() ^ length2;
            i++;
            length2 = hashCode2;
        }
        if (this.g) {
            length2 ^= 286331153;
        }
        return (length2 ^ this.e.hashCode()) ^ this.f.hashCode();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder((c() * 30) + 50);
        sb.append("HttpRoute[");
        if (this.c != null) {
            sb.append(this.c);
            sb.append("->");
        }
        sb.append('{');
        if (this.e == g.TUNNELLED) {
            sb.append('t');
        }
        if (this.f == f.LAYERED) {
            sb.append('l');
        }
        if (this.g) {
            sb.append('s');
        }
        sb.append("}->");
        for (l append : this.d) {
            sb.append(append);
            sb.append("->");
        }
        sb.append(this.b);
        sb.append(']');
        return sb.toString();
    }
}
