package com.agilebinary.a.a.b.f.c;

import com.agilebinary.a.a.b.c;
import com.agilebinary.a.a.b.d;
import com.agilebinary.a.a.b.d.b;
import com.agilebinary.a.a.b.d.e;
import com.agilebinary.a.a.b.d.h;
import com.agilebinary.a.a.b.d.m;
import java.util.Iterator;
import java.util.List;

public class k implements h {

    /* renamed from: a  reason: collision with root package name */
    private final String[] f72a;
    private final boolean b;
    private af c;
    private y d;
    private m e;
    private v f;

    public k() {
        this(null, false);
    }

    public k(String[] strArr, boolean z) {
        this.f72a = strArr == null ? null : (String[]) strArr.clone();
        this.b = z;
    }

    private af c() {
        if (this.c == null) {
            this.c = new af(this.f72a, this.b);
        }
        return this.c;
    }

    private y d() {
        if (this.d == null) {
            this.d = new y(this.f72a, this.b);
        }
        return this.d;
    }

    private m e() {
        if (this.e == null) {
            this.e = new m(this.f72a);
        }
        return this.e;
    }

    private v f() {
        if (this.f == null) {
            this.f = new v(this.f72a);
        }
        return this.f;
    }

    public int a() {
        return c().a();
    }

    public List a(c cVar, e eVar) {
        boolean z = false;
        if (cVar == null) {
            throw new IllegalArgumentException("Header may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            d[] e2 = cVar.e();
            boolean z2 = false;
            for (d dVar : e2) {
                if (dVar.a("version") != null) {
                    z = true;
                }
                if (dVar.a("expires") != null) {
                    z2 = true;
                }
            }
            return z ? "Set-Cookie2".equals(cVar.c()) ? c().a(e2, eVar) : d().a(e2, eVar) : z2 ? f().a(cVar, eVar) : e().a(e2, eVar);
        }
    }

    public List a(List list) {
        if (list == null) {
            throw new IllegalArgumentException("List of cookie may not be null");
        }
        Iterator it = list.iterator();
        int i = Integer.MAX_VALUE;
        boolean z = true;
        while (it.hasNext()) {
            b bVar = (b) it.next();
            if (!(bVar instanceof m)) {
                z = false;
            }
            i = bVar.g() < i ? bVar.g() : i;
        }
        return i > 0 ? z ? c().a(list) : d().a(list) : e().a(list);
    }

    public void a(b bVar, e eVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else if (bVar.g() <= 0) {
            e().a(bVar, eVar);
        } else if (bVar instanceof m) {
            c().a(bVar, eVar);
        } else {
            d().a(bVar, eVar);
        }
    }

    public c b() {
        return c().b();
    }

    public boolean b(b bVar, e eVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (eVar != null) {
            return bVar.g() > 0 ? bVar instanceof m ? c().b(bVar, eVar) : d().b(bVar, eVar) : e().b(bVar, eVar);
        } else {
            throw new IllegalArgumentException("Cookie origin may not be null");
        }
    }

    public String toString() {
        return "best-match";
    }
}
