package com.agilebinary.a.a.b.f.e;

import com.agilebinary.a.a.b.g.a;
import com.agilebinary.a.a.b.g.f;
import java.io.IOException;
import java.io.InputStream;

public class g extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    private long f89a;
    private long b = 0;
    private boolean c = false;
    private f d = null;

    public g(f fVar, long j) {
        if (fVar == null) {
            throw new IllegalArgumentException("Input stream may not be null");
        } else if (j < 0) {
            throw new IllegalArgumentException("Content length may not be negative");
        } else {
            this.d = fVar;
            this.f89a = j;
        }
    }

    public int available() {
        if (this.d instanceof a) {
            return Math.min(((a) this.d).e(), (int) (this.f89a - this.b));
        }
        return 0;
    }

    public void close() {
        if (!this.c) {
            try {
                do {
                } while (read(new byte[2048]) >= 0);
            } finally {
                this.c = true;
            }
        }
    }

    public int read() {
        if (this.c) {
            throw new IOException("Attempted read from closed stream.");
        } else if (this.b >= this.f89a) {
            return -1;
        } else {
            this.b++;
            return this.d.a();
        }
    }

    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    public int read(byte[] bArr, int i, int i2) {
        if (this.c) {
            throw new IOException("Attempted read from closed stream.");
        } else if (this.b >= this.f89a) {
            return -1;
        } else {
            if (this.b + ((long) i2) > this.f89a) {
                i2 = (int) (this.f89a - this.b);
            }
            int a2 = this.d.a(bArr, i, i2);
            this.b += (long) a2;
            return a2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(long, long):long} */
    public long skip(long j) {
        int read;
        if (j <= 0) {
            return 0;
        }
        byte[] bArr = new byte[2048];
        long min = Math.min(j, this.f89a - this.b);
        long j2 = 0;
        while (min > 0 && (read = read(bArr, 0, (int) Math.min(2048L, min))) != -1) {
            j2 += (long) read;
            min -= (long) read;
        }
        return j2;
    }
}
