package com.agilebinary.a.a.b.c;

import com.agilebinary.a.a.b.e.f;
import com.agilebinary.a.a.b.i;
import com.agilebinary.a.a.b.k.c;
import java.io.InputStream;
import java.io.OutputStream;

public class a extends f implements i, k {

    /* renamed from: a  reason: collision with root package name */
    protected m f12a;
    protected final boolean b;

    public a(i iVar, m mVar, boolean z) {
        super(iVar);
        if (mVar == null) {
            throw new IllegalArgumentException("Connection may not be null.");
        }
        this.f12a = mVar;
        this.b = z;
    }

    private void k() {
        if (this.f12a != null) {
            try {
                if (this.b) {
                    c.a(this.c);
                    this.f12a.m();
                }
            } finally {
                j();
            }
        }
    }

    public InputStream a() {
        return new j(this.c.a(), this);
    }

    public void a(OutputStream outputStream) {
        super.a(outputStream);
        k();
    }

    /* JADX INFO: finally extract failed */
    public boolean a(InputStream inputStream) {
        try {
            if (this.b && this.f12a != null) {
                inputStream.close();
                this.f12a.m();
            }
            j();
            return false;
        } catch (Throwable th) {
            j();
            throw th;
        }
    }

    /* JADX INFO: finally extract failed */
    public boolean b(InputStream inputStream) {
        try {
            if (this.b && this.f12a != null) {
                inputStream.close();
                this.f12a.m();
            }
            j();
            return false;
        } catch (Throwable th) {
            j();
            throw th;
        }
    }

    public boolean c() {
        return false;
    }

    public boolean c(InputStream inputStream) {
        if (this.f12a == null) {
            return false;
        }
        this.f12a.i();
        return false;
    }

    public void e_() {
        k();
    }

    public void i() {
        if (this.f12a != null) {
            try {
                this.f12a.i();
            } finally {
                this.f12a = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void j() {
        if (this.f12a != null) {
            try {
                this.f12a.e_();
            } finally {
                this.f12a = null;
            }
        }
    }
}
