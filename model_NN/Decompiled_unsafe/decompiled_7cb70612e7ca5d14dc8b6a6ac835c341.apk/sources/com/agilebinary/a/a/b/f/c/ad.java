package com.agilebinary.a.a.b.f.c;

import com.agilebinary.a.a.b.d.a;
import com.agilebinary.a.a.b.d.b;
import com.agilebinary.a.a.b.d.c;
import com.agilebinary.a.a.b.d.e;
import com.agilebinary.a.a.b.d.g;
import com.agilebinary.a.a.b.d.k;
import com.agilebinary.a.a.b.d.l;
import java.util.Locale;

public class ad implements c {
    public void a(b bVar, e eVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            String lowerCase = eVar.a().toLowerCase(Locale.ENGLISH);
            if (bVar.c() == null) {
                throw new g("Invalid cookie state: domain not specified");
            }
            String lowerCase2 = bVar.c().toLowerCase(Locale.ENGLISH);
            if (!(bVar instanceof a) || !((a) bVar).b("domain")) {
                if (!bVar.c().equals(lowerCase)) {
                    throw new g("Illegal domain attribute: \"" + bVar.c() + "\"." + "Domain of origin: \"" + lowerCase + "\"");
                }
            } else if (!lowerCase2.startsWith(".")) {
                throw new g("Domain attribute \"" + bVar.c() + "\" violates RFC 2109: domain must start with a dot");
            } else {
                int indexOf = lowerCase2.indexOf(46, 1);
                if ((indexOf < 0 || indexOf == lowerCase2.length() - 1) && !lowerCase2.equals(".local")) {
                    throw new g("Domain attribute \"" + bVar.c() + "\" violates RFC 2965: the value contains no embedded dots " + "and the value is not .local");
                } else if (!a(lowerCase, lowerCase2)) {
                    throw new g("Domain attribute \"" + bVar.c() + "\" violates RFC 2965: effective host name does not " + "domain-match domain attribute.");
                } else if (lowerCase.substring(0, lowerCase.length() - lowerCase2.length()).indexOf(46) != -1) {
                    throw new g("Domain attribute \"" + bVar.c() + "\" violates RFC 2965: " + "effective host minus domain may not contain any dots");
                }
            }
        }
    }

    public void a(l lVar, String str) {
        if (lVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (str == null) {
            throw new k("Missing value for domain attribute");
        } else if (str.trim().length() == 0) {
            throw new k("Blank value for domain attribute");
        } else {
            String lowerCase = str.toLowerCase(Locale.ENGLISH);
            if (!lowerCase.startsWith(".")) {
                lowerCase = '.' + lowerCase;
            }
            lVar.d(lowerCase);
        }
    }

    public boolean a(String str, String str2) {
        return str.equals(str2) || (str2.startsWith(".") && str.endsWith(str2));
    }

    public boolean b(b bVar, e eVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            String lowerCase = eVar.a().toLowerCase(Locale.ENGLISH);
            String c = bVar.c();
            return a(lowerCase, c) && lowerCase.substring(0, lowerCase.length() - c.length()).indexOf(46) == -1;
        }
    }
}
