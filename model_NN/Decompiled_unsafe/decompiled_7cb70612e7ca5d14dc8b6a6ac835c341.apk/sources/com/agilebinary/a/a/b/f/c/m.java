package com.agilebinary.a.a.b.f.c;

import com.agilebinary.a.a.b.c;
import com.agilebinary.a.a.b.d;
import com.agilebinary.a.a.b.d.e;
import com.agilebinary.a.a.b.d.k;
import com.agilebinary.a.a.b.h.o;
import com.agilebinary.a.a.b.h.t;
import com.agilebinary.a.a.b.k.b;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class m extends o {
    @Deprecated

    /* renamed from: a  reason: collision with root package name */
    protected static final String[] f73a = {"EEE, dd MMM yyyy HH:mm:ss zzz", "EEEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy", "EEE, dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MMM-yyyy HH-mm-ss z", "EEE, dd MMM yy HH:mm:ss z", "EEE dd-MMM-yyyy HH:mm:ss z", "EEE dd MMM yyyy HH:mm:ss z", "EEE dd-MMM-yyyy HH-mm-ss z", "EEE dd-MMM-yy HH:mm:ss z", "EEE dd MMM yy HH:mm:ss z", "EEE,dd-MMM-yy HH:mm:ss z", "EEE,dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MM-yyyy HH:mm:ss z"};
    private static final String[] b = {"EEE, dd MMM yyyy HH:mm:ss zzz", "EEEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy", "EEE, dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MMM-yyyy HH-mm-ss z", "EEE, dd MMM yy HH:mm:ss z", "EEE dd-MMM-yyyy HH:mm:ss z", "EEE dd MMM yyyy HH:mm:ss z", "EEE dd-MMM-yyyy HH-mm-ss z", "EEE dd-MMM-yy HH:mm:ss z", "EEE dd MMM yy HH:mm:ss z", "EEE,dd-MMM-yy HH:mm:ss z", "EEE,dd-MMM-yyyy HH:mm:ss z", "EEE, dd-MM-yyyy HH:mm:ss z"};
    private final String[] c;

    public m() {
        this(null);
    }

    public m(String[] strArr) {
        if (strArr != null) {
            this.c = (String[]) strArr.clone();
        } else {
            this.c = b;
        }
        a("path", new i());
        a("domain", new f());
        a("max-age", new h());
        a("secure", new j());
        a("comment", new e());
        a("expires", new g(this.c));
    }

    public int a() {
        return 0;
    }

    public List a(c cVar, e eVar) {
        boolean z;
        d[] e;
        b bVar;
        t tVar;
        if (cVar == null) {
            throw new IllegalArgumentException("Header may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            String c2 = cVar.c();
            String d = cVar.d();
            if (!c2.equalsIgnoreCase("Set-Cookie")) {
                throw new k("Unrecognized cookie header '" + cVar.toString() + "'");
            }
            int indexOf = d.toLowerCase(Locale.ENGLISH).indexOf("expires=");
            if (indexOf != -1) {
                int length = "expires=".length() + indexOf;
                int indexOf2 = d.indexOf(59, length);
                if (indexOf2 == -1) {
                    indexOf2 = d.length();
                }
                try {
                    q.a(d.substring(length, indexOf2), this.c);
                    z = true;
                } catch (p e2) {
                    z = false;
                }
            } else {
                z = false;
            }
            if (z) {
                u uVar = u.f76a;
                if (cVar instanceof com.agilebinary.a.a.b.b) {
                    bVar = ((com.agilebinary.a.a.b.b) cVar).a();
                    tVar = new t(((com.agilebinary.a.a.b.b) cVar).b(), bVar.c());
                } else {
                    String d2 = cVar.d();
                    if (d2 == null) {
                        throw new k("Header value is null");
                    }
                    bVar = new b(d2.length());
                    bVar.a(d2);
                    tVar = new t(0, bVar.c());
                }
                e = new d[]{uVar.a(bVar, tVar)};
            } else {
                e = cVar.e();
            }
            return a(e, eVar);
        }
    }

    public List a(List list) {
        if (list == null) {
            throw new IllegalArgumentException("List of cookies may not be null");
        } else if (list.isEmpty()) {
            throw new IllegalArgumentException("List of cookies may not be empty");
        } else {
            b bVar = new b(list.size() * 20);
            bVar.a("Cookie");
            bVar.a(": ");
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < list.size()) {
                    com.agilebinary.a.a.b.d.b bVar2 = (com.agilebinary.a.a.b.d.b) list.get(i2);
                    if (i2 > 0) {
                        bVar.a("; ");
                    }
                    bVar.a(bVar2.a());
                    bVar.a("=");
                    String b2 = bVar2.b();
                    if (b2 != null) {
                        bVar.a(b2);
                    }
                    i = i2 + 1;
                } else {
                    ArrayList arrayList = new ArrayList(1);
                    arrayList.add(new o(bVar));
                    return arrayList;
                }
            }
        }
    }

    public c b() {
        return null;
    }

    public String toString() {
        return "compatibility";
    }
}
