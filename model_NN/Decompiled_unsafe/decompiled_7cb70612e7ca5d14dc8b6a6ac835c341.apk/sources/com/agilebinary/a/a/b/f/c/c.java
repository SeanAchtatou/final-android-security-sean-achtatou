package com.agilebinary.a.a.b.f.c;

import com.agilebinary.a.a.b.d.a;
import com.agilebinary.a.a.b.d.l;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class c implements a, l, Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final String f69a;
    private Map b;
    private String c;
    private String d;
    private String e;
    private Date f;
    private String g;
    private boolean h;
    private int i;

    public c(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        this.f69a = str;
        this.b = new HashMap();
        this.c = str2;
    }

    public String a() {
        return this.f69a;
    }

    public String a(String str) {
        return (String) this.b.get(str);
    }

    public void a(int i2) {
        this.i = i2;
    }

    public void a(String str, String str2) {
        this.b.put(str, str2);
    }

    public void a(boolean z) {
        this.h = z;
    }

    public boolean a(Date date) {
        if (date != null) {
            return this.f != null && this.f.getTime() <= date.getTime();
        }
        throw new IllegalArgumentException("Date may not be null");
    }

    public String b() {
        return this.c;
    }

    public void b(Date date) {
        this.f = date;
    }

    public boolean b(String str) {
        return this.b.get(str) != null;
    }

    public String c() {
        return this.e;
    }

    public void c(String str) {
        this.d = str;
    }

    public Object clone() {
        c cVar = (c) super.clone();
        cVar.b = new HashMap(this.b);
        return cVar;
    }

    public String d() {
        return this.g;
    }

    public void d(String str) {
        if (str != null) {
            this.e = str.toLowerCase(Locale.ENGLISH);
        } else {
            this.e = null;
        }
    }

    public void e(String str) {
        this.g = str;
    }

    public int[] e() {
        return null;
    }

    public boolean f() {
        return this.h;
    }

    public int g() {
        return this.i;
    }

    public String toString() {
        return "[version: " + Integer.toString(this.i) + "]" + "[name: " + this.f69a + "]" + "[value: " + this.c + "]" + "[domain: " + this.e + "]" + "[path: " + this.g + "]" + "[expiry: " + this.f + "]";
    }
}
