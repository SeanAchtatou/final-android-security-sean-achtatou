package com.agilebinary.a.a.b.f.b.a;

import com.agilebinary.a.a.b.c.b.b;
import com.agilebinary.a.a.b.c.e;
import com.agilebinary.a.a.b.c.m;
import java.util.concurrent.TimeUnit;

final class i implements e {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ f f52a;
    final /* synthetic */ b b;
    final /* synthetic */ h c;

    i(h hVar, f fVar, b bVar) {
        this.c = hVar;
        this.f52a = fVar;
        this.b = bVar;
    }

    public m a(long j, TimeUnit timeUnit) {
        if (this.b == null) {
            throw new IllegalArgumentException("Route may not be null.");
        }
        if (this.c.f.isDebugEnabled()) {
            this.c.f.debug("Get connection: " + this.b + ", timeout = " + j);
        }
        return new c(this.c, this.f52a.a(j, timeUnit));
    }

    public void a() {
        this.f52a.a();
    }
}
