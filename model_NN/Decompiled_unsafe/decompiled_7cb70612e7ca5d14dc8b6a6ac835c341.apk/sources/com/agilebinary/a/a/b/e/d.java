package com.agilebinary.a.a.b.e;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class d extends a implements Cloneable {
    protected final byte[] d;

    public d(byte[] bArr) {
        if (bArr == null) {
            throw new IllegalArgumentException("Source byte array may not be null");
        }
        this.d = bArr;
    }

    public InputStream a() {
        return new ByteArrayInputStream(this.d);
    }

    public void a(OutputStream outputStream) {
        if (outputStream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        outputStream.write(this.d);
        outputStream.flush();
    }

    public long b() {
        return (long) this.d.length;
    }

    public boolean c() {
        return true;
    }

    public Object clone() {
        return super.clone();
    }

    public boolean g() {
        return false;
    }
}
