package com.agilebinary.a.a.b.a;

import com.agilebinary.a.a.b.k.e;
import java.util.Locale;

public class d {

    /* renamed from: a  reason: collision with root package name */
    public static final String f4a = null;
    public static final String b = null;
    public static final String c = null;
    public static final d d = new d(f4a, -1, b, c);
    private final String e;
    private final String f;
    private final String g;
    private final int h;

    public d(String str, int i, String str2) {
        this(str, i, str2, c);
    }

    public d(String str, int i, String str2, String str3) {
        this.g = str == null ? f4a : str.toLowerCase(Locale.ENGLISH);
        this.h = i < 0 ? -1 : i;
        this.f = str2 == null ? b : str2;
        this.e = str3 == null ? c : str3.toUpperCase(Locale.ENGLISH);
    }

    public int a(d dVar) {
        int i = 0;
        if (e.a(this.e, dVar.e)) {
            i = 1;
        } else if (!(this.e == c || dVar.e == c)) {
            return -1;
        }
        if (e.a(this.f, dVar.f)) {
            i += 2;
        } else if (!(this.f == b || dVar.f == b)) {
            return -1;
        }
        if (this.h == dVar.h) {
            i += 4;
        } else if (!(this.h == -1 || dVar.h == -1)) {
            return -1;
        }
        if (e.a(this.g, dVar.g)) {
            return i + 8;
        }
        if (this.g == f4a || dVar.g == f4a) {
            return i;
        }
        return -1;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof d)) {
            return super.equals(obj);
        }
        d dVar = (d) obj;
        return e.a(this.g, dVar.g) && this.h == dVar.h && e.a(this.f, dVar.f) && e.a(this.e, dVar.e);
    }

    public int hashCode() {
        return e.a(e.a(e.a(e.a(17, this.g), this.h), this.f), this.e);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.e != null) {
            sb.append(this.e.toUpperCase(Locale.ENGLISH));
            sb.append(' ');
        }
        if (this.f != null) {
            sb.append('\'');
            sb.append(this.f);
            sb.append('\'');
        } else {
            sb.append("<any realm>");
        }
        if (this.g != null) {
            sb.append('@');
            sb.append(this.g);
            if (this.h >= 0) {
                sb.append(':');
                sb.append(this.h);
            }
        }
        return sb.toString();
    }
}
