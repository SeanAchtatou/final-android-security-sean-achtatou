package com.agilebinary.a.a.b.f.b;

import com.agilebinary.a.a.a.a.a;
import com.agilebinary.a.a.b.c.b;
import com.agilebinary.a.a.b.c.b.h;
import com.agilebinary.a.a.b.c.c.g;
import com.agilebinary.a.a.b.c.d;
import com.agilebinary.a.a.b.c.e;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;

public class m implements b {

    /* renamed from: a  reason: collision with root package name */
    protected final g f64a;
    protected final d b;
    protected final boolean c;
    protected p d;
    protected o e;
    protected long f;
    protected long g;
    protected volatile boolean h;
    private final Log i = a.a(getClass());

    public m(g gVar) {
        if (gVar == null) {
            throw new IllegalArgumentException("Scheme registry must not be null.");
        }
        this.f64a = gVar;
        this.b = a(gVar);
        this.d = new p(this);
        this.e = null;
        this.f = -1;
        this.c = false;
        this.h = false;
    }

    public g a() {
        return this.f64a;
    }

    /* access modifiers changed from: protected */
    public d a(g gVar) {
        return new f(gVar);
    }

    public final e a(com.agilebinary.a.a.b.c.b.b bVar, Object obj) {
        return new n(this, bVar, obj);
    }

    public synchronized void a(long j, TimeUnit timeUnit) {
        b();
        if (timeUnit == null) {
            throw new IllegalArgumentException("Time unit must not be null.");
        } else if (this.e == null && this.d.b.d()) {
            if (this.f <= System.currentTimeMillis() - timeUnit.toMillis(j)) {
                try {
                    this.d.c();
                } catch (IOException e2) {
                    this.i.debug("Problem closing idle connection.", e2);
                }
            }
        }
        return;
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:45:0x00a1=Splitter:B:45:0x00a1, B:35:0x006f=Splitter:B:35:0x006f} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(com.agilebinary.a.a.b.c.m r6, long r7, java.util.concurrent.TimeUnit r9) {
        /*
            r5 = this;
            r3 = 0
            monitor-enter(r5)
            r5.b()     // Catch:{ all -> 0x0012 }
            boolean r0 = r6 instanceof com.agilebinary.a.a.b.f.b.o     // Catch:{ all -> 0x0012 }
            if (r0 != 0) goto L_0x0015
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0012 }
            java.lang.String r1 = "Connection class mismatch, connection not obtained from this manager."
            r0.<init>(r1)     // Catch:{ all -> 0x0012 }
            throw r0     // Catch:{ all -> 0x0012 }
        L_0x0012:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x0015:
            org.apache.commons.logging.Log r0 = r5.i     // Catch:{ all -> 0x0012 }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x0012 }
            if (r0 == 0) goto L_0x0035
            org.apache.commons.logging.Log r0 = r5.i     // Catch:{ all -> 0x0012 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0012 }
            r1.<init>()     // Catch:{ all -> 0x0012 }
            java.lang.String r2 = "Releasing connection "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0012 }
            java.lang.StringBuilder r1 = r1.append(r6)     // Catch:{ all -> 0x0012 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0012 }
            r0.debug(r1)     // Catch:{ all -> 0x0012 }
        L_0x0035:
            com.agilebinary.a.a.b.f.b.o r6 = (com.agilebinary.a.a.b.f.b.o) r6     // Catch:{ all -> 0x0012 }
            com.agilebinary.a.a.b.f.b.b r0 = r6.f56a     // Catch:{ all -> 0x0012 }
            if (r0 != 0) goto L_0x003d
        L_0x003b:
            monitor-exit(r5)
            return
        L_0x003d:
            com.agilebinary.a.a.b.c.b r0 = r6.p()     // Catch:{ all -> 0x0012 }
            if (r0 == 0) goto L_0x004d
            if (r0 == r5) goto L_0x004d
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x0012 }
            java.lang.String r1 = "Connection not obtained from this manager."
            r0.<init>(r1)     // Catch:{ all -> 0x0012 }
            throw r0     // Catch:{ all -> 0x0012 }
        L_0x004d:
            boolean r0 = r6.d()     // Catch:{ IOException -> 0x0091 }
            if (r0 == 0) goto L_0x006f
            boolean r0 = r5.c     // Catch:{ IOException -> 0x0091 }
            if (r0 != 0) goto L_0x005d
            boolean r0 = r6.s()     // Catch:{ IOException -> 0x0091 }
            if (r0 != 0) goto L_0x006f
        L_0x005d:
            org.apache.commons.logging.Log r0 = r5.i     // Catch:{ IOException -> 0x0091 }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ IOException -> 0x0091 }
            if (r0 == 0) goto L_0x006c
            org.apache.commons.logging.Log r0 = r5.i     // Catch:{ IOException -> 0x0091 }
            java.lang.String r1 = "Released connection open but not reusable."
            r0.debug(r1)     // Catch:{ IOException -> 0x0091 }
        L_0x006c:
            r6.f()     // Catch:{ IOException -> 0x0091 }
        L_0x006f:
            r6.n()     // Catch:{ all -> 0x0012 }
            r0 = 0
            r5.e = r0     // Catch:{ all -> 0x0012 }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0012 }
            r5.f = r0     // Catch:{ all -> 0x0012 }
            int r0 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x0089
            long r0 = r9.toMillis(r7)     // Catch:{ all -> 0x0012 }
            long r2 = r5.f     // Catch:{ all -> 0x0012 }
            long r0 = r0 + r2
            r5.g = r0     // Catch:{ all -> 0x0012 }
            goto L_0x003b
        L_0x0089:
            r0 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r5.g = r0     // Catch:{ all -> 0x0012 }
            goto L_0x003b
        L_0x0091:
            r0 = move-exception
            org.apache.commons.logging.Log r1 = r5.i     // Catch:{ all -> 0x00c4 }
            boolean r1 = r1.isDebugEnabled()     // Catch:{ all -> 0x00c4 }
            if (r1 == 0) goto L_0x00a1
            org.apache.commons.logging.Log r1 = r5.i     // Catch:{ all -> 0x00c4 }
            java.lang.String r2 = "Exception shutting down released connection."
            r1.debug(r2, r0)     // Catch:{ all -> 0x00c4 }
        L_0x00a1:
            r6.n()     // Catch:{ all -> 0x0012 }
            r0 = 0
            r5.e = r0     // Catch:{ all -> 0x0012 }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0012 }
            r5.f = r0     // Catch:{ all -> 0x0012 }
            int r0 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r0 <= 0) goto L_0x00bb
            long r0 = r9.toMillis(r7)     // Catch:{ all -> 0x0012 }
            long r2 = r5.f     // Catch:{ all -> 0x0012 }
            long r0 = r0 + r2
            r5.g = r0     // Catch:{ all -> 0x0012 }
            goto L_0x003b
        L_0x00bb:
            r0 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r5.g = r0     // Catch:{ all -> 0x0012 }
            goto L_0x003b
        L_0x00c4:
            r0 = move-exception
            r6.n()     // Catch:{ all -> 0x0012 }
            r1 = 0
            r5.e = r1     // Catch:{ all -> 0x0012 }
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0012 }
            r5.f = r1     // Catch:{ all -> 0x0012 }
            int r1 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x00df
            long r1 = r9.toMillis(r7)     // Catch:{ all -> 0x0012 }
            long r3 = r5.f     // Catch:{ all -> 0x0012 }
            long r1 = r1 + r3
            r5.g = r1     // Catch:{ all -> 0x0012 }
        L_0x00de:
            throw r0     // Catch:{ all -> 0x0012 }
        L_0x00df:
            r1 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r5.g = r1     // Catch:{ all -> 0x0012 }
            goto L_0x00de
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.b.f.b.m.a(com.agilebinary.a.a.b.c.m, long, java.util.concurrent.TimeUnit):void");
    }

    public synchronized com.agilebinary.a.a.b.c.m b(com.agilebinary.a.a.b.c.b.b bVar, Object obj) {
        boolean z;
        o oVar;
        boolean z2 = true;
        boolean z3 = false;
        synchronized (this) {
            if (bVar == null) {
                throw new IllegalArgumentException("Route may not be null.");
            }
            b();
            if (this.i.isDebugEnabled()) {
                this.i.debug("Get connection for route " + bVar);
            }
            if (this.e != null) {
                throw new IllegalStateException("Invalid use of SingleClientConnManager: connection still allocated.\nMake sure to release the connection before allocating another one.");
            }
            c();
            if (this.d.b.d()) {
                h hVar = this.d.e;
                boolean z4 = hVar == null || !hVar.h().equals(bVar);
                z = false;
                z3 = z4;
            } else {
                z = true;
            }
            if (z3) {
                try {
                    this.d.d();
                } catch (IOException e2) {
                    this.i.debug("Problem shutting down connection.", e2);
                }
            } else {
                z2 = z;
            }
            if (z2) {
                this.d = new p(this);
            }
            this.e = new o(this, this.d, bVar);
            oVar = this.e;
        }
        return oVar;
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.h) {
            throw new IllegalStateException("Manager is shut down.");
        }
    }

    public synchronized void c() {
        if (System.currentTimeMillis() >= this.g) {
            a(0, TimeUnit.MILLISECONDS);
        }
    }

    public synchronized void d() {
        this.h = true;
        if (this.e != null) {
            this.e.n();
        }
        try {
            if (this.d != null) {
                this.d.d();
            }
            this.d = null;
        } catch (IOException e2) {
            this.i.debug("Problem while shutting down manager.", e2);
            this.d = null;
        } catch (Throwable th) {
            this.d = null;
            throw th;
        }
        return;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        try {
            d();
        } finally {
            super.finalize();
        }
    }
}
