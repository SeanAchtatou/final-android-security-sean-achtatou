package com.agilebinary.a.a.b.h;

import com.agilebinary.a.a.b.ab;
import com.agilebinary.a.a.b.i.e;
import com.agilebinary.a.a.b.o;
import com.agilebinary.a.a.b.z;

public class f extends a implements o {
    private final String c;
    private final String d;
    private ab e;

    public f(ab abVar) {
        if (abVar == null) {
            throw new IllegalArgumentException("Request line may not be null");
        }
        this.e = abVar;
        this.c = abVar.a();
        this.d = abVar.c();
    }

    public f(String str, String str2, z zVar) {
        this(new l(str, str2, zVar));
    }

    public z c() {
        return g().b();
    }

    public ab g() {
        if (this.e == null) {
            this.e = new l(this.c, this.d, e.b(f()));
        }
        return this.e;
    }

    public String toString() {
        return this.c + " " + this.d + " " + this.f96a;
    }
}
