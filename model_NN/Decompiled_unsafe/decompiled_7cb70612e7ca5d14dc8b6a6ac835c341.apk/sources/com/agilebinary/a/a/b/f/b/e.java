package com.agilebinary.a.a.b.f.b;

import com.agilebinary.a.a.a.a.a;
import com.agilebinary.a.a.b.c.n;
import com.agilebinary.a.a.b.f.f;
import com.agilebinary.a.a.b.g.c;
import com.agilebinary.a.a.b.g.g;
import com.agilebinary.a.a.b.i.d;
import com.agilebinary.a.a.b.l;
import com.agilebinary.a.a.b.o;
import com.agilebinary.a.a.b.q;
import com.agilebinary.a.a.b.r;
import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;

public class e extends f implements n, com.agilebinary.a.a.b.j.e {

    /* renamed from: a  reason: collision with root package name */
    private final Log f57a = a.a(getClass());
    private final Log b = a.a("org.apache.http.headers");
    private final Log c = a.a("org.apache.http.wire");
    private volatile Socket d;
    private l e;
    private boolean f;
    private volatile boolean g;
    private final Map h = new HashMap();

    /* access modifiers changed from: protected */
    public c a(com.agilebinary.a.a.b.g.f fVar, r rVar, d dVar) {
        return new h(fVar, null, rVar, dVar);
    }

    /* access modifiers changed from: protected */
    public com.agilebinary.a.a.b.g.f a(Socket socket, int i, d dVar) {
        if (i == -1) {
            i = 8192;
        }
        com.agilebinary.a.a.b.g.f a2 = super.a(socket, i, dVar);
        return this.c.isDebugEnabled() ? new k(a2, new q(this.c), com.agilebinary.a.a.b.i.e.a(dVar)) : a2;
    }

    public q a() {
        q a2 = super.a();
        if (this.f57a.isDebugEnabled()) {
            this.f57a.debug("Receiving response: " + a2.a());
        }
        if (this.b.isDebugEnabled()) {
            this.b.debug("<< " + a2.a().toString());
            com.agilebinary.a.a.b.c[] d2 = a2.d();
            int length = d2.length;
            for (int i = 0; i < length; i++) {
                this.b.debug("<< " + d2[i].toString());
            }
        }
        return a2;
    }

    public Object a(String str) {
        return this.h.get(str);
    }

    public void a(o oVar) {
        if (this.f57a.isDebugEnabled()) {
            this.f57a.debug("Sending request: " + oVar.g());
        }
        super.a(oVar);
        if (this.b.isDebugEnabled()) {
            this.b.debug(">> " + oVar.g().toString());
            com.agilebinary.a.a.b.c[] d2 = oVar.d();
            int length = d2.length;
            for (int i = 0; i < length; i++) {
                this.b.debug(">> " + d2[i].toString());
            }
        }
    }

    public void a(String str, Object obj) {
        this.h.put(str, obj);
    }

    public void a(Socket socket, l lVar) {
        q();
        this.d = socket;
        this.e = lVar;
        if (this.g) {
            socket.close();
            throw new IOException("Connection already shutdown");
        }
    }

    public void a(Socket socket, l lVar, boolean z, d dVar) {
        k();
        if (lVar == null) {
            throw new IllegalArgumentException("Target host must not be null.");
        } else if (dVar == null) {
            throw new IllegalArgumentException("Parameters must not be null.");
        } else {
            if (socket != null) {
                this.d = socket;
                a(socket, dVar);
            }
            this.e = lVar;
            this.f = z;
        }
    }

    public void a(boolean z, d dVar) {
        q();
        if (dVar == null) {
            throw new IllegalArgumentException("Parameters must not be null.");
        }
        this.f = z;
        a(this.d, dVar);
    }

    /* access modifiers changed from: protected */
    public g b(Socket socket, int i, d dVar) {
        if (i == -1) {
            i = 8192;
        }
        g b2 = super.b(socket, i, dVar);
        return this.c.isDebugEnabled() ? new l(b2, new q(this.c), com.agilebinary.a.a.b.i.e.a(dVar)) : b2;
    }

    public void c() {
        try {
            super.c();
            this.f57a.debug("Connection closed");
        } catch (IOException e2) {
            this.f57a.debug("I/O error closing connection", e2);
        }
    }

    public void f() {
        this.g = true;
        try {
            super.f();
            this.f57a.debug("Connection shut down");
            Socket socket = this.d;
            if (socket != null) {
                socket.close();
            }
        } catch (IOException e2) {
            this.f57a.debug("I/O error shutting down connection", e2);
        }
    }

    public final boolean i() {
        return this.f;
    }

    public final Socket j() {
        return this.d;
    }
}
