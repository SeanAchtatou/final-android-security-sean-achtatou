package com.agilebinary.a.a.b.h;

import com.agilebinary.a.a.b.ac;
import com.agilebinary.a.a.b.k.b;
import com.agilebinary.a.a.b.z;
import java.io.Serializable;

public class m implements ac, Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final z f106a;
    private final int b;
    private final String c;

    public m(z zVar, int i, String str) {
        if (zVar == null) {
            throw new IllegalArgumentException("Protocol version may not be null.");
        } else if (i < 0) {
            throw new IllegalArgumentException("Status code may not be negative.");
        } else {
            this.f106a = zVar;
            this.b = i;
            this.c = str;
        }
    }

    public z a() {
        return this.f106a;
    }

    public int b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public Object clone() {
        return super.clone();
    }

    public String toString() {
        return h.f101a.a((b) null, this).toString();
    }
}
