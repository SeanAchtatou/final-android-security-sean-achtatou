package com.agilebinary.a.a.b.f.e;

import com.agilebinary.a.a.b.c;
import com.agilebinary.a.a.b.f;
import com.agilebinary.a.a.b.g.d;
import com.agilebinary.a.a.b.g.g;
import com.agilebinary.a.a.b.h.h;
import com.agilebinary.a.a.b.h.r;
import com.agilebinary.a.a.b.n;

public abstract class b implements d {

    /* renamed from: a  reason: collision with root package name */
    protected final g f84a;
    protected final com.agilebinary.a.a.b.k.b b;
    protected final r c;

    public b(g gVar, r rVar, com.agilebinary.a.a.b.i.d dVar) {
        if (gVar == null) {
            throw new IllegalArgumentException("Session input buffer may not be null");
        }
        this.f84a = gVar;
        this.b = new com.agilebinary.a.a.b.k.b(128);
        this.c = rVar == null ? h.f101a : rVar;
    }

    /* access modifiers changed from: protected */
    public abstract void a(n nVar);

    public void b(n nVar) {
        if (nVar == null) {
            throw new IllegalArgumentException("HTTP message may not be null");
        }
        a(nVar);
        f e = nVar.e();
        while (e.hasNext()) {
            this.f84a.a(this.c.a(this.b, (c) e.next()));
        }
        this.b.a();
        this.f84a.a(this.b);
    }
}
