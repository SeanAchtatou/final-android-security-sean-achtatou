package com.agilebinary.a.a.b.f.b;

import com.agilebinary.a.a.b.f.e.a;
import com.agilebinary.a.a.b.g.f;
import com.agilebinary.a.a.b.h.s;
import com.agilebinary.a.a.b.h.t;
import com.agilebinary.a.a.b.i.d;
import com.agilebinary.a.a.b.k.b;
import com.agilebinary.a.a.b.n;
import com.agilebinary.a.a.b.r;
import com.agilebinary.a.a.b.w;
import com.agilebinary.a.a.b.y;
import org.apache.commons.logging.Log;

public class h extends a {
    private final Log b = com.agilebinary.a.a.a.a.a.a(getClass());
    private final r c;
    private final b d;
    private final int e;

    public h(f fVar, s sVar, r rVar, d dVar) {
        super(fVar, sVar, dVar);
        if (rVar == null) {
            throw new IllegalArgumentException("Response factory may not be null");
        }
        this.c = rVar;
        this.d = new b(128);
        this.e = dVar.a("http.connection.max-status-line-garbage", Integer.MAX_VALUE);
    }

    /* access modifiers changed from: protected */
    public n a(f fVar) {
        int i = 0;
        while (true) {
            this.d.a();
            int a2 = fVar.a(this.d);
            if (a2 == -1 && i == 0) {
                throw new w("The target server failed to respond");
            }
            t tVar = new t(0, this.d.c());
            if (this.f83a.b(this.d, tVar)) {
                return this.c.a(this.f83a.c(this.d, tVar), null);
            } else if (a2 != -1 && i < this.e) {
                if (this.b.isDebugEnabled()) {
                    this.b.debug("Garbage in response: " + this.d.toString());
                }
                i++;
            }
        }
        throw new y("The server failed to respond with a valid HTTP response");
    }
}
