package com.agilebinary.a.a.b;

import java.io.Serializable;

public final class t extends z implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final t f121a = new t(0, 9);
    public static final t b = new t(1, 0);
    public static final t c = new t(1, 1);

    public t(int i, int i2) {
        super("HTTP", i, i2);
    }

    public z a(int i, int i2) {
        if (i == this.e && i2 == this.f) {
            return this;
        }
        if (i == 1) {
            if (i2 == 0) {
                return b;
            }
            if (i2 == 1) {
                return c;
            }
        }
        return (i == 0 && i2 == 9) ? f121a : new t(i, i2);
    }
}
