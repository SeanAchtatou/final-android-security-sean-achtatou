package com.agilebinary.a.a.b.j;

public final class c implements e {

    /* renamed from: a  reason: collision with root package name */
    private final e f114a;
    private final e b;

    public c(e eVar, e eVar2) {
        if (eVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        }
        this.f114a = eVar;
        this.b = eVar2;
    }

    public Object a(String str) {
        Object a2 = this.f114a.a(str);
        return a2 == null ? this.b.a(str) : a2;
    }

    public void a(String str, Object obj) {
        this.f114a.a(str, obj);
    }
}
