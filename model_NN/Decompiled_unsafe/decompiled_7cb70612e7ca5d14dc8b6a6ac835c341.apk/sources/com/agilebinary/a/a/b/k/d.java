package com.agilebinary.a.a.b.k;

import java.lang.reflect.Method;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private static final Method f118a = a();

    private static Method a() {
        try {
            return Throwable.class.getMethod("initCause", Throwable.class);
        } catch (NoSuchMethodException e) {
            return null;
        }
    }

    public static void a(Throwable th, Throwable th2) {
        if (f118a != null) {
            try {
                f118a.invoke(th, th2);
            } catch (Exception e) {
            }
        }
    }
}
