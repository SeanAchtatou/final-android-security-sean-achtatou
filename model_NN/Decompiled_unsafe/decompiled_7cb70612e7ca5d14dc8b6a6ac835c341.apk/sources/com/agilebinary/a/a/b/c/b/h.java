package com.agilebinary.a.a.b.c.b;

import com.agilebinary.a.a.b.l;
import java.net.InetAddress;

public final class h implements e, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final l f18a;
    private final InetAddress b;
    private boolean c;
    private l[] d;
    private g e;
    private f f;
    private boolean g;

    public h(b bVar) {
        this(bVar.a(), bVar.b());
    }

    public h(l lVar, InetAddress inetAddress) {
        if (lVar == null) {
            throw new IllegalArgumentException("Target host may not be null.");
        }
        this.f18a = lVar;
        this.b = inetAddress;
        this.e = g.PLAIN;
        this.f = f.PLAIN;
    }

    public final l a() {
        return this.f18a;
    }

    public final l a(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Hop index must not be negative: " + i);
        }
        int c2 = c();
        if (i < c2) {
            return i < c2 + -1 ? this.d[i] : this.f18a;
        }
        throw new IllegalArgumentException("Hop index " + i + " exceeds tracked route length " + c2 + ".");
    }

    public final void a(l lVar, boolean z) {
        if (lVar == null) {
            throw new IllegalArgumentException("Proxy host may not be null.");
        } else if (this.c) {
            throw new IllegalStateException("Already connected.");
        } else {
            this.c = true;
            this.d = new l[]{lVar};
            this.g = z;
        }
    }

    public final void a(boolean z) {
        if (this.c) {
            throw new IllegalStateException("Already connected.");
        }
        this.c = true;
        this.g = z;
    }

    public final InetAddress b() {
        return this.b;
    }

    public final void b(l lVar, boolean z) {
        if (lVar == null) {
            throw new IllegalArgumentException("Proxy host may not be null.");
        } else if (!this.c) {
            throw new IllegalStateException("No tunnel unless connected.");
        } else if (this.d == null) {
            throw new IllegalStateException("No proxy tunnel without proxy.");
        } else {
            l[] lVarArr = new l[(this.d.length + 1)];
            System.arraycopy(this.d, 0, lVarArr, 0, this.d.length);
            lVarArr[lVarArr.length - 1] = lVar;
            this.d = lVarArr;
            this.g = z;
        }
    }

    public final void b(boolean z) {
        if (!this.c) {
            throw new IllegalStateException("No tunnel unless connected.");
        } else if (this.d == null) {
            throw new IllegalStateException("No tunnel without proxy.");
        } else {
            this.e = g.TUNNELLED;
            this.g = z;
        }
    }

    public final int c() {
        if (!this.c) {
            return 0;
        }
        if (this.d == null) {
            return 1;
        }
        return this.d.length + 1;
    }

    public final void c(boolean z) {
        if (!this.c) {
            throw new IllegalStateException("No layered protocol unless connected.");
        }
        this.f = f.LAYERED;
        this.g = z;
    }

    public Object clone() {
        return super.clone();
    }

    public final boolean d() {
        return this.c;
    }

    public final boolean e() {
        return this.e == g.TUNNELLED;
    }

    public final boolean equals(Object obj) {
        boolean z = true;
        int i = 0;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof h)) {
            return false;
        }
        h hVar = (h) obj;
        boolean equals = (this.d == hVar.d || !(this.d == null || hVar.d == null || this.d.length != hVar.d.length)) & this.f18a.equals(hVar.f18a) & (this.b == hVar.b || (this.b != null && this.b.equals(hVar.b)));
        if (!(this.c == hVar.c && this.g == hVar.g && this.e == hVar.e && this.f == hVar.f)) {
            z = false;
        }
        boolean z2 = equals & z;
        if (z2 && this.d != null) {
            while (z2 && i < this.d.length) {
                z2 = this.d[i].equals(hVar.d[i]);
                i++;
            }
        }
        return z2;
    }

    public final boolean f() {
        return this.f == f.LAYERED;
    }

    public final boolean g() {
        return this.g;
    }

    public final b h() {
        if (!this.c) {
            return null;
        }
        return new b(this.f18a, this.b, this.d, this.g, this.e, this.f);
    }

    public final int hashCode() {
        int i;
        int hashCode = this.f18a.hashCode();
        if (this.b != null) {
            hashCode ^= this.b.hashCode();
        }
        if (this.d != null) {
            i = this.d.length ^ hashCode;
            for (l hashCode2 : this.d) {
                i ^= hashCode2.hashCode();
            }
        } else {
            i = hashCode;
        }
        if (this.c) {
            i ^= 286331153;
        }
        if (this.g) {
            i ^= 572662306;
        }
        return (this.e.hashCode() ^ i) ^ this.f.hashCode();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder((c() * 30) + 50);
        sb.append("RouteTracker[");
        if (this.b != null) {
            sb.append(this.b);
            sb.append("->");
        }
        sb.append('{');
        if (this.c) {
            sb.append('c');
        }
        if (this.e == g.TUNNELLED) {
            sb.append('t');
        }
        if (this.f == f.LAYERED) {
            sb.append('l');
        }
        if (this.g) {
            sb.append('s');
        }
        sb.append("}->");
        if (this.d != null) {
            for (l append : this.d) {
                sb.append(append);
                sb.append("->");
            }
        }
        sb.append(this.f18a);
        sb.append(']');
        return sb.toString();
    }
}
