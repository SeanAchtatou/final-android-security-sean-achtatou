package com.agilebinary.a.a.b.f.a;

import com.agilebinary.a.a.b.h.d;
import com.agilebinary.a.a.b.j.e;
import com.agilebinary.a.a.b.q;

public class g implements com.agilebinary.a.a.b.c.g {
    public long a(q qVar, e eVar) {
        if (qVar == null) {
            throw new IllegalArgumentException("HTTP response may not be null");
        }
        d dVar = new d(qVar.d("Keep-Alive"));
        while (dVar.hasNext()) {
            com.agilebinary.a.a.b.d a2 = dVar.a();
            String a3 = a2.a();
            String b = a2.b();
            if (b != null && a3.equalsIgnoreCase("timeout")) {
                try {
                    return Long.parseLong(b) * 1000;
                } catch (NumberFormatException e) {
                }
            }
        }
        return -1;
    }
}
