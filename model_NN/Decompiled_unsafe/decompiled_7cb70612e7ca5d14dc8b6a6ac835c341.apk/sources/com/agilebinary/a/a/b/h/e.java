package com.agilebinary.a.a.b.h;

import com.agilebinary.a.a.b.d;
import com.agilebinary.a.a.b.k.b;
import com.agilebinary.a.a.b.v;
import java.util.ArrayList;

public class e implements q {

    /* renamed from: a  reason: collision with root package name */
    public static final e f100a = new e();
    private static final char[] b = {';', ','};

    private static boolean a(char c, char[] cArr) {
        if (cArr == null) {
            return false;
        }
        for (char c2 : cArr) {
            if (c == c2) {
                return true;
            }
        }
        return false;
    }

    public static final d[] a(String str, q qVar) {
        if (str == null) {
            throw new IllegalArgumentException("Value to parse may not be null");
        }
        if (qVar == null) {
            qVar = f100a;
        }
        b bVar = new b(str.length());
        bVar.a(str);
        return qVar.a(bVar, new t(0, str.length()));
    }

    /* access modifiers changed from: protected */
    public d a(String str, String str2, v[] vVarArr) {
        return new c(str, str2, vVarArr);
    }

    public v a(b bVar, t tVar, char[] cArr) {
        boolean z;
        String str;
        boolean z2;
        boolean z3 = true;
        if (bVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (tVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            int b2 = tVar.b();
            int b3 = tVar.b();
            int a2 = tVar.a();
            while (true) {
                if (b2 >= a2) {
                    z = false;
                    break;
                }
                char a3 = bVar.a(b2);
                if (a3 == '=') {
                    z = false;
                    break;
                } else if (a(a3, cArr)) {
                    z = true;
                    break;
                } else {
                    b2++;
                }
            }
            if (b2 == a2) {
                str = bVar.b(b3, a2);
                z = true;
            } else {
                String b4 = bVar.b(b3, b2);
                b2++;
                str = b4;
            }
            if (z) {
                tVar.a(b2);
                return a(str, (String) null);
            }
            boolean z4 = false;
            boolean z5 = false;
            int i = b2;
            while (true) {
                if (i < a2) {
                    char a4 = bVar.a(i);
                    if (a4 != '\"' || z4) {
                        z2 = z5;
                    } else {
                        z2 = !z5;
                    }
                    if (!z2 && !z4 && a(a4, cArr)) {
                        break;
                    }
                    i++;
                    z4 = z4 ? false : z2 && a4 == '\\';
                    z5 = z2;
                } else {
                    z3 = z;
                    break;
                }
            }
            int i2 = b2;
            while (i2 < i && com.agilebinary.a.a.b.j.d.a(bVar.a(i2))) {
                i2++;
            }
            int i3 = i;
            while (i3 > i2 && com.agilebinary.a.a.b.j.d.a(bVar.a(i3 - 1))) {
                i3--;
            }
            if (i3 - i2 >= 2 && bVar.a(i2) == '\"' && bVar.a(i3 - 1) == '\"') {
                i2++;
                i3--;
            }
            String a5 = bVar.a(i2, i3);
            tVar.a(z3 ? i + 1 : i);
            return a(str, a5);
        }
    }

    /* access modifiers changed from: protected */
    public v a(String str, String str2) {
        return new k(str, str2);
    }

    public d[] a(b bVar, t tVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (tVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            ArrayList arrayList = new ArrayList();
            while (!tVar.c()) {
                d b2 = b(bVar, tVar);
                if (b2.a().length() != 0 || b2.b() != null) {
                    arrayList.add(b2);
                }
            }
            return (d[]) arrayList.toArray(new d[arrayList.size()]);
        }
    }

    public d b(b bVar, t tVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (tVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            v d = d(bVar, tVar);
            v[] vVarArr = null;
            if (!tVar.c() && bVar.a(tVar.b() - 1) != ',') {
                vVarArr = c(bVar, tVar);
            }
            return a(d.a(), d.b(), vVarArr);
        }
    }

    public v[] c(b bVar, t tVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (tVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            int b2 = tVar.b();
            int a2 = tVar.a();
            while (b2 < a2 && com.agilebinary.a.a.b.j.d.a(bVar.a(b2))) {
                b2++;
            }
            tVar.a(b2);
            if (tVar.c()) {
                return new v[0];
            }
            ArrayList arrayList = new ArrayList();
            while (!tVar.c()) {
                arrayList.add(d(bVar, tVar));
                if (bVar.a(tVar.b() - 1) == ',') {
                    break;
                }
            }
            return (v[]) arrayList.toArray(new v[arrayList.size()]);
        }
    }

    public v d(b bVar, t tVar) {
        return a(bVar, tVar, b);
    }
}
