package com.agilebinary.a.a.b.b.a;

import com.agilebinary.a.a.b.b.d.a;
import com.agilebinary.a.a.b.c;
import com.agilebinary.a.a.b.i;
import com.agilebinary.a.a.b.j;

public abstract class b extends f implements j {
    private i c;

    public void a(i iVar) {
        this.c = iVar;
    }

    public boolean a() {
        c c2 = c("Expect");
        return c2 != null && "100-continue".equalsIgnoreCase(c2.d());
    }

    public i b() {
        return this.c;
    }

    public Object clone() {
        b bVar = (b) super.clone();
        if (this.c != null) {
            bVar.c = (i) a.a(this.c);
        }
        return bVar;
    }
}
