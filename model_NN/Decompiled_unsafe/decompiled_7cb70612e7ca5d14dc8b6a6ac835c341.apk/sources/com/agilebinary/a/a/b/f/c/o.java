package com.agilebinary.a.a.b.f.c;

import com.agilebinary.a.a.b.d;
import com.agilebinary.a.a.b.d.b;
import com.agilebinary.a.a.b.d.c;
import com.agilebinary.a.a.b.d.e;
import com.agilebinary.a.a.b.d.k;
import com.agilebinary.a.a.b.v;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public abstract class o extends b {
    protected static String a(e eVar) {
        String b = eVar.b();
        int lastIndexOf = b.lastIndexOf(47);
        if (lastIndexOf < 0) {
            return b;
        }
        if (lastIndexOf == 0) {
            lastIndexOf = 1;
        }
        return b.substring(0, lastIndexOf);
    }

    protected static String b(e eVar) {
        return eVar.a();
    }

    /* access modifiers changed from: protected */
    public List a(d[] dVarArr, e eVar) {
        ArrayList arrayList = new ArrayList(dVarArr.length);
        for (d dVar : dVarArr) {
            String a2 = dVar.a();
            String b = dVar.b();
            if (a2 == null || a2.length() == 0) {
                throw new k("Cookie name may not be empty");
            }
            c cVar = new c(a2, b);
            cVar.e(a(eVar));
            cVar.d(b(eVar));
            v[] c = dVar.c();
            for (int length = c.length - 1; length >= 0; length--) {
                v vVar = c[length];
                String lowerCase = vVar.a().toLowerCase(Locale.ENGLISH);
                cVar.a(lowerCase, vVar.b());
                c a3 = a(lowerCase);
                if (a3 != null) {
                    a3.a(cVar, vVar.b());
                }
            }
            arrayList.add(cVar);
        }
        return arrayList;
    }

    public void a(b bVar, e eVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            for (c a2 : c()) {
                a2.a(bVar, eVar);
            }
        }
    }

    public boolean b(b bVar, e eVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            for (c b : c()) {
                if (!b.b(bVar, eVar)) {
                    return false;
                }
            }
            return true;
        }
    }
}
