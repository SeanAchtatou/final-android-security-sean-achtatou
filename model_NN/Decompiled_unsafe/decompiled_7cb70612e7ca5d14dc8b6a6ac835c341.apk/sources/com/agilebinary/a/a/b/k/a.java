package com.agilebinary.a.a.b.k;

import java.io.Serializable;

public final class a implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private byte[] f116a;
    private int b;

    public a(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Buffer capacity may not be negative");
        }
        this.f116a = new byte[i];
    }

    private void d(int i) {
        byte[] bArr = new byte[Math.max(this.f116a.length << 1, i)];
        System.arraycopy(this.f116a, 0, bArr, 0, this.b);
        this.f116a = bArr;
    }

    public void a() {
        this.b = 0;
    }

    public void a(int i) {
        int i2 = this.b + 1;
        if (i2 > this.f116a.length) {
            d(i2);
        }
        this.f116a[this.b] = (byte) i;
        this.b = i2;
    }

    public void a(b bVar, int i, int i2) {
        if (bVar != null) {
            a(bVar.b(), i, i2);
        }
    }

    public void a(byte[] bArr, int i, int i2) {
        if (bArr != null) {
            if (i < 0 || i > bArr.length || i2 < 0 || i + i2 < 0 || i + i2 > bArr.length) {
                throw new IndexOutOfBoundsException("off: " + i + " len: " + i2 + " b.length: " + bArr.length);
            } else if (i2 != 0) {
                int i3 = this.b + i2;
                if (i3 > this.f116a.length) {
                    d(i3);
                }
                System.arraycopy(bArr, i, this.f116a, this.b, i2);
                this.b = i3;
            }
        }
    }

    public void a(char[] cArr, int i, int i2) {
        if (cArr != null) {
            if (i < 0 || i > cArr.length || i2 < 0 || i + i2 < 0 || i + i2 > cArr.length) {
                throw new IndexOutOfBoundsException("off: " + i + " len: " + i2 + " b.length: " + cArr.length);
            } else if (i2 != 0) {
                int i3 = this.b;
                int i4 = i3 + i2;
                if (i4 > this.f116a.length) {
                    d(i4);
                }
                while (i3 < i4) {
                    this.f116a[i3] = (byte) cArr[i];
                    i++;
                    i3++;
                }
                this.b = i4;
            }
        }
    }

    public int b(int i) {
        return this.f116a[i];
    }

    public byte[] b() {
        byte[] bArr = new byte[this.b];
        if (this.b > 0) {
            System.arraycopy(this.f116a, 0, bArr, 0, this.b);
        }
        return bArr;
    }

    public int c() {
        return this.f116a.length;
    }

    public void c(int i) {
        if (i < 0 || i > this.f116a.length) {
            throw new IndexOutOfBoundsException("len: " + i + " < 0 or > buffer len: " + this.f116a.length);
        }
        this.b = i;
    }

    public int d() {
        return this.b;
    }

    public byte[] e() {
        return this.f116a;
    }

    public boolean f() {
        return this.b == 0;
    }

    public boolean g() {
        return this.b == this.f116a.length;
    }
}
