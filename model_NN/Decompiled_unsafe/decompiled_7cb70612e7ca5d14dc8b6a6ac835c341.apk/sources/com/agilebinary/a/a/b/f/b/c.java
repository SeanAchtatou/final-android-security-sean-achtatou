package com.agilebinary.a.a.b.f.b;

import com.agilebinary.a.a.b.c.b;
import com.agilebinary.a.a.b.c.n;
import com.agilebinary.a.a.b.i.d;
import com.agilebinary.a.a.b.j.e;
import com.agilebinary.a.a.b.l;

public abstract class c extends a {

    /* renamed from: a  reason: collision with root package name */
    protected volatile b f56a;

    protected c(b bVar, b bVar2) {
        super(bVar, bVar2.b);
        this.f56a = bVar2;
    }

    public void a(com.agilebinary.a.a.b.c.b.b bVar, e eVar, d dVar) {
        b t = t();
        a(t);
        t.a(bVar, eVar, dVar);
    }

    /* access modifiers changed from: protected */
    public void a(b bVar) {
        if (q() || bVar == null) {
            throw new d();
        }
    }

    public void a(e eVar, d dVar) {
        b t = t();
        a(t);
        t.a(eVar, dVar);
    }

    public void a(l lVar, boolean z, d dVar) {
        b t = t();
        a(t);
        t.a(lVar, z, dVar);
    }

    public void a(Object obj) {
        b t = t();
        a(t);
        t.a(obj);
    }

    public void a(boolean z, d dVar) {
        b t = t();
        a(t);
        t.a(z, dVar);
    }

    public void c() {
        b t = t();
        if (t != null) {
            t.b();
        }
        n o = o();
        if (o != null) {
            o.c();
        }
    }

    public void f() {
        b t = t();
        if (t != null) {
            t.b();
        }
        n o = o();
        if (o != null) {
            o.f();
        }
    }

    public com.agilebinary.a.a.b.c.b.b k() {
        b t = t();
        a(t);
        if (t.e == null) {
            return null;
        }
        return t.e.h();
    }

    /* access modifiers changed from: protected */
    public synchronized void n() {
        this.f56a = null;
        super.n();
    }

    /* access modifiers changed from: protected */
    public b t() {
        return this.f56a;
    }
}
