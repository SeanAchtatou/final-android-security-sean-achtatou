package com.agilebinary.a.a.b.f.c;

import com.agilebinary.a.a.b.d.k;
import com.agilebinary.a.a.b.d.l;

public class g extends a {

    /* renamed from: a  reason: collision with root package name */
    private final String[] f71a;

    public g(String[] strArr) {
        if (strArr == null) {
            throw new IllegalArgumentException("Array of date patterns may not be null");
        }
        this.f71a = strArr;
    }

    public void a(l lVar, String str) {
        if (lVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (str == null) {
            throw new k("Missing value for expires attribute");
        } else {
            try {
                lVar.b(q.a(str, this.f71a));
            } catch (p e) {
                throw new k("Unable to parse expires attribute: " + str);
            }
        }
    }
}
