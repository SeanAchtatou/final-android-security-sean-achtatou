package com.agilebinary.a.a.b.b.c;

import com.agilebinary.a.a.b.b.a.g;
import com.agilebinary.a.a.b.c;
import com.agilebinary.a.a.b.c.m;
import com.agilebinary.a.a.b.d.b;
import com.agilebinary.a.a.b.d.h;
import com.agilebinary.a.a.b.d.j;
import com.agilebinary.a.a.b.j.e;
import com.agilebinary.a.a.b.l;
import com.agilebinary.a.a.b.o;
import com.agilebinary.a.a.b.p;
import com.agilebinary.a.a.b.y;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import org.apache.commons.logging.Log;

public class a implements p {

    /* renamed from: a  reason: collision with root package name */
    private final Log f6a = com.agilebinary.a.a.a.a.a.a(getClass());

    public void a(o oVar, e eVar) {
        URI uri;
        int i;
        boolean z;
        c b;
        if (oVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else if (!oVar.g().a().equalsIgnoreCase("CONNECT")) {
            com.agilebinary.a.a.b.b.e eVar2 = (com.agilebinary.a.a.b.b.e) eVar.a("http.cookie-store");
            if (eVar2 == null) {
                this.f6a.info("Cookie store not available in HTTP context");
                return;
            }
            j jVar = (j) eVar.a("http.cookiespec-registry");
            if (jVar == null) {
                this.f6a.info("CookieSpec registry not available in HTTP context");
                return;
            }
            l lVar = (l) eVar.a("http.target_host");
            if (lVar == null) {
                throw new IllegalStateException("Target host not specified in HTTP context");
            }
            m mVar = (m) eVar.a("http.connection");
            if (mVar == null) {
                throw new IllegalStateException("Client connection not specified in HTTP context");
            }
            String c = com.agilebinary.a.a.b.b.b.a.c(oVar.f());
            if (this.f6a.isDebugEnabled()) {
                this.f6a.debug("CookieSpec selected: " + c);
            }
            if (oVar instanceof g) {
                uri = ((g) oVar).h();
            } else {
                try {
                    uri = new URI(oVar.g().c());
                } catch (URISyntaxException e) {
                    throw new y("Invalid request URI: " + oVar.g().c(), e);
                }
            }
            String a2 = lVar.a();
            int b2 = lVar.b();
            if (b2 < 0) {
                com.agilebinary.a.a.b.c.c.g gVar = (com.agilebinary.a.a.b.c.c.g) eVar.a("http.scheme-registry");
                i = gVar != null ? gVar.b(lVar.c()).a(b2) : mVar.h();
            } else {
                i = b2;
            }
            com.agilebinary.a.a.b.d.e eVar3 = new com.agilebinary.a.a.b.d.e(a2, i, uri.getPath(), mVar.j());
            h a3 = jVar.a(c, oVar.f());
            ArrayList<b> arrayList = new ArrayList<>(eVar2.a());
            ArrayList arrayList2 = new ArrayList();
            Date date = new Date();
            for (b bVar : arrayList) {
                if (!bVar.a(date)) {
                    if (a3.b(bVar, eVar3)) {
                        if (this.f6a.isDebugEnabled()) {
                            this.f6a.debug("Cookie " + bVar + " match " + eVar3);
                        }
                        arrayList2.add(bVar);
                    }
                } else if (this.f6a.isDebugEnabled()) {
                    this.f6a.debug("Cookie " + bVar + " expired");
                }
            }
            if (!arrayList2.isEmpty()) {
                for (c a4 : a3.a(arrayList2)) {
                    oVar.a(a4);
                }
            }
            int a5 = a3.a();
            if (a5 > 0) {
                boolean z2 = false;
                Iterator it = arrayList2.iterator();
                while (true) {
                    z = z2;
                    if (!it.hasNext()) {
                        break;
                    }
                    b bVar2 = (b) it.next();
                    z2 = (a5 != bVar2.g() || !(bVar2 instanceof com.agilebinary.a.a.b.d.m)) ? true : z;
                }
                if (z && (b = a3.b()) != null) {
                    oVar.a(b);
                }
            }
            eVar.a("http.cookie-spec", a3);
            eVar.a("http.cookie-origin", eVar3);
        }
    }
}
