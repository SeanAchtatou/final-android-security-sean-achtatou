package com.agilebinary.a.a.b.i;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class b extends a implements Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final HashMap f111a = new HashMap();

    public d a(String str, Object obj) {
        this.f111a.put(str, obj);
        return this;
    }

    public Object a(String str) {
        return this.f111a.get(str);
    }

    /* access modifiers changed from: protected */
    public void a(d dVar) {
        for (Map.Entry entry : this.f111a.entrySet()) {
            if (entry.getKey() instanceof String) {
                dVar.a((String) entry.getKey(), entry.getValue());
            }
        }
    }

    public Object clone() {
        b bVar = (b) super.clone();
        a(bVar);
        return bVar;
    }
}
