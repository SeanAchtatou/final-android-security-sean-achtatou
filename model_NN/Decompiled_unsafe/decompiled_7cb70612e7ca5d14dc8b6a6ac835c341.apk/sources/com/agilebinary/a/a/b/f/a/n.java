package com.agilebinary.a.a.b.f.a;

import com.agilebinary.a.a.b.a.a;
import com.agilebinary.a.a.b.a.e;
import com.agilebinary.a.a.b.a.h;
import com.agilebinary.a.a.b.b.m;
import java.security.Principal;
import javax.net.ssl.SSLSession;

public class n implements m {
    private static Principal a(e eVar) {
        h d;
        a c = eVar.c();
        if (c == null || !c.d() || !c.c() || (d = eVar.d()) == null) {
            return null;
        }
        return d.a();
    }

    public Object a(com.agilebinary.a.a.b.j.e eVar) {
        SSLSession l;
        Principal principal = null;
        e eVar2 = (e) eVar.a("http.auth.target-scope");
        if (eVar2 != null && (principal = a(eVar2)) == null) {
            principal = a((e) eVar.a("http.auth.proxy-scope"));
        }
        if (principal == null) {
            com.agilebinary.a.a.b.c.m mVar = (com.agilebinary.a.a.b.c.m) eVar.a("http.connection");
            if (mVar.d() && (l = mVar.l()) != null) {
                return l.getLocalPrincipal();
            }
        }
        return principal;
    }
}
