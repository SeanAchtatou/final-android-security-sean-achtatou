package com.agilebinary.a.a.b.f.a;

import com.agilebinary.a.a.b.a;
import com.agilebinary.a.a.b.a.e;
import com.agilebinary.a.a.b.a.h;
import com.agilebinary.a.a.b.b.i;
import com.agilebinary.a.a.b.b.j;
import com.agilebinary.a.a.b.b.k;
import com.agilebinary.a.a.b.b.m;
import com.agilebinary.a.a.b.c.b;
import com.agilebinary.a.a.b.c.b.d;
import com.agilebinary.a.a.b.c.g;
import com.agilebinary.a.a.b.i.c;
import com.agilebinary.a.a.b.j.f;
import com.agilebinary.a.a.b.o;
import com.agilebinary.a.a.b.q;
import com.agilebinary.a.a.b.y;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;

public class l implements com.agilebinary.a.a.b.b.l {

    /* renamed from: a  reason: collision with root package name */
    protected final b f42a;
    protected final d b;
    protected final a c;
    protected final g d;
    protected final com.agilebinary.a.a.b.j.g e;
    protected final f f;
    protected final com.agilebinary.a.a.b.b.g g;
    @Deprecated
    protected final j h = null;
    protected final k i;
    protected final com.agilebinary.a.a.b.b.b j;
    protected final com.agilebinary.a.a.b.b.b k;
    protected final m l;
    protected final com.agilebinary.a.a.b.i.d m;
    protected com.agilebinary.a.a.b.c.m n;
    protected final e o;
    protected final e p;
    private final Log q;
    private int r;
    private int s;
    private int t;
    private com.agilebinary.a.a.b.l u;

    public l(Log log, com.agilebinary.a.a.b.j.g gVar, b bVar, a aVar, g gVar2, d dVar, f fVar, com.agilebinary.a.a.b.b.g gVar3, k kVar, com.agilebinary.a.a.b.b.b bVar2, com.agilebinary.a.a.b.b.b bVar3, m mVar, com.agilebinary.a.a.b.i.d dVar2) {
        if (log == null) {
            throw new IllegalArgumentException("Log may not be null.");
        } else if (gVar == null) {
            throw new IllegalArgumentException("Request executor may not be null.");
        } else if (bVar == null) {
            throw new IllegalArgumentException("Client connection manager may not be null.");
        } else if (aVar == null) {
            throw new IllegalArgumentException("Connection reuse strategy may not be null.");
        } else if (gVar2 == null) {
            throw new IllegalArgumentException("Connection keep alive strategy may not be null.");
        } else if (dVar == null) {
            throw new IllegalArgumentException("Route planner may not be null.");
        } else if (fVar == null) {
            throw new IllegalArgumentException("HTTP protocol processor may not be null.");
        } else if (gVar3 == null) {
            throw new IllegalArgumentException("HTTP request retry handler may not be null.");
        } else if (kVar == null) {
            throw new IllegalArgumentException("Redirect strategy may not be null.");
        } else if (bVar2 == null) {
            throw new IllegalArgumentException("Target authentication handler may not be null.");
        } else if (bVar3 == null) {
            throw new IllegalArgumentException("Proxy authentication handler may not be null.");
        } else if (mVar == null) {
            throw new IllegalArgumentException("User token handler may not be null.");
        } else if (dVar2 == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            this.q = log;
            this.e = gVar;
            this.f42a = bVar;
            this.c = aVar;
            this.d = gVar2;
            this.b = dVar;
            this.f = fVar;
            this.g = gVar3;
            this.i = kVar;
            this.j = bVar2;
            this.k = bVar3;
            this.l = mVar;
            this.m = dVar2;
            this.n = null;
            this.r = 0;
            this.s = 0;
            this.t = this.m.a("http.protocol.max-redirects", 100);
            this.o = new e();
            this.p = new e();
        }
    }

    private r a(o oVar) {
        return oVar instanceof com.agilebinary.a.a.b.j ? new o((com.agilebinary.a.a.b.j) oVar) : new r(oVar);
    }

    private void a(e eVar, com.agilebinary.a.a.b.l lVar, com.agilebinary.a.a.b.b.f fVar) {
        if (eVar.b()) {
            String a2 = lVar.a();
            int b2 = lVar.b();
            if (b2 < 0) {
                b2 = this.f42a.a().a(lVar).a();
            }
            com.agilebinary.a.a.b.a.a c2 = eVar.c();
            com.agilebinary.a.a.b.a.d dVar = new com.agilebinary.a.a.b.a.d(a2, b2, c2.b(), c2.a());
            if (this.q.isDebugEnabled()) {
                this.q.debug("Authentication scope: " + dVar);
            }
            h d2 = eVar.d();
            if (d2 == null) {
                d2 = fVar.a(dVar);
                if (this.q.isDebugEnabled()) {
                    if (d2 != null) {
                        this.q.debug("Found credentials");
                    } else {
                        this.q.debug("Credentials not found");
                    }
                }
            } else if (c2.d()) {
                this.q.debug("Authentication failed");
                d2 = null;
            }
            eVar.a(dVar);
            eVar.a(d2);
        }
    }

    private void a(s sVar, com.agilebinary.a.a.b.j.e eVar) {
        com.agilebinary.a.a.b.c.b.b b2 = sVar.b();
        boolean z = true;
        int i2 = 0;
        while (z) {
            int i3 = i2 + 1;
            try {
                if (!this.n.d()) {
                    this.n.a(b2, eVar, this.m);
                } else {
                    this.n.b(c.a(this.m));
                }
                a(b2, eVar);
                i2 = i3;
                z = false;
            } catch (IOException e2) {
                try {
                    this.n.c();
                } catch (IOException e3) {
                }
                if (this.g.a(e2, i3, eVar)) {
                    if (this.q.isInfoEnabled()) {
                        this.q.info("I/O exception (" + e2.getClass().getName() + ") caught when connecting to the target host: " + e2.getMessage());
                    }
                    if (this.q.isDebugEnabled()) {
                        this.q.debug(e2.getMessage(), e2);
                    }
                    this.q.info("Retrying connect");
                    i2 = i3;
                } else {
                    throw e2;
                }
            }
        }
    }

    private void a(Map map, e eVar, com.agilebinary.a.a.b.b.b bVar, q qVar, com.agilebinary.a.a.b.j.e eVar2) {
        com.agilebinary.a.a.b.a.a c2 = eVar.c();
        if (c2 == null) {
            c2 = bVar.a(map, qVar, eVar2);
            eVar.a(c2);
        }
        com.agilebinary.a.a.b.a.a aVar = c2;
        String a2 = aVar.a();
        com.agilebinary.a.a.b.c cVar = (com.agilebinary.a.a.b.c) map.get(a2.toLowerCase(Locale.ENGLISH));
        if (cVar == null) {
            throw new com.agilebinary.a.a.b.a.f(a2 + " authorization challenge expected, but not found");
        }
        aVar.a(cVar);
        this.q.debug("Authorization challenge processed");
    }

    private q b(s sVar, com.agilebinary.a.a.b.j.e eVar) {
        IOException e2 = null;
        r a2 = sVar.a();
        com.agilebinary.a.a.b.c.b.b b2 = sVar.b();
        boolean z = true;
        q qVar = null;
        while (z) {
            this.r++;
            a2.n();
            if (!a2.j()) {
                this.q.debug("Cannot retry non-repeatable request");
                if (e2 != null) {
                    throw new com.agilebinary.a.a.b.b.h("Cannot retry request with a non-repeatable request entity.  The cause lists the reason the original request failed.", e2);
                }
                throw new com.agilebinary.a.a.b.b.h("Cannot retry request with a non-repeatable request entity.");
            }
            try {
                if (this.q.isDebugEnabled()) {
                    this.q.debug("Attempt " + this.r + " to execute request");
                }
                qVar = this.e.a(a2, this.n, eVar);
                z = false;
            } catch (IOException e3) {
                e2 = e3;
                this.q.debug("Closing the connection.");
                try {
                    this.n.c();
                } catch (IOException e4) {
                }
                if (this.g.a(e2, a2.m(), eVar)) {
                    if (this.q.isInfoEnabled()) {
                        this.q.info("I/O exception (" + e2.getClass().getName() + ") caught when processing request: " + e2.getMessage());
                    }
                    if (this.q.isDebugEnabled()) {
                        this.q.debug(e2.getMessage(), e2);
                    }
                    this.q.info("Retrying request");
                    if (!b2.e()) {
                        this.q.debug("Reopening the direct connection.");
                        this.n.a(b2, eVar, this.m);
                    } else {
                        this.q.debug("Proxied connection. Need to start over.");
                        z = false;
                    }
                } else {
                    throw e2;
                }
            }
        }
        return qVar;
    }

    private void b() {
        com.agilebinary.a.a.b.c.m mVar = this.n;
        if (mVar != null) {
            this.n = null;
            try {
                mVar.i();
            } catch (IOException e2) {
                if (this.q.isDebugEnabled()) {
                    this.q.debug(e2.getMessage(), e2);
                }
            }
            try {
                mVar.e_();
            } catch (IOException e3) {
                this.q.debug("Error releasing connection", e3);
            }
        }
    }

    /* access modifiers changed from: protected */
    public s a(s sVar, q qVar, com.agilebinary.a.a.b.j.e eVar) {
        com.agilebinary.a.a.b.c.b.b b2 = sVar.b();
        r a2 = sVar.a();
        com.agilebinary.a.a.b.i.d f2 = a2.f();
        if (!com.agilebinary.a.a.b.b.b.a.a(f2) || !this.i.a(a2, qVar, eVar)) {
            com.agilebinary.a.a.b.b.f fVar = (com.agilebinary.a.a.b.b.f) eVar.a("http.auth.credentials-provider");
            if (fVar != null && com.agilebinary.a.a.b.b.b.a.b(f2)) {
                if (this.j.a(qVar, eVar)) {
                    com.agilebinary.a.a.b.l lVar = (com.agilebinary.a.a.b.l) eVar.a("http.target_host");
                    com.agilebinary.a.a.b.l a3 = lVar == null ? b2.a() : lVar;
                    this.q.debug("Target requested authentication");
                    try {
                        a(this.j.b(qVar, eVar), this.o, this.j, qVar, eVar);
                    } catch (com.agilebinary.a.a.b.a.f e2) {
                        if (this.q.isWarnEnabled()) {
                            this.q.warn("Authentication error: " + e2.getMessage());
                            return null;
                        }
                    }
                    a(this.o, a3, fVar);
                    if (this.o.d() == null) {
                        return null;
                    }
                    return sVar;
                }
                this.o.a((com.agilebinary.a.a.b.a.d) null);
                if (this.k.a(qVar, eVar)) {
                    com.agilebinary.a.a.b.l d2 = b2.d();
                    this.q.debug("Proxy requested authentication");
                    try {
                        a(this.k.b(qVar, eVar), this.p, this.k, qVar, eVar);
                    } catch (com.agilebinary.a.a.b.a.f e3) {
                        if (this.q.isWarnEnabled()) {
                            this.q.warn("Authentication error: " + e3.getMessage());
                            return null;
                        }
                    }
                    a(this.p, d2, fVar);
                    if (this.p.d() == null) {
                        return null;
                    }
                    return sVar;
                }
                this.p.a((com.agilebinary.a.a.b.a.d) null);
            }
            return null;
        } else if (this.s >= this.t) {
            throw new i("Maximum redirects (" + this.t + ") exceeded");
        } else {
            this.s++;
            this.u = null;
            com.agilebinary.a.a.b.b.a.g b3 = this.i.b(a2, qVar, eVar);
            b3.a(a2.l().d());
            URI h2 = b3.h();
            if (h2.getHost() == null) {
                throw new y("Redirect URI does not specify a valid host name: " + h2);
            }
            com.agilebinary.a.a.b.l lVar2 = new com.agilebinary.a.a.b.l(h2.getHost(), h2.getPort(), h2.getScheme());
            this.o.a((com.agilebinary.a.a.b.a.d) null);
            this.p.a((com.agilebinary.a.a.b.a.d) null);
            if (!b2.a().equals(lVar2)) {
                this.o.a();
                com.agilebinary.a.a.b.a.a c2 = this.p.c();
                if (c2 != null && c2.c()) {
                    this.p.a();
                }
            }
            r a4 = a(b3);
            a4.a(f2);
            com.agilebinary.a.a.b.c.b.b b4 = b(lVar2, a4, eVar);
            s sVar2 = new s(a4, b4);
            if (!this.q.isDebugEnabled()) {
                return sVar2;
            }
            this.q.debug("Redirecting to '" + h2 + "' via " + b4);
            return sVar2;
        }
    }

    public q a(com.agilebinary.a.a.b.l lVar, o oVar, com.agilebinary.a.a.b.j.e eVar) {
        s sVar;
        boolean z;
        r a2 = a(oVar);
        a2.a(this.m);
        com.agilebinary.a.a.b.c.b.b b2 = b(lVar, a2, eVar);
        this.u = (com.agilebinary.a.a.b.l) oVar.f().a("http.virtual-host");
        s sVar2 = new s(a2, b2);
        long f2 = (long) c.f(this.m);
        boolean z2 = false;
        q qVar = null;
        boolean z3 = false;
        s sVar3 = sVar2;
        while (!z2) {
            try {
                r a3 = sVar3.a();
                com.agilebinary.a.a.b.c.b.b b3 = sVar3.b();
                Object a4 = eVar.a("http.user-token");
                if (this.n == null) {
                    com.agilebinary.a.a.b.c.e a5 = this.f42a.a(b3, a4);
                    if (oVar instanceof com.agilebinary.a.a.b.b.a.a) {
                        ((com.agilebinary.a.a.b.b.a.a) oVar).a(a5);
                    }
                    this.n = a5.a(f2, TimeUnit.MILLISECONDS);
                    if (c.g(this.m) && this.n.d()) {
                        this.q.debug("Stale connection check");
                        if (this.n.e()) {
                            this.q.debug("Stale connection detected");
                            this.n.c();
                        }
                    }
                }
                if (oVar instanceof com.agilebinary.a.a.b.b.a.a) {
                    ((com.agilebinary.a.a.b.b.a.a) oVar).a(this.n);
                }
                try {
                    a(sVar3, eVar);
                    a3.k();
                    a(a3, b3);
                    com.agilebinary.a.a.b.l lVar2 = this.u;
                    if (lVar2 == null) {
                        lVar2 = b3.a();
                    }
                    com.agilebinary.a.a.b.l d2 = b3.d();
                    eVar.a("http.target_host", lVar2);
                    eVar.a("http.proxy_host", d2);
                    eVar.a("http.connection", this.n);
                    eVar.a("http.auth.target-scope", this.o);
                    eVar.a("http.auth.proxy-scope", this.p);
                    this.e.a(a3, this.f, eVar);
                    q b4 = b(sVar3, eVar);
                    if (b4 == null) {
                        qVar = b4;
                    } else {
                        b4.a(this.m);
                        this.e.a(b4, this.f, eVar);
                        z3 = this.c.a(b4, eVar);
                        if (z3) {
                            long a6 = this.d.a(b4, eVar);
                            if (this.q.isDebugEnabled()) {
                                this.q.debug("Connection can be kept alive for " + (a6 >= 0 ? a6 + " " + TimeUnit.MILLISECONDS : "ever"));
                            }
                            this.n.a(a6, TimeUnit.MILLISECONDS);
                        }
                        s a7 = a(sVar3, b4, eVar);
                        if (a7 == null) {
                            z = true;
                            sVar = sVar3;
                        } else {
                            if (z3) {
                                com.agilebinary.a.a.b.k.c.a(b4.b());
                                this.n.m();
                            } else {
                                this.n.c();
                            }
                            if (!a7.b().equals(sVar3.b())) {
                                a();
                            }
                            boolean z4 = z2;
                            sVar = a7;
                            z = z4;
                        }
                        if (this.n != null && a4 == null) {
                            Object a8 = this.l.a(eVar);
                            eVar.a("http.user-token", a8);
                            if (a8 != null) {
                                this.n.a(a8);
                            }
                        }
                        sVar3 = sVar;
                        z2 = z;
                        qVar = b4;
                    }
                } catch (t e2) {
                    if (this.q.isDebugEnabled()) {
                        this.q.debug(e2.getMessage());
                    }
                    qVar = e2.a();
                }
            } catch (InterruptedException e3) {
                InterruptedIOException interruptedIOException = new InterruptedIOException();
                interruptedIOException.initCause(e3);
                throw interruptedIOException;
            } catch (com.agilebinary.a.a.b.f.b.d e4) {
                InterruptedIOException interruptedIOException2 = new InterruptedIOException("Connection has been shut down");
                interruptedIOException2.initCause(e4);
                throw interruptedIOException2;
            } catch (com.agilebinary.a.a.b.k e5) {
                b();
                throw e5;
            } catch (IOException e6) {
                b();
                throw e6;
            } catch (RuntimeException e7) {
                b();
                throw e7;
            }
        }
        if (qVar == null || qVar.b() == null || !qVar.b().g()) {
            if (z3) {
                this.n.m();
            }
            a();
        } else {
            qVar.a(new com.agilebinary.a.a.b.c.a(qVar.b(), this.n, z3));
        }
        return qVar;
    }

    /* access modifiers changed from: protected */
    public void a() {
        try {
            this.n.e_();
        } catch (IOException e2) {
            this.q.debug("IOException releasing connection", e2);
        }
        this.n = null;
    }

    /* access modifiers changed from: protected */
    public void a(com.agilebinary.a.a.b.c.b.b bVar, com.agilebinary.a.a.b.j.e eVar) {
        int a2;
        com.agilebinary.a.a.b.c.b.a aVar = new com.agilebinary.a.a.b.c.b.a();
        do {
            com.agilebinary.a.a.b.c.b.b k2 = this.n.k();
            a2 = aVar.a(bVar, k2);
            switch (a2) {
                case -1:
                    throw new IllegalStateException("Unable to establish route.\nplanned = " + bVar + "\ncurrent = " + k2);
                case 0:
                    break;
                case 1:
                case 2:
                    this.n.a(bVar, eVar, this.m);
                    continue;
                case 3:
                    boolean b2 = b(bVar, eVar);
                    this.q.debug("Tunnel to target created.");
                    this.n.a(b2, this.m);
                    continue;
                case 4:
                    int c2 = k2.c() - 1;
                    boolean a3 = a(bVar, c2, eVar);
                    this.q.debug("Tunnel to proxy created.");
                    this.n.a(bVar.a(c2), a3, this.m);
                    continue;
                case 5:
                    this.n.a(eVar, this.m);
                    continue;
                default:
                    throw new IllegalStateException("Unknown step indicator " + a2 + " from RouteDirector.");
            }
        } while (a2 > 0);
    }

    /* access modifiers changed from: protected */
    public void a(r rVar, com.agilebinary.a.a.b.c.b.b bVar) {
        try {
            URI h2 = rVar.h();
            if (bVar.d() == null || bVar.e()) {
                if (h2.isAbsolute()) {
                    rVar.a(com.agilebinary.a.a.b.b.d.b.a(h2, (com.agilebinary.a.a.b.l) null));
                }
            } else if (!h2.isAbsolute()) {
                rVar.a(com.agilebinary.a.a.b.b.d.b.a(h2, bVar.a()));
            }
        } catch (URISyntaxException e2) {
            throw new y("Invalid URI: " + rVar.g().c(), e2);
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(com.agilebinary.a.a.b.c.b.b bVar, int i2, com.agilebinary.a.a.b.j.e eVar) {
        throw new UnsupportedOperationException("Proxy chains are not supported.");
    }

    /* access modifiers changed from: protected */
    public com.agilebinary.a.a.b.c.b.b b(com.agilebinary.a.a.b.l lVar, o oVar, com.agilebinary.a.a.b.j.e eVar) {
        com.agilebinary.a.a.b.l lVar2 = lVar == null ? (com.agilebinary.a.a.b.l) oVar.f().a("http.default-host") : lVar;
        if (lVar2 != null) {
            return this.b.a(lVar2, oVar, eVar);
        }
        throw new IllegalStateException("Target host must not be null, or set in parameters.");
    }

    /* access modifiers changed from: protected */
    public boolean b(com.agilebinary.a.a.b.c.b.b bVar, com.agilebinary.a.a.b.j.e eVar) {
        boolean z;
        com.agilebinary.a.a.b.l d2 = bVar.d();
        com.agilebinary.a.a.b.l a2 = bVar.a();
        boolean z2 = false;
        q qVar = null;
        while (true) {
            if (z2) {
                break;
            }
            z2 = true;
            if (!this.n.d()) {
                this.n.a(bVar, eVar, this.m);
            }
            o c2 = c(bVar, eVar);
            c2.a(this.m);
            eVar.a("http.target_host", a2);
            eVar.a("http.proxy_host", d2);
            eVar.a("http.connection", this.n);
            eVar.a("http.auth.target-scope", this.o);
            eVar.a("http.auth.proxy-scope", this.p);
            eVar.a("http.request", c2);
            this.e.a(c2, this.f, eVar);
            qVar = this.e.a(c2, this.n, eVar);
            qVar.a(this.m);
            this.e.a(qVar, this.f, eVar);
            if (qVar.a().b() < 200) {
                throw new com.agilebinary.a.a.b.k("Unexpected response to CONNECT request: " + qVar.a());
            }
            com.agilebinary.a.a.b.b.f fVar = (com.agilebinary.a.a.b.b.f) eVar.a("http.auth.credentials-provider");
            if (fVar != null && com.agilebinary.a.a.b.b.b.a.b(this.m)) {
                if (this.k.a(qVar, eVar)) {
                    this.q.debug("Proxy requested authentication");
                    try {
                        a(this.k.b(qVar, eVar), this.p, this.k, qVar, eVar);
                    } catch (com.agilebinary.a.a.b.a.f e2) {
                        if (this.q.isWarnEnabled()) {
                            this.q.warn("Authentication error: " + e2.getMessage());
                            break;
                        }
                    }
                    a(this.p, d2, fVar);
                    if (this.p.d() == null) {
                        z = true;
                    } else if (this.c.a(qVar, eVar)) {
                        this.q.debug("Connection kept alive");
                        com.agilebinary.a.a.b.k.c.a(qVar.b());
                        z = false;
                    } else {
                        this.n.c();
                        z = false;
                    }
                    z2 = z;
                } else {
                    this.p.a((com.agilebinary.a.a.b.a.d) null);
                }
            }
        }
        if (qVar.a().b() > 299) {
            com.agilebinary.a.a.b.i b2 = qVar.b();
            if (b2 != null) {
                qVar.a(new com.agilebinary.a.a.b.e.c(b2));
            }
            this.n.c();
            throw new t("CONNECT refused by proxy: " + qVar.a(), qVar);
        }
        this.n.m();
        return false;
    }

    /* access modifiers changed from: protected */
    public o c(com.agilebinary.a.a.b.c.b.b bVar, com.agilebinary.a.a.b.j.e eVar) {
        com.agilebinary.a.a.b.l a2 = bVar.a();
        String a3 = a2.a();
        int b2 = a2.b();
        if (b2 < 0) {
            b2 = this.f42a.a().a(a2.c()).a();
        }
        StringBuilder sb = new StringBuilder(a3.length() + 6);
        sb.append(a3);
        sb.append(':');
        sb.append(Integer.toString(b2));
        return new com.agilebinary.a.a.b.h.f("CONNECT", sb.toString(), com.agilebinary.a.a.b.i.e.b(this.m));
    }
}
