package com.agilebinary.a.a.b.c.c;

import com.agilebinary.a.a.b.i.d;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

@Deprecated
class i implements h {

    /* renamed from: a  reason: collision with root package name */
    private final j f23a;

    i(j jVar) {
        this.f23a = jVar;
    }

    public Socket a(d dVar) {
        return this.f23a.a();
    }

    public Socket a(Socket socket, InetSocketAddress inetSocketAddress, InetSocketAddress inetSocketAddress2, d dVar) {
        String hostName = inetSocketAddress.getHostName();
        int port = inetSocketAddress.getPort();
        InetAddress inetAddress = null;
        int i = 0;
        if (inetSocketAddress2 != null) {
            inetAddress = inetSocketAddress2.getAddress();
            i = inetSocketAddress2.getPort();
        }
        return this.f23a.a(socket, hostName, port, inetAddress, i, dVar);
    }

    public boolean a(Socket socket) {
        return this.f23a.a(socket);
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        return obj instanceof i ? this.f23a.equals(((i) obj).f23a) : this.f23a.equals(obj);
    }

    public int hashCode() {
        return this.f23a.hashCode();
    }
}
