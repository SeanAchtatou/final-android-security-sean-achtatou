package com.agilebinary.a.a.b.h;

import com.agilebinary.a.a.b.ab;
import com.agilebinary.a.a.b.ac;
import com.agilebinary.a.a.b.c;
import com.agilebinary.a.a.b.k.b;
import com.agilebinary.a.a.b.z;

public class h implements r {

    /* renamed from: a  reason: collision with root package name */
    public static final h f101a = new h();

    /* access modifiers changed from: protected */
    public int a(z zVar) {
        return zVar.a().length() + 4;
    }

    /* access modifiers changed from: protected */
    public b a(b bVar) {
        if (bVar == null) {
            return new b(64);
        }
        bVar.a();
        return bVar;
    }

    public b a(b bVar, ab abVar) {
        if (abVar == null) {
            throw new IllegalArgumentException("Request line may not be null");
        }
        b a2 = a(bVar);
        b(a2, abVar);
        return a2;
    }

    public b a(b bVar, ac acVar) {
        if (acVar == null) {
            throw new IllegalArgumentException("Status line may not be null");
        }
        b a2 = a(bVar);
        b(a2, acVar);
        return a2;
    }

    public b a(b bVar, c cVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Header may not be null");
        } else if (cVar instanceof com.agilebinary.a.a.b.b) {
            return ((com.agilebinary.a.a.b.b) cVar).a();
        } else {
            b a2 = a(bVar);
            b(a2, cVar);
            return a2;
        }
    }

    public b a(b bVar, z zVar) {
        if (zVar == null) {
            throw new IllegalArgumentException("Protocol version may not be null");
        }
        int a2 = a(zVar);
        if (bVar == null) {
            bVar = new b(a2);
        } else {
            bVar.b(a2);
        }
        bVar.a(zVar.a());
        bVar.a('/');
        bVar.a(Integer.toString(zVar.b()));
        bVar.a('.');
        bVar.a(Integer.toString(zVar.c()));
        return bVar;
    }

    /* access modifiers changed from: protected */
    public void b(b bVar, ab abVar) {
        String a2 = abVar.a();
        String c = abVar.c();
        bVar.b(a2.length() + 1 + c.length() + 1 + a(abVar.b()));
        bVar.a(a2);
        bVar.a(' ');
        bVar.a(c);
        bVar.a(' ');
        a(bVar, abVar.b());
    }

    /* access modifiers changed from: protected */
    public void b(b bVar, ac acVar) {
        int a2 = a(acVar.a()) + 1 + 3 + 1;
        String c = acVar.c();
        if (c != null) {
            a2 += c.length();
        }
        bVar.b(a2);
        a(bVar, acVar.a());
        bVar.a(' ');
        bVar.a(Integer.toString(acVar.b()));
        bVar.a(' ');
        if (c != null) {
            bVar.a(c);
        }
    }

    /* access modifiers changed from: protected */
    public void b(b bVar, c cVar) {
        String c = cVar.c();
        String d = cVar.d();
        int length = c.length() + 2;
        if (d != null) {
            length += d.length();
        }
        bVar.b(length);
        bVar.a(c);
        bVar.a(": ");
        if (d != null) {
            bVar.a(d);
        }
    }
}
