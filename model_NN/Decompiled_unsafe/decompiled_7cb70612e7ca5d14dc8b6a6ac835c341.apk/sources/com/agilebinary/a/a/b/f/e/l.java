package com.agilebinary.a.a.b.f.e;

import com.agilebinary.a.a.b.g.a;
import com.agilebinary.a.a.b.g.f;
import java.io.InputStream;

public class l extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    private final f f92a;
    private boolean b = false;

    public l(f fVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("Session input buffer may not be null");
        }
        this.f92a = fVar;
    }

    public int available() {
        if (this.f92a instanceof a) {
            return ((a) this.f92a).e();
        }
        return 0;
    }

    public void close() {
        this.b = true;
    }

    public int read() {
        if (this.b) {
            return -1;
        }
        return this.f92a.a();
    }

    public int read(byte[] bArr, int i, int i2) {
        if (this.b) {
            return -1;
        }
        return this.f92a.a(bArr, i, i2);
    }
}
