package com.agilebinary.a.a.b.h;

import com.agilebinary.a.a.b.aa;
import com.agilebinary.a.a.b.ac;
import com.agilebinary.a.a.b.i;
import com.agilebinary.a.a.b.q;
import com.agilebinary.a.a.b.z;
import java.util.Locale;

public class g extends a implements q {
    private ac c;
    private i d;
    private aa e;
    private Locale f;

    public g(ac acVar, aa aaVar, Locale locale) {
        if (acVar == null) {
            throw new IllegalArgumentException("Status line may not be null.");
        }
        this.c = acVar;
        this.e = aaVar;
        this.f = locale == null ? Locale.getDefault() : locale;
    }

    public ac a() {
        return this.c;
    }

    public void a(i iVar) {
        this.d = iVar;
    }

    public i b() {
        return this.d;
    }

    public z c() {
        return this.c.a();
    }

    public String toString() {
        return this.c + " " + this.f96a;
    }
}
