package com.agilebinary.a.a.b.j;

import java.util.HashMap;
import java.util.Map;

public class a implements e {

    /* renamed from: a  reason: collision with root package name */
    private final e f112a;
    private Map b;

    public a() {
        this(null);
    }

    public a(e eVar) {
        this.b = null;
        this.f112a = eVar;
    }

    public Object a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Id may not be null");
        }
        Object obj = null;
        if (this.b != null) {
            obj = this.b.get(str);
        }
        return (obj != null || this.f112a == null) ? obj : this.f112a.a(str);
    }

    public void a(String str, Object obj) {
        if (str == null) {
            throw new IllegalArgumentException("Id may not be null");
        }
        if (this.b == null) {
            this.b = new HashMap();
        }
        this.b.put(str, obj);
    }
}
