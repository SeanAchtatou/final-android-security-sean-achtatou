package com.agilebinary.a.a.b.f.c;

import com.agilebinary.a.a.b.d.a;
import com.agilebinary.a.a.b.d.b;
import com.agilebinary.a.a.b.d.c;
import com.agilebinary.a.a.b.d.e;
import com.agilebinary.a.a.b.d.g;
import com.agilebinary.a.a.b.d.k;
import com.agilebinary.a.a.b.d.l;
import com.agilebinary.a.a.b.d.m;
import java.util.StringTokenizer;

public class ae implements c {
    private static boolean a(int i, int[] iArr) {
        for (int i2 : iArr) {
            if (i == i2) {
                return true;
            }
        }
        return false;
    }

    private static int[] a(String str) {
        StringTokenizer stringTokenizer = new StringTokenizer(str, ",");
        int[] iArr = new int[stringTokenizer.countTokens()];
        int i = 0;
        while (stringTokenizer.hasMoreTokens()) {
            try {
                iArr[i] = Integer.parseInt(stringTokenizer.nextToken().trim());
                if (iArr[i] < 0) {
                    throw new k("Invalid Port attribute.");
                }
                i++;
            } catch (NumberFormatException e) {
                throw new k("Invalid Port attribute: " + e.getMessage());
            }
        }
        return iArr;
    }

    public void a(b bVar, e eVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            int c = eVar.c();
            if ((bVar instanceof a) && ((a) bVar).b("port") && !a(c, bVar.e())) {
                throw new g("Port attribute violates RFC 2965: Request port not found in cookie's port list.");
            }
        }
    }

    public void a(l lVar, String str) {
        if (lVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (lVar instanceof m) {
            m mVar = (m) lVar;
            if (str != null && str.trim().length() > 0) {
                mVar.a(a(str));
            }
        }
    }

    public boolean b(b bVar, e eVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            int c = eVar.c();
            if ((bVar instanceof a) && ((a) bVar).b("port")) {
                if (bVar.e() == null) {
                    return false;
                }
                if (!a(c, bVar.e())) {
                    return false;
                }
            }
            return true;
        }
    }
}
