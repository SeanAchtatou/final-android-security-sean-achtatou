package com.agilebinary.a.a.b.b.c;

import com.agilebinary.a.a.a.a.a;
import com.agilebinary.a.a.b.a.d;
import com.agilebinary.a.a.b.a.e;
import com.agilebinary.a.a.b.a.h;
import com.agilebinary.a.a.b.b.f;
import com.agilebinary.a.a.b.f.a.c;
import com.agilebinary.a.a.b.l;
import com.agilebinary.a.a.b.o;
import com.agilebinary.a.a.b.p;
import org.apache.commons.logging.Log;

public class b implements p {

    /* renamed from: a  reason: collision with root package name */
    private final Log f7a = a.a(getClass());

    private void a(l lVar, com.agilebinary.a.a.b.a.a aVar, e eVar, f fVar) {
        String a2 = aVar.a();
        if (this.f7a.isDebugEnabled()) {
            this.f7a.debug("Re-using cached '" + a2 + "' auth scheme for " + lVar);
        }
        h a3 = fVar.a(new d(lVar.a(), lVar.b(), a2));
        if (a3 != null) {
            eVar.a(aVar);
            eVar.a(a3);
            return;
        }
        this.f7a.debug("No credentials for preemptive authentication");
    }

    public void a(o oVar, com.agilebinary.a.a.b.j.e eVar) {
        com.agilebinary.a.a.b.a.a a2;
        com.agilebinary.a.a.b.a.a a3;
        if (oVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else {
            com.agilebinary.a.a.b.b.a aVar = (com.agilebinary.a.a.b.b.a) eVar.a("http.auth.auth-cache");
            if (aVar == null) {
                eVar.a("http.auth.auth-cache", new c());
                return;
            }
            f fVar = (f) eVar.a("http.auth.credentials-provider");
            l lVar = (l) eVar.a("http.target_host");
            if (!(lVar == null || (a3 = aVar.a(lVar)) == null)) {
                a(lVar, a3, (e) eVar.a("http.auth.target-scope"), fVar);
            }
            l lVar2 = (l) eVar.a("http.proxy_host");
            if (lVar2 != null && (a2 = aVar.a(lVar2)) != null) {
                a(lVar2, a2, (e) eVar.a("http.auth.proxy-scope"), fVar);
            }
        }
    }
}
