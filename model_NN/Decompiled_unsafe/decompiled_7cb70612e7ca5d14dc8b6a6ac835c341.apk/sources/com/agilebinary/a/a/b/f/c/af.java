package com.agilebinary.a.a.b.f.c;

import com.agilebinary.a.a.b.d;
import com.agilebinary.a.a.b.d.a;
import com.agilebinary.a.a.b.d.b;
import com.agilebinary.a.a.b.d.c;
import com.agilebinary.a.a.b.d.e;
import com.agilebinary.a.a.b.d.k;
import com.agilebinary.a.a.b.h.o;
import com.agilebinary.a.a.b.v;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class af extends y {
    public af() {
        this(null, false);
    }

    public af(String[] strArr, boolean z) {
        super(strArr, z);
        a("domain", new ad());
        a("port", new ae());
        a("commenturl", new ab());
        a("discard", new ac());
        a("version", new ah());
    }

    private List b(d[] dVarArr, e eVar) {
        ArrayList arrayList = new ArrayList(dVarArr.length);
        for (d dVar : dVarArr) {
            String a2 = dVar.a();
            String b = dVar.b();
            if (a2 == null || a2.length() == 0) {
                throw new k("Cookie name may not be empty");
            }
            d dVar2 = new d(a2, b);
            dVar2.e(a(eVar));
            dVar2.d(b(eVar));
            dVar2.a(new int[]{eVar.c()});
            v[] c = dVar.c();
            HashMap hashMap = new HashMap(c.length);
            for (int length = c.length - 1; length >= 0; length--) {
                v vVar = c[length];
                hashMap.put(vVar.a().toLowerCase(Locale.ENGLISH), vVar);
            }
            for (Map.Entry value : hashMap.entrySet()) {
                v vVar2 = (v) value.getValue();
                String lowerCase = vVar2.a().toLowerCase(Locale.ENGLISH);
                dVar2.a(lowerCase, vVar2.b());
                c a3 = a(lowerCase);
                if (a3 != null) {
                    a3.a(dVar2, vVar2.b());
                }
            }
            arrayList.add(dVar2);
        }
        return arrayList;
    }

    private static e c(e eVar) {
        boolean z = false;
        String a2 = eVar.a();
        int i = 0;
        while (true) {
            if (i >= a2.length()) {
                z = true;
                break;
            }
            char charAt = a2.charAt(i);
            if (charAt == '.' || charAt == ':') {
                break;
            }
            i++;
        }
        return z ? new e(a2 + ".local", eVar.c(), eVar.b(), eVar.d()) : eVar;
    }

    public int a() {
        return 1;
    }

    public List a(com.agilebinary.a.a.b.c cVar, e eVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Header may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else if (!cVar.c().equalsIgnoreCase("Set-Cookie2")) {
            throw new k("Unrecognized cookie header '" + cVar.toString() + "'");
        } else {
            return b(cVar.e(), c(eVar));
        }
    }

    /* access modifiers changed from: protected */
    public List a(d[] dVarArr, e eVar) {
        return b(dVarArr, c(eVar));
    }

    public void a(b bVar, e eVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            super.a(bVar, c(eVar));
        }
    }

    /* access modifiers changed from: protected */
    public void a(com.agilebinary.a.a.b.k.b bVar, b bVar2, int i) {
        String a2;
        int[] e;
        super.a(bVar, bVar2, i);
        if ((bVar2 instanceof a) && (a2 = ((a) bVar2).a("port")) != null) {
            bVar.a("; $Port");
            bVar.a("=\"");
            if (a2.trim().length() > 0 && (e = bVar2.e()) != null) {
                int length = e.length;
                for (int i2 = 0; i2 < length; i2++) {
                    if (i2 > 0) {
                        bVar.a(",");
                    }
                    bVar.a(Integer.toString(e[i2]));
                }
            }
            bVar.a("\"");
        }
    }

    public com.agilebinary.a.a.b.c b() {
        com.agilebinary.a.a.b.k.b bVar = new com.agilebinary.a.a.b.k.b(40);
        bVar.a("Cookie2");
        bVar.a(": ");
        bVar.a("$Version=");
        bVar.a(Integer.toString(a()));
        return new o(bVar);
    }

    public boolean b(b bVar, e eVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (eVar != null) {
            return super.b(bVar, c(eVar));
        } else {
            throw new IllegalArgumentException("Cookie origin may not be null");
        }
    }

    public String toString() {
        return "rfc2965";
    }
}
