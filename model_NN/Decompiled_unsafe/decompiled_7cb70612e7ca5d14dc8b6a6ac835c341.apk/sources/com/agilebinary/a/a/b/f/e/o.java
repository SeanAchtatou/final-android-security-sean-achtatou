package com.agilebinary.a.a.b.f.e;

import com.agilebinary.a.a.b.i.d;
import java.net.Socket;

public class o extends d {
    public o(Socket socket, int i, d dVar) {
        int i2 = 1024;
        if (socket == null) {
            throw new IllegalArgumentException("Socket may not be null");
        }
        int sendBufferSize = i < 0 ? socket.getSendBufferSize() : i;
        a(socket.getOutputStream(), sendBufferSize >= 1024 ? sendBufferSize : i2, dVar);
    }
}
