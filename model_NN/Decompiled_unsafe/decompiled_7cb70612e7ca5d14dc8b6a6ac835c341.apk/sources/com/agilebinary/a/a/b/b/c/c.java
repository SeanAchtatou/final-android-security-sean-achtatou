package com.agilebinary.a.a.b.b.c;

import com.agilebinary.a.a.b.c.b.b;
import com.agilebinary.a.a.b.c.m;
import com.agilebinary.a.a.b.j.e;
import com.agilebinary.a.a.b.o;
import com.agilebinary.a.a.b.p;

public class c implements p {
    public void a(o oVar, e eVar) {
        if (oVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (oVar.g().a().equalsIgnoreCase("CONNECT")) {
            oVar.b("Proxy-Connection", "Keep-Alive");
        } else {
            m mVar = (m) eVar.a("http.connection");
            if (mVar == null) {
                throw new IllegalStateException("Client connection not specified in HTTP context");
            }
            b k = mVar.k();
            if ((k.c() == 1 || k.e()) && !oVar.a("Connection")) {
                oVar.a("Connection", "Keep-Alive");
            }
            if (k.c() == 2 && !k.e() && !oVar.a("Proxy-Connection")) {
                oVar.a("Proxy-Connection", "Keep-Alive");
            }
        }
    }
}
