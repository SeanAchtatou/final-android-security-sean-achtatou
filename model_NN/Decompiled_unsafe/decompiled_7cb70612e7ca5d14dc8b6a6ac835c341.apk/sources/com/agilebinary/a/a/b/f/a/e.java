package com.agilebinary.a.a.b.f.a;

import com.agilebinary.a.a.b.a.d;
import com.agilebinary.a.a.b.a.h;
import com.agilebinary.a.a.b.b.f;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class e implements f {

    /* renamed from: a  reason: collision with root package name */
    private final ConcurrentHashMap f38a = new ConcurrentHashMap();

    private static h a(Map map, d dVar) {
        int i;
        h hVar = (h) map.get(dVar);
        if (hVar != null) {
            return hVar;
        }
        int i2 = -1;
        d dVar2 = null;
        for (d dVar3 : map.keySet()) {
            int a2 = dVar.a(dVar3);
            if (a2 > i2) {
                i = a2;
            } else {
                dVar3 = dVar2;
                i = i2;
            }
            i2 = i;
            dVar2 = dVar3;
        }
        return dVar2 != null ? (h) map.get(dVar2) : hVar;
    }

    public h a(d dVar) {
        if (dVar != null) {
            return a(this.f38a, dVar);
        }
        throw new IllegalArgumentException("Authentication scope may not be null");
    }

    public String toString() {
        return this.f38a.toString();
    }
}
