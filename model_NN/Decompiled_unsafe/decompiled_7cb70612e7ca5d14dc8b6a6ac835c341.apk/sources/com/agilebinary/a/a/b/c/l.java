package com.agilebinary.a.a.b.c;

import java.net.ConnectException;

public class l extends ConnectException {

    /* renamed from: a  reason: collision with root package name */
    private final com.agilebinary.a.a.b.l f28a;

    public l(com.agilebinary.a.a.b.l lVar, ConnectException connectException) {
        super("Connection to " + lVar + " refused");
        this.f28a = lVar;
        initCause(connectException);
    }
}
