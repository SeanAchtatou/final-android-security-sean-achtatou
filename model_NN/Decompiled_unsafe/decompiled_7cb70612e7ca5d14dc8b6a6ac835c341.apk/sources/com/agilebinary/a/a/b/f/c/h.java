package com.agilebinary.a.a.b.f.c;

import com.agilebinary.a.a.b.d.k;
import com.agilebinary.a.a.b.d.l;
import java.util.Date;

public class h extends a {
    public void a(l lVar, String str) {
        if (lVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (str == null) {
            throw new k("Missing value for max-age attribute");
        } else {
            try {
                int parseInt = Integer.parseInt(str);
                if (parseInt < 0) {
                    throw new k("Negative max-age attribute: " + str);
                }
                lVar.b(new Date(System.currentTimeMillis() + (((long) parseInt) * 1000)));
            } catch (NumberFormatException e) {
                throw new k("Invalid max-age attribute: " + str);
            }
        }
    }
}
