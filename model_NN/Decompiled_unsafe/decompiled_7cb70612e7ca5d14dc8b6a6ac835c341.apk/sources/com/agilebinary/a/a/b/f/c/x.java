package com.agilebinary.a.a.b.f.c;

import com.agilebinary.a.a.b.d.b;
import com.agilebinary.a.a.b.d.c;
import com.agilebinary.a.a.b.d.e;
import com.agilebinary.a.a.b.d.g;
import com.agilebinary.a.a.b.d.k;
import com.agilebinary.a.a.b.d.l;
import java.util.Locale;

public class x implements c {
    public void a(b bVar, e eVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            String a2 = eVar.a();
            String c = bVar.c();
            if (c == null) {
                throw new g("Cookie domain may not be null");
            } else if (c.equals(a2)) {
            } else {
                if (c.indexOf(46) == -1) {
                    throw new g("Domain attribute \"" + c + "\" does not match the host \"" + a2 + "\"");
                } else if (!c.startsWith(".")) {
                    throw new g("Domain attribute \"" + c + "\" violates RFC 2109: domain must start with a dot");
                } else {
                    int indexOf = c.indexOf(46, 1);
                    if (indexOf < 0 || indexOf == c.length() - 1) {
                        throw new g("Domain attribute \"" + c + "\" violates RFC 2109: domain must contain an embedded dot");
                    }
                    String lowerCase = a2.toLowerCase(Locale.ENGLISH);
                    if (!lowerCase.endsWith(c)) {
                        throw new g("Illegal domain attribute \"" + c + "\". Domain of origin: \"" + lowerCase + "\"");
                    } else if (lowerCase.substring(0, lowerCase.length() - c.length()).indexOf(46) != -1) {
                        throw new g("Domain attribute \"" + c + "\" violates RFC 2109: host minus domain may not contain any dots");
                    }
                }
            }
        }
    }

    public void a(l lVar, String str) {
        if (lVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (str == null) {
            throw new k("Missing value for domain attribute");
        } else if (str.trim().length() == 0) {
            throw new k("Blank value for domain attribute");
        } else {
            lVar.d(str);
        }
    }

    public boolean b(b bVar, e eVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            String a2 = eVar.a();
            String c = bVar.c();
            if (c == null) {
                return false;
            }
            return a2.equals(c) || (c.startsWith(".") && a2.endsWith(c));
        }
    }
}
