package com.agilebinary.a.a.b.f.b;

import com.agilebinary.a.a.a.a.a;
import com.agilebinary.a.a.b.c.c.b;
import com.agilebinary.a.a.b.c.c.g;
import com.agilebinary.a.a.b.c.d;
import com.agilebinary.a.a.b.c.n;
import com.agilebinary.a.a.b.i.c;
import com.agilebinary.a.a.b.j.e;
import com.agilebinary.a.a.b.l;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.Socket;
import org.apache.commons.logging.Log;

public class f implements d {

    /* renamed from: a  reason: collision with root package name */
    protected final g f58a;
    private final Log b = a.a(getClass());

    public f(g gVar) {
        if (gVar == null) {
            throw new IllegalArgumentException("Scheme registry amy not be null");
        }
        this.f58a = gVar;
    }

    public n a() {
        return new e();
    }

    public void a(n nVar, l lVar, e eVar, com.agilebinary.a.a.b.i.d dVar) {
        if (nVar == null) {
            throw new IllegalArgumentException("Connection may not be null");
        } else if (lVar == null) {
            throw new IllegalArgumentException("Target host may not be null");
        } else if (dVar == null) {
            throw new IllegalArgumentException("Parameters may not be null");
        } else if (!nVar.d()) {
            throw new IllegalStateException("Connection must be open");
        } else {
            com.agilebinary.a.a.b.c.c.f a2 = this.f58a.a(lVar.c());
            if (!(a2.b() instanceof b)) {
                throw new IllegalArgumentException("Target scheme (" + a2.c() + ") must have layered socket factory.");
            }
            b bVar = (b) a2.b();
            try {
                Socket b2 = bVar.b(nVar.j(), lVar.a(), lVar.b(), true);
                a(b2, eVar, dVar);
                nVar.a(b2, lVar, bVar.a(b2), dVar);
            } catch (ConnectException e) {
                throw new com.agilebinary.a.a.b.c.l(lVar, e);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x00c2  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00e6 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.agilebinary.a.a.b.c.n r14, com.agilebinary.a.a.b.l r15, java.net.InetAddress r16, com.agilebinary.a.a.b.j.e r17, com.agilebinary.a.a.b.i.d r18) {
        /*
            r13 = this;
            if (r14 != 0) goto L_0x000a
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r3 = "Connection may not be null"
            r2.<init>(r3)
            throw r2
        L_0x000a:
            if (r15 != 0) goto L_0x0014
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r3 = "Target host may not be null"
            r2.<init>(r3)
            throw r2
        L_0x0014:
            if (r18 != 0) goto L_0x001e
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            java.lang.String r3 = "Parameters may not be null"
            r2.<init>(r3)
            throw r2
        L_0x001e:
            boolean r2 = r14.d()
            if (r2 == 0) goto L_0x002c
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.String r3 = "Connection must not be open"
            r2.<init>(r3)
            throw r2
        L_0x002c:
            com.agilebinary.a.a.b.c.c.g r2 = r13.f58a
            java.lang.String r3 = r15.c()
            com.agilebinary.a.a.b.c.c.f r2 = r2.a(r3)
            com.agilebinary.a.a.b.c.c.h r6 = r2.b()
            java.lang.String r3 = r15.a()
            java.net.InetAddress[] r7 = r13.a(r3)
            int r3 = r15.b()
            int r8 = r2.a(r3)
            r2 = 0
        L_0x004b:
            int r3 = r7.length
            if (r2 >= r3) goto L_0x00aa
            r4 = r7[r2]
            int r3 = r7.length
            int r3 = r3 + -1
            if (r2 != r3) goto L_0x00ab
            r3 = 1
        L_0x0056:
            r0 = r18
            java.net.Socket r5 = r6.a(r0)
            r14.a(r5, r15)
            java.net.InetSocketAddress r9 = new java.net.InetSocketAddress
            r9.<init>(r4, r8)
            r4 = 0
            if (r16 == 0) goto L_0x006f
            java.net.InetSocketAddress r4 = new java.net.InetSocketAddress
            r10 = 0
            r0 = r16
            r4.<init>(r0, r10)
        L_0x006f:
            org.apache.commons.logging.Log r10 = r13.b
            boolean r10 = r10.isDebugEnabled()
            if (r10 == 0) goto L_0x008f
            org.apache.commons.logging.Log r10 = r13.b
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "Connecting to "
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r11 = r11.append(r9)
            java.lang.String r11 = r11.toString()
            r10.debug(r11)
        L_0x008f:
            r0 = r18
            java.net.Socket r4 = r6.a(r5, r9, r4, r0)     // Catch:{ ConnectException -> 0x00ad, f -> 0x00b6 }
            if (r5 == r4) goto L_0x00ea
            r14.a(r4, r15)     // Catch:{ ConnectException -> 0x00ad, f -> 0x00b6 }
        L_0x009a:
            r0 = r17
            r1 = r18
            r13.a(r4, r0, r1)     // Catch:{ ConnectException -> 0x00ad, f -> 0x00b6 }
            boolean r4 = r6.a(r4)     // Catch:{ ConnectException -> 0x00ad, f -> 0x00b6 }
            r0 = r18
            r14.a(r4, r0)     // Catch:{ ConnectException -> 0x00ad, f -> 0x00b6 }
        L_0x00aa:
            return
        L_0x00ab:
            r3 = 0
            goto L_0x0056
        L_0x00ad:
            r4 = move-exception
            if (r3 == 0) goto L_0x00ba
            com.agilebinary.a.a.b.c.l r2 = new com.agilebinary.a.a.b.c.l
            r2.<init>(r15, r4)
            throw r2
        L_0x00b6:
            r4 = move-exception
            if (r3 == 0) goto L_0x00ba
            throw r4
        L_0x00ba:
            org.apache.commons.logging.Log r3 = r13.b
            boolean r3 = r3.isDebugEnabled()
            if (r3 == 0) goto L_0x00e6
            org.apache.commons.logging.Log r3 = r13.b
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Connect to "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r9)
            java.lang.String r5 = " timed out. "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = "Connection will be retried using another IP address"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            r3.debug(r4)
        L_0x00e6:
            int r2 = r2 + 1
            goto L_0x004b
        L_0x00ea:
            r4 = r5
            goto L_0x009a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.b.f.b.f.a(com.agilebinary.a.a.b.c.n, com.agilebinary.a.a.b.l, java.net.InetAddress, com.agilebinary.a.a.b.j.e, com.agilebinary.a.a.b.i.d):void");
    }

    /* access modifiers changed from: protected */
    public void a(Socket socket, e eVar, com.agilebinary.a.a.b.i.d dVar) {
        socket.setTcpNoDelay(c.c(dVar));
        socket.setSoTimeout(c.a(dVar));
        int e = c.e(dVar);
        if (e >= 0) {
            socket.setSoLinger(e > 0, e);
        }
    }

    /* access modifiers changed from: protected */
    public InetAddress[] a(String str) {
        return InetAddress.getAllByName(str);
    }
}
