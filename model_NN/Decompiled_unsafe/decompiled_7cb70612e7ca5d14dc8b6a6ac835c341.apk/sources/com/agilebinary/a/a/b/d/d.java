package com.agilebinary.a.a.b.d;

import java.io.Serializable;
import java.util.Comparator;

public class d implements Serializable, Comparator {
    /* renamed from: a */
    public int compare(b bVar, b bVar2) {
        int compareTo = bVar.a().compareTo(bVar2.a());
        if (compareTo == 0) {
            String c = bVar.c();
            if (c == null) {
                c = "";
            } else if (c.indexOf(46) == -1) {
                c = c + ".local";
            }
            String c2 = bVar2.c();
            if (c2 == null) {
                c2 = "";
            } else if (c2.indexOf(46) == -1) {
                c2 = c2 + ".local";
            }
            compareTo = c.compareToIgnoreCase(c2);
        }
        if (compareTo != 0) {
            return compareTo;
        }
        String d = bVar.d();
        if (d == null) {
            d = "/";
        }
        String d2 = bVar2.d();
        if (d2 == null) {
            d2 = "/";
        }
        return d.compareTo(d2);
    }
}
