package com.agilebinary.a.a.b.h;

import com.agilebinary.a.a.b.c;
import com.agilebinary.a.a.b.e;
import com.agilebinary.a.a.b.f;
import com.agilebinary.a.a.b.k.b;
import java.util.NoSuchElementException;

public class d implements e {

    /* renamed from: a  reason: collision with root package name */
    private final f f99a;
    private final q b;
    private com.agilebinary.a.a.b.d c;
    private b d;
    private t e;

    public d(f fVar) {
        this(fVar, e.f100a);
    }

    public d(f fVar, q qVar) {
        this.c = null;
        this.d = null;
        this.e = null;
        if (fVar == null) {
            throw new IllegalArgumentException("Header iterator may not be null");
        } else if (qVar == null) {
            throw new IllegalArgumentException("Parser may not be null");
        } else {
            this.f99a = fVar;
            this.b = qVar;
        }
    }

    private void b() {
        this.e = null;
        this.d = null;
        while (this.f99a.hasNext()) {
            c a2 = this.f99a.a();
            if (a2 instanceof com.agilebinary.a.a.b.b) {
                this.d = ((com.agilebinary.a.a.b.b) a2).a();
                this.e = new t(0, this.d.c());
                this.e.a(((com.agilebinary.a.a.b.b) a2).b());
                return;
            }
            String d2 = a2.d();
            if (d2 != null) {
                this.d = new b(d2.length());
                this.d.a(d2);
                this.e = new t(0, this.d.c());
                return;
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0028  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void c() {
        /*
            r4 = this;
            r3 = 0
        L_0x0001:
            com.agilebinary.a.a.b.f r0 = r4.f99a
            boolean r0 = r0.hasNext()
            if (r0 != 0) goto L_0x000d
            com.agilebinary.a.a.b.h.t r0 = r4.e
            if (r0 == 0) goto L_0x0044
        L_0x000d:
            com.agilebinary.a.a.b.h.t r0 = r4.e
            if (r0 == 0) goto L_0x0019
            com.agilebinary.a.a.b.h.t r0 = r4.e
            boolean r0 = r0.c()
            if (r0 == 0) goto L_0x001c
        L_0x0019:
            r4.b()
        L_0x001c:
            com.agilebinary.a.a.b.h.t r0 = r4.e
            if (r0 == 0) goto L_0x0001
        L_0x0020:
            com.agilebinary.a.a.b.h.t r0 = r4.e
            boolean r0 = r0.c()
            if (r0 != 0) goto L_0x0045
            com.agilebinary.a.a.b.h.q r0 = r4.b
            com.agilebinary.a.a.b.k.b r1 = r4.d
            com.agilebinary.a.a.b.h.t r2 = r4.e
            com.agilebinary.a.a.b.d r0 = r0.b(r1, r2)
            java.lang.String r1 = r0.a()
            int r1 = r1.length()
            if (r1 != 0) goto L_0x0042
            java.lang.String r1 = r0.b()
            if (r1 == 0) goto L_0x0020
        L_0x0042:
            r4.c = r0
        L_0x0044:
            return
        L_0x0045:
            com.agilebinary.a.a.b.h.t r0 = r4.e
            boolean r0 = r0.c()
            if (r0 == 0) goto L_0x0001
            r4.e = r3
            r4.d = r3
            goto L_0x0001
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.b.h.d.c():void");
    }

    public com.agilebinary.a.a.b.d a() {
        if (this.c == null) {
            c();
        }
        if (this.c == null) {
            throw new NoSuchElementException("No more header elements available");
        }
        com.agilebinary.a.a.b.d dVar = this.c;
        this.c = null;
        return dVar;
    }

    public boolean hasNext() {
        if (this.c == null) {
            c();
        }
        return this.c != null;
    }

    public final Object next() {
        return a();
    }

    public void remove() {
        throw new UnsupportedOperationException("Remove not supported");
    }
}
