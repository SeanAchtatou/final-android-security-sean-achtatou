package com.agilebinary.a.a.b.f.d;

import com.agilebinary.a.a.b.c;
import com.agilebinary.a.a.b.e.b;
import com.agilebinary.a.a.b.e.e;
import com.agilebinary.a.a.b.f.e.g;
import com.agilebinary.a.a.b.f.e.l;
import com.agilebinary.a.a.b.g.f;
import com.agilebinary.a.a.b.i;
import com.agilebinary.a.a.b.n;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private final e f80a;

    public a(e eVar) {
        if (eVar == null) {
            throw new IllegalArgumentException("Content length strategy may not be null");
        }
        this.f80a = eVar;
    }

    /* access modifiers changed from: protected */
    public b a(f fVar, n nVar) {
        b bVar = new b();
        long a2 = this.f80a.a(nVar);
        if (a2 == -2) {
            bVar.a(true);
            bVar.a(-1);
            bVar.a(new com.agilebinary.a.a.b.f.e.e(fVar));
        } else if (a2 == -1) {
            bVar.a(false);
            bVar.a(-1);
            bVar.a(new l(fVar));
        } else {
            bVar.a(false);
            bVar.a(a2);
            bVar.a(new g(fVar, a2));
        }
        c c = nVar.c("Content-Type");
        if (c != null) {
            bVar.a(c);
        }
        c c2 = nVar.c("Content-Encoding");
        if (c2 != null) {
            bVar.b(c2);
        }
        return bVar;
    }

    public i b(f fVar, n nVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("Session input buffer may not be null");
        } else if (nVar != null) {
            return a(fVar, nVar);
        } else {
            throw new IllegalArgumentException("HTTP message may not be null");
        }
    }
}
