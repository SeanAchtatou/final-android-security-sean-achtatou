package com.agilebinary.a.a.b.b.c;

import com.agilebinary.a.a.b.c;
import com.agilebinary.a.a.b.j.e;
import com.agilebinary.a.a.b.o;
import com.agilebinary.a.a.b.p;
import java.util.Collection;

public class d implements p {
    public void a(o oVar, e eVar) {
        Collection<c> collection;
        if (oVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (!oVar.g().a().equalsIgnoreCase("CONNECT") && (collection = (Collection) oVar.f().a("http.default-headers")) != null) {
            for (c a2 : collection) {
                oVar.a(a2);
            }
        }
    }
}
