package com.agilebinary.a.a.b;

import com.agilebinary.a.a.b.k.b;
import com.agilebinary.a.a.b.k.e;
import java.io.Serializable;
import java.util.Locale;

public final class l implements Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    protected final String f120a;
    protected final String b;
    protected final int c;
    protected final String d;

    public l(String str, int i) {
        this(str, i, null);
    }

    public l(String str, int i, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Host name may not be null");
        }
        this.f120a = str;
        this.b = str.toLowerCase(Locale.ENGLISH);
        if (str2 != null) {
            this.d = str2.toLowerCase(Locale.ENGLISH);
        } else {
            this.d = "http";
        }
        this.c = i;
    }

    public String a() {
        return this.f120a;
    }

    public int b() {
        return this.c;
    }

    public String c() {
        return this.d;
    }

    public Object clone() {
        return super.clone();
    }

    public String d() {
        b bVar = new b(32);
        bVar.a(this.d);
        bVar.a("://");
        bVar.a(this.f120a);
        if (this.c != -1) {
            bVar.a(':');
            bVar.a(Integer.toString(this.c));
        }
        return bVar.toString();
    }

    public String e() {
        if (this.c == -1) {
            return this.f120a;
        }
        b bVar = new b(this.f120a.length() + 6);
        bVar.a(this.f120a);
        bVar.a(":");
        bVar.a(Integer.toString(this.c));
        return bVar.toString();
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof l)) {
            return false;
        }
        l lVar = (l) obj;
        if (!this.b.equals(lVar.b) || this.c != lVar.c || !this.d.equals(lVar.d)) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return e.a(e.a(e.a(17, this.b), this.c), this.d);
    }

    public String toString() {
        return d();
    }
}
