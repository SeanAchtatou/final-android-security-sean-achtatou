package com.agilebinary.a.a.b.b.a;

import com.agilebinary.a.a.b.ab;
import com.agilebinary.a.a.b.c.e;
import com.agilebinary.a.a.b.c.i;
import com.agilebinary.a.a.b.h.a;
import com.agilebinary.a.a.b.h.l;
import com.agilebinary.a.a.b.h.p;
import com.agilebinary.a.a.b.i.d;
import com.agilebinary.a.a.b.z;
import java.io.IOException;
import java.net.URI;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public abstract class f extends a implements a, g, Cloneable {
    private Lock c = new ReentrantLock();
    private boolean d;
    private URI e;
    private e f;
    private i g;

    public void a(e eVar) {
        this.c.lock();
        try {
            if (this.d) {
                throw new IOException("Request already aborted");
            }
            this.g = null;
            this.f = eVar;
        } finally {
            this.c.unlock();
        }
    }

    public void a(i iVar) {
        this.c.lock();
        try {
            if (this.d) {
                throw new IOException("Request already aborted");
            }
            this.f = null;
            this.g = iVar;
        } finally {
            this.c.unlock();
        }
    }

    public void a(URI uri) {
        this.e = uri;
    }

    public z c() {
        return com.agilebinary.a.a.b.i.e.b(f());
    }

    public Object clone() {
        f fVar = (f) super.clone();
        fVar.c = new ReentrantLock();
        fVar.d = false;
        fVar.g = null;
        fVar.f = null;
        fVar.f96a = (p) com.agilebinary.a.a.b.b.d.a.a(this.f96a);
        fVar.b = (d) com.agilebinary.a.a.b.b.d.a.a(this.b);
        return fVar;
    }

    public abstract String d_();

    public ab g() {
        String d_ = d_();
        z c2 = c();
        URI h = h();
        String str = null;
        if (h != null) {
            str = h.toASCIIString();
        }
        if (str == null || str.length() == 0) {
            str = "/";
        }
        return new l(d_, str, c2);
    }

    public URI h() {
        return this.e;
    }

    public void i() {
        this.c.lock();
        try {
            if (!this.d) {
                this.d = true;
                e eVar = this.f;
                i iVar = this.g;
                this.c.unlock();
                if (eVar != null) {
                    eVar.a();
                }
                if (iVar != null) {
                    try {
                        iVar.i();
                    } catch (IOException e2) {
                    }
                }
            }
        } finally {
            this.c.unlock();
        }
    }
}
