package com.agilebinary.a.a.b.f.a;

import com.agilebinary.a.a.b.b.m;
import com.agilebinary.a.a.b.c.b;
import com.agilebinary.a.a.b.d.j;
import com.agilebinary.a.a.b.f.c.ag;
import com.agilebinary.a.a.b.f.c.l;
import com.agilebinary.a.a.b.f.c.n;
import com.agilebinary.a.a.b.f.c.w;
import com.agilebinary.a.a.b.f.c.z;
import com.agilebinary.a.a.b.i.c;
import com.agilebinary.a.a.b.i.d;
import com.agilebinary.a.a.b.i.e;
import com.agilebinary.a.a.b.i.f;
import com.agilebinary.a.a.b.j.a;
import com.agilebinary.a.a.b.j.g;
import com.agilebinary.a.a.b.j.i;
import com.agilebinary.a.a.b.j.k;
import com.agilebinary.a.a.b.t;

public class h extends b {
    public h() {
        super(null, null);
    }

    public h(b bVar, d dVar) {
        super(bVar, dVar);
        a(new k());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.b.i.c.a(com.agilebinary.a.a.b.i.d, boolean):void
     arg types: [com.agilebinary.a.a.b.i.f, int]
     candidates:
      com.agilebinary.a.a.b.i.c.a(com.agilebinary.a.a.b.i.d, int):void
      com.agilebinary.a.a.b.i.c.a(com.agilebinary.a.a.b.i.d, boolean):void */
    /* access modifiers changed from: protected */
    public d a() {
        f fVar = new f();
        e.a(fVar, t.c);
        e.a(fVar, "ISO-8859-1");
        c.a((d) fVar, true);
        c.a(fVar, 8192);
        com.agilebinary.a.a.b.k.f a2 = com.agilebinary.a.a.b.k.f.a("org.apache.http.client", getClass().getClassLoader());
        e.b(fVar, "Apache-HttpClient/" + (a2 != null ? a2.a() : "UNAVAILABLE") + " (java 1.5)");
        return fVar;
    }

    /* access modifiers changed from: protected */
    public com.agilebinary.a.a.b.j.e b() {
        a aVar = new a();
        aVar.a("http.scheme-registry", r().a());
        aVar.a("http.authscheme-registry", t());
        aVar.a("http.cookiespec-registry", u());
        aVar.a("http.cookie-store", B());
        aVar.a("http.auth.credentials-provider", C());
        return aVar;
    }

    /* access modifiers changed from: protected */
    public g c() {
        return new g();
    }

    /* JADX WARN: Type inference failed for: r2v8, types: [java.lang.Object] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.agilebinary.a.a.b.c.b d() {
        /*
            r6 = this;
            com.agilebinary.a.a.b.c.c.g r3 = new com.agilebinary.a.a.b.c.c.g
            r3.<init>()
            com.agilebinary.a.a.b.c.c.f r1 = new com.agilebinary.a.a.b.c.c.f
            java.lang.String r2 = "http"
            r4 = 80
            com.agilebinary.a.a.b.c.c.e r5 = com.agilebinary.a.a.b.c.c.e.b()
            r1.<init>(r2, r4, r5)
            r3.a(r1)
            com.agilebinary.a.a.b.c.c.f r1 = new com.agilebinary.a.a.b.c.c.f
            java.lang.String r2 = "https"
            r4 = 443(0x1bb, float:6.21E-43)
            com.agilebinary.a.a.b.c.d.d r5 = com.agilebinary.a.a.b.c.d.d.b()
            r1.<init>(r2, r4, r5)
            r3.a(r1)
            com.agilebinary.a.a.b.i.d r4 = r6.q()
            r2 = 0
            java.lang.String r1 = "http.connection-manager.factory-class-name"
            java.lang.Object r1 = r4.a(r1)
            java.lang.String r1 = (java.lang.String) r1
            if (r1 == 0) goto L_0x007d
            java.lang.Class r2 = java.lang.Class.forName(r1)     // Catch:{ ClassNotFoundException -> 0x0047, IllegalAccessException -> 0x0061, InstantiationException -> 0x006c }
            java.lang.Object r2 = r2.newInstance()     // Catch:{ ClassNotFoundException -> 0x0047, IllegalAccessException -> 0x0061, InstantiationException -> 0x006c }
            r0 = r2
            com.agilebinary.a.a.b.c.c r0 = (com.agilebinary.a.a.b.c.c) r0     // Catch:{ ClassNotFoundException -> 0x0047, IllegalAccessException -> 0x0061, InstantiationException -> 0x006c }
            r1 = r0
        L_0x0040:
            if (r1 == 0) goto L_0x0077
            com.agilebinary.a.a.b.c.b r1 = r1.a(r4, r3)
        L_0x0046:
            return r1
        L_0x0047:
            r2 = move-exception
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Invalid class name: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r1 = r3.append(r1)
            java.lang.String r1 = r1.toString()
            r2.<init>(r1)
            throw r2
        L_0x0061:
            r1 = move-exception
            java.lang.IllegalAccessError r2 = new java.lang.IllegalAccessError
            java.lang.String r1 = r1.getMessage()
            r2.<init>(r1)
            throw r2
        L_0x006c:
            r1 = move-exception
            java.lang.InstantiationError r2 = new java.lang.InstantiationError
            java.lang.String r1 = r1.getMessage()
            r2.<init>(r1)
            throw r2
        L_0x0077:
            com.agilebinary.a.a.b.f.b.m r1 = new com.agilebinary.a.a.b.f.b.m
            r1.<init>(r3)
            goto L_0x0046
        L_0x007d:
            r1 = r2
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.b.f.a.h.d():com.agilebinary.a.a.b.c.b");
    }

    /* access modifiers changed from: protected */
    public com.agilebinary.a.a.b.a.c e() {
        return new com.agilebinary.a.a.b.a.c();
    }

    /* access modifiers changed from: protected */
    public j f() {
        j jVar = new j();
        jVar.a("best-match", new l());
        jVar.a("compatibility", new n());
        jVar.a("netscape", new w());
        jVar.a("rfc2109", new z());
        jVar.a("rfc2965", new ag());
        return jVar;
    }

    /* access modifiers changed from: protected */
    public com.agilebinary.a.a.b.a g() {
        return new com.agilebinary.a.a.b.f.b();
    }

    /* access modifiers changed from: protected */
    public com.agilebinary.a.a.b.c.g h() {
        return new g();
    }

    /* access modifiers changed from: protected */
    public com.agilebinary.a.a.b.j.b i() {
        com.agilebinary.a.a.b.j.b bVar = new com.agilebinary.a.a.b.j.b();
        bVar.b(new com.agilebinary.a.a.b.b.c.d());
        bVar.b(new i());
        bVar.b(new k());
        bVar.b(new com.agilebinary.a.a.b.b.c.c());
        bVar.b(new com.agilebinary.a.a.b.j.l());
        bVar.b(new com.agilebinary.a.a.b.j.j());
        bVar.b(new com.agilebinary.a.a.b.b.c.a());
        bVar.b(new com.agilebinary.a.a.b.b.c.h());
        bVar.b(new com.agilebinary.a.a.b.b.c.b());
        bVar.b(new com.agilebinary.a.a.b.b.c.g());
        bVar.b(new com.agilebinary.a.a.b.b.c.f());
        bVar.b(new com.agilebinary.a.a.b.b.c.e());
        return bVar;
    }

    /* access modifiers changed from: protected */
    public com.agilebinary.a.a.b.b.g j() {
        return new i();
    }

    /* access modifiers changed from: protected */
    public com.agilebinary.a.a.b.b.b k() {
        return new m();
    }

    /* access modifiers changed from: protected */
    public com.agilebinary.a.a.b.b.b l() {
        return new j();
    }

    /* access modifiers changed from: protected */
    public com.agilebinary.a.a.b.b.e m() {
        return new d();
    }

    /* access modifiers changed from: protected */
    public com.agilebinary.a.a.b.b.f n() {
        return new e();
    }

    /* access modifiers changed from: protected */
    public com.agilebinary.a.a.b.c.b.d o() {
        return new com.agilebinary.a.a.b.f.b.g(r().a());
    }

    /* access modifiers changed from: protected */
    public m p() {
        return new n();
    }
}
