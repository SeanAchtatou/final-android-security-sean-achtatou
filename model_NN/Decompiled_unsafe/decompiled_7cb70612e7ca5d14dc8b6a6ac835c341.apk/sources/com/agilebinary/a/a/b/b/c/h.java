package com.agilebinary.a.a.b.b.c;

import com.agilebinary.a.a.a.a.a;
import com.agilebinary.a.a.b.c;
import com.agilebinary.a.a.b.d.b;
import com.agilebinary.a.a.b.d.e;
import com.agilebinary.a.a.b.d.k;
import com.agilebinary.a.a.b.f;
import com.agilebinary.a.a.b.q;
import com.agilebinary.a.a.b.s;
import org.apache.commons.logging.Log;

public class h implements s {

    /* renamed from: a  reason: collision with root package name */
    private final Log f11a = a.a(getClass());

    private void a(f fVar, com.agilebinary.a.a.b.d.h hVar, e eVar, com.agilebinary.a.a.b.b.e eVar2) {
        while (fVar.hasNext()) {
            c a2 = fVar.a();
            try {
                for (b bVar : hVar.a(a2, eVar)) {
                    try {
                        hVar.a(bVar, eVar);
                        eVar2.a(bVar);
                        if (this.f11a.isDebugEnabled()) {
                            this.f11a.debug("Cookie accepted: \"" + bVar + "\". ");
                        }
                    } catch (k e) {
                        if (this.f11a.isWarnEnabled()) {
                            this.f11a.warn("Cookie rejected: \"" + bVar + "\". " + e.getMessage());
                        }
                    }
                }
            } catch (k e2) {
                if (this.f11a.isWarnEnabled()) {
                    this.f11a.warn("Invalid cookie header: \"" + a2 + "\". " + e2.getMessage());
                }
            }
        }
    }

    public void a(q qVar, com.agilebinary.a.a.b.j.e eVar) {
        if (qVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else {
            com.agilebinary.a.a.b.d.h hVar = (com.agilebinary.a.a.b.d.h) eVar.a("http.cookie-spec");
            if (hVar != null) {
                com.agilebinary.a.a.b.b.e eVar2 = (com.agilebinary.a.a.b.b.e) eVar.a("http.cookie-store");
                if (eVar2 == null) {
                    this.f11a.info("CookieStore not available in HTTP context");
                    return;
                }
                e eVar3 = (e) eVar.a("http.cookie-origin");
                if (eVar3 == null) {
                    this.f11a.info("CookieOrigin not available in HTTP context");
                    return;
                }
                a(qVar.d("Set-Cookie"), hVar, eVar3, eVar2);
                if (hVar.a() > 0) {
                    a(qVar.d("Set-Cookie2"), hVar, eVar3, eVar2);
                }
            }
        }
    }
}
