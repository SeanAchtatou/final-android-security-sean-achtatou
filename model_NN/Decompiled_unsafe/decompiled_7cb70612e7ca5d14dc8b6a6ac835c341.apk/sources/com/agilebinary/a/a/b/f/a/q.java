package com.agilebinary.a.a.b.f.a;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class q {

    /* renamed from: a  reason: collision with root package name */
    private final Set f44a = new HashSet();
    private final List b = new ArrayList();

    public boolean a(URI uri) {
        return this.f44a.contains(uri);
    }

    public void b(URI uri) {
        this.f44a.add(uri);
        this.b.add(uri);
    }
}
