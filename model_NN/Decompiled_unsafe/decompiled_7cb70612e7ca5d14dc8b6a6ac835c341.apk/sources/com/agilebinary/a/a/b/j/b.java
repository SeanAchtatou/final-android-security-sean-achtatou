package com.agilebinary.a.a.b.j;

import com.agilebinary.a.a.b.o;
import com.agilebinary.a.a.b.p;
import com.agilebinary.a.a.b.q;
import com.agilebinary.a.a.b.s;
import java.util.ArrayList;
import java.util.List;

public final class b implements f, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    protected final List f113a = new ArrayList();
    protected final List b = new ArrayList();

    public int a() {
        return this.f113a.size();
    }

    public p a(int i) {
        if (i < 0 || i >= this.f113a.size()) {
            return null;
        }
        return (p) this.f113a.get(i);
    }

    /* access modifiers changed from: protected */
    public void a(b bVar) {
        bVar.f113a.clear();
        bVar.f113a.addAll(this.f113a);
        bVar.b.clear();
        bVar.b.addAll(this.b);
    }

    public void a(o oVar, e eVar) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.f113a.size()) {
                ((p) this.f113a.get(i2)).a(oVar, eVar);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void a(p pVar) {
        if (pVar != null) {
            this.f113a.add(pVar);
        }
    }

    public void a(q qVar, e eVar) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.b.size()) {
                ((s) this.b.get(i2)).a(qVar, eVar);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void a(s sVar) {
        if (sVar != null) {
            this.b.add(sVar);
        }
    }

    public int b() {
        return this.b.size();
    }

    public s b(int i) {
        if (i < 0 || i >= this.b.size()) {
            return null;
        }
        return (s) this.b.get(i);
    }

    public final void b(p pVar) {
        a(pVar);
    }

    public final void b(s sVar) {
        a(sVar);
    }

    public Object clone() {
        b bVar = (b) super.clone();
        a(bVar);
        return bVar;
    }
}
