package com.agilebinary.a.a.b.f.c;

import com.agilebinary.a.a.b.d.c;
import com.agilebinary.a.a.b.d.h;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public abstract class b implements h {

    /* renamed from: a  reason: collision with root package name */
    private final Map f68a = new HashMap(10);

    /* access modifiers changed from: protected */
    public c a(String str) {
        return (c) this.f68a.get(str);
    }

    public void a(String str, c cVar) {
        if (str == null) {
            throw new IllegalArgumentException("Attribute name may not be null");
        } else if (cVar == null) {
            throw new IllegalArgumentException("Attribute handler may not be null");
        } else {
            this.f68a.put(str, cVar);
        }
    }

    /* access modifiers changed from: protected */
    public Collection c() {
        return this.f68a.values();
    }
}
