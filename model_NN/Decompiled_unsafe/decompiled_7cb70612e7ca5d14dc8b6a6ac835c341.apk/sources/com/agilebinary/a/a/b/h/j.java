package com.agilebinary.a.a.b.h;

import com.agilebinary.a.a.b.c;
import com.agilebinary.a.a.b.f;
import java.util.List;
import java.util.NoSuchElementException;

public class j implements f {

    /* renamed from: a  reason: collision with root package name */
    protected final List f103a;
    protected int b;
    protected int c;
    protected String d;

    public j(List list, String str) {
        if (list == null) {
            throw new IllegalArgumentException("Header list must not be null.");
        }
        this.f103a = list;
        this.d = str;
        this.b = a(-1);
        this.c = -1;
    }

    /* access modifiers changed from: protected */
    public int a(int i) {
        if (i < -1) {
            return -1;
        }
        int size = this.f103a.size() - 1;
        boolean z = false;
        int i2 = i;
        while (!z && i2 < size) {
            int i3 = i2 + 1;
            z = b(i3);
            i2 = i3;
        }
        if (!z) {
            i2 = -1;
        }
        return i2;
    }

    public c a() {
        int i = this.b;
        if (i < 0) {
            throw new NoSuchElementException("Iteration already finished.");
        }
        this.c = i;
        this.b = a(i);
        return (c) this.f103a.get(i);
    }

    /* access modifiers changed from: protected */
    public boolean b(int i) {
        if (this.d == null) {
            return true;
        }
        return this.d.equalsIgnoreCase(((c) this.f103a.get(i)).c());
    }

    public boolean hasNext() {
        return this.b >= 0;
    }

    public final Object next() {
        return a();
    }

    public void remove() {
        if (this.c < 0) {
            throw new IllegalStateException("No header to remove.");
        }
        this.f103a.remove(this.c);
        this.c = -1;
        this.b--;
    }
}
