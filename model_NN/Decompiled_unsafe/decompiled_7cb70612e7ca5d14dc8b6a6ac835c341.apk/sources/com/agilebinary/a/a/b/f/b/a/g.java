package com.agilebinary.a.a.b.f.b.a;

import com.agilebinary.a.a.b.c.a.a;
import com.agilebinary.a.a.b.c.b.b;
import java.util.LinkedList;
import java.util.Queue;
import org.apache.commons.logging.Log;

public class g {

    /* renamed from: a  reason: collision with root package name */
    protected final b f50a;
    @Deprecated
    protected final int b;
    protected final a c;
    protected final LinkedList d;
    protected final Queue e;
    protected int f;
    private final Log g = com.agilebinary.a.a.a.a.a.a(getClass());

    public g(b bVar, a aVar) {
        this.f50a = bVar;
        this.c = aVar;
        this.b = aVar.a(bVar);
        this.d = new LinkedList();
        this.e = new LinkedList();
        this.f = 0;
    }

    public final b a() {
        return this.f50a;
    }

    /* JADX WARNING: Removed duplicated region for block: B:5:0x001a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.agilebinary.a.a.b.f.b.a.b a(java.lang.Object r5) {
        /*
            r4 = this;
            java.util.LinkedList r0 = r4.d
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x0034
            java.util.LinkedList r0 = r4.d
            java.util.LinkedList r1 = r4.d
            int r1 = r1.size()
            java.util.ListIterator r1 = r0.listIterator(r1)
        L_0x0014:
            boolean r0 = r1.hasPrevious()
            if (r0 == 0) goto L_0x0034
            java.lang.Object r0 = r1.previous()
            com.agilebinary.a.a.b.f.b.a.b r0 = (com.agilebinary.a.a.b.f.b.a.b) r0
            java.lang.Object r2 = r0.a()
            if (r2 == 0) goto L_0x0030
            java.lang.Object r2 = r0.a()
            boolean r2 = com.agilebinary.a.a.b.k.e.a(r5, r2)
            if (r2 == 0) goto L_0x0014
        L_0x0030:
            r1.remove()
        L_0x0033:
            return r0
        L_0x0034:
            int r0 = r4.d()
            if (r0 != 0) goto L_0x005e
            java.util.LinkedList r0 = r4.d
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x005e
            java.util.LinkedList r0 = r4.d
            java.lang.Object r0 = r0.remove()
            com.agilebinary.a.a.b.f.b.a.b r0 = (com.agilebinary.a.a.b.f.b.a.b) r0
            r0.b()
            com.agilebinary.a.a.b.c.n r1 = r0.c()
            r1.c()     // Catch:{ IOException -> 0x0055 }
            goto L_0x0033
        L_0x0055:
            r1 = move-exception
            org.apache.commons.logging.Log r2 = r4.g
            java.lang.String r3 = "I/O error closing connection"
            r2.debug(r3, r1)
            goto L_0x0033
        L_0x005e:
            r0 = 0
            goto L_0x0033
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.b.f.b.a.g.a(java.lang.Object):com.agilebinary.a.a.b.f.b.a.b");
    }

    public void a(b bVar) {
        if (this.f < 1) {
            throw new IllegalStateException("No entry created for this pool. " + this.f50a);
        } else if (this.f <= this.d.size()) {
            throw new IllegalStateException("No entry allocated from this pool. " + this.f50a);
        } else {
            this.d.add(bVar);
        }
    }

    public void a(j jVar) {
        if (jVar == null) {
            throw new IllegalArgumentException("Waiting thread must not be null.");
        }
        this.e.add(jVar);
    }

    public final int b() {
        return this.b;
    }

    public void b(b bVar) {
        if (!this.f50a.equals(bVar.d())) {
            throw new IllegalArgumentException("Entry not planned for this pool.\npool: " + this.f50a + "\nplan: " + bVar.d());
        }
        this.f++;
    }

    public void b(j jVar) {
        if (jVar != null) {
            this.e.remove(jVar);
        }
    }

    public boolean c() {
        return this.f < 1 && this.e.isEmpty();
    }

    public boolean c(b bVar) {
        boolean remove = this.d.remove(bVar);
        if (remove) {
            this.f--;
        }
        return remove;
    }

    public int d() {
        return this.c.a(this.f50a) - this.f;
    }

    public void e() {
        if (this.f < 1) {
            throw new IllegalStateException("There is no entry that could be dropped.");
        }
        this.f--;
    }

    public boolean f() {
        return !this.e.isEmpty();
    }

    public j g() {
        return (j) this.e.peek();
    }
}
