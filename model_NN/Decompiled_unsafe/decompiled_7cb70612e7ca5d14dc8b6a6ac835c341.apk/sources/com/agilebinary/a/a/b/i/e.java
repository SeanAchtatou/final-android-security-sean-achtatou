package com.agilebinary.a.a.b.i;

import com.agilebinary.a.a.b.t;
import com.agilebinary.a.a.b.z;

public final class e {
    public static String a(d dVar) {
        if (dVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        String str = (String) dVar.a("http.protocol.element-charset");
        return str == null ? "US-ASCII" : str;
    }

    public static void a(d dVar, z zVar) {
        if (dVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        dVar.a("http.protocol.version", zVar);
    }

    public static void a(d dVar, String str) {
        if (dVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        dVar.a("http.protocol.content-charset", str);
    }

    public static z b(d dVar) {
        if (dVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        Object a2 = dVar.a("http.protocol.version");
        return a2 == null ? t.c : (z) a2;
    }

    public static void b(d dVar, String str) {
        if (dVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        dVar.a("http.useragent", str);
    }

    public static String c(d dVar) {
        if (dVar != null) {
            return (String) dVar.a("http.useragent");
        }
        throw new IllegalArgumentException("HTTP parameters may not be null");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.b.i.d.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.b.i.d.a(java.lang.String, int):int
      com.agilebinary.a.a.b.i.d.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.b.i.d
      com.agilebinary.a.a.b.i.d.a(java.lang.String, boolean):boolean */
    public static boolean d(d dVar) {
        if (dVar != null) {
            return dVar.a("http.protocol.expect-continue", false);
        }
        throw new IllegalArgumentException("HTTP parameters may not be null");
    }
}
