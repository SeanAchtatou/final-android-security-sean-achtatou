package com.agilebinary.a.a.b.f.e;

import com.agilebinary.a.a.b.g.g;
import java.io.IOException;
import java.io.OutputStream;

public class f extends OutputStream {

    /* renamed from: a  reason: collision with root package name */
    private final g f88a;
    private byte[] b;
    private int c;
    private boolean d;
    private boolean e;

    public f(g gVar) {
        this(gVar, 2048);
    }

    public f(g gVar, int i) {
        this.c = 0;
        this.d = false;
        this.e = false;
        this.b = new byte[i];
        this.f88a = gVar;
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (this.c > 0) {
            this.f88a.a(Integer.toHexString(this.c));
            this.f88a.a(this.b, 0, this.c);
            this.f88a.a("");
            this.c = 0;
        }
    }

    /* access modifiers changed from: protected */
    public void a(byte[] bArr, int i, int i2) {
        this.f88a.a(Integer.toHexString(this.c + i2));
        this.f88a.a(this.b, 0, this.c);
        this.f88a.a(bArr, i, i2);
        this.f88a.a("");
        this.c = 0;
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.f88a.a("0");
        this.f88a.a("");
    }

    public void c() {
        if (!this.d) {
            a();
            b();
            this.d = true;
        }
    }

    public void close() {
        if (!this.e) {
            this.e = true;
            c();
            this.f88a.a();
        }
    }

    public void flush() {
        a();
        this.f88a.a();
    }

    public void write(int i) {
        if (this.e) {
            throw new IOException("Attempted write to closed stream.");
        }
        this.b[this.c] = (byte) i;
        this.c++;
        if (this.c == this.b.length) {
            a();
        }
    }

    public void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public void write(byte[] bArr, int i, int i2) {
        if (this.e) {
            throw new IOException("Attempted write to closed stream.");
        } else if (i2 >= this.b.length - this.c) {
            a(bArr, i, i2);
        } else {
            System.arraycopy(bArr, i, this.b, this.c, i2);
            this.c += i2;
        }
    }
}
