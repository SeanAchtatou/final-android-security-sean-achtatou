package com.agilebinary.a.a.b.d;

import com.agilebinary.a.a.b.i.d;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;

public final class j {

    /* renamed from: a  reason: collision with root package name */
    private final ConcurrentHashMap f30a = new ConcurrentHashMap();

    public h a(String str, d dVar) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        i iVar = (i) this.f30a.get(str.toLowerCase(Locale.ENGLISH));
        if (iVar != null) {
            return iVar.a(dVar);
        }
        throw new IllegalStateException("Unsupported cookie spec: " + str);
    }

    public void a(String str, i iVar) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("Cookie spec factory may not be null");
        } else {
            this.f30a.put(str.toLowerCase(Locale.ENGLISH), iVar);
        }
    }
}
