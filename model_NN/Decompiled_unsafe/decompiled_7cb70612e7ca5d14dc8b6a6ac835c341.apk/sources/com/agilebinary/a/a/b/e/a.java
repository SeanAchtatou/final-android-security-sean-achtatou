package com.agilebinary.a.a.b.e;

import com.agilebinary.a.a.b.c;
import com.agilebinary.a.a.b.i;

public abstract class a implements i {

    /* renamed from: a  reason: collision with root package name */
    protected c f31a;
    protected c b;
    protected boolean c;

    protected a() {
    }

    public void a(c cVar) {
        this.f31a = cVar;
    }

    public void a(boolean z) {
        this.c = z;
    }

    public void b(c cVar) {
        this.b = cVar;
    }

    public boolean d() {
        return this.c;
    }

    public c e() {
        return this.f31a;
    }

    public c f() {
        return this.b;
    }
}
