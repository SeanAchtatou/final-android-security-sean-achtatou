package com.agilebinary.a.a.b.k;

import com.agilebinary.a.a.b.j.d;
import java.io.Serializable;

public final class b implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private char[] f117a;
    private int b;

    public b(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Buffer capacity may not be negative");
        }
        this.f117a = new char[i];
    }

    private void d(int i) {
        char[] cArr = new char[Math.max(this.f117a.length << 1, i)];
        System.arraycopy(this.f117a, 0, cArr, 0, this.b);
        this.f117a = cArr;
    }

    public char a(int i) {
        return this.f117a[i];
    }

    public int a(int i, int i2, int i3) {
        if (i2 < 0) {
            i2 = 0;
        }
        if (i3 > this.b) {
            i3 = this.b;
        }
        if (i2 > i3) {
            return -1;
        }
        for (int i4 = i2; i4 < i3; i4++) {
            if (this.f117a[i4] == i) {
                return i4;
            }
        }
        return -1;
    }

    public String a(int i, int i2) {
        return new String(this.f117a, i, i2 - i);
    }

    public void a() {
        this.b = 0;
    }

    public void a(char c) {
        int i = this.b + 1;
        if (i > this.f117a.length) {
            d(i);
        }
        this.f117a[this.b] = c;
        this.b = i;
    }

    public void a(a aVar, int i, int i2) {
        if (aVar != null) {
            a(aVar.e(), i, i2);
        }
    }

    public void a(b bVar, int i, int i2) {
        if (bVar != null) {
            a(bVar.f117a, i, i2);
        }
    }

    public void a(Object obj) {
        a(String.valueOf(obj));
    }

    public void a(String str) {
        if (str == null) {
            str = "null";
        }
        int length = str.length();
        int i = this.b + length;
        if (i > this.f117a.length) {
            d(i);
        }
        str.getChars(0, length, this.f117a, this.b);
        this.b = i;
    }

    public void a(byte[] bArr, int i, int i2) {
        if (bArr != null) {
            if (i < 0 || i > bArr.length || i2 < 0 || i + i2 < 0 || i + i2 > bArr.length) {
                throw new IndexOutOfBoundsException("off: " + i + " len: " + i2 + " b.length: " + bArr.length);
            } else if (i2 != 0) {
                int i3 = this.b;
                int i4 = i3 + i2;
                if (i4 > this.f117a.length) {
                    d(i4);
                }
                while (i3 < i4) {
                    this.f117a[i3] = (char) (bArr[i] & 255);
                    i++;
                    i3++;
                }
                this.b = i4;
            }
        }
    }

    public void a(char[] cArr, int i, int i2) {
        if (cArr != null) {
            if (i < 0 || i > cArr.length || i2 < 0 || i + i2 < 0 || i + i2 > cArr.length) {
                throw new IndexOutOfBoundsException("off: " + i + " len: " + i2 + " b.length: " + cArr.length);
            } else if (i2 != 0) {
                int i3 = this.b + i2;
                if (i3 > this.f117a.length) {
                    d(i3);
                }
                System.arraycopy(cArr, i, this.f117a, this.b, i2);
                this.b = i3;
            }
        }
    }

    public String b(int i, int i2) {
        if (i < 0) {
            throw new IndexOutOfBoundsException("Negative beginIndex: " + i);
        } else if (i2 > this.b) {
            throw new IndexOutOfBoundsException("endIndex: " + i2 + " > length: " + this.b);
        } else if (i > i2) {
            throw new IndexOutOfBoundsException("beginIndex: " + i + " > endIndex: " + i2);
        } else {
            while (i < i2 && d.a(this.f117a[i])) {
                i++;
            }
            while (i2 > i && d.a(this.f117a[i2 - 1])) {
                i2--;
            }
            return new String(this.f117a, i, i2 - i);
        }
    }

    public void b(int i) {
        if (i > 0 && i > this.f117a.length - this.b) {
            d(this.b + i);
        }
    }

    public char[] b() {
        return this.f117a;
    }

    public int c() {
        return this.b;
    }

    public int c(int i) {
        return a(i, 0, this.b);
    }

    public boolean d() {
        return this.b == 0;
    }

    public String toString() {
        return new String(this.f117a, 0, this.b);
    }
}
