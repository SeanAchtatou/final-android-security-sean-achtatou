package com.agilebinary.a.a.b.e;

import com.agilebinary.a.a.b.i;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class c extends f {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f32a;

    public c(i iVar) {
        super(iVar);
        if (!iVar.c() || iVar.b() < 0) {
            this.f32a = com.agilebinary.a.a.b.k.c.b(iVar);
        } else {
            this.f32a = null;
        }
    }

    public InputStream a() {
        return this.f32a != null ? new ByteArrayInputStream(this.f32a) : this.c.a();
    }

    public void a(OutputStream outputStream) {
        if (outputStream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        } else if (this.f32a != null) {
            outputStream.write(this.f32a);
        } else {
            this.c.a(outputStream);
        }
    }

    public long b() {
        return this.f32a != null ? (long) this.f32a.length : this.c.b();
    }

    public boolean c() {
        return true;
    }

    public boolean d() {
        return this.f32a == null && this.c.d();
    }

    public boolean g() {
        return this.f32a == null && this.c.g();
    }
}
