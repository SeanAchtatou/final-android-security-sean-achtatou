package com.agilebinary.a.a.b;

import com.agilebinary.a.a.b.k.b;
import java.io.Serializable;

public class z implements Serializable, Cloneable {
    protected final String d;
    protected final int e;
    protected final int f;

    public z(String str, int i, int i2) {
        if (str == null) {
            throw new IllegalArgumentException("Protocol name must not be null.");
        } else if (i < 0) {
            throw new IllegalArgumentException("Protocol major version number must not be negative.");
        } else if (i2 < 0) {
            throw new IllegalArgumentException("Protocol minor version number may not be negative");
        } else {
            this.d = str;
            this.e = i;
            this.f = i2;
        }
    }

    public z a(int i, int i2) {
        return (i == this.e && i2 == this.f) ? this : new z(this.d, i, i2);
    }

    public final String a() {
        return this.d;
    }

    public boolean a(z zVar) {
        return zVar != null && this.d.equals(zVar.d);
    }

    public final int b() {
        return this.e;
    }

    public int b(z zVar) {
        if (zVar == null) {
            throw new IllegalArgumentException("Protocol version must not be null.");
        } else if (!this.d.equals(zVar.d)) {
            throw new IllegalArgumentException("Versions for different protocols cannot be compared. " + this + " " + zVar);
        } else {
            int b = b() - zVar.b();
            return b == 0 ? c() - zVar.c() : b;
        }
    }

    public final int c() {
        return this.f;
    }

    public final boolean c(z zVar) {
        return a(zVar) && b(zVar) <= 0;
    }

    public Object clone() {
        return super.clone();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof z)) {
            return false;
        }
        z zVar = (z) obj;
        return this.d.equals(zVar.d) && this.e == zVar.e && this.f == zVar.f;
    }

    public final int hashCode() {
        return (this.d.hashCode() ^ (this.e * 100000)) ^ this.f;
    }

    public String toString() {
        b bVar = new b(16);
        bVar.a(this.d);
        bVar.a('/');
        bVar.a(Integer.toString(this.e));
        bVar.a('.');
        bVar.a(Integer.toString(this.f));
        return bVar.toString();
    }
}
