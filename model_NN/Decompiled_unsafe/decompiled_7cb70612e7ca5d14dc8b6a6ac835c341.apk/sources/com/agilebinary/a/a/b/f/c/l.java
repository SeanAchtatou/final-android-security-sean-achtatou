package com.agilebinary.a.a.b.f.c;

import com.agilebinary.a.a.b.d.h;
import com.agilebinary.a.a.b.d.i;
import com.agilebinary.a.a.b.i.d;
import java.util.Collection;

public class l implements i {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.b.i.d.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.b.i.d.a(java.lang.String, int):int
      com.agilebinary.a.a.b.i.d.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.b.i.d
      com.agilebinary.a.a.b.i.d.a(java.lang.String, boolean):boolean */
    public h a(d dVar) {
        if (dVar == null) {
            return new k();
        }
        Collection collection = (Collection) dVar.a("http.protocol.cookie-datepatterns");
        return new k(collection != null ? (String[]) collection.toArray(new String[collection.size()]) : null, dVar.a("http.protocol.single-cookie-header", false));
    }
}
