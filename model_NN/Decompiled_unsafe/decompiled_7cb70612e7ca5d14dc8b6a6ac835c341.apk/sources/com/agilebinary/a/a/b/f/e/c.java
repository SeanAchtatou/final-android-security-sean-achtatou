package com.agilebinary.a.a.b.f.e;

import com.agilebinary.a.a.b.g.a;
import com.agilebinary.a.a.b.g.f;
import com.agilebinary.a.a.b.i.d;
import com.agilebinary.a.a.b.i.e;
import com.agilebinary.a.a.b.k.b;
import java.io.IOException;
import java.io.InputStream;

public abstract class c implements a, f {

    /* renamed from: a  reason: collision with root package name */
    private InputStream f85a;
    private byte[] b;
    private int c;
    private int d;
    private com.agilebinary.a.a.b.k.a e = null;
    private String f = "US-ASCII";
    private boolean g = true;
    private int h = -1;
    private int i = 512;
    private k j;

    private int a(b bVar, int i2) {
        int i3 = this.c;
        this.c = i2 + 1;
        if (i2 > 0 && this.b[i2 - 1] == 13) {
            i2--;
        }
        int i4 = i2 - i3;
        if (this.g) {
            bVar.a(this.b, i3, i4);
            return i4;
        }
        String str = new String(this.b, i3, i4, this.f);
        bVar.a(str);
        return str.length();
    }

    private int b(b bVar) {
        int d2 = this.e.d();
        if (d2 > 0) {
            if (this.e.b(d2 - 1) == 10) {
                d2--;
                this.e.c(d2);
            }
            if (d2 > 0 && this.e.b(d2 - 1) == 13) {
                this.e.c(d2 - 1);
            }
        }
        int d3 = this.e.d();
        if (this.g) {
            bVar.a(this.e, 0, d3);
        } else {
            String str = new String(this.e.e(), 0, d3, this.f);
            d3 = str.length();
            bVar.a(str);
        }
        this.e.a();
        return d3;
    }

    private int c() {
        for (int i2 = this.c; i2 < this.d; i2++) {
            if (this.b[i2] == 10) {
                return i2;
            }
        }
        return -1;
    }

    public int a() {
        while (!g()) {
            if (f() == -1) {
                return -1;
            }
        }
        byte[] bArr = this.b;
        int i2 = this.c;
        this.c = i2 + 1;
        return bArr[i2] & 255;
    }

    public int a(b bVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        }
        boolean z = true;
        int i2 = 0;
        while (z) {
            int c2 = c();
            if (c2 == -1) {
                if (g()) {
                    this.e.a(this.b, this.c, this.d - this.c);
                    this.c = this.d;
                }
                i2 = f();
                if (i2 == -1) {
                    z = false;
                }
            } else if (this.e.f()) {
                return a(bVar, c2);
            } else {
                this.e.a(this.b, this.c, (c2 + 1) - this.c);
                this.c = c2 + 1;
                z = false;
            }
            if (this.h > 0 && this.e.d() >= this.h) {
                throw new IOException("Maximum line length limit exceeded");
            }
        }
        if (i2 != -1 || !this.e.f()) {
            return b(bVar);
        }
        return -1;
    }

    public int a(byte[] bArr, int i2, int i3) {
        if (bArr == null) {
            return 0;
        }
        if (g()) {
            int min = Math.min(i3, this.d - this.c);
            System.arraycopy(this.b, this.c, bArr, i2, min);
            this.c += min;
            return min;
        } else if (i3 > this.i) {
            return this.f85a.read(bArr, i2, i3);
        } else {
            while (!g()) {
                if (f() == -1) {
                    return -1;
                }
            }
            int min2 = Math.min(i3, this.d - this.c);
            System.arraycopy(this.b, this.c, bArr, i2, min2);
            this.c += min2;
            return min2;
        }
    }

    /* access modifiers changed from: protected */
    public void a(InputStream inputStream, int i2, d dVar) {
        boolean z = false;
        if (inputStream == null) {
            throw new IllegalArgumentException("Input stream may not be null");
        } else if (i2 <= 0) {
            throw new IllegalArgumentException("Buffer size may not be negative or zero");
        } else if (dVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            this.f85a = inputStream;
            this.b = new byte[i2];
            this.c = 0;
            this.d = 0;
            this.e = new com.agilebinary.a.a.b.k.a(i2);
            this.f = e.a(dVar);
            if (this.f.equalsIgnoreCase("US-ASCII") || this.f.equalsIgnoreCase("ASCII")) {
                z = true;
            }
            this.g = z;
            this.h = dVar.a("http.connection.max-line-length", -1);
            this.i = dVar.a("http.connection.min-chunk-limit", 512);
            this.j = d();
        }
    }

    public com.agilebinary.a.a.b.g.e b() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public k d() {
        return new k();
    }

    public int e() {
        return this.d - this.c;
    }

    /* access modifiers changed from: protected */
    public int f() {
        if (this.c > 0) {
            int i2 = this.d - this.c;
            if (i2 > 0) {
                System.arraycopy(this.b, this.c, this.b, 0, i2);
            }
            this.c = 0;
            this.d = i2;
        }
        int i3 = this.d;
        int read = this.f85a.read(this.b, i3, this.b.length - i3);
        if (read == -1) {
            return -1;
        }
        this.d = i3 + read;
        this.j.a((long) read);
        return read;
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        return this.c < this.d;
    }
}
