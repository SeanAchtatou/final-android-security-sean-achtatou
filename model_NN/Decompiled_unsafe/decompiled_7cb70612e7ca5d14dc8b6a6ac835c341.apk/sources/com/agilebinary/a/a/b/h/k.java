package com.agilebinary.a.a.b.h;

import com.agilebinary.a.a.b.k.b;
import com.agilebinary.a.a.b.k.e;
import com.agilebinary.a.a.b.v;
import java.io.Serializable;

public class k implements v, Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final String f104a;
    private final String b;

    public k(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        this.f104a = str;
        this.b = str2;
    }

    public String a() {
        return this.f104a;
    }

    public String b() {
        return this.b;
    }

    public Object clone() {
        return super.clone();
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof v)) {
            return false;
        }
        k kVar = (k) obj;
        if (!this.f104a.equals(kVar.f104a) || !e.a(this.b, kVar.b)) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return e.a(e.a(17, this.f104a), this.b);
    }

    public String toString() {
        if (this.b == null) {
            return this.f104a;
        }
        b bVar = new b(this.f104a.length() + 1 + this.b.length());
        bVar.a(this.f104a);
        bVar.a("=");
        bVar.a(this.b);
        return bVar.toString();
    }
}
