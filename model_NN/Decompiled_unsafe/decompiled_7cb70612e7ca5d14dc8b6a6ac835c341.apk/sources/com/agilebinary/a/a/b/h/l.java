package com.agilebinary.a.a.b.h;

import com.agilebinary.a.a.b.ab;
import com.agilebinary.a.a.b.k.b;
import com.agilebinary.a.a.b.z;
import java.io.Serializable;

public class l implements ab, Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final z f105a;
    private final String b;
    private final String c;

    public l(String str, String str2, z zVar) {
        if (str == null) {
            throw new IllegalArgumentException("Method must not be null.");
        } else if (str2 == null) {
            throw new IllegalArgumentException("URI must not be null.");
        } else if (zVar == null) {
            throw new IllegalArgumentException("Protocol version must not be null.");
        } else {
            this.b = str;
            this.c = str2;
            this.f105a = zVar;
        }
    }

    public String a() {
        return this.b;
    }

    public z b() {
        return this.f105a;
    }

    public String c() {
        return this.c;
    }

    public Object clone() {
        return super.clone();
    }

    public String toString() {
        return h.f101a.a((b) null, this).toString();
    }
}
