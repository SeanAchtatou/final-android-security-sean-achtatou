package com.agilebinary.a.a.b.f.a;

import com.agilebinary.a.a.a.a.a;
import com.agilebinary.a.a.b.b.a.d;
import com.agilebinary.a.a.b.b.a.g;
import com.agilebinary.a.a.b.b.d.b;
import com.agilebinary.a.a.b.c;
import com.agilebinary.a.a.b.j.e;
import com.agilebinary.a.a.b.l;
import com.agilebinary.a.a.b.o;
import com.agilebinary.a.a.b.q;
import com.agilebinary.a.a.b.y;
import java.net.URI;
import java.net.URISyntaxException;
import org.apache.commons.logging.Log;

public class k implements com.agilebinary.a.a.b.b.k {

    /* renamed from: a  reason: collision with root package name */
    private final Log f41a = a.a(getClass());

    /* access modifiers changed from: protected */
    public URI a(String str) {
        try {
            return new URI(str);
        } catch (URISyntaxException e) {
            throw new y("Invalid redirect URI: " + str, e);
        }
    }

    public boolean a(o oVar, q qVar, e eVar) {
        if (qVar == null) {
            throw new IllegalArgumentException("HTTP response may not be null");
        }
        int b = qVar.a().b();
        String a2 = oVar.g().a();
        c c = qVar.c("location");
        switch (b) {
            case 301:
            case 307:
                return a2.equalsIgnoreCase("GET") || a2.equalsIgnoreCase("HEAD");
            case 302:
                return (a2.equalsIgnoreCase("GET") || a2.equalsIgnoreCase("HEAD")) && c != null;
            case 303:
                return true;
            case 304:
            case 305:
            case 306:
            default:
                return false;
        }
    }

    public g b(o oVar, q qVar, e eVar) {
        URI c = c(oVar, qVar, eVar);
        return oVar.g().a().equalsIgnoreCase("HEAD") ? new d(c) : new com.agilebinary.a.a.b.b.a.c(c);
    }

    public URI c(o oVar, q qVar, e eVar) {
        URI uri;
        if (qVar == null) {
            throw new IllegalArgumentException("HTTP response may not be null");
        }
        c c = qVar.c("location");
        if (c == null) {
            throw new y("Received redirect response " + qVar.a() + " but no location header");
        }
        String d = c.d();
        if (this.f41a.isDebugEnabled()) {
            this.f41a.debug("Redirect requested to location '" + d + "'");
        }
        URI a2 = a(d);
        com.agilebinary.a.a.b.i.d f = qVar.f();
        if (!a2.isAbsolute()) {
            if (f.b("http.protocol.reject-relative-redirect")) {
                throw new y("Relative redirect location '" + a2 + "' not allowed");
            }
            l lVar = (l) eVar.a("http.target_host");
            if (lVar == null) {
                throw new IllegalStateException("Target host not available in the HTTP context");
            }
            try {
                a2 = b.a(b.a(new URI(oVar.g().c()), lVar, true), a2);
            } catch (URISyntaxException e) {
                throw new y(e.getMessage(), e);
            }
        }
        if (f.c("http.protocol.allow-circular-redirects")) {
            q qVar2 = (q) eVar.a("http.protocol.redirect-locations");
            if (qVar2 == null) {
                qVar2 = new q();
                eVar.a("http.protocol.redirect-locations", qVar2);
            }
            if (a2.getFragment() != null) {
                try {
                    uri = b.a(a2, new l(a2.getHost(), a2.getPort(), a2.getScheme()), true);
                } catch (URISyntaxException e2) {
                    throw new y(e2.getMessage(), e2);
                }
            } else {
                uri = a2;
            }
            if (qVar2.a(uri)) {
                throw new com.agilebinary.a.a.b.b.c("Circular redirect to '" + uri + "'");
            }
            qVar2.b(uri);
        }
        return a2;
    }
}
