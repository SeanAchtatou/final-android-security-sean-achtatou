package com.agilebinary.a.a.b.f.e;

import com.agilebinary.a.a.b.g.b;
import com.agilebinary.a.a.b.i.d;
import java.io.InterruptedIOException;
import java.net.Socket;

public class n extends c implements b {

    /* renamed from: a  reason: collision with root package name */
    private static final Class f94a = h();
    private final Socket b;
    private boolean c;

    public n(Socket socket, int i, d dVar) {
        int i2 = 1024;
        if (socket == null) {
            throw new IllegalArgumentException("Socket may not be null");
        }
        this.b = socket;
        this.c = false;
        int receiveBufferSize = i < 0 ? socket.getReceiveBufferSize() : i;
        a(socket.getInputStream(), receiveBufferSize >= 1024 ? receiveBufferSize : i2, dVar);
    }

    private static boolean a(InterruptedIOException interruptedIOException) {
        if (f94a != null) {
            return f94a.isInstance(interruptedIOException);
        }
        return true;
    }

    private static Class h() {
        try {
            return Class.forName("java.net.SocketTimeoutException");
        } catch (ClassNotFoundException e) {
            return null;
        }
    }

    public boolean a(int i) {
        boolean g = g();
        if (!g) {
            int soTimeout = this.b.getSoTimeout();
            try {
                this.b.setSoTimeout(i);
                f();
                g = g();
            } catch (InterruptedIOException e) {
                if (!a(e)) {
                    throw e;
                }
            } finally {
                this.b.setSoTimeout(soTimeout);
            }
        }
        return g;
    }

    public boolean c() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public int f() {
        int f = super.f();
        this.c = f == -1;
        return f;
    }
}
