package com.agilebinary.a.a.b.f.a;

import com.agilebinary.a.a.b.c;
import com.agilebinary.a.a.b.i;
import com.agilebinary.a.a.b.j;

public class o extends r implements j {
    private i c;
    /* access modifiers changed from: private */
    public boolean d;

    public o(j jVar) {
        super(jVar);
        a(jVar.b());
    }

    public void a(i iVar) {
        this.c = iVar != null ? new p(this, iVar) : null;
        this.d = false;
    }

    public boolean a() {
        c c2 = c("Expect");
        return c2 != null && "100-continue".equalsIgnoreCase(c2.d());
    }

    public i b() {
        return this.c;
    }

    public boolean j() {
        return this.c == null || this.c.c() || !this.d;
    }
}
