package com.agilebinary.a.a.b.f.b.a;

import com.agilebinary.a.a.b.c.b.b;
import java.util.concurrent.TimeUnit;

final class e implements f {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ k f49a;
    final /* synthetic */ b b;
    final /* synthetic */ Object c;
    final /* synthetic */ d d;

    e(d dVar, k kVar, b bVar, Object obj) {
        this.d = dVar;
        this.f49a = kVar;
        this.b = bVar;
        this.c = obj;
    }

    public b a(long j, TimeUnit timeUnit) {
        return this.d.a(this.b, this.c, j, timeUnit, this.f49a);
    }

    public void a() {
        this.d.o.lock();
        try {
            this.f49a.a();
        } finally {
            this.d.o.unlock();
        }
    }
}
