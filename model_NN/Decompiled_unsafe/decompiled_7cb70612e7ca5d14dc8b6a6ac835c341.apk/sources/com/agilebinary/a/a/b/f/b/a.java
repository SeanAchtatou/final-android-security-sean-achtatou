package com.agilebinary.a.a.b.f.b;

import com.agilebinary.a.a.b.c.b;
import com.agilebinary.a.a.b.c.m;
import com.agilebinary.a.a.b.c.n;
import com.agilebinary.a.a.b.j;
import com.agilebinary.a.a.b.j.e;
import com.agilebinary.a.a.b.o;
import com.agilebinary.a.a.b.q;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

public abstract class a implements m, e {

    /* renamed from: a  reason: collision with root package name */
    private volatile b f47a;
    private volatile n b;
    private volatile boolean c = false;
    private volatile boolean d = false;
    private volatile long e = Long.MAX_VALUE;

    protected a(b bVar, n nVar) {
        this.f47a = bVar;
        this.b = nVar;
    }

    public q a() {
        n o = o();
        a(o);
        r();
        return o.a();
    }

    public synchronized Object a(String str) {
        n o;
        o = o();
        a(o);
        return o instanceof e ? ((e) o).a(str) : null;
    }

    public void a(long j, TimeUnit timeUnit) {
        if (j > 0) {
            this.e = timeUnit.toMillis(j);
        } else {
            this.e = -1;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(n nVar) {
        if (q() || nVar == null) {
            throw new d();
        }
    }

    public void a(j jVar) {
        n o = o();
        a(o);
        r();
        o.a(jVar);
    }

    public void a(o oVar) {
        n o = o();
        a(o);
        r();
        o.a(oVar);
    }

    public void a(q qVar) {
        n o = o();
        a(o);
        r();
        o.a(qVar);
    }

    public synchronized void a(String str, Object obj) {
        n o = o();
        a(o);
        if (o instanceof e) {
            ((e) o).a(str, obj);
        }
    }

    public boolean a(int i) {
        n o = o();
        a(o);
        return o.a(i);
    }

    public void b() {
        n o = o();
        a(o);
        o.b();
    }

    public void b(int i) {
        n o = o();
        a(o);
        o.b(i);
    }

    public boolean d() {
        n o = o();
        if (o == null) {
            return false;
        }
        return o.d();
    }

    public boolean e() {
        n o;
        if (!q() && (o = o()) != null) {
            return o.e();
        }
        return true;
    }

    public synchronized void e_() {
        if (!this.d) {
            this.d = true;
            if (this.f47a != null) {
                this.f47a.a(this, this.e, TimeUnit.MILLISECONDS);
            }
        }
    }

    public InetAddress g() {
        n o = o();
        a(o);
        return o.g();
    }

    public int h() {
        n o = o();
        a(o);
        return o.h();
    }

    public synchronized void i() {
        if (!this.d) {
            this.d = true;
            r();
            try {
                f();
            } catch (IOException e2) {
            }
            if (this.f47a != null) {
                this.f47a.a(this, this.e, TimeUnit.MILLISECONDS);
            }
        }
    }

    public boolean j() {
        n o = o();
        a(o);
        return o.i();
    }

    public SSLSession l() {
        n o = o();
        a(o);
        if (!d()) {
            return null;
        }
        Socket j = o.j();
        return j instanceof SSLSocket ? ((SSLSocket) j).getSession() : null;
    }

    public void m() {
        this.c = true;
    }

    /* access modifiers changed from: protected */
    public synchronized void n() {
        this.b = null;
        this.f47a = null;
        this.e = Long.MAX_VALUE;
    }

    /* access modifiers changed from: protected */
    public n o() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public b p() {
        return this.f47a;
    }

    /* access modifiers changed from: protected */
    public boolean q() {
        return this.d;
    }

    public void r() {
        this.c = false;
    }

    public boolean s() {
        return this.c;
    }
}
