package com.agilebinary.a.a.b.h;

import com.agilebinary.a.a.b.b;
import com.agilebinary.a.a.b.d;
import com.agilebinary.a.a.b.x;
import java.io.Serializable;

public class o implements b, Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final String f108a;
    private final com.agilebinary.a.a.b.k.b b;
    private final int c;

    public o(com.agilebinary.a.a.b.k.b bVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        }
        int c2 = bVar.c(58);
        if (c2 == -1) {
            throw new x("Invalid header: " + bVar.toString());
        }
        String b2 = bVar.b(0, c2);
        if (b2.length() == 0) {
            throw new x("Invalid header: " + bVar.toString());
        }
        this.b = bVar;
        this.f108a = b2;
        this.c = c2 + 1;
    }

    public com.agilebinary.a.a.b.k.b a() {
        return this.b;
    }

    public int b() {
        return this.c;
    }

    public String c() {
        return this.f108a;
    }

    public Object clone() {
        return super.clone();
    }

    public String d() {
        return this.b.b(this.c, this.b.c());
    }

    public d[] e() {
        t tVar = new t(0, this.b.c());
        tVar.a(this.c);
        return e.f100a.a(this.b, tVar);
    }

    public String toString() {
        return this.b.toString();
    }
}
