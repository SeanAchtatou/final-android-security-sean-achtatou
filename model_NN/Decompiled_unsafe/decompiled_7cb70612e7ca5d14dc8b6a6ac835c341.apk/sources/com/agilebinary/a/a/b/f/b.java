package com.agilebinary.a.a.b.f;

import com.agilebinary.a.a.b.a;
import com.agilebinary.a.a.b.ad;
import com.agilebinary.a.a.b.f;
import com.agilebinary.a.a.b.h;
import com.agilebinary.a.a.b.h.n;
import com.agilebinary.a.a.b.i;
import com.agilebinary.a.a.b.j.e;
import com.agilebinary.a.a.b.q;
import com.agilebinary.a.a.b.t;
import com.agilebinary.a.a.b.x;
import com.agilebinary.a.a.b.z;

public class b implements a {
    /* access modifiers changed from: protected */
    public ad a(f fVar) {
        return new n(fVar);
    }

    public boolean a(q qVar, e eVar) {
        boolean z = true;
        if (qVar == null) {
            throw new IllegalArgumentException("HTTP response may not be null.");
        } else if (eVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null.");
        } else {
            h hVar = (h) eVar.a("http.connection");
            if (hVar != null && !hVar.d()) {
                return false;
            }
            i b = qVar.b();
            z a2 = qVar.a().a();
            if (b != null && b.b() < 0 && (!b.d() || a2.c(t.b))) {
                return false;
            }
            f d = qVar.d("Connection");
            if (!d.hasNext()) {
                d = qVar.d("Proxy-Connection");
            }
            if (d.hasNext()) {
                try {
                    ad a3 = a(d);
                    boolean z2 = false;
                    while (a3.hasNext()) {
                        String a4 = a3.a();
                        if ("Close".equalsIgnoreCase(a4)) {
                            return false;
                        }
                        if ("Keep-Alive".equalsIgnoreCase(a4)) {
                            z2 = true;
                        }
                    }
                    if (z2) {
                        return true;
                    }
                } catch (x e) {
                    return false;
                }
            }
            if (a2.c(t.b)) {
                z = false;
            }
            return z;
        }
    }
}
