package com.agilebinary.a.a.b.d;

import java.io.Serializable;
import java.util.Comparator;

public class f implements Serializable, Comparator {
    private String a(b bVar) {
        String d = bVar.d();
        if (d == null) {
            d = "/";
        }
        return !d.endsWith("/") ? d + '/' : d;
    }

    /* renamed from: a */
    public int compare(b bVar, b bVar2) {
        String a2 = a(bVar);
        String a3 = a(bVar2);
        if (a2.equals(a3)) {
            return 0;
        }
        if (a2.startsWith(a3)) {
            return -1;
        }
        return a3.startsWith(a2) ? 1 : 0;
    }
}
