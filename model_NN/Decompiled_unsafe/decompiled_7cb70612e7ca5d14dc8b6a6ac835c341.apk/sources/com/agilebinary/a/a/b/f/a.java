package com.agilebinary.a.a.b.f;

import com.agilebinary.a.a.b.f.d.b;
import com.agilebinary.a.a.b.f.e.i;
import com.agilebinary.a.a.b.f.e.j;
import com.agilebinary.a.a.b.g;
import com.agilebinary.a.a.b.g.c;
import com.agilebinary.a.a.b.g.d;
import com.agilebinary.a.a.b.g.e;
import com.agilebinary.a.a.b.g.f;
import com.agilebinary.a.a.b.o;
import com.agilebinary.a.a.b.q;
import com.agilebinary.a.a.b.r;
import java.io.IOException;

public abstract class a implements g {

    /* renamed from: a  reason: collision with root package name */
    private final b f33a = m();
    private final com.agilebinary.a.a.b.f.d.a b = l();
    private f c = null;
    private com.agilebinary.a.a.b.g.g d = null;
    private com.agilebinary.a.a.b.g.b e = null;
    private c f = null;
    private d g = null;
    private e h = null;

    /* access modifiers changed from: protected */
    public e a(e eVar, e eVar2) {
        return new e(eVar, eVar2);
    }

    /* access modifiers changed from: protected */
    public c a(f fVar, r rVar, com.agilebinary.a.a.b.i.d dVar) {
        return new j(fVar, null, rVar, dVar);
    }

    /* access modifiers changed from: protected */
    public d a(com.agilebinary.a.a.b.g.g gVar, com.agilebinary.a.a.b.i.d dVar) {
        return new i(gVar, null, dVar);
    }

    public q a() {
        k();
        q qVar = (q) this.f.a();
        if (qVar.a().b() >= 200) {
            this.h.b();
        }
        return qVar;
    }

    /* access modifiers changed from: protected */
    public void a(f fVar, com.agilebinary.a.a.b.g.g gVar, com.agilebinary.a.a.b.i.d dVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("Input session buffer may not be null");
        } else if (gVar == null) {
            throw new IllegalArgumentException("Output session buffer may not be null");
        } else {
            this.c = fVar;
            this.d = gVar;
            if (fVar instanceof com.agilebinary.a.a.b.g.b) {
                this.e = (com.agilebinary.a.a.b.g.b) fVar;
            }
            this.f = a(fVar, n(), dVar);
            this.g = a(gVar, dVar);
            this.h = a(fVar.b(), gVar.b());
        }
    }

    public void a(com.agilebinary.a.a.b.j jVar) {
        if (jVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        }
        k();
        if (jVar.b() != null) {
            this.f33a.a(this.d, jVar, jVar.b());
        }
    }

    public void a(o oVar) {
        if (oVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        }
        k();
        this.g.b(oVar);
        this.h.a();
    }

    public void a(q qVar) {
        if (qVar == null) {
            throw new IllegalArgumentException("HTTP response may not be null");
        }
        k();
        qVar.a(this.b.b(this.c, qVar));
    }

    public boolean a(int i) {
        k();
        return this.c.a(i);
    }

    public void b() {
        k();
        o();
    }

    public boolean e() {
        if (!d() || p()) {
            return true;
        }
        try {
            this.c.a(1);
            return p();
        } catch (IOException e2) {
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public abstract void k();

    /* access modifiers changed from: protected */
    public com.agilebinary.a.a.b.f.d.a l() {
        return new com.agilebinary.a.a.b.f.d.a(new com.agilebinary.a.a.b.f.d.c());
    }

    /* access modifiers changed from: protected */
    public b m() {
        return new b(new com.agilebinary.a.a.b.f.d.d());
    }

    /* access modifiers changed from: protected */
    public r n() {
        return new c();
    }

    /* access modifiers changed from: protected */
    public void o() {
        this.d.a();
    }

    /* access modifiers changed from: protected */
    public boolean p() {
        return this.e != null && this.e.c();
    }
}
