package com.agilebinary.a.a.b.f.a;

import com.agilebinary.a.a.b.b.e;
import com.agilebinary.a.a.b.d.b;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeSet;

public class d implements e, Serializable {

    /* renamed from: a  reason: collision with root package name */
    private final TreeSet f37a = new TreeSet(new com.agilebinary.a.a.b.d.d());

    public synchronized List a() {
        return new ArrayList(this.f37a);
    }

    public synchronized void a(b bVar) {
        if (bVar != null) {
            this.f37a.remove(bVar);
            if (!bVar.a(new Date())) {
                this.f37a.add(bVar);
            }
        }
    }

    public synchronized String toString() {
        return this.f37a.toString();
    }
}
