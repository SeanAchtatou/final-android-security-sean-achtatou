package com.agilebinary.a.a.b.h;

import com.agilebinary.a.a.b.d;
import com.agilebinary.a.a.b.k.b;
import com.agilebinary.a.a.b.k.e;
import com.agilebinary.a.a.b.v;

public class c implements d, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final String f98a;
    private final String b;
    private final v[] c;

    public c(String str, String str2, v[] vVarArr) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        this.f98a = str;
        this.b = str2;
        if (vVarArr != null) {
            this.c = vVarArr;
        } else {
            this.c = new v[0];
        }
    }

    public v a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        for (v vVar : this.c) {
            if (vVar.a().equalsIgnoreCase(str)) {
                return vVar;
            }
        }
        return null;
    }

    public String a() {
        return this.f98a;
    }

    public String b() {
        return this.b;
    }

    public v[] c() {
        return (v[]) this.c.clone();
    }

    public Object clone() {
        return super.clone();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.b.k.e.a(java.lang.Object[], java.lang.Object[]):boolean
     arg types: [com.agilebinary.a.a.b.v[], com.agilebinary.a.a.b.v[]]
     candidates:
      com.agilebinary.a.a.b.k.e.a(int, int):int
      com.agilebinary.a.a.b.k.e.a(int, java.lang.Object):int
      com.agilebinary.a.a.b.k.e.a(int, boolean):int
      com.agilebinary.a.a.b.k.e.a(java.lang.Object, java.lang.Object):boolean
      com.agilebinary.a.a.b.k.e.a(java.lang.Object[], java.lang.Object[]):boolean */
    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof d)) {
            return false;
        }
        c cVar = (c) obj;
        if (!this.f98a.equals(cVar.f98a) || !e.a(this.b, cVar.b) || !e.a((Object[]) this.c, (Object[]) cVar.c)) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        int a2 = e.a(e.a(17, this.f98a), this.b);
        for (v a3 : this.c) {
            a2 = e.a(a2, a3);
        }
        return a2;
    }

    public String toString() {
        b bVar = new b(64);
        bVar.a(this.f98a);
        if (this.b != null) {
            bVar.a("=");
            bVar.a(this.b);
        }
        for (v a2 : this.c) {
            bVar.a("; ");
            bVar.a(a2);
        }
        return bVar.toString();
    }
}
