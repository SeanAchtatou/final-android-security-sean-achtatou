package com.agilebinary.a.a.b.f.a;

import com.agilebinary.a.a.b.a.c;
import com.agilebinary.a.a.b.a.f;
import com.agilebinary.a.a.b.a.i;
import com.agilebinary.a.a.b.b.b;
import com.agilebinary.a.a.b.j.d;
import com.agilebinary.a.a.b.j.e;
import com.agilebinary.a.a.b.q;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.apache.commons.logging.Log;

public abstract class a implements b {
    private static final List b = Collections.unmodifiableList(Arrays.asList("negotiate", "NTLM", "Digest", "Basic"));

    /* renamed from: a  reason: collision with root package name */
    private final Log f34a = com.agilebinary.a.a.a.a.a.a(getClass());

    public com.agilebinary.a.a.b.a.a a(Map map, q qVar, e eVar) {
        com.agilebinary.a.a.b.a.a aVar;
        c cVar = (c) eVar.a("http.authscheme-registry");
        if (cVar == null) {
            throw new IllegalStateException("AuthScheme registry not set in HTTP context");
        }
        List c = c(qVar, eVar);
        if (c == null) {
            c = b;
        }
        if (this.f34a.isDebugEnabled()) {
            this.f34a.debug("Authentication schemes in the order of preference: " + c);
        }
        Iterator it = c.iterator();
        while (true) {
            if (!it.hasNext()) {
                aVar = null;
                break;
            }
            String str = (String) it.next();
            if (((com.agilebinary.a.a.b.c) map.get(str.toLowerCase(Locale.ENGLISH))) != null) {
                if (this.f34a.isDebugEnabled()) {
                    this.f34a.debug(str + " authentication scheme selected");
                }
                try {
                    aVar = cVar.a(str, qVar.f());
                    break;
                } catch (IllegalStateException e) {
                    if (this.f34a.isWarnEnabled()) {
                        this.f34a.warn("Authentication scheme " + str + " not supported");
                    }
                }
            } else if (this.f34a.isDebugEnabled()) {
                this.f34a.debug("Challenge for " + str + " authentication scheme not available");
            }
        }
        if (aVar != null) {
            return aVar;
        }
        throw new f("Unable to respond to any of these challenges: " + map);
    }

    /* access modifiers changed from: protected */
    public List a() {
        return b;
    }

    /* access modifiers changed from: protected */
    public Map a(com.agilebinary.a.a.b.c[] cVarArr) {
        com.agilebinary.a.a.b.k.b bVar;
        int i;
        HashMap hashMap = new HashMap(cVarArr.length);
        for (com.agilebinary.a.a.b.c cVar : cVarArr) {
            if (cVar instanceof com.agilebinary.a.a.b.b) {
                bVar = ((com.agilebinary.a.a.b.b) cVar).a();
                i = ((com.agilebinary.a.a.b.b) cVar).b();
            } else {
                String d = cVar.d();
                if (d == null) {
                    throw new i("Header value is null");
                }
                com.agilebinary.a.a.b.k.b bVar2 = new com.agilebinary.a.a.b.k.b(d.length());
                bVar2.a(d);
                bVar = bVar2;
                i = 0;
            }
            while (i < bVar.c() && d.a(bVar.a(i))) {
                i++;
            }
            int i2 = i;
            while (i2 < bVar.c() && !d.a(bVar.a(i2))) {
                i2++;
            }
            hashMap.put(bVar.a(i, i2).toLowerCase(Locale.ENGLISH), cVar);
        }
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public List c(q qVar, e eVar) {
        return a();
    }
}
