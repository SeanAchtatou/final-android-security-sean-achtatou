package com.agilebinary.a.a.b.f.a;

import com.agilebinary.a.a.b.j.e;
import com.agilebinary.a.a.b.q;
import java.util.List;
import java.util.Map;

public class j extends a {
    public boolean a(q qVar, e eVar) {
        if (qVar != null) {
            return qVar.a().b() == 407;
        }
        throw new IllegalArgumentException("HTTP response may not be null");
    }

    public Map b(q qVar, e eVar) {
        if (qVar != null) {
            return a(qVar.b("Proxy-Authenticate"));
        }
        throw new IllegalArgumentException("HTTP response may not be null");
    }

    /* access modifiers changed from: protected */
    public List c(q qVar, e eVar) {
        List list = (List) qVar.f().a("http.auth.proxy-scheme-pref");
        return list != null ? list : super.c(qVar, eVar);
    }
}
