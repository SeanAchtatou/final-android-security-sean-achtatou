package com.agilebinary.a.a.b.f.c;

import com.agilebinary.a.a.b.d;
import com.agilebinary.a.a.b.h.c;
import com.agilebinary.a.a.b.h.e;
import com.agilebinary.a.a.b.h.t;
import com.agilebinary.a.a.b.k.b;
import com.agilebinary.a.a.b.v;
import java.util.ArrayList;

public class u {

    /* renamed from: a  reason: collision with root package name */
    public static final u f76a = new u();
    private static final char[] b = {';'};
    private final e c = e.f100a;

    public d a(b bVar, t tVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (tVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            v a2 = this.c.a(bVar, tVar, b);
            ArrayList arrayList = new ArrayList();
            while (!tVar.c()) {
                arrayList.add(this.c.a(bVar, tVar, b));
            }
            return new c(a2.a(), a2.b(), (v[]) arrayList.toArray(new v[arrayList.size()]));
        }
    }
}
