package com.agilebinary.a.a.b.h;

import com.agilebinary.a.a.b.c;
import com.agilebinary.a.a.b.f;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class p implements Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final List f109a = new ArrayList(16);

    public void a() {
        this.f109a.clear();
    }

    public void a(c cVar) {
        if (cVar != null) {
            this.f109a.add(cVar);
        }
    }

    public void a(c[] cVarArr) {
        a();
        if (cVarArr != null) {
            for (c add : cVarArr) {
                this.f109a.add(add);
            }
        }
    }

    public c[] a(String str) {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.f109a.size()) {
                return (c[]) arrayList.toArray(new c[arrayList.size()]);
            }
            c cVar = (c) this.f109a.get(i2);
            if (cVar.c().equalsIgnoreCase(str)) {
                arrayList.add(cVar);
            }
            i = i2 + 1;
        }
    }

    public c b(String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.f109a.size()) {
                return null;
            }
            c cVar = (c) this.f109a.get(i2);
            if (cVar.c().equalsIgnoreCase(str)) {
                return cVar;
            }
            i = i2 + 1;
        }
    }

    public void b(c cVar) {
        if (cVar != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.f109a.size()) {
                    this.f109a.add(cVar);
                    return;
                } else if (((c) this.f109a.get(i2)).c().equalsIgnoreCase(cVar.c())) {
                    this.f109a.set(i2, cVar);
                    return;
                } else {
                    i = i2 + 1;
                }
            }
        }
    }

    public c[] b() {
        return (c[]) this.f109a.toArray(new c[this.f109a.size()]);
    }

    public f c() {
        return new j(this.f109a, null);
    }

    public boolean c(String str) {
        for (int i = 0; i < this.f109a.size(); i++) {
            if (((c) this.f109a.get(i)).c().equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }

    public Object clone() {
        p pVar = (p) super.clone();
        pVar.f109a.clear();
        pVar.f109a.addAll(this.f109a);
        return pVar;
    }

    public f d(String str) {
        return new j(this.f109a, str);
    }

    public String toString() {
        return this.f109a.toString();
    }
}
