package com.agilebinary.a.a.b.c.a;

import com.agilebinary.a.a.b.c.b.b;
import com.agilebinary.a.a.b.i.d;
import com.agilebinary.a.a.b.l;
import java.net.InetAddress;

public class c {

    /* renamed from: a  reason: collision with root package name */
    public static final l f14a = new l("127.0.0.255", 0, "no-host");
    public static final b b = new b(f14a);

    public static l a(d dVar) {
        if (dVar == null) {
            throw new IllegalArgumentException("Parameters must not be null.");
        }
        l lVar = (l) dVar.a("http.route.default-proxy");
        if (lVar == null || !f14a.equals(lVar)) {
            return lVar;
        }
        return null;
    }

    public static b b(d dVar) {
        if (dVar == null) {
            throw new IllegalArgumentException("Parameters must not be null.");
        }
        b bVar = (b) dVar.a("http.route.forced-route");
        if (bVar == null || !b.equals(bVar)) {
            return bVar;
        }
        return null;
    }

    public static InetAddress c(d dVar) {
        if (dVar != null) {
            return (InetAddress) dVar.a("http.route.local-address");
        }
        throw new IllegalArgumentException("Parameters must not be null.");
    }
}
