package com.agilebinary.a.a.b.f.c;

import com.agilebinary.a.a.b.d.b;
import com.agilebinary.a.a.b.d.c;
import com.agilebinary.a.a.b.d.e;
import com.agilebinary.a.a.b.d.g;
import com.agilebinary.a.a.b.d.k;
import com.agilebinary.a.a.b.d.l;

public class f implements c {
    public void a(b bVar, e eVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            String a2 = eVar.a();
            String c = bVar.c();
            if (c == null) {
                throw new g("Cookie domain may not be null");
            } else if (a2.contains(".")) {
                if (!a2.endsWith(c)) {
                    if (c.startsWith(".")) {
                        c = c.substring(1, c.length());
                    }
                    if (!a2.equals(c)) {
                        throw new g("Illegal domain attribute \"" + c + "\". Domain of origin: \"" + a2 + "\"");
                    }
                }
            } else if (!a2.equals(c)) {
                throw new g("Illegal domain attribute \"" + c + "\". Domain of origin: \"" + a2 + "\"");
            }
        }
    }

    public void a(l lVar, String str) {
        if (lVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (str == null) {
            throw new k("Missing value for domain attribute");
        } else if (str.trim().length() == 0) {
            throw new k("Blank value for domain attribute");
        } else {
            lVar.d(str);
        }
    }

    public boolean b(b bVar, e eVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            String a2 = eVar.a();
            String c = bVar.c();
            if (c == null) {
                return false;
            }
            if (a2.equals(c)) {
                return true;
            }
            if (!c.startsWith(".")) {
                c = '.' + c;
            }
            return a2.endsWith(c) || a2.equals(c.substring(1));
        }
    }
}
