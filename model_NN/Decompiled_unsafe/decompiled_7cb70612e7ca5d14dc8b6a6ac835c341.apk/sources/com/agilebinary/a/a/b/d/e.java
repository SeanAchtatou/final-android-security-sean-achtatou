package com.agilebinary.a.a.b.d;

import java.util.Locale;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private final String f29a;
    private final int b;
    private final String c;
    private final boolean d;

    public e(String str, int i, String str2, boolean z) {
        if (str == null) {
            throw new IllegalArgumentException("Host of origin may not be null");
        } else if (str.trim().length() == 0) {
            throw new IllegalArgumentException("Host of origin may not be blank");
        } else if (i < 0) {
            throw new IllegalArgumentException("Invalid port: " + i);
        } else if (str2 == null) {
            throw new IllegalArgumentException("Path of origin may not be null.");
        } else {
            this.f29a = str.toLowerCase(Locale.ENGLISH);
            this.b = i;
            if (str2.trim().length() != 0) {
                this.c = str2;
            } else {
                this.c = "/";
            }
            this.d = z;
        }
    }

    public String a() {
        return this.f29a;
    }

    public String b() {
        return this.c;
    }

    public int c() {
        return this.b;
    }

    public boolean d() {
        return this.d;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        if (this.d) {
            sb.append("(secure)");
        }
        sb.append(this.f29a);
        sb.append(':');
        sb.append(Integer.toString(this.b));
        sb.append(this.c);
        sb.append(']');
        return sb.toString();
    }
}
