package com.agilebinary.a.a.b.f.a;

import com.agilebinary.a.a.b.b.a;
import com.agilebinary.a.a.b.l;
import java.util.HashMap;

public class c implements a {

    /* renamed from: a  reason: collision with root package name */
    private final HashMap f36a = new HashMap();

    public com.agilebinary.a.a.b.a.a a(l lVar) {
        if (lVar != null) {
            return (com.agilebinary.a.a.b.a.a) this.f36a.get(lVar);
        }
        throw new IllegalArgumentException("HTTP host may not be null");
    }

    public void a(l lVar, com.agilebinary.a.a.b.a.a aVar) {
        if (lVar == null) {
            throw new IllegalArgumentException("HTTP host may not be null");
        }
        this.f36a.put(lVar, aVar);
    }

    public void b(l lVar) {
        if (lVar == null) {
            throw new IllegalArgumentException("HTTP host may not be null");
        }
        this.f36a.remove(lVar);
    }

    public String toString() {
        return this.f36a.toString();
    }
}
