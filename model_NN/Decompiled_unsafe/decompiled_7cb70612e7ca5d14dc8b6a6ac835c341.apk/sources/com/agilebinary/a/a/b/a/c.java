package com.agilebinary.a.a.b.a;

import com.agilebinary.a.a.b.i.d;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private final ConcurrentHashMap f3a = new ConcurrentHashMap();

    public a a(String str, d dVar) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        b bVar = (b) this.f3a.get(str.toLowerCase(Locale.ENGLISH));
        if (bVar != null) {
            return bVar.a(dVar);
        }
        throw new IllegalStateException("Unsupported authentication scheme: " + str);
    }
}
