package com.agilebinary.a.a.b.h;

import com.agilebinary.a.a.b.c;
import com.agilebinary.a.a.b.d;
import java.io.Serializable;

public class b implements c, Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final String f97a;
    private final String b;

    public b(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Name may not be null");
        }
        this.f97a = str;
        this.b = str2;
    }

    public String c() {
        return this.f97a;
    }

    public Object clone() {
        return super.clone();
    }

    public String d() {
        return this.b;
    }

    public d[] e() {
        return this.b != null ? e.a(this.b, (q) null) : new d[0];
    }

    public String toString() {
        return h.f101a.a((com.agilebinary.a.a.b.k.b) null, this).toString();
    }
}
