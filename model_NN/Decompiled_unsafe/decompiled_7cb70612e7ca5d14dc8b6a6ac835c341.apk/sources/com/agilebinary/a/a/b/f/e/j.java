package com.agilebinary.a.a.b.f.e;

import com.agilebinary.a.a.b.g.f;
import com.agilebinary.a.a.b.h.s;
import com.agilebinary.a.a.b.h.t;
import com.agilebinary.a.a.b.i.d;
import com.agilebinary.a.a.b.k.b;
import com.agilebinary.a.a.b.n;
import com.agilebinary.a.a.b.r;
import com.agilebinary.a.a.b.w;

public class j extends a {
    private final r b;
    private final b c;

    public j(f fVar, s sVar, r rVar, d dVar) {
        super(fVar, sVar, dVar);
        if (rVar == null) {
            throw new IllegalArgumentException("Response factory may not be null");
        }
        this.b = rVar;
        this.c = new b(128);
    }

    /* access modifiers changed from: protected */
    public n a(f fVar) {
        this.c.a();
        if (fVar.a(this.c) == -1) {
            throw new w("The target server failed to respond");
        }
        return this.b.a(this.f83a.c(this.c, new t(0, this.c.c())), null);
    }
}
