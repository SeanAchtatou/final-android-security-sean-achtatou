package com.agilebinary.a.a.b.f.c;

import com.agilebinary.a.a.b.d.m;
import java.io.Serializable;
import java.util.Date;

public class d extends c implements m, Serializable {

    /* renamed from: a  reason: collision with root package name */
    private String f70a;
    private int[] b;
    private boolean c;

    public d(String str, String str2) {
        super(str, str2);
    }

    public void a(int[] iArr) {
        this.b = iArr;
    }

    public boolean a(Date date) {
        return this.c || super.a(date);
    }

    public void a_(String str) {
        this.f70a = str;
    }

    public void b(boolean z) {
        this.c = z;
    }

    public Object clone() {
        d dVar = (d) super.clone();
        dVar.b = (int[]) this.b.clone();
        return dVar;
    }

    public int[] e() {
        return this.b;
    }
}
