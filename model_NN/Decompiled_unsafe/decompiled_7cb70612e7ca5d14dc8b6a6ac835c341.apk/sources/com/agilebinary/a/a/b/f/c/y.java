package com.agilebinary.a.a.b.f.c;

import com.agilebinary.a.a.b.c;
import com.agilebinary.a.a.b.d.a;
import com.agilebinary.a.a.b.d.b;
import com.agilebinary.a.a.b.d.e;
import com.agilebinary.a.a.b.d.f;
import com.agilebinary.a.a.b.d.g;
import com.agilebinary.a.a.b.d.k;
import com.agilebinary.a.a.b.h.o;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class y extends o {

    /* renamed from: a  reason: collision with root package name */
    private static final f f78a = new f();
    private static final String[] b = {"EEE, dd MMM yyyy HH:mm:ss zzz", "EEEE, dd-MMM-yy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy"};
    private final String[] c;
    private final boolean d;

    public y() {
        this(null, false);
    }

    public y(String[] strArr, boolean z) {
        if (strArr != null) {
            this.c = (String[]) strArr.clone();
        } else {
            this.c = b;
        }
        this.d = z;
        a("version", new aa());
        a("path", new i());
        a("domain", new x());
        a("max-age", new h());
        a("secure", new j());
        a("comment", new e());
        a("expires", new g(this.c));
    }

    private List b(List list) {
        int i;
        int i2 = Integer.MAX_VALUE;
        Iterator it = list.iterator();
        while (true) {
            i = i2;
            if (!it.hasNext()) {
                break;
            }
            b bVar = (b) it.next();
            i2 = bVar.g() < i ? bVar.g() : i;
        }
        com.agilebinary.a.a.b.k.b bVar2 = new com.agilebinary.a.a.b.k.b(list.size() * 40);
        bVar2.a("Cookie");
        bVar2.a(": ");
        bVar2.a("$Version=");
        bVar2.a(Integer.toString(i));
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            bVar2.a("; ");
            a(bVar2, (b) it2.next(), i);
        }
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(new o(bVar2));
        return arrayList;
    }

    private List c(List list) {
        ArrayList arrayList = new ArrayList(list.size());
        Iterator it = list.iterator();
        while (it.hasNext()) {
            b bVar = (b) it.next();
            int g = bVar.g();
            com.agilebinary.a.a.b.k.b bVar2 = new com.agilebinary.a.a.b.k.b(40);
            bVar2.a("Cookie: ");
            bVar2.a("$Version=");
            bVar2.a(Integer.toString(g));
            bVar2.a("; ");
            a(bVar2, bVar, g);
            arrayList.add(new o(bVar2));
        }
        return arrayList;
    }

    public int a() {
        return 1;
    }

    public List a(c cVar, e eVar) {
        if (cVar == null) {
            throw new IllegalArgumentException("Header may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else if (cVar.c().equalsIgnoreCase("Set-Cookie")) {
            return a(cVar.e(), eVar);
        } else {
            throw new k("Unrecognized cookie header '" + cVar.toString() + "'");
        }
    }

    public List a(List list) {
        if (list == null) {
            throw new IllegalArgumentException("List of cookies may not be null");
        } else if (list.isEmpty()) {
            throw new IllegalArgumentException("List of cookies may not be empty");
        } else {
            if (list.size() > 1) {
                ArrayList arrayList = new ArrayList(list);
                Collections.sort(arrayList, f78a);
                list = arrayList;
            }
            return this.d ? b(list) : c(list);
        }
    }

    public void a(b bVar, e eVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        }
        String a2 = bVar.a();
        if (a2.indexOf(32) != -1) {
            throw new g("Cookie name may not contain blanks");
        } else if (a2.startsWith("$")) {
            throw new g("Cookie name may not start with $");
        } else {
            super.a(bVar, eVar);
        }
    }

    /* access modifiers changed from: protected */
    public void a(com.agilebinary.a.a.b.k.b bVar, b bVar2, int i) {
        a(bVar, bVar2.a(), bVar2.b(), i);
        if (bVar2.d() != null && (bVar2 instanceof a) && ((a) bVar2).b("path")) {
            bVar.a("; ");
            a(bVar, "$Path", bVar2.d(), i);
        }
        if (bVar2.c() != null && (bVar2 instanceof a) && ((a) bVar2).b("domain")) {
            bVar.a("; ");
            a(bVar, "$Domain", bVar2.c(), i);
        }
    }

    /* access modifiers changed from: protected */
    public void a(com.agilebinary.a.a.b.k.b bVar, String str, String str2, int i) {
        bVar.a(str);
        bVar.a("=");
        if (str2 == null) {
            return;
        }
        if (i > 0) {
            bVar.a('\"');
            bVar.a(str2);
            bVar.a('\"');
            return;
        }
        bVar.a(str2);
    }

    public c b() {
        return null;
    }

    public String toString() {
        return "rfc2109";
    }
}
