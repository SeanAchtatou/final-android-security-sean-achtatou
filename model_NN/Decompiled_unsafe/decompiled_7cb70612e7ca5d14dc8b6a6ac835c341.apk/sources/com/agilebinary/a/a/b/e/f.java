package com.agilebinary.a.a.b.e;

import com.agilebinary.a.a.b.c;
import com.agilebinary.a.a.b.i;
import java.io.InputStream;
import java.io.OutputStream;

public class f implements i {
    protected i c;

    public f(i iVar) {
        if (iVar == null) {
            throw new IllegalArgumentException("wrapped entity must not be null");
        }
        this.c = iVar;
    }

    public InputStream a() {
        return this.c.a();
    }

    public void a(OutputStream outputStream) {
        this.c.a(outputStream);
    }

    public long b() {
        return this.c.b();
    }

    public boolean c() {
        return this.c.c();
    }

    public boolean d() {
        return this.c.d();
    }

    public c e() {
        return this.c.e();
    }

    public c f() {
        return this.c.f();
    }

    public boolean g() {
        return this.c.g();
    }
}
