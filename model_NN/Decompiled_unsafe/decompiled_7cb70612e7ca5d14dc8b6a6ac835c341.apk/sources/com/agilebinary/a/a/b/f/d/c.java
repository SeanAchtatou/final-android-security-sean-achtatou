package com.agilebinary.a.a.b.f.d;

import com.agilebinary.a.a.b.d;
import com.agilebinary.a.a.b.e.e;
import com.agilebinary.a.a.b.n;
import com.agilebinary.a.a.b.x;
import com.agilebinary.a.a.b.y;

public class c implements e {
    public long a(n nVar) {
        long j;
        if (nVar == null) {
            throw new IllegalArgumentException("HTTP message may not be null");
        }
        boolean b = nVar.f().b("http.protocol.strict-transfer-encoding");
        com.agilebinary.a.a.b.c c = nVar.c("Transfer-Encoding");
        com.agilebinary.a.a.b.c c2 = nVar.c("Content-Length");
        if (c != null) {
            try {
                d[] e = c.e();
                if (b) {
                    int i = 0;
                    while (i < e.length) {
                        String a2 = e[i].a();
                        if (a2 == null || a2.length() <= 0 || a2.equalsIgnoreCase("chunked") || a2.equalsIgnoreCase("identity")) {
                            i++;
                        } else {
                            throw new y("Unsupported transfer encoding: " + a2);
                        }
                    }
                }
                int length = e.length;
                if ("identity".equalsIgnoreCase(c.d())) {
                    return -1;
                }
                if (length > 0 && "chunked".equalsIgnoreCase(e[length - 1].a())) {
                    return -2;
                }
                if (!b) {
                    return -1;
                }
                throw new y("Chunk-encoding must be the last one applied");
            } catch (x e2) {
                throw new y("Invalid Transfer-Encoding header value: " + c, e2);
            }
        } else if (c2 == null) {
            return -1;
        } else {
            com.agilebinary.a.a.b.c[] b2 = nVar.b("Content-Length");
            if (!b || b2.length <= 1) {
                int length2 = b2.length - 1;
                while (true) {
                    if (length2 < 0) {
                        j = -1;
                        break;
                    }
                    com.agilebinary.a.a.b.c cVar = b2[length2];
                    try {
                        j = Long.parseLong(cVar.d());
                        break;
                    } catch (NumberFormatException e3) {
                        if (b) {
                            throw new y("Invalid content length: " + cVar.d());
                        }
                        length2--;
                    }
                }
                if (j < 0) {
                    return -1;
                }
                return j;
            }
            throw new y("Multiple content length headers");
        }
    }
}
