package com.agilebinary.a.a.b.c.a;

import java.util.concurrent.ConcurrentHashMap;

public final class b implements a {

    /* renamed from: a  reason: collision with root package name */
    private final ConcurrentHashMap f13a;
    private volatile int b;

    public b() {
        this(2);
    }

    public b(int i) {
        this.f13a = new ConcurrentHashMap();
        a(i);
    }

    public int a(com.agilebinary.a.a.b.c.b.b bVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("HTTP route may not be null.");
        }
        Integer num = (Integer) this.f13a.get(bVar);
        return num != null ? num.intValue() : this.b;
    }

    public void a(int i) {
        if (i < 1) {
            throw new IllegalArgumentException("The maximum must be greater than 0.");
        }
        this.b = i;
    }

    public String toString() {
        return this.f13a.toString();
    }
}
