package com.agilebinary.a.a.b.f.c;

import com.agilebinary.a.a.b.d.b;
import com.agilebinary.a.a.b.d.e;
import com.agilebinary.a.a.b.d.l;

public class j extends a {
    public void a(l lVar, String str) {
        if (lVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        }
        lVar.a(true);
    }

    public boolean b(b bVar, e eVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (eVar != null) {
            return !bVar.f() || eVar.d();
        } else {
            throw new IllegalArgumentException("Cookie origin may not be null");
        }
    }
}
