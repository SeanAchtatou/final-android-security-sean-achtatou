package com.agilebinary.a.a.b.f.d;

import com.agilebinary.a.a.b.e.e;
import com.agilebinary.a.a.b.f.e.f;
import com.agilebinary.a.a.b.f.e.h;
import com.agilebinary.a.a.b.f.e.m;
import com.agilebinary.a.a.b.g.g;
import com.agilebinary.a.a.b.i;
import com.agilebinary.a.a.b.n;
import java.io.OutputStream;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private final e f81a;

    public b(e eVar) {
        if (eVar == null) {
            throw new IllegalArgumentException("Content length strategy may not be null");
        }
        this.f81a = eVar;
    }

    /* access modifiers changed from: protected */
    public OutputStream a(g gVar, n nVar) {
        long a2 = this.f81a.a(nVar);
        return a2 == -2 ? new f(gVar) : a2 == -1 ? new m(gVar) : new h(gVar, a2);
    }

    public void a(g gVar, n nVar, i iVar) {
        if (gVar == null) {
            throw new IllegalArgumentException("Session output buffer may not be null");
        } else if (nVar == null) {
            throw new IllegalArgumentException("HTTP message may not be null");
        } else if (iVar == null) {
            throw new IllegalArgumentException("HTTP entity may not be null");
        } else {
            OutputStream a2 = a(gVar, nVar);
            iVar.a(a2);
            a2.close();
        }
    }
}
