package com.agilebinary.a.a.b.h;

import com.agilebinary.a.a.b.c;
import com.agilebinary.a.a.b.f;
import com.agilebinary.a.a.b.i.b;
import com.agilebinary.a.a.b.i.d;
import com.agilebinary.a.a.b.n;

public abstract class a implements n {

    /* renamed from: a  reason: collision with root package name */
    protected p f96a;
    protected d b;

    protected a() {
        this(null);
    }

    protected a(d dVar) {
        this.f96a = new p();
        this.b = dVar;
    }

    public void a(c cVar) {
        this.f96a.a(cVar);
    }

    public void a(d dVar) {
        if (dVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        this.b = dVar;
    }

    public void a(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Header name may not be null");
        }
        this.f96a.a(new b(str, str2));
    }

    public void a(c[] cVarArr) {
        this.f96a.a(cVarArr);
    }

    public boolean a(String str) {
        return this.f96a.c(str);
    }

    public void b(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("Header name may not be null");
        }
        this.f96a.b(new b(str, str2));
    }

    public c[] b(String str) {
        return this.f96a.a(str);
    }

    public c c(String str) {
        return this.f96a.b(str);
    }

    public f d(String str) {
        return this.f96a.d(str);
    }

    public c[] d() {
        return this.f96a.b();
    }

    public f e() {
        return this.f96a.c();
    }

    public d f() {
        if (this.b == null) {
            this.b = new b();
        }
        return this.b;
    }
}
