package com.agilebinary.a.a.b.f.a;

import com.agilebinary.a.a.b.i.a;
import com.agilebinary.a.a.b.i.d;

public class f extends a {

    /* renamed from: a  reason: collision with root package name */
    protected final d f39a;
    protected final d b;
    protected final d c;
    protected final d d;

    public f(d dVar, d dVar2, d dVar3, d dVar4) {
        this.f39a = dVar;
        this.b = dVar2;
        this.c = dVar3;
        this.d = dVar4;
    }

    public d a(String str, Object obj) {
        throw new UnsupportedOperationException("Setting parameters in a stack is not supported.");
    }

    public Object a(String str) {
        if (str == null) {
            throw new IllegalArgumentException("Parameter name must not be null.");
        }
        Object obj = null;
        if (this.d != null) {
            obj = this.d.a(str);
        }
        if (obj == null && this.c != null) {
            obj = this.c.a(str);
        }
        if (obj == null && this.b != null) {
            obj = this.b.a(str);
        }
        return (obj != null || this.f39a == null) ? obj : this.f39a.a(str);
    }
}
