package com.agilebinary.a.a.b.h;

import com.agilebinary.a.a.b.ad;
import com.agilebinary.a.a.b.f;
import com.agilebinary.a.a.b.x;
import java.util.NoSuchElementException;

public class n implements ad {

    /* renamed from: a  reason: collision with root package name */
    protected final f f107a;
    protected String b;
    protected String c;
    protected int d;

    public n(f fVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("Header iterator must not be null.");
        }
        this.f107a = fVar;
        this.d = a(-1);
    }

    /* access modifiers changed from: protected */
    public int a(int i) {
        int c2;
        if (i >= 0) {
            c2 = c(i);
        } else if (!this.f107a.hasNext()) {
            return -1;
        } else {
            this.b = this.f107a.a().d();
            c2 = 0;
        }
        int b2 = b(c2);
        if (b2 < 0) {
            this.c = null;
            return -1;
        }
        int d2 = d(b2);
        this.c = a(this.b, b2, d2);
        return d2;
    }

    public String a() {
        if (this.c == null) {
            throw new NoSuchElementException("Iteration already finished.");
        }
        String str = this.c;
        this.d = a(this.d);
        return str;
    }

    /* access modifiers changed from: protected */
    public String a(String str, int i, int i2) {
        return str.substring(i, i2);
    }

    /* access modifiers changed from: protected */
    public boolean a(char c2) {
        return c2 == ',';
    }

    /* access modifiers changed from: protected */
    public int b(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Search position must not be negative: " + i);
        }
        boolean z = false;
        int i2 = i;
        while (!z && this.b != null) {
            int length = this.b.length();
            boolean z2 = z;
            int i3 = i2;
            boolean z3 = z2;
            while (!z3 && i3 < length) {
                char charAt = this.b.charAt(i3);
                if (a(charAt) || b(charAt)) {
                    i3++;
                } else if (c(this.b.charAt(i3))) {
                    z3 = true;
                } else {
                    throw new x("Invalid character before token (pos " + i3 + "): " + this.b);
                }
            }
            if (!z3) {
                if (this.f107a.hasNext()) {
                    this.b = this.f107a.a().d();
                    i3 = 0;
                } else {
                    this.b = null;
                }
            }
            boolean z4 = z3;
            i2 = i3;
            z = z4;
        }
        if (z) {
            return i2;
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public boolean b(char c2) {
        return c2 == 9 || Character.isSpaceChar(c2);
    }

    /* access modifiers changed from: protected */
    public int c(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Search position must not be negative: " + i);
        }
        boolean z = false;
        int length = this.b.length();
        int i2 = i;
        while (!z && i2 < length) {
            char charAt = this.b.charAt(i2);
            if (a(charAt)) {
                z = true;
            } else if (b(charAt)) {
                i2++;
            } else if (c(charAt)) {
                throw new x("Tokens without separator (pos " + i2 + "): " + this.b);
            } else {
                throw new x("Invalid character after token (pos " + i2 + "): " + this.b);
            }
        }
        return i2;
    }

    /* access modifiers changed from: protected */
    public boolean c(char c2) {
        if (Character.isLetterOrDigit(c2)) {
            return true;
        }
        if (Character.isISOControl(c2)) {
            return false;
        }
        return !d(c2);
    }

    /* access modifiers changed from: protected */
    public int d(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Token start position must not be negative: " + i);
        }
        int length = this.b.length();
        int i2 = i + 1;
        while (i2 < length && c(this.b.charAt(i2))) {
            i2++;
        }
        return i2;
    }

    /* access modifiers changed from: protected */
    public boolean d(char c2) {
        return " ,;=()<>@:\\\"/[]?{}\t".indexOf(c2) >= 0;
    }

    public boolean hasNext() {
        return this.c != null;
    }

    public final Object next() {
        return a();
    }

    public final void remove() {
        throw new UnsupportedOperationException("Removing tokens is not supported.");
    }
}
