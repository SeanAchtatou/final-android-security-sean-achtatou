package com.agilebinary.a.a.b.f.b.a;

import com.agilebinary.a.a.b.c.d;
import com.agilebinary.a.a.b.c.n;
import java.util.concurrent.TimeUnit;

public class b extends com.agilebinary.a.a.b.f.b.b {
    private final long f;
    private long g;
    private long h;
    private long i;

    public b(d dVar, com.agilebinary.a.a.b.c.b.b bVar, long j, TimeUnit timeUnit) {
        super(dVar, bVar);
        if (bVar == null) {
            throw new IllegalArgumentException("HTTP route may not be null");
        }
        this.f = System.currentTimeMillis();
        if (j > 0) {
            this.h = this.f + timeUnit.toMillis(j);
        } else {
            this.h = Long.MAX_VALUE;
        }
        this.i = this.h;
    }

    public void a(long j, TimeUnit timeUnit) {
        this.g = System.currentTimeMillis();
        this.i = Math.min(this.h, j > 0 ? this.g + timeUnit.toMillis(j) : Long.MAX_VALUE);
    }

    public boolean a(long j) {
        return j >= this.i;
    }

    /* access modifiers changed from: protected */
    public void b() {
        super.b();
    }

    /* access modifiers changed from: protected */
    public final n c() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public final com.agilebinary.a.a.b.c.b.b d() {
        return this.c;
    }

    public long e() {
        return this.g;
    }
}
