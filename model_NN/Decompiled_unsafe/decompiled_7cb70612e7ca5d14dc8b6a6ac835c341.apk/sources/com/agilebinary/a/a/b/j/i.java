package com.agilebinary.a.a.b.j;

import com.agilebinary.a.a.b.j;
import com.agilebinary.a.a.b.o;
import com.agilebinary.a.a.b.p;
import com.agilebinary.a.a.b.t;
import com.agilebinary.a.a.b.y;
import com.agilebinary.a.a.b.z;

public class i implements p {
    public void a(o oVar, e eVar) {
        if (oVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (!(oVar instanceof j)) {
        } else {
            if (oVar.a("Transfer-Encoding")) {
                throw new y("Transfer-encoding header already present");
            } else if (oVar.a("Content-Length")) {
                throw new y("Content-Length header already present");
            } else {
                z b = oVar.g().b();
                com.agilebinary.a.a.b.i b2 = ((j) oVar).b();
                if (b2 == null) {
                    oVar.a("Content-Length", "0");
                    return;
                }
                if (!b2.d() && b2.b() >= 0) {
                    oVar.a("Content-Length", Long.toString(b2.b()));
                } else if (b.c(t.b)) {
                    throw new y("Chunked transfer encoding not allowed for " + b);
                } else {
                    oVar.a("Transfer-Encoding", "chunked");
                }
                if (b2.e() != null && !oVar.a("Content-Type")) {
                    oVar.a(b2.e());
                }
                if (b2.f() != null && !oVar.a("Content-Encoding")) {
                    oVar.a(b2.f());
                }
            }
        }
    }
}
