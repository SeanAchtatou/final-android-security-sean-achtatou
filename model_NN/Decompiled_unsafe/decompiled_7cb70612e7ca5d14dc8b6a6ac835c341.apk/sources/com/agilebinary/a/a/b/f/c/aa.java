package com.agilebinary.a.a.b.f.c;

import com.agilebinary.a.a.b.d.b;
import com.agilebinary.a.a.b.d.e;
import com.agilebinary.a.a.b.d.g;
import com.agilebinary.a.a.b.d.k;
import com.agilebinary.a.a.b.d.l;

public class aa extends a {
    public void a(b bVar, e eVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (bVar.g() < 0) {
            throw new g("Cookie version may not be negative");
        }
    }

    public void a(l lVar, String str) {
        if (lVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (str == null) {
            throw new k("Missing value for version attribute");
        } else if (str.trim().length() == 0) {
            throw new k("Blank value for version attribute");
        } else {
            try {
                lVar.a(Integer.parseInt(str));
            } catch (NumberFormatException e) {
                throw new k("Invalid version: " + e.getMessage());
            }
        }
    }
}
