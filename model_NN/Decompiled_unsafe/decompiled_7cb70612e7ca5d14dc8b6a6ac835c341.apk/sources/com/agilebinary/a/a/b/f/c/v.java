package com.agilebinary.a.a.b.f.c;

import com.agilebinary.a.a.b.c;
import com.agilebinary.a.a.b.d;
import com.agilebinary.a.a.b.d.e;
import com.agilebinary.a.a.b.d.k;
import com.agilebinary.a.a.b.h.o;
import com.agilebinary.a.a.b.h.t;
import com.agilebinary.a.a.b.k.b;
import java.util.ArrayList;
import java.util.List;

public class v extends o {

    /* renamed from: a  reason: collision with root package name */
    private final String[] f77a;

    public v() {
        this(null);
    }

    public v(String[] strArr) {
        if (strArr != null) {
            this.f77a = (String[]) strArr.clone();
        } else {
            this.f77a = new String[]{"EEE, dd-MMM-yy HH:mm:ss z"};
        }
        a("path", new i());
        a("domain", new t());
        a("max-age", new h());
        a("secure", new j());
        a("comment", new e());
        a("expires", new g(this.f77a));
    }

    public int a() {
        return 0;
    }

    public List a(c cVar, e eVar) {
        b bVar;
        t tVar;
        if (cVar == null) {
            throw new IllegalArgumentException("Header may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else if (!cVar.c().equalsIgnoreCase("Set-Cookie")) {
            throw new k("Unrecognized cookie header '" + cVar.toString() + "'");
        } else {
            u uVar = u.f76a;
            if (cVar instanceof com.agilebinary.a.a.b.b) {
                bVar = ((com.agilebinary.a.a.b.b) cVar).a();
                tVar = new t(((com.agilebinary.a.a.b.b) cVar).b(), bVar.c());
            } else {
                String d = cVar.d();
                if (d == null) {
                    throw new k("Header value is null");
                }
                bVar = new b(d.length());
                bVar.a(d);
                tVar = new t(0, bVar.c());
            }
            return a(new d[]{uVar.a(bVar, tVar)}, eVar);
        }
    }

    public List a(List list) {
        if (list == null) {
            throw new IllegalArgumentException("List of cookies may not be null");
        } else if (list.isEmpty()) {
            throw new IllegalArgumentException("List of cookies may not be empty");
        } else {
            b bVar = new b(list.size() * 20);
            bVar.a("Cookie");
            bVar.a(": ");
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < list.size()) {
                    com.agilebinary.a.a.b.d.b bVar2 = (com.agilebinary.a.a.b.d.b) list.get(i2);
                    if (i2 > 0) {
                        bVar.a("; ");
                    }
                    bVar.a(bVar2.a());
                    String b = bVar2.b();
                    if (b != null) {
                        bVar.a("=");
                        bVar.a(b);
                    }
                    i = i2 + 1;
                } else {
                    ArrayList arrayList = new ArrayList(1);
                    arrayList.add(new o(bVar));
                    return arrayList;
                }
            }
        }
    }

    public c b() {
        return null;
    }

    public String toString() {
        return "netscape";
    }
}
