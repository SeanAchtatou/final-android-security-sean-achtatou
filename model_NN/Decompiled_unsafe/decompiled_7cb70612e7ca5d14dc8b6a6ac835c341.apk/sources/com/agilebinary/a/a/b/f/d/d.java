package com.agilebinary.a.a.b.f.d;

import com.agilebinary.a.a.b.c;
import com.agilebinary.a.a.b.e.e;
import com.agilebinary.a.a.b.n;
import com.agilebinary.a.a.b.t;
import com.agilebinary.a.a.b.y;

public class d implements e {
    public long a(n nVar) {
        if (nVar == null) {
            throw new IllegalArgumentException("HTTP message may not be null");
        }
        c c = nVar.c("Transfer-Encoding");
        c c2 = nVar.c("Content-Length");
        if (c != null) {
            String d = c.d();
            if ("chunked".equalsIgnoreCase(d)) {
                if (!nVar.c().c(t.b)) {
                    return -2;
                }
                throw new y("Chunked transfer encoding not allowed for " + nVar.c());
            } else if ("identity".equalsIgnoreCase(d)) {
                return -1;
            } else {
                throw new y("Unsupported transfer encoding: " + d);
            }
        } else if (c2 == null) {
            return -1;
        } else {
            String d2 = c2.d();
            try {
                return Long.parseLong(d2);
            } catch (NumberFormatException e) {
                throw new y("Invalid content length: " + d2);
            }
        }
    }
}
