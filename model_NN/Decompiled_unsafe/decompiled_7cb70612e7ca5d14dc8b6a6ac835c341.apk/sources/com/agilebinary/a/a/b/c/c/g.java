package com.agilebinary.a.a.b.c.c;

import com.agilebinary.a.a.b.l;
import java.util.concurrent.ConcurrentHashMap;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    private final ConcurrentHashMap f22a = new ConcurrentHashMap();

    public final f a(f fVar) {
        if (fVar != null) {
            return (f) this.f22a.put(fVar.c(), fVar);
        }
        throw new IllegalArgumentException("Scheme must not be null.");
    }

    public final f a(l lVar) {
        if (lVar != null) {
            return a(lVar.c());
        }
        throw new IllegalArgumentException("Host must not be null.");
    }

    public final f a(String str) {
        f b = b(str);
        if (b != null) {
            return b;
        }
        throw new IllegalStateException("Scheme '" + str + "' not registered.");
    }

    public final f b(String str) {
        if (str != null) {
            return (f) this.f22a.get(str);
        }
        throw new IllegalArgumentException("Name must not be null.");
    }
}
