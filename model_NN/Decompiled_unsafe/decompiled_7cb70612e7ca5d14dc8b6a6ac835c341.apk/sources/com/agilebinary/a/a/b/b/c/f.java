package com.agilebinary.a.a.b.b.c;

import com.agilebinary.a.a.a.a.a;
import com.agilebinary.a.a.b.a.g;
import com.agilebinary.a.a.b.a.h;
import com.agilebinary.a.a.b.j.e;
import com.agilebinary.a.a.b.o;
import com.agilebinary.a.a.b.p;
import org.apache.commons.logging.Log;

public class f implements p {

    /* renamed from: a  reason: collision with root package name */
    private final Log f9a = a.a(getClass());

    public void a(o oVar, e eVar) {
        com.agilebinary.a.a.b.a.e eVar2;
        com.agilebinary.a.a.b.a.a c;
        if (oVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else if (!oVar.g().a().equalsIgnoreCase("CONNECT") && !oVar.a("Authorization") && (eVar2 = (com.agilebinary.a.a.b.a.e) eVar.a("http.auth.target-scope")) != null && (c = eVar2.c()) != null) {
            h d = eVar2.d();
            if (d == null) {
                this.f9a.debug("User credentials not available");
            } else if (eVar2.e() != null || !c.c()) {
                try {
                    oVar.a(c instanceof g ? ((g) c).a(d, oVar, eVar) : c.a(d, oVar));
                } catch (com.agilebinary.a.a.b.a.f e) {
                    if (this.f9a.isErrorEnabled()) {
                        this.f9a.error("Authentication error: " + e.getMessage());
                    }
                }
            }
        }
    }
}
