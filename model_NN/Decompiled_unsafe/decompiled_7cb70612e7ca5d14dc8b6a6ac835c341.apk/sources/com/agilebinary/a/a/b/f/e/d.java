package com.agilebinary.a.a.b.f.e;

import com.agilebinary.a.a.b.g.a;
import com.agilebinary.a.a.b.g.g;
import com.agilebinary.a.a.b.i.e;
import com.agilebinary.a.a.b.k.b;
import java.io.OutputStream;

public abstract class d implements a, g {

    /* renamed from: a  reason: collision with root package name */
    private static final byte[] f86a = {13, 10};
    private OutputStream b;
    private com.agilebinary.a.a.b.k.a c;
    private String d = "US-ASCII";
    private boolean e = true;
    private int f = 512;
    private k g;

    public void a() {
        d();
        this.b.flush();
    }

    public void a(int i) {
        if (this.c.g()) {
            d();
        }
        this.c.a(i);
    }

    public void a(b bVar) {
        if (bVar != null) {
            if (this.e) {
                int i = 0;
                int c2 = bVar.c();
                while (c2 > 0) {
                    int min = Math.min(this.c.c() - this.c.d(), c2);
                    if (min > 0) {
                        this.c.a(bVar, i, min);
                    }
                    if (this.c.g()) {
                        d();
                    }
                    i += min;
                    c2 -= min;
                }
            } else {
                a(bVar.toString().getBytes(this.d));
            }
            a(f86a);
        }
    }

    /* access modifiers changed from: protected */
    public void a(OutputStream outputStream, int i, com.agilebinary.a.a.b.i.d dVar) {
        if (outputStream == null) {
            throw new IllegalArgumentException("Input stream may not be null");
        } else if (i <= 0) {
            throw new IllegalArgumentException("Buffer size may not be negative or zero");
        } else if (dVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            this.b = outputStream;
            this.c = new com.agilebinary.a.a.b.k.a(i);
            this.d = e.a(dVar);
            this.e = this.d.equalsIgnoreCase("US-ASCII") || this.d.equalsIgnoreCase("ASCII");
            this.f = dVar.a("http.connection.min-chunk-limit", 512);
            this.g = c();
        }
    }

    public void a(String str) {
        if (str != null) {
            if (str.length() > 0) {
                a(str.getBytes(this.d));
            }
            a(f86a);
        }
    }

    public void a(byte[] bArr) {
        if (bArr != null) {
            a(bArr, 0, bArr.length);
        }
    }

    public void a(byte[] bArr, int i, int i2) {
        if (bArr != null) {
            if (i2 > this.f || i2 > this.c.c()) {
                d();
                this.b.write(bArr, i, i2);
                this.g.a((long) i2);
                return;
            }
            if (i2 > this.c.c() - this.c.d()) {
                d();
            }
            this.c.a(bArr, i, i2);
        }
    }

    public com.agilebinary.a.a.b.g.e b() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public k c() {
        return new k();
    }

    /* access modifiers changed from: protected */
    public void d() {
        int d2 = this.c.d();
        if (d2 > 0) {
            this.b.write(this.c.e(), 0, d2);
            this.c.a();
            this.g.a((long) d2);
        }
    }

    public int e() {
        return this.c.d();
    }
}
