package com.agilebinary.a.a.b.f.b;

import com.agilebinary.a.a.b.c.b.h;
import com.agilebinary.a.a.b.c.d;
import com.agilebinary.a.a.b.c.n;
import com.agilebinary.a.a.b.j.e;
import com.agilebinary.a.a.b.l;
import java.io.IOException;

public abstract class b {

    /* renamed from: a  reason: collision with root package name */
    protected final d f55a;
    protected final n b;
    protected volatile com.agilebinary.a.a.b.c.b.b c;
    protected volatile Object d;
    protected volatile h e;

    protected b(d dVar, com.agilebinary.a.a.b.c.b.b bVar) {
        if (dVar == null) {
            throw new IllegalArgumentException("Connection operator may not be null");
        }
        this.f55a = dVar;
        this.b = dVar.a();
        this.c = bVar;
        this.e = null;
    }

    public Object a() {
        return this.d;
    }

    public void a(com.agilebinary.a.a.b.c.b.b bVar, e eVar, com.agilebinary.a.a.b.i.d dVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Route must not be null.");
        } else if (dVar == null) {
            throw new IllegalArgumentException("Parameters must not be null.");
        } else if (this.e == null || !this.e.d()) {
            this.e = new h(bVar);
            l d2 = bVar.d();
            this.f55a.a(this.b, d2 != null ? d2 : bVar.a(), bVar.b(), eVar, dVar);
            h hVar = this.e;
            if (hVar == null) {
                throw new IOException("Request aborted");
            } else if (d2 == null) {
                hVar.a(this.b.i());
            } else {
                hVar.a(d2, this.b.i());
            }
        } else {
            throw new IllegalStateException("Connection already open.");
        }
    }

    public void a(e eVar, com.agilebinary.a.a.b.i.d dVar) {
        if (dVar == null) {
            throw new IllegalArgumentException("Parameters must not be null.");
        } else if (this.e == null || !this.e.d()) {
            throw new IllegalStateException("Connection not open.");
        } else if (!this.e.e()) {
            throw new IllegalStateException("Protocol layering without a tunnel not supported.");
        } else if (this.e.f()) {
            throw new IllegalStateException("Multiple protocol layering not supported.");
        } else {
            this.f55a.a(this.b, this.e.a(), eVar, dVar);
            this.e.c(this.b.i());
        }
    }

    public void a(l lVar, boolean z, com.agilebinary.a.a.b.i.d dVar) {
        if (lVar == null) {
            throw new IllegalArgumentException("Next proxy must not be null.");
        } else if (dVar == null) {
            throw new IllegalArgumentException("Parameters must not be null.");
        } else if (this.e == null || !this.e.d()) {
            throw new IllegalStateException("Connection not open.");
        } else {
            this.b.a(null, lVar, z, dVar);
            this.e.b(lVar, z);
        }
    }

    public void a(Object obj) {
        this.d = obj;
    }

    public void a(boolean z, com.agilebinary.a.a.b.i.d dVar) {
        if (dVar == null) {
            throw new IllegalArgumentException("Parameters must not be null.");
        } else if (this.e == null || !this.e.d()) {
            throw new IllegalStateException("Connection not open.");
        } else if (this.e.e()) {
            throw new IllegalStateException("Connection is already tunnelled.");
        } else {
            this.b.a(null, this.e.a(), z, dVar);
            this.e.b(z);
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.e = null;
        this.d = null;
    }
}
