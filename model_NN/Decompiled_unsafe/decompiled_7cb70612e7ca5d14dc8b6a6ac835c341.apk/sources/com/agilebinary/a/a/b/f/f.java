package com.agilebinary.a.a.b.f;

import com.agilebinary.a.a.b.f.e.n;
import com.agilebinary.a.a.b.f.e.o;
import com.agilebinary.a.a.b.g.g;
import com.agilebinary.a.a.b.i.c;
import com.agilebinary.a.a.b.i.d;
import com.agilebinary.a.a.b.m;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;

public class f extends a implements m {

    /* renamed from: a  reason: collision with root package name */
    private volatile boolean f95a;
    private volatile Socket b = null;

    /* access modifiers changed from: protected */
    public com.agilebinary.a.a.b.g.f a(Socket socket, int i, d dVar) {
        return new n(socket, i, dVar);
    }

    /* access modifiers changed from: protected */
    public void a(Socket socket, d dVar) {
        if (socket == null) {
            throw new IllegalArgumentException("Socket may not be null");
        } else if (dVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            this.b = socket;
            int d = c.d(dVar);
            a(a(socket, d, dVar), b(socket, d, dVar), dVar);
            this.f95a = true;
        }
    }

    /* access modifiers changed from: protected */
    public g b(Socket socket, int i, d dVar) {
        return new o(socket, i, dVar);
    }

    public void b(int i) {
        k();
        if (this.b != null) {
            try {
                this.b.setSoTimeout(i);
            } catch (SocketException e) {
            }
        }
    }

    public void c() {
        if (this.f95a) {
            this.f95a = false;
            Socket socket = this.b;
            try {
                o();
                try {
                    socket.shutdownOutput();
                } catch (IOException e) {
                }
                try {
                    socket.shutdownInput();
                } catch (IOException | UnsupportedOperationException e2) {
                }
            } finally {
                socket.close();
            }
        }
    }

    public boolean d() {
        return this.f95a;
    }

    public void f() {
        this.f95a = false;
        Socket socket = this.b;
        if (socket != null) {
            socket.close();
        }
    }

    public InetAddress g() {
        if (this.b != null) {
            return this.b.getInetAddress();
        }
        return null;
    }

    public int h() {
        if (this.b != null) {
            return this.b.getPort();
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public Socket j() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void k() {
        if (!this.f95a) {
            throw new IllegalStateException("Connection is not open");
        }
    }

    /* access modifiers changed from: protected */
    public void q() {
        if (this.f95a) {
            throw new IllegalStateException("Connection is already open");
        }
    }
}
