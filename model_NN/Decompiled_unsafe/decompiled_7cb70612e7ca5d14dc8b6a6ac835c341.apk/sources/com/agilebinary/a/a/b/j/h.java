package com.agilebinary.a.a.b.j;

import com.agilebinary.a.a.b.o;
import com.agilebinary.a.a.b.p;
import com.agilebinary.a.a.b.q;
import com.agilebinary.a.a.b.s;

public final class h implements f {

    /* renamed from: a  reason: collision with root package name */
    private final p[] f115a;
    private final s[] b;

    public h(p[] pVarArr, s[] sVarArr) {
        if (pVarArr != null) {
            int length = pVarArr.length;
            this.f115a = new p[length];
            for (int i = 0; i < length; i++) {
                this.f115a[i] = pVarArr[i];
            }
        } else {
            this.f115a = new p[0];
        }
        if (sVarArr != null) {
            int length2 = sVarArr.length;
            this.b = new s[length2];
            for (int i2 = 0; i2 < length2; i2++) {
                this.b[i2] = sVarArr[i2];
            }
            return;
        }
        this.b = new s[0];
    }

    public void a(o oVar, e eVar) {
        for (p a2 : this.f115a) {
            a2.a(oVar, eVar);
        }
    }

    public void a(q qVar, e eVar) {
        for (s a2 : this.b) {
            a2.a(qVar, eVar);
        }
    }
}
