package com.agilebinary.a.a.b.b.b;

import com.agilebinary.a.a.b.i.d;

public class a {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.b.i.d.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.b.i.d.a(java.lang.String, int):int
      com.agilebinary.a.a.b.i.d.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.b.i.d
      com.agilebinary.a.a.b.i.d.a(java.lang.String, boolean):boolean */
    public static boolean a(d dVar) {
        if (dVar != null) {
            return dVar.a("http.protocol.handle-redirects", true);
        }
        throw new IllegalArgumentException("HTTP parameters may not be null");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.b.i.d.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.b.i.d.a(java.lang.String, int):int
      com.agilebinary.a.a.b.i.d.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.b.i.d
      com.agilebinary.a.a.b.i.d.a(java.lang.String, boolean):boolean */
    public static boolean b(d dVar) {
        if (dVar != null) {
            return dVar.a("http.protocol.handle-authentication", true);
        }
        throw new IllegalArgumentException("HTTP parameters may not be null");
    }

    public static String c(d dVar) {
        if (dVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        String str = (String) dVar.a("http.protocol.cookie-policy");
        return str == null ? "best-match" : str;
    }
}
