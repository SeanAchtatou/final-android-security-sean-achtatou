package com.agilebinary.a.a.b.f.c;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public final class q {

    /* renamed from: a  reason: collision with root package name */
    public static final TimeZone f74a = TimeZone.getTimeZone("GMT");
    private static final String[] b = {"EEEE, dd-MMM-yy HH:mm:ss zzz", "EEE, dd MMM yyyy HH:mm:ss zzz", "EEE MMM d HH:mm:ss yyyy"};
    private static final Date c;

    static {
        Calendar instance = Calendar.getInstance();
        instance.setTimeZone(f74a);
        instance.set(2000, 0, 1, 0, 0, 0);
        instance.set(14, 0);
        c = instance.getTime();
    }

    public static Date a(String str, String[] strArr) {
        return a(str, strArr, null);
    }

    public static Date a(String str, String[] strArr, Date date) {
        if (str == null) {
            throw new IllegalArgumentException("dateValue is null");
        }
        if (strArr == null) {
            strArr = b;
        }
        if (date == null) {
            date = c;
        }
        if (str.length() > 1 && str.startsWith("'") && str.endsWith("'")) {
            str = str.substring(1, str.length() - 1);
        }
        int length = strArr.length;
        int i = 0;
        while (i < length) {
            SimpleDateFormat a2 = r.a(strArr[i]);
            a2.set2DigitYearStart(date);
            try {
                return a2.parse(str);
            } catch (ParseException e) {
                i++;
            }
        }
        throw new p("Unable to parse the date " + str);
    }
}
