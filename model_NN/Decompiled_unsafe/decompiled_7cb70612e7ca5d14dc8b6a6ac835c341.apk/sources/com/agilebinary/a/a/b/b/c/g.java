package com.agilebinary.a.a.b.b.c;

import com.agilebinary.a.a.a.a.a;
import com.agilebinary.a.a.b.a.e;
import com.agilebinary.a.a.b.l;
import com.agilebinary.a.a.b.q;
import com.agilebinary.a.a.b.s;
import org.apache.commons.logging.Log;

public class g implements s {

    /* renamed from: a  reason: collision with root package name */
    private final Log f10a = a.a(getClass());

    private void a(com.agilebinary.a.a.b.b.a aVar, l lVar, e eVar) {
        com.agilebinary.a.a.b.a.a c;
        if (eVar != null && (c = eVar.c()) != null && c.d()) {
            String a2 = c.a();
            if ((!a2.equalsIgnoreCase("Basic") && !a2.equalsIgnoreCase("Digest")) || eVar.e() == null) {
                return;
            }
            if (eVar.d() != null) {
                if (this.f10a.isDebugEnabled()) {
                    this.f10a.debug("Caching '" + a2 + "' auth scheme for " + lVar);
                }
                aVar.a(lVar, c);
                return;
            }
            aVar.b(lVar);
        }
    }

    public void a(q qVar, com.agilebinary.a.a.b.j.e eVar) {
        if (qVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else {
            com.agilebinary.a.a.b.b.a aVar = (com.agilebinary.a.a.b.b.a) eVar.a("http.auth.auth-cache");
            if (aVar != null) {
                a(aVar, (l) eVar.a("http.target_host"), (e) eVar.a("http.auth.target-scope"));
                a(aVar, (l) eVar.a("http.proxy_host"), (e) eVar.a("http.auth.proxy-scope"));
            }
        }
    }
}
