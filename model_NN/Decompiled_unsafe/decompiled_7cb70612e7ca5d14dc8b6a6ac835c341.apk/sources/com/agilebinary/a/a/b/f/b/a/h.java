package com.agilebinary.a.a.b.f.b.a;

import com.agilebinary.a.a.a.a.a;
import com.agilebinary.a.a.b.c.b;
import com.agilebinary.a.a.b.c.c.g;
import com.agilebinary.a.a.b.c.d;
import com.agilebinary.a.a.b.c.e;
import com.agilebinary.a.a.b.f.b.f;
import java.util.concurrent.TimeUnit;
import org.apache.commons.logging.Log;

public class h implements b {

    /* renamed from: a  reason: collision with root package name */
    protected final g f51a;
    @Deprecated
    protected final a b;
    protected final d c;
    protected final d d;
    protected final com.agilebinary.a.a.b.c.a.b e;
    /* access modifiers changed from: private */
    public final Log f;

    public h(g gVar, long j, TimeUnit timeUnit) {
        if (gVar == null) {
            throw new IllegalArgumentException("Scheme registry may not be null");
        }
        this.f = a.a(getClass());
        this.f51a = gVar;
        this.e = new com.agilebinary.a.a.b.c.a.b();
        this.d = a(gVar);
        this.c = b(j, timeUnit);
        this.b = this.c;
    }

    public g a() {
        return this.f51a;
    }

    /* access modifiers changed from: protected */
    public d a(g gVar) {
        return new f(gVar);
    }

    public e a(com.agilebinary.a.a.b.c.b.b bVar, Object obj) {
        return new i(this, this.c.a(bVar, obj), bVar);
    }

    public void a(int i) {
        this.c.a(i);
    }

    public void a(long j, TimeUnit timeUnit) {
        if (this.f.isDebugEnabled()) {
            this.f.debug("Closing connections idle longer than " + j + " " + timeUnit);
        }
        this.c.a(j, timeUnit);
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:39:0x0078=Splitter:B:39:0x0078, B:21:0x003c=Splitter:B:21:0x003c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.agilebinary.a.a.b.c.m r8, long r9, java.util.concurrent.TimeUnit r11) {
        /*
            r7 = this;
            boolean r0 = r8 instanceof com.agilebinary.a.a.b.f.b.a.c
            if (r0 != 0) goto L_0x000c
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Connection class mismatch, connection not obtained from this manager."
            r0.<init>(r1)
            throw r0
        L_0x000c:
            com.agilebinary.a.a.b.f.b.a.c r8 = (com.agilebinary.a.a.b.f.b.a.c) r8
            com.agilebinary.a.a.b.f.b.b r0 = r8.t()
            if (r0 == 0) goto L_0x0022
            com.agilebinary.a.a.b.c.b r0 = r8.p()
            if (r0 == r7) goto L_0x0022
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "Connection not obtained from this manager."
            r0.<init>(r1)
            throw r0
        L_0x0022:
            monitor-enter(r8)
            com.agilebinary.a.a.b.f.b.b r1 = r8.t()     // Catch:{ all -> 0x005d }
            com.agilebinary.a.a.b.f.b.a.b r1 = (com.agilebinary.a.a.b.f.b.a.b) r1     // Catch:{ all -> 0x005d }
            if (r1 != 0) goto L_0x002d
            monitor-exit(r8)     // Catch:{ all -> 0x005d }
        L_0x002c:
            return
        L_0x002d:
            boolean r0 = r8.d()     // Catch:{ IOException -> 0x0068 }
            if (r0 == 0) goto L_0x003c
            boolean r0 = r8.s()     // Catch:{ IOException -> 0x0068 }
            if (r0 != 0) goto L_0x003c
            r8.f()     // Catch:{ IOException -> 0x0068 }
        L_0x003c:
            boolean r2 = r8.s()     // Catch:{ all -> 0x005d }
            org.apache.commons.logging.Log r0 = r7.f     // Catch:{ all -> 0x005d }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x0051
            if (r2 == 0) goto L_0x0060
            org.apache.commons.logging.Log r0 = r7.f     // Catch:{ all -> 0x005d }
            java.lang.String r3 = "Released connection is reusable."
            r0.debug(r3)     // Catch:{ all -> 0x005d }
        L_0x0051:
            r8.n()     // Catch:{ all -> 0x005d }
            com.agilebinary.a.a.b.f.b.a.d r0 = r7.c     // Catch:{ all -> 0x005d }
            r3 = r9
            r5 = r11
            r0.a(r1, r2, r3, r5)     // Catch:{ all -> 0x005d }
        L_0x005b:
            monitor-exit(r8)     // Catch:{ all -> 0x005d }
            goto L_0x002c
        L_0x005d:
            r0 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x005d }
            throw r0
        L_0x0060:
            org.apache.commons.logging.Log r0 = r7.f     // Catch:{ all -> 0x005d }
            java.lang.String r3 = "Released connection is not reusable."
            r0.debug(r3)     // Catch:{ all -> 0x005d }
            goto L_0x0051
        L_0x0068:
            r0 = move-exception
            org.apache.commons.logging.Log r2 = r7.f     // Catch:{ all -> 0x00a0 }
            boolean r2 = r2.isDebugEnabled()     // Catch:{ all -> 0x00a0 }
            if (r2 == 0) goto L_0x0078
            org.apache.commons.logging.Log r2 = r7.f     // Catch:{ all -> 0x00a0 }
            java.lang.String r3 = "Exception shutting down released connection."
            r2.debug(r3, r0)     // Catch:{ all -> 0x00a0 }
        L_0x0078:
            boolean r2 = r8.s()     // Catch:{ all -> 0x005d }
            org.apache.commons.logging.Log r0 = r7.f     // Catch:{ all -> 0x005d }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x008d
            if (r2 == 0) goto L_0x0098
            org.apache.commons.logging.Log r0 = r7.f     // Catch:{ all -> 0x005d }
            java.lang.String r3 = "Released connection is reusable."
            r0.debug(r3)     // Catch:{ all -> 0x005d }
        L_0x008d:
            r8.n()     // Catch:{ all -> 0x005d }
            com.agilebinary.a.a.b.f.b.a.d r0 = r7.c     // Catch:{ all -> 0x005d }
            r3 = r9
            r5 = r11
            r0.a(r1, r2, r3, r5)     // Catch:{ all -> 0x005d }
            goto L_0x005b
        L_0x0098:
            org.apache.commons.logging.Log r0 = r7.f     // Catch:{ all -> 0x005d }
            java.lang.String r3 = "Released connection is not reusable."
            r0.debug(r3)     // Catch:{ all -> 0x005d }
            goto L_0x008d
        L_0x00a0:
            r0 = move-exception
            r6 = r0
            boolean r2 = r8.s()     // Catch:{ all -> 0x005d }
            org.apache.commons.logging.Log r0 = r7.f     // Catch:{ all -> 0x005d }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x005d }
            if (r0 == 0) goto L_0x00b7
            if (r2 == 0) goto L_0x00c2
            org.apache.commons.logging.Log r0 = r7.f     // Catch:{ all -> 0x005d }
            java.lang.String r3 = "Released connection is reusable."
            r0.debug(r3)     // Catch:{ all -> 0x005d }
        L_0x00b7:
            r8.n()     // Catch:{ all -> 0x005d }
            com.agilebinary.a.a.b.f.b.a.d r0 = r7.c     // Catch:{ all -> 0x005d }
            r3 = r9
            r5 = r11
            r0.a(r1, r2, r3, r5)     // Catch:{ all -> 0x005d }
            throw r6     // Catch:{ all -> 0x005d }
        L_0x00c2:
            org.apache.commons.logging.Log r0 = r7.f     // Catch:{ all -> 0x005d }
            java.lang.String r3 = "Released connection is not reusable."
            r0.debug(r3)     // Catch:{ all -> 0x005d }
            goto L_0x00b7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.b.f.b.a.h.a(com.agilebinary.a.a.b.c.m, long, java.util.concurrent.TimeUnit):void");
    }

    /* access modifiers changed from: protected */
    public d b(long j, TimeUnit timeUnit) {
        return new d(this.d, this.e, 20, j, timeUnit);
    }

    public void b() {
        this.f.debug("Shutting down");
        this.c.a();
    }

    public void b(int i) {
        this.e.a(i);
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        try {
            b();
        } finally {
            super.finalize();
        }
    }
}
