package com.agilebinary.a.a.b.f.a;

import com.agilebinary.a.a.b.b.g;
import com.agilebinary.a.a.b.j;
import com.agilebinary.a.a.b.j.e;
import com.agilebinary.a.a.b.o;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ConnectException;
import java.net.UnknownHostException;
import javax.net.ssl.SSLException;

public class i implements g {

    /* renamed from: a  reason: collision with root package name */
    private final int f40a;
    private final boolean b;

    public i() {
        this(3, false);
    }

    public i(int i, boolean z) {
        this.f40a = i;
        this.b = z;
    }

    public boolean a(IOException iOException, int i, e eVar) {
        if (iOException == null) {
            throw new IllegalArgumentException("Exception parameter may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else if (i > this.f40a) {
            return false;
        } else {
            if (iOException instanceof InterruptedIOException) {
                return false;
            }
            if (iOException instanceof UnknownHostException) {
                return false;
            }
            if (iOException instanceof ConnectException) {
                return false;
            }
            if (iOException instanceof SSLException) {
                return false;
            }
            if (!(((o) eVar.a("http.request")) instanceof j)) {
                return true;
            }
            Boolean bool = (Boolean) eVar.a("http.request_sent");
            return !(bool != null && bool.booleanValue()) || this.b;
        }
    }
}
