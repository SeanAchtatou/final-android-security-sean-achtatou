package com.agilebinary.a.a.b.f.c;

import java.lang.ref.SoftReference;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

final class r {

    /* renamed from: a  reason: collision with root package name */
    private static final ThreadLocal f75a = new s();

    public static SimpleDateFormat a(String str) {
        HashMap hashMap;
        Map map = (Map) ((SoftReference) f75a.get()).get();
        if (map == null) {
            HashMap hashMap2 = new HashMap();
            f75a.set(new SoftReference(hashMap2));
            hashMap = hashMap2;
        } else {
            hashMap = map;
        }
        SimpleDateFormat simpleDateFormat = (SimpleDateFormat) hashMap.get(str);
        if (simpleDateFormat != null) {
            return simpleDateFormat;
        }
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat(str, Locale.US);
        simpleDateFormat2.setTimeZone(TimeZone.getTimeZone("GMT"));
        hashMap.put(str, simpleDateFormat2);
        return simpleDateFormat2;
    }
}
