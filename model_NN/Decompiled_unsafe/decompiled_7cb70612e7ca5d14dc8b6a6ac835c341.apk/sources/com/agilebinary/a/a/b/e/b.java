package com.agilebinary.a.a.b.e;

import java.io.InputStream;
import java.io.OutputStream;

public class b extends a {
    private InputStream d;
    private long e = -1;

    public InputStream a() {
        if (this.d != null) {
            return this.d;
        }
        throw new IllegalStateException("Content has not been provided");
    }

    public void a(long j) {
        this.e = j;
    }

    public void a(InputStream inputStream) {
        this.d = inputStream;
    }

    public void a(OutputStream outputStream) {
        if (outputStream == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        InputStream a2 = a();
        try {
            byte[] bArr = new byte[2048];
            while (true) {
                int read = a2.read(bArr);
                if (read != -1) {
                    outputStream.write(bArr, 0, read);
                } else {
                    return;
                }
            }
        } finally {
            a2.close();
        }
    }

    public long b() {
        return this.e;
    }

    public boolean c() {
        return false;
    }

    public boolean g() {
        return this.d != null;
    }
}
