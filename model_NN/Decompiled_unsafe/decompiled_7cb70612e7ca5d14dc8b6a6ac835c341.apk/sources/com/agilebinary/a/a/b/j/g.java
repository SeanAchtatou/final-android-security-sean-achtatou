package com.agilebinary.a.a.b.j;

import com.agilebinary.a.a.b.k;
import com.agilebinary.a.a.b.o;
import com.agilebinary.a.a.b.q;
import java.io.IOException;

public class g {
    private static final void a(com.agilebinary.a.a.b.g gVar) {
        try {
            gVar.c();
        } catch (IOException e) {
        }
    }

    public q a(o oVar, com.agilebinary.a.a.b.g gVar, e eVar) {
        if (oVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (gVar == null) {
            throw new IllegalArgumentException("Client connection may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else {
            try {
                q b = b(oVar, gVar, eVar);
                return b == null ? c(oVar, gVar, eVar) : b;
            } catch (IOException e) {
                a(gVar);
                throw e;
            } catch (k e2) {
                a(gVar);
                throw e2;
            } catch (RuntimeException e3) {
                a(gVar);
                throw e3;
            }
        }
    }

    public void a(o oVar, f fVar, e eVar) {
        if (oVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("HTTP processor may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else {
            eVar.a("http.request", oVar);
            fVar.a(oVar, eVar);
        }
    }

    public void a(q qVar, f fVar, e eVar) {
        if (qVar == null) {
            throw new IllegalArgumentException("HTTP response may not be null");
        } else if (fVar == null) {
            throw new IllegalArgumentException("HTTP processor may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else {
            eVar.a("http.response", qVar);
            fVar.a(qVar, eVar);
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(o oVar, q qVar) {
        int b;
        return ("HEAD".equalsIgnoreCase(oVar.g().a()) || (b = qVar.a().b()) < 200 || b == 204 || b == 304 || b == 205) ? false : true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x009f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.agilebinary.a.a.b.q b(com.agilebinary.a.a.b.o r6, com.agilebinary.a.a.b.g r7, com.agilebinary.a.a.b.j.e r8) {
        /*
            r5 = this;
            r1 = 0
            if (r6 != 0) goto L_0x000b
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "HTTP request may not be null"
            r0.<init>(r1)
            throw r0
        L_0x000b:
            if (r7 != 0) goto L_0x0015
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "HTTP connection may not be null"
            r0.<init>(r1)
            throw r0
        L_0x0015:
            if (r8 != 0) goto L_0x001f
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "HTTP context may not be null"
            r0.<init>(r1)
            throw r0
        L_0x001f:
            java.lang.String r0 = "http.connection"
            r8.a(r0, r7)
            java.lang.String r0 = "http.request_sent"
            java.lang.Boolean r2 = java.lang.Boolean.FALSE
            r8.a(r0, r2)
            r7.a(r6)
            boolean r0 = r6 instanceof com.agilebinary.a.a.b.j
            if (r0 == 0) goto L_0x00b6
            r2 = 1
            com.agilebinary.a.a.b.ab r0 = r6.g()
            com.agilebinary.a.a.b.z r3 = r0.b()
            r0 = r6
            com.agilebinary.a.a.b.j r0 = (com.agilebinary.a.a.b.j) r0
            boolean r0 = r0.a()
            if (r0 == 0) goto L_0x00b3
            com.agilebinary.a.a.b.t r0 = com.agilebinary.a.a.b.t.b
            boolean r0 = r3.c(r0)
            if (r0 != 0) goto L_0x00b3
            r7.b()
            com.agilebinary.a.a.b.i.d r0 = r6.f()
            java.lang.String r3 = "http.protocol.wait-for-continue"
            r4 = 2000(0x7d0, float:2.803E-42)
            int r0 = r0.a(r3, r4)
            boolean r0 = r7.a(r0)
            if (r0 == 0) goto L_0x00b3
            com.agilebinary.a.a.b.q r3 = r7.a()
            boolean r0 = r5.a(r6, r3)
            if (r0 == 0) goto L_0x006e
            r7.a(r3)
        L_0x006e:
            com.agilebinary.a.a.b.ac r0 = r3.a()
            int r0 = r0.b()
            r4 = 200(0xc8, float:2.8E-43)
            if (r0 >= r4) goto L_0x00af
            r4 = 100
            if (r0 == r4) goto L_0x009b
            java.net.ProtocolException r0 = new java.net.ProtocolException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unexpected response: "
            java.lang.StringBuilder r1 = r1.append(r2)
            com.agilebinary.a.a.b.ac r2 = r3.a()
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x009b:
            r0 = r1
            r1 = r2
        L_0x009d:
            if (r1 == 0) goto L_0x00a4
            com.agilebinary.a.a.b.j r6 = (com.agilebinary.a.a.b.j) r6
            r7.a(r6)
        L_0x00a4:
            r7.b()
            java.lang.String r1 = "http.request_sent"
            java.lang.Boolean r2 = java.lang.Boolean.TRUE
            r8.a(r1, r2)
            return r0
        L_0x00af:
            r0 = 0
            r1 = r0
            r0 = r3
            goto L_0x009d
        L_0x00b3:
            r0 = r1
            r1 = r2
            goto L_0x009d
        L_0x00b6:
            r0 = r1
            goto L_0x00a4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.b.j.g.b(com.agilebinary.a.a.b.o, com.agilebinary.a.a.b.g, com.agilebinary.a.a.b.j.e):com.agilebinary.a.a.b.q");
    }

    /* access modifiers changed from: protected */
    public q c(o oVar, com.agilebinary.a.a.b.g gVar, e eVar) {
        if (oVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (gVar == null) {
            throw new IllegalArgumentException("HTTP connection may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else {
            q qVar = null;
            int i = 0;
            while (true) {
                if (qVar != null && i >= 200) {
                    return qVar;
                }
                qVar = gVar.a();
                if (a(oVar, qVar)) {
                    gVar.a(qVar);
                }
                i = qVar.a().b();
            }
        }
    }
}
