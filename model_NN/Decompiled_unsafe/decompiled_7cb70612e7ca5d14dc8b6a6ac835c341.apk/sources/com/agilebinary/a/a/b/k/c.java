package com.agilebinary.a.a.b.k;

import com.agilebinary.a.a.b.d;
import com.agilebinary.a.a.b.i;
import com.agilebinary.a.a.b.v;
import java.io.InputStream;

public final class c {
    public static void a(i iVar) {
        InputStream a2;
        if (iVar != null && iVar.g() && (a2 = iVar.a()) != null) {
            a2.close();
        }
    }

    /* JADX INFO: finally extract failed */
    public static byte[] b(i iVar) {
        int i = 4096;
        if (iVar == null) {
            throw new IllegalArgumentException("HTTP entity may not be null");
        }
        InputStream a2 = iVar.a();
        if (a2 == null) {
            return null;
        }
        if (iVar.b() > 2147483647L) {
            throw new IllegalArgumentException("HTTP entity too large to be buffered in memory");
        }
        int b = (int) iVar.b();
        if (b >= 0) {
            i = b;
        }
        a aVar = new a(i);
        try {
            byte[] bArr = new byte[4096];
            while (true) {
                int read = a2.read(bArr);
                if (read != -1) {
                    aVar.a(bArr, 0, read);
                } else {
                    a2.close();
                    return aVar.b();
                }
            }
        } catch (Throwable th) {
            a2.close();
            throw th;
        }
    }

    public static String c(i iVar) {
        v a2;
        if (iVar == null) {
            throw new IllegalArgumentException("HTTP entity may not be null");
        } else if (iVar.e() == null) {
            return null;
        } else {
            d[] e = iVar.e().e();
            if (e.length <= 0 || (a2 = e[0].a("charset")) == null) {
                return null;
            }
            return a2.b();
        }
    }
}
