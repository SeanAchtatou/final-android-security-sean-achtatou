package com.agilebinary.a.a.b.f.e;

import com.agilebinary.a.a.b.g.g;
import java.io.IOException;
import java.io.OutputStream;

public class m extends OutputStream {

    /* renamed from: a  reason: collision with root package name */
    private final g f93a;
    private boolean b = false;

    public m(g gVar) {
        if (gVar == null) {
            throw new IllegalArgumentException("Session output buffer may not be null");
        }
        this.f93a = gVar;
    }

    public void close() {
        if (!this.b) {
            this.b = true;
            this.f93a.a();
        }
    }

    public void flush() {
        this.f93a.a();
    }

    public void write(int i) {
        if (this.b) {
            throw new IOException("Attempted write to closed stream.");
        }
        this.f93a.a(i);
    }

    public void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public void write(byte[] bArr, int i, int i2) {
        if (this.b) {
            throw new IOException("Attempted write to closed stream.");
        }
        this.f93a.a(bArr, i, i2);
    }
}
