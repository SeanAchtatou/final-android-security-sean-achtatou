package com.agilebinary.a.a.b.f.b.a;

import java.util.Date;
import java.util.concurrent.locks.Condition;

public class j {

    /* renamed from: a  reason: collision with root package name */
    private final Condition f53a;
    private final g b;
    private Thread c;
    private boolean d;

    public j(Condition condition, g gVar) {
        if (condition == null) {
            throw new IllegalArgumentException("Condition must not be null.");
        }
        this.f53a = condition;
        this.b = gVar;
    }

    public void a() {
        if (this.c == null) {
            throw new IllegalStateException("Nobody waiting on this object.");
        }
        this.f53a.signalAll();
    }

    public boolean a(Date date) {
        boolean z;
        if (this.c != null) {
            throw new IllegalStateException("A thread is already waiting on this object.\ncaller: " + Thread.currentThread() + "\nwaiter: " + this.c);
        } else if (this.d) {
            throw new InterruptedException("Operation interrupted");
        } else {
            this.c = Thread.currentThread();
            if (date != null) {
                try {
                    z = this.f53a.awaitUntil(date);
                } catch (Throwable th) {
                    this.c = null;
                    throw th;
                }
            } else {
                this.f53a.await();
                z = true;
            }
            if (this.d) {
                throw new InterruptedException("Operation interrupted");
            }
            this.c = null;
            return z;
        }
    }

    public void b() {
        this.d = true;
        this.f53a.signalAll();
    }
}
