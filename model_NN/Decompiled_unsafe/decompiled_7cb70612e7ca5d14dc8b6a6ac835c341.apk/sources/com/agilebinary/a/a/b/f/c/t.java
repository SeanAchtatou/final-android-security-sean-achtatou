package com.agilebinary.a.a.b.f.c;

import com.agilebinary.a.a.b.d.b;
import com.agilebinary.a.a.b.d.e;
import com.agilebinary.a.a.b.d.g;
import java.util.Locale;
import java.util.StringTokenizer;

public class t extends f {
    private static boolean a(String str) {
        String upperCase = str.toUpperCase(Locale.ENGLISH);
        return upperCase.endsWith(".COM") || upperCase.endsWith(".EDU") || upperCase.endsWith(".NET") || upperCase.endsWith(".GOV") || upperCase.endsWith(".MIL") || upperCase.endsWith(".ORG") || upperCase.endsWith(".INT");
    }

    public void a(b bVar, e eVar) {
        super.a(bVar, eVar);
        String a2 = eVar.a();
        String c = bVar.c();
        if (a2.contains(".")) {
            int countTokens = new StringTokenizer(c, ".").countTokens();
            if (a(c)) {
                if (countTokens < 2) {
                    throw new g("Domain attribute \"" + c + "\" violates the Netscape cookie specification for " + "special domains");
                }
            } else if (countTokens < 3) {
                throw new g("Domain attribute \"" + c + "\" violates the Netscape cookie specification");
            }
        }
    }

    public boolean b(b bVar, e eVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            String a2 = eVar.a();
            String c = bVar.c();
            if (c == null) {
                return false;
            }
            return a2.endsWith(c);
        }
    }
}
