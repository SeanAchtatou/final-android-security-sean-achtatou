package com.agilebinary.a.a.b.f.b;

import com.agilebinary.a.a.a.a.a;
import com.agilebinary.a.a.b.h;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;

@Deprecated
public class i {

    /* renamed from: a  reason: collision with root package name */
    private final Log f60a = a.a(getClass());
    private final Map b = new HashMap();

    public void a() {
        this.b.clear();
    }

    public void a(long j) {
        long currentTimeMillis = System.currentTimeMillis() - j;
        if (this.f60a.isDebugEnabled()) {
            this.f60a.debug("Checking for connections, idle timeout: " + currentTimeMillis);
        }
        for (Map.Entry entry : this.b.entrySet()) {
            h hVar = (h) entry.getKey();
            long a2 = ((j) entry.getValue()).f61a;
            if (a2 <= currentTimeMillis) {
                if (this.f60a.isDebugEnabled()) {
                    this.f60a.debug("Closing idle connection, connection time: " + a2);
                }
                try {
                    hVar.c();
                } catch (IOException e) {
                    this.f60a.debug("I/O error closing connection", e);
                }
            }
        }
    }
}
