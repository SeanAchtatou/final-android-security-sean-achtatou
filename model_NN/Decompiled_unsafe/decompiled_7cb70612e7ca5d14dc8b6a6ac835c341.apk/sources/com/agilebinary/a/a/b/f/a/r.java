package com.agilebinary.a.a.b.f.a;

import com.agilebinary.a.a.b.ab;
import com.agilebinary.a.a.b.b.a.g;
import com.agilebinary.a.a.b.h.a;
import com.agilebinary.a.a.b.h.l;
import com.agilebinary.a.a.b.i.e;
import com.agilebinary.a.a.b.o;
import com.agilebinary.a.a.b.y;
import com.agilebinary.a.a.b.z;
import java.net.URI;
import java.net.URISyntaxException;

public class r extends a implements g {
    private final o c;
    private URI d;
    private String e;
    private z f;
    private int g;

    public r(o oVar) {
        if (oVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        }
        this.c = oVar;
        a(oVar.f());
        a(oVar.d());
        if (oVar instanceof g) {
            this.d = ((g) oVar).h();
            this.e = ((g) oVar).d_();
            this.f = null;
        } else {
            ab g2 = oVar.g();
            try {
                this.d = new URI(g2.c());
                this.e = g2.a();
                this.f = oVar.c();
            } catch (URISyntaxException e2) {
                throw new y("Invalid request URI: " + g2.c(), e2);
            }
        }
        this.g = 0;
    }

    public void a(URI uri) {
        this.d = uri;
    }

    public z c() {
        if (this.f == null) {
            this.f = e.b(f());
        }
        return this.f;
    }

    public String d_() {
        return this.e;
    }

    public ab g() {
        String d_ = d_();
        z c2 = c();
        String str = null;
        if (this.d != null) {
            str = this.d.toASCIIString();
        }
        if (str == null || str.length() == 0) {
            str = "/";
        }
        return new l(d_, str, c2);
    }

    public URI h() {
        return this.d;
    }

    public void i() {
        throw new UnsupportedOperationException();
    }

    public boolean j() {
        return true;
    }

    public void k() {
        this.f96a.a();
        a(this.c.d());
    }

    public o l() {
        return this.c;
    }

    public int m() {
        return this.g;
    }

    public void n() {
        this.g++;
    }
}
