package com.agilebinary.a.a.b.i;

public abstract class a implements d {
    protected a() {
    }

    public int a(String str, int i) {
        Object a2 = a(str);
        return a2 == null ? i : ((Integer) a2).intValue();
    }

    public boolean a(String str, boolean z) {
        Object a2 = a(str);
        return a2 == null ? z : ((Boolean) a2).booleanValue();
    }

    public d b(String str, int i) {
        a(str, new Integer(i));
        return this;
    }

    public d b(String str, boolean z) {
        a(str, z ? Boolean.TRUE : Boolean.FALSE);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.b.i.a.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.b.i.a.a(java.lang.String, int):int
      com.agilebinary.a.a.b.i.d.a(java.lang.String, int):int
      com.agilebinary.a.a.b.i.d.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.b.i.d
      com.agilebinary.a.a.b.i.a.a(java.lang.String, boolean):boolean */
    public boolean b(String str) {
        return a(str, false);
    }

    public boolean c(String str) {
        return !a(str, false);
    }
}
