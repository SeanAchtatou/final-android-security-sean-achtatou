package com.agilebinary.a.a.b.h;

import com.agilebinary.a.a.b.ac;
import com.agilebinary.a.a.b.c;
import com.agilebinary.a.a.b.j.d;
import com.agilebinary.a.a.b.k.b;
import com.agilebinary.a.a.b.t;
import com.agilebinary.a.a.b.x;
import com.agilebinary.a.a.b.z;

public class i implements s {

    /* renamed from: a  reason: collision with root package name */
    public static final i f102a = new i();
    protected final z b;

    public i() {
        this(null);
    }

    public i(z zVar) {
        this.b = zVar == null ? t.c : zVar;
    }

    /* access modifiers changed from: protected */
    public ac a(z zVar, int i, String str) {
        return new m(zVar, i, str);
    }

    public c a(b bVar) {
        return new o(bVar);
    }

    /* access modifiers changed from: protected */
    public z a(int i, int i2) {
        return this.b.a(i, i2);
    }

    public z a(b bVar, t tVar) {
        boolean z = true;
        if (bVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (tVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            String a2 = this.b.a();
            int length = a2.length();
            int b2 = tVar.b();
            int a3 = tVar.a();
            d(bVar, tVar);
            int b3 = tVar.b();
            if (b3 + length + 4 > a3) {
                throw new x("Not a valid protocol version: " + bVar.a(b2, a3));
            }
            int i = 0;
            boolean z2 = true;
            while (z2 && i < length) {
                z2 = bVar.a(b3 + i) == a2.charAt(i);
                i++;
            }
            if (!z2) {
                z = z2;
            } else if (bVar.a(b3 + length) != '/') {
                z = false;
            }
            if (!z) {
                throw new x("Not a valid protocol version: " + bVar.a(b2, a3));
            }
            int i2 = length + 1 + b3;
            int a4 = bVar.a(46, i2, a3);
            if (a4 == -1) {
                throw new x("Invalid protocol version number: " + bVar.a(b2, a3));
            }
            try {
                int parseInt = Integer.parseInt(bVar.b(i2, a4));
                int i3 = a4 + 1;
                int a5 = bVar.a(32, i3, a3);
                if (a5 == -1) {
                    a5 = a3;
                }
                try {
                    int parseInt2 = Integer.parseInt(bVar.b(i3, a5));
                    tVar.a(a5);
                    return a(parseInt, parseInt2);
                } catch (NumberFormatException e) {
                    throw new x("Invalid protocol minor version number: " + bVar.a(b2, a3));
                }
            } catch (NumberFormatException e2) {
                throw new x("Invalid protocol major version number: " + bVar.a(b2, a3));
            }
        }
    }

    public boolean b(b bVar, t tVar) {
        boolean z = true;
        if (bVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (tVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            int b2 = tVar.b();
            String a2 = this.b.a();
            int length = a2.length();
            if (bVar.c() < length + 4) {
                return false;
            }
            if (b2 < 0) {
                b2 = (bVar.c() - 4) - length;
            } else if (b2 == 0) {
                while (b2 < bVar.c() && d.a(bVar.a(b2))) {
                    b2++;
                }
            }
            if (b2 + length + 4 > bVar.c()) {
                return false;
            }
            int i = 0;
            boolean z2 = true;
            while (z2 && i < length) {
                z2 = bVar.a(b2 + i) == a2.charAt(i);
                i++;
            }
            if (!z2) {
                z = z2;
            } else if (bVar.a(b2 + length) != '/') {
                z = false;
            }
            return z;
        }
    }

    public ac c(b bVar, t tVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Char array buffer may not be null");
        } else if (tVar == null) {
            throw new IllegalArgumentException("Parser cursor may not be null");
        } else {
            int b2 = tVar.b();
            int a2 = tVar.a();
            try {
                z a3 = a(bVar, tVar);
                d(bVar, tVar);
                int b3 = tVar.b();
                int a4 = bVar.a(32, b3, a2);
                int i = a4 < 0 ? a2 : a4;
                String b4 = bVar.b(b3, i);
                for (int i2 = 0; i2 < b4.length(); i2++) {
                    if (!Character.isDigit(b4.charAt(i2))) {
                        throw new x("Status line contains invalid status code: " + bVar.a(b2, a2));
                    }
                }
                return a(a3, Integer.parseInt(b4), i < a2 ? bVar.b(i, a2) : "");
            } catch (NumberFormatException e) {
                throw new x("Status line contains invalid status code: " + bVar.a(b2, a2));
            } catch (IndexOutOfBoundsException e2) {
                throw new x("Invalid status line: " + bVar.a(b2, a2));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void d(b bVar, t tVar) {
        int b2 = tVar.b();
        int a2 = tVar.a();
        while (b2 < a2 && d.a(bVar.a(b2))) {
            b2++;
        }
        tVar.a(b2);
    }
}
