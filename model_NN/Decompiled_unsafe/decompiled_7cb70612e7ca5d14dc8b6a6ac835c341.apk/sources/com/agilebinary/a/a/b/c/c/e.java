package com.agilebinary.a.a.b.c.c;

import com.agilebinary.a.a.b.c.f;
import com.agilebinary.a.a.b.i.c;
import com.agilebinary.a.a.b.i.d;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class e implements h, j {

    /* renamed from: a  reason: collision with root package name */
    private static final e f20a = new e();
    private final a b = null;

    public static e b() {
        return f20a;
    }

    public Socket a() {
        return new Socket();
    }

    public Socket a(d dVar) {
        return new Socket();
    }

    @Deprecated
    public Socket a(Socket socket, String str, int i, InetAddress inetAddress, int i2, d dVar) {
        InetSocketAddress inetSocketAddress = null;
        if (inetAddress != null || i2 > 0) {
            if (i2 < 0) {
                i2 = 0;
            }
            inetSocketAddress = new InetSocketAddress(inetAddress, i2);
        }
        return a(socket, new InetSocketAddress(this.b != null ? this.b.a(str) : InetAddress.getByName(str), i), inetSocketAddress, dVar);
    }

    public Socket a(Socket socket, InetSocketAddress inetSocketAddress, InetSocketAddress inetSocketAddress2, d dVar) {
        if (inetSocketAddress == null) {
            throw new IllegalArgumentException("Remote address may not be null");
        } else if (dVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            if (socket == null) {
                socket = a();
            }
            if (inetSocketAddress2 != null) {
                socket.setReuseAddress(c.b(dVar));
                socket.bind(inetSocketAddress2);
            }
            try {
                socket.connect(inetSocketAddress, c.f(dVar));
                return socket;
            } catch (SocketTimeoutException e) {
                throw new f("Connect to " + inetSocketAddress.getHostName() + "/" + inetSocketAddress.getAddress() + " timed out");
            }
        }
    }

    public final boolean a(Socket socket) {
        if (socket == null) {
            throw new IllegalArgumentException("Socket may not be null.");
        } else if (!socket.isClosed()) {
            return false;
        } else {
            throw new IllegalArgumentException("Socket is closed.");
        }
    }
}
