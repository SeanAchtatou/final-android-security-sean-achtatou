package com.agilebinary.a.a.b.c.c;

import com.agilebinary.a.a.b.k.e;
import java.util.Locale;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private final String f21a;
    private final h b;
    private final int c;
    private final boolean d;
    private String e;

    public f(String str, int i, h hVar) {
        if (str == null) {
            throw new IllegalArgumentException("Scheme name may not be null");
        } else if (i <= 0 || i > 65535) {
            throw new IllegalArgumentException("Port is invalid: " + i);
        } else if (hVar == null) {
            throw new IllegalArgumentException("Socket factory may not be null");
        } else {
            this.f21a = str.toLowerCase(Locale.ENGLISH);
            this.b = hVar;
            this.c = i;
            this.d = hVar instanceof b;
        }
    }

    @Deprecated
    public f(String str, j jVar, int i) {
        if (str == null) {
            throw new IllegalArgumentException("Scheme name may not be null");
        } else if (jVar == null) {
            throw new IllegalArgumentException("Socket factory may not be null");
        } else if (i <= 0 || i > 65535) {
            throw new IllegalArgumentException("Port is invalid: " + i);
        } else {
            this.f21a = str.toLowerCase(Locale.ENGLISH);
            if (jVar instanceof d) {
                this.b = new c((d) jVar);
                this.d = true;
            } else {
                this.b = new i(jVar);
                this.d = false;
            }
            this.c = i;
        }
    }

    public final int a() {
        return this.c;
    }

    public final int a(int i) {
        return i <= 0 ? this.c : i;
    }

    public final h b() {
        return this.b;
    }

    public final String c() {
        return this.f21a;
    }

    public final boolean d() {
        return this.d;
    }

    public final boolean equals(Object obj) {
        boolean z = true;
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof f)) {
            return false;
        }
        f fVar = (f) obj;
        if (!this.f21a.equals(fVar.f21a) || this.c != fVar.c || this.d != fVar.d || !this.b.equals(fVar.b)) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return e.a(e.a(e.a(e.a(17, this.c), this.f21a), this.d), this.b);
    }

    public final String toString() {
        if (this.e == null) {
            this.e = this.f21a + ':' + Integer.toString(this.c);
        }
        return this.e;
    }
}
