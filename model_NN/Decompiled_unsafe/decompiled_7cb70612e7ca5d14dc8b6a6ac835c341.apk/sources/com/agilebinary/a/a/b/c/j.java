package com.agilebinary.a.a.b.c;

import java.io.IOException;
import java.io.InputStream;

public class j extends InputStream implements i {

    /* renamed from: a  reason: collision with root package name */
    protected InputStream f27a;
    private boolean b;
    private final k c;

    public j(InputStream inputStream, k kVar) {
        if (inputStream == null) {
            throw new IllegalArgumentException("Wrapped stream may not be null.");
        }
        this.f27a = inputStream;
        this.b = false;
        this.c = kVar;
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        if (this.f27a != null && i < 0) {
            boolean z = true;
            try {
                if (this.c != null) {
                    z = this.c.a(this.f27a);
                }
                if (z) {
                    this.f27a.close();
                }
            } finally {
                this.f27a = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean a() {
        if (!this.b) {
            return this.f27a != null;
        }
        throw new IOException("Attempted read on closed stream.");
    }

    public int available() {
        if (!a()) {
            return 0;
        }
        try {
            return this.f27a.available();
        } catch (IOException e) {
            c();
            throw e;
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (this.f27a != null) {
            boolean z = true;
            try {
                if (this.c != null) {
                    z = this.c.b(this.f27a);
                }
                if (z) {
                    this.f27a.close();
                }
            } finally {
                this.f27a = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void c() {
        if (this.f27a != null) {
            boolean z = true;
            try {
                if (this.c != null) {
                    z = this.c.c(this.f27a);
                }
                if (z) {
                    this.f27a.close();
                }
            } finally {
                this.f27a = null;
            }
        }
    }

    public void close() {
        this.b = true;
        b();
    }

    public void e_() {
        close();
    }

    public void i() {
        this.b = true;
        c();
    }

    public int read() {
        if (!a()) {
            return -1;
        }
        try {
            int read = this.f27a.read();
            a(read);
            return read;
        } catch (IOException e) {
            c();
            throw e;
        }
    }

    public int read(byte[] bArr) {
        if (!a()) {
            return -1;
        }
        try {
            int read = this.f27a.read(bArr);
            a(read);
            return read;
        } catch (IOException e) {
            c();
            throw e;
        }
    }

    public int read(byte[] bArr, int i, int i2) {
        if (!a()) {
            return -1;
        }
        try {
            int read = this.f27a.read(bArr, i, i2);
            a(read);
            return read;
        } catch (IOException e) {
            c();
            throw e;
        }
    }
}
