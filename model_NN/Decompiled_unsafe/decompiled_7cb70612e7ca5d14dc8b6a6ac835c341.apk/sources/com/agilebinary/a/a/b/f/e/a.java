package com.agilebinary.a.a.b.f.e;

import com.agilebinary.a.a.b.g.c;
import com.agilebinary.a.a.b.g.f;
import com.agilebinary.a.a.b.h.i;
import com.agilebinary.a.a.b.h.s;
import com.agilebinary.a.a.b.i.d;
import com.agilebinary.a.a.b.k.b;
import com.agilebinary.a.a.b.n;
import com.agilebinary.a.a.b.x;
import com.agilebinary.a.a.b.y;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class a implements c {

    /* renamed from: a  reason: collision with root package name */
    protected final s f83a;
    private final f b;
    private final int c;
    private final int d;
    private final List e;
    private int f;
    private n g;

    public a(f fVar, s sVar, d dVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("Session input buffer may not be null");
        } else if (dVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            this.b = fVar;
            this.c = dVar.a("http.connection.max-header-count", -1);
            this.d = dVar.a("http.connection.max-line-length", -1);
            this.f83a = sVar == null ? i.f102a : sVar;
            this.e = new ArrayList();
            this.f = 0;
        }
    }

    public static com.agilebinary.a.a.b.c[] a(f fVar, int i, int i2, s sVar) {
        if (sVar == null) {
            sVar = i.f102a;
        }
        return a(fVar, i, i2, sVar, new ArrayList());
    }

    public static com.agilebinary.a.a.b.c[] a(f fVar, int i, int i2, s sVar, List list) {
        b bVar;
        int i3 = 0;
        if (fVar == null) {
            throw new IllegalArgumentException("Session input buffer may not be null");
        } else if (sVar == null) {
            throw new IllegalArgumentException("Line parser may not be null");
        } else if (list == null) {
            throw new IllegalArgumentException("Header line list may not be null");
        } else {
            b bVar2 = null;
            b bVar3 = null;
            while (true) {
                if (bVar3 == null) {
                    bVar3 = new b(64);
                } else {
                    bVar3.a();
                }
                if (fVar.a(bVar3) == -1 || bVar3.c() < 1) {
                    com.agilebinary.a.a.b.c[] cVarArr = new com.agilebinary.a.a.b.c[list.size()];
                } else {
                    if ((bVar3.a(0) == ' ' || bVar3.a(0) == 9) && bVar2 != null) {
                        int i4 = 0;
                        while (i4 < bVar3.c() && ((r5 = bVar3.a(i4)) == ' ' || r5 == 9)) {
                            i4++;
                        }
                        if (i2 <= 0 || ((bVar2.c() + 1) + bVar3.c()) - i4 <= i2) {
                            bVar2.a(' ');
                            bVar2.a(bVar3, i4, bVar3.c() - i4);
                            bVar = bVar3;
                            bVar3 = bVar2;
                        } else {
                            throw new IOException("Maximum line length limit exceeded");
                        }
                    } else {
                        list.add(bVar3);
                        bVar = null;
                    }
                    if (i <= 0 || list.size() < i) {
                        bVar2 = bVar3;
                        bVar3 = bVar;
                    } else {
                        throw new IOException("Maximum header count exceeded");
                    }
                }
            }
            com.agilebinary.a.a.b.c[] cVarArr2 = new com.agilebinary.a.a.b.c[list.size()];
            while (i3 < list.size()) {
                try {
                    cVarArr2[i3] = sVar.a((b) list.get(i3));
                    i3++;
                } catch (x e2) {
                    throw new y(e2.getMessage());
                }
            }
            return cVarArr2;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public n a() {
        switch (this.f) {
            case 0:
                try {
                    this.g = a(this.b);
                    this.f = 1;
                    break;
                } catch (x e2) {
                    throw new y(e2.getMessage(), e2);
                }
            case 1:
                break;
            default:
                throw new IllegalStateException("Inconsistent parser state");
        }
        this.g.a(a(this.b, this.c, this.d, this.f83a, this.e));
        n nVar = this.g;
        this.g = null;
        this.e.clear();
        this.f = 0;
        return nVar;
    }

    /* access modifiers changed from: protected */
    public abstract n a(f fVar);
}
