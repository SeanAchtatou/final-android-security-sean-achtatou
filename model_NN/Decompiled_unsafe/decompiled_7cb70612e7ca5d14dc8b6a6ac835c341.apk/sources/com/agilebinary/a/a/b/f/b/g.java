package com.agilebinary.a.a.b.f.b;

import com.agilebinary.a.a.b.c.a.c;
import com.agilebinary.a.a.b.c.b.b;
import com.agilebinary.a.a.b.c.b.d;
import com.agilebinary.a.a.b.j.e;
import com.agilebinary.a.a.b.l;
import com.agilebinary.a.a.b.o;
import java.net.InetAddress;

public class g implements d {

    /* renamed from: a  reason: collision with root package name */
    protected final com.agilebinary.a.a.b.c.c.g f59a;

    public g(com.agilebinary.a.a.b.c.c.g gVar) {
        if (gVar == null) {
            throw new IllegalArgumentException("SchemeRegistry must not be null.");
        }
        this.f59a = gVar;
    }

    public b a(l lVar, o oVar, e eVar) {
        if (oVar == null) {
            throw new IllegalStateException("Request must not be null.");
        }
        b b = c.b(oVar.f());
        if (b != null) {
            return b;
        }
        if (lVar == null) {
            throw new IllegalStateException("Target host must not be null.");
        }
        InetAddress c = c.c(oVar.f());
        l a2 = c.a(oVar.f());
        boolean d = this.f59a.a(lVar.c()).d();
        return a2 == null ? new b(lVar, c, d) : new b(lVar, c, a2, d);
    }
}
