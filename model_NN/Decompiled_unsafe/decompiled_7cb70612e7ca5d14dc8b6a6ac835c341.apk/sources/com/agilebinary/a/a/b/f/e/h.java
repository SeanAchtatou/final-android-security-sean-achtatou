package com.agilebinary.a.a.b.f.e;

import com.agilebinary.a.a.b.g.g;
import java.io.IOException;
import java.io.OutputStream;

public class h extends OutputStream {

    /* renamed from: a  reason: collision with root package name */
    private final g f90a;
    private final long b;
    private long c = 0;
    private boolean d = false;

    public h(g gVar, long j) {
        if (gVar == null) {
            throw new IllegalArgumentException("Session output buffer may not be null");
        } else if (j < 0) {
            throw new IllegalArgumentException("Content length may not be negative");
        } else {
            this.f90a = gVar;
            this.b = j;
        }
    }

    public void close() {
        if (!this.d) {
            this.d = true;
            this.f90a.a();
        }
    }

    public void flush() {
        this.f90a.a();
    }

    public void write(int i) {
        if (this.d) {
            throw new IOException("Attempted write to closed stream.");
        } else if (this.c < this.b) {
            this.f90a.a(i);
            this.c++;
        }
    }

    public void write(byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public void write(byte[] bArr, int i, int i2) {
        if (this.d) {
            throw new IOException("Attempted write to closed stream.");
        } else if (this.c < this.b) {
            long j = this.b - this.c;
            if (((long) i2) > j) {
                i2 = (int) j;
            }
            this.f90a.a(bArr, i, i2);
            this.c += (long) i2;
        }
    }
}
