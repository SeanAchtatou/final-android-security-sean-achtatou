package com.agilebinary.a.a.b.f.a;

import com.agilebinary.a.a.a.a.a;
import com.agilebinary.a.a.b.a.c;
import com.agilebinary.a.a.b.b.e;
import com.agilebinary.a.a.b.b.f;
import com.agilebinary.a.a.b.b.k;
import com.agilebinary.a.a.b.b.m;
import com.agilebinary.a.a.b.d.j;
import com.agilebinary.a.a.b.i.d;
import com.agilebinary.a.a.b.j.g;
import com.agilebinary.a.a.b.j.h;
import com.agilebinary.a.a.b.l;
import com.agilebinary.a.a.b.o;
import com.agilebinary.a.a.b.p;
import com.agilebinary.a.a.b.q;
import com.agilebinary.a.a.b.s;
import java.net.URI;
import org.apache.commons.logging.Log;

public abstract class b {

    /* renamed from: a  reason: collision with root package name */
    private final Log f35a = a.a(getClass());
    private d b;
    private g c;
    private com.agilebinary.a.a.b.c.b d;
    private com.agilebinary.a.a.b.a e;
    private com.agilebinary.a.a.b.c.g f;
    private j g;
    private c h;
    private com.agilebinary.a.a.b.j.b i;
    private h j;
    private com.agilebinary.a.a.b.b.g k;
    private k l;
    private com.agilebinary.a.a.b.b.b m;
    private com.agilebinary.a.a.b.b.b n;
    private e o;
    private f p;
    private com.agilebinary.a.a.b.c.b.d q;
    private m r;

    protected b(com.agilebinary.a.a.b.c.b bVar, d dVar) {
        this.b = dVar;
        this.d = bVar;
    }

    private final synchronized com.agilebinary.a.a.b.j.f G() {
        h hVar;
        synchronized (this) {
            if (this.j == null) {
                com.agilebinary.a.a.b.j.b F = F();
                int a2 = F.a();
                p[] pVarArr = new p[a2];
                for (int i2 = 0; i2 < a2; i2++) {
                    pVarArr[i2] = F.a(i2);
                }
                int b2 = F.b();
                s[] sVarArr = new s[b2];
                for (int i3 = 0; i3 < b2; i3++) {
                    sVarArr[i3] = F.b(i3);
                }
                this.j = new h(pVarArr, sVarArr);
            }
            hVar = this.j;
        }
        return hVar;
    }

    private l b(com.agilebinary.a.a.b.b.a.g gVar) {
        URI h2 = gVar.h();
        if (!h2.isAbsolute()) {
            return null;
        }
        String schemeSpecificPart = h2.getSchemeSpecificPart();
        String substring = schemeSpecificPart.substring(2, schemeSpecificPart.length());
        String substring2 = substring.substring(0, substring.indexOf(58) > 0 ? substring.indexOf(58) : substring.indexOf(47) > 0 ? substring.indexOf(47) : substring.indexOf(63) > 0 ? substring.indexOf(63) : substring.length());
        int port = h2.getPort();
        String scheme = h2.getScheme();
        if (substring2 != null && !"".equals(substring2)) {
            return new l(substring2, port, scheme);
        }
        throw new com.agilebinary.a.a.b.b.d("URI does not specify a valid host name: " + h2);
    }

    public final synchronized com.agilebinary.a.a.b.b.b A() {
        if (this.n == null) {
            this.n = l();
        }
        return this.n;
    }

    public final synchronized e B() {
        if (this.o == null) {
            this.o = m();
        }
        return this.o;
    }

    public final synchronized f C() {
        if (this.p == null) {
            this.p = n();
        }
        return this.p;
    }

    public final synchronized com.agilebinary.a.a.b.c.b.d D() {
        if (this.q == null) {
            this.q = o();
        }
        return this.q;
    }

    public final synchronized m E() {
        if (this.r == null) {
            this.r = p();
        }
        return this.r;
    }

    /* access modifiers changed from: protected */
    public final synchronized com.agilebinary.a.a.b.j.b F() {
        if (this.i == null) {
            this.i = i();
        }
        return this.i;
    }

    /* access modifiers changed from: protected */
    public com.agilebinary.a.a.b.b.l a(g gVar, com.agilebinary.a.a.b.c.b bVar, com.agilebinary.a.a.b.a aVar, com.agilebinary.a.a.b.c.g gVar2, com.agilebinary.a.a.b.c.b.d dVar, com.agilebinary.a.a.b.j.f fVar, com.agilebinary.a.a.b.b.g gVar3, k kVar, com.agilebinary.a.a.b.b.b bVar2, com.agilebinary.a.a.b.b.b bVar3, m mVar, d dVar2) {
        return new l(this.f35a, gVar, bVar, aVar, gVar2, dVar, fVar, gVar3, kVar, bVar2, bVar3, mVar, dVar2);
    }

    /* access modifiers changed from: protected */
    public abstract d a();

    /* access modifiers changed from: protected */
    public d a(o oVar) {
        return new f(null, q(), oVar.f(), null);
    }

    public final q a(com.agilebinary.a.a.b.b.a.g gVar) {
        return a(gVar, null);
    }

    public final q a(com.agilebinary.a.a.b.b.a.g gVar, com.agilebinary.a.a.b.j.e eVar) {
        if (gVar != null) {
            return a(b(gVar), gVar, eVar);
        }
        throw new IllegalArgumentException("Request must not be null.");
    }

    public final q a(l lVar, o oVar, com.agilebinary.a.a.b.j.e eVar) {
        com.agilebinary.a.a.b.j.c cVar;
        com.agilebinary.a.a.b.b.l a2;
        if (oVar == null) {
            throw new IllegalArgumentException("Request must not be null.");
        }
        synchronized (this) {
            com.agilebinary.a.a.b.j.e b2 = b();
            cVar = eVar == null ? b2 : new com.agilebinary.a.a.b.j.c(eVar, b2);
            a2 = a(s(), r(), v(), w(), D(), G(), x(), y(), z(), A(), E(), a(oVar));
        }
        try {
            return a2.a(lVar, oVar, cVar);
        } catch (com.agilebinary.a.a.b.k e2) {
            throw new com.agilebinary.a.a.b.b.d(e2);
        }
    }

    public synchronized void a(k kVar) {
        this.l = kVar;
    }

    public synchronized void a(p pVar) {
        F().b(pVar);
        this.j = null;
    }

    public synchronized void a(s sVar) {
        F().b(sVar);
        this.j = null;
    }

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.b.j.e b();

    /* access modifiers changed from: protected */
    public abstract g c();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.b.c.b d();

    /* access modifiers changed from: protected */
    public abstract c e();

    /* access modifiers changed from: protected */
    public abstract j f();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.b.a g();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.b.c.g h();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.b.j.b i();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.b.b.g j();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.b.b.b k();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.b.b.b l();

    /* access modifiers changed from: protected */
    public abstract e m();

    /* access modifiers changed from: protected */
    public abstract f n();

    /* access modifiers changed from: protected */
    public abstract com.agilebinary.a.a.b.c.b.d o();

    /* access modifiers changed from: protected */
    public abstract m p();

    public final synchronized d q() {
        if (this.b == null) {
            this.b = a();
        }
        return this.b;
    }

    public final synchronized com.agilebinary.a.a.b.c.b r() {
        if (this.d == null) {
            this.d = d();
        }
        return this.d;
    }

    public final synchronized g s() {
        if (this.c == null) {
            this.c = c();
        }
        return this.c;
    }

    public final synchronized c t() {
        if (this.h == null) {
            this.h = e();
        }
        return this.h;
    }

    public final synchronized j u() {
        if (this.g == null) {
            this.g = f();
        }
        return this.g;
    }

    public final synchronized com.agilebinary.a.a.b.a v() {
        if (this.e == null) {
            this.e = g();
        }
        return this.e;
    }

    public final synchronized com.agilebinary.a.a.b.c.g w() {
        if (this.f == null) {
            this.f = h();
        }
        return this.f;
    }

    public final synchronized com.agilebinary.a.a.b.b.g x() {
        if (this.k == null) {
            this.k = j();
        }
        return this.k;
    }

    public final synchronized k y() {
        if (this.l == null) {
            this.l = new k();
        }
        return this.l;
    }

    public final synchronized com.agilebinary.a.a.b.b.b z() {
        if (this.m == null) {
            this.m = k();
        }
        return this.m;
    }
}
