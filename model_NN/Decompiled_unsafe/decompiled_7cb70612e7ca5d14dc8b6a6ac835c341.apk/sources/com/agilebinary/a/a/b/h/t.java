package com.agilebinary.a.a.b.h;

import com.agilebinary.a.a.b.k.b;

public class t {

    /* renamed from: a  reason: collision with root package name */
    private final int f110a;
    private final int b;
    private int c;

    public t(int i, int i2) {
        if (i < 0) {
            throw new IndexOutOfBoundsException("Lower bound cannot be negative");
        } else if (i > i2) {
            throw new IndexOutOfBoundsException("Lower bound cannot be greater then upper bound");
        } else {
            this.f110a = i;
            this.b = i2;
            this.c = i;
        }
    }

    public int a() {
        return this.b;
    }

    public void a(int i) {
        if (i < this.f110a) {
            throw new IndexOutOfBoundsException("pos: " + i + " < lowerBound: " + this.f110a);
        } else if (i > this.b) {
            throw new IndexOutOfBoundsException("pos: " + i + " > upperBound: " + this.b);
        } else {
            this.c = i;
        }
    }

    public int b() {
        return this.c;
    }

    public boolean c() {
        return this.c >= this.b;
    }

    public String toString() {
        b bVar = new b(16);
        bVar.a('[');
        bVar.a(Integer.toString(this.f110a));
        bVar.a('>');
        bVar.a(Integer.toString(this.c));
        bVar.a('>');
        bVar.a(Integer.toString(this.b));
        bVar.a(']');
        return bVar.toString();
    }
}
