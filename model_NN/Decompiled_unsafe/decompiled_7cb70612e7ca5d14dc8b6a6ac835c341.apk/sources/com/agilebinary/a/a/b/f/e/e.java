package com.agilebinary.a.a.b.f.e;

import com.agilebinary.a.a.b.ae;
import com.agilebinary.a.a.b.c;
import com.agilebinary.a.a.b.g.a;
import com.agilebinary.a.a.b.g.f;
import com.agilebinary.a.a.b.k;
import com.agilebinary.a.a.b.k.b;
import com.agilebinary.a.a.b.k.d;
import com.agilebinary.a.a.b.u;
import java.io.IOException;
import java.io.InputStream;

public class e extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    private final f f87a;
    private final b b;
    private int c;
    private int d;
    private int e;
    private boolean f = false;
    private boolean g = false;
    private c[] h = new c[0];

    public e(f fVar) {
        if (fVar == null) {
            throw new IllegalArgumentException("Session input buffer may not be null");
        }
        this.f87a = fVar;
        this.e = 0;
        this.b = new b(16);
        this.c = 1;
    }

    private void a() {
        this.d = b();
        if (this.d < 0) {
            throw new u("Negative chunk size");
        }
        this.c = 2;
        this.e = 0;
        if (this.d == 0) {
            this.f = true;
            c();
        }
    }

    private int b() {
        switch (this.c) {
            case 1:
                break;
            case 2:
            default:
                throw new IllegalStateException("Inconsistent codec state");
            case 3:
                this.b.a();
                if (this.f87a.a(this.b) != -1) {
                    if (this.b.d()) {
                        this.c = 1;
                        break;
                    } else {
                        throw new u("Unexpected content at the end of chunk");
                    }
                } else {
                    return 0;
                }
        }
        this.b.a();
        if (this.f87a.a(this.b) == -1) {
            return 0;
        }
        int c2 = this.b.c(59);
        if (c2 < 0) {
            c2 = this.b.c();
        }
        try {
            return Integer.parseInt(this.b.b(0, c2), 16);
        } catch (NumberFormatException e2) {
            throw new u("Bad chunk header");
        }
    }

    private void c() {
        try {
            this.h = a.a(this.f87a, -1, -1, null);
        } catch (k e2) {
            u uVar = new u("Invalid footer: " + e2.getMessage());
            d.a(uVar, e2);
            throw uVar;
        }
    }

    public int available() {
        if (this.f87a instanceof a) {
            return Math.min(((a) this.f87a).e(), this.d - this.e);
        }
        return 0;
    }

    public void close() {
        if (!this.g) {
            try {
                if (!this.f) {
                    do {
                    } while (read(new byte[2048]) >= 0);
                }
            } finally {
                this.f = true;
                this.g = true;
            }
        }
    }

    public int read() {
        if (this.g) {
            throw new IOException("Attempted read from closed stream.");
        } else if (this.f) {
            return -1;
        } else {
            if (this.c != 2) {
                a();
                if (this.f) {
                    return -1;
                }
            }
            int a2 = this.f87a.a();
            if (a2 != -1) {
                this.e++;
                if (this.e >= this.d) {
                    this.c = 3;
                }
            }
            return a2;
        }
    }

    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    public int read(byte[] bArr, int i, int i2) {
        if (this.g) {
            throw new IOException("Attempted read from closed stream.");
        } else if (this.f) {
            return -1;
        } else {
            if (this.c != 2) {
                a();
                if (this.f) {
                    return -1;
                }
            }
            int a2 = this.f87a.a(bArr, i, Math.min(i2, this.d - this.e));
            if (a2 != -1) {
                this.e += a2;
                if (this.e >= this.d) {
                    this.c = 3;
                }
                return a2;
            }
            this.f = true;
            throw new ae("Truncated chunk ( expected size: " + this.d + "; actual size: " + this.e + ")");
        }
    }
}
