package com.agilebinary.a.a.b.f.c;

import com.agilebinary.a.a.b.d.b;
import com.agilebinary.a.a.b.d.c;
import com.agilebinary.a.a.b.d.e;
import com.agilebinary.a.a.b.d.g;
import com.agilebinary.a.a.b.d.l;

public class i implements c {
    public void a(b bVar, e eVar) {
        if (!b(bVar, eVar)) {
            throw new g("Illegal path attribute \"" + bVar.d() + "\". Path of origin: \"" + eVar.b() + "\"");
        }
    }

    public void a(l lVar, String str) {
        if (lVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        }
        if (str == null || str.trim().length() == 0) {
            str = "/";
        }
        lVar.e(str);
    }

    public boolean b(b bVar, e eVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("Cookie origin may not be null");
        } else {
            String b = eVar.b();
            String d = bVar.d();
            if (d == null) {
                d = "/";
            }
            if (d.length() > 1 && d.endsWith("/")) {
                d = d.substring(0, d.length() - 1);
            }
            boolean startsWith = b.startsWith(d);
            return (!startsWith || b.length() == d.length() || d.endsWith("/")) ? startsWith : b.charAt(d.length()) == '/';
        }
    }
}
