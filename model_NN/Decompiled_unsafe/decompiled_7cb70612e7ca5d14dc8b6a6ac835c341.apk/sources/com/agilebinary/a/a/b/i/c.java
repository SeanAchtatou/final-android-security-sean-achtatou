package com.agilebinary.a.a.b.i;

public final class c {
    public static int a(d dVar) {
        if (dVar != null) {
            return dVar.a("http.socket.timeout", 0);
        }
        throw new IllegalArgumentException("HTTP parameters may not be null");
    }

    public static void a(d dVar, int i) {
        if (dVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        dVar.b("http.socket.buffer-size", i);
    }

    public static void a(d dVar, boolean z) {
        if (dVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        }
        dVar.b("http.tcp.nodelay", z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.b.i.d.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.b.i.d.a(java.lang.String, int):int
      com.agilebinary.a.a.b.i.d.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.b.i.d
      com.agilebinary.a.a.b.i.d.a(java.lang.String, boolean):boolean */
    public static boolean b(d dVar) {
        if (dVar != null) {
            return dVar.a("http.socket.reuseaddr", false);
        }
        throw new IllegalArgumentException("HTTP parameters may not be null");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.b.i.d.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.b.i.d.a(java.lang.String, int):int
      com.agilebinary.a.a.b.i.d.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.b.i.d
      com.agilebinary.a.a.b.i.d.a(java.lang.String, boolean):boolean */
    public static boolean c(d dVar) {
        if (dVar != null) {
            return dVar.a("http.tcp.nodelay", true);
        }
        throw new IllegalArgumentException("HTTP parameters may not be null");
    }

    public static int d(d dVar) {
        if (dVar != null) {
            return dVar.a("http.socket.buffer-size", -1);
        }
        throw new IllegalArgumentException("HTTP parameters may not be null");
    }

    public static int e(d dVar) {
        if (dVar != null) {
            return dVar.a("http.socket.linger", -1);
        }
        throw new IllegalArgumentException("HTTP parameters may not be null");
    }

    public static int f(d dVar) {
        if (dVar != null) {
            return dVar.a("http.connection.timeout", 0);
        }
        throw new IllegalArgumentException("HTTP parameters may not be null");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.b.i.d.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.a.a.b.i.d.a(java.lang.String, int):int
      com.agilebinary.a.a.b.i.d.a(java.lang.String, java.lang.Object):com.agilebinary.a.a.b.i.d
      com.agilebinary.a.a.b.i.d.a(java.lang.String, boolean):boolean */
    public static boolean g(d dVar) {
        if (dVar != null) {
            return dVar.a("http.connection.stalecheck", true);
        }
        throw new IllegalArgumentException("HTTP parameters may not be null");
    }
}
