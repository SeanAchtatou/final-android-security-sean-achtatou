package com.agilebinary.a.a.b.f.b.a;

import com.agilebinary.a.a.b.c.a.a;
import com.agilebinary.a.a.b.c.b.b;
import com.agilebinary.a.a.b.c.h;
import com.agilebinary.a.a.b.c.n;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import org.apache.commons.logging.Log;

public class d extends a {
    protected final com.agilebinary.a.a.b.c.d e;
    protected final a f;
    protected final Set g;
    protected final Queue h;
    protected final Queue i;
    protected final Map j;
    protected volatile boolean k;
    protected volatile int l;
    protected volatile int m;
    private final Log n = com.agilebinary.a.a.a.a.a.a(getClass());
    /* access modifiers changed from: private */
    public final Lock o;
    private final long p;
    private final TimeUnit q;

    public d(com.agilebinary.a.a.b.c.d dVar, a aVar, int i2, long j2, TimeUnit timeUnit) {
        if (dVar == null) {
            throw new IllegalArgumentException("Connection operator may not be null");
        } else if (aVar == null) {
            throw new IllegalArgumentException("Connections per route may not be null");
        } else {
            this.o = this.f48a;
            this.g = this.b;
            this.e = dVar;
            this.f = aVar;
            this.l = i2;
            this.h = b();
            this.i = c();
            this.j = d();
            this.p = j2;
            this.q = timeUnit;
        }
    }

    private void b(b bVar) {
        n c = bVar.c();
        if (c != null) {
            try {
                c.c();
            } catch (IOException e2) {
                this.n.debug("I/O error closing connection", e2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.b.f.b.a.d.a(com.agilebinary.a.a.b.c.b.b, boolean):com.agilebinary.a.a.b.f.b.a.g
     arg types: [com.agilebinary.a.a.b.c.b.b, int]
     candidates:
      com.agilebinary.a.a.b.f.b.a.d.a(com.agilebinary.a.a.b.f.b.a.g, com.agilebinary.a.a.b.c.d):com.agilebinary.a.a.b.f.b.a.b
      com.agilebinary.a.a.b.f.b.a.d.a(com.agilebinary.a.a.b.f.b.a.g, java.lang.Object):com.agilebinary.a.a.b.f.b.a.b
      com.agilebinary.a.a.b.f.b.a.d.a(com.agilebinary.a.a.b.c.b.b, java.lang.Object):com.agilebinary.a.a.b.f.b.a.f
      com.agilebinary.a.a.b.f.b.a.d.a(java.util.concurrent.locks.Condition, com.agilebinary.a.a.b.f.b.a.g):com.agilebinary.a.a.b.f.b.a.j
      com.agilebinary.a.a.b.f.b.a.d.a(long, java.util.concurrent.TimeUnit):void
      com.agilebinary.a.a.b.f.b.a.a.a(long, java.util.concurrent.TimeUnit):void
      com.agilebinary.a.a.b.f.b.a.d.a(com.agilebinary.a.a.b.c.b.b, boolean):com.agilebinary.a.a.b.f.b.a.g */
    /* access modifiers changed from: protected */
    public b a(b bVar, Object obj, long j2, TimeUnit timeUnit, k kVar) {
        g a2;
        j jVar;
        Date date = null;
        if (j2 > 0) {
            date = new Date(System.currentTimeMillis() + timeUnit.toMillis(j2));
        }
        b bVar2 = null;
        this.o.lock();
        try {
            a2 = a(bVar, true);
            jVar = null;
            while (bVar2 == null) {
                if (this.k) {
                    throw new IllegalStateException("Connection pool shut down");
                }
                if (this.n.isDebugEnabled()) {
                    this.n.debug("[" + bVar + "] total kept alive: " + this.h.size() + ", total issued: " + this.g.size() + ", total allocated: " + this.m + " out of " + this.l);
                }
                bVar2 = a(a2, obj);
                if (bVar2 != null) {
                    break;
                }
                boolean z = a2.d() > 0;
                if (this.n.isDebugEnabled()) {
                    this.n.debug("Available capacity: " + a2.d() + " out of " + a2.b() + " [" + bVar + "][" + obj + "]");
                }
                if (z && this.m < this.l) {
                    bVar2 = a(a2, this.e);
                } else if (!z || this.h.isEmpty()) {
                    if (this.n.isDebugEnabled()) {
                        this.n.debug("Need to wait for connection [" + bVar + "][" + obj + "]");
                    }
                    if (jVar == null) {
                        jVar = a(this.o.newCondition(), a2);
                        kVar.a(jVar);
                    }
                    a2.a(jVar);
                    this.i.add(jVar);
                    boolean a3 = jVar.a(date);
                    a2.b(jVar);
                    this.i.remove(jVar);
                    if (!a3 && date != null && date.getTime() <= System.currentTimeMillis()) {
                        throw new h("Timeout waiting for connection");
                    }
                } else {
                    e();
                    a2 = a(bVar, true);
                    bVar2 = a(a2, this.e);
                }
            }
            this.o.unlock();
            return bVar2;
        } catch (Throwable th) {
            this.o.unlock();
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public b a(g gVar, com.agilebinary.a.a.b.c.d dVar) {
        if (this.n.isDebugEnabled()) {
            this.n.debug("Creating new connection [" + gVar.a() + "]");
        }
        b bVar = new b(dVar, gVar.a(), this.p, this.q);
        this.o.lock();
        try {
            gVar.b(bVar);
            this.m++;
            this.g.add(bVar);
            return bVar;
        } finally {
            this.o.unlock();
        }
    }

    /* access modifiers changed from: protected */
    public b a(g gVar, Object obj) {
        b bVar = null;
        this.o.lock();
        boolean z = false;
        while (!z) {
            try {
                bVar = gVar.a(obj);
                if (bVar != null) {
                    if (this.n.isDebugEnabled()) {
                        this.n.debug("Getting free connection [" + gVar.a() + "][" + obj + "]");
                    }
                    this.h.remove(bVar);
                    if (bVar.a(System.currentTimeMillis())) {
                        if (this.n.isDebugEnabled()) {
                            this.n.debug("Closing expired free connection [" + gVar.a() + "][" + obj + "]");
                        }
                        b(bVar);
                        gVar.e();
                        this.m--;
                    } else {
                        this.g.add(bVar);
                        z = true;
                    }
                } else if (this.n.isDebugEnabled()) {
                    this.n.debug("No free connections [" + gVar.a() + "][" + obj + "]");
                    z = true;
                } else {
                    z = true;
                }
            } catch (Throwable th) {
                this.o.unlock();
                throw th;
            }
        }
        this.o.unlock();
        return bVar;
    }

    public f a(b bVar, Object obj) {
        return new e(this, new k(), bVar, obj);
    }

    /* access modifiers changed from: protected */
    public g a(b bVar) {
        return new g(bVar, this.f);
    }

    /* access modifiers changed from: protected */
    public g a(b bVar, boolean z) {
        this.o.lock();
        try {
            g gVar = (g) this.j.get(bVar);
            if (gVar == null && z) {
                gVar = a(bVar);
                this.j.put(bVar, gVar);
            }
            return gVar;
        } finally {
            this.o.unlock();
        }
    }

    /* access modifiers changed from: protected */
    public j a(Condition condition, g gVar) {
        return new j(condition, gVar);
    }

    public void a() {
        this.o.lock();
        try {
            if (!this.k) {
                this.k = true;
                Iterator it = this.g.iterator();
                while (it.hasNext()) {
                    it.remove();
                    b((b) it.next());
                }
                Iterator it2 = this.h.iterator();
                while (it2.hasNext()) {
                    b bVar = (b) it2.next();
                    it2.remove();
                    if (this.n.isDebugEnabled()) {
                        this.n.debug("Closing connection [" + bVar.d() + "][" + bVar.a() + "]");
                    }
                    b(bVar);
                }
                Iterator it3 = this.i.iterator();
                while (it3.hasNext()) {
                    it3.remove();
                    ((j) it3.next()).a();
                }
                this.j.clear();
                this.o.unlock();
            }
        } finally {
            this.o.unlock();
        }
    }

    public void a(int i2) {
        this.o.lock();
        try {
            this.l = i2;
        } finally {
            this.o.unlock();
        }
    }

    public void a(long j2, TimeUnit timeUnit) {
        if (timeUnit == null) {
            throw new IllegalArgumentException("Time unit must not be null.");
        }
        if (j2 < 0) {
            j2 = 0;
        }
        if (this.n.isDebugEnabled()) {
            this.n.debug("Closing connections idle longer than " + j2 + " " + timeUnit);
        }
        long currentTimeMillis = System.currentTimeMillis() - timeUnit.toMillis(j2);
        this.o.lock();
        try {
            Iterator it = this.h.iterator();
            while (it.hasNext()) {
                b bVar = (b) it.next();
                if (bVar.e() <= currentTimeMillis) {
                    if (this.n.isDebugEnabled()) {
                        this.n.debug("Closing connection last used @ " + new Date(bVar.e()));
                    }
                    it.remove();
                    a(bVar);
                }
            }
        } finally {
            this.o.unlock();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.b.f.b.a.d.a(com.agilebinary.a.a.b.c.b.b, boolean):com.agilebinary.a.a.b.f.b.a.g
     arg types: [com.agilebinary.a.a.b.c.b.b, int]
     candidates:
      com.agilebinary.a.a.b.f.b.a.d.a(com.agilebinary.a.a.b.f.b.a.g, com.agilebinary.a.a.b.c.d):com.agilebinary.a.a.b.f.b.a.b
      com.agilebinary.a.a.b.f.b.a.d.a(com.agilebinary.a.a.b.f.b.a.g, java.lang.Object):com.agilebinary.a.a.b.f.b.a.b
      com.agilebinary.a.a.b.f.b.a.d.a(com.agilebinary.a.a.b.c.b.b, java.lang.Object):com.agilebinary.a.a.b.f.b.a.f
      com.agilebinary.a.a.b.f.b.a.d.a(java.util.concurrent.locks.Condition, com.agilebinary.a.a.b.f.b.a.g):com.agilebinary.a.a.b.f.b.a.j
      com.agilebinary.a.a.b.f.b.a.d.a(long, java.util.concurrent.TimeUnit):void
      com.agilebinary.a.a.b.f.b.a.a.a(long, java.util.concurrent.TimeUnit):void
      com.agilebinary.a.a.b.f.b.a.d.a(com.agilebinary.a.a.b.c.b.b, boolean):com.agilebinary.a.a.b.f.b.a.g */
    /* access modifiers changed from: protected */
    public void a(b bVar) {
        b d = bVar.d();
        if (this.n.isDebugEnabled()) {
            this.n.debug("Deleting connection [" + d + "][" + bVar.a() + "]");
        }
        this.o.lock();
        try {
            b(bVar);
            g a2 = a(d, true);
            a2.c(bVar);
            this.m--;
            if (a2.c()) {
                this.j.remove(d);
            }
        } finally {
            this.o.unlock();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.a.a.b.f.b.a.d.a(com.agilebinary.a.a.b.c.b.b, boolean):com.agilebinary.a.a.b.f.b.a.g
     arg types: [com.agilebinary.a.a.b.c.b.b, int]
     candidates:
      com.agilebinary.a.a.b.f.b.a.d.a(com.agilebinary.a.a.b.f.b.a.g, com.agilebinary.a.a.b.c.d):com.agilebinary.a.a.b.f.b.a.b
      com.agilebinary.a.a.b.f.b.a.d.a(com.agilebinary.a.a.b.f.b.a.g, java.lang.Object):com.agilebinary.a.a.b.f.b.a.b
      com.agilebinary.a.a.b.f.b.a.d.a(com.agilebinary.a.a.b.c.b.b, java.lang.Object):com.agilebinary.a.a.b.f.b.a.f
      com.agilebinary.a.a.b.f.b.a.d.a(java.util.concurrent.locks.Condition, com.agilebinary.a.a.b.f.b.a.g):com.agilebinary.a.a.b.f.b.a.j
      com.agilebinary.a.a.b.f.b.a.d.a(long, java.util.concurrent.TimeUnit):void
      com.agilebinary.a.a.b.f.b.a.a.a(long, java.util.concurrent.TimeUnit):void
      com.agilebinary.a.a.b.f.b.a.d.a(com.agilebinary.a.a.b.c.b.b, boolean):com.agilebinary.a.a.b.f.b.a.g */
    public void a(b bVar, boolean z, long j2, TimeUnit timeUnit) {
        b d = bVar.d();
        if (this.n.isDebugEnabled()) {
            this.n.debug("Releasing connection [" + d + "][" + bVar.a() + "]");
        }
        this.o.lock();
        try {
            if (this.k) {
                b(bVar);
                return;
            }
            this.g.remove(bVar);
            g a2 = a(d, true);
            if (z) {
                if (this.n.isDebugEnabled()) {
                    this.n.debug("Pooling connection [" + d + "][" + bVar.a() + "]; keep alive for " + (j2 >= 0 ? j2 + " " + timeUnit : "ever"));
                }
                a2.a(bVar);
                bVar.a(j2, timeUnit);
                this.h.add(bVar);
            } else {
                a2.e();
                this.m--;
            }
            a(a2);
            this.o.unlock();
        } finally {
            this.o.unlock();
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x003e A[Catch:{ all -> 0x0077 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.agilebinary.a.a.b.f.b.a.g r4) {
        /*
            r3 = this;
            r0 = 0
            java.util.concurrent.locks.Lock r1 = r3.o
            r1.lock()
            if (r4 == 0) goto L_0x0047
            boolean r1 = r4.f()     // Catch:{ all -> 0x0077 }
            if (r1 == 0) goto L_0x0047
            org.apache.commons.logging.Log r0 = r3.n     // Catch:{ all -> 0x0077 }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x0077 }
            if (r0 == 0) goto L_0x0038
            org.apache.commons.logging.Log r0 = r3.n     // Catch:{ all -> 0x0077 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0077 }
            r1.<init>()     // Catch:{ all -> 0x0077 }
            java.lang.String r2 = "Notifying thread waiting on pool ["
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0077 }
            com.agilebinary.a.a.b.c.b.b r2 = r4.a()     // Catch:{ all -> 0x0077 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0077 }
            java.lang.String r2 = "]"
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0077 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0077 }
            r0.debug(r1)     // Catch:{ all -> 0x0077 }
        L_0x0038:
            com.agilebinary.a.a.b.f.b.a.j r0 = r4.g()     // Catch:{ all -> 0x0077 }
        L_0x003c:
            if (r0 == 0) goto L_0x0041
            r0.a()     // Catch:{ all -> 0x0077 }
        L_0x0041:
            java.util.concurrent.locks.Lock r0 = r3.o
            r0.unlock()
            return
        L_0x0047:
            java.util.Queue r1 = r3.i     // Catch:{ all -> 0x0077 }
            boolean r1 = r1.isEmpty()     // Catch:{ all -> 0x0077 }
            if (r1 != 0) goto L_0x0067
            org.apache.commons.logging.Log r0 = r3.n     // Catch:{ all -> 0x0077 }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ all -> 0x0077 }
            if (r0 == 0) goto L_0x005e
            org.apache.commons.logging.Log r0 = r3.n     // Catch:{ all -> 0x0077 }
            java.lang.String r1 = "Notifying thread waiting on any pool"
            r0.debug(r1)     // Catch:{ all -> 0x0077 }
        L_0x005e:
            java.util.Queue r0 = r3.i     // Catch:{ all -> 0x0077 }
            java.lang.Object r0 = r0.remove()     // Catch:{ all -> 0x0077 }
            com.agilebinary.a.a.b.f.b.a.j r0 = (com.agilebinary.a.a.b.f.b.a.j) r0     // Catch:{ all -> 0x0077 }
            goto L_0x003c
        L_0x0067:
            org.apache.commons.logging.Log r1 = r3.n     // Catch:{ all -> 0x0077 }
            boolean r1 = r1.isDebugEnabled()     // Catch:{ all -> 0x0077 }
            if (r1 == 0) goto L_0x003c
            org.apache.commons.logging.Log r1 = r3.n     // Catch:{ all -> 0x0077 }
            java.lang.String r2 = "Notifying no-one, there are no waiting threads"
            r1.debug(r2)     // Catch:{ all -> 0x0077 }
            goto L_0x003c
        L_0x0077:
            r0 = move-exception
            java.util.concurrent.locks.Lock r1 = r3.o
            r1.unlock()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.a.a.b.f.b.a.d.a(com.agilebinary.a.a.b.f.b.a.g):void");
    }

    /* access modifiers changed from: protected */
    public Queue b() {
        return new LinkedList();
    }

    /* access modifiers changed from: protected */
    public Queue c() {
        return new LinkedList();
    }

    /* access modifiers changed from: protected */
    public Map d() {
        return new HashMap();
    }

    /* access modifiers changed from: protected */
    public void e() {
        this.o.lock();
        try {
            b bVar = (b) this.h.remove();
            if (bVar != null) {
                a(bVar);
            } else if (this.n.isDebugEnabled()) {
                this.n.debug("No free connection to delete");
            }
        } finally {
            this.o.unlock();
        }
    }
}
