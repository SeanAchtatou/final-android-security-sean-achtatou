package com.agilebinary.a.a.b.f.b;

import com.agilebinary.a.a.b.g.e;
import com.agilebinary.a.a.b.g.g;
import com.agilebinary.a.a.b.k.b;

public class l implements g {

    /* renamed from: a  reason: collision with root package name */
    private final g f63a;
    private final q b;
    private final String c;

    public l(g gVar, q qVar, String str) {
        this.f63a = gVar;
        this.b = qVar;
        this.c = str == null ? "ASCII" : str;
    }

    public void a() {
        this.f63a.a();
    }

    public void a(int i) {
        this.f63a.a(i);
        if (this.b.a()) {
            this.b.a(i);
        }
    }

    public void a(b bVar) {
        this.f63a.a(bVar);
        if (this.b.a()) {
            this.b.a((new String(bVar.b(), 0, bVar.c()) + "\r\n").getBytes(this.c));
        }
    }

    public void a(String str) {
        this.f63a.a(str);
        if (this.b.a()) {
            this.b.a((str + "\r\n").getBytes(this.c));
        }
    }

    public void a(byte[] bArr, int i, int i2) {
        this.f63a.a(bArr, i, i2);
        if (this.b.a()) {
            this.b.a(bArr, i, i2);
        }
    }

    public e b() {
        return this.f63a.b();
    }
}
