package com.agilebinary.a.a.b.a;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private a f5a;
    private d b;
    private h c;

    public void a() {
        this.f5a = null;
        this.b = null;
        this.c = null;
    }

    public void a(a aVar) {
        if (aVar == null) {
            a();
        } else {
            this.f5a = aVar;
        }
    }

    public void a(d dVar) {
        this.b = dVar;
    }

    public void a(h hVar) {
        this.c = hVar;
    }

    public boolean b() {
        return this.f5a != null;
    }

    public a c() {
        return this.f5a;
    }

    public h d() {
        return this.c;
    }

    public d e() {
        return this.b;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("auth scope [");
        sb.append(this.b);
        sb.append("]; credentials set [");
        sb.append(this.c != null ? "true" : "false");
        sb.append("]");
        return sb.toString();
    }
}
