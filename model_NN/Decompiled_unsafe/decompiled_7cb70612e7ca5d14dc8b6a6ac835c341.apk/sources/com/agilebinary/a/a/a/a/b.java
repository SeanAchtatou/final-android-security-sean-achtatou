package com.agilebinary.a.a.a.a;

import org.apache.commons.logging.Log;

public class b implements Log {

    /* renamed from: a  reason: collision with root package name */
    private String f2a;

    public b(String str) {
        this.f2a = str;
    }

    public void debug(Object obj) {
        com.agilebinary.mobilemonitor.a.a.a.b.b(this.f2a, obj.toString());
    }

    public void debug(Object obj, Throwable th) {
        com.agilebinary.mobilemonitor.a.a.a.b.b(this.f2a, obj.toString(), th);
    }

    public void error(Object obj) {
        com.agilebinary.mobilemonitor.a.a.a.b.e(this.f2a, obj.toString());
    }

    public void error(Object obj, Throwable th) {
        com.agilebinary.mobilemonitor.a.a.a.b.e(this.f2a, obj.toString(), th);
    }

    public void fatal(Object obj) {
        com.agilebinary.mobilemonitor.a.a.a.b.e(this.f2a, obj.toString());
    }

    public void fatal(Object obj, Throwable th) {
        com.agilebinary.mobilemonitor.a.a.a.b.e(this.f2a, obj.toString(), th);
    }

    public void info(Object obj) {
        com.agilebinary.mobilemonitor.a.a.a.b.c(this.f2a, obj.toString());
    }

    public void info(Object obj, Throwable th) {
        com.agilebinary.mobilemonitor.a.a.a.b.c(this.f2a, obj.toString(), th);
    }

    public boolean isDebugEnabled() {
        if (!"org.apache.http.wire".equals(this.f2a) && !"org.apache.http.headers".equals(this.f2a)) {
            return com.agilebinary.mobilemonitor.a.a.a.b.a();
        }
        return false;
    }

    public boolean isErrorEnabled() {
        return com.agilebinary.mobilemonitor.a.a.a.b.a();
    }

    public boolean isFatalEnabled() {
        return com.agilebinary.mobilemonitor.a.a.a.b.a();
    }

    public boolean isInfoEnabled() {
        return com.agilebinary.mobilemonitor.a.a.a.b.a();
    }

    public boolean isTraceEnabled() {
        return com.agilebinary.mobilemonitor.a.a.a.b.a();
    }

    public boolean isWarnEnabled() {
        return com.agilebinary.mobilemonitor.a.a.a.b.a();
    }

    public void trace(Object obj) {
        com.agilebinary.mobilemonitor.a.a.a.b.a(this.f2a, obj.toString());
    }

    public void trace(Object obj, Throwable th) {
        com.agilebinary.mobilemonitor.a.a.a.b.a(this.f2a, obj.toString(), th);
    }

    public void warn(Object obj) {
        com.agilebinary.mobilemonitor.a.a.a.b.d(this.f2a, obj.toString());
    }

    public void warn(Object obj, Throwable th) {
        com.agilebinary.mobilemonitor.a.a.a.b.d(this.f2a, obj.toString(), th);
    }
}
