package com.agilebinary.a.a.b.c.d;

import com.agilebinary.a.a.b.c.c.a;
import com.agilebinary.a.a.b.c.c.b;
import com.agilebinary.a.a.b.c.f;
import com.agilebinary.a.a.b.i.c;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class d implements b, com.agilebinary.a.a.b.c.c.d {

    /* renamed from: a  reason: collision with root package name */
    public static final f f25a = new b();
    public static final f b = new c();
    public static final f c = new e();
    private static final d d = new d();
    private final SSLSocketFactory e = HttpsURLConnection.getDefaultSSLSocketFactory();
    private final a f = null;
    private volatile f g = null;

    private d() {
    }

    public static d b() {
        return d;
    }

    @Deprecated
    public Socket a() {
        return new Socket();
    }

    public Socket a(com.agilebinary.a.a.b.i.d dVar) {
        return new Socket();
    }

    @Deprecated
    public Socket a(Socket socket, String str, int i, InetAddress inetAddress, int i2, com.agilebinary.a.a.b.i.d dVar) {
        InetSocketAddress inetSocketAddress = null;
        if (inetAddress != null || i2 > 0) {
            if (i2 < 0) {
                i2 = 0;
            }
            inetSocketAddress = new InetSocketAddress(inetAddress, i2);
        }
        return a(socket, new InetSocketAddress(this.f != null ? this.f.a(str) : InetAddress.getByName(str), i), inetSocketAddress, dVar);
    }

    @Deprecated
    public Socket a(Socket socket, String str, int i, boolean z) {
        return b(socket, str, i, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException}
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      ClspMth{javax.net.SocketFactory.createSocket(java.lang.String, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException, java.net.UnknownHostException}
      ClspMth{javax.net.SocketFactory.createSocket(java.net.InetAddress, int, java.net.InetAddress, int):java.net.Socket throws java.io.IOException}
      ClspMth{javax.net.ssl.SSLSocketFactory.createSocket(java.net.Socket, java.lang.String, int, boolean):java.net.Socket throws java.io.IOException} */
    public Socket a(Socket socket, InetSocketAddress inetSocketAddress, InetSocketAddress inetSocketAddress2, com.agilebinary.a.a.b.i.d dVar) {
        if (inetSocketAddress == null) {
            throw new IllegalArgumentException("Remote address may not be null");
        } else if (dVar == null) {
            throw new IllegalArgumentException("HTTP parameters may not be null");
        } else {
            Socket socket2 = socket != null ? socket : new Socket();
            if (inetSocketAddress2 != null) {
                socket2.setReuseAddress(c.b(dVar));
                socket2.bind(inetSocketAddress2);
            }
            int f2 = c.f(dVar);
            int a2 = c.a(dVar);
            try {
                socket2.connect(inetSocketAddress, f2);
                socket2.setSoTimeout(a2);
                SSLSocket sSLSocket = socket2 instanceof SSLSocket ? (SSLSocket) socket2 : (SSLSocket) this.e.createSocket(socket2, inetSocketAddress.getHostName(), inetSocketAddress.getPort(), true);
                if (this.g != null) {
                    try {
                        this.g.a(inetSocketAddress.getHostName(), sSLSocket);
                    } catch (IOException e2) {
                        try {
                            sSLSocket.close();
                        } catch (Exception e3) {
                        }
                        throw e2;
                    }
                }
                return sSLSocket;
            } catch (SocketTimeoutException e4) {
                throw new f("Connect to " + inetSocketAddress.getHostName() + "/" + inetSocketAddress.getAddress() + " timed out");
            }
        }
    }

    public boolean a(Socket socket) {
        if (socket == null) {
            throw new IllegalArgumentException("Socket may not be null");
        } else if (!(socket instanceof SSLSocket)) {
            throw new IllegalArgumentException("Socket not created by this factory");
        } else if (!socket.isClosed()) {
            return true;
        } else {
            throw new IllegalArgumentException("Socket is closed");
        }
    }

    public Socket b(Socket socket, String str, int i, boolean z) {
        SSLSocket sSLSocket = (SSLSocket) this.e.createSocket(socket, str, i, z);
        if (this.g != null) {
            this.g.a(str, sSLSocket);
        }
        return sSLSocket;
    }
}
