package com.agilebinary.a.a.b.f.c;

import com.agilebinary.a.a.b.d.h;
import com.agilebinary.a.a.b.d.i;
import com.agilebinary.a.a.b.i.d;
import java.util.Collection;

public class w implements i {
    public h a(d dVar) {
        if (dVar == null) {
            return new v();
        }
        Collection collection = (Collection) dVar.a("http.protocol.cookie-datepatterns");
        return new v(collection != null ? (String[]) collection.toArray(new String[collection.size()]) : null);
    }
}
