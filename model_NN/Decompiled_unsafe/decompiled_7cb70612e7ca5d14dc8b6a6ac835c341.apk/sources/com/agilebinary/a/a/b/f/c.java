package com.agilebinary.a.a.b.f;

import com.agilebinary.a.a.b.aa;
import com.agilebinary.a.a.b.ac;
import com.agilebinary.a.a.b.h.g;
import com.agilebinary.a.a.b.j.e;
import com.agilebinary.a.a.b.q;
import com.agilebinary.a.a.b.r;
import java.util.Locale;

public class c implements r {

    /* renamed from: a  reason: collision with root package name */
    protected final aa f67a;

    public c() {
        this(d.f79a);
    }

    public c(aa aaVar) {
        if (aaVar == null) {
            throw new IllegalArgumentException("Reason phrase catalog must not be null.");
        }
        this.f67a = aaVar;
    }

    public q a(ac acVar, e eVar) {
        if (acVar == null) {
            throw new IllegalArgumentException("Status line may not be null");
        }
        return new g(acVar, this.f67a, a(eVar));
    }

    /* access modifiers changed from: protected */
    public Locale a(e eVar) {
        return Locale.getDefault();
    }
}
