package com.agilebinary.a.a.b.f.c;

import com.agilebinary.a.a.b.d.a;
import com.agilebinary.a.a.b.d.b;
import com.agilebinary.a.a.b.d.c;
import com.agilebinary.a.a.b.d.e;
import com.agilebinary.a.a.b.d.g;
import com.agilebinary.a.a.b.d.k;
import com.agilebinary.a.a.b.d.l;
import com.agilebinary.a.a.b.d.m;

public class ah implements c {
    public void a(b bVar, e eVar) {
        if (bVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if ((bVar instanceof m) && (bVar instanceof a) && !((a) bVar).b("version")) {
            throw new g("Violates RFC 2965. Version attribute is required.");
        }
    }

    public void a(l lVar, String str) {
        int i;
        if (lVar == null) {
            throw new IllegalArgumentException("Cookie may not be null");
        } else if (str == null) {
            throw new k("Missing value for version attribute");
        } else {
            try {
                i = Integer.parseInt(str);
            } catch (NumberFormatException e) {
                i = -1;
            }
            if (i < 0) {
                throw new k("Invalid cookie version.");
            }
            lVar.a(i);
        }
    }

    public boolean b(b bVar, e eVar) {
        return true;
    }
}
