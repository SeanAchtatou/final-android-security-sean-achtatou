package com.agilebinary.a.a.b.j;

import com.agilebinary.a.a.b.h;
import com.agilebinary.a.a.b.l;
import com.agilebinary.a.a.b.m;
import com.agilebinary.a.a.b.o;
import com.agilebinary.a.a.b.p;
import com.agilebinary.a.a.b.t;
import com.agilebinary.a.a.b.y;
import com.agilebinary.a.a.b.z;
import java.net.InetAddress;

public class k implements p {
    public void a(o oVar, e eVar) {
        if (oVar == null) {
            throw new IllegalArgumentException("HTTP request may not be null");
        } else if (eVar == null) {
            throw new IllegalArgumentException("HTTP context may not be null");
        } else {
            z b = oVar.g().b();
            if ((!oVar.g().a().equalsIgnoreCase("CONNECT") || !b.c(t.b)) && !oVar.a("Host")) {
                l lVar = (l) eVar.a("http.target_host");
                if (lVar == null) {
                    h hVar = (h) eVar.a("http.connection");
                    if (hVar instanceof m) {
                        InetAddress g = ((m) hVar).g();
                        int h = ((m) hVar).h();
                        if (g != null) {
                            lVar = new l(g.getHostName(), h);
                        }
                    }
                    if (lVar == null) {
                        if (!b.c(t.b)) {
                            throw new y("Target host missing");
                        }
                        return;
                    }
                }
                oVar.a("Host", lVar.e());
            }
        }
    }
}
