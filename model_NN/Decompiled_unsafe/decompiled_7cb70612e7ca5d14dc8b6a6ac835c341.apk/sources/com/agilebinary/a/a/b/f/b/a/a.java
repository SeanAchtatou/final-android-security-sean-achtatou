package com.agilebinary.a.a.b.f.b.a;

import com.agilebinary.a.a.b.c.n;
import com.agilebinary.a.a.b.f.b.i;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.commons.logging.Log;

@Deprecated
public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    protected final Lock f48a = new ReentrantLock();
    protected Set b = new HashSet();
    protected volatile boolean c;
    protected i d = new i();
    private final Log e = com.agilebinary.a.a.a.a.a.a(getClass());

    protected a() {
    }

    public void a() {
        this.f48a.lock();
        try {
            if (!this.c) {
                Iterator it = this.b.iterator();
                while (it.hasNext()) {
                    it.remove();
                    a(((b) it.next()).c());
                }
                this.d.a();
                this.c = true;
                this.f48a.unlock();
            }
        } finally {
            this.f48a.unlock();
        }
    }

    public void a(long j, TimeUnit timeUnit) {
        if (timeUnit == null) {
            throw new IllegalArgumentException("Time unit must not be null.");
        }
        this.f48a.lock();
        try {
            this.d.a(timeUnit.toMillis(j));
        } finally {
            this.f48a.unlock();
        }
    }

    /* access modifiers changed from: protected */
    public void a(n nVar) {
        if (nVar != null) {
            try {
                nVar.c();
            } catch (IOException e2) {
                this.e.debug("I/O error closing connection", e2);
            }
        }
    }
}
