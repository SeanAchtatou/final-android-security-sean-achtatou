package com.agilebinary.a.a.b.k;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private final String f119a;
    private final String b;
    private final String c;
    private final String d;
    private final String e;

    protected f(String str, String str2, String str3, String str4, String str5) {
        if (str == null) {
            throw new IllegalArgumentException("Package identifier must not be null.");
        }
        this.f119a = str;
        this.b = str2 == null ? "UNAVAILABLE" : str2;
        this.c = str3 == null ? "UNAVAILABLE" : str3;
        this.d = str4 == null ? "UNAVAILABLE" : str4;
        this.e = str5 == null ? "UNAVAILABLE" : str5;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static final f a(String str, ClassLoader classLoader) {
        Properties properties;
        InputStream resourceAsStream;
        if (str == null) {
            throw new IllegalArgumentException("Package identifier must not be null.");
        }
        if (classLoader == null) {
            classLoader = Thread.currentThread().getContextClassLoader();
        }
        try {
            resourceAsStream = classLoader.getResourceAsStream(str.replace('.', '/') + "/" + "version.properties");
            if (resourceAsStream != null) {
                properties = new Properties();
                properties.load(resourceAsStream);
                try {
                    resourceAsStream.close();
                } catch (IOException e2) {
                }
            } else {
                properties = null;
            }
        } catch (IOException e3) {
            properties = null;
        } catch (Throwable th) {
            resourceAsStream.close();
            throw th;
        }
        if (properties != null) {
            return a(str, properties, classLoader);
        }
        return null;
    }

    protected static final f a(String str, Map map, ClassLoader classLoader) {
        String str2;
        String str3;
        String str4;
        String str5 = null;
        if (str == null) {
            throw new IllegalArgumentException("Package identifier must not be null.");
        }
        if (map != null) {
            String str6 = (String) map.get("info.module");
            String str7 = (str6 == null || str6.length() >= 1) ? str6 : null;
            String str8 = (String) map.get("info.release");
            String str9 = (str8 == null || (str8.length() >= 1 && !str8.equals("${pom.version}"))) ? str8 : null;
            String str10 = (String) map.get("info.timestamp");
            if (str10 == null || (str10.length() >= 1 && !str10.equals("${mvn.timestamp}"))) {
                str2 = str10;
                str3 = str9;
                str4 = str7;
            } else {
                str2 = null;
                str3 = str9;
                str4 = str7;
            }
        } else {
            str2 = null;
            str3 = null;
            str4 = null;
        }
        if (classLoader != null) {
            str5 = classLoader.toString();
        }
        return new f(str, str4, str3, str2, str5);
    }

    public final String a() {
        return this.c;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer(this.f119a.length() + 20 + this.b.length() + this.c.length() + this.d.length() + this.e.length());
        stringBuffer.append("VersionInfo(").append(this.f119a).append(':').append(this.b);
        if (!"UNAVAILABLE".equals(this.c)) {
            stringBuffer.append(':').append(this.c);
        }
        if (!"UNAVAILABLE".equals(this.d)) {
            stringBuffer.append(':').append(this.d);
        }
        stringBuffer.append(')');
        if (!"UNAVAILABLE".equals(this.e)) {
            stringBuffer.append('@').append(this.e);
        }
        return stringBuffer.toString();
    }
}
