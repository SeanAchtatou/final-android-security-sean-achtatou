package com.agilebinary.a.a.b.f.b;

import com.agilebinary.a.a.b.g.b;
import com.agilebinary.a.a.b.g.e;
import com.agilebinary.a.a.b.g.f;

public class k implements b, f {

    /* renamed from: a  reason: collision with root package name */
    private final f f62a;
    private final b b;
    private final q c;
    private final String d;

    public k(f fVar, q qVar, String str) {
        this.f62a = fVar;
        this.b = fVar instanceof b ? (b) fVar : null;
        this.c = qVar;
        this.d = str == null ? "ASCII" : str;
    }

    public int a() {
        int a2 = this.f62a.a();
        if (this.c.a() && a2 != -1) {
            this.c.b(a2);
        }
        return a2;
    }

    public int a(com.agilebinary.a.a.b.k.b bVar) {
        int a2 = this.f62a.a(bVar);
        if (this.c.a() && a2 >= 0) {
            this.c.b((new String(bVar.b(), bVar.c() - a2, a2) + "\r\n").getBytes(this.d));
        }
        return a2;
    }

    public int a(byte[] bArr, int i, int i2) {
        int a2 = this.f62a.a(bArr, i, i2);
        if (this.c.a() && a2 > 0) {
            this.c.b(bArr, i, a2);
        }
        return a2;
    }

    public boolean a(int i) {
        return this.f62a.a(i);
    }

    public e b() {
        return this.f62a.b();
    }

    public boolean c() {
        if (this.b != null) {
            return this.b.c();
        }
        return false;
    }
}
