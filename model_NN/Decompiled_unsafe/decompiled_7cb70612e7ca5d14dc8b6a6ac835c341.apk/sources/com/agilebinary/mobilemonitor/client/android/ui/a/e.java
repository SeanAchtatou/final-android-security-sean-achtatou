package com.agilebinary.mobilemonitor.client.android.ui.a;

import android.os.AsyncTask;
import android.os.PowerManager;
import com.agilebinary.a.a.b.b.a.g;
import com.agilebinary.mobilemonitor.client.android.b.m;
import com.agilebinary.mobilemonitor.client.android.b.s;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.mobilemonitor.client.android.ui.AccountActivity;
import com.agilebinary.mobilemonitor.client.android.ui.ah;

public class e extends AsyncTask implements m {

    /* renamed from: a  reason: collision with root package name */
    static final String f208a = f.a("DownloadAccountTask");
    public static e b;
    private final ah c;
    private com.agilebinary.mobilemonitor.client.android.f d;
    private g e;

    public e(ah ahVar) {
        this.c = ahVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public o doInBackground(Void... voidArr) {
        o oVar;
        PowerManager.WakeLock newWakeLock = ((PowerManager) this.c.getSystemService("power")).newWakeLock(6, f208a);
        newWakeLock.acquire();
        try {
            this.d = this.c.g.a();
            this.d = new com.agilebinary.mobilemonitor.client.android.f(this.c.g.b().a().a(this.d.a().a(), this.d.a().b(), this), false);
            oVar = new o(true);
            try {
                newWakeLock.release();
            } catch (Exception e2) {
            }
            b = null;
        } catch (Exception e3) {
            Exception exc = e3;
            exc.printStackTrace();
            oVar = new o(new s(exc));
            try {
                newWakeLock.release();
            } catch (Exception e4) {
            }
            b = null;
        } catch (Throwable th) {
            try {
                newWakeLock.release();
            } catch (Exception e5) {
            }
            b = null;
            throw th;
        }
        return oVar;
    }

    public void a(g gVar) {
        this.e = gVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(o oVar) {
        AccountActivity.a(this.c, oVar, this.d);
    }
}
