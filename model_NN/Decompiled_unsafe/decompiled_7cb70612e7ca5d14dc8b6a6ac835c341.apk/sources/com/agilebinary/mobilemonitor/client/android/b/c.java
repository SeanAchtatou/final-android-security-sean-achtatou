package com.agilebinary.mobilemonitor.client.android.b;

import android.util.Log;
import java.io.Serializable;

public class c implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public String f156a;
    public String b;
    public String c;
    public long d;
    public boolean e;
    public boolean f;

    public c(String str, String str2, boolean z, String str3, long j) {
        this.f156a = str;
        this.b = str2;
        this.c = str3;
        this.d = j;
        this.e = z;
    }

    public void a(boolean z) {
        Log.d("App", "setBlocked Before: " + this);
        this.e = z;
        this.c = "client_set";
        this.d = System.currentTimeMillis();
        Log.d("App", "setBlocked After: " + this);
    }

    public String toString() {
        return "App [packagename=" + this.f156a + ", label=" + this.b + ", status=" + this.c + ", status_time=" + this.d + ", blocked=" + this.e + ", foo=" + this.f + "]";
    }
}
