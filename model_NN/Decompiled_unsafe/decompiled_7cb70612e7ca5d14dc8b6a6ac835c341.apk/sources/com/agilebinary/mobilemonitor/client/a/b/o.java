package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.a;
import com.agilebinary.mobilemonitor.client.a.b;
import com.agilebinary.phonebeagle.client.R;

public class o extends d {
    public o(String str, long j, long j2, a aVar, b bVar) {
        super(str, j, j2, aVar, bVar);
    }

    public byte d() {
        return 4;
    }

    public String d(Context context) {
        return context.getString(R.string.label_event_location_type_combined);
    }

    public String e(Context context) {
        return "";
    }
}
