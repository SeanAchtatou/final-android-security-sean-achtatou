package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.b.g;
import com.agilebinary.mobilemonitor.client.android.b.i;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.mobilemonitor.client.android.ui.a.h;
import com.agilebinary.mobilemonitor.client.android.ui.a.j;
import com.agilebinary.phonebeagle.client.R;
import com.jasonkostempski.android.calendar.CalendarDialog;
import java.util.Calendar;
import java.util.Date;

public abstract class bq extends al implements AdapterView.OnItemClickListener, i {
    static final String l = f.a("EventListActivity_base");
    protected Cursor A;
    private int B;
    private ImageView C;
    private Animation D;
    private long E;
    private boolean F;
    private Calendar G;

    /* renamed from: a  reason: collision with root package name */
    private Calendar f272a;
    private Calendar b;
    /* access modifiers changed from: private */
    public CalendarDialog i;
    private LinearLayout j;
    private TextView k;
    protected byte m;
    protected ListView n;
    protected LinearLayout o;
    protected LinearLayout p;
    protected TextView q;
    protected ImageButton r;
    protected Button s;
    protected Button t;
    protected ImageButton u;
    protected ImageButton v;
    protected bz w;
    protected cl x;
    protected AlertDialog y;
    g z;

    private static Class a(byte b2) {
        switch (b2) {
            case 1:
                return EventListActivity_LOC.class;
            case 2:
                return EventListActivity_CLL.class;
            case 3:
                return EventListActivity_SMS.class;
            case 4:
                return EventListActivity_MMS.class;
            case 5:
            case 7:
            default:
                throw new IllegalArgumentException("not yet implemented");
            case 6:
                return EventListActivity_WEB.class;
            case 8:
                return EventListActivity_SYS.class;
            case 9:
                return EventListActivity_APP.class;
            case 10:
                return EventListActivity_FBK.class;
            case 11:
                return EventListActivity_WPP.class;
            case 12:
                return EventListActivity_LIN.class;
        }
    }

    public static void a(Context context, byte b2) {
        Intent intent = new Intent(context, a(b2));
        intent.putExtra("EXTRA_EVENT_TYPE", b2);
        context.startActivity(intent);
    }

    public static void a(Context context, byte b2, com.agilebinary.mobilemonitor.client.android.ui.a.i iVar) {
        Intent intent = new Intent(context, a(b2));
        if (iVar != null) {
            intent.putExtra("EXTRA_DOWNLOAD_RESULT", iVar);
        }
        intent.putExtra("EXTRA_EVENT_TYPE", b2);
        intent.setFlags(536870912);
        context.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void b(Calendar calendar) {
        a(calendar);
        if (calendar != null) {
            c();
        }
    }

    private void c() {
        b.b(l, "downloadAll...");
        this.h.b(this.m, this.f272a);
        this.h.c(this.m);
        l();
        long[] s2 = s();
        a(s2[0], s2[1], R.string.msg_progress_downloading, false);
        b.b(l, "...downloadAll");
    }

    /* access modifiers changed from: private */
    public void m() {
        b.b(l, "downloadNew...");
        if (!this.g.d()) {
            long b2 = this.h.b(this.m);
            long[] s2 = s();
            if (b2 <= 0) {
                b2 = s2[0];
            }
            a(b2 + 1, s2[1], R.string.msg_progress_updating, true);
        } else {
            n();
        }
        b.b(l, "...downloadNew");
    }

    private void u() {
        this.s.setText(com.agilebinary.mobilemonitor.client.android.d.g.a(this).b(s()[0]));
    }

    private String v() {
        switch (this.m) {
            case 1:
                return getString(R.string.label_event_location_singular);
            case 2:
                return getString(R.string.label_event_call_singular);
            case 3:
                return getString(R.string.label_event_sms_singular);
            case 4:
                return getString(R.string.label_event_mms_singular);
            case 5:
            case 7:
            default:
                return "";
            case 6:
                return getString(R.string.label_event_web_singular);
            case 8:
                return getString(R.string.label_event_system_singular);
            case 9:
                return getString(R.string.label_event_application_singular);
            case 10:
                return getString(R.string.label_event_facebook_singular);
        }
    }

    /* access modifiers changed from: protected */
    public int a() {
        return R.layout.eventlist;
    }

    /* access modifiers changed from: protected */
    public void a(int i2, int i3, int i4) {
        b.a(l, "setDate(", "" + i2, " / " + i3, " / " + i4);
        if (this.f272a == null) {
            this.f272a = Calendar.getInstance();
        }
        this.f272a.set(5, i2);
        this.f272a.set(2, i3);
        this.f272a.set(1, i4);
        this.f272a.set(11, 0);
        this.f272a.set(12, 0);
        this.f272a.set(13, 0);
        this.f272a.set(14, 0);
        u();
    }

    /* access modifiers changed from: protected */
    public void a(long j2, long j3, int i2, boolean z2) {
        if (this.f272a == null) {
            throw new IllegalArgumentException();
        }
        b(i2);
        j a2 = j.a(getClass());
        if (a2 != null) {
            a2.a(true);
        }
        new j(this, this.z, this.m, getClass()).execute(new h(j2, j3, z2));
        b.b(l, "...download");
    }

    public void a(com.agilebinary.mobilemonitor.client.android.ui.a.i iVar) {
        b.b(l, "staticOnPostExecute...");
        if (iVar.a() == null) {
            l();
        } else if (iVar.a().a()) {
            finish();
            this.g.a(this);
            return;
        } else {
            l();
            if (!iVar.a().c()) {
                this.y.show();
            }
        }
        b.b(l, "...staticOnPostExecute");
    }

    /* access modifiers changed from: protected */
    public void a(by byVar) {
        int i2 = 8;
        boolean z2 = true;
        this.p.setVisibility(byVar == by.NO_EVENTS ? 0 : 8);
        this.n.setVisibility(byVar == by.NO_EVENTS ? 8 : 0);
        this.n.setEnabled(byVar == by.LIST);
        LinearLayout linearLayout = this.j;
        if (byVar == by.PROGRESS) {
            i2 = 0;
        }
        linearLayout.setVisibility(i2);
        if (byVar == by.PROGRESS) {
            this.C.startAnimation(this.D);
        } else {
            this.C.clearAnimation();
        }
        this.r.setEnabled(byVar != by.PROGRESS);
        this.s.setEnabled(byVar != by.PROGRESS);
        System.out.println("mFirstDayWithEvents: " + this.G.getTimeInMillis() + " / " + this.G.getTime());
        System.out.println("mCalendar: " + this.f272a.getTimeInMillis() + " / " + this.f272a.getTime());
        this.v.setEnabled(byVar != by.PROGRESS && this.f272a.after(this.G));
        ImageButton imageButton = this.u;
        if (byVar == by.PROGRESS || !this.f272a.before(this.b)) {
            z2 = false;
        }
        imageButton.setEnabled(z2);
    }

    /* access modifiers changed from: protected */
    public void a(Calendar calendar) {
        b.a(l, "setDate(", "" + calendar);
        if (calendar != null) {
            a(calendar.get(5), calendar.get(2), calendar.get(1));
        }
    }

    /* access modifiers changed from: protected */
    public void a(boolean z2) {
        b.b(l, "stopDownloading...");
        try {
            j a2 = j.a(getClass());
            if (a2 != null) {
                try {
                    a2.a(z2);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            n();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        b.b(l, "...stopDownloading");
    }

    public void a_() {
        this.w.a();
    }

    /* access modifiers changed from: protected */
    public abstract cl b();

    /* access modifiers changed from: protected */
    public void b(int i2) {
        this.B = i2;
        this.k.setText(i2);
        a(by.PROGRESS);
    }

    public boolean c(long j2) {
        return j2 > this.E;
    }

    public void finish() {
        b.b(l, "FINISH...");
        try {
            a(true);
            this.g.a(this.m, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.finish();
        b.b(l, "...FINISH");
    }

    /* access modifiers changed from: protected */
    public void l() {
        b.b(l, "updateDataFromStore...");
        this.w.a();
        this.h.c(this.m, this.h.a(this.m, this.f272a.getTimeInMillis()));
        long a2 = this.h.a(this.m);
        this.G.setTimeInMillis(a2);
        this.q.setText(getString(R.string.label_event_filter_range_none_available, new Object[]{v(), a2 == 0 ? getString(R.string.label_event_filter_range_none_available_unknown) : com.agilebinary.mobilemonitor.client.android.d.g.a(this).b(a2)}));
        n();
        b.b(l, "...updateDataFromStore");
    }

    /* access modifiers changed from: protected */
    public void n() {
        a(this.w.getCount() == 0 ? by.NO_EVENTS : by.LIST);
    }

    /* access modifiers changed from: protected */
    public void o() {
        this.f272a.add(5, 1);
        u();
        c();
    }

    public void onCreate(Bundle bundle) {
        b.b(l, "onCreate..." + this);
        super.onCreate(bundle);
        this.b = Calendar.getInstance();
        this.b.set(11, 0);
        this.b.set(12, 0);
        this.b.set(13, 0);
        this.b.set(14, 0);
        this.G = Calendar.getInstance();
        this.G.setTimeInMillis(this.h.a(this.m));
        this.B = R.string.msg_progress_loading_eventcache;
        this.n = (ListView) findViewById(R.id.eventlist_list);
        this.o = (LinearLayout) findViewById(R.id.eventlist_parent);
        this.p = (LinearLayout) findViewById(R.id.eventlist_no_events);
        this.j = (LinearLayout) findViewById(R.id.eventlist_progress);
        this.k = (TextView) findViewById(R.id.eventlist_progress_text);
        if (this.B != 0) {
            this.k.setText(this.B);
        }
        this.C = (ImageView) findViewById(R.id.eventlist_progress_anim);
        this.D = AnimationUtils.loadAnimation(this, R.anim.spinner_black_76);
        this.t = (Button) findViewById(R.id.eventlist_progress_cancel);
        this.q = (TextView) findViewById(R.id.eventlist_no_events_label);
        this.u = (ImageButton) findViewById(R.id.eventlist_navigation_next);
        this.v = (ImageButton) findViewById(R.id.eventlist_navigation_prev);
        this.r = (ImageButton) findViewById(R.id.eventlist_refresh);
        this.s = (Button) findViewById(R.id.eventlist_pick_date);
        this.z = this.g.b().b();
        this.x = b();
        this.t.setOnClickListener(new br(this));
        this.u.setOnClickListener(new bs(this));
        this.v.setOnClickListener(new bt(this));
        this.r.setOnClickListener(new bu(this));
        this.s.setOnClickListener(new bv(this));
        this.n.setOnItemClickListener(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage((int) R.string.error_downloading).setCancelable(true).setNeutralButton("OK", new bw(this));
        this.y = builder.create();
        if (bundle != null) {
            this.m = bundle.getByte("EXTRA_EVENT_TYPE");
            a(bundle.getInt("EXTRA_DATE_DAY"), bundle.getInt("EXTRA_DATE_MONTH"), bundle.getInt("EXTRA_DATE_YEAR"));
            this.B = bundle.getInt("EXTRA_PROGRESS_TEXT_ID");
            if (bundle.getBoolean("EXTRA_ALERT_DIALOG_IS_SHOWING")) {
                this.y.show();
            }
            if (bundle.getBoolean("EXTRA_CALENDAR_IS_SHOWING")) {
                q();
            }
        } else {
            this.m = getIntent().getByteExtra("EXTRA_EVENT_TYPE", (byte) 0);
            if (this.m == 0) {
                b.e(l, "AAARGH, eventType is 0");
            }
            Calendar d = this.h.d();
            if (d == null) {
                r();
            } else {
                a(d);
            }
        }
        this.A = this.h.a(this.m, (String) null);
        this.w = new bz(this, this, this.A, this.x);
        this.n.setAdapter((ListAdapter) this.w);
        this.r.setEnabled(false);
        this.s.setEnabled(false);
        this.v.setEnabled(false);
        this.u.setEnabled(false);
        b.b(l, "...onCreate");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        b.b(l, "onDestroy" + this);
        if (this.A != null) {
            this.A.close();
        }
        super.onDestroy();
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        a(true);
        aw.a(this, this.m, ((Cursor) adapterView.getItemAtPosition(i2)).getLong(0));
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        com.agilebinary.mobilemonitor.client.android.ui.a.i iVar = (com.agilebinary.mobilemonitor.client.android.ui.a.i) intent.getSerializableExtra("EXTRA_DOWNLOAD_RESULT");
        if (iVar == null) {
            t();
        } else {
            a(iVar);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        boolean z2 = true;
        b.b(l, "onSaveInstanceState...");
        bundle.putInt("EXTRA_DATE_DAY", this.f272a.get(5));
        bundle.putInt("EXTRA_DATE_MONTH", this.f272a.get(2));
        bundle.putInt("EXTRA_DATE_YEAR", this.f272a.get(1));
        bundle.putInt("EXTRA_PROGRESS_TEXT_ID", this.B);
        bundle.putByte("EXTRA_EVENT_TYPE", this.m);
        if (this.i == null) {
            z2 = false;
        }
        bundle.putBoolean("EXTRA_CALENDAR_IS_SHOWING", z2);
        bundle.putBoolean("EXTRA_ALERT_DIALOG_IS_SHOWING", this.y.isShowing());
        super.onSaveInstanceState(bundle);
        b.b(l, "...onSaveInstanceState");
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        b.b(l, "onStart...");
        super.onStart();
        if (this.F) {
            this.F = false;
        } else if (this.m == 0) {
            b.e(l, "AAARGH, TODO: why does this happen");
            new Exception().printStackTrace();
            return;
        } else {
            this.E = this.h.b(this.m);
            if (this.y.isShowing() || this.i != null) {
                l();
            } else {
                b(this.B);
                j a2 = j.a(getClass());
                if (a2 != null && !a2.isCancelled()) {
                    b.b(l, "download task is still running!!, so just do nothing...");
                } else if (!this.h.a(this.m, this.f272a)) {
                    c();
                } else if (this.g.a(this.m)) {
                    m();
                } else {
                    l();
                }
            }
        }
        b.b(l, "...onStart");
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        b.b(l, "onStop...");
        super.onStop();
        this.F = true;
        if (this.i != null && this.i.isShowing()) {
            this.i.dismiss();
        }
        if (this.y != null && this.y.isShowing()) {
            this.y.dismiss();
        }
        b.b(l, "...onStop");
    }

    /* access modifiers changed from: protected */
    public void p() {
        this.f272a.add(5, -1);
        u();
        c();
    }

    /* access modifiers changed from: protected */
    public void q() {
        long a2 = this.h.a(this.m);
        this.i = new CalendarDialog(this, new bx(this));
        this.i.setCurrentDate(this.f272a, a2 > 0 ? new Date(a2) : new Date());
        this.i.show();
    }

    /* access modifiers changed from: protected */
    public void r() {
        b.b(l, "setDate()");
        a(Calendar.getInstance());
    }

    /* access modifiers changed from: protected */
    public long[] s() {
        if (this.f272a == null) {
            throw new IllegalArgumentException();
        }
        Calendar calendar = (Calendar) this.f272a.clone();
        calendar.set(11, 0);
        calendar.set(12, 0);
        calendar.set(13, 0);
        calendar.set(14, 0);
        calendar.add(5, 1);
        calendar.add(13, -1);
        long[] jArr = {calendar.getTimeInMillis(), calendar.getTimeInMillis()};
        b.a(l, "getDateRange = ", "" + jArr[0], " | " + jArr[1]);
        return jArr;
    }

    public void t() {
        l();
    }
}
