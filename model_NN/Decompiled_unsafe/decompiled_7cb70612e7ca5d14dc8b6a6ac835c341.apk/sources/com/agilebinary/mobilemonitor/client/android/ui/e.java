package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.View;
import com.agilebinary.phonebeagle.client.R;

final class e implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AccountInfoActivity f318a;

    e(AccountInfoActivity accountInfoActivity) {
        this.f318a = accountInfoActivity;
    }

    public void onClick(View view) {
        ChangePasswordActivity.a(this.f318a, R.string.label_changepassword_reason_change, false, this.f318a.p, 0);
    }
}
