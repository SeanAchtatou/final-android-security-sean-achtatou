package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.agilebinary.phonebeagle.client.R;

class cp extends ArrayAdapter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LoginActivity f296a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cp(LoginActivity loginActivity, Context context, int i) {
        super(context, i, new String[LoginActivity.n.length]);
        this.f296a = loginActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View a(int i, View view, ViewGroup viewGroup) {
        View inflate = this.f296a.getLayoutInflater().inflate((int) R.layout.locale_row, viewGroup, false);
        ((TextView) inflate.findViewById(R.id.locale_row_label)).setText(LoginActivity.n[i].f297a.getDisplayLanguage());
        ((ImageView) inflate.findViewById(R.id.locale_row_image)).setImageResource(LoginActivity.n[i].b);
        return inflate;
    }

    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        return a(i, view, viewGroup);
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        return a(i, view, viewGroup);
    }
}
