package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.AlertDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import com.agilebinary.mobilemonitor.client.android.a;
import com.agilebinary.phonebeagle.client.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EventListActivity_LOC extends bq {
    private a B;

    /* renamed from: a  reason: collision with root package name */
    protected AlertDialog f198a;
    protected Button b;
    protected Button i;
    protected int j = 2;
    protected Set k = new HashSet();

    private double a(Cursor cursor) {
        double d = 1.0d;
        int i2 = cursor.getInt(cursor.getColumnIndex("contenttype"));
        if (i2 == 5 || i2 == 6 || i2 == 4) {
            if (cursor.getInt(cursor.getColumnIndex("powersave")) == 1) {
                d = 2.0d;
            } else if (cursor.getInt(cursor.getColumnIndex("valloc")) != 1) {
                d = 9.9999999E7d;
            }
        } else if (i2 == 7 || i2 == 8 || i2 == 12) {
            d = 1.5d;
        }
        Double valueOf = Double.valueOf(cursor.getDouble(cursor.getColumnIndex("accuracy")));
        if (valueOf == null) {
            return Double.MAX_VALUE;
        }
        return d * valueOf.doubleValue();
    }

    private void a(List list) {
        MapActivity_OSM.a(this, list);
    }

    /* access modifiers changed from: private */
    public void u() {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(this.k);
        Collections.sort(arrayList, new bk(this));
        a(arrayList);
    }

    public void a(long j2) {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(Long.valueOf(j2));
        a(arrayList);
    }

    public void a(long j2, boolean z) {
        if (z) {
            this.k.add(Long.valueOf(j2));
        } else {
            this.k.remove(Long.valueOf(j2));
        }
        this.b.setEnabled(this.k.size() > 0);
    }

    /* access modifiers changed from: protected */
    public void a(by byVar) {
        boolean z = true;
        super.a(byVar);
        this.i.setEnabled(byVar != by.PROGRESS);
        Button button = this.b;
        if (byVar == by.PROGRESS) {
            z = false;
        }
        button.setEnabled(z);
    }

    public void a(boolean z) {
        super.a(z);
        if (this.B != null) {
            this.B.c();
        }
    }

    /* access modifiers changed from: protected */
    public cl b() {
        return new bj(this);
    }

    public boolean b(long j2) {
        return this.k.contains(Long.valueOf(j2));
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public void c() {
        int i2;
        int columnIndex = this.A.getColumnIndex("lat");
        this.k.clear();
        if (this.A.moveToFirst()) {
            switch (this.j) {
                case 0:
                    this.k.clear();
                    break;
                case 1:
                    do {
                        if (!this.A.isNull(columnIndex)) {
                            this.k.add(Long.valueOf(this.A.getLong(0)));
                        }
                    } while (this.A.moveToNext());
                    break;
                case 2:
                    Long l = null;
                    Double d = null;
                    Long l2 = null;
                    do {
                        long j2 = this.A.getLong(0);
                        if (!this.A.isNull(columnIndex)) {
                            if (l != null && Math.abs(l.longValue() - j2) > 5000) {
                                if (l2 != null) {
                                    this.k.add(l2);
                                }
                                d = null;
                                l2 = null;
                            }
                            double a2 = a(this.A);
                            if (l2 == null || a2 < d.doubleValue()) {
                                d = Double.valueOf(a2);
                                l2 = Long.valueOf(j2);
                            }
                            l = Long.valueOf(j2);
                        }
                    } while (this.A.moveToNext());
                    if (l2 != null) {
                        this.k.add(l2);
                        break;
                    }
                    break;
                case 3:
                    int columnIndex2 = this.A.getColumnIndex("contenttype");
                    do {
                        if (!this.A.isNull(columnIndex) && ((i2 = this.A.getInt(columnIndex2)) == 5 || i2 == 6 || i2 == 4)) {
                            this.k.add(Long.valueOf(this.A.getLong(0)));
                        }
                    } while (this.A.moveToNext());
                    break;
                case 4:
                    int columnIndex3 = this.A.getColumnIndex("contenttype");
                    do {
                        if (!this.A.isNull(columnIndex) && this.A.getInt(columnIndex3) == 5) {
                            if (this.A.getInt(this.A.getColumnIndex("valloc")) == 1) {
                                this.k.add(Long.valueOf(this.A.getLong(0)));
                            }
                        }
                    } while (this.A.moveToNext());
                    break;
            }
            this.w.notifyDataSetInvalidated();
        }
        this.b.setEnabled(this.k.size() > 0);
    }

    /* access modifiers changed from: protected */
    public void l() {
        super.l();
        c();
    }

    public boolean m() {
        return this.n.isEnabled();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            this.j = bundle.getInt("EXTRA_FILTER_ID");
        }
        this.o.addView(getLayoutInflater().inflate((int) R.layout.eventlist_footer_loc, (ViewGroup) null), 1);
        this.b = (Button) findViewById(R.id.eventlist_loc_showmap);
        this.b.setOnClickListener(new bg(this));
        this.i = (Button) findViewById(R.id.eventlist_loc_filter);
        this.i.setOnClickListener(new bh(this));
        CharSequence[] charSequenceArr = {getString(R.string.label_map_filter_clear), getString(R.string.label_map_filter_all), getString(R.string.label_map_filter_smart), getString(R.string.label_map_filter_gpsnet), getString(R.string.label_map_filter_gps_only)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle((int) R.string.label_map_filter_prompt);
        builder.setSingleChoiceItems(charSequenceArr, this.j, new bi(this));
        this.f198a = builder.create();
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("EXTRA_FILTER_ID", this.j);
    }
}
