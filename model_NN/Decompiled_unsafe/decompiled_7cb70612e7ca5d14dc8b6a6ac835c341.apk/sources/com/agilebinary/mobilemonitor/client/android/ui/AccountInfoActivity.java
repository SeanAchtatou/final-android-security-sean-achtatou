package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.android.b.c.a;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.phonebeagle.client.R;

public class AccountInfoActivity extends al {

    /* renamed from: a  reason: collision with root package name */
    private static final String f190a = f.a("AccountInfoActivity");
    private Button b;
    private Button i;
    private Button j;
    private TextView k;
    private TextView l;
    private TextView m;
    private TextView n;
    private TextView o;
    /* access modifiers changed from: private */
    public a p;
    private TextView q;
    private TextView r;
    private TextView s;

    public static void a(Activity activity, a aVar) {
        Intent intent = new Intent(activity, AccountInfoActivity.class);
        intent.putExtra("EXTRA_ACC", aVar);
        activity.startActivity(intent);
    }

    private void a(a aVar) {
        this.p = aVar;
        this.k.setText(aVar.a());
        this.l.setText((aVar.d() == null || aVar.d().equals("")) ? getString(R.string.label_accountinfo_notset) : aVar.d());
        this.q.setText((aVar.c() == null || aVar.c().equals("")) ? getString(R.string.label_accountinfo_notset) : aVar.c());
        this.m.setText(aVar.f() + " " + aVar.g());
        this.n.setText(aVar.e() ? R.string.label_accountinfo_status_activated : R.string.label_accountinfo_status_deactivated);
        this.o.setText(com.agilebinary.mobilemonitor.client.android.f.a(this, aVar));
        String t = aVar.t();
        if (t == null || "0".equals(t)) {
            this.r.setText(getString(R.string.label_accountinfo_watcherversion_notinstalled));
        } else {
            this.r.setText(getString(R.string.label_accountinfo_watcherversion_installed, new Object[]{t}));
        }
        String u = aVar.u();
        if (u == null || u.trim().length() == 0) {
            this.s.setText(getString(R.string.label_accountinfo_alertphonenumber_unset));
        } else if ("client_set".equals(u)) {
            this.s.setText(getString(R.string.label_accountinfo_alertphonenumber_clientset));
        } else {
            this.s.setText(getString(R.string.label_accountinfo_alertphonenumber_set, new Object[]{u}));
        }
    }

    /* access modifiers changed from: protected */
    public int a() {
        return R.layout.accountinfo;
    }

    public void onCreate(Bundle bundle) {
        boolean z = true;
        super.onCreate(bundle);
        this.k = (TextView) findViewById(R.id.account_key);
        this.l = (TextView) findViewById(R.id.account_email);
        this.q = (TextView) findViewById(R.id.account_alias);
        this.m = (TextView) findViewById(R.id.account_product);
        this.n = (TextView) findViewById(R.id.account_status);
        this.o = (TextView) findViewById(R.id.account_target);
        this.r = (TextView) findViewById(R.id.account_watcherversion);
        this.s = (TextView) findViewById(R.id.account_alertphonenumber);
        this.b = (Button) findViewById(R.id.accountinfo_changepassword);
        this.b.setEnabled(!this.g.d());
        this.b.setOnClickListener(new e(this));
        this.i = (Button) findViewById(R.id.accountinfo_changeemail);
        this.i.setEnabled(!this.g.d());
        this.i.setOnClickListener(new f(this));
        this.j = (Button) findViewById(R.id.accountinfo_changealias);
        Button button = this.j;
        if (this.g.d()) {
            z = false;
        }
        button.setEnabled(z);
        this.j.setOnClickListener(new g(this));
        a((a) getIntent().getSerializableExtra("EXTRA_ACC"));
    }
}
