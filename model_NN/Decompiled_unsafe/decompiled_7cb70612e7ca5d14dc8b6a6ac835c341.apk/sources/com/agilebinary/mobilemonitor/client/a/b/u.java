package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.a;
import com.agilebinary.mobilemonitor.client.a.b;
import com.agilebinary.phonebeagle.client.R;

public class u extends g {

    /* renamed from: a  reason: collision with root package name */
    protected long f134a;
    protected int b;

    public u(String str, long j, long j2, a aVar, b bVar) {
        super(str, j, j2, aVar, bVar);
        this.f134a = bVar.a(aVar.d());
        this.b = aVar.c();
    }

    public long a() {
        return this.f134a;
    }

    public String a(Context context) {
        int i = 0;
        switch (b()) {
            case 1:
                i = R.string.label_event_system_power_off;
                break;
            case 2:
                i = R.string.label_event_system_power_on;
                break;
            case 3:
                i = R.string.label_event_system_power_reboot;
                break;
            case 4:
                i = R.string.label_event_system_airplanemode_off;
                break;
            case 5:
                i = R.string.label_event_system_airplanemode_on;
                break;
            case 6:
                i = R.string.label_event_system_battery_low;
                break;
            case 7:
                i = R.string.label_event_system_battery_ok;
                break;
            case 8:
                i = R.string.label_event_system_sim_changed;
                break;
        }
        return context.getString(i);
    }

    public int b() {
        return this.b;
    }

    public byte d() {
        return 10;
    }
}
