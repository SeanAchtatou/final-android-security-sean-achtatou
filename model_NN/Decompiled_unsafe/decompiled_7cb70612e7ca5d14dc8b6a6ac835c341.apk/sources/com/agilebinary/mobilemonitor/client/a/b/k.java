package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.a;
import com.agilebinary.mobilemonitor.client.a.b;
import com.agilebinary.phonebeagle.client.R;

public class k extends c {
    protected String c;

    public k(String str, long j, long j2, a aVar, b bVar) {
        super(str, j, j2, aVar, bVar);
        this.c = aVar.g();
    }

    public CharSequence a(Context context) {
        return null;
    }

    public boolean c_() {
        return false;
    }

    public byte d() {
        return 9;
    }

    public String d(Context context) {
        return context.getString(R.string.label_event_location_type_cellbroadcast);
    }

    public String e(Context context) {
        return this.c;
    }

    public Double i() {
        return null;
    }
}
