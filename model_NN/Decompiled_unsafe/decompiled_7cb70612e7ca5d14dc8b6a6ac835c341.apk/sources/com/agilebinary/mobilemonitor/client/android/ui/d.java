package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.View;
import com.agilebinary.mobilemonitor.client.android.b.c.a;
import com.agilebinary.mobilemonitor.client.android.ui.a.l;
import com.agilebinary.phonebeagle.client.R;

final class d implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f307a;
    final /* synthetic */ AccountActivity b;

    d(AccountActivity accountActivity, a aVar) {
        this.b = accountActivity;
        this.f307a = aVar;
    }

    public void onClick(View view) {
        this.b.b((int) R.string.msg_progress_communicating);
        l.b = new l(this.b, this.f307a.a());
        l.b.execute(this.f307a.a(), this.f307a.b());
    }
}
