package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.b.c.a;
import com.agilebinary.mobilemonitor.client.android.d.d;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.mobilemonitor.client.android.ui.a.e;
import com.agilebinary.mobilemonitor.client.android.ui.a.k;
import com.agilebinary.mobilemonitor.client.android.ui.a.o;
import com.agilebinary.phonebeagle.client.R;
import java.io.UnsupportedEncodingException;

public class AccountActivity extends al {

    /* renamed from: a  reason: collision with root package name */
    private static final String f189a = f.a("AccountActivity");
    private ViewGroup b;
    /* access modifiers changed from: private */
    public EditText i;
    /* access modifiers changed from: private */
    public EditText j;
    private ViewGroup k;
    private Button l;
    private LinearLayout m;
    private ImageView n;
    private Animation o;
    private com.agilebinary.mobilemonitor.client.android.f p;

    public static void a(Activity activity) {
        activity.startActivity(new Intent(activity, AccountActivity.class));
    }

    public static void a(Context context, o oVar, com.agilebinary.mobilemonitor.client.android.f fVar) {
        Intent intent = new Intent(context, AccountActivity.class);
        intent.putExtra("EXTRA_MACC", fVar);
        intent.putExtra("EXTRA_SERVICE_RESULT_ACCOUNT_LOAD", oVar);
        intent.setFlags(536870912);
        context.startActivity(intent);
    }

    public static void a(Context context, o oVar, String str) {
        Intent intent = new Intent(context, AccountActivity.class);
        intent.putExtra("EXTRA_SERVICE_RESULT_LINKED_ACCOUNT_REMOVE", oVar);
        intent.putExtra("EXTRA_SERVICE_RESULT_KEY", str);
        intent.setFlags(536870912);
        context.startActivity(intent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, ?[OBJECT, ARRAY], int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void a(LayoutInflater layoutInflater, a aVar, int i2) {
        ViewGroup viewGroup = (ViewGroup) layoutInflater.inflate((int) R.layout.account_row, (ViewGroup) null, false);
        this.k.addView(viewGroup);
        Button button = (Button) viewGroup.findViewById(R.id.account_row_switch);
        Button button2 = (Button) viewGroup.findViewById(R.id.account_row_remove);
        Button button3 = (Button) viewGroup.findViewById(R.id.account_row_details);
        ((TextView) viewGroup.findViewById(R.id.account_row_type)).setText(i2);
        ((TextView) viewGroup.findViewById(R.id.account_row_key)).setText(aVar.a());
        ((TextView) viewGroup.findViewById(R.id.account_row_alias)).setText("[" + aVar.c() + "]");
        if (aVar.a().equals(this.g.a().c())) {
            button.setVisibility(4);
        }
        if (aVar.a().equals(this.g.a().a().a())) {
            button2.setVisibility(4);
        }
        button3.setOnClickListener(new b(this, aVar));
        button.setOnClickListener(new c(this, aVar));
        button2.setOnClickListener(new d(this, aVar));
    }

    private void a(com.agilebinary.mobilemonitor.client.android.f fVar) {
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService("layout_inflater");
        this.p = fVar;
        a(layoutInflater, fVar.a(), (int) R.string.label_account_row_login_account);
        if (fVar.b() != null && fVar.b().size() > 0) {
            findViewById(R.id.account_no_linked_accounts).setVisibility(8);
            for (a a2 : fVar.b()) {
                a(layoutInflater, a2, (int) R.string.label_account_row_linked_account);
            }
        }
        this.m.setVisibility(8);
        this.n.clearAnimation();
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2) {
        try {
            String a2 = d.a(str2);
            b((int) R.string.msg_progress_communicating);
            k.b = new k(this, str);
            k.b.execute(str, a2);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static void b(Context context, o oVar, String str) {
        Intent intent = new Intent(context, AccountActivity.class);
        intent.putExtra("EXTRA_SERVICE_RESULT_LINKED_ACCOUNT_ADD", oVar);
        intent.putExtra("EXTRA_SERVICE_RESULT_KEY", str);
        intent.setFlags(536870912);
        context.startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public int a() {
        return R.layout.account;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.b = (ViewGroup) findViewById(R.id.account_container);
        this.m = (LinearLayout) findViewById(R.id.account_progress);
        this.n = (ImageView) findViewById(R.id.account_progress_anim);
        this.o = AnimationUtils.loadAnimation(this, R.anim.spinner_black_76);
        this.i = (EditText) findViewById(R.id.account_addlinked_key);
        this.j = (EditText) findViewById(R.id.account_addlinked_password);
        this.l = (Button) findViewById(R.id.account_addlinked);
        this.k = (ViewGroup) findViewById(R.id.account_rows);
        this.l.setOnClickListener(new a(this));
        if (bundle != null) {
            this.p = (com.agilebinary.mobilemonitor.client.android.f) bundle.getSerializable("EXTRA_ACC");
        }
        if (this.p != null) {
            a(this.p);
            this.b.setVisibility(0);
        } else if (e.b == null) {
            this.b.setVisibility(8);
            this.m.setVisibility(0);
            this.n.startAnimation(this.o);
            e.b = new e(this);
            e.b.execute(new Void[0]);
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        b.b(f189a, "onNewIntent...");
        o oVar = (o) intent.getSerializableExtra("EXTRA_SERVICE_RESULT_ACCOUNT_LOAD");
        o oVar2 = (o) intent.getSerializableExtra("EXTRA_SERVICE_RESULT_LINKED_ACCOUNT_ADD");
        o oVar3 = (o) intent.getSerializableExtra("EXTRA_SERVICE_RESULT_LINKED_ACCOUNT_REMOVE");
        intent.getStringExtra("EXTRA_SERVICE_RESULT_KEY");
        b(false);
        if (oVar != null) {
            com.agilebinary.mobilemonitor.client.android.f fVar = (com.agilebinary.mobilemonitor.client.android.f) intent.getSerializableExtra("EXTRA_MACC");
            b(false);
            this.b.setVisibility(0);
            if (fVar != null) {
                a(fVar);
                this.g.a(fVar);
            }
        }
        if (oVar2 != null) {
            if (oVar2.b() != null) {
                if (oVar2.b().c()) {
                    a((int) R.string.error_service_account_general);
                } else if (oVar2.b().b()) {
                    a((int) R.string.error_service_account_general);
                } else {
                    a((int) R.string.error_service_account_login);
                }
            } else if (oVar2.a()) {
                finish();
                a((Activity) this);
            }
        }
        if (oVar3 != null) {
            if (oVar3.b() != null) {
                if (oVar3.b().c()) {
                    a((int) R.string.error_service_account_general);
                } else if (oVar3.b().b()) {
                    a((int) R.string.error_service_account_general);
                } else {
                    a((int) R.string.error_service_account_login);
                }
            } else if (oVar3.a()) {
                finish();
                a((Activity) this);
            }
        }
        b.b(f189a, "...onNewIntent");
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putSerializable("EXTRA_ACC", this.p);
    }
}
