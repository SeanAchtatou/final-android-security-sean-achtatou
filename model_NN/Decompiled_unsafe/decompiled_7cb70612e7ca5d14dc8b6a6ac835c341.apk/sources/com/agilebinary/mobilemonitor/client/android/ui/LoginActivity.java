package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.b.x;
import com.agilebinary.mobilemonitor.client.android.d.d;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.mobilemonitor.client.android.ui.a.m;
import com.agilebinary.mobilemonitor.client.android.ui.a.o;
import com.agilebinary.phonebeagle.client.R;
import java.io.UnsupportedEncodingException;
import java.util.Locale;

public class LoginActivity extends ah {

    /* renamed from: a  reason: collision with root package name */
    private static final String f199a = f.a("LoginActivity");
    /* access modifiers changed from: private */
    public static cq[] n;
    private Button b;
    private Button h;
    /* access modifiers changed from: private */
    public EditText i;
    /* access modifiers changed from: private */
    public EditText j;
    private TextView k;
    private boolean l;
    private Spinner m;

    static {
        String[] split = x.a().k().split(",");
        n = new cq[0];
        int i2 = 0;
        for (String str : split) {
            cq cqVar = null;
            if ("en".equals(str)) {
                cqVar = new cq(Locale.UK, R.drawable.flag_uk);
            } else if ("de".equals(str)) {
                cqVar = new cq(Locale.GERMANY, R.drawable.flag_de);
            } else if ("zh".equals(str)) {
                cqVar = new cq(Locale.CHINA, R.drawable.flag_cn);
            }
            if (cqVar != null) {
                cq[] cqVarArr = new cq[(i2 + 1)];
                System.arraycopy(n, 0, cqVarArr, 0, n.length);
                n = cqVarArr;
                n[i2] = cqVar;
                i2++;
            }
        }
    }

    public static void a(Context context, o oVar) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra("EXTRA_LOGIN_RESULT", oVar);
        intent.setFlags(536870912);
        context.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2) {
        b((int) R.string.msg_progress_communicating);
        m.b = new m(this);
        m.b.execute(str, str2);
    }

    private int l() {
        for (int i2 = 0; i2 < n.length; i2++) {
            if (Locale.getDefault().getLanguage().equals(n[i2].f297a.getLanguage())) {
                return i2;
            }
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public int a() {
        return R.layout.login;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.l = true;
        com.agilebinary.mobilemonitor.client.android.f a2 = this.g.a();
        String str = "";
        try {
            str = d.a(a2.c());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        if (this.g.d() || !str.equals(a2.d())) {
            onActivityResult(0, -1, null);
        } else {
            ChangePasswordActivity.a(this, R.string.label_changepassword_reason_default, true, a2.a(), 0);
        }
    }

    /* access modifiers changed from: protected */
    public void e() {
        if (m.b != null) {
            try {
                m.b.a();
            } catch (Exception e) {
            }
        }
    }

    public void finish() {
        b.b(f199a, "FINISH...");
        super.finish();
        b.b(f199a, "...FINISH");
    }

    /* access modifiers changed from: protected */
    public CharSequence i() {
        return getString(R.string.titlebar_fmt_login, new Object[]{getString(R.string.app_name)});
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (i2 == 0) {
            String f = this.g.a().f();
            if (this.g.d() || !(f == null || f.trim().length() == 0)) {
                onActivityResult(1, -1, null);
            } else {
                ChangeEmailActivity.a(this, R.string.label_changeemail_text_missing, true, this.g.a().a(), 1);
            }
        } else if (i2 == 1) {
            finish();
            this.l = false;
            startActivity(new Intent(this, MainActivity.class));
        } else if (i2 == 2 && i3 == -1) {
            a((int) R.string.msg_password_reset_description);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        b.b(f199a, "onCreate...");
        this.b = (Button) findViewById(R.id.login_login);
        this.h = (Button) findViewById(R.id.login_demo);
        this.i = (EditText) findViewById(R.id.login_key);
        this.j = (EditText) findViewById(R.id.login_password);
        this.k = (TextView) findViewById(R.id.login_link);
        this.k.setMovementMethod(LinkMovementMethod.getInstance());
        this.k.setText(Html.fromHtml(x.a().e()));
        if (bundle != null) {
            this.i.setText(bundle.getString("EXTRA_KEY"));
            this.j.setText(bundle.getString("EXTRA_PASSWORD"));
        }
        this.b.setOnClickListener(new cm(this));
        this.h.setOnClickListener(new cn(this));
        this.m = (Spinner) findViewById(R.id.login_locale);
        this.m.setAdapter((SpinnerAdapter) new cp(this, this, R.layout.locale_row));
        this.m.setSelection(l());
        this.m.setOnItemSelectedListener(new co(this));
        b.b(f199a, "...onCreate");
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        b.b(f199a, "onNewIntent...");
        o oVar = (o) intent.getSerializableExtra("EXTRA_LOGIN_RESULT");
        b(false);
        if (oVar != null) {
            if (oVar.b() != null) {
                if (!oVar.b().c()) {
                    if (oVar.b().b()) {
                        a((int) R.string.error_service_account_general);
                    } else {
                        a((int) R.string.error_service_account_login);
                    }
                }
            } else if (oVar.a()) {
                b();
                return;
            } else {
                a((int) R.string.error_service_account_login);
            }
        }
        b.b(f199a, "...onNewIntent");
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_login_forgottenpw /*2131296482*/:
                ResetPasswordActivity.a(this, 2);
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        b.b(f199a, "onSaveInstanceState...");
        bundle.putString("EXTRA_KEY", this.i.getText().toString());
        bundle.putString("EXTRA_PASSWORD", this.i.getText().toString());
        super.onSaveInstanceState(bundle);
        b.b(f199a, "...onSaveInstanceState");
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        b.b(f199a, "onStart...");
        if (!this.l && this.g.c()) {
            b();
        }
        b.b(f199a, "...onStart");
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        b.b(f199a, "onStop...");
        super.onStop();
        b.b(f199a, "...onStop");
    }
}
