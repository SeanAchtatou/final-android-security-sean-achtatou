package com.agilebinary.mobilemonitor.client.android.ui;

import android.database.Cursor;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.android.d.g;
import com.agilebinary.phonebeagle.client.R;

public class ck extends LinearLayout {
    protected TextView d;
    protected ImageView e;
    protected bq f;

    public ck(bq bqVar, AttributeSet attributeSet) {
        super(bqVar, attributeSet);
        this.f = bqVar;
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.d = (TextView) findViewById(R.id.eventlist_rowview_base_time);
        this.e = (ImageView) findViewById(R.id.eventlist_rowview_base_star);
    }

    public void a(Cursor cursor) {
        long j = cursor.getLong(0);
        this.d.setText(g.a(getContext()).a(j));
        this.e.setEnabled(this.f.c(j));
    }
}
