package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.a.b.b;
import com.agilebinary.mobilemonitor.client.a.b.j;
import com.agilebinary.phonebeagle.client.R;

public class EventDetailsActivity_LIN extends aw {
    private TextView n;
    private TextView o;
    private TextView p;
    private TextView q;
    private TextView r;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public void a(ViewGroup viewGroup) {
        ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.eventdetails_lin, viewGroup, true);
        this.n = (TextView) findViewById(R.id.eventdetails_lin_remoteparty);
        this.q = (TextView) findViewById(R.id.eventdetails_lin_time);
        this.o = (TextView) findViewById(R.id.eventdetails_lin_speed);
        this.p = (TextView) findViewById(R.id.eventdetails_lin_message);
        this.r = (TextView) findViewById(R.id.eventdetails_lin_fromto);
    }

    /* access modifiers changed from: protected */
    public void a(b bVar) {
        super.a(bVar);
        j jVar = (j) bVar;
        this.r.setText(getResources().getString(jVar.n() == 1 ? R.string.label_event_from : R.string.label_event_to));
        this.q.setText(jVar.m());
        this.n.setText(jVar.b());
        this.o.setText(com.agilebinary.mobilemonitor.client.android.d.b.a(this, jVar));
        this.p.setText(jVar.c());
        if (com.agilebinary.mobilemonitor.client.android.d.b.a(jVar)) {
            this.o.setTextColor(getResources().getColor(R.color.warning));
        } else {
            this.o.setTextColor(getResources().getColor(R.color.text));
        }
    }
}
