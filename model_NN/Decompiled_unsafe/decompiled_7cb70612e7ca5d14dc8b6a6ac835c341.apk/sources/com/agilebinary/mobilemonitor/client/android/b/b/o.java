package com.agilebinary.mobilemonitor.client.android.b.b;

import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.b.c.d;
import com.agilebinary.mobilemonitor.client.android.b.c.e;
import com.agilebinary.mobilemonitor.client.android.b.l;
import com.agilebinary.mobilemonitor.client.android.b.m;
import com.agilebinary.mobilemonitor.client.android.b.s;
import com.agilebinary.mobilemonitor.client.android.b.x;
import com.agilebinary.mobilemonitor.client.android.d.f;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class o extends a implements l {
    private static final String b = f.a("ProductionLocationServiceGSM");

    public List a(com.agilebinary.mobilemonitor.client.android.f fVar, List list, m mVar) {
        com.agilebinary.mobilemonitor.client.android.b.o oVar;
        b.b(b, "getGeocodeGsmCellLocations...");
        try {
            JSONArray jSONArray = new JSONArray();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                jSONArray.put(new JSONObject(((d) it.next()).a()));
            }
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("gsm_cell_list", jSONArray);
            byte[] bytes = jSONObject.toString().getBytes("UTF-8");
            String format = String.format(x.a().d() + "ServiceLocationRetrieveGSMCell?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s", "1", a(fVar.c()), a(fVar.d()));
            b.a(b, "URL: ", format);
            oVar = this.f149a.a(format, bytes, x.a().n(), mVar);
            try {
                if (oVar.b()) {
                    JSONArray jSONArray2 = new JSONObject(b(oVar)).getJSONArray("geo_code_gsm_cell_list");
                    ArrayList arrayList = new ArrayList();
                    if (jSONArray2.length() > 0) {
                        for (int i = 0; i < jSONArray2.length(); i++) {
                            e eVar = new e();
                            eVar.a(jSONArray2.getJSONObject(i).getLong("create_date"));
                            eVar.e(jSONArray2.getJSONObject(i).getInt("geocoder_source_id"));
                            eVar.a(jSONArray2.getJSONObject(i).getInt("mobile_country_code"));
                            eVar.b(jSONArray2.getJSONObject(i).getInt("mobile_network_code"));
                            eVar.d(jSONArray2.getJSONObject(i).getInt("location_area_code"));
                            eVar.c(jSONArray2.getJSONObject(i).getInt("cell_id"));
                            com.agilebinary.mobilemonitor.client.android.b.c.f fVar2 = new com.agilebinary.mobilemonitor.client.android.b.c.f();
                            fVar2.a(jSONArray2.getJSONObject(i).getDouble("latitude"));
                            fVar2.b(jSONArray2.getJSONObject(i).getDouble("longitude"));
                            fVar2.c(jSONArray2.getJSONObject(i).getDouble("accuracy"));
                            eVar.a(fVar2);
                            arrayList.add(eVar);
                        }
                    }
                    a(oVar);
                    b.b(b, "...getGeocodeGsmCellLocations");
                    return arrayList;
                }
                throw new s(oVar.c());
            } catch (Exception e) {
                e = e;
                try {
                    throw new s(e);
                } catch (Throwable th) {
                    th = th;
                    a(oVar);
                    throw th;
                }
            }
        } catch (Exception e2) {
            e = e2;
            oVar = null;
        } catch (Throwable th2) {
            th = th2;
            oVar = null;
            a(oVar);
            throw th;
        }
    }
}
