package com.agilebinary.mobilemonitor.client.android;

import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.a.e;
import com.agilebinary.mobilemonitor.client.a.f;

final class g implements e {
    g() {
    }

    public f a(String str, int i) {
        if (i > 524288) {
            b.a(MyApplication.b, "skipping oversized MMS part of size ", "" + i);
            return f.NO;
        } else if (str.startsWith("image/")) {
            return f.BITMAP;
        } else {
            b.a(MyApplication.b, "skipping unknown MMS part of type", str);
            return f.NO;
        }
    }
}
