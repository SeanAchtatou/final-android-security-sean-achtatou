package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.a;
import com.agilebinary.mobilemonitor.client.a.b;

public abstract class e extends b {
    protected long q;

    public e(String str, long j, long j2, a aVar, b bVar) {
        super(str, j, j2, aVar, bVar);
        this.q = bVar.a(aVar.d());
    }

    public String b(Context context) {
        return d(context);
    }

    public String c(Context context) {
        return e(context);
    }

    public abstract String d(Context context);

    public abstract String e(Context context);

    public double f(Context context) {
        Double i = i();
        if (i == null) {
            return -1.0d;
        }
        return i.doubleValue();
    }

    public abstract Double i();
}
