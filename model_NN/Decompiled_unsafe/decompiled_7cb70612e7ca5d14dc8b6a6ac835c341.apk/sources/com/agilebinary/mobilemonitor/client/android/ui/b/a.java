package com.agilebinary.mobilemonitor.client.android.ui.b;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import com.agilebinary.mobilemonitor.client.a.b.e;
import com.agilebinary.mobilemonitor.client.a.d;
import com.agilebinary.phonebeagle.client.R;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.osmdroid.c;
import org.osmdroid.d.a.f;
import org.osmdroid.d.b;
import org.osmdroid.d.h;

public class a extends f {

    /* renamed from: a  reason: collision with root package name */
    protected final Paint f248a;
    protected final Paint b;
    protected final Paint c;
    protected final Paint d;
    private List e;
    private Set f;
    private int g;

    public a(Context context) {
        this(context, new org.osmdroid.a(context));
    }

    public a(Context context, c cVar) {
        super(cVar);
        this.g = -1;
        this.f248a = new Paint();
        this.f248a.setColor(context.getResources().getColor(R.color.accuraccy_coords_fill));
        this.f248a.setStyle(Paint.Style.FILL);
        this.f248a.setAntiAlias(true);
        this.b = new Paint();
        this.b.setColor(context.getResources().getColor(R.color.accuraccy_coords_border));
        this.b.setStyle(Paint.Style.STROKE);
        this.b.setStrokeWidth(1.0f);
        this.b.setAntiAlias(true);
        this.c = new Paint();
        this.c.setColor(context.getResources().getColor(R.color.accuraccy_cell_fill));
        this.c.setStyle(Paint.Style.FILL);
        this.c.setAntiAlias(true);
        this.d = new Paint();
        this.d.setColor(context.getResources().getColor(R.color.accuraccy_cell_border));
        this.d.setStyle(Paint.Style.STROKE);
        this.d.setStrokeWidth(1.0f);
        this.d.setAntiAlias(true);
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, b bVar) {
    }

    public void a(List list) {
        this.e = list;
    }

    public void b(Canvas canvas, b bVar) {
        try {
            if (this.e != null && this.e.size() != 0) {
                boolean z = false;
                h projection = bVar.getProjection();
                if (this.g != projection.c()) {
                    this.g = projection.c();
                    z = true;
                }
                if (z) {
                    this.f = new HashSet(this.e.size());
                    Point point = new Point();
                    for (e eVar : this.e) {
                        d dVar = (d) eVar;
                        projection.a(dVar.c(), point);
                        this.f.add(new b(this, point.x, point.y, projection.a((float) dVar.b()), dVar instanceof com.agilebinary.mobilemonitor.client.a.b.c ? this.c : this.f248a, dVar instanceof com.agilebinary.mobilemonitor.client.a.b.c ? this.d : this.b));
                    }
                }
                for (b bVar2 : this.f) {
                    canvas.drawCircle((float) bVar2.f249a, (float) bVar2.b, bVar2.c, bVar2.d);
                    canvas.drawCircle((float) bVar2.f249a, (float) bVar2.b, bVar2.c, bVar2.e);
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
