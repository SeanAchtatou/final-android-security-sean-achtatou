package com.agilebinary.mobilemonitor.client.android.ui.b;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import com.agilebinary.mobilemonitor.client.a.b.e;
import com.agilebinary.mobilemonitor.client.a.d;
import com.agilebinary.phonebeagle.client.R;
import java.util.Iterator;
import java.util.List;
import org.osmdroid.a;
import org.osmdroid.d.a.f;
import org.osmdroid.d.b;
import org.osmdroid.d.h;

public class c extends f {

    /* renamed from: a  reason: collision with root package name */
    protected final Paint f250a = new Paint();
    private Path b;
    private List c;
    private int d = -1;
    private b e;
    private int f;
    private double g = 30.0d;
    private double h = 10.0d;
    private double i = 5.0d;

    public c(Context context, b bVar) {
        super(new a(context));
        this.e = bVar;
        this.f250a.setColor(context.getResources().getColor(R.color.directions_line));
        this.f250a.setStrokeWidth(Float.parseFloat(context.getString(R.string.directions_line_width)));
        this.f250a.setStyle(Paint.Style.STROKE);
        this.f250a.setAntiAlias(true);
        this.g = Double.parseDouble(context.getString(R.string.directions_line_arrow_spacing));
        this.h = Double.parseDouble(context.getString(R.string.directions_line_arrow_head_length));
        this.i = Double.parseDouble(context.getString(R.string.directions_line_min_length));
    }

    private void a(double d2, double d3, double d4, double d5) {
        this.f++;
        float f2 = (float) (0.7853981633974483d + d4);
        float f3 = ((double) f2) > 3.141592653589793d ? (float) (((double) f2) - 6.283185307179586d) : f2;
        float f4 = (float) (d4 - 0.7853981633974483d);
        if (((double) f4) <= -3.141592653589793d) {
            f4 = (float) (((double) f4) + 6.283185307179586d);
        }
        float sin = (float) ((Math.sin((double) f3) * d5) + d3);
        float sin2 = (float) ((Math.sin((double) f4) * d5) + d3);
        this.b.moveTo((float) (d2 - (Math.cos((double) f3) * d5)), sin);
        this.b.lineTo((float) d2, (float) d3);
        this.b.lineTo((float) (d2 - (Math.cos((double) f4) * d5)), sin2);
    }

    private void b(b bVar) {
        double d2;
        h projection = bVar.getProjection();
        this.f = 0;
        Point point = new Point();
        Point point2 = new Point();
        projection.a(((d) this.c.get(0)).c(), point);
        int i2 = 1;
        while (true) {
            int i3 = i2;
            if (i3 < this.c.size()) {
                projection.a(((d) this.c.get(i3)).c(), point2);
                double d3 = (double) (point2.x - point.x);
                double d4 = (double) (point2.y - point.y);
                double sqrt = Math.sqrt((d3 * d3) + (d4 * d4));
                if (sqrt > this.i) {
                    double atan2 = Math.atan2(-d4, d3);
                    boolean z = this.f > 1500;
                    if (this.g == 0.0d) {
                        a((double) point2.x, (double) point2.y, atan2, this.h);
                        return;
                    } else if (this.g == 1.0d || z) {
                        a(((double) point.x) + ((sqrt / 2.0d) * Math.cos(atan2)), ((double) point.y) - ((sqrt / 2.0d) * Math.sin(atan2)), atan2, this.h);
                    } else {
                        double d5 = this.g;
                        while (true) {
                            if (d5 >= sqrt) {
                                d2 = d5;
                                break;
                            }
                            a(((double) point.x) + (Math.cos(atan2) * d5), ((double) point.y) - (Math.sin(atan2) * d5), atan2, this.h);
                            d2 = this.g + d5;
                            if (this.f > 1500) {
                                break;
                            }
                            d5 = d2;
                        }
                        if (d2 == this.g) {
                            a(((double) point.x) + ((sqrt / 2.0d) * Math.cos(atan2)), ((double) point.y) - ((sqrt / 2.0d) * Math.sin(atan2)), atan2, this.h);
                        }
                    }
                }
                point.set(point2.x, point2.y);
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, b bVar) {
    }

    public void a(List list) {
        this.c = list;
    }

    /* access modifiers changed from: protected */
    public void b(Canvas canvas, b bVar) {
        boolean z;
        int i2 = 0;
        if (this.c.size() >= 2) {
            h projection = bVar.getProjection();
            if (this.d != projection.c()) {
                this.d = projection.c();
                z = true;
            } else {
                z = false;
            }
            if (z) {
                this.b = new Path();
                Point point = new Point();
                Iterator it = this.c.iterator();
                while (true) {
                    int i3 = i2;
                    if (!it.hasNext()) {
                        break;
                    }
                    projection.a(((d) ((e) it.next())).c(), point);
                    if (i3 == 0) {
                        this.b.moveTo((float) point.x, (float) point.y);
                    } else {
                        this.b.lineTo((float) point.x, (float) point.y);
                    }
                    i2 = i3 + 1;
                }
                b(bVar);
            }
            canvas.drawPath(this.b, this.f250a);
        }
    }
}
