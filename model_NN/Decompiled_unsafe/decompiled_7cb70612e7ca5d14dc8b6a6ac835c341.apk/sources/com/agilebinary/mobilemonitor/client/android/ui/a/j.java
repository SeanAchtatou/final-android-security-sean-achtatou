package com.agilebinary.mobilemonitor.client.android.ui.a;

import android.os.AsyncTask;
import android.os.PowerManager;
import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.b.g;
import com.agilebinary.mobilemonitor.client.android.b.i;
import com.agilebinary.mobilemonitor.client.android.b.m;
import com.agilebinary.mobilemonitor.client.android.b.o;
import com.agilebinary.mobilemonitor.client.android.b.s;
import com.agilebinary.mobilemonitor.client.android.c.c;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.mobilemonitor.client.android.e;
import com.agilebinary.mobilemonitor.client.android.ui.bq;
import java.util.HashMap;
import java.util.Map;

public class j extends AsyncTask implements i, m {

    /* renamed from: a  reason: collision with root package name */
    static final String f213a = f.a("DownloadEventsTask");
    private static Map b = new HashMap();
    private byte c;
    private g d;
    private c e = this.f.g.e();
    private bq f;
    private Class g;
    private com.agilebinary.a.a.b.b.a.g h;
    private boolean i;

    public j(bq bqVar, g gVar, byte b2, Class cls) {
        b.b(f213a, "<CONSTRUCTOR>");
        this.c = b2;
        this.f = bqVar;
        this.d = gVar;
        this.g = cls;
        a(cls, this);
    }

    public static synchronized j a(Class cls) {
        j jVar;
        synchronized (j.class) {
            jVar = (j) b.get(cls);
        }
        return jVar;
    }

    private static synchronized void a(Class cls, j jVar) {
        synchronized (j.class) {
            b.put(cls, jVar);
        }
    }

    private static synchronized void b(Class cls) {
        synchronized (j.class) {
            b.remove(cls);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public i doInBackground(h... hVarArr) {
        i iVar;
        b.b(f213a, "DownloadTask.doInBackground...");
        PowerManager.WakeLock newWakeLock = ((PowerManager) this.f.getSystemService("power")).newWakeLock(6, f213a);
        newWakeLock.acquire();
        try {
            e eVar = new e(this.c, hVarArr[0].a(), hVarArr[0].b());
            if (isCancelled()) {
                b(this.g);
                this.f.g.f();
                try {
                    newWakeLock.release();
                    return null;
                } catch (Exception e2) {
                    return null;
                }
            } else {
                o a2 = this.d.a(this.f.g.a(), eVar, this.e, this, this);
                if (a2 != null && a2.d()) {
                    throw new s(a2.c());
                } else if (isCancelled()) {
                    b(this.g);
                    this.f.g.f();
                    try {
                        newWakeLock.release();
                        return null;
                    } catch (Exception e3) {
                        return null;
                    }
                } else {
                    if (a2 != null) {
                        if (!a2.g()) {
                            iVar = new i(true);
                            b(this.g);
                            this.f.g.f();
                            newWakeLock.release();
                            b.a(f213a, "...DownloadTask.doInBackground: ", "" + iVar);
                            return iVar;
                        }
                    }
                    iVar = null;
                    b(this.g);
                    this.f.g.f();
                    try {
                        newWakeLock.release();
                    } catch (Exception e4) {
                    }
                    b.a(f213a, "...DownloadTask.doInBackground: ", "" + iVar);
                    return iVar;
                }
            }
        } catch (s e5) {
            iVar = new i(e5);
            b(this.g);
            this.f.g.f();
            try {
                newWakeLock.release();
            } catch (Exception e6) {
            }
        } catch (Throwable th) {
            b(this.g);
            this.f.g.f();
            try {
                newWakeLock.release();
            } catch (Exception e7) {
            }
            throw th;
        }
    }

    public void a(com.agilebinary.a.a.b.b.a.g gVar) {
        this.h = gVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(i iVar) {
        b.b(f213a, "onPostExecute...");
        if (!isCancelled()) {
            bq.a(this.f, this.c, iVar);
        }
        b.b(f213a, "...onPostExecute.");
    }

    public void a(boolean z) {
        b.b(f213a, "doCancel...");
        try {
            this.i = z;
            cancel(false);
            if (this.h != null) {
                try {
                    this.h.i();
                } catch (Exception e2) {
                }
            }
            this.e.a(true);
            this.e.c();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        b.b(f213a, "...doCancel");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onProgressUpdate(Void... voidArr) {
        this.f.a_();
    }

    public void a_() {
        publishProgress(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        b.b(f213a, "onCancelled...");
        try {
            if (!this.i) {
                bq.a(this.f, this.c, (i) null);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        b.b(f213a, "...onCancelled");
    }
}
