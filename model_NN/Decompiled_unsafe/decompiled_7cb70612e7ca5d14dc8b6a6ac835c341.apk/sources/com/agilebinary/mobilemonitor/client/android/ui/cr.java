package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.agilebinary.phonebeagle.client.R;

public class cr extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    protected ImageView f298a = ((ImageView) findViewById(R.id.main_row_icon));
    protected TextView b = ((TextView) findViewById(R.id.main_row_label));
    protected TextView c = ((TextView) findViewById(R.id.main_row_cnt));
    protected ImageView d = ((ImageView) findViewById(R.id.main_row_star));
    final /* synthetic */ MainActivity e;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.agilebinary.mobilemonitor.client.android.ui.cr, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public cr(MainActivity mainActivity, Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.e = mainActivity;
        ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.main_row, (ViewGroup) this, true);
    }

    public void setMenuEntry(cs csVar) {
        int i = 4;
        boolean z = false;
        this.f298a.setImageResource(csVar.f299a);
        this.b.setText(csVar.b);
        this.d.setVisibility(csVar.c < 0 ? 4 : 0);
        if (this.e.g.d()) {
            this.c.setVisibility(8);
        } else {
            TextView textView = this.c;
            if (csVar.c >= 0 && this.e.g.a(csVar.c)) {
                i = 0;
            }
            textView.setVisibility(i);
        }
        this.c.setText(String.valueOf(this.e.g.b(csVar.c)));
        ImageView imageView = this.d;
        if (csVar.c >= 0) {
            z = this.e.g.a(csVar.c);
        }
        imageView.setEnabled(z);
        if (!this.e.g.c(csVar.c) || csVar.c < 0) {
            this.d.clearAnimation();
        } else {
            this.d.startAnimation(this.e.j);
        }
    }
}
