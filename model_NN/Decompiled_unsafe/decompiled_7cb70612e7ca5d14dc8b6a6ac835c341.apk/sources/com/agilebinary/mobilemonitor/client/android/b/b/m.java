package com.agilebinary.mobilemonitor.client.android.b.b;

import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.b.j;
import com.agilebinary.mobilemonitor.client.android.b.o;
import com.agilebinary.mobilemonitor.client.android.b.s;
import com.agilebinary.mobilemonitor.client.android.b.x;
import com.agilebinary.mobilemonitor.client.android.d.f;

public class m extends a implements j {
    private static final String b = f.a("ProductionEventNewestService");

    public long a(com.agilebinary.mobilemonitor.client.android.f fVar, byte b2, com.agilebinary.mobilemonitor.client.android.b.m mVar) {
        b.a(b, "...getNewestEventId,", "" + ((int) b2));
        o oVar = null;
        try {
            String format = String.format(x.a().d() + "ServiceEventRetrieveMaxId?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s&Type=%4$s", "1", a(fVar.c()), a(fVar.d()), Byte.valueOf(b2));
            b.a(b, "URL: ", format);
            oVar = this.f149a.a(format, x.a().n(), mVar);
            if (oVar.b()) {
                long parseLong = Long.parseLong(b(oVar).trim());
                a(oVar);
                b.a(b, "...getNewestEventId ", "" + parseLong);
                return parseLong;
            }
            throw new s(oVar.c());
        } catch (Exception e) {
            throw new s(e);
        } catch (Throwable th) {
            a(oVar);
            throw th;
        }
    }
}
