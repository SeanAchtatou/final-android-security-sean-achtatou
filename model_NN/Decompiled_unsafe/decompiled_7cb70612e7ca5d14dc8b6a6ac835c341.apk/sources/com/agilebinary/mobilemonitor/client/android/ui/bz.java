package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import com.agilebinary.mobilemonitor.a.a.a.b;

public class bz extends CursorAdapter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bq f281a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bz(bq bqVar, Context context, Cursor cursor, cl clVar) {
        super(context, cursor);
        this.f281a = bqVar;
        bqVar.x = clVar;
    }

    public void a() {
        b.b("MyCursorAdapter", "refresh..");
        onContentChanged();
    }

    public void bindView(View view, Context context, Cursor cursor) {
        ((ck) view).a(cursor);
    }

    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return this.f281a.x.a(context);
    }
}
