package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.a.b.b;
import com.agilebinary.mobilemonitor.client.a.b.h;
import com.agilebinary.mobilemonitor.client.android.d.g;
import com.agilebinary.phonebeagle.client.R;

public class EventDetailsActivity_CALL extends aw {
    private TextView n;
    private TextView o;
    private TextView p;
    private TextView q;
    private TextView r;
    private TextView s;
    private TextView t;
    private TextView u;
    private Button v;
    /* access modifiers changed from: private */
    public String w;
    /* access modifiers changed from: private */
    public String x;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public void a(ViewGroup viewGroup) {
        ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.eventdetails_call, viewGroup, true);
        this.n = (TextView) findViewById(R.id.eventdetails_call_direction);
        this.u = (TextView) findViewById(R.id.eventdetails_call_fromto);
        this.r = (TextView) findViewById(R.id.eventdetails_call_remoteparty);
        this.t = (TextView) findViewById(R.id.eventdetails_call_duration);
        this.o = (TextView) findViewById(R.id.eventdetails_call_time_initiated);
        this.p = (TextView) findViewById(R.id.eventdetails_call_time_connected);
        this.q = (TextView) findViewById(R.id.eventdetails_call_time_terminated);
        this.s = (TextView) findViewById(R.id.eventdetails_sms_speed);
        this.v = (Button) findViewById(R.id.eventdetails_call_addblock_button);
        this.v.setOnClickListener(new au(this));
    }

    /* access modifiers changed from: protected */
    public void a(b bVar) {
        int i;
        String string;
        super.a(bVar);
        h hVar = (h) bVar;
        this.w = hVar.n();
        this.x = hVar.o();
        String a2 = com.agilebinary.mobilemonitor.client.android.d.b.a(hVar.n(), hVar.o());
        hVar.o();
        String str = "";
        long a3 = hVar.a();
        long b = hVar.b();
        long c = hVar.c();
        int c2 = (int) ((hVar.c() - hVar.b()) / 1000);
        boolean z = false;
        switch (hVar.m()) {
            case -1:
                i = 0;
                string = getString(R.string.label_event_call_unknown);
                str = getString(R.string.label_event_to);
                break;
            case 0:
            default:
                int i2 = c2;
                string = "";
                i = i2;
                break;
            case 1:
                String string2 = getString(R.string.label_event_call_incoming);
                str = getString(R.string.label_event_from);
                z = true;
                int i3 = c2;
                string = string2;
                i = i3;
                break;
            case 2:
                str = getString(R.string.label_event_to);
                i = c2;
                string = c2 > 0 ? getString(R.string.label_event_call_outgoing) : getString(R.string.label_event_call_outgoing_missed);
                z = true;
                break;
            case 3:
                i = 0;
                string = getString(R.string.label_event_call_incoming_missed);
                str = getString(R.string.label_event_from);
                break;
            case 4:
                i = 0;
                string = getString(R.string.label_event_call_outgoing_blocked);
                str = getString(R.string.label_event_to);
                break;
            case 5:
                i = 0;
                string = getString(R.string.label_event_call_incoming_blocked);
                str = getString(R.string.label_event_to);
                break;
        }
        this.n.setText(string);
        this.u.setText(str);
        this.r.setText(a2);
        this.t.setText(i == 0 ? "" : getString(R.string.label_event_duration_fmt, new Object[]{Integer.valueOf(i / 60), Integer.valueOf(i % 60)}));
        this.o.setText(g.a(this).c(a3));
        this.p.setText(g.a(this).c(b));
        this.q.setText(g.a(this).c(c));
        this.s.setText(com.agilebinary.mobilemonitor.client.android.d.b.a(this, hVar));
        if (!z || !com.agilebinary.mobilemonitor.client.android.d.b.a(hVar)) {
            this.s.setTextColor(getResources().getColor(R.color.text));
        } else {
            this.s.setTextColor(getResources().getColor(R.color.warning));
        }
    }
}
