package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.a;
import com.agilebinary.mobilemonitor.client.a.b;
import com.agilebinary.mobilemonitor.client.a.d;
import com.agilebinary.mobilemonitor.client.android.b.c.f;
import org.osmdroid.util.GeoPoint;

public abstract class c extends e implements d {

    /* renamed from: a  reason: collision with root package name */
    protected String f127a;
    protected f b;

    public c(String str, long j, long j2, a aVar, b bVar) {
        super(str, j, j2, aVar, bVar);
        this.f127a = aVar.g();
    }

    public CharSequence a(Context context) {
        return "";
    }

    public double b() {
        return i().doubleValue();
    }

    public GeoPoint c() {
        if (this.b == null) {
            return null;
        }
        return new GeoPoint(this.b.b(), this.b.c());
    }

    public boolean c_() {
        return this.b != null;
    }

    public Double i() {
        if (this.b == null) {
            return null;
        }
        return Double.valueOf(this.b.d());
    }

    public f j() {
        return this.b;
    }
}
