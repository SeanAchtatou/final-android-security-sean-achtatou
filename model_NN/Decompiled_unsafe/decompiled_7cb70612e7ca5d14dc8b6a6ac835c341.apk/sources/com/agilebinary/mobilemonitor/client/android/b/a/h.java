package com.agilebinary.mobilemonitor.client.android.b.a;

import com.agilebinary.mobilemonitor.client.android.b.g;

public class h extends a implements g {
    public static long a(long j) {
        return (f142a + 86400000) - j;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00b6, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00b7, code lost:
        r9 = r1;
        r1 = null;
        r0 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c0, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00c1, code lost:
        r9 = r1;
        r1 = null;
        r0 = r9;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00b6 A[ExcHandler: all (r1v2 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:1:0x0001] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.agilebinary.mobilemonitor.client.android.b.o a(com.agilebinary.mobilemonitor.client.android.f r11, com.agilebinary.mobilemonitor.client.android.e r12, com.agilebinary.mobilemonitor.client.android.b.h r13, com.agilebinary.mobilemonitor.client.android.b.i r14, com.agilebinary.mobilemonitor.client.android.b.m r15) {
        /*
            r10 = this;
            r0 = 0
            long r1 = r12.b     // Catch:{ Exception -> 0x00c0, all -> 0x00b6 }
            long r3 = com.agilebinary.mobilemonitor.client.android.b.a.h.f142a     // Catch:{ Exception -> 0x00c0, all -> 0x00b6 }
            long r1 = r1 - r3
            r3 = 86400000(0x5265c00, double:4.2687272E-316)
            long r1 = r1 / r3
            double r1 = (double) r1     // Catch:{ Exception -> 0x00c0, all -> 0x00b6 }
            double r1 = java.lang.Math.floor(r1)     // Catch:{ Exception -> 0x00c0, all -> 0x00b6 }
            int r1 = (int) r1     // Catch:{ Exception -> 0x00c0, all -> 0x00b6 }
            int r1 = r1 * -1
            r2 = 2
            if (r1 > r2) goto L_0x00b2
            if (r1 < 0) goto L_0x00b2
            javax.xml.parsers.SAXParserFactory r2 = javax.xml.parsers.SAXParserFactory.newInstance()     // Catch:{ Exception -> 0x00c0, all -> 0x00b6 }
            javax.xml.parsers.SAXParser r2 = r2.newSAXParser()     // Catch:{ Exception -> 0x00c0, all -> 0x00b6 }
            org.xml.sax.XMLReader r2 = r2.getXMLReader()     // Catch:{ Exception -> 0x00c0, all -> 0x00b6 }
            com.agilebinary.mobilemonitor.client.android.b.a.i r3 = new com.agilebinary.mobilemonitor.client.android.b.a.i     // Catch:{ Exception -> 0x00c0, all -> 0x00b6 }
            byte r4 = r12.f184a     // Catch:{ Exception -> 0x00c0, all -> 0x00b6 }
            r3.<init>(r10, r13, r14, r4)     // Catch:{ Exception -> 0x00c0, all -> 0x00b6 }
            r2.setContentHandler(r3)     // Catch:{ Exception -> 0x00c0, all -> 0x00b6 }
            byte r3 = r12.f184a     // Catch:{ Exception -> 0x00c5, all -> 0x00b6 }
            r4 = -1
            long r5 = com.agilebinary.mobilemonitor.client.android.b.a.h.f142a     // Catch:{ Exception -> 0x00c5, all -> 0x00b6 }
            r7 = 172800000(0xa4cb800, double:8.53745436E-316)
            long r5 = r5 - r7
            r13.a(r3, r4, r5)     // Catch:{ Exception -> 0x00c5, all -> 0x00b6 }
            com.agilebinary.mobilemonitor.client.android.b.t r3 = com.agilebinary.mobilemonitor.client.android.b.t.a()     // Catch:{ Exception -> 0x00c5, all -> 0x00b6 }
            r10.b = r3     // Catch:{ Exception -> 0x00c5, all -> 0x00b6 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00c5, all -> 0x00b6 }
            r3.<init>()     // Catch:{ Exception -> 0x00c5, all -> 0x00b6 }
            java.lang.String r4 = a()     // Catch:{ Exception -> 0x00c5, all -> 0x00b6 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00c5, all -> 0x00b6 }
            byte r4 = r12.f184a     // Catch:{ Exception -> 0x00c5, all -> 0x00b6 }
            java.lang.String r4 = r10.a(r4)     // Catch:{ Exception -> 0x00c5, all -> 0x00b6 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00c5, all -> 0x00b6 }
            java.lang.String r4 = "_"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00c5, all -> 0x00b6 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x00c5, all -> 0x00b6 }
            java.lang.String r3 = ".xml"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x00c5, all -> 0x00b6 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00c5, all -> 0x00b6 }
            com.agilebinary.mobilemonitor.client.android.b.t r3 = r10.b     // Catch:{ Exception -> 0x00c5, all -> 0x00b6 }
            com.agilebinary.mobilemonitor.client.android.b.x r4 = com.agilebinary.mobilemonitor.client.android.b.x.a()     // Catch:{ Exception -> 0x00c5, all -> 0x00b6 }
            long r4 = r4.n()     // Catch:{ Exception -> 0x00c5, all -> 0x00b6 }
            com.agilebinary.mobilemonitor.client.android.b.o r0 = r3.a(r1, r4, r15)     // Catch:{ Exception -> 0x00c5, all -> 0x00b6 }
            boolean r1 = r0.b()     // Catch:{ Exception -> 0x0088, all -> 0x00bb }
            if (r1 != 0) goto L_0x00a2
            com.agilebinary.mobilemonitor.client.android.b.s r1 = new com.agilebinary.mobilemonitor.client.android.b.s     // Catch:{ Exception -> 0x0088, all -> 0x00bb }
            int r2 = r0.c()     // Catch:{ Exception -> 0x0088, all -> 0x00bb }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0088, all -> 0x00bb }
            throw r1     // Catch:{ Exception -> 0x0088, all -> 0x00bb }
        L_0x0088:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
        L_0x008c:
            r2 = 1
            r13.a(r2)     // Catch:{ Exception -> 0x0096 }
            com.agilebinary.mobilemonitor.client.android.b.s r2 = new com.agilebinary.mobilemonitor.client.android.b.s     // Catch:{ Exception -> 0x0096 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0096 }
            throw r2     // Catch:{ Exception -> 0x0096 }
        L_0x0096:
            r0 = move-exception
        L_0x0097:
            com.agilebinary.mobilemonitor.client.android.b.s r2 = new com.agilebinary.mobilemonitor.client.android.b.s     // Catch:{ all -> 0x009d }
            r2.<init>(r0)     // Catch:{ all -> 0x009d }
            throw r2     // Catch:{ all -> 0x009d }
        L_0x009d:
            r0 = move-exception
        L_0x009e:
            r10.a(r1)
            throw r0
        L_0x00a2:
            org.xml.sax.InputSource r1 = new org.xml.sax.InputSource     // Catch:{ Exception -> 0x0088, all -> 0x00bb }
            java.io.InputStream r3 = r0.e()     // Catch:{ Exception -> 0x0088, all -> 0x00bb }
            r1.<init>(r3)     // Catch:{ Exception -> 0x0088, all -> 0x00bb }
            r2.parse(r1)     // Catch:{ Exception -> 0x0088, all -> 0x00bb }
            r1 = 0
            r13.a(r1)     // Catch:{ Exception -> 0x0088, all -> 0x00bb }
        L_0x00b2:
            r10.a(r0)
            return r0
        L_0x00b6:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x009e
        L_0x00bb:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x009e
        L_0x00c0:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x0097
        L_0x00c5:
            r1 = move-exception
            r9 = r1
            r1 = r0
            r0 = r9
            goto L_0x008c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.b.a.h.a(com.agilebinary.mobilemonitor.client.android.f, com.agilebinary.mobilemonitor.client.android.e, com.agilebinary.mobilemonitor.client.android.b.h, com.agilebinary.mobilemonitor.client.android.b.i, com.agilebinary.mobilemonitor.client.android.b.m):com.agilebinary.mobilemonitor.client.android.b.o");
    }

    /* access modifiers changed from: protected */
    public String a(byte b) {
        switch (b) {
            case 1:
                return "location";
            case 2:
                return "call";
            case 3:
                return "sms";
            case 4:
                return "mms";
            case 5:
            case 7:
            default:
                return "";
            case 6:
                return "web";
            case 8:
                return "sys";
            case 9:
                return "app";
            case 10:
                return "fbk";
            case 11:
                return "wpp";
            case 12:
                return "lin";
        }
    }
}
