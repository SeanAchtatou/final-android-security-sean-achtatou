package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Dialog;
import android.view.View;

final class da implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Dialog f308a;
    final /* synthetic */ MapActivity_OSM b;

    da(MapActivity_OSM mapActivity_OSM, Dialog dialog) {
        this.b = mapActivity_OSM;
        this.f308a = dialog;
    }

    public void onClick(View view) {
        this.b.r.b(!this.b.r.a());
        this.b.m.invalidate();
        this.b.u.b("MAP_LAYER_ACCURACY__BOOL", this.b.q.a());
        this.f308a.dismiss();
    }
}
