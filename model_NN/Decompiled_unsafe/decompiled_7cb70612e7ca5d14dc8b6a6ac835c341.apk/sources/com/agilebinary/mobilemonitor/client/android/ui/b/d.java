package com.agilebinary.mobilemonitor.client.android.ui.b;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.agilebinary.mobilemonitor.client.a.b.e;
import com.agilebinary.phonebeagle.client.R;
import java.util.List;

public class d extends Dialog implements DialogInterface.OnCancelListener, AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private e f251a;
    private ListView b = ((ListView) findViewById(R.id.map_marker_select_dialog_list));
    private g c = new g(getContext());

    public d(Context context, e eVar) {
        super(context);
        this.f251a = eVar;
        setTitle((int) R.string.label_map_markerselectdialog_title);
        setOnCancelListener(this);
        setContentView((int) R.layout.map_marker_select_dialog);
        this.b.setAdapter((ListAdapter) this.c);
        this.b.setOnItemClickListener(this);
    }

    public void a(List list) {
        this.c.a(list);
    }

    public void onCancel(DialogInterface dialogInterface) {
        dismiss();
        this.f251a.a(null);
    }

    public void onItemClick(AdapterView adapterView, View view, int i, long j) {
        dismiss();
        this.f251a.a((e) adapterView.getItemAtPosition(i));
    }
}
