package com.agilebinary.mobilemonitor.client.android.ui;

import com.jasonkostempski.android.calendar.CalendarDialog;
import java.util.Calendar;

final class bx implements CalendarDialog.OnDateSelectedListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ bq f279a;

    bx(bq bqVar) {
        this.f279a = bqVar;
    }

    public void onDateSelected(Calendar calendar) {
        this.f279a.b(calendar);
        CalendarDialog unused = this.f279a.i = (CalendarDialog) null;
    }
}
