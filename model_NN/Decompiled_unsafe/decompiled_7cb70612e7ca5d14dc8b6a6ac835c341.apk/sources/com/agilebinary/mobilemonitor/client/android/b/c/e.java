package com.agilebinary.mobilemonitor.client.android.b.c;

import java.io.Serializable;

public final class e extends d implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private long f161a;
    private int b;
    private f c;

    public String a() {
        return "{\"create_date\":\"" + this.f161a + "\", \"geocoder_source_id\":\"" + this.b + "\", \"" + "mobile_country_code" + "\":\"" + super.b() + "\", \"" + "mobile_network_code" + "\":\"" + super.c() + "\", \"" + "cell_id" + "\":\"" + super.d() + "\", \"" + "location_area_code" + "\":\"" + super.e() + "\", \"" + "latitude" + "\":\"" + this.c.f162a + "\", \"" + "longitude" + "\":\"" + this.c.b + "\", \"" + "accuracy" + "\":\"" + this.c.c + "\", \"" + "country" + "\":\"" + this.c.d + "\", \"" + "country_code" + "\":\"" + this.c.e + "\", \"" + "region" + "\":\"" + this.c.f + "\", \"" + "city" + "\":\"" + this.c.g + "\", \"" + "street" + "\":\"" + this.c.h + "\"}";
    }

    public void a(long j) {
        this.f161a = j;
    }

    public void a(f fVar) {
        this.c = fVar;
    }

    public void e(int i) {
        this.b = i;
    }

    public f f() {
        return this.c;
    }

    public String toString() {
        return "GeocodeGsmCellLocation: " + a();
    }
}
