package com.agilebinary.mobilemonitor.client.android.ui.b;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.agilebinary.mobilemonitor.client.a.b.e;
import java.util.List;

public class g extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    private List f253a;
    private Context b;

    public g(Context context) {
        this.b = context;
    }

    public void a(List list) {
        this.f253a = list;
        notifyDataSetChanged();
    }

    public int getCount() {
        if (this.f253a == null) {
            return 0;
        }
        return this.f253a.size();
    }

    public Object getItem(int i) {
        return this.f253a.get(i);
    }

    public long getItemId(int i) {
        return ((e) this.f253a.get(i)).h();
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        e eVar = (e) this.f253a.get(i);
        f fVar = view == null ? new f(this.b, null) : (f) view;
        fVar.setEvent(eVar);
        return fVar;
    }
}
