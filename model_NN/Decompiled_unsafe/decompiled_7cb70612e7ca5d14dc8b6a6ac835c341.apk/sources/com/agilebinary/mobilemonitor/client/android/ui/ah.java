package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.util.Linkify;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.MyApplication;
import com.agilebinary.mobilemonitor.client.android.b.x;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.phonebeagle.client.R;
import java.io.Serializable;
import java.util.Stack;

public abstract class ah extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private static final String f229a = f.a("BaseActivity");
    private static Stack b = new Stack();
    protected ProgressDialog c;
    protected boolean d;
    protected AlertDialog e;
    protected CharSequence f;
    public MyApplication g;
    private int h;

    public static void k() {
        b.b(f229a, "clear Stack...");
        while (b.size() > 0) {
            b.a(f229a, "finnishing ", "" + b.peek());
            ((ah) b.pop()).finish();
        }
        b.b(f229a, "...clear Stack");
    }

    /* access modifiers changed from: protected */
    public abstract int a();

    /* access modifiers changed from: protected */
    public void a(int i) {
        this.f = getString(i);
        this.e.setMessage(this.f);
        this.e.show();
    }

    /* access modifiers changed from: protected */
    public void a(CharSequence charSequence) {
        this.f = charSequence;
        this.e.setMessage(this.f);
        this.e.show();
        try {
            Linkify.addLinks((TextView) this.e.findViewById(16908299), 1);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void b(int i) {
        this.h = i;
        this.c.setMessage(getString(i));
        this.c.show();
    }

    /* access modifiers changed from: protected */
    public void b(boolean z) {
        if (!this.c.isShowing()) {
            return;
        }
        if (z) {
            this.c.cancel();
        } else {
            this.c.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public void d() {
        this.e.dismiss();
    }

    /* access modifiers changed from: protected */
    public void e() {
    }

    /* access modifiers changed from: protected */
    public void f() {
    }

    public void finish() {
        b(true);
        if (this.e.isShowing()) {
            this.e.dismiss();
        }
        super.finish();
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        return requestWindowFeature(7);
    }

    /* access modifiers changed from: protected */
    public void h() {
        getWindow().setFeatureInt(7, R.layout.titlebar);
        TextView textView = (TextView) findViewById(R.id.titlebar_text);
        if (textView != null) {
            textView.setText(i());
        }
    }

    /* access modifiers changed from: protected */
    public CharSequence i() {
        return getString(R.string.app_title);
    }

    /* access modifiers changed from: protected */
    public void j() {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        b.push(this);
        super.onCreate(bundle);
        this.g = (MyApplication) getApplication();
        this.d = g();
        setContentView(a());
        if (this.d) {
            h();
        }
        this.c = new ProgressDialog(this);
        this.c.setIndeterminate(true);
        this.c.setCancelable(true);
        this.c.setTitle((int) R.string.label_dialog_title);
        this.c.setOnCancelListener(new ai(this));
        this.c.setOnDismissListener(new aj(this));
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true).setTitle((int) R.string.label_dialog_title).setNeutralButton((int) R.string.label_ok, new ak(this));
        this.e = builder.create();
        if (bundle != null) {
            int i = bundle.getInt("EXTRA_PROGRESS_MSG");
            if (i != 0) {
                b(i);
                return;
            }
            CharSequence charSequence = (CharSequence) bundle.getSerializable("EXTRA_ERROR_MSG");
            if (charSequence != null) {
                a(charSequence);
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.base, menu);
        menu.findItem(R.id.menu_base_help).setVisible(x.a().h().trim().length() > 0);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        b.remove(this);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_base_help /*2131296478*/:
                j();
                try {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse(x.a().h())));
                    return true;
                } catch (ActivityNotFoundException e2) {
                    return true;
                }
            case R.id.menu_base_about /*2131296479*/:
                j();
                AboutActivity.a(this);
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putInt("EXTRA_PROGRESS_MSG", this.c.isShowing() ? this.h : 0);
        bundle.putSerializable("EXTRA_ERROR_MSG", (Serializable) (this.e.isShowing() ? this.f : null));
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        b(false);
        if (this.e.isShowing()) {
            this.e.dismiss();
        }
        super.onStop();
    }
}
