package com.agilebinary.mobilemonitor.client.android.b.b;

import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.b.f;
import com.agilebinary.mobilemonitor.client.android.b.m;
import com.agilebinary.mobilemonitor.client.android.b.o;
import com.agilebinary.mobilemonitor.client.android.b.s;
import com.agilebinary.mobilemonitor.client.android.b.x;

public class k extends a implements f {
    private static final String b = com.agilebinary.mobilemonitor.client.android.d.f.a("ProductionEventCountService");

    public int a(com.agilebinary.mobilemonitor.client.android.f fVar, byte b2, long j, m mVar) {
        b.a(b, "getEventCountFromId ", "" + ((int) b2), " / ", "" + j);
        o oVar = null;
        try {
            String format = String.format(x.a().d() + "ServiceEventRetrieveCountFromId?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s&Type=%4$s&FromId=%5$s", "1", a(fVar.c()), a(fVar.d()), Byte.valueOf(b2), Long.valueOf(j));
            b.a(b, "URL: ", format);
            oVar = this.f149a.a(format, x.a().n(), mVar);
            if (oVar.b()) {
                int parseInt = Integer.parseInt(b(oVar).trim());
                a(oVar);
                b.b(b, "...getEventCountFromId");
                return parseInt;
            }
            throw new s(oVar.c());
        } catch (Exception e) {
            throw new s(e);
        } catch (Throwable th) {
            a(oVar);
            throw th;
        }
    }
}
