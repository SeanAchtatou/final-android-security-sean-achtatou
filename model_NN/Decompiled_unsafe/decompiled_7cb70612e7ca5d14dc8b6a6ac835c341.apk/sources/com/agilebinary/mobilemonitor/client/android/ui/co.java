package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.Intent;
import android.content.res.Configuration;
import android.view.View;
import android.widget.AdapterView;
import java.util.Locale;

final class co implements AdapterView.OnItemSelectedListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ LoginActivity f295a;

    co(LoginActivity loginActivity) {
        this.f295a = loginActivity;
    }

    public void onItemSelected(AdapterView adapterView, View view, int i, long j) {
        Locale locale = LoginActivity.n[i].f297a;
        if (!Locale.getDefault().getLanguage().equals(locale.getLanguage())) {
            Configuration configuration = this.f295a.getBaseContext().getResources().getConfiguration();
            configuration.locale = locale;
            Locale.setDefault(locale);
            this.f295a.getBaseContext().getResources().updateConfiguration(configuration, this.f295a.getBaseContext().getResources().getDisplayMetrics());
            Intent intent = new Intent(this.f295a, LoginActivity.class);
            intent.addFlags(67108864);
            this.f295a.finish();
            this.f295a.startActivity(intent);
        }
    }

    public void onNothingSelected(AdapterView adapterView) {
    }
}
