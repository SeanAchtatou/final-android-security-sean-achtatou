package com.agilebinary.mobilemonitor.client.android.ui.a;

import android.os.AsyncTask;
import android.os.PowerManager;
import com.agilebinary.mobilemonitor.client.android.b.b;
import com.agilebinary.mobilemonitor.client.android.b.o;
import com.agilebinary.mobilemonitor.client.android.b.s;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.mobilemonitor.client.android.ui.AppBlockActivity;
import java.util.ArrayList;
import java.util.List;

public class q extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    static final String f220a = f.a("SyncAppsTask");
    private b b;
    private AppBlockActivity c;
    private boolean d;

    public q(AppBlockActivity appBlockActivity, b bVar) {
        com.agilebinary.mobilemonitor.a.a.a.b.b(f220a, "<CONSTRUCTOR>");
        this.c = appBlockActivity;
        this.b = bVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public f doInBackground(List... listArr) {
        f fVar;
        com.agilebinary.mobilemonitor.a.a.a.b.b(f220a, "SyncAppsTask.doInBackground...");
        PowerManager.WakeLock newWakeLock = ((PowerManager) this.c.getSystemService("power")).newWakeLock(6, f220a);
        newWakeLock.acquire();
        try {
            if (isCancelled()) {
                try {
                    newWakeLock.release();
                } catch (Exception e) {
                }
                return null;
            }
            List list = listArr[0];
            if (list != null) {
                com.agilebinary.mobilemonitor.a.a.a.b.b(f220a, "uploading blacklist");
                o b2 = this.b.b(this.c.g.a(), list);
                if (b2 != null && b2.d()) {
                    throw new s(b2.c());
                }
            } else {
                com.agilebinary.mobilemonitor.a.a.a.b.b(f220a, "NOT uploading blacklist, no local changes! ");
            }
            ArrayList arrayList = new ArrayList();
            o a2 = this.b.a(this.c.g.a(), arrayList);
            if (a2 != null && a2.d()) {
                throw new s(a2.c());
            } else if (isCancelled()) {
                try {
                    newWakeLock.release();
                } catch (Exception e2) {
                }
                return null;
            } else {
                fVar = new f(arrayList);
                try {
                    newWakeLock.release();
                } catch (Exception e3) {
                }
                com.agilebinary.mobilemonitor.a.a.a.b.a(f220a, "...DownloadTask.doInBackground: ", "" + fVar);
                return fVar;
            }
        } catch (s e4) {
            fVar = new f(e4);
            try {
                newWakeLock.release();
            } catch (Exception e5) {
            }
        } catch (Throwable th) {
            try {
                newWakeLock.release();
            } catch (Exception e6) {
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(f fVar) {
        com.agilebinary.mobilemonitor.a.a.a.b.b(f220a, "onPostExecute...");
        if (!isCancelled()) {
            AppBlockActivity.a(this.c, fVar);
        }
        com.agilebinary.mobilemonitor.a.a.a.b.b(f220a, "...onPostExecute.");
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        com.agilebinary.mobilemonitor.a.a.a.b.b(f220a, "onCancelled...");
        try {
            if (!this.d) {
                AppBlockActivity.a(this.c, (f) null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        com.agilebinary.mobilemonitor.a.a.a.b.b(f220a, "...onCancelled");
    }
}
