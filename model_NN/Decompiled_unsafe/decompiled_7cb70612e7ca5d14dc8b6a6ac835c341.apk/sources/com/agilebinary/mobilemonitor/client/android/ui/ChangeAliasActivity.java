package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import com.agilebinary.mobilemonitor.client.android.b.c.a;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.mobilemonitor.client.android.ui.a.b;
import com.agilebinary.mobilemonitor.client.android.ui.a.o;
import com.agilebinary.phonebeagle.client.R;

public class ChangeAliasActivity extends al {

    /* renamed from: a  reason: collision with root package name */
    private static final String f194a = f.a("ChangeAliasActivity");
    /* access modifiers changed from: private */
    public EditText b;
    private Button i;
    private Button j;

    public static void a(Activity activity, a aVar, int i2) {
        Intent intent = new Intent(activity, ChangeAliasActivity.class);
        intent.putExtra("EXTRA_ACCOUNT", aVar);
        activity.startActivityForResult(intent, i2);
    }

    public static void a(Context context, o oVar) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra("EXTRA_SERVICE_RESULT", oVar);
        intent.setFlags(536870912);
        context.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        b((int) R.string.msg_progress_communicating);
        com.agilebinary.mobilemonitor.client.android.ui.a.a aVar = new com.agilebinary.mobilemonitor.client.android.ui.a.a(str, (a) getIntent().getSerializableExtra("EXTRA_ACCOUNT"));
        b.b = new b(this);
        b.b.execute(aVar);
    }

    /* access modifiers changed from: protected */
    public int a() {
        return R.layout.change_alias;
    }

    /* access modifiers changed from: protected */
    public void e() {
        if (b.b != null) {
            try {
                b.b.a();
            } catch (Exception e) {
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.b = (EditText) findViewById(R.id.changealias_alias);
        this.i = (Button) findViewById(R.id.changealias_cancel);
        this.j = (Button) findViewById(R.id.changealias_ok);
        this.j.setOnClickListener(new am(this));
        this.i.setOnClickListener(new an(this));
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        com.agilebinary.mobilemonitor.a.a.a.b.b(f194a, "onNewIntent...");
        o oVar = (o) intent.getSerializableExtra("EXTRA_SERVICE_RESULT");
        b(false);
        if (oVar != null) {
            if (oVar.b() != null) {
                if (oVar.b().c()) {
                    a((int) R.string.error_service_account_general);
                } else if (oVar.b().b()) {
                    a((int) R.string.error_service_account_general);
                } else {
                    a((int) R.string.error_service_account_general);
                }
            } else if (oVar.a()) {
                finish();
            }
        }
        com.agilebinary.mobilemonitor.a.a.a.b.b(f194a, "...onNewIntent");
    }
}
