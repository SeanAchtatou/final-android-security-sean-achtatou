package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.DialogInterface;
import android.content.Intent;

final class h implements DialogInterface.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ah f321a;
    final /* synthetic */ String b;
    final /* synthetic */ String c;

    h(ah ahVar, String str, String str2) {
        this.f321a = ahVar;
        this.b = str;
        this.c = str2;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        if (!this.f321a.g.d()) {
            Intent intent = new Intent(this.f321a, AddressbookActivity.class);
            intent.putExtra("EXTRA_ADD_NUMBER", this.b);
            intent.putExtra("EXTRA_ADD_NAME", this.c);
            intent.setFlags(536870912);
            this.f321a.startActivity(intent);
        }
    }
}
