package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.AlertDialog;
import android.widget.CompoundButton;
import com.agilebinary.phonebeagle.client.R;

final class o implements CompoundButton.OnCheckedChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AddressbookActivity f328a;
    final /* synthetic */ n b;

    o(n nVar, AddressbookActivity addressbookActivity) {
        this.b = nVar;
        this.f328a = addressbookActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.client.android.ui.AddressbookActivity.a(com.agilebinary.mobilemonitor.client.android.ui.AddressbookActivity, boolean):boolean
     arg types: [com.agilebinary.mobilemonitor.client.android.ui.AddressbookActivity, int]
     candidates:
      com.agilebinary.mobilemonitor.client.android.ui.AddressbookActivity.a(com.agilebinary.mobilemonitor.client.android.ui.AddressbookActivity, int):int
      com.agilebinary.mobilemonitor.client.android.ui.AddressbookActivity.a(com.agilebinary.mobilemonitor.client.android.ui.AddressbookActivity, com.agilebinary.mobilemonitor.client.android.ui.a.g):void
      com.agilebinary.mobilemonitor.client.android.ui.AddressbookActivity.a(com.agilebinary.mobilemonitor.client.android.ui.ah, java.lang.String):void
      com.agilebinary.mobilemonitor.client.android.ui.AddressbookActivity.a(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.client.android.ui.AddressbookActivity.a(com.agilebinary.mobilemonitor.client.android.ui.AddressbookActivity, boolean):boolean */
    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (this.b.f.f) {
            return;
        }
        if (this.b.g.g.d()) {
            if (!z) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this.b.g);
                builder.setMessage(this.b.g.getString(R.string.label_event_contact_blacklist_warning_demo, new Object[]{this.b.f.b})).setCancelable(true).setNeutralButton(this.b.g.getString(R.string.label_ok), new p(this));
                builder.create().show();
                return;
            }
            this.b.f.a(false);
            this.b.a(this.b.f);
        } else if (!z) {
            AlertDialog.Builder builder2 = new AlertDialog.Builder(this.b.g);
            builder2.setMessage(this.b.g.getString(R.string.label_event_contact_blacklist_warning, new Object[]{this.b.f.b})).setCancelable(true).setPositiveButton(this.b.g.getString(R.string.label_yes), new r(this)).setNegativeButton(this.b.g.getString(R.string.label_no), new q(this));
            builder2.create().show();
        } else {
            this.b.f.a(false);
            boolean unused = this.b.g.v = true;
            this.b.a(this.b.f);
            this.b.g.m();
        }
    }
}
