package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.a;
import com.agilebinary.mobilemonitor.client.a.b;
import com.agilebinary.phonebeagle.client.R;

public class j extends g {
    protected String A;
    protected String B;
    protected String C;
    protected String D;
    protected String E;

    /* renamed from: a  reason: collision with root package name */
    protected long f132a;
    protected int b;
    protected byte c;
    protected String i;
    protected String w;
    protected String x;
    protected String y;
    protected String z;

    public j(String str, long j, long j2, a aVar, b bVar) {
        super(str, j, j2, aVar, bVar);
        this.b = aVar.c();
        this.f132a = bVar.a(aVar.d());
        this.c = aVar.b();
        this.i = aVar.g();
        this.w = aVar.g();
        this.x = aVar.g();
        this.y = aVar.g();
        this.z = aVar.g();
        this.A = aVar.g();
        this.B = aVar.g();
        this.C = aVar.g();
        this.D = aVar.g();
        this.E = aVar.g();
    }

    public int a() {
        return this.b;
    }

    public String a(Context context) {
        String str = this.i;
        switch (n()) {
            case 1:
                return context.getString(R.string.label_line_msg_from_fmt, str);
            case 2:
                return context.getString(R.string.label_line_msg_to_fmt, str);
            default:
                return "";
        }
    }

    public String b() {
        return this.i;
    }

    public String b(Context context) {
        switch (a()) {
            case 1:
                Object[] objArr = new Object[2];
                objArr[0] = a(context);
                objArr[1] = this.x == null ? "" : this.x;
                return context.getString(R.string.label_event_line_msg, objArr);
            default:
                return "";
        }
    }

    public String c() {
        return this.w;
    }

    public String c(Context context) {
        return this.w;
    }

    public byte d() {
        return 17;
    }

    public String m() {
        return this.x;
    }

    public byte n() {
        return this.c;
    }
}
