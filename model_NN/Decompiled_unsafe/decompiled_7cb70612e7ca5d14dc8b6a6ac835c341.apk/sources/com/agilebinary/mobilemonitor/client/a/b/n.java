package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.a;
import com.agilebinary.mobilemonitor.client.a.b;
import com.agilebinary.phonebeagle.client.R;

public class n extends c {
    protected int c = -1;

    public n(String str, long j, long j2, a aVar, b bVar) {
        super(str, j, j2, aVar, bVar);
        this.c = aVar.c();
    }

    public byte d() {
        return 12;
    }

    public String d(Context context) {
        return context.getString(R.string.label_event_location_type_cell_iden);
    }

    public String e(Context context) {
        return this.f127a;
    }
}
