package com.agilebinary.mobilemonitor.client.android.ui;

import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.agilebinary.phonebeagle.client.R;

public class EventListActivity_APP extends bq {

    /* renamed from: a  reason: collision with root package name */
    protected RadioButton f197a;
    protected RadioButton b;
    protected RadioGroup i;

    /* access modifiers changed from: protected */
    public cl b() {
        return new bc(this);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.o.addView(getLayoutInflater().inflate((int) R.layout.eventlist_footer_app, (ViewGroup) null), 1);
        this.f197a = (RadioButton) findViewById(R.id.eventlist_app_filter_all);
        this.f197a.setSelected(true);
        this.b = (RadioButton) findViewById(R.id.eventlist_app_filter_install_only);
        this.i = (RadioGroup) findViewById(R.id.eventlist_app_filter);
        this.i.setOnCheckedChangeListener(new bb(this));
    }
}
