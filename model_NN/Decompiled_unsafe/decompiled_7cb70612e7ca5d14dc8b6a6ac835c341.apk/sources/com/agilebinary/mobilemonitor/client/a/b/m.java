package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.a;
import com.agilebinary.mobilemonitor.client.a.b;
import com.agilebinary.mobilemonitor.client.a.c;
import com.agilebinary.mobilemonitor.client.android.b.c.f;
import com.agilebinary.phonebeagle.client.R;

public class m extends c implements c {
    protected int c = -1;
    protected int i = -1;
    protected int j = -1;
    protected int k = -1;

    public m(String str, long j2, long j3, a aVar, b bVar) {
        super(str, j2, j3, aVar, bVar);
        this.c = aVar.c();
        this.i = aVar.c();
        this.j = aVar.c();
        this.k = aVar.c();
    }

    public void a(f fVar) {
        this.b = fVar;
    }

    public boolean a() {
        return this.b == null;
    }

    public byte d() {
        return 7;
    }

    public String d(Context context) {
        return context.getString(R.string.label_event_location_type_cell_gsm);
    }

    public String e(Context context) {
        return this.f127a;
    }

    public int k() {
        return this.c;
    }

    public int l() {
        return this.i;
    }

    public int m() {
        return this.j;
    }

    public int n() {
        return this.k;
    }
}
