package com.agilebinary.mobilemonitor.client.android.ui.b;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Picture;
import android.graphics.Rect;
import android.os.Build;
import org.osmdroid.c;
import org.osmdroid.d.a.f;
import org.osmdroid.d.b;
import org.osmdroid.d.h;
import org.osmdroid.e;
import org.osmdroid.util.GeoPoint;

public class i extends f {

    /* renamed from: a  reason: collision with root package name */
    float f255a = 10.0f;
    float b = 10.0f;
    float c = 2.0f;
    int d = 12;
    int e = 0;
    boolean f = false;
    boolean g = false;
    boolean h = true;
    boolean i = false;
    protected final Picture j = new Picture();
    public float k;
    public float l;
    public int m;
    public int n;
    private final Activity p;
    private int q = -1;
    private float r = 0.0f;
    private final c s;
    private Matrix t;
    private final Paint u;
    private final Paint v;
    private h w;
    private boolean x;

    public i(Activity activity, c cVar) {
        super(cVar);
        String str;
        this.s = cVar;
        this.p = activity;
        this.u = new Paint();
        this.u.setColor(-16777216);
        this.u.setAntiAlias(true);
        this.u.setStyle(Paint.Style.FILL);
        this.u.setAlpha(255);
        this.v = new Paint();
        this.v.setColor(-16777216);
        this.v.setAntiAlias(true);
        this.v.setStyle(Paint.Style.FILL);
        this.v.setAlpha(255);
        this.v.setTextSize((float) this.d);
        this.k = this.p.getResources().getDisplayMetrics().xdpi;
        this.l = this.p.getResources().getDisplayMetrics().ydpi;
        this.m = this.p.getResources().getDisplayMetrics().widthPixels;
        this.n = this.p.getResources().getDisplayMetrics().heightPixels;
        try {
            str = (String) Build.class.getField("MANUFACTURER").get(null);
        } catch (Exception e2) {
            str = null;
        }
        if (!"motorola".equals(str) || !"DROIDX".equals(Build.MODEL)) {
            if ("motorola".equals(str) && "Droid".equals(Build.MODEL)) {
                this.k = 264.0f;
                this.l = 264.0f;
            }
        } else if (activity.getWindowManager().getDefaultDisplay().getOrientation() > 0) {
            this.k = (float) (((double) this.m) / 3.75d);
            this.l = (float) (((double) this.n) / 2.1d);
        } else {
            this.k = (float) (((double) this.m) / 2.1d);
            this.l = (float) (((double) this.n) / 3.75d);
        }
    }

    private String a(int i2, boolean z, boolean z2) {
        if (this.f) {
            if (((double) i2) >= 8046.72d) {
                return this.s.a(e.format_distance_miles, Integer.valueOf((int) (((double) i2) / 1609.344d)));
            } else if (((double) i2) >= 321.8688d) {
                return this.s.a(e.format_distance_miles, Double.valueOf(((double) ((int) (((double) i2) / 160.9344d))) / 10.0d));
            } else {
                return this.s.a(e.format_distance_feet, Integer.valueOf((int) (((double) i2) * 3.2808399d)));
            }
        } else if (this.g) {
            if (((double) i2) >= 9260.0d) {
                return this.s.a(e.format_distance_nautical_miles, Integer.valueOf((int) (((double) i2) / 1852.0d)));
            } else if (((double) i2) >= 370.4d) {
                return this.s.a(e.format_distance_nautical_miles, Double.valueOf(((double) ((int) (((double) i2) / 185.2d))) / 10.0d));
            } else {
                return this.s.a(e.format_distance_feet, Integer.valueOf((int) (((double) i2) * 3.2808399d)));
            }
        } else if (i2 >= 5000) {
            return this.s.a(e.format_distance_kilometers, Integer.valueOf(i2 / 1000));
        } else if (i2 >= 200) {
            return this.s.a(e.format_distance_kilometers, Double.valueOf(((double) ((int) (((double) i2) / 100.0d))) / 10.0d));
        } else {
            return this.s.a(e.format_distance_meters, Integer.valueOf(i2));
        }
    }

    private void b(b bVar) {
        this.w = bVar.getProjection();
        if (this.w != null) {
            int a2 = this.w.a(((float) (this.m / 2)) - (this.k / 2.0f), (float) (this.n / 2)).a(this.w.a(((float) (this.m / 2)) + (this.k / 2.0f), (float) (this.n / 2)));
            int a3 = this.w.a((float) (this.m / 2), ((float) (this.n / 2)) - (this.l / 2.0f)).a(this.w.a((float) (this.m / 2), ((float) (this.n / 2)) + (this.l / 2.0f)));
            Canvas beginRecording = this.j.beginRecording((int) this.k, (int) this.l);
            if (this.h) {
                String a4 = a(a2, this.f, this.g);
                Rect rect = new Rect();
                this.v.getTextBounds(a4, 0, a4.length(), rect);
                int height = (int) (((double) rect.height()) / 5.0d);
                beginRecording.drawRect(0.0f, 0.0f, this.k, this.c, this.u);
                beginRecording.drawRect(this.k, 0.0f, this.k + this.c, ((float) rect.height()) + this.c + ((float) height), this.u);
                if (!this.i) {
                    beginRecording.drawRect(0.0f, 0.0f, this.c, ((float) rect.height()) + this.c + ((float) height), this.u);
                }
                beginRecording.drawText(a4, (this.k / 2.0f) - ((float) (rect.width() / 2)), ((float) rect.height()) + this.c + ((float) height), this.v);
            }
            if (this.i) {
                String a5 = a(a3, this.f, this.g);
                Rect rect2 = new Rect();
                this.v.getTextBounds(a5, 0, a5.length(), rect2);
                int height2 = (int) (((double) rect2.height()) / 5.0d);
                beginRecording.drawRect(0.0f, 0.0f, this.c, this.l, this.u);
                beginRecording.drawRect(0.0f, this.l, ((float) rect2.height()) + this.c + ((float) height2), this.l + this.c, this.u);
                if (!this.h) {
                    beginRecording.drawRect(0.0f, 0.0f, ((float) rect2.height()) + this.c + ((float) height2), this.c, this.u);
                }
                float height3 = ((float) rect2.height()) + this.c + ((float) height2);
                float width = (this.l / 2.0f) + ((float) (rect2.width() / 2));
                beginRecording.rotate(-90.0f, height3, width);
                beginRecording.drawText(a5, height3, width + ((float) height2), this.v);
            }
            this.j.endRecording();
        }
    }

    public void a(float f2, float f3) {
        this.f255a = f2;
        this.b = f3;
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, b bVar) {
    }

    public boolean a() {
        return !this.x && super.a();
    }

    public void b(Canvas canvas, b bVar) {
        h projection;
        try {
            if (!bVar.e()) {
                int zoomLevel = bVar.getZoomLevel();
                if (a() && zoomLevel >= this.e && (projection = bVar.getProjection()) != null) {
                    GeoPoint a2 = projection.a(this.m / 2, this.n / 2);
                    if (!(zoomLevel == this.q && ((int) (((double) a2.a()) / 1000000.0d)) == ((int) (((double) this.r) / 1000000.0d)))) {
                        this.q = zoomLevel;
                        this.r = (float) a2.a();
                        b(bVar);
                    }
                    this.t = canvas.getMatrix();
                    canvas.restore();
                    canvas.save();
                    canvas.translate(this.f255a, this.b);
                    canvas.drawPicture(this.j);
                    canvas.setMatrix(this.t);
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            this.x = true;
        }
    }
}
