package com.agilebinary.mobilemonitor.client.android.b;

import com.agilebinary.a.a.b.c;
import com.agilebinary.a.a.b.i;
import com.agilebinary.a.a.b.q;
import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.d.f;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;

public class o implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private static final String f164a = f.a("ProtocolStatus");
    private q b;
    private i c;
    private int d;
    private boolean e;
    private InputStream f;
    private int g;

    public o(q qVar) {
        this.b = qVar;
        this.d = qVar.a().b();
        this.c = qVar.b();
        try {
            if (this.c.g()) {
                this.f = this.c.a();
            }
        } catch (IllegalStateException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        String a2 = a("SI-S");
        this.g = -2;
        if (a2 != null) {
            try {
                this.g = Integer.parseInt(a2);
            } catch (NumberFormatException e4) {
                b.d(f164a, "failed to parse header-value of " + a2);
            }
        }
    }

    protected o(boolean z) {
        if (!z) {
            throw new IllegalArgumentException();
        }
        this.e = z;
        b.a(f164a, "new ProtocolStatus ", "ABORTED=" + this.e);
    }

    public static boolean a(int i) {
        return i == 8 || i == 5 || i == 7 || i == 11;
    }

    public static boolean b(int i) {
        return i == -3;
    }

    public static boolean c(int i) {
        return i == 304;
    }

    public int a() {
        return this.d;
    }

    public String a(String str) {
        c[] b2 = this.b.b(str);
        if (b2.length == 0) {
            return null;
        }
        return b2[0].d();
    }

    public boolean b() {
        if (this.e) {
            return false;
        }
        int c2 = c();
        if (a() == 200) {
            return c2 == 0 || c2 == 8 || c2 == -2 || a("SI-S-T") != null;
        }
        return false;
    }

    public int c() {
        return this.g;
    }

    public boolean d() {
        return a(this.g);
    }

    public InputStream e() {
        return this.f;
    }

    public void f() {
        if (this.f != null) {
            try {
                this.f.close();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    public boolean g() {
        return b(this.g);
    }

    /* JADX INFO: finally extract failed */
    public String h() {
        if (this.f == null) {
            return null;
        }
        if (this.c.b() > 2147483647L) {
            throw new IllegalArgumentException("HTTP entity too large to be buffered in memory");
        }
        int b2 = (int) this.c.b();
        if (b2 < 0) {
            b2 = 4096;
        }
        String c2 = com.agilebinary.a.a.b.k.c.c(this.c);
        if (c2 == null) {
            c2 = "ISO-8859-1";
        }
        InputStreamReader inputStreamReader = new InputStreamReader(this.f, c2);
        com.agilebinary.a.a.b.k.b bVar = new com.agilebinary.a.a.b.k.b(b2);
        try {
            char[] cArr = new char[1024];
            while (true) {
                int read = inputStreamReader.read(cArr);
                if (read != -1) {
                    bVar.a(cArr, 0, read);
                } else {
                    inputStreamReader.close();
                    return bVar.toString();
                }
            }
        } catch (Throwable th) {
            inputStreamReader.close();
            throw th;
        }
    }
}
