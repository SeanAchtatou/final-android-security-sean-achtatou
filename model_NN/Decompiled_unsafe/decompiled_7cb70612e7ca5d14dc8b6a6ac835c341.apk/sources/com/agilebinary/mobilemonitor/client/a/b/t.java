package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.a;
import com.agilebinary.mobilemonitor.client.a.b;
import com.agilebinary.phonebeagle.client.R;

public class t extends f {
    protected String w;
    protected String x;
    protected String y;

    public t(String str, long j, long j2, a aVar, b bVar) {
        super(str, j, j2, aVar, bVar);
        this.w = aVar.g();
        this.x = aVar.g();
        this.y = aVar.g();
    }

    public String a(Context context) {
        String b = com.agilebinary.mobilemonitor.client.android.d.b.b(n(), o());
        switch (c()) {
            case 1:
                return context.getString(R.string.label_event_sms_from_fmt, b);
            case 2:
                return context.getString(R.string.label_event_sms_to_fmt, b);
            case 3:
            default:
                return "";
            case 4:
                return context.getString(R.string.label_event_sms_to_blocked_fmt, b);
            case 5:
                return context.getString(R.string.label_event_sms_from_blocked_fmt, b);
        }
    }

    public String b(Context context) {
        return m();
    }

    public byte d() {
        return 2;
    }

    public String m() {
        return this.w;
    }

    public String n() {
        return this.x;
    }

    public String o() {
        return this.y;
    }
}
