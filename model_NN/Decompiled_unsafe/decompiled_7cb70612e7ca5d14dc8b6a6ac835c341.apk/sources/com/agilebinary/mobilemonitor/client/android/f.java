package com.agilebinary.mobilemonitor.client.android;

import android.content.Context;
import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.b.c.a;
import com.agilebinary.mobilemonitor.client.android.b.x;
import com.agilebinary.phonebeagle.client.R;
import java.io.Serializable;
import java.util.List;

public class f implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    static final String f185a = com.agilebinary.mobilemonitor.client.android.d.f.a("MyAccountContainer");
    private boolean b;
    private a c;
    private a d;

    public f(a aVar, boolean z) {
        b.a(f185a, "<CONSTRUCT>", "" + this);
        this.c = aVar;
        this.d = aVar;
        this.b = z;
    }

    public static CharSequence a(Context context, a aVar) {
        if (!aVar.e()) {
            return context.getString(R.string.label_accountinfo_target_notset);
        }
        return context.getString(R.string.label_accountinfo_target_msg, aVar.i(), b(context, aVar));
    }

    public static String b(Context context, a aVar) {
        String h = aVar.h();
        return h.equals("android") ? context.getString(R.string.application_platform_android) : h.equals("blackberry") ? context.getString(R.string.application_platform_blackberry) : h.equals("iphone") ? context.getString(R.string.application_platform_iphone) : h.equals("symbian_s60") ? context.getString(R.string.application_platform_symbian_s60) : h.equals("symbian_hat3") ? context.getString(R.string.application_platform_symbian_hat3) : h;
    }

    public a a() {
        return this.c;
    }

    public void a(a aVar) {
        this.d = aVar;
    }

    public void a(String str, String str2) {
        if (this.c.a().equals(str)) {
            this.c.c(str2);
            return;
        }
        for (a aVar : this.c.s()) {
            if (str.equals(aVar.a())) {
                aVar.c(str2);
                return;
            }
        }
    }

    public List b() {
        return this.c.s();
    }

    public void b(String str, String str2) {
        if (this.c.a().equals(str)) {
            this.c.d(str2);
            return;
        }
        for (a aVar : this.c.s()) {
            if (str.equals(aVar.a())) {
                aVar.d(str2);
                return;
            }
        }
    }

    public String c() {
        b.a(f185a, "getKey", "" + this, this.d.a());
        return this.d.a();
    }

    public void c(String str, String str2) {
        if (this.c.a().equals(str)) {
            this.c.b(str2);
            return;
        }
        for (a aVar : this.c.s()) {
            if (str.equals(aVar.a())) {
                aVar.b(str2);
                return;
            }
        }
    }

    public String d() {
        return this.d.b();
    }

    public String e() {
        b.a(f185a, "getAlias", "" + this, this.d.c());
        return this.d.c();
    }

    public String f() {
        return this.d.d();
    }

    public boolean g() {
        return this.b || this.d.q();
    }

    public boolean h() {
        return this.b || this.d.n();
    }

    public boolean i() {
        return this.b || this.d.o();
    }

    public boolean j() {
        return this.b || (this.d.h().equals("android") && this.d.p());
    }

    public boolean k() {
        return this.b || (this.d.h().equals("android") && this.d.r());
    }

    public boolean l() {
        return this.b || (this.d.h().equals("android") && this.d.j());
    }

    public boolean m() {
        return this.b || (this.d.h().equals("android") && s() >= 172 && this.d.k());
    }

    public boolean n() {
        return this.b || (this.d.h().equals("android") && s() >= 172 && this.d.l());
    }

    public boolean o() {
        return this.b || (this.d.h().equals("android") && s() >= 172 && this.d.m());
    }

    public boolean p() {
        return true;
    }

    public boolean q() {
        return x.a().m() && (!this.d.j() || !this.d.n() || !this.d.o() || !this.d.p() || !this.d.q() || !this.d.r());
    }

    public String r() {
        return this.d.i();
    }

    public int s() {
        try {
            String[] split = r().split("\\.");
            if (split.length > 2) {
                split[0] + "." + split[1];
            }
            return Integer.parseInt(r().replace(".", ""));
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return 0;
        }
    }
}
