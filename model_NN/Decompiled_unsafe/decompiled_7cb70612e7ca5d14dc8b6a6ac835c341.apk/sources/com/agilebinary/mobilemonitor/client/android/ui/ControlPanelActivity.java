package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.android.b.x;
import com.agilebinary.phonebeagle.client.R;

public class ControlPanelActivity extends al {
    public static void a(Context context) {
        context.startActivity(new Intent(context, ControlPanelActivity.class));
    }

    /* access modifiers changed from: protected */
    public int a() {
        return R.layout.controlpanel;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        ((TextView) findViewById(R.id.controlpanel_url)).setText(x.a().i());
    }
}
