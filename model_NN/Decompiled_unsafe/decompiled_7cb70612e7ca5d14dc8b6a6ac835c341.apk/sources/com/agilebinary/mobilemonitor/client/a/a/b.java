package com.agilebinary.mobilemonitor.client.a.a;

import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

public class b extends FilterInputStream {

    /* renamed from: a  reason: collision with root package name */
    protected Inflater f124a;
    protected byte[] b;
    protected int c;
    boolean d;
    boolean e;
    int f = 0;
    private int g;
    private int h;

    public b(InputStream inputStream, Inflater inflater, byte[] bArr, int i) {
        super(inputStream);
        if (inputStream == null || inflater == null) {
            throw new NullPointerException();
        }
        this.g = i;
        this.f124a = inflater;
        this.b = bArr;
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (this.d) {
            throw new IOException();
        }
        this.c = this.in.read(this.b, 0, Math.min(this.b.length, this.g - this.h));
        if (this.c > 0) {
            this.h += this.c;
            this.f124a.setInput(this.b, 0, this.c);
        }
    }

    public int available() {
        if (!this.d) {
            return this.e ? 0 : 1;
        }
        throw new IOException();
    }

    public int b() {
        int i = this.g - this.h;
        if (i > 0) {
            System.out.println("################ AAAARRRRGGGGHHHHH, " + i + " bytes not read!!!");
            int i2 = i;
            while (i2 > 0) {
                i2 -= this.in.read(this.b, 0, Math.min(i2, this.b.length));
            }
        }
        return i;
    }

    public void close() {
        if (!this.d) {
            this.f124a.end();
            this.d = true;
            this.e = true;
            super.close();
        }
    }

    public void mark(int i) {
    }

    public boolean markSupported() {
        return false;
    }

    public int read() {
        byte[] bArr = new byte[1];
        if (read(bArr, 0, 1) == -1) {
            return -1;
        }
        return bArr[0] & 255;
    }

    public int read(byte[] bArr, int i, int i2) {
        if (this.d) {
            throw new IOException();
        } else if (bArr == null) {
            throw new NullPointerException();
        } else if (i < 0 || i2 < 0 || i + i2 > bArr.length) {
            throw new IndexOutOfBoundsException();
        } else if (i2 == 0) {
            return 0;
        } else {
            if (this.f124a.finished()) {
                this.e = true;
                return -1;
            } else if (i > bArr.length || i2 < 0 || i < 0 || bArr.length - i < i2) {
                throw new ArrayIndexOutOfBoundsException();
            } else {
                do {
                    if (this.f124a.needsInput()) {
                        a();
                    }
                    try {
                        int inflate = this.f124a.inflate(bArr, i, i2);
                        if (inflate > 0) {
                            return inflate;
                        }
                        if (this.f124a.finished()) {
                            this.e = true;
                            return -1;
                        } else if (this.f124a.needsDictionary()) {
                            return -1;
                        }
                    } catch (DataFormatException e2) {
                        if (this.c == -1) {
                            throw new EOFException();
                        }
                        throw ((IOException) new IOException().initCause(e2));
                    }
                } while (this.c != -1);
                throw new EOFException();
            }
        }
    }

    public void reset() {
        throw new IOException();
    }

    public long skip(long j) {
        long j2 = 0;
        if (j >= 0) {
            while (j2 < j) {
                long j3 = j - j2;
                int read = read(this.b, 0, j3 > ((long) this.b.length) ? this.b.length : (int) j3);
                if (read == -1) {
                    this.e = true;
                    return j2;
                }
                j2 += (long) read;
            }
            return j2;
        }
        throw new IllegalArgumentException();
    }
}
