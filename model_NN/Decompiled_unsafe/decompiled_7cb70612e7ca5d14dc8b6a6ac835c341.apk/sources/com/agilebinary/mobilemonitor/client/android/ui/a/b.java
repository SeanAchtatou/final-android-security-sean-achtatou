package com.agilebinary.mobilemonitor.client.android.ui.a;

import android.os.AsyncTask;
import android.os.PowerManager;
import com.agilebinary.a.a.b.b.a.g;
import com.agilebinary.mobilemonitor.client.android.b.m;
import com.agilebinary.mobilemonitor.client.android.b.s;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.mobilemonitor.client.android.ui.ChangeAliasActivity;
import com.agilebinary.mobilemonitor.client.android.ui.ah;

public class b extends AsyncTask implements m {

    /* renamed from: a  reason: collision with root package name */
    static final String f205a = f.a("ChangeAliasTask");
    public static b b;
    private final ah c;
    private g d;

    public b(ah ahVar) {
        this.c = ahVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public o doInBackground(a... aVarArr) {
        o oVar;
        PowerManager.WakeLock newWakeLock = ((PowerManager) this.c.getSystemService("power")).newWakeLock(6, f205a);
        newWakeLock.acquire();
        try {
            this.c.g.b().a().c(aVarArr[0].b.a(), aVarArr[0].b.b(), aVarArr[0].f204a, this);
            this.c.g.c(aVarArr[0]);
            oVar = new o(true);
            try {
                newWakeLock.release();
            } catch (Exception e) {
            }
            b = null;
        } catch (s e2) {
            oVar = new o(e2);
            try {
                newWakeLock.release();
            } catch (Exception e3) {
            }
            b = null;
        } catch (Throwable th) {
            try {
                newWakeLock.release();
            } catch (Exception e4) {
            }
            b = null;
            throw th;
        }
        return oVar;
    }

    public void a() {
        cancel(false);
        if (this.d != null) {
            try {
                this.d.i();
            } catch (Exception e) {
            }
        }
    }

    public void a(g gVar) {
        this.d = gVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(o oVar) {
        ChangeAliasActivity.a(this.c, oVar);
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        ChangeAliasActivity.a(this.c, (o) null);
    }
}
