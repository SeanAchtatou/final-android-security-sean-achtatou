package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.a;
import com.agilebinary.mobilemonitor.client.a.b;
import com.agilebinary.mobilemonitor.client.android.d.g;
import com.agilebinary.phonebeagle.client.R;

public class p extends d {
    private double r;
    private double s;

    public p(String str, long j, long j2, a aVar, b bVar) {
        super(str, j, j2, aVar, bVar);
        this.r = aVar.e();
        this.s = aVar.e();
    }

    public byte d() {
        return 5;
    }

    public String d(Context context) {
        return context.getString(R.string.label_event_location_type_gps);
    }

    public String e(Context context) {
        if (m()) {
            return "";
        }
        return context.getString(R.string.label_event_location_wrapNotCurrent, g.a(context).d(l()));
    }
}
