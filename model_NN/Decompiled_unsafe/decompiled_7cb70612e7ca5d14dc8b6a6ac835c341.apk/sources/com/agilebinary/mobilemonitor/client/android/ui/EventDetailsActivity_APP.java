package com.agilebinary.mobilemonitor.client.android.ui;

import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.a.b.a;
import com.agilebinary.mobilemonitor.client.a.b.b;
import com.agilebinary.mobilemonitor.client.android.b.x;
import com.agilebinary.mobilemonitor.client.android.d.g;
import com.agilebinary.phonebeagle.client.R;

public class EventDetailsActivity_APP extends aw {
    private TextView n;
    private TextView o;
    private TextView p;
    private TextView q;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public void a(ViewGroup viewGroup) {
        ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.eventdetails_app, viewGroup, true);
        this.o = (TextView) findViewById(R.id.eventdetails_app_time);
        this.n = (TextView) findViewById(R.id.eventdetails_app_kind);
        this.p = (TextView) findViewById(R.id.eventdetails_app_name);
        this.q = (TextView) findViewById(R.id.eventdetails_app_details);
        this.q.setMovementMethod(LinkMovementMethod.getInstance());
    }

    /* access modifiers changed from: protected */
    public void a(b bVar) {
        super.a(bVar);
        a aVar = (a) bVar;
        this.n.setText(aVar.a(this));
        this.p.setText(aVar.b(this));
        String[] split = aVar.c().split(":");
        String str = "";
        if (split.length > 1) {
            str = split[1];
        }
        this.o.setText(g.a(this).c(aVar.a()));
        this.q.setText(Html.fromHtml("<a href=\"" + x.a().b(str) + "\">" + str + "</a>"));
    }
}
