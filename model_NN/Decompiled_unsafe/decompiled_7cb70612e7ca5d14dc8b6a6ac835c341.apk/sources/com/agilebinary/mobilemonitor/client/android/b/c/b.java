package com.agilebinary.mobilemonitor.client.android.b.c;

import com.agilebinary.mobilemonitor.client.android.d.f;
import java.io.Serializable;

public class b implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private static final String f158a = f.a("GeocodeCdmaCell");
    private int b;
    private int c;
    private int d;

    public b() {
    }

    public b(int i, int i2, int i3) {
        this.b = i;
        this.c = i2;
        this.d = i3;
    }

    public String a() {
        return "{\"base_station_id\":\"" + this.b + "\", \"" + "network_id" + "\":\"" + this.c + "\", \"" + "system_id" + "\":\"" + this.d + "\"}";
    }

    public void a(int i) {
        this.b = i;
    }

    public int b() {
        return this.b;
    }

    public void b(int i) {
        this.c = i;
    }

    public int c() {
        return this.c;
    }

    public void c(int i) {
        this.d = i;
    }

    public int d() {
        return this.d;
    }

    public String toString() {
        return "GeocodeCdmaCell: " + a();
    }
}
