package com.agilebinary.mobilemonitor.client.android.ui.a;

import android.os.AsyncTask;
import android.os.PowerManager;
import com.agilebinary.a.a.b.b.a.g;
import com.agilebinary.mobilemonitor.client.android.b.m;
import com.agilebinary.mobilemonitor.client.android.b.s;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.mobilemonitor.client.android.ui.AccountActivity;
import com.agilebinary.mobilemonitor.client.android.ui.ah;

public class k extends AsyncTask implements m {

    /* renamed from: a  reason: collision with root package name */
    static final String f214a = f.a("LinkedAccountAddTask");
    public static k b;
    private final ah c;
    private String d;
    private g e;

    public k(ah ahVar, String str) {
        this.c = ahVar;
        this.d = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public o doInBackground(String... strArr) {
        o oVar;
        PowerManager.WakeLock newWakeLock = ((PowerManager) this.c.getSystemService("power")).newWakeLock(6, f214a);
        newWakeLock.acquire();
        try {
            this.c.g.b().a().a(this.c.g.a(), strArr[0], strArr[1], this);
            oVar = new o(true);
            try {
                newWakeLock.release();
            } catch (Exception e2) {
            }
            b = null;
        } catch (s e3) {
            oVar = new o(e3);
            try {
                newWakeLock.release();
            } catch (Exception e4) {
            }
            b = null;
        } catch (Throwable th) {
            try {
                newWakeLock.release();
            } catch (Exception e5) {
            }
            b = null;
            throw th;
        }
        return oVar;
    }

    public void a(g gVar) {
        this.e = gVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(o oVar) {
        AccountActivity.b(this.c, oVar, this.d);
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        AccountActivity.b(this.c, null, this.d);
    }
}
