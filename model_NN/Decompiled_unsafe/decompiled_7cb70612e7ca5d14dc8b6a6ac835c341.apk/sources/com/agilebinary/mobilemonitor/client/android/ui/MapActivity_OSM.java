package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Menu;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.android.d.g;
import com.agilebinary.mobilemonitor.client.android.ui.b.c;
import com.agilebinary.mobilemonitor.client.android.ui.b.d;
import com.agilebinary.mobilemonitor.client.android.ui.b.h;
import com.agilebinary.mobilemonitor.client.android.ui.b.i;
import com.agilebinary.phonebeagle.client.R;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.osmdroid.a;
import org.osmdroid.d.a.e;
import org.osmdroid.d.b;
import org.osmdroid.util.BoundingBoxE6;

public class MapActivity_OSM extends al implements e {
    private com.agilebinary.mobilemonitor.client.a.b.e A;
    private TextView B;
    /* access modifiers changed from: private */
    public dd C;
    /* access modifiers changed from: private */
    public d D;

    /* renamed from: a  reason: collision with root package name */
    protected ImageButton f201a;
    protected ImageButton b;
    protected ImageButton i;
    protected ImageButton j;
    com.agilebinary.mobilemonitor.client.android.ui.b.e k = new dc(this);
    /* access modifiers changed from: private */
    public RelativeLayout l;
    /* access modifiers changed from: private */
    public b m;
    /* access modifiers changed from: private */
    public a n;
    /* access modifiers changed from: private */
    public org.osmdroid.d.a o;
    /* access modifiers changed from: private */
    public i p;
    /* access modifiers changed from: private */
    public c q;
    /* access modifiers changed from: private */
    public com.agilebinary.mobilemonitor.client.android.ui.b.a r;
    /* access modifiers changed from: private */
    public h s;
    /* access modifiers changed from: private */
    public List t;
    /* access modifiers changed from: private */
    public com.agilebinary.mobilemonitor.client.android.c.a u;
    private TextView v;
    private TextView w;
    private TextView x;
    private TextView y;
    private TextView z;

    public static void a(Context context, List list) {
        Intent intent = new Intent(context, MapActivity_OSM.class);
        intent.putExtra("EXTRA_EVENT_IDS", (Serializable) list);
        context.startActivity(intent);
    }

    private boolean a(com.agilebinary.mobilemonitor.client.a.b.e eVar) {
        return this.t.indexOf(eVar) < this.t.size() + -1;
    }

    private boolean b(com.agilebinary.mobilemonitor.client.a.b.e eVar) {
        return this.t.indexOf(eVar) > 0;
    }

    private boolean c(com.agilebinary.mobilemonitor.client.a.b.e eVar) {
        return this.t.indexOf(eVar) == 0;
    }

    private boolean d(com.agilebinary.mobilemonitor.client.a.b.e eVar) {
        return this.t.indexOf(eVar) == this.t.size() + -1;
    }

    /* access modifiers changed from: private */
    public void e(com.agilebinary.mobilemonitor.client.a.b.e eVar) {
        boolean z2 = true;
        if (eVar != null) {
            this.A = eVar;
            this.s.a(this.t.indexOf(eVar));
            this.v.setText(g.a(this).d(this.A.h()));
            this.z.setText(getString(R.string.label_event_location_accuracy_short, new Object[]{eVar.i()}));
            CharSequence a2 = ((com.agilebinary.mobilemonitor.client.a.d) eVar).a(this);
            this.y.setText(a2);
            this.B.setVisibility(a2.length() == 0 ? 4 : 0);
            this.w.setText(eVar.d(this));
            this.x.setText(eVar.e(this));
            this.b.setEnabled(a(this.A));
            this.f201a.setEnabled(b(this.A));
            this.i.setEnabled(!c(this.A));
            ImageButton imageButton = this.j;
            if (d(this.A)) {
                z2 = false;
            }
            imageButton.setEnabled(z2);
            this.m.invalidate();
        }
    }

    private List f(com.agilebinary.mobilemonitor.client.a.b.e eVar) {
        ArrayList arrayList = new ArrayList();
        Point b2 = this.m.getProjection().b(((com.agilebinary.mobilemonitor.client.a.d) eVar).c(), (Point) null);
        Point point = new Point();
        Iterator it = this.t.iterator();
        while (true) {
            Point point2 = point;
            if (!it.hasNext()) {
                return arrayList;
            }
            com.agilebinary.mobilemonitor.client.a.b.e eVar2 = (com.agilebinary.mobilemonitor.client.a.b.e) it.next();
            com.agilebinary.mobilemonitor.client.a.d dVar = (com.agilebinary.mobilemonitor.client.a.d) eVar2;
            if (dVar != eVar && dVar.c_()) {
                point2 = this.m.getProjection().b(dVar.c(), point2);
                if (Math.sqrt(Math.pow((double) (b2.x - point2.x), 2.0d) + Math.pow((double) (b2.y - point2.y), 2.0d)) < 30.0d) {
                    arrayList.add(eVar2);
                }
            }
            point = point2;
        }
    }

    /* access modifiers changed from: private */
    public void n() {
        ArrayList arrayList = new ArrayList();
        for (com.agilebinary.mobilemonitor.client.a.b.e eVar : this.t) {
            com.agilebinary.mobilemonitor.client.a.d dVar = (com.agilebinary.mobilemonitor.client.a.d) eVar;
            if (dVar.c_()) {
                arrayList.add(dVar.c());
            }
        }
        BoundingBoxE6 a2 = BoundingBoxE6.a(arrayList);
        if (arrayList.size() > 0) {
            if (a2.b() > 10) {
                this.o.a(a2);
            } else {
                this.o.a(10000, 10000);
            }
            this.o.a(a2.a());
        }
    }

    /* access modifiers changed from: protected */
    public int a() {
        return R.layout.map;
    }

    /* renamed from: a */
    public boolean b(int i2, dh dhVar) {
        if (this.D == null) {
            List f = f(dhVar.f315a);
            if (f.size() == 0) {
                e(dhVar.f315a);
            } else {
                f.add(0, dhVar.f315a);
                this.D = new d(this, this.k);
                this.D.setCancelable(true);
                this.D.a(f);
                this.D.show();
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void b() {
        e((com.agilebinary.mobilemonitor.client.a.b.e) this.t.get(this.t.indexOf(this.A) + 1));
    }

    /* renamed from: b */
    public boolean a(int i2, dh dhVar) {
        return false;
    }

    /* access modifiers changed from: protected */
    public void c() {
        e((com.agilebinary.mobilemonitor.client.a.b.e) this.t.get(this.t.indexOf(this.A) - 1));
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void l() {
        e((com.agilebinary.mobilemonitor.client.a.b.e) this.t.get(0));
    }

    /* access modifiers changed from: protected */
    public void m() {
        e((com.agilebinary.mobilemonitor.client.a.b.e) this.t.get(this.t.size() - 1));
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.u = com.agilebinary.mobilemonitor.client.android.c.a.a(this);
        this.l = (RelativeLayout) findViewById(R.id.map_main);
        this.m = new b(this, 256);
        this.n = new a(this);
        this.o = this.m.getController();
        this.l.addView(this.m, new LinearLayout.LayoutParams(-1, -1));
        this.v = (TextView) findViewById(R.id.eventdetails_navigation_text);
        this.w = (TextView) findViewById(R.id.map_curevent_source);
        this.x = (TextView) findViewById(R.id.map_curevent_details);
        this.y = (TextView) findViewById(R.id.map_curevent_speed);
        this.B = (TextView) findViewById(R.id.map_curevent_label_speed);
        this.z = (TextView) findViewById(R.id.map_curevent_accuracy);
        this.f201a = (ImageButton) findViewById(R.id.eventdetails_navigation_prev);
        this.b = (ImageButton) findViewById(R.id.eventdetails_navigation_next);
        this.i = (ImageButton) findViewById(R.id.eventdetails_navigation_first);
        this.j = (ImageButton) findViewById(R.id.eventdetails_navigation_last);
        this.f201a.setEnabled(false);
        this.b.setEnabled(false);
        this.i.setEnabled(false);
        this.j.setEnabled(false);
        this.f201a.setOnClickListener(new cu(this));
        this.b.setOnClickListener(new cv(this));
        this.i.setOnClickListener(new cw(this));
        this.j.setOnClickListener(new cx(this));
        List list = (List) getIntent().getSerializableExtra("EXTRA_EVENT_IDS");
        Collections.sort(list);
        this.C = new dd(this);
        this.C.execute(list);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i2) {
        if (i2 != 0) {
            return null;
        }
        Dialog dialog = new Dialog(this);
        dialog.setContentView((int) R.layout.map_menu_dialog);
        dialog.setTitle((int) R.string.label_map_dialog_title);
        ((CheckedTextView) dialog.findViewById(R.id.map_menu_toggle_satellite)).setVisibility(8);
        ((Button) dialog.findViewById(R.id.map_menu_zoomtomarkers)).setOnClickListener(new cy(this, dialog));
        ((CheckedTextView) dialog.findViewById(R.id.map_menu_toggle_directions)).setOnClickListener(new cz(this, dialog));
        ((CheckedTextView) dialog.findViewById(R.id.map_menu_toggle_accuracy)).setOnClickListener(new da(this, dialog));
        ((CheckedTextView) dialog.findViewById(R.id.map_menu_toggle_scale)).setOnClickListener(new db(this, dialog));
        return dialog;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        showDialog(0);
        return false;
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i2, Dialog dialog) {
        super.onPrepareDialog(i2, dialog);
        if (dialog != null) {
            try {
                ((CheckedTextView) dialog.findViewById(R.id.map_menu_toggle_directions)).setChecked(this.q.a());
                ((CheckedTextView) dialog.findViewById(R.id.map_menu_toggle_accuracy)).setChecked(this.r.a());
                ((CheckedTextView) dialog.findViewById(R.id.map_menu_toggle_scale)).setChecked(this.p.a());
            } catch (Exception e) {
                com.agilebinary.mobilemonitor.a.a.a.b.e("MAOSM", "onPrepareDialog");
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        if (this.C != null) {
            this.C.a();
        }
        super.onStop();
    }
}
