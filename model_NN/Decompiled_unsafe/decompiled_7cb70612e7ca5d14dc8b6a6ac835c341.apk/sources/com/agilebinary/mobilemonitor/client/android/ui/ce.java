package com.agilebinary.mobilemonitor.client.android.ui;

import android.database.Cursor;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.agilebinary.phonebeagle.client.R;

public class ce extends ck implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    /* renamed from: a  reason: collision with root package name */
    protected TextView f287a;
    protected TextView b;
    protected CheckBox c;
    private TextView g;
    private EventListActivity_LOC h;
    private long i;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.agilebinary.mobilemonitor.client.android.ui.ce, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public ce(bq bqVar, AttributeSet attributeSet, EventListActivity_LOC eventListActivity_LOC) {
        super(bqVar, attributeSet);
        this.h = eventListActivity_LOC;
        ((LayoutInflater) bqVar.getSystemService("layout_inflater")).inflate((int) R.layout.eventlist_rowview_loc, (ViewGroup) this, true);
        a();
    }

    /* access modifiers changed from: protected */
    public void a() {
        super.a();
        this.f287a = (TextView) findViewById(R.id.eventlist_rowview_loc_line1);
        this.b = (TextView) findViewById(R.id.eventlist_rowview_loc_line2);
        this.g = (TextView) findViewById(R.id.eventlist_rowview_loc_accuracy);
        this.c = (CheckBox) findViewById(R.id.eventlist_rowview_loc_checkbox);
        this.c.setOnCheckedChangeListener(this);
        this.f287a.setOnClickListener(this);
        this.b.setOnClickListener(this);
        this.g.setOnClickListener(this);
        this.e.setOnClickListener(this);
        this.d.setOnClickListener(this);
    }

    public void a(Cursor cursor) {
        int i2 = 0;
        super.a(cursor);
        this.i = cursor.getLong(0);
        this.f287a.setText(cursor.getString(cursor.getColumnIndex("line1")));
        this.b.setText(cursor.getString(cursor.getColumnIndex("line2")));
        Double valueOf = Double.valueOf(cursor.getDouble(cursor.getColumnIndex("accuracy")));
        if (valueOf == null || valueOf.doubleValue() <= 0.0d) {
            this.g.setText("");
        } else {
            this.g.setText(getContext().getString(R.string.label_event_location_accuracy, valueOf));
        }
        this.c.setChecked(this.h.b(this.i));
        CheckBox checkBox = this.c;
        if (cursor.isNull(cursor.getColumnIndex("lat"))) {
            i2 = 4;
        }
        checkBox.setVisibility(i2);
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        this.h.a(this.i, z);
    }

    public void onClick(View view) {
        if (this.c.getVisibility() == 0 && this.h.m()) {
            this.h.a(this.i);
        }
    }
}
