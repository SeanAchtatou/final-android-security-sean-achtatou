package com.agilebinary.mobilemonitor.client.android.ui;

import java.util.Comparator;

final class bk implements Comparator {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ EventListActivity_LOC f266a;

    bk(EventListActivity_LOC eventListActivity_LOC) {
        this.f266a = eventListActivity_LOC;
    }

    /* renamed from: a */
    public int compare(Long l, Long l2) {
        return l.compareTo(l2) * -1;
    }
}
