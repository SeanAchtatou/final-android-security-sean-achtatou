package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.a;
import com.agilebinary.mobilemonitor.client.a.b;
import com.agilebinary.phonebeagle.client.R;

public class h extends g {

    /* renamed from: a  reason: collision with root package name */
    protected long f130a;
    protected long b;
    protected long c;
    protected byte i;
    protected String w;
    protected String x;

    public h(String str, long j, long j2, a aVar, b bVar) {
        super(str, j, j2, aVar, bVar);
        this.f130a = bVar.a(aVar.d());
        this.b = bVar.a(aVar.d());
        this.c = bVar.a(aVar.d());
        this.i = aVar.b();
        this.w = aVar.g();
        this.x = aVar.g();
    }

    public long a() {
        return this.f130a;
    }

    public String a(Context context) {
        return com.agilebinary.mobilemonitor.client.android.d.b.b(n(), o());
    }

    public long b() {
        return this.b;
    }

    public String b(Context context) {
        String str = "";
        String str2 = "";
        int c2 = (int) ((c() - b()) / 1000);
        switch (m()) {
            case -1:
                str2 = context.getString(R.string.label_event_call_unknown);
                str = context.getString(R.string.label_event_call_line1_outgoing);
                break;
            case 1:
                str2 = context.getString(R.string.label_event_call_incoming);
                str = context.getString(R.string.label_event_call_line1_incoming);
                break;
            case 2:
                str = context.getString(R.string.label_event_call_line1_outgoing);
                if (c2 <= 0) {
                    str2 = context.getString(R.string.label_event_call_outgoing_missed);
                    break;
                } else {
                    str2 = context.getString(R.string.label_event_call_outgoing);
                    break;
                }
            case 3:
                str2 = context.getString(R.string.label_event_call_incoming_missed);
                str = context.getString(R.string.label_event_call_line1_incoming);
                break;
            case 4:
                str2 = context.getString(R.string.label_event_call_outgoing_blocked);
                str = context.getString(R.string.label_event_call_line1_outgoing);
                break;
            case 5:
                str2 = context.getString(R.string.label_event_call_incoming_blocked);
                str = context.getString(R.string.label_event_call_line1_incoming);
                break;
        }
        return String.format(str, str2);
    }

    public long c() {
        return this.c;
    }

    public byte d() {
        return 1;
    }

    public byte m() {
        return this.i;
    }

    public String n() {
        return this.w;
    }

    public String o() {
        return this.x;
    }
}
