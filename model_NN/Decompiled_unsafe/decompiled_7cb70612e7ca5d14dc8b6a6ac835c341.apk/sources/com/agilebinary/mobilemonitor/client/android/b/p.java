package com.agilebinary.mobilemonitor.client.android.b;

import com.agilebinary.a.a.b.c.c.a;
import com.agilebinary.a.a.b.c.c.d;
import com.agilebinary.a.a.b.c.d.b;
import com.agilebinary.a.a.b.c.d.f;
import com.agilebinary.a.a.b.i.c;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

public class p implements d {

    /* renamed from: a  reason: collision with root package name */
    public static final f f165a = new b();
    /* access modifiers changed from: private */
    public static final String b = com.agilebinary.mobilemonitor.client.android.d.f.a("PublicKeyTrustedSSLSocketFactory");
    private static final p c = new p();
    private final SSLContext d;
    private final SSLSocketFactory e;
    private final a f;
    private volatile f g;

    private p() {
        this.g = f165a;
        this.d = null;
        this.e = HttpsURLConnection.getDefaultSSLSocketFactory();
        this.f = null;
    }

    public p(x xVar) {
        this.g = f165a;
        this.d = SSLContext.getInstance("TLS");
        this.d.init(null, new TrustManager[]{new q(this, xVar)}, null);
        this.e = this.d.getSocketFactory();
        this.f = null;
    }

    public Socket a() {
        return (SSLSocket) this.e.createSocket();
    }

    public Socket a(Socket socket, String str, int i, InetAddress inetAddress, int i2, com.agilebinary.a.a.b.i.d dVar) {
        if (str == null) {
            throw new IllegalArgumentException("Target host may not be null.");
        } else if (dVar == null) {
            throw new IllegalArgumentException("Parameters may not be null.");
        } else {
            SSLSocket sSLSocket = (SSLSocket) (socket != null ? socket : a());
            if (inetAddress != null || i2 > 0) {
                if (i2 < 0) {
                    i2 = 0;
                }
                sSLSocket.bind(new InetSocketAddress(inetAddress, i2));
            }
            int f2 = c.f(dVar);
            int a2 = c.a(dVar);
            InetSocketAddress inetSocketAddress = this.f != null ? new InetSocketAddress(this.f.a(str), i) : new InetSocketAddress(str, i);
            try {
                sSLSocket.connect(inetSocketAddress, f2);
                sSLSocket.setSoTimeout(a2);
                try {
                    this.g.a(str, sSLSocket);
                    return sSLSocket;
                } catch (IOException e2) {
                    try {
                        sSLSocket.close();
                    } catch (Exception e3) {
                    }
                    throw e2;
                }
            } catch (SocketTimeoutException e4) {
                throw new com.agilebinary.a.a.b.c.f("Connect to " + inetSocketAddress + " timed out");
            }
        }
    }

    public Socket a(Socket socket, String str, int i, boolean z) {
        SSLSocket sSLSocket = (SSLSocket) this.e.createSocket(socket, str, i, z);
        this.g.a(str, sSLSocket);
        return sSLSocket;
    }

    public boolean a(Socket socket) {
        if (socket == null) {
            throw new IllegalArgumentException("Socket may not be null.");
        } else if (!(socket instanceof SSLSocket)) {
            throw new IllegalArgumentException("Socket not created by this factory.");
        } else if (!socket.isClosed()) {
            return true;
        } else {
            throw new IllegalArgumentException("Socket is closed.");
        }
    }
}
