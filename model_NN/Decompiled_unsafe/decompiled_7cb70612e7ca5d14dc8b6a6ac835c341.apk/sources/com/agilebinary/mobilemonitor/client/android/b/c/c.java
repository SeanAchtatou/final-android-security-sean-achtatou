package com.agilebinary.mobilemonitor.client.android.b.c;

import java.io.Serializable;

public final class c extends b implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private long f159a;
    private int b;
    private f c;

    public String a() {
        return "{\"create_date\":\"" + this.f159a + "\", \"geocoder_source_id\":\"" + this.b + "\", \"" + "base_station_id" + "\":\"" + super.b() + "\", \"" + "network_id" + "\":\"" + super.c() + "\", \"" + "system_id" + "\":\"" + super.d() + "\", \"" + "latitude" + "\":\"" + this.c.f162a + "\", \"" + "longitude" + "\":\"" + this.c.b + "\", \"" + "accuracy" + "\":\"" + this.c.c + "\", \"" + "country" + "\":\"" + this.c.d + "\", \"" + "country_code" + "\":\"" + this.c.e + "\", \"" + "region" + "\":\"" + this.c.f + "\", \"" + "city" + "\":\"" + this.c.g + "\", \"" + "street" + "\":\"" + this.c.h + "\"}";
    }

    public void a(long j) {
        this.f159a = j;
    }

    public void a(f fVar) {
        this.c = fVar;
    }

    public void d(int i) {
        this.b = i;
    }

    public f e() {
        return this.c;
    }

    public String toString() {
        return "GeocodeCdmaCellLocation: " + a();
    }
}
