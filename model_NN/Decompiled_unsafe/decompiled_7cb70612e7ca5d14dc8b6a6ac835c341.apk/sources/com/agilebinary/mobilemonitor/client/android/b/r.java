package com.agilebinary.mobilemonitor.client.android.b;

import com.agilebinary.a.a.b.b.a.g;
import com.agilebinary.a.a.b.q;
import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.d.f;
import java.io.IOException;
import java.util.TimerTask;

public class r extends TimerTask {

    /* renamed from: a  reason: collision with root package name */
    public static final String f167a = f.a("RequestTimeoutTimerTask");
    private g b;
    private long c;

    private r(long j) {
        this.c = j;
    }

    private q a(t tVar, g gVar) {
        a(gVar);
        tVar.c().schedule(this, this.c);
        try {
            q a2 = tVar.a(gVar);
            a(null);
            cancel();
            return a2;
        } catch (IOException e) {
            b.d(f167a, "exception when executing a request", e);
            try {
                gVar.i();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
            cancel();
            tVar.b();
            throw e;
        }
    }

    public static q a(t tVar, g gVar, long j) {
        return new r(j).a(tVar, gVar);
    }

    public void a(g gVar) {
        this.b = gVar;
    }

    public void run() {
        b.a(f167a, "running");
        if (this.b != null) {
            try {
                this.b.i();
                b.b(f167a, "RequestTimeoutTimerTask aborted a request due to timeout!");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
