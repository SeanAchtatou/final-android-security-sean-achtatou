package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.mobilemonitor.client.android.ui.a.n;
import com.agilebinary.mobilemonitor.client.android.ui.a.o;
import com.agilebinary.phonebeagle.client.R;

public class ResetPasswordActivity extends ah {

    /* renamed from: a  reason: collision with root package name */
    private static final String f202a = f.a("ResetPasswordActivity");
    /* access modifiers changed from: private */
    public EditText b;
    private Button h;
    private Button i;

    public static void a(Activity activity, int i2) {
        activity.startActivityForResult(new Intent(activity, ResetPasswordActivity.class), i2);
    }

    public static void a(Context context, o oVar) {
        Intent intent = new Intent(context, ResetPasswordActivity.class);
        intent.putExtra("EXTRA_SERVICE_RESULT", oVar);
        intent.setFlags(536870912);
        context.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        b((int) R.string.msg_progress_communicating);
        n.b = new n(this);
        n.b.execute(str);
    }

    /* access modifiers changed from: protected */
    public int a() {
        return R.layout.reset_password;
    }

    /* access modifiers changed from: protected */
    public void e() {
        if (n.b != null) {
            try {
                n.b.a();
            } catch (Exception e) {
            }
        }
    }

    public void finish() {
        b.b(f202a, "FINISH...");
        super.finish();
        b.b(f202a, "...FINISH");
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.b = (EditText) findViewById(R.id.resetpassword_key);
        this.h = (Button) findViewById(R.id.resetpassword_cancel);
        this.i = (Button) findViewById(R.id.resetpassword_ok);
        this.i.setOnClickListener(new di(this));
        this.h.setOnClickListener(new dj(this));
        if (bundle != null) {
            this.b.setText(bundle.getString("EXTRA_KEY"));
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        b.b(f202a, "onNewIntent...");
        this.b.setText("");
        o oVar = (o) intent.getSerializableExtra("EXTRA_SERVICE_RESULT");
        b(false);
        if (oVar != null) {
            if (oVar.b() != null) {
                if (oVar.b().c()) {
                    a((int) R.string.error_service_account_general);
                } else if (oVar.b().b()) {
                    a((int) R.string.error_service_account_general);
                } else if (oVar.b().d()) {
                    a((int) R.string.error_password_reset_missing_email);
                } else {
                    a((int) R.string.error_service_account_general);
                }
            } else if (oVar.a()) {
                setResult(-1);
                finish();
            }
        }
        b.b(f202a, "...onNewIntent");
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        b.b(f202a, "onSaveInstanceState...");
        bundle.putString("EXTRA_KEY", this.b.getText().toString());
        super.onSaveInstanceState(bundle);
        b.b(f202a, "...onSaveInstanceState");
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
