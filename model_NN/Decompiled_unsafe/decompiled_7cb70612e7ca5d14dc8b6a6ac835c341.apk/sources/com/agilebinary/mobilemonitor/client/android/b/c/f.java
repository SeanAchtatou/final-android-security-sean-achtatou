package com.agilebinary.mobilemonitor.client.android.b.c;

import java.io.Serializable;

public final class f implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    double f162a;
    double b;
    double c;
    String d;
    String e;
    String f;
    String g;
    String h;

    public f() {
    }

    public f(double d2, double d3, double d4, String str, String str2, String str3, String str4, String str5) {
        this.f162a = d2;
        this.b = d3;
        this.c = d4;
        this.d = str;
        this.e = str2;
        this.f = str3;
        this.g = str4;
        this.h = str5;
    }

    public String a() {
        return "{\"latitude\":\"" + this.f162a + "\", \"" + "longitude" + "\":\"" + this.b + "\", \"" + "accuracy" + "\":\"" + this.c + "\", \"" + "country" + "\":\"" + this.d + "\", \"" + "country_code" + "\":\"" + this.e + "\", \"" + "region" + "\":\"" + this.f + "\", \"" + "city" + "\":\"" + this.g + "\", \"" + "street" + "\":\"" + this.h + "\"}";
    }

    public void a(double d2) {
        this.f162a = d2;
    }

    public double b() {
        return this.f162a;
    }

    public void b(double d2) {
        this.b = d2;
    }

    public double c() {
        return this.b;
    }

    public void c(double d2) {
        this.c = d2;
    }

    public double d() {
        return this.c;
    }

    public String toString() {
        return "Location: " + a();
    }
}
