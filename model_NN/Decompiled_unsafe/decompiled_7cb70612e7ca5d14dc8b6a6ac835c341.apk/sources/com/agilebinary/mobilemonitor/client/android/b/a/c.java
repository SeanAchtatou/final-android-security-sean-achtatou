package com.agilebinary.mobilemonitor.client.android.b.a;

import com.agilebinary.mobilemonitor.client.android.b.c.e;
import com.agilebinary.mobilemonitor.client.android.b.c.f;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

class c extends DefaultHandler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f143a;
    private List b;
    private e c;
    private char[] d = new char[4096];
    private int e = 0;

    public c(b bVar, List list) {
        this.f143a = bVar;
        this.b = list;
    }

    public void characters(char[] cArr, int i, int i2) {
        System.arraycopy(cArr, i, this.d, this.e, i2);
        this.e += i2;
    }

    public void endElement(String str, String str2, String str3) {
        String replace = new String(this.d, 0, this.e).trim().replace("\n", "");
        this.e = 0;
        if ("mcc".equals(str2)) {
            this.c.a(Integer.parseInt(replace));
        } else if ("mnc".equals(str2)) {
            this.c.b(Integer.parseInt(replace));
        } else if ("cid".equals(str2)) {
            this.c.c(Integer.parseInt(replace));
        } else if ("lac".equals(str2)) {
            this.c.d(Integer.parseInt(replace));
        } else if ("lat".equals(str2)) {
            this.c.f().a(Double.parseDouble(replace));
        } else if ("lon".equals(str2)) {
            this.c.f().b(Double.parseDouble(replace));
        } else if ("acc".equals(str2)) {
            this.c.f().c(Double.parseDouble(replace));
        }
    }

    public void startElement(String str, String str2, String str3, Attributes attributes) {
        if ("c".equals(str2)) {
            this.c = new e();
            this.c.a(new f());
            this.b.add(this.c);
        }
    }
}
