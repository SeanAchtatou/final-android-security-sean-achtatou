package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;
import java.util.List;

public class ct extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    List f300a = new ArrayList();
    final /* synthetic */ MainActivity b;

    public ct(MainActivity mainActivity) {
        this.b = mainActivity;
    }

    public void a(int i, int i2, byte b2) {
        this.f300a.add(new cs(this.b, i, i2, b2));
    }

    public int getCount() {
        return this.f300a.size();
    }

    public Object getItem(int i) {
        return this.f300a.get(i);
    }

    public long getItemId(int i) {
        return (long) ((cs) this.f300a.get(i)).c;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        cs csVar = (cs) getItem(i);
        cr crVar = view == null ? new cr(this.b, this.b, null) : (cr) view;
        crVar.setMenuEntry(csVar);
        return crVar;
    }
}
