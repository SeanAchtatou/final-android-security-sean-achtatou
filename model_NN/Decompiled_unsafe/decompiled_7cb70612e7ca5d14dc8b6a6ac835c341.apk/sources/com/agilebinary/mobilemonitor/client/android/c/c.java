package com.agilebinary.mobilemonitor.client.android.c;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDoneException;
import android.database.sqlite.SQLiteStatement;
import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.a.b.e;
import com.agilebinary.mobilemonitor.client.android.MyApplication;
import com.agilebinary.mobilemonitor.client.android.a;
import com.agilebinary.mobilemonitor.client.android.b.h;
import com.agilebinary.mobilemonitor.client.android.d.f;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class c implements h {

    /* renamed from: a  reason: collision with root package name */
    public static final String f175a = f.a("EventStore");
    public static final byte[] b = {1, 2, 3, 4, 6, 8, 9, 10, 11, 12};
    private Context c;
    private SQLiteDatabase d;
    private SQLiteStatement e;
    private SQLiteStatement f;
    private SQLiteStatement g;
    private SQLiteStatement h;
    private SQLiteStatement i;
    private SQLiteStatement j;
    private SQLiteStatement k;
    private SQLiteStatement l;
    private SQLiteStatement m;
    private SQLiteStatement n;
    private SQLiteStatement o;
    private SQLiteStatement p;
    private byte q = -1;
    private SQLiteStatement r;
    private SQLiteStatement s;
    private List t;
    private a u;
    private Calendar v;
    private ByteArrayOutputStream w = new ByteArrayOutputStream();
    private SQLiteStatement x;
    private SQLiteStatement y;
    private ByteArrayOutputStream z;

    public c(Context context, a aVar) {
        this.c = context;
        this.u = aVar;
        this.d = new o(this, this.c, "events", null, 12).getWritableDatabase();
        this.e = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s,%7$s,%8$s,%9$s,%10$s,%11$s,%12$s) values (?,?,?,?,?,?,?,?,?,?,?) ", "location", i.f176a, i.b, i.c, "line1", "line2", "accuracy", "lat", "lon", "contenttype", "powersave", "valloc"));
        this.f = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s) values (?,?,?,?,?) ", "call", f.f176a, f.b, f.c, "line1", "line2"));
        this.g = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s) values (?,?,?,?,?) ", "sms", k.f176a, k.b, k.c, "dir", "text"));
        this.h = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s,%7$s) values (?,?,?,?,?,?) ", "mms", j.f176a, j.b, j.c, "dir", "text", "thumbnail"));
        this.i = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s) values (?,?,?,?,?) ", "web", m.f176a, m.b, m.c, "line1", "line2"));
        this.j = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s) values (?,?,?,?) ", "syst_m", l.f176a, l.b, l.c, "line1"));
        this.k = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s,%7$s) values (?,?,?,?,?,?) ", e.f, e.f176a, e.b, e.c, e.g, e.h, e.i));
        this.l = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s,%7$s) values (?,?,?,?,?,?) ", g.f, g.f176a, g.b, g.c, g.g, g.h, g.i));
        this.m = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s,%7$s) values (?,?,?,?,?,?) ", n.f, n.f176a, n.b, n.c, n.g, n.h, n.i));
        this.n = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s,%7$s) values (?,?,?,?,?,?) ", h.f, h.f176a, h.b, h.c, h.g, h.h, h.i));
        this.o = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s) values (?,?) ", "curday", "eventtype", "day"));
        this.s = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s) values (?,?) ", "oldestevent", "eventtype", "oldest"));
        this.x = this.d.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s) values (?,?) ", "lastseen", "eventtype", "lastid"));
        this.p = this.d.compileStatement(String.format("SELECT %3$s FROM %1$s where %2$s = ? ", "curday", "eventtype", "day"));
        this.r = this.d.compileStatement(String.format("SELECT %3$s FROM %1$s where %2$s = ? ", "oldestevent", "eventtype", "oldest"));
        this.y = this.d.compileStatement(String.format("SELECT %3$s FROM %1$s where %2$s = ? ", "lastseen", "eventtype", "lastid"));
    }

    private int a(Calendar calendar) {
        return (calendar.get(1) * 1000) + calendar.get(6);
    }

    private void a(SQLiteDatabase sQLiteDatabase, String str) {
        b.a(f175a, "clearTable", str);
        sQLiteDatabase.execSQL(String.format("DELETE FROM %1$s", str));
    }

    private String f(byte b2) {
        switch (b2) {
            case 1:
                return "location";
            case 2:
                return "call";
            case 3:
                return "sms";
            case 4:
                return "mms";
            case 5:
            case 7:
            default:
                return null;
            case 6:
                return "web";
            case 8:
                return "syst_m";
            case 9:
                return e.f;
            case 10:
                return g.f;
            case 11:
                return n.f;
            case 12:
                return h.f;
        }
    }

    private String[] g(byte b2) {
        switch (b2) {
            case 1:
                return i.f;
            case 2:
                return f.f;
            case 3:
                return k.f;
            case 4:
                return j.f;
            case 5:
            case 7:
            default:
                return null;
            case 6:
                return m.f;
            case 8:
                return l.f;
            case 9:
                return e.j;
            case 10:
                return g.j;
            case 11:
                return n.j;
            case 12:
                return h.j;
        }
    }

    public synchronized long a(byte b2) {
        long j2;
        b.a(f175a, "getTimeOfOldestKnownEvent, et=", "" + ((int) b2));
        this.r.bindLong(1, (long) b2);
        j2 = 0;
        try {
            j2 = this.r.simpleQueryForLong();
        } catch (SQLiteDoneException e2) {
        }
        b.a(f175a, "...getTimeOfOldestKnownEvent", " =" + j2);
        return j2;
    }

    public long a(byte b2, long j2) {
        b.a(f175a, "getLatestEventDeviceTime, et=", "" + ((int) b2), " startOfDay=" + j2);
        SQLiteStatement compileStatement = this.d.compileStatement(String.format("SELECT max(%2$s) FROM %1$s  ", f(b2), d.f176a));
        long simpleQueryForLong = compileStatement.simpleQueryForLong();
        compileStatement.close();
        b.a(f175a, "...getLatestEventDeviceTime", " =" + simpleQueryForLong);
        return simpleQueryForLong == 0 ? j2 : simpleQueryForLong;
    }

    public synchronized Cursor a(byte b2, String str) {
        Cursor query;
        b.b(f175a, "getCursor...");
        query = this.d.query(f(b2), g(b2), str, null, null, null, d.f176a + " DESC");
        b.b(f175a, "...getCursor");
        return query;
    }

    public List a(List list) {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add((e) h((byte) 1, ((Long) it.next()).longValue()));
        }
        return arrayList;
    }

    public void a() {
        b.b(f175a, "CLOSE");
        this.d.close();
    }

    public synchronized void a(byte b2, int i2, long j2) {
        b.a(f175a, "onStartEvents ", " / " + ((int) b2), " / " + i2, " / " + new Date(j2));
        if (this.q != -1) {
            throw new IllegalStateException();
        }
        this.q = b2;
        if (b2 == 1) {
            this.t = new ArrayList();
            this.u.a();
        }
        b(b2, j2);
        this.z = new ByteArrayOutputStream(32000);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0069 A[Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0122 A[Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x013b A[Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x015c A[SYNTHETIC, Splitter:B:58:0x015c] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0186 A[Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x01bf A[Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x01ce A[Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x01f0 A[Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0212 A[Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0234 A[Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x003e A[Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(byte r7, com.agilebinary.mobilemonitor.client.a.b.b r8) {
        /*
            r6 = this;
            monitor-enter(r6)
            java.lang.String r1 = com.agilebinary.mobilemonitor.client.android.c.c.f175a     // Catch:{ all -> 0x0109 }
            java.lang.String r2 = "storeEvent..."
            com.agilebinary.mobilemonitor.a.a.a.b.b(r1, r2)     // Catch:{ all -> 0x0109 }
            r2 = 0
            switch(r7) {
                case 1: goto L_0x004b;
                case 2: goto L_0x004e;
                case 3: goto L_0x0051;
                case 4: goto L_0x0054;
                case 5: goto L_0x000c;
                case 6: goto L_0x0057;
                case 7: goto L_0x000c;
                case 8: goto L_0x005a;
                case 9: goto L_0x005d;
                case 10: goto L_0x0060;
                case 11: goto L_0x0063;
                case 12: goto L_0x0066;
                default: goto L_0x000c;
            }
        L_0x000c:
            java.io.ByteArrayOutputStream r1 = r6.w     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1.reset()     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            java.io.ObjectOutputStream r1 = new java.io.ObjectOutputStream     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            java.io.ByteArrayOutputStream r3 = r6.w     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1.writeObject(r8)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1.flush()     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.clearBindings()     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 1
            long r3 = r8.h()     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindLong(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 2
            long r3 = r8.g()     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindLong(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 3
            java.io.ByteArrayOutputStream r3 = r6.w     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            byte[] r3 = r3.toByteArray()     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindBlob(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            switch(r7) {
                case 1: goto L_0x0069;
                case 2: goto L_0x0122;
                case 3: goto L_0x013b;
                case 4: goto L_0x015c;
                case 5: goto L_0x003e;
                case 6: goto L_0x0186;
                case 7: goto L_0x003e;
                case 8: goto L_0x01bf;
                case 9: goto L_0x01ce;
                case 10: goto L_0x01f0;
                case 11: goto L_0x0212;
                case 12: goto L_0x0234;
                default: goto L_0x003e;
            }     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
        L_0x003e:
            r1 = r2
        L_0x003f:
            r1.executeInsert()     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
        L_0x0042:
            java.lang.String r1 = com.agilebinary.mobilemonitor.client.android.c.c.f175a     // Catch:{ all -> 0x0109 }
            java.lang.String r2 = "...storeEvent"
            com.agilebinary.mobilemonitor.a.a.a.b.b(r1, r2)     // Catch:{ all -> 0x0109 }
            monitor-exit(r6)
            return
        L_0x004b:
            android.database.sqlite.SQLiteStatement r2 = r6.e     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            goto L_0x000c
        L_0x004e:
            android.database.sqlite.SQLiteStatement r2 = r6.f     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            goto L_0x000c
        L_0x0051:
            android.database.sqlite.SQLiteStatement r2 = r6.g     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            goto L_0x000c
        L_0x0054:
            android.database.sqlite.SQLiteStatement r2 = r6.h     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            goto L_0x000c
        L_0x0057:
            android.database.sqlite.SQLiteStatement r2 = r6.i     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            goto L_0x000c
        L_0x005a:
            android.database.sqlite.SQLiteStatement r2 = r6.j     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            goto L_0x000c
        L_0x005d:
            android.database.sqlite.SQLiteStatement r2 = r6.k     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            goto L_0x000c
        L_0x0060:
            android.database.sqlite.SQLiteStatement r2 = r6.l     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            goto L_0x000c
        L_0x0063:
            android.database.sqlite.SQLiteStatement r2 = r6.m     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            goto L_0x000c
        L_0x0066:
            android.database.sqlite.SQLiteStatement r2 = r6.n     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            goto L_0x000c
        L_0x0069:
            r0 = r8
            com.agilebinary.mobilemonitor.client.a.b.e r0 = (com.agilebinary.mobilemonitor.client.a.b.e) r0     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = r0
            r3 = 4
            android.content.Context r4 = r6.c     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            java.lang.String r4 = r1.b(r4)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindString(r3, r4)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r3 = 5
            android.content.Context r4 = r6.c     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            java.lang.String r4 = r1.c(r4)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindString(r3, r4)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r3 = 6
            android.content.Context r4 = r6.c     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            double r4 = r1.f(r4)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindDouble(r3, r4)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 9
            byte r3 = r8.d()     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            long r3 = (long) r3     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindLong(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            boolean r1 = r8 instanceof com.agilebinary.mobilemonitor.client.a.b.d     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            if (r1 == 0) goto L_0x00cf
            com.agilebinary.mobilemonitor.client.a.b.d r8 = (com.agilebinary.mobilemonitor.client.a.b.d) r8     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 7
            double r3 = r8.a()     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindDouble(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 8
            double r3 = r8.j()     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindDouble(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 10
            boolean r3 = r8.n()     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            if (r3 == 0) goto L_0x00c9
            r3 = 1
        L_0x00b6:
            r2.bindLong(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 11
            boolean r3 = r8.m()     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            if (r3 == 0) goto L_0x00cc
            r3 = 1
        L_0x00c3:
            r2.bindLong(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = r2
            goto L_0x003f
        L_0x00c9:
            r3 = 0
            goto L_0x00b6
        L_0x00cc:
            r3 = 0
            goto L_0x00c3
        L_0x00cf:
            boolean r1 = r8 instanceof com.agilebinary.mobilemonitor.client.a.b.c     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            if (r1 == 0) goto L_0x010c
            com.agilebinary.mobilemonitor.client.a.b.c r8 = (com.agilebinary.mobilemonitor.client.a.b.c) r8     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            com.agilebinary.mobilemonitor.client.android.b.c.f r1 = r8.j()     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            if (r1 == 0) goto L_0x00f9
            r3 = 7
            double r4 = r1.b()     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindDouble(r3, r4)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r3 = 8
            double r4 = r1.c()     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindDouble(r3, r4)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
        L_0x00ec:
            r1 = 10
            r2.bindNull(r1)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 11
            r2.bindNull(r1)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = r2
            goto L_0x003f
        L_0x00f9:
            r1 = 7
            r2.bindNull(r1)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 8
            r2.bindNull(r1)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            goto L_0x00ec
        L_0x0103:
            r1 = move-exception
            r1.printStackTrace()     // Catch:{ all -> 0x0109 }
            goto L_0x0042
        L_0x0109:
            r1 = move-exception
            monitor-exit(r6)
            throw r1
        L_0x010c:
            r1 = 7
            r2.bindNull(r1)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 8
            r2.bindNull(r1)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 10
            r2.bindNull(r1)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 11
            r2.bindNull(r1)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = r2
            goto L_0x003f
        L_0x0122:
            com.agilebinary.mobilemonitor.client.a.b.h r8 = (com.agilebinary.mobilemonitor.client.a.b.h) r8     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 4
            android.content.Context r3 = r6.c     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            java.lang.String r3 = r8.a(r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 5
            android.content.Context r3 = r6.c     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            java.lang.String r3 = r8.b(r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = r2
            goto L_0x003f
        L_0x013b:
            android.database.sqlite.SQLiteStatement r1 = r6.g     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            com.agilebinary.mobilemonitor.client.a.b.t r8 = (com.agilebinary.mobilemonitor.client.a.b.t) r8     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2 = 4
            android.content.Context r3 = r6.c     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            java.lang.String r3 = r8.a(r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1.bindString(r2, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2 = 5
            android.content.Context r3 = r6.c     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            java.lang.String r3 = r8.b(r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1.bindString(r2, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            goto L_0x003f
        L_0x0155:
            r1 = move-exception
            com.agilebinary.mobilemonitor.client.android.c.p r2 = new com.agilebinary.mobilemonitor.client.android.c.p     // Catch:{ all -> 0x0109 }
            r2.<init>(r1)     // Catch:{ all -> 0x0109 }
            throw r2     // Catch:{ all -> 0x0109 }
        L_0x015c:
            com.agilebinary.mobilemonitor.client.a.b.r r8 = (com.agilebinary.mobilemonitor.client.a.b.r) r8     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 4
            android.content.Context r3 = r6.c     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            java.lang.String r3 = r8.a(r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 5
            android.content.Context r3 = r6.c     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            java.lang.String r3 = r8.b(r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            byte[] r1 = r8.w()     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            if (r1 != 0) goto L_0x017f
            r1 = 6
            r2.bindNull(r1)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = r2
            goto L_0x003f
        L_0x017f:
            r3 = 6
            r2.bindBlob(r3, r1)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = r2
            goto L_0x003f
        L_0x0186:
            boolean r1 = r8 instanceof com.agilebinary.mobilemonitor.client.a.b.v     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            if (r1 == 0) goto L_0x01a2
            r0 = r8
            com.agilebinary.mobilemonitor.client.a.b.v r0 = (com.agilebinary.mobilemonitor.client.a.b.v) r0     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = r0
            r3 = 4
            android.content.Context r4 = r6.c     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            java.lang.String r4 = r1.a(r4)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindString(r3, r4)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r3 = 5
            android.content.Context r4 = r6.c     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            java.lang.String r1 = r1.b(r4)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindString(r3, r1)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
        L_0x01a2:
            boolean r1 = r8 instanceof com.agilebinary.mobilemonitor.client.a.b.w     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            if (r1 == 0) goto L_0x003e
            com.agilebinary.mobilemonitor.client.a.b.w r8 = (com.agilebinary.mobilemonitor.client.a.b.w) r8     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 4
            android.content.Context r3 = r6.c     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            java.lang.String r3 = r8.a(r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 5
            android.content.Context r3 = r6.c     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            java.lang.String r3 = r8.b(r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = r2
            goto L_0x003f
        L_0x01bf:
            com.agilebinary.mobilemonitor.client.a.b.u r8 = (com.agilebinary.mobilemonitor.client.a.b.u) r8     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 4
            android.content.Context r3 = r6.c     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            java.lang.String r3 = r8.a(r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = r2
            goto L_0x003f
        L_0x01ce:
            com.agilebinary.mobilemonitor.client.a.b.a r8 = (com.agilebinary.mobilemonitor.client.a.b.a) r8     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 4
            android.content.Context r3 = r6.c     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            java.lang.String r3 = r8.a(r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 5
            android.content.Context r3 = r6.c     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            java.lang.String r3 = r8.b(r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 6
            int r3 = r8.b()     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            long r3 = (long) r3     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindLong(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = r2
            goto L_0x003f
        L_0x01f0:
            com.agilebinary.mobilemonitor.client.a.b.i r8 = (com.agilebinary.mobilemonitor.client.a.b.i) r8     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 4
            android.content.Context r3 = r6.c     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            java.lang.String r3 = r8.a(r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 5
            android.content.Context r3 = r6.c     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            java.lang.String r3 = r8.b(r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 6
            int r3 = r8.a()     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            long r3 = (long) r3     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindLong(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = r2
            goto L_0x003f
        L_0x0212:
            com.agilebinary.mobilemonitor.client.a.b.x r8 = (com.agilebinary.mobilemonitor.client.a.b.x) r8     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 4
            android.content.Context r3 = r6.c     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            java.lang.String r3 = r8.b(r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 5
            android.content.Context r3 = r6.c     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            java.lang.String r3 = r8.c(r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 6
            int r3 = r8.a()     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            long r3 = (long) r3     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindLong(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = r2
            goto L_0x003f
        L_0x0234:
            com.agilebinary.mobilemonitor.client.a.b.j r8 = (com.agilebinary.mobilemonitor.client.a.b.j) r8     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 4
            android.content.Context r3 = r6.c     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            java.lang.String r3 = r8.b(r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 5
            android.content.Context r3 = r6.c     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            java.lang.String r3 = r8.c(r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindString(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r1 = 6
            int r3 = r8.a()     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            long r3 = (long) r3     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            r2.bindLong(r1, r3)     // Catch:{ Exception -> 0x0103, OutOfMemoryError -> 0x0155 }
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.c.c.a(byte, com.agilebinary.mobilemonitor.client.a.b.b):void");
    }

    public synchronized void a(String str, byte b2, long j2, long j3, String str2, boolean z2, boolean z3, int i2, InputStream inputStream) {
        b.a(f175a, "onEvent...", str, " type=" + ((int) b2), " id=" + j2);
        if (this.q == -1) {
            b.b(f175a, "onEvent...mCurrentEventTypeDownload already -1, ignoring this call");
        } else {
            try {
                com.agilebinary.mobilemonitor.client.a.b.b a2 = com.agilebinary.mobilemonitor.client.a.a.a(str, b2, j2, j3, str2, z2, z3, i2, inputStream, MyApplication.g(), MyApplication.f139a, this.c, this.z);
                if (a2 != null) {
                    if (this.q != 1) {
                        a(b2, a2);
                    } else if (!(a2 instanceof com.agilebinary.mobilemonitor.client.a.c) || !((com.agilebinary.mobilemonitor.client.a.c) a2).a()) {
                        a(b2, a2);
                    } else if (this.u.a((com.agilebinary.mobilemonitor.client.a.c) a2)) {
                        a(b2, a2);
                    } else {
                        this.t.add((com.agilebinary.mobilemonitor.client.a.c) a2);
                    }
                }
            } catch (IOException e2) {
                e2.printStackTrace();
            } catch (OutOfMemoryError e3) {
                throw new p(e3);
            }
            b.b(f175a, "...onEvent");
        }
        return;
    }

    public synchronized void a(boolean z2) {
        b.a(f175a, "onEndEvents..., aborted=", "" + z2);
        this.z = null;
        if (this.q == -1) {
            b.b(f175a, "onEndEvents...mCurrentEventTypeDownload already -1, ignoring this call");
        } else {
            if (!z2) {
                if (this.q == 1) {
                    b.b(f175a, "onEndEvents, resolving locations....");
                    this.u.a(this.t);
                    b.b(f175a, "...onEndEvents, resolving locations");
                    b.b(f175a, "onEndEvents, storing resolved locations ...");
                    for (com.agilebinary.mobilemonitor.client.a.c cVar : this.t) {
                        try {
                            a(this.q, (e) cVar);
                        } catch (p e2) {
                            e2.printStackTrace();
                        }
                    }
                    this.t.clear();
                    b.b(f175a, "... onEndEvents, storing resolved locations");
                }
            }
            this.q = -1;
            if (!z2) {
            }
            this.t = null;
            this.u.b();
            b.b(f175a, "...onEndEvents");
        }
    }

    public synchronized boolean a(byte b2, Calendar calendar) {
        boolean z2 = true;
        synchronized (this) {
            b.a(f175a, "isCurrentDay, et=", "" + ((int) b2), " c=" + calendar);
            this.p.bindLong(1, (long) b2);
            try {
                if (this.p.simpleQueryForLong() != ((long) a(calendar))) {
                    z2 = false;
                }
            } catch (SQLiteDoneException e2) {
                z2 = false;
            }
            b.a(f175a, "...isCurrentDay", "=" + z2);
        }
        return z2;
    }

    public synchronized long b(byte b2) {
        long j2;
        b.a(f175a, "getTimeOfLastssenEvent, et=", "" + ((int) b2));
        this.y.bindLong(1, (long) b2);
        j2 = -1;
        try {
            j2 = this.y.simpleQueryForLong();
        } catch (SQLiteDoneException e2) {
        }
        b.a(f175a, "...getTimeOfLastssenEvent", " =" + j2);
        return j2;
    }

    public void b() {
        a(this.d, "location");
        a(this.d, "call");
        a(this.d, "sms");
        a(this.d, "mms");
        a(this.d, "web");
        a(this.d, "syst_m");
        a(this.d, e.f);
        a(this.d, g.f);
        a(this.d, n.f);
        a(this.d, h.f);
        a(this.d, "lastseen");
        a(this.d, "curday");
        a(this.d, "oldestevent");
    }

    public synchronized void b(byte b2, long j2) {
        b.a(f175a, "setOldestKnownEventTs, et=", "" + ((int) b2), " ts=" + j2);
        this.s.bindLong(1, (long) b2);
        this.s.bindLong(2, j2);
        this.s.executeInsert();
    }

    public synchronized void b(byte b2, Calendar calendar) {
        b.a(f175a, "setCurrentDay, et=", "" + ((int) b2), " c=" + calendar);
        this.v = calendar;
        int a2 = a(calendar);
        this.o.bindLong(1, (long) b2);
        this.o.bindLong(2, (long) a2);
        this.o.executeInsert();
    }

    public void c() {
        this.u.c();
    }

    public void c(byte b2) {
        a(this.d, f(b2));
    }

    public synchronized void c(byte b2, long j2) {
        b.a(f175a, "setLastssenEventTs, et=", "" + ((int) b2), " ts=" + j2 + " (" + new Date(j2) + ")");
        if (j2 > b(b2)) {
            b.b(f175a, "updateLastssenEventTs = " + j2);
            this.x.bindLong(1, (long) b2);
            this.x.bindLong(2, j2);
            this.x.executeInsert();
        }
        b.b(f175a, "...setLastssenEventTs");
    }

    public com.agilebinary.mobilemonitor.client.a.b.b d(byte b2) {
        SQLiteStatement compileStatement = this.d.compileStatement(String.format("SELECT min(%2$s) FROM %1$s  ", f(b2), d.f176a));
        try {
            long simpleQueryForLong = compileStatement.simpleQueryForLong();
            compileStatement.close();
            return h(b2, simpleQueryForLong);
        } catch (Exception e2) {
            b.e(f175a, "getFirstEvent", e2);
            compileStatement.close();
            return null;
        }
    }

    public Calendar d() {
        b.a(f175a, "getLastSetDate", "" + this.v);
        return this.v;
    }

    public boolean d(byte b2, long j2) {
        SQLiteStatement compileStatement = this.d.compileStatement(String.format("SELECT count(*) FROM %1$s where %2$s<? ", f(b2), d.f176a));
        compileStatement.bindLong(1, j2);
        try {
            long simpleQueryForLong = compileStatement.simpleQueryForLong();
            compileStatement.close();
            return simpleQueryForLong > 0;
        } catch (Exception e2) {
            b.e(f175a, "hasPreviousEvent", e2);
            compileStatement.close();
            return false;
        }
    }

    public com.agilebinary.mobilemonitor.client.a.b.b e(byte b2) {
        SQLiteStatement compileStatement = this.d.compileStatement(String.format("SELECT max(%2$s) FROM %1$s  ", f(b2), d.f176a));
        try {
            long simpleQueryForLong = compileStatement.simpleQueryForLong();
            compileStatement.close();
            return h(b2, simpleQueryForLong);
        } catch (Exception e2) {
            b.e(f175a, "getLastEvent", e2);
            compileStatement.close();
            return null;
        }
    }

    public com.agilebinary.mobilemonitor.client.a.b.b e(byte b2, long j2) {
        SQLiteStatement compileStatement = this.d.compileStatement(String.format("SELECT max(%2$s) FROM %1$s where %2$s<? ", f(b2), d.f176a));
        compileStatement.bindLong(1, j2);
        try {
            long simpleQueryForLong = compileStatement.simpleQueryForLong();
            compileStatement.close();
            return h(b2, simpleQueryForLong);
        } catch (Exception e2) {
            b.e(f175a, "getPreviousEvent", e2);
            compileStatement.close();
            return null;
        }
    }

    public void e() {
        b.b(f175a, "onLowMemory...");
        SQLiteDatabase sQLiteDatabase = this.d;
        SQLiteDatabase.releaseMemory();
    }

    public boolean f(byte b2, long j2) {
        SQLiteStatement compileStatement = this.d.compileStatement(String.format("SELECT count(*) FROM %1$s where %2$s>? ", f(b2), d.f176a));
        compileStatement.bindLong(1, j2);
        try {
            long simpleQueryForLong = compileStatement.simpleQueryForLong();
            compileStatement.close();
            return simpleQueryForLong > 0;
        } catch (Exception e2) {
            b.e(f175a, "hasNextEvent", e2);
            compileStatement.close();
            return false;
        }
    }

    public com.agilebinary.mobilemonitor.client.a.b.b g(byte b2, long j2) {
        SQLiteStatement compileStatement = this.d.compileStatement(String.format("SELECT min(%2$s) FROM %1$s where %2$s>? ", f(b2), d.f176a));
        compileStatement.bindLong(1, j2);
        try {
            long simpleQueryForLong = compileStatement.simpleQueryForLong();
            compileStatement.close();
            return h(b2, simpleQueryForLong);
        } catch (Exception e2) {
            b.e(f175a, "getNextEvent", e2);
            compileStatement.close();
            return null;
        }
    }

    public com.agilebinary.mobilemonitor.client.a.b.b h(byte b2, long j2) {
        Cursor query = this.d.query(f(b2), d.e, d.f176a + "=?", new String[]{String.valueOf(j2)}, null, null, null);
        if (!query.moveToFirst()) {
            query.close();
            return null;
        }
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(query.getBlob(0));
            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            com.agilebinary.mobilemonitor.client.a.b.b bVar = (com.agilebinary.mobilemonitor.client.a.b.b) objectInputStream.readObject();
            objectInputStream.close();
            byteArrayInputStream.close();
            query.close();
            return bVar;
        } catch (StreamCorruptedException e2) {
            b.e(f175a, "getEvent", e2);
            query.close();
            return null;
        } catch (OptionalDataException e3) {
            b.e(f175a, "getEvent", e3);
            query.close();
            return null;
        } catch (IOException e4) {
            b.e(f175a, "getEvent", e4);
            query.close();
            return null;
        } catch (ClassNotFoundException e5) {
            b.e(f175a, "getEvent", e5);
            query.close();
            return null;
        } catch (OutOfMemoryError e6) {
            b.e(f175a, "getEvent", e6);
            query.close();
            return null;
        }
    }
}
