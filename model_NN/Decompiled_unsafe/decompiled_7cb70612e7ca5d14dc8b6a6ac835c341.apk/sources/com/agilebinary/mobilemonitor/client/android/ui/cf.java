package com.agilebinary.mobilemonitor.client.android.ui;

import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.agilebinary.phonebeagle.client.R;

public class cf extends ck {

    /* renamed from: a  reason: collision with root package name */
    protected TextView f288a;
    protected TextView b;
    protected ImageView c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.agilebinary.mobilemonitor.client.android.ui.cf, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public cf(bq bqVar, AttributeSet attributeSet) {
        super(bqVar, attributeSet);
        ((LayoutInflater) bqVar.getSystemService("layout_inflater")).inflate((int) R.layout.eventlist_rowview_mms, (ViewGroup) this, true);
        a();
    }

    /* access modifiers changed from: protected */
    public void a() {
        super.a();
        this.b = (TextView) findViewById(R.id.eventlist_rowview_mms_text);
        this.f288a = (TextView) findViewById(R.id.eventlist_rowview_mms_dir);
        this.c = (ImageView) findViewById(R.id.eventlist_rowview_mms_thumb);
    }

    public void a(Cursor cursor) {
        int i = 0;
        super.a(cursor);
        this.b.setText(cursor.getString(cursor.getColumnIndex("text")));
        this.f288a.setText(cursor.getString(cursor.getColumnIndex("dir")));
        byte[] blob = cursor.getBlob(cursor.getColumnIndex("thumbnail"));
        if (blob != null) {
            try {
                this.c.setImageBitmap(BitmapFactory.decodeByteArray(blob, 0, blob.length));
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
            }
        }
        ImageView imageView = this.c;
        if (blob == null) {
            i = 8;
        }
        imageView.setVisibility(i);
    }
}
