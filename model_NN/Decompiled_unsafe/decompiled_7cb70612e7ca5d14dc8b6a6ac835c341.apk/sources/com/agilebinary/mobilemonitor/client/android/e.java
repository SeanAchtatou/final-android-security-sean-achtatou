package com.agilebinary.mobilemonitor.client.android;

public class e {

    /* renamed from: a  reason: collision with root package name */
    public byte f184a;
    public long b;
    public long c;
    public boolean d;

    public e(byte b2, long j, long j2) {
        this.f184a = b2;
        this.b = j;
        this.c = j2;
        this.d = j2 == 0;
    }

    public String toString() {
        return "EventContentRequest [eventType=" + ((int) this.f184a) + ", timeFrom=" + this.b + ", timeTo=" + this.c + ", onlyNewEvents=" + this.d + "]";
    }
}
