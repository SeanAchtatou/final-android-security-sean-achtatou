package com.agilebinary.mobilemonitor.client.a.b;

import com.agilebinary.mobilemonitor.client.a.a.a;
import com.agilebinary.mobilemonitor.client.a.b;

public abstract class g extends b {
    protected boolean j;
    protected double k;
    protected double l;
    protected boolean m;
    protected double n;
    protected boolean o;
    protected double p;
    protected double q;
    protected boolean r;
    protected double s;
    protected boolean t;
    protected double u;
    protected long v;

    public g(String str, long j2, long j3, a aVar, b bVar) {
        super(str, j2, j3, aVar, bVar);
        this.j = aVar.a();
        if (this.j) {
            this.k = aVar.e();
            this.l = aVar.e();
            this.m = aVar.a();
            if (this.m) {
                this.n = aVar.e();
            }
            this.o = aVar.a();
            if (this.o) {
                this.p = aVar.e();
                this.q = aVar.e();
            }
            this.r = aVar.a();
            if (this.r) {
                this.s = aVar.e();
            }
            this.t = aVar.a();
            if (this.t) {
                this.u = aVar.e();
            }
            this.v = bVar.a(aVar.d());
        }
    }

    public boolean i() {
        return this.j;
    }

    public double j() {
        return this.s;
    }

    public long k() {
        return this.v;
    }

    public boolean l() {
        return this.r;
    }
}
