package com.agilebinary.mobilemonitor.client.android.b.a;

import com.agilebinary.mobilemonitor.client.android.b.l;
import com.agilebinary.mobilemonitor.client.android.b.m;
import com.agilebinary.mobilemonitor.client.android.b.s;
import com.agilebinary.mobilemonitor.client.android.f;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

public abstract class b extends a implements l {
    public abstract InputStream a(m mVar);

    public List a(f fVar, List list, m mVar) {
        try {
            XMLReader xMLReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();
            ArrayList arrayList = new ArrayList();
            xMLReader.setContentHandler(new c(this, arrayList));
            xMLReader.parse(new InputSource(a(mVar)));
            return arrayList;
        } catch (Exception e) {
            throw new s(e);
        }
    }
}
