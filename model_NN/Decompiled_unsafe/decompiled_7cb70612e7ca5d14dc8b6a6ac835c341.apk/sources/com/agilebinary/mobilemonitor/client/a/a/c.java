package com.agilebinary.mobilemonitor.client.a.a;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class c extends FilterInputStream {

    /* renamed from: a  reason: collision with root package name */
    protected volatile byte[] f125a;
    protected int b;
    protected int c;
    protected int d = -1;
    protected int e;
    private boolean f = false;

    public c(InputStream inputStream, byte[] bArr) {
        super(inputStream);
        this.f125a = bArr;
    }

    private int a() {
        int i;
        int i2 = 0;
        if (this.d == -1 || this.e - this.d >= this.c) {
            i = this.in.read(this.f125a);
            if (i > 0) {
                this.d = -1;
                this.e = 0;
                if (i != -1) {
                    i2 = i;
                }
                this.b = i2;
            }
        } else {
            if (this.d == 0 && this.c > this.f125a.length) {
                int length = this.f125a.length * 2;
                if (length > this.c) {
                    length = this.c;
                }
                byte[] bArr = new byte[length];
                System.arraycopy(this.f125a, 0, bArr, 0, this.f125a.length);
                this.f125a = bArr;
            } else if (this.d > 0) {
                System.arraycopy(this.f125a, this.d, this.f125a, 0, this.f125a.length - this.d);
            }
            this.e -= this.d;
            this.d = 0;
            this.b = 0;
            i = this.in.read(this.f125a, this.e, this.f125a.length - this.e);
            this.b = i <= 0 ? this.e : this.e + i;
        }
        return i;
    }

    public synchronized int available() {
        if (this.f125a == null) {
            throw new IOException("Stream is closed");
        }
        return (this.b - this.e) + this.in.available();
    }

    public synchronized void close() {
        if (this.in != null) {
            super.close();
            this.in = null;
        }
        this.f125a = null;
        this.f = true;
    }

    public synchronized void mark(int i) {
        this.c = i;
        this.d = this.e;
    }

    public boolean markSupported() {
        return true;
    }

    public synchronized int read() {
        byte b2 = -1;
        synchronized (this) {
            if (this.in == null) {
                throw new IOException("Stream is closed");
            } else if (this.e < this.b || a() != -1) {
                if (this.b - this.e > 0) {
                    byte[] bArr = this.f125a;
                    int i = this.e;
                    this.e = i + 1;
                    b2 = bArr[i] & 255;
                }
            }
        }
        return b2;
    }

    public synchronized int read(byte[] bArr, int i, int i2) {
        int i3;
        int i4;
        int i5 = -1;
        synchronized (this) {
            if (this.f) {
                throw new IOException("Stream is closed");
            } else if (bArr == null) {
                throw new NullPointerException("K0047");
            } else if ((i | i2) < 0 || i > bArr.length - i2) {
                throw new IndexOutOfBoundsException("K002f");
            } else if (i2 == 0) {
                i5 = 0;
            } else if (this.f125a == null) {
                throw new IOException("K0059");
            } else {
                if (this.e < this.b) {
                    int i6 = this.b - this.e >= i2 ? i2 : this.b - this.e;
                    System.arraycopy(this.f125a, this.e, bArr, i, i6);
                    this.e += i6;
                    if (i6 == i2 || this.in.available() == 0) {
                        i5 = i6;
                    } else {
                        i += i6;
                        i3 = i2 - i6;
                    }
                } else {
                    i3 = i2;
                }
                while (true) {
                    if (this.d == -1 && i3 >= this.f125a.length) {
                        i4 = this.in.read(bArr, i, i3);
                        if (i4 == -1) {
                            if (i3 != i2) {
                                i5 = i2 - i3;
                            }
                        }
                    } else if (a() != -1) {
                        i4 = this.b - this.e >= i3 ? i3 : this.b - this.e;
                        System.arraycopy(this.f125a, this.e, bArr, i, i4);
                        this.e += i4;
                    } else if (i3 != i2) {
                        i5 = i2 - i3;
                    }
                    i3 -= i4;
                    if (i3 == 0) {
                        i5 = i2;
                        break;
                    } else if (this.in.available() == 0) {
                        i5 = i2 - i3;
                        break;
                    } else {
                        i += i4;
                    }
                }
            }
        }
        return i5;
    }

    public synchronized void reset() {
        if (this.f) {
            throw new IOException("Stream is closed");
        } else if (-1 == this.d) {
            throw new IOException("Mark has been invalidated.");
        } else {
            this.e = this.d;
        }
    }

    public synchronized long skip(long j) {
        if (this.in == null) {
            throw new IOException("K0059");
        } else if (j < 1) {
            j = 0;
        } else if (((long) (this.b - this.e)) >= j) {
            this.e = (int) (((long) this.e) + j);
        } else {
            long j2 = (long) (this.b - this.e);
            this.e = this.b;
            if (this.d != -1) {
                if (j > ((long) this.c)) {
                    this.d = -1;
                } else if (a() == -1) {
                    j = j2;
                } else if (((long) (this.b - this.e)) >= j - j2) {
                    this.e = (int) ((j - j2) + ((long) this.e));
                } else {
                    j = j2 + ((long) (this.b - this.e));
                    this.e = this.b;
                }
            }
            j = j2 + this.in.skip(j - j2);
        }
        return j;
    }
}
