package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.android.b.x;
import com.agilebinary.phonebeagle.client.R;

public class AboutActivity extends ah {

    /* renamed from: a  reason: collision with root package name */
    private TextView f188a;
    private TextView b;
    private TextView h;

    public static void a(Context context) {
        context.startActivity(new Intent(context, AboutActivity.class));
    }

    /* access modifiers changed from: protected */
    public int a() {
        return R.layout.about;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.h = (TextView) findViewById(R.id.about_version);
        this.h.setText(x.a().o());
        this.f188a = (TextView) findViewById(R.id.about_url);
        this.f188a.setMovementMethod(LinkMovementMethod.getInstance());
        this.f188a.setText(Html.fromHtml(getString(R.string.label_about_url, new Object[]{x.a().f()})));
        this.b = (TextView) findViewById(R.id.about_copyright);
        this.b.setText(getString(R.string.label_about_copyright, new Object[]{x.a().j()}));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }
}
