package com.agilebinary.mobilemonitor.client.android.ui;

import android.database.Cursor;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import com.agilebinary.phonebeagle.client.R;

public class cb extends ck {

    /* renamed from: a  reason: collision with root package name */
    protected TextView f284a;
    protected TextView b;
    private int c = -1;
    private int g = -1;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.agilebinary.mobilemonitor.client.android.ui.cb, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public cb(bq bqVar, AttributeSet attributeSet) {
        super(bqVar, attributeSet);
        ((LayoutInflater) bqVar.getSystemService("layout_inflater")).inflate((int) R.layout.eventlist_rowview_call, (ViewGroup) this, true);
        a();
    }

    /* access modifiers changed from: protected */
    public void a() {
        super.a();
        this.f284a = (TextView) findViewById(R.id.eventlist_rowview_call_line1);
        this.b = (TextView) findViewById(R.id.eventlist_rowview_call_line2);
    }

    public void a(Cursor cursor) {
        super.a(cursor);
        if (this.c < 0) {
            this.c = cursor.getColumnIndex("line1");
            this.g = cursor.getColumnIndex("line2");
        }
        this.f284a.setText(cursor.getString(this.c));
        this.b.setText(cursor.getString(this.g));
    }
}
