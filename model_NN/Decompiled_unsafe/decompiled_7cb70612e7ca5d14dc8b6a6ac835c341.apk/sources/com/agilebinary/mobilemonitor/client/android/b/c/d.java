package com.agilebinary.mobilemonitor.client.android.b.c;

import com.agilebinary.mobilemonitor.client.android.d.f;
import java.io.Serializable;
import java.util.logging.Logger;

public class d implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private static final String f160a = f.a("GeocodeGsmCell");
    private static final Logger b = Logger.getLogger(d.class.getName());
    private int c;
    private int d;
    private int e;
    private int f;

    public d() {
    }

    public d(int i, int i2, int i3, int i4) {
        this.c = i;
        this.d = i2;
        this.e = i3;
        this.f = i4;
    }

    public String a() {
        return "{\"mobile_country_code\":\"" + this.c + "\", \"" + "mobile_network_code" + "\":\"" + this.d + "\", \"" + "cell_id" + "\":\"" + this.e + "\", \"" + "location_area_code" + "\":\"" + this.f + "\"}";
    }

    public void a(int i) {
        this.c = i;
    }

    public int b() {
        return this.c;
    }

    public void b(int i) {
        this.d = i;
    }

    public int c() {
        return this.d;
    }

    public void c(int i) {
        this.e = i;
    }

    public int d() {
        return this.e;
    }

    public void d(int i) {
        this.f = i;
    }

    public int e() {
        return this.f;
    }

    public String toString() {
        return "GeocodeGsmCell: " + a();
    }
}
