package com.agilebinary.mobilemonitor.client.android.ui.a;

import android.os.AsyncTask;
import android.os.PowerManager;
import com.agilebinary.a.a.b.b.a.g;
import com.agilebinary.mobilemonitor.client.android.MyApplication;
import com.agilebinary.mobilemonitor.client.android.b.s;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.mobilemonitor.client.android.ui.LoginActivity;
import com.agilebinary.mobilemonitor.client.android.ui.ah;

public class m extends AsyncTask implements com.agilebinary.mobilemonitor.client.android.b.m {

    /* renamed from: a  reason: collision with root package name */
    static final String f216a = f.a("LoginTask");
    public static m b;
    private ah c;
    private MyApplication d;
    private g e;

    public m(ah ahVar) {
        this.c = ahVar;
        this.d = ahVar.g;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public o doInBackground(String... strArr) {
        o oVar;
        PowerManager.WakeLock newWakeLock = ((PowerManager) this.c.getSystemService("power")).newWakeLock(6, f216a);
        newWakeLock.acquire();
        try {
            oVar = new o(this.d.a(strArr[0], strArr[1], this));
            try {
                newWakeLock.release();
            } catch (Exception e2) {
            }
            b = null;
        } catch (s e3) {
            oVar = new o(e3);
            try {
                newWakeLock.release();
            } catch (Exception e4) {
            }
            b = null;
        } catch (Throwable th) {
            try {
                newWakeLock.release();
            } catch (Exception e5) {
            }
            b = null;
            throw th;
        }
        return oVar;
    }

    public void a() {
        cancel(false);
        if (this.e != null) {
            try {
                this.e.i();
            } catch (Exception e2) {
            }
        }
    }

    public void a(g gVar) {
        this.e = gVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(o oVar) {
        super.onPostExecute(oVar);
        LoginActivity.a(this.c, oVar);
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        LoginActivity.a(this.c, (o) null);
    }
}
