package com.agilebinary.mobilemonitor.client.android.ui.a;

import com.agilebinary.mobilemonitor.client.android.b.s;
import java.io.Serializable;

public class o implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private boolean f218a;
    private s b;

    public o(s sVar) {
        this.b = sVar;
    }

    public o(boolean z) {
        this.f218a = z;
    }

    public boolean a() {
        return this.f218a;
    }

    public s b() {
        return this.b;
    }
}
