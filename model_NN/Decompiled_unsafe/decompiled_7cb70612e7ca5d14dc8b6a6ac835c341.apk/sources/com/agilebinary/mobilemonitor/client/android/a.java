package com.agilebinary.mobilemonitor.client.android;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import com.agilebinary.a.a.b.b.a.g;
import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.b.c.c;
import com.agilebinary.mobilemonitor.client.android.b.c.d;
import com.agilebinary.mobilemonitor.client.android.b.c.e;
import com.agilebinary.mobilemonitor.client.android.b.k;
import com.agilebinary.mobilemonitor.client.android.b.l;
import com.agilebinary.mobilemonitor.client.android.b.m;
import com.agilebinary.mobilemonitor.client.android.d.f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class a implements m {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f140a = f.a("LocationResolver");
    private boolean b;
    private l c;
    private k d;
    private MyApplication e;
    private SQLiteStatement f;
    private SQLiteStatement g;
    private SQLiteDatabase h;
    private g i;
    private Map j = new HashMap();
    private String[] k;

    public a(MyApplication myApplication) {
        this.e = myApplication;
        this.c = myApplication.b().e();
        this.d = myApplication.b().f();
    }

    private String a(com.agilebinary.mobilemonitor.client.a.b.l lVar) {
        return lVar.k() + "-" + lVar.l() + "-" + lVar.m();
    }

    private String a(com.agilebinary.mobilemonitor.client.a.b.m mVar) {
        return mVar.m() + "-" + mVar.n() + "-" + mVar.k() + "-" + mVar.l();
    }

    private void a(c cVar, com.agilebinary.mobilemonitor.client.android.b.c.f fVar) {
        b.a(f140a, "storeLocation CDMA ...", "" + fVar);
        this.g.bindLong(1, System.currentTimeMillis());
        this.g.bindLong(2, this.e.d() ? 1 : 0);
        this.g.bindLong(3, (long) cVar.c());
        this.g.bindLong(4, (long) cVar.b());
        this.g.bindLong(5, (long) cVar.d());
        this.g.bindDouble(6, fVar.b());
        this.g.bindDouble(7, fVar.c());
        this.g.bindDouble(8, fVar.d());
        this.g.executeInsert();
    }

    private void a(e eVar, com.agilebinary.mobilemonitor.client.android.b.c.f fVar) {
        b.a(f140a, "storeLocation GSM ...", "" + fVar);
        this.f.bindLong(1, System.currentTimeMillis());
        this.f.bindLong(2, this.e.d() ? 1 : 0);
        this.f.bindLong(3, (long) eVar.b());
        this.f.bindLong(4, (long) eVar.c());
        this.f.bindLong(5, (long) eVar.d());
        this.f.bindLong(6, (long) eVar.e());
        this.f.bindDouble(7, fVar.b());
        this.f.bindDouble(8, fVar.c());
        this.f.bindDouble(9, fVar.d());
        this.f.executeInsert();
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x0123  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.agilebinary.mobilemonitor.client.android.b.c.f b(com.agilebinary.mobilemonitor.client.a.b.l r15) {
        /*
            r14 = this;
            java.lang.String r0 = com.agilebinary.mobilemonitor.client.android.a.f140a
            java.lang.String r1 = "getLocation CDMA ..."
            com.agilebinary.mobilemonitor.a.a.a.b.b(r0, r1)
            java.lang.String r13 = r14.a(r15)
            java.util.Map r0 = r14.j
            java.lang.Object r0 = r0.get(r13)
            com.agilebinary.mobilemonitor.client.android.b.c.f r0 = (com.agilebinary.mobilemonitor.client.android.b.c.f) r0
            if (r0 != 0) goto L_0x00ff
            r8 = 0
            r0 = 4
            java.lang.String[] r1 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            r2 = 0
            com.agilebinary.mobilemonitor.client.android.MyApplication r0 = r14.e     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            boolean r0 = r0.d()     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            if (r0 == 0) goto L_0x0111
            java.lang.String r0 = "1"
        L_0x0024:
            r1[r2] = r0     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            r0 = 1
            int r2 = r15.k()     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            r1[r0] = r2     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            r0 = 2
            int r2 = r15.l()     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            r1[r0] = r2     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            r0 = 3
            int r2 = r15.m()     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String r2 = java.lang.String.valueOf(r2)     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            r1[r0] = r2     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            r14.k = r1     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String r0 = com.agilebinary.mobilemonitor.client.android.a.f140a     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            r1.<init>()     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String r2 = "querying CDMA table, demo="
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String[] r2 = r14.k     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            r3 = 0
            r2 = r2[r3]     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String r2 = " bssid="
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String[] r2 = r14.k     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            r3 = 1
            r2 = r2[r3]     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String r2 = " nid="
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String[] r2 = r14.k     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            r3 = 2
            r2 = r2[r3]     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String r2 = " sysid="
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String[] r2 = r14.k     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            r3 = 3
            r2 = r2[r3]     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String r1 = r1.toString()     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            com.agilebinary.mobilemonitor.a.a.a.b.b(r0, r1)     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            android.database.sqlite.SQLiteDatabase r0 = r14.h     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String r1 = com.agilebinary.mobilemonitor.client.android.b.f141a     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String[] r2 = com.agilebinary.mobilemonitor.client.android.b.j     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            r3.<init>()     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.b.i     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String r4 = "=? and "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.b.c     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String r4 = "=? and "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.b.d     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String r4 = "=? and "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.b.e     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String r4 = "=?"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            java.lang.String[] r4 = r14.k     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r12 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0117, all -> 0x0127 }
            boolean r0 = r12.moveToNext()     // Catch:{ SQLiteException -> 0x012c, all -> 0x0129 }
            if (r0 == 0) goto L_0x0115
            com.agilebinary.mobilemonitor.client.android.b.c.f r0 = new com.agilebinary.mobilemonitor.client.android.b.c.f     // Catch:{ SQLiteException -> 0x012c, all -> 0x0129 }
            r1 = 0
            double r1 = r12.getDouble(r1)     // Catch:{ SQLiteException -> 0x012c, all -> 0x0129 }
            r3 = 1
            double r3 = r12.getDouble(r3)     // Catch:{ SQLiteException -> 0x012c, all -> 0x0129 }
            r5 = 2
            double r5 = r12.getDouble(r5)     // Catch:{ SQLiteException -> 0x012c, all -> 0x0129 }
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r0.<init>(r1, r3, r5, r7, r8, r9, r10, r11)     // Catch:{ SQLiteException -> 0x012c, all -> 0x0129 }
        L_0x00fa:
            if (r12 == 0) goto L_0x00ff
            r12.close()
        L_0x00ff:
            if (r0 == 0) goto L_0x0106
            java.util.Map r1 = r14.j
            r1.put(r13, r0)
        L_0x0106:
            r1 = 0
            r14.i = r1
            java.lang.String r1 = com.agilebinary.mobilemonitor.client.android.a.f140a
            java.lang.String r2 = "...getLocation CDMA"
            com.agilebinary.mobilemonitor.a.a.a.b.b(r1, r2)
            return r0
        L_0x0111:
            java.lang.String r0 = "0"
            goto L_0x0024
        L_0x0115:
            r0 = 0
            goto L_0x00fa
        L_0x0117:
            r0 = move-exception
            r1 = r8
        L_0x0119:
            java.lang.Exception r2 = new java.lang.Exception     // Catch:{ all -> 0x011f }
            r2.<init>(r0)     // Catch:{ all -> 0x011f }
            throw r2     // Catch:{ all -> 0x011f }
        L_0x011f:
            r0 = move-exception
            r8 = r1
        L_0x0121:
            if (r8 == 0) goto L_0x0126
            r8.close()
        L_0x0126:
            throw r0
        L_0x0127:
            r0 = move-exception
            goto L_0x0121
        L_0x0129:
            r0 = move-exception
            r8 = r12
            goto L_0x0121
        L_0x012c:
            r0 = move-exception
            r1 = r12
            goto L_0x0119
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.a.b(com.agilebinary.mobilemonitor.client.a.b.l):com.agilebinary.mobilemonitor.client.android.b.c.f");
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x011d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.agilebinary.mobilemonitor.client.android.b.c.f b(com.agilebinary.mobilemonitor.client.a.b.m r15) {
        /*
            r14 = this;
            java.lang.String r0 = com.agilebinary.mobilemonitor.client.android.a.f140a
            java.lang.String r1 = "getLocation GSM ..."
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            int r3 = r15.n()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r3 = " / "
            java.lang.StringBuilder r2 = r2.append(r3)
            int r3 = r15.k()
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.agilebinary.mobilemonitor.a.a.a.b.a(r0, r1, r2)
            java.lang.String r13 = r14.a(r15)
            java.util.Map r0 = r14.j
            java.lang.Object r0 = r0.get(r13)
            com.agilebinary.mobilemonitor.client.android.b.c.f r0 = (com.agilebinary.mobilemonitor.client.android.b.c.f) r0
            if (r0 != 0) goto L_0x00e7
            r8 = 0
            android.database.sqlite.SQLiteDatabase r0 = r14.h     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            java.lang.String r1 = com.agilebinary.mobilemonitor.client.android.c.f173a     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            java.lang.String[] r2 = com.agilebinary.mobilemonitor.client.android.c.k     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            r3.<init>()     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.c.j     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            java.lang.String r4 = "=? and "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.c.c     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            java.lang.String r4 = "=? and "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.c.d     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            java.lang.String r4 = "=? and "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.c.e     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            java.lang.String r4 = "=? and "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            java.lang.String r4 = com.agilebinary.mobilemonitor.client.android.c.f     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            java.lang.String r4 = "=?"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            r4 = 5
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            r6 = 0
            com.agilebinary.mobilemonitor.client.android.MyApplication r5 = r14.e     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            boolean r5 = r5.d()     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            if (r5 == 0) goto L_0x010c
            java.lang.String r5 = "1"
        L_0x008e:
            r4[r6] = r5     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            r5 = 1
            int r6 = r15.m()     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            r5 = 2
            int r6 = r15.n()     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            r5 = 3
            int r6 = r15.k()     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            r5 = 4
            int r6 = r15.l()     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            r4[r5] = r6     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r12 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0111, all -> 0x0121 }
            boolean r0 = r12.moveToNext()     // Catch:{ SQLiteException -> 0x0126, all -> 0x0123 }
            if (r0 == 0) goto L_0x010f
            com.agilebinary.mobilemonitor.client.android.b.c.f r0 = new com.agilebinary.mobilemonitor.client.android.b.c.f     // Catch:{ SQLiteException -> 0x0126, all -> 0x0123 }
            r1 = 0
            double r1 = r12.getDouble(r1)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0123 }
            r3 = 1
            double r3 = r12.getDouble(r3)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0123 }
            r5 = 2
            double r5 = r12.getDouble(r5)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0123 }
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r0.<init>(r1, r3, r5, r7, r8, r9, r10, r11)     // Catch:{ SQLiteException -> 0x0126, all -> 0x0123 }
        L_0x00e2:
            if (r12 == 0) goto L_0x00e7
            r12.close()
        L_0x00e7:
            if (r0 == 0) goto L_0x00ee
            java.util.Map r1 = r14.j
            r1.put(r13, r0)
        L_0x00ee:
            r1 = 0
            r14.i = r1
            java.lang.String r1 = com.agilebinary.mobilemonitor.client.android.a.f140a
            java.lang.String r2 = "...getLocation GSM, res = "
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = ""
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r0)
            java.lang.String r3 = r3.toString()
            com.agilebinary.mobilemonitor.a.a.a.b.a(r1, r2, r3)
            return r0
        L_0x010c:
            java.lang.String r5 = "0"
            goto L_0x008e
        L_0x010f:
            r0 = 0
            goto L_0x00e2
        L_0x0111:
            r0 = move-exception
            r1 = r8
        L_0x0113:
            java.lang.Exception r2 = new java.lang.Exception     // Catch:{ all -> 0x0119 }
            r2.<init>(r0)     // Catch:{ all -> 0x0119 }
            throw r2     // Catch:{ all -> 0x0119 }
        L_0x0119:
            r0 = move-exception
            r8 = r1
        L_0x011b:
            if (r8 == 0) goto L_0x0120
            r8.close()
        L_0x0120:
            throw r0
        L_0x0121:
            r0 = move-exception
            goto L_0x011b
        L_0x0123:
            r0 = move-exception
            r8 = r12
            goto L_0x011b
        L_0x0126:
            r0 = move-exception
            r1 = r12
            goto L_0x0113
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.a.b(com.agilebinary.mobilemonitor.client.a.b.m):com.agilebinary.mobilemonitor.client.android.b.c.f");
    }

    public synchronized void a() {
        this.h = new d(this, this.e, "location_cache", null, 6).getWritableDatabase();
        b.b(f140a, "resolve, got database ");
        this.f = this.h.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s,%7$s,%8$s,%9$s,%10$s) values (?,?,?,?,?,?,?,?,?) ", c.f173a, c.b, c.j, c.c, c.d, c.e, c.f, c.g, c.h, c.i));
        this.g = this.h.compileStatement(String.format("INSERT INTO %1$s (%2$s,%3$s,%4$s,%5$s,%6$s,%7$s,%8$s,%9$s) values (?,?,?,?,?,?,?,?) ", b.f141a, b.b, b.i, b.d, b.c, b.e, b.f, b.g, b.h));
        SQLiteStatement compileStatement = this.h.compileStatement(String.format("DELETE FROM %1$s  where %2$s<?", c.f173a, c.b));
        SQLiteStatement compileStatement2 = this.h.compileStatement(String.format("DELETE FROM %1$s  where %2$s<?", b.f141a, b.b));
        compileStatement.bindLong(1, System.currentTimeMillis() - 1209600000);
        compileStatement.execute();
        compileStatement.close();
        compileStatement2.bindLong(1, System.currentTimeMillis() - 1209600000);
        compileStatement2.execute();
        compileStatement2.close();
        b.b(f140a, "resolve, deleted expired ");
    }

    public void a(g gVar) {
        this.i = gVar;
    }

    public synchronized void a(List list) {
        b.b(f140a, "resolve ...");
        this.c = this.e.b().e();
        this.b = false;
        try {
            ArrayList arrayList = new ArrayList();
            HashSet hashSet = new HashSet();
            HashSet<com.agilebinary.mobilemonitor.client.a.c> hashSet2 = new HashSet<>();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                com.agilebinary.mobilemonitor.client.a.c cVar = (com.agilebinary.mobilemonitor.client.a.c) it.next();
                if (cVar.a() && (cVar instanceof com.agilebinary.mobilemonitor.client.a.b.m)) {
                    com.agilebinary.mobilemonitor.client.a.b.m mVar = (com.agilebinary.mobilemonitor.client.a.b.m) cVar;
                    com.agilebinary.mobilemonitor.client.android.b.c.f b2 = b(mVar);
                    if (b2 == null) {
                        String a2 = a(mVar);
                        if (!hashSet.contains(a2)) {
                            arrayList.add(new d(mVar.m(), mVar.n(), mVar.k(), mVar.l()));
                            hashSet.add(a2);
                        }
                        hashSet2.add(cVar);
                    } else {
                        cVar.a(b2);
                    }
                }
            }
            if (arrayList.size() > 0) {
                List<e> a3 = this.c.a(this.e.a(), arrayList, this);
                if (!this.b && a3 != null) {
                    b.b(f140a, "resolve, storing GSM locations... ");
                    for (e eVar : a3) {
                        a(eVar, eVar.f());
                    }
                    b.b(f140a, "...resolve, storing GSM locations");
                    for (com.agilebinary.mobilemonitor.client.a.c cVar2 : hashSet2) {
                        if (cVar2 instanceof com.agilebinary.mobilemonitor.client.a.b.m) {
                            cVar2.a(b((com.agilebinary.mobilemonitor.client.a.b.m) cVar2));
                        }
                    }
                }
            }
            b.b(f140a, "...resolveing CDMA");
            ArrayList arrayList2 = new ArrayList();
            HashSet hashSet3 = new HashSet();
            HashSet<com.agilebinary.mobilemonitor.client.a.c> hashSet4 = new HashSet<>();
            Iterator it2 = list.iterator();
            while (it2.hasNext()) {
                com.agilebinary.mobilemonitor.client.a.c cVar3 = (com.agilebinary.mobilemonitor.client.a.c) it2.next();
                if (cVar3.a() && (cVar3 instanceof com.agilebinary.mobilemonitor.client.a.b.l)) {
                    com.agilebinary.mobilemonitor.client.a.b.l lVar = (com.agilebinary.mobilemonitor.client.a.b.l) cVar3;
                    com.agilebinary.mobilemonitor.client.android.b.c.f b3 = b(lVar);
                    b.a(f140a, "...resolveing CDMA lcoation ", "" + lVar.k());
                    if (b3 == null) {
                        String a4 = a(lVar);
                        if (!hashSet3.contains(a4)) {
                            arrayList2.add(new com.agilebinary.mobilemonitor.client.android.b.c.b(lVar.k(), lVar.l(), lVar.m()));
                            hashSet3.add(a4);
                            b.b(f140a, "...adding to request");
                        }
                        hashSet4.add(cVar3);
                    } else {
                        cVar3.a(b3);
                    }
                }
            }
            if (arrayList2.size() > 0) {
                List<c> a5 = this.d.a(this.e.a(), arrayList2, this);
                if (!this.b && a5 != null) {
                    b.b(f140a, "resolve, storing CDMA locations... ");
                    for (c cVar4 : a5) {
                        a(cVar4, cVar4.e());
                    }
                    b.b(f140a, "...resolve, storing CDMA locations");
                    for (com.agilebinary.mobilemonitor.client.a.c cVar5 : hashSet4) {
                        if (cVar5 instanceof com.agilebinary.mobilemonitor.client.a.b.l) {
                            cVar5.a(b((com.agilebinary.mobilemonitor.client.a.b.l) cVar5));
                        }
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        b.b(f140a, "...resolve");
        return;
    }

    public boolean a(com.agilebinary.mobilemonitor.client.a.c cVar) {
        b.b(f140a, "resolveFromCache...");
        com.agilebinary.mobilemonitor.client.android.b.c.f fVar = null;
        try {
            if (cVar instanceof com.agilebinary.mobilemonitor.client.a.b.l) {
                fVar = b((com.agilebinary.mobilemonitor.client.a.b.l) cVar);
            }
            if (cVar instanceof com.agilebinary.mobilemonitor.client.a.b.m) {
                fVar = b((com.agilebinary.mobilemonitor.client.a.b.m) cVar);
            }
            if (fVar != null) {
                cVar.a(fVar);
                b.b(f140a, "...resolveFromCache, success");
                return true;
            }
            b.b(f140a, "...resolveFromCache, not found");
            return false;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void b() {
        /*
            r1 = this;
            monitor-enter(r1)
            android.database.sqlite.SQLiteStatement r0 = r1.f     // Catch:{ Exception -> 0x0015, all -> 0x0012 }
            r0.close()     // Catch:{ Exception -> 0x0015, all -> 0x0012 }
            android.database.sqlite.SQLiteStatement r0 = r1.g     // Catch:{ Exception -> 0x0015, all -> 0x0012 }
            r0.close()     // Catch:{ Exception -> 0x0015, all -> 0x0012 }
            android.database.sqlite.SQLiteDatabase r0 = r1.h     // Catch:{ Exception -> 0x0015, all -> 0x0012 }
            r0.close()     // Catch:{ Exception -> 0x0015, all -> 0x0012 }
        L_0x0010:
            monitor-exit(r1)
            return
        L_0x0012:
            r0 = move-exception
            monitor-exit(r1)
            throw r0
        L_0x0015:
            r0 = move-exception
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.a.b():void");
    }

    public synchronized void c() {
        b.b(f140a, "cancel");
        this.b = true;
        if (this.i != null) {
            try {
                this.i.i();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        return;
    }
}
