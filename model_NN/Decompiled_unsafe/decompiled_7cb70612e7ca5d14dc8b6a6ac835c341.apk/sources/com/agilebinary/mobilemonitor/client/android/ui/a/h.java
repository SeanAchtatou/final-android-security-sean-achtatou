package com.agilebinary.mobilemonitor.client.android.ui.a;

public class h {

    /* renamed from: a  reason: collision with root package name */
    private long f211a;
    private long b;
    private boolean c;

    public h(long j, long j2, boolean z) {
        this.c = z;
        this.f211a = j;
        this.b = j2;
    }

    public long a() {
        return this.f211a;
    }

    public long b() {
        return this.b;
    }
}
