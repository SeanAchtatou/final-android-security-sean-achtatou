package com.agilebinary.mobilemonitor.client.android.b;

import com.agilebinary.a.a.b.c.c.e;
import com.agilebinary.a.a.b.c.c.g;
import com.agilebinary.a.a.b.f.a.h;
import com.agilebinary.a.a.b.i.d;
import com.agilebinary.a.a.b.q;
import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.d.f;
import java.util.Timer;
import java.util.concurrent.TimeUnit;

public class t {

    /* renamed from: a  reason: collision with root package name */
    private static final String f169a = f.a("SpecializedHttpClient");
    private static t b;
    private Timer c = new Timer("HttpTimeoutTimer");
    private h d;

    private t(d dVar) {
        com.agilebinary.a.a.b.f.b.a.h hVar = new com.agilebinary.a.a.b.f.b.a.h(new g(), 30, TimeUnit.SECONDS);
        hVar.b(3);
        hVar.a(3);
        this.d = new h(hVar, dVar);
        try {
            p pVar = new p(x.a());
            this.d.r().a().a(new com.agilebinary.a.a.b.c.c.f("http", e.b(), 80));
            this.d.r().a().a(new com.agilebinary.a.a.b.c.c.f("https", pVar, 443));
        } catch (Exception e) {
            b.e(f169a, "error creating SSLSocketFactory", e);
        }
        this.d.a(new u(this));
        this.d.a(new v(this));
    }

    public static t a() {
        if (b == null) {
            com.agilebinary.a.a.b.i.b bVar = new com.agilebinary.a.a.b.i.b();
            bVar.b("http.connection.timeout", 20000);
            bVar.b("http.socket.timeout", 20000);
            bVar.b("http.socket.linger", 20);
            bVar.a("http.useragent", x.a().p());
            b = new t(bVar);
        }
        return b;
    }

    public q a(com.agilebinary.a.a.b.b.a.g gVar) {
        return this.d.a(gVar);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r1.i();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003f, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r1.i();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0052, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0053, code lost:
        com.agilebinary.mobilemonitor.a.a.a.b.e(com.agilebinary.mobilemonitor.client.android.b.t.f169a, "ConnectionShutdownException", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x005f, code lost:
        throw new com.agilebinary.mobilemonitor.client.android.b.s(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0015, code lost:
        r0 = e;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x001f A[SYNTHETIC, Splitter:B:12:0x001f] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0049 A[SYNTHETIC, Splitter:B:28:0x0049] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0052 A[Catch:{ InterruptedIOException -> 0x0068, IOException -> 0x0065, d -> 0x0052 }, ExcHandler: d (r0v2 'e' com.agilebinary.a.a.b.f.b.d A[CUSTOM_DECLARE, Catch:{  }]), Splitter:B:1:0x0001] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:25:0x0040=Splitter:B:25:0x0040, B:14:0x0022=Splitter:B:14:0x0022, B:9:0x0016=Splitter:B:9:0x0016, B:30:0x004c=Splitter:B:30:0x004c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.agilebinary.mobilemonitor.client.android.b.o a(java.lang.String r5, long r6, com.agilebinary.mobilemonitor.client.android.b.m r8) {
        /*
            r4 = this;
            r2 = 0
            com.agilebinary.a.a.b.b.a.c r1 = new com.agilebinary.a.a.b.b.a.c     // Catch:{ InterruptedIOException -> 0x0068, IOException -> 0x0065, d -> 0x0052 }
            r1.<init>(r5)     // Catch:{ InterruptedIOException -> 0x0068, IOException -> 0x0065, d -> 0x0052 }
            r8.a(r1)     // Catch:{ InterruptedIOException -> 0x0015, IOException -> 0x003f, d -> 0x0052 }
            com.agilebinary.a.a.b.q r0 = com.agilebinary.mobilemonitor.client.android.b.r.a(r4, r1, r6)     // Catch:{ InterruptedIOException -> 0x0015, IOException -> 0x003f, d -> 0x0052 }
            if (r0 != 0) goto L_0x002a
            com.agilebinary.mobilemonitor.client.android.b.s r0 = new com.agilebinary.mobilemonitor.client.android.b.s     // Catch:{ InterruptedIOException -> 0x0015, IOException -> 0x003f, d -> 0x0052 }
            r0.<init>()     // Catch:{ InterruptedIOException -> 0x0015, IOException -> 0x003f, d -> 0x0052 }
            throw r0     // Catch:{ InterruptedIOException -> 0x0015, IOException -> 0x003f, d -> 0x0052 }
        L_0x0015:
            r0 = move-exception
        L_0x0016:
            java.lang.String r2 = com.agilebinary.mobilemonitor.client.android.b.t.f169a     // Catch:{ all -> 0x0028 }
            java.lang.String r3 = "IOException"
            com.agilebinary.mobilemonitor.a.a.a.b.e(r2, r3, r0)     // Catch:{ all -> 0x0028 }
            if (r1 == 0) goto L_0x0022
            r1.i()     // Catch:{ Exception -> 0x0060 }
        L_0x0022:
            com.agilebinary.mobilemonitor.client.android.b.s r0 = new com.agilebinary.mobilemonitor.client.android.b.s     // Catch:{ all -> 0x0028 }
            r0.<init>()     // Catch:{ all -> 0x0028 }
            throw r0     // Catch:{ all -> 0x0028 }
        L_0x0028:
            r0 = move-exception
            throw r0
        L_0x002a:
            com.agilebinary.mobilemonitor.client.android.b.o r2 = new com.agilebinary.mobilemonitor.client.android.b.o     // Catch:{ InterruptedIOException -> 0x0015, IOException -> 0x003f, d -> 0x0052 }
            r2.<init>(r0)     // Catch:{ InterruptedIOException -> 0x0015, IOException -> 0x003f, d -> 0x0052 }
            boolean r0 = r2.d()     // Catch:{ InterruptedIOException -> 0x0015, IOException -> 0x003f, d -> 0x0052 }
            if (r0 == 0) goto L_0x0064
            com.agilebinary.mobilemonitor.client.android.b.s r0 = new com.agilebinary.mobilemonitor.client.android.b.s     // Catch:{ InterruptedIOException -> 0x0015, IOException -> 0x003f, d -> 0x0052 }
            int r2 = r2.c()     // Catch:{ InterruptedIOException -> 0x0015, IOException -> 0x003f, d -> 0x0052 }
            r0.<init>(r2)     // Catch:{ InterruptedIOException -> 0x0015, IOException -> 0x003f, d -> 0x0052 }
            throw r0     // Catch:{ InterruptedIOException -> 0x0015, IOException -> 0x003f, d -> 0x0052 }
        L_0x003f:
            r0 = move-exception
        L_0x0040:
            java.lang.String r2 = com.agilebinary.mobilemonitor.client.android.b.t.f169a     // Catch:{ all -> 0x0028 }
            java.lang.String r3 = "IOException"
            com.agilebinary.mobilemonitor.a.a.a.b.e(r2, r3, r0)     // Catch:{ all -> 0x0028 }
            if (r1 == 0) goto L_0x004c
            r1.i()     // Catch:{ Exception -> 0x0062 }
        L_0x004c:
            com.agilebinary.mobilemonitor.client.android.b.s r1 = new com.agilebinary.mobilemonitor.client.android.b.s     // Catch:{ all -> 0x0028 }
            r1.<init>(r0)     // Catch:{ all -> 0x0028 }
            throw r1     // Catch:{ all -> 0x0028 }
        L_0x0052:
            r0 = move-exception
            java.lang.String r1 = com.agilebinary.mobilemonitor.client.android.b.t.f169a     // Catch:{ all -> 0x0028 }
            java.lang.String r2 = "ConnectionShutdownException"
            com.agilebinary.mobilemonitor.a.a.a.b.e(r1, r2, r0)     // Catch:{ all -> 0x0028 }
            com.agilebinary.mobilemonitor.client.android.b.s r1 = new com.agilebinary.mobilemonitor.client.android.b.s     // Catch:{ all -> 0x0028 }
            r1.<init>(r0)     // Catch:{ all -> 0x0028 }
            throw r1     // Catch:{ all -> 0x0028 }
        L_0x0060:
            r0 = move-exception
            goto L_0x0022
        L_0x0062:
            r1 = move-exception
            goto L_0x004c
        L_0x0064:
            return r2
        L_0x0065:
            r0 = move-exception
            r1 = r2
            goto L_0x0040
        L_0x0068:
            r0 = move-exception
            r1 = r2
            goto L_0x0016
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.b.t.a(java.lang.String, long, com.agilebinary.mobilemonitor.client.android.b.m):com.agilebinary.mobilemonitor.client.android.b.o");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        com.agilebinary.mobilemonitor.a.a.a.b.e(com.agilebinary.mobilemonitor.client.android.b.t.f169a, "IOException", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0026, code lost:
        if (r1 != null) goto L_0x0028;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r1.i();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0030, code lost:
        throw new com.agilebinary.mobilemonitor.client.android.b.s();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0048, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        com.agilebinary.mobilemonitor.a.a.a.b.e(com.agilebinary.mobilemonitor.client.android.b.t.f169a, "IOException", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0050, code lost:
        if (r1 != null) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:?, code lost:
        r1.i();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x005a, code lost:
        throw new com.agilebinary.mobilemonitor.client.android.b.s(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x005b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x005c, code lost:
        com.agilebinary.mobilemonitor.a.a.a.b.e(com.agilebinary.mobilemonitor.client.android.b.t.f169a, "ConnectionShutdownException", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0068, code lost:
        throw new com.agilebinary.mobilemonitor.client.android.b.s(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.agilebinary.mobilemonitor.client.android.b.o a(java.lang.String r5, byte[] r6, long r7, com.agilebinary.mobilemonitor.client.android.b.m r9) {
        /*
            r4 = this;
            com.agilebinary.a.a.b.b.a.e r1 = new com.agilebinary.a.a.b.b.a.e
            r1.<init>(r5)
            r9.a(r1)
            if (r6 == 0) goto L_0x0012
            com.agilebinary.a.a.b.e.d r0 = new com.agilebinary.a.a.b.e.d
            r0.<init>(r6)
            r1.a(r0)
        L_0x0012:
            com.agilebinary.a.a.b.q r0 = com.agilebinary.mobilemonitor.client.android.b.r.a(r4, r1, r7)     // Catch:{ InterruptedIOException -> 0x001e, IOException -> 0x0048, d -> 0x005b }
            if (r0 != 0) goto L_0x0033
            com.agilebinary.mobilemonitor.client.android.b.s r0 = new com.agilebinary.mobilemonitor.client.android.b.s     // Catch:{ InterruptedIOException -> 0x001e, IOException -> 0x0048, d -> 0x005b }
            r0.<init>()     // Catch:{ InterruptedIOException -> 0x001e, IOException -> 0x0048, d -> 0x005b }
            throw r0     // Catch:{ InterruptedIOException -> 0x001e, IOException -> 0x0048, d -> 0x005b }
        L_0x001e:
            r0 = move-exception
            java.lang.String r2 = com.agilebinary.mobilemonitor.client.android.b.t.f169a     // Catch:{ all -> 0x0031 }
            java.lang.String r3 = "IOException"
            com.agilebinary.mobilemonitor.a.a.a.b.e(r2, r3, r0)     // Catch:{ all -> 0x0031 }
            if (r1 == 0) goto L_0x002b
            r1.i()     // Catch:{ Exception -> 0x0069 }
        L_0x002b:
            com.agilebinary.mobilemonitor.client.android.b.s r0 = new com.agilebinary.mobilemonitor.client.android.b.s     // Catch:{ all -> 0x0031 }
            r0.<init>()     // Catch:{ all -> 0x0031 }
            throw r0     // Catch:{ all -> 0x0031 }
        L_0x0031:
            r0 = move-exception
            throw r0
        L_0x0033:
            com.agilebinary.mobilemonitor.client.android.b.o r2 = new com.agilebinary.mobilemonitor.client.android.b.o     // Catch:{ InterruptedIOException -> 0x001e, IOException -> 0x0048, d -> 0x005b }
            r2.<init>(r0)     // Catch:{ InterruptedIOException -> 0x001e, IOException -> 0x0048, d -> 0x005b }
            boolean r0 = r2.d()     // Catch:{ InterruptedIOException -> 0x001e, IOException -> 0x0048, d -> 0x005b }
            if (r0 == 0) goto L_0x006d
            com.agilebinary.mobilemonitor.client.android.b.s r0 = new com.agilebinary.mobilemonitor.client.android.b.s     // Catch:{ InterruptedIOException -> 0x001e, IOException -> 0x0048, d -> 0x005b }
            int r2 = r2.c()     // Catch:{ InterruptedIOException -> 0x001e, IOException -> 0x0048, d -> 0x005b }
            r0.<init>(r2)     // Catch:{ InterruptedIOException -> 0x001e, IOException -> 0x0048, d -> 0x005b }
            throw r0     // Catch:{ InterruptedIOException -> 0x001e, IOException -> 0x0048, d -> 0x005b }
        L_0x0048:
            r0 = move-exception
            java.lang.String r2 = com.agilebinary.mobilemonitor.client.android.b.t.f169a     // Catch:{ all -> 0x0031 }
            java.lang.String r3 = "IOException"
            com.agilebinary.mobilemonitor.a.a.a.b.e(r2, r3, r0)     // Catch:{ all -> 0x0031 }
            if (r1 == 0) goto L_0x0055
            r1.i()     // Catch:{ Exception -> 0x006b }
        L_0x0055:
            com.agilebinary.mobilemonitor.client.android.b.s r1 = new com.agilebinary.mobilemonitor.client.android.b.s     // Catch:{ all -> 0x0031 }
            r1.<init>(r0)     // Catch:{ all -> 0x0031 }
            throw r1     // Catch:{ all -> 0x0031 }
        L_0x005b:
            r0 = move-exception
            java.lang.String r1 = com.agilebinary.mobilemonitor.client.android.b.t.f169a     // Catch:{ all -> 0x0031 }
            java.lang.String r2 = "ConnectionShutdownException"
            com.agilebinary.mobilemonitor.a.a.a.b.e(r1, r2, r0)     // Catch:{ all -> 0x0031 }
            com.agilebinary.mobilemonitor.client.android.b.s r1 = new com.agilebinary.mobilemonitor.client.android.b.s     // Catch:{ all -> 0x0031 }
            r1.<init>(r0)     // Catch:{ all -> 0x0031 }
            throw r1     // Catch:{ all -> 0x0031 }
        L_0x0069:
            r0 = move-exception
            goto L_0x002b
        L_0x006b:
            r1 = move-exception
            goto L_0x0055
        L_0x006d:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.b.t.a(java.lang.String, byte[], long, com.agilebinary.mobilemonitor.client.android.b.m):com.agilebinary.mobilemonitor.client.android.b.o");
    }

    public void b() {
        this.d.r().a(1000, TimeUnit.MILLISECONDS);
    }

    public Timer c() {
        return this.c;
    }
}
