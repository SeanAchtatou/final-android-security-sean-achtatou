package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import com.agilebinary.phonebeagle.client.R;

public class AddressbookAddNumberActivity extends ah {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public EditText f192a;
    private Button b;

    public static void a(Context context) {
        context.startActivity(new Intent(context, AddressbookAddNumberActivity.class));
    }

    /* access modifiers changed from: protected */
    public int a() {
        return R.layout.addressbook_add_number;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f192a = (EditText) findViewById(R.id.addressbook_add_number_number);
        this.b = (Button) findViewById(R.id.addressbook_add_number_add);
        this.b.setOnClickListener(new v(this));
    }
}
