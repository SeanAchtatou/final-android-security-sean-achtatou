package com.agilebinary.mobilemonitor.client.android.b.a;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.android.b.d;
import com.agilebinary.mobilemonitor.client.android.b.e;
import com.agilebinary.mobilemonitor.client.android.b.o;
import java.util.List;
import java.util.Locale;

public class f implements d {

    /* renamed from: a  reason: collision with root package name */
    private Context f145a;

    public f(Context context) {
        this.f145a = context;
    }

    public o a(com.agilebinary.mobilemonitor.client.android.f fVar, List list) {
        if (Locale.getDefault().getLanguage().equals(Locale.GERMAN.getLanguage())) {
            list.add(new e("Angie", "01790492819", false, null, 0));
            list.add(new e("Karl", "01779284928", false, null, 0));
            list.add(new e("Steve", "01759284928", false, null, 0));
            list.add(new e("Nele", "01798492848", false, null, 0));
            list.add(new e("Kottak", "01778839293", false, null, 0));
            list.add(new e("Bruda", "01689392839", false, null, 0));
        } else {
            list.add(new e("Natasha", "12025551213", false, null, 0));
            list.add(new e("Secret Service Dog Patrol", "12025551217", false, null, 0));
            list.add(new e("White House - Dog Patrol", "12025551216", false, null, 0));
            list.add(new e("Barrack", "12025551214", false, null, 0));
            list.add(new e("Malia Anne", "12025551212", false, null, 0));
            list.add(new e("Michelle", "12025551215", false, null, 0));
        }
        return new k();
    }

    public o b(com.agilebinary.mobilemonitor.client.android.f fVar, List list) {
        return new k();
    }
}
