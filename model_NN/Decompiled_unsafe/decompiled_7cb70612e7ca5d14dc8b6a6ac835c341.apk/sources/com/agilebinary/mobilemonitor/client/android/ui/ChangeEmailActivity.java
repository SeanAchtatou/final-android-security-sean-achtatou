package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.b.c.a;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.mobilemonitor.client.android.ui.a.c;
import com.agilebinary.mobilemonitor.client.android.ui.a.o;
import com.agilebinary.phonebeagle.client.R;
import java.util.regex.Pattern;

public class ChangeEmailActivity extends al {

    /* renamed from: a  reason: collision with root package name */
    private static final String f195a = f.a("ChangeEmailActivity");
    private TextView b;
    /* access modifiers changed from: private */
    public EditText i;
    /* access modifiers changed from: private */
    public EditText j;
    private Button k;
    private Button l;
    private Button m;
    private boolean n;

    public static void a(Activity activity, int i2, boolean z, a aVar, int i3) {
        Intent intent = new Intent(activity, ChangeEmailActivity.class);
        intent.putExtra("EXTRA_TEXT_ID", i2);
        intent.putExtra("EXTRA_SKIP", z);
        intent.putExtra("EXTRA_ACCOUNT", aVar);
        activity.startActivityForResult(intent, i3);
    }

    public static void a(Context context, o oVar) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra("EXTRA_SERVICE_RESULT", oVar);
        intent.setFlags(536870912);
        context.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        b((int) R.string.msg_progress_communicating);
        com.agilebinary.mobilemonitor.client.android.ui.a.a aVar = new com.agilebinary.mobilemonitor.client.android.ui.a.a(str, (a) getIntent().getSerializableExtra("EXTRA_ACCOUNT"));
        c.b = new c(this);
        c.b.execute(aVar);
    }

    /* access modifiers changed from: private */
    public boolean b(String str) {
        return Pattern.compile(".+@.+\\.[a-z]+").matcher(str).matches();
    }

    /* access modifiers changed from: protected */
    public int a() {
        return R.layout.change_email;
    }

    /* access modifiers changed from: protected */
    public void e() {
        if (c.b != null) {
            try {
                c.b.a();
            } catch (Exception e) {
            }
        }
    }

    public void onCreate(Bundle bundle) {
        int i2 = 0;
        super.onCreate(bundle);
        this.b = (TextView) findViewById(R.id.changeemail_caption);
        int intExtra = getIntent().getIntExtra("EXTRA_TEXT_ID", 0);
        this.n = getIntent().getBooleanExtra("EXTRA_SKIP", false);
        this.b.setText(intExtra);
        this.i = (EditText) findViewById(R.id.changeemail_email1);
        this.j = (EditText) findViewById(R.id.changeemail_email2);
        this.k = (Button) findViewById(R.id.changeemail_cancel);
        this.l = (Button) findViewById(R.id.changeemail_skip);
        this.k.setVisibility(!this.n ? 0 : 8);
        Button button = this.l;
        if (!this.n) {
            i2 = 8;
        }
        button.setVisibility(i2);
        this.m = (Button) findViewById(R.id.changeemail_ok);
        this.m.setOnClickListener(new ao(this));
        this.k.setOnClickListener(new ap(this));
        this.l.setOnClickListener(new aq(this));
        if (bundle != null) {
            this.i.setText(bundle.getString("EXTRA_EMAIL1"));
            this.j.setText(bundle.getString("EXTRA_EMAIL2"));
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        b.b(f195a, "onNewIntent...");
        o oVar = (o) intent.getSerializableExtra("EXTRA_SERVICE_RESULT");
        b(false);
        if (oVar != null) {
            if (oVar.b() != null) {
                if (oVar.b().c()) {
                    a((int) R.string.error_service_account_general);
                } else if (oVar.b().b()) {
                    a((int) R.string.error_service_account_general);
                } else {
                    a((int) R.string.error_service_account_general);
                }
            } else if (oVar.a()) {
                setResult(-1);
                finish();
            }
        }
        b.b(f195a, "...onNewIntent");
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        b.b(f195a, "onSaveInstanceState...");
        bundle.putString("EXTRA_EMAIL1", this.i.getText().toString());
        bundle.putString("EXTRA_EMAIL2", this.j.getText().toString());
        super.onSaveInstanceState(bundle);
        b.b(f195a, "...onSaveInstanceState");
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.l.getVisibility() == 0) {
            this.l.requestFocus();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
