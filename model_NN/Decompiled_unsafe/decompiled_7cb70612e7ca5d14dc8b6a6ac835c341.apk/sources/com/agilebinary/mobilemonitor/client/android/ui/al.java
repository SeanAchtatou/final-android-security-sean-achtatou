package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.c.a;
import com.agilebinary.mobilemonitor.client.android.c.c;
import com.agilebinary.mobilemonitor.client.android.f;
import com.agilebinary.phonebeagle.client.R;

public abstract class al extends ah {
    protected c h;

    private void b() {
        b.b(getClass().getName(), "assertLoggedIn...");
        if (!this.g.c()) {
            Intent intent = new Intent(this, LoginActivity.class);
            intent.addFlags(67108864);
            b.b(getClass().getName(), "starting LoginActivity from assertLoggedIn");
            startActivity(intent);
        }
        b.b(getClass().getName(), "...assertLoggedIn");
    }

    /* access modifiers changed from: protected */
    public CharSequence i() {
        String str = null;
        f a2 = this.g.a();
        String e = a2 == null ? null : a2.e();
        if (a2 != null) {
            str = a2.c();
        }
        if (e == null || e.trim().length() == 0) {
            e = str;
        }
        b.b(getClass().getName(), "########################" + e);
        return getString(R.string.titlebar_fmt, new Object[]{getString(R.string.app_title), e});
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.h = this.g.e();
        b();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.base_loggedin, menu);
        menu.findItem(R.id.menu_base_loggedin_accountinfo).setVisible(!this.g.d());
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.g.f();
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_base_loggedin_logout /*2131296480*/:
                finish();
                this.g.a(this);
                return true;
            case R.id.menu_base_loggedin_accountinfo /*2131296481*/:
                j();
                AccountActivity.a(this);
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        StringBuffer i = this.g.i();
        if (i != null && i.length() > 0) {
            if (System.currentTimeMillis() - a.a(getApplicationContext()).a("LAST_ACCOUNT_EXPIRY_WARNING_DATE", 0) > 14400000) {
                a(i.toString());
                a.a(getApplicationContext()).b("LAST_ACCOUNT_EXPIRY_WARNING_DATE", System.currentTimeMillis());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        b();
        super.onStart();
    }
}
