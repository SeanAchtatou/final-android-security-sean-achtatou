package com.agilebinary.mobilemonitor.client.android.ui.a;

import com.agilebinary.mobilemonitor.client.android.b.s;
import java.io.Serializable;
import java.util.List;

public class f implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    s f209a;
    private List b;

    public f(s sVar) {
        this.f209a = sVar;
    }

    public f(List list) {
        this.b = list;
    }

    public s a() {
        return this.f209a;
    }

    public List b() {
        return this.b;
    }
}
