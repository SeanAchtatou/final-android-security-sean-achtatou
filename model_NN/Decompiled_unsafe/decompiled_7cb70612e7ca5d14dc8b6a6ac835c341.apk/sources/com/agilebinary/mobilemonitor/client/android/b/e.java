package com.agilebinary.mobilemonitor.client.android.b;

import android.util.Log;
import java.io.Serializable;

public class e implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public String f163a;
    public String b;
    public String c;
    public long d;
    public boolean e;
    public boolean f;

    public e(String str, String str2, boolean z, String str3, long j) {
        this.f163a = str == null ? "" : str;
        this.b = str2;
        this.c = str3;
        this.d = j;
        this.e = z;
    }

    public void a(boolean z) {
        Log.d("Contact", "setBlocked Before: " + this);
        this.e = z;
        this.c = "client_set";
        this.d = System.currentTimeMillis();
        Log.d("Contact", "setBlocked After: " + this);
    }

    public String toString() {
        return "Contact [ename=" + this.f163a + ", phonenumber=" + this.b + ", status=" + this.c + ", status_time=" + this.d + ", blocked=" + this.e + "]";
    }
}
