package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Dialog;
import android.view.View;

final class cy implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Dialog f305a;
    final /* synthetic */ MapActivity_OSM b;

    cy(MapActivity_OSM mapActivity_OSM, Dialog dialog) {
        this.b = mapActivity_OSM;
        this.f305a = dialog;
    }

    public void onClick(View view) {
        this.b.n();
        this.f305a.dismiss();
    }
}
