package com.agilebinary.mobilemonitor.client.android.b.a;

import com.agilebinary.mobilemonitor.client.android.b.o;
import com.agilebinary.mobilemonitor.client.android.b.t;
import com.agilebinary.mobilemonitor.client.android.b.x;
import java.util.Calendar;
import java.util.Locale;

public class a {

    /* renamed from: a  reason: collision with root package name */
    public static final long f142a = b();
    protected t b;

    public static String a() {
        String c = x.a().c();
        String l = x.a().l();
        return (!Locale.getDefault().getLanguage().equals("de") || l == null || !l.contains("de")) ? c : c + "de_DE/";
    }

    protected static long b() {
        Calendar instance = Calendar.getInstance();
        instance.set(11, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        return instance.getTimeInMillis();
    }

    /* access modifiers changed from: protected */
    public void a(o oVar) {
        if (oVar != null) {
            oVar.f();
        }
    }
}
