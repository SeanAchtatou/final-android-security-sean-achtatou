package com.agilebinary.mobilemonitor.client.android.ui;

import android.database.Cursor;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.android.c.g;
import com.agilebinary.phonebeagle.client.R;

public class cc extends ck {

    /* renamed from: a  reason: collision with root package name */
    protected TextView f285a;
    protected TextView b;
    private int c = -1;
    private int g = -1;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.agilebinary.mobilemonitor.client.android.ui.cc, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public cc(bq bqVar, AttributeSet attributeSet) {
        super(bqVar, attributeSet);
        ((LayoutInflater) bqVar.getSystemService("layout_inflater")).inflate((int) R.layout.eventlist_rowview_fbk, (ViewGroup) this, true);
        a();
    }

    /* access modifiers changed from: protected */
    public void a() {
        super.a();
        this.f285a = (TextView) findViewById(R.id.eventlist_rowview_fbk_line1);
        this.b = (TextView) findViewById(R.id.eventlist_rowview_fbk_line2);
    }

    public void a(Cursor cursor) {
        super.a(cursor);
        if (this.c < 0) {
            this.c = cursor.getColumnIndex(g.g);
            this.g = cursor.getColumnIndex(g.h);
        }
        this.f285a.setText(cursor.getString(this.c));
        this.b.setText(cursor.getString(this.g));
    }
}
