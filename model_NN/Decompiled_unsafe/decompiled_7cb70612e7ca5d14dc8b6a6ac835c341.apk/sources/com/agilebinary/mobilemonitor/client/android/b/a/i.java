package com.agilebinary.mobilemonitor.client.android.b.a;

import com.agilebinary.mobilemonitor.client.android.b.h;
import com.agilebinary.mobilemonitor.client.android.c.p;
import com.agilebinary.mobilemonitor.client.android.d.a;
import java.io.ByteArrayInputStream;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

class i extends DefaultHandler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ h f146a;
    private char[] b = new char[1048576];
    private int c = 0;
    private byte d;
    private h e;
    private com.agilebinary.mobilemonitor.client.android.b.i f;
    private byte g;
    private long h;
    private long i;

    public i(h hVar, h hVar2, com.agilebinary.mobilemonitor.client.android.b.i iVar, byte b2) {
        this.f146a = hVar;
        this.e = hVar2;
        this.f = iVar;
        this.d = b2;
    }

    public void characters(char[] cArr, int i2, int i3) {
        for (int i4 = 0; i4 < i3; i4++) {
            char c2 = cArr[i2 + i4];
            if (c2 != 10) {
                this.b[this.c] = c2;
                this.c++;
            }
        }
    }

    public void endElement(String str, String str2, String str3) {
        if ("i".equals(str2)) {
            this.h = (a.f142a + 86400000) - Long.parseLong(new String(this.b, 0, this.c));
            this.i = this.h + 1980000;
        } else if ("t".equals(str2)) {
            this.g = Byte.parseByte(new String(this.b, 0, this.c));
        }
        if ("d".equals(str2)) {
            byte[] a2 = a.a(this.b, 0, this.c);
            try {
                this.e.a(d.c, this.g, this.h, this.i, d.d, false, true, a2.length, new ByteArrayInputStream(a2));
                this.f.a_();
            } catch (p e2) {
                throw new SAXException(e2);
            }
        }
        this.c = 0;
    }

    public void startElement(String str, String str2, String str3, Attributes attributes) {
    }
}
