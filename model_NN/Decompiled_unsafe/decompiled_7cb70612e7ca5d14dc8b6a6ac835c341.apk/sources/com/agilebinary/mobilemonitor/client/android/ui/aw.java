package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.a.b.b;
import com.agilebinary.mobilemonitor.client.android.d.g;
import com.agilebinary.phonebeagle.client.R;

public abstract class aw extends al {

    /* renamed from: a  reason: collision with root package name */
    protected TextView f243a;
    protected TextView b;
    protected ImageButton i;
    protected ImageButton j;
    protected ImageButton k;
    protected ImageButton l;
    protected b m;
    private TextView n;
    private TextView o;
    private long p;
    private byte q;

    public static void a(Context context, byte b2, long j2) {
        Class cls;
        switch (b2) {
            case 2:
                cls = EventDetailsActivity_CALL.class;
                break;
            case 3:
                cls = EventDetailsActivity_SMS.class;
                break;
            case 4:
                cls = EventDetailsActivity_MMS.class;
                break;
            case 5:
            case 7:
            default:
                throw new IllegalArgumentException("not yet implemented");
            case 6:
                cls = EventDetailsActivity_WEB.class;
                break;
            case 8:
                cls = EventDetailsActivity_SYS.class;
                break;
            case 9:
                cls = EventDetailsActivity_APP.class;
                break;
            case 10:
                cls = EventDetailsActivity_FBK.class;
                break;
            case 11:
                cls = EventDetailsActivity_WPP.class;
                break;
            case 12:
                cls = EventDetailsActivity_LIN.class;
                break;
        }
        Intent intent = new Intent(context, cls);
        intent.putExtra("EXTRA_EVENT_ID", j2);
        intent.putExtra("EXTRA_EVENT_TYPE", b2);
        context.startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public int a() {
        return R.layout.eventdetails_base;
    }

    /* access modifiers changed from: protected */
    public abstract void a(ViewGroup viewGroup);

    /* access modifiers changed from: protected */
    public void a(b bVar) {
        this.m = bVar;
        this.f243a.setText(bVar.e());
        this.b.setText(g.a(this).c(bVar.g()));
        this.j.setEnabled(this.h.f(this.q, bVar.h()));
        this.i.setEnabled(this.h.d(this.q, bVar.h()));
        this.k.setEnabled(this.i.isEnabled());
        this.l.setEnabled(this.j.isEnabled());
        this.o.setText(g.a(this).d(bVar.h()));
        this.n.setText(com.agilebinary.mobilemonitor.client.android.d.b.a(this, this.m.f()));
    }

    /* access modifiers changed from: protected */
    public void b() {
        b g = this.h.g(this.q, this.m.h());
        if (g != null) {
            a(g);
        }
    }

    /* access modifiers changed from: protected */
    public void c() {
        b e = this.h.e(this.q, this.m.h());
        if (e != null) {
            a(e);
        }
    }

    /* access modifiers changed from: protected */
    public void l() {
        b d = this.h.d(this.q);
        if (d != null) {
            a(d);
        }
    }

    /* access modifiers changed from: protected */
    public void m() {
        b e = this.h.e(this.q);
        if (e != null) {
            a(e);
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        a((ViewGroup) findViewById(R.id.eventdetails_typespecific));
        this.p = 0;
        this.q = -1;
        if (bundle != null) {
            this.p = bundle.getLong("EXTRA_EVENT_ID");
            this.q = bundle.getByte("EXTRA_EVENT_TYPE");
        } else {
            this.p = getIntent().getLongExtra("EXTRA_EVENT_ID", 0);
            this.q = getIntent().getByteExtra("EXTRA_EVENT_TYPE", (byte) -1);
        }
        this.f243a = (TextView) findViewById(R.id.eventdetails_common_phone_identifier);
        this.b = (TextView) findViewById(R.id.eventdetails_common_servertime);
        this.o = (TextView) findViewById(R.id.eventdetails_navigation_text);
        this.n = (TextView) findViewById(R.id.eventdetails_common_country);
        this.i = (ImageButton) findViewById(R.id.eventdetails_navigation_prev);
        this.j = (ImageButton) findViewById(R.id.eventdetails_navigation_next);
        this.k = (ImageButton) findViewById(R.id.eventdetails_navigation_first);
        this.l = (ImageButton) findViewById(R.id.eventdetails_navigation_last);
        this.i.setEnabled(false);
        this.j.setEnabled(false);
        this.k.setEnabled(false);
        this.l.setEnabled(false);
        this.i.setOnClickListener(new ax(this));
        this.j.setOnClickListener(new ay(this));
        this.k.setOnClickListener(new az(this));
        this.l.setOnClickListener(new ba(this));
        a(this.h.h(this.q, this.p));
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        bundle.putLong("EXTRA_EVENT_ID", this.m.h());
        bundle.putByte("EXTRA_EVENT_TYPE", this.q);
        super.onSaveInstanceState(bundle);
    }
}
