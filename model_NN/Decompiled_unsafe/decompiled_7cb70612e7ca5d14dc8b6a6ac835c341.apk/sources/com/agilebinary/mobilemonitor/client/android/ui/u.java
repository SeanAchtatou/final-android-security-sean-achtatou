package com.agilebinary.mobilemonitor.client.android.ui;

import com.agilebinary.mobilemonitor.client.android.b.e;
import java.util.Comparator;

final class u implements Comparator {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AddressbookActivity f334a;
    final /* synthetic */ t b;

    u(t tVar, AddressbookActivity addressbookActivity) {
        this.b = tVar;
        this.f334a = addressbookActivity;
    }

    /* renamed from: a */
    public int compare(e eVar, e eVar2) {
        return eVar.f163a.compareTo(eVar2.f163a);
    }
}
