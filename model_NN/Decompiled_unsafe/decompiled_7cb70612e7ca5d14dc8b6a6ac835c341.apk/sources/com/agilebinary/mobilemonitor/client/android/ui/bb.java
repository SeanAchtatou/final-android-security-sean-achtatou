package com.agilebinary.mobilemonitor.client.android.ui;

import android.widget.ListAdapter;
import android.widget.RadioGroup;
import com.agilebinary.mobilemonitor.client.android.c.e;
import com.agilebinary.phonebeagle.client.R;

final class bb implements RadioGroup.OnCheckedChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ EventListActivity_APP f257a;

    bb(EventListActivity_APP eventListActivity_APP) {
        this.f257a = eventListActivity_APP;
    }

    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        if (i == R.id.eventlist_app_filter_install_only) {
            this.f257a.A = this.f257a.h.a(this.f257a.m, e.i + "!=" + 5);
            this.f257a.w = new bz(this.f257a, this.f257a, this.f257a.A, this.f257a.x);
            this.f257a.n.setAdapter((ListAdapter) this.f257a.w);
            System.out.println("HELLO A");
        }
        if (i == R.id.eventlist_app_filter_all) {
            this.f257a.A = this.f257a.h.a(this.f257a.m, (String) null);
            this.f257a.w = new bz(this.f257a, this.f257a, this.f257a.A, this.f257a.x);
            this.f257a.n.setAdapter((ListAdapter) this.f257a.w);
            System.out.println("HELLO B");
        }
    }
}
