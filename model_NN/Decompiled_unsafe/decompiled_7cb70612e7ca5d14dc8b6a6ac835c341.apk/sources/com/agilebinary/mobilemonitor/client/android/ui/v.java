package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.View;
import com.agilebinary.phonebeagle.client.R;

final class v implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AddressbookAddNumberActivity f335a;

    v(AddressbookAddNumberActivity addressbookAddNumberActivity) {
        this.f335a = addressbookAddNumberActivity;
    }

    public void onClick(View view) {
        String obj = this.f335a.f192a.getText().toString();
        if (obj.trim().length() > 3) {
            this.f335a.finish();
            AddressbookActivity.a(this.f335a, obj);
            return;
        }
        this.f335a.a((int) R.string.error_blocknumber_too_short);
    }
}
