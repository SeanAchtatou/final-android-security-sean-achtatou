package com.agilebinary.mobilemonitor.client.android.d;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.b.g;
import com.agilebinary.phonebeagle.client.R;
import java.text.NumberFormat;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private static NumberFormat f180a = NumberFormat.getIntegerInstance();

    public static double a(double d, String str) {
        return "mph".equals(str) ? (d * 3.6d) / 1.609d : d * 3.6d;
    }

    public static CharSequence a(Context context, g gVar) {
        if (gVar.i()) {
            String string = context.getString(R.string.speed_unit);
            String str = "";
            try {
                str = context.getResources().getString(com.agilebinary.b.a.b.class.getField("speed_unit_display_" + string).getInt(null));
            } catch (Exception e) {
                e.printStackTrace();
            }
            Double valueOf = Double.valueOf(0.0d);
            if (gVar.l()) {
                valueOf = Double.valueOf(a(gVar.j(), string));
            }
            String c = g.a(context).c(gVar.k());
            if (gVar.l()) {
                return context.getString(R.string.msg_event_speed, a(valueOf.doubleValue()), str, c);
            }
        }
        return context.getString(R.string.label_event_not_available);
    }

    public static CharSequence a(Context context, Double d) {
        if (d == null) {
            return context.getString(R.string.label_event_not_available);
        }
        String string = context.getString(R.string.speed_unit);
        String str = "";
        try {
            str = context.getResources().getString(com.agilebinary.b.a.b.class.getField("speed_unit_display_" + string).getInt(null));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return context.getString(R.string.msg_event_speed_short, a(Double.valueOf(a(d.doubleValue(), string)).doubleValue()), str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0020  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x001b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.CharSequence a(android.content.Context r8, java.lang.String r9) {
        /*
            r7 = 1
            r3 = 0
            r4 = 0
            java.lang.String r0 = "\\|"
            java.lang.String[] r1 = r9.split(r0)
            int r0 = r1.length
            if (r0 <= 0) goto L_0x008e
            r0 = 0
            r0 = r1[r0]     // Catch:{ Exception -> 0x0077 }
            java.util.TimeZone r0 = java.util.TimeZone.getTimeZone(r0)     // Catch:{ Exception -> 0x0077 }
        L_0x0013:
            if (r0 == 0) goto L_0x008e
            java.lang.String r0 = r0.getDisplayName()
        L_0x0019:
            if (r0 != 0) goto L_0x001d
            r0 = r1[r4]
        L_0x001d:
            int r2 = r1.length
            if (r2 <= r7) goto L_0x008b
            r2 = r1[r7]
            int r1 = java.lang.Integer.parseInt(r2)     // Catch:{ NumberFormatException -> 0x007d }
        L_0x0026:
            if (r1 != 0) goto L_0x0080
            java.lang.String r2 = r2.toUpperCase()
        L_0x002c:
            java.lang.Class<com.agilebinary.b.a.b> r1 = com.agilebinary.b.a.b.class
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0085 }
            r5.<init>()     // Catch:{ Exception -> 0x0085 }
            java.lang.String r6 = "CC_"
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ Exception -> 0x0085 }
            java.lang.StringBuilder r5 = r5.append(r2)     // Catch:{ Exception -> 0x0085 }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x0085 }
            java.lang.reflect.Field r1 = r1.getField(r5)     // Catch:{ Exception -> 0x0085 }
            r5 = 0
            int r1 = r1.getInt(r5)     // Catch:{ Exception -> 0x0085 }
            android.content.res.Resources r5 = r8.getResources()     // Catch:{ Exception -> 0x0085 }
            java.lang.String r3 = r5.getString(r1)     // Catch:{ Exception -> 0x0085 }
            r1 = r3
        L_0x0053:
            if (r1 != 0) goto L_0x0064
            android.content.res.Resources r1 = r8.getResources()
            r3 = 2131099850(0x7f0600ca, float:1.7812065E38)
            java.lang.Object[] r5 = new java.lang.Object[r7]
            r5[r4] = r2
            java.lang.String r1 = r1.getString(r3, r5)
        L_0x0064:
            android.content.res.Resources r2 = r8.getResources()
            r3 = 2131099851(0x7f0600cb, float:1.7812067E38)
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]
            r5[r4] = r1
            r5[r7] = r0
            java.lang.String r0 = r2.getString(r3, r5)
            return r0
        L_0x0077:
            r0 = move-exception
            r0.printStackTrace()
            r0 = r3
            goto L_0x0013
        L_0x007d:
            r1 = move-exception
            r1 = r4
            goto L_0x0026
        L_0x0080:
            java.lang.String r2 = com.agilebinary.mobilemonitor.client.android.d.e.a(r1)
            goto L_0x002c
        L_0x0085:
            r1 = move-exception
            r1.printStackTrace()
            r1 = r3
            goto L_0x0053
        L_0x008b:
            r2 = r3
            r1 = r3
            goto L_0x0053
        L_0x008e:
            r0 = r3
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.d.b.a(android.content.Context, java.lang.String):java.lang.CharSequence");
    }

    private static String a(double d) {
        return f180a.format(d);
    }

    public static String a(Context context, String[] strArr, String[] strArr2) {
        if (strArr.length <= 0) {
            return context.getString(R.string.label_event_mms_address_not_provided);
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < strArr2.length; i++) {
            if (i > 0) {
                stringBuffer.append(", ");
            }
            stringBuffer.append(b(strArr[i], strArr2[i]));
        }
        return stringBuffer.toString();
    }

    public static String a(String str, String str2) {
        return (str2 == null || str2.trim().length() == 0) ? str : str2 + " <" + str + ">";
    }

    public static boolean a(g gVar) {
        return gVar.i() && gVar.l() && gVar.j() > 1.7d;
    }

    public static boolean a(String str) {
        return str == null || str.trim().length() == 0;
    }

    public static String b(String str, String str2) {
        return a(str2) ? str : str2;
    }
}
