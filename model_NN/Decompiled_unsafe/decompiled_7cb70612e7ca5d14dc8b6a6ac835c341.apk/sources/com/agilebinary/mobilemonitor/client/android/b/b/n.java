package com.agilebinary.mobilemonitor.client.android.b.b;

import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.b.c.c;
import com.agilebinary.mobilemonitor.client.android.b.k;
import com.agilebinary.mobilemonitor.client.android.b.m;
import com.agilebinary.mobilemonitor.client.android.b.o;
import com.agilebinary.mobilemonitor.client.android.b.s;
import com.agilebinary.mobilemonitor.client.android.b.x;
import com.agilebinary.mobilemonitor.client.android.d.f;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class n extends a implements k {
    private static final String b = f.a("ProductionLocationServiceCDMA");

    public List a(com.agilebinary.mobilemonitor.client.android.f fVar, List list, m mVar) {
        o oVar;
        b.b(b, "getGeocodeCdmaCellLocations...");
        try {
            JSONArray jSONArray = new JSONArray();
            Iterator it = list.iterator();
            while (it.hasNext()) {
                jSONArray.put(new JSONObject(((com.agilebinary.mobilemonitor.client.android.b.c.b) it.next()).a()));
            }
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("cdma_cell_list", jSONArray);
            byte[] bytes = jSONObject.toString().getBytes("UTF-8");
            String format = String.format(x.a().d() + "ServiceLocationRetrieveCDMACell?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s&RequestAddress=false", "1", a(fVar.c()), a(fVar.d()));
            b.a(b, "URL: ", format);
            oVar = this.f149a.a(format, bytes, x.a().n(), mVar);
            try {
                if (oVar.b()) {
                    String b2 = b(oVar);
                    b.a(b, "response: ", "" + b2);
                    JSONArray jSONArray2 = new JSONObject(b2).getJSONArray("geo_code_cdma_cell_list");
                    ArrayList arrayList = new ArrayList();
                    if (jSONArray2.length() > 0) {
                        for (int i = 0; i < jSONArray2.length(); i++) {
                            c cVar = new c();
                            cVar.a(jSONArray2.getJSONObject(i).getLong("create_date"));
                            cVar.d(jSONArray2.getJSONObject(i).getInt("geocoder_source_id"));
                            cVar.a(jSONArray2.getJSONObject(i).getInt("base_station_id"));
                            cVar.b(jSONArray2.getJSONObject(i).getInt("network_id"));
                            cVar.c(jSONArray2.getJSONObject(i).getInt("system_id"));
                            com.agilebinary.mobilemonitor.client.android.b.c.f fVar2 = new com.agilebinary.mobilemonitor.client.android.b.c.f();
                            fVar2.a(jSONArray2.getJSONObject(i).getDouble("latitude"));
                            fVar2.b(jSONArray2.getJSONObject(i).getDouble("longitude"));
                            fVar2.c(jSONArray2.getJSONObject(i).getDouble("accuracy"));
                            cVar.a(fVar2);
                            arrayList.add(cVar);
                        }
                    }
                    a(oVar);
                    b.b(b, "...getGeocodeCdmaCellLocations");
                    return arrayList;
                }
                throw new s(oVar.c());
            } catch (Exception e) {
                e = e;
                try {
                    e.printStackTrace();
                    throw new s(e);
                } catch (Throwable th) {
                    th = th;
                    a(oVar);
                    throw th;
                }
            }
        } catch (Exception e2) {
            e = e2;
            oVar = null;
        } catch (Throwable th2) {
            th = th2;
            oVar = null;
            a(oVar);
            throw th;
        }
    }
}
