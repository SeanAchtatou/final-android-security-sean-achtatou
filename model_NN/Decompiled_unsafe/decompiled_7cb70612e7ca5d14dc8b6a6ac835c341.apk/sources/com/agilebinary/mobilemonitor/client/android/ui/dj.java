package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.View;

final class dj implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ResetPasswordActivity f317a;

    dj(ResetPasswordActivity resetPasswordActivity) {
        this.f317a = resetPasswordActivity;
    }

    public void onClick(View view) {
        this.f317a.setResult(0);
        this.f317a.finish();
    }
}
