package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.AlertDialog;
import android.widget.CompoundButton;
import com.agilebinary.phonebeagle.client.R;

final class aa implements CompoundButton.OnCheckedChangeListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppBlockActivity f222a;
    final /* synthetic */ z b;

    aa(z zVar, AppBlockActivity appBlockActivity) {
        this.b = zVar;
        this.f222a = appBlockActivity;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.client.android.ui.AppBlockActivity.a(com.agilebinary.mobilemonitor.client.android.ui.AppBlockActivity, boolean):boolean
     arg types: [com.agilebinary.mobilemonitor.client.android.ui.AppBlockActivity, int]
     candidates:
      com.agilebinary.mobilemonitor.client.android.ui.AppBlockActivity.a(com.agilebinary.mobilemonitor.client.android.ui.AppBlockActivity, int):int
      com.agilebinary.mobilemonitor.client.android.ui.AppBlockActivity.a(com.agilebinary.mobilemonitor.client.android.ui.AppBlockActivity, com.agilebinary.mobilemonitor.client.android.ui.a.f):void
      com.agilebinary.mobilemonitor.client.android.ui.AppBlockActivity.a(com.agilebinary.mobilemonitor.client.android.ui.AppBlockActivity, boolean):boolean */
    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (this.b.g.f) {
            return;
        }
        if (this.b.h.g.d()) {
            if (!z) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this.b.h);
                builder.setMessage(this.b.h.getString(R.string.label_event_application_blacklist_warning_demo, new Object[]{this.b.g.b})).setCancelable(true).setNeutralButton(this.b.h.getString(R.string.label_ok), new ab(this));
                builder.create().show();
                return;
            }
            this.b.g.a(false);
            this.b.a(this.b.g);
        } else if (!z) {
            AlertDialog.Builder builder2 = new AlertDialog.Builder(this.b.h);
            builder2.setMessage(this.b.h.getString(R.string.label_event_application_blacklist_warning, new Object[]{this.b.g.b})).setCancelable(true).setPositiveButton(this.b.h.getString(R.string.label_yes), new ad(this)).setNegativeButton(this.b.h.getString(R.string.label_no), new ac(this));
            builder2.create().show();
        } else {
            this.b.g.a(false);
            boolean unused = this.b.h.u = true;
            this.b.a(this.b.g);
            this.b.h.m();
        }
    }
}
