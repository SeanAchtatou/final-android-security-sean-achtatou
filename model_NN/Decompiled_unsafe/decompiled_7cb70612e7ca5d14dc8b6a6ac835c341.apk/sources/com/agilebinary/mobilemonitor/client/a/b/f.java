package com.agilebinary.mobilemonitor.client.a.b;

import com.agilebinary.mobilemonitor.client.a.a.a;
import com.agilebinary.mobilemonitor.client.a.b;

public abstract class f extends g {

    /* renamed from: a  reason: collision with root package name */
    protected long f129a;
    protected long b;
    protected byte c;
    protected String i;

    public f(String str, long j, long j2, a aVar, b bVar) {
        super(str, j, j2, aVar, bVar);
        this.f129a = bVar.a(aVar.d());
        this.b = bVar.a(aVar.d());
        this.c = aVar.b();
        this.i = aVar.g();
    }

    public long a() {
        return this.f129a;
    }

    public long b() {
        return this.b;
    }

    public byte c() {
        return this.c;
    }
}
