package com.agilebinary.mobilemonitor.client.android.c;

import android.content.Context;
import android.content.SharedPreferences;

public abstract class b {

    /* renamed from: a  reason: collision with root package name */
    protected SharedPreferences f174a;

    public b(Context context, String str) {
        this.f174a = context.getSharedPreferences(str, 0);
    }

    public long a(String str, long j) {
        return this.f174a.getLong(str, j);
    }

    public boolean a(String str, boolean z) {
        return this.f174a.getBoolean(str, z);
    }

    public void b(String str, long j) {
        SharedPreferences.Editor edit = this.f174a.edit();
        edit.putLong(str, j);
        edit.commit();
    }

    public void b(String str, boolean z) {
        SharedPreferences.Editor edit = this.f174a.edit();
        edit.putBoolean(str, z);
        edit.commit();
    }
}
