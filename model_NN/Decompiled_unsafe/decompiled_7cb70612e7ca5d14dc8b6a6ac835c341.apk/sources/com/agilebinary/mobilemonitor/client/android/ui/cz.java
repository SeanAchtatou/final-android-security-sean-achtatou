package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Dialog;
import android.view.View;

final class cz implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Dialog f306a;
    final /* synthetic */ MapActivity_OSM b;

    cz(MapActivity_OSM mapActivity_OSM, Dialog dialog) {
        this.b = mapActivity_OSM;
        this.f306a = dialog;
    }

    public void onClick(View view) {
        this.b.q.b(!this.b.q.a());
        this.b.m.invalidate();
        this.b.u.b("MAP_LAYER_DIRECTIONS__BOOL", this.b.q.a());
        this.f306a.dismiss();
    }
}
