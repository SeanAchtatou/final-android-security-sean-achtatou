package com.agilebinary.mobilemonitor.client.android;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import com.agilebinary.mobilemonitor.client.a.b;
import com.agilebinary.mobilemonitor.client.a.e;
import com.agilebinary.mobilemonitor.client.android.b.a;
import com.agilebinary.mobilemonitor.client.android.b.a.l;
import com.agilebinary.mobilemonitor.client.android.b.b.p;
import com.agilebinary.mobilemonitor.client.android.b.m;
import com.agilebinary.mobilemonitor.client.android.b.n;
import com.agilebinary.mobilemonitor.client.android.b.x;
import com.agilebinary.mobilemonitor.client.android.c.c;
import com.agilebinary.mobilemonitor.client.android.d.d;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.mobilemonitor.client.android.d.g;
import com.agilebinary.mobilemonitor.client.android.ui.LoginActivity;
import com.agilebinary.mobilemonitor.client.android.ui.MainActivity;
import com.agilebinary.mobilemonitor.client.android.ui.ah;
import com.agilebinary.phonebeagle.client.R;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MyApplication extends Application {

    /* renamed from: a  reason: collision with root package name */
    public static e f139a = new g();
    /* access modifiers changed from: private */
    public static final String b = f.a("MyApplication");
    private static b k;
    private f c;
    private n d;
    private boolean e = true;
    private a f;
    private a g;
    private c h;
    private int i = 0;
    private Map j = new HashMap();

    public static b g() {
        return k;
    }

    private void k() {
        this.g = new a(this);
        k = d() ? new h(this) : new i(this);
    }

    private f l() {
        return new f(new com.agilebinary.mobilemonitor.client.android.b.c.a("DEMO", "DEMO", System.currentTimeMillis(), Long.MAX_VALUE, "DEMO", "demo@phonebeagle.com", "", "01234567890ABCDEF", true, System.currentTimeMillis(), true, System.currentTimeMillis(), "applicationCategoryString", "applicationIdString", "applicationPlatformString", "applicationVersionString", "activationHistoryString", "linkKeyListString", true, true, true, true, true, true, true, true, true), true);
    }

    private void m() {
        com.agilebinary.mobilemonitor.a.a.a.b.b(b, "loadAccount");
        try {
            FileInputStream openFileInput = openFileInput("account");
            ObjectInputStream objectInputStream = new ObjectInputStream(openFileInput);
            this.e = objectInputStream.readBoolean();
            this.c = new f((com.agilebinary.mobilemonitor.client.android.b.c.a) objectInputStream.readObject(), this.e);
            objectInputStream.close();
            openFileInput.close();
        } catch (Exception e2) {
            com.agilebinary.mobilemonitor.a.a.a.b.b(b, " exception reading account data from account");
        }
    }

    private void n() {
        com.agilebinary.mobilemonitor.a.a.a.b.b(b, "saveAccount");
        try {
            if (this.c != null) {
                FileOutputStream openFileOutput = openFileOutput("account", 0);
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(openFileOutput);
                objectOutputStream.writeBoolean(this.e);
                objectOutputStream.writeObject(this.c.a());
                objectOutputStream.close();
                openFileOutput.close();
                return;
            }
            o();
        } catch (Exception e2) {
            com.agilebinary.mobilemonitor.a.a.a.b.e(b, " exception creating/writing account data to account", e2);
        }
    }

    private void o() {
        com.agilebinary.mobilemonitor.a.a.a.b.b(b, "clearAccount");
        try {
            deleteFile("account");
        } catch (Exception e2) {
            com.agilebinary.mobilemonitor.a.a.a.b.e(b, " exception deleting account", e2);
        }
    }

    public f a() {
        return this.c;
    }

    public void a(byte b2, int i2) {
        this.j.put(Byte.valueOf(b2), Integer.valueOf(i2));
    }

    public void a(Activity activity, com.agilebinary.mobilemonitor.client.android.b.c.a aVar) {
        a().a(aVar);
        n();
        ah.k();
        e().b();
        activity.startActivity(new Intent(this, MainActivity.class));
    }

    public void a(Context context) {
        com.agilebinary.mobilemonitor.a.a.a.b.b(b, "logout...");
        ah.k();
        e().b();
        f();
        o();
        this.c = null;
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(67108864);
        com.agilebinary.mobilemonitor.a.a.a.b.b(b, "starting LoginActivity from logout");
        context.startActivity(intent);
        com.agilebinary.mobilemonitor.a.a.a.b.b(b, "...logout");
    }

    public void a(f fVar) {
        this.c = fVar;
        n();
    }

    public void a(com.agilebinary.mobilemonitor.client.android.ui.a.a aVar) {
        this.c.c(aVar.b.a(), aVar.f204a);
        n();
    }

    public boolean a(byte b2) {
        Integer num = (Integer) this.j.get(Byte.valueOf(b2));
        return num != null && num.intValue() > 0;
    }

    public boolean a(String str, String str2, m mVar) {
        com.agilebinary.mobilemonitor.a.a.a.b.a(b, "login", str);
        if (str == null) {
            this.d = new l();
            this.c = l();
            this.e = true;
            k();
            n();
            return true;
        }
        this.d = new p();
        this.f = this.d.a();
        try {
            this.c = new f(this.f.a(str, d.a(str2), mVar), false);
            this.e = false;
            n();
            k();
            return true;
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            this.f = null;
            return false;
        }
    }

    public n b() {
        return this.d;
    }

    public Integer b(byte b2) {
        return (Integer) this.j.get(Byte.valueOf(b2));
    }

    public void b(com.agilebinary.mobilemonitor.client.android.ui.a.a aVar) {
        this.c.b(aVar.b.a(), aVar.f204a);
        n();
    }

    public void c(com.agilebinary.mobilemonitor.client.android.ui.a.a aVar) {
        this.c.a(aVar.b.a(), aVar.f204a);
        n();
    }

    public boolean c() {
        return this.c != null;
    }

    public boolean c(byte b2) {
        return ((Integer) this.j.get(Byte.valueOf(b2))) == null;
    }

    public boolean d() {
        return this.e;
    }

    public synchronized c e() {
        if (this.h == null) {
            this.h = new c(this, this.g);
        }
        this.i++;
        com.agilebinary.mobilemonitor.a.a.a.b.a(b, "getEventStore", "mEventStoreAllocationCount=" + this.i);
        return this.h;
    }

    public synchronized void f() {
        this.i--;
        com.agilebinary.mobilemonitor.a.a.a.b.a(b, "releaseEventStore", "mEventStoreAllocationCount=" + this.i);
        if (this.i == 0) {
            this.h.a();
            this.h = null;
        }
    }

    public void h() {
        for (byte valueOf : c.b) {
            this.j.remove(Byte.valueOf(valueOf));
        }
    }

    public StringBuffer i() {
        try {
            if (this.c == null || this.c.a() == null) {
                return null;
            }
            ArrayList<com.agilebinary.mobilemonitor.client.android.b.c.a> arrayList = new ArrayList<>();
            arrayList.add(this.c.a());
            if (this.c.b() != null) {
                arrayList.addAll(this.c.b());
            }
            StringBuffer stringBuffer = new StringBuffer();
            for (com.agilebinary.mobilemonitor.client.android.b.c.a aVar : arrayList) {
                long v = aVar.v() - System.currentTimeMillis();
                String c2 = aVar.c();
                String a2 = aVar.a();
                if (!(c2 == null || c2.trim().length() == 0)) {
                    a2 = c2 + " [" + a2 + "]";
                }
                if (v < 0) {
                    if (stringBuffer.length() > 0) {
                        stringBuffer.append("\n");
                    }
                    stringBuffer.append(getString(R.string.label_account_expired_warning_message, new Object[]{a2}));
                } else if (v < 1296000000) {
                    if (stringBuffer.length() > 0) {
                        stringBuffer.append("\n");
                    }
                    stringBuffer.append(getString(R.string.label_account_expiry_warning_message, new Object[]{a2, Integer.valueOf((int) (v / 86400000)), g.a(this).b(aVar.v())}));
                }
            }
            if (stringBuffer.length() > 0) {
                stringBuffer.append("\n\n");
                stringBuffer.append(getString(R.string.label_account_expiry_warning_link_message, new Object[]{x.a().g()}));
            }
            return stringBuffer;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public void onCreate() {
        super.onCreate();
        com.agilebinary.mobilemonitor.a.a.a.b.f122a = new com.agilebinary.mobilemonitor.client.android.a.a();
        com.agilebinary.mobilemonitor.a.a.a.b.b(b, "onCreate");
        try {
            m();
            if (this.c != null) {
                this.d = this.e ? new l() : new p();
                k();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            System.exit(0);
        }
    }

    public void onLowMemory() {
        super.onLowMemory();
        if (this.h != null) {
            this.h.e();
        }
    }
}
