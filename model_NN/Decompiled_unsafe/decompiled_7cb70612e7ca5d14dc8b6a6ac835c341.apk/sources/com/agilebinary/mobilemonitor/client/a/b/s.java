package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import android.graphics.Bitmap;
import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.a.a.a;
import com.agilebinary.mobilemonitor.client.a.e;
import com.agilebinary.mobilemonitor.client.a.f;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;

public class s implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    protected String f133a;
    protected String b;
    protected String c;
    protected byte[] d;
    protected int e = -1;
    protected String f;
    protected boolean g;
    final /* synthetic */ r h;

    public s(r rVar, a aVar, e eVar, Context context, ByteArrayOutputStream byteArrayOutputStream) {
        this.h = rVar;
        b.a(r.w, "CONSTRUCTOR PART from stream...");
        this.f133a = aVar.g();
        this.b = aVar.g();
        this.c = aVar.g();
        this.g = aVar.a();
        if (this.g) {
            this.f = aVar.g();
        } else {
            this.e = aVar.c();
            if (eVar.a(this.f133a, this.e) == f.BITMAP) {
                byte[] bArr = new byte[this.e];
                b.a(r.w, "readfully " + this.e + " bytes...");
                aVar.a(bArr);
                b.a(r.w, "...readfully");
                Bitmap[] a2 = r.b(context, bArr, 50, 480);
                b.a(r.w, "...createdAndScaled");
                byteArrayOutputStream.reset();
                b.a(r.w, "compress");
                a2[1].compress(Bitmap.CompressFormat.JPEG, 50, byteArrayOutputStream);
                b.a(r.w, "...compressed");
                this.d = byteArrayOutputStream.toByteArray();
                b.b(r.w, "...toByteArray");
                if (rVar.K == null) {
                    byteArrayOutputStream.reset();
                    a2[0].compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                    byte[] unused = rVar.K = byteArrayOutputStream.toByteArray();
                    b.b(r.w, "...thumbnail processes");
                }
            } else {
                b.a(r.w, "skipping " + this.e + " bytes...");
                r.a(aVar, this.e);
                b.a(r.w, "...skipped");
            }
        }
        b.a(r.w, "...CONSTRUCTOR PART from stream");
    }

    public String a() {
        return this.f133a;
    }

    public String b() {
        return this.c;
    }

    public byte[] c() {
        return this.d;
    }

    public boolean d() {
        return this.g;
    }

    public int e() {
        if (!this.g) {
            return this.e;
        }
        if (this.f == null) {
            return 0;
        }
        return this.f.length();
    }

    public String f() {
        return this.f;
    }
}
