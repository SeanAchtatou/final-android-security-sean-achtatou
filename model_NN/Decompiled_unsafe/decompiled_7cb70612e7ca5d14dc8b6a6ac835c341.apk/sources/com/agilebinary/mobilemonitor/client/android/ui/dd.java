package com.agilebinary.mobilemonitor.client.android.ui;

import android.os.AsyncTask;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.a.b.e;
import com.agilebinary.mobilemonitor.client.a.b.m;
import com.agilebinary.mobilemonitor.client.a.d;
import com.agilebinary.mobilemonitor.client.android.ui.b.a;
import com.agilebinary.mobilemonitor.client.android.ui.b.c;
import com.agilebinary.mobilemonitor.client.android.ui.b.h;
import com.agilebinary.mobilemonitor.client.android.ui.b.i;
import com.agilebinary.phonebeagle.client.R;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class dd extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ MapActivity_OSM f311a;

    dd(MapActivity_OSM mapActivity_OSM) {
        this.f311a = mapActivity_OSM;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(List... listArr) {
        try {
            List unused = this.f311a.t = this.f311a.h.a(listArr[0]);
            if (!isCancelled()) {
                Iterator it = this.f311a.t.iterator();
                while (it.hasNext()) {
                    e eVar = (e) it.next();
                    if (((d) eVar).c() == null) {
                        b.e("FOO", "aaargh, mappable point is null " + eVar.h() + "  / " + eVar.d(this.f311a) + " / " + eVar.e(this.f311a));
                        if (eVar instanceof m) {
                            b.e("FOO", "CID =" + ((m) eVar).k());
                        }
                        it.remove();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        dd unused2 = this.f311a.C = (dd) null;
        return null;
    }

    public void a() {
        cancel(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.client.android.c.b.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.agilebinary.mobilemonitor.client.android.c.b.a(java.lang.String, long):long
      com.agilebinary.mobilemonitor.client.android.c.b.a(java.lang.String, boolean):boolean */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Void voidR) {
        if (!isCancelled()) {
            i unused = this.f311a.p = new i(this.f311a, this.f311a.n);
            this.f311a.m.getOverlays().add(this.f311a.p);
            this.f311a.p.a(((float) (this.f311a.getResources().getDisplayMetrics().widthPixels / 2)) - (this.f311a.getResources().getDisplayMetrics().xdpi / 2.0f), 10.0f);
            this.f311a.p.b(this.f311a.u.a("MAP_LAYER_SCALEBAR__BOOL", true));
            c unused2 = this.f311a.q = new c(this.f311a, this.f311a.m);
            this.f311a.m.getOverlays().add(this.f311a.q);
            this.f311a.q.a(this.f311a.t);
            this.f311a.q.b(false);
            a unused3 = this.f311a.r = new a(this.f311a);
            this.f311a.m.getOverlays().add(this.f311a.r);
            this.f311a.r.a(this.f311a.t);
            this.f311a.r.b(this.f311a.u.a("MAP_LAYER_ACCURACY__BOOL", true));
            ArrayList arrayList = new ArrayList(this.f311a.t.size());
            for (e dhVar : this.f311a.t) {
                try {
                    arrayList.add(new dh(dhVar, this.f311a));
                } catch (Exception e) {
                    b.e("Foobar", "mapactivityosm1");
                }
            }
            h unused4 = this.f311a.s = new h(this.f311a, arrayList, this.f311a, this.f311a.n);
            this.f311a.s.a(false);
            this.f311a.m.getOverlays().add(this.f311a.s);
            ImageView imageView = new ImageView(this.f311a);
            imageView.setImageResource(R.drawable.zoom_in);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(11);
            layoutParams.addRule(10);
            this.f311a.l.addView(imageView, layoutParams);
            imageView.setOnClickListener(new de(this));
            ImageView imageView2 = new ImageView(this.f311a);
            imageView2.setImageResource(R.drawable.zoom_out);
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams2.addRule(9);
            layoutParams2.addRule(10);
            this.f311a.l.addView(imageView2, layoutParams2);
            imageView2.setOnClickListener(new df(this));
            this.f311a.l.postDelayed(new dg(this), 500);
        }
    }
}
