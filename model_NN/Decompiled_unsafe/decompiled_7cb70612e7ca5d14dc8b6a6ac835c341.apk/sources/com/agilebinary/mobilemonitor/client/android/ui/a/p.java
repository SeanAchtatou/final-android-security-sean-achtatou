package com.agilebinary.mobilemonitor.client.android.ui.a;

import android.os.AsyncTask;
import android.os.PowerManager;
import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.b.d;
import com.agilebinary.mobilemonitor.client.android.b.o;
import com.agilebinary.mobilemonitor.client.android.b.s;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.mobilemonitor.client.android.ui.AddressbookActivity;
import java.util.ArrayList;
import java.util.List;

public class p extends AsyncTask {

    /* renamed from: a  reason: collision with root package name */
    static final String f219a = f.a("SyncAddressbookTask");
    private d b;
    private AddressbookActivity c;
    private boolean d;

    public p(AddressbookActivity addressbookActivity, d dVar) {
        b.b(f219a, "<CONSTRUCTOR>");
        this.c = addressbookActivity;
        this.b = dVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public g doInBackground(List... listArr) {
        g gVar;
        b.b(f219a, "SyncAddressbookTask.doInBackground...");
        PowerManager.WakeLock newWakeLock = ((PowerManager) this.c.getSystemService("power")).newWakeLock(6, f219a);
        newWakeLock.acquire();
        try {
            if (isCancelled()) {
                try {
                    newWakeLock.release();
                } catch (Exception e) {
                }
                return null;
            }
            List list = listArr[0];
            if (list != null) {
                b.b(f219a, "uploading blocked contacts list");
                o b2 = this.b.b(this.c.g.a(), list);
                if (b2 != null && b2.d()) {
                    throw new s(b2.c());
                }
            } else {
                b.b(f219a, "NOT uploading blacklist, no local changes! ");
            }
            ArrayList arrayList = new ArrayList();
            o a2 = this.b.a(this.c.g.a(), arrayList);
            if (a2 != null && a2.d()) {
                throw new s(a2.c());
            } else if (isCancelled()) {
                try {
                    newWakeLock.release();
                } catch (Exception e2) {
                }
                return null;
            } else {
                gVar = new g(arrayList);
                try {
                    newWakeLock.release();
                } catch (Exception e3) {
                }
                b.a(f219a, "...DownloadTask.doInBackground: ", "" + gVar);
                return gVar;
            }
        } catch (s e4) {
            gVar = new g(e4);
            try {
                newWakeLock.release();
            } catch (Exception e5) {
            }
        } catch (Throwable th) {
            try {
                newWakeLock.release();
            } catch (Exception e6) {
            }
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(g gVar) {
        b.b(f219a, "onPostExecute...");
        if (!isCancelled()) {
            AddressbookActivity.a(this.c, gVar);
        }
        b.b(f219a, "...onPostExecute.");
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        b.b(f219a, "onCancelled...");
        try {
            if (!this.d) {
                AddressbookActivity.a(this.c, (g) null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        b.b(f219a, "...onCancelled");
    }
}
