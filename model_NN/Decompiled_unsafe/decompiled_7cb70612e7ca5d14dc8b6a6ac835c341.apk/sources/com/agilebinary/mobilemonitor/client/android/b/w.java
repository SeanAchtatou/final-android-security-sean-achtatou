package com.agilebinary.mobilemonitor.client.android.b;

import com.agilebinary.a.a.b.e.f;
import com.agilebinary.a.a.b.i;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

class w extends f {
    public w(i iVar) {
        super(iVar);
    }

    public InputStream a() {
        return new GZIPInputStream(this.c.a());
    }

    public long b() {
        return -1;
    }
}
