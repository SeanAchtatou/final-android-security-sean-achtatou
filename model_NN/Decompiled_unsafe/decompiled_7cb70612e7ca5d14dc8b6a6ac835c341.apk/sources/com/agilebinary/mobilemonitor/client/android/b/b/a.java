package com.agilebinary.mobilemonitor.client.android.b.b;

import com.agilebinary.mobilemonitor.client.android.b.o;
import com.agilebinary.mobilemonitor.client.android.b.t;
import com.agilebinary.mobilemonitor.client.android.d.f;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class a {
    private static final String b = f.a("BaseProductionDataService");

    /* renamed from: a  reason: collision with root package name */
    protected t f149a = t.a();

    public String a(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return str;
        }
    }

    /* access modifiers changed from: protected */
    public void a(o oVar) {
        if (oVar != null) {
            oVar.f();
        }
    }

    /* access modifiers changed from: protected */
    public String b(o oVar) {
        if (oVar != null) {
            return oVar.h();
        }
        return null;
    }
}
