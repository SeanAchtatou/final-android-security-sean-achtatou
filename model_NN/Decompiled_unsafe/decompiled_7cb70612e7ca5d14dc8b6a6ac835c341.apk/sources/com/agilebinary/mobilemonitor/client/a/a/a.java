package com.agilebinary.mobilemonitor.client.a.a;

import java.io.EOFException;
import java.io.FilterInputStream;
import java.io.InputStream;

public class a extends FilterInputStream {
    public a(InputStream inputStream) {
        super(inputStream);
    }

    private final int h() {
        InputStream inputStream = this.in;
        int read = inputStream.read();
        int read2 = inputStream.read();
        if ((read | read2) < 0) {
            throw new EOFException();
        }
        return (read2 << 0) + (read << 8);
    }

    public final void a(byte[] bArr) {
        int i = 0;
        do {
            int read = this.in.read(bArr, i, bArr.length - i);
            if (read != -1) {
                i += read;
            } else {
                return;
            }
        } while (i != bArr.length);
    }

    public final boolean a() {
        int read = this.in.read();
        if (read >= 0) {
            return read != 0;
        }
        throw new EOFException();
    }

    public final byte b() {
        int read = this.in.read();
        if (read >= 0) {
            return (byte) read;
        }
        throw new EOFException();
    }

    public final int c() {
        InputStream inputStream = this.in;
        int read = inputStream.read();
        int read2 = inputStream.read();
        int read3 = inputStream.read();
        int read4 = inputStream.read();
        if ((read | read2 | read3 | read4) < 0) {
            throw new EOFException();
        }
        return (read4 << 0) + (read << 24) + (read2 << 16) + (read3 << 8);
    }

    public final long d() {
        InputStream inputStream = this.in;
        return (((long) c()) << 32) + (((long) c()) & 4294967295L);
    }

    public final double e() {
        int i = 0;
        char[] cArr = new char[this.in.read()];
        int length = (cArr.length & 1) + (cArr.length / 2);
        for (int i2 = 0; i2 < length; i2++) {
            int read = this.in.read();
            cArr[i] = (char) ((read >> 4) + 45);
            if (cArr[i] == '/') {
                cArr[i] = 'E';
            }
            i++;
            if (i < cArr.length) {
                cArr[i] = (char) ((read & 15) + 45);
                if (cArr[i] == '/') {
                    cArr[i] = 'E';
                }
                i++;
            }
        }
        try {
            return Double.valueOf(new String(cArr)).doubleValue();
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return 0.0d;
        }
    }

    public final String[] f() {
        String[] strArr = new String[c()];
        for (int i = 0; i < strArr.length; i++) {
            strArr[i] = g();
        }
        return strArr;
    }

    public final String g() {
        if (b() == 1) {
            return null;
        }
        int h = h();
        char[] cArr = new char[h];
        byte[] bArr = new byte[h];
        read(bArr, 0, h);
        int i = 0;
        int i2 = 0;
        while (i2 < h) {
            byte b = bArr[i2] & 255;
            switch (b >> 4) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    cArr[i] = (char) b;
                    i++;
                    i2++;
                    break;
                case 8:
                case 9:
                case 10:
                case 11:
                default:
                    throw new IllegalArgumentException();
                case 12:
                case 13:
                    int i3 = i2 + 2;
                    if (i3 <= h) {
                        byte b2 = bArr[i3 - 1];
                        if ((b2 & 192) == 128) {
                            cArr[i] = (char) (((b & 31) << 6) | (b2 & 63));
                            i++;
                            i2 = i3;
                            break;
                        } else {
                            throw new IllegalArgumentException();
                        }
                    } else {
                        throw new IllegalArgumentException();
                    }
                case 14:
                    int i4 = i2 + 3;
                    if (i4 <= h) {
                        byte b3 = bArr[i4 - 2];
                        byte b4 = bArr[i4 - 1];
                        if ((b3 & 192) == 128 && (b4 & 192) == 128) {
                            cArr[i] = (char) (((b & 15) << 12) | ((b3 & 63) << 6) | ((b4 & 63) << 0));
                            i++;
                            i2 = i4;
                            break;
                        } else {
                            throw new IllegalArgumentException();
                        }
                    } else {
                        throw new IllegalArgumentException();
                    }
            }
        }
        return new String(cArr, 0, i);
    }

    public final int read(byte[] bArr, int i, int i2) {
        return this.in.read(bArr, i, i2);
    }
}
