package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.a;
import com.agilebinary.mobilemonitor.client.a.b;
import com.agilebinary.phonebeagle.client.R;

public class i extends g {
    protected String A;
    protected String B;
    protected String C;
    protected String D;

    /* renamed from: a  reason: collision with root package name */
    protected long f131a;
    protected int b;
    protected String c;
    protected String i;
    protected String w;
    protected String x;
    protected String y;
    protected String z;

    public i(String str, long j, long j2, a aVar, b bVar) {
        super(str, j, j2, aVar, bVar);
        this.b = aVar.c();
        this.f131a = bVar.a(aVar.d());
        this.c = aVar.g();
        this.i = aVar.g();
        this.w = aVar.g();
        this.x = aVar.g();
        this.y = aVar.g();
        this.z = aVar.g();
        this.A = aVar.g();
        this.B = aVar.g();
        this.C = aVar.g();
        this.D = aVar.g();
    }

    public int a() {
        return this.b;
    }

    public String a(Context context) {
        switch (a()) {
            case 1:
                Object[] objArr = new Object[3];
                objArr[0] = this.c;
                objArr[1] = this.w == null ? "" : this.w;
                objArr[2] = this.x;
                return context.getString(R.string.label_event_facebook_msg, objArr);
            default:
                return "";
        }
    }

    public String b() {
        return this.c;
    }

    public String b(Context context) {
        return this.i;
    }

    public String c() {
        return this.i;
    }

    public byte d() {
        return 15;
    }

    public String m() {
        return this.w;
    }
}
