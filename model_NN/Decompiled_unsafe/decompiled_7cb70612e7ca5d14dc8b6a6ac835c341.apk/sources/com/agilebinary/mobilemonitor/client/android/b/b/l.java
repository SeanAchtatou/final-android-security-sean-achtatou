package com.agilebinary.mobilemonitor.client.android.b.b;

import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.b.g;
import com.agilebinary.mobilemonitor.client.android.b.h;
import com.agilebinary.mobilemonitor.client.android.b.i;
import com.agilebinary.mobilemonitor.client.android.b.m;
import com.agilebinary.mobilemonitor.client.android.b.o;
import com.agilebinary.mobilemonitor.client.android.b.s;
import com.agilebinary.mobilemonitor.client.android.b.x;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.mobilemonitor.client.android.e;
import java.io.DataInputStream;
import java.io.IOException;

public class l extends a implements g {
    private static final String b = f.a("ProductionEventDataService");

    public o a(com.agilebinary.mobilemonitor.client.android.f fVar, e eVar, h hVar, i iVar, m mVar) {
        o oVar;
        try {
            b.a(b, "getEvents,", eVar.toString());
            o oVar2 = null;
            try {
                String format = String.format(x.a().d() + "ServiceEventRetrieveBinary?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s&Type=%4$s&FromDate=%5$s&ToDate=%6$s", "1", a(fVar.c()), a(fVar.d()), Byte.valueOf(eVar.f184a), Long.valueOf(eVar.b), Long.valueOf(eVar.c));
                b.a(b, "URL: ", format);
                oVar = this.f149a.a(format, x.a().n(), mVar);
                try {
                    if (oVar.b()) {
                        DataInputStream dataInputStream = new DataInputStream(oVar.e());
                        hVar.a(dataInputStream.readByte(), dataInputStream.readInt(), dataInputStream.readLong());
                        int readInt = dataInputStream.readInt();
                        b.b(b, "NumEvents: " + readInt);
                        for (int i = 0; i < readInt; i++) {
                            b.b(b, "reading Event " + i);
                            String readUTF = dataInputStream.readUTF();
                            byte readByte = dataInputStream.readByte();
                            long readLong = dataInputStream.readLong();
                            long readLong2 = dataInputStream.readLong();
                            String readUTF2 = dataInputStream.readUTF();
                            boolean readBoolean = dataInputStream.readBoolean();
                            boolean readBoolean2 = dataInputStream.readBoolean();
                            boolean readBoolean3 = dataInputStream.readBoolean();
                            int readInt2 = dataInputStream.readInt();
                            if (readBoolean3) {
                                byte[] bArr = new byte[readInt2];
                                dataInputStream.readFully(bArr);
                                b.a(b, "ignored JSON formatted event: ", new String(bArr));
                            } else {
                                hVar.a(readUTF, readByte, readLong, readLong2, readUTF2, readBoolean, readBoolean2, readInt2, dataInputStream);
                                iVar.a_();
                            }
                        }
                        hVar.a(false);
                        a(oVar);
                        b.b(b, "...getEvents");
                        return oVar;
                    }
                    throw new s(oVar.c());
                } catch (Exception e) {
                    e.printStackTrace();
                    hVar.a(true);
                    throw new s(e);
                } catch (IOException e2) {
                    e = e2;
                    oVar2 = oVar;
                    try {
                        throw new s(e);
                    } catch (Throwable th) {
                        th = th;
                        oVar = oVar2;
                        a(oVar);
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    a(oVar);
                    throw th;
                }
            } catch (IOException e3) {
                e = e3;
            } catch (Throwable th3) {
                th = th3;
                oVar = null;
                a(oVar);
                throw th;
            }
        } catch (Exception e4) {
            throw new s(e4);
        }
    }
}
