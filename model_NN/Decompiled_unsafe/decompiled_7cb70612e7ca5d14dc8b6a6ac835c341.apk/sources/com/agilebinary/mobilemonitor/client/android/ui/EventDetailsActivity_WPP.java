package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.a.b.b;
import com.agilebinary.mobilemonitor.client.a.b.x;
import com.agilebinary.phonebeagle.client.R;

public class EventDetailsActivity_WPP extends aw {
    private TextView n;
    private TextView o;
    private TextView p;
    private TextView q;
    private TextView r;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public void a(ViewGroup viewGroup) {
        ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.eventdetails_wpp, viewGroup, true);
        this.n = (TextView) findViewById(R.id.eventdetails_wpp_remoteparty);
        this.q = (TextView) findViewById(R.id.eventdetails_wpp_time);
        this.o = (TextView) findViewById(R.id.eventdetails_wpp_speed);
        this.p = (TextView) findViewById(R.id.eventdetails_wpp_message);
        this.r = (TextView) findViewById(R.id.eventdetails_wpp_fromto);
    }

    /* access modifiers changed from: protected */
    public void a(b bVar) {
        super.a(bVar);
        x xVar = (x) bVar;
        System.out.println("ARGHLINE1: " + xVar.b(this));
        this.r.setText(getResources().getString(xVar.n() == 1 ? R.string.label_event_from : R.string.label_event_to));
        this.q.setText(xVar.m());
        this.n.setText(xVar.b());
        this.o.setText(com.agilebinary.mobilemonitor.client.android.d.b.a(this, xVar));
        this.p.setText(xVar.c());
        if (com.agilebinary.mobilemonitor.client.android.d.b.a(xVar)) {
            this.o.setTextColor(getResources().getColor(R.color.warning));
        } else {
            this.o.setTextColor(getResources().getColor(R.color.text));
        }
    }
}
