package com.agilebinary.mobilemonitor.client.android.ui.a;

import android.os.AsyncTask;
import android.os.PowerManager;
import com.agilebinary.a.a.b.b.a.g;
import com.agilebinary.mobilemonitor.client.android.b.b.b;
import com.agilebinary.mobilemonitor.client.android.b.m;
import com.agilebinary.mobilemonitor.client.android.b.s;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.mobilemonitor.client.android.ui.ResetPasswordActivity;
import com.agilebinary.mobilemonitor.client.android.ui.ah;
import java.util.Locale;

public class n extends AsyncTask implements m {

    /* renamed from: a  reason: collision with root package name */
    static final String f217a = f.a("ResetPasswordTask");
    public static n b;
    private ah c;
    private g d;

    public n(ah ahVar) {
        this.c = ahVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public o doInBackground(String... strArr) {
        o oVar;
        PowerManager.WakeLock newWakeLock = ((PowerManager) this.c.getSystemService("power")).newWakeLock(6, f217a);
        newWakeLock.acquire();
        try {
            new b().b(strArr[0], Locale.getDefault().getCountry(), this);
            oVar = new o(true);
            try {
                newWakeLock.release();
            } catch (Exception e) {
            }
            b = null;
        } catch (s e2) {
            oVar = new o(e2);
            try {
                newWakeLock.release();
            } catch (Exception e3) {
            }
            b = null;
        } catch (Throwable th) {
            try {
                newWakeLock.release();
            } catch (Exception e4) {
            }
            b = null;
            throw th;
        }
        return oVar;
    }

    public void a() {
        cancel(false);
        if (this.d != null) {
            try {
                this.d.i();
            } catch (Exception e) {
            }
        }
    }

    public void a(g gVar) {
        this.d = gVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(o oVar) {
        ResetPasswordActivity.a(this.c, oVar);
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        ResetPasswordActivity.a(this.c, (o) null);
    }
}
