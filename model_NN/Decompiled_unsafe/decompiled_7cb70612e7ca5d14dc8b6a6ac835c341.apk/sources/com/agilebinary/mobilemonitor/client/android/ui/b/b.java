package com.agilebinary.mobilemonitor.client.android.ui.b;

import android.graphics.Paint;

class b {

    /* renamed from: a  reason: collision with root package name */
    int f249a;
    int b;
    float c;
    Paint d;
    Paint e;
    final /* synthetic */ a f;

    public b(a aVar, int i, int i2, float f2, Paint paint, Paint paint2) {
        this.f = aVar;
        this.f249a = i;
        this.b = i2;
        this.c = f2;
        this.d = paint;
        this.e = paint2;
    }

    private a a() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        b bVar = (b) obj;
        if (!a().equals(bVar.a())) {
            return false;
        }
        if (this.d == null) {
            if (bVar.d != null) {
                return false;
            }
        } else if (!this.d.equals(bVar.d)) {
            return false;
        }
        if (this.e == null) {
            if (bVar.e != null) {
                return false;
            }
        } else if (!this.e.equals(bVar.e)) {
            return false;
        }
        if (Float.floatToIntBits(this.c) != Float.floatToIntBits(bVar.c)) {
            return false;
        }
        if (this.f249a != bVar.f249a) {
            return false;
        }
        return this.b == bVar.b;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((this.d == null ? 0 : this.d.hashCode()) + ((a().hashCode() + 31) * 31)) * 31;
        if (this.e != null) {
            i = this.e.hashCode();
        }
        return ((((((hashCode + i) * 31) + Float.floatToIntBits(this.c)) * 31) + this.f249a) * 31) + this.b;
    }
}
