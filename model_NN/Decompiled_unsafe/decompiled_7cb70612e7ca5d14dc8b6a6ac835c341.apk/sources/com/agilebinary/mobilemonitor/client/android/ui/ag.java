package com.agilebinary.mobilemonitor.client.android.ui;

import com.agilebinary.mobilemonitor.client.android.b.c;
import java.util.Comparator;

final class ag implements Comparator {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppBlockActivity f228a;
    final /* synthetic */ af b;

    ag(af afVar, AppBlockActivity appBlockActivity) {
        this.b = afVar;
        this.f228a = appBlockActivity;
    }

    /* renamed from: a */
    public int compare(c cVar, c cVar2) {
        return cVar.b.compareTo(cVar2.b);
    }
}
