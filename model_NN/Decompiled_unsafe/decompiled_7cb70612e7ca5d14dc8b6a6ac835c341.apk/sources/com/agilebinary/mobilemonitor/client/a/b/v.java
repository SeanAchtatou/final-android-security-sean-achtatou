package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.a;
import com.agilebinary.mobilemonitor.client.a.b;
import com.agilebinary.phonebeagle.client.R;

public class v extends g {

    /* renamed from: a  reason: collision with root package name */
    protected long f135a;
    protected String b;

    public v(String str, long j, long j2, a aVar, b bVar) {
        super(str, j, j2, aVar, bVar);
        this.f135a = bVar.a(aVar.d());
        this.b = aVar.g();
    }

    public long a() {
        return this.f135a;
    }

    public String a(Context context) {
        return context.getString(R.string.label_event_web_search, b());
    }

    public String b() {
        return this.b;
    }

    public String b(Context context) {
        return "";
    }

    public byte d() {
        return 13;
    }
}
