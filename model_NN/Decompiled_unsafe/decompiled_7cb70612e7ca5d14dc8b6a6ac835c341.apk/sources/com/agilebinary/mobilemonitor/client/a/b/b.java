package com.agilebinary.mobilemonitor.client.a.b;

import com.agilebinary.mobilemonitor.client.a.a.a;
import java.io.Serializable;

public abstract class b implements Serializable {
    protected long d;
    protected long e;
    protected String f;
    protected String g;
    protected String h;

    protected b(String str, long j, long j2, a aVar, com.agilebinary.mobilemonitor.client.a.b bVar) {
        this.f = str;
        this.d = j;
        this.e = j2;
        this.g = aVar.g();
        this.h = aVar.g();
    }

    public abstract byte d();

    public String e() {
        return this.g;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return this.d == ((b) obj).d;
    }

    public String f() {
        return this.h;
    }

    public long g() {
        return this.e;
    }

    public long h() {
        return this.d;
    }

    public int hashCode() {
        return ((int) (this.d ^ (this.d >>> 32))) + 31;
    }
}
