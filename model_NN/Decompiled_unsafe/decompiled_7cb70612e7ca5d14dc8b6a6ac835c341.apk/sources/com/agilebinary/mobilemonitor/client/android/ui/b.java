package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.View;
import com.agilebinary.mobilemonitor.client.android.b.c.a;

final class b implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ a f247a;
    final /* synthetic */ AccountActivity b;

    b(AccountActivity accountActivity, a aVar) {
        this.b = accountActivity;
        this.f247a = aVar;
    }

    public void onClick(View view) {
        AccountInfoActivity.a(this.b, this.f247a);
    }
}
