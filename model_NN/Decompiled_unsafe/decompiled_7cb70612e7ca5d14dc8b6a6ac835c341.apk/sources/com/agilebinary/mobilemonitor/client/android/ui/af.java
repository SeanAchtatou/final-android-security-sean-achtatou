package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.agilebinary.mobilemonitor.client.android.b.c;
import com.agilebinary.mobilemonitor.client.android.ui.a.f;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class af extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AppBlockActivity f227a;
    private List b;
    private List c = new ArrayList();

    public af(AppBlockActivity appBlockActivity, f fVar) {
        this.f227a = appBlockActivity;
        if (fVar != null) {
            this.b = fVar.b();
            Collections.sort(this.b, new ag(this, appBlockActivity));
        }
    }

    public int getCount() {
        if (this.b == null) {
            return 0;
        }
        return this.b.size();
    }

    public Object getItem(int i) {
        return this.b.get(i);
    }

    public long getItemId(int i) {
        return (long) ((c) this.b.get(i)).hashCode();
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view != null) {
            z zVar = (z) view;
            zVar.a((c) this.b.get(i));
            return zVar;
        }
        z zVar2 = new z(this.f227a, this.f227a, null);
        zVar2.a((c) this.b.get(i));
        return zVar2;
    }
}
