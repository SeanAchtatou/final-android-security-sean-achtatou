package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.agilebinary.mobilemonitor.client.android.b.e;
import com.agilebinary.mobilemonitor.client.android.ui.a.g;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class t extends BaseAdapter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AddressbookActivity f333a;
    private List b;
    private List c = new ArrayList();

    public t(AddressbookActivity addressbookActivity, g gVar) {
        this.f333a = addressbookActivity;
        if (gVar != null) {
            this.b = gVar.b();
            Collections.sort(this.b, new u(this, addressbookActivity));
        }
    }

    public int getCount() {
        if (this.b == null) {
            return 0;
        }
        return this.b.size();
    }

    public Object getItem(int i) {
        return this.b.get(i);
    }

    public long getItemId(int i) {
        return (long) ((e) this.b.get(i)).hashCode();
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view != null) {
            n nVar = (n) view;
            nVar.a((e) this.b.get(i));
            return nVar;
        }
        n nVar2 = new n(this.f333a, this.f333a, null);
        nVar2.a((e) this.b.get(i));
        return nVar2;
    }
}
