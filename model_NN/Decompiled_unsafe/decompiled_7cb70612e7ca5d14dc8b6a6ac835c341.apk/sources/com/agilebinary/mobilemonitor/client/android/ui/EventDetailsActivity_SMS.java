package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.a.b.b;
import com.agilebinary.mobilemonitor.client.a.b.t;
import com.agilebinary.mobilemonitor.client.android.d.g;
import com.agilebinary.phonebeagle.client.R;

public class EventDetailsActivity_SMS extends aw {
    private TextView n;
    private TextView o;
    private TextView p;
    private TextView q;
    private TextView r;
    private TextView s;
    private TextView t;
    private Button u;
    /* access modifiers changed from: private */
    public String v;
    /* access modifiers changed from: private */
    public String w;

    private int a(byte b) {
        switch (b) {
            case -1:
                return R.string.empty;
            case 0:
            default:
                return R.string.empty;
            case 1:
            case 2:
                return R.string.label_event_sms_incoming;
            case 3:
                return R.string.empty;
            case 4:
                return R.string.label_event_sms_outgoing_blocked;
            case 5:
                return R.string.label_event_sms_incoming_blocked;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public void a(ViewGroup viewGroup) {
        ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.eventdetails_sms, viewGroup, true);
        this.n = (TextView) findViewById(R.id.eventdetails_sms_direction);
        this.o = (TextView) findViewById(R.id.eventdetails_sms_time_received);
        this.p = (TextView) findViewById(R.id.eventdetails_sms_time_sent);
        this.t = (TextView) findViewById(R.id.eventdetails_sms_fromto);
        this.q = (TextView) findViewById(R.id.eventdetails_sms_remoteparty);
        this.r = (TextView) findViewById(R.id.eventdetails_sms_speed);
        this.s = (TextView) findViewById(R.id.eventdetails_sms_message);
        this.u = (Button) findViewById(R.id.eventdetails_sms_addblock_button);
        this.u.setOnClickListener(new av(this));
    }

    /* access modifiers changed from: protected */
    public void a(b bVar) {
        super.a(bVar);
        t tVar = (t) bVar;
        String a2 = com.agilebinary.mobilemonitor.client.android.d.b.a(tVar.n(), tVar.o());
        this.v = tVar.n();
        this.w = tVar.o();
        this.n.setText(a(tVar.c()));
        this.o.setText(g.a(this).c(tVar.b()));
        this.p.setText(g.a(this).c(tVar.a()));
        this.t.setText(getResources().getString(tVar.c() == 1 ? R.string.label_event_from : R.string.label_event_to));
        this.q.setText(a2);
        this.r.setText(com.agilebinary.mobilemonitor.client.android.d.b.a(this, tVar));
        this.s.setText(tVar.m());
        if (tVar.c() != 2 || !com.agilebinary.mobilemonitor.client.android.d.b.a(tVar)) {
            this.r.setTextColor(getResources().getColor(R.color.text));
        } else {
            this.r.setTextColor(getResources().getColor(R.color.warning));
        }
    }
}
