package com.agilebinary.mobilemonitor.client.android.b;

import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.d.c;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashSet;
import javax.net.ssl.X509TrustManager;

public class q implements X509TrustManager {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ p f166a;
    private PublicKey b;
    private boolean c;
    private HashSet d = new HashSet();

    q(p pVar, x xVar) {
        this.f166a = pVar;
        String b2 = xVar.b();
        b.a("TAG", "mPublicKey=", b2);
        if (b2.trim().length() > 0) {
            try {
                this.b = KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(c.a(b2.toCharArray())));
            } catch (Exception e) {
                b.e(p.b, "invalid public key!");
                this.c = true;
            }
        }
    }

    public void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) {
    }

    public void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) {
        b.d(p.b, "*** CHECKING A SSL SERVER CERTIFICATE. IF YOU SEE THIS MESSAGE TOO OFTEN YOU SHOULD TUNE THE keep-alive SETTINGS ON THE SERVER!!");
        if (x509CertificateArr.length == 0) {
            throw new CertificateException("no certificate in chain");
        } else if (this.c) {
            throw new CertificateException("certificate invalid because couldn't decode configured public key");
        } else if (this.b == null) {
            b.b(p.b, "certificate valid because no public key configured");
        } else {
            X509Certificate x509Certificate = x509CertificateArr[0];
            if (this.d.contains(x509Certificate)) {
                b.b(p.b, "certificate valid because cached!");
                return;
            }
            try {
                x509Certificate.verify(this.b);
                this.d.add(x509Certificate);
            } catch (InvalidKeyException e) {
                b.e(p.b, "", e);
                throw new CertificateException(e);
            } catch (NoSuchAlgorithmException e2) {
                b.e(p.b, "", e2);
                throw new CertificateException(e2);
            } catch (NoSuchProviderException e3) {
                b.e(p.b, "", e3);
                throw new CertificateException(e3);
            } catch (SignatureException e4) {
                b.e(p.b, "", e4);
                throw new CertificateException(e4);
            }
        }
    }

    public X509Certificate[] getAcceptedIssuers() {
        return null;
    }
}
