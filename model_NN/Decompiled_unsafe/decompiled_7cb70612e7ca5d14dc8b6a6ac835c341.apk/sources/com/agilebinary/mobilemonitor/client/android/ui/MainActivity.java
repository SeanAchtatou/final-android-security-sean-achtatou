package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.b.x;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.mobilemonitor.client.android.ui.a.r;
import com.agilebinary.phonebeagle.client.R;

public class MainActivity extends al implements AdapterView.OnItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    static final String f200a = f.a("MainActivity");
    protected ListView b;
    private ct i;
    /* access modifiers changed from: private */
    public Animation j;

    public MainActivity() {
        b.a(f200a, "<CONSTRUCTOR>", "" + this);
    }

    public static void a(Context context, byte b2, int i2) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("EXTRA_PROGRESS_EVENTTYPE", b2);
        intent.putExtra("EXTRA_PROGRESS_NUM", i2);
        intent.setFlags(536870912);
        context.startActivity(intent);
    }

    private void c() {
        if (r.b == null) {
            this.g.h();
            b();
            r.b = new r(this);
            r.b.execute(new Void[0]);
        }
    }

    private void l() {
        b.b(f200a, "stopDownloading...");
        if (r.b != null) {
            try {
                r.b.a();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    public int a() {
        return R.layout.main;
    }

    public void b() {
        this.b.invalidateViews();
    }

    public void finish() {
        b.b(f200a, "FINISH...");
        l();
        super.finish();
        b.b(f200a, "...FINISH");
    }

    /* access modifiers changed from: protected */
    public void j() {
        l();
        super.j();
    }

    public void onCreate(Bundle bundle) {
        b.a(f200a, "onCreate...", "" + this);
        super.onCreate(bundle);
        if (this.g.a() == null) {
            finish();
            return;
        }
        this.b = (ListView) findViewById(R.id.main_list);
        this.i = new ct(this);
        this.b.setAdapter((ListAdapter) this.i);
        this.b.setOnItemClickListener(this);
        if (!this.g.d() && this.g.a().q()) {
            this.i.a(R.drawable.shoppingcart, R.string.label_main_upgrade, (byte) -3);
        }
        if (this.g.a().g()) {
            this.i.a(R.drawable.sms, R.string.label_event_sms, (byte) 3);
        }
        if (this.g.a().m()) {
            this.i.a(R.drawable.facebook, R.string.label_event_facebook, (byte) 10);
        }
        if (this.g.a().n()) {
            this.i.a(R.drawable.whatsapp, R.string.label_event_whatsapp, (byte) 11);
        }
        if (this.g.a().o()) {
            this.i.a(R.drawable.line, R.string.label_event_line, (byte) 12);
        }
        if (this.g.a().h()) {
            this.i.a(R.drawable.call, R.string.label_event_call, (byte) 2);
        }
        if (this.g.a().i()) {
            this.i.a(R.drawable.location, R.string.label_event_location, (byte) 1);
        }
        if (this.g.a().j()) {
            this.i.a(R.drawable.mms, R.string.label_event_mms, (byte) 4);
        }
        if (this.g.a().k()) {
            this.i.a(R.drawable.web, R.string.label_event_web, (byte) 6);
        }
        if (this.g.a().l()) {
            this.i.a(R.drawable.application, R.string.label_event_application, (byte) 9);
        }
        if (this.g.d() || (this.g.a().l() && this.g.a().s() >= 156)) {
            this.i.a(R.drawable.application_block, R.string.label_application_block, (byte) -4);
        }
        this.i.a(R.drawable.addressbook, R.string.label_addressbook, (byte) -5);
        if (this.g.a().p()) {
            this.i.a(R.drawable.system, R.string.label_event_system, (byte) 8);
        }
        this.i.a(R.drawable.control, R.string.label_main_controlpanel, (byte) -2);
        this.j = AnimationUtils.loadAnimation(this, R.anim.updating_star);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        b.b(f200a, "onDestroy...");
        super.onDestroy();
    }

    public void onItemClick(AdapterView adapterView, View view, int i2, long j2) {
        cs csVar = (cs) this.i.getItem(i2);
        if (csVar.c == -2) {
            l();
            ControlPanelActivity.a(this);
        } else if (csVar.c == -3) {
            l();
            try {
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse(x.a().a(this.g.a().c()))));
            } catch (ActivityNotFoundException e) {
            }
        } else if (csVar.c == -4) {
            l();
            AppBlockActivity.a(this);
        } else if (csVar.c == -5) {
            this.g.a();
            l();
            AddressbookActivity.a(this);
        } else {
            l();
            bq.a(this, csVar.c);
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        b.b(f200a, "onNewIntent...");
        this.g.a(intent.getByteExtra("EXTRA_PROGRESS_EVENTTYPE", (byte) 0), intent.getIntExtra("EXTRA_PROGRESS_NUM", 0));
        b();
        b.b(f200a, "...onNewIntent");
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_main_refresh /*2131296483*/:
                c();
                return true;
            default:
                return super.onOptionsItemSelected(menuItem);
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        b.b(f200a, "onStart...");
        super.onStart();
        c();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        b.b(f200a, "onStop...");
        super.onStop();
    }
}
