package com.agilebinary.mobilemonitor.client.android.ui.a;

import android.os.AsyncTask;
import com.agilebinary.a.a.b.b.a.g;
import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.MyApplication;
import com.agilebinary.mobilemonitor.client.android.b.m;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.mobilemonitor.client.android.ui.MainActivity;
import com.agilebinary.mobilemonitor.client.android.ui.ah;

public class r extends AsyncTask implements m {

    /* renamed from: a  reason: collision with root package name */
    static final String f221a = f.a("UpdateNumNewEventsTask");
    public static r b;
    private final ah c;
    private final MyApplication d;
    private g e;

    public r(ah ahVar) {
        this.c = ahVar;
        this.d = ahVar.g;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x013f A[Catch:{ s -> 0x0158, all -> 0x0161 }, LOOP:0: B:3:0x0012->B:30:0x013f, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x001c A[EDGE_INSN: B:48:0x001c->B:6:0x001c ?: BREAK  , SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Void doInBackground(java.lang.Void... r15) {
        /*
            r14 = this;
            com.agilebinary.mobilemonitor.client.android.MyApplication r0 = r14.d
            com.agilebinary.mobilemonitor.client.android.c.c r8 = r0.e()
            java.lang.String r0 = com.agilebinary.mobilemonitor.client.android.ui.a.r.f221a     // Catch:{ all -> 0x0161 }
            java.lang.String r1 = "doInBackground..."
            com.agilebinary.mobilemonitor.a.a.a.b.b(r0, r1)     // Catch:{ all -> 0x0161 }
            byte[] r9 = com.agilebinary.mobilemonitor.client.android.c.c.b     // Catch:{ all -> 0x0161 }
            int r10 = r9.length     // Catch:{ all -> 0x0161 }
            r0 = 0
            r7 = r0
        L_0x0012:
            if (r7 >= r10) goto L_0x001c
            byte r2 = r9[r7]     // Catch:{ all -> 0x0161 }
            boolean r0 = r14.isCancelled()     // Catch:{ all -> 0x0161 }
            if (r0 == 0) goto L_0x002d
        L_0x001c:
            com.agilebinary.mobilemonitor.client.android.MyApplication r0 = r14.d
            r0.f()
            r0 = 0
            com.agilebinary.mobilemonitor.client.android.ui.a.r.b = r0
            java.lang.String r0 = com.agilebinary.mobilemonitor.client.android.ui.a.r.f221a
            java.lang.String r1 = "...doInBackground"
            com.agilebinary.mobilemonitor.a.a.a.b.b(r0, r1)
            r0 = 0
            return r0
        L_0x002d:
            long r0 = r8.b(r2)     // Catch:{ all -> 0x0161 }
            java.lang.String r3 = com.agilebinary.mobilemonitor.client.android.ui.a.r.f221a     // Catch:{ all -> 0x0161 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0161 }
            r4.<init>()     // Catch:{ all -> 0x0161 }
            java.lang.String r5 = "TimeOfLastssenEvent...for event type "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0161 }
            java.lang.StringBuilder r4 = r4.append(r2)     // Catch:{ all -> 0x0161 }
            java.lang.String r5 = " = "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x0161 }
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ all -> 0x0161 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0161 }
            com.agilebinary.mobilemonitor.a.a.a.b.b(r3, r4)     // Catch:{ all -> 0x0161 }
            boolean r3 = r14.isCancelled()     // Catch:{ all -> 0x0161 }
            if (r3 != 0) goto L_0x001c
            r3 = 0
            int r3 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r3 >= 0) goto L_0x00ba
            java.lang.String r3 = com.agilebinary.mobilemonitor.client.android.ui.a.r.f221a     // Catch:{ s -> 0x0158 }
            java.lang.String r4 = "getting newest event id for event type "
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ s -> 0x0158 }
            r5.<init>()     // Catch:{ s -> 0x0158 }
            java.lang.String r6 = ""
            java.lang.StringBuilder r5 = r5.append(r6)     // Catch:{ s -> 0x0158 }
            java.lang.StringBuilder r5 = r5.append(r2)     // Catch:{ s -> 0x0158 }
            java.lang.String r5 = r5.toString()     // Catch:{ s -> 0x0158 }
            com.agilebinary.mobilemonitor.a.a.a.b.a(r3, r4, r5)     // Catch:{ s -> 0x0158 }
            com.agilebinary.mobilemonitor.client.android.MyApplication r3 = r14.d     // Catch:{ s -> 0x0158 }
            com.agilebinary.mobilemonitor.client.android.b.n r3 = r3.b()     // Catch:{ s -> 0x0158 }
            com.agilebinary.mobilemonitor.client.android.b.j r3 = r3.d()     // Catch:{ s -> 0x0158 }
            com.agilebinary.mobilemonitor.client.android.MyApplication r4 = r14.d     // Catch:{ s -> 0x0158 }
            com.agilebinary.mobilemonitor.client.android.f r4 = r4.a()     // Catch:{ s -> 0x0158 }
            long r0 = r3.a(r4, r2, r14)     // Catch:{ s -> 0x0158 }
            java.lang.String r3 = com.agilebinary.mobilemonitor.client.android.ui.a.r.f221a     // Catch:{ s -> 0x0158 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ s -> 0x0158 }
            r4.<init>()     // Catch:{ s -> 0x0158 }
            java.lang.String r5 = "Now, TimeOfLastssenEvent...for event type "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ s -> 0x0158 }
            java.lang.StringBuilder r4 = r4.append(r2)     // Catch:{ s -> 0x0158 }
            java.lang.String r5 = " = +ref"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ s -> 0x0158 }
            java.lang.String r4 = r4.toString()     // Catch:{ s -> 0x0158 }
            com.agilebinary.mobilemonitor.a.a.a.b.b(r3, r4)     // Catch:{ s -> 0x0158 }
            boolean r3 = r14.isCancelled()     // Catch:{ s -> 0x0158 }
            if (r3 != 0) goto L_0x001c
            r8.c(r2, r0)     // Catch:{ s -> 0x0158 }
            boolean r3 = r14.isCancelled()     // Catch:{ s -> 0x0158 }
            if (r3 != 0) goto L_0x001c
        L_0x00ba:
            r3 = r0
        L_0x00bb:
            r6 = 0
            boolean r0 = r14.isCancelled()     // Catch:{ s -> 0x016b }
            if (r0 != 0) goto L_0x001c
            java.lang.String r0 = com.agilebinary.mobilemonitor.client.android.ui.a.r.f221a     // Catch:{ s -> 0x016b }
            java.lang.String r1 = "getting number of new events event type "
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ s -> 0x016b }
            r5.<init>()     // Catch:{ s -> 0x016b }
            java.lang.String r11 = ""
            java.lang.StringBuilder r5 = r5.append(r11)     // Catch:{ s -> 0x016b }
            java.lang.StringBuilder r5 = r5.append(r2)     // Catch:{ s -> 0x016b }
            java.lang.String r5 = r5.toString()     // Catch:{ s -> 0x016b }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ s -> 0x016b }
            r11.<init>()     // Catch:{ s -> 0x016b }
            java.lang.String r12 = " since "
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ s -> 0x016b }
            java.lang.StringBuilder r11 = r11.append(r3)     // Catch:{ s -> 0x016b }
            java.lang.String r12 = " ("
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ s -> 0x016b }
            java.util.Date r12 = new java.util.Date     // Catch:{ s -> 0x016b }
            r12.<init>(r3)     // Catch:{ s -> 0x016b }
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ s -> 0x016b }
            java.lang.String r12 = ")"
            java.lang.StringBuilder r11 = r11.append(r12)     // Catch:{ s -> 0x016b }
            java.lang.String r11 = r11.toString()     // Catch:{ s -> 0x016b }
            com.agilebinary.mobilemonitor.a.a.a.b.a(r0, r1, r5, r11)     // Catch:{ s -> 0x016b }
            com.agilebinary.mobilemonitor.client.android.MyApplication r0 = r14.d     // Catch:{ s -> 0x016b }
            com.agilebinary.mobilemonitor.client.android.b.n r0 = r0.b()     // Catch:{ s -> 0x016b }
            com.agilebinary.mobilemonitor.client.android.b.f r0 = r0.c()     // Catch:{ s -> 0x016b }
            com.agilebinary.mobilemonitor.client.android.MyApplication r1 = r14.d     // Catch:{ s -> 0x016b }
            com.agilebinary.mobilemonitor.client.android.f r1 = r1.a()     // Catch:{ s -> 0x016b }
            r5 = r14
            int r1 = r0.a(r1, r2, r3, r5)     // Catch:{ s -> 0x016b }
            java.lang.String r0 = com.agilebinary.mobilemonitor.client.android.ui.a.r.f221a     // Catch:{ s -> 0x0171 }
            java.lang.String r3 = "number of new events ="
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ s -> 0x0171 }
            r4.<init>()     // Catch:{ s -> 0x0171 }
            java.lang.String r5 = ""
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ s -> 0x0171 }
            java.lang.StringBuilder r4 = r4.append(r1)     // Catch:{ s -> 0x0171 }
            java.lang.String r4 = r4.toString()     // Catch:{ s -> 0x0171 }
            com.agilebinary.mobilemonitor.a.a.a.b.a(r0, r3, r4)     // Catch:{ s -> 0x0171 }
            boolean r0 = r14.isCancelled()     // Catch:{ s -> 0x0171 }
            if (r0 != 0) goto L_0x001c
        L_0x0139:
            boolean r0 = r14.isCancelled()     // Catch:{ all -> 0x0161 }
            if (r0 != 0) goto L_0x001c
            r0 = 2
            java.lang.Integer[] r0 = new java.lang.Integer[r0]     // Catch:{ all -> 0x0161 }
            r3 = 0
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x0161 }
            r0[r3] = r2     // Catch:{ all -> 0x0161 }
            r2 = 1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ all -> 0x0161 }
            r0[r2] = r1     // Catch:{ all -> 0x0161 }
            r14.publishProgress(r0)     // Catch:{ all -> 0x0161 }
            int r0 = r7 + 1
            r7 = r0
            goto L_0x0012
        L_0x0158:
            r3 = move-exception
            r13 = r3
            r3 = r0
            r0 = r13
            r0.printStackTrace()     // Catch:{ all -> 0x0161 }
            goto L_0x00bb
        L_0x0161:
            r0 = move-exception
            com.agilebinary.mobilemonitor.client.android.MyApplication r1 = r14.d
            r1.f()
            r1 = 0
            com.agilebinary.mobilemonitor.client.android.ui.a.r.b = r1
            throw r0
        L_0x016b:
            r0 = move-exception
            r1 = r6
        L_0x016d:
            r0.printStackTrace()     // Catch:{ all -> 0x0161 }
            goto L_0x0139
        L_0x0171:
            r0 = move-exception
            goto L_0x016d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.agilebinary.mobilemonitor.client.android.ui.a.r.doInBackground(java.lang.Void[]):java.lang.Void");
    }

    public void a() {
        b.b(f221a, "doCancel");
        cancel(false);
        if (this.e != null) {
            try {
                this.e.i();
            } catch (Exception e2) {
            }
        }
    }

    public void a(g gVar) {
        this.e = gVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onProgressUpdate(Integer... numArr) {
        MainActivity.a(this.c, (byte) numArr[0].intValue(), numArr[1].intValue());
    }
}
