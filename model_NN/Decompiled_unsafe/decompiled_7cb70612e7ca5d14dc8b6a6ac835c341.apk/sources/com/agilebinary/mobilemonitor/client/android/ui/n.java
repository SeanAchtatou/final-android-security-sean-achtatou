package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.android.b.e;
import com.agilebinary.phonebeagle.client.R;
import java.text.SimpleDateFormat;
import java.util.Date;

public class n extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    protected TextView f327a = ((TextView) findViewById(R.id.addressbook_rowview_name));
    protected TextView b = ((TextView) findViewById(R.id.addressbook_rowview_phonenumber));
    protected TextView c = ((TextView) findViewById(R.id.addressbook_rowview_transferstatus));
    protected TextView d = ((TextView) findViewById(R.id.addressbook_rowview_status_textview));
    protected CheckBox e = ((CheckBox) findViewById(R.id.addressbook_rowview_spinner_checkbox));
    protected e f;
    final /* synthetic */ AddressbookActivity g;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.agilebinary.mobilemonitor.client.android.ui.n, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n(AddressbookActivity addressbookActivity, Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.g = addressbookActivity;
        ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.addressbook_rowview, (ViewGroup) this, true);
        this.e.setOnCheckedChangeListener(new o(this, addressbookActivity));
    }

    public void a(e eVar) {
        String string;
        int i;
        int i2 = R.color.red;
        Log.d(AddressbookActivity.f191a, "bind: " + eVar);
        this.f = eVar;
        this.f327a.setText(eVar.f163a);
        this.b.setText(eVar.b);
        String format = SimpleDateFormat.getDateTimeInstance().format(new Date(eVar.d));
        if ("client_set".equals(eVar.c)) {
            string = this.g.getString(R.string.label_block_list_status_client_set_time, new Object[]{format});
            i = R.color.red;
        } else if ("device_write".equals(eVar.c)) {
            string = this.g.getString(R.string.label_block_list_status_device_write_time, new Object[]{format});
            i = R.color.green;
        } else {
            string = this.g.getString(R.string.label_block_list_status_unknown);
            i = R.color.black;
        }
        this.c.setText(string);
        this.c.setTextColor(getResources().getColor(i));
        this.f.f = true;
        this.e.setChecked(!eVar.e);
        this.f.f = false;
        this.d.setText(this.g.getString(eVar.e ? R.string.label_contact_blocked : R.string.label_contact_unblocked));
        TextView textView = this.d;
        Resources resources = getResources();
        if (!eVar.e) {
            i2 = R.color.green;
        }
        textView.setTextColor(resources.getColor(i2));
    }
}
