package com.agilebinary.mobilemonitor.client.android.ui.b;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.a.b.e;
import com.agilebinary.mobilemonitor.client.android.d.g;
import com.agilebinary.phonebeagle.client.R;

public class f extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    protected TextView f252a = ((TextView) findViewById(R.id.location_select_dialog_row_line1));
    protected TextView b = ((TextView) findViewById(R.id.location_select_dialog_row_line2));
    private TextView c = ((TextView) findViewById(R.id.location_select_dialog_row_accuracy));
    private TextView d = ((TextView) findViewById(R.id.location_select_dialog_row_time));

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.agilebinary.mobilemonitor.client.android.ui.b.f, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public f(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.location_select_dialog_row, (ViewGroup) this, true);
    }

    public void setEvent(e eVar) {
        this.d.setText(g.a(getContext()).a(eVar.h()));
        this.f252a.setText(eVar.b(getContext()));
        this.b.setText(eVar.c(getContext()));
        Double i = eVar.i();
        if (i != null) {
            this.c.setText(getContext().getString(R.string.label_event_location_accuracy, i));
            return;
        }
        this.c.setText("");
    }
}
