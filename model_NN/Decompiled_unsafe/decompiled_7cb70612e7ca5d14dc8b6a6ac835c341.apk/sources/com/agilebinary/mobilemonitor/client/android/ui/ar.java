package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.View;
import com.agilebinary.phonebeagle.client.R;

final class ar implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ChangePasswordActivity f238a;

    ar(ChangePasswordActivity changePasswordActivity) {
        this.f238a = changePasswordActivity;
    }

    public void onClick(View view) {
        String obj = this.f238a.i.getText().toString();
        if (!obj.equals(this.f238a.j.getText().toString())) {
            this.f238a.i.setText("");
            this.f238a.j.setText("");
            this.f238a.a((int) R.string.error_password_match);
        } else if (obj.length() < 8) {
            this.f238a.i.setText("");
            this.f238a.j.setText("");
            this.f238a.a((int) R.string.error_password_length);
        } else {
            this.f238a.a(obj);
        }
    }
}
