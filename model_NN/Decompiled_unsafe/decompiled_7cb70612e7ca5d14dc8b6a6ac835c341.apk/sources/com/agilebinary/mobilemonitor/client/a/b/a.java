package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.b;
import com.agilebinary.phonebeagle.client.R;

public class a extends g {

    /* renamed from: a  reason: collision with root package name */
    protected long f126a;
    protected int b;
    protected String c;

    public a(String str, long j, long j2, com.agilebinary.mobilemonitor.client.a.a.a aVar, b bVar) {
        super(str, j, j2, aVar, bVar);
        this.f126a = bVar.a(aVar.d());
        this.b = aVar.c();
        this.c = aVar.g();
    }

    public long a() {
        return this.f126a;
    }

    public String a(Context context) {
        switch (b()) {
            case 1:
                return context.getString(R.string.label_event_application_added);
            case 2:
                return context.getString(R.string.label_event_application_removed);
            case 3:
                return context.getString(R.string.label_event_application_updated);
            case 4:
                return context.getString(R.string.label_event_application_data_cleared);
            case 5:
                return context.getString(R.string.label_event_application_opened);
            default:
                return "";
        }
    }

    public int b() {
        return this.b;
    }

    public String b(Context context) {
        String[] split = c().split(":");
        return split.length > 1 ? split[1] : "";
    }

    public String c() {
        return this.c;
    }

    public byte d() {
        return 11;
    }
}
