package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.b.c.a;
import com.agilebinary.mobilemonitor.client.android.d.d;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.mobilemonitor.client.android.ui.a.o;
import com.agilebinary.phonebeagle.client.R;
import java.io.UnsupportedEncodingException;

public class ChangePasswordActivity extends al {

    /* renamed from: a  reason: collision with root package name */
    private static final String f196a = f.a("ChangePasswordActivity");
    private TextView b;
    /* access modifiers changed from: private */
    public EditText i;
    /* access modifiers changed from: private */
    public EditText j;
    private Button k;
    private Button l;
    private Button m;
    private int n;
    private boolean o;

    public static void a(Activity activity, int i2, boolean z, a aVar, int i3) {
        Intent intent = new Intent(activity, ChangePasswordActivity.class);
        intent.putExtra("EXTRA_ACCOUNT", aVar);
        intent.putExtra("EXTRA_TEXT_ID", i2);
        intent.putExtra("EXTRA_SKIP", z);
        activity.startActivityForResult(intent, i3);
    }

    public static void a(Context context, o oVar) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra("EXTRA_SERVICE_RESULT", oVar);
        intent.setFlags(536870912);
        context.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        String str2;
        try {
            str2 = d.a(str);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            str2 = "";
        }
        this.i.setText("");
        this.j.setText("");
        b((int) R.string.msg_progress_communicating);
        com.agilebinary.mobilemonitor.client.android.ui.a.a aVar = new com.agilebinary.mobilemonitor.client.android.ui.a.a(str2, (a) getIntent().getSerializableExtra("EXTRA_ACCOUNT"));
        com.agilebinary.mobilemonitor.client.android.ui.a.d.b = new com.agilebinary.mobilemonitor.client.android.ui.a.d(this);
        com.agilebinary.mobilemonitor.client.android.ui.a.d.b.execute(aVar);
    }

    /* access modifiers changed from: protected */
    public int a() {
        return R.layout.change_password;
    }

    /* access modifiers changed from: protected */
    public void e() {
        if (com.agilebinary.mobilemonitor.client.android.ui.a.d.b != null) {
            try {
                com.agilebinary.mobilemonitor.client.android.ui.a.d.b.a();
            } catch (Exception e) {
            }
        }
    }

    public void onCreate(Bundle bundle) {
        int i2 = 0;
        super.onCreate(bundle);
        this.b = (TextView) findViewById(R.id.changepassword_reason);
        this.n = getIntent().getIntExtra("EXTRA_TEXT_ID", 0);
        this.o = getIntent().getBooleanExtra("EXTRA_SKIP", false);
        this.b.setText(this.n);
        this.i = (EditText) findViewById(R.id.changepassword_password1);
        this.j = (EditText) findViewById(R.id.changepassword_password2);
        this.k = (Button) findViewById(R.id.changepassword_cancel);
        this.l = (Button) findViewById(R.id.changepassword_skip);
        this.k.setVisibility(!this.o ? 0 : 8);
        Button button = this.l;
        if (!this.o) {
            i2 = 8;
        }
        button.setVisibility(i2);
        this.m = (Button) findViewById(R.id.changepassword_ok);
        this.m.setOnClickListener(new ar(this));
        this.k.setOnClickListener(new as(this));
        this.l.setOnClickListener(new at(this));
        if (bundle != null) {
            this.i.setText(bundle.getString("EXTRA_PW1"));
            this.j.setText(bundle.getString("EXTRA_PW2"));
            this.b.setText(bundle.getString("EXTRA_REASON"));
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        b.b(f196a, "onNewIntent...");
        o oVar = (o) intent.getSerializableExtra("EXTRA_SERVICE_RESULT");
        b(false);
        if (oVar != null) {
            if (oVar.b() != null) {
                if (oVar.b().c()) {
                    a((int) R.string.error_service_account_general);
                } else if (oVar.b().b()) {
                    a((int) R.string.error_service_account_general);
                } else {
                    a((int) R.string.error_service_account_general);
                }
            } else if (oVar.a()) {
                setResult(-1);
                finish();
            }
        }
        b.b(f196a, "...onNewIntent");
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        b.b(f196a, "onSaveInstanceState...");
        bundle.putString("EXTRA_PW1", this.i.getText().toString());
        bundle.putString("EXTRA_PW2", this.j.getText().toString());
        bundle.putInt("EXTRA_REASON", this.n);
        super.onSaveInstanceState(bundle);
        b.b(f196a, "...onSaveInstanceState");
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        if (this.l.getVisibility() == 0) {
            this.l.requestFocus();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
