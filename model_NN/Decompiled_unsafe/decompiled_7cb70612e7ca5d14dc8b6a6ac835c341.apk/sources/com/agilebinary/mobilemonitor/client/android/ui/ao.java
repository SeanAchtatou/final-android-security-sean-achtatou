package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.View;
import com.agilebinary.phonebeagle.client.R;

final class ao implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ChangeEmailActivity f235a;

    ao(ChangeEmailActivity changeEmailActivity) {
        this.f235a = changeEmailActivity;
    }

    public void onClick(View view) {
        String obj = this.f235a.i.getText().toString();
        if (!obj.equals(this.f235a.j.getText().toString())) {
            this.f235a.a((int) R.string.error_email_match);
        } else if (!this.f235a.b(obj)) {
            this.f235a.a((int) R.string.error_email_invalid);
        } else {
            this.f235a.a(obj);
        }
    }
}
