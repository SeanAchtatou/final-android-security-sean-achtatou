package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.Dialog;
import android.view.View;

final class db implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Dialog f309a;
    final /* synthetic */ MapActivity_OSM b;

    db(MapActivity_OSM mapActivity_OSM, Dialog dialog) {
        this.b = mapActivity_OSM;
        this.f309a = dialog;
    }

    public void onClick(View view) {
        this.b.p.b(!this.b.p.a());
        this.b.m.invalidate();
        this.b.u.b("MAP_LAYER_SCALEBAR__BOOL", this.b.q.a());
        this.f309a.dismiss();
    }
}
