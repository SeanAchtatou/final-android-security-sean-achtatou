package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.a;
import com.agilebinary.mobilemonitor.client.a.b;

public class w extends g {

    /* renamed from: a  reason: collision with root package name */
    protected long f136a;
    protected String b;
    protected String c;
    protected boolean i;
    protected int w;

    public w(String str, long j, long j2, a aVar, b bVar) {
        super(str, j, j2, aVar, bVar);
        this.f136a = bVar.a(aVar.d());
        this.b = aVar.g();
        this.c = aVar.g();
        this.i = aVar.a();
        this.w = aVar.c();
    }

    public long a() {
        return this.f136a;
    }

    public String a(Context context) {
        return b();
    }

    public String b() {
        return this.b;
    }

    public String b(Context context) {
        return c();
    }

    public String c() {
        return this.c;
    }

    public byte d() {
        return 14;
    }

    public boolean m() {
        return this.i;
    }

    public int n() {
        return this.w;
    }
}
