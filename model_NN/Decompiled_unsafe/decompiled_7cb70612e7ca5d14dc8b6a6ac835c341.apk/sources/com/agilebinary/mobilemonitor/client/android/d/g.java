package com.agilebinary.mobilemonitor.client.android.d;

import android.content.Context;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class g {

    /* renamed from: a  reason: collision with root package name */
    private static g f183a;
    private DateFormat b = SimpleDateFormat.getDateInstance(3);
    private DateFormat c = SimpleDateFormat.getDateInstance(2);
    private DateFormat d = SimpleDateFormat.getTimeInstance(3);
    private DateFormat e = SimpleDateFormat.getTimeInstance(2);
    private DateFormat f = SimpleDateFormat.getDateTimeInstance(3, 2);
    private DateFormat g = SimpleDateFormat.getDateTimeInstance(2, 2);
    private TimeZone h = TimeZone.getDefault();
    private DateFormat i = new SimpleDateFormat("d MMM");

    public g(Context context) {
    }

    public static synchronized g a(Context context) {
        g gVar;
        synchronized (g.class) {
            if (f183a == null) {
                f183a = new g(context);
            }
            gVar = f183a;
        }
        return gVar;
    }

    public String a(long j) {
        Date date = new Date(j);
        return String.format("%s\n%s", this.d.format(date), this.i.format(date));
    }

    public String b(long j) {
        return this.b.format(new Date(j));
    }

    public String c(long j) {
        if (j == 0) {
            return "";
        }
        Date date = new Date(j);
        if (this.h.getID().equals(TimeZone.getDefault().getID())) {
            return this.g.format(date);
        }
        return String.format("%s [%s]", this.g.format(date), this.h.getDisplayName());
    }

    public String d(long j) {
        if (j == 0) {
            return "";
        }
        Date date = new Date(j);
        if (this.h.getID().equals(TimeZone.getDefault().getID())) {
            return this.f.format(date);
        }
        return String.format("%s [%s]", this.f.format(date), this.h.getDisplayName());
    }
}
