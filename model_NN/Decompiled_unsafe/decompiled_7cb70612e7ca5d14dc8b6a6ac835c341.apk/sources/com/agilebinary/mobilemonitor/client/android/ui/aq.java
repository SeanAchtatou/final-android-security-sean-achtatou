package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.View;

final class aq implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ChangeEmailActivity f237a;

    aq(ChangeEmailActivity changeEmailActivity) {
        this.f237a = changeEmailActivity;
    }

    public void onClick(View view) {
        this.f237a.setResult(0);
        this.f237a.finish();
    }
}
