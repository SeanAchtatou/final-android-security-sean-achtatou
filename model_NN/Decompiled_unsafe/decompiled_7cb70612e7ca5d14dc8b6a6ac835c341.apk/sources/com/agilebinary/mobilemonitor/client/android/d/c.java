package com.agilebinary.mobilemonitor.client.android.d;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private static char[] f181a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    public static byte[] a(char[] cArr) {
        int i = 0;
        int length = cArr.length;
        if ((length & 1) != 0) {
            throw new Exception("Odd number of characters.");
        }
        byte[] bArr = new byte[(length >> 1)];
        int i2 = 0;
        while (i < length) {
            int i3 = i + 1;
            int digit = Character.digit(cArr[i], 16) << 4;
            i = i3 + 1;
            bArr[i2] = (byte) ((Character.digit(cArr[i3], 16) | digit) & 255);
            i2++;
        }
        return bArr;
    }

    public static char[] a(byte[] bArr) {
        int i = 0;
        int length = bArr.length;
        char[] cArr = new char[(length << 1)];
        for (int i2 = 0; i2 < length; i2++) {
            int i3 = i + 1;
            cArr[i] = f181a[(bArr[i2] & 240) >>> 4];
            i = i3 + 1;
            cArr[i3] = f181a[bArr[i2] & 15];
        }
        return cArr;
    }
}
