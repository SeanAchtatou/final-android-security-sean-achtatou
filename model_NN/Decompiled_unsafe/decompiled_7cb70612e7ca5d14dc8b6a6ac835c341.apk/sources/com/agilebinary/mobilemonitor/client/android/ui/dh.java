package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import com.agilebinary.mobilemonitor.client.a.b.e;
import com.agilebinary.mobilemonitor.client.a.d;
import com.agilebinary.phonebeagle.client.R;
import org.osmdroid.d.a.h;
import org.osmdroid.d.a.j;

public class dh extends h {

    /* renamed from: a  reason: collision with root package name */
    e f315a;

    public dh(e eVar, Context context) {
        super("foo", "bar", ((d) eVar).c());
        this.f315a = eVar;
        if (!(eVar instanceof com.agilebinary.mobilemonitor.client.a.b.d)) {
            a(context.getResources().getDrawable(R.drawable.mapmarker_blue));
        } else if (((com.agilebinary.mobilemonitor.client.a.b.d) eVar).m()) {
            a(context.getResources().getDrawable(R.drawable.mapmarker_green));
        } else {
            a(context.getResources().getDrawable(R.drawable.mapmarker_red));
        }
        a(j.BOTTOM_CENTER);
    }

    public Drawable a(int i) {
        if (this.g == null) {
            return null;
        }
        this.g.setState(i == 4 ? new int[]{16842908} : new int[0]);
        return this.g;
    }

    public Point b(int i) {
        return i == 0 ? new Point(10, 35) : new Point(14, 48);
    }
}
