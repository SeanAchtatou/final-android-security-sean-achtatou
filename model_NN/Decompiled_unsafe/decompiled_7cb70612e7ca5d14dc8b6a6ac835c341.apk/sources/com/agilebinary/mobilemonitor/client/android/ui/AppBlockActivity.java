package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.android.b.b;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.mobilemonitor.client.android.ui.a.q;
import com.agilebinary.phonebeagle.client.R;
import java.util.List;

public class AppBlockActivity extends al {

    /* renamed from: a  reason: collision with root package name */
    static final String f193a = f.a("AppBlockActivity");
    protected AlertDialog b;
    /* access modifiers changed from: private */
    public ListView i;
    private LinearLayout j;
    private TextView k;
    private Button l;
    private ImageView m;
    private Animation n;
    private ImageButton o;
    private q p;
    private b q;
    private af r;
    private com.agilebinary.mobilemonitor.client.android.ui.a.f s;
    /* access modifiers changed from: private */
    public int t;
    /* access modifiers changed from: private */
    public boolean u;

    public static void a(Context context) {
        context.startActivity(new Intent(context, AppBlockActivity.class));
    }

    public static void a(AppBlockActivity appBlockActivity, com.agilebinary.mobilemonitor.client.android.ui.a.f fVar) {
        Intent intent = new Intent(appBlockActivity, AppBlockActivity.class);
        intent.putExtra("EXTRA_DOWNLOAD_APP_RESULTS", fVar);
        intent.setFlags(536870912);
        appBlockActivity.startActivity(intent);
    }

    private void a(com.agilebinary.mobilemonitor.client.android.ui.a.f fVar) {
        c();
        if (fVar.a() == null) {
            this.s = fVar;
            this.r = new af(this, fVar);
            this.i.setAdapter((ListAdapter) this.r);
            this.o.setEnabled(true);
            a(ae.LIST);
            this.i.setSelection(this.t);
        } else if (fVar.a().a()) {
            finish();
            this.g.a(this);
        } else if (!fVar.a().c()) {
            this.b.show();
        }
    }

    private void l() {
        c();
    }

    /* access modifiers changed from: private */
    public void m() {
        List list = null;
        com.agilebinary.mobilemonitor.a.a.a.b.b(f193a, "downloadNew...");
        this.r = new af(this, null);
        this.i.setAdapter((ListAdapter) this.r);
        this.o.setEnabled(false);
        b();
        if (this.u && this.s != null) {
            list = this.s.b();
        }
        this.p = new q(this, this.q);
        this.p.execute(list);
        com.agilebinary.mobilemonitor.a.a.a.b.b(f193a, "...downloadNew");
    }

    /* access modifiers changed from: protected */
    public int a() {
        return R.layout.appblock;
    }

    /* access modifiers changed from: protected */
    public void a(ae aeVar) {
        int i2 = 8;
        boolean z = true;
        this.i.setVisibility(aeVar == ae.NO_EVENTS ? 8 : 0);
        this.i.setEnabled(aeVar == ae.LIST);
        LinearLayout linearLayout = this.j;
        if (aeVar == ae.PROGRESS) {
            i2 = 0;
        }
        linearLayout.setVisibility(i2);
        if (aeVar == ae.PROGRESS) {
            this.m.startAnimation(this.n);
        } else {
            this.m.clearAnimation();
        }
        ImageButton imageButton = this.o;
        if (aeVar == ae.PROGRESS) {
            z = false;
        }
        imageButton.setEnabled(z);
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        com.agilebinary.mobilemonitor.a.a.a.b.b(f193a, "stopDownloading...");
        try {
            if (this.p != null) {
                try {
                    this.p.cancel(z);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                this.p = null;
            }
            c();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        com.agilebinary.mobilemonitor.a.a.a.b.b(f193a, "...stopDownloading");
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.k.setText((int) R.string.msg_progress_loading_apps);
        a(ae.PROGRESS);
    }

    /* access modifiers changed from: protected */
    public void c() {
        a((this.r == null || this.r.getCount() == 0) ? ae.NO_EVENTS : ae.LIST);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.i = (ListView) findViewById(R.id.appblock_list);
        this.j = (LinearLayout) findViewById(R.id.appblock_progress);
        this.k = (TextView) findViewById(R.id.appblock_progress_text);
        this.m = (ImageView) findViewById(R.id.appblock_progress_anim);
        this.n = AnimationUtils.loadAnimation(this, R.anim.spinner_black_76);
        this.l = (Button) findViewById(R.id.appblock_progress_cancel);
        this.o = (ImageButton) findViewById(R.id.appblock_refresh);
        this.q = this.g.b().a(this);
        this.l.setOnClickListener(new w(this));
        this.o.setOnClickListener(new x(this));
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage((int) R.string.error_downloading).setCancelable(true).setNeutralButton("OK", new y(this));
        this.b = builder.create();
        if (bundle == null) {
            m();
        } else if (bundle.getBoolean("EXTRA_ALERT_DIALOG_IS_SHOWING")) {
            this.b.show();
        } else {
            this.s = (com.agilebinary.mobilemonitor.client.android.ui.a.f) bundle.getSerializable("EXTRA_DATA");
            this.u = bundle.getBoolean("EXTRA_HAS_CHANGES");
            if (this.s == null) {
                m();
            } else {
                a(this.s);
            }
        }
        com.agilebinary.mobilemonitor.a.a.a.b.b(f193a, "...onCreate");
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        com.agilebinary.mobilemonitor.client.android.ui.a.f fVar = (com.agilebinary.mobilemonitor.client.android.ui.a.f) intent.getSerializableExtra("EXTRA_DOWNLOAD_APP_RESULTS");
        if (fVar == null) {
            l();
            return;
        }
        a(fVar);
        this.u = false;
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        com.agilebinary.mobilemonitor.a.a.a.b.b(f193a, "onSaveInstanceState...");
        bundle.putBoolean("EXTRA_ALERT_DIALOG_IS_SHOWING", this.b.isShowing());
        bundle.putSerializable("EXTRA_DATA", this.s);
        bundle.putBoolean("EXTRA_HAS_CHANGES", this.u);
        super.onSaveInstanceState(bundle);
        com.agilebinary.mobilemonitor.a.a.a.b.b(f193a, "...onSaveInstanceState");
    }
}
