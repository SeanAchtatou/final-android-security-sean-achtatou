package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import com.agilebinary.mobilemonitor.client.a.a.a;
import com.agilebinary.mobilemonitor.client.a.b;
import com.agilebinary.mobilemonitor.client.a.e;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.phonebeagle.client.R;
import java.io.ByteArrayOutputStream;

public class r extends f {
    static final String w = f.a("MmsEvent");
    protected boolean A;
    protected String B;
    protected String C;
    protected String[] D;
    protected String[] E;
    protected String[] F;
    protected String[] G;
    protected String[] H;
    protected String[] I;
    protected s[] J;
    /* access modifiers changed from: private */
    public byte[] K;
    protected String x;
    protected String y;
    protected String z;

    public r(String str, long j, long j2, a aVar, b bVar, e eVar, Context context, ByteArrayOutputStream byteArrayOutputStream) {
        super(str, j, j2, aVar, bVar);
        com.agilebinary.mobilemonitor.a.a.a.b.b(w, "CONSTRUCTOR from stream...");
        this.B = aVar.g();
        this.C = aVar.g();
        this.A = aVar.a();
        if (!this.A) {
            this.x = aVar.g();
            this.y = aVar.g();
            this.z = aVar.g();
            this.D = aVar.f();
            this.E = aVar.f();
            this.F = aVar.f();
            this.G = aVar.f();
            this.H = aVar.f();
            this.I = aVar.f();
            this.J = new s[aVar.c()];
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= this.J.length) {
                    break;
                }
                this.J[i2] = new s(this, aVar, eVar, context, byteArrayOutputStream);
                i = i2 + 1;
            }
        }
        com.agilebinary.mobilemonitor.a.a.a.b.b(w, "...CONSTRUCTOR from stream");
    }

    public static void a(a aVar, int i) {
        byte[] bArr = new byte[2048];
        do {
            i -= aVar.read(bArr, 0, Math.min(bArr.length, i));
        } while (i > 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* access modifiers changed from: private */
    public static Bitmap[] b(Context context, byte[] bArr, int i, int i2) {
        float f;
        float f2;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options);
            int i3 = options.outWidth;
            int i4 = options.outHeight;
            com.agilebinary.mobilemonitor.a.a.a.b.b(w, "dimensions of original image " + i3 + " / " + i4);
            int ceil = (int) Math.ceil(((double) Math.max(i3, i4)) / ((double) Math.max(i, i2)));
            com.agilebinary.mobilemonitor.a.a.a.b.b(w, "sampleSize " + ceil);
            BitmapFactory.Options options2 = new BitmapFactory.Options();
            if (ceil > 1) {
                options2.inSampleSize = ceil;
            }
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length, options2);
            int width = decodeByteArray.getWidth();
            int height = decodeByteArray.getHeight();
            com.agilebinary.mobilemonitor.a.a.a.b.b(w, "decoded bitmap has size " + width + " / " + height);
            if (width > height) {
                f = ((float) i) / ((float) width);
                f2 = ((float) i2) / ((float) width);
            } else {
                f = ((float) i) / ((float) height);
                f2 = ((float) i2) / ((float) height);
            }
            float min = Math.min(f, 1.0f);
            float min2 = Math.min(f2, 1.0f);
            Matrix matrix = new Matrix();
            matrix.postScale(min, min);
            Matrix matrix2 = new Matrix();
            matrix.postScale(min2, min2);
            return new Bitmap[]{Bitmap.createBitmap(decodeByteArray, 0, 0, width, height, matrix, true), Bitmap.createBitmap(decodeByteArray, 0, 0, width, height, matrix2, true)};
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            return null;
        }
    }

    public String a(Context context) {
        String str = "";
        String str2 = "";
        if (c() == 1) {
            str = context.getString(R.string.label_event_mms_from_fmt);
            str2 = com.agilebinary.mobilemonitor.client.android.d.b.b(n(), o());
        } else if (c() == 2) {
            str = context.getString(R.string.label_event_mms_to_fmt);
            String[] strArr = new String[(q().length + s().length + u().length)];
            String[] strArr2 = new String[strArr.length];
            System.arraycopy(q(), 0, strArr, 0, q().length);
            System.arraycopy(p(), 0, strArr2, 0, p().length);
            int length = q().length + 0;
            System.arraycopy(s(), 0, strArr, length, s().length);
            System.arraycopy(r(), 0, strArr2, length, r().length);
            int length2 = length + s().length;
            System.arraycopy(u(), 0, strArr, length2, u().length);
            System.arraycopy(t(), 0, strArr2, length2, t().length);
            str2 = com.agilebinary.mobilemonitor.client.android.d.b.a(context, strArr2, strArr);
        }
        return String.format(str, str2);
    }

    public String b(Context context) {
        if (!com.agilebinary.mobilemonitor.client.android.d.b.a(m())) {
            return m();
        }
        for (s sVar : v()) {
            if (sVar.d()) {
                return sVar.f();
            }
        }
        s[] v = v();
        return v.length > 0 ? v[0].a() : "";
    }

    public byte d() {
        return 3;
    }

    public String m() {
        return this.x;
    }

    public String n() {
        return this.B;
    }

    public String o() {
        return this.C;
    }

    public String[] p() {
        return this.D;
    }

    public String[] q() {
        return this.E;
    }

    public String[] r() {
        return this.F;
    }

    public String[] s() {
        return this.G;
    }

    public String[] t() {
        return this.H;
    }

    public String[] u() {
        return this.I;
    }

    public s[] v() {
        return this.J;
    }

    public byte[] w() {
        return this.K;
    }
}
