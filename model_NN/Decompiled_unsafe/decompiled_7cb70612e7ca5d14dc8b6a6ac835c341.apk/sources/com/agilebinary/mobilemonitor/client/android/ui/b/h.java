package com.agilebinary.mobilemonitor.client.android.ui.b;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import java.util.List;
import org.osmdroid.c;
import org.osmdroid.d.a.a;
import org.osmdroid.d.a.e;
import org.osmdroid.d.b;

public class h extends a {

    /* renamed from: a  reason: collision with root package name */
    protected int f254a;
    protected boolean b;
    private final Point g;

    public h(Context context, List list, Drawable drawable, Point point, Drawable drawable2, Point point2, int i, e eVar, c cVar) {
        super(context, list, drawable, point, eVar, cVar);
        this.g = new Point();
        b_();
    }

    public h(Context context, List list, e eVar, c cVar) {
        this(context, list, null, null, null, null, Integer.MIN_VALUE, eVar, cVar);
    }

    public void a(int i) {
        this.f254a = i;
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, int i, Point point) {
        if (this.f254a == Integer.MIN_VALUE || i != this.f254a) {
            super.a(canvas, i, point);
        } else {
            this.g.set(point.x, point.y);
        }
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, b bVar) {
        if (this.f254a != Integer.MIN_VALUE) {
            org.osmdroid.d.a.h hVar = (org.osmdroid.d.a.h) this.d.get(this.f254a);
            Drawable a2 = hVar.a(4);
            Point b2 = hVar.b(4);
            int intrinsicWidth = a2.getIntrinsicWidth();
            int intrinsicHeight = a2.getIntrinsicHeight();
            int i = this.g.x - b2.x;
            int i2 = this.g.y - b2.y;
            a2.setBounds(i, i2, intrinsicWidth + i, intrinsicHeight + i2);
            a2.draw(canvas);
        }
    }

    public void a(boolean z) {
        this.b = z;
    }

    /* access modifiers changed from: protected */
    public boolean a(int i, org.osmdroid.d.a.h hVar) {
        if (this.b) {
            this.f254a = i;
        }
        return this.c.b(i, hVar);
    }

    public void b_() {
        this.f254a = Integer.MIN_VALUE;
    }
}
