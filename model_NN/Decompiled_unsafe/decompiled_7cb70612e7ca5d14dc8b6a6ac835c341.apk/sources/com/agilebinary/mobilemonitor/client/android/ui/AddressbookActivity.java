package com.agilebinary.mobilemonitor.client.android.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.a.a.a.b;
import com.agilebinary.mobilemonitor.client.android.b.d;
import com.agilebinary.mobilemonitor.client.android.d.f;
import com.agilebinary.mobilemonitor.client.android.ui.a.g;
import com.agilebinary.mobilemonitor.client.android.ui.a.p;
import com.agilebinary.phonebeagle.client.R;
import java.util.List;

public class AddressbookActivity extends al {

    /* renamed from: a  reason: collision with root package name */
    static final String f191a = f.a("AddressbookActivity");
    protected AlertDialog b;
    /* access modifiers changed from: private */
    public ListView i;
    private LinearLayout j;
    private TextView k;
    private Button l;
    private ImageView m;
    private Animation n;
    private ImageButton o;
    private Button p;
    private p q;
    private d r;
    private t s;
    /* access modifiers changed from: private */
    public g t;
    /* access modifiers changed from: private */
    public int u;
    /* access modifiers changed from: private */
    public boolean v;
    private Runnable w;

    public static void a(Context context) {
        context.startActivity(new Intent(context, AddressbookActivity.class));
    }

    public static void a(AddressbookActivity addressbookActivity, g gVar) {
        Intent intent = new Intent(addressbookActivity, AddressbookActivity.class);
        intent.putExtra("EXTRA_DOWNLOAD_CONTACTS_RESULTS", gVar);
        intent.setFlags(536870912);
        addressbookActivity.startActivity(intent);
    }

    private void a(g gVar) {
        c();
        if (gVar.a() == null) {
            this.t = gVar;
            this.s = new t(this, gVar);
            this.i.setAdapter((ListAdapter) this.s);
            this.o.setEnabled(true);
            a(s.LIST);
            this.i.setSelection(this.u);
        } else if (gVar.a().a()) {
            finish();
            this.g.a(this);
        } else if (!gVar.a().c()) {
            this.b.show();
        }
    }

    public static void a(ah ahVar, String str) {
        Intent intent = new Intent(ahVar, AddressbookActivity.class);
        intent.putExtra("EXTRA_ADD_NUMBER", str);
        intent.setFlags(536870912);
        ahVar.startActivity(intent);
    }

    public static void a(ah ahVar, String str, String str2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ahVar);
        builder.setMessage(ahVar.getString(ahVar.g.d() ? R.string.label_event_contact_blacklist_warning_demo : R.string.label_event_contact_blacklist_warning, new Object[]{str2})).setCancelable(true).setNeutralButton(ahVar.getString(R.string.label_ok), new h(ahVar, str2, str));
        builder.create().show();
    }

    private void a(String str, String str2) {
        this.w = new m(this, str2, str);
        m();
    }

    private void l() {
        c();
    }

    /* access modifiers changed from: private */
    public void m() {
        List list = null;
        b.b(f191a, "downloadNew...");
        this.s = new t(this, null);
        this.i.setAdapter((ListAdapter) this.s);
        this.o.setEnabled(false);
        b();
        if (this.v && this.t != null) {
            list = this.t.b();
        }
        this.q = new p(this, this.r);
        this.q.execute(list);
        b.b(f191a, "...downloadNew");
    }

    /* access modifiers changed from: protected */
    public int a() {
        return R.layout.addressbook;
    }

    /* access modifiers changed from: protected */
    public void a(s sVar) {
        int i2 = 8;
        boolean z = true;
        this.i.setVisibility(sVar == s.NO_EVENTS ? 8 : 0);
        this.i.setEnabled(sVar == s.LIST);
        LinearLayout linearLayout = this.j;
        if (sVar == s.PROGRESS) {
            i2 = 0;
        }
        linearLayout.setVisibility(i2);
        if (sVar == s.PROGRESS) {
            this.m.startAnimation(this.n);
        } else {
            this.m.clearAnimation();
        }
        ImageButton imageButton = this.o;
        if (sVar == s.PROGRESS) {
            z = false;
        }
        imageButton.setEnabled(z);
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        b.b(f191a, "stopDownloading...");
        try {
            if (this.q != null) {
                try {
                    this.q.cancel(z);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                this.q = null;
            }
            c();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        b.b(f191a, "...stopDownloading");
    }

    /* access modifiers changed from: protected */
    public void b() {
        this.k.setText((int) R.string.msg_progress_loading_contacts);
        a(s.PROGRESS);
    }

    /* access modifiers changed from: protected */
    public void c() {
        a((this.s == null || this.s.getCount() == 0) ? s.NO_EVENTS : s.LIST);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.i = (ListView) findViewById(R.id.addressbook_list);
        this.j = (LinearLayout) findViewById(R.id.addressbook_progress);
        this.k = (TextView) findViewById(R.id.addressbook_progress_text);
        this.m = (ImageView) findViewById(R.id.addressbook_progress_anim);
        this.n = AnimationUtils.loadAnimation(this, R.anim.spinner_black_76);
        this.l = (Button) findViewById(R.id.addressbook_progress_cancel);
        this.o = (ImageButton) findViewById(R.id.addressbook_refresh);
        this.p = (Button) findViewById(R.id.addressbook_add_number);
        this.r = this.g.b().b(this);
        this.p.setOnClickListener(new i(this));
        this.l.setOnClickListener(new j(this));
        this.o.setOnClickListener(new k(this));
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage((int) R.string.error_downloading).setCancelable(true).setNeutralButton("OK", new l(this));
        this.b = builder.create();
        if (bundle == null) {
            String stringExtra = getIntent().getStringExtra("EXTRA_ADD_NAME");
            String stringExtra2 = getIntent().getStringExtra("EXTRA_ADD_NUMBER");
            if (stringExtra2 != null) {
                a(stringExtra2, stringExtra);
            } else {
                m();
            }
        } else if (bundle.getBoolean("EXTRA_ALERT_DIALOG_IS_SHOWING")) {
            this.b.show();
        } else {
            this.t = (g) bundle.getSerializable("EXTRA_DATA");
            this.v = bundle.getBoolean("EXTRA_HAS_CHANGES");
            if (this.t == null) {
                m();
            } else {
                a(this.t);
            }
        }
        b.b(f191a, "...onCreate");
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        Object serializableExtra = intent.getSerializableExtra("EXTRA_DOWNLOAD_CONTACTS_RESULTS");
        if (serializableExtra == null && (serializableExtra = intent.getStringExtra("EXTRA_ADD_NUMBER")) != null) {
            a((String) serializableExtra, (String) null);
        } else if (serializableExtra == null) {
            l();
        } else {
            a((g) serializableExtra);
            this.v = false;
            if (this.w != null) {
                Runnable runnable = this.w;
                this.w = null;
                runnable.run();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        b.b(f191a, "onSaveInstanceState...");
        bundle.putBoolean("EXTRA_ALERT_DIALOG_IS_SHOWING", this.b.isShowing());
        bundle.putSerializable("EXTRA_DATA", this.t);
        bundle.putBoolean("EXTRA_HAS_CHANGES", this.v);
        super.onSaveInstanceState(bundle);
        b.b(f191a, "...onSaveInstanceState");
    }
}
