package com.agilebinary.mobilemonitor.client.android.ui.a;

import com.agilebinary.mobilemonitor.client.android.b.s;
import java.io.Serializable;
import java.util.List;

public class g implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    s f210a;
    private List b;

    public g(s sVar) {
        this.f210a = sVar;
    }

    public g(List list) {
        this.b = list;
    }

    public s a() {
        return this.f210a;
    }

    public List b() {
        return this.b;
    }
}
