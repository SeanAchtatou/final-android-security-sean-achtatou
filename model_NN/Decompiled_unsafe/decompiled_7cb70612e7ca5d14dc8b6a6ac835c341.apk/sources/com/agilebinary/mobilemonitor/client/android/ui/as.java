package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.View;

final class as implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ChangePasswordActivity f239a;

    as(ChangePasswordActivity changePasswordActivity) {
        this.f239a = changePasswordActivity;
    }

    public void onClick(View view) {
        this.f239a.setResult(0);
        this.f239a.finish();
    }
}
