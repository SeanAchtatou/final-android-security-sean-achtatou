package com.agilebinary.mobilemonitor.client.android.b.b;

import com.agilebinary.mobilemonitor.client.android.b.a;
import com.agilebinary.mobilemonitor.client.android.b.m;
import com.agilebinary.mobilemonitor.client.android.b.o;
import com.agilebinary.mobilemonitor.client.android.b.s;
import com.agilebinary.mobilemonitor.client.android.b.x;
import com.agilebinary.mobilemonitor.client.android.d.f;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;

public class b extends a implements a {
    private static final String b = f.a("ProductionAccountService");

    public com.agilebinary.mobilemonitor.client.android.b.c.a a(String str, String str2, m mVar) {
        com.agilebinary.mobilemonitor.a.a.a.b.a(b, "getAccount... ", str);
        o oVar = null;
        try {
            String format = String.format(x.a().d() + "ServiceAccountRetrieve?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s", "1", a(str), a(str2));
            com.agilebinary.mobilemonitor.a.a.a.b.a(b, "URL: ", format);
            oVar = this.f149a.a(format, x.a().n(), mVar);
            if (oVar.b()) {
                String b2 = b(oVar);
                com.agilebinary.mobilemonitor.a.a.a.b.b(b, "Response: " + b2);
                com.agilebinary.mobilemonitor.client.android.b.c.a aVar = new com.agilebinary.mobilemonitor.client.android.b.c.a(new JSONObject(b2));
                a(oVar);
                com.agilebinary.mobilemonitor.a.a.a.b.b(b, "...getAccount");
                return aVar;
            }
            throw new s(oVar.c());
        } catch (JSONException e) {
            throw new s(e);
        } catch (IOException e2) {
            throw new s(e2);
        } catch (Throwable th) {
            a(oVar);
            throw th;
        }
    }

    public void a(com.agilebinary.mobilemonitor.client.android.f fVar, String str, String str2, m mVar) {
        o oVar;
        com.agilebinary.mobilemonitor.a.a.a.b.b(b, "addLinkedAccount...");
        try {
            String format = String.format(x.a().d() + "ServiceAccountLinkAdd?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s&LinkKey=%4$s&LinkPassword=%5$s", "1", a(fVar.c()), a(fVar.d()), a(str), a(str2));
            com.agilebinary.mobilemonitor.a.a.a.b.a(b, "URL: ", format);
            oVar = this.f149a.a(format, new byte[0], x.a().n(), mVar);
            try {
                if (!oVar.b()) {
                    throw new s(oVar.c());
                }
                a(oVar);
                com.agilebinary.mobilemonitor.a.a.a.b.b(b, "...addLinkedAccount");
            } catch (Throwable th) {
                th = th;
                a(oVar);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            oVar = null;
        }
    }

    public void a(String str, String str2, String str3, m mVar) {
        o oVar;
        com.agilebinary.mobilemonitor.a.a.a.b.b(b, "changeAccountPassword...");
        try {
            String format = String.format(x.a().d() + "ServiceAccountUpdatePassword?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s&NewPassword=%4$s", "1", a(str), a(str2), a(str3));
            com.agilebinary.mobilemonitor.a.a.a.b.a(b, "URL: ", format);
            oVar = this.f149a.a(format, new byte[0], x.a().n(), mVar);
            try {
                if (!oVar.b()) {
                    throw new s(oVar.c());
                }
                a(oVar);
                com.agilebinary.mobilemonitor.a.a.a.b.b(b, "...changeAccountPassword");
            } catch (Throwable th) {
                th = th;
                a(oVar);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            oVar = null;
        }
    }

    public void b(com.agilebinary.mobilemonitor.client.android.f fVar, String str, String str2, m mVar) {
        o oVar;
        com.agilebinary.mobilemonitor.a.a.a.b.b(b, "removeLinkedAccount...");
        try {
            String format = String.format(x.a().d() + "ServiceAccountLinkRemove?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s&LinkKey=%4$s&LinkPassword=%5$s", "1", a(fVar.c()), a(fVar.d()), a(str), a(str2));
            com.agilebinary.mobilemonitor.a.a.a.b.a(b, "URL: ", format);
            oVar = this.f149a.a(format, new byte[0], x.a().n(), mVar);
            try {
                if (!oVar.b()) {
                    throw new s(oVar.c());
                }
                a(oVar);
                com.agilebinary.mobilemonitor.a.a.a.b.b(b, "...removeLinkedAccount");
            } catch (Throwable th) {
                th = th;
                a(oVar);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            oVar = null;
        }
    }

    public void b(String str, String str2, m mVar) {
        o oVar;
        com.agilebinary.mobilemonitor.a.a.a.b.b(b, "resetPassword...");
        try {
            String format = String.format(x.a().d() + "ServiceAccountUpdatePasswordReset?ProtocolVersion=%1$s&Key=%2$s&Locale=%3$s", "1", a(str), a(str2));
            com.agilebinary.mobilemonitor.a.a.a.b.a(b, "URL: ", format);
            oVar = this.f149a.a(format, new byte[0], x.a().n(), mVar);
            try {
                if (!oVar.b()) {
                    throw new s(oVar.c());
                }
                a(oVar);
                com.agilebinary.mobilemonitor.a.a.a.b.b(b, "...resetPassword");
            } catch (Throwable th) {
                th = th;
                a(oVar);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            oVar = null;
        }
    }

    public void b(String str, String str2, String str3, m mVar) {
        o oVar;
        com.agilebinary.mobilemonitor.a.a.a.b.b(b, "changeAccountPassword...");
        try {
            String format = String.format(x.a().d() + "ServiceAccountUpdateEmail?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s&Email=%4$s", "1", a(str), a(str2), a(str3));
            com.agilebinary.mobilemonitor.a.a.a.b.a(b, "URL: ", format);
            oVar = this.f149a.a(format, new byte[0], x.a().n(), mVar);
            try {
                if (!oVar.b()) {
                    throw new s(oVar.c());
                }
                a(oVar);
                com.agilebinary.mobilemonitor.a.a.a.b.b(b, "...changeAccountPassword");
            } catch (Throwable th) {
                th = th;
                a(oVar);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            oVar = null;
        }
    }

    public void c(String str, String str2, String str3, m mVar) {
        o oVar;
        com.agilebinary.mobilemonitor.a.a.a.b.b(b, "changeAccountAlias...");
        try {
            String format = String.format(x.a().d() + "ServiceAccountUpdateAlias?ProtocolVersion=%1$s&Key=%2$s&Password=%3$s&Alias=%4$s", "1", a(str), a(str2), a(str3));
            com.agilebinary.mobilemonitor.a.a.a.b.a(b, "URL: ", format);
            oVar = this.f149a.a(format, new byte[0], x.a().n(), mVar);
            try {
                if (!oVar.b()) {
                    throw new s(oVar.c());
                }
                a(oVar);
                com.agilebinary.mobilemonitor.a.a.a.b.b(b, "...changeAccountAlias");
            } catch (Throwable th) {
                th = th;
                a(oVar);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            oVar = null;
        }
    }
}
