package com.agilebinary.mobilemonitor.client.a.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.a;
import com.agilebinary.mobilemonitor.client.a.b;
import org.osmdroid.util.GeoPoint;

public abstract class d extends e implements com.agilebinary.mobilemonitor.client.a.d {

    /* renamed from: a  reason: collision with root package name */
    protected double f128a;
    protected double b;
    protected double c;
    protected long i;
    protected boolean j;
    protected boolean k;
    protected String l;
    protected boolean m;
    protected double n;
    protected boolean o;
    protected double p;

    public d(String str, long j2, long j3, a aVar, b bVar) {
        super(str, j2, j3, aVar, bVar);
        this.f128a = aVar.e();
        this.b = aVar.e();
        this.c = aVar.e();
        this.i = bVar.a(aVar.d());
        this.j = aVar.a();
        this.k = aVar.a();
        this.l = aVar.g();
        this.m = aVar.a();
        if (this.m) {
            this.n = aVar.e();
        }
        this.o = aVar.a();
        if (this.o) {
            this.p = aVar.e();
        }
    }

    public double a() {
        return this.f128a;
    }

    public CharSequence a(Context context) {
        return com.agilebinary.mobilemonitor.client.android.d.b.a(context, Double.valueOf(k()));
    }

    public double b() {
        return this.c;
    }

    public GeoPoint c() {
        return new GeoPoint(this.f128a, this.b);
    }

    public boolean c_() {
        return true;
    }

    public Double i() {
        return Double.valueOf(this.c);
    }

    public double j() {
        return this.b;
    }

    public double k() {
        return this.n;
    }

    public long l() {
        return this.i;
    }

    public boolean m() {
        return this.j;
    }

    public boolean n() {
        return this.k;
    }
}
