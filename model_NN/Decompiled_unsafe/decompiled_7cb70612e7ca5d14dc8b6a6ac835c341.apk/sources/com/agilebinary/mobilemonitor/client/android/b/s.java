package com.agilebinary.mobilemonitor.client.android.b;

import java.io.Serializable;

public class s extends Exception implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private int f168a;
    private Exception b;

    public s() {
    }

    public s(int i) {
        this.f168a = i;
    }

    public s(Exception exc) {
        this.b = exc;
    }

    public boolean a() {
        return o.a(this.f168a);
    }

    public boolean b() {
        return this.b != null;
    }

    public boolean c() {
        return o.b(this.f168a);
    }

    public boolean d() {
        return o.c(this.f168a);
    }
}
