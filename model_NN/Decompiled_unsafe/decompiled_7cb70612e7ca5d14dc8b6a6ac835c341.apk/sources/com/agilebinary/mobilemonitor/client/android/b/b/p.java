package com.agilebinary.mobilemonitor.client.android.b.b;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.android.b.a;
import com.agilebinary.mobilemonitor.client.android.b.b;
import com.agilebinary.mobilemonitor.client.android.b.d;
import com.agilebinary.mobilemonitor.client.android.b.f;
import com.agilebinary.mobilemonitor.client.android.b.g;
import com.agilebinary.mobilemonitor.client.android.b.j;
import com.agilebinary.mobilemonitor.client.android.b.k;
import com.agilebinary.mobilemonitor.client.android.b.l;
import com.agilebinary.mobilemonitor.client.android.b.n;

public class p implements n {
    public a a() {
        return new b();
    }

    public b a(Context context) {
        return new c(context);
    }

    public d b(Context context) {
        return new g(context);
    }

    public g b() {
        return new l();
    }

    public f c() {
        return new k();
    }

    public j d() {
        return new m();
    }

    public l e() {
        return new o();
    }

    public k f() {
        return new n();
    }
}
