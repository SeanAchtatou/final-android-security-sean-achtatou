package com.agilebinary.mobilemonitor.client.android.ui;

import com.agilebinary.mobilemonitor.client.android.b.e;
import com.agilebinary.phonebeagle.client.R;

final class m implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f326a;
    final /* synthetic */ String b;
    final /* synthetic */ AddressbookActivity c;

    m(AddressbookActivity addressbookActivity, String str, String str2) {
        this.c = addressbookActivity;
        this.f326a = str;
        this.b = str2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.agilebinary.mobilemonitor.client.android.ui.AddressbookActivity.a(com.agilebinary.mobilemonitor.client.android.ui.AddressbookActivity, boolean):boolean
     arg types: [com.agilebinary.mobilemonitor.client.android.ui.AddressbookActivity, int]
     candidates:
      com.agilebinary.mobilemonitor.client.android.ui.AddressbookActivity.a(com.agilebinary.mobilemonitor.client.android.ui.AddressbookActivity, int):int
      com.agilebinary.mobilemonitor.client.android.ui.AddressbookActivity.a(com.agilebinary.mobilemonitor.client.android.ui.AddressbookActivity, com.agilebinary.mobilemonitor.client.android.ui.a.g):void
      com.agilebinary.mobilemonitor.client.android.ui.AddressbookActivity.a(com.agilebinary.mobilemonitor.client.android.ui.ah, java.lang.String):void
      com.agilebinary.mobilemonitor.client.android.ui.AddressbookActivity.a(java.lang.String, java.lang.String):void
      com.agilebinary.mobilemonitor.client.android.ui.AddressbookActivity.a(com.agilebinary.mobilemonitor.client.android.ui.AddressbookActivity, boolean):boolean */
    public void run() {
        if (this.f326a == null) {
            this.c.getString(R.string.label_addressbook_custom_entry);
        }
        e eVar = new e(this.f326a, this.b, false, null, 0);
        eVar.a(true);
        this.c.t.b().add(eVar);
        boolean unused = this.c.v = true;
        this.c.m();
    }
}
