package com.agilebinary.mobilemonitor.client.android.ui;

import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TableRow;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.a.b.b;
import com.agilebinary.mobilemonitor.client.a.b.v;
import com.agilebinary.mobilemonitor.client.a.b.w;
import com.agilebinary.mobilemonitor.client.android.d.g;
import com.agilebinary.phonebeagle.client.R;

public class EventDetailsActivity_WEB extends aw {
    private TextView n;
    private TextView o;
    private TextView p;
    private TextView q;
    private TextView r;
    private TextView s;
    private TextView t;
    private TextView u;
    private TableRow v;
    private TableRow w;
    private TableRow x;
    private TableRow y;
    private TableRow z;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public void a(ViewGroup viewGroup) {
        ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.eventdetails_web, viewGroup, true);
        this.n = (TextView) findViewById(R.id.eventdetails_web_kind);
        this.o = (TextView) findViewById(R.id.eventdetails_web_time);
        this.p = (TextView) findViewById(R.id.eventdetails_web_keywords);
        this.q = (TextView) findViewById(R.id.eventdetails_web_domain);
        this.r = (TextView) findViewById(R.id.eventdetails_web_title);
        this.s = (TextView) findViewById(R.id.eventdetails_web_numvisits);
        this.t = (TextView) findViewById(R.id.eventdetails_web_bookmarked);
        this.u = (TextView) findViewById(R.id.eventdetails_web_speed);
        this.v = (TableRow) findViewById(R.id.eventdetails_web_row_keywords);
        this.w = (TableRow) findViewById(R.id.eventdetails_web_row_address);
        this.x = (TableRow) findViewById(R.id.eventdetails_web_row_title);
        this.y = (TableRow) findViewById(R.id.eventdetails_web_row_numvisits);
        this.z = (TableRow) findViewById(R.id.eventdetails_web_row_bookmarked);
        this.q.setMovementMethod(LinkMovementMethod.getInstance());
    }

    /* access modifiers changed from: protected */
    public void a(b bVar) {
        super.a(bVar);
        if (bVar instanceof v) {
            this.n.setText((int) R.string.label_event_web_kind_search);
            this.v.setVisibility(0);
            this.w.setVisibility(8);
            this.x.setVisibility(8);
            this.y.setVisibility(8);
            this.z.setVisibility(8);
            v vVar = (v) bVar;
            this.p.setText(vVar.b());
            this.o.setText(g.a(this).c(vVar.a()));
        } else if (bVar instanceof w) {
            this.n.setText((int) R.string.label_event_web_kind_visit);
            this.v.setVisibility(8);
            this.w.setVisibility(0);
            this.x.setVisibility(0);
            this.y.setVisibility(0);
            this.z.setVisibility(0);
            w wVar = (w) bVar;
            this.q.setText(Html.fromHtml("<a href=\"" + wVar.b() + "\" >" + wVar.b() + "</a>"));
            this.r.setText(wVar.c());
            this.s.setText(String.valueOf(wVar.n()));
            this.t.setText(getString(wVar.m() ? R.string.label_yes : R.string.label_no));
            this.o.setText(g.a(this).c(wVar.a()));
        }
        com.agilebinary.mobilemonitor.client.a.b.g gVar = (com.agilebinary.mobilemonitor.client.a.b.g) bVar;
        this.u.setText(com.agilebinary.mobilemonitor.client.android.d.b.a(this, gVar));
        if (com.agilebinary.mobilemonitor.client.android.d.b.a(gVar)) {
            this.u.setTextColor(getResources().getColor(R.color.warning));
        } else {
            this.u.setTextColor(getResources().getColor(R.color.text));
        }
    }
}
