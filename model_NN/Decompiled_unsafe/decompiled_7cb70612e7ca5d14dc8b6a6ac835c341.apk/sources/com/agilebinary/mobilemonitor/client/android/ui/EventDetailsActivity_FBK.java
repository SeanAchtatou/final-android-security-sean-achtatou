package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.a.b.b;
import com.agilebinary.mobilemonitor.client.a.b.i;
import com.agilebinary.phonebeagle.client.R;

public class EventDetailsActivity_FBK extends aw {
    private TextView n;
    private TextView o;
    private TextView p;
    private TextView q;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public void a(ViewGroup viewGroup) {
        ((LayoutInflater) getSystemService("layout_inflater")).inflate((int) R.layout.eventdetails_fbk, viewGroup, true);
        this.n = (TextView) findViewById(R.id.eventdetails_fbk_author);
        this.q = (TextView) findViewById(R.id.eventdetails_fbk_time);
        this.o = (TextView) findViewById(R.id.eventdetails_fbk_speed);
        this.p = (TextView) findViewById(R.id.eventdetails_fbk_message);
    }

    /* access modifiers changed from: protected */
    public void a(b bVar) {
        super.a(bVar);
        i iVar = (i) bVar;
        this.q.setText(iVar.m());
        this.n.setText(iVar.b());
        this.o.setText(com.agilebinary.mobilemonitor.client.android.d.b.a(this, iVar));
        this.p.setText(iVar.c());
        if (com.agilebinary.mobilemonitor.client.android.d.b.a(iVar)) {
            this.o.setTextColor(getResources().getColor(R.color.warning));
        } else {
            this.o.setTextColor(getResources().getColor(R.color.text));
        }
    }
}
