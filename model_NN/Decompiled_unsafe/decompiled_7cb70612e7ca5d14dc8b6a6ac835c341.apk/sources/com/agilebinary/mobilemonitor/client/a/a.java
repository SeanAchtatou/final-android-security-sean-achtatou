package com.agilebinary.mobilemonitor.client.a;

import android.content.Context;
import com.agilebinary.mobilemonitor.client.a.a.c;
import com.agilebinary.mobilemonitor.client.a.b.b;
import com.agilebinary.mobilemonitor.client.a.b.h;
import com.agilebinary.mobilemonitor.client.a.b.i;
import com.agilebinary.mobilemonitor.client.a.b.j;
import com.agilebinary.mobilemonitor.client.a.b.k;
import com.agilebinary.mobilemonitor.client.a.b.l;
import com.agilebinary.mobilemonitor.client.a.b.m;
import com.agilebinary.mobilemonitor.client.a.b.n;
import com.agilebinary.mobilemonitor.client.a.b.o;
import com.agilebinary.mobilemonitor.client.a.b.p;
import com.agilebinary.mobilemonitor.client.a.b.q;
import com.agilebinary.mobilemonitor.client.a.b.r;
import com.agilebinary.mobilemonitor.client.a.b.t;
import com.agilebinary.mobilemonitor.client.a.b.u;
import com.agilebinary.mobilemonitor.client.a.b.v;
import com.agilebinary.mobilemonitor.client.a.b.w;
import com.agilebinary.mobilemonitor.client.a.b.x;
import com.agilebinary.mobilemonitor.client.android.d.f;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.InputStream;
import java.util.zip.Inflater;

public class a {

    /* renamed from: a  reason: collision with root package name */
    static final String f123a = f.a("EventDeserializer");
    static byte[] b = new byte[8192];
    static byte[] c = new byte[2048];
    static Inflater d = new Inflater(false);

    public static synchronized b a(String str, byte b2, long j, long j2, String str2, boolean z, boolean z2, int i, InputStream inputStream, b bVar, e eVar, Context context, ByteArrayOutputStream byteArrayOutputStream) {
        com.agilebinary.mobilemonitor.client.a.a.a aVar;
        com.agilebinary.mobilemonitor.client.a.a.b bVar2;
        b jVar;
        synchronized (a.class) {
            com.agilebinary.mobilemonitor.a.a.a.b.b(f123a, "decode...");
            try {
                com.agilebinary.mobilemonitor.a.a.a.b.b(f123a, "decode1...");
                short a2 = a(inputStream);
                if (a2 != 0) {
                    throw new IllegalArgumentException("unknown encoding version: " + ((int) a2));
                }
                byte b3 = b(inputStream);
                if (z) {
                    throw new IllegalArgumentException("PENDING: encryption not yet implemented");
                }
                if (z2) {
                    d.reset();
                    com.agilebinary.mobilemonitor.client.a.a.b bVar3 = new com.agilebinary.mobilemonitor.client.a.a.b(inputStream, d, c, i - 3);
                    aVar = new com.agilebinary.mobilemonitor.client.a.a.a(new c(bVar3, b));
                    bVar2 = bVar3;
                } else {
                    aVar = new com.agilebinary.mobilemonitor.client.a.a.a(inputStream);
                    bVar2 = null;
                }
                com.agilebinary.mobilemonitor.a.a.a.b.b(f123a, "decode2...");
                switch (b3) {
                    case 1:
                        jVar = new h(str, j, j2, aVar, bVar);
                        break;
                    case 2:
                        jVar = new t(str, j, j2, aVar, bVar);
                        break;
                    case 3:
                        jVar = new r(str, j, j2, aVar, bVar, eVar, context, byteArrayOutputStream);
                        break;
                    case 4:
                        jVar = new o(str, j, j2, aVar, bVar);
                        break;
                    case 5:
                        jVar = new p(str, j, j2, aVar, bVar);
                        break;
                    case 6:
                        jVar = new q(str, j, j2, aVar, bVar);
                        break;
                    case 7:
                        jVar = new m(str, j, j2, aVar, bVar);
                        break;
                    case 8:
                        jVar = new l(str, j, j2, aVar, bVar);
                        break;
                    case 9:
                        jVar = new k(str, j, j2, aVar, bVar);
                        break;
                    case 10:
                        jVar = new u(str, j, j2, aVar, bVar);
                        break;
                    case 11:
                        jVar = new com.agilebinary.mobilemonitor.client.a.b.a(str, j, j2, aVar, bVar);
                        break;
                    case 12:
                        jVar = new n(str, j, j2, aVar, bVar);
                        break;
                    case 13:
                        jVar = new v(str, j, j2, aVar, bVar);
                        break;
                    case 14:
                        jVar = new w(str, j, j2, aVar, bVar);
                        break;
                    case 15:
                        jVar = new i(str, j, j2, aVar, bVar);
                        break;
                    case 16:
                        jVar = new x(str, j, j2, aVar, bVar);
                        break;
                    case 17:
                        jVar = new j(str, j, j2, aVar, bVar);
                        break;
                    default:
                        throw new IllegalArgumentException("unknown content type: " + ((int) b3));
                }
                if (bVar2 != null && bVar2.b() > 0) {
                    com.agilebinary.mobilemonitor.a.a.a.b.e(f123a, "not all bytes read from compressed stream whhen deserializing " + jVar);
                }
                com.agilebinary.mobilemonitor.a.a.a.b.b(f123a, "...decode");
            } catch (Throwable th) {
                com.agilebinary.mobilemonitor.a.a.a.b.b(f123a, "...decode");
                throw th;
            }
        }
        return jVar;
    }

    private static short a(InputStream inputStream) {
        int read = inputStream.read();
        int read2 = inputStream.read();
        if ((read | read2) >= 0) {
            return (short) ((read << 8) + (read2 << 0));
        }
        throw new EOFException();
    }

    private static final byte b(InputStream inputStream) {
        int read = inputStream.read();
        if (read >= 0) {
            return (byte) read;
        }
        throw new EOFException();
    }
}
