package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.View;

final class ap implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ChangeEmailActivity f236a;

    ap(ChangeEmailActivity changeEmailActivity) {
        this.f236a = changeEmailActivity;
    }

    public void onClick(View view) {
        this.f236a.setResult(0);
        this.f236a.finish();
    }
}
