package com.agilebinary.mobilemonitor.client.android.c;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.agilebinary.mobilemonitor.a.a.a.b;

class o extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ c f177a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public o(c cVar, Context context, String str, SQLiteDatabase.CursorFactory cursorFactory, int i) {
        super(context, str, cursorFactory, i);
        this.f177a = cVar;
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        String format = String.format("CREATE TABLE %1$s (%2$s INTEGER PRIMAY KEY, %3$s INTEGER, %4$s BLOB, %5$s STRING, %6$s STRING, %7$s DOUBLE,%8$s DOUBLE, %9$s DOUBLE, %10$s INTEGER, %11$s INTEGER, %12$s INTEGER )", "location", i.f176a, i.b, i.c, "line1", "line2", "accuracy", "lat", "lon", "contenttype", "powersave", "valloc");
        b.b(c.f175a, String.format("Executing SQL: %1$s", format));
        sQLiteDatabase.execSQL(format);
        String format2 = String.format("CREATE TABLE %1$s (%2$s INTEGER PRIMAY KEY, %3$s INTEGER, %4$s BLOB, %5$s STRING, %6$s STRING )", "call", f.f176a, f.b, f.c, "line1", "line2");
        b.b(c.f175a, String.format("Executing SQL: %1$s", format2));
        sQLiteDatabase.execSQL(format2);
        String format3 = String.format("CREATE TABLE %1$s (%2$s INTEGER PRIMAY KEY, %3$s INTEGER, %4$s BLOB, %5$s STRING, %6$s STRING )", "sms", k.f176a, k.b, k.c, "dir", "text");
        b.b(c.f175a, String.format("Executing SQL: %1$s", format3));
        sQLiteDatabase.execSQL(format3);
        String format4 = String.format("CREATE TABLE %1$s (%2$s INTEGER PRIMAY KEY, %3$s INTEGER, %4$s BLOB, %5$s STRING, %6$s STRING, %7$s BLOB )", "mms", j.f176a, j.b, j.c, "dir", "text", "thumbnail");
        b.b(c.f175a, String.format("Executing SQL: %1$s", format4));
        sQLiteDatabase.execSQL(format4);
        String format5 = String.format("CREATE TABLE %1$s (%2$s INTEGER PRIMAY KEY, %3$s INTEGER, %4$s BLOB, %5$s STRING, %6$s STRING )", "web", m.f176a, m.b, m.c, "line1", "line2");
        b.b(c.f175a, String.format("Executing SQL: %1$s", format5));
        sQLiteDatabase.execSQL(format5);
        String format6 = String.format("CREATE TABLE %1$s (%2$s INTEGER PRIMAY KEY, %3$s INTEGER, %4$s BLOB, %5$s STRING)", "syst_m", l.f176a, l.b, l.c, "line1");
        b.b(c.f175a, String.format("Executing SQL: %1$s", format6));
        sQLiteDatabase.execSQL(format6);
        String format7 = String.format("CREATE TABLE %1$s (%2$s INTEGER PRIMAY KEY, %3$s INTEGER, %4$s BLOB, %5$s STRING, %6$s STRING, %7$s INTEGER )", e.f, e.f176a, e.b, e.c, e.g, e.h, e.i);
        b.b(c.f175a, String.format("Executing SQL: %1$s", format7));
        sQLiteDatabase.execSQL(format7);
        String format8 = String.format("CREATE TABLE %1$s (%2$s INTEGER PRIMAY KEY, %3$s INTEGER, %4$s BLOB, %5$s STRING, %6$s STRING, %7$s INTEGER )", g.f, g.f176a, g.b, g.c, g.g, g.h, g.i);
        b.b(c.f175a, String.format("Executing SQL: %1$s", format8));
        sQLiteDatabase.execSQL(format8);
        String format9 = String.format("CREATE TABLE %1$s (%2$s INTEGER PRIMAY KEY, %3$s INTEGER, %4$s BLOB, %5$s STRING, %6$s STRING, %7$s INTEGER )", n.f, n.f176a, n.b, n.c, n.g, n.h, n.i);
        b.b(c.f175a, String.format("Executing SQL: %1$s", format9));
        sQLiteDatabase.execSQL(format9);
        String format10 = String.format("CREATE TABLE %1$s (%2$s INTEGER PRIMAY KEY, %3$s INTEGER, %4$s BLOB, %5$s STRING, %6$s STRING, %7$s INTEGER )", h.f, h.f176a, h.b, h.c, h.g, h.h, h.i);
        b.b(c.f175a, String.format("Executing SQL: %1$s", format10));
        sQLiteDatabase.execSQL(format10);
        String format11 = String.format("CREATE TABLE %1$s (%2$s INTEGER PRIMAY KEY , %3$s INTEGER,  UNIQUE (%2$s) ON CONFLICT REPLACE ) ", "curday", "eventtype", "day");
        b.b(c.f175a, String.format("Executing SQL: %1$s", format11));
        sQLiteDatabase.execSQL(format11);
        String format12 = String.format("CREATE TABLE %1$s (%2$s INTEGER PRIMAY KEY , %3$s INTEGER,  UNIQUE (%2$s) ON CONFLICT REPLACE ) ", "oldestevent", "eventtype", "oldest");
        b.b(c.f175a, String.format("Executing SQL: %1$s", format12));
        sQLiteDatabase.execSQL(format12);
        String format13 = String.format("CREATE TABLE %1$s (%2$s INTEGER PRIMAY KEY , %3$s INTEGER,  UNIQUE (%2$s) ON CONFLICT REPLACE ) ", "lastseen", "eventtype", "lastid");
        b.b(c.f175a, String.format("Executing SQL: %1$s", format13));
        sQLiteDatabase.execSQL(format13);
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        b.b(c.f175a, String.format("Upgrading database from version %1$s to %2$s, destroying all existing data", Integer.valueOf(i), Integer.valueOf(i2)));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", "location"));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", "call"));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", "sms"));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", "mms"));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", "web"));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", "syst_m"));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", e.f));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", g.f));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", n.f));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", h.f));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", "curday"));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", "oldestevent"));
        sQLiteDatabase.execSQL(String.format("DROP TABLE IF EXISTS %1$s", "lastseen"));
        onCreate(sQLiteDatabase);
    }
}
