package com.agilebinary.mobilemonitor.client.android.ui;

import android.content.Context;
import android.content.res.Resources;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.android.b.c;
import com.agilebinary.mobilemonitor.client.android.b.x;
import com.agilebinary.phonebeagle.client.R;
import java.text.SimpleDateFormat;
import java.util.Date;

public class z extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    protected TextView f339a = ((TextView) findViewById(R.id.appblock_rowview_packagename));
    protected TextView b = ((TextView) findViewById(R.id.appblock_rowview_link));
    protected TextView c = ((TextView) findViewById(R.id.appblock_rowview_label));
    protected TextView d = ((TextView) findViewById(R.id.appblock_rowview_transferstatus));
    protected TextView e = ((TextView) findViewById(R.id.appblock_rowview_status_textview));
    protected CheckBox f = ((CheckBox) findViewById(R.id.appblock_rowview_spinner_checkbox));
    protected c g;
    final /* synthetic */ AppBlockActivity h;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, com.agilebinary.mobilemonitor.client.android.ui.z, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public z(AppBlockActivity appBlockActivity, Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.h = appBlockActivity;
        ((LayoutInflater) context.getSystemService("layout_inflater")).inflate((int) R.layout.appblock_rowview, (ViewGroup) this, true);
        this.f.setOnCheckedChangeListener(new aa(this, appBlockActivity));
    }

    public void a(c cVar) {
        String string;
        int i;
        int i2 = R.color.red;
        Log.d(AppBlockActivity.f193a, "bind: " + cVar);
        this.g = cVar;
        this.b.setText(Html.fromHtml("<a href=\"" + x.a().b(cVar.f156a) + "\">" + cVar.f156a + "</a>"));
        this.b.setMovementMethod(LinkMovementMethod.getInstance());
        this.f339a.setText(cVar.f156a);
        this.c.setText(cVar.b);
        String format = SimpleDateFormat.getDateTimeInstance().format(new Date(cVar.d));
        if ("client_set".equals(cVar.c)) {
            string = this.h.getString(R.string.label_block_list_status_client_set_time, new Object[]{format});
            i = R.color.red;
        } else if ("device_write".equals(cVar.c)) {
            string = this.h.getString(R.string.label_block_list_status_device_write_time, new Object[]{format});
            i = R.color.green;
        } else {
            string = this.h.getString(R.string.label_block_list_status_unknown);
            i = R.color.black;
        }
        this.d.setText(string);
        this.d.setTextColor(getResources().getColor(i));
        this.g.f = true;
        this.f.setChecked(!this.g.e);
        this.g.f = false;
        this.e.setText(this.h.getString(this.g.e ? R.string.label_app_blocked : R.string.label_app_unblocked));
        TextView textView = this.e;
        Resources resources = getResources();
        if (!this.g.e) {
            i2 = R.color.green;
        }
        textView.setTextColor(resources.getColor(i2));
    }
}
