package com.agilebinary.mobilemonitor.client.android.ui;

import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.agilebinary.mobilemonitor.client.a.b.r;
import com.agilebinary.mobilemonitor.client.a.b.s;
import com.agilebinary.mobilemonitor.client.android.d.b;
import com.agilebinary.mobilemonitor.client.android.d.g;
import com.agilebinary.phonebeagle.client.R;

public class EventDetailsActivity_MMS extends aw {
    private TableLayout n;
    private TextView o;
    private TextView p;
    private TextView q;
    private TableRow r;
    private TableRow s;
    private TableRow t;
    private LayoutInflater u;
    private ViewGroup v;

    private int a(byte b) {
        switch (b) {
            case -1:
                return R.string.empty;
            case 0:
            default:
                return R.string.empty;
            case 1:
            case 2:
                return R.string.label_event_mms_incoming;
            case 3:
                return R.string.empty;
            case 4:
                return R.string.label_event_mms_outgoing_blocked;
            case 5:
                return R.string.label_event_mms_incoming_blocked;
        }
    }

    private int a(int i, int i2, String str) {
        TableRow tableRow = (TableRow) this.u.inflate((int) R.layout.eventdetails_row, (ViewGroup) null);
        ((TextView) tableRow.getChildAt(0)).setText(i2);
        ((TextView) tableRow.getChildAt(1)).setText(str);
        this.n.addView(tableRow, i);
        return 1;
    }

    private int a(int i, int i2, String str, String str2) {
        return a(i, i2, b.a(str, str2));
    }

    private int a(int i, int i2, String[] strArr, String[] strArr2) {
        for (int i3 = 0; i3 < strArr2.length; i3++) {
            a(i + i3, i2, strArr[i3], strArr2[i3]);
        }
        return strArr.length;
    }

    private int a(int i, s sVar) {
        boolean z;
        boolean z2;
        TableRow tableRow = new TableRow(this);
        tableRow.setBackgroundColor(i % 2 == 0 ? R.color.background_mms_part_even : R.color.background_mms_part_odd);
        tableRow.setLayoutParams(new TableLayout.LayoutParams(-2, -2));
        if (sVar.d()) {
            TextView textView = (TextView) this.u.inflate((int) R.layout.eventdetails_mms_textpart, (ViewGroup) null);
            TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(-1, -2);
            layoutParams.span = 2;
            textView.setLayoutParams(layoutParams);
            tableRow.addView(textView);
            textView.setText(sVar.f());
            z = true;
        } else {
            z = false;
        }
        if (!z && sVar.c() != null) {
            try {
                LinearLayout linearLayout = (LinearLayout) this.u.inflate((int) R.layout.eventdetails_mms_imagepart, (ViewGroup) null);
                TableRow.LayoutParams layoutParams2 = new TableRow.LayoutParams(-1, -2);
                layoutParams2.span = 2;
                linearLayout.setLayoutParams(layoutParams2);
                tableRow.addView(linearLayout);
                ImageView imageView = (ImageView) linearLayout.getChildAt(0);
                imageView.setAdjustViewBounds(true);
                imageView.setImageBitmap(BitmapFactory.decodeByteArray(sVar.c(), 0, sVar.c().length));
                z2 = true;
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
                z2 = z;
            }
            if (!z2) {
                TextView textView2 = (TextView) this.u.inflate((int) R.layout.eventdetails_mms_textpart, (ViewGroup) null);
                TableRow.LayoutParams layoutParams3 = new TableRow.LayoutParams(-1, -2);
                layoutParams3.span = 2;
                textView2.setLayoutParams(layoutParams3);
                tableRow.addView(textView2);
                textView2.setText(getString(R.string.label_event_mms_part_cantshow, new Object[]{sVar.a(), sVar.b(), Integer.valueOf(sVar.e())}));
            }
        }
        this.n.addView(tableRow, i);
        return 1;
    }

    private int a(ViewGroup viewGroup, View view) {
        int childCount = viewGroup.getChildCount();
        for (int i = 0; i < childCount; i++) {
            if (viewGroup.getChildAt(i) == view) {
                return i;
            }
        }
        return -1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* access modifiers changed from: protected */
    public void a(ViewGroup viewGroup) {
        this.u = (LayoutInflater) getSystemService("layout_inflater");
        this.u.inflate((int) R.layout.eventdetails_mms, viewGroup, true);
        this.n = (TableLayout) findViewById(R.id.eventdetails_mms_table);
        this.o = (TextView) findViewById(R.id.eventdetails_mms_kind);
        this.p = (TextView) findViewById(R.id.eventdetails_mms_time);
        this.q = (TextView) findViewById(R.id.eventdetails_mms_speed);
        this.r = (TableRow) findViewById(R.id.eventdetails_mms_row_kind);
        this.s = (TableRow) findViewById(R.id.eventdetails_mms_row_time);
        this.t = (TableRow) findViewById(R.id.eventdetails_mms_row_speed);
        this.v = viewGroup;
    }

    /* access modifiers changed from: protected */
    public void a(com.agilebinary.mobilemonitor.client.a.b.b bVar) {
        super.a(bVar);
        int a2 = a(this.n, this.s);
        int a3 = (a(this.n, this.t) - a2) - 1;
        for (int i = 0; i < a3; i++) {
            this.n.removeViewAt(a2 + 1);
        }
        r rVar = (r) bVar;
        this.o.setText(getResources().getString(a(rVar.c())));
        this.p.setText(g.a(this).c(rVar.c() == 1 ? rVar.b() : rVar.a()));
        this.q.setText(b.a(this, rVar));
        if (rVar.c() != 2 || !b.a(rVar)) {
            this.q.setTextColor(getResources().getColor(R.color.text));
        } else {
            this.q.setTextColor(getResources().getColor(R.color.warning));
        }
        int a4 = a(this.n, this.s) + 1;
        if (rVar.c() == 1) {
            a4 += a(a4, (int) R.string.label_event_mms_header_from, rVar.n(), rVar.o());
        }
        int a5 = a4 + a(a4, (int) R.string.label_event_mms_header_to, rVar.p(), rVar.q());
        int a6 = a5 + a(a5, (int) R.string.label_event_mms_header_cc, rVar.r(), rVar.s());
        int a7 = a6 + a(a6, (int) R.string.label_event_mms_header_bcc, rVar.t(), rVar.u());
        if (!b.a(rVar.m())) {
            a7 += a(a7, (int) R.string.label_event_mms_header_subject, rVar.m());
        }
        int i2 = a7;
        for (s a8 : rVar.v()) {
            i2 += a(i2, a8);
        }
    }
}
