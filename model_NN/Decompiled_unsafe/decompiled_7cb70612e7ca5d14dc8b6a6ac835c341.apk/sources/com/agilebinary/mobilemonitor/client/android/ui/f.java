package com.agilebinary.mobilemonitor.client.android.ui;

import android.view.View;
import com.agilebinary.phonebeagle.client.R;

final class f implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ AccountInfoActivity f319a;

    f(AccountInfoActivity accountInfoActivity) {
        this.f319a = accountInfoActivity;
    }

    public void onClick(View view) {
        ChangeEmailActivity.a(this.f319a, R.string.label_changeemail_text_normal, false, this.f319a.p, 1);
    }
}
