package org.a.a.a.a;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class a {
    private static MessageDigest a() {
        return a("SHA-256");
    }

    static MessageDigest a(String str) {
        try {
            return MessageDigest.getInstance(str);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static byte[] a(byte[] bArr) {
        return a().digest(bArr);
    }
}
