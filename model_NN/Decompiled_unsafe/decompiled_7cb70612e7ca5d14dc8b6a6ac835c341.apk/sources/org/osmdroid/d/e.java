package org.osmdroid.d;

import android.view.GestureDetector;
import android.view.MotionEvent;

class e implements GestureDetector.OnGestureListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f408a;

    private e(b bVar) {
        this.f408a = bVar;
    }

    public boolean onDown(MotionEvent motionEvent) {
        this.f408a.o.a(this.f408a.p);
        return true;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        int worldSizePx = this.f408a.getWorldSizePx();
        this.f408a.j.fling(this.f408a.getScrollX(), this.f408a.getScrollY(), (int) (-f), (int) (-f2), -worldSizePx, worldSizePx, -worldSizePx, worldSizePx);
        return true;
    }

    public void onLongPress(MotionEvent motionEvent) {
        this.f408a.a(motionEvent);
    }

    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        this.f408a.scrollBy((int) f, (int) f2);
        return true;
    }

    public void onShowPress(MotionEvent motionEvent) {
    }

    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return this.f408a.b(motionEvent);
    }
}
