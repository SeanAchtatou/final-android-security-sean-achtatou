package org.osmdroid.d.a;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import org.c.b;
import org.c.c;
import org.osmdroid.c.d;
import org.osmdroid.c.g;
import org.osmdroid.d.h;
import org.osmdroid.util.f;

public class k extends f {
    private static final b c = c.a(k.class);

    /* renamed from: a  reason: collision with root package name */
    protected final g f405a;
    protected final Paint b = new Paint();
    private final Rect d = new Rect();
    private final Rect e = new Rect();
    private int f;

    public k(g gVar, org.osmdroid.c cVar) {
        super(cVar);
        this.f405a = gVar;
    }

    public void a(Canvas canvas, int i, int i2, Rect rect) {
        int a2 = org.osmdroid.d.b.a(i2);
        int i3 = (rect.left >> a2) - 1;
        int i4 = rect.right >> a2;
        int i5 = (rect.top >> a2) - 1;
        int i6 = rect.bottom >> a2;
        int i7 = 1 << i;
        this.f405a.a(((i6 - i5) + 1) * ((i4 - i3) + 1));
        for (int i8 = i5; i8 <= i6; i8++) {
            for (int i9 = i3; i9 <= i4; i9++) {
                Drawable a3 = this.f405a.a(new d(i, f.a(i9, i7), f.a(i8, i7)));
                if (a3 != null) {
                    this.d.set(i9 * i2, i8 * i2, (i9 * i2) + i2, (i8 * i2) + i2);
                    a(canvas, a3, this.d);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, Drawable drawable, Rect rect) {
        rect.offset(-this.f, -this.f);
        drawable.setBounds(rect);
        drawable.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, org.osmdroid.d.b bVar) {
    }

    public void a(org.osmdroid.d.b bVar) {
        this.f405a.a();
    }

    public void a(boolean z) {
        this.f405a.a(z);
    }

    public int b() {
        return this.f405a.b();
    }

    /* access modifiers changed from: protected */
    public void b(Canvas canvas, org.osmdroid.d.b bVar) {
        h projection = bVar.getProjection();
        this.f = 1 << ((projection.c() + projection.b()) - 1);
        canvas.getClipBounds(this.e);
        this.e.offset(this.f, this.f);
        a(canvas, projection.c(), projection.a(), this.e);
    }

    public int c() {
        return this.f405a.c();
    }
}
