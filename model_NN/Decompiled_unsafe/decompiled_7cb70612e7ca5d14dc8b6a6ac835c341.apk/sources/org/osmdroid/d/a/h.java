package org.osmdroid.d.a;

import android.graphics.Point;
import android.graphics.drawable.Drawable;
import org.osmdroid.c;
import org.osmdroid.d;
import org.osmdroid.util.GeoPoint;

public class h {
    protected static final Point b = new Point(26, 94);
    public final long c;
    public final String d;
    public final String e;
    public final GeoPoint f;
    protected Drawable g;
    protected Point h;
    protected j i;

    public h(long j, String str, String str2, GeoPoint geoPoint) {
        this.d = str;
        this.e = str2;
        this.f = geoPoint;
        this.c = j;
    }

    public h(String str, String str2, GeoPoint geoPoint) {
        this(-1, str, str2, geoPoint);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.osmdroid.util.GeoPoint.<init>(double, double):void
     arg types: [int, int]
     candidates:
      org.osmdroid.util.GeoPoint.<init>(int, int):void
      org.osmdroid.util.GeoPoint.<init>(android.os.Parcel, org.osmdroid.util.e):void
      org.osmdroid.util.GeoPoint.<init>(double, double):void */
    public static h a(Drawable drawable, Point point, j jVar, c cVar) {
        h hVar = new h("<default>", "used when no marker is specified", new GeoPoint(0.0d, 0.0d));
        if (drawable == null) {
            drawable = cVar.b(d.marker_default);
        }
        hVar.g = drawable;
        if (point != null) {
            hVar.h = point;
            if (jVar == null) {
                hVar.i = j.CUSTOM;
            } else {
                hVar.i = jVar;
            }
        } else if (jVar == null) {
            hVar.b();
        } else {
            hVar.i = jVar;
            hVar.b();
        }
        return hVar;
    }

    public static void a(Drawable drawable, int i2) {
        drawable.setState(new int[]{(-i2) & 1, i2 & 2, i2 & 4});
    }

    public int a() {
        return this.g.getIntrinsicWidth();
    }

    public Point a(j jVar) {
        if (jVar == null) {
            jVar = j.BOTTOM_CENTER;
        }
        this.i = jVar;
        b();
        return this.h;
    }

    public Drawable a(int i2) {
        if (this.g == null) {
            return null;
        }
        a(this.g, i2);
        return this.g;
    }

    public void a(Drawable drawable) {
        this.g = drawable;
        b();
    }

    /* access modifiers changed from: protected */
    public Point b() {
        if (this.i == null) {
            this.i = j.CUSTOM;
        }
        Point point = this.g == null ? b : new Point(a(), a());
        switch (i.f403a[this.i.ordinal()]) {
            case 1:
                if (this.h == null) {
                    this.i = j.BOTTOM_CENTER;
                    this.h = new Point(point.x / 2, point.y / 2);
                    break;
                }
                break;
            case 2:
                this.h = new Point(point.x / 2, point.y / 2);
                break;
            case 3:
                this.h = new Point(point.x / 2, point.y);
                break;
            case 4:
                this.h = new Point(point.x / 2, 0);
                break;
            case 5:
                this.h = new Point(point.x, point.y / 2);
                break;
            case 6:
                this.h = new Point(0, point.y / 2);
                break;
            case 7:
                this.h = new Point(point.x, 0);
                break;
            case 8:
                this.h = new Point(point.x, point.y);
                break;
            case 9:
                this.h = new Point(0, 0);
                break;
            case 10:
                this.h = new Point(0, point.y);
                break;
        }
        return this.h;
    }

    public Point b(int i2) {
        return this.h == null ? b() : this.h;
    }
}
