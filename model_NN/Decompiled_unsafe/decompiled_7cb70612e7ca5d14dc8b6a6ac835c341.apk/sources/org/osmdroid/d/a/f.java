package org.osmdroid.d.a;

import android.graphics.Canvas;
import android.view.KeyEvent;
import android.view.MotionEvent;
import org.osmdroid.c;
import org.osmdroid.d.b;

public abstract class f {

    /* renamed from: a  reason: collision with root package name */
    private boolean f402a = true;
    protected final c o;

    public f(c cVar) {
        this.o = cVar;
    }

    /* access modifiers changed from: protected */
    public abstract void a(Canvas canvas, b bVar);

    public void a(b bVar) {
    }

    public boolean a() {
        return this.f402a;
    }

    public boolean a(int i, KeyEvent keyEvent, b bVar) {
        return false;
    }

    public boolean a(MotionEvent motionEvent, b bVar) {
        return false;
    }

    /* access modifiers changed from: protected */
    public abstract void b(Canvas canvas, b bVar);

    public void b(boolean z) {
        this.f402a = z;
    }

    public boolean b(int i, KeyEvent keyEvent, b bVar) {
        return false;
    }

    public boolean b(MotionEvent motionEvent, b bVar) {
        return false;
    }

    public void c(Canvas canvas, b bVar) {
        if (this.f402a) {
            b(canvas, bVar);
            a(canvas, bVar);
        }
    }

    public boolean c(MotionEvent motionEvent, b bVar) {
        return false;
    }

    public boolean d(MotionEvent motionEvent, b bVar) {
        return false;
    }

    public boolean e(MotionEvent motionEvent, b bVar) {
        return false;
    }
}
