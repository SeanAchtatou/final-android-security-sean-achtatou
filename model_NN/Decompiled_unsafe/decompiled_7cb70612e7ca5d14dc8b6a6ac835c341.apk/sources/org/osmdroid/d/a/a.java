package org.osmdroid.d.a;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import java.util.List;
import org.osmdroid.c;
import org.osmdroid.d.b;
import org.osmdroid.d.h;

public class a extends f {
    private static final Paint b = new Paint();
    static final /* synthetic */ boolean f = (!a.class.desiredAssertionStatus());
    private static final Paint g = new Paint();

    /* renamed from: a  reason: collision with root package name */
    private int f399a;
    protected e c;
    protected final List d;
    protected final h e;
    private Point h;

    static {
        b.setStyle(Paint.Style.STROKE);
        b.setColor(-65536);
        g.setStyle(Paint.Style.STROKE);
        g.setColor(-16776961);
    }

    public a(Context context, List list, Drawable drawable, Point point, e eVar, c cVar) {
        this(context, list, drawable, point, null, eVar, cVar);
    }

    public a(Context context, List list, Drawable drawable, Point point, j jVar, e eVar, c cVar) {
        super(cVar);
        this.f399a = Integer.MAX_VALUE;
        this.h = null;
        if (!f && context == null) {
            throw new AssertionError();
        } else if (f || list != null) {
            this.e = h.a(drawable, point, jVar, cVar);
            this.c = eVar;
            this.d = list;
        } else {
            throw new AssertionError();
        }
    }

    private Rect a(h hVar, Rect rect, Point point) {
        Drawable a2 = hVar.a(0) == null ? this.e.a(0) : hVar.a(0);
        Point b2 = hVar.b(0) == null ? this.e.b(0) : hVar.b(0);
        int intrinsicWidth = a2.getIntrinsicWidth();
        int intrinsicHeight = a2.getIntrinsicHeight();
        int i = point.x - b2.x;
        int i2 = point.y - b2.y;
        rect.set(i, i2, intrinsicWidth + i, intrinsicHeight + i2);
        return rect;
    }

    private boolean a(MotionEvent motionEvent, b bVar, d dVar) {
        h projection = bVar.getProjection();
        Point point = new Point();
        projection.a((int) motionEvent.getX(), (int) motionEvent.getY(), point);
        this.h = point;
        Rect rect = new Rect();
        Point point2 = new Point();
        for (int i = 0; i < this.d.size(); i++) {
            h hVar = (h) this.d.get(i);
            projection.a(hVar.f, point2);
            a(hVar, rect, point2);
            if (rect.contains(point.x, point.y) && dVar.a(i)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, int i, Point point) {
        h hVar = (h) this.d.get(i);
        Drawable a2 = hVar.a(0) == null ? this.e.a(0) : hVar.a(0);
        Rect rect = new Rect();
        a(hVar, rect, point);
        a2.setBounds(rect);
        a2.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public void a(Canvas canvas, b bVar) {
    }

    /* access modifiers changed from: protected */
    public boolean a(int i, h hVar) {
        return this.c.b(i, hVar);
    }

    public boolean a(MotionEvent motionEvent, b bVar) {
        if (a(motionEvent, bVar, new b(this))) {
            return true;
        }
        return super.a(motionEvent, bVar);
    }

    public void b(Canvas canvas, b bVar) {
        h projection = bVar.getProjection();
        Point point = new Point();
        int size = this.d.size() - 1;
        if (size > this.f399a) {
            size = this.f399a;
        }
        for (int i = size; i >= 0; i--) {
            projection.a(((h) this.d.get(i)).f, point);
            a(canvas, i, point);
        }
    }

    /* access modifiers changed from: protected */
    public boolean b(int i, h hVar) {
        return this.c.a(i, hVar);
    }

    public boolean b(MotionEvent motionEvent, b bVar) {
        if (a(motionEvent, bVar, new c(this))) {
            return true;
        }
        return super.b(motionEvent, bVar);
    }
}
