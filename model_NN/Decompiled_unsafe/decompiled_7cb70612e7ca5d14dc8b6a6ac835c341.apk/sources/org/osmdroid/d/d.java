package org.osmdroid.d;

import android.view.GestureDetector;
import android.view.MotionEvent;
import org.osmdroid.d.a.f;

class d implements GestureDetector.OnDoubleTapListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f407a;

    private d(b bVar) {
        this.f407a = bVar;
    }

    public boolean onDoubleTap(MotionEvent motionEvent) {
        for (int size = this.f407a.f.size() - 1; size >= 0; size--) {
            if (((f) this.f407a.f.get(size)).e(motionEvent, this.f407a)) {
                return true;
            }
        }
        return this.f407a.a(this.f407a.getProjection().a(motionEvent.getX(), motionEvent.getY()));
    }

    public boolean onDoubleTapEvent(MotionEvent motionEvent) {
        return false;
    }

    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        return false;
    }
}
