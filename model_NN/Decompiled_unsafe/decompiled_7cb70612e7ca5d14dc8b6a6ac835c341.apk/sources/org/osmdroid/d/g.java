package org.osmdroid.d;

import android.view.animation.Animation;

class g implements Animation.AnimationListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f410a;
    /* access modifiers changed from: private */
    public int b;
    /* access modifiers changed from: private */
    public boolean c;

    private g(b bVar) {
        this.f410a = bVar;
    }

    public void onAnimationEnd(Animation animation) {
        this.c = false;
        this.f410a.b(this.b);
    }

    public void onAnimationRepeat(Animation animation) {
    }

    public void onAnimationStart(Animation animation) {
        this.c = true;
    }
}
