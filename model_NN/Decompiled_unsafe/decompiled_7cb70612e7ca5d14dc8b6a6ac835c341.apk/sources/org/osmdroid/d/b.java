package org.osmdroid.d;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.ScaleAnimation;
import android.widget.Scroller;
import java.util.LinkedList;
import java.util.List;
import org.b.a.a.d;
import org.c.c;
import org.osmdroid.b.a;
import org.osmdroid.c.g;
import org.osmdroid.c.h;
import org.osmdroid.d.a.f;
import org.osmdroid.d.a.k;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;

public class b extends View implements org.b.a.a.b {
    private static final org.c.b b = c.a(b.class);
    private static final double c = (1.0d / Math.log(1.5384615384615383d));

    /* renamed from: a  reason: collision with root package name */
    protected a f406a;
    /* access modifiers changed from: private */
    public int d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public final LinkedList f;
    private h g;
    private final k h;
    private final GestureDetector i;
    /* access modifiers changed from: private */
    public final Scroller j;
    private final ScaleAnimation k;
    private final ScaleAnimation l;
    private final g m;
    private final a n;
    /* access modifiers changed from: private */
    public final a.a.a.a o;
    /* access modifiers changed from: private */
    public boolean p;
    private org.osmdroid.c q;
    private org.b.a.a.a r;
    private float s;
    private final Matrix t;
    private final g u;
    private final Handler v;

    public b(Context context, int i2) {
        this(context, null, null, i2, null);
    }

    private b(Context context, Handler handler, AttributeSet attributeSet, int i2, g gVar) {
        super(context, attributeSet);
        this.d = 0;
        this.e = 0;
        this.f = new LinkedList();
        this.m = new g(this);
        this.p = false;
        this.s = 1.0f;
        this.t = new Matrix();
        this.q = new org.osmdroid.a(context);
        this.n = new a(this);
        this.j = new Scroller(context);
        this.e = i2;
        gVar = gVar == null ? new h(context, a(attributeSet)) : gVar;
        this.v = handler == null ? new org.osmdroid.c.d.b(this) : handler;
        this.u = gVar;
        this.u.a(this.v);
        this.h = new k(this.u, this.q);
        this.f.add(this.h);
        this.o = new a.a.a.a(this);
        this.o.a(new f(this));
        this.k = new ScaleAnimation(1.0f, 2.0f, 1.0f, 2.0f, 1, 0.5f, 1, 0.5f);
        this.l = new ScaleAnimation(1.0f, 0.5f, 1.0f, 0.5f, 1, 0.5f, 1, 0.5f);
        this.k.setDuration(500);
        this.l.setDuration(500);
        this.k.setAnimationListener(this.m);
        this.l.setAnimationListener(this.m);
        this.i = new GestureDetector(context, new e(this));
        this.i.setOnDoubleTapListener(new d(this));
    }

    public static int a(int i2) {
        int i3 = 0;
        if (i2 <= 0) {
            return 0;
        }
        while (i2 != 0) {
            i2 >>= 1;
            i3++;
        }
        return i3 - 1;
    }

    /* access modifiers changed from: private */
    public Point a(Point point, int i2, Point point2) {
        if (point2 == null) {
            point2 = new Point();
        }
        int i3 = 1 << (this.d - 1);
        point2.set(((point.x - i3) * i2) - (i2 / 2), ((point.y - i3) * i2) - (i2 / 2));
        return point2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:11:0x0037  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x009a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.osmdroid.c.c.d a(android.util.AttributeSet r7) {
        /*
            r6 = this;
            r5 = 0
            org.osmdroid.c.c.e r0 = org.osmdroid.c.c.f.k
            if (r7 == 0) goto L_0x0079
            java.lang.String r1 = "tilesource"
            java.lang.String r1 = r7.getAttributeValue(r5, r1)
            if (r1 == 0) goto L_0x0079
            org.osmdroid.c.c.d r1 = org.osmdroid.c.c.f.a(r1)     // Catch:{ IllegalArgumentException -> 0x0060 }
            org.c.b r2 = org.osmdroid.d.b.b     // Catch:{ IllegalArgumentException -> 0x0060 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x0060 }
            r3.<init>()     // Catch:{ IllegalArgumentException -> 0x0060 }
            java.lang.String r4 = "Using tile source specified in layout attributes: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ IllegalArgumentException -> 0x0060 }
            java.lang.StringBuilder r3 = r3.append(r1)     // Catch:{ IllegalArgumentException -> 0x0060 }
            java.lang.String r3 = r3.toString()     // Catch:{ IllegalArgumentException -> 0x0060 }
            r2.b(r3)     // Catch:{ IllegalArgumentException -> 0x0060 }
        L_0x0029:
            if (r7 == 0) goto L_0x0047
            boolean r0 = r1 instanceof org.osmdroid.c.c.c
            if (r0 == 0) goto L_0x0047
            java.lang.String r0 = "style"
            java.lang.String r0 = r7.getAttributeValue(r5, r0)
            if (r0 != 0) goto L_0x009a
            java.lang.String r0 = "cloudmadeStyle"
            java.lang.String r0 = r7.getAttributeValue(r5, r0)
            r2 = r0
        L_0x003e:
            if (r2 != 0) goto L_0x007b
            org.c.b r0 = org.osmdroid.d.b.b
            java.lang.String r2 = "Using default style: 1"
            r0.b(r2)
        L_0x0047:
            org.c.b r0 = org.osmdroid.d.b.b
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Using tile source: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r1)
            java.lang.String r2 = r2.toString()
            r0.b(r2)
            return r1
        L_0x0060:
            r1 = move-exception
            org.c.b r1 = org.osmdroid.d.b.b
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Invalid tile souce specified in layout attributes: "
            java.lang.StringBuilder r2 = r2.append(r3)
            java.lang.StringBuilder r2 = r2.append(r0)
            java.lang.String r2 = r2.toString()
            r1.c(r2)
        L_0x0079:
            r1 = r0
            goto L_0x0029
        L_0x007b:
            org.c.b r0 = org.osmdroid.d.b.b
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Using style specified in layout attributes: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r2)
            java.lang.String r3 = r3.toString()
            r0.b(r3)
            r0 = r1
            org.osmdroid.c.c.c r0 = (org.osmdroid.c.c.c) r0
            r0.b(r2)
            goto L_0x0047
        L_0x009a:
            r2 = r0
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: org.osmdroid.d.b.a(android.util.AttributeSet):org.osmdroid.c.c.d");
    }

    private void g() {
        this.o.b(a());
        this.o.c(b());
    }

    public int a(boolean z) {
        return (!z || !this.m.c) ? this.d : this.m.b;
    }

    public Object a(org.b.a.a.c cVar) {
        return this;
    }

    public BoundingBoxE6 a(int i2, int i3) {
        int a2 = a(this.e);
        int i4 = 1 << ((this.d + a2) - 1);
        return org.osmdroid.d.b.a.a((getScrollX() + i4) - (getWidth() / 2), (getScrollY() + i4) - (getHeight() / 2), i4 + getScrollX() + (getWidth() / 2), getScrollY() + i4 + (getHeight() / 2), a2 + this.d);
    }

    public void a(MotionEvent motionEvent) {
        int size = this.f.size() - 1;
        while (size >= 0 && !((f) this.f.get(size)).b(motionEvent, this)) {
            size--;
        }
    }

    public void a(Object obj, org.b.a.a.c cVar) {
        if (obj == null && this.s != 1.0f) {
            b(Math.round((float) (Math.log((double) this.s) * c)) + this.d);
        }
        this.s = 1.0f;
    }

    public void a(Object obj, d dVar) {
        dVar.a(0.0f, 0.0f, true, this.s, false, 0.0f, 0.0f, false, 0.0f);
    }

    public boolean a() {
        int maxZoomLevel = getMaxZoomLevel();
        if (this.d >= maxZoomLevel) {
            return false;
        }
        return !this.m.c || this.m.b < maxZoomLevel;
    }

    public boolean a(Object obj, d dVar, org.b.a.a.c cVar) {
        this.s = dVar.a();
        invalidate();
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean a(GeoPoint geoPoint) {
        setMapCenter(geoPoint);
        return c();
    }

    /* access modifiers changed from: package-private */
    public int b(int i2) {
        int max = Math.max(getMinimumZoomLevel(), Math.min(getMaximumZoomLevel(), i2));
        int i3 = this.d;
        this.d = max;
        g();
        if (max > i3) {
            scrollTo(getScrollX() << (max - i3), getScrollY() << (max - i3));
        } else if (max < i3) {
            scrollTo(getScrollX() >> (i3 - max), getScrollY() >> (i3 - max));
        }
        Point point = new Point();
        this.g = new h(this);
        for (int size = this.f.size() - 1; size >= 0; size--) {
            if ((this.f.get(size) instanceof org.osmdroid.d.a.g) && ((org.osmdroid.d.a.g) this.f.get(size)).a(getScrollX(), getScrollY(), point, this)) {
                scrollTo(point.x, point.y);
            }
        }
        if (!(max == i3 || this.f406a == null)) {
            this.f406a.a(new org.osmdroid.b.c(this, max));
        }
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public void b(int i2, int i3) {
        Point a2 = org.osmdroid.d.b.a.a(i2, i3, getPixelZoomLevel(), (Point) null);
        int worldSizePx = getWorldSizePx() / 2;
        if (getAnimation() == null || getAnimation().hasEnded()) {
            b.a("StartScroll");
            this.j.startScroll(getScrollX(), getScrollY(), (a2.x - worldSizePx) - getScrollX(), (a2.y - worldSizePx) - getScrollY(), 500);
            postInvalidate();
        }
    }

    public boolean b() {
        int minimumZoomLevel = getMinimumZoomLevel();
        if (this.d <= minimumZoomLevel) {
            return false;
        }
        return !this.m.c || this.m.b > minimumZoomLevel;
    }

    public boolean b(MotionEvent motionEvent) {
        for (int size = this.f.size() - 1; size >= 0; size--) {
            if (((f) this.f.get(size)).a(motionEvent, this)) {
                postInvalidate();
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.osmdroid.d.g.a(org.osmdroid.d.g, boolean):boolean
     arg types: [org.osmdroid.d.g, int]
     candidates:
      org.osmdroid.d.g.a(org.osmdroid.d.g, int):int
      org.osmdroid.d.g.a(org.osmdroid.d.g, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean c() {
        if (!a() || this.m.c) {
            return false;
        }
        int unused = this.m.b = this.d + 1;
        boolean unused2 = this.m.c = true;
        startAnimation(this.k);
        return true;
    }

    public void computeScroll() {
        if (this.j.computeScrollOffset()) {
            if (this.j.isFinished()) {
                b(this.d);
            } else {
                scrollTo(this.j.getCurrX(), this.j.getCurrY());
            }
            postInvalidate();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.osmdroid.d.g.a(org.osmdroid.d.g, boolean):boolean
     arg types: [org.osmdroid.d.g, int]
     candidates:
      org.osmdroid.d.g.a(org.osmdroid.d.g, int):int
      org.osmdroid.d.g.a(org.osmdroid.d.g, boolean):boolean */
    /* access modifiers changed from: package-private */
    public boolean d() {
        if (!b() || this.m.c) {
            return false;
        }
        int unused = this.m.b = this.d - 1;
        boolean unused2 = this.m.c = true;
        startAnimation(this.l);
        return true;
    }

    public boolean e() {
        return this.m.c;
    }

    public void f() {
        for (int size = this.f.size() - 1; size >= 0; size--) {
            ((f) this.f.get(size)).a(this);
        }
    }

    public BoundingBoxE6 getBoundingBox() {
        return a(getWidth(), getHeight());
    }

    public a getController() {
        return this.n;
    }

    public int getLatitudeSpan() {
        return getBoundingBox().c();
    }

    public int getLongitudeSpan() {
        return getBoundingBox().d();
    }

    public GeoPoint getMapCenter() {
        return new GeoPoint(getMapCenterLatitudeE6(), getMapCenterLongitudeE6());
    }

    public int getMapCenterLatitudeE6() {
        return (int) (org.osmdroid.d.b.a.b(getScrollY() + (getWorldSizePx() / 2), getPixelZoomLevel()) * 1000000.0d);
    }

    public int getMapCenterLongitudeE6() {
        return (int) (org.osmdroid.d.b.a.a(getScrollX() + (getWorldSizePx() / 2), getPixelZoomLevel()) * 1000000.0d);
    }

    public int getMaxZoomLevel() {
        return this.h.c();
    }

    @Deprecated
    public int getMaximumZoomLevel() {
        return this.h.c();
    }

    public int getMinimumZoomLevel() {
        return this.h.b();
    }

    public List getOverlays() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public int getPixelZoomLevel() {
        return this.d + a(this.e);
    }

    public h getProjection() {
        if (this.g == null) {
            this.g = new h(this);
        }
        return this.g;
    }

    public Scroller getScroller() {
        return this.j;
    }

    public g getTileProvider() {
        return this.u;
    }

    public Handler getTileRequestCompleteHandler() {
        return this.v;
    }

    /* access modifiers changed from: package-private */
    public int getWorldSizePx() {
        return 1 << getPixelZoomLevel();
    }

    public int getZoomLevel() {
        return a(true);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.o.a(false);
        f();
        super.onDetachedFromWindow();
    }

    public void onDraw(Canvas canvas) {
        System.currentTimeMillis();
        this.g = new h(this);
        if (this.s == 1.0f) {
            canvas.translate((float) (getWidth() / 2), (float) (getHeight() / 2));
        } else {
            canvas.getMatrix(this.t);
            this.t.postTranslate((float) (getWidth() / 2), (float) (getHeight() / 2));
            this.t.preScale(this.s, this.s, (float) getScrollX(), (float) getScrollY());
            canvas.setMatrix(this.t);
        }
        canvas.drawColor(-3355444);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.f.size()) {
                ((f) this.f.get(i3)).c(canvas, this);
                i2 = i3 + 1;
            } else {
                System.currentTimeMillis();
                return;
            }
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        for (int size = this.f.size() - 1; size >= 0; size--) {
            if (((f) this.f.get(size)).a(i2, keyEvent, this)) {
                return true;
            }
        }
        return super.onKeyDown(i2, keyEvent);
    }

    public boolean onKeyUp(int i2, KeyEvent keyEvent) {
        for (int size = this.f.size() - 1; size >= 0; size--) {
            if (((f) this.f.get(size)).b(i2, keyEvent, this)) {
                return true;
            }
        }
        return super.onKeyUp(i2, keyEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        for (int size = this.f.size() - 1; size >= 0; size--) {
            if (((f) this.f.get(size)).c(motionEvent, this)) {
                return true;
            }
        }
        if (this.r != null && this.r.a(motionEvent)) {
            return true;
        }
        if (this.i.onTouchEvent(motionEvent)) {
            return true;
        }
        boolean onTouchEvent = super.onTouchEvent(motionEvent);
        if (onTouchEvent) {
        }
        return onTouchEvent;
    }

    public boolean onTrackballEvent(MotionEvent motionEvent) {
        for (int size = this.f.size() - 1; size >= 0; size--) {
            if (((f) this.f.get(size)).d(motionEvent, this)) {
                return true;
            }
        }
        scrollBy((int) (motionEvent.getX() * 25.0f), (int) (motionEvent.getY() * 25.0f));
        return super.onTrackballEvent(motionEvent);
    }

    public void scrollTo(int i2, int i3) {
        int worldSizePx = getWorldSizePx();
        int i4 = i2 % worldSizePx;
        int i5 = i3 % worldSizePx;
        super.scrollTo(i4, i5);
        if (this.f406a != null) {
            this.f406a.a(new org.osmdroid.b.b(this, i4, i5));
        }
    }

    public void setBuiltInZoomControls(boolean z) {
        this.p = z;
        g();
    }

    /* access modifiers changed from: package-private */
    public void setMapCenter(GeoPoint geoPoint) {
        b(geoPoint.a(), geoPoint.b());
    }

    public void setMapListener(a aVar) {
        this.f406a = aVar;
    }

    public void setMultiTouchControls(boolean z) {
        this.r = z ? new org.b.a.a.a(this, false) : null;
    }

    public void setResourceProxy(org.osmdroid.c cVar) {
        this.q = cVar;
    }

    public void setTileSource(org.osmdroid.c.c.d dVar) {
        this.u.a(dVar);
        this.e = dVar.f();
        g();
        b(this.d);
        postInvalidate();
    }

    public void setUseDataConnection(boolean z) {
        this.h.a(z);
    }
}
