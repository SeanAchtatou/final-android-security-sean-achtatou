package org.osmdroid.d;

import android.graphics.Point;
import org.osmdroid.a.c;
import org.osmdroid.d.b.a;
import org.osmdroid.util.BoundingBoxE6;
import org.osmdroid.util.GeoPoint;

public class h implements c {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ b f411a;
    private final int b;
    private final int c;
    private final int d;
    private final int e;
    private final int f;
    private final BoundingBoxE6 g;
    private final int h;
    private final int i;
    private final int j;
    private final Point k;
    private final Point l;

    private h(b bVar) {
        this.f411a = bVar;
        this.b = this.f411a.getWidth() / 2;
        this.c = this.f411a.getHeight() / 2;
        this.d = this.f411a.getWorldSizePx() / 2;
        this.e = -this.d;
        this.f = -this.d;
        this.h = bVar.d;
        this.i = bVar.e;
        this.j = b.a(a());
        this.k = b(a(), c());
        this.l = bVar.a(d(), a(), null);
        this.g = bVar.getBoundingBox();
    }

    private Point b(int i2, int i3) {
        int a2 = b.a(i2);
        int i4 = 1 << (i3 - 1);
        return new Point((this.f411a.getScrollX() >> a2) + i4, (this.f411a.getScrollY() >> a2) + i4);
    }

    public float a(float f2) {
        return (f2 / 4.0075016E7f) * ((float) this.f411a.getWorldSizePx());
    }

    public int a() {
        return this.i;
    }

    public Point a(int i2, int i3, Point point) {
        if (point == null) {
            point = new Point();
        }
        point.set(i2 - this.b, i3 - this.c);
        point.offset(this.f411a.getScrollX(), this.f411a.getScrollY());
        return point;
    }

    public Point a(GeoPoint geoPoint, Point point) {
        if (point == null) {
            point = new Point();
        }
        Point a2 = a.a(geoPoint.a(), geoPoint.b(), this.f411a.getPixelZoomLevel(), (Point) null);
        point.set(a2.x, a2.y);
        point.offset(this.e, this.f);
        return point;
    }

    public GeoPoint a(float f2, float f3) {
        return e().a(f2 / ((float) this.f411a.getWidth()), f3 / ((float) this.f411a.getHeight()));
    }

    public GeoPoint a(int i2, int i3) {
        return a((float) i2, (float) i3);
    }

    public int b() {
        return this.j;
    }

    public Point b(GeoPoint geoPoint, Point point) {
        return a(geoPoint, point);
    }

    public int c() {
        return this.h;
    }

    public Point d() {
        return this.k;
    }

    public BoundingBoxE6 e() {
        return this.g;
    }
}
