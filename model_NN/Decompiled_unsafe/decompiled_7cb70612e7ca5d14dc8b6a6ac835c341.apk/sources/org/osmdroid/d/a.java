package org.osmdroid.d;

import android.graphics.Point;
import org.osmdroid.a.b;
import org.osmdroid.util.BoundingBoxE6;

public class a implements b {

    /* renamed from: a  reason: collision with root package name */
    private final b f398a;

    public a(b bVar) {
        this.f398a = bVar;
    }

    public void a(int i, int i2) {
        if (i > 0 && i2 > 0) {
            BoundingBoxE6 boundingBox = this.f398a.getBoundingBox();
            int zoomLevel = this.f398a.getZoomLevel();
            float max = Math.max(((float) i) / ((float) boundingBox.c()), ((float) i2) / ((float) boundingBox.d()));
            if (max > 1.0f) {
                this.f398a.b(zoomLevel - org.osmdroid.d.b.b.a(max));
            } else if (((double) max) < 0.5d) {
                this.f398a.b((org.osmdroid.d.b.b.a(1.0f / max) + zoomLevel) - 1);
            }
        }
    }

    public void a(org.osmdroid.a.a aVar) {
        Point a2 = org.osmdroid.d.b.a.a(aVar, this.f398a.getPixelZoomLevel(), null);
        int worldSizePx = this.f398a.getWorldSizePx() / 2;
        this.f398a.scrollTo(a2.x - worldSizePx, a2.y - worldSizePx);
    }

    public void a(BoundingBoxE6 boundingBoxE6) {
        a(boundingBoxE6.c(), boundingBoxE6.d());
    }

    public boolean a() {
        return this.f398a.c();
    }

    public boolean b() {
        return this.f398a.d();
    }
}
