package org.osmdroid.d.b;

import android.graphics.Point;
import org.osmdroid.util.BoundingBoxE6;

public class a {
    public static double a(int i, int i2) {
        return ((((double) i) / ((double) (1 << i2))) * 360.0d) - 180.0d;
    }

    public static Point a(double d, double d2, int i, Point point) {
        if (point == null) {
            point = new Point(0, 0);
        }
        point.x = (int) Math.floor(((180.0d + d2) / 360.0d) * ((double) (1 << i)));
        point.y = (int) Math.floor(((1.0d - (Math.log(Math.tan(d * 0.017453292519943295d) + (1.0d / Math.cos(d * 0.017453292519943295d))) / 3.141592653589793d)) / 2.0d) * ((double) (1 << i)));
        return point;
    }

    public static Point a(int i, int i2, int i3, Point point) {
        return a(((double) i) * 1.0E-6d, ((double) i2) * 1.0E-6d, i3, point);
    }

    public static Point a(org.osmdroid.a.a aVar, int i, Point point) {
        return a(((double) aVar.a()) * 1.0E-6d, ((double) aVar.b()) * 1.0E-6d, i, point);
    }

    public static BoundingBoxE6 a(int i, int i2, int i3, int i4, int i5) {
        return new BoundingBoxE6(b(i2, i5), a(i3, i5), b(i4, i5), a(i, i5));
    }

    public static double b(int i, int i2) {
        double d = 3.141592653589793d - ((6.283185307179586d * ((double) i)) / ((double) (1 << i2)));
        return Math.atan((Math.exp(d) - Math.exp(-d)) * 0.5d) * 57.29577951308232d;
    }
}
