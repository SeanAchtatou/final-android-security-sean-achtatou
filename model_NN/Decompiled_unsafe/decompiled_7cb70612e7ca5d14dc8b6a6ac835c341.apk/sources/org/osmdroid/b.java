package org.osmdroid;

/* synthetic */ class b {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ int[] f363a = new int[e.values().length];

    static {
        try {
            f363a[e.osmarender.ordinal()] = 1;
        } catch (NoSuchFieldError e) {
        }
        try {
            f363a[e.mapnik.ordinal()] = 2;
        } catch (NoSuchFieldError e2) {
        }
        try {
            f363a[e.cyclemap.ordinal()] = 3;
        } catch (NoSuchFieldError e3) {
        }
        try {
            f363a[e.public_transport.ordinal()] = 4;
        } catch (NoSuchFieldError e4) {
        }
        try {
            f363a[e.base.ordinal()] = 5;
        } catch (NoSuchFieldError e5) {
        }
        try {
            f363a[e.topo.ordinal()] = 6;
        } catch (NoSuchFieldError e6) {
        }
        try {
            f363a[e.hills.ordinal()] = 7;
        } catch (NoSuchFieldError e7) {
        }
        try {
            f363a[e.cloudmade_standard.ordinal()] = 8;
        } catch (NoSuchFieldError e8) {
        }
        try {
            f363a[e.cloudmade_small.ordinal()] = 9;
        } catch (NoSuchFieldError e9) {
        }
        try {
            f363a[e.mapquest_osm.ordinal()] = 10;
        } catch (NoSuchFieldError e10) {
        }
        try {
            f363a[e.fiets_nl.ordinal()] = 11;
        } catch (NoSuchFieldError e11) {
        }
        try {
            f363a[e.base_nl.ordinal()] = 12;
        } catch (NoSuchFieldError e12) {
        }
        try {
            f363a[e.roads_nl.ordinal()] = 13;
        } catch (NoSuchFieldError e13) {
        }
        try {
            f363a[e.unknown.ordinal()] = 14;
        } catch (NoSuchFieldError e14) {
        }
        try {
            f363a[e.format_distance_meters.ordinal()] = 15;
        } catch (NoSuchFieldError e15) {
        }
        try {
            f363a[e.format_distance_kilometers.ordinal()] = 16;
        } catch (NoSuchFieldError e16) {
        }
        try {
            f363a[e.format_distance_miles.ordinal()] = 17;
        } catch (NoSuchFieldError e17) {
        }
        try {
            f363a[e.format_distance_nautical_miles.ordinal()] = 18;
        } catch (NoSuchFieldError e18) {
        }
        try {
            f363a[e.format_distance_feet.ordinal()] = 19;
        } catch (NoSuchFieldError e19) {
        }
    }
}
