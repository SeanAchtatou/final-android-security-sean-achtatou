package org.osmdroid.util;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import org.osmdroid.a.a;

public class GeoPoint implements Parcelable, Serializable, Cloneable, a {
    public static final Parcelable.Creator CREATOR = new e();

    /* renamed from: a  reason: collision with root package name */
    private int f414a;
    private int b;
    private int c;

    public GeoPoint(double d, double d2) {
        this.b = (int) (d * 1000000.0d);
        this.f414a = (int) (d2 * 1000000.0d);
    }

    public GeoPoint(int i, int i2) {
        this.b = i;
        this.f414a = i2;
    }

    private GeoPoint(Parcel parcel) {
        this.b = parcel.readInt();
        this.f414a = parcel.readInt();
        this.c = parcel.readInt();
    }

    /* synthetic */ GeoPoint(Parcel parcel, e eVar) {
        this(parcel);
    }

    public int a() {
        return this.b;
    }

    public int a(GeoPoint geoPoint) {
        double d = ((double) (0.017453292f * ((float) this.b))) / 1000000.0d;
        double d2 = ((double) (0.017453292f * ((float) this.f414a))) / 1000000.0d;
        double d3 = ((double) (0.017453292f * ((float) geoPoint.b))) / 1000000.0d;
        double d4 = ((double) (0.017453292f * ((float) geoPoint.f414a))) / 1000000.0d;
        double cos = Math.cos(d);
        double cos2 = Math.cos(d3);
        return (int) (Math.acos((Math.sin(d) * Math.sin(d3)) + (Math.sin(d2) * cos * cos2 * Math.sin(d4)) + (Math.cos(d2) * cos * cos2 * Math.cos(d4))) * 6378137.0d);
    }

    public int b() {
        return this.f414a;
    }

    public Object clone() {
        return new GeoPoint(this.b, this.f414a);
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        GeoPoint geoPoint = (GeoPoint) obj;
        if (!(geoPoint.b == this.b && geoPoint.f414a == this.f414a && geoPoint.c == this.c)) {
            z = false;
        }
        return z;
    }

    public String toString() {
        return this.b + "," + this.f414a + "," + this.c;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.b);
        parcel.writeInt(this.f414a);
        parcel.writeInt(this.c);
    }
}
