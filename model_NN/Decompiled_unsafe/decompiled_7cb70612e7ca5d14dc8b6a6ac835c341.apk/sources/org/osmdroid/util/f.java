package org.osmdroid.util;

public class f {
    public static int a(int i, int i2) {
        if (i > 0) {
            return i % i2;
        }
        int i3 = i;
        while (i3 < 0) {
            i3 += i2;
        }
        return i3;
    }
}
