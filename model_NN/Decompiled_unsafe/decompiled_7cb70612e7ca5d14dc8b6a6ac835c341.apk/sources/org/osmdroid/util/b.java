package org.osmdroid.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private String f415a;
    private List b;
    private List c;
    private List d;
    private HashMap e;
    private boolean f;
    private int g;

    public b(File file) {
        this(file.getAbsolutePath());
    }

    public b(String str) {
        this.b = new ArrayList();
        this.c = new ArrayList();
        this.d = new ArrayList();
        this.e = new HashMap();
        this.f = false;
        this.g = 0;
        this.f415a = str;
        b();
        c();
    }

    private void b() {
        this.b.add(new RandomAccessFile(new File(this.f415a), "r"));
        int i = 0;
        while (true) {
            i++;
            File file = new File(this.f415a + "-" + i);
            if (file.exists()) {
                this.b.add(new RandomAccessFile(file, "r"));
            } else {
                return;
            }
        }
    }

    private void c() {
        RandomAccessFile randomAccessFile = (RandomAccessFile) this.b.get(0);
        for (RandomAccessFile length : this.b) {
            this.d.add(Long.valueOf(length.length()));
        }
        int readInt = randomAccessFile.readInt();
        if (readInt != 4) {
            throw new IOException("Bad file version: " + readInt);
        }
        int readInt2 = randomAccessFile.readInt();
        if (readInt2 != 256) {
            throw new IOException("Bad tile size: " + readInt2);
        }
        int readInt3 = randomAccessFile.readInt();
        for (int i = 0; i < readInt3; i++) {
            int readInt4 = randomAccessFile.readInt();
            int readInt5 = randomAccessFile.readInt();
            byte[] bArr = new byte[readInt5];
            randomAccessFile.read(bArr, 0, readInt5);
            this.e.put(new Integer(readInt4), new String(bArr));
        }
        int readInt6 = randomAccessFile.readInt();
        for (int i2 = 0; i2 < readInt6; i2++) {
            d dVar = new d(this);
            dVar.f416a = Integer.valueOf(randomAccessFile.readInt());
            dVar.b = Integer.valueOf(randomAccessFile.readInt());
            dVar.c = Integer.valueOf(randomAccessFile.readInt());
            dVar.d = Integer.valueOf(randomAccessFile.readInt());
            dVar.e = Integer.valueOf(randomAccessFile.readInt());
            dVar.f = Integer.valueOf(randomAccessFile.readInt());
            dVar.g = Long.valueOf(randomAccessFile.readLong());
            this.c.add(dVar);
        }
    }

    public InputStream a(int i, int i2, int i3) {
        d dVar;
        RandomAccessFile randomAccessFile;
        Iterator it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                dVar = null;
                break;
            }
            dVar = (d) it.next();
            if (i3 == dVar.f416a.intValue() && i >= dVar.b.intValue() && i <= dVar.c.intValue() && i2 >= dVar.d.intValue() && i2 <= dVar.e.intValue()) {
                if (!this.f || dVar.f.intValue() == this.g) {
                    break;
                }
            }
        }
        if (dVar == null) {
            return null;
        }
        try {
            long intValue = (((long) ((((dVar.e.intValue() + 1) - dVar.d.intValue()) * (i - dVar.b.intValue())) + (i2 - dVar.d.intValue()))) * 12) + dVar.g.longValue();
            RandomAccessFile randomAccessFile2 = (RandomAccessFile) this.b.get(0);
            randomAccessFile2.seek(intValue);
            long readLong = randomAccessFile2.readLong();
            int readInt = randomAccessFile2.readInt();
            RandomAccessFile randomAccessFile3 = (RandomAccessFile) this.b.get(0);
            if (readLong > ((Long) this.d.get(0)).longValue()) {
                int size = this.d.size();
                int i4 = 0;
                while (i4 < size - 1 && readLong > ((Long) this.d.get(i4)).longValue()) {
                    readLong -= ((Long) this.d.get(i4)).longValue();
                    i4++;
                }
                randomAccessFile = (RandomAccessFile) this.b.get(i4);
            } else {
                randomAccessFile = randomAccessFile3;
            }
            byte[] bArr = new byte[readInt];
            randomAccessFile.seek(readLong);
            for (int read = randomAccessFile.read(bArr, 0, readInt); read < readInt; read += randomAccessFile.read(bArr, read, readInt)) {
            }
            return new ByteArrayInputStream(bArr, 0, readInt);
        } catch (IOException e2) {
            return null;
        }
    }

    public String a() {
        return this.f415a;
    }
}
