package org.osmdroid.util;

class d {

    /* renamed from: a  reason: collision with root package name */
    Integer f416a;
    Integer b;
    Integer c;
    Integer d;
    Integer e;
    Integer f;
    Long g;
    final /* synthetic */ b h;

    private d(b bVar) {
        this.h = bVar;
    }

    public String toString() {
        return String.format("GEMF Range: source=%d, zoom=%d, x=%d-%d, y=%d-%d, offset=0x%08X", this.f, this.f416a, this.b, this.c, this.d, this.e, this.g);
    }
}
