package org.osmdroid.util;

import android.os.Parcel;
import android.os.Parcelable;

final class a implements Parcelable.Creator {
    a() {
    }

    /* renamed from: a */
    public BoundingBoxE6 createFromParcel(Parcel parcel) {
        return BoundingBoxE6.b(parcel);
    }

    /* renamed from: a */
    public BoundingBoxE6[] newArray(int i) {
        return new BoundingBoxE6[i];
    }
}
