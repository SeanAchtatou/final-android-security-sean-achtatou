package org.osmdroid.util;

import android.os.Parcel;
import android.os.Parcelable;

final class e implements Parcelable.Creator {
    e() {
    }

    /* renamed from: a */
    public GeoPoint createFromParcel(Parcel parcel) {
        return new GeoPoint(parcel, (e) null);
    }

    /* renamed from: a */
    public GeoPoint[] newArray(int i) {
        return new GeoPoint[i];
    }
}
