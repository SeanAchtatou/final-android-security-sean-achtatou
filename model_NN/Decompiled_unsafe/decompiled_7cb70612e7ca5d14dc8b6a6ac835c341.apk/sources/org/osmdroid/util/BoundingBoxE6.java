package org.osmdroid.util;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class BoundingBoxE6 implements Parcelable, Serializable {
    public static final Parcelable.Creator CREATOR = new a();

    /* renamed from: a  reason: collision with root package name */
    protected final int f413a;
    protected final int b;
    protected final int c;
    protected final int d;

    public BoundingBoxE6(double d2, double d3, double d4, double d5) {
        this.f413a = (int) (d2 * 1000000.0d);
        this.c = (int) (d3 * 1000000.0d);
        this.b = (int) (d4 * 1000000.0d);
        this.d = (int) (d5 * 1000000.0d);
    }

    public BoundingBoxE6(int i, int i2, int i3, int i4) {
        this.f413a = i;
        this.c = i2;
        this.b = i3;
        this.d = i4;
    }

    public static BoundingBoxE6 a(ArrayList arrayList) {
        int i = Integer.MIN_VALUE;
        Iterator it = arrayList.iterator();
        int i2 = Integer.MIN_VALUE;
        int i3 = Integer.MAX_VALUE;
        int i4 = Integer.MAX_VALUE;
        while (true) {
            int i5 = i;
            if (!it.hasNext()) {
                return new BoundingBoxE6(i4, i3, i2, i5);
            }
            GeoPoint geoPoint = (GeoPoint) it.next();
            int a2 = geoPoint.a();
            int b2 = geoPoint.b();
            i4 = Math.min(i4, a2);
            i3 = Math.min(i3, b2);
            i2 = Math.max(i2, a2);
            i = Math.max(i5, b2);
        }
    }

    /* access modifiers changed from: private */
    public static BoundingBoxE6 b(Parcel parcel) {
        return new BoundingBoxE6(parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readInt());
    }

    public GeoPoint a() {
        return new GeoPoint((this.f413a + this.b) / 2, (this.c + this.d) / 2);
    }

    public GeoPoint a(float f, float f2) {
        int c2 = (int) (((float) this.f413a) - (((float) c()) * f2));
        int d2 = (int) (((float) this.d) + (((float) d()) * f));
        while (c2 > 90500000) {
            c2 -= 90500000;
        }
        while (c2 < -90500000) {
            c2 += 90500000;
        }
        while (d2 > 180000000) {
            d2 -= 180000000;
        }
        while (d2 < -180000000) {
            d2 += 180000000;
        }
        return new GeoPoint(c2, d2);
    }

    public int b() {
        return new GeoPoint(this.f413a, this.d).a(new GeoPoint(this.b, this.c));
    }

    public int c() {
        return Math.abs(this.f413a - this.b);
    }

    public int d() {
        return Math.abs(this.c - this.d);
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        return new StringBuffer().append("N:").append(this.f413a).append("; E:").append(this.c).append("; S:").append(this.b).append("; W:").append(this.d).toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f413a);
        parcel.writeInt(this.c);
        parcel.writeInt(this.b);
        parcel.writeInt(this.d);
    }
}
