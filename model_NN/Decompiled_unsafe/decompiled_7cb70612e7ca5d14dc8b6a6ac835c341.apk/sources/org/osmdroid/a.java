package org.osmdroid;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import org.c.b;
import org.c.c;

public class a implements c {

    /* renamed from: a  reason: collision with root package name */
    private static final b f362a = c.a(a.class);
    private DisplayMetrics b;

    public a(Context context) {
        if (context != null) {
            this.b = context.getResources().getDisplayMetrics();
        }
    }

    private BitmapFactory.Options a() {
        try {
            Field declaredField = DisplayMetrics.class.getDeclaredField("DENSITY_DEFAULT");
            Field declaredField2 = BitmapFactory.Options.class.getDeclaredField("inDensity");
            Field declaredField3 = BitmapFactory.Options.class.getDeclaredField("inTargetDensity");
            Field declaredField4 = DisplayMetrics.class.getDeclaredField("densityDpi");
            BitmapFactory.Options options = new BitmapFactory.Options();
            declaredField2.setInt(options, declaredField.getInt(null));
            declaredField3.setInt(options, declaredField4.getInt(this.b));
            return options;
        } catch (IllegalAccessException | NoSuchFieldException e) {
            return null;
        }
    }

    public Bitmap a(d dVar) {
        InputStream inputStream;
        BitmapFactory.Options options = null;
        try {
            String str = dVar.name() + ".png";
            inputStream = getClass().getResourceAsStream(str);
            if (inputStream == null) {
                try {
                    throw new IllegalArgumentException("Resource not found: " + str);
                } catch (Throwable th) {
                    th = th;
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e) {
                        }
                    }
                    throw th;
                }
            } else {
                if (this.b != null) {
                    options = a();
                }
                Bitmap decodeStream = BitmapFactory.decodeStream(inputStream, null, options);
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e2) {
                    }
                }
                return decodeStream;
            }
        } catch (Throwable th2) {
            th = th2;
            inputStream = null;
        }
    }

    public String a(e eVar) {
        switch (b.f363a[eVar.ordinal()]) {
            case 1:
                return "Osmarender";
            case 2:
                return "Mapnik";
            case 3:
                return "Cycle Map";
            case 4:
                return "Public transport";
            case 5:
                return "OSM base layer";
            case 6:
                return "Topographic";
            case 7:
                return "Hills";
            case 8:
                return "CloudMade (Standard tiles)";
            case 9:
                return "CloudMade (small tiles)";
            case 10:
                return "Mapquest";
            case 11:
                return "OpenFietsKaart overlay";
            case 12:
                return "Netherlands base overlay";
            case 13:
                return "Netherlands roads overlay";
            case 14:
                return "Unknown";
            case 15:
                return "%s m";
            case 16:
                return "%s km";
            case 17:
                return "%s mi";
            case 18:
                return "%s nm";
            case 19:
                return "%s ft";
            default:
                throw new IllegalArgumentException();
        }
    }

    public String a(e eVar, Object... objArr) {
        return String.format(a(eVar), objArr);
    }

    public Drawable b(d dVar) {
        return new BitmapDrawable(a(dVar));
    }
}
