package org.osmdroid;

public enum e {
    osmarender,
    mapnik,
    cyclemap,
    public_transport,
    base,
    topo,
    hills,
    cloudmade_small,
    cloudmade_standard,
    mapquest_osm,
    fiets_nl,
    base_nl,
    roads_nl,
    unknown,
    format_distance_meters,
    format_distance_kilometers,
    format_distance_miles,
    format_distance_nautical_miles,
    format_distance_feet
}
