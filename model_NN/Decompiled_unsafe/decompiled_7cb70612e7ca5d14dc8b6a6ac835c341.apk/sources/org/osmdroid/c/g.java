package org.osmdroid.c;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import org.c.b;
import org.c.c;
import org.osmdroid.c.a.a;
import org.osmdroid.c.c.d;

public abstract class g implements a, a {

    /* renamed from: a  reason: collision with root package name */
    private static final b f395a = c.a(g.class);
    protected final e b;
    protected Handler c;
    protected boolean d;
    private d g;

    public g() {
        this(null);
    }

    public g(Handler handler) {
        this.d = true;
        this.b = new e();
        this.c = handler;
    }

    public abstract Drawable a(d dVar);

    public abstract void a();

    public void a(int i) {
        this.b.a(i);
    }

    public void a(Handler handler) {
        this.c = handler;
    }

    public void a(d dVar) {
        this.g = dVar;
    }

    public void a(i iVar) {
        iVar.a();
        if (this.c != null) {
            this.c.sendEmptyMessage(1);
        }
    }

    public void a(i iVar, Drawable drawable) {
        d a2 = iVar.a();
        if (drawable != null) {
            this.b.a(a2, drawable);
        }
        if (this.c != null) {
            this.c.sendEmptyMessage(0);
        }
    }

    public void a(boolean z) {
        this.d = z;
    }

    public abstract int b();

    public void b(i iVar, Drawable drawable) {
        a(iVar, drawable);
    }

    public abstract int c();

    public void d() {
        this.b.a();
    }

    public boolean e() {
        return this.d;
    }
}
