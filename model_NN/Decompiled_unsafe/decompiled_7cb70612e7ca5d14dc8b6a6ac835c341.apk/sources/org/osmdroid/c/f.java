package org.osmdroid.c;

import android.graphics.drawable.Drawable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import org.c.b;
import org.c.c;
import org.osmdroid.c.b.s;
import org.osmdroid.c.c.d;

public class f extends g {
    private static final b h = c.a(f.class);

    /* renamed from: a  reason: collision with root package name */
    protected final List f394a;
    private final ConcurrentHashMap g;

    protected f(b bVar) {
        this(bVar, new s[0]);
    }

    public f(b bVar, s[] sVarArr) {
        this.g = new ConcurrentHashMap();
        this.f394a = new ArrayList();
        Collections.addAll(this.f394a, sVarArr);
    }

    private s b(i iVar) {
        s c;
        do {
            c = iVar.c();
            if (c == null || a(c) || e()) {
                return c;
            }
        } while (c.a());
        return c;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x004f, code lost:
        r0 = b(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0053, code lost:
        if (r0 == null) goto L_0x005d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0055, code lost:
        r0.a(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x005d, code lost:
        a(r3);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.drawable.Drawable a(org.osmdroid.c.d r6) {
        /*
            r5 = this;
            r1 = 0
            org.osmdroid.c.e r0 = r5.b
            boolean r0 = r0.b(r6)
            if (r0 == 0) goto L_0x0010
            org.osmdroid.c.e r0 = r5.b
            android.graphics.drawable.Drawable r0 = r0.a(r6)
        L_0x000f:
            return r0
        L_0x0010:
            java.util.concurrent.ConcurrentHashMap r2 = r5.g
            monitor-enter(r2)
            java.util.concurrent.ConcurrentHashMap r0 = r5.g     // Catch:{ all -> 0x0043 }
            boolean r0 = r0.containsValue(r6)     // Catch:{ all -> 0x0043 }
            monitor-exit(r2)     // Catch:{ all -> 0x0043 }
            if (r0 != 0) goto L_0x0058
            java.util.List r2 = r5.f394a
            monitor-enter(r2)
            java.util.List r0 = r5.f394a     // Catch:{ all -> 0x0046 }
            int r0 = r0.size()     // Catch:{ all -> 0x0046 }
            org.osmdroid.c.b.s[] r0 = new org.osmdroid.c.b.s[r0]     // Catch:{ all -> 0x0046 }
            org.osmdroid.c.i r3 = new org.osmdroid.c.i     // Catch:{ all -> 0x0046 }
            java.util.List r4 = r5.f394a     // Catch:{ all -> 0x0046 }
            java.lang.Object[] r0 = r4.toArray(r0)     // Catch:{ all -> 0x0046 }
            org.osmdroid.c.b.s[] r0 = (org.osmdroid.c.b.s[]) r0     // Catch:{ all -> 0x0046 }
            r3.<init>(r6, r0, r5)     // Catch:{ all -> 0x0046 }
            monitor-exit(r2)     // Catch:{ all -> 0x0046 }
            java.util.concurrent.ConcurrentHashMap r2 = r5.g
            monitor-enter(r2)
            java.util.concurrent.ConcurrentHashMap r0 = r5.g     // Catch:{ all -> 0x005a }
            boolean r0 = r0.containsValue(r6)     // Catch:{ all -> 0x005a }
            if (r0 == 0) goto L_0x0049
            monitor-exit(r2)     // Catch:{ all -> 0x005a }
            r0 = r1
            goto L_0x000f
        L_0x0043:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0043 }
            throw r0
        L_0x0046:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0046 }
            throw r0
        L_0x0049:
            java.util.concurrent.ConcurrentHashMap r0 = r5.g     // Catch:{ all -> 0x005a }
            r0.put(r3, r6)     // Catch:{ all -> 0x005a }
            monitor-exit(r2)     // Catch:{ all -> 0x005a }
            org.osmdroid.c.b.s r0 = r5.b(r3)
            if (r0 == 0) goto L_0x005d
            r0.a(r3)
        L_0x0058:
            r0 = r1
            goto L_0x000f
        L_0x005a:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x005a }
            throw r0
        L_0x005d:
            r5.a(r3)
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: org.osmdroid.c.f.a(org.osmdroid.c.d):android.graphics.drawable.Drawable");
    }

    public void a() {
        synchronized (this.f394a) {
            for (s j : this.f394a) {
                j.j();
            }
        }
    }

    public void a(d dVar) {
        super.a(dVar);
        for (s a2 : this.f394a) {
            a2.a(dVar);
            d();
        }
    }

    public void a(i iVar) {
        s b = b(iVar);
        if (b != null) {
            b.a(iVar);
            return;
        }
        synchronized (this.g) {
            this.g.remove(iVar);
        }
        super.a(iVar);
    }

    public void a(i iVar, Drawable drawable) {
        synchronized (this.g) {
            this.g.remove(iVar);
        }
        super.a(iVar, drawable);
    }

    public boolean a(s sVar) {
        boolean contains;
        synchronized (this.f394a) {
            contains = this.f394a.contains(sVar);
        }
        return contains;
    }

    public int b() {
        int i = 23;
        synchronized (this.f394a) {
            for (s sVar : this.f394a) {
                i = sVar.d() < i ? sVar.d() : i;
            }
        }
        return i;
    }

    public int c() {
        int i = 0;
        synchronized (this.f394a) {
            for (s sVar : this.f394a) {
                i = sVar.e() > i ? sVar.e() : i;
            }
        }
        return i;
    }
}
