package org.osmdroid.c;

import android.graphics.drawable.Drawable;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.osmdroid.c.a.a;

public final class e implements a {

    /* renamed from: a  reason: collision with root package name */
    protected c f393a;
    private final ReadWriteLock b;

    public e() {
        this(9);
    }

    public e(int i) {
        this.b = new ReentrantReadWriteLock();
        this.f393a = new c(i);
    }

    public Drawable a(d dVar) {
        this.b.readLock().lock();
        Drawable drawable = (Drawable) this.f393a.get(dVar);
        this.b.readLock().unlock();
        return drawable;
    }

    public void a() {
        this.b.writeLock().lock();
        this.f393a.clear();
        this.b.writeLock().unlock();
    }

    public void a(int i) {
        this.b.readLock().lock();
        this.f393a.a(i);
        this.b.readLock().unlock();
    }

    public void a(d dVar, Drawable drawable) {
        if (drawable != null) {
            this.b.writeLock().lock();
            this.f393a.put(dVar, drawable);
            this.b.writeLock().unlock();
        }
    }

    public boolean b(d dVar) {
        this.b.readLock().lock();
        boolean containsKey = this.f393a.containsKey(dVar);
        this.b.readLock().unlock();
        return containsKey;
    }
}
