package org.osmdroid.c;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;
import org.osmdroid.c.b.s;

public class i {

    /* renamed from: a  reason: collision with root package name */
    private final Queue f396a = new LinkedList();
    private final d b;
    private final a c;
    private s d;

    public i(d dVar, s[] sVarArr, a aVar) {
        Collections.addAll(this.f396a, sVarArr);
        this.b = dVar;
        this.c = aVar;
    }

    public d a() {
        return this.b;
    }

    public a b() {
        return this.c;
    }

    public s c() {
        this.d = (s) this.f396a.poll();
        return this.d;
    }
}
