package org.osmdroid.c.c;

import org.osmdroid.c.d;

public abstract class e extends a {
    private final String[] d;

    public e(String str, org.osmdroid.e eVar, int i, int i2, int i3, String str2, String... strArr) {
        super(str, eVar, i, i2, i3, str2);
        this.d = strArr;
    }

    public abstract String b(d dVar);

    /* access modifiers changed from: protected */
    public String g() {
        return this.d[this.c.nextInt(this.d.length)];
    }
}
