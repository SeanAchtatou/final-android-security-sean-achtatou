package org.osmdroid.c.c;

import org.osmdroid.c.d;
import org.osmdroid.c.d.a;
import org.osmdroid.e;

class b extends e implements c {
    private Integer d = 1;

    b(String str, e eVar, int i, int i2, int i3, String str2, String... strArr) {
        super(str, eVar, i, i2, i3, str2, strArr);
    }

    public String b() {
        return (this.d == null || this.d.intValue() <= 1) ? this.f386a : this.f386a + this.d;
    }

    public String b(d dVar) {
        String a2 = a.a();
        String b = a.b();
        return String.format(g(), a2, this.d, Integer.valueOf(f()), Integer.valueOf(dVar.a()), Integer.valueOf(dVar.b()), Integer.valueOf(dVar.c()), this.b, b);
    }

    public void b(String str) {
        this.d = Integer.getInteger(str);
    }
}
