package org.osmdroid.c.c;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import java.io.File;
import java.io.InputStream;
import java.util.Random;
import org.c.b;
import org.c.c;
import org.osmdroid.c.d;
import org.osmdroid.e;

public abstract class a implements org.osmdroid.c.a.a, d {
    private static final b d = c.a(a.class);
    private static int g = 0;

    /* renamed from: a  reason: collision with root package name */
    protected final String f386a;
    protected final String b;
    protected final Random c = new Random();
    private final int h;
    private final int i;
    private final int j;
    private final int k;
    private final e l;

    public a(String str, e eVar, int i2, int i3, int i4, String str2) {
        this.l = eVar;
        int i5 = g;
        g = i5 + 1;
        this.j = i5;
        this.f386a = str;
        this.h = i2;
        this.i = i3;
        this.k = i4;
        this.b = str2;
    }

    public Drawable a(InputStream inputStream) {
        try {
            Bitmap decodeStream = BitmapFactory.decodeStream(inputStream);
            if (decodeStream != null) {
                return new BitmapDrawable(decodeStream);
            }
        } catch (OutOfMemoryError e) {
            d.d("OutOfMemoryError loading bitmap");
            System.gc();
        }
        return null;
    }

    public Drawable a(String str) {
        try {
            Bitmap decodeFile = BitmapFactory.decodeFile(str);
            if (decodeFile != null) {
                return new BitmapDrawable(decodeFile);
            }
            try {
                new File(str).delete();
            } catch (Throwable th) {
                d.c("Error deleting invalid file: " + str, th);
            }
            return null;
        } catch (OutOfMemoryError e) {
            d.d("OutOfMemoryError loading bitmap: " + str);
            System.gc();
        }
    }

    public String a() {
        return this.f386a;
    }

    public String a(d dVar) {
        return b() + '/' + dVar.a() + '/' + dVar.b() + '/' + dVar.c() + c();
    }

    public String b() {
        return this.f386a;
    }

    public String c() {
        return this.b;
    }

    public int d() {
        return this.h;
    }

    public int e() {
        return this.i;
    }

    public int f() {
        return this.k;
    }
}
