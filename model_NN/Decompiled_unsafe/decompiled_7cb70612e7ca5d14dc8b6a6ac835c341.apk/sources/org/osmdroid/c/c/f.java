package org.osmdroid.c.c;

import java.util.ArrayList;
import java.util.Iterator;
import org.c.b;
import org.c.c;
import org.osmdroid.e;

public class f {

    /* renamed from: a  reason: collision with root package name */
    public static final e f387a = new g("Osmarender", e.osmarender, 0, 17, 256, ".png", "http://tah.openstreetmap.org/Tiles/tile/");
    public static final e b = new g("Mapnik", e.mapnik, 0, 18, 256, ".png", "http://tile.openstreetmap.org/");
    public static final e c = new g("CycleMap", e.cyclemap, 0, 17, 256, ".png", "http://a.andy.sandbox.cloudmade.com/tiles/cycle/", "http://b.andy.sandbox.cloudmade.com/tiles/cycle/", "http://c.andy.sandbox.cloudmade.com/tiles/cycle/");
    public static final e d = new g("OSMPublicTransport", e.public_transport, 0, 17, 256, ".png", "http://tile.xn--pnvkarte-m4a.de/tilegen/");
    public static final e e = new g("Base", e.base, 4, 17, 256, ".png", "http://topo.openstreetmap.de/base/");
    public static final e f = new g("Topo", e.topo, 4, 17, 256, ".png", "http://topo.openstreetmap.de/topo/");
    public static final e g = new g("Hills", e.hills, 8, 17, 256, ".png", "http://topo.geofabrik.de/hills/");
    public static final e h = new b("CloudMadeStandardTiles", e.cloudmade_standard, 0, 18, 256, ".png", "http://a.tile.cloudmade.com/%s/%d/%d/%d/%d/%d%s?token=%s", "http://b.tile.cloudmade.com/%s/%d/%d/%d/%d/%d%s?token=%s", "http://c.tile.cloudmade.com/%s/%d/%d/%d/%d/%d%s?token=%s");
    public static final e i = new b("CloudMadeSmallTiles", e.cloudmade_small, 0, 21, 64, ".png", "http://a.tile.cloudmade.com/%s/%d/%d/%d/%d/%d%s?token=%s", "http://b.tile.cloudmade.com/%s/%d/%d/%d/%d/%d%s?token=%s", "http://c.tile.cloudmade.com/%s/%d/%d/%d/%d/%d%s?token=%s");
    public static final e j = new g("MapquestOSM", e.mapquest_osm, 0, 18, 256, ".png", "http://otile1.mqcdn.com/tiles/1.0.0/osm/", "http://otile2.mqcdn.com/tiles/1.0.0/osm/", "http://otile3.mqcdn.com/tiles/1.0.0/osm/", "http://otile4.mqcdn.com/tiles/1.0.0/osm/");
    public static final e k = b;
    public static final e l = new g("Fiets", e.fiets_nl, 3, 16, 256, ".png", "http://overlay.openstreetmap.nl/openfietskaart-overlay/");
    public static final e m = new g("BaseNL", e.base_nl, 0, 18, 256, ".png", "http://overlay.openstreetmap.nl/basemap/");
    public static final e n = new g("RoadsNL", e.roads_nl, 0, 18, 256, ".png", "http://overlay.openstreetmap.nl/roads/");
    private static final b o = c.a(f.class);
    private static ArrayList p = new ArrayList();

    static {
        p.add(f387a);
        p.add(b);
        p.add(c);
        p.add(d);
        p.add(e);
        p.add(f);
        p.add(g);
        p.add(h);
        p.add(i);
        p.add(j);
    }

    public static d a(String str) {
        Iterator it = p.iterator();
        while (it.hasNext()) {
            d dVar = (d) it.next();
            if (dVar.a().equals(str)) {
                return dVar;
            }
        }
        throw new IllegalArgumentException("No such tile source: " + str);
    }
}
