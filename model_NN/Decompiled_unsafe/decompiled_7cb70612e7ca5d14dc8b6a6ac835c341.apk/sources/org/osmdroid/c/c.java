package org.osmdroid.c;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import java.util.LinkedHashMap;
import java.util.Map;
import org.c.b;

public class c extends LinkedHashMap {

    /* renamed from: a  reason: collision with root package name */
    private static final b f385a = org.c.c.a(c.class);
    private int b;

    public c(int i) {
        super(i + 2, 0.1f, true);
        this.b = i;
    }

    /* renamed from: a */
    public Drawable remove(Object obj) {
        Bitmap bitmap;
        Drawable drawable = (Drawable) super.remove(obj);
        if ((drawable instanceof BitmapDrawable) && (bitmap = ((BitmapDrawable) drawable).getBitmap()) != null) {
            bitmap.recycle();
        }
        return drawable;
    }

    public void a(int i) {
        if (i > this.b) {
            f385a.b("Tile cache increased from " + this.b + " to " + i);
            this.b = i;
        }
    }

    public void clear() {
        while (size() > 0) {
            remove(keySet().iterator().next());
        }
        super.clear();
    }

    /* access modifiers changed from: protected */
    public boolean removeEldestEntry(Map.Entry entry) {
        if (size() <= this.b) {
            return false;
        }
        remove(entry.getKey());
        return false;
    }
}
