package org.osmdroid.c.b;

import android.graphics.drawable.Drawable;
import java.io.File;
import org.osmdroid.c.a.a;
import org.osmdroid.c.d;
import org.osmdroid.c.i;

class r extends v {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ p f377a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private r(p pVar) {
        super(pVar);
        this.f377a = pVar;
    }

    public Drawable a(i iVar) {
        if (this.f377a.d == null) {
            return null;
        }
        d a2 = iVar.a();
        if (!this.f377a.i()) {
            return null;
        }
        File file = new File(a.f, this.f377a.d.a(a2) + ".tile");
        if (!file.exists()) {
            return null;
        }
        if (!(file.lastModified() < System.currentTimeMillis() - this.f377a.c)) {
            return this.f377a.d.a(file.getPath());
        }
        a(iVar, this.f377a.d.a(file.getPath()));
        return null;
    }
}
