package org.osmdroid.c.b;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class w implements f {

    /* renamed from: a  reason: collision with root package name */
    private final ConnectivityManager f381a;

    public w(Context context) {
        this.f381a = (ConnectivityManager) context.getSystemService("connectivity");
    }

    public boolean a() {
        NetworkInfo activeNetworkInfo = this.f381a.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isAvailable();
    }
}
