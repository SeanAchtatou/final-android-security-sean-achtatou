package org.osmdroid.c.b;

import java.util.LinkedHashMap;
import java.util.concurrent.ConcurrentHashMap;
import org.c.b;
import org.c.c;
import org.osmdroid.c.a.a;
import org.osmdroid.c.d;
import org.osmdroid.c.i;

public abstract class s implements a {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final b f378a = c.a(s.class);
    final LinkedHashMap b;
    private final int c;
    private final ThreadGroup d = new ThreadGroup(b());
    /* access modifiers changed from: private */
    public final ConcurrentHashMap g;

    public s(int i, int i2) {
        this.c = i;
        this.g = new ConcurrentHashMap();
        this.b = new t(this, i2 + 2, 0.1f, true, i2);
    }

    /* access modifiers changed from: private */
    public void a(d dVar) {
        synchronized (this.b) {
            this.b.remove(dVar);
        }
        this.g.remove(dVar);
    }

    /* access modifiers changed from: private */
    public void f() {
        synchronized (this.b) {
            this.b.clear();
        }
        this.g.clear();
    }

    public abstract void a(org.osmdroid.c.c.d dVar);

    public void a(i iVar) {
        int activeCount = this.d.activeCount();
        synchronized (this.b) {
            this.b.put(iVar.a(), iVar);
        }
        if (activeCount < this.c) {
            new Thread(this.d, c()).start();
        }
    }

    public abstract boolean a();

    /* access modifiers changed from: protected */
    public abstract String b();

    /* access modifiers changed from: protected */
    public abstract Runnable c();

    public abstract int d();

    public abstract int e();

    public void j() {
        f();
        this.d.interrupt();
    }
}
