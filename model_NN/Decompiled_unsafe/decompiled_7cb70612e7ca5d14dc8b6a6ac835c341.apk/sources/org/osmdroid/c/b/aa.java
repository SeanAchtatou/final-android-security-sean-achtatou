package org.osmdroid.c.b;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.c.b;
import org.c.c;
import org.osmdroid.c.c.d;

public class aa implements d {

    /* renamed from: a  reason: collision with root package name */
    private static final b f367a = c.a(aa.class);
    private final ZipFile b;

    private aa(ZipFile zipFile) {
        this.b = zipFile;
    }

    public static aa a(File file) {
        return new aa(new ZipFile(file));
    }

    public InputStream a(d dVar, org.osmdroid.c.d dVar2) {
        try {
            ZipEntry entry = this.b.getEntry(dVar.a(dVar2));
            if (entry != null) {
                return this.b.getInputStream(entry);
            }
        } catch (IOException e) {
            f367a.b("Error getting zip stream: " + dVar2, e);
        }
        return null;
    }

    public String toString() {
        return "ZipFileArchive [mZipFile=" + this.b.getName() + "]";
    }
}
