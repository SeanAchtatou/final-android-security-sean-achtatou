package org.osmdroid.c.b;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.c.b;
import org.c.c;
import org.osmdroid.c.a.a;

public class x implements a, e {

    /* renamed from: a  reason: collision with root package name */
    private static final b f382a = c.a(x.class);
    /* access modifiers changed from: private */
    public static long b;

    public x() {
        y yVar = new y(this);
        yVar.setPriority(1);
        yVar.start();
    }

    private boolean a(File file) {
        if (file.mkdirs()) {
            return true;
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
        }
        return file.exists();
    }

    /* access modifiers changed from: private */
    public void b() {
        synchronized (f) {
            if (b > 524288000) {
                f382a.b("Trimming tile cache from " + b + " to " + 524288000L);
                File[] fileArr = (File[]) c(f).toArray(new File[0]);
                Arrays.sort(fileArr, new z(this));
                for (File file : fileArr) {
                    if (b <= 524288000) {
                        break;
                    }
                    long length = file.length();
                    if (file.delete()) {
                        b -= length;
                    }
                }
                f382a.b("Finished trimming tile cache");
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(File file) {
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                if (file2.isFile()) {
                    b += file2.length();
                }
                if (file2.isDirectory()) {
                    b(file2);
                }
            }
        }
    }

    private List c(File file) {
        ArrayList arrayList = new ArrayList();
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                if (file2.isFile()) {
                    arrayList.add(file2);
                }
                if (file2.isDirectory()) {
                    arrayList.addAll(c(file2));
                }
            }
        }
        return arrayList;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(org.osmdroid.c.c.d r7, org.osmdroid.c.d r8, java.io.InputStream r9) {
        /*
            r6 = this;
            r0 = 0
            java.io.File r3 = new java.io.File
            java.io.File r1 = org.osmdroid.c.b.x.f
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r4 = r7.a(r8)
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r4 = ".tile"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.String r2 = r2.toString()
            r3.<init>(r1, r2)
            java.io.File r1 = r3.getParentFile()
            boolean r2 = r1.exists()
            if (r2 != 0) goto L_0x0030
            boolean r1 = r6.a(r1)
            if (r1 != 0) goto L_0x0030
        L_0x002f:
            return r0
        L_0x0030:
            r2 = 0
            java.io.BufferedOutputStream r1 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x005d, all -> 0x0065 }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x005d, all -> 0x0065 }
            java.lang.String r3 = r3.getPath()     // Catch:{ IOException -> 0x005d, all -> 0x0065 }
            r4.<init>(r3)     // Catch:{ IOException -> 0x005d, all -> 0x0065 }
            r3 = 8192(0x2000, float:1.14794E-41)
            r1.<init>(r4, r3)     // Catch:{ IOException -> 0x005d, all -> 0x0065 }
            long r2 = org.osmdroid.c.d.d.a(r9, r1)     // Catch:{ IOException -> 0x006f, all -> 0x006d }
            long r4 = org.osmdroid.c.b.x.b     // Catch:{ IOException -> 0x006f, all -> 0x006d }
            long r2 = r2 + r4
            org.osmdroid.c.b.x.b = r2     // Catch:{ IOException -> 0x006f, all -> 0x006d }
            long r2 = org.osmdroid.c.b.x.b     // Catch:{ IOException -> 0x006f, all -> 0x006d }
            r4 = 629145600(0x25800000, double:3.10839227E-315)
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x0056
            r6.b()     // Catch:{ IOException -> 0x006f, all -> 0x006d }
        L_0x0056:
            if (r1 == 0) goto L_0x005b
            org.osmdroid.c.d.d.a(r1)
        L_0x005b:
            r0 = 1
            goto L_0x002f
        L_0x005d:
            r1 = move-exception
            r1 = r2
        L_0x005f:
            if (r1 == 0) goto L_0x002f
            org.osmdroid.c.d.d.a(r1)
            goto L_0x002f
        L_0x0065:
            r0 = move-exception
            r1 = r2
        L_0x0067:
            if (r1 == 0) goto L_0x006c
            org.osmdroid.c.d.d.a(r1)
        L_0x006c:
            throw r0
        L_0x006d:
            r0 = move-exception
            goto L_0x0067
        L_0x006f:
            r2 = move-exception
            goto L_0x005f
        */
        throw new UnsupportedOperationException("Method not decompiled: org.osmdroid.c.b.x.a(org.osmdroid.c.c.d, org.osmdroid.c.d, java.io.InputStream):boolean");
    }
}
