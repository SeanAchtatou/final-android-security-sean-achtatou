package org.osmdroid.c.b;

class l extends v {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ j f373a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private l(j jVar) {
        super(jVar);
        this.f373a = jVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0046  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.graphics.drawable.Drawable a(org.osmdroid.c.i r6) {
        /*
            r5 = this;
            r0 = 0
            org.osmdroid.c.b.j r1 = r5.f373a
            org.osmdroid.c.c.d r1 = r1.f372a
            if (r1 != 0) goto L_0x0008
        L_0x0007:
            return r0
        L_0x0008:
            org.osmdroid.c.d r1 = r6.a()
            org.osmdroid.c.b.j r2 = r5.f373a
            boolean r2 = r2.i()
            if (r2 == 0) goto L_0x0007
            org.osmdroid.c.b.j r2 = r5.f373a     // Catch:{ Throwable -> 0x0030, all -> 0x0041 }
            java.io.InputStream r2 = r2.a(r1)     // Catch:{ Throwable -> 0x0030, all -> 0x0041 }
            if (r2 == 0) goto L_0x002a
            org.osmdroid.c.b.j r1 = r5.f373a     // Catch:{ Throwable -> 0x004c }
            org.osmdroid.c.c.d r1 = r1.f372a     // Catch:{ Throwable -> 0x004c }
            android.graphics.drawable.Drawable r0 = r1.a(r2)     // Catch:{ Throwable -> 0x004c }
            if (r2 == 0) goto L_0x0007
            org.osmdroid.c.d.d.a(r2)
            goto L_0x0007
        L_0x002a:
            if (r2 == 0) goto L_0x0007
            org.osmdroid.c.d.d.a(r2)
            goto L_0x0007
        L_0x0030:
            r1 = move-exception
            r2 = r0
        L_0x0032:
            org.c.b r3 = org.osmdroid.c.b.j.c     // Catch:{ all -> 0x004a }
            java.lang.String r4 = "Error loading tile"
            r3.c(r4, r1)     // Catch:{ all -> 0x004a }
            if (r2 == 0) goto L_0x0007
            org.osmdroid.c.d.d.a(r2)
            goto L_0x0007
        L_0x0041:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x0044:
            if (r2 == 0) goto L_0x0049
            org.osmdroid.c.d.d.a(r2)
        L_0x0049:
            throw r0
        L_0x004a:
            r0 = move-exception
            goto L_0x0044
        L_0x004c:
            r1 = move-exception
            goto L_0x0032
        */
        throw new UnsupportedOperationException("Method not decompiled: org.osmdroid.c.b.l.a(org.osmdroid.c.i):android.graphics.drawable.Drawable");
    }
}
