package org.osmdroid.c.b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

class o extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ m f375a;

    private o(m mVar) {
        this.f375a = mVar;
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        this.f375a.h();
        if ("android.intent.action.MEDIA_MOUNTED".equals(action)) {
            this.f375a.f();
        } else if ("android.intent.action.MEDIA_UNMOUNTED".equals(action)) {
            this.f375a.g();
        }
    }
}
