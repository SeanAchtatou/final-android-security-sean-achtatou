package org.osmdroid.c.b;

import java.io.File;
import java.util.Comparator;

class z implements Comparator {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ x f384a;

    z(x xVar) {
        this.f384a = xVar;
    }

    /* renamed from: a */
    public int compare(File file, File file2) {
        return Long.valueOf(file.lastModified()).compareTo(Long.valueOf(file2.lastModified()));
    }
}
