package org.osmdroid.c.b;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import org.c.b;
import org.c.c;
import org.osmdroid.c.c.d;

public class j extends m {
    /* access modifiers changed from: private */
    public static final b c = c.a(j.class);

    /* renamed from: a  reason: collision with root package name */
    protected d f372a;
    private final ArrayList d = new ArrayList();

    public j(org.osmdroid.c.b bVar, d dVar) {
        super(bVar, 8, 40);
        this.f372a = dVar;
        l();
    }

    /* access modifiers changed from: private */
    public synchronized InputStream a(org.osmdroid.c.d dVar) {
        InputStream inputStream;
        Iterator it = this.d.iterator();
        while (true) {
            if (!it.hasNext()) {
                inputStream = null;
                break;
            }
            inputStream = ((d) it.next()).a(this.f372a, dVar);
            if (inputStream != null) {
                break;
            }
        }
        return inputStream;
    }

    private void l() {
        File[] listFiles;
        this.d.clear();
        if (i() && (listFiles = e.listFiles()) != null) {
            for (File a2 : listFiles) {
                d a3 = a.a(a2);
                if (a3 != null) {
                    this.d.add(a3);
                }
            }
        }
    }

    public void a(d dVar) {
        this.f372a = dVar;
    }

    public boolean a() {
        return false;
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "filearchive";
    }

    /* access modifiers changed from: protected */
    public Runnable c() {
        return new l(this);
    }

    public int d() {
        if (this.f372a != null) {
            return this.f372a.d();
        }
        return 23;
    }

    public int e() {
        if (this.f372a != null) {
            return this.f372a.e();
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public void f() {
        l();
    }

    /* access modifiers changed from: protected */
    public void g() {
        l();
    }
}
