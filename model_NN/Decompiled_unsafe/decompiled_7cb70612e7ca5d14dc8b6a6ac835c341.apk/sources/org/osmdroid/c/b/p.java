package org.osmdroid.c.b;

import org.c.b;
import org.c.c;
import org.osmdroid.c.c.d;
import org.osmdroid.c.c.f;

public class p extends m {

    /* renamed from: a  reason: collision with root package name */
    private static final b f376a = c.a(p.class);
    /* access modifiers changed from: private */
    public final long c;
    /* access modifiers changed from: private */
    public d d;

    public p(org.osmdroid.c.b bVar) {
        this(bVar, f.k);
    }

    public p(org.osmdroid.c.b bVar, d dVar) {
        this(bVar, dVar, 604800000);
    }

    public p(org.osmdroid.c.b bVar, d dVar, long j) {
        super(bVar, 8, 40);
        this.d = dVar;
        this.c = j;
    }

    public void a(d dVar) {
        this.d = dVar;
    }

    public boolean a() {
        return false;
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "filesystem";
    }

    /* access modifiers changed from: protected */
    public Runnable c() {
        return new r(this);
    }

    public int d() {
        if (this.d != null) {
            return this.d.d();
        }
        return 23;
    }

    public int e() {
        if (this.d != null) {
            return this.d.e();
        }
        return 0;
    }
}
