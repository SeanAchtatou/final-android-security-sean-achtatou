package org.osmdroid.c.b;

import java.util.LinkedHashMap;
import java.util.Map;

class t extends LinkedHashMap {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ int f379a;
    final /* synthetic */ s b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    t(s sVar, int i, float f, boolean z, int i2) {
        super(i, f, z);
        this.b = sVar;
        this.f379a = i2;
    }

    /* access modifiers changed from: protected */
    public boolean removeEldestEntry(Map.Entry entry) {
        return size() > this.f379a;
    }
}
