package org.osmdroid.c.b;

import org.c.b;
import org.c.c;
import org.osmdroid.c.c.d;
import org.osmdroid.c.c.e;

public class g extends s {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final b f370a = c.a(g.class);
    /* access modifiers changed from: private */
    public final e c;
    /* access modifiers changed from: private */
    public e d;
    /* access modifiers changed from: private */
    public final f g;

    public g(d dVar, e eVar, f fVar) {
        super(2, 40);
        this.c = eVar;
        this.g = fVar;
        a(dVar);
    }

    public void a(d dVar) {
        if (dVar instanceof e) {
            this.d = (e) dVar;
        } else {
            this.d = null;
        }
    }

    public boolean a() {
        return true;
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "downloader";
    }

    /* access modifiers changed from: protected */
    public Runnable c() {
        return new i(this);
    }

    public int d() {
        if (this.d != null) {
            return this.d.d();
        }
        return 23;
    }

    public int e() {
        if (this.d != null) {
            return this.d.e();
        }
        return 0;
    }
}
