package org.osmdroid.c.b;

import android.graphics.drawable.Drawable;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import org.osmdroid.c.d;
import org.osmdroid.c.i;

public abstract class v implements Runnable {
    final /* synthetic */ s b;

    protected v(s sVar) {
        this.b = sVar;
    }

    private i a() {
        i iVar;
        synchronized (this.b.b) {
            Iterator it = this.b.b.keySet().iterator();
            d dVar = null;
            while (it.hasNext()) {
                try {
                    d dVar2 = (d) it.next();
                    if (this.b.g.containsKey(dVar2)) {
                        dVar2 = dVar;
                    }
                    dVar = dVar2;
                } catch (ConcurrentModificationException e) {
                    if (dVar != null) {
                        break;
                    }
                    it = this.b.b.keySet().iterator();
                }
            }
            if (dVar != null) {
                this.b.g.put(dVar, this.b.b.get(dVar));
            }
            iVar = dVar != null ? (i) this.b.b.get(dVar) : null;
        }
        return iVar;
    }

    private void b(i iVar) {
        this.b.a(iVar.a());
        iVar.b().a(iVar);
    }

    private void b(i iVar, Drawable drawable) {
        this.b.a(iVar.a());
        iVar.b().a(iVar, drawable);
    }

    /* access modifiers changed from: protected */
    public abstract Drawable a(i iVar);

    /* access modifiers changed from: protected */
    public void a(i iVar, Drawable drawable) {
        iVar.b().b(iVar, drawable);
    }

    public final void run() {
        while (true) {
            i a2 = a();
            if (a2 != null) {
                Drawable drawable = null;
                try {
                    drawable = a(a2);
                } catch (u e) {
                    s.f378a.a("Tile loader can't continue", e);
                    this.b.f();
                } catch (Throwable th) {
                    s.f378a.c("Error downloading tile: " + a2, th);
                }
                if (drawable != null) {
                    b(a2, drawable);
                } else {
                    b(a2);
                }
            } else {
                return;
            }
        }
    }
}
