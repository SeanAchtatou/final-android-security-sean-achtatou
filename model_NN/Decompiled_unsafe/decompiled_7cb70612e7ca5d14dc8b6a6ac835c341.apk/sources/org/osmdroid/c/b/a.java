package org.osmdroid.c.b;

import android.database.sqlite.SQLiteException;
import java.io.File;
import java.io.IOException;
import org.c.b;
import org.c.c;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final b f366a = c.a(a.class);

    public static d a(File file) {
        if (file.getName().endsWith(".zip")) {
            try {
                return aa.a(file);
            } catch (IOException e) {
                f366a.c("Error opening ZIP file", e);
            }
        }
        if (file.getName().endsWith(".sqlite")) {
            try {
                return b.a(file);
            } catch (SQLiteException e2) {
                f366a.c("Error opening SQL file", e2);
            }
        }
        if (file.getName().endsWith(".gemf")) {
            try {
                return c.a(file);
            } catch (IOException e3) {
                f366a.c("Error opening GEMF file", e3);
            }
        }
        return null;
    }
}
