package org.osmdroid.c.b;

import java.io.File;
import java.io.InputStream;
import org.osmdroid.c.c.d;
import org.osmdroid.util.b;

public class c implements d {

    /* renamed from: a  reason: collision with root package name */
    private final b f369a;

    private c(File file) {
        this.f369a = new b(file);
    }

    public static c a(File file) {
        return new c(file);
    }

    public InputStream a(d dVar, org.osmdroid.c.d dVar2) {
        return this.f369a.a(dVar2.b(), dVar2.c(), dVar2.a());
    }

    public String toString() {
        return "GEMFFileArchive [mGEMFFile=" + this.f369a.a() + "]";
    }
}
