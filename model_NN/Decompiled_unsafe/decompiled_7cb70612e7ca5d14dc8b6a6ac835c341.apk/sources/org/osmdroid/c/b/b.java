package org.osmdroid.c.b;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import org.c.c;
import org.osmdroid.c.c.d;

public class b implements d {

    /* renamed from: a  reason: collision with root package name */
    private static final org.c.b f368a = c.a(b.class);
    private final SQLiteDatabase b;

    private b(SQLiteDatabase sQLiteDatabase) {
        this.b = sQLiteDatabase;
    }

    public static b a(File file) {
        return new b(SQLiteDatabase.openOrCreateDatabase(file, (SQLiteDatabase.CursorFactory) null));
    }

    public InputStream a(d dVar, org.osmdroid.c.d dVar2) {
        ByteArrayInputStream byteArrayInputStream;
        try {
            long a2 = (long) dVar2.a();
            Cursor query = this.b.query("tiles", new String[]{"tile"}, "key = " + (((long) dVar2.c()) + ((((long) dVar2.b()) + (a2 << ((int) a2))) << ((int) a2))) + " and provider = '" + dVar.a() + "'", null, null, null, null);
            if (query.getCount() != 0) {
                query.moveToFirst();
                byteArrayInputStream = new ByteArrayInputStream(query.getBlob(0));
            } else {
                byteArrayInputStream = null;
            }
            query.close();
            if (byteArrayInputStream != null) {
                return byteArrayInputStream;
            }
        } catch (Throwable th) {
            f368a.b("Error getting db stream: " + dVar2, th);
        }
        return null;
    }

    public String toString() {
        return "DatabaseFileArchive [mDatabase=" + this.b.getPath() + "]";
    }
}
