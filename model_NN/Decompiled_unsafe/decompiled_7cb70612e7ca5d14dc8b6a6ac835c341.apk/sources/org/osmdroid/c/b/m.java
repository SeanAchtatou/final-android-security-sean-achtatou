package org.osmdroid.c.b;

import android.content.IntentFilter;
import android.os.Environment;
import org.c.b;
import org.c.c;

public abstract class m extends s {

    /* renamed from: a  reason: collision with root package name */
    private static final b f374a = c.a(m.class);
    private boolean c = true;
    private final org.osmdroid.c.b d;
    private o g;

    public m(org.osmdroid.c.b bVar, int i, int i2) {
        super(i, i2);
        h();
        this.d = bVar;
        this.g = new o(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.MEDIA_MOUNTED");
        intentFilter.addAction("android.intent.action.MEDIA_UNMOUNTED");
        intentFilter.addDataScheme("file");
        bVar.a(this.g, intentFilter);
    }

    /* access modifiers changed from: private */
    public void h() {
        String externalStorageState = Environment.getExternalStorageState();
        f374a.b("sdcard state: " + externalStorageState);
        this.c = "mounted".equals(externalStorageState);
    }

    /* access modifiers changed from: protected */
    public void f() {
    }

    /* access modifiers changed from: protected */
    public void g() {
    }

    /* access modifiers changed from: protected */
    public boolean i() {
        return this.c;
    }

    public void j() {
        if (this.g != null) {
            this.d.a(this.g);
            this.g = null;
        }
        super.j();
    }
}
