package org.osmdroid.c.d;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import org.osmdroid.c.b;

public class c implements b {

    /* renamed from: a  reason: collision with root package name */
    private final Context f391a;

    public c(Context context) {
        this.f391a = context;
    }

    public Intent a(BroadcastReceiver broadcastReceiver, IntentFilter intentFilter) {
        return this.f391a.registerReceiver(broadcastReceiver, intentFilter);
    }

    public void a(BroadcastReceiver broadcastReceiver) {
        this.f391a.unregisterReceiver(broadcastReceiver);
    }
}
