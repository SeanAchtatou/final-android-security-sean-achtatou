package org.osmdroid.c.d;

import android.content.SharedPreferences;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.c.b;
import org.c.c;

public class a implements org.osmdroid.c.a.a {

    /* renamed from: a  reason: collision with root package name */
    private static final b f389a = c.a(a.class);
    private static String b = "android_id";
    private static String c = "";
    private static String d = "";
    private static SharedPreferences.Editor g;

    public static String a() {
        return c;
    }

    public static String b() {
        if (d.length() == 0) {
            synchronized (d) {
                if (d.length() == 0) {
                    try {
                        HttpResponse execute = new DefaultHttpClient().execute(new HttpPost("http://auth.cloudmade.com/token/" + c + "?userid=" + b));
                        if (execute.getStatusLine().getStatusCode() == 200) {
                            d = new BufferedReader(new InputStreamReader(execute.getEntity().getContent()), 8192).readLine().trim();
                            if (d.length() > 0) {
                                g.putString("CLOUDMADE_TOKEN", d);
                                g.commit();
                                g = null;
                            } else {
                                f389a.d("No authorization token received from Cloudmade");
                            }
                        }
                    } catch (IOException e) {
                        f389a.d("No authorization token received from Cloudmade: " + e);
                    }
                }
            }
        }
        return d;
    }
}
