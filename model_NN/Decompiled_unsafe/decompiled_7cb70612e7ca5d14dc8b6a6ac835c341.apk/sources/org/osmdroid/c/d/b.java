package org.osmdroid.c.d;

import android.os.Handler;
import android.os.Message;
import android.view.View;

public class b extends Handler {

    /* renamed from: a  reason: collision with root package name */
    private final View f390a;

    public b(View view) {
        this.f390a = view;
    }

    public void handleMessage(Message message) {
        switch (message.what) {
            case 0:
                this.f390a.invalidate();
                return;
            default:
                return;
        }
    }
}
