package org.osmdroid.c;

import android.content.Context;
import org.osmdroid.c.b.f;
import org.osmdroid.c.b.g;
import org.osmdroid.c.b.j;
import org.osmdroid.c.b.p;
import org.osmdroid.c.b.w;
import org.osmdroid.c.b.x;
import org.osmdroid.c.c.d;
import org.osmdroid.c.d.c;

public class h extends f implements a {
    public h(Context context, d dVar) {
        this(new c(context), new w(context), dVar);
    }

    public h(b bVar, f fVar, d dVar) {
        super(bVar);
        x xVar = new x();
        this.f394a.add(new p(bVar));
        this.f394a.add(new j(bVar, dVar));
        this.f394a.add(new g(dVar, xVar, fVar));
    }
}
