package org.osmdroid.c;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private final int f388a;
    private final int b;
    private final int c;

    public d(int i, int i2, int i3) {
        this.c = i;
        this.f388a = i2;
        this.b = i3;
    }

    public int a() {
        return this.c;
    }

    public int b() {
        return this.f388a;
    }

    public int c() {
        return this.b;
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof d)) {
            return false;
        }
        d dVar = (d) obj;
        if (!(this.c == dVar.c && this.f388a == dVar.f388a && this.b == dVar.b)) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        return 17 * (this.c + 37) * (this.f388a + 37) * (this.b + 37);
    }

    public String toString() {
        return "/" + this.c + "/" + this.f388a + "/" + this.b;
    }
}
