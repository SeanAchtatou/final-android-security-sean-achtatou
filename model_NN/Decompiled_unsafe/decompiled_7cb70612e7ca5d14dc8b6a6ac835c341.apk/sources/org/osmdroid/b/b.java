package org.osmdroid.b;

public class b {

    /* renamed from: a  reason: collision with root package name */
    protected org.osmdroid.d.b f364a;
    protected int b;
    protected int c;

    public b(org.osmdroid.d.b bVar, int i, int i2) {
        this.f364a = bVar;
        this.b = i;
        this.c = i2;
    }

    public String toString() {
        return "ScrollEvent [source=" + this.f364a + ", x=" + this.b + ", y=" + this.c + "]";
    }
}
