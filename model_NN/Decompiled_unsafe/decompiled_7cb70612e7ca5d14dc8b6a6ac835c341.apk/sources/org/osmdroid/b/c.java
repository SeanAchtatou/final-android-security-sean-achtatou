package org.osmdroid.b;

import org.osmdroid.d.b;

public class c {

    /* renamed from: a  reason: collision with root package name */
    protected b f365a;
    protected int b;

    public c(b bVar, int i) {
        this.f365a = bVar;
        this.b = i;
    }

    public String toString() {
        return "ZoomEvent [source=" + this.f365a + ", zoomLevel=" + this.b + "]";
    }
}
