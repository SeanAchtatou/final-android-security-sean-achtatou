package org.b.a.a;

public class d {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public float f355a;
    /* access modifiers changed from: private */
    public float b;
    /* access modifiers changed from: private */
    public float c;
    /* access modifiers changed from: private */
    public float d;
    /* access modifiers changed from: private */
    public float e;
    /* access modifiers changed from: private */
    public float f;
    /* access modifiers changed from: private */
    public boolean g;
    /* access modifiers changed from: private */
    public boolean h;
    /* access modifiers changed from: private */
    public boolean i;

    public float a() {
        if (!this.g) {
            return 1.0f;
        }
        return this.c;
    }

    /* access modifiers changed from: protected */
    public void a(float f2, float f3, float f4, float f5, float f6, float f7) {
        float f8 = 1.0f;
        this.f355a = f2;
        this.b = f3;
        if (f4 == 0.0f) {
            f4 = 1.0f;
        }
        this.c = f4;
        if (f5 == 0.0f) {
            f5 = 1.0f;
        }
        this.d = f5;
        if (f6 != 0.0f) {
            f8 = f6;
        }
        this.e = f8;
        this.f = f7;
    }

    public void a(float f2, float f3, boolean z, float f4, boolean z2, float f5, float f6, boolean z3, float f7) {
        float f8 = 1.0f;
        this.f355a = f2;
        this.b = f3;
        this.g = z;
        if (f4 == 0.0f) {
            f4 = 1.0f;
        }
        this.c = f4;
        this.h = z2;
        if (f5 == 0.0f) {
            f5 = 1.0f;
        }
        this.d = f5;
        if (f6 != 0.0f) {
            f8 = f6;
        }
        this.e = f8;
        this.i = z3;
        this.f = f7;
    }
}
