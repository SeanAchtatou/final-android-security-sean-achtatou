package org.b.a.a;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private int f354a;
    private float[] b = new float[20];
    private float[] c = new float[20];
    private float[] d = new float[20];
    private int[] e = new int[20];
    private float f;
    private float g;
    private float h;
    private float i;
    private float j;
    private float k;
    private float l;
    private float m;
    private boolean n;
    private boolean o;
    private boolean p;
    private boolean q;
    private boolean r;
    private int s;
    /* access modifiers changed from: private */
    public long t;

    private int a(int i2) {
        int i3 = 32768;
        int i4 = i2;
        int i5 = 15;
        int i6 = 0;
        while (true) {
            int i7 = i5 - 1;
            int i8 = ((i6 << 1) + i3) << i5;
            if (i4 >= i8) {
                i6 += i3;
                i4 -= i8;
            }
            int i9 = i3 >> 1;
            if (i9 <= 0) {
                return i6;
            }
            i3 = i9;
            i5 = i7;
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, float[] fArr, float[] fArr2, float[] fArr3, int[] iArr, int i3, boolean z, long j2) {
        this.t = j2;
        this.s = i3;
        this.f354a = i2;
        for (int i4 = 0; i4 < i2; i4++) {
            this.b[i4] = fArr[i4];
            this.c[i4] = fArr2[i4];
            this.d[i4] = fArr3[i4];
            this.e[i4] = iArr[i4];
        }
        this.n = z;
        this.o = i2 >= 2;
        if (this.o) {
            this.f = (fArr[0] + fArr[1]) * 0.5f;
            this.g = (fArr2[0] + fArr2[1]) * 0.5f;
            this.h = (fArr3[0] + fArr3[1]) * 0.5f;
            this.i = Math.abs(fArr[1] - fArr[0]);
            this.j = Math.abs(fArr2[1] - fArr2[0]);
        } else {
            this.f = fArr[0];
            this.g = fArr2[0];
            this.h = fArr3[0];
            this.j = 0.0f;
            this.i = 0.0f;
        }
        this.r = false;
        this.q = false;
        this.p = false;
    }

    public boolean a() {
        return this.o;
    }

    public float b() {
        if (this.o) {
            return this.i;
        }
        return 0.0f;
    }

    public float c() {
        if (this.o) {
            return this.j;
        }
        return 0.0f;
    }

    public float d() {
        if (!this.p) {
            this.l = this.o ? (this.i * this.i) + (this.j * this.j) : 0.0f;
            this.p = true;
        }
        return this.l;
    }

    public float e() {
        float f2 = 0.0f;
        if (!this.q) {
            if (!this.o) {
                this.k = 0.0f;
            } else {
                float d2 = d();
                if (d2 != 0.0f) {
                    f2 = ((float) a((int) (256.0f * d2))) / 16.0f;
                }
                this.k = f2;
                if (this.k < this.i) {
                    this.k = this.i;
                }
                if (this.k < this.j) {
                    this.k = this.j;
                }
            }
            this.q = true;
        }
        return this.k;
    }

    public float f() {
        if (!this.r) {
            if (!this.o) {
                this.m = 0.0f;
            } else {
                this.m = (float) Math.atan2((double) (this.c[1] - this.c[0]), (double) (this.b[1] - this.b[0]));
            }
            this.r = true;
        }
        return this.m;
    }

    public float g() {
        return this.f;
    }

    public float h() {
        return this.g;
    }

    public boolean i() {
        return this.n;
    }

    public long j() {
        return this.t;
    }
}
