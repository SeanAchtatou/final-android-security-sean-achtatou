package org.b.a.a;

import android.util.Log;
import android.view.MotionEvent;
import java.lang.reflect.Method;

public class a {
    private static Method A;
    private static Method B;
    private static Method C;
    private static Method D;
    private static int E;
    private static int F;
    private static final float[] G = new float[20];
    private static final float[] H = new float[20];
    private static final float[] I = new float[20];
    private static final int[] J = new int[20];
    public static final boolean b;
    private static Method w;
    private static Method x;
    private static Method y;
    private static Method z;

    /* renamed from: a  reason: collision with root package name */
    b f353a;
    private c c = new c();
    private c d = new c();
    private float e;
    private float f;
    private float g;
    private float h;
    private float i;
    private float j;
    private boolean k;
    private Object l = null;
    private d m = new d();
    private long n;
    private long o;
    private float p;
    private float q;
    private float r;
    private float s;
    private float t;
    private float u;
    private int v = 0;

    static {
        boolean z2 = true;
        E = 6;
        F = 8;
        try {
            w = MotionEvent.class.getMethod("getPointerCount", new Class[0]);
            x = MotionEvent.class.getMethod("getPointerId", Integer.TYPE);
            y = MotionEvent.class.getMethod("getPressure", Integer.TYPE);
            z = MotionEvent.class.getMethod("getHistoricalX", Integer.TYPE, Integer.TYPE);
            A = MotionEvent.class.getMethod("getHistoricalY", Integer.TYPE, Integer.TYPE);
            B = MotionEvent.class.getMethod("getHistoricalPressure", Integer.TYPE, Integer.TYPE);
            C = MotionEvent.class.getMethod("getX", Integer.TYPE);
            D = MotionEvent.class.getMethod("getY", Integer.TYPE);
        } catch (Exception e2) {
            Log.e("MultiTouchController", "static initializer failed", e2);
            z2 = false;
        }
        b = z2;
        if (b) {
            try {
                E = MotionEvent.class.getField("ACTION_POINTER_UP").getInt(null);
                F = MotionEvent.class.getField("ACTION_POINTER_INDEX_SHIFT").getInt(null);
            } catch (Exception e3) {
            }
        }
    }

    public a(b bVar, boolean z2) {
        this.k = z2;
        this.f353a = bVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    private void a() {
        float f2 = 0.0f;
        this.e = this.c.g();
        this.f = this.c.h();
        this.g = Math.max(21.3f, !this.m.g ? 0.0f : this.c.e());
        this.h = Math.max(30.0f, !this.m.h ? 0.0f : this.c.b());
        this.i = Math.max(30.0f, !this.m.h ? 0.0f : this.c.c());
        if (this.m.i) {
            f2 = this.c.f();
        }
        this.j = f2;
    }

    private void a(int i2, float[] fArr, float[] fArr2, float[] fArr3, int[] iArr, int i3, boolean z2, long j2) {
        c cVar = this.d;
        this.d = this.c;
        this.c = cVar;
        this.c.a(i2, fArr, fArr2, fArr3, iArr, i3, z2, j2);
        d();
    }

    private void b() {
        if (this.l != null) {
            this.f353a.a(this.l, this.m);
            float d2 = 1.0f / (!this.m.g ? 1.0f : this.m.c == 0.0f ? 1.0f : this.m.c);
            a();
            this.p = (this.e - this.m.f355a) * d2;
            this.q = d2 * (this.f - this.m.b);
            this.r = this.m.c / this.g;
            this.t = this.m.d / this.h;
            this.u = this.m.e / this.i;
            this.s = this.m.f - this.j;
        }
    }

    private void c() {
        float f2 = 1.0f;
        if (this.l != null) {
            if (this.m.g && this.m.c != 0.0f) {
                f2 = this.m.c;
            }
            a();
            this.m.a(this.e - (this.p * f2), this.f - (f2 * this.q), this.g * this.r, this.h * this.t, this.i * this.u, this.j + this.s);
            if (!this.f353a.a(this.l, this.m, this.c)) {
            }
        }
    }

    private void d() {
        switch (this.v) {
            case 0:
                if (this.c.i()) {
                    this.l = this.f353a.a(this.c);
                    if (this.l != null) {
                        this.v = 1;
                        this.f353a.a(this.l, this.c);
                        b();
                        long j2 = this.c.j();
                        this.o = j2;
                        this.n = j2;
                        return;
                    }
                    return;
                }
                return;
            case 1:
                if (!this.c.i()) {
                    this.v = 0;
                    b bVar = this.f353a;
                    this.l = null;
                    bVar.a((Object) null, this.c);
                    return;
                } else if (this.c.a()) {
                    this.v = 2;
                    b();
                    this.n = this.c.j();
                    this.o = this.n + 20;
                    return;
                } else if (this.c.j() < this.o) {
                    b();
                    return;
                } else {
                    c();
                    return;
                }
            case 2:
                if (!this.c.a() || !this.c.i()) {
                    if (!this.c.i()) {
                        this.v = 0;
                        b bVar2 = this.f353a;
                        this.l = null;
                        bVar2.a((Object) null, this.c);
                        return;
                    }
                    this.v = 1;
                    b();
                    this.n = this.c.j();
                    this.o = this.n + 20;
                    return;
                } else if (Math.abs(this.c.g() - this.d.g()) > 30.0f || Math.abs(this.c.h() - this.d.h()) > 30.0f || Math.abs(this.c.b() - this.d.b()) * 0.5f > 40.0f || Math.abs(this.c.c() - this.d.c()) * 0.5f > 40.0f) {
                    b();
                    this.n = this.c.j();
                    this.o = this.n + 20;
                    return;
                } else if (this.c.t < this.o) {
                    b();
                    return;
                } else {
                    c();
                    return;
                }
            default:
                return;
        }
    }

    public boolean a(MotionEvent motionEvent) {
        try {
            int intValue = b ? ((Integer) w.invoke(motionEvent, new Object[0])).intValue() : 1;
            if (this.v == 0 && !this.k && intValue == 1) {
                return false;
            }
            int action = motionEvent.getAction();
            int historySize = motionEvent.getHistorySize() / intValue;
            int i2 = 0;
            while (i2 <= historySize) {
                boolean z2 = i2 < historySize;
                if (!b || intValue == 1) {
                    G[0] = z2 ? motionEvent.getHistoricalX(i2) : motionEvent.getX();
                    H[0] = z2 ? motionEvent.getHistoricalY(i2) : motionEvent.getY();
                    I[0] = z2 ? motionEvent.getHistoricalPressure(i2) : motionEvent.getPressure();
                } else {
                    int min = Math.min(intValue, 20);
                    for (int i3 = 0; i3 < min; i3++) {
                        J[i3] = ((Integer) x.invoke(motionEvent, Integer.valueOf(i3))).intValue();
                        G[i3] = ((Float) (z2 ? z.invoke(motionEvent, Integer.valueOf(i3), Integer.valueOf(i2)) : C.invoke(motionEvent, Integer.valueOf(i3)))).floatValue();
                        H[i3] = ((Float) (z2 ? A.invoke(motionEvent, Integer.valueOf(i3), Integer.valueOf(i2)) : D.invoke(motionEvent, Integer.valueOf(i3)))).floatValue();
                        I[i3] = ((Float) (z2 ? B.invoke(motionEvent, Integer.valueOf(i3), Integer.valueOf(i2)) : y.invoke(motionEvent, Integer.valueOf(i3)))).floatValue();
                    }
                }
                a(intValue, G, H, I, J, z2 ? 2 : action, z2 ? true : (action == 1 || (((1 << F) + -1) & action) == E || action == 3) ? false : true, z2 ? motionEvent.getHistoricalEventTime(i2) : motionEvent.getEventTime());
                i2++;
            }
            return true;
        } catch (Exception e2) {
            Log.e("MultiTouchController", "onTouchEvent() failed", e2);
            return false;
        }
    }
}
