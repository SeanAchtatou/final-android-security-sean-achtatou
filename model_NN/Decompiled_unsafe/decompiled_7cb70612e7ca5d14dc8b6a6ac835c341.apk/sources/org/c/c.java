package org.c;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import org.c.a.e;
import org.c.a.g;
import org.c.a.h;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    static int f361a = 0;
    static g b = new g();
    static e c = new e();
    private static final String[] d = {"1.6"};
    private static String e = "org/slf4j/impl/StaticLoggerBinder.class";

    private c() {
    }

    public static a a() {
        if (f361a == 0) {
            f361a = 1;
            b();
        }
        switch (f361a) {
            case 1:
                return b;
            case 2:
                throw new IllegalStateException("org.slf4j.LoggerFactory could not be successfully initialized. See also http://www.slf4j.org/codes.html#unsuccessfulInit");
            case 3:
                return org.c.b.c.a().b();
            case 4:
                return c;
            default:
                throw new IllegalStateException("Unreachable code");
        }
    }

    public static b a(Class cls) {
        return a(cls.getName());
    }

    public static b a(String str) {
        return a().a(str);
    }

    static void a(Throwable th) {
        f361a = 2;
        h.a("Failed to instantiate SLF4J LoggerFactory", th);
    }

    private static final void b() {
        f();
        c();
        if (f361a == 3) {
            e();
        }
    }

    private static final void c() {
        try {
            org.c.b.c.a();
            f361a = 3;
            d();
        } catch (NoClassDefFoundError e2) {
            String message = e2.getMessage();
            if (message == null || message.indexOf("org/slf4j/impl/StaticLoggerBinder") == -1) {
                a(e2);
                throw e2;
            }
            f361a = 4;
            h.a("Failed to load class \"org.slf4j.impl.StaticLoggerBinder\".");
            h.a("Defaulting to no-operation (NOP) logger implementation");
            h.a("See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.");
        } catch (NoSuchMethodError e3) {
            String message2 = e3.getMessage();
            if (!(message2 == null || message2.indexOf("org.slf4j.impl.StaticLoggerBinder.getSingleton()") == -1)) {
                f361a = 2;
                h.a("slf4j-api 1.6.x (or later) is incompatible with this binding.");
                h.a("Your binding is version 1.5.5 or earlier.");
                h.a("Upgrade your binding to version 1.6.x. or 2.0.x");
            }
            throw e3;
        } catch (Exception e4) {
            a(e4);
            throw new IllegalStateException("Unexpected initialization failure", e4);
        }
    }

    private static final void d() {
        List a2 = b.a();
        if (a2.size() != 0) {
            h.a("The following loggers will not work becasue they were created");
            h.a("during the default configuration phase of the underlying logging system.");
            h.a("See also http://www.slf4j.org/codes.html#substituteLogger");
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < a2.size()) {
                    h.a((String) a2.get(i2));
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    private static final void e() {
        boolean z = false;
        try {
            String str = org.c.b.c.f360a;
            for (String startsWith : d) {
                if (str.startsWith(startsWith)) {
                    z = true;
                }
            }
            if (!z) {
                h.a("The requested version " + str + " by your slf4j binding is not compatible with " + Arrays.asList(d).toString());
                h.a("See http://www.slf4j.org/codes.html#version_mismatch for further details.");
            }
        } catch (NoSuchFieldError e2) {
        } catch (Throwable th) {
            h.a("Unexpected problem occured during version sanity check", th);
        }
    }

    private static void f() {
        try {
            ClassLoader classLoader = c.class.getClassLoader();
            Enumeration<URL> systemResources = classLoader == null ? ClassLoader.getSystemResources(e) : classLoader.getResources(e);
            ArrayList arrayList = new ArrayList();
            while (systemResources.hasMoreElements()) {
                arrayList.add(systemResources.nextElement());
            }
            if (arrayList.size() > 1) {
                h.a("Class path contains multiple SLF4J bindings.");
                for (int i = 0; i < arrayList.size(); i++) {
                    h.a("Found binding in [" + arrayList.get(i) + "]");
                }
                h.a("See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.");
            }
        } catch (IOException e2) {
            h.a("Error getting resources from path", e2);
        }
    }
}
