package org.c.a;

public class h {
    public static final void a(String str) {
        System.err.println("SLF4J: " + str);
    }

    public static final void a(String str, Throwable th) {
        System.err.println(str);
        System.err.println("Reported exception:");
        th.printStackTrace();
    }
}
