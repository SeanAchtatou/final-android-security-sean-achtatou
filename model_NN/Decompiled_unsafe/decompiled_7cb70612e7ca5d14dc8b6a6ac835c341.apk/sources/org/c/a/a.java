package org.c.a;

public class a {

    /* renamed from: a  reason: collision with root package name */
    public static a f356a = new a(null);
    private String b;
    private Throwable c;
    private Object[] d;

    public a(String str) {
        this(str, null, null);
    }

    public a(String str, Object[] objArr, Throwable th) {
        this.b = str;
        this.c = th;
        if (th == null) {
            this.d = objArr;
        } else {
            this.d = a(objArr);
        }
    }

    static Object[] a(Object[] objArr) {
        if (objArr == null || objArr.length == 0) {
            throw new IllegalStateException("non-sensical empty or null argument array");
        }
        int length = objArr.length - 1;
        Object[] objArr2 = new Object[length];
        System.arraycopy(objArr, 0, objArr2, 0, length);
        return objArr2;
    }

    public String a() {
        return this.b;
    }
}
