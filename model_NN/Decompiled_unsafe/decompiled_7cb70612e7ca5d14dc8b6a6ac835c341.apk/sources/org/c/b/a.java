package org.c.b;

import android.util.Log;
import org.c.a.b;
import org.c.a.c;

public class a extends b {
    a(String str) {
        this.b = str;
    }

    private String b(String str, Object obj, Object obj2) {
        return c.a(str, obj, obj2).a();
    }

    public void a(String str) {
        Log.d(this.b, str);
    }

    public void a(String str, Object obj, Object obj2) {
        Log.e(this.b, b(str, obj, obj2));
    }

    public void a(String str, Throwable th) {
        Log.i(this.b, str, th);
    }

    public void b(String str) {
        Log.i(this.b, str);
    }

    public void b(String str, Throwable th) {
        Log.w(this.b, str, th);
    }

    public void c(String str) {
        Log.w(this.b, str);
    }

    public void c(String str, Throwable th) {
        Log.e(this.b, str, th);
    }

    public void d(String str) {
        Log.e(this.b, str);
    }
}
