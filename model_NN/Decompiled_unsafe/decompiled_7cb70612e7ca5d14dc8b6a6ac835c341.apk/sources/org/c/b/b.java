package org.c.b;

import android.util.Log;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import org.c.a;

public class b implements a {

    /* renamed from: a  reason: collision with root package name */
    private final Map f359a = new HashMap();

    private final String c(String str) {
        if (str == null || str.length() <= 23) {
            return str;
        }
        StringTokenizer stringTokenizer = new StringTokenizer(str, ".");
        if (stringTokenizer.hasMoreTokens()) {
            StringBuilder sb = new StringBuilder();
            do {
                String nextToken = stringTokenizer.nextToken();
                if (nextToken.length() == 1) {
                    sb.append(nextToken);
                    sb.append('.');
                } else if (stringTokenizer.hasMoreTokens()) {
                    sb.append(nextToken.charAt(0));
                    sb.append("*.");
                } else {
                    sb.append(nextToken);
                }
            } while (stringTokenizer.hasMoreTokens());
            str = sb.toString();
        }
        return str.length() > 23 ? str.substring(0, 22) + '*' : str;
    }

    /* renamed from: b */
    public a a(String str) {
        a aVar;
        String c = c(str);
        synchronized (this) {
            aVar = (a) this.f359a.get(c);
            if (aVar == null) {
                if (!c.equals(str)) {
                    Log.i(b.class.getSimpleName(), "Logger name '" + str + "' exceeds maximum length of " + 23 + " characters, using '" + c + "' instead.");
                }
                aVar = new a(c);
                this.f359a.put(c, aVar);
            }
        }
        return aVar;
    }
}
