package afroggyphoto201011_3.com;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int bg1 = 2130837504;
        public static final int bg2 = 2130837505;
        public static final int button = 2130837506;
        public static final int buttonselected = 2130837507;
        public static final int icon = 2130837508;
        public static final int main_bg = 2130837509;
        public static final int s = 2130837510;
        public static final int tnc = 2130837511;
    }

    public static final class id {
        public static final int Button01 = 2131034126;
        public static final int Button02 = 2131034116;
        public static final int ImageView01 = 2131034114;
        public static final int ImageView02 = 2131034127;
        public static final int Next = 2131034128;
        public static final int OK = 2131034115;
        public static final int ServiceGin = 2131034123;
        public static final int ServiceText01 = 2131034121;
        public static final int TextView01 = 2131034113;
        public static final int TextView02 = 2131034118;
        public static final int TextView03 = 2131034124;
        public static final int TextView04 = 2131034119;
        public static final int TextView05 = 2131034120;
        public static final int TextView06 = 2131034122;
        public static final int TncView = 2131034112;
        public static final int imageview = 2131034125;
        public static final int service = 2131034117;
    }

    public static final class layout {
        public static final int main = 2130903040;
        public static final int service = 2130903041;
        public static final int sub = 2130903042;
    }

    public static final class string {
        public static final int TQ = 2130968578;
        public static final int app_name = 2130968577;
        public static final int hello = 2130968576;
    }
}
