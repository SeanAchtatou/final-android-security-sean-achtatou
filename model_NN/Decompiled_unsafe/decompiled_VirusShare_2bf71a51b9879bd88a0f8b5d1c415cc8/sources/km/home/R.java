package km.home;

public final class R {

    public static final class anim {
        public static final int fade_in = 2130968576;
        public static final int fade_out = 2130968577;
        public static final int grid_entry = 2130968578;
        public static final int grid_exit = 2130968579;
        public static final int hide_applications = 2130968580;
        public static final int show_applications = 2130968581;
    }

    public static final class attr {
        public static final int marginBottom = 2130771972;
        public static final int marginLeft = 2130771969;
        public static final int marginRight = 2130771971;
        public static final int marginTop = 2130771970;
        public static final int stackOrientation = 2130771968;
    }

    public static final class color {
        public static final int bright_text_dark_focused = 2131165188;
        public static final int bubble_dark_background = 2131165186;
        public static final int delete_color_filter = 2131165187;
        public static final int grid_dark_background = 2131165185;
        public static final int window_background = 2131165184;
    }

    public static final class drawable {
        public static final int all_applications = 2130837504;
        public static final int all_applications_background = 2130837505;
        public static final int all_applications_button_background = 2130837506;
        public static final int all_applications_label_background = 2130837507;
        public static final int application_background = 2130837508;
        public static final int application_background_static = 2130837509;
        public static final int bg_android = 2130837510;
        public static final int bg_android_icon = 2130837511;
        public static final int bg_sunrise = 2130837512;
        public static final int bg_sunrise_icon = 2130837513;
        public static final int bg_sunset = 2130837514;
        public static final int bg_sunset_icon = 2130837515;
        public static final int favorite_background = 2130837516;
        public static final int focused_application_background_static = 2130837517;
        public static final int grid_selector = 2130837518;
        public static final int hide_all_applications = 2130837519;
        public static final int ic_launcher_allhide = 2130837520;
        public static final int ic_launcher_allshow = 2130837521;
        public static final int ic_launcher_home = 2130837522;
        public static final int icon = 2130837523;
        public static final int pressed_application_background_static = 2130837524;
        public static final int show_all_applications = 2130837525;
    }

    public static final class id {
        public static final int all_apps = 2131099654;
        public static final int gallery = 2131099662;
        public static final int home = 2131099653;
        public static final int horizontal = 2131099648;
        public static final int icon = 2131099655;
        public static final int label = 2131099652;
        public static final int show_all_apps = 2131099650;
        public static final int show_all_apps_check = 2131099651;
        public static final int themeIcon = 2131099656;
        public static final int themeLabel = 2131099657;
        public static final int unlock = 2131099658;
        public static final int unlock_button = 2131099661;
        public static final int unlock_date = 2131099660;
        public static final int unlock_time = 2131099659;
        public static final int vertical = 2131099649;
    }

    public static final class layout {
        public static final int all_applications_button = 2130903040;
        public static final int application = 2130903041;
        public static final int favorite = 2130903042;
        public static final int home = 2130903043;
        public static final int new_application = 2130903044;
        public static final int theme_item = 2130903045;
        public static final int unlock = 2130903046;
        public static final int wallpaper = 2130903047;
    }

    public static final class raw {
        public static final int default_theme = 2131034112;
    }

    public static final class string {
        public static final int home_title = 2131230720;
        public static final int menu_search = 2131230723;
        public static final int menu_settings = 2131230724;
        public static final int menu_theme = 2131230725;
        public static final int menu_wallpaper = 2131230722;
        public static final int show_all_apps = 2131230721;
        public static final int unlock_button = 2131230726;
        public static final int wallpaper_instructions = 2131230727;
    }

    public static final class style {
        public static final int Theme = 2131296256;
    }

    public static final class styleable {
        public static final int[] ApplicationsStackLayout = {R.attr.stackOrientation, R.attr.marginLeft, R.attr.marginTop, R.attr.marginRight, R.attr.marginBottom};
        public static final int ApplicationsStackLayout_marginBottom = 4;
        public static final int ApplicationsStackLayout_marginLeft = 1;
        public static final int ApplicationsStackLayout_marginRight = 3;
        public static final int ApplicationsStackLayout_marginTop = 2;
        public static final int ApplicationsStackLayout_stackOrientation = 0;
    }
}
