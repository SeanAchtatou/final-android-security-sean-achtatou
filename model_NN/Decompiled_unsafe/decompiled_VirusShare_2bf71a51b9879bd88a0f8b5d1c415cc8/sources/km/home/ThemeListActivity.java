package km.home;

import android.app.ListActivity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.io.File;
import java.util.ArrayList;
import km.theme.ThemeAdapter;
import km.theme.ThemeInfo;

public class ThemeListActivity extends ListActivity {
    private ListAdapter adapter;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        String value;
        super.onCreate(savedInstanceState);
        setTitle("已安装KM主题列表");
        SharedPreferences sp = UnlockActivity.themeMgr.getSp();
        ArrayList<ThemeInfo> list = new ArrayList<>();
        for (String key : sp.getAll().keySet()) {
            if (!(key == null || key.equals(ThemeManager.CURRENT_THEME) || (value = sp.getString(key, null)) == null)) {
                ThemeInfo themeInfo = new ThemeInfo();
                themeInfo.id = key;
                themeInfo.path = String.valueOf(UnlockActivity.themeMgr.getThemePath()) + File.separator + value;
                list.add(themeInfo);
            }
        }
        this.adapter = new ThemeAdapter(this, list);
        setListAdapter(this.adapter);
    }

    /* access modifiers changed from: protected */
    public void onListItemClick(ListView l, View v, int position, long id) {
    }
}
