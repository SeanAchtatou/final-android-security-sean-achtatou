package km.home;

import android.content.ComponentName;
import android.content.Intent;
import android.graphics.drawable.Drawable;

class ApplicationInfo {
    boolean filtered;
    Drawable icon;
    Intent intent;
    CharSequence title;

    ApplicationInfo() {
    }

    /* access modifiers changed from: package-private */
    public final void setActivity(ComponentName className, int launchFlags) {
        this.intent = new Intent("android.intent.action.MAIN");
        this.intent.addCategory("android.intent.category.LAUNCHER");
        this.intent.setComponent(className);
        this.intent.setFlags(launchFlags);
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ApplicationInfo)) {
            return false;
        }
        ApplicationInfo that = (ApplicationInfo) o;
        return this.title.equals(that.title) && this.intent.getComponent().getClassName().equals(that.intent.getComponent().getClassName());
    }

    public int hashCode() {
        int result;
        int i = 0;
        if (this.title != null) {
            result = this.title.hashCode();
        } else {
            result = 0;
        }
        String name = this.intent.getComponent().getClassName();
        int i2 = result * 31;
        if (name != null) {
            i = name.hashCode();
        }
        return i2 + i;
    }
}
