package km.home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import com.km.MainActivity;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.zip.ZipException;
import km.home.config.Config;
import km.home.config.ConfigXmlParser;
import km.tools.FileTool;
import km.tools.ZipUtils;
import org.xmlpull.v1.XmlPullParserException;

public class ThemeManager implements MainActivity.ChargeListener {
    public static final String CURRENT_THEME = "currentTheme";
    private static final String DB_THEME_NAMES = "themes";
    public static final byte FAIL = 2;
    public static final byte SUCCESS = 1;
    private Config config;
    private Context context;
    private ThemeSetupListener listener;
    private String sdcardPath;
    private SharedPreferences sp;
    private String themePath;
    private Config tmpConfig;
    private String tmpThemePath;

    public interface ThemeSetupListener {
        void themeSetupAction(int i);
    }

    public ThemeManager(Context _context) {
        this(_context, null);
    }

    public ThemeManager(Context _context, ThemeSetupListener _listener) {
        this.context = _context;
        this.listener = _listener;
        this.sp = this.context.getSharedPreferences(DB_THEME_NAMES, 0);
        initPath();
        setup();
    }

    private void initPath() {
        this.sdcardPath = Environment.getExternalStorageDirectory().getPath();
        this.tmpThemePath = String.valueOf(this.sdcardPath) + "/KMHome/tmpTheme";
        this.themePath = String.valueOf(this.sdcardPath) + "/KMHome/Theme";
    }

    private void setup() {
        unzipTheme();
        this.tmpConfig = parserConfigXml(String.valueOf(this.tmpThemePath) + File.separator + "default_theme" + File.separator + "config.xml");
        if (this.tmpConfig == null || isSetup()) {
            String path = this.sp.getString(this.sp.getString(CURRENT_THEME, "-1"), null);
            if (path != null) {
                this.config = parserConfigXml(String.valueOf(this.themePath) + File.separator + path + File.separator + "config.xml");
            }
            if (this.config == null) {
                this.config = new Config();
            } else {
                setWallpaper(this.config);
            }
            if (this.listener != null) {
                this.listener.themeSetupAction(1);
                return;
            }
            return;
        }
        MainActivity.setListener(this);
        this.context.startActivity(new Intent(this.context, MainActivity.class));
    }

    private void unzipTheme() {
        File tmpDir = new File(this.tmpThemePath);
        if (tmpDir.exists()) {
            FileTool.fileKiller(tmpDir);
        }
        tmpDir.mkdirs();
        try {
            ZipUtils.upZipInputStream(this.context.getResources().openRawResource(R.raw.default_theme), this.tmpThemePath);
        } catch (ZipException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    private Config parserConfigXml(String path) {
        try {
            return new ConfigXmlParser(new FileInputStream(path)).getConfig();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        return null;
    }

    private boolean isSetup() {
        return this.sp.getString(this.tmpConfig.id, null) != null;
    }

    public boolean setupTheme() {
        FileTool.fileKiller(new File(getCurrentThemePath(this.tmpConfig)));
        if (FileTool.dirCopy(new File(String.valueOf(this.tmpThemePath) + File.separator + "default_theme"), getCurrentThemePath(this.tmpConfig), false)) {
            return this.sp.edit().putString(this.tmpConfig.id, String.valueOf(this.tmpConfig.cnName) + "_" + this.tmpConfig.id).putString(CURRENT_THEME, this.tmpConfig.id).commit();
        }
        return false;
    }

    private void setWallpaper(Config _config) {
        if (_config.workSpaceWallpaper != null) {
            try {
                this.context.setWallpaper(new FileInputStream(String.valueOf(getCurrentThemePath(_config)) + File.separator + _config.workSpaceWallpaper));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    public void setConfig(Config _config) {
        this.config = _config;
        setWallpaper(this.config);
    }

    public void changeTheme(String themeConfigPath) {
        setConfig(parserConfigXml(themeConfigPath));
    }

    public String getCurrentThemePath(Config _config) {
        return String.valueOf(this.themePath) + File.separator + _config.cnName + "_" + _config.id;
    }

    public String getThemePath() {
        return this.themePath;
    }

    public Config getConfig() {
        return this.config;
    }

    public SharedPreferences getSp() {
        return this.sp;
    }

    public void chargeAction(int state) {
        switch (state) {
            case 0:
                return;
            case 1:
                if (setupTheme()) {
                    setConfig(this.tmpConfig);
                    if (this.listener != null) {
                        this.listener.themeSetupAction(1);
                        return;
                    }
                    return;
                } else if (this.listener != null) {
                    this.listener.themeSetupAction(2);
                    return;
                } else {
                    return;
                }
            default:
                if (this.listener != null) {
                    this.listener.themeSetupAction(2);
                    return;
                }
                return;
        }
    }
}
