package km.home;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;

public class ApplicationsStackLayout extends ViewGroup implements View.OnClickListener {
    public static final int HORIZONTAL = 0;
    public static final int VERTICAL = 1;
    private Drawable mBackground;
    private View mButton;
    private Rect mDrawRect = new Rect();
    private List<ApplicationInfo> mFavorites;
    private int mFavoritesEnd;
    private int mFavoritesStart;
    private int mIconSize;
    private LayoutInflater mInflater;
    private int mMarginBottom;
    private int mMarginLeft;
    private int mMarginRight;
    private int mMarginTop;
    private int mOrientation = 1;
    private List<ApplicationInfo> mRecents;

    public ApplicationsStackLayout(Context context) {
        super(context);
        initLayout();
    }

    public ApplicationsStackLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.ApplicationsStackLayout);
        this.mOrientation = a.getInt(0, 1);
        this.mMarginLeft = a.getDimensionPixelSize(1, 0);
        this.mMarginTop = a.getDimensionPixelSize(2, 0);
        this.mMarginRight = a.getDimensionPixelSize(3, 0);
        this.mMarginBottom = a.getDimensionPixelSize(4, 0);
        a.recycle();
        this.mIconSize = 42;
        initLayout();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, km.home.ApplicationsStackLayout, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private void initLayout() {
        this.mInflater = LayoutInflater.from(getContext());
        this.mButton = this.mInflater.inflate((int) R.layout.all_applications_button, (ViewGroup) this, false);
        addView(this.mButton);
        this.mBackground = getBackground();
        setBackgroundDrawable(null);
        setWillNotDraw(false);
    }

    public int getOrientation() {
        return this.mOrientation;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Drawable background = this.mBackground;
        int right = getWidth();
        int bottom = getHeight();
        if (this.mOrientation == 1) {
            this.mDrawRect.set(0, 0, right, this.mFavoritesStart);
        } else {
            this.mDrawRect.set(0, 0, this.mFavoritesStart, bottom);
        }
        background.setBounds(this.mDrawRect);
        background.draw(canvas);
        if (this.mFavoritesStart > -1) {
            if (this.mOrientation == 1) {
                this.mDrawRect.set(0, this.mFavoritesStart, right, this.mFavoritesEnd);
            } else {
                this.mDrawRect.set(this.mFavoritesStart, 0, this.mFavoritesEnd, bottom);
            }
            background.setBounds(this.mDrawRect);
            background.draw(canvas);
        }
        super.onDraw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int widthMode = View.MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = View.MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = View.MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = View.MeasureSpec.getSize(heightMeasureSpec);
        if (widthMode == 1073741824 && heightMode == 1073741824) {
            setMeasuredDimension(widthSize, heightSize);
            return;
        }
        throw new IllegalStateException("ApplicationsStackLayout can only be used with measure spec mode=EXACTLY");
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean changed, int l, int t, int r, int b) {
        removeAllApplications();
        ViewGroup.LayoutParams layoutParams = this.mButton.getLayoutParams();
        this.mButton.measure(View.MeasureSpec.makeMeasureSpec(layoutParams.width, 1073741824), View.MeasureSpec.makeMeasureSpec(layoutParams.height, 1073741824));
        if (this.mOrientation == 1) {
            layoutVertical();
        } else {
            layoutHorizontal();
        }
    }

    private void layoutVertical() {
        int childTop = getHeight();
        int childWidth = this.mButton.getMeasuredWidth();
        int childHeight = this.mButton.getMeasuredHeight();
        int childTop2 = childTop - (this.mMarginBottom + childHeight);
        this.mButton.layout(0, childTop2, 0 + childWidth, childTop2 + childHeight);
        int childTop3 = childTop2 - this.mMarginTop;
        this.mFavoritesEnd = childTop3 - this.mMarginBottom;
        int oldChildTop = childTop3;
        int childTop4 = stackApplications(this.mFavorites, 0, childTop3);
        if (childTop4 != oldChildTop) {
            this.mFavoritesStart = this.mMarginTop + childTop4;
        } else {
            this.mFavoritesStart = -1;
        }
        stackApplications(this.mRecents, 0, childTop4);
    }

    private void layoutHorizontal() {
        int childLeft = getWidth();
        int childWidth = this.mButton.getMeasuredWidth();
        int childLeft2 = childLeft - childWidth;
        this.mButton.layout(childLeft2, 0, childLeft2 + childWidth, 0 + this.mButton.getMeasuredHeight());
        int childLeft3 = childLeft2 - this.mMarginLeft;
        this.mFavoritesEnd = childLeft3 - this.mMarginRight;
        int oldChildLeft = childLeft3;
        int childLeft4 = stackApplications(this.mFavorites, childLeft3, 0);
        if (childLeft4 != oldChildLeft) {
            this.mFavoritesStart = this.mMarginLeft + childLeft4;
        } else {
            this.mFavoritesStart = -1;
        }
        stackApplications(this.mRecents, childLeft4, 0);
    }

    private int stackApplications(List<ApplicationInfo> applications, int childLeft, int childTop) {
        boolean isVertical = this.mOrientation == 1;
        int i = applications.size() - 1;
        while (true) {
            if (i < 0) {
                break;
            }
            View view = createApplicationIcon(this.mInflater, this, applications.get(i));
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            view.measure(View.MeasureSpec.makeMeasureSpec(layoutParams.width, 1073741824), View.MeasureSpec.makeMeasureSpec(layoutParams.height, 1073741824));
            int childWidth = view.getMeasuredWidth();
            int childHeight = view.getMeasuredHeight();
            if (!isVertical) {
                childLeft -= this.mMarginRight + childWidth;
                if (childLeft < 0) {
                    childLeft += this.mMarginRight + childWidth;
                    break;
                }
            } else {
                childTop -= this.mMarginBottom + childHeight;
                if (childTop < 0) {
                    childTop += this.mMarginBottom + childHeight;
                    break;
                }
            }
            addViewInLayout(view, -1, layoutParams);
            view.layout(childLeft, childTop, childLeft + childWidth, childTop + childHeight);
            if (isVertical) {
                childTop -= this.mMarginTop;
            } else {
                childLeft -= this.mMarginLeft;
            }
            i--;
        }
        if (isVertical) {
            return childTop;
        }
        return childLeft;
    }

    private void removeAllApplications() {
        for (int i = getChildCount() - 1; i >= 0; i--) {
            if (getChildAt(i) != this.mButton) {
                removeViewAt(i);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    private View createApplicationIcon(LayoutInflater inflater, ViewGroup group, ApplicationInfo info) {
        TextView textView = (TextView) inflater.inflate((int) R.layout.favorite, group, false);
        info.icon.setBounds(0, 0, this.mIconSize, this.mIconSize);
        textView.setCompoundDrawables(null, info.icon, null, null);
        textView.setText(info.title);
        textView.setTag(info.intent);
        textView.setOnClickListener(this);
        return textView;
    }

    public void setFavorites(List<ApplicationInfo> applications) {
        this.mFavorites = applications;
        requestLayout();
    }

    public void setRecents(List<ApplicationInfo> applications) {
        this.mRecents = applications;
        requestLayout();
    }

    public void onClick(View v) {
        getContext().startActivity((Intent) v.getTag());
    }
}
