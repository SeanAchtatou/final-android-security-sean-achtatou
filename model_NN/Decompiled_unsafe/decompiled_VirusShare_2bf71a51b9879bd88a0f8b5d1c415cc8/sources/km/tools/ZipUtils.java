package km.tools;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class ZipUtils {
    private static final int BUFF_SIZE = 1048576;

    public static void zipFiles(Collection<File> resFileList, File zipFile) throws IOException {
        zipFiles(resFileList, zipFile, null);
    }

    public static void zipFiles(Collection<File> resFileList, File zipFile, String comment) throws IOException {
        ZipOutputStream zipout = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipFile), BUFF_SIZE));
        for (File resFile : resFileList) {
            zipFile(resFile, zipout, "");
        }
        if (comment != null) {
            zipout.setComment(comment);
        }
        zipout.close();
    }

    public static void upZipInputStream(InputStream is, String folderPath) throws ZipException, IOException {
        ZipInputStream zis = new ZipInputStream(is);
        while (true) {
            ZipEntry z = zis.getNextEntry();
            if (z == null) {
                zis.close();
                return;
            }
            String name = z.getName();
            if (z.isDirectory()) {
                new File(String.valueOf(folderPath) + File.separator + name.substring(0, name.length() - 1)).mkdirs();
            } else {
                File file = new File(String.valueOf(folderPath) + File.separator + name);
                File dir = file.getParentFile();
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                file.createNewFile();
                FileOutputStream out = new FileOutputStream(file);
                byte[] buffer = new byte[1024];
                while (true) {
                    int ch = zis.read(buffer);
                    if (ch == -1) {
                        break;
                    }
                    out.write(buffer, 0, ch);
                }
                out.close();
            }
        }
    }

    public static void upZipFile(File zipFile, String folderPath) throws ZipException, IOException {
        File desDir = new File(folderPath);
        if (!desDir.exists()) {
            desDir.mkdirs();
        }
        ZipFile zf = new ZipFile(zipFile);
        Enumeration<?> entries = zf.entries();
        while (entries.hasMoreElements()) {
            ZipEntry entry = (ZipEntry) entries.nextElement();
            InputStream in = zf.getInputStream(entry);
            File desFile = new File(String.valueOf(folderPath) + File.separator + entry.getName());
            if (!desFile.exists()) {
                File fileParentDir = desFile.getParentFile();
                if (!fileParentDir.exists()) {
                    fileParentDir.mkdirs();
                }
                desFile.createNewFile();
            }
            OutputStream out = new FileOutputStream(desFile);
            byte[] buffer = new byte[BUFF_SIZE];
            while (true) {
                int realLength = in.read(buffer);
                if (realLength <= 0) {
                    break;
                }
                out.write(buffer, 0, realLength);
            }
            in.close();
            out.close();
        }
    }

    public static ArrayList<File> upZipSelectedFile(File zipFile, String folderPath, String nameContains) throws ZipException, IOException {
        ArrayList<File> fileList = new ArrayList<>();
        File desDir = new File(folderPath);
        if (!desDir.exists()) {
            desDir.mkdir();
        }
        ZipFile zf = new ZipFile(zipFile);
        Enumeration<?> entries = zf.entries();
        while (entries.hasMoreElements()) {
            ZipEntry entry = (ZipEntry) entries.nextElement();
            if (entry.getName().contains(nameContains)) {
                InputStream in = zf.getInputStream(entry);
                File desFile = new File(String.valueOf(folderPath) + File.separator + entry.getName());
                if (!desFile.exists()) {
                    File fileParentDir = desFile.getParentFile();
                    if (!fileParentDir.exists()) {
                        fileParentDir.mkdirs();
                    }
                    desFile.createNewFile();
                }
                OutputStream out = new FileOutputStream(desFile);
                byte[] buffer = new byte[BUFF_SIZE];
                while (true) {
                    int realLength = in.read(buffer);
                    if (realLength <= 0) {
                        break;
                    }
                    out.write(buffer, 0, realLength);
                }
                in.close();
                out.close();
                fileList.add(desFile);
            }
        }
        return fileList;
    }

    public static ArrayList<String> getEntriesNames(File zipFile) throws ZipException, IOException {
        ArrayList<String> entryNames = new ArrayList<>();
        Enumeration<?> entries = getEntriesEnumeration(zipFile);
        while (entries.hasMoreElements()) {
            entryNames.add(getEntryName((ZipEntry) entries.nextElement()));
        }
        return entryNames;
    }

    public static Enumeration<?> getEntriesEnumeration(File zipFile) throws ZipException, IOException {
        return new ZipFile(zipFile).entries();
    }

    public static String getEntryComment(ZipEntry entry) throws UnsupportedEncodingException {
        return entry.getComment();
    }

    public static String getEntryName(ZipEntry entry) throws UnsupportedEncodingException {
        return entry.getName();
    }

    private static void zipFile(File resFile, ZipOutputStream zipout, String rootpath) throws FileNotFoundException, IOException {
        String rootpath2 = String.valueOf(rootpath) + (rootpath.trim().length() == 0 ? "" : File.separator) + resFile.getName();
        if (resFile.isDirectory()) {
            for (File file : resFile.listFiles()) {
                zipFile(file, zipout, rootpath2);
            }
            return;
        }
        byte[] buffer = new byte[BUFF_SIZE];
        BufferedInputStream in = new BufferedInputStream(new FileInputStream(resFile), BUFF_SIZE);
        zipout.putNextEntry(new ZipEntry(rootpath2));
        while (true) {
            int realLength = in.read(buffer);
            if (realLength == -1) {
                in.close();
                zipout.flush();
                zipout.closeEntry();
                return;
            }
            zipout.write(buffer, 0, realLength);
        }
    }
}
