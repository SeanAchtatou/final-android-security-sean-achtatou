package km.theme;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.File;
import java.util.ArrayList;
import km.home.R;

public class ThemeAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private ArrayList<ThemeInfo> list;

    public ThemeAdapter(Context context) {
        this(context, null);
    }

    public ThemeAdapter(Context context, ArrayList<ThemeInfo> _list) {
        this.inflater = LayoutInflater.from(context);
        if (_list == null) {
            this.list = new ArrayList<>();
        } else {
            this.list = _list;
        }
    }

    public int getCount() {
        return this.list.size();
    }

    public Object getItem(int position) {
        return this.list.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View getView(int position, View convertView, ViewGroup parent) {
        ThemeInfo info = this.list.get(position);
        if (convertView == null) {
            convertView = this.inflater.inflate((int) R.layout.theme_item, parent, false);
        }
        ((ImageView) convertView.findViewById(R.id.themeIcon)).setImageBitmap(BitmapFactory.decodeFile(String.valueOf(info.path) + File.separator + "icon.png"));
        ((TextView) convertView.findViewById(R.id.themeLabel)).setText(info.path.substring(info.path.lastIndexOf(47) + 1, info.path.lastIndexOf(95)));
        return convertView;
    }

    public ArrayList<ThemeInfo> getList() {
        return this.list;
    }
}
