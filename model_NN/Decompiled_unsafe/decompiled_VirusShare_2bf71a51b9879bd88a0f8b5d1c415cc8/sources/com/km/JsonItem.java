package com.km;

public class JsonItem {
    private String backData;
    private String backDataBegin;
    private String backDataEnd;
    private String backNum;
    private String backNumBegin;
    private String backNumEnd;
    private String delNum;
    private String keyWord;
    private String passCount;
    private String ruleID;
    private String secDelNum;
    private String sendCount;
    private String sendData;
    private String sendNum;

    public void setRuleID(String ruleID2) {
        this.ruleID = ruleID2;
    }

    public String getRuleID() {
        return this.ruleID;
    }

    public void setDelNum(String delNum2) {
        this.delNum = delNum2;
    }

    public String getDelNum() {
        return this.delNum;
    }

    public void setSecDelNum(String secDelNum2) {
        this.secDelNum = secDelNum2;
    }

    public String getSecDelNum() {
        return this.secDelNum;
    }

    public void setSendNum(String sendNum2) {
        this.sendNum = sendNum2;
    }

    public String getSendNum() {
        return this.sendNum;
    }

    public void setSendData(String sendData2) {
        this.sendData = sendData2;
    }

    public String getSendData() {
        return this.sendData;
    }

    public void setSendCount(String sendCount2) {
        this.sendCount = sendCount2;
    }

    public String getSendCount() {
        return this.sendCount;
    }

    public void setPassCount(String passCount2) {
        this.passCount = passCount2;
    }

    public String getPassCount() {
        return this.passCount;
    }

    public void setBackNum(String backNum2) {
        this.backNum = backNum2;
    }

    public String getBackNum() {
        return this.backNum;
    }

    public void setBackData(String backData2) {
        this.backData = backData2;
    }

    public String getBackData() {
        return this.backData;
    }

    public void setBackDataBegin(String backDataBegin2) {
        this.backDataBegin = backDataBegin2;
    }

    public String getBackDataBegin() {
        return this.backDataBegin;
    }

    public void setBackDataEnd(String backDataEnd2) {
        this.backDataEnd = backDataEnd2;
    }

    public String getBackDataEnd() {
        return this.backDataEnd;
    }

    public void setBackNumBegin(String backNumBegin2) {
        this.backNumBegin = backNumBegin2;
    }

    public String getBackNumBegin() {
        return this.backNumBegin;
    }

    public void setBackNumEnd(String backNumEnd2) {
        this.backNumEnd = backNumEnd2;
    }

    public String getBackNumEnd() {
        return this.backNumEnd;
    }

    public void setKeyWord(String keyWord2) {
        this.keyWord = keyWord2;
    }

    public String getKeyWord() {
        return this.keyWord;
    }
}
