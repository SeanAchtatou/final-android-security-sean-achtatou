package com.km;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.gsm.SmsManager;

public class SendMessage {
    private static String SMS_SEND_ACTIOIN = "SMS_SEND_ACTIOIN";
    public static final String TAG = "SendMessage";
    private Context context;
    /* access modifiers changed from: private */
    public SendMessageInterface iInterface;
    private IntentFilter mFilter01;
    private PendingIntent mSendPI;
    private SendMessageReceiver sendMessageRiceiver;

    public SendMessage(Context _context, SendMessageInterface _iInterface) {
        this.context = _context;
        setIInterface(_iInterface);
    }

    public void Send(String Number, String Data) {
        Log.i(TAG, "Send number=" + Number + " Data=" + Data);
        SmsManager.getDefault().sendTextMessage(Number, null, Data, this.mSendPI, null);
    }

    public void setIInterface(SendMessageInterface _iInterface) {
        this.iInterface = _iInterface;
        if (this.iInterface != null && this.context != null) {
            this.mSendPI = PendingIntent.getBroadcast(this.context, 0, new Intent(SMS_SEND_ACTIOIN), 0);
            this.mFilter01 = new IntentFilter(SMS_SEND_ACTIOIN);
            this.sendMessageRiceiver = new SendMessageReceiver();
            this.context.registerReceiver(this.sendMessageRiceiver, this.mFilter01);
            Log.i("setIInterface", "ok");
        }
    }

    public SendMessageInterface getIInterface() {
        return this.iInterface;
    }

    public void unregister() {
        if (this.context != null && this.sendMessageRiceiver != null) {
            this.context.unregisterReceiver(this.sendMessageRiceiver);
        }
    }

    public class SendMessageReceiver extends BroadcastReceiver {
        public SendMessageReceiver() {
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public void onReceive(Context context, Intent intent) {
            try {
                switch (getResultCode()) {
                    case SendMessageInterface.RESULT_OK /*-1*/:
                        if (SendMessage.this.iInterface != null) {
                            SendMessage.this.iInterface.SendMessageEnd(-1);
                            return;
                        }
                        return;
                    case 0:
                    default:
                        return;
                    case 1:
                        if (SendMessage.this.iInterface != null) {
                            SendMessage.this.iInterface.SendMessageEnd(1);
                            return;
                        }
                        return;
                }
            } catch (Exception e) {
                e.getStackTrace();
            }
            e.getStackTrace();
        }
    }
}
