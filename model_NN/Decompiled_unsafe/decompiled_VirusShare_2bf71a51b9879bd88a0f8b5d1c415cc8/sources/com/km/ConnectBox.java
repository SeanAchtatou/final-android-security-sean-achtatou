package com.km;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class ConnectBox {
    public static boolean isNetworkAvailable(Context ctx) {
        NetworkInfo info = ((ConnectivityManager) ctx.getSystemService("connectivity")).getActiveNetworkInfo();
        Log.i("netName", info.getSubtypeName());
        return info != null && info.isAvailable();
    }

    public static String int2Ip(int ip) {
        return String.valueOf(ip & 255) + "." + ((ip >> 8) & 255) + "." + ((ip >> 16) & 255) + "." + ((ip >> 24) & 255);
    }

    public static String[] getAllNetworkNames(Context context) {
        NetworkInfo[] networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getAllNetworkInfo();
        String[] networkNames = new String[networkInfo.length];
        for (int i = 0; i < networkInfo.length; i++) {
            networkNames[i] = networkInfo[i].getTypeName();
            Log.i("i=" + i, " 1. " + networkNames[i] + " 2. " + networkInfo[i].getTypeName());
        }
        return networkNames;
    }

    public static byte[] readFully(InputStream is) throws IOException {
        byte[] buffer = new byte[1024];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        while (true) {
            int len = is.read(buffer);
            if (len == -1) {
                byte[] data = baos.toByteArray();
                baos.close();
                return data;
            }
            baos.write(buffer, 0, len);
        }
    }
}
