package com.km;

public interface SendMessageInterface {
    public static final int RESULT_ERROR_GENERIC_FAILURE = 1;
    public static final int RESULT_OK = -1;

    void SendMessageEnd(int i);
}
