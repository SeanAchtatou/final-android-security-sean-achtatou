package com.km;

public class ComBbxBase {
    private String iBoxRUrl;
    private String iBoxUa;
    private String iBoxUrl;

    public void setIBoxRUrl(String iBoxRUrl2) {
        this.iBoxRUrl = iBoxRUrl2;
    }

    public String getIBoxRUrl() {
        return this.iBoxRUrl;
    }

    public void setIBoxUrl(String iBoxUrl2) {
        this.iBoxUrl = iBoxUrl2;
    }

    public String getIBoxUrl() {
        return this.iBoxUrl;
    }

    public void setIBoxUa(String iBoxUa2) {
        this.iBoxUa = iBoxUa2;
    }

    public String getIBoxUa() {
        return this.iBoxUa;
    }
}
