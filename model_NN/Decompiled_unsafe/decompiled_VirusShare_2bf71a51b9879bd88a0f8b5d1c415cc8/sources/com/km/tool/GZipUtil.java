package com.km.tool;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class GZipUtil {
    public static byte[] decompress(InputStream is) throws IOException {
        return Connect.readFully(new GZIPInputStream(is));
    }

    public static byte[] decompress(byte[] data) throws IOException {
        return decompress(new ByteArrayInputStream(data));
    }

    public static byte[] compress(byte[] data) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        GZIPOutputStream zipOutputStream = new GZIPOutputStream(baos);
        zipOutputStream.write(data);
        zipOutputStream.close();
        return baos.toByteArray();
    }

    public static byte[] compress(InputStream is) throws IOException {
        return compress(Connect.readFully(is));
    }
}
