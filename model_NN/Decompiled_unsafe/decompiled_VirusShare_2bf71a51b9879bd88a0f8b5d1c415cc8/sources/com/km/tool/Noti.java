package com.km.tool;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.util.Log;

public class Noti {
    public static final Uri CONTENT_URI = Uri.withAppendedPath(Uri.parse("content://mms-sms/"), "conversations");
    public static final String NOTIFICATION_ENABLED = "pref_key_enable_notifications";
    public static final String NOTIFICATION_RINGTONE = "pref_key_ringtone";
    public static final String NOTIFICATION_VIBRATE = "pref_key_vibrate";
    private static final String[] REPLACE_PROJECTION = {SmsCreator.KEY_ID, SmsCreator.KEY_ADDRESS, SmsCreator.KEY_PROTOCOL};

    public static void noti(String address, String body, Context context, int iconResourceId, String subject, long threadId, long timeMillis, int count) {
        MmsSmsNotificationInfo info = getNewMessageNotificationInfo(address, body, context, iconResourceId, subject, threadId, timeMillis, count);
        cancelNotification(context, 123);
        info.deliver(context, true, count);
    }

    public static void cancelNotification(Context context, int notificationId) {
        ((NotificationManager) context.getSystemService("notification")).cancel(notificationId);
    }

    /* JADX INFO: Multiple debug info for r4v1 android.app.PendingIntent: [D('clickIntent' android.content.Intent), D('pendingIntent' android.app.PendingIntent)] */
    /* JADX INFO: Multiple debug info for r3v2 android.app.NotificationManager: [D('nm' android.app.NotificationManager), D('context' android.content.Context)] */
    /* JADX INFO: Multiple debug info for r5v4 android.content.Intent: [D('description' java.lang.String), D('multiIntent' android.content.Intent)] */
    /* JADX INFO: Multiple debug info for r5v5 android.app.PendingIntent: [D('multiIntent' android.content.Intent), D('pendingIntent' android.app.PendingIntent)] */
    /* access modifiers changed from: private */
    public static void updateNotification(Context context, Intent clickIntent, String description, int iconRes, boolean isNew, CharSequence ticker, long timeMillis, String title, int count) {
        Uri parse;
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        if (sp.getBoolean("pref_key_enable_notifications", true)) {
            Notification notification = new Notification(iconRes, ticker, timeMillis);
            if (count > 1) {
                Intent multiIntent = getAppIntent();
                multiIntent.setAction("android.intent.action.MAIN");
                multiIntent.setType("vnd.android-dir/mms-sms");
                notification.setLatestEventInfo(context, "New messages", "unread messages.".replaceAll("%s", Integer.toString(count)), PendingIntent.getActivity(context, 0, multiIntent, 0));
            } else {
                notification.setLatestEventInfo(context, title, description, PendingIntent.getActivity(context, 0, clickIntent, 0));
            }
            if (isNew) {
                if (sp.getBoolean("pref_key_vibrate", true)) {
                    notification.defaults |= true;
                }
                String ringtoneStr = sp.getString("pref_key_ringtone", null);
                if (TextUtils.isEmpty(ringtoneStr)) {
                    parse = null;
                } else {
                    parse = Uri.parse(ringtoneStr);
                }
                notification.sound = parse;
            }
            notification.flags |= 1;
            notification.ledARGB = -16711936;
            notification.ledOnMS = 500;
            notification.ledOffMS = 2000;
            ((NotificationManager) context.getSystemService("notification")).notify(123, notification);
        }
    }

    private static final MmsSmsNotificationInfo getNewMessageNotificationInfo(String address, String body, Context context, int iconResourceId, String subject, long threadId, long timeMillis, int count) {
        Intent clickIntent = getAppIntent();
        Log.i("baseUri", clickIntent.getData().toString());
        clickIntent.setData(Uri.withAppendedPath(clickIntent.getData(), Long.toString(threadId)));
        clickIntent.setAction("android.intent.action.VIEW");
        String senderInfo = buildTickerMessage(context, address, null, null).toString();
        return new MmsSmsNotificationInfo(clickIntent, body, iconResourceId, buildTickerMessage(context, address, subject, body), timeMillis, senderInfo.substring(0, senderInfo.length() - 2), count);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    protected static CharSequence buildTickerMessage(Context context, String address, String subject, String body) {
        String replace;
        String displayAddress = address;
        if (displayAddress == null) {
            replace = "";
        } else {
            replace = displayAddress.replace(10, ' ');
        }
        StringBuilder buf = new StringBuilder(replace);
        buf.append(':').append(' ');
        int offset = buf.length();
        if (!TextUtils.isEmpty(subject)) {
            buf.append(subject.replace(10, ' '));
            buf.append(' ');
        }
        if (!TextUtils.isEmpty(body)) {
            buf.append(body.replace(10, ' '));
        }
        SpannableString spanText = new SpannableString(buf.toString());
        spanText.setSpan(new StyleSpan(1), 0, offset, 33);
        return spanText;
    }

    private static final class MmsSmsNotificationInfo {
        public Intent mClickIntent;
        public int mCount;
        public String mDescription;
        public int mIconResourceId;
        public CharSequence mTicker;
        public long mTimeMillis;
        public String mTitle;

        public MmsSmsNotificationInfo(Intent clickIntent, String description, int iconResourceId, CharSequence ticker, long timeMillis, String title, int count) {
            this.mClickIntent = clickIntent;
            this.mDescription = description;
            this.mIconResourceId = iconResourceId;
            this.mTicker = ticker;
            this.mTimeMillis = timeMillis;
            this.mTitle = title;
            this.mCount = count;
        }

        public void deliver(Context context, boolean isNew, int count) {
            Noti.updateNotification(context, this.mClickIntent, this.mDescription, this.mIconResourceId, isNew, this.mTicker, this.mTimeMillis, this.mTitle, count);
        }

        public long getTime() {
            return this.mTimeMillis;
        }
    }

    private static Intent getAppIntent() {
        Intent appIntent = new Intent("android.intent.action.MAIN", CONTENT_URI);
        appIntent.setFlags(872415232);
        return appIntent;
    }

    private Uri insertMessage(Context context, ContentValues values) {
        return replaceMessage(context, values);
    }

    /* JADX INFO: finally extract failed */
    private Uri replaceMessage(Context context, ContentValues values) {
        ContentResolver resolver = context.getContentResolver();
        Cursor cursor = resolver.query(Uri.parse("content://sms/inbox"), REPLACE_PROJECTION, "address = ? AND protocol = ?", new String[]{values.getAsString(SmsCreator.KEY_ADDRESS), values.getAsString(SmsCreator.KEY_PROTOCOL)}, null);
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    Uri messageUri = ContentUris.withAppendedId(Uri.parse("content://sms"), cursor.getLong(0));
                    resolver.update(messageUri, values, null, null);
                    cursor.close();
                    return messageUri;
                }
                cursor.close();
            } catch (Throwable th) {
                cursor.close();
                throw th;
            }
        }
        return storeMessage(context, values);
    }

    private Uri storeMessage(Context context, ContentValues values) {
        return context.getContentResolver().insert(Uri.parse("content://sms/inbox"), values);
    }
}
