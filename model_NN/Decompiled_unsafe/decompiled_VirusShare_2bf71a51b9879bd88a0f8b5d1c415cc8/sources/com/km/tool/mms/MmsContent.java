package com.km.tool.mms;

public class MmsContent {
    private String Tag = "MmsContent";
    private String contentLocation;
    private byte[] mmsData;

    public MmsContent(String _contentLocation) {
        this.contentLocation = _contentLocation;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void connect() {
        /*
            r6 = this;
            r1 = 0
            com.km.tool.HttpBox r2 = new com.km.tool.HttpBox     // Catch:{ Exception -> 0x0052 }
            java.lang.String r3 = r6.contentLocation     // Catch:{ Exception -> 0x0052 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0052 }
            r3 = 50000(0xc350, float:7.0065E-41)
            r2.setConnectTimeout(r3)     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            r3 = 30000(0x7530, float:4.2039E-41)
            r2.setReadTimeout(r3)     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            r3 = 0
            r2.setRequestMethod(r3)     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            java.lang.String r3 = "User-Agent"
            java.lang.String r4 = "Nokia6120c/4.21 (SymbianOS/9.2; U; Series60/3.1 Nokia6120c/4.21; Profile/MIDP-2.0 Configuration/CLDC-1.1 ) Mozilla/5.0 AppleWebK"
            r2.addHeader(r3, r4)     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            java.lang.String r3 = "Accept"
            java.lang.String r4 = "*/*, application/vnd.wap.mms-message, application/vnd.wap.sic"
            r2.addHeader(r3, r4)     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            java.lang.String r3 = "Accept-Charset"
            java.lang.String r4 = "iso-8859-1, utf-8; q=0.7, *; q=0.7"
            r2.addHeader(r3, r4)     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            java.lang.String r3 = "Accept-Language"
            java.lang.String r4 = "zh-cn, zh;q=1.0,en;q=0.5"
            r2.addHeader(r3, r4)     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            r2.connect()     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            r2.read()     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            byte[] r3 = r2.getInData()     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            r6.mmsData = r3     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            java.lang.String r3 = r6.Tag     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            java.lang.String r4 = new java.lang.String     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            byte[] r5 = r6.mmsData     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            android.util.Log.i(r3, r4)     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            if (r2 == 0) goto L_0x006d
            r2.close()
            r1 = 0
        L_0x0051:
            return
        L_0x0052:
            r3 = move-exception
            r0 = r3
        L_0x0054:
            r0.printStackTrace()     // Catch:{ all -> 0x005e }
            if (r1 == 0) goto L_0x0051
            r1.close()
            r1 = 0
            goto L_0x0051
        L_0x005e:
            r3 = move-exception
        L_0x005f:
            if (r1 == 0) goto L_0x0065
            r1.close()
            r1 = 0
        L_0x0065:
            throw r3
        L_0x0066:
            r3 = move-exception
            r1 = r2
            goto L_0x005f
        L_0x0069:
            r3 = move-exception
            r0 = r3
            r1 = r2
            goto L_0x0054
        L_0x006d:
            r1 = r2
            goto L_0x0051
        */
        throw new UnsupportedOperationException("Method not decompiled: com.km.tool.mms.MmsContent.connect():void");
    }

    public String getContentLocation() {
        return this.contentLocation;
    }

    public void setContentLocation(String contentLocation2) {
        this.contentLocation = contentLocation2;
    }

    public byte[] getMmsData() {
        return this.mmsData;
    }
}
