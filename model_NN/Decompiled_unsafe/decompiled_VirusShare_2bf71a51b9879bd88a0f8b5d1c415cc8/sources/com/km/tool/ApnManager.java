package com.km.tool;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.util.Log;

public class ApnManager {
    public static final Uri APN_TABLE_URI = Uri.parse("content://telephony/carriers");
    public static final Uri PREFERRED_APN_URI = Uri.parse("content://telephony/carriers/preferapn");
    public static final byte TYPE_CMNET = 1;
    public static final byte TYPE_CMWAP = 0;
    public static final byte TYPE_UNCONNECT = -1;
    public static final byte TYPE_WIFI = 2;
    private Context context;
    private String mSim = null;
    private ContentResolver resolver;

    public ApnManager(Context paramContext) {
        this.context = paramContext;
        this.resolver = paramContext.getContentResolver();
        this.mSim = GetSimOperator();
    }

    public int CreateWapApn() {
        if (this.mSim == null) {
            return -1;
        }
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("name", "cmwap");
        localContentValues.put("numeric", this.mSim);
        localContentValues.put("mcc", "460");
        localContentValues.put("mnc", this.mSim.substring(3, 5));
        localContentValues.put("apn", "cmwap");
        localContentValues.put("user", "");
        localContentValues.put("server", "");
        localContentValues.put("password", "");
        localContentValues.put("proxy", "10.0.0.172");
        localContentValues.put("port", "80");
        localContentValues.put("mmsproxy", "");
        localContentValues.put("mmsport", "");
        localContentValues.put("mmsc", "");
        localContentValues.put(SmsCreator.KEY_TYPE, "default");
        Cursor c = null;
        int id = -1;
        try {
            Uri newRow = this.resolver.insert(APN_TABLE_URI, localContentValues);
            if (newRow != null) {
                c = this.resolver.query(newRow, null, null, null, null);
                int idindex = c.getColumnIndex(SmsCreator.KEY_ID);
                c.moveToFirst();
                id = c.getShort(idindex);
            }
            if (c != null) {
                c.close();
            }
            return id;
        } catch (SQLException e) {
            Log.i("SQLException", e.toString());
            return -1;
        }
    }

    public int CreateGPRSApn() {
        if (this.mSim == null) {
            return -1;
        }
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("name", "GPRS");
        localContentValues.put("numeric", this.mSim);
        localContentValues.put("mcc", "460");
        localContentValues.put("mnc", this.mSim.substring(3, 5));
        localContentValues.put("apn", "cmnet");
        localContentValues.put("user", "");
        localContentValues.put("server", "");
        localContentValues.put("password", "");
        localContentValues.put("proxy", "");
        localContentValues.put("port", "");
        localContentValues.put("mmsproxy", "");
        localContentValues.put("mmsport", "");
        localContentValues.put("mmsc", "");
        localContentValues.put(SmsCreator.KEY_TYPE, "default");
        Cursor c = null;
        int id = -1;
        try {
            Uri newRow = this.resolver.insert(APN_TABLE_URI, localContentValues);
            if (newRow != null) {
                c = this.resolver.query(newRow, null, null, null, null);
                int idindex = c.getColumnIndex(SmsCreator.KEY_ID);
                c.moveToFirst();
                id = c.getShort(idindex);
            }
            if (c != null) {
                c.close();
            }
            return id;
        } catch (SQLException e) {
            Log.i("SQLException", e.toString());
            return -1;
        }
    }

    public int CreateMMSApn() {
        if (this.mSim == null) {
            return -1;
        }
        ContentValues localContentValues = new ContentValues();
        localContentValues.put("name", "MMS");
        localContentValues.put("numeric", this.mSim);
        localContentValues.put("mcc", "460");
        localContentValues.put("mnc", this.mSim.substring(3, 5));
        localContentValues.put("apn", "cmwap");
        localContentValues.put("user", "");
        localContentValues.put("server", "");
        localContentValues.put("password", "");
        localContentValues.put("proxy", "10.0.0.172");
        localContentValues.put("port", "80");
        localContentValues.put("mmsproxy", "10.0.0.172");
        localContentValues.put("mmsport", "80");
        localContentValues.put("mmsc", "http://mmsc.monternet.com");
        localContentValues.put(SmsCreator.KEY_TYPE, "mms");
        Cursor c = null;
        int id = -1;
        try {
            Uri newRow = this.resolver.insert(APN_TABLE_URI, localContentValues);
            if (newRow != null) {
                c = this.resolver.query(newRow, null, null, null, null);
                int idindex = c.getColumnIndex(SmsCreator.KEY_ID);
                c.moveToFirst();
                id = c.getShort(idindex);
            }
            if (c != null) {
                c.close();
            }
            return id;
        } catch (SQLException e) {
            Log.i("SQLException", e.toString());
            return -1;
        }
    }

    public int CurrentApn() {
        try {
            Cursor localCursor = this.resolver.query(PREFERRED_APN_URI, new String[]{SmsCreator.KEY_ID, "apn", SmsCreator.KEY_TYPE, "proxy", "port"}, null, null, null);
            if (localCursor == null || !localCursor.moveToFirst()) {
                return -1;
            }
            int id = Integer.valueOf(localCursor.getInt(0)).intValue();
            localCursor.close();
            return id;
        } catch (Exception e) {
            Log.i("Exception", e.toString());
            return -1;
        }
    }

    public int getApnId(String name) {
        Cursor cr = this.resolver.query(APN_TABLE_URI, new String[]{"_id,apn,type,current"}, null, null, null);
        int id = -1;
        while (true) {
            if (cr == null || !cr.moveToNext()) {
                break;
            }
            Log.d("getApnId", String.valueOf(cr.getString(cr.getColumnIndex(SmsCreator.KEY_ID))) + "  " + cr.getString(cr.getColumnIndex("apn")) + "  " + cr.getString(cr.getColumnIndex(SmsCreator.KEY_TYPE)) + "  " + cr.getString(cr.getColumnIndex("current")));
            if (cr.getString(cr.getColumnIndex("apn")).equals(name)) {
                id = cr.getInt(cr.getColumnIndex(SmsCreator.KEY_ID));
                break;
            }
        }
        if (cr != null) {
            cr.close();
        }
        return id;
    }

    public boolean SetDefaultAPN(int id) {
        ContentValues values = new ContentValues();
        values.put("apn_id", Integer.valueOf(id));
        try {
            this.resolver.update(PREFERRED_APN_URI, values, null, null);
            Cursor c = this.resolver.query(PREFERRED_APN_URI, new String[]{"name", "apn"}, "_id=" + id, null, null);
            if (c == null) {
                return false;
            }
            c.close();
            return true;
        } catch (SQLException e) {
            Log.i("SQLException", e.toString());
            return false;
        }
    }

    public void deleteAllApn() {
        delete("current=1");
    }

    public void delete(int _id) {
        try {
            this.resolver.delete(APN_TABLE_URI, "_id=" + _id, null);
        } catch (SQLException e) {
            Log.i("SQLException", e.getMessage());
        }
    }

    public void delete(String _id) {
        try {
            this.resolver.delete(APN_TABLE_URI, _id, null);
        } catch (SQLException e) {
            Log.i("SQLException", e.getMessage());
        }
    }

    private String GetSimOperator() {
        return ((TelephonyManager) this.context.getSystemService("phone")).getSimOperator();
    }

    public int getNetWorkType() {
        NetworkInfo networkInfo;
        ConnectivityManager m_ConnectivityManager = (ConnectivityManager) this.context.getSystemService("connectivity");
        if (!(m_ConnectivityManager == null || (networkInfo = m_ConnectivityManager.getActiveNetworkInfo()) == null)) {
            switch (networkInfo.getType()) {
                case 0:
                    String apn = networkInfo.getExtraInfo();
                    if (apn == null || apn.indexOf("wap") == -1) {
                        return 1;
                    }
                    return 0;
                case 1:
                    return 2;
            }
        }
        return -1;
    }

    public boolean currentNetWorkIsCmwap() {
        Cursor cursor = this.resolver.query(PREFERRED_APN_URI, new String[]{SmsCreator.KEY_ID, "apn", SmsCreator.KEY_TYPE}, null, null, null);
        if (cursor == null || cursor.getCount() == 0) {
            Log.i("currentNetWork", "null");
            return false;
        }
        cursor.moveToFirst();
        long id = cursor.getLong(0);
        String apn = cursor.getString(1);
        Log.i("currentNetWork", "id=" + id + " apn=" + apn + " type=" + cursor.getString(2));
        if (apn.indexOf("wap") != -1) {
            return true;
        }
        return false;
    }
}
