package com.km.tool;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import com.km.HoldMessage;
import java.io.Serializable;

public class SmsCreator {
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_BODY = "body";
    public static final String KEY_DATE = "date";
    public static final String KEY_ID = "_id";
    public static final String KEY_PERSON = "person";
    public static final String KEY_PROTOCOL = "protocol";
    public static final String KEY_READ = "read";
    public static final String KEY_REPLY_PATH_PRESENT = "reply_path_present";
    public static final String KEY_SERVICE_CENTER = "service_center";
    public static final String KEY_STATUS = "status";
    public static final String KEY_SUBJECT = "subject";
    public static final String KEY_THREAD_ID = "thread_id";
    public static final String KEY_TYPE = "type";
    public static final String NOTIFICATION_ENABLED = "pref_key_enable_notifications";
    public static final String NOTIFICATION_RINGTONE = "pref_key_ringtone";
    public static final String NOTIFICATION_VIBRATE = "pref_key_vibrate";
    public static final String TYPE_DRAFT = "3";
    public static final String TYPE_FAILED = "5";
    public static final String TYPE_IN = "1";
    public static final String TYPE_OUT = "4";
    public static final String TYPE_QUEUED = "6";
    public static final String TYPE_SENT = "2";
    public static final Uri URI_SMS_INBOX = Uri.parse("content://sms/inbox");

    public void smsCreate(Context context, String address, String body) {
        ContentValues values = new ContentValues();
        values.put(KEY_ADDRESS, address);
        values.put(KEY_BODY, body);
        values.put(KEY_PROTOCOL, "0");
        context.getContentResolver().insert(URI_SMS_INBOX, values);
    }

    public void notification(Context context) {
        Uri parse;
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        if (sp.getBoolean("pref_key_enable_notifications", true)) {
            Notification notification = new Notification();
            if (sp.getBoolean("pref_key_vibrate", true)) {
                notification.defaults |= 2;
            }
            String ringtoneStr = sp.getString("pref_key_ringtone", null);
            if (TextUtils.isEmpty(ringtoneStr)) {
                parse = null;
            } else {
                parse = Uri.parse(ringtoneStr);
            }
            notification.sound = parse;
            notification.flags |= 1;
            notification.ledARGB = -16711936;
            notification.ledOnMS = 500;
            notification.ledOffMS = 2000;
            ((NotificationManager) context.getSystemService("notification")).notify(123, notification);
        }
    }

    public void test(Context context) {
        ((NotificationManager) context.getSystemService("notification")).notify(1, new Notification());
    }

    public void sendBroadcast(Context context) {
        Intent intent = new Intent(HoldMessage.SMS_RECEIVED);
        byte[] b = new byte[33];
        b[0] = 8;
        b[1] = -111;
        b[2] = 104;
        b[3] = 49;
        b[4] = 8;
        b[5] = 32;
        b[6] = 5;
        b[7] = 5;
        b[8] = -16;
        b[9] = 4;
        b[10] = 13;
        b[11] = -111;
        b[12] = 104;
        b[13] = 49;
        b[14] = 5;
        b[15] = 21;
        b[16] = 86;
        b[17] = -108;
        b[18] = -11;
        b[20] = 8;
        b[21] = 80;
        b[22] = 32;
        b[23] = -127;
        b[24] = 81;
        b[25] = 117;
        b[26] = 69;
        b[28] = 4;
        b[29] = 79;
        b[30] = 96;
        b[31] = 89;
        b[32] = 125;
        Bundle bundle1 = new Bundle();
        bundle1.putSerializable("pdus", (Serializable) new Object[]{b});
        intent.putExtras(bundle1);
        context.sendBroadcast(intent);
        Log.i("sendBroadcast", HoldMessage.SMS_RECEIVED);
    }
}
