package com.km.tool;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

public class Http {
    /* access modifiers changed from: private */
    public boolean cmwap;
    private int connectTimeout;
    private int contentLenght;
    private String contentType;
    private Context context;
    private HashMap<String, String> header;
    private HttpURLConnection http;
    private byte[] inData;
    private byte[] outData;
    private boolean post;
    private String postContentType;
    private int readTimeout;
    private ConnectivityReceiver receiver;
    private int responseCode;
    private URL url;

    public Http(Context _context) {
        this.connectTimeout = 30000;
        this.readTimeout = 30000;
        this.context = _context;
    }

    public Http(Context _context, String _url) throws MalformedURLException {
        this.connectTimeout = 30000;
        this.readTimeout = 30000;
        this.context = _context;
        setUrl(_url);
    }

    public Http(Context _context, String _url, byte[] params) throws MalformedURLException {
        this(_context, _url, params, "application/x-www-form-urlencoded");
    }

    public Http(Context _context, String _url, String params) throws MalformedURLException {
        this.connectTimeout = 30000;
        this.readTimeout = 30000;
        this.context = _context;
        setUrl(_url);
        if (params != null) {
            try {
                postData(URLEncoder.encode(params, "UTF-8").getBytes(), "application/x-www-form-urlencoded");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    public Http(Context _context, String _url, byte[] _data, String _contentType) throws MalformedURLException {
        this.connectTimeout = 30000;
        this.readTimeout = 30000;
        this.context = _context;
        setUrl(_url);
        postData(_data, _contentType);
    }

    private boolean apnChangedCheck() {
        ApnManager apn = new ApnManager(this.context);
        int netType = apn.getNetWorkType();
        int id = -1;
        Log.i("netType", new StringBuilder().append(netType).toString());
        if (netType == -1) {
            id = this.cmwap ? apn.CreateWapApn() : apn.CreateGPRSApn();
        } else if (netType != 0 && this.cmwap) {
            int id2 = apn.getApnId("cmwap");
            if (id2 != -1) {
                apn.delete(id2);
            }
            id = apn.CreateWapApn();
        } else if (netType == 0 && !this.cmwap) {
            int id3 = apn.getApnId("cmnet");
            if (id3 != -1) {
                apn.delete(id3);
            }
            id = apn.CreateGPRSApn();
        }
        if (id != -1) {
            return apn.SetDefaultAPN(id);
        }
        return false;
    }

    public void connect() throws IOException {
        if (apnChangedCheck()) {
            registerListen();
            Log.i("synchronized", "wait");
            synchronized (this) {
                try {
                    wait(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            Log.i("synchronized", "notify");
            unregisterListen();
        }
        connectOperate();
        if (this.cmwap && !this.post && this.contentType != null && this.contentType.indexOf("text/vnd.wap.wml") != -1) {
            Log.i("cmwap", "reConnect");
            this.http.disconnect();
            this.http = null;
            this.responseCode = 0;
            this.contentLenght = 0;
            this.contentType = null;
            connectOperate();
        }
    }

    private void connectOperate() throws IOException {
        URL _url = this.url;
        if (this.cmwap) {
            String host = this.url.getHost();
            _url = new URL(this.url.toString().replace(host, "10.0.0.172"));
            addHeader("X-Online-Host", host);
        }
        this.http = (HttpURLConnection) _url.openConnection();
        setHeader();
        this.http.setConnectTimeout(this.connectTimeout);
        this.http.setReadTimeout(this.readTimeout);
        if (this.post) {
            this.http.setRequestMethod("POST");
            write();
        } else {
            this.http.setRequestMethod("GET");
        }
        this.responseCode = this.http.getResponseCode();
        this.contentLenght = this.http.getContentLength();
        this.contentType = this.http.getContentType();
    }

    public void read() throws IOException {
        InputStream is = this.http.getInputStream();
        this.inData = Connect.readFully(is);
        is.close();
    }

    private void setHeader() {
        if (this.header != null && this.header.size() > 0) {
            String[] keys = new String[this.header.size()];
            this.header.keySet().toArray(keys);
            for (int i = 0; i < keys.length; i++) {
                this.http.setRequestProperty(keys[i], this.header.get(keys[i]));
            }
        }
    }

    private void write() throws IOException {
        if (this.outData != null) {
            this.http.setDoOutput(true);
            OutputStream os = this.http.getOutputStream();
            os.write(this.outData);
            os.flush();
            os.close();
        }
    }

    public void setUrl(String _url) throws MalformedURLException {
        this.url = new URL(_url);
    }

    public void setRequestMethod(boolean _post) {
        this.post = _post;
    }

    public void postData(byte[] _data, String _contentType) {
        this.outData = _data;
        if (_data != null) {
            this.outData = _data;
            if (_contentType == null || _contentType.trim().equals("")) {
                this.postContentType = "application/octet-stream";
            } else {
                this.postContentType = _contentType;
            }
            this.post = true;
            addHeader("Content-Type", this.postContentType);
            addHeader("Content-Length", String.valueOf(this.outData.length));
            return;
        }
        this.post = false;
    }

    public void addHeader(String key, String value) {
        if (this.header == null) {
            this.header = new HashMap<>();
        }
        this.header.put(key, value);
    }

    public void close() {
        if (this.http != null) {
            this.http.disconnect();
            this.http = null;
        }
        if (this.header != null) {
            this.header.clear();
            this.header = null;
        }
        this.url = null;
        this.outData = null;
        this.postContentType = null;
        this.inData = null;
    }

    public int getConnectTimeout() {
        return this.connectTimeout;
    }

    public void setConnectTimeout(int _connectTimeout) {
        this.connectTimeout = _connectTimeout;
    }

    public int getReadTimeout() {
        return this.readTimeout;
    }

    public void setReadTimeout(int _readTimeout) {
        this.readTimeout = _readTimeout;
    }

    public HttpURLConnection getHttp() {
        return this.http;
    }

    public int getContentLenght() {
        return this.contentLenght;
    }

    public byte[] getInData() {
        return this.inData;
    }

    public int getResponseCode() {
        return this.responseCode;
    }

    public String getContentType() {
        return this.contentType;
    }

    public boolean isCmwap() {
        return this.cmwap;
    }

    public void setCmwap(boolean cmwap2) {
        this.cmwap = cmwap2;
    }

    private void registerListen() {
        IntentFilter mFilter01 = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        this.receiver = new ConnectivityReceiver();
        this.context.registerReceiver(this.receiver, mFilter01);
    }

    private void unregisterListen() {
        this.context.unregisterReceiver(this.receiver);
    }

    public class ConnectivityReceiver extends BroadcastReceiver {
        public ConnectivityReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            String extraInfo;
            NetworkInfo info = (NetworkInfo) intent.getExtras().get("networkInfo");
            Log.w("ConnectivityReceiver", info.toString());
            if (info.getState() == NetworkInfo.State.CONNECTED && (extraInfo = info.getExtraInfo()) != null) {
                if ((Http.this.cmwap && extraInfo.indexOf("cmwap") != -1) || (!Http.this.cmwap && extraInfo.indexOf("cmnet") != -1)) {
                    synchronized (Http.this) {
                        Http.this.notify();
                    }
                }
            }
        }
    }
}
