package com.km.tool;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

public class SmsMmsManager {
    public static final byte ALL = 0;
    public static final byte READ = 1;
    public static final byte UNREAD = 2;

    /* JADX INFO: Multiple debug info for r4v3 int: [D('id' int), D('numberID' int)] */
    /* JADX INFO: Multiple debug info for r2v3 int: [D('id' int), D('bodyID' int)] */
    public static void smsDel(Context context, String[] delAdds) {
        int readID;
        int protocolID;
        int numberID;
        int numberID2;
        int bodyID;
        String string;
        ContentResolver contentResolver = context.getContentResolver();
        Cursor cursor = contentResolver.query(Uri.parse("content://sms/inbox"), null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                String[] s = cursor.getColumnNames();
                int id = 0;
                int numberID3 = 0;
                int protocolID2 = 0;
                int bodyID2 = 0;
                int numberID4 = 0;
                Log.i("s", "len=" + s.length);
                int i = 0;
                while (true) {
                    readID = numberID4;
                    protocolID = protocolID2;
                    numberID = id;
                    numberID2 = bodyID2;
                    bodyID = numberID3;
                    if (i >= s.length) {
                        break;
                    }
                    if (SmsCreator.KEY_ADDRESS.equals(s[i])) {
                        numberID4 = readID;
                        protocolID2 = protocolID;
                        int id2 = numberID2;
                        id = i;
                        numberID3 = bodyID;
                        bodyID2 = id2;
                    } else if (SmsCreator.KEY_BODY.equals(s[i])) {
                        numberID3 = i;
                        bodyID2 = numberID2;
                        protocolID2 = protocolID;
                        id = numberID;
                        numberID4 = readID;
                    } else if (SmsCreator.KEY_PROTOCOL.equals(s[i])) {
                        protocolID2 = i;
                        numberID3 = bodyID;
                        bodyID2 = numberID2;
                        id = numberID;
                        numberID4 = readID;
                    } else if (SmsCreator.KEY_ID.equals(s[i])) {
                        protocolID2 = protocolID;
                        id = numberID;
                        numberID4 = readID;
                        int bodyID3 = bodyID;
                        bodyID2 = i;
                        numberID3 = bodyID3;
                    } else if (SmsCreator.KEY_READ.equals(s[i])) {
                        protocolID2 = protocolID;
                        int id3 = numberID2;
                        id = numberID;
                        numberID4 = i;
                        numberID3 = bodyID;
                        bodyID2 = id3;
                    } else {
                        protocolID2 = protocolID;
                        numberID3 = bodyID;
                        bodyID2 = numberID2;
                        id = numberID;
                        numberID4 = readID;
                    }
                    i++;
                }
                while (true) {
                    for (int i2 = 0; i2 < s.length; i2++) {
                        String str = s[i2];
                        if (cursor.getString(i2) == null) {
                            string = "null";
                        } else {
                            string = cursor.getString(i2);
                        }
                        Log.i(str, string);
                    }
                    Log.i("---", "-----------");
                    String number = cursor.getString(numberID);
                    String protocol = cursor.getString(protocolID);
                    String body = cursor.getString(bodyID);
                    String _id = cursor.getString(numberID2);
                    String read = cursor.getString(readID);
                    Log.i("number", number);
                    Log.i(SmsCreator.KEY_PROTOCOL, protocol);
                    Log.i(SmsCreator.KEY_BODY, body);
                    Log.i(SmsCreator.KEY_ID, _id);
                    Log.i(SmsCreator.KEY_READ, read);
                    if ("0".equals(read) && isDel(number, body, delAdds)) {
                        Log.i("delete", "id=" + _id);
                        contentResolver.delete(Uri.parse("content://sms/" + _id), null, null);
                    }
                    if (!cursor.isLast()) {
                        cursor.moveToNext();
                    } else {
                        return;
                    }
                }
            }
        }
    }

    private static boolean isDel(String number, String body, String[] delAdds) {
        return false;
    }

    public static void mmsDel(Context context, String[] delAdds, int readState) {
        String string;
        ContentResolver contentResolver = context.getContentResolver();
        Cursor cursor = contentResolver.query(Uri.parse("content://mms/"), null, null, null, null);
        if (cursor != null) {
            String[] s = cursor.getColumnNames();
            Log.i("--------", "count=" + cursor.getCount());
            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                while (true) {
                    boolean del = false;
                    boolean unread = false;
                    String id = "";
                    for (int i = 0; i < s.length; i++) {
                        String str = s[i];
                        if (cursor.getString(i) == null) {
                            string = "null";
                        } else {
                            string = cursor.getString(i);
                        }
                        Log.i(str, string);
                        if (s[i].equals(SmsCreator.KEY_ID)) {
                            id = cursor.getString(i);
                        } else if (s[i].equals(SmsCreator.KEY_READ)) {
                            unread = cursor.getString(i).equals("0");
                        }
                    }
                    if (readState == 0 || ((readState == 2 && unread) || (readState == 1 && !unread))) {
                        del = mmsAddress(context, delAdds, id);
                    }
                    Log.i("---", "-----------");
                    if (del) {
                        contentResolver.delete(Uri.parse("content://mms/" + id + "/addr"), null, null);
                        contentResolver.delete(Uri.parse("content://mms/" + id), null, null);
                        Log.w("delete", "id=" + id);
                    }
                    if (!cursor.isLast()) {
                        cursor.moveToNext();
                    } else {
                        return;
                    }
                }
            }
        }
    }

    private static boolean mmsAddress(Context context, String[] delAdds, String _id) {
        String string;
        Log.i("mmsAddress", "here");
        Cursor cursor = context.getContentResolver().query(Uri.parse("content://mms/" + _id + "/addr"), null, null, null, null);
        if (cursor != null) {
            String[] s = cursor.getColumnNames();
            Log.i("--------", "count=" + cursor.getCount());
            if (cursor.getCount() <= 0) {
                return false;
            }
            cursor.moveToFirst();
            while (true) {
                String address = "";
                int i = 0;
                while (true) {
                    if (i >= s.length) {
                        break;
                    }
                    String str = s[i];
                    if (cursor.getString(i) == null) {
                        string = "null";
                    } else {
                        string = cursor.getString(i);
                    }
                    Log.i(str, string);
                    if (s[i].equals(SmsCreator.KEY_ADDRESS)) {
                        address = cursor.getString(i);
                        break;
                    }
                    i++;
                }
                String number = Util.phoneNumberModify(address);
                if (delAdds != null && delAdds.length != 0) {
                    for (String equals : delAdds) {
                        if (number.equals(equals)) {
                            return true;
                        }
                    }
                    Log.i("---", "-----------");
                    if (cursor.isLast()) {
                        break;
                    }
                    cursor.moveToNext();
                }
            }
            return true;
        }
        return false;
    }

    public static void printSQL(Context context, Uri uri) {
        String string;
        Log.i("uri", uri.toString());
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        if (cursor != null) {
            String[] s = cursor.getColumnNames();
            for (int i = 0; i < s.length; i++) {
                Log.i("i=" + i, s[i]);
            }
            Log.i("--------", "count=" + cursor.getCount());
            cursor.moveToFirst();
            if (cursor.getCount() > 0) {
                while (true) {
                    for (int i2 = 0; i2 < s.length; i2++) {
                        String str = s[i2];
                        if (cursor.getString(i2) == null) {
                            string = "null";
                        } else {
                            string = cursor.getString(i2);
                        }
                        Log.i(str, string);
                    }
                    Log.i("---", "-----------");
                    if (!cursor.isLast()) {
                        cursor.moveToNext();
                    } else {
                        return;
                    }
                }
            }
        }
    }
}
