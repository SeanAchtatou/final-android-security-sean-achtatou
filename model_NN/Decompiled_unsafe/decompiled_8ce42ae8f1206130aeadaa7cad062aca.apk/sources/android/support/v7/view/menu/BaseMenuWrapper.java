package android.support.v7.view.menu;

import android.content.Context;
import android.support.v4.internal.view.SupportMenuItem;
import android.support.v4.internal.view.SupportSubMenu;
import android.support.v4.util.ArrayMap;
import android.view.MenuItem;
import android.view.SubMenu;
import java.util.Iterator;
import java.util.Map;

abstract class BaseMenuWrapper<T> extends BaseWrapper<T> {
    final Context mContext;
    private Map<SupportMenuItem, MenuItem> mMenuItems;
    private Map<SupportSubMenu, SubMenu> mSubMenus;

    BaseMenuWrapper(Context context, T t) {
        super(t);
        this.mContext = context;
    }

    /* access modifiers changed from: package-private */
    public final MenuItem getMenuItemWrapper(MenuItem menuItem) {
        Map<SupportMenuItem, MenuItem> map;
        MenuItem menuItem2 = menuItem;
        if (!(menuItem2 instanceof SupportMenuItem)) {
            return menuItem2;
        }
        SupportMenuItem supportMenuItem = (SupportMenuItem) menuItem2;
        if (this.mMenuItems == null) {
            new ArrayMap();
            this.mMenuItems = map;
        }
        MenuItem menuItem3 = this.mMenuItems.get(menuItem2);
        if (null == menuItem3) {
            menuItem3 = MenuWrapperFactory.wrapSupportMenuItem(this.mContext, supportMenuItem);
            MenuItem put = this.mMenuItems.put(supportMenuItem, menuItem3);
        }
        return menuItem3;
    }

    /* access modifiers changed from: package-private */
    public final SubMenu getSubMenuWrapper(SubMenu subMenu) {
        Map<SupportSubMenu, SubMenu> map;
        SubMenu subMenu2 = subMenu;
        if (!(subMenu2 instanceof SupportSubMenu)) {
            return subMenu2;
        }
        SupportSubMenu supportSubMenu = (SupportSubMenu) subMenu2;
        if (this.mSubMenus == null) {
            new ArrayMap();
            this.mSubMenus = map;
        }
        SubMenu subMenu3 = this.mSubMenus.get(supportSubMenu);
        if (null == subMenu3) {
            subMenu3 = MenuWrapperFactory.wrapSupportSubMenu(this.mContext, supportSubMenu);
            SubMenu put = this.mSubMenus.put(supportSubMenu, subMenu3);
        }
        return subMenu3;
    }

    /* access modifiers changed from: package-private */
    public final void internalClear() {
        if (this.mMenuItems != null) {
            this.mMenuItems.clear();
        }
        if (this.mSubMenus != null) {
            this.mSubMenus.clear();
        }
    }

    /* access modifiers changed from: package-private */
    public final void internalRemoveGroup(int i) {
        int i2 = i;
        if (this.mMenuItems != null) {
            Iterator<SupportMenuItem> it = this.mMenuItems.keySet().iterator();
            while (it.hasNext()) {
                if (i2 == it.next().getGroupId()) {
                    it.remove();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void internalRemoveItem(int i) {
        int i2 = i;
        if (this.mMenuItems != null) {
            Iterator<SupportMenuItem> it = this.mMenuItems.keySet().iterator();
            while (it.hasNext()) {
                if (i2 == it.next().getItemId()) {
                    it.remove();
                    return;
                }
            }
        }
    }
}
