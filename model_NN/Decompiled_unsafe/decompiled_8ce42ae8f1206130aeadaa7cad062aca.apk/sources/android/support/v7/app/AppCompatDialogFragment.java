package android.support.v7.app;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class AppCompatDialogFragment extends DialogFragment {
    public Dialog onCreateDialog(Bundle bundle) {
        Dialog dialog;
        new AppCompatDialog(getActivity(), getTheme());
        return dialog;
    }

    public void setupDialog(Dialog dialog, int i) {
        Dialog dialog2 = dialog;
        int i2 = i;
        if (dialog2 instanceof AppCompatDialog) {
            AppCompatDialog appCompatDialog = (AppCompatDialog) dialog2;
            switch (i2) {
                case 1:
                case 2:
                    break;
                default:
                    return;
                case 3:
                    dialog2.getWindow().addFlags(24);
                    break;
            }
            boolean supportRequestWindowFeature = appCompatDialog.supportRequestWindowFeature(1);
            return;
        }
        super.setupDialog(dialog2, i2);
    }
}
