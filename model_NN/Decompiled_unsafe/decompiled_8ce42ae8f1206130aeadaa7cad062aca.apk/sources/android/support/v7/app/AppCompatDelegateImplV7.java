package android.support.v7.app;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v4.app.NavUtils;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v4.view.LayoutInflaterFactory;
import android.support.v4.view.OnApplyWindowInsetsListener;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewConfigurationCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.support.v4.view.WindowInsetsCompat;
import android.support.v4.widget.PopupWindowCompat;
import android.support.v7.appcompat.R;
import android.support.v7.view.ActionMode;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.view.StandaloneActionMode;
import android.support.v7.view.menu.ListMenuPresenter;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPresenter;
import android.support.v7.view.menu.MenuView;
import android.support.v7.widget.ActionBarContextView;
import android.support.v7.widget.ContentFrameLayout;
import android.support.v7.widget.DecorContentParent;
import android.support.v7.widget.FitWindowsViewGroup;
import android.support.v7.widget.TintManager;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.ViewStubCompat;
import android.support.v7.widget.ViewUtils;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

class AppCompatDelegateImplV7 extends AppCompatDelegateImplBase implements MenuBuilder.Callback, LayoutInflaterFactory {
    private ActionMenuPresenterCallback mActionMenuPresenterCallback;
    ActionMode mActionMode;
    PopupWindow mActionModePopup;
    ActionBarContextView mActionModeView;
    private AppCompatViewInflater mAppCompatViewInflater;
    private boolean mClosingActionMenu;
    private DecorContentParent mDecorContentParent;
    private boolean mEnableDefaultActionBarUp;
    ViewPropertyAnimatorCompat mFadeAnim = null;
    private boolean mFeatureIndeterminateProgress;
    private boolean mFeatureProgress;
    /* access modifiers changed from: private */
    public int mInvalidatePanelMenuFeatures;
    private boolean mInvalidatePanelMenuPosted;
    private final Runnable mInvalidatePanelMenuRunnable;
    private boolean mLongPressBackDown;
    private PanelMenuPresenterCallback mPanelMenuPresenterCallback;
    private PanelFeatureState[] mPanels;
    private PanelFeatureState mPreparedPanel;
    Runnable mShowActionModePopup;
    private View mStatusGuard;
    private ViewGroup mSubDecor;
    private boolean mSubDecorInstalled;
    private Rect mTempRect1;
    private Rect mTempRect2;
    private TextView mTitleView;
    private ViewGroup mWindowDecor;

    static /* synthetic */ int access$002(AppCompatDelegateImplV7 appCompatDelegateImplV7, int i) {
        int i2 = i;
        int i3 = i2;
        appCompatDelegateImplV7.mInvalidatePanelMenuFeatures = i3;
        return i2;
    }

    static /* synthetic */ boolean access$202(AppCompatDelegateImplV7 appCompatDelegateImplV7, boolean z) {
        boolean z2 = z;
        boolean z3 = z2;
        appCompatDelegateImplV7.mInvalidatePanelMenuPosted = z3;
        return z2;
    }

    AppCompatDelegateImplV7(Context context, Window window, AppCompatCallback appCompatCallback) {
        super(context, window, appCompatCallback);
        Runnable runnable;
        new Runnable() {
            public void run() {
                if ((AppCompatDelegateImplV7.this.mInvalidatePanelMenuFeatures & 1) != 0) {
                    AppCompatDelegateImplV7.this.doInvalidatePanelMenu(0);
                }
                if ((AppCompatDelegateImplV7.this.mInvalidatePanelMenuFeatures & 4096) != 0) {
                    AppCompatDelegateImplV7.this.doInvalidatePanelMenu(108);
                }
                boolean access$202 = AppCompatDelegateImplV7.access$202(AppCompatDelegateImplV7.this, false);
                int access$002 = AppCompatDelegateImplV7.access$002(AppCompatDelegateImplV7.this, 0);
            }
        };
        this.mInvalidatePanelMenuRunnable = runnable;
    }

    public void onCreate(Bundle bundle) {
        this.mWindowDecor = (ViewGroup) this.mWindow.getDecorView();
        if ((this.mOriginalWindowCallback instanceof Activity) && NavUtils.getParentActivityName((Activity) this.mOriginalWindowCallback) != null) {
            ActionBar peekSupportActionBar = peekSupportActionBar();
            if (peekSupportActionBar == null) {
                this.mEnableDefaultActionBarUp = true;
            } else {
                peekSupportActionBar.setDefaultDisplayHomeAsUpEnabled(true);
            }
        }
    }

    public void onPostCreate(Bundle bundle) {
        ensureSubDecor();
    }

    public void initWindowDecorActionBar() {
        ActionBar actionBar;
        ActionBar actionBar2;
        ensureSubDecor();
        if (this.mHasActionBar && this.mActionBar == null) {
            if (this.mOriginalWindowCallback instanceof Activity) {
                new WindowDecorActionBar((Activity) this.mOriginalWindowCallback, this.mOverlayActionBar);
                this.mActionBar = actionBar2;
            } else if (this.mOriginalWindowCallback instanceof Dialog) {
                new WindowDecorActionBar((Dialog) this.mOriginalWindowCallback);
                this.mActionBar = actionBar;
            }
            if (this.mActionBar != null) {
                this.mActionBar.setDefaultDisplayHomeAsUpEnabled(this.mEnableDefaultActionBarUp);
            }
        }
    }

    public void setSupportActionBar(Toolbar toolbar) {
        ToolbarActionBar toolbarActionBar;
        Throwable th;
        Toolbar toolbar2 = toolbar;
        if (this.mOriginalWindowCallback instanceof Activity) {
            if (getSupportActionBar() instanceof WindowDecorActionBar) {
                Throwable th2 = th;
                new IllegalStateException("This Activity already has an action bar supplied by the window decor. Do not request Window.FEATURE_SUPPORT_ACTION_BAR and set windowActionBar to false in your theme to use a Toolbar instead.");
                throw th2;
            }
            this.mMenuInflater = null;
            new ToolbarActionBar(toolbar2, ((Activity) this.mContext).getTitle(), this.mAppCompatWindowCallback);
            ToolbarActionBar toolbarActionBar2 = toolbarActionBar;
            this.mActionBar = toolbarActionBar2;
            this.mWindow.setCallback(toolbarActionBar2.getWrappedWindowCallback());
            boolean invalidateOptionsMenu = toolbarActionBar2.invalidateOptionsMenu();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        ActionBar supportActionBar;
        Configuration configuration2 = configuration;
        if (this.mHasActionBar && this.mSubDecorInstalled && (supportActionBar = getSupportActionBar()) != null) {
            supportActionBar.onConfigurationChanged(configuration2);
        }
    }

    public void onStop() {
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setShowHideAnimationEnabled(false);
        }
    }

    public void onPostResume() {
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setShowHideAnimationEnabled(true);
        }
    }

    public void setContentView(View view) {
        ensureSubDecor();
        ViewGroup viewGroup = (ViewGroup) this.mSubDecor.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view);
        this.mOriginalWindowCallback.onContentChanged();
    }

    public void setContentView(int i) {
        ensureSubDecor();
        ViewGroup viewGroup = (ViewGroup) this.mSubDecor.findViewById(16908290);
        viewGroup.removeAllViews();
        View inflate = LayoutInflater.from(this.mContext).inflate(i, viewGroup);
        this.mOriginalWindowCallback.onContentChanged();
    }

    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        ensureSubDecor();
        ViewGroup viewGroup = (ViewGroup) this.mSubDecor.findViewById(16908290);
        viewGroup.removeAllViews();
        viewGroup.addView(view, layoutParams);
        this.mOriginalWindowCallback.onContentChanged();
    }

    public void addContentView(View view, ViewGroup.LayoutParams layoutParams) {
        ensureSubDecor();
        ((ViewGroup) this.mSubDecor.findViewById(16908290)).addView(view, layoutParams);
        this.mOriginalWindowCallback.onContentChanged();
    }

    private void ensureSubDecor() {
        if (!this.mSubDecorInstalled) {
            this.mSubDecor = createSubDecor();
            CharSequence title = getTitle();
            if (!TextUtils.isEmpty(title)) {
                onTitleChanged(title);
            }
            applyFixedSizeWindow();
            onSubDecorInstalled(this.mSubDecor);
            this.mSubDecorInstalled = true;
            PanelFeatureState panelState = getPanelState(0, false);
            if (isDestroyed()) {
                return;
            }
            if (panelState == null || panelState.menu == null) {
                invalidatePanelMenu(108);
            }
        }
    }

    private ViewGroup createSubDecor() {
        FitWindowsViewGroup.OnFitSystemWindowsListener onFitSystemWindowsListener;
        OnApplyWindowInsetsListener onApplyWindowInsetsListener;
        ContentFrameLayout.OnAttachListener onAttachListener;
        Throwable th;
        StringBuilder sb;
        TypedValue typedValue;
        Context context;
        Context context2;
        Throwable th2;
        TypedArray obtainStyledAttributes = this.mContext.obtainStyledAttributes(R.styleable.Theme);
        if (!obtainStyledAttributes.hasValue(R.styleable.Theme_windowActionBar)) {
            obtainStyledAttributes.recycle();
            Throwable th3 = th2;
            new IllegalStateException("You need to use a Theme.AppCompat theme (or descendant) with this activity.");
            throw th3;
        }
        if (obtainStyledAttributes.getBoolean(R.styleable.Theme_windowNoTitle, false)) {
            boolean requestWindowFeature = requestWindowFeature(1);
        } else if (obtainStyledAttributes.getBoolean(R.styleable.Theme_windowActionBar, false)) {
            boolean requestWindowFeature2 = requestWindowFeature(108);
        }
        if (obtainStyledAttributes.getBoolean(R.styleable.Theme_windowActionBarOverlay, false)) {
            boolean requestWindowFeature3 = requestWindowFeature(109);
        }
        if (obtainStyledAttributes.getBoolean(R.styleable.Theme_windowActionModeOverlay, false)) {
            boolean requestWindowFeature4 = requestWindowFeature(10);
        }
        this.mIsFloating = obtainStyledAttributes.getBoolean(R.styleable.Theme_android_windowIsFloating, false);
        obtainStyledAttributes.recycle();
        LayoutInflater from = LayoutInflater.from(this.mContext);
        ViewGroup viewGroup = null;
        if (this.mWindowNoTitle) {
            if (this.mOverlayActionMode) {
                viewGroup = (ViewGroup) from.inflate(R.layout.abc_screen_simple_overlay_action_mode, (ViewGroup) null);
            } else {
                viewGroup = (ViewGroup) from.inflate(R.layout.abc_screen_simple, (ViewGroup) null);
            }
            if (Build.VERSION.SDK_INT >= 21) {
                new OnApplyWindowInsetsListener() {
                    public WindowInsetsCompat onApplyWindowInsets(View view, WindowInsetsCompat windowInsetsCompat) {
                        View view2 = view;
                        WindowInsetsCompat windowInsetsCompat2 = windowInsetsCompat;
                        int systemWindowInsetTop = windowInsetsCompat2.getSystemWindowInsetTop();
                        int access$300 = AppCompatDelegateImplV7.this.updateStatusGuard(systemWindowInsetTop);
                        if (systemWindowInsetTop != access$300) {
                            windowInsetsCompat2 = windowInsetsCompat2.replaceSystemWindowInsets(windowInsetsCompat2.getSystemWindowInsetLeft(), access$300, windowInsetsCompat2.getSystemWindowInsetRight(), windowInsetsCompat2.getSystemWindowInsetBottom());
                        }
                        return ViewCompat.onApplyWindowInsets(view2, windowInsetsCompat2);
                    }
                };
                ViewCompat.setOnApplyWindowInsetsListener(viewGroup, onApplyWindowInsetsListener);
            } else {
                new FitWindowsViewGroup.OnFitSystemWindowsListener() {
                    public void onFitSystemWindows(Rect rect) {
                        Rect rect2 = rect;
                        rect2.top = AppCompatDelegateImplV7.this.updateStatusGuard(rect2.top);
                    }
                };
                ((FitWindowsViewGroup) viewGroup).setOnFitSystemWindowsListener(onFitSystemWindowsListener);
            }
        } else if (this.mIsFloating) {
            viewGroup = (ViewGroup) from.inflate(R.layout.abc_dialog_title_material, (ViewGroup) null);
            this.mOverlayActionBar = false;
            this.mHasActionBar = false;
        } else if (this.mHasActionBar) {
            new TypedValue();
            TypedValue typedValue2 = typedValue;
            boolean resolveAttribute = this.mContext.getTheme().resolveAttribute(R.attr.actionBarTheme, typedValue2, true);
            if (typedValue2.resourceId != 0) {
                new ContextThemeWrapper(this.mContext, typedValue2.resourceId);
                context = context2;
            } else {
                context = this.mContext;
            }
            viewGroup = (ViewGroup) LayoutInflater.from(context).inflate(R.layout.abc_screen_toolbar, (ViewGroup) null);
            this.mDecorContentParent = (DecorContentParent) viewGroup.findViewById(R.id.decor_content_parent);
            this.mDecorContentParent.setWindowCallback(getWindowCallback());
            if (this.mOverlayActionBar) {
                this.mDecorContentParent.initFeature(109);
            }
            if (this.mFeatureProgress) {
                this.mDecorContentParent.initFeature(2);
            }
            if (this.mFeatureIndeterminateProgress) {
                this.mDecorContentParent.initFeature(5);
            }
        }
        if (viewGroup == null) {
            Throwable th4 = th;
            new StringBuilder();
            new IllegalArgumentException(sb.append("AppCompat does not support the current theme features: { windowActionBar: ").append(this.mHasActionBar).append(", windowActionBarOverlay: ").append(this.mOverlayActionBar).append(", android:windowIsFloating: ").append(this.mIsFloating).append(", windowActionModeOverlay: ").append(this.mOverlayActionMode).append(", windowNoTitle: ").append(this.mWindowNoTitle).append(" }").toString());
            throw th4;
        }
        if (this.mDecorContentParent == null) {
            this.mTitleView = (TextView) viewGroup.findViewById(R.id.title);
        }
        ViewUtils.makeOptionalFitsSystemWindows(viewGroup);
        ViewGroup viewGroup2 = (ViewGroup) this.mWindow.findViewById(16908290);
        ContentFrameLayout contentFrameLayout = (ContentFrameLayout) viewGroup.findViewById(R.id.action_bar_activity_content);
        while (viewGroup2.getChildCount() > 0) {
            View childAt = viewGroup2.getChildAt(0);
            viewGroup2.removeViewAt(0);
            contentFrameLayout.addView(childAt);
        }
        this.mWindow.setContentView(viewGroup);
        viewGroup2.setId(-1);
        contentFrameLayout.setId(16908290);
        if (viewGroup2 instanceof FrameLayout) {
            ((FrameLayout) viewGroup2).setForeground(null);
        }
        new ContentFrameLayout.OnAttachListener() {
            public void onAttachedFromWindow() {
            }

            public void onDetachedFromWindow() {
                AppCompatDelegateImplV7.this.dismissPopups();
            }
        };
        contentFrameLayout.setAttachListener(onAttachListener);
        return viewGroup;
    }

    /* access modifiers changed from: package-private */
    public void onSubDecorInstalled(ViewGroup viewGroup) {
    }

    private void applyFixedSizeWindow() {
        ContentFrameLayout contentFrameLayout = (ContentFrameLayout) this.mSubDecor.findViewById(16908290);
        contentFrameLayout.setDecorPadding(this.mWindowDecor.getPaddingLeft(), this.mWindowDecor.getPaddingTop(), this.mWindowDecor.getPaddingRight(), this.mWindowDecor.getPaddingBottom());
        TypedArray obtainStyledAttributes = this.mContext.obtainStyledAttributes(R.styleable.Theme);
        boolean value = obtainStyledAttributes.getValue(R.styleable.Theme_windowMinWidthMajor, contentFrameLayout.getMinWidthMajor());
        boolean value2 = obtainStyledAttributes.getValue(R.styleable.Theme_windowMinWidthMinor, contentFrameLayout.getMinWidthMinor());
        if (obtainStyledAttributes.hasValue(R.styleable.Theme_windowFixedWidthMajor)) {
            boolean value3 = obtainStyledAttributes.getValue(R.styleable.Theme_windowFixedWidthMajor, contentFrameLayout.getFixedWidthMajor());
        }
        if (obtainStyledAttributes.hasValue(R.styleable.Theme_windowFixedWidthMinor)) {
            boolean value4 = obtainStyledAttributes.getValue(R.styleable.Theme_windowFixedWidthMinor, contentFrameLayout.getFixedWidthMinor());
        }
        if (obtainStyledAttributes.hasValue(R.styleable.Theme_windowFixedHeightMajor)) {
            boolean value5 = obtainStyledAttributes.getValue(R.styleable.Theme_windowFixedHeightMajor, contentFrameLayout.getFixedHeightMajor());
        }
        if (obtainStyledAttributes.hasValue(R.styleable.Theme_windowFixedHeightMinor)) {
            boolean value6 = obtainStyledAttributes.getValue(R.styleable.Theme_windowFixedHeightMinor, contentFrameLayout.getFixedHeightMinor());
        }
        obtainStyledAttributes.recycle();
        contentFrameLayout.requestLayout();
    }

    public boolean requestWindowFeature(int i) {
        int sanitizeWindowFeatureId = sanitizeWindowFeatureId(i);
        if (this.mWindowNoTitle && sanitizeWindowFeatureId == 108) {
            return false;
        }
        if (this.mHasActionBar && sanitizeWindowFeatureId == 1) {
            this.mHasActionBar = false;
        }
        switch (sanitizeWindowFeatureId) {
            case 1:
                throwFeatureRequestIfSubDecorInstalled();
                this.mWindowNoTitle = true;
                return true;
            case 2:
                throwFeatureRequestIfSubDecorInstalled();
                this.mFeatureProgress = true;
                return true;
            case 5:
                throwFeatureRequestIfSubDecorInstalled();
                this.mFeatureIndeterminateProgress = true;
                return true;
            case 10:
                throwFeatureRequestIfSubDecorInstalled();
                this.mOverlayActionMode = true;
                return true;
            case 108:
                throwFeatureRequestIfSubDecorInstalled();
                this.mHasActionBar = true;
                return true;
            case 109:
                throwFeatureRequestIfSubDecorInstalled();
                this.mOverlayActionBar = true;
                return true;
            default:
                return this.mWindow.requestFeature(sanitizeWindowFeatureId);
        }
    }

    public boolean hasWindowFeature(int i) {
        int sanitizeWindowFeatureId = sanitizeWindowFeatureId(i);
        switch (sanitizeWindowFeatureId) {
            case 1:
                return this.mWindowNoTitle;
            case 2:
                return this.mFeatureProgress;
            case 5:
                return this.mFeatureIndeterminateProgress;
            case 10:
                return this.mOverlayActionMode;
            case 108:
                return this.mHasActionBar;
            case 109:
                return this.mOverlayActionBar;
            default:
                return this.mWindow.hasFeature(sanitizeWindowFeatureId);
        }
    }

    /* access modifiers changed from: package-private */
    public void onTitleChanged(CharSequence charSequence) {
        CharSequence charSequence2 = charSequence;
        if (this.mDecorContentParent != null) {
            this.mDecorContentParent.setWindowTitle(charSequence2);
        } else if (peekSupportActionBar() != null) {
            peekSupportActionBar().setWindowTitle(charSequence2);
        } else if (this.mTitleView != null) {
            this.mTitleView.setText(charSequence2);
        }
    }

    /* access modifiers changed from: package-private */
    public void onPanelClosed(int i, Menu menu) {
        int i2 = i;
        if (i2 == 108) {
            ActionBar supportActionBar = getSupportActionBar();
            if (supportActionBar != null) {
                supportActionBar.dispatchMenuVisibilityChanged(false);
            }
        } else if (i2 == 0) {
            PanelFeatureState panelState = getPanelState(i2, true);
            if (panelState.isOpen) {
                closePanel(panelState, false);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean onMenuOpened(int i, Menu menu) {
        if (i != 108) {
            return false;
        }
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.dispatchMenuVisibilityChanged(true);
        }
        return true;
    }

    public boolean onMenuItemSelected(MenuBuilder menuBuilder, MenuItem menuItem) {
        PanelFeatureState findMenuPanel;
        MenuBuilder menuBuilder2 = menuBuilder;
        MenuItem menuItem2 = menuItem;
        Window.Callback windowCallback = getWindowCallback();
        if (windowCallback == null || isDestroyed() || (findMenuPanel = findMenuPanel(menuBuilder2.getRootMenu())) == null) {
            return false;
        }
        return windowCallback.onMenuItemSelected(findMenuPanel.featureId, menuItem2);
    }

    public void onMenuModeChange(MenuBuilder menuBuilder) {
        reopenMenu(menuBuilder, true);
    }

    public ActionMode startSupportActionMode(ActionMode.Callback callback) {
        ActionMode.Callback callback2;
        Throwable th;
        ActionMode.Callback callback3 = callback;
        if (callback3 == null) {
            Throwable th2 = th;
            new IllegalArgumentException("ActionMode callback can not be null.");
            throw th2;
        }
        if (this.mActionMode != null) {
            this.mActionMode.finish();
        }
        new ActionModeCallbackWrapperV7(callback3);
        ActionMode.Callback callback4 = callback2;
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            this.mActionMode = supportActionBar.startActionMode(callback4);
            if (!(this.mActionMode == null || this.mAppCompatCallback == null)) {
                this.mAppCompatCallback.onSupportActionModeStarted(this.mActionMode);
            }
        }
        if (this.mActionMode == null) {
            this.mActionMode = startSupportActionModeFromWindow(callback4);
        }
        return this.mActionMode;
    }

    public void invalidateOptionsMenu() {
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar == null || !supportActionBar.invalidateOptionsMenu()) {
            invalidatePanelMenu(0);
        }
    }

    /* access modifiers changed from: package-private */
    public ActionMode startSupportActionModeFromWindow(ActionMode.Callback callback) {
        ActionMode.Callback callback2;
        StandaloneActionMode standaloneActionMode;
        ViewPropertyAnimatorListener viewPropertyAnimatorListener;
        TypedValue typedValue;
        Context context;
        ActionBarContextView actionBarContextView;
        PopupWindow popupWindow;
        Runnable runnable;
        Context context2;
        ActionMode.Callback callback3 = callback;
        endOnGoingFadeAnimation();
        if (this.mActionMode != null) {
            this.mActionMode.finish();
        }
        new ActionModeCallbackWrapperV7(callback3);
        ActionMode.Callback callback4 = callback2;
        ActionMode actionMode = null;
        if (this.mAppCompatCallback != null && !isDestroyed()) {
            try {
                actionMode = this.mAppCompatCallback.onWindowStartingSupportActionMode(callback4);
            } catch (AbstractMethodError e) {
            }
        }
        if (actionMode != null) {
            this.mActionMode = actionMode;
        } else {
            if (this.mActionModeView == null) {
                if (this.mIsFloating) {
                    new TypedValue();
                    TypedValue typedValue2 = typedValue;
                    Resources.Theme theme = this.mContext.getTheme();
                    boolean resolveAttribute = theme.resolveAttribute(R.attr.actionBarTheme, typedValue2, true);
                    if (typedValue2.resourceId != 0) {
                        Resources.Theme newTheme = this.mContext.getResources().newTheme();
                        newTheme.setTo(theme);
                        newTheme.applyStyle(typedValue2.resourceId, true);
                        new ContextThemeWrapper(this.mContext, 0);
                        context = context2;
                        context.getTheme().setTo(newTheme);
                    } else {
                        context = this.mContext;
                    }
                    new ActionBarContextView(context);
                    this.mActionModeView = actionBarContextView;
                    new PopupWindow(context, (AttributeSet) null, R.attr.actionModePopupWindowStyle);
                    this.mActionModePopup = popupWindow;
                    PopupWindowCompat.setWindowLayoutType(this.mActionModePopup, 2);
                    this.mActionModePopup.setContentView(this.mActionModeView);
                    this.mActionModePopup.setWidth(-1);
                    boolean resolveAttribute2 = context.getTheme().resolveAttribute(R.attr.actionBarSize, typedValue2, true);
                    this.mActionModeView.setContentHeight(TypedValue.complexToDimensionPixelSize(typedValue2.data, context.getResources().getDisplayMetrics()));
                    this.mActionModePopup.setHeight(-2);
                    new Runnable() {
                        public void run() {
                            ViewPropertyAnimatorListener viewPropertyAnimatorListener;
                            AppCompatDelegateImplV7.this.mActionModePopup.showAtLocation(AppCompatDelegateImplV7.this.mActionModeView, 55, 0, 0);
                            AppCompatDelegateImplV7.this.endOnGoingFadeAnimation();
                            ViewCompat.setAlpha(AppCompatDelegateImplV7.this.mActionModeView, 0.0f);
                            AppCompatDelegateImplV7.this.mFadeAnim = ViewCompat.animate(AppCompatDelegateImplV7.this.mActionModeView).alpha(1.0f);
                            new ViewPropertyAnimatorListenerAdapter() {
                                public void onAnimationEnd(View view) {
                                    ViewCompat.setAlpha(AppCompatDelegateImplV7.this.mActionModeView, 1.0f);
                                    ViewPropertyAnimatorCompat listener = AppCompatDelegateImplV7.this.mFadeAnim.setListener(null);
                                    AppCompatDelegateImplV7.this.mFadeAnim = null;
                                }

                                public void onAnimationStart(View view) {
                                    AppCompatDelegateImplV7.this.mActionModeView.setVisibility(0);
                                }
                            };
                            ViewPropertyAnimatorCompat listener = AppCompatDelegateImplV7.this.mFadeAnim.setListener(viewPropertyAnimatorListener);
                        }
                    };
                    this.mShowActionModePopup = runnable;
                } else {
                    ViewStubCompat viewStubCompat = (ViewStubCompat) this.mSubDecor.findViewById(R.id.action_mode_bar_stub);
                    if (viewStubCompat != null) {
                        viewStubCompat.setLayoutInflater(LayoutInflater.from(getActionBarThemedContext()));
                        this.mActionModeView = (ActionBarContextView) viewStubCompat.inflate();
                    }
                }
            }
            if (this.mActionModeView != null) {
                endOnGoingFadeAnimation();
                this.mActionModeView.killMode();
                StandaloneActionMode standaloneActionMode2 = standaloneActionMode;
                new StandaloneActionMode(this.mActionModeView.getContext(), this.mActionModeView, callback4, this.mActionModePopup == null);
                StandaloneActionMode standaloneActionMode3 = standaloneActionMode2;
                if (callback3.onCreateActionMode(standaloneActionMode3, standaloneActionMode3.getMenu())) {
                    standaloneActionMode3.invalidate();
                    this.mActionModeView.initForMode(standaloneActionMode3);
                    this.mActionMode = standaloneActionMode3;
                    ViewCompat.setAlpha(this.mActionModeView, 0.0f);
                    this.mFadeAnim = ViewCompat.animate(this.mActionModeView).alpha(1.0f);
                    new ViewPropertyAnimatorListenerAdapter() {
                        public void onAnimationEnd(View view) {
                            ViewCompat.setAlpha(AppCompatDelegateImplV7.this.mActionModeView, 1.0f);
                            ViewPropertyAnimatorCompat listener = AppCompatDelegateImplV7.this.mFadeAnim.setListener(null);
                            AppCompatDelegateImplV7.this.mFadeAnim = null;
                        }

                        public void onAnimationStart(View view) {
                            AppCompatDelegateImplV7.this.mActionModeView.setVisibility(0);
                            AppCompatDelegateImplV7.this.mActionModeView.sendAccessibilityEvent(32);
                            if (AppCompatDelegateImplV7.this.mActionModeView.getParent() != null) {
                                ViewCompat.requestApplyInsets((View) AppCompatDelegateImplV7.this.mActionModeView.getParent());
                            }
                        }
                    };
                    ViewPropertyAnimatorCompat listener = this.mFadeAnim.setListener(viewPropertyAnimatorListener);
                    if (this.mActionModePopup != null) {
                        boolean post = this.mWindow.getDecorView().post(this.mShowActionModePopup);
                    }
                } else {
                    this.mActionMode = null;
                }
            }
        }
        if (!(this.mActionMode == null || this.mAppCompatCallback == null)) {
            this.mAppCompatCallback.onSupportActionModeStarted(this.mActionMode);
        }
        return this.mActionMode;
    }

    /* access modifiers changed from: private */
    public void endOnGoingFadeAnimation() {
        if (this.mFadeAnim != null) {
            this.mFadeAnim.cancel();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean onBackPressed() {
        if (this.mActionMode != null) {
            this.mActionMode.finish();
            return true;
        }
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar == null || !supportActionBar.collapseActionView()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean onKeyShortcut(int i, KeyEvent keyEvent) {
        int i2 = i;
        KeyEvent keyEvent2 = keyEvent;
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null && supportActionBar.onKeyShortcut(i2, keyEvent2)) {
            return true;
        }
        if (this.mPreparedPanel == null || !performPanelShortcut(this.mPreparedPanel, keyEvent2.getKeyCode(), keyEvent2, 1)) {
            if (this.mPreparedPanel == null) {
                PanelFeatureState panelState = getPanelState(0, true);
                boolean preparePanel = preparePanel(panelState, keyEvent2);
                boolean performPanelShortcut = performPanelShortcut(panelState, keyEvent2.getKeyCode(), keyEvent2, 1);
                panelState.isPrepared = false;
                if (performPanelShortcut) {
                    return true;
                }
            }
            return false;
        }
        if (this.mPreparedPanel != null) {
            this.mPreparedPanel.isHandled = true;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        KeyEvent keyEvent2 = keyEvent;
        if (keyEvent2.getKeyCode() == 82 && this.mOriginalWindowCallback.dispatchKeyEvent(keyEvent2)) {
            return true;
        }
        int keyCode = keyEvent2.getKeyCode();
        return keyEvent2.getAction() == 0 ? onKeyDown(keyCode, keyEvent2) : onKeyUp(keyCode, keyEvent2);
    }

    /* access modifiers changed from: package-private */
    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        KeyEvent keyEvent2 = keyEvent;
        switch (i) {
            case 82:
                boolean onKeyUpPanel = onKeyUpPanel(0, keyEvent2);
                return true;
            case 4:
                boolean z = this.mLongPressBackDown;
                this.mLongPressBackDown = false;
                PanelFeatureState panelState = getPanelState(0, false);
                if (panelState != null && panelState.isOpen) {
                    if (!z) {
                        closePanel(panelState, true);
                    }
                    return true;
                } else if (onBackPressed()) {
                    return true;
                }
                break;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        int i2 = i;
        KeyEvent keyEvent2 = keyEvent;
        switch (i2) {
            case 82:
                boolean onKeyDownPanel = onKeyDownPanel(0, keyEvent2);
                return true;
            case 4:
                this.mLongPressBackDown = (keyEvent2.getFlags() & 128) != 0;
                break;
        }
        if (Build.VERSION.SDK_INT < 11) {
            boolean onKeyShortcut = onKeyShortcut(i2, keyEvent2);
        }
        return false;
    }

    public View createView(View view, String str, @NonNull Context context, @NonNull AttributeSet attributeSet) {
        AppCompatViewInflater appCompatViewInflater;
        View view2 = view;
        String str2 = str;
        Context context2 = context;
        AttributeSet attributeSet2 = attributeSet;
        boolean z = Build.VERSION.SDK_INT < 21;
        if (this.mAppCompatViewInflater == null) {
            new AppCompatViewInflater();
            this.mAppCompatViewInflater = appCompatViewInflater;
        }
        return this.mAppCompatViewInflater.createView(view2, str2, context2, attributeSet2, z && this.mSubDecorInstalled && shouldInheritContext((ViewParent) view2), z, true);
    }

    private boolean shouldInheritContext(ViewParent viewParent) {
        ViewParent viewParent2 = viewParent;
        if (viewParent2 == null) {
            return false;
        }
        while (viewParent2 != null) {
            if (viewParent2 == this.mWindowDecor || !(viewParent2 instanceof View) || ViewCompat.isAttachedToWindow((View) viewParent2)) {
                return false;
            }
            viewParent2 = viewParent2.getParent();
        }
        return true;
    }

    public void installViewFactory() {
        LayoutInflater from = LayoutInflater.from(this.mContext);
        if (from.getFactory() == null) {
            LayoutInflaterCompat.setFactory(from, this);
        } else {
            int i = Log.i("AppCompatDelegate", "The Activity's LayoutInflater already has a Factory installed so we can not install AppCompat's");
        }
    }

    public final View onCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        View view2 = view;
        String str2 = str;
        Context context2 = context;
        AttributeSet attributeSet2 = attributeSet;
        View callActivityOnCreateView = callActivityOnCreateView(view2, str2, context2, attributeSet2);
        if (callActivityOnCreateView != null) {
            return callActivityOnCreateView;
        }
        return createView(view2, str2, context2, attributeSet2);
    }

    /* access modifiers changed from: package-private */
    public View callActivityOnCreateView(View view, String str, Context context, AttributeSet attributeSet) {
        View onCreateView;
        String str2 = str;
        Context context2 = context;
        AttributeSet attributeSet2 = attributeSet;
        if (!(this.mOriginalWindowCallback instanceof LayoutInflater.Factory) || (onCreateView = ((LayoutInflater.Factory) this.mOriginalWindowCallback).onCreateView(str2, context2, attributeSet2)) == null) {
            return null;
        }
        return onCreateView;
    }

    private void openPanel(PanelFeatureState panelFeatureState, KeyEvent keyEvent) {
        WindowManager.LayoutParams layoutParams;
        ViewGroup.LayoutParams layoutParams2;
        ViewGroup.LayoutParams layoutParams3;
        PanelFeatureState panelFeatureState2 = panelFeatureState;
        KeyEvent keyEvent2 = keyEvent;
        if (!panelFeatureState2.isOpen && !isDestroyed()) {
            if (panelFeatureState2.featureId == 0) {
                Context context = this.mContext;
                boolean z = (context.getResources().getConfiguration().screenLayout & 15) == 4;
                boolean z2 = context.getApplicationInfo().targetSdkVersion >= 11;
                if (z && z2) {
                    return;
                }
            }
            Window.Callback windowCallback = getWindowCallback();
            if (windowCallback == null || windowCallback.onMenuOpened(panelFeatureState2.featureId, panelFeatureState2.menu)) {
                WindowManager windowManager = (WindowManager) this.mContext.getSystemService("window");
                if (windowManager != null && preparePanel(panelFeatureState2, keyEvent2)) {
                    int i = -2;
                    if (panelFeatureState2.decorView == null || panelFeatureState2.refreshDecorView) {
                        if (panelFeatureState2.decorView == null) {
                            if (!initializePanelDecor(panelFeatureState2) || panelFeatureState2.decorView == null) {
                                return;
                            }
                        } else if (panelFeatureState2.refreshDecorView && panelFeatureState2.decorView.getChildCount() > 0) {
                            panelFeatureState2.decorView.removeAllViews();
                        }
                        if (initializePanelContent(panelFeatureState2) && panelFeatureState2.hasPanelItems()) {
                            ViewGroup.LayoutParams layoutParams4 = panelFeatureState2.shownPanelView.getLayoutParams();
                            if (layoutParams4 == null) {
                                new ViewGroup.LayoutParams(-2, -2);
                                layoutParams4 = layoutParams2;
                            }
                            panelFeatureState2.decorView.setBackgroundResource(panelFeatureState2.background);
                            ViewParent parent = panelFeatureState2.shownPanelView.getParent();
                            if (parent != null && (parent instanceof ViewGroup)) {
                                ((ViewGroup) parent).removeView(panelFeatureState2.shownPanelView);
                            }
                            panelFeatureState2.decorView.addView(panelFeatureState2.shownPanelView, layoutParams4);
                            if (!panelFeatureState2.shownPanelView.hasFocus()) {
                                boolean requestFocus = panelFeatureState2.shownPanelView.requestFocus();
                            }
                        } else {
                            return;
                        }
                    } else if (!(panelFeatureState2.createdPanelView == null || (layoutParams3 = panelFeatureState2.createdPanelView.getLayoutParams()) == null || layoutParams3.width != -1)) {
                        i = -1;
                    }
                    panelFeatureState2.isHandled = false;
                    new WindowManager.LayoutParams(i, -2, panelFeatureState2.x, panelFeatureState2.y, 1002, 8519680, -3);
                    WindowManager.LayoutParams layoutParams5 = layoutParams;
                    layoutParams5.gravity = panelFeatureState2.gravity;
                    layoutParams5.windowAnimations = panelFeatureState2.windowAnimations;
                    windowManager.addView(panelFeatureState2.decorView, layoutParams5);
                    panelFeatureState2.isOpen = true;
                    return;
                }
                return;
            }
            closePanel(panelFeatureState2, true);
        }
    }

    private boolean initializePanelDecor(PanelFeatureState panelFeatureState) {
        ViewGroup viewGroup;
        PanelFeatureState panelFeatureState2 = panelFeatureState;
        panelFeatureState2.setStyle(getActionBarThemedContext());
        new ListMenuDecorView(panelFeatureState2.listPresenterContext);
        panelFeatureState2.decorView = viewGroup;
        panelFeatureState2.gravity = 81;
        return true;
    }

    private void reopenMenu(MenuBuilder menuBuilder, boolean z) {
        boolean z2 = z;
        if (this.mDecorContentParent == null || !this.mDecorContentParent.canShowOverflowMenu() || (ViewConfigurationCompat.hasPermanentMenuKey(ViewConfiguration.get(this.mContext)) && !this.mDecorContentParent.isOverflowMenuShowPending())) {
            PanelFeatureState panelState = getPanelState(0, true);
            panelState.refreshDecorView = true;
            closePanel(panelState, false);
            openPanel(panelState, null);
            return;
        }
        Window.Callback windowCallback = getWindowCallback();
        if (this.mDecorContentParent.isOverflowMenuShowing() && z2) {
            boolean hideOverflowMenu = this.mDecorContentParent.hideOverflowMenu();
            if (!isDestroyed()) {
                windowCallback.onPanelClosed(108, getPanelState(0, true).menu);
            }
        } else if (windowCallback != null && !isDestroyed()) {
            if (this.mInvalidatePanelMenuPosted && (this.mInvalidatePanelMenuFeatures & 1) != 0) {
                boolean removeCallbacks = this.mWindowDecor.removeCallbacks(this.mInvalidatePanelMenuRunnable);
                this.mInvalidatePanelMenuRunnable.run();
            }
            PanelFeatureState panelState2 = getPanelState(0, true);
            if (panelState2.menu != null && !panelState2.refreshMenuContent && windowCallback.onPreparePanel(0, panelState2.createdPanelView, panelState2.menu)) {
                boolean onMenuOpened = windowCallback.onMenuOpened(108, panelState2.menu);
                boolean showOverflowMenu = this.mDecorContentParent.showOverflowMenu();
            }
        }
    }

    private boolean initializePanelMenu(PanelFeatureState panelFeatureState) {
        MenuBuilder menuBuilder;
        TypedValue typedValue;
        Context context;
        PanelFeatureState panelFeatureState2 = panelFeatureState;
        Context context2 = this.mContext;
        if ((panelFeatureState2.featureId == 0 || panelFeatureState2.featureId == 108) && this.mDecorContentParent != null) {
            new TypedValue();
            TypedValue typedValue2 = typedValue;
            Resources.Theme theme = context2.getTheme();
            boolean resolveAttribute = theme.resolveAttribute(R.attr.actionBarTheme, typedValue2, true);
            Resources.Theme theme2 = null;
            if (typedValue2.resourceId != 0) {
                theme2 = context2.getResources().newTheme();
                theme2.setTo(theme);
                theme2.applyStyle(typedValue2.resourceId, true);
                boolean resolveAttribute2 = theme2.resolveAttribute(R.attr.actionBarWidgetTheme, typedValue2, true);
            } else {
                boolean resolveAttribute3 = theme.resolveAttribute(R.attr.actionBarWidgetTheme, typedValue2, true);
            }
            if (typedValue2.resourceId != 0) {
                if (theme2 == null) {
                    theme2 = context2.getResources().newTheme();
                    theme2.setTo(theme);
                }
                theme2.applyStyle(typedValue2.resourceId, true);
            }
            if (theme2 != null) {
                new ContextThemeWrapper(context2, 0);
                context2 = context;
                context2.getTheme().setTo(theme2);
            }
        }
        new MenuBuilder(context2);
        MenuBuilder menuBuilder2 = menuBuilder;
        menuBuilder2.setCallback(this);
        panelFeatureState2.setMenu(menuBuilder2);
        return true;
    }

    private boolean initializePanelContent(PanelFeatureState panelFeatureState) {
        PanelMenuPresenterCallback panelMenuPresenterCallback;
        PanelFeatureState panelFeatureState2 = panelFeatureState;
        if (panelFeatureState2.createdPanelView != null) {
            panelFeatureState2.shownPanelView = panelFeatureState2.createdPanelView;
            return true;
        } else if (panelFeatureState2.menu == null) {
            return false;
        } else {
            if (this.mPanelMenuPresenterCallback == null) {
                new PanelMenuPresenterCallback();
                this.mPanelMenuPresenterCallback = panelMenuPresenterCallback;
            }
            panelFeatureState2.shownPanelView = (View) panelFeatureState2.getListMenuView(this.mPanelMenuPresenterCallback);
            return panelFeatureState2.shownPanelView != null;
        }
    }

    private boolean preparePanel(PanelFeatureState panelFeatureState, KeyEvent keyEvent) {
        ActionMenuPresenterCallback actionMenuPresenterCallback;
        PanelFeatureState panelFeatureState2 = panelFeatureState;
        KeyEvent keyEvent2 = keyEvent;
        if (isDestroyed()) {
            return false;
        }
        if (panelFeatureState2.isPrepared) {
            return true;
        }
        if (!(this.mPreparedPanel == null || this.mPreparedPanel == panelFeatureState2)) {
            closePanel(this.mPreparedPanel, false);
        }
        Window.Callback windowCallback = getWindowCallback();
        if (windowCallback != null) {
            panelFeatureState2.createdPanelView = windowCallback.onCreatePanelView(panelFeatureState2.featureId);
        }
        boolean z = panelFeatureState2.featureId == 0 || panelFeatureState2.featureId == 108;
        if (z && this.mDecorContentParent != null) {
            this.mDecorContentParent.setMenuPrepared();
        }
        if (panelFeatureState2.createdPanelView == null && (!z || !(peekSupportActionBar() instanceof ToolbarActionBar))) {
            if (panelFeatureState2.menu == null || panelFeatureState2.refreshMenuContent) {
                if (panelFeatureState2.menu == null && (!initializePanelMenu(panelFeatureState2) || panelFeatureState2.menu == null)) {
                    return false;
                }
                if (z && this.mDecorContentParent != null) {
                    if (this.mActionMenuPresenterCallback == null) {
                        new ActionMenuPresenterCallback();
                        this.mActionMenuPresenterCallback = actionMenuPresenterCallback;
                    }
                    this.mDecorContentParent.setMenu(panelFeatureState2.menu, this.mActionMenuPresenterCallback);
                }
                panelFeatureState2.menu.stopDispatchingItemsChanged();
                if (!windowCallback.onCreatePanelMenu(panelFeatureState2.featureId, panelFeatureState2.menu)) {
                    panelFeatureState2.setMenu(null);
                    if (z && this.mDecorContentParent != null) {
                        this.mDecorContentParent.setMenu(null, this.mActionMenuPresenterCallback);
                    }
                    return false;
                }
                panelFeatureState2.refreshMenuContent = false;
            }
            panelFeatureState2.menu.stopDispatchingItemsChanged();
            if (panelFeatureState2.frozenActionViewState != null) {
                panelFeatureState2.menu.restoreActionViewStates(panelFeatureState2.frozenActionViewState);
                panelFeatureState2.frozenActionViewState = null;
            }
            if (!windowCallback.onPreparePanel(0, panelFeatureState2.createdPanelView, panelFeatureState2.menu)) {
                if (z && this.mDecorContentParent != null) {
                    this.mDecorContentParent.setMenu(null, this.mActionMenuPresenterCallback);
                }
                panelFeatureState2.menu.startDispatchingItemsChanged();
                return false;
            }
            panelFeatureState2.qwertyMode = KeyCharacterMap.load(keyEvent2 != null ? keyEvent2.getDeviceId() : -1).getKeyboardType() != 1;
            panelFeatureState2.menu.setQwertyMode(panelFeatureState2.qwertyMode);
            panelFeatureState2.menu.startDispatchingItemsChanged();
        }
        panelFeatureState2.isPrepared = true;
        panelFeatureState2.isHandled = false;
        this.mPreparedPanel = panelFeatureState2;
        return true;
    }

    /* access modifiers changed from: private */
    public void checkCloseActionMenu(MenuBuilder menuBuilder) {
        MenuBuilder menuBuilder2 = menuBuilder;
        if (!this.mClosingActionMenu) {
            this.mClosingActionMenu = true;
            this.mDecorContentParent.dismissPopups();
            Window.Callback windowCallback = getWindowCallback();
            if (windowCallback != null && !isDestroyed()) {
                windowCallback.onPanelClosed(108, menuBuilder2);
            }
            this.mClosingActionMenu = false;
        }
    }

    /* access modifiers changed from: private */
    public void closePanel(int i) {
        closePanel(getPanelState(i, true), true);
    }

    /* access modifiers changed from: private */
    public void closePanel(PanelFeatureState panelFeatureState, boolean z) {
        PanelFeatureState panelFeatureState2 = panelFeatureState;
        boolean z2 = z;
        if (!z2 || panelFeatureState2.featureId != 0 || this.mDecorContentParent == null || !this.mDecorContentParent.isOverflowMenuShowing()) {
            WindowManager windowManager = (WindowManager) this.mContext.getSystemService("window");
            if (!(windowManager == null || !panelFeatureState2.isOpen || panelFeatureState2.decorView == null)) {
                windowManager.removeView(panelFeatureState2.decorView);
                if (z2) {
                    callOnPanelClosed(panelFeatureState2.featureId, panelFeatureState2, null);
                }
            }
            panelFeatureState2.isPrepared = false;
            panelFeatureState2.isHandled = false;
            panelFeatureState2.isOpen = false;
            panelFeatureState2.shownPanelView = null;
            panelFeatureState2.refreshDecorView = true;
            if (this.mPreparedPanel == panelFeatureState2) {
                this.mPreparedPanel = null;
                return;
            }
            return;
        }
        checkCloseActionMenu(panelFeatureState2.menu);
    }

    private boolean onKeyDownPanel(int i, KeyEvent keyEvent) {
        int i2 = i;
        KeyEvent keyEvent2 = keyEvent;
        if (keyEvent2.getRepeatCount() == 0) {
            PanelFeatureState panelState = getPanelState(i2, true);
            if (!panelState.isOpen) {
                return preparePanel(panelState, keyEvent2);
            }
        }
        return false;
    }

    private boolean onKeyUpPanel(int i, KeyEvent keyEvent) {
        int i2 = i;
        KeyEvent keyEvent2 = keyEvent;
        if (this.mActionMode != null) {
            return false;
        }
        boolean z = false;
        PanelFeatureState panelState = getPanelState(i2, true);
        if (i2 != 0 || this.mDecorContentParent == null || !this.mDecorContentParent.canShowOverflowMenu() || ViewConfigurationCompat.hasPermanentMenuKey(ViewConfiguration.get(this.mContext))) {
            if (panelState.isOpen || panelState.isHandled) {
                z = panelState.isOpen;
                closePanel(panelState, true);
            } else if (panelState.isPrepared) {
                boolean z2 = true;
                if (panelState.refreshMenuContent) {
                    panelState.isPrepared = false;
                    z2 = preparePanel(panelState, keyEvent2);
                }
                if (z2) {
                    openPanel(panelState, keyEvent2);
                    z = true;
                }
            }
        } else if (this.mDecorContentParent.isOverflowMenuShowing()) {
            z = this.mDecorContentParent.hideOverflowMenu();
        } else if (!isDestroyed() && preparePanel(panelState, keyEvent2)) {
            z = this.mDecorContentParent.showOverflowMenu();
        }
        if (z) {
            AudioManager audioManager = (AudioManager) this.mContext.getSystemService("audio");
            if (audioManager != null) {
                audioManager.playSoundEffect(0);
            } else {
                int w = Log.w("AppCompatDelegate", "Couldn't get audio manager");
            }
        }
        return z;
    }

    /* access modifiers changed from: private */
    public void callOnPanelClosed(int i, PanelFeatureState panelFeatureState, Menu menu) {
        int i2 = i;
        PanelFeatureState panelFeatureState2 = panelFeatureState;
        MenuBuilder menuBuilder = menu;
        if (menuBuilder == null) {
            if (panelFeatureState2 == null && i2 >= 0 && i2 < this.mPanels.length) {
                panelFeatureState2 = this.mPanels[i2];
            }
            if (panelFeatureState2 != null) {
                menuBuilder = panelFeatureState2.menu;
            }
        }
        if ((panelFeatureState2 == null || panelFeatureState2.isOpen) && !isDestroyed()) {
            this.mOriginalWindowCallback.onPanelClosed(i2, menuBuilder);
        }
    }

    /* access modifiers changed from: private */
    public PanelFeatureState findMenuPanel(Menu menu) {
        Menu menu2 = menu;
        PanelFeatureState[] panelFeatureStateArr = this.mPanels;
        int length = panelFeatureStateArr != null ? panelFeatureStateArr.length : 0;
        for (int i = 0; i < length; i++) {
            PanelFeatureState panelFeatureState = panelFeatureStateArr[i];
            if (panelFeatureState != null && panelFeatureState.menu == menu2) {
                return panelFeatureState;
            }
        }
        return null;
    }

    private PanelFeatureState getPanelState(int i, boolean z) {
        PanelFeatureState panelFeatureState;
        int i2 = i;
        PanelFeatureState[] panelFeatureStateArr = this.mPanels;
        PanelFeatureState[] panelFeatureStateArr2 = panelFeatureStateArr;
        if (panelFeatureStateArr == null || panelFeatureStateArr2.length <= i2) {
            PanelFeatureState[] panelFeatureStateArr3 = new PanelFeatureState[(i2 + 1)];
            if (panelFeatureStateArr2 != null) {
                System.arraycopy(panelFeatureStateArr2, 0, panelFeatureStateArr3, 0, panelFeatureStateArr2.length);
            }
            PanelFeatureState[] panelFeatureStateArr4 = panelFeatureStateArr3;
            panelFeatureStateArr2 = panelFeatureStateArr4;
            this.mPanels = panelFeatureStateArr4;
        }
        PanelFeatureState panelFeatureState2 = panelFeatureStateArr2[i2];
        if (panelFeatureState2 == null) {
            new PanelFeatureState(i2);
            PanelFeatureState panelFeatureState3 = panelFeatureState;
            panelFeatureState2 = panelFeatureState3;
            panelFeatureStateArr2[i2] = panelFeatureState3;
        }
        return panelFeatureState2;
    }

    private boolean performPanelShortcut(PanelFeatureState panelFeatureState, int i, KeyEvent keyEvent, int i2) {
        PanelFeatureState panelFeatureState2 = panelFeatureState;
        int i3 = i;
        KeyEvent keyEvent2 = keyEvent;
        int i4 = i2;
        if (keyEvent2.isSystem()) {
            return false;
        }
        boolean z = false;
        if ((panelFeatureState2.isPrepared || preparePanel(panelFeatureState2, keyEvent2)) && panelFeatureState2.menu != null) {
            z = panelFeatureState2.menu.performShortcut(i3, keyEvent2, i4);
        }
        if (z && (i4 & 1) == 0 && this.mDecorContentParent == null) {
            closePanel(panelFeatureState2, true);
        }
        return z;
    }

    private void invalidatePanelMenu(int i) {
        this.mInvalidatePanelMenuFeatures = this.mInvalidatePanelMenuFeatures | (1 << i);
        if (!this.mInvalidatePanelMenuPosted && this.mWindowDecor != null) {
            ViewCompat.postOnAnimation(this.mWindowDecor, this.mInvalidatePanelMenuRunnable);
            this.mInvalidatePanelMenuPosted = true;
        }
    }

    /* access modifiers changed from: private */
    public void doInvalidatePanelMenu(int i) {
        PanelFeatureState panelState;
        Bundle bundle;
        int i2 = i;
        PanelFeatureState panelState2 = getPanelState(i2, true);
        if (panelState2.menu != null) {
            new Bundle();
            Bundle bundle2 = bundle;
            panelState2.menu.saveActionViewStates(bundle2);
            if (bundle2.size() > 0) {
                panelState2.frozenActionViewState = bundle2;
            }
            panelState2.menu.stopDispatchingItemsChanged();
            panelState2.menu.clear();
        }
        panelState2.refreshMenuContent = true;
        panelState2.refreshDecorView = true;
        if ((i2 == 108 || i2 == 0) && this.mDecorContentParent != null && (panelState = getPanelState(0, false)) != null) {
            panelState.isPrepared = false;
            boolean preparePanel = preparePanel(panelState, null);
        }
    }

    /* access modifiers changed from: private */
    public int updateStatusGuard(int i) {
        View view;
        ViewGroup.LayoutParams layoutParams;
        Rect rect;
        Rect rect2;
        int i2 = i;
        boolean z = false;
        if (this.mActionModeView != null && (this.mActionModeView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams)) {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.mActionModeView.getLayoutParams();
            boolean z2 = false;
            if (this.mActionModeView.isShown()) {
                if (this.mTempRect1 == null) {
                    new Rect();
                    this.mTempRect1 = rect;
                    new Rect();
                    this.mTempRect2 = rect2;
                }
                Rect rect3 = this.mTempRect1;
                Rect rect4 = this.mTempRect2;
                rect3.set(0, i2, 0, 0);
                ViewUtils.computeFitSystemWindows(this.mSubDecor, rect3, rect4);
                if (marginLayoutParams.topMargin != (rect4.top == 0 ? i2 : 0)) {
                    z2 = true;
                    marginLayoutParams.topMargin = i2;
                    if (this.mStatusGuard == null) {
                        new View(this.mContext);
                        this.mStatusGuard = view;
                        this.mStatusGuard.setBackgroundColor(this.mContext.getResources().getColor(R.color.abc_input_method_navigation_guard));
                        new ViewGroup.LayoutParams(-1, i2);
                        this.mSubDecor.addView(this.mStatusGuard, -1, layoutParams);
                    } else {
                        ViewGroup.LayoutParams layoutParams2 = this.mStatusGuard.getLayoutParams();
                        if (layoutParams2.height != i2) {
                            layoutParams2.height = i2;
                            this.mStatusGuard.setLayoutParams(layoutParams2);
                        }
                    }
                }
                z = this.mStatusGuard != null;
                if (!this.mOverlayActionMode && z) {
                    i2 = 0;
                }
            } else if (marginLayoutParams.topMargin != 0) {
                z2 = true;
                marginLayoutParams.topMargin = 0;
            }
            if (z2) {
                this.mActionModeView.setLayoutParams(marginLayoutParams);
            }
        }
        if (this.mStatusGuard != null) {
            this.mStatusGuard.setVisibility(z ? 0 : 8);
        }
        return i2;
    }

    private void throwFeatureRequestIfSubDecorInstalled() {
        Throwable th;
        if (this.mSubDecorInstalled) {
            Throwable th2 = th;
            new AndroidRuntimeException("Window feature must be requested before adding content");
            throw th2;
        }
    }

    private int sanitizeWindowFeatureId(int i) {
        int i2 = i;
        if (i2 == 8) {
            int i3 = Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR id when requesting this feature.");
            return 108;
        } else if (i2 != 9) {
            return i2;
        } else {
            int i4 = Log.i("AppCompatDelegate", "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY id when requesting this feature.");
            return 109;
        }
    }

    /* access modifiers changed from: package-private */
    public ViewGroup getSubDecor() {
        return this.mSubDecor;
    }

    /* access modifiers changed from: private */
    public void dismissPopups() {
        if (this.mDecorContentParent != null) {
            this.mDecorContentParent.dismissPopups();
        }
        if (this.mActionModePopup != null) {
            boolean removeCallbacks = this.mWindowDecor.removeCallbacks(this.mShowActionModePopup);
            if (this.mActionModePopup.isShowing()) {
                try {
                    this.mActionModePopup.dismiss();
                } catch (IllegalArgumentException e) {
                }
            }
            this.mActionModePopup = null;
        }
        endOnGoingFadeAnimation();
        PanelFeatureState panelState = getPanelState(0, false);
        if (panelState != null && panelState.menu != null) {
            panelState.menu.close();
        }
    }

    class ActionModeCallbackWrapperV7 implements ActionMode.Callback {
        private ActionMode.Callback mWrapped;

        public ActionModeCallbackWrapperV7(ActionMode.Callback callback) {
            this.mWrapped = callback;
        }

        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            return this.mWrapped.onCreateActionMode(actionMode, menu);
        }

        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return this.mWrapped.onPrepareActionMode(actionMode, menu);
        }

        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            return this.mWrapped.onActionItemClicked(actionMode, menuItem);
        }

        public void onDestroyActionMode(ActionMode actionMode) {
            ViewPropertyAnimatorListener viewPropertyAnimatorListener;
            this.mWrapped.onDestroyActionMode(actionMode);
            if (AppCompatDelegateImplV7.this.mActionModePopup != null) {
                boolean removeCallbacks = AppCompatDelegateImplV7.this.mWindow.getDecorView().removeCallbacks(AppCompatDelegateImplV7.this.mShowActionModePopup);
            }
            if (AppCompatDelegateImplV7.this.mActionModeView != null) {
                AppCompatDelegateImplV7.this.endOnGoingFadeAnimation();
                AppCompatDelegateImplV7.this.mFadeAnim = ViewCompat.animate(AppCompatDelegateImplV7.this.mActionModeView).alpha(0.0f);
                new ViewPropertyAnimatorListenerAdapter() {
                    public void onAnimationEnd(View view) {
                        AppCompatDelegateImplV7.this.mActionModeView.setVisibility(8);
                        if (AppCompatDelegateImplV7.this.mActionModePopup != null) {
                            AppCompatDelegateImplV7.this.mActionModePopup.dismiss();
                        } else if (AppCompatDelegateImplV7.this.mActionModeView.getParent() instanceof View) {
                            ViewCompat.requestApplyInsets((View) AppCompatDelegateImplV7.this.mActionModeView.getParent());
                        }
                        AppCompatDelegateImplV7.this.mActionModeView.removeAllViews();
                        ViewPropertyAnimatorCompat listener = AppCompatDelegateImplV7.this.mFadeAnim.setListener(null);
                        AppCompatDelegateImplV7.this.mFadeAnim = null;
                    }
                };
                ViewPropertyAnimatorCompat listener = AppCompatDelegateImplV7.this.mFadeAnim.setListener(viewPropertyAnimatorListener);
            }
            if (AppCompatDelegateImplV7.this.mAppCompatCallback != null) {
                AppCompatDelegateImplV7.this.mAppCompatCallback.onSupportActionModeFinished(AppCompatDelegateImplV7.this.mActionMode);
            }
            AppCompatDelegateImplV7.this.mActionMode = null;
        }
    }

    private final class PanelMenuPresenterCallback implements MenuPresenter.Callback {
        private PanelMenuPresenterCallback() {
        }

        public void onCloseMenu(MenuBuilder menuBuilder, boolean z) {
            MenuBuilder menuBuilder2 = menuBuilder;
            boolean z2 = z;
            MenuBuilder rootMenu = menuBuilder2.getRootMenu();
            boolean z3 = rootMenu != menuBuilder2;
            PanelFeatureState access$800 = AppCompatDelegateImplV7.this.findMenuPanel(z3 ? rootMenu : menuBuilder2);
            if (access$800 == null) {
                return;
            }
            if (z3) {
                AppCompatDelegateImplV7.this.callOnPanelClosed(access$800.featureId, access$800, rootMenu);
                AppCompatDelegateImplV7.this.closePanel(access$800, true);
                return;
            }
            AppCompatDelegateImplV7.this.closePanel(access$800, z2);
        }

        public boolean onOpenSubMenu(MenuBuilder menuBuilder) {
            Window.Callback windowCallback;
            MenuBuilder menuBuilder2 = menuBuilder;
            if (menuBuilder2 == null && AppCompatDelegateImplV7.this.mHasActionBar && (windowCallback = AppCompatDelegateImplV7.this.getWindowCallback()) != null && !AppCompatDelegateImplV7.this.isDestroyed()) {
                boolean onMenuOpened = windowCallback.onMenuOpened(108, menuBuilder2);
            }
            return true;
        }
    }

    private final class ActionMenuPresenterCallback implements MenuPresenter.Callback {
        private ActionMenuPresenterCallback() {
        }

        public boolean onOpenSubMenu(MenuBuilder menuBuilder) {
            MenuBuilder menuBuilder2 = menuBuilder;
            Window.Callback windowCallback = AppCompatDelegateImplV7.this.getWindowCallback();
            if (windowCallback != null) {
                boolean onMenuOpened = windowCallback.onMenuOpened(108, menuBuilder2);
            }
            return true;
        }

        public void onCloseMenu(MenuBuilder menuBuilder, boolean z) {
            AppCompatDelegateImplV7.this.checkCloseActionMenu(menuBuilder);
        }
    }

    private static final class PanelFeatureState {
        int background;
        View createdPanelView;
        ViewGroup decorView;
        int featureId;
        Bundle frozenActionViewState;
        Bundle frozenMenuState;
        int gravity;
        boolean isHandled;
        boolean isOpen;
        boolean isPrepared;
        ListMenuPresenter listMenuPresenter;
        Context listPresenterContext;
        MenuBuilder menu;
        public boolean qwertyMode;
        boolean refreshDecorView = false;
        boolean refreshMenuContent;
        View shownPanelView;
        boolean wasLastOpen;
        int windowAnimations;
        int x;
        int y;

        PanelFeatureState(int i) {
            this.featureId = i;
        }

        public boolean hasPanelItems() {
            if (this.shownPanelView == null) {
                return false;
            }
            if (this.createdPanelView != null) {
                return true;
            }
            return this.listMenuPresenter.getAdapter().getCount() > 0;
        }

        public void clearMenuPresenters() {
            if (this.menu != null) {
                this.menu.removeMenuPresenter(this.listMenuPresenter);
            }
            this.listMenuPresenter = null;
        }

        /* access modifiers changed from: package-private */
        public void setStyle(Context context) {
            TypedValue typedValue;
            Context context2;
            Context context3 = context;
            new TypedValue();
            TypedValue typedValue2 = typedValue;
            Resources.Theme newTheme = context3.getResources().newTheme();
            newTheme.setTo(context3.getTheme());
            boolean resolveAttribute = newTheme.resolveAttribute(R.attr.actionBarPopupTheme, typedValue2, true);
            if (typedValue2.resourceId != 0) {
                newTheme.applyStyle(typedValue2.resourceId, true);
            }
            boolean resolveAttribute2 = newTheme.resolveAttribute(R.attr.panelMenuListTheme, typedValue2, true);
            if (typedValue2.resourceId != 0) {
                newTheme.applyStyle(typedValue2.resourceId, true);
            } else {
                newTheme.applyStyle(R.style.Theme_AppCompat_CompactMenu, true);
            }
            new ContextThemeWrapper(context3, 0);
            Context context4 = context2;
            context4.getTheme().setTo(newTheme);
            this.listPresenterContext = context4;
            TypedArray obtainStyledAttributes = context4.obtainStyledAttributes(R.styleable.Theme);
            this.background = obtainStyledAttributes.getResourceId(R.styleable.Theme_panelBackground, 0);
            this.windowAnimations = obtainStyledAttributes.getResourceId(R.styleable.Theme_android_windowAnimationStyle, 0);
            obtainStyledAttributes.recycle();
        }

        /* access modifiers changed from: package-private */
        public void setMenu(MenuBuilder menuBuilder) {
            MenuBuilder menuBuilder2 = menuBuilder;
            if (menuBuilder2 != this.menu) {
                if (this.menu != null) {
                    this.menu.removeMenuPresenter(this.listMenuPresenter);
                }
                this.menu = menuBuilder2;
                if (menuBuilder2 != null && this.listMenuPresenter != null) {
                    menuBuilder2.addMenuPresenter(this.listMenuPresenter);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public MenuView getListMenuView(MenuPresenter.Callback callback) {
            ListMenuPresenter listMenuPresenter2;
            MenuPresenter.Callback callback2 = callback;
            if (this.menu == null) {
                return null;
            }
            if (this.listMenuPresenter == null) {
                new ListMenuPresenter(this.listPresenterContext, R.layout.abc_list_menu_item_layout);
                this.listMenuPresenter = listMenuPresenter2;
                this.listMenuPresenter.setCallback(callback2);
                this.menu.addMenuPresenter(this.listMenuPresenter);
            }
            return this.listMenuPresenter.getMenuView(this.decorView);
        }

        /* access modifiers changed from: package-private */
        public Parcelable onSaveInstanceState() {
            SavedState savedState;
            Bundle bundle;
            new SavedState();
            SavedState savedState2 = savedState;
            savedState2.featureId = this.featureId;
            savedState2.isOpen = this.isOpen;
            if (this.menu != null) {
                new Bundle();
                savedState2.menuState = bundle;
                this.menu.savePresenterStates(savedState2.menuState);
            }
            return savedState2;
        }

        /* access modifiers changed from: package-private */
        public void onRestoreInstanceState(Parcelable parcelable) {
            SavedState savedState = (SavedState) parcelable;
            this.featureId = savedState.featureId;
            this.wasLastOpen = savedState.isOpen;
            this.frozenMenuState = savedState.menuState;
            this.shownPanelView = null;
            this.decorView = null;
        }

        /* access modifiers changed from: package-private */
        public void applyFrozenState() {
            if (this.menu != null && this.frozenMenuState != null) {
                this.menu.restorePresenterStates(this.frozenMenuState);
                this.frozenMenuState = null;
            }
        }

        private static class SavedState implements Parcelable {
            public static final Parcelable.Creator<SavedState> CREATOR;
            int featureId;
            boolean isOpen;
            Bundle menuState;

            private SavedState() {
            }

            public int describeContents() {
                return 0;
            }

            public void writeToParcel(Parcel parcel, int i) {
                Parcel parcel2 = parcel;
                parcel2.writeInt(this.featureId);
                parcel2.writeInt(this.isOpen ? 1 : 0);
                if (this.isOpen) {
                    parcel2.writeBundle(this.menuState);
                }
            }

            /* access modifiers changed from: private */
            public static SavedState readFromParcel(Parcel parcel, ClassLoader classLoader) {
                SavedState savedState;
                Parcel parcel2 = parcel;
                ClassLoader classLoader2 = classLoader;
                new SavedState();
                SavedState savedState2 = savedState;
                savedState2.featureId = parcel2.readInt();
                savedState2.isOpen = parcel2.readInt() == 1;
                if (savedState2.isOpen) {
                    savedState2.menuState = parcel2.readBundle(classLoader2);
                }
                return savedState2;
            }

            static {
                Object obj;
                new ParcelableCompatCreatorCallbacks<SavedState>() {
                    public SavedState createFromParcel(Parcel parcel, ClassLoader classLoader) {
                        return SavedState.readFromParcel(parcel, classLoader);
                    }

                    public SavedState[] newArray(int i) {
                        return new SavedState[i];
                    }
                };
                CREATOR = ParcelableCompat.newCreator(obj);
            }
        }
    }

    private class ListMenuDecorView extends ContentFrameLayout {
        public ListMenuDecorView(Context context) {
            super(context);
        }

        public boolean dispatchKeyEvent(KeyEvent keyEvent) {
            KeyEvent keyEvent2 = keyEvent;
            return AppCompatDelegateImplV7.this.dispatchKeyEvent(keyEvent2) || super.dispatchKeyEvent(keyEvent2);
        }

        public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
            MotionEvent motionEvent2 = motionEvent;
            if (motionEvent2.getAction() == 0) {
                if (isOutOfBounds((int) motionEvent2.getX(), (int) motionEvent2.getY())) {
                    AppCompatDelegateImplV7.this.closePanel(0);
                    return true;
                }
            }
            return super.onInterceptTouchEvent(motionEvent2);
        }

        public void setBackgroundResource(int i) {
            setBackgroundDrawable(TintManager.getDrawable(getContext(), i));
        }

        private boolean isOutOfBounds(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            return i3 < -5 || i4 < -5 || i3 > getWidth() + 5 || i4 > getHeight() + 5;
        }
    }
}
