package android.support.v7.app;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.util.ArrayMap;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatCheckedTextView;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatMultiAutoCompleteTextView;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.InflateException;
import android.view.View;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

class AppCompatViewInflater {
    private static final String LOG_TAG = "AppCompatViewInflater";
    private static final Map<String, Constructor<? extends View>> sConstructorMap;
    private static final Class<?>[] sConstructorSignature;
    private static final int[] sOnClickAttrs = {16843375};
    private final Object[] mConstructorArgs = new Object[2];

    AppCompatViewInflater() {
    }

    static {
        Map<String, Constructor<? extends View>> map;
        Class<?>[] clsArr = new Class[2];
        clsArr[0] = Context.class;
        Class<?>[] clsArr2 = clsArr;
        clsArr2[1] = AttributeSet.class;
        sConstructorSignature = clsArr2;
        new ArrayMap();
        sConstructorMap = map;
    }

    public final View createView(View view, String str, @NonNull Context context, @NonNull AttributeSet attributeSet, boolean z, boolean z2, boolean z3) {
        View view2;
        View view3;
        View view4;
        View view5;
        View view6;
        View view7;
        View view8;
        View view9;
        View view10;
        View view11;
        View view12;
        View view13;
        View view14;
        View view15 = view;
        String str2 = str;
        Context context2 = context;
        AttributeSet attributeSet2 = attributeSet;
        boolean z4 = z2;
        boolean z5 = z3;
        Context context3 = context2;
        if (z && view15 != null) {
            context2 = view15.getContext();
        }
        if (z4 || z5) {
            context2 = themifyContext(context2, attributeSet2, z4, z5);
        }
        View view16 = null;
        String str3 = str2;
        boolean z6 = true;
        switch (str3.hashCode()) {
            case -1946472170:
                if (str3.equals("RatingBar")) {
                    z6 = true;
                    break;
                }
                break;
            case -1455429095:
                if (str3.equals("CheckedTextView")) {
                    z6 = true;
                    break;
                }
                break;
            case -1346021293:
                if (str3.equals("MultiAutoCompleteTextView")) {
                    z6 = true;
                    break;
                }
                break;
            case -938935918:
                if (str3.equals("TextView")) {
                    z6 = false;
                    break;
                }
                break;
            case -937446323:
                if (str3.equals("ImageButton")) {
                    z6 = true;
                    break;
                }
                break;
            case -658531749:
                if (str3.equals("SeekBar")) {
                    z6 = true;
                    break;
                }
                break;
            case -339785223:
                if (str3.equals("Spinner")) {
                    z6 = true;
                    break;
                }
                break;
            case 776382189:
                if (str3.equals("RadioButton")) {
                    z6 = true;
                    break;
                }
                break;
            case 1125864064:
                if (str3.equals("ImageView")) {
                    z6 = true;
                    break;
                }
                break;
            case 1413872058:
                if (str3.equals("AutoCompleteTextView")) {
                    z6 = true;
                    break;
                }
                break;
            case 1601505219:
                if (str3.equals("CheckBox")) {
                    z6 = true;
                    break;
                }
                break;
            case 1666676343:
                if (str3.equals("EditText")) {
                    z6 = true;
                    break;
                }
                break;
            case 2001146706:
                if (str3.equals("Button")) {
                    z6 = true;
                    break;
                }
                break;
        }
        switch (z6) {
            case false:
                new AppCompatTextView(context2, attributeSet2);
                view16 = view14;
                break;
            case true:
                new AppCompatImageView(context2, attributeSet2);
                view16 = view13;
                break;
            case true:
                new AppCompatButton(context2, attributeSet2);
                view16 = view12;
                break;
            case true:
                new AppCompatEditText(context2, attributeSet2);
                view16 = view11;
                break;
            case true:
                new AppCompatSpinner(context2, attributeSet2);
                view16 = view10;
                break;
            case true:
                new AppCompatImageButton(context2, attributeSet2);
                view16 = view9;
                break;
            case true:
                new AppCompatCheckBox(context2, attributeSet2);
                view16 = view8;
                break;
            case true:
                new AppCompatRadioButton(context2, attributeSet2);
                view16 = view7;
                break;
            case true:
                new AppCompatCheckedTextView(context2, attributeSet2);
                view16 = view6;
                break;
            case true:
                new AppCompatAutoCompleteTextView(context2, attributeSet2);
                view16 = view5;
                break;
            case true:
                new AppCompatMultiAutoCompleteTextView(context2, attributeSet2);
                view16 = view4;
                break;
            case true:
                new AppCompatRatingBar(context2, attributeSet2);
                view16 = view3;
                break;
            case true:
                new AppCompatSeekBar(context2, attributeSet2);
                view16 = view2;
                break;
        }
        if (view16 == null && context3 != context2) {
            view16 = createViewFromTag(context2, str2, attributeSet2);
        }
        if (view16 != null) {
            checkOnClickListener(view16, attributeSet2);
        }
        return view16;
    }

    private View createViewFromTag(Context context, String str, AttributeSet attributeSet) {
        Context context2 = context;
        String str2 = str;
        AttributeSet attributeSet2 = attributeSet;
        if (str2.equals("view")) {
            str2 = attributeSet2.getAttributeValue(null, "class");
        }
        try {
            this.mConstructorArgs[0] = context2;
            this.mConstructorArgs[1] = attributeSet2;
            if (-1 == str2.indexOf(46)) {
                View createView = createView(context2, str2, "android.widget.");
                this.mConstructorArgs[0] = null;
                this.mConstructorArgs[1] = null;
                return createView;
            }
            View createView2 = createView(context2, str2, null);
            this.mConstructorArgs[0] = null;
            this.mConstructorArgs[1] = null;
            return createView2;
        } catch (Exception e) {
            this.mConstructorArgs[0] = null;
            this.mConstructorArgs[1] = null;
            return null;
        } catch (Throwable th) {
            Throwable th2 = th;
            this.mConstructorArgs[0] = null;
            this.mConstructorArgs[1] = null;
            throw th2;
        }
    }

    private void checkOnClickListener(View view, AttributeSet attributeSet) {
        View.OnClickListener onClickListener;
        View view2 = view;
        AttributeSet attributeSet2 = attributeSet;
        Context context = view2.getContext();
        if (ViewCompat.hasOnClickListeners(view2) && (context instanceof ContextWrapper)) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet2, sOnClickAttrs);
            String string = obtainStyledAttributes.getString(0);
            if (string != null) {
                new DeclaredOnClickListener(view2, string);
                view2.setOnClickListener(onClickListener);
            }
            obtainStyledAttributes.recycle();
        }
    }

    private View createView(Context context, String str, String str2) throws ClassNotFoundException, InflateException {
        String str3;
        StringBuilder sb;
        Context context2 = context;
        String str4 = str;
        String str5 = str2;
        Constructor<? extends U> constructor = sConstructorMap.get(str4);
        if (constructor == null) {
            try {
                ClassLoader classLoader = context2.getClassLoader();
                if (str5 != null) {
                    new StringBuilder();
                    str3 = sb.append(str5).append(str4).toString();
                } else {
                    str3 = str4;
                }
                constructor = classLoader.loadClass(str3).asSubclass(View.class).getConstructor(sConstructorSignature);
                Constructor<? extends View> put = sConstructorMap.put(str4, constructor);
            } catch (Exception e) {
                return null;
            }
        }
        constructor.setAccessible(true);
        return (View) constructor.newInstance(this.mConstructorArgs);
    }

    private static Context themifyContext(Context context, AttributeSet attributeSet, boolean z, boolean z2) {
        Context context2;
        Context context3 = context;
        boolean z3 = z2;
        TypedArray obtainStyledAttributes = context3.obtainStyledAttributes(attributeSet, R.styleable.View, 0, 0);
        int i = 0;
        if (z) {
            i = obtainStyledAttributes.getResourceId(R.styleable.View_android_theme, 0);
        }
        if (z3 && i == 0) {
            i = obtainStyledAttributes.getResourceId(R.styleable.View_theme, 0);
            if (i != 0) {
                int i2 = Log.i(LOG_TAG, "app:theme is now deprecated. Please move to using android:theme instead.");
            }
        }
        obtainStyledAttributes.recycle();
        if (i != 0 && (!(context3 instanceof ContextThemeWrapper) || ((ContextThemeWrapper) context3).getThemeResId() != i)) {
            new ContextThemeWrapper(context3, i);
            context3 = context2;
        }
        return context3;
    }

    private static class DeclaredOnClickListener implements View.OnClickListener {
        private final View mHostView;
        private final String mMethodName;
        private Context mResolvedContext;
        private Method mResolvedMethod;

        public DeclaredOnClickListener(@NonNull View view, @NonNull String str) {
            this.mHostView = view;
            this.mMethodName = str;
        }

        public void onClick(@NonNull View view) {
            Throwable th;
            Throwable th2;
            View view2 = view;
            if (this.mResolvedMethod == null) {
                resolveMethod(this.mHostView.getContext(), this.mMethodName);
            }
            try {
                Object invoke = this.mResolvedMethod.invoke(this.mResolvedContext, view2);
            } catch (IllegalAccessException e) {
                IllegalAccessException illegalAccessException = e;
                Throwable th3 = th2;
                new IllegalStateException("Could not execute non-public method for android:onClick", illegalAccessException);
                throw th3;
            } catch (InvocationTargetException e2) {
                InvocationTargetException invocationTargetException = e2;
                Throwable th4 = th;
                new IllegalStateException("Could not execute method for android:onClick", invocationTargetException);
                throw th4;
            }
        }

        @NonNull
        private void resolveMethod(@Nullable Context context, @NonNull String str) {
            StringBuilder sb;
            String sb2;
            Throwable th;
            StringBuilder sb3;
            Method method;
            Context context2 = context;
            while (context2 != null) {
                try {
                    if (!context2.isRestricted() && (method = context2.getClass().getMethod(this.mMethodName, View.class)) != null) {
                        this.mResolvedMethod = method;
                        this.mResolvedContext = context2;
                        return;
                    }
                } catch (NoSuchMethodException e) {
                }
                if (context2 instanceof ContextWrapper) {
                    context2 = ((ContextWrapper) context2).getBaseContext();
                } else {
                    context2 = null;
                }
            }
            int id = this.mHostView.getId();
            if (id == -1) {
                sb2 = "";
            } else {
                new StringBuilder();
                sb2 = sb.append(" with id '").append(this.mHostView.getContext().getResources().getResourceEntryName(id)).append("'").toString();
            }
            String str2 = sb2;
            Throwable th2 = th;
            new StringBuilder();
            new IllegalStateException(sb3.append("Could not find method ").append(this.mMethodName).append("(View) in a parent or ancestor Context for android:onClick ").append("attribute defined on view ").append(this.mHostView.getClass()).append(str2).toString());
            throw th2;
        }
    }
}
