package android.support.v7.widget;

import android.os.Bundle;
import android.support.v4.view.AccessibilityDelegateCompat;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;

public class RecyclerViewAccessibilityDelegate extends AccessibilityDelegateCompat {
    final AccessibilityDelegateCompat mItemDelegate;
    final RecyclerView mRecyclerView;

    public RecyclerViewAccessibilityDelegate(RecyclerView recyclerView) {
        AccessibilityDelegateCompat accessibilityDelegateCompat;
        new AccessibilityDelegateCompat() {
            public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
                View view2 = view;
                AccessibilityNodeInfoCompat accessibilityNodeInfoCompat2 = accessibilityNodeInfoCompat;
                super.onInitializeAccessibilityNodeInfo(view2, accessibilityNodeInfoCompat2);
                if (!RecyclerViewAccessibilityDelegate.this.shouldIgnore() && RecyclerViewAccessibilityDelegate.this.mRecyclerView.getLayoutManager() != null) {
                    RecyclerViewAccessibilityDelegate.this.mRecyclerView.getLayoutManager().onInitializeAccessibilityNodeInfoForItem(view2, accessibilityNodeInfoCompat2);
                }
            }

            public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
                View view2 = view;
                int i2 = i;
                Bundle bundle2 = bundle;
                if (super.performAccessibilityAction(view2, i2, bundle2)) {
                    return true;
                }
                if (RecyclerViewAccessibilityDelegate.this.shouldIgnore() || RecyclerViewAccessibilityDelegate.this.mRecyclerView.getLayoutManager() == null) {
                    return false;
                }
                return RecyclerViewAccessibilityDelegate.this.mRecyclerView.getLayoutManager().performAccessibilityActionForItem(view2, i2, bundle2);
            }
        };
        this.mItemDelegate = accessibilityDelegateCompat;
        this.mRecyclerView = recyclerView;
    }

    /* access modifiers changed from: private */
    public boolean shouldIgnore() {
        return this.mRecyclerView.hasPendingAdapterUpdates();
    }

    public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
        int i2 = i;
        Bundle bundle2 = bundle;
        if (super.performAccessibilityAction(view, i2, bundle2)) {
            return true;
        }
        if (shouldIgnore() || this.mRecyclerView.getLayoutManager() == null) {
            return false;
        }
        return this.mRecyclerView.getLayoutManager().performAccessibilityAction(i2, bundle2);
    }

    public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        AccessibilityNodeInfoCompat accessibilityNodeInfoCompat2 = accessibilityNodeInfoCompat;
        super.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat2);
        accessibilityNodeInfoCompat2.setClassName(RecyclerView.class.getName());
        if (!shouldIgnore() && this.mRecyclerView.getLayoutManager() != null) {
            this.mRecyclerView.getLayoutManager().onInitializeAccessibilityNodeInfo(accessibilityNodeInfoCompat2);
        }
    }

    public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        View view2 = view;
        AccessibilityEvent accessibilityEvent2 = accessibilityEvent;
        super.onInitializeAccessibilityEvent(view2, accessibilityEvent2);
        accessibilityEvent2.setClassName(RecyclerView.class.getName());
        if ((view2 instanceof RecyclerView) && !shouldIgnore()) {
            RecyclerView recyclerView = (RecyclerView) view2;
            if (recyclerView.getLayoutManager() != null) {
                recyclerView.getLayoutManager().onInitializeAccessibilityEvent(accessibilityEvent2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public AccessibilityDelegateCompat getItemDelegate() {
        return this.mItemDelegate;
    }
}
