package android.support.v7.widget;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.support.v4.view.ActionProvider;
import android.support.v7.appcompat.R;
import android.support.v7.widget.ActivityChooserModel;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;

public class ShareActionProvider extends ActionProvider {
    private static final int DEFAULT_INITIAL_ACTIVITY_COUNT = 4;
    public static final String DEFAULT_SHARE_HISTORY_FILE_NAME = "share_history.xml";
    /* access modifiers changed from: private */
    public final Context mContext;
    private int mMaxShownActivityCount = 4;
    private ActivityChooserModel.OnChooseActivityListener mOnChooseActivityListener;
    private final ShareMenuItemOnMenuItemClickListener mOnMenuItemClickListener;
    /* access modifiers changed from: private */
    public OnShareTargetSelectedListener mOnShareTargetSelectedListener;
    /* access modifiers changed from: private */
    public String mShareHistoryFileName;

    public interface OnShareTargetSelectedListener {
        boolean onShareTargetSelected(ShareActionProvider shareActionProvider, Intent intent);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ShareActionProvider(android.content.Context r9) {
        /*
            r8 = this;
            r0 = r8
            r1 = r9
            r2 = r0
            r3 = r1
            r2.<init>(r3)
            r2 = r0
            r3 = 4
            r2.mMaxShownActivityCount = r3
            r2 = r0
            android.support.v7.widget.ShareActionProvider$ShareMenuItemOnMenuItemClickListener r3 = new android.support.v7.widget.ShareActionProvider$ShareMenuItemOnMenuItemClickListener
            r7 = r3
            r3 = r7
            r4 = r7
            r5 = r0
            r6 = 0
            r4.<init>()
            r2.mOnMenuItemClickListener = r3
            r2 = r0
            java.lang.String r3 = "share_history.xml"
            r2.mShareHistoryFileName = r3
            r2 = r0
            r3 = r1
            r2.mContext = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.ShareActionProvider.<init>(android.content.Context):void");
    }

    public void setOnShareTargetSelectedListener(OnShareTargetSelectedListener onShareTargetSelectedListener) {
        this.mOnShareTargetSelectedListener = onShareTargetSelectedListener;
        setActivityChooserPolicyIfNeeded();
    }

    public View onCreateActionView() {
        ActivityChooserView activityChooserView;
        TypedValue typedValue;
        new ActivityChooserView(this.mContext);
        ActivityChooserView activityChooserView2 = activityChooserView;
        if (!activityChooserView2.isInEditMode()) {
            activityChooserView2.setActivityChooserModel(ActivityChooserModel.get(this.mContext, this.mShareHistoryFileName));
        }
        new TypedValue();
        TypedValue typedValue2 = typedValue;
        boolean resolveAttribute = this.mContext.getTheme().resolveAttribute(R.attr.actionModeShareDrawable, typedValue2, true);
        activityChooserView2.setExpandActivityOverflowButtonDrawable(TintManager.getDrawable(this.mContext, typedValue2.resourceId));
        activityChooserView2.setProvider(this);
        activityChooserView2.setDefaultActionButtonContentDescription(R.string.abc_shareactionprovider_share_with_application);
        activityChooserView2.setExpandActivityOverflowButtonContentDescription(R.string.abc_shareactionprovider_share_with);
        return activityChooserView2;
    }

    public boolean hasSubMenu() {
        return true;
    }

    public void onPrepareSubMenu(SubMenu subMenu) {
        SubMenu subMenu2 = subMenu;
        subMenu2.clear();
        ActivityChooserModel activityChooserModel = ActivityChooserModel.get(this.mContext, this.mShareHistoryFileName);
        PackageManager packageManager = this.mContext.getPackageManager();
        int activityCount = activityChooserModel.getActivityCount();
        int min = Math.min(activityCount, this.mMaxShownActivityCount);
        for (int i = 0; i < min; i++) {
            ResolveInfo activity = activityChooserModel.getActivity(i);
            MenuItem onMenuItemClickListener = subMenu2.add(0, i, i, activity.loadLabel(packageManager)).setIcon(activity.loadIcon(packageManager)).setOnMenuItemClickListener(this.mOnMenuItemClickListener);
        }
        if (min < activityCount) {
            SubMenu addSubMenu = subMenu2.addSubMenu(0, min, min, this.mContext.getString(R.string.abc_activity_chooser_view_see_all));
            for (int i2 = 0; i2 < activityCount; i2++) {
                ResolveInfo activity2 = activityChooserModel.getActivity(i2);
                MenuItem onMenuItemClickListener2 = addSubMenu.add(0, i2, i2, activity2.loadLabel(packageManager)).setIcon(activity2.loadIcon(packageManager)).setOnMenuItemClickListener(this.mOnMenuItemClickListener);
            }
        }
    }

    public void setShareHistoryFileName(String str) {
        this.mShareHistoryFileName = str;
        setActivityChooserPolicyIfNeeded();
    }

    public void setShareIntent(Intent intent) {
        Intent intent2 = intent;
        if (intent2 != null) {
            String action = intent2.getAction();
            if ("android.intent.action.SEND".equals(action) || "android.intent.action.SEND_MULTIPLE".equals(action)) {
                updateIntent(intent2);
            }
        }
        ActivityChooserModel.get(this.mContext, this.mShareHistoryFileName).setIntent(intent2);
    }

    private class ShareMenuItemOnMenuItemClickListener implements MenuItem.OnMenuItemClickListener {
        private ShareMenuItemOnMenuItemClickListener() {
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            Intent chooseActivity = ActivityChooserModel.get(ShareActionProvider.this.mContext, ShareActionProvider.this.mShareHistoryFileName).chooseActivity(menuItem.getItemId());
            if (chooseActivity != null) {
                String action = chooseActivity.getAction();
                if ("android.intent.action.SEND".equals(action) || "android.intent.action.SEND_MULTIPLE".equals(action)) {
                    ShareActionProvider.this.updateIntent(chooseActivity);
                }
                ShareActionProvider.this.mContext.startActivity(chooseActivity);
            }
            return true;
        }
    }

    private void setActivityChooserPolicyIfNeeded() {
        ActivityChooserModel.OnChooseActivityListener onChooseActivityListener;
        if (this.mOnShareTargetSelectedListener != null) {
            if (this.mOnChooseActivityListener == null) {
                new ShareActivityChooserModelPolicy();
                this.mOnChooseActivityListener = onChooseActivityListener;
            }
            ActivityChooserModel.get(this.mContext, this.mShareHistoryFileName).setOnChooseActivityListener(this.mOnChooseActivityListener);
        }
    }

    private class ShareActivityChooserModelPolicy implements ActivityChooserModel.OnChooseActivityListener {
        private ShareActivityChooserModelPolicy() {
        }

        public boolean onChooseActivity(ActivityChooserModel activityChooserModel, Intent intent) {
            Intent intent2 = intent;
            if (ShareActionProvider.this.mOnShareTargetSelectedListener != null) {
                boolean onShareTargetSelected = ShareActionProvider.this.mOnShareTargetSelectedListener.onShareTargetSelected(ShareActionProvider.this, intent2);
            }
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void updateIntent(Intent intent) {
        Intent intent2 = intent;
        if (Build.VERSION.SDK_INT >= 21) {
            Intent addFlags = intent2.addFlags(134742016);
        } else {
            Intent addFlags2 = intent2.addFlags(524288);
        }
    }
}
