package android.support.v7.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.appcompat.R;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.ActivityChooserView;
import android.view.ActionMode;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

public class ActionBarContainer extends FrameLayout {
    private View mActionBarView;
    Drawable mBackground;
    private View mContextView;
    private int mHeight;
    boolean mIsSplit;
    boolean mIsStacked;
    private boolean mIsTransitioning;
    Drawable mSplitBackground;
    Drawable mStackedBackground;
    private View mTabContainer;

    public ActionBarContainer(Context context) {
        this(context, null);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public ActionBarContainer(android.content.Context r11, android.util.AttributeSet r12) {
        /*
            r10 = this;
            r0 = r10
            r1 = r11
            r2 = r12
            r5 = r0
            r6 = r1
            r7 = r2
            r5.<init>(r6, r7)
            int r5 = android.os.Build.VERSION.SDK_INT
            r6 = 21
            if (r5 < r6) goto L_0x0071
            android.support.v7.widget.ActionBarBackgroundDrawableV21 r5 = new android.support.v7.widget.ActionBarBackgroundDrawableV21
            r9 = r5
            r5 = r9
            r6 = r9
            r7 = r0
            r6.<init>(r7)
        L_0x0018:
            r3 = r5
            r5 = r0
            r6 = r3
            r5.setBackgroundDrawable(r6)
            r5 = r1
            r6 = r2
            int[] r7 = android.support.v7.appcompat.R.styleable.ActionBar
            android.content.res.TypedArray r5 = r5.obtainStyledAttributes(r6, r7)
            r4 = r5
            r5 = r0
            r6 = r4
            int r7 = android.support.v7.appcompat.R.styleable.ActionBar_background
            android.graphics.drawable.Drawable r6 = r6.getDrawable(r7)
            r5.mBackground = r6
            r5 = r0
            r6 = r4
            int r7 = android.support.v7.appcompat.R.styleable.ActionBar_backgroundStacked
            android.graphics.drawable.Drawable r6 = r6.getDrawable(r7)
            r5.mStackedBackground = r6
            r5 = r0
            r6 = r4
            int r7 = android.support.v7.appcompat.R.styleable.ActionBar_height
            r8 = -1
            int r6 = r6.getDimensionPixelSize(r7, r8)
            r5.mHeight = r6
            r5 = r0
            int r5 = r5.getId()
            int r6 = android.support.v7.appcompat.R.id.split_action_bar
            if (r5 != r6) goto L_0x005d
            r5 = r0
            r6 = 1
            r5.mIsSplit = r6
            r5 = r0
            r6 = r4
            int r7 = android.support.v7.appcompat.R.styleable.ActionBar_backgroundSplit
            android.graphics.drawable.Drawable r6 = r6.getDrawable(r7)
            r5.mSplitBackground = r6
        L_0x005d:
            r5 = r4
            r5.recycle()
            r5 = r0
            r6 = r0
            boolean r6 = r6.mIsSplit
            if (r6 == 0) goto L_0x007d
            r6 = r0
            android.graphics.drawable.Drawable r6 = r6.mSplitBackground
            if (r6 != 0) goto L_0x007b
            r6 = 1
        L_0x006d:
            r5.setWillNotDraw(r6)
            return
        L_0x0071:
            android.support.v7.widget.ActionBarBackgroundDrawable r5 = new android.support.v7.widget.ActionBarBackgroundDrawable
            r9 = r5
            r5 = r9
            r6 = r9
            r7 = r0
            r6.<init>(r7)
            goto L_0x0018
        L_0x007b:
            r6 = 0
            goto L_0x006d
        L_0x007d:
            r6 = r0
            android.graphics.drawable.Drawable r6 = r6.mBackground
            if (r6 != 0) goto L_0x0089
            r6 = r0
            android.graphics.drawable.Drawable r6 = r6.mStackedBackground
            if (r6 != 0) goto L_0x0089
            r6 = 1
            goto L_0x006d
        L_0x0089:
            r6 = 0
            goto L_0x006d
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.ActionBarContainer.<init>(android.content.Context, android.util.AttributeSet):void");
    }

    public void onFinishInflate() {
        super.onFinishInflate();
        this.mActionBarView = findViewById(R.id.action_bar);
        this.mContextView = findViewById(R.id.action_context_bar);
    }

    public void setPrimaryBackground(Drawable drawable) {
        Drawable drawable2 = drawable;
        if (this.mBackground != null) {
            this.mBackground.setCallback(null);
            unscheduleDrawable(this.mBackground);
        }
        this.mBackground = drawable2;
        if (drawable2 != null) {
            drawable2.setCallback(this);
            if (this.mActionBarView != null) {
                this.mBackground.setBounds(this.mActionBarView.getLeft(), this.mActionBarView.getTop(), this.mActionBarView.getRight(), this.mActionBarView.getBottom());
            }
        }
        setWillNotDraw(this.mIsSplit ? this.mSplitBackground == null : this.mBackground == null && this.mStackedBackground == null);
        invalidate();
    }

    public void setStackedBackground(Drawable drawable) {
        Drawable drawable2 = drawable;
        if (this.mStackedBackground != null) {
            this.mStackedBackground.setCallback(null);
            unscheduleDrawable(this.mStackedBackground);
        }
        this.mStackedBackground = drawable2;
        if (drawable2 != null) {
            drawable2.setCallback(this);
            if (this.mIsStacked && this.mStackedBackground != null) {
                this.mStackedBackground.setBounds(this.mTabContainer.getLeft(), this.mTabContainer.getTop(), this.mTabContainer.getRight(), this.mTabContainer.getBottom());
            }
        }
        setWillNotDraw(this.mIsSplit ? this.mSplitBackground == null : this.mBackground == null && this.mStackedBackground == null);
        invalidate();
    }

    public void setSplitBackground(Drawable drawable) {
        Drawable drawable2 = drawable;
        if (this.mSplitBackground != null) {
            this.mSplitBackground.setCallback(null);
            unscheduleDrawable(this.mSplitBackground);
        }
        this.mSplitBackground = drawable2;
        if (drawable2 != null) {
            drawable2.setCallback(this);
            if (this.mIsSplit && this.mSplitBackground != null) {
                this.mSplitBackground.setBounds(0, 0, getMeasuredWidth(), getMeasuredHeight());
            }
        }
        setWillNotDraw(this.mIsSplit ? this.mSplitBackground == null : this.mBackground == null && this.mStackedBackground == null);
        invalidate();
    }

    public void setVisibility(int i) {
        int i2 = i;
        super.setVisibility(i2);
        boolean z = i2 == 0;
        if (this.mBackground != null) {
            boolean visible = this.mBackground.setVisible(z, false);
        }
        if (this.mStackedBackground != null) {
            boolean visible2 = this.mStackedBackground.setVisible(z, false);
        }
        if (this.mSplitBackground != null) {
            boolean visible3 = this.mSplitBackground.setVisible(z, false);
        }
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        Drawable drawable2 = drawable;
        return (drawable2 == this.mBackground && !this.mIsSplit) || (drawable2 == this.mStackedBackground && this.mIsStacked) || ((drawable2 == this.mSplitBackground && this.mIsSplit) || super.verifyDrawable(drawable2));
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.mBackground != null && this.mBackground.isStateful()) {
            boolean state = this.mBackground.setState(getDrawableState());
        }
        if (this.mStackedBackground != null && this.mStackedBackground.isStateful()) {
            boolean state2 = this.mStackedBackground.setState(getDrawableState());
        }
        if (this.mSplitBackground != null && this.mSplitBackground.isStateful()) {
            boolean state3 = this.mSplitBackground.setState(getDrawableState());
        }
    }

    public void jumpDrawablesToCurrentState() {
        if (Build.VERSION.SDK_INT >= 11) {
            super.jumpDrawablesToCurrentState();
            if (this.mBackground != null) {
                this.mBackground.jumpToCurrentState();
            }
            if (this.mStackedBackground != null) {
                this.mStackedBackground.jumpToCurrentState();
            }
            if (this.mSplitBackground != null) {
                this.mSplitBackground.jumpToCurrentState();
            }
        }
    }

    public void setTransitioning(boolean z) {
        boolean z2 = z;
        this.mIsTransitioning = z2;
        setDescendantFocusability(z2 ? 393216 : 262144);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return this.mIsTransitioning || super.onInterceptTouchEvent(motionEvent);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean onTouchEvent = super.onTouchEvent(motionEvent);
        return true;
    }

    public void setTabContainer(ScrollingTabContainerView scrollingTabContainerView) {
        ScrollingTabContainerView scrollingTabContainerView2 = scrollingTabContainerView;
        if (this.mTabContainer != null) {
            removeView(this.mTabContainer);
        }
        this.mTabContainer = scrollingTabContainerView2;
        if (scrollingTabContainerView2 != null) {
            addView(scrollingTabContainerView2);
            ViewGroup.LayoutParams layoutParams = scrollingTabContainerView2.getLayoutParams();
            layoutParams.width = -1;
            layoutParams.height = -2;
            scrollingTabContainerView2.setAllowCollapse(false);
        }
    }

    public View getTabContainer() {
        return this.mTabContainer;
    }

    public ActionMode startActionModeForChild(View view, ActionMode.Callback callback) {
        return null;
    }

    public android.view.ActionMode startActionModeForChild(View view, ActionMode.Callback callback) {
        return null;
    }

    private boolean isCollapsed(View view) {
        View view2 = view;
        return view2 == null || view2.getVisibility() == 8 || view2.getMeasuredHeight() == 0;
    }

    private int getMeasuredHeightWithMargins(View view) {
        View view2 = view;
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view2.getLayoutParams();
        return view2.getMeasuredHeight() + layoutParams.topMargin + layoutParams.bottomMargin;
    }

    public void onMeasure(int i, int i2) {
        int i3;
        int i4 = i;
        int i5 = i2;
        if (this.mActionBarView == null && View.MeasureSpec.getMode(i5) == Integer.MIN_VALUE && this.mHeight >= 0) {
            i5 = View.MeasureSpec.makeMeasureSpec(Math.min(this.mHeight, View.MeasureSpec.getSize(i5)), Integer.MIN_VALUE);
        }
        super.onMeasure(i4, i5);
        if (this.mActionBarView != null) {
            int mode = View.MeasureSpec.getMode(i5);
            if (this.mTabContainer != null && this.mTabContainer.getVisibility() != 8 && mode != 1073741824) {
                if (!isCollapsed(this.mActionBarView)) {
                    i3 = getMeasuredHeightWithMargins(this.mActionBarView);
                } else if (!isCollapsed(this.mContextView)) {
                    i3 = getMeasuredHeightWithMargins(this.mContextView);
                } else {
                    i3 = 0;
                }
                setMeasuredDimension(getMeasuredWidth(), Math.min(i3 + getMeasuredHeightWithMargins(this.mTabContainer), mode == Integer.MIN_VALUE ? View.MeasureSpec.getSize(i5) : ActivityChooserView.ActivityChooserViewAdapter.MAX_ACTIVITY_COUNT_UNLIMITED));
            }
        }
    }

    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = i;
        int i6 = i3;
        super.onLayout(z, i5, i2, i6, i4);
        View view = this.mTabContainer;
        boolean z2 = (view == null || view.getVisibility() == 8) ? false : true;
        if (!(view == null || view.getVisibility() == 8)) {
            int measuredHeight = getMeasuredHeight();
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
            view.layout(i5, (measuredHeight - view.getMeasuredHeight()) - layoutParams.bottomMargin, i6, measuredHeight - layoutParams.bottomMargin);
        }
        boolean z3 = false;
        if (!this.mIsSplit) {
            if (this.mBackground != null) {
                if (this.mActionBarView.getVisibility() == 0) {
                    this.mBackground.setBounds(this.mActionBarView.getLeft(), this.mActionBarView.getTop(), this.mActionBarView.getRight(), this.mActionBarView.getBottom());
                } else if (this.mContextView == null || this.mContextView.getVisibility() != 0) {
                    this.mBackground.setBounds(0, 0, 0, 0);
                } else {
                    this.mBackground.setBounds(this.mContextView.getLeft(), this.mContextView.getTop(), this.mContextView.getRight(), this.mContextView.getBottom());
                }
                z3 = true;
            }
            this.mIsStacked = z2;
            if (z2 && this.mStackedBackground != null) {
                this.mStackedBackground.setBounds(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
                z3 = true;
            }
        } else if (this.mSplitBackground != null) {
            this.mSplitBackground.setBounds(0, 0, getMeasuredWidth(), getMeasuredHeight());
            z3 = true;
        }
        if (z3) {
            invalidate();
        }
    }
}
