package android.support.v7.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.view.TintableBackgroundView;
import android.support.v7.appcompat.R;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

public class AppCompatAutoCompleteTextView extends AutoCompleteTextView implements TintableBackgroundView {
    private static final int[] TINT_ATTRS = {16843126};
    private AppCompatBackgroundHelper mBackgroundTintHelper;
    private AppCompatTextHelper mTextHelper;
    private TintManager mTintManager;

    public AppCompatAutoCompleteTextView(Context context) {
        this(context, null);
    }

    public AppCompatAutoCompleteTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.autoCompleteTextViewStyle);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AppCompatAutoCompleteTextView(android.content.Context r12, android.util.AttributeSet r13, int r14) {
        /*
            r11 = this;
            r0 = r11
            r1 = r12
            r2 = r13
            r3 = r14
            r5 = r0
            r6 = r1
            android.content.Context r6 = android.support.v7.widget.TintContextWrapper.wrap(r6)
            r7 = r2
            r8 = r3
            r5.<init>(r6, r7, r8)
            r5 = r0
            android.content.Context r5 = r5.getContext()
            r6 = r2
            int[] r7 = android.support.v7.widget.AppCompatAutoCompleteTextView.TINT_ATTRS
            r8 = r3
            r9 = 0
            android.support.v7.widget.TintTypedArray r5 = android.support.v7.widget.TintTypedArray.obtainStyledAttributes(r5, r6, r7, r8, r9)
            r4 = r5
            r5 = r0
            r6 = r4
            android.support.v7.widget.TintManager r6 = r6.getTintManager()
            r5.mTintManager = r6
            r5 = r4
            r6 = 0
            boolean r5 = r5.hasValue(r6)
            if (r5 == 0) goto L_0x0038
            r5 = r0
            r6 = r4
            r7 = 0
            android.graphics.drawable.Drawable r6 = r6.getDrawable(r7)
            r5.setDropDownBackgroundDrawable(r6)
        L_0x0038:
            r5 = r4
            r5.recycle()
            r5 = r0
            android.support.v7.widget.AppCompatBackgroundHelper r6 = new android.support.v7.widget.AppCompatBackgroundHelper
            r10 = r6
            r6 = r10
            r7 = r10
            r8 = r0
            r9 = r0
            android.support.v7.widget.TintManager r9 = r9.mTintManager
            r7.<init>(r8, r9)
            r5.mBackgroundTintHelper = r6
            r5 = r0
            android.support.v7.widget.AppCompatBackgroundHelper r5 = r5.mBackgroundTintHelper
            r6 = r2
            r7 = r3
            r5.loadFromAttributes(r6, r7)
            r5 = r0
            r6 = r0
            android.support.v7.widget.AppCompatTextHelper r6 = android.support.v7.widget.AppCompatTextHelper.create(r6)
            r5.mTextHelper = r6
            r5 = r0
            android.support.v7.widget.AppCompatTextHelper r5 = r5.mTextHelper
            r6 = r2
            r7 = r3
            r5.loadFromAttributes(r6, r7)
            r5 = r0
            android.support.v7.widget.AppCompatTextHelper r5 = r5.mTextHelper
            r5.applyCompoundDrawablesTints()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.AppCompatAutoCompleteTextView.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public void setDropDownBackgroundResource(@DrawableRes int i) {
        int i2 = i;
        if (this.mTintManager != null) {
            setDropDownBackgroundDrawable(this.mTintManager.getDrawable(i2));
        } else {
            super.setDropDownBackgroundResource(i2);
        }
    }

    public void setBackgroundResource(@DrawableRes int i) {
        int i2 = i;
        super.setBackgroundResource(i2);
        if (this.mBackgroundTintHelper != null) {
            this.mBackgroundTintHelper.onSetBackgroundResource(i2);
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        Drawable drawable2 = drawable;
        super.setBackgroundDrawable(drawable2);
        if (this.mBackgroundTintHelper != null) {
            this.mBackgroundTintHelper.onSetBackgroundDrawable(drawable2);
        }
    }

    public void setSupportBackgroundTintList(@Nullable ColorStateList colorStateList) {
        ColorStateList colorStateList2 = colorStateList;
        if (this.mBackgroundTintHelper != null) {
            this.mBackgroundTintHelper.setSupportBackgroundTintList(colorStateList2);
        }
    }

    @Nullable
    public ColorStateList getSupportBackgroundTintList() {
        return this.mBackgroundTintHelper != null ? this.mBackgroundTintHelper.getSupportBackgroundTintList() : null;
    }

    public void setSupportBackgroundTintMode(@Nullable PorterDuff.Mode mode) {
        PorterDuff.Mode mode2 = mode;
        if (this.mBackgroundTintHelper != null) {
            this.mBackgroundTintHelper.setSupportBackgroundTintMode(mode2);
        }
    }

    @Nullable
    public PorterDuff.Mode getSupportBackgroundTintMode() {
        return this.mBackgroundTintHelper != null ? this.mBackgroundTintHelper.getSupportBackgroundTintMode() : null;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        if (this.mBackgroundTintHelper != null) {
            this.mBackgroundTintHelper.applySupportBackgroundTint();
        }
        if (this.mTextHelper != null) {
            this.mTextHelper.applyCompoundDrawablesTints();
        }
    }

    public void setTextAppearance(Context context, int i) {
        Context context2 = context;
        int i2 = i;
        super.setTextAppearance(context2, i2);
        if (this.mTextHelper != null) {
            this.mTextHelper.onSetTextAppearance(context2, i2);
        }
    }
}
