package android.support.v7.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R;
import android.support.v7.text.AllCapsTransformationMethod;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.TransformationMethod;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.CompoundButton;

public class SwitchCompat extends CompoundButton {
    private static final String ACCESSIBILITY_EVENT_CLASS_NAME = "android.widget.Switch";
    private static final int[] CHECKED_STATE_SET = {16842912};
    private static final int MONOSPACE = 3;
    private static final int SANS = 1;
    private static final int SERIF = 2;
    private static final int THUMB_ANIMATION_DURATION = 250;
    private static final int TOUCH_MODE_DOWN = 1;
    private static final int TOUCH_MODE_DRAGGING = 2;
    private static final int TOUCH_MODE_IDLE = 0;
    private int mMinFlingVelocity;
    private Layout mOffLayout;
    private Layout mOnLayout;
    /* access modifiers changed from: private */
    public ThumbAnimation mPositionAnimator;
    private boolean mShowText;
    private boolean mSplitTrack;
    private int mSwitchBottom;
    private int mSwitchHeight;
    private int mSwitchLeft;
    private int mSwitchMinWidth;
    private int mSwitchPadding;
    private int mSwitchRight;
    private int mSwitchTop;
    private TransformationMethod mSwitchTransformationMethod;
    private int mSwitchWidth;
    private final Rect mTempRect;
    private ColorStateList mTextColors;
    private CharSequence mTextOff;
    private CharSequence mTextOn;
    private TextPaint mTextPaint;
    private Drawable mThumbDrawable;
    private float mThumbPosition;
    private int mThumbTextPadding;
    private int mThumbWidth;
    private final TintManager mTintManager;
    private int mTouchMode;
    private int mTouchSlop;
    private float mTouchX;
    private float mTouchY;
    private Drawable mTrackDrawable;
    private VelocityTracker mVelocityTracker;

    static /* synthetic */ ThumbAnimation access$102(SwitchCompat switchCompat, ThumbAnimation thumbAnimation) {
        ThumbAnimation thumbAnimation2 = thumbAnimation;
        ThumbAnimation thumbAnimation3 = thumbAnimation2;
        switchCompat.mPositionAnimator = thumbAnimation3;
        return thumbAnimation2;
    }

    public SwitchCompat(Context context) {
        this(context, null);
    }

    public SwitchCompat(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.switchStyle);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SwitchCompat(android.content.Context r15, android.util.AttributeSet r16, int r17) {
        /*
            r14 = this;
            r0 = r14
            r1 = r15
            r2 = r16
            r3 = r17
            r8 = r0
            r9 = r1
            r10 = r2
            r11 = r3
            r8.<init>(r9, r10, r11)
            r8 = r0
            android.view.VelocityTracker r9 = android.view.VelocityTracker.obtain()
            r8.mVelocityTracker = r9
            r8 = r0
            android.graphics.Rect r9 = new android.graphics.Rect
            r13 = r9
            r9 = r13
            r10 = r13
            r10.<init>()
            r8.mTempRect = r9
            r8 = r0
            android.text.TextPaint r9 = new android.text.TextPaint
            r13 = r9
            r9 = r13
            r10 = r13
            r11 = 1
            r10.<init>(r11)
            r8.mTextPaint = r9
            r8 = r0
            android.content.res.Resources r8 = r8.getResources()
            r4 = r8
            r8 = r0
            android.text.TextPaint r8 = r8.mTextPaint
            r9 = r4
            android.util.DisplayMetrics r9 = r9.getDisplayMetrics()
            float r9 = r9.density
            r8.density = r9
            r8 = r1
            r9 = r2
            int[] r10 = android.support.v7.appcompat.R.styleable.SwitchCompat
            r11 = r3
            r12 = 0
            android.support.v7.widget.TintTypedArray r8 = android.support.v7.widget.TintTypedArray.obtainStyledAttributes(r8, r9, r10, r11, r12)
            r5 = r8
            r8 = r0
            r9 = r5
            int r10 = android.support.v7.appcompat.R.styleable.SwitchCompat_android_thumb
            android.graphics.drawable.Drawable r9 = r9.getDrawable(r10)
            r8.mThumbDrawable = r9
            r8 = r0
            android.graphics.drawable.Drawable r8 = r8.mThumbDrawable
            if (r8 == 0) goto L_0x005e
            r8 = r0
            android.graphics.drawable.Drawable r8 = r8.mThumbDrawable
            r9 = r0
            r8.setCallback(r9)
        L_0x005e:
            r8 = r0
            r9 = r5
            int r10 = android.support.v7.appcompat.R.styleable.SwitchCompat_track
            android.graphics.drawable.Drawable r9 = r9.getDrawable(r10)
            r8.mTrackDrawable = r9
            r8 = r0
            android.graphics.drawable.Drawable r8 = r8.mTrackDrawable
            if (r8 == 0) goto L_0x0074
            r8 = r0
            android.graphics.drawable.Drawable r8 = r8.mTrackDrawable
            r9 = r0
            r8.setCallback(r9)
        L_0x0074:
            r8 = r0
            r9 = r5
            int r10 = android.support.v7.appcompat.R.styleable.SwitchCompat_android_textOn
            java.lang.CharSequence r9 = r9.getText(r10)
            r8.mTextOn = r9
            r8 = r0
            r9 = r5
            int r10 = android.support.v7.appcompat.R.styleable.SwitchCompat_android_textOff
            java.lang.CharSequence r9 = r9.getText(r10)
            r8.mTextOff = r9
            r8 = r0
            r9 = r5
            int r10 = android.support.v7.appcompat.R.styleable.SwitchCompat_showText
            r11 = 1
            boolean r9 = r9.getBoolean(r10, r11)
            r8.mShowText = r9
            r8 = r0
            r9 = r5
            int r10 = android.support.v7.appcompat.R.styleable.SwitchCompat_thumbTextPadding
            r11 = 0
            int r9 = r9.getDimensionPixelSize(r10, r11)
            r8.mThumbTextPadding = r9
            r8 = r0
            r9 = r5
            int r10 = android.support.v7.appcompat.R.styleable.SwitchCompat_switchMinWidth
            r11 = 0
            int r9 = r9.getDimensionPixelSize(r10, r11)
            r8.mSwitchMinWidth = r9
            r8 = r0
            r9 = r5
            int r10 = android.support.v7.appcompat.R.styleable.SwitchCompat_switchPadding
            r11 = 0
            int r9 = r9.getDimensionPixelSize(r10, r11)
            r8.mSwitchPadding = r9
            r8 = r0
            r9 = r5
            int r10 = android.support.v7.appcompat.R.styleable.SwitchCompat_splitTrack
            r11 = 0
            boolean r9 = r9.getBoolean(r10, r11)
            r8.mSplitTrack = r9
            r8 = r5
            int r9 = android.support.v7.appcompat.R.styleable.SwitchCompat_switchTextAppearance
            r10 = 0
            int r8 = r8.getResourceId(r9, r10)
            r6 = r8
            r8 = r6
            if (r8 == 0) goto L_0x00d1
            r8 = r0
            r9 = r1
            r10 = r6
            r8.setSwitchTextAppearance(r9, r10)
        L_0x00d1:
            r8 = r0
            r9 = r5
            android.support.v7.widget.TintManager r9 = r9.getTintManager()
            r8.mTintManager = r9
            r8 = r5
            r8.recycle()
            r8 = r1
            android.view.ViewConfiguration r8 = android.view.ViewConfiguration.get(r8)
            r7 = r8
            r8 = r0
            r9 = r7
            int r9 = r9.getScaledTouchSlop()
            r8.mTouchSlop = r9
            r8 = r0
            r9 = r7
            int r9 = r9.getScaledMinimumFlingVelocity()
            r8.mMinFlingVelocity = r9
            r8 = r0
            r8.refreshDrawableState()
            r8 = r0
            r9 = r0
            boolean r9 = r9.isChecked()
            r8.setChecked(r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v7.widget.SwitchCompat.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public void setSwitchTextAppearance(Context context, int i) {
        TransformationMethod transformationMethod;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i, R.styleable.TextAppearance);
        ColorStateList colorStateList = obtainStyledAttributes.getColorStateList(R.styleable.TextAppearance_android_textColor);
        if (colorStateList != null) {
            this.mTextColors = colorStateList;
        } else {
            this.mTextColors = getTextColors();
        }
        int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(R.styleable.TextAppearance_android_textSize, 0);
        if (!(dimensionPixelSize == 0 || ((float) dimensionPixelSize) == this.mTextPaint.getTextSize())) {
            this.mTextPaint.setTextSize((float) dimensionPixelSize);
            requestLayout();
        }
        setSwitchTypefaceByIndex(obtainStyledAttributes.getInt(R.styleable.TextAppearance_android_typeface, -1), obtainStyledAttributes.getInt(R.styleable.TextAppearance_android_textStyle, -1));
        if (obtainStyledAttributes.getBoolean(R.styleable.TextAppearance_textAllCaps, false)) {
            new AllCapsTransformationMethod(getContext());
            this.mSwitchTransformationMethod = transformationMethod;
        } else {
            this.mSwitchTransformationMethod = null;
        }
        obtainStyledAttributes.recycle();
    }

    private void setSwitchTypefaceByIndex(int i, int i2) {
        int i3 = i2;
        Typeface typeface = null;
        switch (i) {
            case 1:
                typeface = Typeface.SANS_SERIF;
                break;
            case 2:
                typeface = Typeface.SERIF;
                break;
            case 3:
                typeface = Typeface.MONOSPACE;
                break;
        }
        setSwitchTypeface(typeface, i3);
    }

    public void setSwitchTypeface(Typeface typeface, int i) {
        Typeface create;
        Typeface typeface2 = typeface;
        int i2 = i;
        if (i2 > 0) {
            if (typeface2 == null) {
                create = Typeface.defaultFromStyle(i2);
            } else {
                create = Typeface.create(typeface2, i2);
            }
            setSwitchTypeface(create);
            int style = i2 & ((create != null ? create.getStyle() : 0) ^ -1);
            this.mTextPaint.setFakeBoldText((style & 1) != 0);
            this.mTextPaint.setTextSkewX((style & 2) != 0 ? -0.25f : 0.0f);
            return;
        }
        this.mTextPaint.setFakeBoldText(false);
        this.mTextPaint.setTextSkewX(0.0f);
        setSwitchTypeface(typeface2);
    }

    public void setSwitchTypeface(Typeface typeface) {
        Typeface typeface2 = typeface;
        if (this.mTextPaint.getTypeface() != typeface2) {
            Typeface typeface3 = this.mTextPaint.setTypeface(typeface2);
            requestLayout();
            invalidate();
        }
    }

    public void setSwitchPadding(int i) {
        this.mSwitchPadding = i;
        requestLayout();
    }

    public int getSwitchPadding() {
        return this.mSwitchPadding;
    }

    public void setSwitchMinWidth(int i) {
        this.mSwitchMinWidth = i;
        requestLayout();
    }

    public int getSwitchMinWidth() {
        return this.mSwitchMinWidth;
    }

    public void setThumbTextPadding(int i) {
        this.mThumbTextPadding = i;
        requestLayout();
    }

    public int getThumbTextPadding() {
        return this.mThumbTextPadding;
    }

    public void setTrackDrawable(Drawable drawable) {
        this.mTrackDrawable = drawable;
        requestLayout();
    }

    public void setTrackResource(int i) {
        setTrackDrawable(this.mTintManager.getDrawable(i));
    }

    public Drawable getTrackDrawable() {
        return this.mTrackDrawable;
    }

    public void setThumbDrawable(Drawable drawable) {
        this.mThumbDrawable = drawable;
        requestLayout();
    }

    public void setThumbResource(int i) {
        setThumbDrawable(this.mTintManager.getDrawable(i));
    }

    public Drawable getThumbDrawable() {
        return this.mThumbDrawable;
    }

    public void setSplitTrack(boolean z) {
        this.mSplitTrack = z;
        invalidate();
    }

    public boolean getSplitTrack() {
        return this.mSplitTrack;
    }

    public CharSequence getTextOn() {
        return this.mTextOn;
    }

    public void setTextOn(CharSequence charSequence) {
        this.mTextOn = charSequence;
        requestLayout();
    }

    public CharSequence getTextOff() {
        return this.mTextOff;
    }

    public void setTextOff(CharSequence charSequence) {
        this.mTextOff = charSequence;
        requestLayout();
    }

    public void setShowText(boolean z) {
        boolean z2 = z;
        if (this.mShowText != z2) {
            this.mShowText = z2;
            requestLayout();
        }
    }

    public boolean getShowText() {
        return this.mShowText;
    }

    public void onMeasure(int i, int i2) {
        int i3;
        int i4;
        int i5;
        int i6;
        int i7 = i;
        int i8 = i2;
        if (this.mShowText) {
            if (this.mOnLayout == null) {
                this.mOnLayout = makeLayout(this.mTextOn);
            }
            if (this.mOffLayout == null) {
                this.mOffLayout = makeLayout(this.mTextOff);
            }
        }
        Rect rect = this.mTempRect;
        if (this.mThumbDrawable != null) {
            boolean padding = this.mThumbDrawable.getPadding(rect);
            i3 = (this.mThumbDrawable.getIntrinsicWidth() - rect.left) - rect.right;
            i4 = this.mThumbDrawable.getIntrinsicHeight();
        } else {
            i3 = 0;
            i4 = 0;
        }
        if (this.mShowText) {
            i5 = Math.max(this.mOnLayout.getWidth(), this.mOffLayout.getWidth()) + (this.mThumbTextPadding * 2);
        } else {
            i5 = 0;
        }
        this.mThumbWidth = Math.max(i5, i3);
        if (this.mTrackDrawable != null) {
            boolean padding2 = this.mTrackDrawable.getPadding(rect);
            i6 = this.mTrackDrawable.getIntrinsicHeight();
        } else {
            rect.setEmpty();
            i6 = 0;
        }
        int i9 = rect.left;
        int i10 = rect.right;
        if (this.mThumbDrawable != null) {
            Rect opticalBounds = DrawableUtils.getOpticalBounds(this.mThumbDrawable);
            i9 = Math.max(i9, opticalBounds.left);
            i10 = Math.max(i10, opticalBounds.right);
        }
        int max = Math.max(this.mSwitchMinWidth, (2 * this.mThumbWidth) + i9 + i10);
        int max2 = Math.max(i6, i4);
        this.mSwitchWidth = max;
        this.mSwitchHeight = max2;
        super.onMeasure(i7, i8);
        if (getMeasuredHeight() < max2) {
            setMeasuredDimension(ViewCompat.getMeasuredWidthAndState(this), max2);
        }
    }

    @TargetApi(14)
    public void onPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        AccessibilityEvent accessibilityEvent2 = accessibilityEvent;
        super.onPopulateAccessibilityEvent(accessibilityEvent2);
        CharSequence charSequence = isChecked() ? this.mTextOn : this.mTextOff;
        if (charSequence != null) {
            boolean add = accessibilityEvent2.getText().add(charSequence);
        }
    }

    private Layout makeLayout(CharSequence charSequence) {
        StaticLayout staticLayout;
        CharSequence charSequence2 = charSequence;
        CharSequence transformation = this.mSwitchTransformationMethod != null ? this.mSwitchTransformationMethod.getTransformation(charSequence2, this) : charSequence2;
        StaticLayout staticLayout2 = staticLayout;
        new StaticLayout(transformation, this.mTextPaint, transformation != null ? (int) Math.ceil((double) Layout.getDesiredWidth(transformation, this.mTextPaint)) : 0, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, true);
        return staticLayout2;
    }

    private boolean hitThumb(float f, float f2) {
        float f3 = f;
        float f4 = f2;
        if (this.mThumbDrawable == null) {
            return false;
        }
        int thumbOffset = getThumbOffset();
        boolean padding = this.mThumbDrawable.getPadding(this.mTempRect);
        int i = this.mSwitchTop - this.mTouchSlop;
        int i2 = (this.mSwitchLeft + thumbOffset) - this.mTouchSlop;
        return f3 > ((float) i2) && f3 < ((float) ((((i2 + this.mThumbWidth) + this.mTempRect.left) + this.mTempRect.right) + this.mTouchSlop)) && f4 > ((float) i) && f4 < ((float) (this.mSwitchBottom + this.mTouchSlop));
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        float f;
        MotionEvent motionEvent2 = motionEvent;
        this.mVelocityTracker.addMovement(motionEvent2);
        switch (MotionEventCompat.getActionMasked(motionEvent2)) {
            case 0:
                float x = motionEvent2.getX();
                float y = motionEvent2.getY();
                if (isEnabled() && hitThumb(x, y)) {
                    this.mTouchMode = 1;
                    this.mTouchX = x;
                    this.mTouchY = y;
                    break;
                }
            case 1:
            case 3:
                if (this.mTouchMode != 2) {
                    this.mTouchMode = 0;
                    this.mVelocityTracker.clear();
                    break;
                } else {
                    stopDrag(motionEvent2);
                    boolean onTouchEvent = super.onTouchEvent(motionEvent2);
                    return true;
                }
            case 2:
                switch (this.mTouchMode) {
                    case 2:
                        float x2 = motionEvent2.getX();
                        int thumbScrollRange = getThumbScrollRange();
                        float f2 = x2 - this.mTouchX;
                        if (thumbScrollRange != 0) {
                            f = f2 / ((float) thumbScrollRange);
                        } else {
                            f = f2 > 0.0f ? 1.0f : -1.0f;
                        }
                        if (ViewUtils.isLayoutRtl(this)) {
                            f = -f;
                        }
                        float constrain = constrain(this.mThumbPosition + f, 0.0f, 1.0f);
                        if (constrain != this.mThumbPosition) {
                            this.mTouchX = x2;
                            setThumbPosition(constrain);
                        }
                        return true;
                    case 1:
                        float x3 = motionEvent2.getX();
                        float y2 = motionEvent2.getY();
                        if (Math.abs(x3 - this.mTouchX) > ((float) this.mTouchSlop) || Math.abs(y2 - this.mTouchY) > ((float) this.mTouchSlop)) {
                            this.mTouchMode = 2;
                            getParent().requestDisallowInterceptTouchEvent(true);
                            this.mTouchX = x3;
                            this.mTouchY = y2;
                            return true;
                        }
                }
                break;
        }
        return super.onTouchEvent(motionEvent2);
    }

    private void cancelSuperTouch(MotionEvent motionEvent) {
        MotionEvent obtain = MotionEvent.obtain(motionEvent);
        obtain.setAction(3);
        boolean onTouchEvent = super.onTouchEvent(obtain);
        obtain.recycle();
    }

    private void stopDrag(MotionEvent motionEvent) {
        boolean z;
        MotionEvent motionEvent2 = motionEvent;
        this.mTouchMode = 0;
        boolean z2 = motionEvent2.getAction() == 1 && isEnabled();
        boolean isChecked = isChecked();
        if (z2) {
            this.mVelocityTracker.computeCurrentVelocity(1000);
            float xVelocity = this.mVelocityTracker.getXVelocity();
            if (Math.abs(xVelocity) > ((float) this.mMinFlingVelocity)) {
                z = ViewUtils.isLayoutRtl(this) ? xVelocity < 0.0f : xVelocity > 0.0f;
            } else {
                z = getTargetCheckedState();
            }
        } else {
            z = isChecked;
        }
        if (z != isChecked) {
            playSoundEffect(0);
        }
        setChecked(z);
        cancelSuperTouch(motionEvent2);
    }

    private void animateThumbToCheckedState(boolean z) {
        ThumbAnimation thumbAnimation;
        Animation.AnimationListener animationListener;
        boolean z2 = z;
        if (this.mPositionAnimator != null) {
            cancelPositionAnimator();
        }
        ThumbAnimation thumbAnimation2 = thumbAnimation;
        new ThumbAnimation(this.mThumbPosition, z2 ? 1.0f : 0.0f);
        this.mPositionAnimator = thumbAnimation2;
        this.mPositionAnimator.setDuration(250);
        final boolean z3 = z2;
        new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                if (SwitchCompat.this.mPositionAnimator == animation) {
                    SwitchCompat.this.setThumbPosition(z3 ? 1.0f : 0.0f);
                    ThumbAnimation access$102 = SwitchCompat.access$102(SwitchCompat.this, null);
                }
            }

            public void onAnimationRepeat(Animation animation) {
            }
        };
        this.mPositionAnimator.setAnimationListener(animationListener);
        startAnimation(this.mPositionAnimator);
    }

    private void cancelPositionAnimator() {
        if (this.mPositionAnimator != null) {
            clearAnimation();
            this.mPositionAnimator = null;
        }
    }

    private boolean getTargetCheckedState() {
        return this.mThumbPosition > 0.5f;
    }

    /* access modifiers changed from: private */
    public void setThumbPosition(float f) {
        this.mThumbPosition = f;
        invalidate();
    }

    public void toggle() {
        setChecked(!isChecked());
    }

    public void setChecked(boolean z) {
        super.setChecked(z);
        boolean isChecked = isChecked();
        if (getWindowToken() == null || !ViewCompat.isLaidOut(this) || !isShown()) {
            cancelPositionAnimator();
            setThumbPosition(isChecked ? 1.0f : 0.0f);
            return;
        }
        animateThumbToCheckedState(isChecked);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int width;
        int i5;
        int height;
        int i6;
        super.onLayout(z, i, i2, i3, i4);
        int i7 = 0;
        int i8 = 0;
        if (this.mThumbDrawable != null) {
            Rect rect = this.mTempRect;
            if (this.mTrackDrawable != null) {
                boolean padding = this.mTrackDrawable.getPadding(rect);
            } else {
                rect.setEmpty();
            }
            Rect opticalBounds = DrawableUtils.getOpticalBounds(this.mThumbDrawable);
            i7 = Math.max(0, opticalBounds.left - rect.left);
            i8 = Math.max(0, opticalBounds.right - rect.right);
        }
        if (ViewUtils.isLayoutRtl(this)) {
            i5 = getPaddingLeft() + i7;
            width = ((i5 + this.mSwitchWidth) - i7) - i8;
        } else {
            width = (getWidth() - getPaddingRight()) - i8;
            i5 = (width - this.mSwitchWidth) + i7 + i8;
        }
        switch (getGravity() & 112) {
            case 16:
                i6 = (((getPaddingTop() + getHeight()) - getPaddingBottom()) / 2) - (this.mSwitchHeight / 2);
                height = i6 + this.mSwitchHeight;
                break;
            case 80:
                height = getHeight() - getPaddingBottom();
                i6 = height - this.mSwitchHeight;
                break;
            default:
                i6 = getPaddingTop();
                height = i6 + this.mSwitchHeight;
                break;
        }
        this.mSwitchLeft = i5;
        this.mSwitchTop = i6;
        this.mSwitchBottom = height;
        this.mSwitchRight = width;
    }

    public void draw(Canvas canvas) {
        Rect rect;
        Canvas canvas2 = canvas;
        Rect rect2 = this.mTempRect;
        int i = this.mSwitchLeft;
        int i2 = this.mSwitchTop;
        int i3 = this.mSwitchRight;
        int i4 = this.mSwitchBottom;
        int thumbOffset = i + getThumbOffset();
        if (this.mThumbDrawable != null) {
            rect = DrawableUtils.getOpticalBounds(this.mThumbDrawable);
        } else {
            rect = DrawableUtils.INSETS_NONE;
        }
        if (this.mTrackDrawable != null) {
            boolean padding = this.mTrackDrawable.getPadding(rect2);
            thumbOffset += rect2.left;
            int i5 = i;
            int i6 = i2;
            int i7 = i3;
            int i8 = i4;
            if (rect != null) {
                if (rect.left > rect2.left) {
                    i5 += rect.left - rect2.left;
                }
                if (rect.top > rect2.top) {
                    i6 += rect.top - rect2.top;
                }
                if (rect.right > rect2.right) {
                    i7 -= rect.right - rect2.right;
                }
                if (rect.bottom > rect2.bottom) {
                    i8 -= rect.bottom - rect2.bottom;
                }
            }
            this.mTrackDrawable.setBounds(i5, i6, i7, i8);
        }
        if (this.mThumbDrawable != null) {
            boolean padding2 = this.mThumbDrawable.getPadding(rect2);
            int i9 = thumbOffset - rect2.left;
            int i10 = thumbOffset + this.mThumbWidth + rect2.right;
            this.mThumbDrawable.setBounds(i9, i2, i10, i4);
            Drawable background = getBackground();
            if (background != null) {
                DrawableCompat.setHotspotBounds(background, i9, i2, i10, i4);
            }
        }
        super.draw(canvas2);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        Layout layout;
        int width;
        Canvas canvas2 = canvas;
        super.onDraw(canvas2);
        Rect rect = this.mTempRect;
        Drawable drawable = this.mTrackDrawable;
        if (drawable != null) {
            boolean padding = drawable.getPadding(rect);
        } else {
            rect.setEmpty();
        }
        int i = this.mSwitchTop;
        int i2 = this.mSwitchBottom;
        int i3 = i + rect.top;
        int i4 = i2 - rect.bottom;
        Drawable drawable2 = this.mThumbDrawable;
        if (drawable != null) {
            if (!this.mSplitTrack || drawable2 == null) {
                drawable.draw(canvas2);
            } else {
                Rect opticalBounds = DrawableUtils.getOpticalBounds(drawable2);
                drawable2.copyBounds(rect);
                rect.left += opticalBounds.left;
                rect.right -= opticalBounds.right;
                int save = canvas2.save();
                boolean clipRect = canvas2.clipRect(rect, Region.Op.DIFFERENCE);
                drawable.draw(canvas2);
                canvas2.restoreToCount(save);
            }
        }
        int save2 = canvas2.save();
        if (drawable2 != null) {
            drawable2.draw(canvas2);
        }
        if (getTargetCheckedState()) {
            layout = this.mOnLayout;
        } else {
            layout = this.mOffLayout;
        }
        Layout layout2 = layout;
        if (layout2 != null) {
            int[] drawableState = getDrawableState();
            if (this.mTextColors != null) {
                this.mTextPaint.setColor(this.mTextColors.getColorForState(drawableState, 0));
            }
            this.mTextPaint.drawableState = drawableState;
            if (drawable2 != null) {
                Rect bounds = drawable2.getBounds();
                width = bounds.left + bounds.right;
            } else {
                width = getWidth();
            }
            canvas2.translate((float) ((width / 2) - (layout2.getWidth() / 2)), (float) (((i3 + i4) / 2) - (layout2.getHeight() / 2)));
            layout2.draw(canvas2);
        }
        canvas2.restoreToCount(save2);
    }

    public int getCompoundPaddingLeft() {
        if (!ViewUtils.isLayoutRtl(this)) {
            return super.getCompoundPaddingLeft();
        }
        int compoundPaddingLeft = super.getCompoundPaddingLeft() + this.mSwitchWidth;
        if (!TextUtils.isEmpty(getText())) {
            compoundPaddingLeft += this.mSwitchPadding;
        }
        return compoundPaddingLeft;
    }

    public int getCompoundPaddingRight() {
        if (ViewUtils.isLayoutRtl(this)) {
            return super.getCompoundPaddingRight();
        }
        int compoundPaddingRight = super.getCompoundPaddingRight() + this.mSwitchWidth;
        if (!TextUtils.isEmpty(getText())) {
            compoundPaddingRight += this.mSwitchPadding;
        }
        return compoundPaddingRight;
    }

    private int getThumbOffset() {
        float f;
        if (ViewUtils.isLayoutRtl(this)) {
            f = 1.0f - this.mThumbPosition;
        } else {
            f = this.mThumbPosition;
        }
        return (int) ((f * ((float) getThumbScrollRange())) + 0.5f);
    }

    private int getThumbScrollRange() {
        Rect rect;
        if (this.mTrackDrawable == null) {
            return 0;
        }
        Rect rect2 = this.mTempRect;
        boolean padding = this.mTrackDrawable.getPadding(rect2);
        if (this.mThumbDrawable != null) {
            rect = DrawableUtils.getOpticalBounds(this.mThumbDrawable);
        } else {
            rect = DrawableUtils.INSETS_NONE;
        }
        return ((((this.mSwitchWidth - this.mThumbWidth) - rect2.left) - rect2.right) - rect.left) - rect.right;
    }

    /* access modifiers changed from: protected */
    public int[] onCreateDrawableState(int i) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i + 1);
        if (isChecked()) {
            int[] mergeDrawableStates = mergeDrawableStates(onCreateDrawableState, CHECKED_STATE_SET);
        }
        return onCreateDrawableState;
    }

    /* access modifiers changed from: protected */
    public void drawableStateChanged() {
        super.drawableStateChanged();
        int[] drawableState = getDrawableState();
        if (this.mThumbDrawable != null) {
            boolean state = this.mThumbDrawable.setState(drawableState);
        }
        if (this.mTrackDrawable != null) {
            boolean state2 = this.mTrackDrawable.setState(drawableState);
        }
        invalidate();
    }

    public void drawableHotspotChanged(float f, float f2) {
        float f3 = f;
        float f4 = f2;
        if (Build.VERSION.SDK_INT >= 21) {
            super.drawableHotspotChanged(f3, f4);
        }
        if (this.mThumbDrawable != null) {
            DrawableCompat.setHotspot(this.mThumbDrawable, f3, f4);
        }
        if (this.mTrackDrawable != null) {
            DrawableCompat.setHotspot(this.mTrackDrawable, f3, f4);
        }
    }

    /* access modifiers changed from: protected */
    public boolean verifyDrawable(Drawable drawable) {
        Drawable drawable2 = drawable;
        return super.verifyDrawable(drawable2) || drawable2 == this.mThumbDrawable || drawable2 == this.mTrackDrawable;
    }

    public void jumpDrawablesToCurrentState() {
        if (Build.VERSION.SDK_INT >= 11) {
            super.jumpDrawablesToCurrentState();
            if (this.mThumbDrawable != null) {
                this.mThumbDrawable.jumpToCurrentState();
            }
            if (this.mTrackDrawable != null) {
                this.mTrackDrawable.jumpToCurrentState();
            }
            cancelPositionAnimator();
        }
    }

    @TargetApi(14)
    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        AccessibilityEvent accessibilityEvent2 = accessibilityEvent;
        super.onInitializeAccessibilityEvent(accessibilityEvent2);
        accessibilityEvent2.setClassName(ACCESSIBILITY_EVENT_CLASS_NAME);
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        StringBuilder sb;
        AccessibilityNodeInfo accessibilityNodeInfo2 = accessibilityNodeInfo;
        if (Build.VERSION.SDK_INT >= 14) {
            super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo2);
            accessibilityNodeInfo2.setClassName(ACCESSIBILITY_EVENT_CLASS_NAME);
            CharSequence charSequence = isChecked() ? this.mTextOn : this.mTextOff;
            if (!TextUtils.isEmpty(charSequence)) {
                CharSequence text = accessibilityNodeInfo2.getText();
                if (TextUtils.isEmpty(text)) {
                    accessibilityNodeInfo2.setText(charSequence);
                    return;
                }
                new StringBuilder();
                StringBuilder sb2 = sb;
                StringBuilder append = sb2.append(text).append(' ').append(charSequence);
                accessibilityNodeInfo2.setText(sb2);
            }
        }
    }

    private static float constrain(float f, float f2, float f3) {
        float f4 = f;
        float f5 = f2;
        float f6 = f3;
        return f4 < f5 ? f5 : f4 > f6 ? f6 : f4;
    }

    private class ThumbAnimation extends Animation {
        final float mDiff;
        final float mEndPosition;
        final float mStartPosition;
        final /* synthetic */ SwitchCompat this$0;

        private ThumbAnimation(SwitchCompat switchCompat, float f, float f2) {
            float f3 = f;
            float f4 = f2;
            this.this$0 = switchCompat;
            this.mStartPosition = f3;
            this.mEndPosition = f4;
            this.mDiff = f4 - f3;
        }

        /* access modifiers changed from: protected */
        public void applyTransformation(float f, Transformation transformation) {
            this.this$0.setThumbPosition(this.mStartPosition + (this.mDiff * f));
        }
    }
}
