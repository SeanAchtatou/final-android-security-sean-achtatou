package android.support.v7.util;

import android.util.SparseArray;
import java.lang.reflect.Array;

class TileList<T> {
    Tile<T> mLastAccessedTile;
    final int mTileSize;
    private final SparseArray<Tile<T>> mTiles;

    public TileList(int i) {
        SparseArray<Tile<T>> sparseArray;
        new SparseArray<>(10);
        this.mTiles = sparseArray;
        this.mTileSize = i;
    }

    public T getItemAt(int i) {
        int i2 = i;
        if (this.mLastAccessedTile == null || !this.mLastAccessedTile.containsPosition(i2)) {
            int indexOfKey = this.mTiles.indexOfKey(i2 - (i2 % this.mTileSize));
            if (indexOfKey < 0) {
                return null;
            }
            this.mLastAccessedTile = this.mTiles.valueAt(indexOfKey);
        }
        return this.mLastAccessedTile.getByPosition(i2);
    }

    public int size() {
        return this.mTiles.size();
    }

    public void clear() {
        this.mTiles.clear();
    }

    public Tile<T> getAtIndex(int i) {
        return this.mTiles.valueAt(i);
    }

    public Tile<T> addOrReplace(Tile<T> tile) {
        Tile<T> tile2 = tile;
        int indexOfKey = this.mTiles.indexOfKey(tile2.mStartPosition);
        if (indexOfKey < 0) {
            this.mTiles.put(tile2.mStartPosition, tile2);
            return null;
        }
        Tile<T> valueAt = this.mTiles.valueAt(indexOfKey);
        this.mTiles.setValueAt(indexOfKey, tile2);
        if (this.mLastAccessedTile == valueAt) {
            this.mLastAccessedTile = tile2;
        }
        return valueAt;
    }

    public Tile<T> removeAtPos(int i) {
        int i2 = i;
        Tile<T> tile = this.mTiles.get(i2);
        if (this.mLastAccessedTile == tile) {
            this.mLastAccessedTile = null;
        }
        this.mTiles.delete(i2);
        return tile;
    }

    public static class Tile<T> {
        public int mItemCount;
        public final T[] mItems;
        Tile<T> mNext;
        public int mStartPosition;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
         arg types: [java.lang.Class<T>, int]
         candidates:
          ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
          ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException} */
        public Tile(Class<T> cls, int i) {
            this.mItems = (Object[]) Array.newInstance((Class<?>) cls, i);
        }

        /* access modifiers changed from: package-private */
        public boolean containsPosition(int i) {
            int i2 = i;
            return this.mStartPosition <= i2 && i2 < this.mStartPosition + this.mItemCount;
        }

        /* access modifiers changed from: package-private */
        public T getByPosition(int i) {
            return this.mItems[i - this.mStartPosition];
        }
    }
}
