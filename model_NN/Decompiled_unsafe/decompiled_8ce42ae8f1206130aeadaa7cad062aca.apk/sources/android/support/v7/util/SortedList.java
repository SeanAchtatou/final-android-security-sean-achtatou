package android.support.v7.util;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;

public class SortedList<T> {
    private static final int CAPACITY_GROWTH = 10;
    private static final int DELETION = 2;
    private static final int INSERTION = 1;
    public static final int INVALID_POSITION = -1;
    private static final int LOOKUP = 4;
    private static final int MIN_CAPACITY = 10;
    private BatchedCallback mBatchedCallback;
    private Callback mCallback;
    T[] mData;
    private int mMergedSize;
    private T[] mOldData;
    private int mOldDataSize;
    private int mOldDataStart;
    private int mSize;
    private final Class<T> mTClass;

    public static abstract class Callback<T2> implements Comparator<T2> {
        public abstract boolean areContentsTheSame(T2 t2, T2 t22);

        public abstract boolean areItemsTheSame(T2 t2, T2 t22);

        public abstract int compare(T2 t2, T2 t22);

        public abstract void onChanged(int i, int i2);

        public abstract void onInserted(int i, int i2);

        public abstract void onMoved(int i, int i2);

        public abstract void onRemoved(int i, int i2);
    }

    public SortedList(Class<T> cls, Callback<T> callback) {
        this(cls, callback, 10);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<T>, int]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException} */
    public SortedList(Class<T> cls, Callback<T> callback, int i) {
        Class<T> cls2 = cls;
        this.mTClass = cls2;
        this.mData = (Object[]) Array.newInstance((Class<?>) cls2, i);
        this.mCallback = callback;
        this.mSize = 0;
    }

    public int size() {
        return this.mSize;
    }

    public int add(T t) {
        throwIfMerging();
        return add(t, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<T>, int]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException} */
    public void addAll(T[] tArr, boolean z) {
        T[] tArr2 = tArr;
        boolean z2 = z;
        throwIfMerging();
        if (tArr2.length != 0) {
            if (z2) {
                addAllInternal(tArr2);
                return;
            }
            Object[] objArr = (Object[]) Array.newInstance((Class<?>) this.mTClass, tArr2.length);
            System.arraycopy(tArr2, 0, objArr, 0, tArr2.length);
            addAllInternal(objArr);
        }
    }

    public void addAll(T... tArr) {
        addAll(tArr, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<T>, int]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException} */
    public void addAll(Collection<T> collection) {
        Collection<T> collection2 = collection;
        addAll(collection2.toArray((Object[]) Array.newInstance((Class<?>) this.mTClass, collection2.size())), true);
    }

    private void addAllInternal(T[] tArr) {
        T[] tArr2 = tArr;
        boolean z = !(this.mCallback instanceof BatchedCallback);
        if (z) {
            beginBatchedUpdates();
        }
        this.mOldData = this.mData;
        this.mOldDataStart = 0;
        this.mOldDataSize = this.mSize;
        Arrays.sort(tArr2, this.mCallback);
        int deduplicate = deduplicate(tArr2);
        if (this.mSize == 0) {
            this.mData = tArr2;
            this.mSize = deduplicate;
            this.mMergedSize = deduplicate;
            this.mCallback.onInserted(0, deduplicate);
        } else {
            merge(tArr2, deduplicate);
        }
        this.mOldData = null;
        if (z) {
            endBatchedUpdates();
        }
    }

    private int deduplicate(T[] tArr) {
        Throwable th;
        Throwable th2;
        T[] tArr2 = tArr;
        if (tArr2.length == 0) {
            Throwable th3 = th2;
            new IllegalArgumentException("Input array must be non-empty");
            throw th3;
        }
        int i = 0;
        int i2 = 1;
        for (int i3 = 1; i3 < tArr2.length; i3++) {
            T t = tArr2[i3];
            int compare = this.mCallback.compare(tArr2[i], t);
            if (compare > 0) {
                Throwable th4 = th;
                new IllegalArgumentException("Input must be sorted in ascending order.");
                throw th4;
            }
            if (compare == 0) {
                int findSameItem = findSameItem(t, tArr2, i, i2);
                if (findSameItem != -1) {
                    tArr2[findSameItem] = t;
                } else {
                    if (i2 != i3) {
                        tArr2[i2] = t;
                    }
                    i2++;
                }
            } else {
                if (i2 != i3) {
                    tArr2[i2] = t;
                }
                int i4 = i2;
                i2++;
                i = i4;
            }
        }
        return i2;
    }

    private int findSameItem(T t, T[] tArr, int i, int i2) {
        T t2 = t;
        T[] tArr2 = tArr;
        int i3 = i2;
        for (int i4 = i; i4 < i3; i4++) {
            if (this.mCallback.areItemsTheSame(tArr2[i4], t2)) {
                return i4;
            }
        }
        return -1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<T>, int]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException} */
    private void merge(T[] tArr, int i) {
        T[] tArr2 = tArr;
        int i2 = i;
        this.mData = (Object[]) Array.newInstance((Class<?>) this.mTClass, this.mSize + i2 + 10);
        this.mMergedSize = 0;
        int i3 = 0;
        while (true) {
            if (this.mOldDataStart >= this.mOldDataSize && i3 >= i2) {
                return;
            }
            if (this.mOldDataStart == this.mOldDataSize) {
                int i4 = i2 - i3;
                System.arraycopy(tArr2, i3, this.mData, this.mMergedSize, i4);
                this.mMergedSize = this.mMergedSize + i4;
                this.mSize = this.mSize + i4;
                this.mCallback.onInserted(this.mMergedSize - i4, i4);
                return;
            } else if (i3 == i2) {
                int i5 = this.mOldDataSize - this.mOldDataStart;
                System.arraycopy(this.mOldData, this.mOldDataStart, this.mData, this.mMergedSize, i5);
                this.mMergedSize = this.mMergedSize + i5;
                return;
            } else {
                T t = this.mOldData[this.mOldDataStart];
                T t2 = tArr2[i3];
                int compare = this.mCallback.compare(t, t2);
                if (compare > 0) {
                    T[] tArr3 = this.mData;
                    int i6 = this.mMergedSize;
                    this.mMergedSize = i6 + 1;
                    tArr3[i6] = t2;
                    this.mSize = this.mSize + 1;
                    i3++;
                    this.mCallback.onInserted(this.mMergedSize - 1, 1);
                } else if (compare != 0 || !this.mCallback.areItemsTheSame(t, t2)) {
                    T[] tArr4 = this.mData;
                    int i7 = this.mMergedSize;
                    this.mMergedSize = i7 + 1;
                    tArr4[i7] = t;
                    this.mOldDataStart = this.mOldDataStart + 1;
                } else {
                    T[] tArr5 = this.mData;
                    int i8 = this.mMergedSize;
                    this.mMergedSize = i8 + 1;
                    tArr5[i8] = t2;
                    i3++;
                    this.mOldDataStart = this.mOldDataStart + 1;
                    if (!this.mCallback.areContentsTheSame(t, t2)) {
                        this.mCallback.onChanged(this.mMergedSize - 1, 1);
                    }
                }
            }
        }
    }

    private void throwIfMerging() {
        Throwable th;
        if (this.mOldData != null) {
            Throwable th2 = th;
            new IllegalStateException("Cannot call this method from within addAll");
            throw th2;
        }
    }

    public void beginBatchedUpdates() {
        BatchedCallback batchedCallback;
        throwIfMerging();
        if (!(this.mCallback instanceof BatchedCallback)) {
            if (this.mBatchedCallback == null) {
                new BatchedCallback(this.mCallback);
                this.mBatchedCallback = batchedCallback;
            }
            this.mCallback = this.mBatchedCallback;
        }
    }

    public void endBatchedUpdates() {
        throwIfMerging();
        if (this.mCallback instanceof BatchedCallback) {
            ((BatchedCallback) this.mCallback).dispatchLastEvent();
        }
        if (this.mCallback == this.mBatchedCallback) {
            this.mCallback = this.mBatchedCallback.mWrappedCallback;
        }
    }

    private int add(T t, boolean z) {
        T t2 = t;
        boolean z2 = z;
        int findIndexOf = findIndexOf(t2, this.mData, 0, this.mSize, 1);
        if (findIndexOf == -1) {
            findIndexOf = 0;
        } else if (findIndexOf < this.mSize) {
            T t3 = this.mData[findIndexOf];
            if (this.mCallback.areItemsTheSame(t3, t2)) {
                if (this.mCallback.areContentsTheSame(t3, t2)) {
                    this.mData[findIndexOf] = t2;
                    return findIndexOf;
                }
                this.mData[findIndexOf] = t2;
                this.mCallback.onChanged(findIndexOf, 1);
                return findIndexOf;
            }
        }
        addToData(findIndexOf, t2);
        if (z2) {
            this.mCallback.onInserted(findIndexOf, 1);
        }
        return findIndexOf;
    }

    public boolean remove(T t) {
        throwIfMerging();
        return remove(t, true);
    }

    public T removeItemAt(int i) {
        int i2 = i;
        throwIfMerging();
        removeItemAtIndex(i2, true);
        return get(i2);
    }

    private boolean remove(T t, boolean z) {
        boolean z2 = z;
        int findIndexOf = findIndexOf(t, this.mData, 0, this.mSize, 2);
        if (findIndexOf == -1) {
            return false;
        }
        removeItemAtIndex(findIndexOf, z2);
        return true;
    }

    private void removeItemAtIndex(int i, boolean z) {
        int i2 = i;
        System.arraycopy(this.mData, i2 + 1, this.mData, i2, (this.mSize - i2) - 1);
        this.mSize = this.mSize - 1;
        this.mData[this.mSize] = null;
        if (z) {
            this.mCallback.onRemoved(i2, 1);
        }
    }

    public void updateItemAt(int i, T t) {
        int i2 = i;
        T t2 = t;
        throwIfMerging();
        T t3 = get(i2);
        boolean z = t3 == t2 || !this.mCallback.areContentsTheSame(t3, t2);
        if (t3 == t2 || this.mCallback.compare(t3, t2) != 0) {
            if (z) {
                this.mCallback.onChanged(i2, 1);
            }
            removeItemAtIndex(i2, false);
            int add = add(t2, false);
            if (i2 != add) {
                this.mCallback.onMoved(i2, add);
                return;
            }
            return;
        }
        this.mData[i2] = t2;
        if (z) {
            this.mCallback.onChanged(i2, 1);
        }
    }

    public void recalculatePositionOfItemAt(int i) {
        int i2 = i;
        throwIfMerging();
        removeItemAtIndex(i2, false);
        int add = add(get(i2), false);
        if (i2 != add) {
            this.mCallback.onMoved(i2, add);
        }
    }

    public T get(int i) throws IndexOutOfBoundsException {
        Throwable th;
        StringBuilder sb;
        int i2 = i;
        if (i2 >= this.mSize || i2 < 0) {
            Throwable th2 = th;
            new StringBuilder();
            new IndexOutOfBoundsException(sb.append("Asked to get item at ").append(i2).append(" but size is ").append(this.mSize).toString());
            throw th2;
        } else if (this.mOldData == null || i2 < this.mMergedSize) {
            return this.mData[i2];
        } else {
            return this.mOldData[(i2 - this.mMergedSize) + this.mOldDataStart];
        }
    }

    public int indexOf(T t) {
        T t2 = t;
        if (this.mOldData == null) {
            return findIndexOf(t2, this.mData, 0, this.mSize, 4);
        }
        int findIndexOf = findIndexOf(t2, this.mData, 0, this.mMergedSize, 4);
        if (findIndexOf != -1) {
            return findIndexOf;
        }
        int findIndexOf2 = findIndexOf(t2, this.mOldData, this.mOldDataStart, this.mOldDataSize, 4);
        if (findIndexOf2 != -1) {
            return (findIndexOf2 - this.mOldDataStart) + this.mMergedSize;
        }
        return -1;
    }

    private int findIndexOf(T t, T[] tArr, int i, int i2, int i3) {
        T t2 = t;
        T[] tArr2 = tArr;
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        while (i4 < i5) {
            int i7 = (i4 + i5) / 2;
            T t3 = tArr2[i7];
            int compare = this.mCallback.compare(t3, t2);
            if (compare < 0) {
                i4 = i7 + 1;
            } else if (compare != 0) {
                i5 = i7;
            } else if (this.mCallback.areItemsTheSame(t3, t2)) {
                return i7;
            } else {
                int linearEqualitySearch = linearEqualitySearch(t2, i7, i4, i5);
                if (i6 != 1) {
                    return linearEqualitySearch;
                }
                return linearEqualitySearch == -1 ? i7 : linearEqualitySearch;
            }
        }
        return i6 == 1 ? i4 : -1;
    }

    private int linearEqualitySearch(T t, int i, int i2, int i3) {
        T t2 = t;
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        int i7 = i4 - 1;
        while (i7 >= i5) {
            T t3 = this.mData[i7];
            if (this.mCallback.compare(t3, t2) != 0) {
                break;
            } else if (this.mCallback.areItemsTheSame(t3, t2)) {
                return i7;
            } else {
                i7--;
            }
        }
        int i8 = i4 + 1;
        while (i8 < i6) {
            T t4 = this.mData[i8];
            if (this.mCallback.compare(t4, t2) != 0) {
                break;
            } else if (this.mCallback.areItemsTheSame(t4, t2)) {
                return i8;
            } else {
                i8++;
            }
        }
        return -1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<T>, int]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException} */
    private void addToData(int i, T t) {
        Throwable th;
        StringBuilder sb;
        int i2 = i;
        T t2 = t;
        if (i2 > this.mSize) {
            Throwable th2 = th;
            new StringBuilder();
            new IndexOutOfBoundsException(sb.append("cannot add item to ").append(i2).append(" because size is ").append(this.mSize).toString());
            throw th2;
        }
        if (this.mSize == this.mData.length) {
            T[] tArr = (Object[]) Array.newInstance((Class<?>) this.mTClass, this.mData.length + 10);
            System.arraycopy(this.mData, 0, tArr, 0, i2);
            tArr[i2] = t2;
            System.arraycopy(this.mData, i2, tArr, i2 + 1, this.mSize - i2);
            this.mData = tArr;
        } else {
            System.arraycopy(this.mData, i2, this.mData, i2 + 1, this.mSize - i2);
            this.mData[i2] = t2;
        }
        this.mSize = this.mSize + 1;
    }

    public void clear() {
        throwIfMerging();
        if (this.mSize != 0) {
            int i = this.mSize;
            Arrays.fill(this.mData, 0, i, (Object) null);
            this.mSize = 0;
            this.mCallback.onRemoved(0, i);
        }
    }

    public static class BatchedCallback<T2> extends Callback<T2> {
        static final int TYPE_ADD = 1;
        static final int TYPE_CHANGE = 3;
        static final int TYPE_MOVE = 4;
        static final int TYPE_NONE = 0;
        static final int TYPE_REMOVE = 2;
        int mLastEventCount = -1;
        int mLastEventPosition = -1;
        int mLastEventType = 0;
        /* access modifiers changed from: private */
        public final Callback<T2> mWrappedCallback;

        public BatchedCallback(Callback<T2> callback) {
            this.mWrappedCallback = callback;
        }

        public int compare(T2 t2, T2 t22) {
            return this.mWrappedCallback.compare(t2, t22);
        }

        public void onInserted(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            if (this.mLastEventType != 1 || i3 < this.mLastEventPosition || i3 > this.mLastEventPosition + this.mLastEventCount) {
                dispatchLastEvent();
                this.mLastEventPosition = i3;
                this.mLastEventCount = i4;
                this.mLastEventType = 1;
                return;
            }
            this.mLastEventCount = this.mLastEventCount + i4;
            this.mLastEventPosition = Math.min(i3, this.mLastEventPosition);
        }

        public void onRemoved(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            if (this.mLastEventType == 2 && this.mLastEventPosition == i3) {
                this.mLastEventCount = this.mLastEventCount + i4;
                return;
            }
            dispatchLastEvent();
            this.mLastEventPosition = i3;
            this.mLastEventCount = i4;
            this.mLastEventType = 2;
        }

        public void onMoved(int i, int i2) {
            dispatchLastEvent();
            this.mWrappedCallback.onMoved(i, i2);
        }

        public void onChanged(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            if (this.mLastEventType != 3 || i3 > this.mLastEventPosition + this.mLastEventCount || i3 + i4 < this.mLastEventPosition) {
                dispatchLastEvent();
                this.mLastEventPosition = i3;
                this.mLastEventCount = i4;
                this.mLastEventType = 3;
                return;
            }
            this.mLastEventPosition = Math.min(i3, this.mLastEventPosition);
            this.mLastEventCount = Math.max(this.mLastEventPosition + this.mLastEventCount, i3 + i4) - this.mLastEventPosition;
        }

        public boolean areContentsTheSame(T2 t2, T2 t22) {
            return this.mWrappedCallback.areContentsTheSame(t2, t22);
        }

        public boolean areItemsTheSame(T2 t2, T2 t22) {
            return this.mWrappedCallback.areItemsTheSame(t2, t22);
        }

        public void dispatchLastEvent() {
            if (this.mLastEventType != 0) {
                switch (this.mLastEventType) {
                    case 1:
                        this.mWrappedCallback.onInserted(this.mLastEventPosition, this.mLastEventCount);
                        break;
                    case 2:
                        this.mWrappedCallback.onRemoved(this.mLastEventPosition, this.mLastEventCount);
                        break;
                    case 3:
                        this.mWrappedCallback.onChanged(this.mLastEventPosition, this.mLastEventCount);
                        break;
                }
                this.mLastEventType = 0;
            }
        }
    }
}
