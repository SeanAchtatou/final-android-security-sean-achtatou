package android.support.design.widget;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.lang.ref.WeakReference;

class SnackbarManager {
    private static final int LONG_DURATION_MS = 2750;
    private static final int MSG_TIMEOUT = 0;
    private static final int SHORT_DURATION_MS = 1500;
    private static SnackbarManager sSnackbarManager;
    private SnackbarRecord mCurrentSnackbar;
    private final Handler mHandler;
    private final Object mLock;
    private SnackbarRecord mNextSnackbar;

    interface Callback {
        void dismiss(int i);

        void show();
    }

    static SnackbarManager getInstance() {
        SnackbarManager snackbarManager;
        if (sSnackbarManager == null) {
            new SnackbarManager();
            sSnackbarManager = snackbarManager;
        }
        return sSnackbarManager;
    }

    private SnackbarManager() {
        Object obj;
        Handler handler;
        Handler.Callback callback;
        new Object();
        this.mLock = obj;
        new Handler.Callback() {
            public boolean handleMessage(Message message) {
                Message message2 = message;
                switch (message2.what) {
                    case 0:
                        SnackbarManager.this.handleTimeout((SnackbarRecord) message2.obj);
                        return true;
                    default:
                        return false;
                }
            }
        };
        new Handler(Looper.getMainLooper(), callback);
        this.mHandler = handler;
    }

    /* JADX INFO: finally extract failed */
    public void show(int i, Callback callback) {
        SnackbarRecord snackbarRecord;
        int i2 = i;
        Callback callback2 = callback;
        Object obj = this.mLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                if (isCurrentSnackbarLocked(callback2)) {
                    int access$102 = SnackbarRecord.access$102(this.mCurrentSnackbar, i2);
                    this.mHandler.removeCallbacksAndMessages(this.mCurrentSnackbar);
                    scheduleTimeoutLocked(this.mCurrentSnackbar);
                    return;
                }
                if (isNextSnackbarLocked(callback2)) {
                    int access$1022 = SnackbarRecord.access$102(this.mNextSnackbar, i2);
                } else {
                    new SnackbarRecord(i2, callback2);
                    this.mNextSnackbar = snackbarRecord;
                }
                if (this.mCurrentSnackbar == null || !cancelSnackbarLocked(this.mCurrentSnackbar, 4)) {
                    this.mCurrentSnackbar = null;
                    showNextSnackbarLocked();
                    return;
                }
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public void dismiss(Callback callback, int i) {
        Callback callback2 = callback;
        int i2 = i;
        Object obj = this.mLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                if (isCurrentSnackbarLocked(callback2)) {
                    boolean cancelSnackbarLocked = cancelSnackbarLocked(this.mCurrentSnackbar, i2);
                } else if (isNextSnackbarLocked(callback2)) {
                    boolean cancelSnackbarLocked2 = cancelSnackbarLocked(this.mNextSnackbar, i2);
                }
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    public void onDismissed(Callback callback) {
        Callback callback2 = callback;
        Object obj = this.mLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                if (isCurrentSnackbarLocked(callback2)) {
                    this.mCurrentSnackbar = null;
                    if (this.mNextSnackbar != null) {
                        showNextSnackbarLocked();
                    }
                }
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public void onShown(Callback callback) {
        Callback callback2 = callback;
        Object obj = this.mLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                if (isCurrentSnackbarLocked(callback2)) {
                    scheduleTimeoutLocked(this.mCurrentSnackbar);
                }
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    public void cancelTimeout(Callback callback) {
        Callback callback2 = callback;
        Object obj = this.mLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                if (isCurrentSnackbarLocked(callback2)) {
                    this.mHandler.removeCallbacksAndMessages(this.mCurrentSnackbar);
                }
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public void restoreTimeout(Callback callback) {
        Callback callback2 = callback;
        Object obj = this.mLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                if (isCurrentSnackbarLocked(callback2)) {
                    scheduleTimeoutLocked(this.mCurrentSnackbar);
                }
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    public boolean isCurrent(Callback callback) {
        Callback callback2 = callback;
        Object obj = this.mLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                boolean isCurrentSnackbarLocked = isCurrentSnackbarLocked(callback2);
                return isCurrentSnackbarLocked;
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public boolean isCurrentOrNext(Callback callback) {
        Callback callback2 = callback;
        Object obj = this.mLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                boolean z = isCurrentSnackbarLocked(callback2) || isNextSnackbarLocked(callback2);
                return z;
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }

    private static class SnackbarRecord {
        /* access modifiers changed from: private */
        public final WeakReference<Callback> callback;
        /* access modifiers changed from: private */
        public int duration;

        static /* synthetic */ int access$102(SnackbarRecord snackbarRecord, int i) {
            int i2 = i;
            int i3 = i2;
            snackbarRecord.duration = i3;
            return i2;
        }

        SnackbarRecord(int i, Callback callback2) {
            WeakReference<Callback> weakReference;
            new WeakReference<>(callback2);
            this.callback = weakReference;
            this.duration = i;
        }

        /* access modifiers changed from: package-private */
        public boolean isSnackbar(Callback callback2) {
            Callback callback3 = callback2;
            return callback3 != null && this.callback.get() == callback3;
        }
    }

    private void showNextSnackbarLocked() {
        if (this.mNextSnackbar != null) {
            this.mCurrentSnackbar = this.mNextSnackbar;
            this.mNextSnackbar = null;
            Callback callback = (Callback) this.mCurrentSnackbar.callback.get();
            if (callback != null) {
                callback.show();
            } else {
                this.mCurrentSnackbar = null;
            }
        }
    }

    private boolean cancelSnackbarLocked(SnackbarRecord snackbarRecord, int i) {
        int i2 = i;
        Callback callback = (Callback) snackbarRecord.callback.get();
        if (callback == null) {
            return false;
        }
        callback.dismiss(i2);
        return true;
    }

    private boolean isCurrentSnackbarLocked(Callback callback) {
        return this.mCurrentSnackbar != null && this.mCurrentSnackbar.isSnackbar(callback);
    }

    private boolean isNextSnackbarLocked(Callback callback) {
        return this.mNextSnackbar != null && this.mNextSnackbar.isSnackbar(callback);
    }

    private void scheduleTimeoutLocked(SnackbarRecord snackbarRecord) {
        SnackbarRecord snackbarRecord2 = snackbarRecord;
        if (snackbarRecord2.duration != -2) {
            int i = LONG_DURATION_MS;
            if (snackbarRecord2.duration > 0) {
                i = snackbarRecord2.duration;
            } else if (snackbarRecord2.duration == -1) {
                i = SHORT_DURATION_MS;
            }
            this.mHandler.removeCallbacksAndMessages(snackbarRecord2);
            boolean sendMessageDelayed = this.mHandler.sendMessageDelayed(Message.obtain(this.mHandler, 0, snackbarRecord2), (long) i);
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: private */
    public void handleTimeout(SnackbarRecord snackbarRecord) {
        SnackbarRecord snackbarRecord2 = snackbarRecord;
        Object obj = this.mLock;
        Object obj2 = obj;
        synchronized (obj) {
            try {
                if (this.mCurrentSnackbar == snackbarRecord2 || this.mNextSnackbar == snackbarRecord2) {
                    boolean cancelSnackbarLocked = cancelSnackbarLocked(snackbarRecord2, 2);
                }
            } catch (Throwable th) {
                Throwable th2 = th;
                Object obj3 = obj2;
                throw th2;
            }
        }
    }
}
