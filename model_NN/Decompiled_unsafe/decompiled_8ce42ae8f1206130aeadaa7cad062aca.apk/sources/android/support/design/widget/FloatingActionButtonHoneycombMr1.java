package android.support.design.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButtonImpl;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.ViewPropertyAnimator;

class FloatingActionButtonHoneycombMr1 extends FloatingActionButtonEclairMr1 {
    private boolean mIsHiding;

    static /* synthetic */ boolean access$002(FloatingActionButtonHoneycombMr1 floatingActionButtonHoneycombMr1, boolean z) {
        boolean z2 = z;
        boolean z3 = z2;
        floatingActionButtonHoneycombMr1.mIsHiding = z3;
        return z2;
    }

    FloatingActionButtonHoneycombMr1(View view, ShadowViewDelegate shadowViewDelegate) {
        super(view, shadowViewDelegate);
    }

    /* access modifiers changed from: package-private */
    public boolean requirePreDrawListener() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public void onPreDraw() {
        updateFromViewRotation(this.mView.getRotation());
    }

    /* access modifiers changed from: package-private */
    public void hide(@Nullable FloatingActionButtonImpl.InternalVisibilityChangedListener internalVisibilityChangedListener) {
        Animator.AnimatorListener animatorListener;
        FloatingActionButtonImpl.InternalVisibilityChangedListener internalVisibilityChangedListener2 = internalVisibilityChangedListener;
        if (this.mIsHiding || this.mView.getVisibility() != 0) {
            if (internalVisibilityChangedListener2 != null) {
                internalVisibilityChangedListener2.onHidden();
            }
        } else if (!ViewCompat.isLaidOut(this.mView) || this.mView.isInEditMode()) {
            this.mView.setVisibility(8);
            if (internalVisibilityChangedListener2 != null) {
                internalVisibilityChangedListener2.onHidden();
            }
        } else {
            final FloatingActionButtonImpl.InternalVisibilityChangedListener internalVisibilityChangedListener3 = internalVisibilityChangedListener2;
            new AnimatorListenerAdapter() {
                public void onAnimationStart(Animator animator) {
                    boolean access$002 = FloatingActionButtonHoneycombMr1.access$002(FloatingActionButtonHoneycombMr1.this, true);
                    FloatingActionButtonHoneycombMr1.this.mView.setVisibility(0);
                }

                public void onAnimationCancel(Animator animator) {
                    boolean access$002 = FloatingActionButtonHoneycombMr1.access$002(FloatingActionButtonHoneycombMr1.this, false);
                }

                public void onAnimationEnd(Animator animator) {
                    boolean access$002 = FloatingActionButtonHoneycombMr1.access$002(FloatingActionButtonHoneycombMr1.this, false);
                    FloatingActionButtonHoneycombMr1.this.mView.setVisibility(8);
                    if (internalVisibilityChangedListener3 != null) {
                        internalVisibilityChangedListener3.onHidden();
                    }
                }
            };
            ViewPropertyAnimator listener = this.mView.animate().scaleX(0.0f).scaleY(0.0f).alpha(0.0f).setDuration(200).setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR).setListener(animatorListener);
        }
    }

    /* access modifiers changed from: package-private */
    public void show(@Nullable FloatingActionButtonImpl.InternalVisibilityChangedListener internalVisibilityChangedListener) {
        Animator.AnimatorListener animatorListener;
        FloatingActionButtonImpl.InternalVisibilityChangedListener internalVisibilityChangedListener2 = internalVisibilityChangedListener;
        if (this.mView.getVisibility() == 0) {
            return;
        }
        if (!ViewCompat.isLaidOut(this.mView) || this.mView.isInEditMode()) {
            this.mView.setVisibility(0);
            this.mView.setAlpha(1.0f);
            this.mView.setScaleY(1.0f);
            this.mView.setScaleX(1.0f);
            if (internalVisibilityChangedListener2 != null) {
                internalVisibilityChangedListener2.onShown();
                return;
            }
            return;
        }
        this.mView.setAlpha(0.0f);
        this.mView.setScaleY(0.0f);
        this.mView.setScaleX(0.0f);
        final FloatingActionButtonImpl.InternalVisibilityChangedListener internalVisibilityChangedListener3 = internalVisibilityChangedListener2;
        new AnimatorListenerAdapter() {
            public void onAnimationStart(Animator animator) {
                FloatingActionButtonHoneycombMr1.this.mView.setVisibility(0);
            }

            public void onAnimationEnd(Animator animator) {
                if (internalVisibilityChangedListener3 != null) {
                    internalVisibilityChangedListener3.onShown();
                }
            }
        };
        ViewPropertyAnimator listener = this.mView.animate().scaleX(1.0f).scaleY(1.0f).alpha(1.0f).setDuration(200).setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR).setListener(animatorListener);
    }

    private void updateFromViewRotation(float f) {
        float f2 = f;
        if (this.mShadowDrawable != null) {
            this.mShadowDrawable.setRotation(-f2);
        }
        if (this.mBorderDrawable != null) {
            this.mBorderDrawable.setRotation(-f2);
        }
    }
}
