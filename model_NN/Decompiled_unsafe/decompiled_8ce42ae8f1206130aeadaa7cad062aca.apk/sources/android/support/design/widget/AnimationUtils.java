package android.support.design.widget;

import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;

class AnimationUtils {
    static final Interpolator DECELERATE_INTERPOLATOR;
    static final Interpolator FAST_OUT_SLOW_IN_INTERPOLATOR;
    static final Interpolator LINEAR_INTERPOLATOR;

    AnimationUtils() {
    }

    static {
        Interpolator interpolator;
        Interpolator interpolator2;
        Interpolator interpolator3;
        new LinearInterpolator();
        LINEAR_INTERPOLATOR = interpolator;
        new FastOutSlowInInterpolator();
        FAST_OUT_SLOW_IN_INTERPOLATOR = interpolator2;
        new DecelerateInterpolator();
        DECELERATE_INTERPOLATOR = interpolator3;
    }

    static float lerp(float f, float f2, float f3) {
        float f4 = f;
        return f4 + (f3 * (f2 - f4));
    }

    static int lerp(int i, int i2, float f) {
        int i3 = i;
        return i3 + Math.round(f * ((float) (i2 - i3)));
    }

    static class AnimationListenerAdapter implements Animation.AnimationListener {
        AnimationListenerAdapter() {
        }

        public void onAnimationStart(Animation animation) {
        }

        public void onAnimationEnd(Animation animation) {
        }

        public void onAnimationRepeat(Animation animation) {
        }
    }
}
