package android.support.design.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.R;
import android.support.design.widget.ValueAnimatorCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.TintManager;
import android.text.Layout;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;

public class TabLayout extends HorizontalScrollView {
    private static final int ANIMATION_DURATION = 300;
    private static final int DEFAULT_GAP_TEXT_ICON = 8;
    private static final int DEFAULT_HEIGHT = 48;
    private static final int DEFAULT_HEIGHT_WITH_TEXT_ICON = 72;
    private static final int FIXED_WRAP_GUTTER_MIN = 16;
    public static final int GRAVITY_CENTER = 1;
    public static final int GRAVITY_FILL = 0;
    private static final int INVALID_WIDTH = -1;
    public static final int MODE_FIXED = 1;
    public static final int MODE_SCROLLABLE = 0;
    private static final int MOTION_NON_ADJACENT_OFFSET = 24;
    private static final int TAB_MIN_WIDTH_MARGIN = 56;
    private int mContentInsetStart;
    private ValueAnimatorCompat mIndicatorAnimator;
    /* access modifiers changed from: private */
    public int mMode;
    private OnTabSelectedListener mOnTabSelectedListener;
    private final int mRequestedTabMaxWidth;
    private final int mRequestedTabMinWidth;
    private ValueAnimatorCompat mScrollAnimator;
    private final int mScrollableTabMinWidth;
    private Tab mSelectedTab;
    /* access modifiers changed from: private */
    public final int mTabBackgroundResId;
    private View.OnClickListener mTabClickListener;
    /* access modifiers changed from: private */
    public int mTabGravity;
    /* access modifiers changed from: private */
    public int mTabMaxWidth;
    /* access modifiers changed from: private */
    public int mTabPaddingBottom;
    /* access modifiers changed from: private */
    public int mTabPaddingEnd;
    /* access modifiers changed from: private */
    public int mTabPaddingStart;
    /* access modifiers changed from: private */
    public int mTabPaddingTop;
    private final SlidingTabStrip mTabStrip;
    /* access modifiers changed from: private */
    public int mTabTextAppearance;
    /* access modifiers changed from: private */
    public ColorStateList mTabTextColors;
    /* access modifiers changed from: private */
    public float mTabTextMultiLineSize;
    /* access modifiers changed from: private */
    public float mTabTextSize;
    private final ArrayList<Tab> mTabs;

    @Retention(RetentionPolicy.SOURCE)
    public @interface Mode {
    }

    public interface OnTabSelectedListener {
        void onTabReselected(Tab tab);

        void onTabSelected(Tab tab);

        void onTabUnselected(Tab tab);
    }

    @Retention(RetentionPolicy.SOURCE)
    public @interface TabGravity {
    }

    static /* synthetic */ int access$1602(TabLayout tabLayout, int i) {
        int i2 = i;
        int i3 = i2;
        tabLayout.mTabGravity = i3;
        return i2;
    }

    static /* synthetic */ ValueAnimatorCompat access$1802(TabLayout tabLayout, ValueAnimatorCompat valueAnimatorCompat) {
        ValueAnimatorCompat valueAnimatorCompat2 = valueAnimatorCompat;
        ValueAnimatorCompat valueAnimatorCompat3 = valueAnimatorCompat2;
        tabLayout.mIndicatorAnimator = valueAnimatorCompat3;
        return valueAnimatorCompat2;
    }

    public TabLayout(Context context) {
        this(context, null);
    }

    public TabLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public TabLayout(android.content.Context r17, android.util.AttributeSet r18, int r19) {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            r2 = r18
            r3 = r19
            r7 = r0
            r8 = r1
            r9 = r2
            r10 = r3
            r7.<init>(r8, r9, r10)
            r7 = r0
            java.util.ArrayList r8 = new java.util.ArrayList
            r14 = r8
            r8 = r14
            r9 = r14
            r9.<init>()
            r7.mTabs = r8
            r7 = r0
            r8 = 2147483647(0x7fffffff, float:NaN)
            r7.mTabMaxWidth = r8
            r7 = r1
            android.support.design.widget.ThemeUtils.checkAppCompatTheme(r7)
            r7 = r0
            r8 = 0
            r7.setHorizontalScrollBarEnabled(r8)
            r7 = r0
            android.support.design.widget.TabLayout$SlidingTabStrip r8 = new android.support.design.widget.TabLayout$SlidingTabStrip
            r14 = r8
            r8 = r14
            r9 = r14
            r10 = r0
            r11 = r1
            r9.<init>(r11)
            r7.mTabStrip = r8
            r7 = r0
            r8 = r0
            android.support.design.widget.TabLayout$SlidingTabStrip r8 = r8.mTabStrip
            r9 = -2
            r10 = -1
            r7.addView(r8, r9, r10)
            r7 = r1
            r8 = r2
            int[] r9 = android.support.design.R.styleable.TabLayout
            r10 = r3
            int r11 = android.support.design.R.style.Widget_Design_TabLayout
            android.content.res.TypedArray r7 = r7.obtainStyledAttributes(r8, r9, r10, r11)
            r4 = r7
            r7 = r0
            android.support.design.widget.TabLayout$SlidingTabStrip r7 = r7.mTabStrip
            r8 = r4
            int r9 = android.support.design.R.styleable.TabLayout_tabIndicatorHeight
            r10 = 0
            int r8 = r8.getDimensionPixelSize(r9, r10)
            r7.setSelectedIndicatorHeight(r8)
            r7 = r0
            android.support.design.widget.TabLayout$SlidingTabStrip r7 = r7.mTabStrip
            r8 = r4
            int r9 = android.support.design.R.styleable.TabLayout_tabIndicatorColor
            r10 = 0
            int r8 = r8.getColor(r9, r10)
            r7.setSelectedIndicatorColor(r8)
            r7 = r0
            r8 = r0
            r9 = r0
            r10 = r0
            r11 = r4
            int r12 = android.support.design.R.styleable.TabLayout_tabPadding
            r13 = 0
            int r11 = r11.getDimensionPixelSize(r12, r13)
            r14 = r10
            r15 = r11
            r10 = r15
            r11 = r14
            r12 = r15
            r11.mTabPaddingBottom = r12
            r14 = r9
            r15 = r10
            r9 = r15
            r10 = r14
            r11 = r15
            r10.mTabPaddingEnd = r11
            r14 = r8
            r15 = r9
            r8 = r15
            r9 = r14
            r10 = r15
            r9.mTabPaddingTop = r10
            r7.mTabPaddingStart = r8
            r7 = r0
            r8 = r4
            int r9 = android.support.design.R.styleable.TabLayout_tabPaddingStart
            r10 = r0
            int r10 = r10.mTabPaddingStart
            int r8 = r8.getDimensionPixelSize(r9, r10)
            r7.mTabPaddingStart = r8
            r7 = r0
            r8 = r4
            int r9 = android.support.design.R.styleable.TabLayout_tabPaddingTop
            r10 = r0
            int r10 = r10.mTabPaddingTop
            int r8 = r8.getDimensionPixelSize(r9, r10)
            r7.mTabPaddingTop = r8
            r7 = r0
            r8 = r4
            int r9 = android.support.design.R.styleable.TabLayout_tabPaddingEnd
            r10 = r0
            int r10 = r10.mTabPaddingEnd
            int r8 = r8.getDimensionPixelSize(r9, r10)
            r7.mTabPaddingEnd = r8
            r7 = r0
            r8 = r4
            int r9 = android.support.design.R.styleable.TabLayout_tabPaddingBottom
            r10 = r0
            int r10 = r10.mTabPaddingBottom
            int r8 = r8.getDimensionPixelSize(r9, r10)
            r7.mTabPaddingBottom = r8
            r7 = r0
            r8 = r4
            int r9 = android.support.design.R.styleable.TabLayout_tabTextAppearance
            int r10 = android.support.design.R.style.TextAppearance_Design_Tab
            int r8 = r8.getResourceId(r9, r10)
            r7.mTabTextAppearance = r8
            r7 = r1
            r8 = r0
            int r8 = r8.mTabTextAppearance
            int[] r9 = android.support.design.R.styleable.TextAppearance
            android.content.res.TypedArray r7 = r7.obtainStyledAttributes(r8, r9)
            r5 = r7
            r7 = r0
            r8 = r5
            int r9 = android.support.design.R.styleable.TextAppearance_android_textSize     // Catch:{ all -> 0x0189 }
            r10 = 0
            int r8 = r8.getDimensionPixelSize(r9, r10)     // Catch:{ all -> 0x0189 }
            float r8 = (float) r8     // Catch:{ all -> 0x0189 }
            r7.mTabTextSize = r8     // Catch:{ all -> 0x0189 }
            r7 = r0
            r8 = r5
            int r9 = android.support.design.R.styleable.TextAppearance_android_textColor     // Catch:{ all -> 0x0189 }
            android.content.res.ColorStateList r8 = r8.getColorStateList(r9)     // Catch:{ all -> 0x0189 }
            r7.mTabTextColors = r8     // Catch:{ all -> 0x0189 }
            r7 = r5
            r7.recycle()
            r7 = r4
            int r8 = android.support.design.R.styleable.TabLayout_tabTextColor
            boolean r7 = r7.hasValue(r8)
            if (r7 == 0) goto L_0x0102
            r7 = r0
            r8 = r4
            int r9 = android.support.design.R.styleable.TabLayout_tabTextColor
            android.content.res.ColorStateList r8 = r8.getColorStateList(r9)
            r7.mTabTextColors = r8
        L_0x0102:
            r7 = r4
            int r8 = android.support.design.R.styleable.TabLayout_tabSelectedTextColor
            boolean r7 = r7.hasValue(r8)
            if (r7 == 0) goto L_0x0123
            r7 = r4
            int r8 = android.support.design.R.styleable.TabLayout_tabSelectedTextColor
            r9 = 0
            int r7 = r7.getColor(r8, r9)
            r6 = r7
            r7 = r0
            r8 = r0
            android.content.res.ColorStateList r8 = r8.mTabTextColors
            int r8 = r8.getDefaultColor()
            r9 = r6
            android.content.res.ColorStateList r8 = createColorStateList(r8, r9)
            r7.mTabTextColors = r8
        L_0x0123:
            r7 = r0
            r8 = r4
            int r9 = android.support.design.R.styleable.TabLayout_tabMinWidth
            r10 = -1
            int r8 = r8.getDimensionPixelSize(r9, r10)
            r7.mRequestedTabMinWidth = r8
            r7 = r0
            r8 = r4
            int r9 = android.support.design.R.styleable.TabLayout_tabMaxWidth
            r10 = -1
            int r8 = r8.getDimensionPixelSize(r9, r10)
            r7.mRequestedTabMaxWidth = r8
            r7 = r0
            r8 = r4
            int r9 = android.support.design.R.styleable.TabLayout_tabBackground
            r10 = 0
            int r8 = r8.getResourceId(r9, r10)
            r7.mTabBackgroundResId = r8
            r7 = r0
            r8 = r4
            int r9 = android.support.design.R.styleable.TabLayout_tabContentStart
            r10 = 0
            int r8 = r8.getDimensionPixelSize(r9, r10)
            r7.mContentInsetStart = r8
            r7 = r0
            r8 = r4
            int r9 = android.support.design.R.styleable.TabLayout_tabMode
            r10 = 1
            int r8 = r8.getInt(r9, r10)
            r7.mMode = r8
            r7 = r0
            r8 = r4
            int r9 = android.support.design.R.styleable.TabLayout_tabGravity
            r10 = 0
            int r8 = r8.getInt(r9, r10)
            r7.mTabGravity = r8
            r7 = r4
            r7.recycle()
            r7 = r0
            android.content.res.Resources r7 = r7.getResources()
            r6 = r7
            r7 = r0
            r8 = r6
            int r9 = android.support.design.R.dimen.design_tab_text_size_2line
            int r8 = r8.getDimensionPixelSize(r9)
            float r8 = (float) r8
            r7.mTabTextMultiLineSize = r8
            r7 = r0
            r8 = r6
            int r9 = android.support.design.R.dimen.design_tab_scrollable_min_width
            int r8 = r8.getDimensionPixelSize(r9)
            r7.mScrollableTabMinWidth = r8
            r7 = r0
            r7.applyModeAndGravity()
            return
        L_0x0189:
            r7 = move-exception
            r6 = r7
            r7 = r5
            r7.recycle()
            r7 = r6
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.TabLayout.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public void setSelectedTabIndicatorColor(@ColorInt int i) {
        this.mTabStrip.setSelectedIndicatorColor(i);
    }

    public void setSelectedTabIndicatorHeight(int i) {
        this.mTabStrip.setSelectedIndicatorHeight(i);
    }

    public void setScrollPosition(int i, float f, boolean z) {
        int i2 = i;
        float f2 = f;
        boolean z2 = z;
        if ((this.mIndicatorAnimator == null || !this.mIndicatorAnimator.isRunning()) && i2 >= 0 && i2 < this.mTabStrip.getChildCount()) {
            this.mTabStrip.setIndicatorPositionFromTabPosition(i2, f2);
            scrollTo(calculateScrollXForTab(i2, f2), 0);
            if (z2) {
                setSelectedTabView(Math.round(((float) i2) + f2));
            }
        }
    }

    private float getScrollPosition() {
        return this.mTabStrip.getIndicatorPosition();
    }

    public void addTab(@NonNull Tab tab) {
        addTab(tab, this.mTabs.isEmpty());
    }

    public void addTab(@NonNull Tab tab, int i) {
        addTab(tab, i, this.mTabs.isEmpty());
    }

    public void addTab(@NonNull Tab tab, boolean z) {
        Throwable th;
        Tab tab2 = tab;
        boolean z2 = z;
        if (tab2.mParent != this) {
            Throwable th2 = th;
            new IllegalArgumentException("Tab belongs to a different TabLayout.");
            throw th2;
        }
        addTabView(tab2, z2);
        configureTab(tab2, this.mTabs.size());
        if (z2) {
            tab2.select();
        }
    }

    public void addTab(@NonNull Tab tab, int i, boolean z) {
        Throwable th;
        Tab tab2 = tab;
        int i2 = i;
        boolean z2 = z;
        if (tab2.mParent != this) {
            Throwable th2 = th;
            new IllegalArgumentException("Tab belongs to a different TabLayout.");
            throw th2;
        }
        addTabView(tab2, i2, z2);
        configureTab(tab2, i2);
        if (z2) {
            tab2.select();
        }
    }

    public void setOnTabSelectedListener(OnTabSelectedListener onTabSelectedListener) {
        this.mOnTabSelectedListener = onTabSelectedListener;
    }

    @NonNull
    public Tab newTab() {
        Tab tab;
        new Tab(this);
        return tab;
    }

    public int getTabCount() {
        return this.mTabs.size();
    }

    @Nullable
    public Tab getTabAt(int i) {
        return this.mTabs.get(i);
    }

    public int getSelectedTabPosition() {
        return this.mSelectedTab != null ? this.mSelectedTab.getPosition() : -1;
    }

    public void removeTab(Tab tab) {
        Throwable th;
        Tab tab2 = tab;
        if (tab2.mParent != this) {
            Throwable th2 = th;
            new IllegalArgumentException("Tab does not belong to this TabLayout.");
            throw th2;
        }
        removeTabAt(tab2.getPosition());
    }

    public void removeTabAt(int i) {
        int i2 = i;
        int position = this.mSelectedTab != null ? this.mSelectedTab.getPosition() : 0;
        removeTabViewAt(i2);
        Tab remove = this.mTabs.remove(i2);
        if (remove != null) {
            remove.setPosition(-1);
        }
        int size = this.mTabs.size();
        for (int i3 = i2; i3 < size; i3++) {
            this.mTabs.get(i3).setPosition(i3);
        }
        if (position == i2) {
            selectTab(this.mTabs.isEmpty() ? null : this.mTabs.get(Math.max(0, i2 - 1)));
        }
    }

    public void removeAllTabs() {
        this.mTabStrip.removeAllViews();
        Iterator<Tab> it = this.mTabs.iterator();
        while (it.hasNext()) {
            it.next().setPosition(-1);
            it.remove();
        }
        this.mSelectedTab = null;
    }

    public void setTabMode(int i) {
        int i2 = i;
        if (i2 != this.mMode) {
            this.mMode = i2;
            applyModeAndGravity();
        }
    }

    public int getTabMode() {
        return this.mMode;
    }

    public void setTabGravity(int i) {
        int i2 = i;
        if (this.mTabGravity != i2) {
            this.mTabGravity = i2;
            applyModeAndGravity();
        }
    }

    public int getTabGravity() {
        return this.mTabGravity;
    }

    public void setTabTextColors(@Nullable ColorStateList colorStateList) {
        ColorStateList colorStateList2 = colorStateList;
        if (this.mTabTextColors != colorStateList2) {
            this.mTabTextColors = colorStateList2;
            updateAllTabs();
        }
    }

    @Nullable
    public ColorStateList getTabTextColors() {
        return this.mTabTextColors;
    }

    public void setTabTextColors(int i, int i2) {
        setTabTextColors(createColorStateList(i, i2));
    }

    public void setupWithViewPager(@NonNull ViewPager viewPager) {
        ViewPager.OnPageChangeListener onPageChangeListener;
        OnTabSelectedListener onTabSelectedListener;
        int currentItem;
        Throwable th;
        ViewPager viewPager2 = viewPager;
        PagerAdapter adapter = viewPager2.getAdapter();
        if (adapter == null) {
            Throwable th2 = th;
            new IllegalArgumentException("ViewPager does not have a PagerAdapter set");
            throw th2;
        }
        setTabsFromPagerAdapter(adapter);
        new TabLayoutOnPageChangeListener(this);
        viewPager2.addOnPageChangeListener(onPageChangeListener);
        new ViewPagerOnTabSelectedListener(viewPager2);
        setOnTabSelectedListener(onTabSelectedListener);
        if (adapter.getCount() > 0 && getSelectedTabPosition() != (currentItem = viewPager2.getCurrentItem())) {
            selectTab(getTabAt(currentItem));
        }
    }

    public void setTabsFromPagerAdapter(@NonNull PagerAdapter pagerAdapter) {
        PagerAdapter pagerAdapter2 = pagerAdapter;
        removeAllTabs();
        int count = pagerAdapter2.getCount();
        for (int i = 0; i < count; i++) {
            addTab(newTab().setText(pagerAdapter2.getPageTitle(i)));
        }
    }

    private void updateAllTabs() {
        int childCount = this.mTabStrip.getChildCount();
        for (int i = 0; i < childCount; i++) {
            updateTab(i);
        }
    }

    private TabView createTabView(Tab tab) {
        TabView tabView;
        View.OnClickListener onClickListener;
        new TabView(this, getContext(), tab);
        TabView tabView2 = tabView;
        tabView2.setFocusable(true);
        tabView2.setMinimumWidth(getTabMinWidth());
        if (this.mTabClickListener == null) {
            new View.OnClickListener() {
                public void onClick(View view) {
                    ((TabView) view).getTab().select();
                }
            };
            this.mTabClickListener = onClickListener;
        }
        tabView2.setOnClickListener(this.mTabClickListener);
        return tabView2;
    }

    private void configureTab(Tab tab, int i) {
        Tab tab2 = tab;
        int i2 = i;
        tab2.setPosition(i2);
        this.mTabs.add(i2, tab2);
        int size = this.mTabs.size();
        for (int i3 = i2 + 1; i3 < size; i3++) {
            this.mTabs.get(i3).setPosition(i3);
        }
    }

    /* access modifiers changed from: private */
    public void updateTab(int i) {
        TabView tabView = getTabView(i);
        if (tabView != null) {
            tabView.update();
        }
    }

    /* access modifiers changed from: private */
    public TabView getTabView(int i) {
        return (TabView) this.mTabStrip.getChildAt(i);
    }

    private void addTabView(Tab tab, boolean z) {
        TabView createTabView = createTabView(tab);
        this.mTabStrip.addView(createTabView, createLayoutParamsForTabs());
        if (z) {
            createTabView.setSelected(true);
        }
    }

    private void addTabView(Tab tab, int i, boolean z) {
        TabView createTabView = createTabView(tab);
        this.mTabStrip.addView(createTabView, i, createLayoutParamsForTabs());
        if (z) {
            createTabView.setSelected(true);
        }
    }

    private LinearLayout.LayoutParams createLayoutParamsForTabs() {
        LinearLayout.LayoutParams layoutParams;
        new LinearLayout.LayoutParams(-2, -1);
        LinearLayout.LayoutParams layoutParams2 = layoutParams;
        updateTabViewLayoutParams(layoutParams2);
        return layoutParams2;
    }

    private void updateTabViewLayoutParams(LinearLayout.LayoutParams layoutParams) {
        LinearLayout.LayoutParams layoutParams2 = layoutParams;
        if (this.mMode == 1 && this.mTabGravity == 0) {
            layoutParams2.width = 0;
            layoutParams2.weight = 1.0f;
            return;
        }
        layoutParams2.width = -2;
        layoutParams2.weight = 0.0f;
    }

    /* access modifiers changed from: private */
    public int dpToPx(int i) {
        return Math.round(getResources().getDisplayMetrics().density * ((float) i));
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int dpToPx;
        int i3 = i;
        int i4 = i2;
        int dpToPx2 = dpToPx(getDefaultHeight()) + getPaddingTop() + getPaddingBottom();
        switch (View.MeasureSpec.getMode(i4)) {
            case Integer.MIN_VALUE:
                i4 = View.MeasureSpec.makeMeasureSpec(Math.min(dpToPx2, View.MeasureSpec.getSize(i4)), 1073741824);
                break;
            case 0:
                i4 = View.MeasureSpec.makeMeasureSpec(dpToPx2, 1073741824);
                break;
        }
        int size = View.MeasureSpec.getSize(i3);
        if (View.MeasureSpec.getMode(i3) != 0) {
            if (this.mRequestedTabMaxWidth > 0) {
                dpToPx = this.mRequestedTabMaxWidth;
            } else {
                dpToPx = size - dpToPx(56);
            }
            this.mTabMaxWidth = dpToPx;
        }
        super.onMeasure(i3, i4);
        if (getChildCount() == 1) {
            View childAt = getChildAt(0);
            boolean z = false;
            switch (this.mMode) {
                case 0:
                    z = childAt.getMeasuredWidth() < getMeasuredWidth();
                    break;
                case 1:
                    z = childAt.getMeasuredWidth() != getMeasuredWidth();
                    break;
            }
            if (z) {
                childAt.measure(View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824), getChildMeasureSpec(i4, getPaddingTop() + getPaddingBottom(), childAt.getLayoutParams().height));
            }
        }
    }

    private void removeTabViewAt(int i) {
        this.mTabStrip.removeViewAt(i);
        requestLayout();
    }

    private void animateToTab(int i) {
        ValueAnimatorCompat.AnimatorUpdateListener animatorUpdateListener;
        int i2 = i;
        if (i2 != -1) {
            if (getWindowToken() == null || !ViewCompat.isLaidOut(this) || this.mTabStrip.childrenNeedLayout()) {
                setScrollPosition(i2, 0.0f, true);
                return;
            }
            int scrollX = getScrollX();
            int calculateScrollXForTab = calculateScrollXForTab(i2, 0.0f);
            if (scrollX != calculateScrollXForTab) {
                if (this.mScrollAnimator == null) {
                    this.mScrollAnimator = ViewUtils.createAnimator();
                    this.mScrollAnimator.setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR);
                    this.mScrollAnimator.setDuration(ANIMATION_DURATION);
                    new ValueAnimatorCompat.AnimatorUpdateListener() {
                        public void onAnimationUpdate(ValueAnimatorCompat valueAnimatorCompat) {
                            TabLayout.this.scrollTo(valueAnimatorCompat.getAnimatedIntValue(), 0);
                        }
                    };
                    this.mScrollAnimator.setUpdateListener(animatorUpdateListener);
                }
                this.mScrollAnimator.setIntValues(scrollX, calculateScrollXForTab);
                this.mScrollAnimator.start();
            }
            this.mTabStrip.animateIndicatorToPosition(i2, ANIMATION_DURATION);
        }
    }

    private void setSelectedTabView(int i) {
        int i2 = i;
        int childCount = this.mTabStrip.getChildCount();
        if (i2 < childCount && !this.mTabStrip.getChildAt(i2).isSelected()) {
            int i3 = 0;
            while (i3 < childCount) {
                this.mTabStrip.getChildAt(i3).setSelected(i3 == i2);
                i3++;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void selectTab(Tab tab) {
        selectTab(tab, true);
    }

    /* access modifiers changed from: package-private */
    public void selectTab(Tab tab, boolean z) {
        Tab tab2 = tab;
        boolean z2 = z;
        if (this.mSelectedTab != tab2) {
            if (z2) {
                int position = tab2 != null ? tab2.getPosition() : -1;
                if (position != -1) {
                    setSelectedTabView(position);
                }
                if ((this.mSelectedTab == null || this.mSelectedTab.getPosition() == -1) && position != -1) {
                    setScrollPosition(position, 0.0f, true);
                } else {
                    animateToTab(position);
                }
            }
            if (!(this.mSelectedTab == null || this.mOnTabSelectedListener == null)) {
                this.mOnTabSelectedListener.onTabUnselected(this.mSelectedTab);
            }
            this.mSelectedTab = tab2;
            if (this.mSelectedTab != null && this.mOnTabSelectedListener != null) {
                this.mOnTabSelectedListener.onTabSelected(this.mSelectedTab);
            }
        } else if (this.mSelectedTab != null) {
            if (this.mOnTabSelectedListener != null) {
                this.mOnTabSelectedListener.onTabReselected(this.mSelectedTab);
            }
            animateToTab(tab2.getPosition());
        }
    }

    private int calculateScrollXForTab(int i, float f) {
        int i2 = i;
        float f2 = f;
        if (this.mMode != 0) {
            return 0;
        }
        View childAt = this.mTabStrip.getChildAt(i2);
        View childAt2 = i2 + 1 < this.mTabStrip.getChildCount() ? this.mTabStrip.getChildAt(i2 + 1) : null;
        return ((childAt.getLeft() + ((int) ((((float) ((childAt != null ? childAt.getWidth() : 0) + (childAt2 != null ? childAt2.getWidth() : 0))) * f2) * 0.5f))) + (childAt.getWidth() / 2)) - (getWidth() / 2);
    }

    private void applyModeAndGravity() {
        int i = 0;
        if (this.mMode == 0) {
            i = Math.max(0, this.mContentInsetStart - this.mTabPaddingStart);
        }
        ViewCompat.setPaddingRelative(this.mTabStrip, i, 0, 0, 0);
        switch (this.mMode) {
            case 0:
                this.mTabStrip.setGravity(8388611);
                break;
            case 1:
                this.mTabStrip.setGravity(1);
                break;
        }
        updateTabViews(true);
    }

    /* access modifiers changed from: private */
    public void updateTabViews(boolean z) {
        boolean z2 = z;
        for (int i = 0; i < this.mTabStrip.getChildCount(); i++) {
            View childAt = this.mTabStrip.getChildAt(i);
            childAt.setMinimumWidth(getTabMinWidth());
            updateTabViewLayoutParams((LinearLayout.LayoutParams) childAt.getLayoutParams());
            if (z2) {
                childAt.requestLayout();
            }
        }
    }

    public static final class Tab {
        public static final int INVALID_POSITION = -1;
        private CharSequence mContentDesc;
        private View mCustomView;
        private Drawable mIcon;
        /* access modifiers changed from: private */
        public final TabLayout mParent;
        private int mPosition = -1;
        private Object mTag;
        private CharSequence mText;

        Tab(TabLayout tabLayout) {
            this.mParent = tabLayout;
        }

        @Nullable
        public Object getTag() {
            return this.mTag;
        }

        @NonNull
        public Tab setTag(@Nullable Object obj) {
            this.mTag = obj;
            return this;
        }

        @Nullable
        public View getCustomView() {
            return this.mCustomView;
        }

        @NonNull
        public Tab setCustomView(@Nullable View view) {
            this.mCustomView = view;
            if (this.mPosition >= 0) {
                this.mParent.updateTab(this.mPosition);
            }
            return this;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.support.design.widget.TabLayout$TabView, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        @NonNull
        public Tab setCustomView(@LayoutRes int i) {
            TabView access$200 = this.mParent.getTabView(this.mPosition);
            return setCustomView(LayoutInflater.from(access$200.getContext()).inflate(i, (ViewGroup) access$200, false));
        }

        @Nullable
        public Drawable getIcon() {
            return this.mIcon;
        }

        public int getPosition() {
            return this.mPosition;
        }

        /* access modifiers changed from: package-private */
        public void setPosition(int i) {
            this.mPosition = i;
        }

        @Nullable
        public CharSequence getText() {
            return this.mText;
        }

        @NonNull
        public Tab setIcon(@Nullable Drawable drawable) {
            this.mIcon = drawable;
            if (this.mPosition >= 0) {
                this.mParent.updateTab(this.mPosition);
            }
            return this;
        }

        @NonNull
        public Tab setIcon(@DrawableRes int i) {
            return setIcon(TintManager.getDrawable(this.mParent.getContext(), i));
        }

        @NonNull
        public Tab setText(@Nullable CharSequence charSequence) {
            this.mText = charSequence;
            if (this.mPosition >= 0) {
                this.mParent.updateTab(this.mPosition);
            }
            return this;
        }

        @NonNull
        public Tab setText(@StringRes int i) {
            return setText(this.mParent.getResources().getText(i));
        }

        public void select() {
            this.mParent.selectTab(this);
        }

        public boolean isSelected() {
            return this.mParent.getSelectedTabPosition() == this.mPosition;
        }

        @NonNull
        public Tab setContentDescription(@StringRes int i) {
            return setContentDescription(this.mParent.getResources().getText(i));
        }

        @NonNull
        public Tab setContentDescription(@Nullable CharSequence charSequence) {
            this.mContentDesc = charSequence;
            if (this.mPosition >= 0) {
                this.mParent.updateTab(this.mPosition);
            }
            return this;
        }

        @Nullable
        public CharSequence getContentDescription() {
            return this.mContentDesc;
        }
    }

    class TabView extends LinearLayout implements View.OnLongClickListener {
        private ImageView mCustomIconView;
        private TextView mCustomTextView;
        private View mCustomView;
        private int mDefaultMaxLines = 2;
        private ImageView mIconView;
        private final Tab mTab;
        private TextView mTextView;
        final /* synthetic */ TabLayout this$0;

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public TabView(android.support.design.widget.TabLayout r10, android.content.Context r11, android.support.design.widget.TabLayout.Tab r12) {
            /*
                r9 = this;
                r0 = r9
                r1 = r10
                r2 = r11
                r3 = r12
                r4 = r0
                r5 = r1
                r4.this$0 = r5
                r4 = r0
                r5 = r2
                r4.<init>(r5)
                r4 = r0
                r5 = 2
                r4.mDefaultMaxLines = r5
                r4 = r0
                r5 = r3
                r4.mTab = r5
                r4 = r1
                int r4 = r4.mTabBackgroundResId
                if (r4 == 0) goto L_0x002a
                r4 = r0
                r5 = r2
                r6 = r1
                int r6 = r6.mTabBackgroundResId
                android.graphics.drawable.Drawable r5 = android.support.v7.widget.TintManager.getDrawable(r5, r6)
                r4.setBackgroundDrawable(r5)
            L_0x002a:
                r4 = r0
                r5 = r1
                int r5 = r5.mTabPaddingStart
                r6 = r1
                int r6 = r6.mTabPaddingTop
                r7 = r1
                int r7 = r7.mTabPaddingEnd
                r8 = r1
                int r8 = r8.mTabPaddingBottom
                android.support.v4.view.ViewCompat.setPaddingRelative(r4, r5, r6, r7, r8)
                r4 = r0
                r5 = 17
                r4.setGravity(r5)
                r4 = r0
                r5 = 1
                r4.setOrientation(r5)
                r4 = r0
                r4.update()
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.TabLayout.TabView.<init>(android.support.design.widget.TabLayout, android.content.Context, android.support.design.widget.TabLayout$Tab):void");
        }

        public void setSelected(boolean z) {
            boolean z2 = z;
            boolean z3 = isSelected() != z2;
            super.setSelected(z2);
            if (z3 && z2) {
                sendAccessibilityEvent(4);
                if (this.mTextView != null) {
                    this.mTextView.setSelected(z2);
                }
                if (this.mIconView != null) {
                    this.mIconView.setSelected(z2);
                }
            }
        }

        @TargetApi(14)
        public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
            AccessibilityEvent accessibilityEvent2 = accessibilityEvent;
            super.onInitializeAccessibilityEvent(accessibilityEvent2);
            accessibilityEvent2.setClassName(ActionBar.Tab.class.getName());
        }

        @TargetApi(14)
        public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
            AccessibilityNodeInfo accessibilityNodeInfo2 = accessibilityNodeInfo;
            super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo2);
            accessibilityNodeInfo2.setClassName(ActionBar.Tab.class.getName());
        }

        public void onMeasure(int i, int i2) {
            int i3;
            Layout layout;
            int i4 = i;
            int size = View.MeasureSpec.getSize(i4);
            int mode = View.MeasureSpec.getMode(i4);
            int access$800 = this.this$0.getTabMaxWidth();
            int i5 = i2;
            if (access$800 <= 0 || (mode != 0 && size <= access$800)) {
                i3 = i4;
            } else {
                i3 = View.MeasureSpec.makeMeasureSpec(this.this$0.mTabMaxWidth, mode);
            }
            super.onMeasure(i3, i5);
            if (this.mTextView != null) {
                Resources resources = getResources();
                float access$1000 = this.this$0.mTabTextSize;
                int i6 = this.mDefaultMaxLines;
                if (this.mIconView != null && this.mIconView.getVisibility() == 0) {
                    i6 = 1;
                } else if (this.mTextView != null && this.mTextView.getLineCount() > 1) {
                    access$1000 = this.this$0.mTabTextMultiLineSize;
                }
                float textSize = this.mTextView.getTextSize();
                int lineCount = this.mTextView.getLineCount();
                int maxLines = TextViewCompat.getMaxLines(this.mTextView);
                if (access$1000 != textSize || (maxLines >= 0 && i6 != maxLines)) {
                    boolean z = true;
                    if (this.this$0.mMode == 1 && access$1000 > textSize && lineCount == 1 && ((layout = this.mTextView.getLayout()) == null || approximateLineWidth(layout, 0, access$1000) > ((float) layout.getWidth()))) {
                        z = false;
                    }
                    if (z) {
                        this.mTextView.setTextSize(0, access$1000);
                        this.mTextView.setMaxLines(i6);
                        super.onMeasure(i3, i5);
                    }
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
         arg types: [int, android.support.design.widget.TabLayout$TabView, int]
         candidates:
          ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
          ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
        /* access modifiers changed from: package-private */
        public final void update() {
            Tab tab = this.mTab;
            View customView = tab.getCustomView();
            if (customView != null) {
                ViewParent parent = customView.getParent();
                if (parent != this) {
                    if (parent != null) {
                        ((ViewGroup) parent).removeView(customView);
                    }
                    addView(customView);
                }
                this.mCustomView = customView;
                if (this.mTextView != null) {
                    this.mTextView.setVisibility(8);
                }
                if (this.mIconView != null) {
                    this.mIconView.setVisibility(8);
                    this.mIconView.setImageDrawable(null);
                }
                this.mCustomTextView = (TextView) customView.findViewById(16908308);
                if (this.mCustomTextView != null) {
                    this.mDefaultMaxLines = TextViewCompat.getMaxLines(this.mCustomTextView);
                }
                this.mCustomIconView = (ImageView) customView.findViewById(16908294);
            } else {
                if (this.mCustomView != null) {
                    removeView(this.mCustomView);
                    this.mCustomView = null;
                }
                this.mCustomTextView = null;
                this.mCustomIconView = null;
            }
            if (this.mCustomView == null) {
                if (this.mIconView == null) {
                    ImageView imageView = (ImageView) LayoutInflater.from(getContext()).inflate(R.layout.design_layout_tab_icon, (ViewGroup) this, false);
                    addView(imageView, 0);
                    this.mIconView = imageView;
                }
                if (this.mTextView == null) {
                    TextView textView = (TextView) LayoutInflater.from(getContext()).inflate(R.layout.design_layout_tab_text, (ViewGroup) this, false);
                    addView(textView);
                    this.mTextView = textView;
                    this.mDefaultMaxLines = TextViewCompat.getMaxLines(this.mTextView);
                }
                this.mTextView.setTextAppearance(getContext(), this.this$0.mTabTextAppearance);
                if (this.this$0.mTabTextColors != null) {
                    this.mTextView.setTextColor(this.this$0.mTabTextColors);
                }
                updateTextAndIcon(tab, this.mTextView, this.mIconView);
            } else if (this.mCustomTextView != null || this.mCustomIconView != null) {
                updateTextAndIcon(tab, this.mCustomTextView, this.mCustomIconView);
            }
        }

        private void updateTextAndIcon(Tab tab, TextView textView, ImageView imageView) {
            Tab tab2 = tab;
            TextView textView2 = textView;
            ImageView imageView2 = imageView;
            Drawable icon = tab2.getIcon();
            CharSequence text = tab2.getText();
            if (imageView2 != null) {
                if (icon != null) {
                    imageView2.setImageDrawable(icon);
                    imageView2.setVisibility(0);
                    setVisibility(0);
                } else {
                    imageView2.setVisibility(8);
                    imageView2.setImageDrawable(null);
                }
                imageView2.setContentDescription(tab2.getContentDescription());
            }
            boolean z = !TextUtils.isEmpty(text);
            if (textView2 != null) {
                if (z) {
                    textView2.setText(text);
                    textView2.setContentDescription(tab2.getContentDescription());
                    textView2.setVisibility(0);
                    setVisibility(0);
                } else {
                    textView2.setVisibility(8);
                    textView2.setText((CharSequence) null);
                }
            }
            if (imageView2 != null) {
                ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) imageView2.getLayoutParams();
                int i = 0;
                if (z && imageView2.getVisibility() == 0) {
                    i = this.this$0.dpToPx(8);
                }
                if (i != marginLayoutParams.bottomMargin) {
                    marginLayoutParams.bottomMargin = i;
                    imageView2.requestLayout();
                }
            }
            if (z || TextUtils.isEmpty(tab2.getContentDescription())) {
                setOnLongClickListener(null);
                setLongClickable(false);
                return;
            }
            setOnLongClickListener(this);
        }

        public boolean onLongClick(View view) {
            int[] iArr = new int[2];
            getLocationOnScreen(iArr);
            Context context = getContext();
            int width = getWidth();
            int height = getHeight();
            int i = context.getResources().getDisplayMetrics().widthPixels;
            Toast makeText = Toast.makeText(context, this.mTab.getContentDescription(), 0);
            makeText.setGravity(49, (iArr[0] + (width / 2)) - (i / 2), height);
            makeText.show();
            return true;
        }

        public Tab getTab() {
            return this.mTab;
        }

        private float approximateLineWidth(Layout layout, int i, float f) {
            Layout layout2 = layout;
            return layout2.getLineWidth(i) * (f / layout2.getPaint().getTextSize());
        }
    }

    private class SlidingTabStrip extends LinearLayout {
        private ValueAnimatorCompat mCurrentAnimator;
        private int mIndicatorLeft = -1;
        private int mIndicatorRight = -1;
        private int mSelectedIndicatorHeight;
        private final Paint mSelectedIndicatorPaint;
        private int mSelectedPosition = -1;
        private float mSelectionOffset;

        static /* synthetic */ int access$2002(SlidingTabStrip slidingTabStrip, int i) {
            int i2 = i;
            int i3 = i2;
            slidingTabStrip.mSelectedPosition = i3;
            return i2;
        }

        static /* synthetic */ float access$2102(SlidingTabStrip slidingTabStrip, float f) {
            float f2 = f;
            float f3 = f2;
            slidingTabStrip.mSelectionOffset = f3;
            return f2;
        }

        SlidingTabStrip(Context context) {
            super(context);
            Paint paint;
            setWillNotDraw(false);
            new Paint();
            this.mSelectedIndicatorPaint = paint;
        }

        /* access modifiers changed from: package-private */
        public void setSelectedIndicatorColor(int i) {
            int i2 = i;
            if (this.mSelectedIndicatorPaint.getColor() != i2) {
                this.mSelectedIndicatorPaint.setColor(i2);
                ViewCompat.postInvalidateOnAnimation(this);
            }
        }

        /* access modifiers changed from: package-private */
        public void setSelectedIndicatorHeight(int i) {
            int i2 = i;
            if (this.mSelectedIndicatorHeight != i2) {
                this.mSelectedIndicatorHeight = i2;
                ViewCompat.postInvalidateOnAnimation(this);
            }
        }

        /* access modifiers changed from: package-private */
        public boolean childrenNeedLayout() {
            int childCount = getChildCount();
            for (int i = 0; i < childCount; i++) {
                if (getChildAt(i).getWidth() <= 0) {
                    return true;
                }
            }
            return false;
        }

        /* access modifiers changed from: package-private */
        public void setIndicatorPositionFromTabPosition(int i, float f) {
            this.mSelectedPosition = i;
            this.mSelectionOffset = f;
            updateIndicatorPosition();
        }

        /* access modifiers changed from: package-private */
        public float getIndicatorPosition() {
            return ((float) this.mSelectedPosition) + this.mSelectionOffset;
        }

        /* access modifiers changed from: protected */
        public void onMeasure(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            super.onMeasure(i3, i4);
            if (View.MeasureSpec.getMode(i3) == 1073741824 && TabLayout.this.mMode == 1 && TabLayout.this.mTabGravity == 1) {
                int childCount = getChildCount();
                int i5 = 0;
                int i6 = childCount;
                for (int i7 = 0; i7 < i6; i7++) {
                    View childAt = getChildAt(i7);
                    if (childAt.getVisibility() == 0) {
                        i5 = Math.max(i5, childAt.getMeasuredWidth());
                    }
                }
                if (i5 > 0) {
                    boolean z = false;
                    if (i5 * childCount <= getMeasuredWidth() - (TabLayout.this.dpToPx(16) * 2)) {
                        for (int i8 = 0; i8 < childCount; i8++) {
                            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) getChildAt(i8).getLayoutParams();
                            if (layoutParams.width != i5 || layoutParams.weight != 0.0f) {
                                layoutParams.width = i5;
                                layoutParams.weight = 0.0f;
                                z = true;
                            }
                        }
                    } else {
                        int access$1602 = TabLayout.access$1602(TabLayout.this, 0);
                        TabLayout.this.updateTabViews(false);
                        z = true;
                    }
                    if (z) {
                        super.onMeasure(i3, i4);
                    }
                }
            }
        }

        /* access modifiers changed from: protected */
        public void onLayout(boolean z, int i, int i2, int i3, int i4) {
            super.onLayout(z, i, i2, i3, i4);
            if (this.mCurrentAnimator == null || !this.mCurrentAnimator.isRunning()) {
                updateIndicatorPosition();
                return;
            }
            this.mCurrentAnimator.cancel();
            animateIndicatorToPosition(this.mSelectedPosition, Math.round((1.0f - this.mCurrentAnimator.getAnimatedFraction()) * ((float) this.mCurrentAnimator.getDuration())));
        }

        private void updateIndicatorPosition() {
            int i;
            int i2;
            View childAt = getChildAt(this.mSelectedPosition);
            if (childAt == null || childAt.getWidth() <= 0) {
                i = -1;
                i2 = -1;
            } else {
                i2 = childAt.getLeft();
                i = childAt.getRight();
                if (this.mSelectionOffset > 0.0f && this.mSelectedPosition < getChildCount() - 1) {
                    View childAt2 = getChildAt(this.mSelectedPosition + 1);
                    i2 = (int) ((this.mSelectionOffset * ((float) childAt2.getLeft())) + ((1.0f - this.mSelectionOffset) * ((float) i2)));
                    i = (int) ((this.mSelectionOffset * ((float) childAt2.getRight())) + ((1.0f - this.mSelectionOffset) * ((float) i)));
                }
            }
            setIndicatorPosition(i2, i);
        }

        /* access modifiers changed from: private */
        public void setIndicatorPosition(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            if (i3 != this.mIndicatorLeft || i4 != this.mIndicatorRight) {
                this.mIndicatorLeft = i3;
                this.mIndicatorRight = i4;
                ViewCompat.postInvalidateOnAnimation(this);
            }
        }

        /* access modifiers changed from: package-private */
        public void animateIndicatorToPosition(int i, int i2) {
            int i3;
            int i4;
            ValueAnimatorCompat.AnimatorUpdateListener animatorUpdateListener;
            ValueAnimatorCompat.AnimatorListener animatorListener;
            int i5 = i;
            int i6 = i2;
            boolean z = ViewCompat.getLayoutDirection(this) == 1;
            View childAt = getChildAt(i5);
            int left = childAt.getLeft();
            int right = childAt.getRight();
            if (Math.abs(i5 - this.mSelectedPosition) <= 1) {
                i4 = this.mIndicatorLeft;
                i3 = this.mIndicatorRight;
            } else {
                int access$1500 = TabLayout.this.dpToPx(24);
                if (i5 < this.mSelectedPosition) {
                    if (z) {
                        int i7 = left - access$1500;
                        i3 = i7;
                        i4 = i7;
                    } else {
                        int i8 = right + access$1500;
                        i3 = i8;
                        i4 = i8;
                    }
                } else if (z) {
                    int i9 = right + access$1500;
                    i3 = i9;
                    i4 = i9;
                } else {
                    int i10 = left - access$1500;
                    i3 = i10;
                    i4 = i10;
                }
            }
            if (i4 != left || i3 != right) {
                ValueAnimatorCompat access$1802 = TabLayout.access$1802(TabLayout.this, ViewUtils.createAnimator());
                access$1802.setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR);
                access$1802.setDuration(i6);
                access$1802.setFloatValues(0.0f, 1.0f);
                final int i11 = i4;
                final int i12 = left;
                final int i13 = i3;
                final int i14 = right;
                new ValueAnimatorCompat.AnimatorUpdateListener() {
                    public void onAnimationUpdate(ValueAnimatorCompat valueAnimatorCompat) {
                        float animatedFraction = valueAnimatorCompat.getAnimatedFraction();
                        SlidingTabStrip.this.setIndicatorPosition(AnimationUtils.lerp(i11, i12, animatedFraction), AnimationUtils.lerp(i13, i14, animatedFraction));
                    }
                };
                access$1802.setUpdateListener(animatorUpdateListener);
                final int i15 = i5;
                new ValueAnimatorCompat.AnimatorListenerAdapter() {
                    public void onAnimationEnd(ValueAnimatorCompat valueAnimatorCompat) {
                        int access$2002 = SlidingTabStrip.access$2002(SlidingTabStrip.this, i15);
                        float access$2102 = SlidingTabStrip.access$2102(SlidingTabStrip.this, 0.0f);
                    }

                    public void onAnimationCancel(ValueAnimatorCompat valueAnimatorCompat) {
                        int access$2002 = SlidingTabStrip.access$2002(SlidingTabStrip.this, i15);
                        float access$2102 = SlidingTabStrip.access$2102(SlidingTabStrip.this, 0.0f);
                    }
                };
                access$1802.setListener(animatorListener);
                access$1802.start();
                this.mCurrentAnimator = access$1802;
            }
        }

        public void draw(Canvas canvas) {
            Canvas canvas2 = canvas;
            super.draw(canvas2);
            if (this.mIndicatorLeft >= 0 && this.mIndicatorRight > this.mIndicatorLeft) {
                canvas2.drawRect((float) this.mIndicatorLeft, (float) (getHeight() - this.mSelectedIndicatorHeight), (float) this.mIndicatorRight, (float) getHeight(), this.mSelectedIndicatorPaint);
            }
        }
    }

    private static ColorStateList createColorStateList(int i, int i2) {
        ColorStateList colorStateList;
        int[][] iArr = new int[2][];
        int[] iArr2 = new int[2];
        iArr[0] = SELECTED_STATE_SET;
        iArr2[0] = i2;
        int i3 = 0 + 1;
        iArr[i3] = EMPTY_STATE_SET;
        iArr2[i3] = i;
        int i4 = i3 + 1;
        new ColorStateList(iArr, iArr2);
        return colorStateList;
    }

    private int getDefaultHeight() {
        int i;
        boolean z = false;
        int i2 = 0;
        int size = this.mTabs.size();
        while (true) {
            if (i2 < size) {
                Tab tab = this.mTabs.get(i2);
                if (tab != null && tab.getIcon() != null && !TextUtils.isEmpty(tab.getText())) {
                    z = true;
                    break;
                }
                i2++;
            } else {
                break;
            }
        }
        if (z) {
            i = 72;
        } else {
            i = 48;
        }
        return i;
    }

    private int getTabMinWidth() {
        if (this.mRequestedTabMinWidth != -1) {
            return this.mRequestedTabMinWidth;
        }
        return this.mMode == 0 ? this.mScrollableTabMinWidth : 0;
    }

    /* access modifiers changed from: private */
    public int getTabMaxWidth() {
        return this.mTabMaxWidth;
    }

    public static class TabLayoutOnPageChangeListener implements ViewPager.OnPageChangeListener {
        private int mPreviousScrollState;
        private int mScrollState;
        private final WeakReference<TabLayout> mTabLayoutRef;

        public TabLayoutOnPageChangeListener(TabLayout tabLayout) {
            WeakReference<TabLayout> weakReference;
            new WeakReference<>(tabLayout);
            this.mTabLayoutRef = weakReference;
        }

        public void onPageScrollStateChanged(int i) {
            this.mPreviousScrollState = this.mScrollState;
            this.mScrollState = i;
        }

        public void onPageScrolled(int i, float f, int i2) {
            int i3 = i;
            float f2 = f;
            TabLayout tabLayout = this.mTabLayoutRef.get();
            if (tabLayout != null) {
                tabLayout.setScrollPosition(i3, f2, this.mScrollState == 1 || (this.mScrollState == 2 && this.mPreviousScrollState == 1));
            }
        }

        public void onPageSelected(int i) {
            int i2 = i;
            TabLayout tabLayout = this.mTabLayoutRef.get();
            if (tabLayout != null && tabLayout.getSelectedTabPosition() != i2) {
                tabLayout.selectTab(tabLayout.getTabAt(i2), this.mScrollState == 0);
            }
        }
    }

    public static class ViewPagerOnTabSelectedListener implements OnTabSelectedListener {
        private final ViewPager mViewPager;

        public ViewPagerOnTabSelectedListener(ViewPager viewPager) {
            this.mViewPager = viewPager;
        }

        public void onTabSelected(Tab tab) {
            this.mViewPager.setCurrentItem(tab.getPosition());
        }

        public void onTabUnselected(Tab tab) {
        }

        public void onTabReselected(Tab tab) {
        }
    }
}
