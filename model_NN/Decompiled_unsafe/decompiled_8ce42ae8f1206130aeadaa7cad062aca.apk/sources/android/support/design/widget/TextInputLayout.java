package android.support.design.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.design.R;
import android.support.design.widget.ValueAnimatorCompat;
import android.support.v4.view.AccessibilityDelegateCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v4.view.ViewPropertyAnimatorListenerAdapter;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v4.widget.Space;
import android.support.v7.widget.TintManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TextInputLayout extends LinearLayout {
    private static final int ANIMATION_DURATION = 200;
    private static final int INVALID_MAX_LENGTH = -1;
    private ValueAnimatorCompat mAnimator;
    /* access modifiers changed from: private */
    public final CollapsingTextHelper mCollapsingTextHelper;
    /* access modifiers changed from: private */
    public boolean mCounterEnabled;
    private int mCounterMaxLength;
    private int mCounterOverflowTextAppearance;
    private boolean mCounterOverflowed;
    private int mCounterTextAppearance;
    private TextView mCounterView;
    private ColorStateList mDefaultTextColor;
    /* access modifiers changed from: private */
    public EditText mEditText;
    private boolean mErrorEnabled;
    private boolean mErrorShown;
    private int mErrorTextAppearance;
    /* access modifiers changed from: private */
    public TextView mErrorView;
    private ColorStateList mFocusedTextColor;
    private CharSequence mHint;
    private boolean mHintAnimationEnabled;
    private LinearLayout mIndicatorArea;
    private Paint mTmpPaint;

    public TextInputLayout(Context context) {
        this(context, null);
    }

    public TextInputLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public TextInputLayout(android.content.Context r16, android.util.AttributeSet r17, int r18) {
        /*
            r15 = this;
            r0 = r15
            r1 = r16
            r2 = r17
            r3 = r18
            r8 = r0
            r9 = r1
            r10 = r2
            r8.<init>(r9, r10)
            r8 = r0
            android.support.design.widget.CollapsingTextHelper r9 = new android.support.design.widget.CollapsingTextHelper
            r13 = r9
            r9 = r13
            r10 = r13
            r11 = r0
            r10.<init>(r11)
            r8.mCollapsingTextHelper = r9
            r8 = r1
            android.support.design.widget.ThemeUtils.checkAppCompatTheme(r8)
            r8 = r0
            r9 = 1
            r8.setOrientation(r9)
            r8 = r0
            r9 = 0
            r8.setWillNotDraw(r9)
            r8 = r0
            r9 = 1
            r8.setAddStatesFromChildren(r9)
            r8 = r0
            android.support.design.widget.CollapsingTextHelper r8 = r8.mCollapsingTextHelper
            android.view.animation.Interpolator r9 = android.support.design.widget.AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR
            r8.setTextSizeInterpolator(r9)
            r8 = r0
            android.support.design.widget.CollapsingTextHelper r8 = r8.mCollapsingTextHelper
            android.view.animation.AccelerateInterpolator r9 = new android.view.animation.AccelerateInterpolator
            r13 = r9
            r9 = r13
            r10 = r13
            r10.<init>()
            r8.setPositionInterpolator(r9)
            r8 = r0
            android.support.design.widget.CollapsingTextHelper r8 = r8.mCollapsingTextHelper
            r9 = 8388659(0x800033, float:1.1755015E-38)
            r8.setCollapsedTextGravity(r9)
            r8 = r1
            r9 = r2
            int[] r10 = android.support.design.R.styleable.TextInputLayout
            r11 = r3
            int r12 = android.support.design.R.style.Widget_Design_TextInputLayout
            android.content.res.TypedArray r8 = r8.obtainStyledAttributes(r9, r10, r11, r12)
            r4 = r8
            r8 = r0
            r9 = r4
            int r10 = android.support.design.R.styleable.TextInputLayout_android_hint
            java.lang.CharSequence r9 = r9.getText(r10)
            r8.setHint(r9)
            r8 = r0
            r9 = r4
            int r10 = android.support.design.R.styleable.TextInputLayout_hintAnimationEnabled
            r11 = 1
            boolean r9 = r9.getBoolean(r10, r11)
            r8.mHintAnimationEnabled = r9
            r8 = r4
            int r9 = android.support.design.R.styleable.TextInputLayout_android_textColorHint
            boolean r8 = r8.hasValue(r9)
            if (r8 == 0) goto L_0x0088
            r8 = r0
            r9 = r0
            r10 = r4
            int r11 = android.support.design.R.styleable.TextInputLayout_android_textColorHint
            android.content.res.ColorStateList r10 = r10.getColorStateList(r11)
            r13 = r9
            r14 = r10
            r9 = r14
            r10 = r13
            r11 = r14
            r10.mFocusedTextColor = r11
            r8.mDefaultTextColor = r9
        L_0x0088:
            r8 = r4
            int r9 = android.support.design.R.styleable.TextInputLayout_hintTextAppearance
            r10 = -1
            int r8 = r8.getResourceId(r9, r10)
            r5 = r8
            r8 = r5
            r9 = -1
            if (r8 == r9) goto L_0x00a1
            r8 = r0
            r9 = r4
            int r10 = android.support.design.R.styleable.TextInputLayout_hintTextAppearance
            r11 = 0
            int r9 = r9.getResourceId(r10, r11)
            r8.setHintTextAppearance(r9)
        L_0x00a1:
            r8 = r0
            r9 = r4
            int r10 = android.support.design.R.styleable.TextInputLayout_errorTextAppearance
            r11 = 0
            int r9 = r9.getResourceId(r10, r11)
            r8.mErrorTextAppearance = r9
            r8 = r4
            int r9 = android.support.design.R.styleable.TextInputLayout_errorEnabled
            r10 = 0
            boolean r8 = r8.getBoolean(r9, r10)
            r6 = r8
            r8 = r4
            int r9 = android.support.design.R.styleable.TextInputLayout_counterEnabled
            r10 = 0
            boolean r8 = r8.getBoolean(r9, r10)
            r7 = r8
            r8 = r0
            r9 = r4
            int r10 = android.support.design.R.styleable.TextInputLayout_counterMaxLength
            r11 = -1
            int r9 = r9.getInt(r10, r11)
            r8.setCounterMaxLength(r9)
            r8 = r0
            r9 = r4
            int r10 = android.support.design.R.styleable.TextInputLayout_counterTextAppearance
            r11 = 0
            int r9 = r9.getResourceId(r10, r11)
            r8.mCounterTextAppearance = r9
            r8 = r0
            r9 = r4
            int r10 = android.support.design.R.styleable.TextInputLayout_counterOverflowTextAppearance
            r11 = 0
            int r9 = r9.getResourceId(r10, r11)
            r8.mCounterOverflowTextAppearance = r9
            r8 = r4
            r8.recycle()
            r8 = r0
            r9 = r6
            r8.setErrorEnabled(r9)
            r8 = r0
            r9 = r7
            r8.setCounterEnabled(r9)
            r8 = r0
            int r8 = android.support.v4.view.ViewCompat.getImportantForAccessibility(r8)
            if (r8 != 0) goto L_0x00fa
            r8 = r0
            r9 = 1
            android.support.v4.view.ViewCompat.setImportantForAccessibility(r8, r9)
        L_0x00fa:
            r8 = r0
            android.support.design.widget.TextInputLayout$TextInputAccessibilityDelegate r9 = new android.support.design.widget.TextInputLayout$TextInputAccessibilityDelegate
            r13 = r9
            r9 = r13
            r10 = r13
            r11 = r0
            r12 = 0
            r10.<init>()
            android.support.v4.view.ViewCompat.setAccessibilityDelegate(r8, r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.design.widget.TextInputLayout.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        View view2 = view;
        int i2 = i;
        ViewGroup.LayoutParams layoutParams2 = layoutParams;
        if (view2 instanceof EditText) {
            setEditText((EditText) view2);
            super.addView(view2, 0, updateEditTextMargin(layoutParams2));
            return;
        }
        super.addView(view2, i2, layoutParams2);
    }

    public void setTypeface(@Nullable Typeface typeface) {
        this.mCollapsingTextHelper.setTypefaces(typeface);
    }

    @NonNull
    public Typeface getTypeface() {
        return this.mCollapsingTextHelper.getCollapsedTypeface();
    }

    private void setEditText(EditText editText) {
        TextWatcher textWatcher;
        Throwable th;
        EditText editText2 = editText;
        if (this.mEditText != null) {
            Throwable th2 = th;
            new IllegalArgumentException("We already have an EditText, can only have one");
            throw th2;
        }
        this.mEditText = editText2;
        this.mCollapsingTextHelper.setTypefaces(this.mEditText.getTypeface());
        this.mCollapsingTextHelper.setExpandedTextSize(this.mEditText.getTextSize());
        this.mCollapsingTextHelper.setExpandedTextGravity(this.mEditText.getGravity());
        new TextWatcher() {
            public void afterTextChanged(Editable editable) {
                Editable editable2 = editable;
                TextInputLayout.this.updateLabelVisibility(true);
                if (TextInputLayout.this.mCounterEnabled) {
                    TextInputLayout.this.updateCounter(editable2.length());
                }
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }
        };
        this.mEditText.addTextChangedListener(textWatcher);
        if (this.mDefaultTextColor == null) {
            this.mDefaultTextColor = this.mEditText.getHintTextColors();
        }
        if (TextUtils.isEmpty(this.mHint)) {
            setHint(this.mEditText.getHint());
            this.mEditText.setHint((CharSequence) null);
        }
        if (this.mCounterView != null) {
            updateCounter(this.mEditText.getText().length());
        }
        if (this.mIndicatorArea != null) {
            adjustIndicatorPadding();
        }
        updateLabelVisibility(false);
    }

    private LinearLayout.LayoutParams updateEditTextMargin(ViewGroup.LayoutParams layoutParams) {
        LinearLayout.LayoutParams layoutParams2;
        LinearLayout.LayoutParams layoutParams3;
        Paint paint;
        ViewGroup.LayoutParams layoutParams4 = layoutParams;
        if (layoutParams4 instanceof LinearLayout.LayoutParams) {
            layoutParams3 = (LinearLayout.LayoutParams) layoutParams4;
        } else {
            layoutParams3 = layoutParams2;
            new LinearLayout.LayoutParams(layoutParams4);
        }
        LinearLayout.LayoutParams layoutParams5 = layoutParams3;
        if (this.mTmpPaint == null) {
            new Paint();
            this.mTmpPaint = paint;
        }
        Typeface typeface = this.mTmpPaint.setTypeface(this.mCollapsingTextHelper.getCollapsedTypeface());
        this.mTmpPaint.setTextSize(this.mCollapsingTextHelper.getCollapsedTextSize());
        layoutParams5.topMargin = (int) (-this.mTmpPaint.ascent());
        return layoutParams5;
    }

    /* access modifiers changed from: private */
    public void updateLabelVisibility(boolean z) {
        boolean z2 = z;
        boolean z3 = this.mEditText != null && !TextUtils.isEmpty(this.mEditText.getText());
        boolean arrayContains = arrayContains(getDrawableState(), 16842908);
        boolean z4 = !TextUtils.isEmpty(getError());
        if (this.mDefaultTextColor != null) {
            this.mCollapsingTextHelper.setExpandedTextColor(this.mDefaultTextColor.getDefaultColor());
        }
        if (this.mCounterOverflowed && this.mCounterView != null) {
            this.mCollapsingTextHelper.setCollapsedTextColor(this.mCounterView.getCurrentTextColor());
        } else if (z4 && this.mErrorView != null) {
            this.mCollapsingTextHelper.setCollapsedTextColor(this.mErrorView.getCurrentTextColor());
        } else if (arrayContains && this.mFocusedTextColor != null) {
            this.mCollapsingTextHelper.setCollapsedTextColor(this.mFocusedTextColor.getDefaultColor());
        } else if (this.mDefaultTextColor != null) {
            this.mCollapsingTextHelper.setCollapsedTextColor(this.mDefaultTextColor.getDefaultColor());
        }
        if (z3 || arrayContains || z4) {
            collapseHint(z2);
        } else {
            expandHint(z2);
        }
    }

    @Nullable
    public EditText getEditText() {
        return this.mEditText;
    }

    public void setHint(@Nullable CharSequence charSequence) {
        CharSequence charSequence2 = charSequence;
        this.mHint = charSequence2;
        this.mCollapsingTextHelper.setText(charSequence2);
        sendAccessibilityEvent(2048);
    }

    @Nullable
    public CharSequence getHint() {
        return this.mHint;
    }

    public void setHintTextAppearance(@StyleRes int i) {
        this.mCollapsingTextHelper.setCollapsedTextAppearance(i);
        this.mFocusedTextColor = ColorStateList.valueOf(this.mCollapsingTextHelper.getCollapsedTextColor());
        if (this.mEditText != null) {
            updateLabelVisibility(false);
            this.mEditText.setLayoutParams(updateEditTextMargin(this.mEditText.getLayoutParams()));
            this.mEditText.requestLayout();
        }
    }

    private void addIndicator(TextView textView, int i) {
        LinearLayout linearLayout;
        View view;
        ViewGroup.LayoutParams layoutParams;
        TextView textView2 = textView;
        int i2 = i;
        if (this.mIndicatorArea == null) {
            new LinearLayout(getContext());
            this.mIndicatorArea = linearLayout;
            this.mIndicatorArea.setOrientation(0);
            addView(this.mIndicatorArea, -1, -2);
            new Space(getContext());
            new LinearLayout.LayoutParams(0, 0, 1.0f);
            this.mIndicatorArea.addView(view, layoutParams);
            if (this.mEditText != null) {
                adjustIndicatorPadding();
            }
        }
        this.mIndicatorArea.setVisibility(0);
        this.mIndicatorArea.addView(textView2, i2);
    }

    private void adjustIndicatorPadding() {
        ViewCompat.setPaddingRelative(this.mIndicatorArea, ViewCompat.getPaddingStart(this.mEditText), 0, ViewCompat.getPaddingEnd(this.mEditText), this.mEditText.getPaddingBottom());
    }

    private void removeIndicator(TextView textView) {
        TextView textView2 = textView;
        if (this.mIndicatorArea != null) {
            this.mIndicatorArea.removeView(textView2);
            if (this.mIndicatorArea.getChildCount() == 0) {
                this.mIndicatorArea.setVisibility(8);
            }
        }
    }

    public void setErrorEnabled(boolean z) {
        TextView textView;
        boolean z2 = z;
        if (this.mErrorEnabled != z2) {
            if (this.mErrorView != null) {
                ViewCompat.animate(this.mErrorView).cancel();
            }
            if (z2) {
                new TextView(getContext());
                this.mErrorView = textView;
                this.mErrorView.setTextAppearance(getContext(), this.mErrorTextAppearance);
                this.mErrorView.setVisibility(4);
                ViewCompat.setAccessibilityLiveRegion(this.mErrorView, 1);
                addIndicator(this.mErrorView, 0);
            } else {
                this.mErrorShown = false;
                updateEditTextBackground();
                removeIndicator(this.mErrorView);
                this.mErrorView = null;
            }
            this.mErrorEnabled = z2;
        }
    }

    public boolean isErrorEnabled() {
        return this.mErrorEnabled;
    }

    public void setError(@Nullable CharSequence charSequence) {
        ViewPropertyAnimatorListener viewPropertyAnimatorListener;
        ViewPropertyAnimatorListener viewPropertyAnimatorListener2;
        CharSequence charSequence2 = charSequence;
        if (!this.mErrorEnabled) {
            if (!TextUtils.isEmpty(charSequence2)) {
                setErrorEnabled(true);
            } else {
                return;
            }
        }
        if (!TextUtils.isEmpty(charSequence2)) {
            ViewCompat.setAlpha(this.mErrorView, 0.0f);
            this.mErrorView.setText(charSequence2);
            new ViewPropertyAnimatorListenerAdapter() {
                public void onAnimationStart(View view) {
                    view.setVisibility(0);
                }
            };
            ViewCompat.animate(this.mErrorView).alpha(1.0f).setDuration(200).setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR).setListener(viewPropertyAnimatorListener2).start();
            this.mErrorShown = true;
            updateEditTextBackground();
            updateLabelVisibility(true);
        } else if (this.mErrorView.getVisibility() == 0) {
            new ViewPropertyAnimatorListenerAdapter() {
                public void onAnimationEnd(View view) {
                    view.setVisibility(4);
                    TextInputLayout.this.updateLabelVisibility(true);
                }
            };
            ViewCompat.animate(this.mErrorView).alpha(0.0f).setDuration(200).setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR).setListener(viewPropertyAnimatorListener).start();
            this.mErrorShown = false;
            updateEditTextBackground();
        }
    }

    public void setCounterEnabled(boolean z) {
        TextView textView;
        boolean z2 = z;
        if (this.mCounterEnabled != z2) {
            if (z2) {
                new TextView(getContext());
                this.mCounterView = textView;
                this.mCounterView.setMaxLines(1);
                this.mCounterView.setTextAppearance(getContext(), this.mCounterTextAppearance);
                ViewCompat.setAccessibilityLiveRegion(this.mCounterView, 1);
                addIndicator(this.mCounterView, -1);
                if (this.mEditText == null) {
                    updateCounter(0);
                } else {
                    updateCounter(this.mEditText.getText().length());
                }
            } else {
                removeIndicator(this.mCounterView);
                this.mCounterView = null;
            }
            this.mCounterEnabled = z2;
        }
    }

    public void setCounterMaxLength(int i) {
        int i2 = i;
        if (this.mCounterMaxLength != i2) {
            if (i2 > 0) {
                this.mCounterMaxLength = i2;
            } else {
                this.mCounterMaxLength = -1;
            }
            if (this.mCounterEnabled) {
                updateCounter(this.mEditText == null ? 0 : this.mEditText.getText().length());
            }
        }
    }

    public int getCounterMaxLength() {
        return this.mCounterMaxLength;
    }

    /* access modifiers changed from: private */
    public void updateCounter(int i) {
        int i2 = i;
        boolean z = this.mCounterOverflowed;
        if (this.mCounterMaxLength == -1) {
            this.mCounterView.setText(String.valueOf(i2));
            this.mCounterOverflowed = false;
        } else {
            this.mCounterOverflowed = i2 > this.mCounterMaxLength;
            if (z != this.mCounterOverflowed) {
                this.mCounterView.setTextAppearance(getContext(), this.mCounterOverflowed ? this.mCounterOverflowTextAppearance : this.mCounterTextAppearance);
            }
            TextView textView = this.mCounterView;
            Context context = getContext();
            int i3 = R.string.character_counter_pattern;
            Object[] objArr = new Object[2];
            objArr[0] = Integer.valueOf(i2);
            Object[] objArr2 = objArr;
            objArr2[1] = Integer.valueOf(this.mCounterMaxLength);
            textView.setText(context.getString(i3, objArr2));
        }
        if (this.mEditText != null && z != this.mCounterOverflowed) {
            updateLabelVisibility(false);
            updateEditTextBackground();
        }
    }

    private void updateEditTextBackground() {
        if (this.mErrorShown && this.mErrorView != null) {
            ViewCompat.setBackgroundTintList(this.mEditText, ColorStateList.valueOf(this.mErrorView.getCurrentTextColor()));
        } else if (!this.mCounterOverflowed || this.mCounterView == null) {
            ViewCompat.setBackgroundTintList(this.mEditText, TintManager.get(getContext()).getTintList(R.drawable.abc_edit_text_material));
        } else {
            ViewCompat.setBackgroundTintList(this.mEditText, ColorStateList.valueOf(this.mCounterView.getCurrentTextColor()));
        }
    }

    @Nullable
    public CharSequence getError() {
        if (!this.mErrorEnabled || this.mErrorView == null || this.mErrorView.getVisibility() != 0) {
            return null;
        }
        return this.mErrorView.getText();
    }

    public boolean isHintAnimationEnabled() {
        return this.mHintAnimationEnabled;
    }

    public void setHintAnimationEnabled(boolean z) {
        this.mHintAnimationEnabled = z;
    }

    public void draw(Canvas canvas) {
        Canvas canvas2 = canvas;
        super.draw(canvas2);
        this.mCollapsingTextHelper.draw(canvas2);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = i2;
        int i6 = i4;
        super.onLayout(z, i, i5, i3, i6);
        if (this.mEditText != null) {
            int left = this.mEditText.getLeft() + this.mEditText.getCompoundPaddingLeft();
            int right = this.mEditText.getRight() - this.mEditText.getCompoundPaddingRight();
            this.mCollapsingTextHelper.setExpandedBounds(left, this.mEditText.getTop() + this.mEditText.getCompoundPaddingTop(), right, this.mEditText.getBottom() - this.mEditText.getCompoundPaddingBottom());
            this.mCollapsingTextHelper.setCollapsedBounds(left, getPaddingTop(), right, (i6 - i5) - getPaddingBottom());
            this.mCollapsingTextHelper.recalculate();
        }
    }

    public void refreshDrawableState() {
        super.refreshDrawableState();
        updateLabelVisibility(ViewCompat.isLaidOut(this));
    }

    private void collapseHint(boolean z) {
        boolean z2 = z;
        if (this.mAnimator != null && this.mAnimator.isRunning()) {
            this.mAnimator.cancel();
        }
        if (!z2 || !this.mHintAnimationEnabled) {
            this.mCollapsingTextHelper.setExpansionFraction(1.0f);
        } else {
            animateToExpansionFraction(1.0f);
        }
    }

    private void expandHint(boolean z) {
        boolean z2 = z;
        if (this.mAnimator != null && this.mAnimator.isRunning()) {
            this.mAnimator.cancel();
        }
        if (!z2 || !this.mHintAnimationEnabled) {
            this.mCollapsingTextHelper.setExpansionFraction(0.0f);
        } else {
            animateToExpansionFraction(0.0f);
        }
    }

    private void animateToExpansionFraction(float f) {
        ValueAnimatorCompat.AnimatorUpdateListener animatorUpdateListener;
        float f2 = f;
        if (this.mCollapsingTextHelper.getExpansionFraction() != f2) {
            if (this.mAnimator == null) {
                this.mAnimator = ViewUtils.createAnimator();
                this.mAnimator.setInterpolator(AnimationUtils.LINEAR_INTERPOLATOR);
                this.mAnimator.setDuration(200);
                new ValueAnimatorCompat.AnimatorUpdateListener() {
                    public void onAnimationUpdate(ValueAnimatorCompat valueAnimatorCompat) {
                        TextInputLayout.this.mCollapsingTextHelper.setExpansionFraction(valueAnimatorCompat.getAnimatedFloatValue());
                    }
                };
                this.mAnimator.setUpdateListener(animatorUpdateListener);
            }
            this.mAnimator.setFloatValues(this.mCollapsingTextHelper.getExpansionFraction(), f2);
            this.mAnimator.start();
        }
    }

    private int getThemeAttrColor(int i) {
        TypedValue typedValue;
        new TypedValue();
        TypedValue typedValue2 = typedValue;
        if (getContext().getTheme().resolveAttribute(i, typedValue2, true)) {
            return typedValue2.data;
        }
        return -65281;
    }

    private class TextInputAccessibilityDelegate extends AccessibilityDelegateCompat {
        private TextInputAccessibilityDelegate() {
        }

        public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            AccessibilityEvent accessibilityEvent2 = accessibilityEvent;
            super.onInitializeAccessibilityEvent(view, accessibilityEvent2);
            accessibilityEvent2.setClassName(TextInputLayout.class.getSimpleName());
        }

        public void onPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            AccessibilityEvent accessibilityEvent2 = accessibilityEvent;
            super.onPopulateAccessibilityEvent(view, accessibilityEvent2);
            CharSequence text = TextInputLayout.this.mCollapsingTextHelper.getText();
            if (!TextUtils.isEmpty(text)) {
                boolean add = accessibilityEvent2.getText().add(text);
            }
        }

        public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            AccessibilityNodeInfoCompat accessibilityNodeInfoCompat2 = accessibilityNodeInfoCompat;
            super.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat2);
            accessibilityNodeInfoCompat2.setClassName(TextInputLayout.class.getSimpleName());
            CharSequence text = TextInputLayout.this.mCollapsingTextHelper.getText();
            if (!TextUtils.isEmpty(text)) {
                accessibilityNodeInfoCompat2.setText(text);
            }
            if (TextInputLayout.this.mEditText != null) {
                accessibilityNodeInfoCompat2.setLabelFor(TextInputLayout.this.mEditText);
            }
            CharSequence text2 = TextInputLayout.this.mErrorView != null ? TextInputLayout.this.mErrorView.getText() : null;
            if (!TextUtils.isEmpty(text2)) {
                accessibilityNodeInfoCompat2.setContentInvalid(true);
                accessibilityNodeInfoCompat2.setError(text2);
            }
        }
    }

    private static boolean arrayContains(int[] iArr, int i) {
        int i2 = i;
        int[] iArr2 = iArr;
        int length = iArr2.length;
        for (int i3 = 0; i3 < length; i3++) {
            if (iArr2[i3] == i2) {
                return true;
            }
        }
        return false;
    }
}
