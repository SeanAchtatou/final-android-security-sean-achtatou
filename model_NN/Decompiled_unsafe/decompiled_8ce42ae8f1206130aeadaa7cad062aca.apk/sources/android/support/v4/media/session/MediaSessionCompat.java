package android.support.v4.media.session;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.os.SystemClock;
import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.RatingCompat;
import android.support.v4.media.VolumeProviderCompat;
import android.support.v4.media.session.IMediaSession;
import android.support.v4.media.session.MediaSessionCompatApi14;
import android.support.v4.media.session.MediaSessionCompatApi21;
import android.support.v4.media.session.MediaSessionCompatApi23;
import android.support.v4.media.session.PlaybackStateCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MediaSessionCompat {
    public static final int FLAG_HANDLES_MEDIA_BUTTONS = 1;
    public static final int FLAG_HANDLES_TRANSPORT_CONTROLS = 2;
    private static final String TAG = "MediaSessionCompat";
    private final ArrayList<OnActiveChangeListener> mActiveListeners;
    private final MediaControllerCompat mController;
    private final MediaSessionImpl mImpl;

    interface MediaSessionImpl {
        Object getMediaSession();

        Object getRemoteControlClient();

        Token getSessionToken();

        boolean isActive();

        void release();

        void sendSessionEvent(String str, Bundle bundle);

        void setActive(boolean z);

        void setCallback(Callback callback, Handler handler);

        void setExtras(Bundle bundle);

        void setFlags(int i);

        void setMediaButtonReceiver(PendingIntent pendingIntent);

        void setMetadata(MediaMetadataCompat mediaMetadataCompat);

        void setPlaybackState(PlaybackStateCompat playbackStateCompat);

        void setPlaybackToLocal(int i);

        void setPlaybackToRemote(VolumeProviderCompat volumeProviderCompat);

        void setQueue(List<QueueItem> list);

        void setQueueTitle(CharSequence charSequence);

        void setRatingType(int i);

        void setSessionActivity(PendingIntent pendingIntent);
    }

    public interface OnActiveChangeListener {
        void onActiveChanged();
    }

    @Retention(RetentionPolicy.SOURCE)
    public @interface SessionFlags {
    }

    public MediaSessionCompat(Context context, String str) {
        this(context, str, null, null);
    }

    public MediaSessionCompat(Context context, String str, ComponentName componentName, PendingIntent pendingIntent) {
        ArrayList<OnActiveChangeListener> arrayList;
        MediaSessionImpl mediaSessionImpl;
        MediaControllerCompat mediaControllerCompat;
        MediaSessionImpl mediaSessionImpl2;
        Intent intent;
        Intent intent2;
        ComponentName componentName2;
        Throwable th;
        Throwable th2;
        Context context2 = context;
        String str2 = str;
        ComponentName componentName3 = componentName;
        PendingIntent pendingIntent2 = pendingIntent;
        new ArrayList<>();
        this.mActiveListeners = arrayList;
        if (context2 == null) {
            Throwable th3 = th2;
            new IllegalArgumentException("context must not be null");
            throw th3;
        } else if (TextUtils.isEmpty(str2)) {
            Throwable th4 = th;
            new IllegalArgumentException("tag must not be null or empty");
            throw th4;
        } else {
            if (componentName3 == null) {
                new Intent("android.intent.action.MEDIA_BUTTON");
                Intent intent3 = intent2;
                Intent intent4 = intent3.setPackage(context2.getPackageName());
                List<ResolveInfo> queryBroadcastReceivers = context2.getPackageManager().queryBroadcastReceivers(intent3, 0);
                if (queryBroadcastReceivers.size() == 1) {
                    ResolveInfo resolveInfo = queryBroadcastReceivers.get(0);
                    new ComponentName(resolveInfo.activityInfo.packageName, resolveInfo.activityInfo.name);
                    componentName3 = componentName2;
                } else if (queryBroadcastReceivers.size() > 1) {
                    int w = Log.w(TAG, "More than one BroadcastReceiver that handles android.intent.action.MEDIA_BUTTON was found, using null. Provide a specific ComponentName to use as this session's media button receiver");
                }
            }
            if (componentName3 != null && pendingIntent2 == null) {
                new Intent("android.intent.action.MEDIA_BUTTON");
                Intent intent5 = intent;
                Intent component = intent5.setComponent(componentName3);
                pendingIntent2 = PendingIntent.getBroadcast(context2, 0, intent5, 0);
            }
            if (Build.VERSION.SDK_INT >= 21) {
                new MediaSessionImplApi21(context2, str2);
                this.mImpl = mediaSessionImpl2;
                this.mImpl.setMediaButtonReceiver(pendingIntent2);
            } else {
                new MediaSessionImplBase(context2, str2, componentName3, pendingIntent2);
                this.mImpl = mediaSessionImpl;
            }
            new MediaControllerCompat(context2, this);
            this.mController = mediaControllerCompat;
        }
    }

    private MediaSessionCompat(Context context, MediaSessionImpl mediaSessionImpl) {
        ArrayList<OnActiveChangeListener> arrayList;
        MediaControllerCompat mediaControllerCompat;
        new ArrayList<>();
        this.mActiveListeners = arrayList;
        this.mImpl = mediaSessionImpl;
        new MediaControllerCompat(context, this);
        this.mController = mediaControllerCompat;
    }

    public void setCallback(Callback callback) {
        setCallback(callback, null);
    }

    public void setCallback(Callback callback, Handler handler) {
        Handler handler2;
        Handler handler3;
        Handler handler4 = handler;
        MediaSessionImpl mediaSessionImpl = this.mImpl;
        Callback callback2 = callback;
        if (handler4 != null) {
            handler3 = handler4;
        } else {
            handler3 = handler2;
            new Handler();
        }
        mediaSessionImpl.setCallback(callback2, handler3);
    }

    public void setSessionActivity(PendingIntent pendingIntent) {
        this.mImpl.setSessionActivity(pendingIntent);
    }

    public void setMediaButtonReceiver(PendingIntent pendingIntent) {
        this.mImpl.setMediaButtonReceiver(pendingIntent);
    }

    public void setFlags(int i) {
        this.mImpl.setFlags(i);
    }

    public void setPlaybackToLocal(int i) {
        this.mImpl.setPlaybackToLocal(i);
    }

    public void setPlaybackToRemote(VolumeProviderCompat volumeProviderCompat) {
        Throwable th;
        VolumeProviderCompat volumeProviderCompat2 = volumeProviderCompat;
        if (volumeProviderCompat2 == null) {
            Throwable th2 = th;
            new IllegalArgumentException("volumeProvider may not be null!");
            throw th2;
        }
        this.mImpl.setPlaybackToRemote(volumeProviderCompat2);
    }

    public void setActive(boolean z) {
        this.mImpl.setActive(z);
        Iterator<OnActiveChangeListener> it = this.mActiveListeners.iterator();
        while (it.hasNext()) {
            it.next().onActiveChanged();
        }
    }

    public boolean isActive() {
        return this.mImpl.isActive();
    }

    public void sendSessionEvent(String str, Bundle bundle) {
        Throwable th;
        String str2 = str;
        Bundle bundle2 = bundle;
        if (TextUtils.isEmpty(str2)) {
            Throwable th2 = th;
            new IllegalArgumentException("event cannot be null or empty");
            throw th2;
        }
        this.mImpl.sendSessionEvent(str2, bundle2);
    }

    public void release() {
        this.mImpl.release();
    }

    public Token getSessionToken() {
        return this.mImpl.getSessionToken();
    }

    public MediaControllerCompat getController() {
        return this.mController;
    }

    public void setPlaybackState(PlaybackStateCompat playbackStateCompat) {
        this.mImpl.setPlaybackState(playbackStateCompat);
    }

    public void setMetadata(MediaMetadataCompat mediaMetadataCompat) {
        this.mImpl.setMetadata(mediaMetadataCompat);
    }

    public void setQueue(List<QueueItem> list) {
        this.mImpl.setQueue(list);
    }

    public void setQueueTitle(CharSequence charSequence) {
        this.mImpl.setQueueTitle(charSequence);
    }

    public void setRatingType(int i) {
        this.mImpl.setRatingType(i);
    }

    public void setExtras(Bundle bundle) {
        this.mImpl.setExtras(bundle);
    }

    public Object getMediaSession() {
        return this.mImpl.getMediaSession();
    }

    public Object getRemoteControlClient() {
        return this.mImpl.getRemoteControlClient();
    }

    public void addOnActiveChangeListener(OnActiveChangeListener onActiveChangeListener) {
        Throwable th;
        OnActiveChangeListener onActiveChangeListener2 = onActiveChangeListener;
        if (onActiveChangeListener2 == null) {
            Throwable th2 = th;
            new IllegalArgumentException("Listener may not be null");
            throw th2;
        }
        boolean add = this.mActiveListeners.add(onActiveChangeListener2);
    }

    public void removeOnActiveChangeListener(OnActiveChangeListener onActiveChangeListener) {
        Throwable th;
        OnActiveChangeListener onActiveChangeListener2 = onActiveChangeListener;
        if (onActiveChangeListener2 == null) {
            Throwable th2 = th;
            new IllegalArgumentException("Listener may not be null");
            throw th2;
        }
        boolean remove = this.mActiveListeners.remove(onActiveChangeListener2);
    }

    public static MediaSessionCompat obtain(Context context, Object obj) {
        MediaSessionCompat mediaSessionCompat;
        MediaSessionImpl mediaSessionImpl;
        new MediaSessionImplApi21(obj);
        new MediaSessionCompat(context, mediaSessionImpl);
        return mediaSessionCompat;
    }

    public static abstract class Callback {
        final Object mCallbackObj;

        public Callback() {
            MediaSessionCompatApi21.Callback callback;
            MediaSessionCompatApi23.Callback callback2;
            if (Build.VERSION.SDK_INT >= 23) {
                new StubApi23();
                this.mCallbackObj = MediaSessionCompatApi23.createCallback(callback2);
            } else if (Build.VERSION.SDK_INT >= 21) {
                new StubApi21();
                this.mCallbackObj = MediaSessionCompatApi21.createCallback(callback);
            } else {
                this.mCallbackObj = null;
            }
        }

        public void onCommand(String str, Bundle bundle, ResultReceiver resultReceiver) {
        }

        public boolean onMediaButtonEvent(Intent intent) {
            return false;
        }

        public void onPlay() {
        }

        public void onPlayFromMediaId(String str, Bundle bundle) {
        }

        public void onPlayFromSearch(String str, Bundle bundle) {
        }

        public void onPlayFromUri(Uri uri, Bundle bundle) {
        }

        public void onSkipToQueueItem(long j) {
        }

        public void onPause() {
        }

        public void onSkipToNext() {
        }

        public void onSkipToPrevious() {
        }

        public void onFastForward() {
        }

        public void onRewind() {
        }

        public void onStop() {
        }

        public void onSeekTo(long j) {
        }

        public void onSetRating(RatingCompat ratingCompat) {
        }

        public void onCustomAction(String str, Bundle bundle) {
        }

        private class StubApi21 implements MediaSessionCompatApi21.Callback {
            private StubApi21() {
            }

            public void onCommand(String str, Bundle bundle, ResultReceiver resultReceiver) {
                Callback.this.onCommand(str, bundle, resultReceiver);
            }

            public boolean onMediaButtonEvent(Intent intent) {
                return Callback.this.onMediaButtonEvent(intent);
            }

            public void onPlay() {
                Callback.this.onPlay();
            }

            public void onPlayFromMediaId(String str, Bundle bundle) {
                Callback.this.onPlayFromMediaId(str, bundle);
            }

            public void onPlayFromSearch(String str, Bundle bundle) {
                Callback.this.onPlayFromSearch(str, bundle);
            }

            public void onSkipToQueueItem(long j) {
                Callback.this.onSkipToQueueItem(j);
            }

            public void onPause() {
                Callback.this.onPause();
            }

            public void onSkipToNext() {
                Callback.this.onSkipToNext();
            }

            public void onSkipToPrevious() {
                Callback.this.onSkipToPrevious();
            }

            public void onFastForward() {
                Callback.this.onFastForward();
            }

            public void onRewind() {
                Callback.this.onRewind();
            }

            public void onStop() {
                Callback.this.onStop();
            }

            public void onSeekTo(long j) {
                Callback.this.onSeekTo(j);
            }

            public void onSetRating(Object obj) {
                Callback.this.onSetRating(RatingCompat.fromRating(obj));
            }

            public void onCustomAction(String str, Bundle bundle) {
                Callback.this.onCustomAction(str, bundle);
            }
        }

        private class StubApi23 extends StubApi21 implements MediaSessionCompatApi23.Callback {
            final /* synthetic */ Callback this$0;

            /* JADX WARNING: Illegal instructions before constructor call */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            private StubApi23(android.support.v4.media.session.MediaSessionCompat.Callback r6) {
                /*
                    r5 = this;
                    r0 = r5
                    r1 = r6
                    r2 = r0
                    r3 = r1
                    r2.this$0 = r3
                    r2 = r0
                    r3 = r1
                    r4 = 0
                    r2.<init>()
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: android.support.v4.media.session.MediaSessionCompat.Callback.StubApi23.<init>(android.support.v4.media.session.MediaSessionCompat$Callback):void");
            }

            public void onPlayFromUri(Uri uri, Bundle bundle) {
                this.this$0.onPlayFromUri(uri, bundle);
            }
        }
    }

    public static final class Token implements Parcelable {
        public static final Parcelable.Creator<Token> CREATOR;
        private final Object mInner;

        Token(Object obj) {
            this.mInner = obj;
        }

        public static Token fromToken(Object obj) {
            Token token;
            Object obj2 = obj;
            if (obj2 == null || Build.VERSION.SDK_INT < 21) {
                return null;
            }
            new Token(MediaSessionCompatApi21.verifyToken(obj2));
            return token;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            Parcel parcel2 = parcel;
            int i2 = i;
            if (Build.VERSION.SDK_INT >= 21) {
                parcel2.writeParcelable((Parcelable) this.mInner, i2);
            } else {
                parcel2.writeStrongBinder((IBinder) this.mInner);
            }
        }

        public Object getToken() {
            return this.mInner;
        }

        static {
            Parcelable.Creator<Token> creator;
            new Parcelable.Creator<Token>() {
                public Token createFromParcel(Parcel parcel) {
                    Object readStrongBinder;
                    Token token;
                    Parcel parcel2 = parcel;
                    if (Build.VERSION.SDK_INT >= 21) {
                        readStrongBinder = parcel2.readParcelable(null);
                    } else {
                        readStrongBinder = parcel2.readStrongBinder();
                    }
                    new Token(readStrongBinder);
                    return token;
                }

                public Token[] newArray(int i) {
                    return new Token[i];
                }
            };
            CREATOR = creator;
        }
    }

    public static final class QueueItem implements Parcelable {
        public static final Parcelable.Creator<QueueItem> CREATOR;
        public static final int UNKNOWN_ID = -1;
        private final MediaDescriptionCompat mDescription;
        private final long mId;
        private Object mItem;

        public QueueItem(MediaDescriptionCompat mediaDescriptionCompat, long j) {
            this(null, mediaDescriptionCompat, j);
        }

        private QueueItem(Object obj, MediaDescriptionCompat mediaDescriptionCompat, long j) {
            Throwable th;
            Throwable th2;
            Object obj2 = obj;
            MediaDescriptionCompat mediaDescriptionCompat2 = mediaDescriptionCompat;
            long j2 = j;
            if (mediaDescriptionCompat2 == null) {
                Throwable th3 = th2;
                new IllegalArgumentException("Description cannot be null.");
                throw th3;
            } else if (j2 == -1) {
                Throwable th4 = th;
                new IllegalArgumentException("Id cannot be QueueItem.UNKNOWN_ID");
                throw th4;
            } else {
                this.mDescription = mediaDescriptionCompat2;
                this.mId = j2;
                this.mItem = obj2;
            }
        }

        private QueueItem(Parcel parcel) {
            Parcel parcel2 = parcel;
            this.mDescription = MediaDescriptionCompat.CREATOR.createFromParcel(parcel2);
            this.mId = parcel2.readLong();
        }

        public MediaDescriptionCompat getDescription() {
            return this.mDescription;
        }

        public long getQueueId() {
            return this.mId;
        }

        public void writeToParcel(Parcel parcel, int i) {
            Parcel parcel2 = parcel;
            this.mDescription.writeToParcel(parcel2, i);
            parcel2.writeLong(this.mId);
        }

        public int describeContents() {
            return 0;
        }

        public Object getQueueItem() {
            if (this.mItem != null || Build.VERSION.SDK_INT < 21) {
                return this.mItem;
            }
            this.mItem = MediaSessionCompatApi21.QueueItem.createItem(this.mDescription.getMediaDescription(), this.mId);
            return this.mItem;
        }

        public static QueueItem obtain(Object obj) {
            QueueItem queueItem;
            Object obj2 = obj;
            new QueueItem(obj2, MediaDescriptionCompat.fromMediaDescription(MediaSessionCompatApi21.QueueItem.getDescription(obj2)), MediaSessionCompatApi21.QueueItem.getQueueId(obj2));
            return queueItem;
        }

        static {
            Parcelable.Creator<QueueItem> creator;
            new Parcelable.Creator<QueueItem>() {
                public QueueItem createFromParcel(Parcel parcel) {
                    QueueItem queueItem;
                    new QueueItem(parcel);
                    return queueItem;
                }

                public QueueItem[] newArray(int i) {
                    return new QueueItem[i];
                }
            };
            CREATOR = creator;
        }

        public String toString() {
            StringBuilder sb;
            new StringBuilder();
            return sb.append("MediaSession.QueueItem {Description=").append(this.mDescription).append(", Id=").append(this.mId).append(" }").toString();
        }
    }

    static final class ResultReceiverWrapper implements Parcelable {
        public static final Parcelable.Creator<ResultReceiverWrapper> CREATOR;
        /* access modifiers changed from: private */
        public ResultReceiver mResultReceiver;

        public ResultReceiverWrapper(ResultReceiver resultReceiver) {
            this.mResultReceiver = resultReceiver;
        }

        ResultReceiverWrapper(Parcel parcel) {
            this.mResultReceiver = (ResultReceiver) ResultReceiver.CREATOR.createFromParcel(parcel);
        }

        static {
            Parcelable.Creator<ResultReceiverWrapper> creator;
            new Parcelable.Creator<ResultReceiverWrapper>() {
                public ResultReceiverWrapper createFromParcel(Parcel parcel) {
                    ResultReceiverWrapper resultReceiverWrapper;
                    new ResultReceiverWrapper(parcel);
                    return resultReceiverWrapper;
                }

                public ResultReceiverWrapper[] newArray(int i) {
                    return new ResultReceiverWrapper[i];
                }
            };
            CREATOR = creator;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            this.mResultReceiver.writeToParcel(parcel, i);
        }
    }

    static class MediaSessionImplBase implements MediaSessionImpl {
        /* access modifiers changed from: private */
        public final AudioManager mAudioManager;
        /* access modifiers changed from: private */
        public Callback mCallback;
        private final ComponentName mComponentName;
        private final Context mContext;
        /* access modifiers changed from: private */
        public final RemoteCallbackList<IMediaControllerCallback> mControllerCallbacks;
        /* access modifiers changed from: private */
        public boolean mDestroyed = false;
        /* access modifiers changed from: private */
        public Bundle mExtras;
        /* access modifiers changed from: private */
        public int mFlags;
        /* access modifiers changed from: private */
        public final MessageHandler mHandler;
        private boolean mIsActive = false;
        private boolean mIsMbrRegistered = false;
        private boolean mIsRccRegistered = false;
        /* access modifiers changed from: private */
        public int mLocalStream;
        /* access modifiers changed from: private */
        public final Object mLock;
        private final PendingIntent mMediaButtonEventReceiver;
        /* access modifiers changed from: private */
        public MediaMetadataCompat mMetadata;
        /* access modifiers changed from: private */
        public final String mPackageName;
        /* access modifiers changed from: private */
        public List<QueueItem> mQueue;
        /* access modifiers changed from: private */
        public CharSequence mQueueTitle;
        /* access modifiers changed from: private */
        public int mRatingType;
        private final Object mRccObj;
        /* access modifiers changed from: private */
        public PendingIntent mSessionActivity;
        /* access modifiers changed from: private */
        public PlaybackStateCompat mState;
        private final MediaSessionStub mStub;
        /* access modifiers changed from: private */
        public final String mTag;
        private final Token mToken;
        private VolumeProviderCompat.Callback mVolumeCallback;
        /* access modifiers changed from: private */
        public VolumeProviderCompat mVolumeProvider;
        /* access modifiers changed from: private */
        public int mVolumeType;

        public MediaSessionImplBase(Context context, String str, ComponentName componentName, PendingIntent pendingIntent) {
            Object obj;
            RemoteCallbackList<IMediaControllerCallback> remoteCallbackList;
            VolumeProviderCompat.Callback callback;
            MediaSessionStub mediaSessionStub;
            Token token;
            MessageHandler messageHandler;
            Throwable th;
            Context context2 = context;
            String str2 = str;
            ComponentName componentName2 = componentName;
            PendingIntent pendingIntent2 = pendingIntent;
            new Object();
            this.mLock = obj;
            new RemoteCallbackList<>();
            this.mControllerCallbacks = remoteCallbackList;
            new VolumeProviderCompat.Callback() {
                public void onVolumeChanged(VolumeProviderCompat volumeProviderCompat) {
                    ParcelableVolumeInfo parcelableVolumeInfo;
                    VolumeProviderCompat volumeProviderCompat2 = volumeProviderCompat;
                    if (MediaSessionImplBase.this.mVolumeProvider == volumeProviderCompat2) {
                        new ParcelableVolumeInfo(MediaSessionImplBase.this.mVolumeType, MediaSessionImplBase.this.mLocalStream, volumeProviderCompat2.getVolumeControl(), volumeProviderCompat2.getMaxVolume(), volumeProviderCompat2.getCurrentVolume());
                        MediaSessionImplBase.this.sendVolumeInfoChanged(parcelableVolumeInfo);
                    }
                }
            };
            this.mVolumeCallback = callback;
            if (componentName2 == null) {
                Throwable th2 = th;
                new IllegalArgumentException("MediaButtonReceiver component may not be null.");
                throw th2;
            }
            this.mContext = context2;
            this.mPackageName = context2.getPackageName();
            this.mAudioManager = (AudioManager) context2.getSystemService("audio");
            this.mTag = str2;
            this.mComponentName = componentName2;
            this.mMediaButtonEventReceiver = pendingIntent2;
            new MediaSessionStub();
            this.mStub = mediaSessionStub;
            new Token(this.mStub);
            this.mToken = token;
            new MessageHandler(Looper.myLooper());
            this.mHandler = messageHandler;
            this.mRatingType = 0;
            this.mVolumeType = 1;
            this.mLocalStream = 3;
            if (Build.VERSION.SDK_INT >= 14) {
                this.mRccObj = MediaSessionCompatApi14.createRemoteControlClient(pendingIntent2);
            } else {
                this.mRccObj = null;
            }
        }

        public void setCallback(Callback callback, Handler handler) {
            MediaSessionCompatApi14.Callback callback2;
            Callback callback3 = callback;
            Handler handler2 = handler;
            if (callback3 != this.mCallback) {
                if (callback3 == null || Build.VERSION.SDK_INT < 18) {
                    if (Build.VERSION.SDK_INT >= 18) {
                        MediaSessionCompatApi18.setOnPlaybackPositionUpdateListener(this.mRccObj, null);
                    }
                    if (Build.VERSION.SDK_INT >= 19) {
                        MediaSessionCompatApi19.setOnMetadataUpdateListener(this.mRccObj, null);
                    }
                } else {
                    if (handler2 == null) {
                        new Handler();
                    }
                    final Callback callback4 = callback3;
                    new MediaSessionCompatApi14.Callback() {
                        public void onStop() {
                            callback4.onStop();
                        }

                        public void onSkipToPrevious() {
                            callback4.onSkipToPrevious();
                        }

                        public void onSkipToNext() {
                            callback4.onSkipToNext();
                        }

                        public void onSetRating(Object obj) {
                            callback4.onSetRating(RatingCompat.fromRating(obj));
                        }

                        public void onSeekTo(long j) {
                            callback4.onSeekTo(j);
                        }

                        public void onRewind() {
                            callback4.onRewind();
                        }

                        public void onPlay() {
                            callback4.onPlay();
                        }

                        public void onPause() {
                            callback4.onPause();
                        }

                        public boolean onMediaButtonEvent(Intent intent) {
                            return callback4.onMediaButtonEvent(intent);
                        }

                        public void onFastForward() {
                            callback4.onFastForward();
                        }

                        public void onCommand(String str, Bundle bundle, ResultReceiver resultReceiver) {
                            callback4.onCommand(str, bundle, resultReceiver);
                        }
                    };
                    MediaSessionCompatApi14.Callback callback5 = callback2;
                    if (Build.VERSION.SDK_INT >= 18) {
                        MediaSessionCompatApi18.setOnPlaybackPositionUpdateListener(this.mRccObj, MediaSessionCompatApi18.createPlaybackPositionUpdateListener(callback5));
                    }
                    if (Build.VERSION.SDK_INT >= 19) {
                        MediaSessionCompatApi19.setOnMetadataUpdateListener(this.mRccObj, MediaSessionCompatApi19.createMetadataUpdateListener(callback5));
                    }
                }
                this.mCallback = callback3;
            }
        }

        public void setFlags(int i) {
            int i2 = i;
            Object obj = this.mLock;
            Object obj2 = obj;
            synchronized (obj) {
                try {
                    this.mFlags = i2;
                    boolean update = update();
                } catch (Throwable th) {
                    while (true) {
                        throw th;
                    }
                }
            }
        }

        public void setPlaybackToLocal(int i) {
            ParcelableVolumeInfo parcelableVolumeInfo;
            if (this.mVolumeProvider != null) {
                this.mVolumeProvider.setCallback(null);
            }
            this.mVolumeType = 1;
            new ParcelableVolumeInfo(this.mVolumeType, this.mLocalStream, 2, this.mAudioManager.getStreamMaxVolume(this.mLocalStream), this.mAudioManager.getStreamVolume(this.mLocalStream));
            sendVolumeInfoChanged(parcelableVolumeInfo);
        }

        public void setPlaybackToRemote(VolumeProviderCompat volumeProviderCompat) {
            ParcelableVolumeInfo parcelableVolumeInfo;
            Throwable th;
            VolumeProviderCompat volumeProviderCompat2 = volumeProviderCompat;
            if (volumeProviderCompat2 == null) {
                Throwable th2 = th;
                new IllegalArgumentException("volumeProvider may not be null");
                throw th2;
            }
            if (this.mVolumeProvider != null) {
                this.mVolumeProvider.setCallback(null);
            }
            this.mVolumeType = 2;
            this.mVolumeProvider = volumeProviderCompat2;
            new ParcelableVolumeInfo(this.mVolumeType, this.mLocalStream, this.mVolumeProvider.getVolumeControl(), this.mVolumeProvider.getMaxVolume(), this.mVolumeProvider.getCurrentVolume());
            sendVolumeInfoChanged(parcelableVolumeInfo);
            volumeProviderCompat2.setCallback(this.mVolumeCallback);
        }

        public void setActive(boolean z) {
            boolean z2 = z;
            if (z2 != this.mIsActive) {
                this.mIsActive = z2;
                if (update()) {
                    setMetadata(this.mMetadata);
                    setPlaybackState(this.mState);
                }
            }
        }

        public boolean isActive() {
            return this.mIsActive;
        }

        public void sendSessionEvent(String str, Bundle bundle) {
            sendEvent(str, bundle);
        }

        public void release() {
            this.mIsActive = false;
            this.mDestroyed = true;
            boolean update = update();
            sendSessionDestroyed();
        }

        public Token getSessionToken() {
            return this.mToken;
        }

        /* JADX INFO: finally extract failed */
        public void setPlaybackState(PlaybackStateCompat playbackStateCompat) {
            PlaybackStateCompat playbackStateCompat2 = playbackStateCompat;
            Object obj = this.mLock;
            Object obj2 = obj;
            synchronized (obj) {
                try {
                    this.mState = playbackStateCompat2;
                    sendState(playbackStateCompat2);
                    if (this.mIsActive) {
                        if (playbackStateCompat2 != null) {
                            if (Build.VERSION.SDK_INT >= 18) {
                                MediaSessionCompatApi18.setState(this.mRccObj, playbackStateCompat2.getState(), playbackStateCompat2.getPosition(), playbackStateCompat2.getPlaybackSpeed(), playbackStateCompat2.getLastPositionUpdateTime());
                            } else if (Build.VERSION.SDK_INT >= 14) {
                                MediaSessionCompatApi14.setState(this.mRccObj, playbackStateCompat2.getState());
                            }
                            if (Build.VERSION.SDK_INT >= 19) {
                                MediaSessionCompatApi19.setTransportControlFlags(this.mRccObj, playbackStateCompat2.getActions());
                            } else if (Build.VERSION.SDK_INT >= 18) {
                                MediaSessionCompatApi18.setTransportControlFlags(this.mRccObj, playbackStateCompat2.getActions());
                            } else if (Build.VERSION.SDK_INT >= 14) {
                                MediaSessionCompatApi14.setTransportControlFlags(this.mRccObj, playbackStateCompat2.getActions());
                            }
                        } else if (Build.VERSION.SDK_INT >= 14) {
                            MediaSessionCompatApi14.setState(this.mRccObj, 0);
                            MediaSessionCompatApi14.setTransportControlFlags(this.mRccObj, 0);
                        }
                    }
                } catch (Throwable th) {
                    while (true) {
                        throw th;
                    }
                }
            }
        }

        /* JADX INFO: finally extract failed */
        public void setMetadata(MediaMetadataCompat mediaMetadataCompat) {
            MediaMetadataCompat mediaMetadataCompat2 = mediaMetadataCompat;
            Object obj = this.mLock;
            Object obj2 = obj;
            synchronized (obj) {
                try {
                    this.mMetadata = mediaMetadataCompat2;
                    sendMetadata(mediaMetadataCompat2);
                    if (this.mIsActive) {
                        if (Build.VERSION.SDK_INT >= 19) {
                            MediaSessionCompatApi19.setMetadata(this.mRccObj, mediaMetadataCompat2 == null ? null : mediaMetadataCompat2.getBundle(), this.mState == null ? 0 : this.mState.getActions());
                        } else if (Build.VERSION.SDK_INT >= 14) {
                            MediaSessionCompatApi14.setMetadata(this.mRccObj, mediaMetadataCompat2 == null ? null : mediaMetadataCompat2.getBundle());
                        }
                    }
                } catch (Throwable th) {
                    while (true) {
                        throw th;
                    }
                }
            }
        }

        public void setSessionActivity(PendingIntent pendingIntent) {
            PendingIntent pendingIntent2 = pendingIntent;
            Object obj = this.mLock;
            Object obj2 = obj;
            synchronized (obj) {
                try {
                    this.mSessionActivity = pendingIntent2;
                } catch (Throwable th) {
                    Throwable th2 = th;
                    Object obj3 = obj2;
                    throw th2;
                }
            }
        }

        public void setMediaButtonReceiver(PendingIntent pendingIntent) {
        }

        public void setQueue(List<QueueItem> list) {
            List<QueueItem> list2 = list;
            this.mQueue = list2;
            sendQueue(list2);
        }

        public void setQueueTitle(CharSequence charSequence) {
            CharSequence charSequence2 = charSequence;
            this.mQueueTitle = charSequence2;
            sendQueueTitle(charSequence2);
        }

        public Object getMediaSession() {
            return null;
        }

        public Object getRemoteControlClient() {
            return this.mRccObj;
        }

        public void setRatingType(int i) {
            this.mRatingType = i;
        }

        public void setExtras(Bundle bundle) {
            this.mExtras = bundle;
        }

        private boolean update() {
            boolean z = false;
            if (this.mIsActive) {
                if (Build.VERSION.SDK_INT >= 8) {
                    if (!this.mIsMbrRegistered && (this.mFlags & 1) != 0) {
                        if (Build.VERSION.SDK_INT >= 18) {
                            MediaSessionCompatApi18.registerMediaButtonEventReceiver(this.mContext, this.mMediaButtonEventReceiver, this.mComponentName);
                        } else {
                            MediaSessionCompatApi8.registerMediaButtonEventReceiver(this.mContext, this.mComponentName);
                        }
                        this.mIsMbrRegistered = true;
                    } else if (this.mIsMbrRegistered && (this.mFlags & 1) == 0) {
                        if (Build.VERSION.SDK_INT >= 18) {
                            MediaSessionCompatApi18.unregisterMediaButtonEventReceiver(this.mContext, this.mMediaButtonEventReceiver, this.mComponentName);
                        } else {
                            MediaSessionCompatApi8.unregisterMediaButtonEventReceiver(this.mContext, this.mComponentName);
                        }
                        this.mIsMbrRegistered = false;
                    }
                }
                if (Build.VERSION.SDK_INT >= 14) {
                    if (!this.mIsRccRegistered && (this.mFlags & 2) != 0) {
                        MediaSessionCompatApi14.registerRemoteControlClient(this.mContext, this.mRccObj);
                        this.mIsRccRegistered = true;
                        z = true;
                    } else if (this.mIsRccRegistered && (this.mFlags & 2) == 0) {
                        MediaSessionCompatApi14.setState(this.mRccObj, 0);
                        MediaSessionCompatApi14.unregisterRemoteControlClient(this.mContext, this.mRccObj);
                        this.mIsRccRegistered = false;
                    }
                }
            } else {
                if (this.mIsMbrRegistered) {
                    if (Build.VERSION.SDK_INT >= 18) {
                        MediaSessionCompatApi18.unregisterMediaButtonEventReceiver(this.mContext, this.mMediaButtonEventReceiver, this.mComponentName);
                    } else {
                        MediaSessionCompatApi8.unregisterMediaButtonEventReceiver(this.mContext, this.mComponentName);
                    }
                    this.mIsMbrRegistered = false;
                }
                if (this.mIsRccRegistered) {
                    MediaSessionCompatApi14.setState(this.mRccObj, 0);
                    MediaSessionCompatApi14.unregisterRemoteControlClient(this.mContext, this.mRccObj);
                    this.mIsRccRegistered = false;
                }
            }
            return z;
        }

        /* access modifiers changed from: private */
        public void adjustVolume(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            if (this.mVolumeType != 2) {
                this.mAudioManager.adjustStreamVolume(this.mLocalStream, i3, i4);
            } else if (this.mVolumeProvider != null) {
                this.mVolumeProvider.onAdjustVolume(i3);
            }
        }

        /* access modifiers changed from: private */
        public void setVolumeTo(int i, int i2) {
            int i3 = i;
            int i4 = i2;
            if (this.mVolumeType != 2) {
                this.mAudioManager.setStreamVolume(this.mLocalStream, i3, i4);
            } else if (this.mVolumeProvider != null) {
                this.mVolumeProvider.onSetVolumeTo(i3);
            }
        }

        /* JADX INFO: finally extract failed */
        /* access modifiers changed from: private */
        public PlaybackStateCompat getStateWithUpdatedPosition() {
            PlaybackStateCompat playbackStateCompat;
            PlaybackStateCompat.Builder builder;
            long j = -1;
            Object obj = this.mLock;
            Object obj2 = obj;
            synchronized (obj) {
                try {
                    PlaybackStateCompat playbackStateCompat2 = this.mState;
                    if (this.mMetadata != null && this.mMetadata.containsKey(MediaMetadataCompat.METADATA_KEY_DURATION)) {
                        j = this.mMetadata.getLong(MediaMetadataCompat.METADATA_KEY_DURATION);
                    }
                    PlaybackStateCompat playbackStateCompat3 = null;
                    if (playbackStateCompat2 != null && (playbackStateCompat2.getState() == 3 || playbackStateCompat2.getState() == 4 || playbackStateCompat2.getState() == 5)) {
                        long lastPositionUpdateTime = playbackStateCompat2.getLastPositionUpdateTime();
                        long elapsedRealtime = SystemClock.elapsedRealtime();
                        if (lastPositionUpdateTime > 0) {
                            long playbackSpeed = ((long) (playbackStateCompat2.getPlaybackSpeed() * ((float) (elapsedRealtime - lastPositionUpdateTime)))) + playbackStateCompat2.getPosition();
                            if (j >= 0 && playbackSpeed > j) {
                                playbackSpeed = j;
                            } else if (playbackSpeed < 0) {
                                playbackSpeed = 0;
                            }
                            new PlaybackStateCompat.Builder(playbackStateCompat2);
                            PlaybackStateCompat.Builder builder2 = builder;
                            PlaybackStateCompat.Builder state = builder2.setState(playbackStateCompat2.getState(), playbackSpeed, playbackStateCompat2.getPlaybackSpeed(), elapsedRealtime);
                            playbackStateCompat3 = builder2.build();
                        }
                    }
                    if (playbackStateCompat3 == null) {
                        playbackStateCompat = playbackStateCompat2;
                    } else {
                        playbackStateCompat = playbackStateCompat3;
                    }
                    return playbackStateCompat;
                } catch (Throwable th) {
                    while (true) {
                        throw th;
                    }
                }
            }
        }

        /* access modifiers changed from: private */
        public void sendVolumeInfoChanged(ParcelableVolumeInfo parcelableVolumeInfo) {
            ParcelableVolumeInfo parcelableVolumeInfo2 = parcelableVolumeInfo;
            for (int beginBroadcast = this.mControllerCallbacks.beginBroadcast() - 1; beginBroadcast >= 0; beginBroadcast--) {
                try {
                    this.mControllerCallbacks.getBroadcastItem(beginBroadcast).onVolumeInfoChanged(parcelableVolumeInfo2);
                } catch (RemoteException e) {
                }
            }
            this.mControllerCallbacks.finishBroadcast();
        }

        private void sendSessionDestroyed() {
            for (int beginBroadcast = this.mControllerCallbacks.beginBroadcast() - 1; beginBroadcast >= 0; beginBroadcast--) {
                try {
                    this.mControllerCallbacks.getBroadcastItem(beginBroadcast).onSessionDestroyed();
                } catch (RemoteException e) {
                }
            }
            this.mControllerCallbacks.finishBroadcast();
            this.mControllerCallbacks.kill();
        }

        private void sendEvent(String str, Bundle bundle) {
            String str2 = str;
            Bundle bundle2 = bundle;
            for (int beginBroadcast = this.mControllerCallbacks.beginBroadcast() - 1; beginBroadcast >= 0; beginBroadcast--) {
                try {
                    this.mControllerCallbacks.getBroadcastItem(beginBroadcast).onEvent(str2, bundle2);
                } catch (RemoteException e) {
                }
            }
            this.mControllerCallbacks.finishBroadcast();
        }

        private void sendState(PlaybackStateCompat playbackStateCompat) {
            PlaybackStateCompat playbackStateCompat2 = playbackStateCompat;
            for (int beginBroadcast = this.mControllerCallbacks.beginBroadcast() - 1; beginBroadcast >= 0; beginBroadcast--) {
                try {
                    this.mControllerCallbacks.getBroadcastItem(beginBroadcast).onPlaybackStateChanged(playbackStateCompat2);
                } catch (RemoteException e) {
                }
            }
            this.mControllerCallbacks.finishBroadcast();
        }

        private void sendMetadata(MediaMetadataCompat mediaMetadataCompat) {
            MediaMetadataCompat mediaMetadataCompat2 = mediaMetadataCompat;
            for (int beginBroadcast = this.mControllerCallbacks.beginBroadcast() - 1; beginBroadcast >= 0; beginBroadcast--) {
                try {
                    this.mControllerCallbacks.getBroadcastItem(beginBroadcast).onMetadataChanged(mediaMetadataCompat2);
                } catch (RemoteException e) {
                }
            }
            this.mControllerCallbacks.finishBroadcast();
        }

        private void sendQueue(List<QueueItem> list) {
            List<QueueItem> list2 = list;
            for (int beginBroadcast = this.mControllerCallbacks.beginBroadcast() - 1; beginBroadcast >= 0; beginBroadcast--) {
                try {
                    this.mControllerCallbacks.getBroadcastItem(beginBroadcast).onQueueChanged(list2);
                } catch (RemoteException e) {
                }
            }
            this.mControllerCallbacks.finishBroadcast();
        }

        private void sendQueueTitle(CharSequence charSequence) {
            CharSequence charSequence2 = charSequence;
            for (int beginBroadcast = this.mControllerCallbacks.beginBroadcast() - 1; beginBroadcast >= 0; beginBroadcast--) {
                try {
                    this.mControllerCallbacks.getBroadcastItem(beginBroadcast).onQueueTitleChanged(charSequence2);
                } catch (RemoteException e) {
                }
            }
            this.mControllerCallbacks.finishBroadcast();
        }

        class MediaSessionStub extends IMediaSession.Stub {
            MediaSessionStub() {
            }

            public void sendCommand(String str, Bundle bundle, ResultReceiverWrapper resultReceiverWrapper) {
                Object obj;
                new Command(str, bundle, resultReceiverWrapper.mResultReceiver);
                MediaSessionImplBase.this.mHandler.post(15, obj);
            }

            public boolean sendMediaButton(KeyEvent keyEvent) {
                KeyEvent keyEvent2 = keyEvent;
                boolean z = (MediaSessionImplBase.this.mFlags & 1) != 0;
                if (z) {
                    MediaSessionImplBase.this.mHandler.post(14, keyEvent2);
                }
                return z;
            }

            public void registerCallbackListener(IMediaControllerCallback iMediaControllerCallback) {
                IMediaControllerCallback iMediaControllerCallback2 = iMediaControllerCallback;
                if (MediaSessionImplBase.this.mDestroyed) {
                    try {
                        iMediaControllerCallback2.onSessionDestroyed();
                    } catch (Exception e) {
                    }
                } else {
                    boolean register = MediaSessionImplBase.this.mControllerCallbacks.register(iMediaControllerCallback2);
                }
            }

            public void unregisterCallbackListener(IMediaControllerCallback iMediaControllerCallback) {
                boolean unregister = MediaSessionImplBase.this.mControllerCallbacks.unregister(iMediaControllerCallback);
            }

            public String getPackageName() {
                return MediaSessionImplBase.this.mPackageName;
            }

            public String getTag() {
                return MediaSessionImplBase.this.mTag;
            }

            public PendingIntent getLaunchPendingIntent() {
                Object access$1400 = MediaSessionImplBase.this.mLock;
                Object obj = access$1400;
                synchronized (access$1400) {
                    try {
                        PendingIntent access$1500 = MediaSessionImplBase.this.mSessionActivity;
                        return access$1500;
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        Object obj2 = obj;
                        throw th2;
                    }
                }
            }

            public long getFlags() {
                Object access$1400 = MediaSessionImplBase.this.mLock;
                Object obj = access$1400;
                synchronized (access$1400) {
                    try {
                        long access$900 = (long) MediaSessionImplBase.this.mFlags;
                        return access$900;
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        Object obj2 = obj;
                        throw th2;
                    }
                }
            }

            /* JADX INFO: finally extract failed */
            public ParcelableVolumeInfo getVolumeAttributes() {
                int i;
                int streamMaxVolume;
                int streamVolume;
                ParcelableVolumeInfo parcelableVolumeInfo;
                Object access$1400 = MediaSessionImplBase.this.mLock;
                Object obj = access$1400;
                synchronized (access$1400) {
                    try {
                        int access$400 = MediaSessionImplBase.this.mVolumeType;
                        int access$500 = MediaSessionImplBase.this.mLocalStream;
                        VolumeProviderCompat access$300 = MediaSessionImplBase.this.mVolumeProvider;
                        if (access$400 == 2) {
                            i = access$300.getVolumeControl();
                            streamMaxVolume = access$300.getMaxVolume();
                            streamVolume = access$300.getCurrentVolume();
                        } else {
                            i = 2;
                            streamMaxVolume = MediaSessionImplBase.this.mAudioManager.getStreamMaxVolume(access$500);
                            streamVolume = MediaSessionImplBase.this.mAudioManager.getStreamVolume(access$500);
                        }
                        new ParcelableVolumeInfo(access$400, access$500, i, streamMaxVolume, streamVolume);
                        return parcelableVolumeInfo;
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        Object obj2 = obj;
                        throw th2;
                    }
                }
            }

            public void adjustVolume(int i, int i2, String str) {
                MediaSessionImplBase.this.adjustVolume(i, i2);
            }

            public void setVolumeTo(int i, int i2, String str) {
                MediaSessionImplBase.this.setVolumeTo(i, i2);
            }

            public void play() throws RemoteException {
                MediaSessionImplBase.this.mHandler.post(1);
            }

            public void playFromMediaId(String str, Bundle bundle) throws RemoteException {
                MediaSessionImplBase.this.mHandler.post(2, str, bundle);
            }

            public void playFromSearch(String str, Bundle bundle) throws RemoteException {
                MediaSessionImplBase.this.mHandler.post(3, str, bundle);
            }

            public void playFromUri(Uri uri, Bundle bundle) throws RemoteException {
                MediaSessionImplBase.this.mHandler.post(18, uri, bundle);
            }

            public void skipToQueueItem(long j) {
                MediaSessionImplBase.this.mHandler.post(4, Long.valueOf(j));
            }

            public void pause() throws RemoteException {
                MediaSessionImplBase.this.mHandler.post(5);
            }

            public void stop() throws RemoteException {
                MediaSessionImplBase.this.mHandler.post(6);
            }

            public void next() throws RemoteException {
                MediaSessionImplBase.this.mHandler.post(7);
            }

            public void previous() throws RemoteException {
                MediaSessionImplBase.this.mHandler.post(8);
            }

            public void fastForward() throws RemoteException {
                MediaSessionImplBase.this.mHandler.post(9);
            }

            public void rewind() throws RemoteException {
                MediaSessionImplBase.this.mHandler.post(10);
            }

            public void seekTo(long j) throws RemoteException {
                MediaSessionImplBase.this.mHandler.post(11, Long.valueOf(j));
            }

            public void rate(RatingCompat ratingCompat) throws RemoteException {
                MediaSessionImplBase.this.mHandler.post(12, ratingCompat);
            }

            public void sendCustomAction(String str, Bundle bundle) throws RemoteException {
                MediaSessionImplBase.this.mHandler.post(13, str, bundle);
            }

            public MediaMetadataCompat getMetadata() {
                return MediaSessionImplBase.this.mMetadata;
            }

            public PlaybackStateCompat getPlaybackState() {
                return MediaSessionImplBase.this.getStateWithUpdatedPosition();
            }

            public List<QueueItem> getQueue() {
                Object access$1400 = MediaSessionImplBase.this.mLock;
                Object obj = access$1400;
                synchronized (access$1400) {
                    try {
                        List<QueueItem> access$2100 = MediaSessionImplBase.this.mQueue;
                        return access$2100;
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        Object obj2 = obj;
                        throw th2;
                    }
                }
            }

            public CharSequence getQueueTitle() {
                return MediaSessionImplBase.this.mQueueTitle;
            }

            public Bundle getExtras() {
                Object access$1400 = MediaSessionImplBase.this.mLock;
                Object obj = access$1400;
                synchronized (access$1400) {
                    try {
                        Bundle access$2300 = MediaSessionImplBase.this.mExtras;
                        return access$2300;
                    } catch (Throwable th) {
                        Throwable th2 = th;
                        Object obj2 = obj;
                        throw th2;
                    }
                }
            }

            public int getRatingType() {
                return MediaSessionImplBase.this.mRatingType;
            }

            public boolean isTransportControlEnabled() {
                return (MediaSessionImplBase.this.mFlags & 2) != 0;
            }
        }

        private static final class Command {
            public final String command;
            public final Bundle extras;
            public final ResultReceiver stub;

            public Command(String str, Bundle bundle, ResultReceiver resultReceiver) {
                this.command = str;
                this.extras = bundle;
                this.stub = resultReceiver;
            }
        }

        private class MessageHandler extends Handler {
            private static final int KEYCODE_MEDIA_PAUSE = 127;
            private static final int KEYCODE_MEDIA_PLAY = 126;
            private static final int MSG_ADJUST_VOLUME = 16;
            private static final int MSG_COMMAND = 15;
            private static final int MSG_CUSTOM_ACTION = 13;
            private static final int MSG_FAST_FORWARD = 9;
            private static final int MSG_MEDIA_BUTTON = 14;
            private static final int MSG_NEXT = 7;
            private static final int MSG_PAUSE = 5;
            private static final int MSG_PLAY = 1;
            private static final int MSG_PLAY_MEDIA_ID = 2;
            private static final int MSG_PLAY_SEARCH = 3;
            private static final int MSG_PLAY_URI = 18;
            private static final int MSG_PREVIOUS = 8;
            private static final int MSG_RATE = 12;
            private static final int MSG_REWIND = 10;
            private static final int MSG_SEEK_TO = 11;
            private static final int MSG_SET_VOLUME = 17;
            private static final int MSG_SKIP_TO_ITEM = 4;
            private static final int MSG_STOP = 6;

            public MessageHandler(Looper looper) {
                super(looper);
            }

            public void post(int i, Object obj, Bundle bundle) {
                Message obtainMessage = obtainMessage(i, obj);
                obtainMessage.setData(bundle);
                obtainMessage.sendToTarget();
            }

            public void post(int i, Object obj) {
                obtainMessage(i, obj).sendToTarget();
            }

            public void post(int i) {
                post(i, null);
            }

            public void post(int i, Object obj, int i2) {
                obtainMessage(i, i2, 0, obj).sendToTarget();
            }

            public void handleMessage(Message message) {
                Intent intent;
                Message message2 = message;
                if (MediaSessionImplBase.this.mCallback != null) {
                    switch (message2.what) {
                        case 1:
                            MediaSessionImplBase.this.mCallback.onPlay();
                            return;
                        case 2:
                            MediaSessionImplBase.this.mCallback.onPlayFromMediaId((String) message2.obj, message2.getData());
                            return;
                        case 3:
                            MediaSessionImplBase.this.mCallback.onPlayFromSearch((String) message2.obj, message2.getData());
                            return;
                        case 4:
                            MediaSessionImplBase.this.mCallback.onSkipToQueueItem(((Long) message2.obj).longValue());
                            return;
                        case 5:
                            MediaSessionImplBase.this.mCallback.onPause();
                            return;
                        case 6:
                            MediaSessionImplBase.this.mCallback.onStop();
                            return;
                        case 7:
                            MediaSessionImplBase.this.mCallback.onSkipToNext();
                            return;
                        case 8:
                            MediaSessionImplBase.this.mCallback.onSkipToPrevious();
                            return;
                        case 9:
                            MediaSessionImplBase.this.mCallback.onFastForward();
                            return;
                        case 10:
                            MediaSessionImplBase.this.mCallback.onRewind();
                            return;
                        case 11:
                            MediaSessionImplBase.this.mCallback.onSeekTo(((Long) message2.obj).longValue());
                            return;
                        case 12:
                            MediaSessionImplBase.this.mCallback.onSetRating((RatingCompat) message2.obj);
                            return;
                        case 13:
                            MediaSessionImplBase.this.mCallback.onCustomAction((String) message2.obj, message2.getData());
                            return;
                        case 14:
                            KeyEvent keyEvent = (KeyEvent) message2.obj;
                            new Intent("android.intent.action.MEDIA_BUTTON");
                            Intent intent2 = intent;
                            Intent putExtra = intent2.putExtra("android.intent.extra.KEY_EVENT", keyEvent);
                            if (!MediaSessionImplBase.this.mCallback.onMediaButtonEvent(intent2)) {
                                onMediaButtonEvent(keyEvent);
                                return;
                            }
                            return;
                        case 15:
                            Command command = (Command) message2.obj;
                            MediaSessionImplBase.this.mCallback.onCommand(command.command, command.extras, command.stub);
                            return;
                        case 16:
                            MediaSessionImplBase.this.adjustVolume(((Integer) message2.obj).intValue(), 0);
                            return;
                        case 17:
                            MediaSessionImplBase.this.setVolumeTo(((Integer) message2.obj).intValue(), 0);
                            return;
                        case 18:
                            MediaSessionImplBase.this.mCallback.onPlayFromUri((Uri) message2.obj, message2.getData());
                            return;
                        default:
                            return;
                    }
                }
            }

            private void onMediaButtonEvent(KeyEvent keyEvent) {
                KeyEvent keyEvent2 = keyEvent;
                if (keyEvent2 != null && keyEvent2.getAction() == 0) {
                    long actions = MediaSessionImplBase.this.mState == null ? 0 : MediaSessionImplBase.this.mState.getActions();
                    switch (keyEvent2.getKeyCode()) {
                        case 79:
                        case 85:
                            boolean z = MediaSessionImplBase.this.mState != null && MediaSessionImplBase.this.mState.getState() == 3;
                            boolean z2 = (actions & 516) != 0;
                            boolean z3 = (actions & 514) != 0;
                            if (z && z3) {
                                MediaSessionImplBase.this.mCallback.onPause();
                                return;
                            } else if (!z && z2) {
                                MediaSessionImplBase.this.mCallback.onPlay();
                                return;
                            } else {
                                return;
                            }
                        case 86:
                            if ((actions & 1) != 0) {
                                MediaSessionImplBase.this.mCallback.onStop();
                                return;
                            }
                            return;
                        case 87:
                            if ((actions & 32) != 0) {
                                MediaSessionImplBase.this.mCallback.onSkipToNext();
                                return;
                            }
                            return;
                        case 88:
                            if ((actions & 16) != 0) {
                                MediaSessionImplBase.this.mCallback.onSkipToPrevious();
                                return;
                            }
                            return;
                        case 89:
                            if ((actions & 8) != 0) {
                                MediaSessionImplBase.this.mCallback.onRewind();
                                return;
                            }
                            return;
                        case 90:
                            if ((actions & 64) != 0) {
                                MediaSessionImplBase.this.mCallback.onFastForward();
                                return;
                            }
                            return;
                        case 126:
                            if ((actions & 4) != 0) {
                                MediaSessionImplBase.this.mCallback.onPlay();
                                return;
                            }
                            return;
                        case 127:
                            if ((actions & 2) != 0) {
                                MediaSessionImplBase.this.mCallback.onPause();
                                return;
                            }
                            return;
                        default:
                            return;
                    }
                }
            }
        }
    }

    static class MediaSessionImplApi21 implements MediaSessionImpl {
        private PendingIntent mMediaButtonIntent;
        private final Object mSessionObj;
        private final Token mToken;

        public MediaSessionImplApi21(Context context, String str) {
            Token token;
            this.mSessionObj = MediaSessionCompatApi21.createSession(context, str);
            new Token(MediaSessionCompatApi21.getSessionToken(this.mSessionObj));
            this.mToken = token;
        }

        public MediaSessionImplApi21(Object obj) {
            Token token;
            this.mSessionObj = MediaSessionCompatApi21.verifySession(obj);
            new Token(MediaSessionCompatApi21.getSessionToken(this.mSessionObj));
            this.mToken = token;
        }

        public void setCallback(Callback callback, Handler handler) {
            Callback callback2 = callback;
            MediaSessionCompatApi21.setCallback(this.mSessionObj, callback2 == null ? null : callback2.mCallbackObj, handler);
        }

        public void setFlags(int i) {
            MediaSessionCompatApi21.setFlags(this.mSessionObj, i);
        }

        public void setPlaybackToLocal(int i) {
            MediaSessionCompatApi21.setPlaybackToLocal(this.mSessionObj, i);
        }

        public void setPlaybackToRemote(VolumeProviderCompat volumeProviderCompat) {
            MediaSessionCompatApi21.setPlaybackToRemote(this.mSessionObj, volumeProviderCompat.getVolumeProvider());
        }

        public void setActive(boolean z) {
            MediaSessionCompatApi21.setActive(this.mSessionObj, z);
        }

        public boolean isActive() {
            return MediaSessionCompatApi21.isActive(this.mSessionObj);
        }

        public void sendSessionEvent(String str, Bundle bundle) {
            MediaSessionCompatApi21.sendSessionEvent(this.mSessionObj, str, bundle);
        }

        public void release() {
            MediaSessionCompatApi21.release(this.mSessionObj);
        }

        public Token getSessionToken() {
            return this.mToken;
        }

        public void setPlaybackState(PlaybackStateCompat playbackStateCompat) {
            PlaybackStateCompat playbackStateCompat2 = playbackStateCompat;
            MediaSessionCompatApi21.setPlaybackState(this.mSessionObj, playbackStateCompat2 == null ? null : playbackStateCompat2.getPlaybackState());
        }

        public void setMetadata(MediaMetadataCompat mediaMetadataCompat) {
            MediaMetadataCompat mediaMetadataCompat2 = mediaMetadataCompat;
            MediaSessionCompatApi21.setMetadata(this.mSessionObj, mediaMetadataCompat2 == null ? null : mediaMetadataCompat2.getMediaMetadata());
        }

        public void setSessionActivity(PendingIntent pendingIntent) {
            MediaSessionCompatApi21.setSessionActivity(this.mSessionObj, pendingIntent);
        }

        public void setMediaButtonReceiver(PendingIntent pendingIntent) {
            PendingIntent pendingIntent2 = pendingIntent;
            this.mMediaButtonIntent = pendingIntent2;
            MediaSessionCompatApi21.setMediaButtonReceiver(this.mSessionObj, pendingIntent2);
        }

        public void setQueue(List<QueueItem> list) {
            List list2;
            List<QueueItem> list3 = list;
            List list4 = null;
            if (list3 != null) {
                new ArrayList();
                list4 = list2;
                for (QueueItem queueItem : list3) {
                    boolean add = list4.add(queueItem.getQueueItem());
                }
            }
            MediaSessionCompatApi21.setQueue(this.mSessionObj, list4);
        }

        public void setQueueTitle(CharSequence charSequence) {
            MediaSessionCompatApi21.setQueueTitle(this.mSessionObj, charSequence);
        }

        public void setRatingType(int i) {
            int i2 = i;
            if (Build.VERSION.SDK_INT >= 22) {
                MediaSessionCompatApi22.setRatingType(this.mSessionObj, i2);
            }
        }

        public void setExtras(Bundle bundle) {
            MediaSessionCompatApi21.setExtras(this.mSessionObj, bundle);
        }

        public Object getMediaSession() {
            return this.mSessionObj;
        }

        public Object getRemoteControlClient() {
            return null;
        }
    }
}
