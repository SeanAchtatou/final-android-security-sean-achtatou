package android.support.v4.provider;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import java.io.File;

public abstract class DocumentFile {
    static final String TAG = "DocumentFile";
    private final DocumentFile mParent;

    public abstract boolean canRead();

    public abstract boolean canWrite();

    public abstract DocumentFile createDirectory(String str);

    public abstract DocumentFile createFile(String str, String str2);

    public abstract boolean delete();

    public abstract boolean exists();

    public abstract String getName();

    public abstract String getType();

    public abstract Uri getUri();

    public abstract boolean isDirectory();

    public abstract boolean isFile();

    public abstract long lastModified();

    public abstract long length();

    public abstract DocumentFile[] listFiles();

    public abstract boolean renameTo(String str);

    DocumentFile(DocumentFile documentFile) {
        this.mParent = documentFile;
    }

    public static DocumentFile fromFile(File file) {
        DocumentFile documentFile;
        new RawDocumentFile(null, file);
        return documentFile;
    }

    public static DocumentFile fromSingleUri(Context context, Uri uri) {
        DocumentFile documentFile;
        Context context2 = context;
        Uri uri2 = uri;
        if (Build.VERSION.SDK_INT < 19) {
            return null;
        }
        new SingleDocumentFile(null, context2, uri2);
        return documentFile;
    }

    public static DocumentFile fromTreeUri(Context context, Uri uri) {
        DocumentFile documentFile;
        Context context2 = context;
        Uri uri2 = uri;
        if (Build.VERSION.SDK_INT < 21) {
            return null;
        }
        new TreeDocumentFile(null, context2, DocumentsContractApi21.prepareTreeUri(uri2));
        return documentFile;
    }

    public static boolean isDocumentUri(Context context, Uri uri) {
        Context context2 = context;
        Uri uri2 = uri;
        if (Build.VERSION.SDK_INT >= 19) {
            return DocumentsContractApi19.isDocumentUri(context2, uri2);
        }
        return false;
    }

    public DocumentFile getParentFile() {
        return this.mParent;
    }

    public DocumentFile findFile(String str) {
        String str2 = str;
        DocumentFile[] listFiles = listFiles();
        int length = listFiles.length;
        for (int i = 0; i < length; i++) {
            DocumentFile documentFile = listFiles[i];
            if (str2.equals(documentFile.getName())) {
                return documentFile;
            }
        }
        return null;
    }
}
