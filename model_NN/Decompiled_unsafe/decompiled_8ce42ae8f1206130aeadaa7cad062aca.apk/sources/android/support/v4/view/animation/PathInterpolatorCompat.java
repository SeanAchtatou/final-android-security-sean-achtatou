package android.support.v4.view.animation;

import android.graphics.Path;
import android.os.Build;
import android.view.animation.Interpolator;

public class PathInterpolatorCompat {
    private PathInterpolatorCompat() {
    }

    public static Interpolator create(Path path) {
        Path path2 = path;
        if (Build.VERSION.SDK_INT >= 21) {
            return PathInterpolatorCompatApi21.create(path2);
        }
        return PathInterpolatorCompatBase.create(path2);
    }

    public static Interpolator create(float f, float f2) {
        float f3 = f;
        float f4 = f2;
        if (Build.VERSION.SDK_INT >= 21) {
            return PathInterpolatorCompatApi21.create(f3, f4);
        }
        return PathInterpolatorCompatBase.create(f3, f4);
    }

    public static Interpolator create(float f, float f2, float f3, float f4) {
        float f5 = f;
        float f6 = f2;
        float f7 = f3;
        float f8 = f4;
        if (Build.VERSION.SDK_INT >= 21) {
            return PathInterpolatorCompatApi21.create(f5, f6, f7, f8);
        }
        return PathInterpolatorCompatBase.create(f5, f6, f7, f8);
    }
}
