package android.support.v4.view;

import android.support.v4.view.LayoutInflaterCompatHC;
import android.view.LayoutInflater;

class LayoutInflaterCompatLollipop {
    LayoutInflaterCompatLollipop() {
    }

    static void setFactory(LayoutInflater layoutInflater, LayoutInflaterFactory layoutInflaterFactory) {
        LayoutInflater.Factory2 factory2;
        LayoutInflater.Factory2 factory22;
        LayoutInflaterFactory layoutInflaterFactory2 = layoutInflaterFactory;
        LayoutInflater layoutInflater2 = layoutInflater;
        if (layoutInflaterFactory2 != null) {
            factory2 = factory22;
            new LayoutInflaterCompatHC.FactoryWrapperHC(layoutInflaterFactory2);
        } else {
            factory2 = null;
        }
        layoutInflater2.setFactory2(factory2);
    }
}
