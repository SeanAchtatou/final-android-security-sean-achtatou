package android.support.v4.text;

import android.os.Build;
import java.util.Locale;

public class ICUCompat {
    private static final ICUCompatImpl IMPL;

    interface ICUCompatImpl {
        String maximizeAndGetScript(Locale locale);
    }

    static class ICUCompatImplBase implements ICUCompatImpl {
        ICUCompatImplBase() {
        }

        public String maximizeAndGetScript(Locale locale) {
            return null;
        }
    }

    static class ICUCompatImplIcs implements ICUCompatImpl {
        ICUCompatImplIcs() {
        }

        public String maximizeAndGetScript(Locale locale) {
            return ICUCompatIcs.maximizeAndGetScript(locale);
        }
    }

    static class ICUCompatImplLollipop implements ICUCompatImpl {
        ICUCompatImplLollipop() {
        }

        public String maximizeAndGetScript(Locale locale) {
            return ICUCompatApi23.maximizeAndGetScript(locale);
        }
    }

    static {
        ICUCompatImpl iCUCompatImpl;
        ICUCompatImpl iCUCompatImpl2;
        ICUCompatImpl iCUCompatImpl3;
        int i = Build.VERSION.SDK_INT;
        if (i >= 21) {
            new ICUCompatImplLollipop();
            IMPL = iCUCompatImpl3;
        } else if (i >= 14) {
            new ICUCompatImplIcs();
            IMPL = iCUCompatImpl2;
        } else {
            new ICUCompatImplBase();
            IMPL = iCUCompatImpl;
        }
    }

    public static String maximizeAndGetScript(Locale locale) {
        return IMPL.maximizeAndGetScript(locale);
    }
}
