package android.support.v4.os;

import android.os.Build;

public final class CancellationSignal {
    private boolean mCancelInProgress;
    private Object mCancellationSignalObj;
    private boolean mIsCanceled;
    private OnCancelListener mOnCancelListener;

    public interface OnCancelListener {
        void onCancel();
    }

    public boolean isCanceled() {
        synchronized (this) {
            try {
                boolean z = this.mIsCanceled;
                return z;
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    public void throwIfCanceled() {
        Throwable th;
        if (isCanceled()) {
            Throwable th2 = th;
            new OperationCanceledException();
            throw th2;
        }
    }

    public void cancel() {
        synchronized (this) {
            try {
                if (this.mIsCanceled) {
                    return;
                }
                this.mIsCanceled = true;
                this.mCancelInProgress = true;
                OnCancelListener onCancelListener = this.mOnCancelListener;
                Object obj = this.mCancellationSignalObj;
                if (onCancelListener != null) {
                    try {
                        onCancelListener.onCancel();
                    } catch (Throwable th) {
                        while (true) {
                            throw th;
                        }
                    }
                }
                if (obj != null) {
                    CancellationSignalCompatJellybean.cancel(obj);
                }
                synchronized (this) {
                    try {
                        this.mCancelInProgress = false;
                        notifyAll();
                    } catch (Throwable th2) {
                        while (true) {
                            throw th2;
                        }
                    }
                }
            } finally {
                while (true) {
                    Throwable th3 = th2;
                }
            }
        }
    }

    /* JADX INFO: finally extract failed */
    public void setOnCancelListener(OnCancelListener onCancelListener) {
        OnCancelListener onCancelListener2 = onCancelListener;
        synchronized (this) {
            try {
                waitForCancelFinishedLocked();
                if (this.mOnCancelListener == onCancelListener2) {
                    return;
                }
                this.mOnCancelListener = onCancelListener2;
                if (!this.mIsCanceled || onCancelListener2 == null) {
                    return;
                }
                onCancelListener2.onCancel();
            } catch (Throwable th) {
                while (true) {
                    throw th;
                }
            }
        }
    }

    public Object getCancellationSignalObject() {
        if (Build.VERSION.SDK_INT < 16) {
            return null;
        }
        synchronized (this) {
            try {
                if (this.mCancellationSignalObj == null) {
                    this.mCancellationSignalObj = CancellationSignalCompatJellybean.create();
                    if (this.mIsCanceled) {
                        CancellationSignalCompatJellybean.cancel(this.mCancellationSignalObj);
                    }
                }
                Object obj = this.mCancellationSignalObj;
                return obj;
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    private void waitForCancelFinishedLocked() {
        while (this.mCancelInProgress) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
    }
}
