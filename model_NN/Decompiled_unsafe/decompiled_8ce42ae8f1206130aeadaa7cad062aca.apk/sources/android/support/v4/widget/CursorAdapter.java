package android.support.v4.widget;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.support.v4.widget.CursorFilter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.FilterQueryProvider;
import android.widget.Filterable;

public abstract class CursorAdapter extends BaseAdapter implements Filterable, CursorFilter.CursorFilterClient {
    @Deprecated
    public static final int FLAG_AUTO_REQUERY = 1;
    public static final int FLAG_REGISTER_CONTENT_OBSERVER = 2;
    protected boolean mAutoRequery;
    protected ChangeObserver mChangeObserver;
    protected Context mContext;
    protected Cursor mCursor;
    protected CursorFilter mCursorFilter;
    protected DataSetObserver mDataSetObserver;
    protected boolean mDataValid;
    protected FilterQueryProvider mFilterQueryProvider;
    protected int mRowIDColumn;

    public abstract void bindView(View view, Context context, Cursor cursor);

    public abstract View newView(Context context, Cursor cursor, ViewGroup viewGroup);

    @Deprecated
    public CursorAdapter(Context context, Cursor cursor) {
        init(context, cursor, 1);
    }

    public CursorAdapter(Context context, Cursor cursor, boolean z) {
        init(context, cursor, z ? 1 : 2);
    }

    public CursorAdapter(Context context, Cursor cursor, int i) {
        init(context, cursor, i);
    }

    /* access modifiers changed from: protected */
    @Deprecated
    public void init(Context context, Cursor cursor, boolean z) {
        init(context, cursor, z ? 1 : 2);
    }

    /* access modifiers changed from: package-private */
    public void init(Context context, Cursor cursor, int i) {
        ChangeObserver changeObserver;
        DataSetObserver dataSetObserver;
        Context context2 = context;
        Cursor cursor2 = cursor;
        int i2 = i;
        if ((i2 & 1) == 1) {
            i2 |= 2;
            this.mAutoRequery = true;
        } else {
            this.mAutoRequery = false;
        }
        boolean z = cursor2 != null;
        this.mCursor = cursor2;
        this.mDataValid = z;
        this.mContext = context2;
        this.mRowIDColumn = z ? cursor2.getColumnIndexOrThrow("_id") : -1;
        if ((i2 & 2) == 2) {
            new ChangeObserver();
            this.mChangeObserver = changeObserver;
            new MyDataSetObserver();
            this.mDataSetObserver = dataSetObserver;
        } else {
            this.mChangeObserver = null;
            this.mDataSetObserver = null;
        }
        if (z) {
            if (this.mChangeObserver != null) {
                cursor2.registerContentObserver(this.mChangeObserver);
            }
            if (this.mDataSetObserver != null) {
                cursor2.registerDataSetObserver(this.mDataSetObserver);
            }
        }
    }

    public Cursor getCursor() {
        return this.mCursor;
    }

    public int getCount() {
        if (!this.mDataValid || this.mCursor == null) {
            return 0;
        }
        return this.mCursor.getCount();
    }

    public Object getItem(int i) {
        int i2 = i;
        if (!this.mDataValid || this.mCursor == null) {
            return null;
        }
        boolean moveToPosition = this.mCursor.moveToPosition(i2);
        return this.mCursor;
    }

    public long getItemId(int i) {
        int i2 = i;
        if (!this.mDataValid || this.mCursor == null) {
            return 0;
        }
        if (this.mCursor.moveToPosition(i2)) {
            return this.mCursor.getLong(this.mRowIDColumn);
        }
        return 0;
    }

    public boolean hasStableIds() {
        return true;
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        Throwable th;
        StringBuilder sb;
        Throwable th2;
        int i2 = i;
        View view3 = view;
        ViewGroup viewGroup2 = viewGroup;
        if (!this.mDataValid) {
            Throwable th3 = th2;
            new IllegalStateException("this should only be called when the cursor is valid");
            throw th3;
        } else if (!this.mCursor.moveToPosition(i2)) {
            Throwable th4 = th;
            new StringBuilder();
            new IllegalStateException(sb.append("couldn't move cursor to position ").append(i2).toString());
            throw th4;
        } else {
            if (view3 == null) {
                view2 = newView(this.mContext, this.mCursor, viewGroup2);
            } else {
                view2 = view3;
            }
            bindView(view2, this.mContext, this.mCursor);
            return view2;
        }
    }

    public View getDropDownView(int i, View view, ViewGroup viewGroup) {
        View view2;
        int i2 = i;
        View view3 = view;
        ViewGroup viewGroup2 = viewGroup;
        if (!this.mDataValid) {
            return null;
        }
        boolean moveToPosition = this.mCursor.moveToPosition(i2);
        if (view3 == null) {
            view2 = newDropDownView(this.mContext, this.mCursor, viewGroup2);
        } else {
            view2 = view3;
        }
        bindView(view2, this.mContext, this.mCursor);
        return view2;
    }

    public View newDropDownView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return newView(context, cursor, viewGroup);
    }

    public void changeCursor(Cursor cursor) {
        Cursor swapCursor = swapCursor(cursor);
        if (swapCursor != null) {
            swapCursor.close();
        }
    }

    public Cursor swapCursor(Cursor cursor) {
        Cursor cursor2 = cursor;
        if (cursor2 == this.mCursor) {
            return null;
        }
        Cursor cursor3 = this.mCursor;
        if (cursor3 != null) {
            if (this.mChangeObserver != null) {
                cursor3.unregisterContentObserver(this.mChangeObserver);
            }
            if (this.mDataSetObserver != null) {
                cursor3.unregisterDataSetObserver(this.mDataSetObserver);
            }
        }
        this.mCursor = cursor2;
        if (cursor2 != null) {
            if (this.mChangeObserver != null) {
                cursor2.registerContentObserver(this.mChangeObserver);
            }
            if (this.mDataSetObserver != null) {
                cursor2.registerDataSetObserver(this.mDataSetObserver);
            }
            this.mRowIDColumn = cursor2.getColumnIndexOrThrow("_id");
            this.mDataValid = true;
            notifyDataSetChanged();
        } else {
            this.mRowIDColumn = -1;
            this.mDataValid = false;
            notifyDataSetInvalidated();
        }
        return cursor3;
    }

    public CharSequence convertToString(Cursor cursor) {
        Cursor cursor2 = cursor;
        return cursor2 == null ? "" : cursor2.toString();
    }

    public Cursor runQueryOnBackgroundThread(CharSequence charSequence) {
        CharSequence charSequence2 = charSequence;
        if (this.mFilterQueryProvider != null) {
            return this.mFilterQueryProvider.runQuery(charSequence2);
        }
        return this.mCursor;
    }

    public Filter getFilter() {
        CursorFilter cursorFilter;
        if (this.mCursorFilter == null) {
            new CursorFilter(this);
            this.mCursorFilter = cursorFilter;
        }
        return this.mCursorFilter;
    }

    public FilterQueryProvider getFilterQueryProvider() {
        return this.mFilterQueryProvider;
    }

    public void setFilterQueryProvider(FilterQueryProvider filterQueryProvider) {
        this.mFilterQueryProvider = filterQueryProvider;
    }

    /* access modifiers changed from: protected */
    public void onContentChanged() {
        if (this.mAutoRequery && this.mCursor != null && !this.mCursor.isClosed()) {
            this.mDataValid = this.mCursor.requery();
        }
    }

    private class ChangeObserver extends ContentObserver {
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public ChangeObserver() {
            /*
                r6 = this;
                r0 = r6
                r1 = r7
                r2 = r0
                r3 = r1
                android.support.v4.widget.CursorAdapter.this = r3
                r2 = r0
                android.os.Handler r3 = new android.os.Handler
                r5 = r3
                r3 = r5
                r4 = r5
                r4.<init>()
                r2.<init>(r3)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: android.support.v4.widget.CursorAdapter.ChangeObserver.<init>(android.support.v4.widget.CursorAdapter):void");
        }

        public boolean deliverSelfNotifications() {
            return true;
        }

        public void onChange(boolean z) {
            CursorAdapter.this.onContentChanged();
        }
    }

    private class MyDataSetObserver extends DataSetObserver {
        private MyDataSetObserver() {
        }

        public void onChanged() {
            CursorAdapter.this.mDataValid = true;
            CursorAdapter.this.notifyDataSetChanged();
        }

        public void onInvalidated() {
            CursorAdapter.this.mDataValid = false;
            CursorAdapter.this.notifyDataSetInvalidated();
        }
    }
}
