package android.support.v4.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RadialGradient;
import android.graphics.Shader;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.Shape;
import android.os.Build;
import android.support.v4.view.ViewCompat;
import android.view.animation.Animation;
import android.widget.ImageView;

class CircleImageView extends ImageView {
    private static final int FILL_SHADOW_COLOR = 1023410176;
    private static final int KEY_SHADOW_COLOR = 503316480;
    private static final int SHADOW_ELEVATION = 4;
    private static final float SHADOW_RADIUS = 3.5f;
    private static final float X_OFFSET = 0.0f;
    private static final float Y_OFFSET = 1.75f;
    private Animation.AnimationListener mListener;
    /* access modifiers changed from: private */
    public int mShadowRadius;

    static /* synthetic */ int access$002(CircleImageView circleImageView, int i) {
        int i2 = i;
        int i3 = i2;
        circleImageView.mShadowRadius = i3;
        return i2;
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CircleImageView(Context context, int i, float f) {
        super(context);
        Shape shape;
        ShapeDrawable shapeDrawable;
        ShapeDrawable shapeDrawable2;
        ShapeDrawable shapeDrawable3;
        Shape shape2;
        int i2 = i;
        float f2 = getContext().getResources().getDisplayMetrics().density;
        int i3 = (int) (f * f2 * 2.0f);
        int i4 = (int) (f2 * Y_OFFSET);
        int i5 = (int) (f2 * 0.0f);
        this.mShadowRadius = (int) (f2 * SHADOW_RADIUS);
        if (elevationSupported()) {
            new OvalShape();
            new ShapeDrawable(shape2);
            shapeDrawable2 = shapeDrawable3;
            ViewCompat.setElevation(this, 4.0f * f2);
        } else {
            new OvalShadow(this, this.mShadowRadius, i3);
            new ShapeDrawable(shape);
            shapeDrawable2 = shapeDrawable;
            ViewCompat.setLayerType(this, 1, shapeDrawable2.getPaint());
            shapeDrawable2.getPaint().setShadowLayer((float) this.mShadowRadius, (float) i5, (float) i4, (int) KEY_SHADOW_COLOR);
            int i6 = this.mShadowRadius;
            setPadding(i6, i6, i6, i6);
        }
        shapeDrawable2.getPaint().setColor(i2);
        setBackgroundDrawable(shapeDrawable2);
    }

    private boolean elevationSupported() {
        return Build.VERSION.SDK_INT >= 21;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (!elevationSupported()) {
            setMeasuredDimension(getMeasuredWidth() + (this.mShadowRadius * 2), getMeasuredHeight() + (this.mShadowRadius * 2));
        }
    }

    public void setAnimationListener(Animation.AnimationListener animationListener) {
        this.mListener = animationListener;
    }

    public void onAnimationStart() {
        super.onAnimationStart();
        if (this.mListener != null) {
            this.mListener.onAnimationStart(getAnimation());
        }
    }

    public void onAnimationEnd() {
        super.onAnimationEnd();
        if (this.mListener != null) {
            this.mListener.onAnimationEnd(getAnimation());
        }
    }

    public void setBackgroundColorRes(int i) {
        setBackgroundColor(getContext().getResources().getColor(i));
    }

    public void setBackgroundColor(int i) {
        int i2 = i;
        if (getBackground() instanceof ShapeDrawable) {
            ((ShapeDrawable) getBackground()).getPaint().setColor(i2);
        }
    }

    private class OvalShadow extends OvalShape {
        private int mCircleDiameter;
        private RadialGradient mRadialGradient;
        private Paint mShadowPaint;
        final /* synthetic */ CircleImageView this$0;

        public OvalShadow(CircleImageView circleImageView, int i, int i2) {
            Paint paint;
            RadialGradient radialGradient;
            CircleImageView circleImageView2 = circleImageView;
            this.this$0 = circleImageView2;
            new Paint();
            this.mShadowPaint = paint;
            int access$002 = CircleImageView.access$002(circleImageView2, i);
            this.mCircleDiameter = i2;
            new RadialGradient((float) (this.mCircleDiameter / 2), (float) (this.mCircleDiameter / 2), (float) circleImageView2.mShadowRadius, new int[]{CircleImageView.FILL_SHADOW_COLOR, 0}, (float[]) null, Shader.TileMode.CLAMP);
            this.mRadialGradient = radialGradient;
            Shader shader = this.mShadowPaint.setShader(this.mRadialGradient);
        }

        public void draw(Canvas canvas, Paint paint) {
            Canvas canvas2 = canvas;
            int width = this.this$0.getWidth();
            int height = this.this$0.getHeight();
            canvas2.drawCircle((float) (width / 2), (float) (height / 2), (float) ((this.mCircleDiameter / 2) + this.this$0.mShadowRadius), this.mShadowPaint);
            canvas2.drawCircle((float) (width / 2), (float) (height / 2), (float) (this.mCircleDiameter / 2), paint);
        }
    }
}
