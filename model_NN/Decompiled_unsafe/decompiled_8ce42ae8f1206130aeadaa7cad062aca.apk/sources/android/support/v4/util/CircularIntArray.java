package android.support.v4.util;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    */
public final class CircularIntArray {
    private int mCapacityBitmask;
    private int[] mElements;
    private int mHead;
    private int mTail;

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: java.lang.Integer.bitCount(int):int in method: android.support.v4.util.CircularIntArray.<init>(int):void, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:58)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: java.lang.Integer.bitCount(int):int
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:528)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 5 more
        */
    public CircularIntArray(int r1) {
        /*
            r7 = this;
            r0 = r7
            r1 = r8
            r3 = r0
            r3.<init>()
            r3 = r1
            if (r3 > 0) goto L_0x0014
            java.lang.IllegalArgumentException r3 = new java.lang.IllegalArgumentException
            r6 = r3
            r3 = r6
            r4 = r6
            java.lang.String r5 = "capacity must be positive"
            r4.<init>(r5)
            throw r3
        L_0x0014:
            r3 = r1
            r2 = r3
            r3 = r1
            int r3 = java.lang.Integer.bitCount(r3)
            r4 = 1
            if (r3 == r4) goto L_0x0029
            r3 = 1
            r4 = r1
            int r4 = java.lang.Integer.highestOneBit(r4)
            r5 = 1
            int r4 = r4 + 1
            int r3 = r3 << r4
            r2 = r3
        L_0x0029:
            r3 = r0
            r4 = r2
            r5 = 1
            int r4 = r4 + -1
            r3.mCapacityBitmask = r4
            r3 = r0
            r4 = r2
            int[] r4 = new int[r4]
            r3.mElements = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.util.CircularIntArray.<init>(int):void");
    }

    private void doubleCapacity() {
        Throwable th;
        int length = this.mElements.length;
        int i = length - this.mHead;
        int i2 = length << 1;
        if (i2 < 0) {
            Throwable th2 = th;
            new RuntimeException("Max array capacity exceeded");
            throw th2;
        }
        int[] iArr = new int[i2];
        System.arraycopy(this.mElements, this.mHead, iArr, 0, i);
        System.arraycopy(this.mElements, 0, iArr, i, this.mHead);
        this.mElements = iArr;
        this.mHead = 0;
        this.mTail = length;
        this.mCapacityBitmask = i2 - 1;
    }

    public CircularIntArray() {
        this(8);
    }

    public void addFirst(int i) {
        this.mHead = (this.mHead - 1) & this.mCapacityBitmask;
        this.mElements[this.mHead] = i;
        if (this.mHead == this.mTail) {
            doubleCapacity();
        }
    }

    public void addLast(int i) {
        this.mElements[this.mTail] = i;
        this.mTail = (this.mTail + 1) & this.mCapacityBitmask;
        if (this.mTail == this.mHead) {
            doubleCapacity();
        }
    }

    public int popFirst() {
        Throwable th;
        if (this.mHead == this.mTail) {
            Throwable th2 = th;
            new ArrayIndexOutOfBoundsException();
            throw th2;
        }
        this.mHead = (this.mHead + 1) & this.mCapacityBitmask;
        return this.mElements[this.mHead];
    }

    public int popLast() {
        Throwable th;
        if (this.mHead == this.mTail) {
            Throwable th2 = th;
            new ArrayIndexOutOfBoundsException();
            throw th2;
        }
        int i = (this.mTail - 1) & this.mCapacityBitmask;
        this.mTail = i;
        return this.mElements[i];
    }

    public void clear() {
        this.mTail = this.mHead;
    }

    public void removeFromStart(int i) {
        Throwable th;
        int i2 = i;
        if (i2 > 0) {
            if (i2 > size()) {
                Throwable th2 = th;
                new ArrayIndexOutOfBoundsException();
                throw th2;
            }
            this.mHead = (this.mHead + i2) & this.mCapacityBitmask;
        }
    }

    public void removeFromEnd(int i) {
        Throwable th;
        int i2 = i;
        if (i2 > 0) {
            if (i2 > size()) {
                Throwable th2 = th;
                new ArrayIndexOutOfBoundsException();
                throw th2;
            }
            this.mTail = (this.mTail - i2) & this.mCapacityBitmask;
        }
    }

    public int getFirst() {
        Throwable th;
        if (this.mHead != this.mTail) {
            return this.mElements[this.mHead];
        }
        Throwable th2 = th;
        new ArrayIndexOutOfBoundsException();
        throw th2;
    }

    public int getLast() {
        Throwable th;
        if (this.mHead != this.mTail) {
            return this.mElements[(this.mTail - 1) & this.mCapacityBitmask];
        }
        Throwable th2 = th;
        new ArrayIndexOutOfBoundsException();
        throw th2;
    }

    public int get(int i) {
        Throwable th;
        int i2 = i;
        if (i2 >= 0 && i2 < size()) {
            return this.mElements[(this.mHead + i2) & this.mCapacityBitmask];
        }
        Throwable th2 = th;
        new ArrayIndexOutOfBoundsException();
        throw th2;
    }

    public int size() {
        return (this.mTail - this.mHead) & this.mCapacityBitmask;
    }

    public boolean isEmpty() {
        return this.mHead == this.mTail;
    }
}
