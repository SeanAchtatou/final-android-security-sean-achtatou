package android.support.v4.app;

import android.os.Build;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransitionCompat21;
import android.support.v4.util.ArrayMap;
import android.support.v4.util.LogWriter;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

final class BackStackRecord extends FragmentTransaction implements FragmentManager.BackStackEntry, Runnable {
    static final int OP_ADD = 1;
    static final int OP_ATTACH = 7;
    static final int OP_DETACH = 6;
    static final int OP_HIDE = 4;
    static final int OP_NULL = 0;
    static final int OP_REMOVE = 3;
    static final int OP_REPLACE = 2;
    static final int OP_SHOW = 5;
    static final boolean SUPPORTS_TRANSITIONS = (Build.VERSION.SDK_INT >= 21);
    static final String TAG = "FragmentManager";
    boolean mAddToBackStack;
    boolean mAllowAddToBackStack = true;
    int mBreadCrumbShortTitleRes;
    CharSequence mBreadCrumbShortTitleText;
    int mBreadCrumbTitleRes;
    CharSequence mBreadCrumbTitleText;
    boolean mCommitted;
    int mEnterAnim;
    int mExitAnim;
    Op mHead;
    int mIndex = -1;
    final FragmentManagerImpl mManager;
    String mName;
    int mNumOp;
    int mPopEnterAnim;
    int mPopExitAnim;
    ArrayList<String> mSharedElementSourceNames;
    ArrayList<String> mSharedElementTargetNames;
    Op mTail;
    int mTransition;
    int mTransitionStyle;

    static final class Op {
        int cmd;
        int enterAnim;
        int exitAnim;
        Fragment fragment;
        Op next;
        int popEnterAnim;
        int popExitAnim;
        Op prev;
        ArrayList<Fragment> removed;

        Op() {
        }
    }

    public String toString() {
        StringBuilder sb;
        new StringBuilder(128);
        StringBuilder sb2 = sb;
        StringBuilder append = sb2.append("BackStackEntry{");
        StringBuilder append2 = sb2.append(Integer.toHexString(System.identityHashCode(this)));
        if (this.mIndex >= 0) {
            StringBuilder append3 = sb2.append(" #");
            StringBuilder append4 = sb2.append(this.mIndex);
        }
        if (this.mName != null) {
            StringBuilder append5 = sb2.append(" ");
            StringBuilder append6 = sb2.append(this.mName);
        }
        StringBuilder append7 = sb2.append("}");
        return sb2.toString();
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        dump(str, printWriter, true);
    }

    public void dump(String str, PrintWriter printWriter, boolean z) {
        StringBuilder sb;
        String str2;
        StringBuilder sb2;
        String str3 = str;
        PrintWriter printWriter2 = printWriter;
        boolean z2 = z;
        if (z2) {
            printWriter2.print(str3);
            printWriter2.print("mName=");
            printWriter2.print(this.mName);
            printWriter2.print(" mIndex=");
            printWriter2.print(this.mIndex);
            printWriter2.print(" mCommitted=");
            printWriter2.println(this.mCommitted);
            if (this.mTransition != 0) {
                printWriter2.print(str3);
                printWriter2.print("mTransition=#");
                printWriter2.print(Integer.toHexString(this.mTransition));
                printWriter2.print(" mTransitionStyle=#");
                printWriter2.println(Integer.toHexString(this.mTransitionStyle));
            }
            if (!(this.mEnterAnim == 0 && this.mExitAnim == 0)) {
                printWriter2.print(str3);
                printWriter2.print("mEnterAnim=#");
                printWriter2.print(Integer.toHexString(this.mEnterAnim));
                printWriter2.print(" mExitAnim=#");
                printWriter2.println(Integer.toHexString(this.mExitAnim));
            }
            if (!(this.mPopEnterAnim == 0 && this.mPopExitAnim == 0)) {
                printWriter2.print(str3);
                printWriter2.print("mPopEnterAnim=#");
                printWriter2.print(Integer.toHexString(this.mPopEnterAnim));
                printWriter2.print(" mPopExitAnim=#");
                printWriter2.println(Integer.toHexString(this.mPopExitAnim));
            }
            if (!(this.mBreadCrumbTitleRes == 0 && this.mBreadCrumbTitleText == null)) {
                printWriter2.print(str3);
                printWriter2.print("mBreadCrumbTitleRes=#");
                printWriter2.print(Integer.toHexString(this.mBreadCrumbTitleRes));
                printWriter2.print(" mBreadCrumbTitleText=");
                printWriter2.println(this.mBreadCrumbTitleText);
            }
            if (!(this.mBreadCrumbShortTitleRes == 0 && this.mBreadCrumbShortTitleText == null)) {
                printWriter2.print(str3);
                printWriter2.print("mBreadCrumbShortTitleRes=#");
                printWriter2.print(Integer.toHexString(this.mBreadCrumbShortTitleRes));
                printWriter2.print(" mBreadCrumbShortTitleText=");
                printWriter2.println(this.mBreadCrumbShortTitleText);
            }
        }
        if (this.mHead != null) {
            printWriter2.print(str3);
            printWriter2.println("Operations:");
            new StringBuilder();
            String sb3 = sb.append(str3).append("    ").toString();
            Op op = this.mHead;
            int i = 0;
            while (op != null) {
                switch (op.cmd) {
                    case 0:
                        str2 = "NULL";
                        break;
                    case 1:
                        str2 = "ADD";
                        break;
                    case 2:
                        str2 = "REPLACE";
                        break;
                    case 3:
                        str2 = "REMOVE";
                        break;
                    case 4:
                        str2 = "HIDE";
                        break;
                    case 5:
                        str2 = "SHOW";
                        break;
                    case 6:
                        str2 = "DETACH";
                        break;
                    case 7:
                        str2 = "ATTACH";
                        break;
                    default:
                        new StringBuilder();
                        str2 = sb2.append("cmd=").append(op.cmd).toString();
                        break;
                }
                printWriter2.print(str3);
                printWriter2.print("  Op #");
                printWriter2.print(i);
                printWriter2.print(": ");
                printWriter2.print(str2);
                printWriter2.print(" ");
                printWriter2.println(op.fragment);
                if (z2) {
                    if (!(op.enterAnim == 0 && op.exitAnim == 0)) {
                        printWriter2.print(str3);
                        printWriter2.print("enterAnim=#");
                        printWriter2.print(Integer.toHexString(op.enterAnim));
                        printWriter2.print(" exitAnim=#");
                        printWriter2.println(Integer.toHexString(op.exitAnim));
                    }
                    if (!(op.popEnterAnim == 0 && op.popExitAnim == 0)) {
                        printWriter2.print(str3);
                        printWriter2.print("popEnterAnim=#");
                        printWriter2.print(Integer.toHexString(op.popEnterAnim));
                        printWriter2.print(" popExitAnim=#");
                        printWriter2.println(Integer.toHexString(op.popExitAnim));
                    }
                }
                if (op.removed != null && op.removed.size() > 0) {
                    for (int i2 = 0; i2 < op.removed.size(); i2++) {
                        printWriter2.print(sb3);
                        if (op.removed.size() == 1) {
                            printWriter2.print("Removed: ");
                        } else {
                            if (i2 == 0) {
                                printWriter2.println("Removed:");
                            }
                            printWriter2.print(sb3);
                            printWriter2.print("  #");
                            printWriter2.print(i2);
                            printWriter2.print(": ");
                        }
                        printWriter2.println(op.removed.get(i2));
                    }
                }
                op = op.next;
                i++;
            }
        }
    }

    public BackStackRecord(FragmentManagerImpl fragmentManagerImpl) {
        this.mManager = fragmentManagerImpl;
    }

    public int getId() {
        return this.mIndex;
    }

    public int getBreadCrumbTitleRes() {
        return this.mBreadCrumbTitleRes;
    }

    public int getBreadCrumbShortTitleRes() {
        return this.mBreadCrumbShortTitleRes;
    }

    public CharSequence getBreadCrumbTitle() {
        if (this.mBreadCrumbTitleRes != 0) {
            return this.mManager.mHost.getContext().getText(this.mBreadCrumbTitleRes);
        }
        return this.mBreadCrumbTitleText;
    }

    public CharSequence getBreadCrumbShortTitle() {
        if (this.mBreadCrumbShortTitleRes != 0) {
            return this.mManager.mHost.getContext().getText(this.mBreadCrumbShortTitleRes);
        }
        return this.mBreadCrumbShortTitleText;
    }

    /* access modifiers changed from: package-private */
    public void addOp(Op op) {
        Op op2 = op;
        if (this.mHead == null) {
            Op op3 = op2;
            this.mTail = op3;
            this.mHead = op3;
        } else {
            op2.prev = this.mTail;
            this.mTail.next = op2;
            this.mTail = op2;
        }
        op2.enterAnim = this.mEnterAnim;
        op2.exitAnim = this.mExitAnim;
        op2.popEnterAnim = this.mPopEnterAnim;
        op2.popExitAnim = this.mPopExitAnim;
        this.mNumOp = this.mNumOp + 1;
    }

    public FragmentTransaction add(Fragment fragment, String str) {
        doAddOp(0, fragment, str, 1);
        return this;
    }

    public FragmentTransaction add(int i, Fragment fragment) {
        doAddOp(i, fragment, null, 1);
        return this;
    }

    public FragmentTransaction add(int i, Fragment fragment, String str) {
        doAddOp(i, fragment, str, 1);
        return this;
    }

    private void doAddOp(int i, Fragment fragment, String str, int i2) {
        Op op;
        Throwable th;
        StringBuilder sb;
        Throwable th2;
        StringBuilder sb2;
        int i3 = i;
        Fragment fragment2 = fragment;
        String str2 = str;
        int i4 = i2;
        fragment2.mFragmentManager = this.mManager;
        if (str2 != null) {
            if (fragment2.mTag == null || str2.equals(fragment2.mTag)) {
                fragment2.mTag = str2;
            } else {
                Throwable th3 = th2;
                new StringBuilder();
                new IllegalStateException(sb2.append("Can't change tag of fragment ").append(fragment2).append(": was ").append(fragment2.mTag).append(" now ").append(str2).toString());
                throw th3;
            }
        }
        if (i3 != 0) {
            if (fragment2.mFragmentId == 0 || fragment2.mFragmentId == i3) {
                int i5 = i3;
                int i6 = i5;
                fragment2.mFragmentId = i6;
                fragment2.mContainerId = i5;
            } else {
                Throwable th4 = th;
                new StringBuilder();
                new IllegalStateException(sb.append("Can't change container ID of fragment ").append(fragment2).append(": was ").append(fragment2.mFragmentId).append(" now ").append(i3).toString());
                throw th4;
            }
        }
        new Op();
        Op op2 = op;
        op2.cmd = i4;
        op2.fragment = fragment2;
        addOp(op2);
    }

    public FragmentTransaction replace(int i, Fragment fragment) {
        return replace(i, fragment, null);
    }

    public FragmentTransaction replace(int i, Fragment fragment, String str) {
        Throwable th;
        int i2 = i;
        Fragment fragment2 = fragment;
        String str2 = str;
        if (i2 == 0) {
            Throwable th2 = th;
            new IllegalArgumentException("Must use non-zero containerViewId");
            throw th2;
        }
        doAddOp(i2, fragment2, str2, 2);
        return this;
    }

    public FragmentTransaction remove(Fragment fragment) {
        Op op;
        new Op();
        Op op2 = op;
        op2.cmd = 3;
        op2.fragment = fragment;
        addOp(op2);
        return this;
    }

    public FragmentTransaction hide(Fragment fragment) {
        Op op;
        new Op();
        Op op2 = op;
        op2.cmd = 4;
        op2.fragment = fragment;
        addOp(op2);
        return this;
    }

    public FragmentTransaction show(Fragment fragment) {
        Op op;
        new Op();
        Op op2 = op;
        op2.cmd = 5;
        op2.fragment = fragment;
        addOp(op2);
        return this;
    }

    public FragmentTransaction detach(Fragment fragment) {
        Op op;
        new Op();
        Op op2 = op;
        op2.cmd = 6;
        op2.fragment = fragment;
        addOp(op2);
        return this;
    }

    public FragmentTransaction attach(Fragment fragment) {
        Op op;
        new Op();
        Op op2 = op;
        op2.cmd = 7;
        op2.fragment = fragment;
        addOp(op2);
        return this;
    }

    public FragmentTransaction setCustomAnimations(int i, int i2) {
        return setCustomAnimations(i, i2, 0, 0);
    }

    public FragmentTransaction setCustomAnimations(int i, int i2, int i3, int i4) {
        this.mEnterAnim = i;
        this.mExitAnim = i2;
        this.mPopEnterAnim = i3;
        this.mPopExitAnim = i4;
        return this;
    }

    public FragmentTransaction setTransition(int i) {
        this.mTransition = i;
        return this;
    }

    public FragmentTransaction addSharedElement(View view, String str) {
        ArrayList<String> arrayList;
        ArrayList<String> arrayList2;
        Throwable th;
        View view2 = view;
        String str2 = str;
        if (SUPPORTS_TRANSITIONS) {
            String transitionName = FragmentTransitionCompat21.getTransitionName(view2);
            if (transitionName == null) {
                Throwable th2 = th;
                new IllegalArgumentException("Unique transitionNames are required for all sharedElements");
                throw th2;
            }
            if (this.mSharedElementSourceNames == null) {
                new ArrayList<>();
                this.mSharedElementSourceNames = arrayList;
                new ArrayList<>();
                this.mSharedElementTargetNames = arrayList2;
            }
            boolean add = this.mSharedElementSourceNames.add(transitionName);
            boolean add2 = this.mSharedElementTargetNames.add(str2);
        }
        return this;
    }

    public FragmentTransaction setTransitionStyle(int i) {
        this.mTransitionStyle = i;
        return this;
    }

    public FragmentTransaction addToBackStack(String str) {
        Throwable th;
        String str2 = str;
        if (!this.mAllowAddToBackStack) {
            Throwable th2 = th;
            new IllegalStateException("This FragmentTransaction is not allowed to be added to the back stack.");
            throw th2;
        }
        this.mAddToBackStack = true;
        this.mName = str2;
        return this;
    }

    public boolean isAddToBackStackAllowed() {
        return this.mAllowAddToBackStack;
    }

    public FragmentTransaction disallowAddToBackStack() {
        Throwable th;
        if (this.mAddToBackStack) {
            Throwable th2 = th;
            new IllegalStateException("This transaction is already being added to the back stack");
            throw th2;
        }
        this.mAllowAddToBackStack = false;
        return this;
    }

    public FragmentTransaction setBreadCrumbTitle(int i) {
        this.mBreadCrumbTitleRes = i;
        this.mBreadCrumbTitleText = null;
        return this;
    }

    public FragmentTransaction setBreadCrumbTitle(CharSequence charSequence) {
        this.mBreadCrumbTitleRes = 0;
        this.mBreadCrumbTitleText = charSequence;
        return this;
    }

    public FragmentTransaction setBreadCrumbShortTitle(int i) {
        this.mBreadCrumbShortTitleRes = i;
        this.mBreadCrumbShortTitleText = null;
        return this;
    }

    public FragmentTransaction setBreadCrumbShortTitle(CharSequence charSequence) {
        this.mBreadCrumbShortTitleRes = 0;
        this.mBreadCrumbShortTitleText = charSequence;
        return this;
    }

    /* access modifiers changed from: package-private */
    public void bumpBackStackNesting(int i) {
        StringBuilder sb;
        StringBuilder sb2;
        StringBuilder sb3;
        int i2 = i;
        if (this.mAddToBackStack) {
            if (FragmentManagerImpl.DEBUG) {
                new StringBuilder();
                int v = Log.v(TAG, sb3.append("Bump nesting in ").append(this).append(" by ").append(i2).toString());
            }
            Op op = this.mHead;
            while (true) {
                Op op2 = op;
                if (op2 != null) {
                    if (op2.fragment != null) {
                        op2.fragment.mBackStackNesting += i2;
                        if (FragmentManagerImpl.DEBUG) {
                            new StringBuilder();
                            int v2 = Log.v(TAG, sb2.append("Bump nesting of ").append(op2.fragment).append(" to ").append(op2.fragment.mBackStackNesting).toString());
                        }
                    }
                    if (op2.removed != null) {
                        for (int size = op2.removed.size() - 1; size >= 0; size--) {
                            Fragment fragment = op2.removed.get(size);
                            fragment.mBackStackNesting += i2;
                            if (FragmentManagerImpl.DEBUG) {
                                new StringBuilder();
                                int v3 = Log.v(TAG, sb.append("Bump nesting of ").append(fragment).append(" to ").append(fragment.mBackStackNesting).toString());
                            }
                        }
                    }
                    op = op2.next;
                } else {
                    return;
                }
            }
        }
    }

    public int commit() {
        return commitInternal(false);
    }

    public int commitAllowingStateLoss() {
        return commitInternal(true);
    }

    /* access modifiers changed from: package-private */
    public int commitInternal(boolean z) {
        StringBuilder sb;
        Writer writer;
        PrintWriter printWriter;
        Throwable th;
        boolean z2 = z;
        if (this.mCommitted) {
            Throwable th2 = th;
            new IllegalStateException("commit already called");
            throw th2;
        }
        if (FragmentManagerImpl.DEBUG) {
            new StringBuilder();
            int v = Log.v(TAG, sb.append("Commit: ").append(this).toString());
            new LogWriter(TAG);
            new PrintWriter(writer);
            dump("  ", null, printWriter, null);
        }
        this.mCommitted = true;
        if (this.mAddToBackStack) {
            this.mIndex = this.mManager.allocBackStackIndex(this);
        } else {
            this.mIndex = -1;
        }
        this.mManager.enqueueAction(this, z2);
        return this.mIndex;
    }

    public void run() {
        Throwable th;
        StringBuilder sb;
        StringBuilder sb2;
        ArrayList<Fragment> arrayList;
        StringBuilder sb3;
        SparseArray sparseArray;
        SparseArray sparseArray2;
        Throwable th2;
        StringBuilder sb4;
        if (FragmentManagerImpl.DEBUG) {
            new StringBuilder();
            int v = Log.v(TAG, sb4.append("Run: ").append(this).toString());
        }
        if (!this.mAddToBackStack || this.mIndex >= 0) {
            bumpBackStackNesting(1);
            TransitionState transitionState = null;
            if (SUPPORTS_TRANSITIONS) {
                new SparseArray();
                SparseArray sparseArray3 = sparseArray;
                new SparseArray();
                SparseArray sparseArray4 = sparseArray2;
                calculateFragments(sparseArray3, sparseArray4);
                transitionState = beginTransition(sparseArray3, sparseArray4, false);
            }
            int i = transitionState != null ? 0 : this.mTransitionStyle;
            int i2 = transitionState != null ? 0 : this.mTransition;
            Op op = this.mHead;
            while (true) {
                Op op2 = op;
                if (op2 != null) {
                    int i3 = transitionState != null ? 0 : op2.enterAnim;
                    int i4 = transitionState != null ? 0 : op2.exitAnim;
                    switch (op2.cmd) {
                        case 1:
                            Fragment fragment = op2.fragment;
                            fragment.mNextAnim = i3;
                            this.mManager.addFragment(fragment, false);
                            break;
                        case 2:
                            Fragment fragment2 = op2.fragment;
                            int i5 = fragment2.mContainerId;
                            if (this.mManager.mAdded != null) {
                                for (int i6 = 0; i6 < this.mManager.mAdded.size(); i6++) {
                                    Fragment fragment3 = this.mManager.mAdded.get(i6);
                                    if (FragmentManagerImpl.DEBUG) {
                                        new StringBuilder();
                                        int v2 = Log.v(TAG, sb3.append("OP_REPLACE: adding=").append(fragment2).append(" old=").append(fragment3).toString());
                                    }
                                    if (fragment3.mContainerId == i5) {
                                        if (fragment3 == fragment2) {
                                            fragment2 = null;
                                            op2.fragment = null;
                                        } else {
                                            if (op2.removed == null) {
                                                new ArrayList<>();
                                                op2.removed = arrayList;
                                            }
                                            boolean add = op2.removed.add(fragment3);
                                            fragment3.mNextAnim = i4;
                                            if (this.mAddToBackStack) {
                                                fragment3.mBackStackNesting++;
                                                if (FragmentManagerImpl.DEBUG) {
                                                    new StringBuilder();
                                                    int v3 = Log.v(TAG, sb2.append("Bump nesting of ").append(fragment3).append(" to ").append(fragment3.mBackStackNesting).toString());
                                                }
                                            }
                                            this.mManager.removeFragment(fragment3, i2, i);
                                        }
                                    }
                                }
                            }
                            if (fragment2 == null) {
                                break;
                            } else {
                                fragment2.mNextAnim = i3;
                                this.mManager.addFragment(fragment2, false);
                                break;
                            }
                        case 3:
                            Fragment fragment4 = op2.fragment;
                            fragment4.mNextAnim = i4;
                            this.mManager.removeFragment(fragment4, i2, i);
                            break;
                        case 4:
                            Fragment fragment5 = op2.fragment;
                            fragment5.mNextAnim = i4;
                            this.mManager.hideFragment(fragment5, i2, i);
                            break;
                        case 5:
                            Fragment fragment6 = op2.fragment;
                            fragment6.mNextAnim = i3;
                            this.mManager.showFragment(fragment6, i2, i);
                            break;
                        case 6:
                            Fragment fragment7 = op2.fragment;
                            fragment7.mNextAnim = i4;
                            this.mManager.detachFragment(fragment7, i2, i);
                            break;
                        case 7:
                            Fragment fragment8 = op2.fragment;
                            fragment8.mNextAnim = i3;
                            this.mManager.attachFragment(fragment8, i2, i);
                            break;
                        default:
                            Throwable th3 = th;
                            new StringBuilder();
                            new IllegalArgumentException(sb.append("Unknown cmd: ").append(op2.cmd).toString());
                            throw th3;
                    }
                    op = op2.next;
                } else {
                    this.mManager.moveToState(this.mManager.mCurState, i2, i, true);
                    if (this.mAddToBackStack) {
                        this.mManager.addBackStackState(this);
                        return;
                    }
                    return;
                }
            }
        } else {
            Throwable th4 = th2;
            new IllegalStateException("addToBackStack() called after commit()");
            throw th4;
        }
    }

    private static void setFirstOut(SparseArray<Fragment> sparseArray, Fragment fragment) {
        int i;
        SparseArray<Fragment> sparseArray2 = sparseArray;
        Fragment fragment2 = fragment;
        if (fragment2 != null && (i = fragment2.mContainerId) != 0 && !fragment2.isHidden() && fragment2.isAdded() && fragment2.getView() != null && sparseArray2.get(i) == null) {
            sparseArray2.put(i, fragment2);
        }
    }

    private void setLastIn(SparseArray<Fragment> sparseArray, Fragment fragment) {
        int i;
        SparseArray<Fragment> sparseArray2 = sparseArray;
        Fragment fragment2 = fragment;
        if (fragment2 != null && (i = fragment2.mContainerId) != 0) {
            sparseArray2.put(i, fragment2);
        }
    }

    private void calculateFragments(SparseArray<Fragment> sparseArray, SparseArray<Fragment> sparseArray2) {
        SparseArray<Fragment> sparseArray3 = sparseArray;
        SparseArray<Fragment> sparseArray4 = sparseArray2;
        if (this.mManager.mContainer.onHasView()) {
            Op op = this.mHead;
            while (true) {
                Op op2 = op;
                if (op2 != null) {
                    switch (op2.cmd) {
                        case 1:
                            setLastIn(sparseArray4, op2.fragment);
                            break;
                        case 2:
                            Fragment fragment = op2.fragment;
                            if (this.mManager.mAdded != null) {
                                for (int i = 0; i < this.mManager.mAdded.size(); i++) {
                                    Fragment fragment2 = this.mManager.mAdded.get(i);
                                    if (fragment == null || fragment2.mContainerId == fragment.mContainerId) {
                                        if (fragment2 == fragment) {
                                            fragment = null;
                                        } else {
                                            setFirstOut(sparseArray3, fragment2);
                                        }
                                    }
                                }
                            }
                            setLastIn(sparseArray4, fragment);
                            break;
                        case 3:
                            setFirstOut(sparseArray3, op2.fragment);
                            break;
                        case 4:
                            setFirstOut(sparseArray3, op2.fragment);
                            break;
                        case 5:
                            setLastIn(sparseArray4, op2.fragment);
                            break;
                        case 6:
                            setFirstOut(sparseArray3, op2.fragment);
                            break;
                        case 7:
                            setLastIn(sparseArray4, op2.fragment);
                            break;
                    }
                    op = op2.next;
                } else {
                    return;
                }
            }
        }
    }

    public void calculateBackFragments(SparseArray<Fragment> sparseArray, SparseArray<Fragment> sparseArray2) {
        SparseArray<Fragment> sparseArray3 = sparseArray;
        SparseArray<Fragment> sparseArray4 = sparseArray2;
        if (this.mManager.mContainer.onHasView()) {
            Op op = this.mHead;
            while (true) {
                Op op2 = op;
                if (op2 != null) {
                    switch (op2.cmd) {
                        case 1:
                            setFirstOut(sparseArray3, op2.fragment);
                            break;
                        case 2:
                            if (op2.removed != null) {
                                for (int size = op2.removed.size() - 1; size >= 0; size--) {
                                    setLastIn(sparseArray4, op2.removed.get(size));
                                }
                            }
                            setFirstOut(sparseArray3, op2.fragment);
                            break;
                        case 3:
                            setLastIn(sparseArray4, op2.fragment);
                            break;
                        case 4:
                            setLastIn(sparseArray4, op2.fragment);
                            break;
                        case 5:
                            setFirstOut(sparseArray3, op2.fragment);
                            break;
                        case 6:
                            setLastIn(sparseArray4, op2.fragment);
                            break;
                        case 7:
                            setFirstOut(sparseArray3, op2.fragment);
                            break;
                    }
                    op = op2.next;
                } else {
                    return;
                }
            }
        }
    }

    public TransitionState popFromBackStack(boolean z, TransitionState transitionState, SparseArray<Fragment> sparseArray, SparseArray<Fragment> sparseArray2) {
        Throwable th;
        StringBuilder sb;
        StringBuilder sb2;
        Writer writer;
        PrintWriter printWriter;
        boolean z2 = z;
        TransitionState transitionState2 = transitionState;
        SparseArray<Fragment> sparseArray3 = sparseArray;
        SparseArray<Fragment> sparseArray4 = sparseArray2;
        if (FragmentManagerImpl.DEBUG) {
            new StringBuilder();
            int v = Log.v(TAG, sb2.append("popFromBackStack: ").append(this).toString());
            new LogWriter(TAG);
            new PrintWriter(writer);
            dump("  ", null, printWriter, null);
        }
        if (SUPPORTS_TRANSITIONS) {
            if (transitionState2 == null) {
                if (!(sparseArray3.size() == 0 && sparseArray4.size() == 0)) {
                    transitionState2 = beginTransition(sparseArray3, sparseArray4, true);
                }
            } else if (!z2) {
                setNameOverrides(transitionState2, this.mSharedElementTargetNames, this.mSharedElementSourceNames);
            }
        }
        bumpBackStackNesting(-1);
        int i = transitionState2 != null ? 0 : this.mTransitionStyle;
        int i2 = transitionState2 != null ? 0 : this.mTransition;
        Op op = this.mTail;
        while (true) {
            Op op2 = op;
            if (op2 != null) {
                int i3 = transitionState2 != null ? 0 : op2.popEnterAnim;
                int i4 = transitionState2 != null ? 0 : op2.popExitAnim;
                switch (op2.cmd) {
                    case 1:
                        Fragment fragment = op2.fragment;
                        fragment.mNextAnim = i4;
                        this.mManager.removeFragment(fragment, FragmentManagerImpl.reverseTransit(i2), i);
                        break;
                    case 2:
                        Fragment fragment2 = op2.fragment;
                        if (fragment2 != null) {
                            fragment2.mNextAnim = i4;
                            this.mManager.removeFragment(fragment2, FragmentManagerImpl.reverseTransit(i2), i);
                        }
                        if (op2.removed == null) {
                            break;
                        } else {
                            for (int i5 = 0; i5 < op2.removed.size(); i5++) {
                                Fragment fragment3 = op2.removed.get(i5);
                                fragment3.mNextAnim = i3;
                                this.mManager.addFragment(fragment3, false);
                            }
                            break;
                        }
                    case 3:
                        Fragment fragment4 = op2.fragment;
                        fragment4.mNextAnim = i3;
                        this.mManager.addFragment(fragment4, false);
                        break;
                    case 4:
                        Fragment fragment5 = op2.fragment;
                        fragment5.mNextAnim = i3;
                        this.mManager.showFragment(fragment5, FragmentManagerImpl.reverseTransit(i2), i);
                        break;
                    case 5:
                        Fragment fragment6 = op2.fragment;
                        fragment6.mNextAnim = i4;
                        this.mManager.hideFragment(fragment6, FragmentManagerImpl.reverseTransit(i2), i);
                        break;
                    case 6:
                        Fragment fragment7 = op2.fragment;
                        fragment7.mNextAnim = i3;
                        this.mManager.attachFragment(fragment7, FragmentManagerImpl.reverseTransit(i2), i);
                        break;
                    case 7:
                        Fragment fragment8 = op2.fragment;
                        fragment8.mNextAnim = i3;
                        this.mManager.detachFragment(fragment8, FragmentManagerImpl.reverseTransit(i2), i);
                        break;
                    default:
                        Throwable th2 = th;
                        new StringBuilder();
                        new IllegalArgumentException(sb.append("Unknown cmd: ").append(op2.cmd).toString());
                        throw th2;
                }
                op = op2.prev;
            } else {
                if (z2) {
                    this.mManager.moveToState(this.mManager.mCurState, FragmentManagerImpl.reverseTransit(i2), i, true);
                    transitionState2 = null;
                }
                if (this.mIndex >= 0) {
                    this.mManager.freeBackStackIndex(this.mIndex);
                    this.mIndex = -1;
                }
                return transitionState2;
            }
        }
    }

    public String getName() {
        return this.mName;
    }

    public int getTransition() {
        return this.mTransition;
    }

    public int getTransitionStyle() {
        return this.mTransitionStyle;
    }

    public boolean isEmpty() {
        return this.mNumOp == 0;
    }

    private TransitionState beginTransition(SparseArray<Fragment> sparseArray, SparseArray<Fragment> sparseArray2, boolean z) {
        TransitionState transitionState;
        View view;
        SparseArray<Fragment> sparseArray3 = sparseArray;
        SparseArray<Fragment> sparseArray4 = sparseArray2;
        boolean z2 = z;
        new TransitionState();
        TransitionState transitionState2 = transitionState;
        new View(this.mManager.mHost.getContext());
        transitionState2.nonExistentView = view;
        boolean z3 = false;
        for (int i = 0; i < sparseArray3.size(); i++) {
            if (configureTransitions(sparseArray3.keyAt(i), transitionState2, z2, sparseArray3, sparseArray4)) {
                z3 = true;
            }
        }
        for (int i2 = 0; i2 < sparseArray4.size(); i2++) {
            int keyAt = sparseArray4.keyAt(i2);
            if (sparseArray3.get(keyAt) == null && configureTransitions(keyAt, transitionState2, z2, sparseArray3, sparseArray4)) {
                z3 = true;
            }
        }
        if (!z3) {
            transitionState2 = null;
        }
        return transitionState2;
    }

    private static Object getEnterTransition(Fragment fragment, boolean z) {
        Fragment fragment2 = fragment;
        boolean z2 = z;
        if (fragment2 == null) {
            return null;
        }
        return FragmentTransitionCompat21.cloneTransition(z2 ? fragment2.getReenterTransition() : fragment2.getEnterTransition());
    }

    private static Object getExitTransition(Fragment fragment, boolean z) {
        Fragment fragment2 = fragment;
        boolean z2 = z;
        if (fragment2 == null) {
            return null;
        }
        return FragmentTransitionCompat21.cloneTransition(z2 ? fragment2.getReturnTransition() : fragment2.getExitTransition());
    }

    private static Object getSharedElementTransition(Fragment fragment, Fragment fragment2, boolean z) {
        Fragment fragment3 = fragment;
        Fragment fragment4 = fragment2;
        boolean z2 = z;
        if (fragment3 == null || fragment4 == null) {
            return null;
        }
        return FragmentTransitionCompat21.wrapSharedElementTransition(z2 ? fragment4.getSharedElementReturnTransition() : fragment3.getSharedElementEnterTransition());
    }

    private static Object captureExitingViews(Object obj, Fragment fragment, ArrayList<View> arrayList, ArrayMap<String, View> arrayMap, View view) {
        Object obj2 = obj;
        Fragment fragment2 = fragment;
        ArrayList<View> arrayList2 = arrayList;
        ArrayMap<String, View> arrayMap2 = arrayMap;
        View view2 = view;
        if (obj2 != null) {
            obj2 = FragmentTransitionCompat21.captureExitingViews(obj2, fragment2.getView(), arrayList2, arrayMap2, view2);
        }
        return obj2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.BackStackRecord.setNameOverrides(android.support.v4.app.BackStackRecord$TransitionState, android.support.v4.util.ArrayMap<java.lang.String, android.view.View>, boolean):void
     arg types: [android.support.v4.app.BackStackRecord$TransitionState, android.support.v4.util.ArrayMap, int]
     candidates:
      android.support.v4.app.BackStackRecord.setNameOverrides(android.support.v4.app.BackStackRecord$TransitionState, java.util.ArrayList<java.lang.String>, java.util.ArrayList<java.lang.String>):void
      android.support.v4.app.BackStackRecord.setNameOverrides(android.support.v4.app.BackStackRecord$TransitionState, android.support.v4.util.ArrayMap<java.lang.String, android.view.View>, boolean):void */
    private ArrayMap<String, View> remapSharedElements(TransitionState transitionState, Fragment fragment, boolean z) {
        ArrayMap arrayMap;
        TransitionState transitionState2 = transitionState;
        Fragment fragment2 = fragment;
        boolean z2 = z;
        new ArrayMap<>();
        ArrayMap<String, View> arrayMap2 = arrayMap;
        if (this.mSharedElementSourceNames != null) {
            FragmentTransitionCompat21.findNamedViews(arrayMap2, fragment2.getView());
            if (z2) {
                boolean retainAll = arrayMap2.retainAll(this.mSharedElementTargetNames);
            } else {
                arrayMap2 = remapNames(this.mSharedElementSourceNames, this.mSharedElementTargetNames, arrayMap2);
            }
        }
        if (z2) {
            if (fragment2.mEnterTransitionCallback != null) {
                fragment2.mEnterTransitionCallback.onMapSharedElements(this.mSharedElementTargetNames, arrayMap2);
            }
            setBackNameOverrides(transitionState2, arrayMap2, false);
        } else {
            if (fragment2.mExitTransitionCallback != null) {
                fragment2.mExitTransitionCallback.onMapSharedElements(this.mSharedElementTargetNames, arrayMap2);
            }
            setNameOverrides(transitionState2, (ArrayMap<String, View>) arrayMap2, false);
        }
        return arrayMap2;
    }

    private boolean configureTransitions(int i, TransitionState transitionState, boolean z, SparseArray<Fragment> sparseArray, SparseArray<Fragment> sparseArray2) {
        ArrayList arrayList;
        ArrayList arrayList2;
        FragmentTransitionCompat21.ViewRetriever viewRetriever;
        ArrayList arrayList3;
        Map map;
        boolean z2;
        boolean allowEnterTransitionOverlap;
        View view;
        SharedElementCallback sharedElementCallback;
        List list;
        List list2;
        int i2 = i;
        TransitionState transitionState2 = transitionState;
        boolean z3 = z;
        SparseArray<Fragment> sparseArray3 = sparseArray;
        SparseArray<Fragment> sparseArray4 = sparseArray2;
        ViewGroup viewGroup = (ViewGroup) this.mManager.mContainer.onFindViewById(i2);
        if (viewGroup == null) {
            return false;
        }
        Fragment fragment = sparseArray4.get(i2);
        Fragment fragment2 = sparseArray3.get(i2);
        Object enterTransition = getEnterTransition(fragment, z3);
        Object sharedElementTransition = getSharedElementTransition(fragment, fragment2, z3);
        Object exitTransition = getExitTransition(fragment2, z3);
        ArrayMap<String, View> arrayMap = null;
        new ArrayList();
        ArrayList arrayList4 = arrayList;
        if (sharedElementTransition != null) {
            arrayMap = remapSharedElements(transitionState2, fragment2, z3);
            if (arrayMap.isEmpty()) {
                sharedElementTransition = null;
                arrayMap = null;
            } else {
                if (z3) {
                    sharedElementCallback = fragment2.mEnterTransitionCallback;
                } else {
                    sharedElementCallback = fragment.mEnterTransitionCallback;
                }
                SharedElementCallback sharedElementCallback2 = sharedElementCallback;
                if (sharedElementCallback2 != null) {
                    new ArrayList(arrayMap.keySet());
                    new ArrayList(arrayMap.values());
                    sharedElementCallback2.onSharedElementStart(list, list2, null);
                }
                prepareSharedElementTransition(transitionState2, viewGroup, sharedElementTransition, fragment, fragment2, z3, arrayList4);
            }
        }
        if (enterTransition == null && sharedElementTransition == null && exitTransition == null) {
            return false;
        }
        new ArrayList();
        ArrayList arrayList5 = arrayList2;
        Object captureExitingViews = captureExitingViews(exitTransition, fragment2, arrayList5, arrayMap, transitionState2.nonExistentView);
        if (!(this.mSharedElementTargetNames == null || arrayMap == null || (view = (View) arrayMap.get(this.mSharedElementTargetNames.get(0))) == null)) {
            if (captureExitingViews != null) {
                FragmentTransitionCompat21.setEpicenter(captureExitingViews, view);
            }
            if (sharedElementTransition != null) {
                FragmentTransitionCompat21.setEpicenter(sharedElementTransition, view);
            }
        }
        final Fragment fragment3 = fragment;
        new FragmentTransitionCompat21.ViewRetriever() {
            public View getView() {
                return fragment3.getView();
            }
        };
        FragmentTransitionCompat21.ViewRetriever viewRetriever2 = viewRetriever;
        new ArrayList();
        ArrayList arrayList6 = arrayList3;
        new ArrayMap();
        Map map2 = map;
        boolean z4 = true;
        if (fragment != null) {
            if (z3) {
                allowEnterTransitionOverlap = fragment.getAllowReturnTransitionOverlap();
            } else {
                allowEnterTransitionOverlap = fragment.getAllowEnterTransitionOverlap();
            }
            z4 = allowEnterTransitionOverlap;
        }
        Object mergeTransitions = FragmentTransitionCompat21.mergeTransitions(enterTransition, captureExitingViews, sharedElementTransition, z4);
        if (mergeTransitions != null) {
            FragmentTransitionCompat21.addTransitionTargets(enterTransition, sharedElementTransition, viewGroup, viewRetriever2, transitionState2.nonExistentView, transitionState2.enteringEpicenterView, transitionState2.nameOverrides, arrayList6, arrayMap, map2, arrayList4);
            excludeHiddenFragmentsAfterEnter(viewGroup, transitionState2, i2, mergeTransitions);
            FragmentTransitionCompat21.excludeTarget(mergeTransitions, transitionState2.nonExistentView, true);
            excludeHiddenFragments(transitionState2, i2, mergeTransitions);
            FragmentTransitionCompat21.beginDelayedTransition(viewGroup, mergeTransitions);
            FragmentTransitionCompat21.cleanupTransitions(viewGroup, transitionState2.nonExistentView, enterTransition, arrayList6, captureExitingViews, arrayList5, sharedElementTransition, arrayList4, mergeTransitions, transitionState2.hiddenFragmentViews, map2);
        }
        if (mergeTransitions != null) {
            z2 = true;
        } else {
            z2 = false;
        }
        return z2;
    }

    private void prepareSharedElementTransition(TransitionState transitionState, View view, Object obj, Fragment fragment, Fragment fragment2, boolean z, ArrayList<View> arrayList) {
        ViewTreeObserver.OnPreDrawListener onPreDrawListener;
        View view2 = view;
        final View view3 = view2;
        final Object obj2 = obj;
        final ArrayList<View> arrayList2 = arrayList;
        final TransitionState transitionState2 = transitionState;
        final boolean z2 = z;
        final Fragment fragment3 = fragment;
        final Fragment fragment4 = fragment2;
        new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                view3.getViewTreeObserver().removeOnPreDrawListener(this);
                if (obj2 != null) {
                    FragmentTransitionCompat21.removeTargets(obj2, arrayList2);
                    arrayList2.clear();
                    ArrayMap access$000 = BackStackRecord.this.mapSharedElementsIn(transitionState2, z2, fragment3);
                    FragmentTransitionCompat21.setSharedElementTargets(obj2, transitionState2.nonExistentView, access$000, arrayList2);
                    BackStackRecord.this.setEpicenterIn(access$000, transitionState2);
                    BackStackRecord.this.callSharedElementEnd(transitionState2, fragment3, fragment4, z2, access$000);
                }
                return true;
            }
        };
        view2.getViewTreeObserver().addOnPreDrawListener(onPreDrawListener);
    }

    /* access modifiers changed from: private */
    public void callSharedElementEnd(TransitionState transitionState, Fragment fragment, Fragment fragment2, boolean z, ArrayMap<String, View> arrayMap) {
        List list;
        List list2;
        ArrayMap<String, View> arrayMap2 = arrayMap;
        SharedElementCallback sharedElementCallback = z ? fragment2.mEnterTransitionCallback : fragment.mEnterTransitionCallback;
        if (sharedElementCallback != null) {
            new ArrayList(arrayMap2.keySet());
            new ArrayList(arrayMap2.values());
            sharedElementCallback.onSharedElementEnd(list, list2, null);
        }
    }

    /* access modifiers changed from: private */
    public void setEpicenterIn(ArrayMap<String, View> arrayMap, TransitionState transitionState) {
        ArrayMap<String, View> arrayMap2 = arrayMap;
        TransitionState transitionState2 = transitionState;
        if (this.mSharedElementTargetNames != null && !arrayMap2.isEmpty()) {
            View view = arrayMap2.get(this.mSharedElementTargetNames.get(0));
            if (view != null) {
                transitionState2.enteringEpicenterView.epicenter = view;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.v4.app.BackStackRecord.setNameOverrides(android.support.v4.app.BackStackRecord$TransitionState, android.support.v4.util.ArrayMap<java.lang.String, android.view.View>, boolean):void
     arg types: [android.support.v4.app.BackStackRecord$TransitionState, android.support.v4.util.ArrayMap<java.lang.String, android.view.View>, int]
     candidates:
      android.support.v4.app.BackStackRecord.setNameOverrides(android.support.v4.app.BackStackRecord$TransitionState, java.util.ArrayList<java.lang.String>, java.util.ArrayList<java.lang.String>):void
      android.support.v4.app.BackStackRecord.setNameOverrides(android.support.v4.app.BackStackRecord$TransitionState, android.support.v4.util.ArrayMap<java.lang.String, android.view.View>, boolean):void */
    /* access modifiers changed from: private */
    public ArrayMap<String, View> mapSharedElementsIn(TransitionState transitionState, boolean z, Fragment fragment) {
        TransitionState transitionState2 = transitionState;
        boolean z2 = z;
        Fragment fragment2 = fragment;
        ArrayMap<String, View> mapEnteringSharedElements = mapEnteringSharedElements(transitionState2, fragment2, z2);
        if (z2) {
            if (fragment2.mExitTransitionCallback != null) {
                fragment2.mExitTransitionCallback.onMapSharedElements(this.mSharedElementTargetNames, mapEnteringSharedElements);
            }
            setBackNameOverrides(transitionState2, mapEnteringSharedElements, true);
        } else {
            if (fragment2.mEnterTransitionCallback != null) {
                fragment2.mEnterTransitionCallback.onMapSharedElements(this.mSharedElementTargetNames, mapEnteringSharedElements);
            }
            setNameOverrides(transitionState2, mapEnteringSharedElements, true);
        }
        return mapEnteringSharedElements;
    }

    /* JADX WARN: Type inference failed for: r10v0 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static android.support.v4.util.ArrayMap<java.lang.String, android.view.View> remapNames(java.util.ArrayList<java.lang.String> r11, java.util.ArrayList<java.lang.String> r12, android.support.v4.util.ArrayMap<java.lang.String, android.view.View> r13) {
        /*
            r0 = r11
            r1 = r12
            r2 = r13
            r7 = r2
            boolean r7 = r7.isEmpty()
            if (r7 == 0) goto L_0x000d
            r7 = r2
            r0 = r7
        L_0x000c:
            return r0
        L_0x000d:
            android.support.v4.util.ArrayMap r7 = new android.support.v4.util.ArrayMap
            r10 = r7
            r7 = r10
            r8 = r10
            r8.<init>()
            r3 = r7
            r7 = r0
            int r7 = r7.size()
            r4 = r7
            r7 = 0
            r5 = r7
        L_0x001e:
            r7 = r5
            r8 = r4
            if (r7 >= r8) goto L_0x0042
            r7 = r2
            r8 = r0
            r9 = r5
            java.lang.Object r8 = r8.get(r9)
            java.lang.Object r7 = r7.get(r8)
            android.view.View r7 = (android.view.View) r7
            r6 = r7
            r7 = r6
            if (r7 == 0) goto L_0x003f
            r7 = r3
            r8 = r1
            r9 = r5
            java.lang.Object r8 = r8.get(r9)
            r9 = r6
            java.lang.Object r7 = r7.put(r8, r9)
        L_0x003f:
            int r5 = r5 + 1
            goto L_0x001e
        L_0x0042:
            r7 = r3
            r0 = r7
            goto L_0x000c
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.app.BackStackRecord.remapNames(java.util.ArrayList, java.util.ArrayList, android.support.v4.util.ArrayMap):android.support.v4.util.ArrayMap");
    }

    /* JADX WARN: Type inference failed for: r9v0 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.support.v4.util.ArrayMap<java.lang.String, android.view.View> mapEnteringSharedElements(android.support.v4.app.BackStackRecord.TransitionState r11, android.support.v4.app.Fragment r12, boolean r13) {
        /*
            r10 = this;
            r0 = r10
            r1 = r11
            r2 = r12
            r3 = r13
            android.support.v4.util.ArrayMap r6 = new android.support.v4.util.ArrayMap
            r9 = r6
            r6 = r9
            r7 = r9
            r7.<init>()
            r4 = r6
            r6 = r2
            android.view.View r6 = r6.getView()
            r5 = r6
            r6 = r5
            if (r6 == 0) goto L_0x002f
            r6 = r0
            java.util.ArrayList<java.lang.String> r6 = r6.mSharedElementSourceNames
            if (r6 == 0) goto L_0x002f
            r6 = r4
            r7 = r5
            android.support.v4.app.FragmentTransitionCompat21.findNamedViews(r6, r7)
            r6 = r3
            if (r6 == 0) goto L_0x0032
            r6 = r0
            java.util.ArrayList<java.lang.String> r6 = r6.mSharedElementSourceNames
            r7 = r0
            java.util.ArrayList<java.lang.String> r7 = r7.mSharedElementTargetNames
            r8 = r4
            android.support.v4.util.ArrayMap r6 = remapNames(r6, r7, r8)
            r4 = r6
        L_0x002f:
            r6 = r4
            r0 = r6
            return r0
        L_0x0032:
            r6 = r4
            r7 = r0
            java.util.ArrayList<java.lang.String> r7 = r7.mSharedElementTargetNames
            boolean r6 = r6.retainAll(r7)
            goto L_0x002f
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.v4.app.BackStackRecord.mapEnteringSharedElements(android.support.v4.app.BackStackRecord$TransitionState, android.support.v4.app.Fragment, boolean):android.support.v4.util.ArrayMap");
    }

    private void excludeHiddenFragmentsAfterEnter(View view, TransitionState transitionState, int i, Object obj) {
        ViewTreeObserver.OnPreDrawListener onPreDrawListener;
        View view2 = view;
        final View view3 = view2;
        final TransitionState transitionState2 = transitionState;
        final int i2 = i;
        final Object obj2 = obj;
        new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                view3.getViewTreeObserver().removeOnPreDrawListener(this);
                BackStackRecord.this.excludeHiddenFragments(transitionState2, i2, obj2);
                return true;
            }
        };
        view2.getViewTreeObserver().addOnPreDrawListener(onPreDrawListener);
    }

    /* access modifiers changed from: private */
    public void excludeHiddenFragments(TransitionState transitionState, int i, Object obj) {
        TransitionState transitionState2 = transitionState;
        int i2 = i;
        Object obj2 = obj;
        if (this.mManager.mAdded != null) {
            for (int i3 = 0; i3 < this.mManager.mAdded.size(); i3++) {
                Fragment fragment = this.mManager.mAdded.get(i3);
                if (!(fragment.mView == null || fragment.mContainer == null || fragment.mContainerId != i2)) {
                    if (!fragment.mHidden) {
                        FragmentTransitionCompat21.excludeTarget(obj2, fragment.mView, false);
                        boolean remove = transitionState2.hiddenFragmentViews.remove(fragment.mView);
                    } else if (!transitionState2.hiddenFragmentViews.contains(fragment.mView)) {
                        FragmentTransitionCompat21.excludeTarget(obj2, fragment.mView, true);
                        boolean add = transitionState2.hiddenFragmentViews.add(fragment.mView);
                    }
                }
            }
        }
    }

    private static void setNameOverride(ArrayMap<String, String> arrayMap, String str, String str2) {
        ArrayMap<String, String> arrayMap2 = arrayMap;
        String str3 = str;
        String str4 = str2;
        if (str3 != null && str4 != null) {
            for (int i = 0; i < arrayMap2.size(); i++) {
                if (str3.equals(arrayMap2.valueAt(i))) {
                    String valueAt = arrayMap2.setValueAt(i, str4);
                    return;
                }
            }
            String put = arrayMap2.put(str3, str4);
        }
    }

    private static void setNameOverrides(TransitionState transitionState, ArrayList<String> arrayList, ArrayList<String> arrayList2) {
        TransitionState transitionState2 = transitionState;
        ArrayList<String> arrayList3 = arrayList;
        ArrayList<String> arrayList4 = arrayList2;
        if (arrayList3 != null) {
            for (int i = 0; i < arrayList3.size(); i++) {
                setNameOverride(transitionState2.nameOverrides, arrayList3.get(i), arrayList4.get(i));
            }
        }
    }

    private void setBackNameOverrides(TransitionState transitionState, ArrayMap<String, View> arrayMap, boolean z) {
        TransitionState transitionState2 = transitionState;
        ArrayMap<String, View> arrayMap2 = arrayMap;
        boolean z2 = z;
        int size = this.mSharedElementTargetNames == null ? 0 : this.mSharedElementTargetNames.size();
        for (int i = 0; i < size; i++) {
            String str = this.mSharedElementSourceNames.get(i);
            View view = arrayMap2.get(this.mSharedElementTargetNames.get(i));
            if (view != null) {
                String transitionName = FragmentTransitionCompat21.getTransitionName(view);
                if (z2) {
                    setNameOverride(transitionState2.nameOverrides, str, transitionName);
                } else {
                    setNameOverride(transitionState2.nameOverrides, transitionName, str);
                }
            }
        }
    }

    private void setNameOverrides(TransitionState transitionState, ArrayMap<String, View> arrayMap, boolean z) {
        TransitionState transitionState2 = transitionState;
        ArrayMap<String, View> arrayMap2 = arrayMap;
        boolean z2 = z;
        int size = arrayMap2.size();
        for (int i = 0; i < size; i++) {
            String keyAt = arrayMap2.keyAt(i);
            String transitionName = FragmentTransitionCompat21.getTransitionName(arrayMap2.valueAt(i));
            if (z2) {
                setNameOverride(transitionState2.nameOverrides, keyAt, transitionName);
            } else {
                setNameOverride(transitionState2.nameOverrides, transitionName, keyAt);
            }
        }
    }

    public class TransitionState {
        public FragmentTransitionCompat21.EpicenterView enteringEpicenterView;
        public ArrayList<View> hiddenFragmentViews;
        public ArrayMap<String, String> nameOverrides;
        public View nonExistentView;

        public TransitionState() {
            ArrayMap<String, String> arrayMap;
            ArrayList<View> arrayList;
            FragmentTransitionCompat21.EpicenterView epicenterView;
            new ArrayMap<>();
            this.nameOverrides = arrayMap;
            new ArrayList<>();
            this.hiddenFragmentViews = arrayList;
            new FragmentTransitionCompat21.EpicenterView();
            this.enteringEpicenterView = epicenterView;
        }
    }
}
