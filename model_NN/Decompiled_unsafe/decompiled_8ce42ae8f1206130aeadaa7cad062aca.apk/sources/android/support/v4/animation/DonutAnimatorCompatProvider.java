package android.support.v4.animation;

import android.view.View;
import java.util.ArrayList;
import java.util.List;

class DonutAnimatorCompatProvider implements AnimatorProvider {
    DonutAnimatorCompatProvider() {
    }

    public ValueAnimatorCompat emptyValueAnimator() {
        ValueAnimatorCompat valueAnimatorCompat;
        new DonutFloatValueAnimator();
        return valueAnimatorCompat;
    }

    private static class DonutFloatValueAnimator implements ValueAnimatorCompat {
        /* access modifiers changed from: private */
        public long mDuration = 200;
        private boolean mEnded = false;
        /* access modifiers changed from: private */
        public float mFraction = 0.0f;
        List<AnimatorListenerCompat> mListeners;
        /* access modifiers changed from: private */
        public Runnable mLoopRunnable;
        /* access modifiers changed from: private */
        public long mStartTime;
        private boolean mStarted = false;
        View mTarget;
        List<AnimatorUpdateListenerCompat> mUpdateListeners;

        static /* synthetic */ float access$302(DonutFloatValueAnimator donutFloatValueAnimator, float f) {
            float f2 = f;
            float f3 = f2;
            donutFloatValueAnimator.mFraction = f3;
            return f2;
        }

        public DonutFloatValueAnimator() {
            List<AnimatorListenerCompat> list;
            List<AnimatorUpdateListenerCompat> list2;
            Runnable runnable;
            new ArrayList();
            this.mListeners = list;
            new ArrayList();
            this.mUpdateListeners = list2;
            new Runnable() {
                public void run() {
                    float access$000 = (((float) (DonutFloatValueAnimator.this.getTime() - DonutFloatValueAnimator.this.mStartTime)) * 1.0f) / ((float) DonutFloatValueAnimator.this.mDuration);
                    if (access$000 > 1.0f || DonutFloatValueAnimator.this.mTarget.getParent() == null) {
                        access$000 = 1.0f;
                    }
                    float access$302 = DonutFloatValueAnimator.access$302(DonutFloatValueAnimator.this, access$000);
                    DonutFloatValueAnimator.this.notifyUpdateListeners();
                    if (DonutFloatValueAnimator.this.mFraction >= 1.0f) {
                        DonutFloatValueAnimator.this.dispatchEnd();
                    } else {
                        boolean postDelayed = DonutFloatValueAnimator.this.mTarget.postDelayed(DonutFloatValueAnimator.this.mLoopRunnable, 16);
                    }
                }
            };
            this.mLoopRunnable = runnable;
        }

        /* access modifiers changed from: private */
        public void notifyUpdateListeners() {
            for (int size = this.mUpdateListeners.size() - 1; size >= 0; size--) {
                this.mUpdateListeners.get(size).onAnimationUpdate(this);
            }
        }

        public void setTarget(View view) {
            this.mTarget = view;
        }

        public void addListener(AnimatorListenerCompat animatorListenerCompat) {
            boolean add = this.mListeners.add(animatorListenerCompat);
        }

        public void setDuration(long j) {
            long j2 = j;
            if (!this.mStarted) {
                this.mDuration = j2;
            }
        }

        public void start() {
            if (!this.mStarted) {
                this.mStarted = true;
                dispatchStart();
                this.mFraction = 0.0f;
                this.mStartTime = getTime();
                boolean postDelayed = this.mTarget.postDelayed(this.mLoopRunnable, 16);
            }
        }

        /* access modifiers changed from: private */
        public long getTime() {
            return this.mTarget.getDrawingTime();
        }

        private void dispatchStart() {
            for (int size = this.mListeners.size() - 1; size >= 0; size--) {
                this.mListeners.get(size).onAnimationStart(this);
            }
        }

        /* access modifiers changed from: private */
        public void dispatchEnd() {
            for (int size = this.mListeners.size() - 1; size >= 0; size--) {
                this.mListeners.get(size).onAnimationEnd(this);
            }
        }

        private void dispatchCancel() {
            for (int size = this.mListeners.size() - 1; size >= 0; size--) {
                this.mListeners.get(size).onAnimationCancel(this);
            }
        }

        public void cancel() {
            if (!this.mEnded) {
                this.mEnded = true;
                if (this.mStarted) {
                    dispatchCancel();
                }
                dispatchEnd();
            }
        }

        public void addUpdateListener(AnimatorUpdateListenerCompat animatorUpdateListenerCompat) {
            boolean add = this.mUpdateListeners.add(animatorUpdateListenerCompat);
        }

        public float getAnimatedFraction() {
            return this.mFraction;
        }
    }

    public void clearInterpolator(View view) {
    }
}
