package android.support.v4.content;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.Loader;
import android.support.v4.os.CancellationSignal;
import android.support.v4.os.OperationCanceledException;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Arrays;

public class CursorLoader extends AsyncTaskLoader<Cursor> {
    CancellationSignal mCancellationSignal;
    Cursor mCursor;
    final Loader<Cursor>.ForceLoadContentObserver mObserver;
    String[] mProjection;
    String mSelection;
    String[] mSelectionArgs;
    String mSortOrder;
    Uri mUri;

    /* JADX INFO: finally extract failed */
    public Cursor loadInBackground() {
        CancellationSignal cancellationSignal;
        Cursor query;
        Throwable th;
        synchronized (this) {
            try {
                if (isLoadInBackgroundCanceled()) {
                    Throwable th2 = th;
                    new OperationCanceledException();
                    throw th2;
                }
                new CancellationSignal();
                this.mCancellationSignal = cancellationSignal;
                try {
                    query = ContentResolverCompat.query(getContext().getContentResolver(), this.mUri, this.mProjection, this.mSelection, this.mSelectionArgs, this.mSortOrder, this.mCancellationSignal);
                    if (query != null) {
                        int count = query.getCount();
                        query.registerContentObserver(this.mObserver);
                    }
                    Cursor cursor = query;
                    synchronized (this) {
                        try {
                            this.mCancellationSignal = null;
                            return cursor;
                        } catch (Throwable th3) {
                            while (true) {
                                throw th3;
                            }
                        }
                    }
                } catch (RuntimeException e) {
                    RuntimeException runtimeException = e;
                    query.close();
                    throw runtimeException;
                } catch (Throwable th4) {
                    Throwable th5 = th4;
                    synchronized (this) {
                        try {
                            this.mCancellationSignal = null;
                            throw th5;
                        } catch (Throwable th6) {
                            while (true) {
                                throw th6;
                            }
                        }
                    }
                }
            } catch (Throwable th7) {
                throw th7;
            }
        }
    }

    public void cancelLoadInBackground() {
        super.cancelLoadInBackground();
        synchronized (this) {
            try {
                if (this.mCancellationSignal != null) {
                    this.mCancellationSignal.cancel();
                }
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    public void deliverResult(Cursor cursor) {
        Cursor cursor2 = cursor;
        if (!isReset()) {
            Cursor cursor3 = this.mCursor;
            this.mCursor = cursor2;
            if (isStarted()) {
                super.deliverResult((Object) cursor2);
            }
            if (cursor3 != null && cursor3 != cursor2 && !cursor3.isClosed()) {
                cursor3.close();
            }
        } else if (cursor2 != null) {
            cursor2.close();
        }
    }

    public CursorLoader(Context context) {
        super(context);
        Loader<Cursor>.ForceLoadContentObserver forceLoadContentObserver;
        new Loader.ForceLoadContentObserver();
        this.mObserver = forceLoadContentObserver;
    }

    public CursorLoader(Context context, Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        super(context);
        Loader<Cursor>.ForceLoadContentObserver forceLoadContentObserver;
        new Loader.ForceLoadContentObserver();
        this.mObserver = forceLoadContentObserver;
        this.mUri = uri;
        this.mProjection = strArr;
        this.mSelection = str;
        this.mSelectionArgs = strArr2;
        this.mSortOrder = str2;
    }

    /* access modifiers changed from: protected */
    public void onStartLoading() {
        if (this.mCursor != null) {
            deliverResult(this.mCursor);
        }
        if (takeContentChanged() || this.mCursor == null) {
            forceLoad();
        }
    }

    /* access modifiers changed from: protected */
    public void onStopLoading() {
        boolean cancelLoad = cancelLoad();
    }

    public void onCanceled(Cursor cursor) {
        Cursor cursor2 = cursor;
        if (cursor2 != null && !cursor2.isClosed()) {
            cursor2.close();
        }
    }

    /* access modifiers changed from: protected */
    public void onReset() {
        super.onReset();
        onStopLoading();
        if (this.mCursor != null && !this.mCursor.isClosed()) {
            this.mCursor.close();
        }
        this.mCursor = null;
    }

    public Uri getUri() {
        return this.mUri;
    }

    public void setUri(Uri uri) {
        this.mUri = uri;
    }

    public String[] getProjection() {
        return this.mProjection;
    }

    public void setProjection(String[] strArr) {
        this.mProjection = strArr;
    }

    public String getSelection() {
        return this.mSelection;
    }

    public void setSelection(String str) {
        this.mSelection = str;
    }

    public String[] getSelectionArgs() {
        return this.mSelectionArgs;
    }

    public void setSelectionArgs(String[] strArr) {
        this.mSelectionArgs = strArr;
    }

    public String getSortOrder() {
        return this.mSortOrder;
    }

    public void setSortOrder(String str) {
        this.mSortOrder = str;
    }

    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        String str2 = str;
        PrintWriter printWriter2 = printWriter;
        super.dump(str2, fileDescriptor, printWriter2, strArr);
        printWriter2.print(str2);
        printWriter2.print("mUri=");
        printWriter2.println(this.mUri);
        printWriter2.print(str2);
        printWriter2.print("mProjection=");
        printWriter2.println(Arrays.toString(this.mProjection));
        printWriter2.print(str2);
        printWriter2.print("mSelection=");
        printWriter2.println(this.mSelection);
        printWriter2.print(str2);
        printWriter2.print("mSelectionArgs=");
        printWriter2.println(Arrays.toString(this.mSelectionArgs));
        printWriter2.print(str2);
        printWriter2.print("mSortOrder=");
        printWriter2.println(this.mSortOrder);
        printWriter2.print(str2);
        printWriter2.print("mCursor=");
        printWriter2.println(this.mCursor);
        printWriter2.print(str2);
        printWriter2.print("mContentChanged=");
        printWriter2.println(this.mContentChanged);
    }
}
