package com.depositmobi;

import android.util.Pair;
import java.util.ArrayList;

public class KSGetActivationSchemes {
    public ArrayList<Pair<String, String>> list;
    public int smsQuantity;

    public KSGetActivationSchemes(int smsQuantity2, ArrayList<Pair<String, String>> list2) {
        this.smsQuantity = smsQuantity2;
        this.list = list2;
    }
}
