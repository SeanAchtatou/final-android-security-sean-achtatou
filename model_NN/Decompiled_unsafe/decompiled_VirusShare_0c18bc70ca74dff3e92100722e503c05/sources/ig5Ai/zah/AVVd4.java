package ig5Ai.zah;

import android.app.AlertDialog;
import android.view.View;

final class AVVd4 implements View.OnClickListener {
    private /* synthetic */ Aeseew7 veHgIUy1o;

    AVVd4(Aeseew7 aeseew7) {
        this.veHgIUy1o = aeseew7;
    }

    public final void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.veHgIUy1o);
        builder.setMessage((int) R.string.info).setCancelable(false).setNeutralButton((int) R.string.ok, new A72KE6c(this));
        builder.create().show();
    }
}
