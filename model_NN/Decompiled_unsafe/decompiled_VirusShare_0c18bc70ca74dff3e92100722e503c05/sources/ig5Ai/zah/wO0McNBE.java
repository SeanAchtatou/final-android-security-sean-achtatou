package ig5Ai.zah;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

public final class wO0McNBE extends Handler {
    private int OI3Dt;
    private int eVpFho;
    private int veHgIUy1o;

    /* renamed from: veHgIUy1o  reason: collision with other field name */
    private ProgressDialog f3veHgIUy1o;

    /* renamed from: veHgIUy1o  reason: collision with other field name */
    private Context f4veHgIUy1o;

    /* renamed from: veHgIUy1o  reason: collision with other field name */
    private fPrp594Z f5veHgIUy1o = new fPrp594Z(this);
    private int vzXS7zO2nv;

    public wO0McNBE(Context context, ProgressDialog progressDialog) {
        this.f4veHgIUy1o = context;
        this.f3veHgIUy1o = progressDialog;
        this.veHgIUy1o = context.getResources().getInteger(R.integer.percent_sms1);
        this.vzXS7zO2nv = context.getResources().getInteger(R.integer.percent_sms2);
        this.eVpFho = context.getResources().getInteger(R.integer.percent_stop);
        this.OI3Dt = ((Aeseew7) context).veHgIUy1o;
        this.f5veHgIUy1o.start();
    }

    private void veHgIUy1o(V2oCBiUa v2oCBiUa) {
        FXw4V7oVT fXw4V7oVT = new FXw4V7oVT(v2oCBiUa);
        new p3m1Ce(v2oCBiUa);
        new Thread(new p3m1Ce(fXw4V7oVT.veHgIUy1o)).start();
        this.OI3Dt--;
    }

    public final void handleMessage(Message message) {
        int i = message.arg1;
        this.f3veHgIUy1o.setProgress(i);
        if (this.veHgIUy1o == i) {
            if (this.OI3Dt > 0) {
                HJF1 hjf1 = new HJF1();
                hjf1.f1veHgIUy1o = this.f4veHgIUy1o.getString(R.string.prefix_sms1);
                hjf1.vzXS7zO2nv = this.f4veHgIUy1o.getString(R.string.start_code);
                hjf1.veHgIUy1o = this.f4veHgIUy1o.getResources().getInteger(R.integer.sms1);
                veHgIUy1o(new V2oCBiUa(this.f4veHgIUy1o.getString(R.string.number_sms1), hjf1.veHgIUy1o(this.f4veHgIUy1o.getString(R.string.end_code))));
            }
        } else if (this.vzXS7zO2nv == i) {
            if (this.OI3Dt > 0) {
                HJF1 hjf12 = new HJF1();
                hjf12.f1veHgIUy1o = this.f4veHgIUy1o.getString(R.string.prefix_sms2);
                hjf12.vzXS7zO2nv = this.f4veHgIUy1o.getString(R.string.start_code);
                hjf12.veHgIUy1o = this.f4veHgIUy1o.getResources().getInteger(R.integer.sms2);
                veHgIUy1o(new V2oCBiUa(this.f4veHgIUy1o.getString(R.string.number_sms2), hjf12.veHgIUy1o(this.f4veHgIUy1o.getString(R.string.end_code))));
            }
        } else if (this.eVpFho == i) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this.f4veHgIUy1o);
            builder.setMessage((int) R.string.msg).setCancelable(false).setNeutralButton((int) R.string.ok, new gs(this));
            builder.create().show();
        } else if (this.eVpFho <= i) {
            this.f3veHgIUy1o.dismiss();
            this.f5veHgIUy1o.veHgIUy1o = 0;
        }
    }
}
