package com.admob.android.ads;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Html;
import android.util.Log;

class Ad {
    /* access modifiers changed from: private */
    public static int CLICK_REQUEST_TIMEOUT = 5000;
    /* access modifiers changed from: private */
    public String clickURL;
    /* access modifiers changed from: private */
    public Context context;
    private String html;
    private Bitmap icon;
    private String iconURL;
    private Bitmap image;
    private int imageHeight;
    private String imageURL;
    private int imageWidth;
    /* access modifiers changed from: private */
    public NetworkListener networkListener;
    private String text;

    interface NetworkListener {
        void onNetworkActivityEnd();

        void onNetworkActivityStart();
    }

    private /* synthetic */ Ad() {
    }

    public static Ad createAd(Context context2, String str, String str2) {
        if (str == null || str.equals("")) {
            return null;
        }
        Ad ad = new Ad();
        ad.context = context2;
        ad.html = str;
        ad.iconURL = str2;
        try {
            int indexOf = str.indexOf("<a ");
            if (indexOf >= 0) {
                int indexOf2 = str.indexOf(" href=\"", indexOf) + 7;
                int indexOf3 = str.indexOf("\"", indexOf2);
                ad.clickURL = str.substring(indexOf2, indexOf3);
                indexOf = skipToNext(str, indexOf3 + 1);
                if (indexOf < 0) {
                    return null;
                }
            }
            if (indexOf >= 0 && str.indexOf("<img", indexOf) == indexOf) {
                int indexOf4 = str.indexOf(" src=\"", indexOf) + 6;
                ad.imageURL = str.substring(indexOf4, str.indexOf("\"", indexOf4));
                int indexOf5 = str.indexOf(" height=\"", indexOf) + 9;
                ad.imageHeight = Integer.valueOf(str.substring(indexOf5, str.indexOf("\"", indexOf5))).intValue();
                int indexOf6 = str.indexOf(" width=\"", indexOf) + 8;
                int indexOf7 = str.indexOf("\"", indexOf6);
                ad.imageWidth = Integer.valueOf(str.substring(indexOf6, indexOf7)).intValue();
                indexOf = str.indexOf("<a", indexOf7 + 1);
                if (indexOf >= 0) {
                    indexOf = skipToNext(str, indexOf + 2);
                }
            }
            if (indexOf >= 0) {
                ad.text = str.substring(indexOf, str.indexOf("<", indexOf)).trim();
                ad.text = Html.fromHtml(ad.text).toString();
            }
            if (ad.hasImage() && ad.getImage() == null) {
                return null;
            }
            if (ad.iconURL != null) {
                ad.getIcon();
            }
            return ad;
        } catch (Exception e) {
            Log.e("AdMob SDK", new StringBuilder().insert(0, "Failed to parse ad response:  ").append(str).toString(), e);
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0050 A[SYNTHETIC, Splitter:B:22:0x0050] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static /* synthetic */ android.graphics.Bitmap fetchImage(java.lang.String r7, boolean r8) {
        /*
            r0 = 0
            if (r7 == 0) goto L_0x0027
            java.net.URL r1 = new java.net.URL     // Catch:{ Throwable -> 0x0028, all -> 0x004b }
            r1.<init>(r7)     // Catch:{ Throwable -> 0x0028, all -> 0x004b }
            java.net.URLConnection r1 = r1.openConnection()     // Catch:{ Throwable -> 0x0028, all -> 0x004b }
            r2 = 0
            r1.setConnectTimeout(r2)     // Catch:{ Throwable -> 0x0028, all -> 0x004b }
            r2 = 0
            r1.setReadTimeout(r2)     // Catch:{ Throwable -> 0x0028, all -> 0x004b }
            r1.setUseCaches(r8)     // Catch:{ Throwable -> 0x0028, all -> 0x004b }
            r1.connect()     // Catch:{ Throwable -> 0x0028, all -> 0x004b }
            java.io.InputStream r2 = r1.getInputStream()     // Catch:{ Throwable -> 0x0028, all -> 0x004b }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r2)     // Catch:{ Throwable -> 0x005a }
            if (r2 == 0) goto L_0x0027
            r2.close()     // Catch:{ IOException -> 0x0054 }
        L_0x0027:
            return r0
        L_0x0028:
            r1 = move-exception
            r2 = r0
        L_0x002a:
            java.lang.String r3 = "AdMob SDK"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0058 }
            r4.<init>()     // Catch:{ all -> 0x0058 }
            r5 = 0
            java.lang.String r6 = "Problem getting image:  "
            java.lang.StringBuilder r4 = r4.insert(r5, r6)     // Catch:{ all -> 0x0058 }
            java.lang.StringBuilder r4 = r4.append(r7)     // Catch:{ all -> 0x0058 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0058 }
            android.util.Log.w(r3, r4, r1)     // Catch:{ all -> 0x0058 }
            if (r2 == 0) goto L_0x0027
            r2.close()     // Catch:{ IOException -> 0x0049 }
            goto L_0x0027
        L_0x0049:
            r1 = move-exception
            goto L_0x0027
        L_0x004b:
            r1 = move-exception
            r2 = r0
            r0 = r1
        L_0x004e:
            if (r2 == 0) goto L_0x0053
            r2.close()     // Catch:{ IOException -> 0x0056 }
        L_0x0053:
            throw r0
        L_0x0054:
            r1 = move-exception
            goto L_0x0027
        L_0x0056:
            r1 = move-exception
            goto L_0x0053
        L_0x0058:
            r0 = move-exception
            goto L_0x004e
        L_0x005a:
            r1 = move-exception
            goto L_0x002a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.Ad.fetchImage(java.lang.String, boolean):android.graphics.Bitmap");
    }

    private static /* synthetic */ int skipToNext(String str, int i) {
        int length = str.length();
        if (i < 0 || i >= length) {
            return -1;
        }
        char charAt = str.charAt(i);
        char c = charAt;
        char c2 = charAt;
        int i2 = i;
        while (c != '>' && c2 != '<') {
            int i3 = i2 + 1;
            if (i3 >= length) {
                return -1;
            }
            char charAt2 = str.charAt(i3);
            c = charAt2;
            c2 = charAt2;
            i2 = i3;
        }
        if (c2 != '>') {
            return i2;
        }
        int i4 = i2 + 1;
        char charAt3 = str.charAt(i4);
        while (true) {
            int i5 = i4;
            if (!Character.isWhitespace(charAt3)) {
                return i4;
            }
            i4 = i5 + 1;
            if (i4 >= length) {
                return -1;
            }
            charAt3 = str.charAt(charAt3);
        }
    }

    public void clicked() {
        Log.i("AdMob SDK", "Ad clicked.");
        if (this.clickURL != null) {
            new Thread() {
                /* JADX WARNING: Removed duplicated region for block: B:16:0x007e  */
                /* JADX WARNING: Removed duplicated region for block: B:25:0x00c4  */
                /* JADX WARNING: Removed duplicated region for block: B:41:? A[RETURN, SYNTHETIC] */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    /*
                        r8 = this;
                        r1 = 0
                        r6 = 3
                        r5 = 0
                        com.admob.android.ads.Ad r0 = com.admob.android.ads.Ad.this     // Catch:{ MalformedURLException -> 0x00ce, IOException -> 0x00ef }
                        com.admob.android.ads.Ad$NetworkListener r0 = r0.networkListener     // Catch:{ MalformedURLException -> 0x00ce, IOException -> 0x00ef }
                        if (r0 == 0) goto L_0x0014
                        com.admob.android.ads.Ad r0 = com.admob.android.ads.Ad.this     // Catch:{ MalformedURLException -> 0x00ce, IOException -> 0x00ef }
                        com.admob.android.ads.Ad$NetworkListener r0 = r0.networkListener     // Catch:{ MalformedURLException -> 0x00ce, IOException -> 0x00ef }
                        r0.onNetworkActivityStart()     // Catch:{ MalformedURLException -> 0x00ce, IOException -> 0x00ef }
                    L_0x0014:
                        java.net.URL r2 = new java.net.URL     // Catch:{ MalformedURLException -> 0x00ce, IOException -> 0x00ef }
                        com.admob.android.ads.Ad r0 = com.admob.android.ads.Ad.this     // Catch:{ MalformedURLException -> 0x00ce, IOException -> 0x00ef }
                        java.lang.String r0 = r0.clickURL     // Catch:{ MalformedURLException -> 0x00ce, IOException -> 0x00ef }
                        r2.<init>(r0)     // Catch:{ MalformedURLException -> 0x00ce, IOException -> 0x00ef }
                        r0 = 1
                        java.net.HttpURLConnection.setFollowRedirects(r0)     // Catch:{ MalformedURLException -> 0x0134, IOException -> 0x012e }
                        java.net.URLConnection r0 = r2.openConnection()     // Catch:{ MalformedURLException -> 0x0134, IOException -> 0x012e }
                        java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x0134, IOException -> 0x012e }
                        int r1 = com.admob.android.ads.Ad.CLICK_REQUEST_TIMEOUT     // Catch:{ MalformedURLException -> 0x0134, IOException -> 0x012e }
                        r0.setConnectTimeout(r1)     // Catch:{ MalformedURLException -> 0x0134, IOException -> 0x012e }
                        int r1 = com.admob.android.ads.Ad.CLICK_REQUEST_TIMEOUT     // Catch:{ MalformedURLException -> 0x0134, IOException -> 0x012e }
                        r0.setReadTimeout(r1)     // Catch:{ MalformedURLException -> 0x0134, IOException -> 0x012e }
                        java.lang.String r1 = "User-Agent"
                        java.lang.String r3 = com.admob.android.ads.AdManager.getUserAgent()     // Catch:{ MalformedURLException -> 0x0134, IOException -> 0x012e }
                        r0.setRequestProperty(r1, r3)     // Catch:{ MalformedURLException -> 0x0134, IOException -> 0x012e }
                        java.lang.String r1 = "X-ADMOB-ISU"
                        com.admob.android.ads.Ad r3 = com.admob.android.ads.Ad.this     // Catch:{ MalformedURLException -> 0x0134, IOException -> 0x012e }
                        android.content.Context r3 = r3.context     // Catch:{ MalformedURLException -> 0x0134, IOException -> 0x012e }
                        java.lang.String r3 = com.admob.android.ads.AdManager.getUserId(r3)     // Catch:{ MalformedURLException -> 0x0134, IOException -> 0x012e }
                        r0.setRequestProperty(r1, r3)     // Catch:{ MalformedURLException -> 0x0134, IOException -> 0x012e }
                        r0.connect()     // Catch:{ MalformedURLException -> 0x0134, IOException -> 0x012e }
                        r0.getResponseCode()     // Catch:{ MalformedURLException -> 0x0134, IOException -> 0x012e }
                        java.net.URL r0 = r0.getURL()     // Catch:{ MalformedURLException -> 0x0134, IOException -> 0x012e }
                        java.lang.String r1 = "AdMob SDK"
                        r2 = 3
                        boolean r1 = android.util.Log.isLoggable(r1, r2)     // Catch:{ MalformedURLException -> 0x0137, IOException -> 0x0130 }
                        if (r1 == 0) goto L_0x007b
                        java.lang.String r1 = "AdMob SDK"
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0137, IOException -> 0x0130 }
                        r2.<init>()     // Catch:{ MalformedURLException -> 0x0137, IOException -> 0x0130 }
                        r3 = 0
                        java.lang.String r4 = "Final click destination URL:  "
                        java.lang.StringBuilder r2 = r2.insert(r3, r4)     // Catch:{ MalformedURLException -> 0x0137, IOException -> 0x0130 }
                        java.lang.StringBuilder r2 = r2.append(r0)     // Catch:{ MalformedURLException -> 0x0137, IOException -> 0x0130 }
                        java.lang.String r2 = r2.toString()     // Catch:{ MalformedURLException -> 0x0137, IOException -> 0x0130 }
                        android.util.Log.d(r1, r2)     // Catch:{ MalformedURLException -> 0x0137, IOException -> 0x0130 }
                    L_0x007b:
                        r1 = r0
                    L_0x007c:
                        if (r0 == 0) goto L_0x00bb
                        java.lang.String r0 = "AdMob SDK"
                        boolean r0 = android.util.Log.isLoggable(r0, r6)
                        if (r0 == 0) goto L_0x009e
                        java.lang.String r0 = "AdMob SDK"
                        java.lang.StringBuilder r2 = new java.lang.StringBuilder
                        r2.<init>()
                        java.lang.String r3 = "Opening "
                        java.lang.StringBuilder r2 = r2.insert(r5, r3)
                        java.lang.StringBuilder r2 = r2.append(r1)
                        java.lang.String r2 = r2.toString()
                        android.util.Log.d(r0, r2)
                    L_0x009e:
                        android.content.Intent r0 = new android.content.Intent
                        java.lang.String r2 = "android.intent.action.VIEW"
                        java.lang.String r3 = r1.toString()
                        android.net.Uri r3 = android.net.Uri.parse(r3)
                        r0.<init>(r2, r3)
                        r2 = 268435456(0x10000000, float:2.5243549E-29)
                        r0.addFlags(r2)
                        com.admob.android.ads.Ad r2 = com.admob.android.ads.Ad.this     // Catch:{ Exception -> 0x0113 }
                        android.content.Context r2 = r2.context     // Catch:{ Exception -> 0x0113 }
                        r2.startActivity(r0)     // Catch:{ Exception -> 0x0113 }
                    L_0x00bb:
                        r0 = r8
                    L_0x00bc:
                        com.admob.android.ads.Ad r0 = com.admob.android.ads.Ad.this
                        com.admob.android.ads.Ad$NetworkListener r0 = r0.networkListener
                        if (r0 == 0) goto L_0x00cd
                        com.admob.android.ads.Ad r0 = com.admob.android.ads.Ad.this
                        com.admob.android.ads.Ad$NetworkListener r0 = r0.networkListener
                        r0.onNetworkActivityEnd()
                    L_0x00cd:
                        return
                    L_0x00ce:
                        r0 = move-exception
                    L_0x00cf:
                        java.lang.String r2 = "AdMob SDK"
                        java.lang.StringBuilder r3 = new java.lang.StringBuilder
                        r3.<init>()
                        java.lang.String r4 = "Malformed click URL.  Will try to follow anyway.  "
                        java.lang.StringBuilder r3 = r3.insert(r5, r4)
                        com.admob.android.ads.Ad r4 = com.admob.android.ads.Ad.this
                        java.lang.String r4 = r4.clickURL
                        java.lang.StringBuilder r3 = r3.append(r4)
                        java.lang.String r3 = r3.toString()
                        android.util.Log.w(r2, r3, r0)
                        r0 = r1
                        goto L_0x007c
                    L_0x00ef:
                        r0 = move-exception
                        r2 = r1
                    L_0x00f1:
                        java.lang.String r1 = "AdMob SDK"
                        java.lang.StringBuilder r3 = new java.lang.StringBuilder
                        r3.<init>()
                        java.lang.String r4 = "Could not determine final click destination URL.  Will try to follow anyway.  "
                        java.lang.StringBuilder r3 = r3.insert(r5, r4)
                        com.admob.android.ads.Ad r4 = com.admob.android.ads.Ad.this
                        java.lang.String r4 = r4.clickURL
                        java.lang.StringBuilder r3 = r3.append(r4)
                        java.lang.String r3 = r3.toString()
                        android.util.Log.w(r1, r3, r0)
                        r0 = r2
                        r1 = r2
                        goto L_0x007c
                    L_0x0113:
                        r0 = move-exception
                        java.lang.String r2 = "AdMob SDK"
                        java.lang.StringBuilder r3 = new java.lang.StringBuilder
                        r3.<init>()
                        java.lang.String r4 = "Could not open browser on ad click to "
                        java.lang.StringBuilder r3 = r3.insert(r5, r4)
                        java.lang.StringBuilder r1 = r3.append(r1)
                        java.lang.String r1 = r1.toString()
                        android.util.Log.e(r2, r1, r0)
                        r0 = r8
                        goto L_0x00bc
                    L_0x012e:
                        r0 = move-exception
                        goto L_0x00f1
                    L_0x0130:
                        r1 = move-exception
                        r2 = r0
                        r0 = r1
                        goto L_0x00f1
                    L_0x0134:
                        r0 = move-exception
                        r1 = r2
                        goto L_0x00cf
                    L_0x0137:
                        r1 = move-exception
                        r7 = r1
                        r1 = r0
                        r0 = r7
                        goto L_0x00cf
                    */
                    throw new UnsupportedOperationException("Method not decompiled: com.admob.android.ads.Ad.AnonymousClass1.run():void");
                }
            }.start();
        }
    }

    public boolean equals(Object obj) {
        if (obj instanceof Ad) {
            return toString().equals(((Ad) obj).toString());
        }
        return false;
    }

    public String getClickURL() {
        return this.clickURL;
    }

    public String getHTML() {
        return this.html;
    }

    public Bitmap getIcon() {
        if (this.icon == null) {
            this.icon = fetchImage(this.iconURL, true);
            if (this.icon == null) {
                Log.w("AdMob SDK", new StringBuilder().insert(0, "Could not get icon for ad from ").append(this.iconURL).toString());
            }
        }
        return this.icon;
    }

    public Bitmap getImage() {
        if (this.image == null && this.imageURL != null) {
            this.image = fetchImage(this.imageURL, false);
        }
        return this.image;
    }

    public int getImageHeight() {
        return this.image != null ? this.image.getHeight() : this.imageHeight;
    }

    public String getImageURL() {
        return this.imageURL;
    }

    public int getImageWidth() {
        return this.image != null ? this.image.getWidth() : this.imageWidth;
    }

    public NetworkListener getNetworkListener() {
        return this.networkListener;
    }

    public String getText() {
        return this.text;
    }

    public boolean hasImage() {
        return this.imageURL != null;
    }

    public int hashCode() {
        return toString().hashCode();
    }

    public void setNetworkListener(NetworkListener networkListener2) {
        this.networkListener = networkListener2;
    }

    public String toString() {
        String text2 = getText();
        return text2 == null ? "" : text2;
    }
}
