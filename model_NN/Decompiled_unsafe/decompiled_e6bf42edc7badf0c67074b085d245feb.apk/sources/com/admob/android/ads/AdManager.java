package com.admob.android.ads;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class AdManager {
    private static final long LOCATION_UPDATE_INTERVAL = 900000;
    static final String LOG = "AdMob SDK";
    static final String SDK_SITE_ID = "a1496ced2842262";
    public static final String SDK_VERSION = "20091123-ANDROID-3312276cc1406347";
    private static final String SDK_VERSION_CHECKSUM = "3312276cc1406347";
    static final String SDK_VERSION_DATE = "20091123";
    private static GregorianCalendar birthday;
    /* access modifiers changed from: private */
    public static Location coordinates;
    /* access modifiers changed from: private */
    public static long coordinatesTimestamp;
    private static Gender gender;
    private static String publisherId;
    private static boolean testMode;
    private static String userAgent;
    private static String userId;

    public enum Gender {
        MALE,
        FEMALE
    }

    static {
        Log.i(LOG, "AdMob SDK version is 20091123-ANDROID-3312276cc1406347");
    }

    protected static void clientError(String str) {
        Log.e(LOG, str);
        throw new IllegalArgumentException(str);
    }

    public static GregorianCalendar getBirthday() {
        return birthday;
    }

    static String getBirthdayAsString() {
        GregorianCalendar birthday2 = getBirthday();
        if (birthday2 == null) {
            return null;
        }
        return String.format("%04d%02d%02d", Integer.valueOf(birthday2.get(1)), Integer.valueOf(birthday2.get(2) + 1), Integer.valueOf(birthday2.get(5)));
    }

    public static Location getCoordinates(Context context) {
        Context context2;
        String str;
        String str2;
        boolean z;
        final LocationManager locationManager;
        LocationManager locationManager2 = null;
        boolean z2 = false;
        if (context != null && (coordinates == null || System.currentTimeMillis() > coordinatesTimestamp + LOCATION_UPDATE_INTERVAL)) {
            synchronized (context) {
                if (coordinates == null || System.currentTimeMillis() > coordinatesTimestamp + LOCATION_UPDATE_INTERVAL) {
                    coordinatesTimestamp = System.currentTimeMillis();
                    if (context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
                        if (Log.isLoggable(LOG, 3)) {
                            Log.d(LOG, "Trying to get locations from the network.");
                        }
                        LocationManager locationManager3 = (LocationManager) context.getSystemService("location");
                        if (locationManager3 != null) {
                            Criteria criteria = new Criteria();
                            criteria.setAccuracy(2);
                            criteria.setCostAllowed(false);
                            str = locationManager3.getBestProvider(criteria, true);
                            locationManager2 = locationManager3;
                            z2 = true;
                        } else {
                            str = null;
                            locationManager2 = locationManager3;
                            z2 = true;
                        }
                    } else {
                        str = null;
                    }
                    if (str == null && context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
                        if (Log.isLoggable(LOG, 3)) {
                            Log.d(LOG, "Trying to get locations from GPS.");
                        }
                        locationManager = (LocationManager) context.getSystemService("location");
                        if (locationManager != null) {
                            Criteria criteria2 = new Criteria();
                            criteria2.setAccuracy(1);
                            criteria2.setCostAllowed(false);
                            str2 = locationManager.getBestProvider(criteria2, true);
                            z = true;
                        } else {
                            str2 = str;
                            z = true;
                        }
                    } else {
                        str2 = str;
                        z = z2;
                        locationManager = locationManager2;
                    }
                    if (!z) {
                        if (Log.isLoggable(LOG, 3)) {
                            Log.d(LOG, "Cannot access user's location.  Permissions are not set.");
                        }
                    } else if (str2 != null) {
                        Log.i(LOG, "Location provider setup successfully.");
                        locationManager.requestLocationUpdates(str2, 0, 0.0f, new LocationListener() {
                            public void onLocationChanged(Location location) {
                                Location unused = AdManager.coordinates = location;
                                long unused2 = AdManager.coordinatesTimestamp = System.currentTimeMillis();
                                locationManager.removeUpdates(this);
                                Log.i(AdManager.LOG, new StringBuilder().insert(0, "Aquired location ").append(AdManager.coordinates.getLatitude()).append(",").append(AdManager.coordinates.getLongitude()).append(" at ").append(new Date(AdManager.coordinatesTimestamp).toString()).append(".").toString());
                            }

                            public void onProviderDisabled(String str) {
                            }

                            public void onProviderEnabled(String str) {
                            }

                            public void onStatusChanged(String str, int i, Bundle bundle) {
                            }
                        }, context.getMainLooper());
                        context2 = context;
                    } else if (Log.isLoggable(LOG, 3)) {
                        Log.d(LOG, "No location providers are available.  Ads will not be geotargeted.");
                        context2 = context;
                    }
                }
                context2 = context;
            }
        }
        return coordinates;
    }

    static String getCoordinatesAsString(Context context) {
        String str = null;
        Location coordinates2 = getCoordinates(context);
        if (coordinates2 != null) {
            str = coordinates2.getLatitude() + "," + coordinates2.getLongitude();
        }
        if (Log.isLoggable(LOG, 3)) {
            Log.d(LOG, new StringBuilder().insert(0, "User coordinates are ").append(str).toString());
        }
        return str;
    }

    public static Gender getGender() {
        return gender;
    }

    static String getGenderAsString() {
        if (gender == Gender.MALE) {
            return "m";
        }
        if (gender == Gender.FEMALE) {
            return "f";
        }
        return null;
    }

    public static String getPublisherId(Context context) {
        if (publisherId == null) {
            try {
                ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
                if (applicationInfo != null) {
                    String string = applicationInfo.metaData.getString("ADMOB_PUBLISHER_ID");
                    Log.d(LOG, new StringBuilder().insert(0, "Publisher ID read from AndroidManifest.xml is ").append(string).toString());
                    if (!string.equals(SDK_SITE_ID) || (!context.getPackageName().equals("com.admob.android.test") && !context.getPackageName().equals("com.example.admob.lunarlander"))) {
                        setPublisherId(string);
                    } else {
                        Log.i(LOG, "This is a sample application so allowing sample publisher ID.");
                        publisherId = string;
                    }
                }
            } catch (Exception e) {
                Log.e(LOG, "Could not read ADMOB_PUBLISHER_ID meta-data from AndroidManifest.xml.", e);
            }
        }
        return publisherId;
    }

    static String getUserAgent() {
        StringBuffer stringBuffer;
        if (userAgent == null) {
            StringBuffer stringBuffer2 = new StringBuffer();
            String str = Build.VERSION.RELEASE;
            if (str.length() > 0) {
                stringBuffer2.append(str);
                stringBuffer = stringBuffer2;
            } else {
                stringBuffer2.append("1.0");
                stringBuffer = stringBuffer2;
            }
            stringBuffer.append("; ");
            Locale locale = Locale.getDefault();
            String language = locale.getLanguage();
            if (language != null) {
                stringBuffer2.append(language.toLowerCase());
                String country = locale.getCountry();
                if (country != null) {
                    stringBuffer2.append("-");
                    stringBuffer2.append(country.toLowerCase());
                }
            } else {
                stringBuffer2.append("en");
            }
            String str2 = Build.MODEL;
            if (str2.length() > 0) {
                stringBuffer2.append("; ");
                stringBuffer2.append(str2);
            }
            String str3 = Build.ID;
            if (str3.length() > 0) {
                stringBuffer2.append(" Build/");
                stringBuffer2.append(str3);
            }
            userAgent = String.format("Mozilla/5.0 (Linux; U; Android %s) AppleWebKit/525.10+ (KHTML, like Gecko) Version/3.0.4 Mobile Safari/523.12.2 (AdMob-ANDROID-%s)", stringBuffer2, SDK_VERSION_DATE);
            if (Log.isLoggable(LOG, 3)) {
                Log.d(LOG, new StringBuilder().insert(0, "Phone's user-agent is:  ").append(userAgent).toString());
            }
        }
        return userAgent;
    }

    public static String getUserId(Context context) {
        if (userId == null) {
            userId = Settings.System.getString(context.getContentResolver(), "android_id");
            userId = md5(userId);
            Log.i(LOG, new StringBuilder().insert(0, "The user ID is ").append(userId).toString());
        }
        return userId;
    }

    public static boolean isInTestMode() {
        return testMode;
    }

    private static /* synthetic */ String md5(String str) {
        if (str == null || str.length() <= 0) {
            return null;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes(), 0, str.length());
            return String.format("%032X", new BigInteger(1, instance.digest()));
        } catch (Exception e) {
            Log.d(LOG, new StringBuilder().insert(0, "Could not generate hash of ").append(str).toString(), e);
            userId = userId.substring(0, 32);
            return null;
        }
    }

    public static void setBirthday(int i, int i2, int i3) {
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.set(i, i2 - 1, i3);
        setBirthday(gregorianCalendar);
    }

    public static void setBirthday(GregorianCalendar gregorianCalendar) {
        birthday = gregorianCalendar;
    }

    public static void setGender(Gender gender2) {
        gender = gender2;
    }

    public static void setInTestMode(boolean z) {
        testMode = z;
    }

    public static void setPublisherId(String str) {
        if (str == null || str.length() != 15) {
            clientError(new StringBuilder().insert(0, "SETUP ERROR:  Incorrect AdMob publisher ID.  Should 15 [a-f,0-9] characters:  ").append(publisherId).toString());
        }
        if (str.equalsIgnoreCase(SDK_SITE_ID)) {
            clientError("SETUP ERROR:  Cannot use the sample publisher ID (a1496ced2842262).  Yours is available on www.admob.com.");
        }
        Log.i(LOG, new StringBuilder().insert(0, "Publisher ID set to ").append(str).toString());
        publisherId = str;
    }
}
