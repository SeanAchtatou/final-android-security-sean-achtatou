package com.iPhand.FirstAid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import java.util.ArrayList;
import java.util.HashMap;

public class FirstAid extends Activity {
    private static final int MENU_ABOUT = 0;
    String[] Catalogs = {"常见急救", "急救须知", "户外意外急救", "内科", "外科", "五官科", "妇产科", "儿科", "皮肤科", "心理科"};
    int[] Icons = {R.drawable.icon_01, R.drawable.icon_02, R.drawable.icon_03, R.drawable.icon_04, R.drawable.icon_05, R.drawable.icon_06, R.drawable.icon_07, R.drawable.icon_08, R.drawable.icon_09, R.drawable.icon_10};
    private ImageView ImgFlash = null;
    ListView mListView;
    private AlphaAnimation myAnimation_Alpha = null;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.flashscreen);
        this.ImgFlash = (ImageView) findViewById(R.id.ivStart);
        this.myAnimation_Alpha = new AlphaAnimation(1.0f, 0.9f);
        this.myAnimation_Alpha.setDuration(2000);
        this.myAnimation_Alpha.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                FirstAid.this.setContentView((int) R.layout.dialogueclass_list);
                FirstAid.this.mListView = (ListView) FirstAid.this.findViewById(R.id.DialogueClassList);
                FirstAid.this.setListAdapter(FirstAid.this.Catalogs);
                FirstAid.this.mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                        Intent intent = new Intent(FirstAid.this, ClassesListView.class);
                        Bundle bundle = new Bundle();
                        bundle.putInt("Num", i);
                        bundle.putString("Title", FirstAid.this.Catalogs[i]);
                        intent.putExtras(bundle);
                        FirstAid.this.startActivity(intent);
                    }
                });
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 0, 1, "关于").setIcon((int) R.drawable.about);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        switch (menuItem.getItemId()) {
            case R.styleable.com_admob_android_ads_AdView_testing /*0*/:
                startActivity(new Intent(this, about.class));
                return true;
            default:
                return false;
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        this.ImgFlash.startAnimation(this.myAnimation_Alpha);
    }

    public void setListAdapter(String[] strArr) {
        int i = 0;
        try {
            ArrayList arrayList = new ArrayList();
            while (true) {
                int i2 = i;
                if (i >= strArr.length) {
                    this.mListView.setAdapter((ListAdapter) new SimpleAdapter(this, arrayList, R.layout.dialogueclass_list_itme, new String[]{"Image", "ItemTitle"}, new int[]{R.id.DialogueclassImage, R.id.DialogueclassTitle}));
                    return;
                }
                HashMap hashMap = new HashMap();
                hashMap.put("Image", Integer.valueOf(this.Icons[i2]));
                hashMap.put("ItemTitle", strArr[i2]);
                arrayList.add(hashMap);
                i = i2 + 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
