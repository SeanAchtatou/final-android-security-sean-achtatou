package com.soft.android.appinstaller.core;

import java.util.Comparator;

/* compiled from: Internals */
class PrefixComparator implements Comparator<PrefixNamePair> {
    PrefixComparator() {
    }

    public int compare(PrefixNamePair o1, PrefixNamePair o2) {
        if (o1.prefix.length() > o2.prefix.length()) {
            return -1;
        }
        if (o1.prefix.length() < o2.prefix.length()) {
            return 1;
        }
        return o1.prefix.compareTo(o2.prefix);
    }
}
