package com.soft.android.appinstaller.core;

/* compiled from: Internals */
class PrefixNamePair {
    String name;
    String prefix;

    public PrefixNamePair(String prefix2, String name2) {
        this.prefix = prefix2;
        this.name = name2;
    }

    public String getPrefix() {
        return this.prefix;
    }

    public String getName() {
        return this.name;
    }
}
