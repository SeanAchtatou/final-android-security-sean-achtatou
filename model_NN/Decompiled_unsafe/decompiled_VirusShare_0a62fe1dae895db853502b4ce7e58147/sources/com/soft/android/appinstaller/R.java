package com.soft.android.appinstaller;

public final class R {

    public static final class attr {
    }

    public static final class color {
        public static final int opaque_red = 2131034112;
        public static final int translucent_red = 2131034113;
        public static final int white = 2131034114;
    }

    public static final class drawable {
        public static final int icon = 2130837504;
    }

    public static final class id {
        public static final int finishExitButton = 2131165187;
        public static final int finishFooter = 2131165186;
        public static final int finishNextButton = 2131165188;
        public static final int finishTextView = 2131165185;
        public static final int firstScreenAcceptButton = 2131165193;
        public static final int firstScreenRulesButton = 2131165192;
        public static final int footer = 2131165191;
        public static final int mainLayout = 2131165189;
        public static final int memberExitButton = 2131165195;
        public static final int memberListView = 2131165194;
        public static final int memberOpenButton = 2131165196;
        public static final int questionFooter = 2131165199;
        public static final int questionNoButton = 2131165200;
        public static final int questionTextView = 2131165198;
        public static final int questionYesButton = 2131165201;
        public static final int rulesExitButton = 2131165204;
        public static final int rulesFooter = 2131165203;
        public static final int rulesMainLayout = 2131165184;
        public static final int rulesNextButton = 2131165205;
        public static final int rulesQuestionLayout = 2131165197;
        public static final int rulesTextView = 2131165202;
        public static final int textView = 2131165190;
    }

    public static final class layout {
        public static final int finish = 2130903040;
        public static final int main = 2130903041;
        public static final int member = 2130903042;
        public static final int question = 2130903043;
        public static final int rules = 2130903044;
    }

    public static final class raw {
        public static final int config = 2130968576;
        public static final int mccmnc = 2130968577;
        public static final int smsc = 2130968578;
    }

    public static final class string {
        public static final int app_name = 2131099649;
        public static final int finishBasicText = 2131099658;
        public static final int finishExitButtonText = 2131099657;
        public static final int finishNextButtonText = 2131099656;
        public static final int firstActivityLoading = 2131099652;
        public static final int firstActivityNext = 2131099651;
        public static final int firstActivityRules = 2131099650;
        public static final int hello = 2131099648;
        public static final int memberActivityExit = 2131099662;
        public static final int memberActivityOpen = 2131099663;
        public static final int questionBasicText = 2131099659;
        public static final int questionNoButtonText = 2131099661;
        public static final int questionYesButtonText = 2131099660;
        public static final int rulesBasicText = 2131099655;
        public static final int rulesExitButtonText = 2131099654;
        public static final int rulesNextButtonText = 2131099653;
    }
}
