package com.ptowngames.jigsaur;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Debug;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;

public final class b implements m {
    Activity a;

    public b(Activity activity) {
        this.a = activity;
    }

    public final OutputStream a(String str) {
        try {
            return this.a.openFileOutput(str, 1);
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    public final InputStream b(String str) {
        try {
            return this.a.openFileInput(str);
        } catch (FileNotFoundException e) {
            return null;
        }
    }

    public final void a(int i, final Runnable runnable, final Runnable runnable2) {
        final AlertDialog.Builder onCancelListener = new AlertDialog.Builder(this.a).setMessage(i).setPositiveButton(17039370, new DialogInterface.OnClickListener() {
            public final void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                if (runnable != null) {
                    runnable.run();
                }
            }
        }).setNegativeButton(17039360, new DialogInterface.OnClickListener() {
            public final void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                if (runnable2 != null) {
                    runnable2.run();
                }
            }
        }).setOnCancelListener(new DialogInterface.OnCancelListener() {
            public final void onCancel(DialogInterface dialogInterface) {
                dialogInterface.dismiss();
                if (runnable2 != null) {
                    runnable2.run();
                }
            }
        });
        this.a.runOnUiThread(new Runnable() {
            public final void run() {
                onCancelListener.create().show();
            }
        });
    }

    public final long a() {
        "--- N1: " + Debug.getNativeHeapAllocatedSize();
        "--- N2: " + Debug.getNativeHeapFreeSize();
        "--- N3: " + Debug.getNativeHeapSize();
        "--- V1: " + Runtime.getRuntime().totalMemory();
        "--- V2: " + Runtime.getRuntime().freeMemory();
        return Debug.getNativeHeapSize();
    }

    public final long b() {
        return Debug.getNativeHeapAllocatedSize();
    }

    public final void c() {
        this.a.finish();
    }

    public final String d() {
        return this.a.getResources().getConfiguration().locale.toString();
    }

    public final void e() {
        this.a.runOnUiThread(new Runnable() {
            public final void run() {
                b.this.a.openOptionsMenu();
            }
        });
    }
}
