package com.ptowngames.jigsaur;

import com.badlogic.gdx.g;
import com.ptowngames.jigsaur.Jigsaur;
import java.util.Hashtable;

public class ae {
    private static ae a = null;
    private static /* synthetic */ boolean c = (!ae.class.desiredAssertionStatus());
    private Hashtable<Integer, Jigsaur.PuzzleImageInfo> b = new Hashtable<>();

    public static ae a() {
        return a;
    }

    private ae() {
    }

    public final Jigsaur.PuzzleImageInfo a(int i) {
        return this.b.get(Integer.valueOf(i));
    }

    public static boolean a(String str) {
        try {
            Jigsaur.PuzzleImageInfos parseFrom = Jigsaur.PuzzleImageInfos.parseFrom(g.e.b(str).b());
            a = null;
            ae aeVar = new ae();
            a = aeVar;
            int puzzleImageInfosCount = parseFrom.getPuzzleImageInfosCount();
            int i = 0;
            while (i < puzzleImageInfosCount) {
                Jigsaur.PuzzleImageInfo puzzleImageInfos = parseFrom.getPuzzleImageInfos(i);
                if (c || !aeVar.b.containsKey(Integer.valueOf(puzzleImageInfos.getUid()))) {
                    aeVar.b.put(Integer.valueOf(puzzleImageInfos.getUid()), puzzleImageInfos);
                    i++;
                } else {
                    throw new AssertionError();
                }
            }
            return true;
        } catch (Exception e) {
            "Failed to load '" + str + "': " + e;
            return false;
        }
    }
}
