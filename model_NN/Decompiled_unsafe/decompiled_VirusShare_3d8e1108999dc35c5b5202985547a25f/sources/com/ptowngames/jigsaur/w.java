package com.ptowngames.jigsaur;

import com.badlogic.gdx.graphics.d;
import com.badlogic.gdx.graphics.i;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.f;
import com.badlogic.gdx.math.g;
import java.util.ArrayList;

public class w extends d {
    private static /* synthetic */ boolean K = (!w.class.desiredAssertionStatus());
    private int A = -1;
    private f B = null;
    private com.badlogic.gdx.math.a.a C;
    private com.badlogic.gdx.math.a.a D = new com.badlogic.gdx.math.a.a(new f(), new f());
    private float E = 0.0f;
    private f F = new f();
    private boolean G = false;
    private float H;
    private Matrix4 I;
    private f J;
    private float n;
    private float o = Float.POSITIVE_INFINITY;
    private float p = 0.0f;
    private boolean q;
    private ArrayList<a> r;
    private g s = new g(Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY);
    private g t = new g(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY);
    private boolean u = true;
    private boolean v = false;
    private f w = new f();
    private com.badlogic.gdx.math.a.a x;
    private com.badlogic.gdx.math.a.a y = new com.badlogic.gdx.math.a.a(new f(), new f());
    private int z = -1;

    interface a {
        void a(w wVar);
    }

    public final void a(float f, float f2) {
        this.s.a(f, f2);
    }

    public final void b(float f, float f2) {
        this.t.a(f, f2);
    }

    public final void a(boolean z2) {
        this.u = z2;
        this.q = z2;
    }

    public final g d() {
        return a(-5.0f, new g());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ptowngames.jigsaur.w.c(float, float):com.badlogic.gdx.math.a.a
     arg types: [int, int]
     candidates:
      com.ptowngames.jigsaur.w.c(int, int):void
      com.ptowngames.jigsaur.w.c(float, float):com.badlogic.gdx.math.a.a */
    public final g a(float f, g gVar) {
        gVar.a.a = Float.POSITIVE_INFINITY;
        gVar.a.b = Float.POSITIVE_INFINITY;
        gVar.b.a = Float.NEGATIVE_INFINITY;
        gVar.b.b = Float.NEGATIVE_INFINITY;
        float d = (float) com.badlogic.gdx.g.b.d();
        float e = (float) com.badlogic.gdx.g.b.e();
        u.a(c(0.0f, 0.0f), f, this.J);
        gVar.b(this.J.a, this.J.b);
        u.a(c(d, 0.0f), f, this.J);
        gVar.b(this.J.a, this.J.b);
        u.a(c(d, e), f, this.J);
        gVar.b(this.J.a, this.J.b);
        u.a(c(0.0f, e), f, this.J);
        gVar.b(this.J.a, this.J.b);
        return gVar;
    }

    public w(float f, float f2) {
        super(f);
        if (K || f2 > 0.0f) {
            this.n = f2;
            this.q = true;
            this.r = new ArrayList<>();
            return;
        }
        throw new AssertionError();
    }

    public final void a(a aVar) {
        this.r.add(aVar);
    }

    public final float e() {
        return this.o;
    }

    public final void a(float f) {
        this.o = f;
    }

    public final float f() {
        return this.p;
    }

    public final void b(float f) {
        this.p = f;
    }

    public final float g() {
        return this.n;
    }

    public final void c(float f) {
        if (K || f > 0.0f) {
            if (f > this.o) {
                f = this.o;
            }
            if (f < this.p) {
                f = this.p;
            }
            this.n = f;
            this.q = true;
            return;
        }
        throw new AssertionError();
    }

    public final void d(float f) {
        if (this.z < 0 || this.A < 0) {
            c(f);
        } else if (K || this.w != null) {
            a(f, this.z, this.A, this.w.c);
        } else {
            throw new AssertionError();
        }
    }

    public final void a(float f, int i, int i2, float f2) {
        if (K || f > 0.0f) {
            j();
            float d = ((float) com.badlogic.gdx.g.b.d()) / 2.0f;
            float e = ((float) com.badlogic.gdx.g.b.e()) / 2.0f;
            u.a(c((float) i, (float) i2), f2, this.J);
            float f3 = this.n / f;
            u.a(c(d + ((((float) i) - d) * f3), e + (f3 * (((float) i2) - e))), f2, this.F);
            this.a.a(this.J.c(this.F.c(this.a)));
            this.q = true;
            c(f);
            return;
        }
        throw new AssertionError();
    }

    public final void a(f fVar, float f) {
        if (K || fVar != this.a) {
            this.b.b(this.a);
            this.c.b(this.a);
            float cos = (float) Math.cos((double) f);
            float sin = (float) Math.sin((double) f);
            this.a.a -= fVar.a;
            this.a.b -= fVar.b;
            float f2 = (this.a.a * cos) - (this.a.b * sin);
            float f3 = (this.a.a * sin) + (this.a.b * cos);
            this.a.a = f2 + fVar.a;
            this.a.b = f3 + fVar.b;
            this.b.a -= fVar.a;
            this.b.b -= fVar.b;
            float f4 = (this.b.a * cos) - (this.b.b * sin);
            float f5 = (this.b.a * sin) + (this.b.b * cos);
            this.b.a = f4 + fVar.a;
            this.b.b = f5 + fVar.b;
            this.c.a -= fVar.a;
            this.c.b -= fVar.b;
            float f6 = (this.c.a * cos) - (this.c.b * sin);
            float f7 = (cos * this.c.b) + (sin * this.c.a);
            this.c.a = f6 + fVar.a;
            this.c.b = f7 + fVar.b;
            this.b.c(this.a);
            this.c.c(this.a);
            this.q = true;
            return;
        }
        throw new AssertionError();
    }

    public final float h() {
        if (this.c.a == this.c.b && this.c.a == 0.0f) {
            return 0.0f;
        }
        return (float) Math.atan2((double) (-this.c.a), (double) this.c.b);
    }

    public final void a(float f, float f2, float f3) {
        this.a.a(f, f2, f3);
        this.q = true;
    }

    public final void i() {
        this.q = true;
    }

    public final void j() {
        if (this.q) {
            this.q = false;
            c();
        }
    }

    public final void a(i iVar) {
        j();
        super.a(iVar);
    }

    public final void a(int i, int i2) {
        j();
        this.x = c((float) i, (float) i2);
        u.a(this.x, -5.0f, this.w);
        this.y.b.a(this.x.b).a(-1.0f);
        this.v = true;
        this.z = i;
        this.A = i2;
    }

    public final boolean k() {
        return this.v;
    }

    public final void b(int i, int i2) {
        if (K || this.v) {
            com.badlogic.gdx.math.a.a c = c((float) i, (float) i2);
            u.a(c, this.w.c, this.F);
            this.y.a.a(this.F);
            u.a(this.y, this.a.c, this.F);
            this.B = this.a.a().a(2.0f).c(this.F);
            this.q = true;
            this.C = c;
            this.D.b.a(this.C.b).a(-1.0f);
            this.z = i;
            this.A = i2;
            return;
        }
        throw new AssertionError();
    }

    public final void c(int i, int i2) {
        if (K || this.v) {
            j();
            u.a(c((float) i, (float) i2), this.w.c, this.F);
            this.H = (float) Math.atan2((double) (this.F.b - this.w.b), (double) (this.F.a - this.w.a));
            this.G = true;
            return;
        }
        throw new AssertionError();
    }

    public final void d(int i, int i2) {
        if (!K && !this.v) {
            throw new AssertionError();
        } else if (K || this.G) {
            u.a(c((float) i, (float) i2), this.w.c, this.F);
            this.E = ((float) Math.atan2((double) (this.F.b - this.w.b), (double) (this.F.a - this.w.a))) - this.H;
            this.q = true;
        } else {
            throw new AssertionError();
        }
    }

    public final void c() {
        if (this.J == null) {
            this.I = new Matrix4();
            this.J = new f();
        }
        if (this.v) {
            if (this.B != null) {
                this.a.a(this.B);
                this.B = null;
                if (this.E == 0.0f) {
                    this.x = this.C;
                    this.y.b.a(this.D.b);
                }
            }
            if (this.E != 0.0f) {
                a(this.w, -this.E);
                this.E = 0.0f;
                this.x.a.a(this.a);
                this.x.b.a(this.w.a().c(this.a));
                this.y.b.a(this.x.b).a(-1.0f);
            }
        }
        if (this.s != null && this.u) {
            if (!K && this.s.a >= this.t.a) {
                throw new AssertionError();
            } else if (K || this.s.b < this.t.b) {
                if (this.a.a < this.s.a) {
                    this.a.a = this.s.a;
                } else if (this.a.a > this.t.a) {
                    this.a.a = this.t.a;
                }
                if (this.a.b < this.s.b) {
                    this.a.b = this.s.b;
                } else if (this.a.b > this.t.b) {
                    this.a.b = this.t.b;
                }
            } else {
                throw new AssertionError();
            }
        }
        this.d.a(Math.abs(this.h), Math.abs(this.i), this.m, this.j / this.k);
        this.e.c(this.n, this.n);
        this.e.b(this.I.a(this.a, this.J.a(this.a).b(this.b), this.c));
        this.f.a(this.d);
        Matrix4.mul(this.f.a, this.e.a);
        this.g.a(this.f);
        Matrix4.inv(this.g.a);
        this.l.a(this.g);
        if (this.r != null) {
            int size = this.r.size();
            while (true) {
                int i = size - 1;
                if (i < 0) {
                    break;
                }
                this.r.get(i).a(this);
                size = i;
            }
        }
        this.q = false;
    }

    public final com.badlogic.gdx.math.a.a c(float f, float f2) {
        com.badlogic.gdx.math.a.a aVar = new com.badlogic.gdx.math.a.a(new f(this.a.a, this.a.b, this.a.c), new f(0.0f, 0.0f, 0.0f));
        aVar.b.a(f, f2, 1.0f);
        a(aVar.b, (float) com.badlogic.gdx.g.b.d(), (float) com.badlogic.gdx.g.b.e());
        aVar.b.c(aVar.a);
        if (aVar.b.c >= 0.0f) {
            System.err.println("ERROR: safe downward Z went into effect");
            aVar.b.c = -1.0E-12f;
        }
        return aVar;
    }
}
