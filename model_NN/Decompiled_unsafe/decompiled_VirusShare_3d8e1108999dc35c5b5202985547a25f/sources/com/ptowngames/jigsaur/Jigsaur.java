package com.ptowngames.jigsaur;

import com.badlogic.gdx.physics.box2d.Transform;
import com.google.protobuf.DescriptorProtos;
import com.google.protobuf.GeneratedMessage;
import com.google.protobuf.a;
import com.google.protobuf.ab;
import com.google.protobuf.ac;
import com.google.protobuf.c;
import com.google.protobuf.g;
import com.google.protobuf.h;
import com.google.protobuf.q;
import com.google.protobuf.s;
import com.google.protobuf.v;
import com.google.protobuf.x;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class Jigsaur {
    /* access modifiers changed from: private */
    public static c.a A;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable B;
    /* access modifiers changed from: private */
    public static c.a C;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable D;
    /* access modifiers changed from: private */
    public static c.a E;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable F;
    /* access modifiers changed from: private */
    public static c.a G;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable H;
    /* access modifiers changed from: private */
    public static c.a I;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable J;
    /* access modifiers changed from: private */
    public static c.a K;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable L;
    /* access modifiers changed from: private */
    public static c.b M;
    /* access modifiers changed from: private */
    public static c.a a;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable b;
    /* access modifiers changed from: private */
    public static c.a c;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable d;
    /* access modifiers changed from: private */
    public static c.a e;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable f;
    /* access modifiers changed from: private */
    public static c.a g;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable h;
    /* access modifiers changed from: private */
    public static c.a i;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable j;
    /* access modifiers changed from: private */
    public static c.a k;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable l;
    /* access modifiers changed from: private */
    public static c.a m;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable n;
    /* access modifiers changed from: private */
    public static c.a o;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable p;
    /* access modifiers changed from: private */
    public static c.a q;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable r;
    /* access modifiers changed from: private */
    public static c.a s;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable t;
    /* access modifiers changed from: private */
    public static c.a u;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable v;
    /* access modifiers changed from: private */
    public static c.a w;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable x;
    /* access modifiers changed from: private */
    public static c.a y;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable z;

    private Jigsaur() {
    }

    public static final class PolygonVertex extends GeneratedMessage {
        public static final int X_FIELD_NUMBER = 1;
        public static final int Y_FIELD_NUMBER = 2;
        private static final PolygonVertex defaultInstance = new PolygonVertex(true);
        /* access modifiers changed from: private */
        public boolean hasX;
        /* access modifiers changed from: private */
        public boolean hasY;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public float x_;
        /* access modifiers changed from: private */
        public float y_;

        /* synthetic */ PolygonVertex(ai aiVar) {
            this();
        }

        private PolygonVertex() {
            this.x_ = 0.0f;
            this.y_ = 0.0f;
            this.memoizedSerializedSize = -1;
        }

        private PolygonVertex(boolean z) {
            this.x_ = 0.0f;
            this.y_ = 0.0f;
            this.memoizedSerializedSize = -1;
        }

        public static PolygonVertex getDefaultInstance() {
            return defaultInstance;
        }

        public final PolygonVertex getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Jigsaur.a;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Jigsaur.b;
        }

        public final boolean hasX() {
            return this.hasX;
        }

        public final float getX() {
            return this.x_;
        }

        public final boolean hasY() {
            return this.hasY;
        }

        public final float getY() {
            return this.y_;
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            if (this.hasX && this.hasY) {
                return true;
            }
            return false;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasX()) {
                xVar.a(1, getX());
            }
            if (hasY()) {
                xVar.a(2, getY());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (hasX()) {
                getX();
                i2 = x.d(1) + 0;
            }
            if (hasY()) {
                getY();
                i2 += x.d(2);
            }
            int serializedSize = i2 + getUnknownFields().getSerializedSize();
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static PolygonVertex parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PolygonVertex.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PolygonVertex.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PolygonVertex.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PolygonVertex.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.PolygonVertex.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PolygonVertex$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static PolygonVertex parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static PolygonVertex parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PolygonVertex.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PolygonVertex.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PolygonVertex.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PolygonVertex.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.PolygonVertex.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PolygonVertex$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static PolygonVertex parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static PolygonVertex parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PolygonVertex.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PolygonVertex.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PolygonVertex.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PolygonVertex.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.PolygonVertex.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PolygonVertex$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static PolygonVertex parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static PolygonVertex parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static PolygonVertex parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static PolygonVertex parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.jigsaur.Jigsaur.PolygonVertex.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PolygonVertex$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PolygonVertex.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PolygonVertex.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PolygonVertex.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PolygonVertex.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PolygonVertex.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PolygonVertex$Builder */
        public static PolygonVertex parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(PolygonVertex polygonVertex) {
            return newBuilder().mergeFrom(polygonVertex);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private PolygonVertex result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new PolygonVertex((ai) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final PolygonVertex internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new PolygonVertex((ai) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return PolygonVertex.getDescriptor();
            }

            public final PolygonVertex getDefaultInstanceForType() {
                return PolygonVertex.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final PolygonVertex build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public PolygonVertex buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final PolygonVertex buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                PolygonVertex polygonVertex = this.result;
                this.result = null;
                return polygonVertex;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof PolygonVertex) {
                    return mergeFrom((PolygonVertex) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(PolygonVertex polygonVertex) {
                if (polygonVertex != PolygonVertex.getDefaultInstance()) {
                    if (polygonVertex.hasX()) {
                        setX(polygonVertex.getX());
                    }
                    if (polygonVertex.hasY()) {
                        setY(polygonVertex.getY());
                    }
                    mergeUnknownFields(polygonVertex.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 13:
                            setX(hVar.c());
                            break;
                        case 21:
                            setY(hVar.c());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasX() {
                return this.result.hasX();
            }

            public final float getX() {
                return this.result.getX();
            }

            public final Builder setX(float f) {
                boolean unused = this.result.hasX = true;
                float unused2 = this.result.x_ = f;
                return this;
            }

            public final Builder clearX() {
                boolean unused = this.result.hasX = false;
                float unused2 = this.result.x_ = 0.0f;
                return this;
            }

            public final boolean hasY() {
                return this.result.hasY();
            }

            public final float getY() {
                return this.result.getY();
            }

            public final Builder setY(float f) {
                boolean unused = this.result.hasY = true;
                float unused2 = this.result.y_ = f;
                return this;
            }

            public final Builder clearY() {
                boolean unused = this.result.hasY = false;
                float unused2 = this.result.y_ = 0.0f;
                return this;
            }
        }
    }

    public static final class Vertex3 extends GeneratedMessage {
        public static final int X_FIELD_NUMBER = 1;
        public static final int Y_FIELD_NUMBER = 2;
        public static final int Z_FIELD_NUMBER = 3;
        private static final Vertex3 defaultInstance = new Vertex3(true);
        /* access modifiers changed from: private */
        public boolean hasX;
        /* access modifiers changed from: private */
        public boolean hasY;
        /* access modifiers changed from: private */
        public boolean hasZ;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public float x_;
        /* access modifiers changed from: private */
        public float y_;
        /* access modifiers changed from: private */
        public float z_;

        /* synthetic */ Vertex3(ai aiVar) {
            this();
        }

        private Vertex3() {
            this.x_ = 0.0f;
            this.y_ = 0.0f;
            this.z_ = 0.0f;
            this.memoizedSerializedSize = -1;
        }

        private Vertex3(boolean z) {
            this.x_ = 0.0f;
            this.y_ = 0.0f;
            this.z_ = 0.0f;
            this.memoizedSerializedSize = -1;
        }

        public static Vertex3 getDefaultInstance() {
            return defaultInstance;
        }

        public final Vertex3 getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Jigsaur.c;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Jigsaur.d;
        }

        public final boolean hasX() {
            return this.hasX;
        }

        public final float getX() {
            return this.x_;
        }

        public final boolean hasY() {
            return this.hasY;
        }

        public final float getY() {
            return this.y_;
        }

        public final boolean hasZ() {
            return this.hasZ;
        }

        public final float getZ() {
            return this.z_;
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            if (this.hasX && this.hasY && this.hasZ) {
                return true;
            }
            return false;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasX()) {
                xVar.a(1, getX());
            }
            if (hasY()) {
                xVar.a(2, getY());
            }
            if (hasZ()) {
                xVar.a(3, getZ());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (hasX()) {
                getX();
                i2 = x.d(1) + 0;
            }
            if (hasY()) {
                getY();
                i2 += x.d(2);
            }
            if (hasZ()) {
                getZ();
                i2 += x.d(3);
            }
            int serializedSize = i2 + getUnknownFields().getSerializedSize();
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static Vertex3 parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.Vertex3.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.Vertex3.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.Vertex3.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.Vertex3.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.Vertex3.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$Vertex3$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static Vertex3 parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static Vertex3 parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.Vertex3.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.Vertex3.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.Vertex3.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.Vertex3.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.Vertex3.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$Vertex3$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static Vertex3 parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static Vertex3 parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.Vertex3.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.Vertex3.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.Vertex3.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.Vertex3.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.Vertex3.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$Vertex3$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static Vertex3 parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static Vertex3 parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static Vertex3 parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static Vertex3 parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.jigsaur.Jigsaur.Vertex3.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$Vertex3$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.Vertex3.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.Vertex3.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.Vertex3.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.Vertex3.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.Vertex3.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$Vertex3$Builder */
        public static Vertex3 parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(Vertex3 vertex3) {
            return newBuilder().mergeFrom(vertex3);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private Vertex3 result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new Vertex3((ai) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final Vertex3 internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new Vertex3((ai) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return Vertex3.getDescriptor();
            }

            public final Vertex3 getDefaultInstanceForType() {
                return Vertex3.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final Vertex3 build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public Vertex3 buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final Vertex3 buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                Vertex3 vertex3 = this.result;
                this.result = null;
                return vertex3;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof Vertex3) {
                    return mergeFrom((Vertex3) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(Vertex3 vertex3) {
                if (vertex3 != Vertex3.getDefaultInstance()) {
                    if (vertex3.hasX()) {
                        setX(vertex3.getX());
                    }
                    if (vertex3.hasY()) {
                        setY(vertex3.getY());
                    }
                    if (vertex3.hasZ()) {
                        setZ(vertex3.getZ());
                    }
                    mergeUnknownFields(vertex3.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 13:
                            setX(Float.intBitsToFloat(hVar.p()));
                            break;
                        case 21:
                            setY(Float.intBitsToFloat(hVar.p()));
                            break;
                        case 29:
                            setZ(Float.intBitsToFloat(hVar.p()));
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasX() {
                return this.result.hasX();
            }

            public final float getX() {
                return this.result.getX();
            }

            public final Builder setX(float f) {
                boolean unused = this.result.hasX = true;
                float unused2 = this.result.x_ = f;
                return this;
            }

            public final Builder clearX() {
                boolean unused = this.result.hasX = false;
                float unused2 = this.result.x_ = 0.0f;
                return this;
            }

            public final boolean hasY() {
                return this.result.hasY();
            }

            public final float getY() {
                return this.result.getY();
            }

            public final Builder setY(float f) {
                boolean unused = this.result.hasY = true;
                float unused2 = this.result.y_ = f;
                return this;
            }

            public final Builder clearY() {
                boolean unused = this.result.hasY = false;
                float unused2 = this.result.y_ = 0.0f;
                return this;
            }

            public final boolean hasZ() {
                return this.result.hasZ();
            }

            public final float getZ() {
                return this.result.getZ();
            }

            public final Builder setZ(float f) {
                boolean unused = this.result.hasZ = true;
                float unused2 = this.result.z_ = f;
                return this;
            }

            public final Builder clearZ() {
                boolean unused = this.result.hasZ = false;
                float unused2 = this.result.z_ = 0.0f;
                return this;
            }
        }
    }

    public static final class JoinCorner extends GeneratedMessage {
        public static final int EAST_JOIN_PRIORITY_FIELD_NUMBER = 5;
        public static final int NORTH_JOIN_PRIORITY_FIELD_NUMBER = 4;
        public static final int SCALE_FIELD_NUMBER = 3;
        public static final int SOUTH_JOIN_PRIORITY_FIELD_NUMBER = 6;
        public static final int WEST_JOIN_PRIORITY_FIELD_NUMBER = 7;
        public static final int XOFF_FIELD_NUMBER = 1;
        public static final int YOFF_FIELD_NUMBER = 2;
        private static final JoinCorner defaultInstance = new JoinCorner(true);
        /* access modifiers changed from: private */
        public int eastJoinPriority_;
        /* access modifiers changed from: private */
        public boolean hasEastJoinPriority;
        /* access modifiers changed from: private */
        public boolean hasNorthJoinPriority;
        /* access modifiers changed from: private */
        public boolean hasScale;
        /* access modifiers changed from: private */
        public boolean hasSouthJoinPriority;
        /* access modifiers changed from: private */
        public boolean hasWestJoinPriority;
        /* access modifiers changed from: private */
        public boolean hasXoff;
        /* access modifiers changed from: private */
        public boolean hasYoff;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public int northJoinPriority_;
        /* access modifiers changed from: private */
        public float scale_;
        /* access modifiers changed from: private */
        public int southJoinPriority_;
        /* access modifiers changed from: private */
        public int westJoinPriority_;
        /* access modifiers changed from: private */
        public float xoff_;
        /* access modifiers changed from: private */
        public float yoff_;

        /* synthetic */ JoinCorner(ai aiVar) {
            this();
        }

        private JoinCorner() {
            this.xoff_ = 0.0f;
            this.yoff_ = 0.0f;
            this.scale_ = 0.0f;
            this.northJoinPriority_ = 0;
            this.eastJoinPriority_ = 0;
            this.southJoinPriority_ = 0;
            this.westJoinPriority_ = 0;
            this.memoizedSerializedSize = -1;
        }

        private JoinCorner(boolean z) {
            this.xoff_ = 0.0f;
            this.yoff_ = 0.0f;
            this.scale_ = 0.0f;
            this.northJoinPriority_ = 0;
            this.eastJoinPriority_ = 0;
            this.southJoinPriority_ = 0;
            this.westJoinPriority_ = 0;
            this.memoizedSerializedSize = -1;
        }

        public static JoinCorner getDefaultInstance() {
            return defaultInstance;
        }

        public final JoinCorner getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Jigsaur.e;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Jigsaur.f;
        }

        public final boolean hasXoff() {
            return this.hasXoff;
        }

        public final float getXoff() {
            return this.xoff_;
        }

        public final boolean hasYoff() {
            return this.hasYoff;
        }

        public final float getYoff() {
            return this.yoff_;
        }

        public final boolean hasScale() {
            return this.hasScale;
        }

        public final float getScale() {
            return this.scale_;
        }

        public final boolean hasNorthJoinPriority() {
            return this.hasNorthJoinPriority;
        }

        public final int getNorthJoinPriority() {
            return this.northJoinPriority_;
        }

        public final boolean hasEastJoinPriority() {
            return this.hasEastJoinPriority;
        }

        public final int getEastJoinPriority() {
            return this.eastJoinPriority_;
        }

        public final boolean hasSouthJoinPriority() {
            return this.hasSouthJoinPriority;
        }

        public final int getSouthJoinPriority() {
            return this.southJoinPriority_;
        }

        public final boolean hasWestJoinPriority() {
            return this.hasWestJoinPriority;
        }

        public final int getWestJoinPriority() {
            return this.westJoinPriority_;
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            if (this.hasXoff && this.hasYoff && this.hasScale && this.hasNorthJoinPriority && this.hasEastJoinPriority && this.hasSouthJoinPriority && this.hasWestJoinPriority) {
                return true;
            }
            return false;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasXoff()) {
                xVar.a(1, getXoff());
            }
            if (hasYoff()) {
                xVar.a(2, getYoff());
            }
            if (hasScale()) {
                xVar.a(3, getScale());
            }
            if (hasNorthJoinPriority()) {
                xVar.b(4, getNorthJoinPriority());
            }
            if (hasEastJoinPriority()) {
                xVar.b(5, getEastJoinPriority());
            }
            if (hasSouthJoinPriority()) {
                xVar.b(6, getSouthJoinPriority());
            }
            if (hasWestJoinPriority()) {
                xVar.b(7, getWestJoinPriority());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (hasXoff()) {
                getXoff();
                i2 = x.d(1) + 0;
            }
            if (hasYoff()) {
                getYoff();
                i2 += x.d(2);
            }
            if (hasScale()) {
                getScale();
                i2 += x.d(3);
            }
            if (hasNorthJoinPriority()) {
                i2 += x.e(4, getNorthJoinPriority());
            }
            if (hasEastJoinPriority()) {
                i2 += x.e(5, getEastJoinPriority());
            }
            if (hasSouthJoinPriority()) {
                i2 += x.e(6, getSouthJoinPriority());
            }
            if (hasWestJoinPriority()) {
                i2 += x.e(7, getWestJoinPriority());
            }
            int serializedSize = i2 + getUnknownFields().getSerializedSize();
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static JoinCorner parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.JoinCorner.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.JoinCorner.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.JoinCorner.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.JoinCorner.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.JoinCorner.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$JoinCorner$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static JoinCorner parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static JoinCorner parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.JoinCorner.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.JoinCorner.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.JoinCorner.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.JoinCorner.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.JoinCorner.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$JoinCorner$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static JoinCorner parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static JoinCorner parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.JoinCorner.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.JoinCorner.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.JoinCorner.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.JoinCorner.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.JoinCorner.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$JoinCorner$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static JoinCorner parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static JoinCorner parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static JoinCorner parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static JoinCorner parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.jigsaur.Jigsaur.JoinCorner.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$JoinCorner$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.JoinCorner.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.JoinCorner.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.JoinCorner.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.JoinCorner.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.JoinCorner.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$JoinCorner$Builder */
        public static JoinCorner parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(JoinCorner joinCorner) {
            return newBuilder().mergeFrom(joinCorner);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private JoinCorner result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new JoinCorner((ai) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final JoinCorner internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new JoinCorner((ai) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return JoinCorner.getDescriptor();
            }

            public final JoinCorner getDefaultInstanceForType() {
                return JoinCorner.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final JoinCorner build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public JoinCorner buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final JoinCorner buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                JoinCorner joinCorner = this.result;
                this.result = null;
                return joinCorner;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof JoinCorner) {
                    return mergeFrom((JoinCorner) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(JoinCorner joinCorner) {
                if (joinCorner != JoinCorner.getDefaultInstance()) {
                    if (joinCorner.hasXoff()) {
                        setXoff(joinCorner.getXoff());
                    }
                    if (joinCorner.hasYoff()) {
                        setYoff(joinCorner.getYoff());
                    }
                    if (joinCorner.hasScale()) {
                        setScale(joinCorner.getScale());
                    }
                    if (joinCorner.hasNorthJoinPriority()) {
                        setNorthJoinPriority(joinCorner.getNorthJoinPriority());
                    }
                    if (joinCorner.hasEastJoinPriority()) {
                        setEastJoinPriority(joinCorner.getEastJoinPriority());
                    }
                    if (joinCorner.hasSouthJoinPriority()) {
                        setSouthJoinPriority(joinCorner.getSouthJoinPriority());
                    }
                    if (joinCorner.hasWestJoinPriority()) {
                        setWestJoinPriority(joinCorner.getWestJoinPriority());
                    }
                    mergeUnknownFields(joinCorner.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 13:
                            setXoff(hVar.c());
                            break;
                        case 21:
                            setYoff(hVar.c());
                            break;
                        case 29:
                            setScale(hVar.c());
                            break;
                        case 32:
                            setNorthJoinPriority(hVar.l());
                            break;
                        case 40:
                            setEastJoinPriority(hVar.l());
                            break;
                        case 48:
                            setSouthJoinPriority(hVar.l());
                            break;
                        case 56:
                            setWestJoinPriority(hVar.l());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasXoff() {
                return this.result.hasXoff();
            }

            public final float getXoff() {
                return this.result.getXoff();
            }

            public final Builder setXoff(float f) {
                boolean unused = this.result.hasXoff = true;
                float unused2 = this.result.xoff_ = f;
                return this;
            }

            public final Builder clearXoff() {
                boolean unused = this.result.hasXoff = false;
                float unused2 = this.result.xoff_ = 0.0f;
                return this;
            }

            public final boolean hasYoff() {
                return this.result.hasYoff();
            }

            public final float getYoff() {
                return this.result.getYoff();
            }

            public final Builder setYoff(float f) {
                boolean unused = this.result.hasYoff = true;
                float unused2 = this.result.yoff_ = f;
                return this;
            }

            public final Builder clearYoff() {
                boolean unused = this.result.hasYoff = false;
                float unused2 = this.result.yoff_ = 0.0f;
                return this;
            }

            public final boolean hasScale() {
                return this.result.hasScale();
            }

            public final float getScale() {
                return this.result.getScale();
            }

            public final Builder setScale(float f) {
                boolean unused = this.result.hasScale = true;
                float unused2 = this.result.scale_ = f;
                return this;
            }

            public final Builder clearScale() {
                boolean unused = this.result.hasScale = false;
                float unused2 = this.result.scale_ = 0.0f;
                return this;
            }

            public final boolean hasNorthJoinPriority() {
                return this.result.hasNorthJoinPriority();
            }

            public final int getNorthJoinPriority() {
                return this.result.getNorthJoinPriority();
            }

            public final Builder setNorthJoinPriority(int i) {
                boolean unused = this.result.hasNorthJoinPriority = true;
                int unused2 = this.result.northJoinPriority_ = i;
                return this;
            }

            public final Builder clearNorthJoinPriority() {
                boolean unused = this.result.hasNorthJoinPriority = false;
                int unused2 = this.result.northJoinPriority_ = 0;
                return this;
            }

            public final boolean hasEastJoinPriority() {
                return this.result.hasEastJoinPriority();
            }

            public final int getEastJoinPriority() {
                return this.result.getEastJoinPriority();
            }

            public final Builder setEastJoinPriority(int i) {
                boolean unused = this.result.hasEastJoinPriority = true;
                int unused2 = this.result.eastJoinPriority_ = i;
                return this;
            }

            public final Builder clearEastJoinPriority() {
                boolean unused = this.result.hasEastJoinPriority = false;
                int unused2 = this.result.eastJoinPriority_ = 0;
                return this;
            }

            public final boolean hasSouthJoinPriority() {
                return this.result.hasSouthJoinPriority();
            }

            public final int getSouthJoinPriority() {
                return this.result.getSouthJoinPriority();
            }

            public final Builder setSouthJoinPriority(int i) {
                boolean unused = this.result.hasSouthJoinPriority = true;
                int unused2 = this.result.southJoinPriority_ = i;
                return this;
            }

            public final Builder clearSouthJoinPriority() {
                boolean unused = this.result.hasSouthJoinPriority = false;
                int unused2 = this.result.southJoinPriority_ = 0;
                return this;
            }

            public final boolean hasWestJoinPriority() {
                return this.result.hasWestJoinPriority();
            }

            public final int getWestJoinPriority() {
                return this.result.getWestJoinPriority();
            }

            public final Builder setWestJoinPriority(int i) {
                boolean unused = this.result.hasWestJoinPriority = true;
                int unused2 = this.result.westJoinPriority_ = i;
                return this;
            }

            public final Builder clearWestJoinPriority() {
                boolean unused = this.result.hasWestJoinPriority = false;
                int unused2 = this.result.westJoinPriority_ = 0;
                return this;
            }
        }
    }

    public static final class JoinInfo extends GeneratedMessage {
        public static final int BASE_ORIENTATION_FIELD_NUMBER = 2;
        public static final int JOIN_CIRCLE_CENTER_FIELD_NUMBER = 1;
        public static final int NEIGHBOR_ID_FIELD_NUMBER = 3;
        public static final int OUTTIE_STATUS_FIELD_NUMBER = 4;
        private static final JoinInfo defaultInstance;
        /* access modifiers changed from: private */
        public float baseOrientation_;
        /* access modifiers changed from: private */
        public boolean hasBaseOrientation;
        /* access modifiers changed from: private */
        public boolean hasJoinCircleCenter;
        /* access modifiers changed from: private */
        public boolean hasNeighborId;
        /* access modifiers changed from: private */
        public boolean hasOuttieStatus;
        /* access modifiers changed from: private */
        public PolygonVertex joinCircleCenter_;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public int neighborId_;
        /* access modifiers changed from: private */
        public boolean outtieStatus_;

        /* synthetic */ JoinInfo(ai aiVar) {
            this();
        }

        private JoinInfo() {
            this.baseOrientation_ = 0.0f;
            this.neighborId_ = 0;
            this.outtieStatus_ = false;
            this.memoizedSerializedSize = -1;
            this.joinCircleCenter_ = PolygonVertex.getDefaultInstance();
        }

        private JoinInfo(boolean z) {
            this.baseOrientation_ = 0.0f;
            this.neighborId_ = 0;
            this.outtieStatus_ = false;
            this.memoizedSerializedSize = -1;
        }

        public static JoinInfo getDefaultInstance() {
            return defaultInstance;
        }

        public final JoinInfo getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Jigsaur.g;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Jigsaur.h;
        }

        public final boolean hasJoinCircleCenter() {
            return this.hasJoinCircleCenter;
        }

        public final PolygonVertex getJoinCircleCenter() {
            return this.joinCircleCenter_;
        }

        public final boolean hasBaseOrientation() {
            return this.hasBaseOrientation;
        }

        public final float getBaseOrientation() {
            return this.baseOrientation_;
        }

        public final boolean hasNeighborId() {
            return this.hasNeighborId;
        }

        public final int getNeighborId() {
            return this.neighborId_;
        }

        public final boolean hasOuttieStatus() {
            return this.hasOuttieStatus;
        }

        public final boolean getOuttieStatus() {
            return this.outtieStatus_;
        }

        private void initFields() {
            this.joinCircleCenter_ = PolygonVertex.getDefaultInstance();
        }

        public final boolean isInitialized() {
            if (this.hasJoinCircleCenter && this.hasBaseOrientation && this.hasNeighborId && this.hasOuttieStatus && getJoinCircleCenter().isInitialized()) {
                return true;
            }
            return false;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasJoinCircleCenter()) {
                xVar.b(1, getJoinCircleCenter());
            }
            if (hasBaseOrientation()) {
                xVar.a(2, getBaseOrientation());
            }
            if (hasNeighborId()) {
                xVar.b(3, getNeighborId());
            }
            if (hasOuttieStatus()) {
                xVar.a(4, getOuttieStatus());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (hasJoinCircleCenter()) {
                i2 = x.d(1, getJoinCircleCenter()) + 0;
            }
            if (hasBaseOrientation()) {
                getBaseOrientation();
                i2 += x.d(2);
            }
            if (hasNeighborId()) {
                i2 += x.e(3, getNeighborId());
            }
            if (hasOuttieStatus()) {
                getOuttieStatus();
                i2 += x.e(4);
            }
            int serializedSize = i2 + getUnknownFields().getSerializedSize();
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static JoinInfo parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.JoinInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.JoinInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.JoinInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.JoinInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.JoinInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$JoinInfo$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static JoinInfo parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static JoinInfo parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.JoinInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.JoinInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.JoinInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.JoinInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.JoinInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$JoinInfo$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static JoinInfo parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static JoinInfo parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.JoinInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.JoinInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.JoinInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.JoinInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.JoinInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$JoinInfo$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static JoinInfo parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static JoinInfo parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static JoinInfo parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static JoinInfo parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.jigsaur.Jigsaur.JoinInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$JoinInfo$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.JoinInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.JoinInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.JoinInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.JoinInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.JoinInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$JoinInfo$Builder */
        public static JoinInfo parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(JoinInfo joinInfo) {
            return newBuilder().mergeFrom(joinInfo);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private JoinInfo result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new JoinInfo((ai) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final JoinInfo internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new JoinInfo((ai) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return JoinInfo.getDescriptor();
            }

            public final JoinInfo getDefaultInstanceForType() {
                return JoinInfo.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final JoinInfo build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public JoinInfo buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final JoinInfo buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                JoinInfo joinInfo = this.result;
                this.result = null;
                return joinInfo;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof JoinInfo) {
                    return mergeFrom((JoinInfo) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(JoinInfo joinInfo) {
                if (joinInfo != JoinInfo.getDefaultInstance()) {
                    if (joinInfo.hasJoinCircleCenter()) {
                        mergeJoinCircleCenter(joinInfo.getJoinCircleCenter());
                    }
                    if (joinInfo.hasBaseOrientation()) {
                        setBaseOrientation(joinInfo.getBaseOrientation());
                    }
                    if (joinInfo.hasNeighborId()) {
                        setNeighborId(joinInfo.getNeighborId());
                    }
                    if (joinInfo.hasOuttieStatus()) {
                        setOuttieStatus(joinInfo.getOuttieStatus());
                    }
                    mergeUnknownFields(joinInfo.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 10:
                            PolygonVertex.Builder newBuilder = PolygonVertex.newBuilder();
                            if (hasJoinCircleCenter()) {
                                newBuilder.mergeFrom(getJoinCircleCenter());
                            }
                            hVar.a(newBuilder, abVar);
                            setJoinCircleCenter(newBuilder.buildPartial());
                            break;
                        case 21:
                            setBaseOrientation(hVar.c());
                            break;
                        case 24:
                            setNeighborId(hVar.l());
                            break;
                        case 32:
                            setOuttieStatus(hVar.i());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasJoinCircleCenter() {
                return this.result.hasJoinCircleCenter();
            }

            public final PolygonVertex getJoinCircleCenter() {
                return this.result.getJoinCircleCenter();
            }

            public final Builder setJoinCircleCenter(PolygonVertex polygonVertex) {
                if (polygonVertex == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasJoinCircleCenter = true;
                PolygonVertex unused2 = this.result.joinCircleCenter_ = polygonVertex;
                return this;
            }

            public final Builder setJoinCircleCenter(PolygonVertex.Builder builder) {
                boolean unused = this.result.hasJoinCircleCenter = true;
                PolygonVertex unused2 = this.result.joinCircleCenter_ = builder.build();
                return this;
            }

            public final Builder mergeJoinCircleCenter(PolygonVertex polygonVertex) {
                if (!this.result.hasJoinCircleCenter() || this.result.joinCircleCenter_ == PolygonVertex.getDefaultInstance()) {
                    PolygonVertex unused = this.result.joinCircleCenter_ = polygonVertex;
                } else {
                    PolygonVertex unused2 = this.result.joinCircleCenter_ = PolygonVertex.newBuilder(this.result.joinCircleCenter_).mergeFrom(polygonVertex).buildPartial();
                }
                boolean unused3 = this.result.hasJoinCircleCenter = true;
                return this;
            }

            public final Builder clearJoinCircleCenter() {
                boolean unused = this.result.hasJoinCircleCenter = false;
                PolygonVertex unused2 = this.result.joinCircleCenter_ = PolygonVertex.getDefaultInstance();
                return this;
            }

            public final boolean hasBaseOrientation() {
                return this.result.hasBaseOrientation();
            }

            public final float getBaseOrientation() {
                return this.result.getBaseOrientation();
            }

            public final Builder setBaseOrientation(float f) {
                boolean unused = this.result.hasBaseOrientation = true;
                float unused2 = this.result.baseOrientation_ = f;
                return this;
            }

            public final Builder clearBaseOrientation() {
                boolean unused = this.result.hasBaseOrientation = false;
                float unused2 = this.result.baseOrientation_ = 0.0f;
                return this;
            }

            public final boolean hasNeighborId() {
                return this.result.hasNeighborId();
            }

            public final int getNeighborId() {
                return this.result.getNeighborId();
            }

            public final Builder setNeighborId(int i) {
                boolean unused = this.result.hasNeighborId = true;
                int unused2 = this.result.neighborId_ = i;
                return this;
            }

            public final Builder clearNeighborId() {
                boolean unused = this.result.hasNeighborId = false;
                int unused2 = this.result.neighborId_ = 0;
                return this;
            }

            public final boolean hasOuttieStatus() {
                return this.result.hasOuttieStatus();
            }

            public final boolean getOuttieStatus() {
                return this.result.getOuttieStatus();
            }

            public final Builder setOuttieStatus(boolean z) {
                boolean unused = this.result.hasOuttieStatus = true;
                boolean unused2 = this.result.outtieStatus_ = z;
                return this;
            }

            public final Builder clearOuttieStatus() {
                boolean unused = this.result.hasOuttieStatus = false;
                boolean unused2 = this.result.outtieStatus_ = false;
                return this;
            }
        }

        static {
            JoinInfo joinInfo = new JoinInfo(true);
            defaultInstance = joinInfo;
            joinInfo.joinCircleCenter_ = PolygonVertex.getDefaultInstance();
        }
    }

    public static final class Piece extends GeneratedMessage {
        public static final int CORNER_POINTS_FIELD_NUMBER = 6;
        public static final int EXTRUDED_POLYGON_INDICES_FIELD_NUMBER = 3;
        public static final int ID_FIELD_NUMBER = 1;
        public static final int JOIN_INFOS_FIELD_NUMBER = 4;
        public static final int POLYGON_INDICES_FIELD_NUMBER = 2;
        public static final int TRIANGULATION_INDICES_FIELD_NUMBER = 5;
        private static final Piece defaultInstance = new Piece(true);
        /* access modifiers changed from: private */
        public List<PolygonVertex> cornerPoints_;
        private int extrudedPolygonIndicesMemoizedSerializedSize;
        /* access modifiers changed from: private */
        public List<Integer> extrudedPolygonIndices_;
        /* access modifiers changed from: private */
        public boolean hasId;
        /* access modifiers changed from: private */
        public int id_;
        /* access modifiers changed from: private */
        public List<JoinInfo> joinInfos_;
        private int memoizedSerializedSize;
        private int polygonIndicesMemoizedSerializedSize;
        /* access modifiers changed from: private */
        public List<Integer> polygonIndices_;
        private int triangulationIndicesMemoizedSerializedSize;
        /* access modifiers changed from: private */
        public List<Integer> triangulationIndices_;

        /* synthetic */ Piece(ai aiVar) {
            this();
        }

        private Piece() {
            this.id_ = 0;
            this.polygonIndices_ = Collections.emptyList();
            this.polygonIndicesMemoizedSerializedSize = -1;
            this.extrudedPolygonIndices_ = Collections.emptyList();
            this.extrudedPolygonIndicesMemoizedSerializedSize = -1;
            this.joinInfos_ = Collections.emptyList();
            this.triangulationIndices_ = Collections.emptyList();
            this.triangulationIndicesMemoizedSerializedSize = -1;
            this.cornerPoints_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        private Piece(boolean z) {
            this.id_ = 0;
            this.polygonIndices_ = Collections.emptyList();
            this.polygonIndicesMemoizedSerializedSize = -1;
            this.extrudedPolygonIndices_ = Collections.emptyList();
            this.extrudedPolygonIndicesMemoizedSerializedSize = -1;
            this.joinInfos_ = Collections.emptyList();
            this.triangulationIndices_ = Collections.emptyList();
            this.triangulationIndicesMemoizedSerializedSize = -1;
            this.cornerPoints_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        public static Piece getDefaultInstance() {
            return defaultInstance;
        }

        public final Piece getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Jigsaur.i;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Jigsaur.j;
        }

        public final boolean hasId() {
            return this.hasId;
        }

        public final int getId() {
            return this.id_;
        }

        public final List<Integer> getPolygonIndicesList() {
            return this.polygonIndices_;
        }

        public final int getPolygonIndicesCount() {
            return this.polygonIndices_.size();
        }

        public final int getPolygonIndices(int i) {
            return this.polygonIndices_.get(i).intValue();
        }

        public final List<Integer> getExtrudedPolygonIndicesList() {
            return this.extrudedPolygonIndices_;
        }

        public final int getExtrudedPolygonIndicesCount() {
            return this.extrudedPolygonIndices_.size();
        }

        public final int getExtrudedPolygonIndices(int i) {
            return this.extrudedPolygonIndices_.get(i).intValue();
        }

        public final List<JoinInfo> getJoinInfosList() {
            return this.joinInfos_;
        }

        public final int getJoinInfosCount() {
            return this.joinInfos_.size();
        }

        public final JoinInfo getJoinInfos(int i) {
            return this.joinInfos_.get(i);
        }

        public final List<Integer> getTriangulationIndicesList() {
            return this.triangulationIndices_;
        }

        public final int getTriangulationIndicesCount() {
            return this.triangulationIndices_.size();
        }

        public final int getTriangulationIndices(int i) {
            return this.triangulationIndices_.get(i).intValue();
        }

        public final List<PolygonVertex> getCornerPointsList() {
            return this.cornerPoints_;
        }

        public final int getCornerPointsCount() {
            return this.cornerPoints_.size();
        }

        public final PolygonVertex getCornerPoints(int i) {
            return this.cornerPoints_.get(i);
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            if (!this.hasId) {
                return false;
            }
            for (JoinInfo isInitialized : getJoinInfosList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            for (PolygonVertex isInitialized2 : getCornerPointsList()) {
                if (!isInitialized2.isInitialized()) {
                    return false;
                }
            }
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasId()) {
                xVar.b(1, getId());
            }
            if (getPolygonIndicesList().size() > 0) {
                xVar.h(18);
                xVar.h(this.polygonIndicesMemoizedSerializedSize);
            }
            for (Integer intValue : getPolygonIndicesList()) {
                xVar.c(intValue.intValue());
            }
            if (getExtrudedPolygonIndicesList().size() > 0) {
                xVar.h(26);
                xVar.h(this.extrudedPolygonIndicesMemoizedSerializedSize);
            }
            for (Integer intValue2 : getExtrudedPolygonIndicesList()) {
                xVar.c(intValue2.intValue());
            }
            for (JoinInfo b : getJoinInfosList()) {
                xVar.b(4, b);
            }
            if (getTriangulationIndicesList().size() > 0) {
                xVar.h(42);
                xVar.h(this.triangulationIndicesMemoizedSerializedSize);
            }
            for (Integer intValue3 : getTriangulationIndicesList()) {
                xVar.c(intValue3.intValue());
            }
            for (PolygonVertex b2 : getCornerPointsList()) {
                xVar.b(6, b2);
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i;
            int i2;
            int i3;
            int i4 = 0;
            int i5 = this.memoizedSerializedSize;
            if (i5 != -1) {
                return i5;
            }
            if (hasId()) {
                i = x.e(1, getId()) + 0;
            } else {
                i = 0;
            }
            int i6 = 0;
            for (Integer intValue : getPolygonIndicesList()) {
                i6 = x.i(intValue.intValue()) + i6;
            }
            int i7 = i + i6;
            if (!getPolygonIndicesList().isEmpty()) {
                i2 = i7 + 1 + x.f(i6);
            } else {
                i2 = i7;
            }
            this.polygonIndicesMemoizedSerializedSize = i6;
            int i8 = 0;
            for (Integer intValue2 : getExtrudedPolygonIndicesList()) {
                i8 = x.i(intValue2.intValue()) + i8;
            }
            int i9 = i2 + i8;
            if (!getExtrudedPolygonIndicesList().isEmpty()) {
                i9 = i9 + 1 + x.f(i8);
            }
            this.extrudedPolygonIndicesMemoizedSerializedSize = i8;
            Iterator<JoinInfo> it = getJoinInfosList().iterator();
            while (true) {
                i3 = i9;
                if (!it.hasNext()) {
                    break;
                }
                i9 = x.d(4, it.next()) + i3;
            }
            for (Integer intValue3 : getTriangulationIndicesList()) {
                i4 += x.i(intValue3.intValue());
            }
            int i10 = i3 + i4;
            if (!getTriangulationIndicesList().isEmpty()) {
                i10 = i10 + 1 + x.f(i4);
            }
            this.triangulationIndicesMemoizedSerializedSize = i4;
            Iterator<PolygonVertex> it2 = getCornerPointsList().iterator();
            while (true) {
                int i11 = i10;
                if (it2.hasNext()) {
                    i10 = x.d(6, it2.next()) + i11;
                } else {
                    int serializedSize = getUnknownFields().getSerializedSize() + i11;
                    this.memoizedSerializedSize = serializedSize;
                    return serializedSize;
                }
            }
        }

        public static Piece parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.Piece.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.Piece.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.Piece.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.Piece.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.Piece.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$Piece$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static Piece parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static Piece parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.Piece.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.Piece.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.Piece.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.Piece.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.Piece.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$Piece$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static Piece parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static Piece parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.Piece.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.Piece.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.Piece.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.Piece.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.Piece.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$Piece$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static Piece parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static Piece parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static Piece parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static Piece parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.jigsaur.Jigsaur.Piece.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$Piece$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.Piece.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.Piece.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.Piece.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.Piece.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.Piece.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$Piece$Builder */
        public static Piece parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(Piece piece) {
            return newBuilder().mergeFrom(piece);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private Piece result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new Piece((ai) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final Piece internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new Piece((ai) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return Piece.getDescriptor();
            }

            public final Piece getDefaultInstanceForType() {
                return Piece.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final Piece build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public Piece buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final Piece buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.polygonIndices_ != Collections.EMPTY_LIST) {
                    List unused = this.result.polygonIndices_ = Collections.unmodifiableList(this.result.polygonIndices_);
                }
                if (this.result.extrudedPolygonIndices_ != Collections.EMPTY_LIST) {
                    List unused2 = this.result.extrudedPolygonIndices_ = Collections.unmodifiableList(this.result.extrudedPolygonIndices_);
                }
                if (this.result.joinInfos_ != Collections.EMPTY_LIST) {
                    List unused3 = this.result.joinInfos_ = Collections.unmodifiableList(this.result.joinInfos_);
                }
                if (this.result.triangulationIndices_ != Collections.EMPTY_LIST) {
                    List unused4 = this.result.triangulationIndices_ = Collections.unmodifiableList(this.result.triangulationIndices_);
                }
                if (this.result.cornerPoints_ != Collections.EMPTY_LIST) {
                    List unused5 = this.result.cornerPoints_ = Collections.unmodifiableList(this.result.cornerPoints_);
                }
                Piece piece = this.result;
                this.result = null;
                return piece;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof Piece) {
                    return mergeFrom((Piece) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(Piece piece) {
                if (piece != Piece.getDefaultInstance()) {
                    if (piece.hasId()) {
                        setId(piece.getId());
                    }
                    if (!piece.polygonIndices_.isEmpty()) {
                        if (this.result.polygonIndices_.isEmpty()) {
                            List unused = this.result.polygonIndices_ = new ArrayList();
                        }
                        this.result.polygonIndices_.addAll(piece.polygonIndices_);
                    }
                    if (!piece.extrudedPolygonIndices_.isEmpty()) {
                        if (this.result.extrudedPolygonIndices_.isEmpty()) {
                            List unused2 = this.result.extrudedPolygonIndices_ = new ArrayList();
                        }
                        this.result.extrudedPolygonIndices_.addAll(piece.extrudedPolygonIndices_);
                    }
                    if (!piece.joinInfos_.isEmpty()) {
                        if (this.result.joinInfos_.isEmpty()) {
                            List unused3 = this.result.joinInfos_ = new ArrayList();
                        }
                        this.result.joinInfos_.addAll(piece.joinInfos_);
                    }
                    if (!piece.triangulationIndices_.isEmpty()) {
                        if (this.result.triangulationIndices_.isEmpty()) {
                            List unused4 = this.result.triangulationIndices_ = new ArrayList();
                        }
                        this.result.triangulationIndices_.addAll(piece.triangulationIndices_);
                    }
                    if (!piece.cornerPoints_.isEmpty()) {
                        if (this.result.cornerPoints_.isEmpty()) {
                            List unused5 = this.result.cornerPoints_ = new ArrayList();
                        }
                        this.result.cornerPoints_.addAll(piece.cornerPoints_);
                    }
                    mergeUnknownFields(piece.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 8:
                            setId(hVar.l());
                            break;
                        case 16:
                            addPolygonIndices(hVar.l());
                            break;
                        case DescriptorProtos.FileOptions.PY_GENERIC_SERVICES_FIELD_NUMBER:
                            int c = hVar.c(hVar.n());
                            while (hVar.r() > 0) {
                                addPolygonIndices(hVar.l());
                            }
                            hVar.d(c);
                            break;
                        case 24:
                            addExtrudedPolygonIndices(hVar.l());
                            break;
                        case 26:
                            int c2 = hVar.c(hVar.n());
                            while (hVar.r() > 0) {
                                addExtrudedPolygonIndices(hVar.l());
                            }
                            hVar.d(c2);
                            break;
                        case 34:
                            JoinInfo.Builder newBuilder = JoinInfo.newBuilder();
                            hVar.a(newBuilder, abVar);
                            addJoinInfos(newBuilder.buildPartial());
                            break;
                        case 40:
                            addTriangulationIndices(hVar.l());
                            break;
                        case 42:
                            int c3 = hVar.c(hVar.n());
                            while (hVar.r() > 0) {
                                addTriangulationIndices(hVar.l());
                            }
                            hVar.d(c3);
                            break;
                        case 50:
                            PolygonVertex.Builder newBuilder2 = PolygonVertex.newBuilder();
                            hVar.a(newBuilder2, abVar);
                            addCornerPoints(newBuilder2.buildPartial());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasId() {
                return this.result.hasId();
            }

            public final int getId() {
                return this.result.getId();
            }

            public final Builder setId(int i) {
                boolean unused = this.result.hasId = true;
                int unused2 = this.result.id_ = i;
                return this;
            }

            public final Builder clearId() {
                boolean unused = this.result.hasId = false;
                int unused2 = this.result.id_ = 0;
                return this;
            }

            public final List<Integer> getPolygonIndicesList() {
                return Collections.unmodifiableList(this.result.polygonIndices_);
            }

            public final int getPolygonIndicesCount() {
                return this.result.getPolygonIndicesCount();
            }

            public final int getPolygonIndices(int i) {
                return this.result.getPolygonIndices(i);
            }

            public final Builder setPolygonIndices(int i, int i2) {
                this.result.polygonIndices_.set(i, Integer.valueOf(i2));
                return this;
            }

            public final Builder addPolygonIndices(int i) {
                if (this.result.polygonIndices_.isEmpty()) {
                    List unused = this.result.polygonIndices_ = new ArrayList();
                }
                this.result.polygonIndices_.add(Integer.valueOf(i));
                return this;
            }

            public final Builder addAllPolygonIndices(Iterable<? extends Integer> iterable) {
                if (this.result.polygonIndices_.isEmpty()) {
                    List unused = this.result.polygonIndices_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.polygonIndices_);
                return this;
            }

            public final Builder clearPolygonIndices() {
                List unused = this.result.polygonIndices_ = Collections.emptyList();
                return this;
            }

            public final List<Integer> getExtrudedPolygonIndicesList() {
                return Collections.unmodifiableList(this.result.extrudedPolygonIndices_);
            }

            public final int getExtrudedPolygonIndicesCount() {
                return this.result.getExtrudedPolygonIndicesCount();
            }

            public final int getExtrudedPolygonIndices(int i) {
                return this.result.getExtrudedPolygonIndices(i);
            }

            public final Builder setExtrudedPolygonIndices(int i, int i2) {
                this.result.extrudedPolygonIndices_.set(i, Integer.valueOf(i2));
                return this;
            }

            public final Builder addExtrudedPolygonIndices(int i) {
                if (this.result.extrudedPolygonIndices_.isEmpty()) {
                    List unused = this.result.extrudedPolygonIndices_ = new ArrayList();
                }
                this.result.extrudedPolygonIndices_.add(Integer.valueOf(i));
                return this;
            }

            public final Builder addAllExtrudedPolygonIndices(Iterable<? extends Integer> iterable) {
                if (this.result.extrudedPolygonIndices_.isEmpty()) {
                    List unused = this.result.extrudedPolygonIndices_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.extrudedPolygonIndices_);
                return this;
            }

            public final Builder clearExtrudedPolygonIndices() {
                List unused = this.result.extrudedPolygonIndices_ = Collections.emptyList();
                return this;
            }

            public final List<JoinInfo> getJoinInfosList() {
                return Collections.unmodifiableList(this.result.joinInfos_);
            }

            public final int getJoinInfosCount() {
                return this.result.getJoinInfosCount();
            }

            public final JoinInfo getJoinInfos(int i) {
                return this.result.getJoinInfos(i);
            }

            public final Builder setJoinInfos(int i, JoinInfo joinInfo) {
                if (joinInfo == null) {
                    throw new NullPointerException();
                }
                this.result.joinInfos_.set(i, joinInfo);
                return this;
            }

            public final Builder setJoinInfos(int i, JoinInfo.Builder builder) {
                this.result.joinInfos_.set(i, builder.build());
                return this;
            }

            public final Builder addJoinInfos(JoinInfo joinInfo) {
                if (joinInfo == null) {
                    throw new NullPointerException();
                }
                if (this.result.joinInfos_.isEmpty()) {
                    List unused = this.result.joinInfos_ = new ArrayList();
                }
                this.result.joinInfos_.add(joinInfo);
                return this;
            }

            public final Builder addJoinInfos(JoinInfo.Builder builder) {
                if (this.result.joinInfos_.isEmpty()) {
                    List unused = this.result.joinInfos_ = new ArrayList();
                }
                this.result.joinInfos_.add(builder.build());
                return this;
            }

            public final Builder addAllJoinInfos(Iterable<? extends JoinInfo> iterable) {
                if (this.result.joinInfos_.isEmpty()) {
                    List unused = this.result.joinInfos_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.joinInfos_);
                return this;
            }

            public final Builder clearJoinInfos() {
                List unused = this.result.joinInfos_ = Collections.emptyList();
                return this;
            }

            public final List<Integer> getTriangulationIndicesList() {
                return Collections.unmodifiableList(this.result.triangulationIndices_);
            }

            public final int getTriangulationIndicesCount() {
                return this.result.getTriangulationIndicesCount();
            }

            public final int getTriangulationIndices(int i) {
                return this.result.getTriangulationIndices(i);
            }

            public final Builder setTriangulationIndices(int i, int i2) {
                this.result.triangulationIndices_.set(i, Integer.valueOf(i2));
                return this;
            }

            public final Builder addTriangulationIndices(int i) {
                if (this.result.triangulationIndices_.isEmpty()) {
                    List unused = this.result.triangulationIndices_ = new ArrayList();
                }
                this.result.triangulationIndices_.add(Integer.valueOf(i));
                return this;
            }

            public final Builder addAllTriangulationIndices(Iterable<? extends Integer> iterable) {
                if (this.result.triangulationIndices_.isEmpty()) {
                    List unused = this.result.triangulationIndices_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.triangulationIndices_);
                return this;
            }

            public final Builder clearTriangulationIndices() {
                List unused = this.result.triangulationIndices_ = Collections.emptyList();
                return this;
            }

            public final List<PolygonVertex> getCornerPointsList() {
                return Collections.unmodifiableList(this.result.cornerPoints_);
            }

            public final int getCornerPointsCount() {
                return this.result.getCornerPointsCount();
            }

            public final PolygonVertex getCornerPoints(int i) {
                return this.result.getCornerPoints(i);
            }

            public final Builder setCornerPoints(int i, PolygonVertex polygonVertex) {
                if (polygonVertex == null) {
                    throw new NullPointerException();
                }
                this.result.cornerPoints_.set(i, polygonVertex);
                return this;
            }

            public final Builder setCornerPoints(int i, PolygonVertex.Builder builder) {
                this.result.cornerPoints_.set(i, builder.build());
                return this;
            }

            public final Builder addCornerPoints(PolygonVertex polygonVertex) {
                if (polygonVertex == null) {
                    throw new NullPointerException();
                }
                if (this.result.cornerPoints_.isEmpty()) {
                    List unused = this.result.cornerPoints_ = new ArrayList();
                }
                this.result.cornerPoints_.add(polygonVertex);
                return this;
            }

            public final Builder addCornerPoints(PolygonVertex.Builder builder) {
                if (this.result.cornerPoints_.isEmpty()) {
                    List unused = this.result.cornerPoints_ = new ArrayList();
                }
                this.result.cornerPoints_.add(builder.build());
                return this;
            }

            public final Builder addAllCornerPoints(Iterable<? extends PolygonVertex> iterable) {
                if (this.result.cornerPoints_.isEmpty()) {
                    List unused = this.result.cornerPoints_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.cornerPoints_);
                return this;
            }

            public final Builder clearCornerPoints() {
                List unused = this.result.cornerPoints_ = Collections.emptyList();
                return this;
            }
        }
    }

    public static final class PieceSet extends GeneratedMessage {
        public static final int EXTRUSION_DEPTH_FIELD_NUMBER = 7;
        public static final int MAXIMUM_JOIN_DISTANCE_FIELD_NUMBER = 4;
        public static final int NUM_TOTAL_INDICES_FIELD_NUMBER = 3;
        public static final int PIECES_FIELD_NUMBER = 1;
        public static final int PIECE_VERTICES_FIELD_NUMBER = 2;
        public static final int UNUSED1_FIELD_NUMBER = 5;
        public static final int UNUSED2_FIELD_NUMBER = 6;
        private static final PieceSet defaultInstance = new PieceSet(true);
        /* access modifiers changed from: private */
        public float extrusionDepth_;
        /* access modifiers changed from: private */
        public boolean hasExtrusionDepth;
        /* access modifiers changed from: private */
        public boolean hasMaximumJoinDistance;
        /* access modifiers changed from: private */
        public boolean hasNumTotalIndices;
        /* access modifiers changed from: private */
        public boolean hasPieceVertices;
        /* access modifiers changed from: private */
        public boolean hasUnused1;
        /* access modifiers changed from: private */
        public boolean hasUnused2;
        /* access modifiers changed from: private */
        public float maximumJoinDistance_;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public int numTotalIndices_;
        /* access modifiers changed from: private */
        public q pieceVertices_;
        /* access modifiers changed from: private */
        public List<Piece> pieces_;
        /* access modifiers changed from: private */
        public float unused1_;
        /* access modifiers changed from: private */
        public float unused2_;

        /* synthetic */ PieceSet(ai aiVar) {
            this();
        }

        private PieceSet() {
            this.pieces_ = Collections.emptyList();
            this.pieceVertices_ = q.a;
            this.numTotalIndices_ = 0;
            this.maximumJoinDistance_ = 0.0f;
            this.unused1_ = 0.0f;
            this.unused2_ = 0.0f;
            this.extrusionDepth_ = 0.0f;
            this.memoizedSerializedSize = -1;
        }

        private PieceSet(boolean z) {
            this.pieces_ = Collections.emptyList();
            this.pieceVertices_ = q.a;
            this.numTotalIndices_ = 0;
            this.maximumJoinDistance_ = 0.0f;
            this.unused1_ = 0.0f;
            this.unused2_ = 0.0f;
            this.extrusionDepth_ = 0.0f;
            this.memoizedSerializedSize = -1;
        }

        public static PieceSet getDefaultInstance() {
            return defaultInstance;
        }

        public final PieceSet getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Jigsaur.k;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Jigsaur.l;
        }

        public final List<Piece> getPiecesList() {
            return this.pieces_;
        }

        public final int getPiecesCount() {
            return this.pieces_.size();
        }

        public final Piece getPieces(int i) {
            return this.pieces_.get(i);
        }

        public final boolean hasPieceVertices() {
            return this.hasPieceVertices;
        }

        public final q getPieceVertices() {
            return this.pieceVertices_;
        }

        public final boolean hasNumTotalIndices() {
            return this.hasNumTotalIndices;
        }

        public final int getNumTotalIndices() {
            return this.numTotalIndices_;
        }

        public final boolean hasMaximumJoinDistance() {
            return this.hasMaximumJoinDistance;
        }

        public final float getMaximumJoinDistance() {
            return this.maximumJoinDistance_;
        }

        public final boolean hasUnused1() {
            return this.hasUnused1;
        }

        public final float getUnused1() {
            return this.unused1_;
        }

        public final boolean hasUnused2() {
            return this.hasUnused2;
        }

        public final float getUnused2() {
            return this.unused2_;
        }

        public final boolean hasExtrusionDepth() {
            return this.hasExtrusionDepth;
        }

        public final float getExtrusionDepth() {
            return this.extrusionDepth_;
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            if (!this.hasPieceVertices) {
                return false;
            }
            if (!this.hasNumTotalIndices) {
                return false;
            }
            if (!this.hasMaximumJoinDistance) {
                return false;
            }
            if (!this.hasExtrusionDepth) {
                return false;
            }
            for (Piece isInitialized : getPiecesList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            for (Piece b : getPiecesList()) {
                xVar.b(1, b);
            }
            if (hasPieceVertices()) {
                xVar.a(2, getPieceVertices());
            }
            if (hasNumTotalIndices()) {
                xVar.b(3, getNumTotalIndices());
            }
            if (hasMaximumJoinDistance()) {
                xVar.a(4, getMaximumJoinDistance());
            }
            if (hasUnused1()) {
                xVar.a(5, getUnused1());
            }
            if (hasUnused2()) {
                xVar.a(6, getUnused2());
            }
            if (hasExtrusionDepth()) {
                xVar.a(7, getExtrusionDepth());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i;
            int i2 = this.memoizedSerializedSize;
            if (i2 != -1) {
                return i2;
            }
            int i3 = 0;
            Iterator<Piece> it = getPiecesList().iterator();
            while (true) {
                i = i3;
                if (!it.hasNext()) {
                    break;
                }
                i3 = x.d(1, it.next()) + i;
            }
            if (hasPieceVertices()) {
                i += x.b(2, getPieceVertices());
            }
            if (hasNumTotalIndices()) {
                i += x.e(3, getNumTotalIndices());
            }
            if (hasMaximumJoinDistance()) {
                getMaximumJoinDistance();
                i += x.d(4);
            }
            if (hasUnused1()) {
                getUnused1();
                i += x.d(5);
            }
            if (hasUnused2()) {
                getUnused2();
                i += x.d(6);
            }
            if (hasExtrusionDepth()) {
                getExtrusionDepth();
                i += x.d(7);
            }
            int serializedSize = getUnknownFields().getSerializedSize() + i;
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static PieceSet parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.PieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PieceSet$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static PieceSet parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static PieceSet parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.PieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PieceSet$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static PieceSet parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static PieceSet parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.PieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PieceSet$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static PieceSet parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static PieceSet parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static PieceSet parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static PieceSet parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.jigsaur.Jigsaur.PieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PieceSet$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PieceSet$Builder */
        public static PieceSet parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(PieceSet pieceSet) {
            return newBuilder().mergeFrom(pieceSet);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private PieceSet result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new PieceSet((ai) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final PieceSet internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new PieceSet((ai) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return PieceSet.getDescriptor();
            }

            public final PieceSet getDefaultInstanceForType() {
                return PieceSet.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final PieceSet build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public PieceSet buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final PieceSet buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.pieces_ != Collections.EMPTY_LIST) {
                    List unused = this.result.pieces_ = Collections.unmodifiableList(this.result.pieces_);
                }
                PieceSet pieceSet = this.result;
                this.result = null;
                return pieceSet;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof PieceSet) {
                    return mergeFrom((PieceSet) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(PieceSet pieceSet) {
                if (pieceSet != PieceSet.getDefaultInstance()) {
                    if (!pieceSet.pieces_.isEmpty()) {
                        if (this.result.pieces_.isEmpty()) {
                            List unused = this.result.pieces_ = new ArrayList();
                        }
                        this.result.pieces_.addAll(pieceSet.pieces_);
                    }
                    if (pieceSet.hasPieceVertices()) {
                        setPieceVertices(pieceSet.getPieceVertices());
                    }
                    if (pieceSet.hasNumTotalIndices()) {
                        setNumTotalIndices(pieceSet.getNumTotalIndices());
                    }
                    if (pieceSet.hasMaximumJoinDistance()) {
                        setMaximumJoinDistance(pieceSet.getMaximumJoinDistance());
                    }
                    if (pieceSet.hasUnused1()) {
                        setUnused1(pieceSet.getUnused1());
                    }
                    if (pieceSet.hasUnused2()) {
                        setUnused2(pieceSet.getUnused2());
                    }
                    if (pieceSet.hasExtrusionDepth()) {
                        setExtrusionDepth(pieceSet.getExtrusionDepth());
                    }
                    mergeUnknownFields(pieceSet.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 10:
                            Piece.Builder newBuilder = Piece.newBuilder();
                            hVar.a(newBuilder, abVar);
                            addPieces(newBuilder.buildPartial());
                            break;
                        case DescriptorProtos.FileOptions.PY_GENERIC_SERVICES_FIELD_NUMBER:
                            setPieceVertices(hVar.k());
                            break;
                        case 24:
                            setNumTotalIndices(hVar.l());
                            break;
                        case 37:
                            setMaximumJoinDistance(hVar.c());
                            break;
                        case 45:
                            setUnused1(hVar.c());
                            break;
                        case 53:
                            setUnused2(hVar.c());
                            break;
                        case 61:
                            setExtrusionDepth(hVar.c());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final List<Piece> getPiecesList() {
                return Collections.unmodifiableList(this.result.pieces_);
            }

            public final int getPiecesCount() {
                return this.result.getPiecesCount();
            }

            public final Piece getPieces(int i) {
                return this.result.getPieces(i);
            }

            public final Builder setPieces(int i, Piece piece) {
                if (piece == null) {
                    throw new NullPointerException();
                }
                this.result.pieces_.set(i, piece);
                return this;
            }

            public final Builder setPieces(int i, Piece.Builder builder) {
                this.result.pieces_.set(i, builder.build());
                return this;
            }

            public final Builder addPieces(Piece piece) {
                if (piece == null) {
                    throw new NullPointerException();
                }
                if (this.result.pieces_.isEmpty()) {
                    List unused = this.result.pieces_ = new ArrayList();
                }
                this.result.pieces_.add(piece);
                return this;
            }

            public final Builder addPieces(Piece.Builder builder) {
                if (this.result.pieces_.isEmpty()) {
                    List unused = this.result.pieces_ = new ArrayList();
                }
                this.result.pieces_.add(builder.build());
                return this;
            }

            public final Builder addAllPieces(Iterable<? extends Piece> iterable) {
                if (this.result.pieces_.isEmpty()) {
                    List unused = this.result.pieces_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.pieces_);
                return this;
            }

            public final Builder clearPieces() {
                List unused = this.result.pieces_ = Collections.emptyList();
                return this;
            }

            public final boolean hasPieceVertices() {
                return this.result.hasPieceVertices();
            }

            public final q getPieceVertices() {
                return this.result.getPieceVertices();
            }

            public final Builder setPieceVertices(q qVar) {
                if (qVar == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasPieceVertices = true;
                q unused2 = this.result.pieceVertices_ = qVar;
                return this;
            }

            public final Builder clearPieceVertices() {
                boolean unused = this.result.hasPieceVertices = false;
                q unused2 = this.result.pieceVertices_ = PieceSet.getDefaultInstance().getPieceVertices();
                return this;
            }

            public final boolean hasNumTotalIndices() {
                return this.result.hasNumTotalIndices();
            }

            public final int getNumTotalIndices() {
                return this.result.getNumTotalIndices();
            }

            public final Builder setNumTotalIndices(int i) {
                boolean unused = this.result.hasNumTotalIndices = true;
                int unused2 = this.result.numTotalIndices_ = i;
                return this;
            }

            public final Builder clearNumTotalIndices() {
                boolean unused = this.result.hasNumTotalIndices = false;
                int unused2 = this.result.numTotalIndices_ = 0;
                return this;
            }

            public final boolean hasMaximumJoinDistance() {
                return this.result.hasMaximumJoinDistance();
            }

            public final float getMaximumJoinDistance() {
                return this.result.getMaximumJoinDistance();
            }

            public final Builder setMaximumJoinDistance(float f) {
                boolean unused = this.result.hasMaximumJoinDistance = true;
                float unused2 = this.result.maximumJoinDistance_ = f;
                return this;
            }

            public final Builder clearMaximumJoinDistance() {
                boolean unused = this.result.hasMaximumJoinDistance = false;
                float unused2 = this.result.maximumJoinDistance_ = 0.0f;
                return this;
            }

            public final boolean hasUnused1() {
                return this.result.hasUnused1();
            }

            public final float getUnused1() {
                return this.result.getUnused1();
            }

            public final Builder setUnused1(float f) {
                boolean unused = this.result.hasUnused1 = true;
                float unused2 = this.result.unused1_ = f;
                return this;
            }

            public final Builder clearUnused1() {
                boolean unused = this.result.hasUnused1 = false;
                float unused2 = this.result.unused1_ = 0.0f;
                return this;
            }

            public final boolean hasUnused2() {
                return this.result.hasUnused2();
            }

            public final float getUnused2() {
                return this.result.getUnused2();
            }

            public final Builder setUnused2(float f) {
                boolean unused = this.result.hasUnused2 = true;
                float unused2 = this.result.unused2_ = f;
                return this;
            }

            public final Builder clearUnused2() {
                boolean unused = this.result.hasUnused2 = false;
                float unused2 = this.result.unused2_ = 0.0f;
                return this;
            }

            public final boolean hasExtrusionDepth() {
                return this.result.hasExtrusionDepth();
            }

            public final float getExtrusionDepth() {
                return this.result.getExtrusionDepth();
            }

            public final Builder setExtrusionDepth(float f) {
                boolean unused = this.result.hasExtrusionDepth = true;
                float unused2 = this.result.extrusionDepth_ = f;
                return this;
            }

            public final Builder clearExtrusionDepth() {
                boolean unused = this.result.hasExtrusionDepth = false;
                float unused2 = this.result.extrusionDepth_ = 0.0f;
                return this;
            }
        }
    }

    public static final class PieceSetStub extends GeneratedMessage {
        public static final int PIECE_SET_FILE_NAME_PREFIX_FIELD_NUMBER = 1;
        public static final int PIECE_SET_TYPE_FIELD_NUMBER = 2;
        private static final PieceSetStub defaultInstance = new PieceSetStub(true);
        /* access modifiers changed from: private */
        public boolean hasPieceSetFileNamePrefix;
        /* access modifiers changed from: private */
        public boolean hasPieceSetType;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public String pieceSetFileNamePrefix_;
        /* access modifiers changed from: private */
        public String pieceSetType_;

        /* synthetic */ PieceSetStub(ai aiVar) {
            this();
        }

        private PieceSetStub() {
            this.pieceSetFileNamePrefix_ = "";
            this.pieceSetType_ = "";
            this.memoizedSerializedSize = -1;
        }

        private PieceSetStub(boolean z) {
            this.pieceSetFileNamePrefix_ = "";
            this.pieceSetType_ = "";
            this.memoizedSerializedSize = -1;
        }

        public static PieceSetStub getDefaultInstance() {
            return defaultInstance;
        }

        public final PieceSetStub getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Jigsaur.m;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Jigsaur.n;
        }

        public final boolean hasPieceSetFileNamePrefix() {
            return this.hasPieceSetFileNamePrefix;
        }

        public final String getPieceSetFileNamePrefix() {
            return this.pieceSetFileNamePrefix_;
        }

        public final boolean hasPieceSetType() {
            return this.hasPieceSetType;
        }

        public final String getPieceSetType() {
            return this.pieceSetType_;
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            if (this.hasPieceSetFileNamePrefix && this.hasPieceSetType) {
                return true;
            }
            return false;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasPieceSetFileNamePrefix()) {
                xVar.a(1, getPieceSetFileNamePrefix());
            }
            if (hasPieceSetType()) {
                xVar.a(2, getPieceSetType());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (hasPieceSetFileNamePrefix()) {
                i2 = x.b(1, getPieceSetFileNamePrefix()) + 0;
            }
            if (hasPieceSetType()) {
                i2 += x.b(2, getPieceSetType());
            }
            int serializedSize = i2 + getUnknownFields().getSerializedSize();
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static PieceSetStub parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PieceSetStub.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStub.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStub.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStub.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStub.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PieceSetStub$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static PieceSetStub parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static PieceSetStub parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PieceSetStub.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStub.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStub.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStub.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStub.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PieceSetStub$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static PieceSetStub parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static PieceSetStub parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PieceSetStub.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStub.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStub.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStub.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStub.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PieceSetStub$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static PieceSetStub parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static PieceSetStub parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static PieceSetStub parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static PieceSetStub parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.jigsaur.Jigsaur.PieceSetStub.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PieceSetStub$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PieceSetStub.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStub.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStub.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStub.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStub.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PieceSetStub$Builder */
        public static PieceSetStub parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(PieceSetStub pieceSetStub) {
            return newBuilder().mergeFrom(pieceSetStub);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private PieceSetStub result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new PieceSetStub((ai) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final PieceSetStub internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new PieceSetStub((ai) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return PieceSetStub.getDescriptor();
            }

            public final PieceSetStub getDefaultInstanceForType() {
                return PieceSetStub.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final PieceSetStub build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public PieceSetStub buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final PieceSetStub buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                PieceSetStub pieceSetStub = this.result;
                this.result = null;
                return pieceSetStub;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof PieceSetStub) {
                    return mergeFrom((PieceSetStub) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(PieceSetStub pieceSetStub) {
                if (pieceSetStub != PieceSetStub.getDefaultInstance()) {
                    if (pieceSetStub.hasPieceSetFileNamePrefix()) {
                        setPieceSetFileNamePrefix(pieceSetStub.getPieceSetFileNamePrefix());
                    }
                    if (pieceSetStub.hasPieceSetType()) {
                        setPieceSetType(pieceSetStub.getPieceSetType());
                    }
                    mergeUnknownFields(pieceSetStub.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 10:
                            setPieceSetFileNamePrefix(hVar.j());
                            break;
                        case DescriptorProtos.FileOptions.PY_GENERIC_SERVICES_FIELD_NUMBER:
                            setPieceSetType(hVar.j());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasPieceSetFileNamePrefix() {
                return this.result.hasPieceSetFileNamePrefix();
            }

            public final String getPieceSetFileNamePrefix() {
                return this.result.getPieceSetFileNamePrefix();
            }

            public final Builder setPieceSetFileNamePrefix(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasPieceSetFileNamePrefix = true;
                String unused2 = this.result.pieceSetFileNamePrefix_ = str;
                return this;
            }

            public final Builder clearPieceSetFileNamePrefix() {
                boolean unused = this.result.hasPieceSetFileNamePrefix = false;
                String unused2 = this.result.pieceSetFileNamePrefix_ = PieceSetStub.getDefaultInstance().getPieceSetFileNamePrefix();
                return this;
            }

            public final boolean hasPieceSetType() {
                return this.result.hasPieceSetType();
            }

            public final String getPieceSetType() {
                return this.result.getPieceSetType();
            }

            public final Builder setPieceSetType(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasPieceSetType = true;
                String unused2 = this.result.pieceSetType_ = str;
                return this;
            }

            public final Builder clearPieceSetType() {
                boolean unused = this.result.hasPieceSetType = false;
                String unused2 = this.result.pieceSetType_ = PieceSetStub.getDefaultInstance().getPieceSetType();
                return this;
            }
        }
    }

    public static final class PieceSetStubs extends GeneratedMessage {
        public static final int STUBS_FIELD_NUMBER = 1;
        private static final PieceSetStubs defaultInstance = new PieceSetStubs(true);
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public List<PieceSetStub> stubs_;

        /* synthetic */ PieceSetStubs(ai aiVar) {
            this();
        }

        private PieceSetStubs() {
            this.stubs_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        private PieceSetStubs(boolean z) {
            this.stubs_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        public static PieceSetStubs getDefaultInstance() {
            return defaultInstance;
        }

        public final PieceSetStubs getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Jigsaur.o;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Jigsaur.p;
        }

        public final List<PieceSetStub> getStubsList() {
            return this.stubs_;
        }

        public final int getStubsCount() {
            return this.stubs_.size();
        }

        public final PieceSetStub getStubs(int i) {
            return this.stubs_.get(i);
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            for (PieceSetStub isInitialized : getStubsList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            for (PieceSetStub b : getStubsList()) {
                xVar.b(1, b);
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            Iterator<PieceSetStub> it = getStubsList().iterator();
            while (true) {
                int i3 = i2;
                if (it.hasNext()) {
                    i2 = x.d(1, it.next()) + i3;
                } else {
                    int serializedSize = getUnknownFields().getSerializedSize() + i3;
                    this.memoizedSerializedSize = serializedSize;
                    return serializedSize;
                }
            }
        }

        public static PieceSetStubs parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PieceSetStubs.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStubs.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStubs.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStubs.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStubs.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PieceSetStubs$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static PieceSetStubs parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static PieceSetStubs parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PieceSetStubs.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStubs.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStubs.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStubs.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStubs.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PieceSetStubs$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static PieceSetStubs parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static PieceSetStubs parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PieceSetStubs.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStubs.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStubs.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStubs.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStubs.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PieceSetStubs$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static PieceSetStubs parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static PieceSetStubs parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static PieceSetStubs parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static PieceSetStubs parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.jigsaur.Jigsaur.PieceSetStubs.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PieceSetStubs$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PieceSetStubs.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStubs.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStubs.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStubs.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PieceSetStubs.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PieceSetStubs$Builder */
        public static PieceSetStubs parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(PieceSetStubs pieceSetStubs) {
            return newBuilder().mergeFrom(pieceSetStubs);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private PieceSetStubs result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new PieceSetStubs((ai) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final PieceSetStubs internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new PieceSetStubs((ai) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return PieceSetStubs.getDescriptor();
            }

            public final PieceSetStubs getDefaultInstanceForType() {
                return PieceSetStubs.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final PieceSetStubs build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public PieceSetStubs buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final PieceSetStubs buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.stubs_ != Collections.EMPTY_LIST) {
                    List unused = this.result.stubs_ = Collections.unmodifiableList(this.result.stubs_);
                }
                PieceSetStubs pieceSetStubs = this.result;
                this.result = null;
                return pieceSetStubs;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof PieceSetStubs) {
                    return mergeFrom((PieceSetStubs) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(PieceSetStubs pieceSetStubs) {
                if (pieceSetStubs != PieceSetStubs.getDefaultInstance()) {
                    if (!pieceSetStubs.stubs_.isEmpty()) {
                        if (this.result.stubs_.isEmpty()) {
                            List unused = this.result.stubs_ = new ArrayList();
                        }
                        this.result.stubs_.addAll(pieceSetStubs.stubs_);
                    }
                    mergeUnknownFields(pieceSetStubs.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 10:
                            PieceSetStub.Builder newBuilder = PieceSetStub.newBuilder();
                            hVar.a(newBuilder, abVar);
                            addStubs(newBuilder.buildPartial());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final List<PieceSetStub> getStubsList() {
                return Collections.unmodifiableList(this.result.stubs_);
            }

            public final int getStubsCount() {
                return this.result.getStubsCount();
            }

            public final PieceSetStub getStubs(int i) {
                return this.result.getStubs(i);
            }

            public final Builder setStubs(int i, PieceSetStub pieceSetStub) {
                if (pieceSetStub == null) {
                    throw new NullPointerException();
                }
                this.result.stubs_.set(i, pieceSetStub);
                return this;
            }

            public final Builder setStubs(int i, PieceSetStub.Builder builder) {
                this.result.stubs_.set(i, builder.build());
                return this;
            }

            public final Builder addStubs(PieceSetStub pieceSetStub) {
                if (pieceSetStub == null) {
                    throw new NullPointerException();
                }
                if (this.result.stubs_.isEmpty()) {
                    List unused = this.result.stubs_ = new ArrayList();
                }
                this.result.stubs_.add(pieceSetStub);
                return this;
            }

            public final Builder addStubs(PieceSetStub.Builder builder) {
                if (this.result.stubs_.isEmpty()) {
                    List unused = this.result.stubs_ = new ArrayList();
                }
                this.result.stubs_.add(builder.build());
                return this;
            }

            public final Builder addAllStubs(Iterable<? extends PieceSetStub> iterable) {
                if (this.result.stubs_.isEmpty()) {
                    List unused = this.result.stubs_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.stubs_);
                return this;
            }

            public final Builder clearStubs() {
                List unused = this.result.stubs_ = Collections.emptyList();
                return this;
            }
        }
    }

    public static final class TypeOnePrimordialPieceSet extends GeneratedMessage {
        public static final int BOARD_HEIGHT_FIELD_NUMBER = 7;
        public static final int BOARD_WIDTH_FIELD_NUMBER = 6;
        public static final int EXTRUSION_DEPTH_FIELD_NUMBER = 5;
        public static final int GRID_POINTS_FIELD_NUMBER = 3;
        public static final int JOIN_CORNERS_FIELD_NUMBER = 4;
        public static final int NUM_HORIZONTALS_FIELD_NUMBER = 2;
        public static final int NUM_VERTICALS_FIELD_NUMBER = 1;
        private static final TypeOnePrimordialPieceSet defaultInstance = new TypeOnePrimordialPieceSet(true);
        /* access modifiers changed from: private */
        public float boardHeight_;
        /* access modifiers changed from: private */
        public float boardWidth_;
        /* access modifiers changed from: private */
        public float extrusionDepth_;
        /* access modifiers changed from: private */
        public List<PolygonVertex> gridPoints_;
        /* access modifiers changed from: private */
        public boolean hasBoardHeight;
        /* access modifiers changed from: private */
        public boolean hasBoardWidth;
        /* access modifiers changed from: private */
        public boolean hasExtrusionDepth;
        /* access modifiers changed from: private */
        public boolean hasNumHorizontals;
        /* access modifiers changed from: private */
        public boolean hasNumVerticals;
        /* access modifiers changed from: private */
        public List<JoinCorner> joinCorners_;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public int numHorizontals_;
        /* access modifiers changed from: private */
        public int numVerticals_;

        /* synthetic */ TypeOnePrimordialPieceSet(ai aiVar) {
            this();
        }

        private TypeOnePrimordialPieceSet() {
            this.numVerticals_ = 0;
            this.numHorizontals_ = 0;
            this.gridPoints_ = Collections.emptyList();
            this.joinCorners_ = Collections.emptyList();
            this.extrusionDepth_ = 0.0f;
            this.boardWidth_ = 0.0f;
            this.boardHeight_ = 0.0f;
            this.memoizedSerializedSize = -1;
        }

        private TypeOnePrimordialPieceSet(boolean z) {
            this.numVerticals_ = 0;
            this.numHorizontals_ = 0;
            this.gridPoints_ = Collections.emptyList();
            this.joinCorners_ = Collections.emptyList();
            this.extrusionDepth_ = 0.0f;
            this.boardWidth_ = 0.0f;
            this.boardHeight_ = 0.0f;
            this.memoizedSerializedSize = -1;
        }

        public static TypeOnePrimordialPieceSet getDefaultInstance() {
            return defaultInstance;
        }

        public final TypeOnePrimordialPieceSet getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Jigsaur.q;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Jigsaur.r;
        }

        public final boolean hasNumVerticals() {
            return this.hasNumVerticals;
        }

        public final int getNumVerticals() {
            return this.numVerticals_;
        }

        public final boolean hasNumHorizontals() {
            return this.hasNumHorizontals;
        }

        public final int getNumHorizontals() {
            return this.numHorizontals_;
        }

        public final List<PolygonVertex> getGridPointsList() {
            return this.gridPoints_;
        }

        public final int getGridPointsCount() {
            return this.gridPoints_.size();
        }

        public final PolygonVertex getGridPoints(int i) {
            return this.gridPoints_.get(i);
        }

        public final List<JoinCorner> getJoinCornersList() {
            return this.joinCorners_;
        }

        public final int getJoinCornersCount() {
            return this.joinCorners_.size();
        }

        public final JoinCorner getJoinCorners(int i) {
            return this.joinCorners_.get(i);
        }

        public final boolean hasExtrusionDepth() {
            return this.hasExtrusionDepth;
        }

        public final float getExtrusionDepth() {
            return this.extrusionDepth_;
        }

        public final boolean hasBoardWidth() {
            return this.hasBoardWidth;
        }

        public final float getBoardWidth() {
            return this.boardWidth_;
        }

        public final boolean hasBoardHeight() {
            return this.hasBoardHeight;
        }

        public final float getBoardHeight() {
            return this.boardHeight_;
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            if (!this.hasNumVerticals) {
                return false;
            }
            if (!this.hasNumHorizontals) {
                return false;
            }
            if (!this.hasExtrusionDepth) {
                return false;
            }
            if (!this.hasBoardWidth) {
                return false;
            }
            if (!this.hasBoardHeight) {
                return false;
            }
            for (PolygonVertex isInitialized : getGridPointsList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            for (JoinCorner isInitialized2 : getJoinCornersList()) {
                if (!isInitialized2.isInitialized()) {
                    return false;
                }
            }
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasNumVerticals()) {
                xVar.b(1, getNumVerticals());
            }
            if (hasNumHorizontals()) {
                xVar.b(2, getNumHorizontals());
            }
            for (PolygonVertex b : getGridPointsList()) {
                xVar.b(3, b);
            }
            for (JoinCorner b2 : getJoinCornersList()) {
                xVar.b(4, b2);
            }
            if (hasExtrusionDepth()) {
                xVar.a(5, getExtrusionDepth());
            }
            if (hasBoardWidth()) {
                xVar.a(6, getBoardWidth());
            }
            if (hasBoardHeight()) {
                xVar.a(7, getBoardHeight());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i;
            int i2 = this.memoizedSerializedSize;
            if (i2 != -1) {
                return i2;
            }
            int i3 = 0;
            if (hasNumVerticals()) {
                i3 = x.e(1, getNumVerticals()) + 0;
            }
            if (hasNumHorizontals()) {
                i3 += x.e(2, getNumHorizontals());
            }
            Iterator<PolygonVertex> it = getGridPointsList().iterator();
            while (true) {
                i = i3;
                if (!it.hasNext()) {
                    break;
                }
                i3 = x.d(3, it.next()) + i;
            }
            for (JoinCorner d : getJoinCornersList()) {
                i += x.d(4, d);
            }
            if (hasExtrusionDepth()) {
                getExtrusionDepth();
                i += x.d(5);
            }
            if (hasBoardWidth()) {
                getBoardWidth();
                i += x.d(6);
            }
            if (hasBoardHeight()) {
                getBoardHeight();
                i += x.d(7);
            }
            int serializedSize = getUnknownFields().getSerializedSize() + i;
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static TypeOnePrimordialPieceSet parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.TypeOnePrimordialPieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.TypeOnePrimordialPieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.TypeOnePrimordialPieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.TypeOnePrimordialPieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.TypeOnePrimordialPieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$TypeOnePrimordialPieceSet$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static TypeOnePrimordialPieceSet parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static TypeOnePrimordialPieceSet parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.TypeOnePrimordialPieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.TypeOnePrimordialPieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.TypeOnePrimordialPieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.TypeOnePrimordialPieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.TypeOnePrimordialPieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$TypeOnePrimordialPieceSet$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static TypeOnePrimordialPieceSet parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static TypeOnePrimordialPieceSet parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.TypeOnePrimordialPieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.TypeOnePrimordialPieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.TypeOnePrimordialPieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.TypeOnePrimordialPieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.TypeOnePrimordialPieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$TypeOnePrimordialPieceSet$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static TypeOnePrimordialPieceSet parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static TypeOnePrimordialPieceSet parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static TypeOnePrimordialPieceSet parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static TypeOnePrimordialPieceSet parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.jigsaur.Jigsaur.TypeOnePrimordialPieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$TypeOnePrimordialPieceSet$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.TypeOnePrimordialPieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.TypeOnePrimordialPieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.TypeOnePrimordialPieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.TypeOnePrimordialPieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.TypeOnePrimordialPieceSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$TypeOnePrimordialPieceSet$Builder */
        public static TypeOnePrimordialPieceSet parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(TypeOnePrimordialPieceSet typeOnePrimordialPieceSet) {
            return newBuilder().mergeFrom(typeOnePrimordialPieceSet);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private TypeOnePrimordialPieceSet result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new TypeOnePrimordialPieceSet((ai) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final TypeOnePrimordialPieceSet internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new TypeOnePrimordialPieceSet((ai) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return TypeOnePrimordialPieceSet.getDescriptor();
            }

            public final TypeOnePrimordialPieceSet getDefaultInstanceForType() {
                return TypeOnePrimordialPieceSet.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final TypeOnePrimordialPieceSet build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public TypeOnePrimordialPieceSet buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final TypeOnePrimordialPieceSet buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.gridPoints_ != Collections.EMPTY_LIST) {
                    List unused = this.result.gridPoints_ = Collections.unmodifiableList(this.result.gridPoints_);
                }
                if (this.result.joinCorners_ != Collections.EMPTY_LIST) {
                    List unused2 = this.result.joinCorners_ = Collections.unmodifiableList(this.result.joinCorners_);
                }
                TypeOnePrimordialPieceSet typeOnePrimordialPieceSet = this.result;
                this.result = null;
                return typeOnePrimordialPieceSet;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof TypeOnePrimordialPieceSet) {
                    return mergeFrom((TypeOnePrimordialPieceSet) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(TypeOnePrimordialPieceSet typeOnePrimordialPieceSet) {
                if (typeOnePrimordialPieceSet != TypeOnePrimordialPieceSet.getDefaultInstance()) {
                    if (typeOnePrimordialPieceSet.hasNumVerticals()) {
                        setNumVerticals(typeOnePrimordialPieceSet.getNumVerticals());
                    }
                    if (typeOnePrimordialPieceSet.hasNumHorizontals()) {
                        setNumHorizontals(typeOnePrimordialPieceSet.getNumHorizontals());
                    }
                    if (!typeOnePrimordialPieceSet.gridPoints_.isEmpty()) {
                        if (this.result.gridPoints_.isEmpty()) {
                            List unused = this.result.gridPoints_ = new ArrayList();
                        }
                        this.result.gridPoints_.addAll(typeOnePrimordialPieceSet.gridPoints_);
                    }
                    if (!typeOnePrimordialPieceSet.joinCorners_.isEmpty()) {
                        if (this.result.joinCorners_.isEmpty()) {
                            List unused2 = this.result.joinCorners_ = new ArrayList();
                        }
                        this.result.joinCorners_.addAll(typeOnePrimordialPieceSet.joinCorners_);
                    }
                    if (typeOnePrimordialPieceSet.hasExtrusionDepth()) {
                        setExtrusionDepth(typeOnePrimordialPieceSet.getExtrusionDepth());
                    }
                    if (typeOnePrimordialPieceSet.hasBoardWidth()) {
                        setBoardWidth(typeOnePrimordialPieceSet.getBoardWidth());
                    }
                    if (typeOnePrimordialPieceSet.hasBoardHeight()) {
                        setBoardHeight(typeOnePrimordialPieceSet.getBoardHeight());
                    }
                    mergeUnknownFields(typeOnePrimordialPieceSet.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 8:
                            setNumVerticals(hVar.l());
                            break;
                        case 16:
                            setNumHorizontals(hVar.l());
                            break;
                        case 26:
                            PolygonVertex.Builder newBuilder = PolygonVertex.newBuilder();
                            hVar.a(newBuilder, abVar);
                            addGridPoints(newBuilder.buildPartial());
                            break;
                        case 34:
                            JoinCorner.Builder newBuilder2 = JoinCorner.newBuilder();
                            hVar.a(newBuilder2, abVar);
                            addJoinCorners(newBuilder2.buildPartial());
                            break;
                        case 45:
                            setExtrusionDepth(Float.intBitsToFloat(hVar.p()));
                            break;
                        case 53:
                            setBoardWidth(Float.intBitsToFloat(hVar.p()));
                            break;
                        case 61:
                            setBoardHeight(Float.intBitsToFloat(hVar.p()));
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasNumVerticals() {
                return this.result.hasNumVerticals();
            }

            public final int getNumVerticals() {
                return this.result.getNumVerticals();
            }

            public final Builder setNumVerticals(int i) {
                boolean unused = this.result.hasNumVerticals = true;
                int unused2 = this.result.numVerticals_ = i;
                return this;
            }

            public final Builder clearNumVerticals() {
                boolean unused = this.result.hasNumVerticals = false;
                int unused2 = this.result.numVerticals_ = 0;
                return this;
            }

            public final boolean hasNumHorizontals() {
                return this.result.hasNumHorizontals();
            }

            public final int getNumHorizontals() {
                return this.result.getNumHorizontals();
            }

            public final Builder setNumHorizontals(int i) {
                boolean unused = this.result.hasNumHorizontals = true;
                int unused2 = this.result.numHorizontals_ = i;
                return this;
            }

            public final Builder clearNumHorizontals() {
                boolean unused = this.result.hasNumHorizontals = false;
                int unused2 = this.result.numHorizontals_ = 0;
                return this;
            }

            public final List<PolygonVertex> getGridPointsList() {
                return Collections.unmodifiableList(this.result.gridPoints_);
            }

            public final int getGridPointsCount() {
                return this.result.getGridPointsCount();
            }

            public final PolygonVertex getGridPoints(int i) {
                return this.result.getGridPoints(i);
            }

            public final Builder setGridPoints(int i, PolygonVertex polygonVertex) {
                if (polygonVertex == null) {
                    throw new NullPointerException();
                }
                this.result.gridPoints_.set(i, polygonVertex);
                return this;
            }

            public final Builder setGridPoints(int i, PolygonVertex.Builder builder) {
                this.result.gridPoints_.set(i, builder.build());
                return this;
            }

            public final Builder addGridPoints(PolygonVertex polygonVertex) {
                if (polygonVertex == null) {
                    throw new NullPointerException();
                }
                if (this.result.gridPoints_.isEmpty()) {
                    List unused = this.result.gridPoints_ = new ArrayList();
                }
                this.result.gridPoints_.add(polygonVertex);
                return this;
            }

            public final Builder addGridPoints(PolygonVertex.Builder builder) {
                if (this.result.gridPoints_.isEmpty()) {
                    List unused = this.result.gridPoints_ = new ArrayList();
                }
                this.result.gridPoints_.add(builder.build());
                return this;
            }

            public final Builder addAllGridPoints(Iterable<? extends PolygonVertex> iterable) {
                if (this.result.gridPoints_.isEmpty()) {
                    List unused = this.result.gridPoints_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.gridPoints_);
                return this;
            }

            public final Builder clearGridPoints() {
                List unused = this.result.gridPoints_ = Collections.emptyList();
                return this;
            }

            public final List<JoinCorner> getJoinCornersList() {
                return Collections.unmodifiableList(this.result.joinCorners_);
            }

            public final int getJoinCornersCount() {
                return this.result.getJoinCornersCount();
            }

            public final JoinCorner getJoinCorners(int i) {
                return this.result.getJoinCorners(i);
            }

            public final Builder setJoinCorners(int i, JoinCorner joinCorner) {
                if (joinCorner == null) {
                    throw new NullPointerException();
                }
                this.result.joinCorners_.set(i, joinCorner);
                return this;
            }

            public final Builder setJoinCorners(int i, JoinCorner.Builder builder) {
                this.result.joinCorners_.set(i, builder.build());
                return this;
            }

            public final Builder addJoinCorners(JoinCorner joinCorner) {
                if (joinCorner == null) {
                    throw new NullPointerException();
                }
                if (this.result.joinCorners_.isEmpty()) {
                    List unused = this.result.joinCorners_ = new ArrayList();
                }
                this.result.joinCorners_.add(joinCorner);
                return this;
            }

            public final Builder addJoinCorners(JoinCorner.Builder builder) {
                if (this.result.joinCorners_.isEmpty()) {
                    List unused = this.result.joinCorners_ = new ArrayList();
                }
                this.result.joinCorners_.add(builder.build());
                return this;
            }

            public final Builder addAllJoinCorners(Iterable<? extends JoinCorner> iterable) {
                if (this.result.joinCorners_.isEmpty()) {
                    List unused = this.result.joinCorners_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.joinCorners_);
                return this;
            }

            public final Builder clearJoinCorners() {
                List unused = this.result.joinCorners_ = Collections.emptyList();
                return this;
            }

            public final boolean hasExtrusionDepth() {
                return this.result.hasExtrusionDepth();
            }

            public final float getExtrusionDepth() {
                return this.result.getExtrusionDepth();
            }

            public final Builder setExtrusionDepth(float f) {
                boolean unused = this.result.hasExtrusionDepth = true;
                float unused2 = this.result.extrusionDepth_ = f;
                return this;
            }

            public final Builder clearExtrusionDepth() {
                boolean unused = this.result.hasExtrusionDepth = false;
                float unused2 = this.result.extrusionDepth_ = 0.0f;
                return this;
            }

            public final boolean hasBoardWidth() {
                return this.result.hasBoardWidth();
            }

            public final float getBoardWidth() {
                return this.result.getBoardWidth();
            }

            public final Builder setBoardWidth(float f) {
                boolean unused = this.result.hasBoardWidth = true;
                float unused2 = this.result.boardWidth_ = f;
                return this;
            }

            public final Builder clearBoardWidth() {
                boolean unused = this.result.hasBoardWidth = false;
                float unused2 = this.result.boardWidth_ = 0.0f;
                return this;
            }

            public final boolean hasBoardHeight() {
                return this.result.hasBoardHeight();
            }

            public final float getBoardHeight() {
                return this.result.getBoardHeight();
            }

            public final Builder setBoardHeight(float f) {
                boolean unused = this.result.hasBoardHeight = true;
                float unused2 = this.result.boardHeight_ = f;
                return this;
            }

            public final Builder clearBoardHeight() {
                boolean unused = this.result.hasBoardHeight = false;
                float unused2 = this.result.boardHeight_ = 0.0f;
                return this;
            }
        }
    }

    public static final class PieceGlobConfiguration extends GeneratedMessage {
        public static final int ANGLE_FIELD_NUMBER = 3;
        public static final int CENTER_FIELD_NUMBER = 2;
        public static final int PIECE_IDS_FIELD_NUMBER = 1;
        public static final int STATE_FIELD_NUMBER = 4;
        private static final PieceGlobConfiguration defaultInstance;
        /* access modifiers changed from: private */
        public float angle_;
        /* access modifiers changed from: private */
        public Vertex3 center_;
        /* access modifiers changed from: private */
        public boolean hasAngle;
        /* access modifiers changed from: private */
        public boolean hasCenter;
        /* access modifiers changed from: private */
        public boolean hasState;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public List<Integer> pieceIds_;
        /* access modifiers changed from: private */
        public a state_;

        /* synthetic */ PieceGlobConfiguration(ai aiVar) {
            this();
        }

        private PieceGlobConfiguration() {
            this.pieceIds_ = Collections.emptyList();
            this.angle_ = 0.0f;
            this.memoizedSerializedSize = -1;
            initFields();
        }

        private PieceGlobConfiguration(boolean z) {
            this.pieceIds_ = Collections.emptyList();
            this.angle_ = 0.0f;
            this.memoizedSerializedSize = -1;
        }

        public static PieceGlobConfiguration getDefaultInstance() {
            return defaultInstance;
        }

        public final PieceGlobConfiguration getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Jigsaur.s;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Jigsaur.t;
        }

        public enum a implements v {
            INVALID(0, 0),
            RAISED(1, 1),
            SOLVED(2, 2),
            NORMAL(3, 3);
            
            private static g.b<a> e = new l();
            private static final a[] f = {INVALID, RAISED, SOLVED, NORMAL};
            private final int g;
            private final int h;

            public final int d_() {
                return this.h;
            }

            public static a a(int i2) {
                switch (i2) {
                    case Transform.POS_X:
                        return INVALID;
                    case 1:
                        return RAISED;
                    case 2:
                        return SOLVED;
                    case 3:
                        return NORMAL;
                    default:
                        return null;
                }
            }

            private a(int i2, int i3) {
                this.g = i2;
                this.h = i3;
            }
        }

        public final List<Integer> getPieceIdsList() {
            return this.pieceIds_;
        }

        public final int getPieceIdsCount() {
            return this.pieceIds_.size();
        }

        public final int getPieceIds(int i) {
            return this.pieceIds_.get(i).intValue();
        }

        public final boolean hasCenter() {
            return this.hasCenter;
        }

        public final Vertex3 getCenter() {
            return this.center_;
        }

        public final boolean hasAngle() {
            return this.hasAngle;
        }

        public final float getAngle() {
            return this.angle_;
        }

        public final boolean hasState() {
            return this.hasState;
        }

        public final a getState() {
            return this.state_;
        }

        private void initFields() {
            this.center_ = Vertex3.getDefaultInstance();
            this.state_ = a.INVALID;
        }

        public final boolean isInitialized() {
            if (this.hasCenter && this.hasAngle && this.hasState && getCenter().isInitialized()) {
                return true;
            }
            return false;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            for (Integer intValue : getPieceIdsList()) {
                xVar.b(1, intValue.intValue());
            }
            if (hasCenter()) {
                xVar.b(2, getCenter());
            }
            if (hasAngle()) {
                xVar.a(3, getAngle());
            }
            if (hasState()) {
                xVar.c(4, getState().d_());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i;
            int i2 = this.memoizedSerializedSize;
            if (i2 != -1) {
                return i2;
            }
            int i3 = 0;
            Iterator<Integer> it = getPieceIdsList().iterator();
            while (true) {
                i = i3;
                if (!it.hasNext()) {
                    break;
                }
                i3 = x.i(it.next().intValue()) + i;
            }
            int size = i + 0 + (getPieceIdsList().size() * 1);
            if (hasCenter()) {
                size += x.d(2, getCenter());
            }
            if (hasAngle()) {
                getAngle();
                size += x.d(3);
            }
            if (hasState()) {
                size += x.f(4, getState().d_());
            }
            int serializedSize = size + getUnknownFields().getSerializedSize();
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static PieceGlobConfiguration parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PieceGlobConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PieceGlobConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PieceGlobConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PieceGlobConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.PieceGlobConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PieceGlobConfiguration$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static PieceGlobConfiguration parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static PieceGlobConfiguration parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PieceGlobConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PieceGlobConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PieceGlobConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PieceGlobConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.PieceGlobConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PieceGlobConfiguration$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static PieceGlobConfiguration parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static PieceGlobConfiguration parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PieceGlobConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PieceGlobConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PieceGlobConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PieceGlobConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.PieceGlobConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PieceGlobConfiguration$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static PieceGlobConfiguration parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static PieceGlobConfiguration parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static PieceGlobConfiguration parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static PieceGlobConfiguration parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.jigsaur.Jigsaur.PieceGlobConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PieceGlobConfiguration$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PieceGlobConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PieceGlobConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PieceGlobConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PieceGlobConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PieceGlobConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PieceGlobConfiguration$Builder */
        public static PieceGlobConfiguration parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(PieceGlobConfiguration pieceGlobConfiguration) {
            return newBuilder().mergeFrom(pieceGlobConfiguration);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private PieceGlobConfiguration result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new PieceGlobConfiguration((ai) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final PieceGlobConfiguration internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new PieceGlobConfiguration((ai) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return PieceGlobConfiguration.getDescriptor();
            }

            public final PieceGlobConfiguration getDefaultInstanceForType() {
                return PieceGlobConfiguration.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final PieceGlobConfiguration build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public PieceGlobConfiguration buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final PieceGlobConfiguration buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.pieceIds_ != Collections.EMPTY_LIST) {
                    List unused = this.result.pieceIds_ = Collections.unmodifiableList(this.result.pieceIds_);
                }
                PieceGlobConfiguration pieceGlobConfiguration = this.result;
                this.result = null;
                return pieceGlobConfiguration;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof PieceGlobConfiguration) {
                    return mergeFrom((PieceGlobConfiguration) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(PieceGlobConfiguration pieceGlobConfiguration) {
                if (pieceGlobConfiguration != PieceGlobConfiguration.getDefaultInstance()) {
                    if (!pieceGlobConfiguration.pieceIds_.isEmpty()) {
                        if (this.result.pieceIds_.isEmpty()) {
                            List unused = this.result.pieceIds_ = new ArrayList();
                        }
                        this.result.pieceIds_.addAll(pieceGlobConfiguration.pieceIds_);
                    }
                    if (pieceGlobConfiguration.hasCenter()) {
                        mergeCenter(pieceGlobConfiguration.getCenter());
                    }
                    if (pieceGlobConfiguration.hasAngle()) {
                        setAngle(pieceGlobConfiguration.getAngle());
                    }
                    if (pieceGlobConfiguration.hasState()) {
                        setState(pieceGlobConfiguration.getState());
                    }
                    mergeUnknownFields(pieceGlobConfiguration.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = com.google.protobuf.a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 8:
                            addPieceIds(hVar.l());
                            break;
                        case 10:
                            int c = hVar.c(hVar.n());
                            while (hVar.r() > 0) {
                                addPieceIds(hVar.l());
                            }
                            hVar.d(c);
                            break;
                        case DescriptorProtos.FileOptions.PY_GENERIC_SERVICES_FIELD_NUMBER:
                            Vertex3.Builder newBuilder = Vertex3.newBuilder();
                            if (hasCenter()) {
                                newBuilder.mergeFrom(getCenter());
                            }
                            hVar.a(newBuilder, abVar);
                            setCenter(newBuilder.buildPartial());
                            break;
                        case 29:
                            setAngle(hVar.c());
                            break;
                        case 32:
                            int m = hVar.m();
                            a a3 = a.a(m);
                            if (a3 != null) {
                                setState(a3);
                                break;
                            } else {
                                a.a(4, m);
                                break;
                            }
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final List<Integer> getPieceIdsList() {
                return Collections.unmodifiableList(this.result.pieceIds_);
            }

            public final int getPieceIdsCount() {
                return this.result.getPieceIdsCount();
            }

            public final int getPieceIds(int i) {
                return this.result.getPieceIds(i);
            }

            public final Builder setPieceIds(int i, int i2) {
                this.result.pieceIds_.set(i, Integer.valueOf(i2));
                return this;
            }

            public final Builder addPieceIds(int i) {
                if (this.result.pieceIds_.isEmpty()) {
                    List unused = this.result.pieceIds_ = new ArrayList();
                }
                this.result.pieceIds_.add(Integer.valueOf(i));
                return this;
            }

            public final Builder addAllPieceIds(Iterable<? extends Integer> iterable) {
                if (this.result.pieceIds_.isEmpty()) {
                    List unused = this.result.pieceIds_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.pieceIds_);
                return this;
            }

            public final Builder clearPieceIds() {
                List unused = this.result.pieceIds_ = Collections.emptyList();
                return this;
            }

            public final boolean hasCenter() {
                return this.result.hasCenter();
            }

            public final Vertex3 getCenter() {
                return this.result.getCenter();
            }

            public final Builder setCenter(Vertex3 vertex3) {
                if (vertex3 == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasCenter = true;
                Vertex3 unused2 = this.result.center_ = vertex3;
                return this;
            }

            public final Builder setCenter(Vertex3.Builder builder) {
                boolean unused = this.result.hasCenter = true;
                Vertex3 unused2 = this.result.center_ = builder.build();
                return this;
            }

            public final Builder mergeCenter(Vertex3 vertex3) {
                if (!this.result.hasCenter() || this.result.center_ == Vertex3.getDefaultInstance()) {
                    Vertex3 unused = this.result.center_ = vertex3;
                } else {
                    Vertex3 unused2 = this.result.center_ = Vertex3.newBuilder(this.result.center_).mergeFrom(vertex3).buildPartial();
                }
                boolean unused3 = this.result.hasCenter = true;
                return this;
            }

            public final Builder clearCenter() {
                boolean unused = this.result.hasCenter = false;
                Vertex3 unused2 = this.result.center_ = Vertex3.getDefaultInstance();
                return this;
            }

            public final boolean hasAngle() {
                return this.result.hasAngle();
            }

            public final float getAngle() {
                return this.result.getAngle();
            }

            public final Builder setAngle(float f) {
                boolean unused = this.result.hasAngle = true;
                float unused2 = this.result.angle_ = f;
                return this;
            }

            public final Builder clearAngle() {
                boolean unused = this.result.hasAngle = false;
                float unused2 = this.result.angle_ = 0.0f;
                return this;
            }

            public final boolean hasState() {
                return this.result.hasState();
            }

            public final a getState() {
                return this.result.getState();
            }

            public final Builder setState(a aVar) {
                if (aVar == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasState = true;
                a unused2 = this.result.state_ = aVar;
                return this;
            }

            public final Builder clearState() {
                boolean unused = this.result.hasState = false;
                a unused2 = this.result.state_ = a.INVALID;
                return this;
            }
        }

        static {
            PieceGlobConfiguration pieceGlobConfiguration = new PieceGlobConfiguration(true);
            defaultInstance = pieceGlobConfiguration;
            pieceGlobConfiguration.initFields();
        }
    }

    public static final class WorkspaceConfiguration extends GeneratedMessage {
        public static final int GLOBS_FIELD_NUMBER = 1;
        private static final WorkspaceConfiguration defaultInstance = new WorkspaceConfiguration(true);
        /* access modifiers changed from: private */
        public List<PieceGlobConfiguration> globs_;
        private int memoizedSerializedSize;

        /* synthetic */ WorkspaceConfiguration(ai aiVar) {
            this();
        }

        private WorkspaceConfiguration() {
            this.globs_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        private WorkspaceConfiguration(boolean z) {
            this.globs_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        public static WorkspaceConfiguration getDefaultInstance() {
            return defaultInstance;
        }

        public final WorkspaceConfiguration getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Jigsaur.u;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Jigsaur.v;
        }

        public final List<PieceGlobConfiguration> getGlobsList() {
            return this.globs_;
        }

        public final int getGlobsCount() {
            return this.globs_.size();
        }

        public final PieceGlobConfiguration getGlobs(int i) {
            return this.globs_.get(i);
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            for (PieceGlobConfiguration isInitialized : getGlobsList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            for (PieceGlobConfiguration b : getGlobsList()) {
                xVar.b(1, b);
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            Iterator<PieceGlobConfiguration> it = getGlobsList().iterator();
            while (true) {
                int i3 = i2;
                if (it.hasNext()) {
                    i2 = x.d(1, it.next()) + i3;
                } else {
                    int serializedSize = getUnknownFields().getSerializedSize() + i3;
                    this.memoizedSerializedSize = serializedSize;
                    return serializedSize;
                }
            }
        }

        public static WorkspaceConfiguration parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.WorkspaceConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.WorkspaceConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.WorkspaceConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.WorkspaceConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.WorkspaceConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$WorkspaceConfiguration$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static WorkspaceConfiguration parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static WorkspaceConfiguration parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.WorkspaceConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.WorkspaceConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.WorkspaceConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.WorkspaceConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.WorkspaceConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$WorkspaceConfiguration$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static WorkspaceConfiguration parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static WorkspaceConfiguration parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.WorkspaceConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.WorkspaceConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.WorkspaceConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.WorkspaceConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.WorkspaceConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$WorkspaceConfiguration$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static WorkspaceConfiguration parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static WorkspaceConfiguration parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static WorkspaceConfiguration parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static WorkspaceConfiguration parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.jigsaur.Jigsaur.WorkspaceConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$WorkspaceConfiguration$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.WorkspaceConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.WorkspaceConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.WorkspaceConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.WorkspaceConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.WorkspaceConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$WorkspaceConfiguration$Builder */
        public static WorkspaceConfiguration parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(WorkspaceConfiguration workspaceConfiguration) {
            return newBuilder().mergeFrom(workspaceConfiguration);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private WorkspaceConfiguration result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new WorkspaceConfiguration((ai) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final WorkspaceConfiguration internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new WorkspaceConfiguration((ai) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return WorkspaceConfiguration.getDescriptor();
            }

            public final WorkspaceConfiguration getDefaultInstanceForType() {
                return WorkspaceConfiguration.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final WorkspaceConfiguration build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public WorkspaceConfiguration buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final WorkspaceConfiguration buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.globs_ != Collections.EMPTY_LIST) {
                    List unused = this.result.globs_ = Collections.unmodifiableList(this.result.globs_);
                }
                WorkspaceConfiguration workspaceConfiguration = this.result;
                this.result = null;
                return workspaceConfiguration;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof WorkspaceConfiguration) {
                    return mergeFrom((WorkspaceConfiguration) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(WorkspaceConfiguration workspaceConfiguration) {
                if (workspaceConfiguration != WorkspaceConfiguration.getDefaultInstance()) {
                    if (!workspaceConfiguration.globs_.isEmpty()) {
                        if (this.result.globs_.isEmpty()) {
                            List unused = this.result.globs_ = new ArrayList();
                        }
                        this.result.globs_.addAll(workspaceConfiguration.globs_);
                    }
                    mergeUnknownFields(workspaceConfiguration.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 10:
                            PieceGlobConfiguration.Builder newBuilder = PieceGlobConfiguration.newBuilder();
                            hVar.a(newBuilder, abVar);
                            addGlobs(newBuilder.buildPartial());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final List<PieceGlobConfiguration> getGlobsList() {
                return Collections.unmodifiableList(this.result.globs_);
            }

            public final int getGlobsCount() {
                return this.result.getGlobsCount();
            }

            public final PieceGlobConfiguration getGlobs(int i) {
                return this.result.getGlobs(i);
            }

            public final Builder setGlobs(int i, PieceGlobConfiguration pieceGlobConfiguration) {
                if (pieceGlobConfiguration == null) {
                    throw new NullPointerException();
                }
                this.result.globs_.set(i, pieceGlobConfiguration);
                return this;
            }

            public final Builder setGlobs(int i, PieceGlobConfiguration.Builder builder) {
                this.result.globs_.set(i, builder.build());
                return this;
            }

            public final Builder addGlobs(PieceGlobConfiguration pieceGlobConfiguration) {
                if (pieceGlobConfiguration == null) {
                    throw new NullPointerException();
                }
                if (this.result.globs_.isEmpty()) {
                    List unused = this.result.globs_ = new ArrayList();
                }
                this.result.globs_.add(pieceGlobConfiguration);
                return this;
            }

            public final Builder addGlobs(PieceGlobConfiguration.Builder builder) {
                if (this.result.globs_.isEmpty()) {
                    List unused = this.result.globs_ = new ArrayList();
                }
                this.result.globs_.add(builder.build());
                return this;
            }

            public final Builder addAllGlobs(Iterable<? extends PieceGlobConfiguration> iterable) {
                if (this.result.globs_.isEmpty()) {
                    List unused = this.result.globs_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.globs_);
                return this;
            }

            public final Builder clearGlobs() {
                List unused = this.result.globs_ = Collections.emptyList();
                return this;
            }
        }
    }

    public static final class SavedConfiguration extends GeneratedMessage {
        public static final int CAMERA_CENTER_FIELD_NUMBER = 3;
        public static final int NUM_PIECES_FIELD_NUMBER = 1;
        public static final int SCALE_FACTOR_FIELD_NUMBER = 4;
        public static final int WORKSPACES_FIELD_NUMBER = 2;
        private static final SavedConfiguration defaultInstance;
        /* access modifiers changed from: private */
        public PolygonVertex cameraCenter_;
        /* access modifiers changed from: private */
        public boolean hasCameraCenter;
        /* access modifiers changed from: private */
        public boolean hasNumPieces;
        /* access modifiers changed from: private */
        public boolean hasScaleFactor;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public int numPieces_;
        /* access modifiers changed from: private */
        public float scaleFactor_;
        /* access modifiers changed from: private */
        public List<WorkspaceConfiguration> workspaces_;

        /* synthetic */ SavedConfiguration(ai aiVar) {
            this();
        }

        private SavedConfiguration() {
            this.numPieces_ = 0;
            this.workspaces_ = Collections.emptyList();
            this.scaleFactor_ = 0.0f;
            this.memoizedSerializedSize = -1;
            this.cameraCenter_ = PolygonVertex.getDefaultInstance();
        }

        private SavedConfiguration(boolean z) {
            this.numPieces_ = 0;
            this.workspaces_ = Collections.emptyList();
            this.scaleFactor_ = 0.0f;
            this.memoizedSerializedSize = -1;
        }

        public static SavedConfiguration getDefaultInstance() {
            return defaultInstance;
        }

        public final SavedConfiguration getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Jigsaur.w;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Jigsaur.x;
        }

        public final boolean hasNumPieces() {
            return this.hasNumPieces;
        }

        public final int getNumPieces() {
            return this.numPieces_;
        }

        public final List<WorkspaceConfiguration> getWorkspacesList() {
            return this.workspaces_;
        }

        public final int getWorkspacesCount() {
            return this.workspaces_.size();
        }

        public final WorkspaceConfiguration getWorkspaces(int i) {
            return this.workspaces_.get(i);
        }

        public final boolean hasCameraCenter() {
            return this.hasCameraCenter;
        }

        public final PolygonVertex getCameraCenter() {
            return this.cameraCenter_;
        }

        public final boolean hasScaleFactor() {
            return this.hasScaleFactor;
        }

        public final float getScaleFactor() {
            return this.scaleFactor_;
        }

        private void initFields() {
            this.cameraCenter_ = PolygonVertex.getDefaultInstance();
        }

        public final boolean isInitialized() {
            if (!this.hasNumPieces) {
                return false;
            }
            if (!this.hasCameraCenter) {
                return false;
            }
            if (!this.hasScaleFactor) {
                return false;
            }
            for (WorkspaceConfiguration isInitialized : getWorkspacesList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            if (!getCameraCenter().isInitialized()) {
                return false;
            }
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasNumPieces()) {
                xVar.b(1, getNumPieces());
            }
            for (WorkspaceConfiguration b : getWorkspacesList()) {
                xVar.b(2, b);
            }
            if (hasCameraCenter()) {
                xVar.b(3, getCameraCenter());
            }
            if (hasScaleFactor()) {
                xVar.a(4, getScaleFactor());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i;
            int i2 = this.memoizedSerializedSize;
            if (i2 != -1) {
                return i2;
            }
            int i3 = 0;
            if (hasNumPieces()) {
                i3 = x.e(1, getNumPieces()) + 0;
            }
            Iterator<WorkspaceConfiguration> it = getWorkspacesList().iterator();
            while (true) {
                i = i3;
                if (!it.hasNext()) {
                    break;
                }
                i3 = x.d(2, it.next()) + i;
            }
            if (hasCameraCenter()) {
                i += x.d(3, getCameraCenter());
            }
            if (hasScaleFactor()) {
                getScaleFactor();
                i += x.d(4);
            }
            int serializedSize = getUnknownFields().getSerializedSize() + i;
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static SavedConfiguration parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.SavedConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.SavedConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.SavedConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.SavedConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.SavedConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$SavedConfiguration$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static SavedConfiguration parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static SavedConfiguration parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.SavedConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.SavedConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.SavedConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.SavedConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.SavedConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$SavedConfiguration$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static SavedConfiguration parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static SavedConfiguration parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.SavedConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.SavedConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.SavedConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.SavedConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.SavedConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$SavedConfiguration$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static SavedConfiguration parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static SavedConfiguration parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static SavedConfiguration parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static SavedConfiguration parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.jigsaur.Jigsaur.SavedConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$SavedConfiguration$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.SavedConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.SavedConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.SavedConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.SavedConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.SavedConfiguration.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$SavedConfiguration$Builder */
        public static SavedConfiguration parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(SavedConfiguration savedConfiguration) {
            return newBuilder().mergeFrom(savedConfiguration);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private SavedConfiguration result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new SavedConfiguration((ai) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final SavedConfiguration internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new SavedConfiguration((ai) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return SavedConfiguration.getDescriptor();
            }

            public final SavedConfiguration getDefaultInstanceForType() {
                return SavedConfiguration.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final SavedConfiguration build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public SavedConfiguration buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final SavedConfiguration buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.workspaces_ != Collections.EMPTY_LIST) {
                    List unused = this.result.workspaces_ = Collections.unmodifiableList(this.result.workspaces_);
                }
                SavedConfiguration savedConfiguration = this.result;
                this.result = null;
                return savedConfiguration;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof SavedConfiguration) {
                    return mergeFrom((SavedConfiguration) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(SavedConfiguration savedConfiguration) {
                if (savedConfiguration != SavedConfiguration.getDefaultInstance()) {
                    if (savedConfiguration.hasNumPieces()) {
                        setNumPieces(savedConfiguration.getNumPieces());
                    }
                    if (!savedConfiguration.workspaces_.isEmpty()) {
                        if (this.result.workspaces_.isEmpty()) {
                            List unused = this.result.workspaces_ = new ArrayList();
                        }
                        this.result.workspaces_.addAll(savedConfiguration.workspaces_);
                    }
                    if (savedConfiguration.hasCameraCenter()) {
                        mergeCameraCenter(savedConfiguration.getCameraCenter());
                    }
                    if (savedConfiguration.hasScaleFactor()) {
                        setScaleFactor(savedConfiguration.getScaleFactor());
                    }
                    mergeUnknownFields(savedConfiguration.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 8:
                            setNumPieces(hVar.l());
                            break;
                        case DescriptorProtos.FileOptions.PY_GENERIC_SERVICES_FIELD_NUMBER:
                            WorkspaceConfiguration.Builder newBuilder = WorkspaceConfiguration.newBuilder();
                            hVar.a(newBuilder, abVar);
                            addWorkspaces(newBuilder.buildPartial());
                            break;
                        case 26:
                            PolygonVertex.Builder newBuilder2 = PolygonVertex.newBuilder();
                            if (hasCameraCenter()) {
                                newBuilder2.mergeFrom(getCameraCenter());
                            }
                            hVar.a(newBuilder2, abVar);
                            setCameraCenter(newBuilder2.buildPartial());
                            break;
                        case 37:
                            setScaleFactor(hVar.c());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasNumPieces() {
                return this.result.hasNumPieces();
            }

            public final int getNumPieces() {
                return this.result.getNumPieces();
            }

            public final Builder setNumPieces(int i) {
                boolean unused = this.result.hasNumPieces = true;
                int unused2 = this.result.numPieces_ = i;
                return this;
            }

            public final Builder clearNumPieces() {
                boolean unused = this.result.hasNumPieces = false;
                int unused2 = this.result.numPieces_ = 0;
                return this;
            }

            public final List<WorkspaceConfiguration> getWorkspacesList() {
                return Collections.unmodifiableList(this.result.workspaces_);
            }

            public final int getWorkspacesCount() {
                return this.result.getWorkspacesCount();
            }

            public final WorkspaceConfiguration getWorkspaces(int i) {
                return this.result.getWorkspaces(i);
            }

            public final Builder setWorkspaces(int i, WorkspaceConfiguration workspaceConfiguration) {
                if (workspaceConfiguration == null) {
                    throw new NullPointerException();
                }
                this.result.workspaces_.set(i, workspaceConfiguration);
                return this;
            }

            public final Builder setWorkspaces(int i, WorkspaceConfiguration.Builder builder) {
                this.result.workspaces_.set(i, builder.build());
                return this;
            }

            public final Builder addWorkspaces(WorkspaceConfiguration workspaceConfiguration) {
                if (workspaceConfiguration == null) {
                    throw new NullPointerException();
                }
                if (this.result.workspaces_.isEmpty()) {
                    List unused = this.result.workspaces_ = new ArrayList();
                }
                this.result.workspaces_.add(workspaceConfiguration);
                return this;
            }

            public final Builder addWorkspaces(WorkspaceConfiguration.Builder builder) {
                if (this.result.workspaces_.isEmpty()) {
                    List unused = this.result.workspaces_ = new ArrayList();
                }
                this.result.workspaces_.add(builder.build());
                return this;
            }

            public final Builder addAllWorkspaces(Iterable<? extends WorkspaceConfiguration> iterable) {
                if (this.result.workspaces_.isEmpty()) {
                    List unused = this.result.workspaces_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.workspaces_);
                return this;
            }

            public final Builder clearWorkspaces() {
                List unused = this.result.workspaces_ = Collections.emptyList();
                return this;
            }

            public final boolean hasCameraCenter() {
                return this.result.hasCameraCenter();
            }

            public final PolygonVertex getCameraCenter() {
                return this.result.getCameraCenter();
            }

            public final Builder setCameraCenter(PolygonVertex polygonVertex) {
                if (polygonVertex == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasCameraCenter = true;
                PolygonVertex unused2 = this.result.cameraCenter_ = polygonVertex;
                return this;
            }

            public final Builder setCameraCenter(PolygonVertex.Builder builder) {
                boolean unused = this.result.hasCameraCenter = true;
                PolygonVertex unused2 = this.result.cameraCenter_ = builder.build();
                return this;
            }

            public final Builder mergeCameraCenter(PolygonVertex polygonVertex) {
                if (!this.result.hasCameraCenter() || this.result.cameraCenter_ == PolygonVertex.getDefaultInstance()) {
                    PolygonVertex unused = this.result.cameraCenter_ = polygonVertex;
                } else {
                    PolygonVertex unused2 = this.result.cameraCenter_ = PolygonVertex.newBuilder(this.result.cameraCenter_).mergeFrom(polygonVertex).buildPartial();
                }
                boolean unused3 = this.result.hasCameraCenter = true;
                return this;
            }

            public final Builder clearCameraCenter() {
                boolean unused = this.result.hasCameraCenter = false;
                PolygonVertex unused2 = this.result.cameraCenter_ = PolygonVertex.getDefaultInstance();
                return this;
            }

            public final boolean hasScaleFactor() {
                return this.result.hasScaleFactor();
            }

            public final float getScaleFactor() {
                return this.result.getScaleFactor();
            }

            public final Builder setScaleFactor(float f) {
                boolean unused = this.result.hasScaleFactor = true;
                float unused2 = this.result.scaleFactor_ = f;
                return this;
            }

            public final Builder clearScaleFactor() {
                boolean unused = this.result.hasScaleFactor = false;
                float unused2 = this.result.scaleFactor_ = 0.0f;
                return this;
            }
        }

        static {
            SavedConfiguration savedConfiguration = new SavedConfiguration(true);
            defaultInstance = savedConfiguration;
            savedConfiguration.cameraCenter_ = PolygonVertex.getDefaultInstance();
        }
    }

    public static final class PuzzleImageInfo extends GeneratedMessage {
        public static final int ACQUISITION_FIELD_NUMBER = 6;
        public static final int ATTRIBUTION_FIELD_NUMBER = 5;
        public static final int FPS_FIELD_NUMBER = 3;
        public static final int NUM_FRAMES_FIELD_NUMBER = 2;
        public static final int PUZZLE_IMAGE_HEIGHT_FIELD_NUMBER = 11;
        public static final int PUZZLE_IMAGE_WIDTH_FIELD_NUMBER = 10;
        public static final int RESOURCE_NAME_FIELD_NUMBER = 4;
        public static final int TIME_BEGIN_FIELD_NUMBER = 7;
        public static final int TIME_END_FIELD_NUMBER = 8;
        public static final int TYPE_FIELD_NUMBER = 1;
        public static final int UID_FIELD_NUMBER = 9;
        private static final PuzzleImageInfo defaultInstance;
        /* access modifiers changed from: private */
        public String acquisition_;
        /* access modifiers changed from: private */
        public String attribution_;
        /* access modifiers changed from: private */
        public float fps_;
        /* access modifiers changed from: private */
        public boolean hasAcquisition;
        /* access modifiers changed from: private */
        public boolean hasAttribution;
        /* access modifiers changed from: private */
        public boolean hasFps;
        /* access modifiers changed from: private */
        public boolean hasNumFrames;
        /* access modifiers changed from: private */
        public boolean hasPuzzleImageHeight;
        /* access modifiers changed from: private */
        public boolean hasPuzzleImageWidth;
        /* access modifiers changed from: private */
        public boolean hasResourceName;
        /* access modifiers changed from: private */
        public boolean hasTimeBegin;
        /* access modifiers changed from: private */
        public boolean hasTimeEnd;
        /* access modifiers changed from: private */
        public boolean hasType;
        /* access modifiers changed from: private */
        public boolean hasUid;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public int numFrames_;
        /* access modifiers changed from: private */
        public float puzzleImageHeight_;
        /* access modifiers changed from: private */
        public float puzzleImageWidth_;
        /* access modifiers changed from: private */
        public String resourceName_;
        /* access modifiers changed from: private */
        public float timeBegin_;
        /* access modifiers changed from: private */
        public float timeEnd_;
        /* access modifiers changed from: private */
        public a type_;
        /* access modifiers changed from: private */
        public int uid_;

        /* synthetic */ PuzzleImageInfo(ai aiVar) {
            this();
        }

        private PuzzleImageInfo() {
            this.numFrames_ = 0;
            this.fps_ = 0.0f;
            this.resourceName_ = "";
            this.attribution_ = "";
            this.acquisition_ = "";
            this.timeBegin_ = 0.0f;
            this.timeEnd_ = 0.0f;
            this.uid_ = 0;
            this.puzzleImageWidth_ = 0.0f;
            this.puzzleImageHeight_ = 0.0f;
            this.memoizedSerializedSize = -1;
            this.type_ = a.MOVIE;
        }

        private PuzzleImageInfo(boolean z) {
            this.numFrames_ = 0;
            this.fps_ = 0.0f;
            this.resourceName_ = "";
            this.attribution_ = "";
            this.acquisition_ = "";
            this.timeBegin_ = 0.0f;
            this.timeEnd_ = 0.0f;
            this.uid_ = 0;
            this.puzzleImageWidth_ = 0.0f;
            this.puzzleImageHeight_ = 0.0f;
            this.memoizedSerializedSize = -1;
        }

        public static PuzzleImageInfo getDefaultInstance() {
            return defaultInstance;
        }

        public final PuzzleImageInfo getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Jigsaur.y;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Jigsaur.z;
        }

        public enum a implements v {
            MOVIE(0, 1),
            MOVIE_FRAMES(1, 2);
            
            private static g.b<a> c = new am();
            private static final a[] d = {MOVIE, MOVIE_FRAMES};
            private final int e;
            private final int f;

            public final int d_() {
                return this.f;
            }

            public static a a(int i) {
                switch (i) {
                    case 1:
                        return MOVIE;
                    case 2:
                        return MOVIE_FRAMES;
                    default:
                        return null;
                }
            }

            private a(int i, int i2) {
                this.e = i;
                this.f = i2;
            }
        }

        public final boolean hasType() {
            return this.hasType;
        }

        public final a getType() {
            return this.type_;
        }

        public final boolean hasNumFrames() {
            return this.hasNumFrames;
        }

        public final int getNumFrames() {
            return this.numFrames_;
        }

        public final boolean hasFps() {
            return this.hasFps;
        }

        public final float getFps() {
            return this.fps_;
        }

        public final boolean hasResourceName() {
            return this.hasResourceName;
        }

        public final String getResourceName() {
            return this.resourceName_;
        }

        public final boolean hasAttribution() {
            return this.hasAttribution;
        }

        public final String getAttribution() {
            return this.attribution_;
        }

        public final boolean hasAcquisition() {
            return this.hasAcquisition;
        }

        public final String getAcquisition() {
            return this.acquisition_;
        }

        public final boolean hasTimeBegin() {
            return this.hasTimeBegin;
        }

        public final float getTimeBegin() {
            return this.timeBegin_;
        }

        public final boolean hasTimeEnd() {
            return this.hasTimeEnd;
        }

        public final float getTimeEnd() {
            return this.timeEnd_;
        }

        public final boolean hasUid() {
            return this.hasUid;
        }

        public final int getUid() {
            return this.uid_;
        }

        public final boolean hasPuzzleImageWidth() {
            return this.hasPuzzleImageWidth;
        }

        public final float getPuzzleImageWidth() {
            return this.puzzleImageWidth_;
        }

        public final boolean hasPuzzleImageHeight() {
            return this.hasPuzzleImageHeight;
        }

        public final float getPuzzleImageHeight() {
            return this.puzzleImageHeight_;
        }

        private void initFields() {
            this.type_ = a.MOVIE;
        }

        public final boolean isInitialized() {
            if (this.hasType && this.hasNumFrames && this.hasFps && this.hasResourceName && this.hasAttribution && this.hasAcquisition && this.hasUid && this.hasPuzzleImageWidth && this.hasPuzzleImageHeight) {
                return true;
            }
            return false;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasType()) {
                xVar.c(1, getType().d_());
            }
            if (hasNumFrames()) {
                xVar.b(2, getNumFrames());
            }
            if (hasFps()) {
                xVar.a(3, getFps());
            }
            if (hasResourceName()) {
                xVar.a(4, getResourceName());
            }
            if (hasAttribution()) {
                xVar.a(5, getAttribution());
            }
            if (hasAcquisition()) {
                xVar.a(6, getAcquisition());
            }
            if (hasTimeBegin()) {
                xVar.a(7, getTimeBegin());
            }
            if (hasTimeEnd()) {
                xVar.a(8, getTimeEnd());
            }
            if (hasUid()) {
                xVar.b(9, getUid());
            }
            if (hasPuzzleImageWidth()) {
                xVar.a(10, getPuzzleImageWidth());
            }
            if (hasPuzzleImageHeight()) {
                xVar.a(11, getPuzzleImageHeight());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (hasType()) {
                i2 = x.f(1, getType().d_()) + 0;
            }
            if (hasNumFrames()) {
                i2 += x.e(2, getNumFrames());
            }
            if (hasFps()) {
                getFps();
                i2 += x.d(3);
            }
            if (hasResourceName()) {
                i2 += x.b(4, getResourceName());
            }
            if (hasAttribution()) {
                i2 += x.b(5, getAttribution());
            }
            if (hasAcquisition()) {
                i2 += x.b(6, getAcquisition());
            }
            if (hasTimeBegin()) {
                getTimeBegin();
                i2 += x.d(7);
            }
            if (hasTimeEnd()) {
                getTimeEnd();
                i2 += x.d(8);
            }
            if (hasUid()) {
                i2 += x.e(9, getUid());
            }
            if (hasPuzzleImageWidth()) {
                getPuzzleImageWidth();
                i2 += x.d(10);
            }
            if (hasPuzzleImageHeight()) {
                getPuzzleImageHeight();
                i2 += x.d(11);
            }
            int serializedSize = i2 + getUnknownFields().getSerializedSize();
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static PuzzleImageInfo parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PuzzleImageInfo$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static PuzzleImageInfo parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static PuzzleImageInfo parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PuzzleImageInfo$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static PuzzleImageInfo parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static PuzzleImageInfo parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PuzzleImageInfo$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static PuzzleImageInfo parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static PuzzleImageInfo parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static PuzzleImageInfo parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static PuzzleImageInfo parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PuzzleImageInfo$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfo.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PuzzleImageInfo$Builder */
        public static PuzzleImageInfo parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(PuzzleImageInfo puzzleImageInfo) {
            return newBuilder().mergeFrom(puzzleImageInfo);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private PuzzleImageInfo result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new PuzzleImageInfo((ai) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final PuzzleImageInfo internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new PuzzleImageInfo((ai) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return PuzzleImageInfo.getDescriptor();
            }

            public final PuzzleImageInfo getDefaultInstanceForType() {
                return PuzzleImageInfo.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final PuzzleImageInfo build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public PuzzleImageInfo buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final PuzzleImageInfo buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                PuzzleImageInfo puzzleImageInfo = this.result;
                this.result = null;
                return puzzleImageInfo;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof PuzzleImageInfo) {
                    return mergeFrom((PuzzleImageInfo) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(PuzzleImageInfo puzzleImageInfo) {
                if (puzzleImageInfo != PuzzleImageInfo.getDefaultInstance()) {
                    if (puzzleImageInfo.hasType()) {
                        setType(puzzleImageInfo.getType());
                    }
                    if (puzzleImageInfo.hasNumFrames()) {
                        setNumFrames(puzzleImageInfo.getNumFrames());
                    }
                    if (puzzleImageInfo.hasFps()) {
                        setFps(puzzleImageInfo.getFps());
                    }
                    if (puzzleImageInfo.hasResourceName()) {
                        setResourceName(puzzleImageInfo.getResourceName());
                    }
                    if (puzzleImageInfo.hasAttribution()) {
                        setAttribution(puzzleImageInfo.getAttribution());
                    }
                    if (puzzleImageInfo.hasAcquisition()) {
                        setAcquisition(puzzleImageInfo.getAcquisition());
                    }
                    if (puzzleImageInfo.hasTimeBegin()) {
                        setTimeBegin(puzzleImageInfo.getTimeBegin());
                    }
                    if (puzzleImageInfo.hasTimeEnd()) {
                        setTimeEnd(puzzleImageInfo.getTimeEnd());
                    }
                    if (puzzleImageInfo.hasUid()) {
                        setUid(puzzleImageInfo.getUid());
                    }
                    if (puzzleImageInfo.hasPuzzleImageWidth()) {
                        setPuzzleImageWidth(puzzleImageInfo.getPuzzleImageWidth());
                    }
                    if (puzzleImageInfo.hasPuzzleImageHeight()) {
                        setPuzzleImageHeight(puzzleImageInfo.getPuzzleImageHeight());
                    }
                    mergeUnknownFields(puzzleImageInfo.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = com.google.protobuf.a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 8:
                            int m = hVar.m();
                            a a3 = a.a(m);
                            if (a3 != null) {
                                setType(a3);
                                break;
                            } else {
                                a.a(1, m);
                                break;
                            }
                        case 16:
                            setNumFrames(hVar.l());
                            break;
                        case 29:
                            setFps(Float.intBitsToFloat(hVar.p()));
                            break;
                        case 34:
                            setResourceName(hVar.j());
                            break;
                        case 42:
                            setAttribution(hVar.j());
                            break;
                        case 50:
                            setAcquisition(hVar.j());
                            break;
                        case 61:
                            setTimeBegin(Float.intBitsToFloat(hVar.p()));
                            break;
                        case 69:
                            setTimeEnd(Float.intBitsToFloat(hVar.p()));
                            break;
                        case 72:
                            setUid(hVar.l());
                            break;
                        case 85:
                            setPuzzleImageWidth(Float.intBitsToFloat(hVar.p()));
                            break;
                        case 93:
                            setPuzzleImageHeight(Float.intBitsToFloat(hVar.p()));
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasType() {
                return this.result.hasType();
            }

            public final a getType() {
                return this.result.getType();
            }

            public final Builder setType(a aVar) {
                if (aVar == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasType = true;
                a unused2 = this.result.type_ = aVar;
                return this;
            }

            public final Builder clearType() {
                boolean unused = this.result.hasType = false;
                a unused2 = this.result.type_ = a.MOVIE;
                return this;
            }

            public final boolean hasNumFrames() {
                return this.result.hasNumFrames();
            }

            public final int getNumFrames() {
                return this.result.getNumFrames();
            }

            public final Builder setNumFrames(int i) {
                boolean unused = this.result.hasNumFrames = true;
                int unused2 = this.result.numFrames_ = i;
                return this;
            }

            public final Builder clearNumFrames() {
                boolean unused = this.result.hasNumFrames = false;
                int unused2 = this.result.numFrames_ = 0;
                return this;
            }

            public final boolean hasFps() {
                return this.result.hasFps();
            }

            public final float getFps() {
                return this.result.getFps();
            }

            public final Builder setFps(float f) {
                boolean unused = this.result.hasFps = true;
                float unused2 = this.result.fps_ = f;
                return this;
            }

            public final Builder clearFps() {
                boolean unused = this.result.hasFps = false;
                float unused2 = this.result.fps_ = 0.0f;
                return this;
            }

            public final boolean hasResourceName() {
                return this.result.hasResourceName();
            }

            public final String getResourceName() {
                return this.result.getResourceName();
            }

            public final Builder setResourceName(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasResourceName = true;
                String unused2 = this.result.resourceName_ = str;
                return this;
            }

            public final Builder clearResourceName() {
                boolean unused = this.result.hasResourceName = false;
                String unused2 = this.result.resourceName_ = PuzzleImageInfo.getDefaultInstance().getResourceName();
                return this;
            }

            public final boolean hasAttribution() {
                return this.result.hasAttribution();
            }

            public final String getAttribution() {
                return this.result.getAttribution();
            }

            public final Builder setAttribution(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasAttribution = true;
                String unused2 = this.result.attribution_ = str;
                return this;
            }

            public final Builder clearAttribution() {
                boolean unused = this.result.hasAttribution = false;
                String unused2 = this.result.attribution_ = PuzzleImageInfo.getDefaultInstance().getAttribution();
                return this;
            }

            public final boolean hasAcquisition() {
                return this.result.hasAcquisition();
            }

            public final String getAcquisition() {
                return this.result.getAcquisition();
            }

            public final Builder setAcquisition(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasAcquisition = true;
                String unused2 = this.result.acquisition_ = str;
                return this;
            }

            public final Builder clearAcquisition() {
                boolean unused = this.result.hasAcquisition = false;
                String unused2 = this.result.acquisition_ = PuzzleImageInfo.getDefaultInstance().getAcquisition();
                return this;
            }

            public final boolean hasTimeBegin() {
                return this.result.hasTimeBegin();
            }

            public final float getTimeBegin() {
                return this.result.getTimeBegin();
            }

            public final Builder setTimeBegin(float f) {
                boolean unused = this.result.hasTimeBegin = true;
                float unused2 = this.result.timeBegin_ = f;
                return this;
            }

            public final Builder clearTimeBegin() {
                boolean unused = this.result.hasTimeBegin = false;
                float unused2 = this.result.timeBegin_ = 0.0f;
                return this;
            }

            public final boolean hasTimeEnd() {
                return this.result.hasTimeEnd();
            }

            public final float getTimeEnd() {
                return this.result.getTimeEnd();
            }

            public final Builder setTimeEnd(float f) {
                boolean unused = this.result.hasTimeEnd = true;
                float unused2 = this.result.timeEnd_ = f;
                return this;
            }

            public final Builder clearTimeEnd() {
                boolean unused = this.result.hasTimeEnd = false;
                float unused2 = this.result.timeEnd_ = 0.0f;
                return this;
            }

            public final boolean hasUid() {
                return this.result.hasUid();
            }

            public final int getUid() {
                return this.result.getUid();
            }

            public final Builder setUid(int i) {
                boolean unused = this.result.hasUid = true;
                int unused2 = this.result.uid_ = i;
                return this;
            }

            public final Builder clearUid() {
                boolean unused = this.result.hasUid = false;
                int unused2 = this.result.uid_ = 0;
                return this;
            }

            public final boolean hasPuzzleImageWidth() {
                return this.result.hasPuzzleImageWidth();
            }

            public final float getPuzzleImageWidth() {
                return this.result.getPuzzleImageWidth();
            }

            public final Builder setPuzzleImageWidth(float f) {
                boolean unused = this.result.hasPuzzleImageWidth = true;
                float unused2 = this.result.puzzleImageWidth_ = f;
                return this;
            }

            public final Builder clearPuzzleImageWidth() {
                boolean unused = this.result.hasPuzzleImageWidth = false;
                float unused2 = this.result.puzzleImageWidth_ = 0.0f;
                return this;
            }

            public final boolean hasPuzzleImageHeight() {
                return this.result.hasPuzzleImageHeight();
            }

            public final float getPuzzleImageHeight() {
                return this.result.getPuzzleImageHeight();
            }

            public final Builder setPuzzleImageHeight(float f) {
                boolean unused = this.result.hasPuzzleImageHeight = true;
                float unused2 = this.result.puzzleImageHeight_ = f;
                return this;
            }

            public final Builder clearPuzzleImageHeight() {
                boolean unused = this.result.hasPuzzleImageHeight = false;
                float unused2 = this.result.puzzleImageHeight_ = 0.0f;
                return this;
            }
        }

        static {
            PuzzleImageInfo puzzleImageInfo = new PuzzleImageInfo(true);
            defaultInstance = puzzleImageInfo;
            puzzleImageInfo.type_ = a.MOVIE;
        }
    }

    public static final class PuzzleImageInfos extends GeneratedMessage {
        public static final int PUZZLE_IMAGE_INFOS_FIELD_NUMBER = 1;
        private static final PuzzleImageInfos defaultInstance = new PuzzleImageInfos(true);
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public List<PuzzleImageInfo> puzzleImageInfos_;

        /* synthetic */ PuzzleImageInfos(ai aiVar) {
            this();
        }

        private PuzzleImageInfos() {
            this.puzzleImageInfos_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        private PuzzleImageInfos(boolean z) {
            this.puzzleImageInfos_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        public static PuzzleImageInfos getDefaultInstance() {
            return defaultInstance;
        }

        public final PuzzleImageInfos getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Jigsaur.A;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Jigsaur.B;
        }

        public final List<PuzzleImageInfo> getPuzzleImageInfosList() {
            return this.puzzleImageInfos_;
        }

        public final int getPuzzleImageInfosCount() {
            return this.puzzleImageInfos_.size();
        }

        public final PuzzleImageInfo getPuzzleImageInfos(int i) {
            return this.puzzleImageInfos_.get(i);
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            for (PuzzleImageInfo isInitialized : getPuzzleImageInfosList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            for (PuzzleImageInfo b : getPuzzleImageInfosList()) {
                xVar.b(1, b);
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            Iterator<PuzzleImageInfo> it = getPuzzleImageInfosList().iterator();
            while (true) {
                int i3 = i2;
                if (it.hasNext()) {
                    i2 = x.d(1, it.next()) + i3;
                } else {
                    int serializedSize = getUnknownFields().getSerializedSize() + i3;
                    this.memoizedSerializedSize = serializedSize;
                    return serializedSize;
                }
            }
        }

        public static PuzzleImageInfos parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfos.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfos.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfos.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfos.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfos.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PuzzleImageInfos$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static PuzzleImageInfos parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static PuzzleImageInfos parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfos.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfos.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfos.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfos.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfos.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PuzzleImageInfos$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static PuzzleImageInfos parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static PuzzleImageInfos parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfos.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfos.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfos.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfos.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfos.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PuzzleImageInfos$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static PuzzleImageInfos parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static PuzzleImageInfos parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static PuzzleImageInfos parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static PuzzleImageInfos parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfos.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PuzzleImageInfos$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfos.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfos.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfos.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfos.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.PuzzleImageInfos.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$PuzzleImageInfos$Builder */
        public static PuzzleImageInfos parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(PuzzleImageInfos puzzleImageInfos) {
            return newBuilder().mergeFrom(puzzleImageInfos);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private PuzzleImageInfos result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new PuzzleImageInfos((ai) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final PuzzleImageInfos internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new PuzzleImageInfos((ai) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return PuzzleImageInfos.getDescriptor();
            }

            public final PuzzleImageInfos getDefaultInstanceForType() {
                return PuzzleImageInfos.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final PuzzleImageInfos build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public PuzzleImageInfos buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final PuzzleImageInfos buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.puzzleImageInfos_ != Collections.EMPTY_LIST) {
                    List unused = this.result.puzzleImageInfos_ = Collections.unmodifiableList(this.result.puzzleImageInfos_);
                }
                PuzzleImageInfos puzzleImageInfos = this.result;
                this.result = null;
                return puzzleImageInfos;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof PuzzleImageInfos) {
                    return mergeFrom((PuzzleImageInfos) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(PuzzleImageInfos puzzleImageInfos) {
                if (puzzleImageInfos != PuzzleImageInfos.getDefaultInstance()) {
                    if (!puzzleImageInfos.puzzleImageInfos_.isEmpty()) {
                        if (this.result.puzzleImageInfos_.isEmpty()) {
                            List unused = this.result.puzzleImageInfos_ = new ArrayList();
                        }
                        this.result.puzzleImageInfos_.addAll(puzzleImageInfos.puzzleImageInfos_);
                    }
                    mergeUnknownFields(puzzleImageInfos.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 10:
                            PuzzleImageInfo.Builder newBuilder = PuzzleImageInfo.newBuilder();
                            hVar.a(newBuilder, abVar);
                            addPuzzleImageInfos(newBuilder.buildPartial());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final List<PuzzleImageInfo> getPuzzleImageInfosList() {
                return Collections.unmodifiableList(this.result.puzzleImageInfos_);
            }

            public final int getPuzzleImageInfosCount() {
                return this.result.getPuzzleImageInfosCount();
            }

            public final PuzzleImageInfo getPuzzleImageInfos(int i) {
                return this.result.getPuzzleImageInfos(i);
            }

            public final Builder setPuzzleImageInfos(int i, PuzzleImageInfo puzzleImageInfo) {
                if (puzzleImageInfo == null) {
                    throw new NullPointerException();
                }
                this.result.puzzleImageInfos_.set(i, puzzleImageInfo);
                return this;
            }

            public final Builder setPuzzleImageInfos(int i, PuzzleImageInfo.Builder builder) {
                this.result.puzzleImageInfos_.set(i, builder.build());
                return this;
            }

            public final Builder addPuzzleImageInfos(PuzzleImageInfo puzzleImageInfo) {
                if (puzzleImageInfo == null) {
                    throw new NullPointerException();
                }
                if (this.result.puzzleImageInfos_.isEmpty()) {
                    List unused = this.result.puzzleImageInfos_ = new ArrayList();
                }
                this.result.puzzleImageInfos_.add(puzzleImageInfo);
                return this;
            }

            public final Builder addPuzzleImageInfos(PuzzleImageInfo.Builder builder) {
                if (this.result.puzzleImageInfos_.isEmpty()) {
                    List unused = this.result.puzzleImageInfos_ = new ArrayList();
                }
                this.result.puzzleImageInfos_.add(builder.build());
                return this;
            }

            public final Builder addAllPuzzleImageInfos(Iterable<? extends PuzzleImageInfo> iterable) {
                if (this.result.puzzleImageInfos_.isEmpty()) {
                    List unused = this.result.puzzleImageInfos_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.puzzleImageInfos_);
                return this;
            }

            public final Builder clearPuzzleImageInfos() {
                List unused = this.result.puzzleImageInfos_ = Collections.emptyList();
                return this;
            }
        }
    }

    public static final class LevelPredicate extends GeneratedMessage {
        public static final int COMPLETION_PERCENTAGE_FIELD_NUMBER = 3;
        public static final int PRIOR_LEVEL_ID_FIELD_NUMBER = 2;
        public static final int TYPE_FIELD_NUMBER = 1;
        private static final LevelPredicate defaultInstance;
        /* access modifiers changed from: private */
        public int completionPercentage_;
        /* access modifiers changed from: private */
        public boolean hasCompletionPercentage;
        /* access modifiers changed from: private */
        public boolean hasPriorLevelId;
        /* access modifiers changed from: private */
        public boolean hasType;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public int priorLevelId_;
        /* access modifiers changed from: private */
        public a type_;

        /* synthetic */ LevelPredicate(ai aiVar) {
            this();
        }

        private LevelPredicate() {
            this.priorLevelId_ = 0;
            this.completionPercentage_ = 0;
            this.memoizedSerializedSize = -1;
            this.type_ = a.PRIOR_LEVEL;
        }

        private LevelPredicate(boolean z) {
            this.priorLevelId_ = 0;
            this.completionPercentage_ = 0;
            this.memoizedSerializedSize = -1;
        }

        public static LevelPredicate getDefaultInstance() {
            return defaultInstance;
        }

        public final LevelPredicate getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Jigsaur.C;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Jigsaur.D;
        }

        public enum a implements v {
            PRIOR_LEVEL;
            
            private static g.b<a> b = new aq();
            private static final a[] c = {PRIOR_LEVEL};
            private final int d = 0;
            private final int e = 1;

            public final int d_() {
                return this.e;
            }

            public static a a(int i) {
                switch (i) {
                    case 1:
                        return PRIOR_LEVEL;
                    default:
                        return null;
                }
            }
        }

        public final boolean hasType() {
            return this.hasType;
        }

        public final a getType() {
            return this.type_;
        }

        public final boolean hasPriorLevelId() {
            return this.hasPriorLevelId;
        }

        public final int getPriorLevelId() {
            return this.priorLevelId_;
        }

        public final boolean hasCompletionPercentage() {
            return this.hasCompletionPercentage;
        }

        public final int getCompletionPercentage() {
            return this.completionPercentage_;
        }

        private void initFields() {
            this.type_ = a.PRIOR_LEVEL;
        }

        public final boolean isInitialized() {
            if (!this.hasType) {
                return false;
            }
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasType()) {
                xVar.c(1, getType().d_());
            }
            if (hasPriorLevelId()) {
                xVar.b(2, getPriorLevelId());
            }
            if (hasCompletionPercentage()) {
                xVar.b(3, getCompletionPercentage());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (hasType()) {
                i2 = x.f(1, getType().d_()) + 0;
            }
            if (hasPriorLevelId()) {
                i2 += x.e(2, getPriorLevelId());
            }
            if (hasCompletionPercentage()) {
                i2 += x.e(3, getCompletionPercentage());
            }
            int serializedSize = i2 + getUnknownFields().getSerializedSize();
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static LevelPredicate parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.LevelPredicate.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.LevelPredicate.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.LevelPredicate.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.LevelPredicate.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.LevelPredicate.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$LevelPredicate$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static LevelPredicate parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static LevelPredicate parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.LevelPredicate.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.LevelPredicate.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.LevelPredicate.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.LevelPredicate.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.LevelPredicate.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$LevelPredicate$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static LevelPredicate parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static LevelPredicate parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.LevelPredicate.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.LevelPredicate.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.LevelPredicate.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.LevelPredicate.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.LevelPredicate.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$LevelPredicate$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static LevelPredicate parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static LevelPredicate parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static LevelPredicate parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static LevelPredicate parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.jigsaur.Jigsaur.LevelPredicate.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$LevelPredicate$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.LevelPredicate.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.LevelPredicate.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.LevelPredicate.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.LevelPredicate.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.LevelPredicate.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$LevelPredicate$Builder */
        public static LevelPredicate parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(LevelPredicate levelPredicate) {
            return newBuilder().mergeFrom(levelPredicate);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private LevelPredicate result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new LevelPredicate((ai) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final LevelPredicate internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new LevelPredicate((ai) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return LevelPredicate.getDescriptor();
            }

            public final LevelPredicate getDefaultInstanceForType() {
                return LevelPredicate.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final LevelPredicate build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public LevelPredicate buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final LevelPredicate buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                LevelPredicate levelPredicate = this.result;
                this.result = null;
                return levelPredicate;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof LevelPredicate) {
                    return mergeFrom((LevelPredicate) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(LevelPredicate levelPredicate) {
                if (levelPredicate != LevelPredicate.getDefaultInstance()) {
                    if (levelPredicate.hasType()) {
                        setType(levelPredicate.getType());
                    }
                    if (levelPredicate.hasPriorLevelId()) {
                        setPriorLevelId(levelPredicate.getPriorLevelId());
                    }
                    if (levelPredicate.hasCompletionPercentage()) {
                        setCompletionPercentage(levelPredicate.getCompletionPercentage());
                    }
                    mergeUnknownFields(levelPredicate.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = com.google.protobuf.a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 8:
                            int m = hVar.m();
                            a a3 = a.a(m);
                            if (a3 != null) {
                                setType(a3);
                                break;
                            } else {
                                a.a(1, m);
                                break;
                            }
                        case 16:
                            setPriorLevelId(hVar.l());
                            break;
                        case 24:
                            setCompletionPercentage(hVar.l());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasType() {
                return this.result.hasType();
            }

            public final a getType() {
                return this.result.getType();
            }

            public final Builder setType(a aVar) {
                if (aVar == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasType = true;
                a unused2 = this.result.type_ = aVar;
                return this;
            }

            public final Builder clearType() {
                boolean unused = this.result.hasType = false;
                a unused2 = this.result.type_ = a.PRIOR_LEVEL;
                return this;
            }

            public final boolean hasPriorLevelId() {
                return this.result.hasPriorLevelId();
            }

            public final int getPriorLevelId() {
                return this.result.getPriorLevelId();
            }

            public final Builder setPriorLevelId(int i) {
                boolean unused = this.result.hasPriorLevelId = true;
                int unused2 = this.result.priorLevelId_ = i;
                return this;
            }

            public final Builder clearPriorLevelId() {
                boolean unused = this.result.hasPriorLevelId = false;
                int unused2 = this.result.priorLevelId_ = 0;
                return this;
            }

            public final boolean hasCompletionPercentage() {
                return this.result.hasCompletionPercentage();
            }

            public final int getCompletionPercentage() {
                return this.result.getCompletionPercentage();
            }

            public final Builder setCompletionPercentage(int i) {
                boolean unused = this.result.hasCompletionPercentage = true;
                int unused2 = this.result.completionPercentage_ = i;
                return this;
            }

            public final Builder clearCompletionPercentage() {
                boolean unused = this.result.hasCompletionPercentage = false;
                int unused2 = this.result.completionPercentage_ = 0;
                return this;
            }
        }

        static {
            LevelPredicate levelPredicate = new LevelPredicate(true);
            defaultInstance = levelPredicate;
            levelPredicate.type_ = a.PRIOR_LEVEL;
        }
    }

    public static final class Level extends GeneratedMessage {
        public static final int DIFFICULTY_FAMILY_FIELD_NUMBER = 14;
        public static final int IMAGE_INFO_UID_FIELD_NUMBER = 3;
        public static final int INTERNAL_NAME_FIELD_NUMBER = 11;
        public static final int IS_PREMIUM_FIELD_NUMBER = 100;
        public static final int MAX_X_FIELD_NUMBER = 8;
        public static final int MAX_Y_FIELD_NUMBER = 10;
        public static final int MAX_ZOOM_FIELD_NUMBER = 6;
        public static final int MIN_X_FIELD_NUMBER = 7;
        public static final int MIN_Y_FIELD_NUMBER = 9;
        public static final int MIN_ZOOM_FIELD_NUMBER = 5;
        public static final int NEXT_LEVEL_FIELD_NUMBER = 12;
        public static final int PIECE_SET_TYPE_FIELD_NUMBER = 2;
        public static final int PREDICES_FIELD_NUMBER = 101;
        public static final int PREV_LEVEL_FIELD_NUMBER = 13;
        public static final int STARTING_CONFIGURATION_FIELD_NUMBER = 4;
        public static final int UID_FIELD_NUMBER = 1;
        private static final Level defaultInstance;
        /* access modifiers changed from: private */
        public List<String> difficultyFamily_;
        /* access modifiers changed from: private */
        public boolean hasImageInfoUid;
        /* access modifiers changed from: private */
        public boolean hasInternalName;
        /* access modifiers changed from: private */
        public boolean hasIsPremium;
        /* access modifiers changed from: private */
        public boolean hasMaxX;
        /* access modifiers changed from: private */
        public boolean hasMaxY;
        /* access modifiers changed from: private */
        public boolean hasMaxZoom;
        /* access modifiers changed from: private */
        public boolean hasMinX;
        /* access modifiers changed from: private */
        public boolean hasMinY;
        /* access modifiers changed from: private */
        public boolean hasMinZoom;
        /* access modifiers changed from: private */
        public boolean hasNextLevel;
        /* access modifiers changed from: private */
        public boolean hasPieceSetType;
        /* access modifiers changed from: private */
        public boolean hasPrevLevel;
        /* access modifiers changed from: private */
        public boolean hasStartingConfiguration;
        /* access modifiers changed from: private */
        public boolean hasUid;
        /* access modifiers changed from: private */
        public int imageInfoUid_;
        /* access modifiers changed from: private */
        public String internalName_;
        /* access modifiers changed from: private */
        public boolean isPremium_;
        /* access modifiers changed from: private */
        public float maxX_;
        /* access modifiers changed from: private */
        public float maxY_;
        /* access modifiers changed from: private */
        public float maxZoom_;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public float minX_;
        /* access modifiers changed from: private */
        public float minY_;
        /* access modifiers changed from: private */
        public float minZoom_;
        /* access modifiers changed from: private */
        public String nextLevel_;
        /* access modifiers changed from: private */
        public String pieceSetType_;
        /* access modifiers changed from: private */
        public List<LevelPredicate> predices_;
        /* access modifiers changed from: private */
        public String prevLevel_;
        /* access modifiers changed from: private */
        public SavedConfiguration startingConfiguration_;
        /* access modifiers changed from: private */
        public int uid_;

        /* synthetic */ Level(ai aiVar) {
            this();
        }

        private Level() {
            this.uid_ = 0;
            this.pieceSetType_ = "";
            this.imageInfoUid_ = 0;
            this.minZoom_ = 0.0f;
            this.maxZoom_ = 0.0f;
            this.minX_ = 0.0f;
            this.maxX_ = 0.0f;
            this.minY_ = 0.0f;
            this.maxY_ = 0.0f;
            this.internalName_ = "";
            this.nextLevel_ = "";
            this.prevLevel_ = "";
            this.difficultyFamily_ = Collections.emptyList();
            this.isPremium_ = false;
            this.predices_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
            this.startingConfiguration_ = SavedConfiguration.getDefaultInstance();
        }

        private Level(boolean z) {
            this.uid_ = 0;
            this.pieceSetType_ = "";
            this.imageInfoUid_ = 0;
            this.minZoom_ = 0.0f;
            this.maxZoom_ = 0.0f;
            this.minX_ = 0.0f;
            this.maxX_ = 0.0f;
            this.minY_ = 0.0f;
            this.maxY_ = 0.0f;
            this.internalName_ = "";
            this.nextLevel_ = "";
            this.prevLevel_ = "";
            this.difficultyFamily_ = Collections.emptyList();
            this.isPremium_ = false;
            this.predices_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        public static Level getDefaultInstance() {
            return defaultInstance;
        }

        public final Level getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Jigsaur.E;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Jigsaur.F;
        }

        public final boolean hasUid() {
            return this.hasUid;
        }

        public final int getUid() {
            return this.uid_;
        }

        public final boolean hasPieceSetType() {
            return this.hasPieceSetType;
        }

        public final String getPieceSetType() {
            return this.pieceSetType_;
        }

        public final boolean hasImageInfoUid() {
            return this.hasImageInfoUid;
        }

        public final int getImageInfoUid() {
            return this.imageInfoUid_;
        }

        public final boolean hasStartingConfiguration() {
            return this.hasStartingConfiguration;
        }

        public final SavedConfiguration getStartingConfiguration() {
            return this.startingConfiguration_;
        }

        public final boolean hasMinZoom() {
            return this.hasMinZoom;
        }

        public final float getMinZoom() {
            return this.minZoom_;
        }

        public final boolean hasMaxZoom() {
            return this.hasMaxZoom;
        }

        public final float getMaxZoom() {
            return this.maxZoom_;
        }

        public final boolean hasMinX() {
            return this.hasMinX;
        }

        public final float getMinX() {
            return this.minX_;
        }

        public final boolean hasMaxX() {
            return this.hasMaxX;
        }

        public final float getMaxX() {
            return this.maxX_;
        }

        public final boolean hasMinY() {
            return this.hasMinY;
        }

        public final float getMinY() {
            return this.minY_;
        }

        public final boolean hasMaxY() {
            return this.hasMaxY;
        }

        public final float getMaxY() {
            return this.maxY_;
        }

        public final boolean hasInternalName() {
            return this.hasInternalName;
        }

        public final String getInternalName() {
            return this.internalName_;
        }

        public final boolean hasNextLevel() {
            return this.hasNextLevel;
        }

        public final String getNextLevel() {
            return this.nextLevel_;
        }

        public final boolean hasPrevLevel() {
            return this.hasPrevLevel;
        }

        public final String getPrevLevel() {
            return this.prevLevel_;
        }

        public final List<String> getDifficultyFamilyList() {
            return this.difficultyFamily_;
        }

        public final int getDifficultyFamilyCount() {
            return this.difficultyFamily_.size();
        }

        public final String getDifficultyFamily(int i) {
            return this.difficultyFamily_.get(i);
        }

        public final boolean hasIsPremium() {
            return this.hasIsPremium;
        }

        public final boolean getIsPremium() {
            return this.isPremium_;
        }

        public final List<LevelPredicate> getPredicesList() {
            return this.predices_;
        }

        public final int getPredicesCount() {
            return this.predices_.size();
        }

        public final LevelPredicate getPredices(int i) {
            return this.predices_.get(i);
        }

        private void initFields() {
            this.startingConfiguration_ = SavedConfiguration.getDefaultInstance();
        }

        public final boolean isInitialized() {
            if (!this.hasUid) {
                return false;
            }
            if (!this.hasPieceSetType) {
                return false;
            }
            if (!this.hasImageInfoUid) {
                return false;
            }
            if (!this.hasMinZoom) {
                return false;
            }
            if (!this.hasMaxZoom) {
                return false;
            }
            if (!this.hasMinX) {
                return false;
            }
            if (!this.hasMaxX) {
                return false;
            }
            if (!this.hasMinY) {
                return false;
            }
            if (!this.hasMaxY) {
                return false;
            }
            if (!this.hasInternalName) {
                return false;
            }
            if (!this.hasNextLevel) {
                return false;
            }
            if (!this.hasPrevLevel) {
                return false;
            }
            if (!this.hasIsPremium) {
                return false;
            }
            if (hasStartingConfiguration() && !getStartingConfiguration().isInitialized()) {
                return false;
            }
            for (LevelPredicate isInitialized : getPredicesList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasUid()) {
                xVar.b(1, getUid());
            }
            if (hasPieceSetType()) {
                xVar.a(2, getPieceSetType());
            }
            if (hasImageInfoUid()) {
                xVar.b(3, getImageInfoUid());
            }
            if (hasStartingConfiguration()) {
                xVar.b(4, getStartingConfiguration());
            }
            if (hasMinZoom()) {
                xVar.a(5, getMinZoom());
            }
            if (hasMaxZoom()) {
                xVar.a(6, getMaxZoom());
            }
            if (hasMinX()) {
                xVar.a(7, getMinX());
            }
            if (hasMaxX()) {
                xVar.a(8, getMaxX());
            }
            if (hasMinY()) {
                xVar.a(9, getMinY());
            }
            if (hasMaxY()) {
                xVar.a(10, getMaxY());
            }
            if (hasInternalName()) {
                xVar.a(11, getInternalName());
            }
            if (hasNextLevel()) {
                xVar.a(12, getNextLevel());
            }
            if (hasPrevLevel()) {
                xVar.a(13, getPrevLevel());
            }
            for (String a : getDifficultyFamilyList()) {
                xVar.a(14, a);
            }
            if (hasIsPremium()) {
                xVar.a(100, getIsPremium());
            }
            for (LevelPredicate b : getPredicesList()) {
                xVar.b((int) PREDICES_FIELD_NUMBER, b);
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i;
            int i2;
            int i3 = 0;
            int i4 = this.memoizedSerializedSize;
            if (i4 != -1) {
                return i4;
            }
            if (hasUid()) {
                i = x.e(1, getUid()) + 0;
            } else {
                i = 0;
            }
            if (hasPieceSetType()) {
                i += x.b(2, getPieceSetType());
            }
            if (hasImageInfoUid()) {
                i += x.e(3, getImageInfoUid());
            }
            if (hasStartingConfiguration()) {
                i += x.d(4, getStartingConfiguration());
            }
            if (hasMinZoom()) {
                getMinZoom();
                i += x.d(5);
            }
            if (hasMaxZoom()) {
                getMaxZoom();
                i += x.d(6);
            }
            if (hasMinX()) {
                getMinX();
                i += x.d(7);
            }
            if (hasMaxX()) {
                getMaxX();
                i += x.d(8);
            }
            if (hasMinY()) {
                getMinY();
                i += x.d(9);
            }
            if (hasMaxY()) {
                getMaxY();
                i += x.d(10);
            }
            if (hasInternalName()) {
                i += x.b(11, getInternalName());
            }
            if (hasNextLevel()) {
                i += x.b(12, getNextLevel());
            }
            if (hasPrevLevel()) {
                i2 = i + x.b(13, getPrevLevel());
            } else {
                i2 = i;
            }
            for (String b : getDifficultyFamilyList()) {
                i3 += x.b(b);
            }
            int size = i2 + i3 + (getDifficultyFamilyList().size() * 1);
            if (hasIsPremium()) {
                getIsPremium();
                size += x.e(100);
            }
            Iterator<LevelPredicate> it = getPredicesList().iterator();
            while (true) {
                int i5 = size;
                if (it.hasNext()) {
                    size = x.d((int) PREDICES_FIELD_NUMBER, it.next()) + i5;
                } else {
                    int serializedSize = getUnknownFields().getSerializedSize() + i5;
                    this.memoizedSerializedSize = serializedSize;
                    return serializedSize;
                }
            }
        }

        public static Level parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.Level.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.Level.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.Level.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.Level.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.Level.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$Level$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static Level parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static Level parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.Level.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.Level.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.Level.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.Level.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.Level.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$Level$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static Level parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static Level parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.Level.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.Level.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.Level.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.Level.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.Level.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$Level$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static Level parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static Level parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static Level parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static Level parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.jigsaur.Jigsaur.Level.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$Level$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.Level.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.Level.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.Level.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.Level.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.Level.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$Level$Builder */
        public static Level parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(Level level) {
            return newBuilder().mergeFrom(level);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private Level result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new Level((ai) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final Level internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new Level((ai) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return Level.getDescriptor();
            }

            public final Level getDefaultInstanceForType() {
                return Level.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final Level build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public Level buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final Level buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.difficultyFamily_ != Collections.EMPTY_LIST) {
                    List unused = this.result.difficultyFamily_ = Collections.unmodifiableList(this.result.difficultyFamily_);
                }
                if (this.result.predices_ != Collections.EMPTY_LIST) {
                    List unused2 = this.result.predices_ = Collections.unmodifiableList(this.result.predices_);
                }
                Level level = this.result;
                this.result = null;
                return level;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof Level) {
                    return mergeFrom((Level) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(Level level) {
                if (level != Level.getDefaultInstance()) {
                    if (level.hasUid()) {
                        setUid(level.getUid());
                    }
                    if (level.hasPieceSetType()) {
                        setPieceSetType(level.getPieceSetType());
                    }
                    if (level.hasImageInfoUid()) {
                        setImageInfoUid(level.getImageInfoUid());
                    }
                    if (level.hasStartingConfiguration()) {
                        mergeStartingConfiguration(level.getStartingConfiguration());
                    }
                    if (level.hasMinZoom()) {
                        setMinZoom(level.getMinZoom());
                    }
                    if (level.hasMaxZoom()) {
                        setMaxZoom(level.getMaxZoom());
                    }
                    if (level.hasMinX()) {
                        setMinX(level.getMinX());
                    }
                    if (level.hasMaxX()) {
                        setMaxX(level.getMaxX());
                    }
                    if (level.hasMinY()) {
                        setMinY(level.getMinY());
                    }
                    if (level.hasMaxY()) {
                        setMaxY(level.getMaxY());
                    }
                    if (level.hasInternalName()) {
                        setInternalName(level.getInternalName());
                    }
                    if (level.hasNextLevel()) {
                        setNextLevel(level.getNextLevel());
                    }
                    if (level.hasPrevLevel()) {
                        setPrevLevel(level.getPrevLevel());
                    }
                    if (!level.difficultyFamily_.isEmpty()) {
                        if (this.result.difficultyFamily_.isEmpty()) {
                            List unused = this.result.difficultyFamily_ = new ArrayList();
                        }
                        this.result.difficultyFamily_.addAll(level.difficultyFamily_);
                    }
                    if (level.hasIsPremium()) {
                        setIsPremium(level.getIsPremium());
                    }
                    if (!level.predices_.isEmpty()) {
                        if (this.result.predices_.isEmpty()) {
                            List unused2 = this.result.predices_ = new ArrayList();
                        }
                        this.result.predices_.addAll(level.predices_);
                    }
                    mergeUnknownFields(level.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 8:
                            setUid(hVar.l());
                            break;
                        case DescriptorProtos.FileOptions.PY_GENERIC_SERVICES_FIELD_NUMBER:
                            setPieceSetType(hVar.j());
                            break;
                        case 24:
                            setImageInfoUid(hVar.l());
                            break;
                        case 34:
                            SavedConfiguration.Builder newBuilder = SavedConfiguration.newBuilder();
                            if (hasStartingConfiguration()) {
                                newBuilder.mergeFrom(getStartingConfiguration());
                            }
                            hVar.a(newBuilder, abVar);
                            setStartingConfiguration(newBuilder.buildPartial());
                            break;
                        case 45:
                            setMinZoom(hVar.c());
                            break;
                        case 53:
                            setMaxZoom(hVar.c());
                            break;
                        case 61:
                            setMinX(hVar.c());
                            break;
                        case 69:
                            setMaxX(hVar.c());
                            break;
                        case 77:
                            setMinY(hVar.c());
                            break;
                        case 85:
                            setMaxY(hVar.c());
                            break;
                        case 90:
                            setInternalName(hVar.j());
                            break;
                        case 98:
                            setNextLevel(hVar.j());
                            break;
                        case 106:
                            setPrevLevel(hVar.j());
                            break;
                        case 114:
                            addDifficultyFamily(hVar.j());
                            break;
                        case 800:
                            setIsPremium(hVar.i());
                            break;
                        case 810:
                            LevelPredicate.Builder newBuilder2 = LevelPredicate.newBuilder();
                            hVar.a(newBuilder2, abVar);
                            addPredices(newBuilder2.buildPartial());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasUid() {
                return this.result.hasUid();
            }

            public final int getUid() {
                return this.result.getUid();
            }

            public final Builder setUid(int i) {
                boolean unused = this.result.hasUid = true;
                int unused2 = this.result.uid_ = i;
                return this;
            }

            public final Builder clearUid() {
                boolean unused = this.result.hasUid = false;
                int unused2 = this.result.uid_ = 0;
                return this;
            }

            public final boolean hasPieceSetType() {
                return this.result.hasPieceSetType();
            }

            public final String getPieceSetType() {
                return this.result.getPieceSetType();
            }

            public final Builder setPieceSetType(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasPieceSetType = true;
                String unused2 = this.result.pieceSetType_ = str;
                return this;
            }

            public final Builder clearPieceSetType() {
                boolean unused = this.result.hasPieceSetType = false;
                String unused2 = this.result.pieceSetType_ = Level.getDefaultInstance().getPieceSetType();
                return this;
            }

            public final boolean hasImageInfoUid() {
                return this.result.hasImageInfoUid();
            }

            public final int getImageInfoUid() {
                return this.result.getImageInfoUid();
            }

            public final Builder setImageInfoUid(int i) {
                boolean unused = this.result.hasImageInfoUid = true;
                int unused2 = this.result.imageInfoUid_ = i;
                return this;
            }

            public final Builder clearImageInfoUid() {
                boolean unused = this.result.hasImageInfoUid = false;
                int unused2 = this.result.imageInfoUid_ = 0;
                return this;
            }

            public final boolean hasStartingConfiguration() {
                return this.result.hasStartingConfiguration();
            }

            public final SavedConfiguration getStartingConfiguration() {
                return this.result.getStartingConfiguration();
            }

            public final Builder setStartingConfiguration(SavedConfiguration savedConfiguration) {
                if (savedConfiguration == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasStartingConfiguration = true;
                SavedConfiguration unused2 = this.result.startingConfiguration_ = savedConfiguration;
                return this;
            }

            public final Builder setStartingConfiguration(SavedConfiguration.Builder builder) {
                boolean unused = this.result.hasStartingConfiguration = true;
                SavedConfiguration unused2 = this.result.startingConfiguration_ = builder.build();
                return this;
            }

            public final Builder mergeStartingConfiguration(SavedConfiguration savedConfiguration) {
                if (!this.result.hasStartingConfiguration() || this.result.startingConfiguration_ == SavedConfiguration.getDefaultInstance()) {
                    SavedConfiguration unused = this.result.startingConfiguration_ = savedConfiguration;
                } else {
                    SavedConfiguration unused2 = this.result.startingConfiguration_ = SavedConfiguration.newBuilder(this.result.startingConfiguration_).mergeFrom(savedConfiguration).buildPartial();
                }
                boolean unused3 = this.result.hasStartingConfiguration = true;
                return this;
            }

            public final Builder clearStartingConfiguration() {
                boolean unused = this.result.hasStartingConfiguration = false;
                SavedConfiguration unused2 = this.result.startingConfiguration_ = SavedConfiguration.getDefaultInstance();
                return this;
            }

            public final boolean hasMinZoom() {
                return this.result.hasMinZoom();
            }

            public final float getMinZoom() {
                return this.result.getMinZoom();
            }

            public final Builder setMinZoom(float f) {
                boolean unused = this.result.hasMinZoom = true;
                float unused2 = this.result.minZoom_ = f;
                return this;
            }

            public final Builder clearMinZoom() {
                boolean unused = this.result.hasMinZoom = false;
                float unused2 = this.result.minZoom_ = 0.0f;
                return this;
            }

            public final boolean hasMaxZoom() {
                return this.result.hasMaxZoom();
            }

            public final float getMaxZoom() {
                return this.result.getMaxZoom();
            }

            public final Builder setMaxZoom(float f) {
                boolean unused = this.result.hasMaxZoom = true;
                float unused2 = this.result.maxZoom_ = f;
                return this;
            }

            public final Builder clearMaxZoom() {
                boolean unused = this.result.hasMaxZoom = false;
                float unused2 = this.result.maxZoom_ = 0.0f;
                return this;
            }

            public final boolean hasMinX() {
                return this.result.hasMinX();
            }

            public final float getMinX() {
                return this.result.getMinX();
            }

            public final Builder setMinX(float f) {
                boolean unused = this.result.hasMinX = true;
                float unused2 = this.result.minX_ = f;
                return this;
            }

            public final Builder clearMinX() {
                boolean unused = this.result.hasMinX = false;
                float unused2 = this.result.minX_ = 0.0f;
                return this;
            }

            public final boolean hasMaxX() {
                return this.result.hasMaxX();
            }

            public final float getMaxX() {
                return this.result.getMaxX();
            }

            public final Builder setMaxX(float f) {
                boolean unused = this.result.hasMaxX = true;
                float unused2 = this.result.maxX_ = f;
                return this;
            }

            public final Builder clearMaxX() {
                boolean unused = this.result.hasMaxX = false;
                float unused2 = this.result.maxX_ = 0.0f;
                return this;
            }

            public final boolean hasMinY() {
                return this.result.hasMinY();
            }

            public final float getMinY() {
                return this.result.getMinY();
            }

            public final Builder setMinY(float f) {
                boolean unused = this.result.hasMinY = true;
                float unused2 = this.result.minY_ = f;
                return this;
            }

            public final Builder clearMinY() {
                boolean unused = this.result.hasMinY = false;
                float unused2 = this.result.minY_ = 0.0f;
                return this;
            }

            public final boolean hasMaxY() {
                return this.result.hasMaxY();
            }

            public final float getMaxY() {
                return this.result.getMaxY();
            }

            public final Builder setMaxY(float f) {
                boolean unused = this.result.hasMaxY = true;
                float unused2 = this.result.maxY_ = f;
                return this;
            }

            public final Builder clearMaxY() {
                boolean unused = this.result.hasMaxY = false;
                float unused2 = this.result.maxY_ = 0.0f;
                return this;
            }

            public final boolean hasInternalName() {
                return this.result.hasInternalName();
            }

            public final String getInternalName() {
                return this.result.getInternalName();
            }

            public final Builder setInternalName(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasInternalName = true;
                String unused2 = this.result.internalName_ = str;
                return this;
            }

            public final Builder clearInternalName() {
                boolean unused = this.result.hasInternalName = false;
                String unused2 = this.result.internalName_ = Level.getDefaultInstance().getInternalName();
                return this;
            }

            public final boolean hasNextLevel() {
                return this.result.hasNextLevel();
            }

            public final String getNextLevel() {
                return this.result.getNextLevel();
            }

            public final Builder setNextLevel(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasNextLevel = true;
                String unused2 = this.result.nextLevel_ = str;
                return this;
            }

            public final Builder clearNextLevel() {
                boolean unused = this.result.hasNextLevel = false;
                String unused2 = this.result.nextLevel_ = Level.getDefaultInstance().getNextLevel();
                return this;
            }

            public final boolean hasPrevLevel() {
                return this.result.hasPrevLevel();
            }

            public final String getPrevLevel() {
                return this.result.getPrevLevel();
            }

            public final Builder setPrevLevel(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasPrevLevel = true;
                String unused2 = this.result.prevLevel_ = str;
                return this;
            }

            public final Builder clearPrevLevel() {
                boolean unused = this.result.hasPrevLevel = false;
                String unused2 = this.result.prevLevel_ = Level.getDefaultInstance().getPrevLevel();
                return this;
            }

            public final List<String> getDifficultyFamilyList() {
                return Collections.unmodifiableList(this.result.difficultyFamily_);
            }

            public final int getDifficultyFamilyCount() {
                return this.result.getDifficultyFamilyCount();
            }

            public final String getDifficultyFamily(int i) {
                return this.result.getDifficultyFamily(i);
            }

            public final Builder setDifficultyFamily(int i, String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                this.result.difficultyFamily_.set(i, str);
                return this;
            }

            public final Builder addDifficultyFamily(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                if (this.result.difficultyFamily_.isEmpty()) {
                    List unused = this.result.difficultyFamily_ = new ArrayList();
                }
                this.result.difficultyFamily_.add(str);
                return this;
            }

            public final Builder addAllDifficultyFamily(Iterable<? extends String> iterable) {
                if (this.result.difficultyFamily_.isEmpty()) {
                    List unused = this.result.difficultyFamily_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.difficultyFamily_);
                return this;
            }

            public final Builder clearDifficultyFamily() {
                List unused = this.result.difficultyFamily_ = Collections.emptyList();
                return this;
            }

            public final boolean hasIsPremium() {
                return this.result.hasIsPremium();
            }

            public final boolean getIsPremium() {
                return this.result.getIsPremium();
            }

            public final Builder setIsPremium(boolean z) {
                boolean unused = this.result.hasIsPremium = true;
                boolean unused2 = this.result.isPremium_ = z;
                return this;
            }

            public final Builder clearIsPremium() {
                boolean unused = this.result.hasIsPremium = false;
                boolean unused2 = this.result.isPremium_ = false;
                return this;
            }

            public final List<LevelPredicate> getPredicesList() {
                return Collections.unmodifiableList(this.result.predices_);
            }

            public final int getPredicesCount() {
                return this.result.getPredicesCount();
            }

            public final LevelPredicate getPredices(int i) {
                return this.result.getPredices(i);
            }

            public final Builder setPredices(int i, LevelPredicate levelPredicate) {
                if (levelPredicate == null) {
                    throw new NullPointerException();
                }
                this.result.predices_.set(i, levelPredicate);
                return this;
            }

            public final Builder setPredices(int i, LevelPredicate.Builder builder) {
                this.result.predices_.set(i, builder.build());
                return this;
            }

            public final Builder addPredices(LevelPredicate levelPredicate) {
                if (levelPredicate == null) {
                    throw new NullPointerException();
                }
                if (this.result.predices_.isEmpty()) {
                    List unused = this.result.predices_ = new ArrayList();
                }
                this.result.predices_.add(levelPredicate);
                return this;
            }

            public final Builder addPredices(LevelPredicate.Builder builder) {
                if (this.result.predices_.isEmpty()) {
                    List unused = this.result.predices_ = new ArrayList();
                }
                this.result.predices_.add(builder.build());
                return this;
            }

            public final Builder addAllPredices(Iterable<? extends LevelPredicate> iterable) {
                if (this.result.predices_.isEmpty()) {
                    List unused = this.result.predices_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.predices_);
                return this;
            }

            public final Builder clearPredices() {
                List unused = this.result.predices_ = Collections.emptyList();
                return this;
            }
        }

        static {
            Level level = new Level(true);
            defaultInstance = level;
            level.startingConfiguration_ = SavedConfiguration.getDefaultInstance();
        }
    }

    public static final class Settings extends GeneratedMessage {
        public static final int BLINK_ON_WIN_FIELD_NUMBER = 4;
        public static final int BLINK_ON_WIN_SECONDS_FIELD_NUMBER = 5;
        public static final int DETAIL_LEVEL_FIELD_NUMBER = 1;
        public static final int DISPLAY_ZOOM_SLIDER_FIELD_NUMBER = 8;
        public static final int DO_AUTO_JOINING_FIELD_NUMBER = 2;
        public static final int ENABLE_ACCELEROMETER_FIELD_NUMBER = 7;
        public static final int LAST_LEVEL_PLAYED_FIELD_NUMBER = 10;
        public static final int SHOW_SOLUTION_FIELD_NUMBER = 6;
        public static final int SOUND_ENABLED_FIELD_NUMBER = 13;
        public static final int STARTUP_COUNT_FIELD_NUMBER = 12;
        public static final int TIMESTAMP_FIRST_PLAYED_FIELD_NUMBER = 11;
        public static final int TONE_DEAFNESS_FIELD_NUMBER = 3;
        public static final int USER_ID_FIELD_NUMBER = 9;
        private static final Settings defaultInstance = new Settings(true);
        /* access modifiers changed from: private */
        public int blinkOnWinSeconds_;
        /* access modifiers changed from: private */
        public boolean blinkOnWin_;
        /* access modifiers changed from: private */
        public int detailLevel_;
        /* access modifiers changed from: private */
        public boolean displayZoomSlider_;
        /* access modifiers changed from: private */
        public boolean doAutoJoining_;
        /* access modifiers changed from: private */
        public boolean enableAccelerometer_;
        /* access modifiers changed from: private */
        public boolean hasBlinkOnWin;
        /* access modifiers changed from: private */
        public boolean hasBlinkOnWinSeconds;
        /* access modifiers changed from: private */
        public boolean hasDetailLevel;
        /* access modifiers changed from: private */
        public boolean hasDisplayZoomSlider;
        /* access modifiers changed from: private */
        public boolean hasDoAutoJoining;
        /* access modifiers changed from: private */
        public boolean hasEnableAccelerometer;
        /* access modifiers changed from: private */
        public boolean hasLastLevelPlayed;
        /* access modifiers changed from: private */
        public boolean hasShowSolution;
        /* access modifiers changed from: private */
        public boolean hasSoundEnabled;
        /* access modifiers changed from: private */
        public boolean hasStartupCount;
        /* access modifiers changed from: private */
        public boolean hasTimestampFirstPlayed;
        /* access modifiers changed from: private */
        public boolean hasToneDeafness;
        /* access modifiers changed from: private */
        public boolean hasUserId;
        /* access modifiers changed from: private */
        public String lastLevelPlayed_;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public boolean showSolution_;
        /* access modifiers changed from: private */
        public boolean soundEnabled_;
        /* access modifiers changed from: private */
        public long startupCount_;
        /* access modifiers changed from: private */
        public long timestampFirstPlayed_;
        /* access modifiers changed from: private */
        public float toneDeafness_;
        /* access modifiers changed from: private */
        public long userId_;

        /* synthetic */ Settings(ai aiVar) {
            this();
        }

        private Settings() {
            this.detailLevel_ = 0;
            this.doAutoJoining_ = false;
            this.toneDeafness_ = 0.0f;
            this.blinkOnWin_ = false;
            this.blinkOnWinSeconds_ = 0;
            this.showSolution_ = false;
            this.enableAccelerometer_ = false;
            this.displayZoomSlider_ = false;
            this.userId_ = 0;
            this.lastLevelPlayed_ = "";
            this.timestampFirstPlayed_ = 0;
            this.startupCount_ = 0;
            this.soundEnabled_ = false;
            this.memoizedSerializedSize = -1;
        }

        private Settings(boolean z) {
            this.detailLevel_ = 0;
            this.doAutoJoining_ = false;
            this.toneDeafness_ = 0.0f;
            this.blinkOnWin_ = false;
            this.blinkOnWinSeconds_ = 0;
            this.showSolution_ = false;
            this.enableAccelerometer_ = false;
            this.displayZoomSlider_ = false;
            this.userId_ = 0;
            this.lastLevelPlayed_ = "";
            this.timestampFirstPlayed_ = 0;
            this.startupCount_ = 0;
            this.soundEnabled_ = false;
            this.memoizedSerializedSize = -1;
        }

        public static Settings getDefaultInstance() {
            return defaultInstance;
        }

        public final Settings getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Jigsaur.G;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Jigsaur.H;
        }

        public final boolean hasDetailLevel() {
            return this.hasDetailLevel;
        }

        public final int getDetailLevel() {
            return this.detailLevel_;
        }

        public final boolean hasDoAutoJoining() {
            return this.hasDoAutoJoining;
        }

        public final boolean getDoAutoJoining() {
            return this.doAutoJoining_;
        }

        public final boolean hasToneDeafness() {
            return this.hasToneDeafness;
        }

        public final float getToneDeafness() {
            return this.toneDeafness_;
        }

        public final boolean hasBlinkOnWin() {
            return this.hasBlinkOnWin;
        }

        public final boolean getBlinkOnWin() {
            return this.blinkOnWin_;
        }

        public final boolean hasBlinkOnWinSeconds() {
            return this.hasBlinkOnWinSeconds;
        }

        public final int getBlinkOnWinSeconds() {
            return this.blinkOnWinSeconds_;
        }

        public final boolean hasShowSolution() {
            return this.hasShowSolution;
        }

        public final boolean getShowSolution() {
            return this.showSolution_;
        }

        public final boolean hasEnableAccelerometer() {
            return this.hasEnableAccelerometer;
        }

        public final boolean getEnableAccelerometer() {
            return this.enableAccelerometer_;
        }

        public final boolean hasDisplayZoomSlider() {
            return this.hasDisplayZoomSlider;
        }

        public final boolean getDisplayZoomSlider() {
            return this.displayZoomSlider_;
        }

        public final boolean hasUserId() {
            return this.hasUserId;
        }

        public final long getUserId() {
            return this.userId_;
        }

        public final boolean hasLastLevelPlayed() {
            return this.hasLastLevelPlayed;
        }

        public final String getLastLevelPlayed() {
            return this.lastLevelPlayed_;
        }

        public final boolean hasTimestampFirstPlayed() {
            return this.hasTimestampFirstPlayed;
        }

        public final long getTimestampFirstPlayed() {
            return this.timestampFirstPlayed_;
        }

        public final boolean hasStartupCount() {
            return this.hasStartupCount;
        }

        public final long getStartupCount() {
            return this.startupCount_;
        }

        public final boolean hasSoundEnabled() {
            return this.hasSoundEnabled;
        }

        public final boolean getSoundEnabled() {
            return this.soundEnabled_;
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasDetailLevel()) {
                xVar.b(1, getDetailLevel());
            }
            if (hasDoAutoJoining()) {
                xVar.a(2, getDoAutoJoining());
            }
            if (hasToneDeafness()) {
                xVar.a(3, getToneDeafness());
            }
            if (hasBlinkOnWin()) {
                xVar.a(4, getBlinkOnWin());
            }
            if (hasBlinkOnWinSeconds()) {
                xVar.b(5, getBlinkOnWinSeconds());
            }
            if (hasShowSolution()) {
                xVar.a(6, getShowSolution());
            }
            if (hasEnableAccelerometer()) {
                xVar.a(7, getEnableAccelerometer());
            }
            if (hasDisplayZoomSlider()) {
                xVar.a(8, getDisplayZoomSlider());
            }
            if (hasUserId()) {
                xVar.a(9, getUserId());
            }
            if (hasLastLevelPlayed()) {
                xVar.a(10, getLastLevelPlayed());
            }
            if (hasTimestampFirstPlayed()) {
                xVar.a(11, getTimestampFirstPlayed());
            }
            if (hasStartupCount()) {
                xVar.a(12, getStartupCount());
            }
            if (hasSoundEnabled()) {
                xVar.a(13, getSoundEnabled());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (hasDetailLevel()) {
                i2 = x.e(1, getDetailLevel()) + 0;
            }
            if (hasDoAutoJoining()) {
                getDoAutoJoining();
                i2 += x.e(2);
            }
            if (hasToneDeafness()) {
                getToneDeafness();
                i2 += x.d(3);
            }
            if (hasBlinkOnWin()) {
                getBlinkOnWin();
                i2 += x.e(4);
            }
            if (hasBlinkOnWinSeconds()) {
                i2 += x.e(5, getBlinkOnWinSeconds());
            }
            if (hasShowSolution()) {
                getShowSolution();
                i2 += x.e(6);
            }
            if (hasEnableAccelerometer()) {
                getEnableAccelerometer();
                i2 += x.e(7);
            }
            if (hasDisplayZoomSlider()) {
                getDisplayZoomSlider();
                i2 += x.e(8);
            }
            if (hasUserId()) {
                i2 += x.b(9, getUserId());
            }
            if (hasLastLevelPlayed()) {
                i2 += x.b(10, getLastLevelPlayed());
            }
            if (hasTimestampFirstPlayed()) {
                i2 += x.b(11, getTimestampFirstPlayed());
            }
            if (hasStartupCount()) {
                i2 += x.b(12, getStartupCount());
            }
            if (hasSoundEnabled()) {
                getSoundEnabled();
                i2 += x.e(13);
            }
            int serializedSize = i2 + getUnknownFields().getSerializedSize();
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static Settings parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.Settings.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.Settings.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.Settings.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.Settings.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.Settings.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$Settings$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static Settings parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static Settings parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.Settings.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.Settings.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.Settings.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.Settings.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.Settings.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$Settings$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static Settings parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static Settings parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.Settings.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.Settings.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.Settings.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.Settings.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.Settings.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$Settings$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static Settings parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static Settings parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static Settings parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static Settings parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.jigsaur.Jigsaur.Settings.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$Settings$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.Settings.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.Settings.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.Settings.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.Settings.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.Settings.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$Settings$Builder */
        public static Settings parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(Settings settings) {
            return newBuilder().mergeFrom(settings);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private Settings result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new Settings((ai) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final Settings internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new Settings((ai) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return Settings.getDescriptor();
            }

            public final Settings getDefaultInstanceForType() {
                return Settings.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final Settings build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public Settings buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final Settings buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                Settings settings = this.result;
                this.result = null;
                return settings;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof Settings) {
                    return mergeFrom((Settings) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(Settings settings) {
                if (settings != Settings.getDefaultInstance()) {
                    if (settings.hasDetailLevel()) {
                        setDetailLevel(settings.getDetailLevel());
                    }
                    if (settings.hasDoAutoJoining()) {
                        setDoAutoJoining(settings.getDoAutoJoining());
                    }
                    if (settings.hasToneDeafness()) {
                        setToneDeafness(settings.getToneDeafness());
                    }
                    if (settings.hasBlinkOnWin()) {
                        setBlinkOnWin(settings.getBlinkOnWin());
                    }
                    if (settings.hasBlinkOnWinSeconds()) {
                        setBlinkOnWinSeconds(settings.getBlinkOnWinSeconds());
                    }
                    if (settings.hasShowSolution()) {
                        setShowSolution(settings.getShowSolution());
                    }
                    if (settings.hasEnableAccelerometer()) {
                        setEnableAccelerometer(settings.getEnableAccelerometer());
                    }
                    if (settings.hasDisplayZoomSlider()) {
                        setDisplayZoomSlider(settings.getDisplayZoomSlider());
                    }
                    if (settings.hasUserId()) {
                        setUserId(settings.getUserId());
                    }
                    if (settings.hasLastLevelPlayed()) {
                        setLastLevelPlayed(settings.getLastLevelPlayed());
                    }
                    if (settings.hasTimestampFirstPlayed()) {
                        setTimestampFirstPlayed(settings.getTimestampFirstPlayed());
                    }
                    if (settings.hasStartupCount()) {
                        setStartupCount(settings.getStartupCount());
                    }
                    if (settings.hasSoundEnabled()) {
                        setSoundEnabled(settings.getSoundEnabled());
                    }
                    mergeUnknownFields(settings.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 8:
                            setDetailLevel(hVar.l());
                            break;
                        case 16:
                            setDoAutoJoining(hVar.i());
                            break;
                        case 29:
                            setToneDeafness(hVar.c());
                            break;
                        case 32:
                            setBlinkOnWin(hVar.i());
                            break;
                        case 40:
                            setBlinkOnWinSeconds(hVar.l());
                            break;
                        case 48:
                            setShowSolution(hVar.i());
                            break;
                        case 56:
                            setEnableAccelerometer(hVar.i());
                            break;
                        case 64:
                            setDisplayZoomSlider(hVar.i());
                            break;
                        case 72:
                            setUserId(hVar.d());
                            break;
                        case 82:
                            setLastLevelPlayed(hVar.j());
                            break;
                        case 88:
                            setTimestampFirstPlayed(hVar.d());
                            break;
                        case 96:
                            setStartupCount(hVar.d());
                            break;
                        case 104:
                            setSoundEnabled(hVar.i());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasDetailLevel() {
                return this.result.hasDetailLevel();
            }

            public final int getDetailLevel() {
                return this.result.getDetailLevel();
            }

            public final Builder setDetailLevel(int i) {
                boolean unused = this.result.hasDetailLevel = true;
                int unused2 = this.result.detailLevel_ = i;
                return this;
            }

            public final Builder clearDetailLevel() {
                boolean unused = this.result.hasDetailLevel = false;
                int unused2 = this.result.detailLevel_ = 0;
                return this;
            }

            public final boolean hasDoAutoJoining() {
                return this.result.hasDoAutoJoining();
            }

            public final boolean getDoAutoJoining() {
                return this.result.getDoAutoJoining();
            }

            public final Builder setDoAutoJoining(boolean z) {
                boolean unused = this.result.hasDoAutoJoining = true;
                boolean unused2 = this.result.doAutoJoining_ = z;
                return this;
            }

            public final Builder clearDoAutoJoining() {
                boolean unused = this.result.hasDoAutoJoining = false;
                boolean unused2 = this.result.doAutoJoining_ = false;
                return this;
            }

            public final boolean hasToneDeafness() {
                return this.result.hasToneDeafness();
            }

            public final float getToneDeafness() {
                return this.result.getToneDeafness();
            }

            public final Builder setToneDeafness(float f) {
                boolean unused = this.result.hasToneDeafness = true;
                float unused2 = this.result.toneDeafness_ = f;
                return this;
            }

            public final Builder clearToneDeafness() {
                boolean unused = this.result.hasToneDeafness = false;
                float unused2 = this.result.toneDeafness_ = 0.0f;
                return this;
            }

            public final boolean hasBlinkOnWin() {
                return this.result.hasBlinkOnWin();
            }

            public final boolean getBlinkOnWin() {
                return this.result.getBlinkOnWin();
            }

            public final Builder setBlinkOnWin(boolean z) {
                boolean unused = this.result.hasBlinkOnWin = true;
                boolean unused2 = this.result.blinkOnWin_ = z;
                return this;
            }

            public final Builder clearBlinkOnWin() {
                boolean unused = this.result.hasBlinkOnWin = false;
                boolean unused2 = this.result.blinkOnWin_ = false;
                return this;
            }

            public final boolean hasBlinkOnWinSeconds() {
                return this.result.hasBlinkOnWinSeconds();
            }

            public final int getBlinkOnWinSeconds() {
                return this.result.getBlinkOnWinSeconds();
            }

            public final Builder setBlinkOnWinSeconds(int i) {
                boolean unused = this.result.hasBlinkOnWinSeconds = true;
                int unused2 = this.result.blinkOnWinSeconds_ = i;
                return this;
            }

            public final Builder clearBlinkOnWinSeconds() {
                boolean unused = this.result.hasBlinkOnWinSeconds = false;
                int unused2 = this.result.blinkOnWinSeconds_ = 0;
                return this;
            }

            public final boolean hasShowSolution() {
                return this.result.hasShowSolution();
            }

            public final boolean getShowSolution() {
                return this.result.getShowSolution();
            }

            public final Builder setShowSolution(boolean z) {
                boolean unused = this.result.hasShowSolution = true;
                boolean unused2 = this.result.showSolution_ = z;
                return this;
            }

            public final Builder clearShowSolution() {
                boolean unused = this.result.hasShowSolution = false;
                boolean unused2 = this.result.showSolution_ = false;
                return this;
            }

            public final boolean hasEnableAccelerometer() {
                return this.result.hasEnableAccelerometer();
            }

            public final boolean getEnableAccelerometer() {
                return this.result.getEnableAccelerometer();
            }

            public final Builder setEnableAccelerometer(boolean z) {
                boolean unused = this.result.hasEnableAccelerometer = true;
                boolean unused2 = this.result.enableAccelerometer_ = z;
                return this;
            }

            public final Builder clearEnableAccelerometer() {
                boolean unused = this.result.hasEnableAccelerometer = false;
                boolean unused2 = this.result.enableAccelerometer_ = false;
                return this;
            }

            public final boolean hasDisplayZoomSlider() {
                return this.result.hasDisplayZoomSlider();
            }

            public final boolean getDisplayZoomSlider() {
                return this.result.getDisplayZoomSlider();
            }

            public final Builder setDisplayZoomSlider(boolean z) {
                boolean unused = this.result.hasDisplayZoomSlider = true;
                boolean unused2 = this.result.displayZoomSlider_ = z;
                return this;
            }

            public final Builder clearDisplayZoomSlider() {
                boolean unused = this.result.hasDisplayZoomSlider = false;
                boolean unused2 = this.result.displayZoomSlider_ = false;
                return this;
            }

            public final boolean hasUserId() {
                return this.result.hasUserId();
            }

            public final long getUserId() {
                return this.result.getUserId();
            }

            public final Builder setUserId(long j) {
                boolean unused = this.result.hasUserId = true;
                long unused2 = this.result.userId_ = j;
                return this;
            }

            public final Builder clearUserId() {
                boolean unused = this.result.hasUserId = false;
                long unused2 = this.result.userId_ = 0;
                return this;
            }

            public final boolean hasLastLevelPlayed() {
                return this.result.hasLastLevelPlayed();
            }

            public final String getLastLevelPlayed() {
                return this.result.getLastLevelPlayed();
            }

            public final Builder setLastLevelPlayed(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasLastLevelPlayed = true;
                String unused2 = this.result.lastLevelPlayed_ = str;
                return this;
            }

            public final Builder clearLastLevelPlayed() {
                boolean unused = this.result.hasLastLevelPlayed = false;
                String unused2 = this.result.lastLevelPlayed_ = Settings.getDefaultInstance().getLastLevelPlayed();
                return this;
            }

            public final boolean hasTimestampFirstPlayed() {
                return this.result.hasTimestampFirstPlayed();
            }

            public final long getTimestampFirstPlayed() {
                return this.result.getTimestampFirstPlayed();
            }

            public final Builder setTimestampFirstPlayed(long j) {
                boolean unused = this.result.hasTimestampFirstPlayed = true;
                long unused2 = this.result.timestampFirstPlayed_ = j;
                return this;
            }

            public final Builder clearTimestampFirstPlayed() {
                boolean unused = this.result.hasTimestampFirstPlayed = false;
                long unused2 = this.result.timestampFirstPlayed_ = 0;
                return this;
            }

            public final boolean hasStartupCount() {
                return this.result.hasStartupCount();
            }

            public final long getStartupCount() {
                return this.result.getStartupCount();
            }

            public final Builder setStartupCount(long j) {
                boolean unused = this.result.hasStartupCount = true;
                long unused2 = this.result.startupCount_ = j;
                return this;
            }

            public final Builder clearStartupCount() {
                boolean unused = this.result.hasStartupCount = false;
                long unused2 = this.result.startupCount_ = 0;
                return this;
            }

            public final boolean hasSoundEnabled() {
                return this.result.hasSoundEnabled();
            }

            public final boolean getSoundEnabled() {
                return this.result.getSoundEnabled();
            }

            public final Builder setSoundEnabled(boolean z) {
                boolean unused = this.result.hasSoundEnabled = true;
                boolean unused2 = this.result.soundEnabled_ = z;
                return this;
            }

            public final Builder clearSoundEnabled() {
                boolean unused = this.result.hasSoundEnabled = false;
                boolean unused2 = this.result.soundEnabled_ = false;
                return this;
            }
        }
    }

    public static final class VirtualSequence extends GeneratedMessage {
        public static final int ANGLE_FIELD_NUMBER = 9;
        public static final int POINTER_FIELD_NUMBER = 2;
        public static final int TIME_END_FIELD_NUMBER = 8;
        public static final int TIME_START_FIELD_NUMBER = 7;
        public static final int TYPE_FIELD_NUMBER = 1;
        public static final int X1_FIELD_NUMBER = 3;
        public static final int X2_FIELD_NUMBER = 5;
        public static final int Y1_FIELD_NUMBER = 4;
        public static final int Y2_FIELD_NUMBER = 6;
        private static final VirtualSequence defaultInstance;
        /* access modifiers changed from: private */
        public float angle_;
        /* access modifiers changed from: private */
        public boolean hasAngle;
        /* access modifiers changed from: private */
        public boolean hasPointer;
        /* access modifiers changed from: private */
        public boolean hasTimeEnd;
        /* access modifiers changed from: private */
        public boolean hasTimeStart;
        /* access modifiers changed from: private */
        public boolean hasType;
        /* access modifiers changed from: private */
        public boolean hasX1;
        /* access modifiers changed from: private */
        public boolean hasX2;
        /* access modifiers changed from: private */
        public boolean hasY1;
        /* access modifiers changed from: private */
        public boolean hasY2;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public int pointer_;
        /* access modifiers changed from: private */
        public int timeEnd_;
        /* access modifiers changed from: private */
        public int timeStart_;
        /* access modifiers changed from: private */
        public a type_;
        /* access modifiers changed from: private */
        public float x1_;
        /* access modifiers changed from: private */
        public float x2_;
        /* access modifiers changed from: private */
        public float y1_;
        /* access modifiers changed from: private */
        public float y2_;

        /* synthetic */ VirtualSequence(ai aiVar) {
            this();
        }

        private VirtualSequence() {
            this.pointer_ = 0;
            this.x1_ = 0.0f;
            this.y1_ = 0.0f;
            this.x2_ = 0.0f;
            this.y2_ = 0.0f;
            this.timeStart_ = 0;
            this.timeEnd_ = 0;
            this.angle_ = 0.0f;
            this.memoizedSerializedSize = -1;
            this.type_ = a.LINEAR;
        }

        private VirtualSequence(boolean z) {
            this.pointer_ = 0;
            this.x1_ = 0.0f;
            this.y1_ = 0.0f;
            this.x2_ = 0.0f;
            this.y2_ = 0.0f;
            this.timeStart_ = 0;
            this.timeEnd_ = 0;
            this.angle_ = 0.0f;
            this.memoizedSerializedSize = -1;
        }

        public static VirtualSequence getDefaultInstance() {
            return defaultInstance;
        }

        public final VirtualSequence getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Jigsaur.I;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Jigsaur.J;
        }

        public enum a implements v {
            LINEAR(0, 1),
            CIRCULAR(1, 2);
            
            private static g.b<a> c = new e();
            private static final a[] d = {LINEAR, CIRCULAR};
            private final int e;
            private final int f;

            public final int d_() {
                return this.f;
            }

            public static a a(int i) {
                switch (i) {
                    case 1:
                        return LINEAR;
                    case 2:
                        return CIRCULAR;
                    default:
                        return null;
                }
            }

            private a(int i, int i2) {
                this.e = i;
                this.f = i2;
            }
        }

        public final boolean hasType() {
            return this.hasType;
        }

        public final a getType() {
            return this.type_;
        }

        public final boolean hasPointer() {
            return this.hasPointer;
        }

        public final int getPointer() {
            return this.pointer_;
        }

        public final boolean hasX1() {
            return this.hasX1;
        }

        public final float getX1() {
            return this.x1_;
        }

        public final boolean hasY1() {
            return this.hasY1;
        }

        public final float getY1() {
            return this.y1_;
        }

        public final boolean hasX2() {
            return this.hasX2;
        }

        public final float getX2() {
            return this.x2_;
        }

        public final boolean hasY2() {
            return this.hasY2;
        }

        public final float getY2() {
            return this.y2_;
        }

        public final boolean hasTimeStart() {
            return this.hasTimeStart;
        }

        public final int getTimeStart() {
            return this.timeStart_;
        }

        public final boolean hasTimeEnd() {
            return this.hasTimeEnd;
        }

        public final int getTimeEnd() {
            return this.timeEnd_;
        }

        public final boolean hasAngle() {
            return this.hasAngle;
        }

        public final float getAngle() {
            return this.angle_;
        }

        private void initFields() {
            this.type_ = a.LINEAR;
        }

        public final boolean isInitialized() {
            if (this.hasType && this.hasPointer && this.hasX1 && this.hasY1 && this.hasX2 && this.hasY2 && this.hasTimeStart && this.hasTimeEnd) {
                return true;
            }
            return false;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasType()) {
                xVar.c(1, getType().d_());
            }
            if (hasPointer()) {
                xVar.b(2, getPointer());
            }
            if (hasX1()) {
                xVar.a(3, getX1());
            }
            if (hasY1()) {
                xVar.a(4, getY1());
            }
            if (hasX2()) {
                xVar.a(5, getX2());
            }
            if (hasY2()) {
                xVar.a(6, getY2());
            }
            if (hasTimeStart()) {
                xVar.b(7, getTimeStart());
            }
            if (hasTimeEnd()) {
                xVar.b(8, getTimeEnd());
            }
            if (hasAngle()) {
                xVar.a(9, getAngle());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (hasType()) {
                i2 = x.f(1, getType().d_()) + 0;
            }
            if (hasPointer()) {
                i2 += x.e(2, getPointer());
            }
            if (hasX1()) {
                getX1();
                i2 += x.d(3);
            }
            if (hasY1()) {
                getY1();
                i2 += x.d(4);
            }
            if (hasX2()) {
                getX2();
                i2 += x.d(5);
            }
            if (hasY2()) {
                getY2();
                i2 += x.d(6);
            }
            if (hasTimeStart()) {
                i2 += x.e(7, getTimeStart());
            }
            if (hasTimeEnd()) {
                i2 += x.e(8, getTimeEnd());
            }
            if (hasAngle()) {
                getAngle();
                i2 += x.d(9);
            }
            int serializedSize = i2 + getUnknownFields().getSerializedSize();
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static VirtualSequence parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.VirtualSequence.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequence.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequence.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequence.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequence.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$VirtualSequence$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static VirtualSequence parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static VirtualSequence parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.VirtualSequence.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequence.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequence.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequence.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequence.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$VirtualSequence$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static VirtualSequence parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static VirtualSequence parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.VirtualSequence.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequence.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequence.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequence.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequence.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$VirtualSequence$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static VirtualSequence parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static VirtualSequence parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static VirtualSequence parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static VirtualSequence parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.jigsaur.Jigsaur.VirtualSequence.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$VirtualSequence$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.VirtualSequence.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequence.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequence.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequence.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequence.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$VirtualSequence$Builder */
        public static VirtualSequence parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(VirtualSequence virtualSequence) {
            return newBuilder().mergeFrom(virtualSequence);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private VirtualSequence result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new VirtualSequence((ai) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final VirtualSequence internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new VirtualSequence((ai) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return VirtualSequence.getDescriptor();
            }

            public final VirtualSequence getDefaultInstanceForType() {
                return VirtualSequence.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final VirtualSequence build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public VirtualSequence buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final VirtualSequence buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                VirtualSequence virtualSequence = this.result;
                this.result = null;
                return virtualSequence;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof VirtualSequence) {
                    return mergeFrom((VirtualSequence) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(VirtualSequence virtualSequence) {
                if (virtualSequence != VirtualSequence.getDefaultInstance()) {
                    if (virtualSequence.hasType()) {
                        setType(virtualSequence.getType());
                    }
                    if (virtualSequence.hasPointer()) {
                        setPointer(virtualSequence.getPointer());
                    }
                    if (virtualSequence.hasX1()) {
                        setX1(virtualSequence.getX1());
                    }
                    if (virtualSequence.hasY1()) {
                        setY1(virtualSequence.getY1());
                    }
                    if (virtualSequence.hasX2()) {
                        setX2(virtualSequence.getX2());
                    }
                    if (virtualSequence.hasY2()) {
                        setY2(virtualSequence.getY2());
                    }
                    if (virtualSequence.hasTimeStart()) {
                        setTimeStart(virtualSequence.getTimeStart());
                    }
                    if (virtualSequence.hasTimeEnd()) {
                        setTimeEnd(virtualSequence.getTimeEnd());
                    }
                    if (virtualSequence.hasAngle()) {
                        setAngle(virtualSequence.getAngle());
                    }
                    mergeUnknownFields(virtualSequence.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = com.google.protobuf.a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 8:
                            int m = hVar.m();
                            a a3 = a.a(m);
                            if (a3 != null) {
                                setType(a3);
                                break;
                            } else {
                                a.a(1, m);
                                break;
                            }
                        case 16:
                            setPointer(hVar.l());
                            break;
                        case 29:
                            setX1(hVar.c());
                            break;
                        case 37:
                            setY1(hVar.c());
                            break;
                        case 45:
                            setX2(hVar.c());
                            break;
                        case 53:
                            setY2(hVar.c());
                            break;
                        case 56:
                            setTimeStart(hVar.l());
                            break;
                        case 64:
                            setTimeEnd(hVar.l());
                            break;
                        case 77:
                            setAngle(hVar.c());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasType() {
                return this.result.hasType();
            }

            public final a getType() {
                return this.result.getType();
            }

            public final Builder setType(a aVar) {
                if (aVar == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasType = true;
                a unused2 = this.result.type_ = aVar;
                return this;
            }

            public final Builder clearType() {
                boolean unused = this.result.hasType = false;
                a unused2 = this.result.type_ = a.LINEAR;
                return this;
            }

            public final boolean hasPointer() {
                return this.result.hasPointer();
            }

            public final int getPointer() {
                return this.result.getPointer();
            }

            public final Builder setPointer(int i) {
                boolean unused = this.result.hasPointer = true;
                int unused2 = this.result.pointer_ = i;
                return this;
            }

            public final Builder clearPointer() {
                boolean unused = this.result.hasPointer = false;
                int unused2 = this.result.pointer_ = 0;
                return this;
            }

            public final boolean hasX1() {
                return this.result.hasX1();
            }

            public final float getX1() {
                return this.result.getX1();
            }

            public final Builder setX1(float f) {
                boolean unused = this.result.hasX1 = true;
                float unused2 = this.result.x1_ = f;
                return this;
            }

            public final Builder clearX1() {
                boolean unused = this.result.hasX1 = false;
                float unused2 = this.result.x1_ = 0.0f;
                return this;
            }

            public final boolean hasY1() {
                return this.result.hasY1();
            }

            public final float getY1() {
                return this.result.getY1();
            }

            public final Builder setY1(float f) {
                boolean unused = this.result.hasY1 = true;
                float unused2 = this.result.y1_ = f;
                return this;
            }

            public final Builder clearY1() {
                boolean unused = this.result.hasY1 = false;
                float unused2 = this.result.y1_ = 0.0f;
                return this;
            }

            public final boolean hasX2() {
                return this.result.hasX2();
            }

            public final float getX2() {
                return this.result.getX2();
            }

            public final Builder setX2(float f) {
                boolean unused = this.result.hasX2 = true;
                float unused2 = this.result.x2_ = f;
                return this;
            }

            public final Builder clearX2() {
                boolean unused = this.result.hasX2 = false;
                float unused2 = this.result.x2_ = 0.0f;
                return this;
            }

            public final boolean hasY2() {
                return this.result.hasY2();
            }

            public final float getY2() {
                return this.result.getY2();
            }

            public final Builder setY2(float f) {
                boolean unused = this.result.hasY2 = true;
                float unused2 = this.result.y2_ = f;
                return this;
            }

            public final Builder clearY2() {
                boolean unused = this.result.hasY2 = false;
                float unused2 = this.result.y2_ = 0.0f;
                return this;
            }

            public final boolean hasTimeStart() {
                return this.result.hasTimeStart();
            }

            public final int getTimeStart() {
                return this.result.getTimeStart();
            }

            public final Builder setTimeStart(int i) {
                boolean unused = this.result.hasTimeStart = true;
                int unused2 = this.result.timeStart_ = i;
                return this;
            }

            public final Builder clearTimeStart() {
                boolean unused = this.result.hasTimeStart = false;
                int unused2 = this.result.timeStart_ = 0;
                return this;
            }

            public final boolean hasTimeEnd() {
                return this.result.hasTimeEnd();
            }

            public final int getTimeEnd() {
                return this.result.getTimeEnd();
            }

            public final Builder setTimeEnd(int i) {
                boolean unused = this.result.hasTimeEnd = true;
                int unused2 = this.result.timeEnd_ = i;
                return this;
            }

            public final Builder clearTimeEnd() {
                boolean unused = this.result.hasTimeEnd = false;
                int unused2 = this.result.timeEnd_ = 0;
                return this;
            }

            public final boolean hasAngle() {
                return this.result.hasAngle();
            }

            public final float getAngle() {
                return this.result.getAngle();
            }

            public final Builder setAngle(float f) {
                boolean unused = this.result.hasAngle = true;
                float unused2 = this.result.angle_ = f;
                return this;
            }

            public final Builder clearAngle() {
                boolean unused = this.result.hasAngle = false;
                float unused2 = this.result.angle_ = 0.0f;
                return this;
            }
        }

        static {
            VirtualSequence virtualSequence = new VirtualSequence(true);
            defaultInstance = virtualSequence;
            virtualSequence.type_ = a.LINEAR;
        }
    }

    public static final class VirtualSequences extends GeneratedMessage {
        public static final int SEQUENCES_FIELD_NUMBER = 1;
        private static final VirtualSequences defaultInstance = new VirtualSequences(true);
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public List<VirtualSequence> sequences_;

        /* synthetic */ VirtualSequences(ai aiVar) {
            this();
        }

        private VirtualSequences() {
            this.sequences_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        private VirtualSequences(boolean z) {
            this.sequences_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        public static VirtualSequences getDefaultInstance() {
            return defaultInstance;
        }

        public final VirtualSequences getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Jigsaur.K;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Jigsaur.L;
        }

        public final List<VirtualSequence> getSequencesList() {
            return this.sequences_;
        }

        public final int getSequencesCount() {
            return this.sequences_.size();
        }

        public final VirtualSequence getSequences(int i) {
            return this.sequences_.get(i);
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            for (VirtualSequence isInitialized : getSequencesList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            for (VirtualSequence b : getSequencesList()) {
                xVar.b(1, b);
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            Iterator<VirtualSequence> it = getSequencesList().iterator();
            while (true) {
                int i3 = i2;
                if (it.hasNext()) {
                    i2 = x.d(1, it.next()) + i3;
                } else {
                    int serializedSize = getUnknownFields().getSerializedSize() + i3;
                    this.memoizedSerializedSize = serializedSize;
                    return serializedSize;
                }
            }
        }

        public static VirtualSequences parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.VirtualSequences.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequences.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequences.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequences.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequences.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$VirtualSequences$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static VirtualSequences parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static VirtualSequences parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.VirtualSequences.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequences.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequences.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequences.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequences.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$VirtualSequences$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static VirtualSequences parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static VirtualSequences parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.VirtualSequences.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequences.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequences.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequences.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequences.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$VirtualSequences$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static VirtualSequences parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static VirtualSequences parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static VirtualSequences parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static VirtualSequences parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.jigsaur.Jigsaur.VirtualSequences.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$VirtualSequences$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.jigsaur.Jigsaur.VirtualSequences.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequences.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequences.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequences.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.jigsaur.Jigsaur.VirtualSequences.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.jigsaur.Jigsaur$VirtualSequences$Builder */
        public static VirtualSequences parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(VirtualSequences virtualSequences) {
            return newBuilder().mergeFrom(virtualSequences);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private VirtualSequences result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new VirtualSequences((ai) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final VirtualSequences internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new VirtualSequences((ai) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return VirtualSequences.getDescriptor();
            }

            public final VirtualSequences getDefaultInstanceForType() {
                return VirtualSequences.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final VirtualSequences build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public VirtualSequences buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final VirtualSequences buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.sequences_ != Collections.EMPTY_LIST) {
                    List unused = this.result.sequences_ = Collections.unmodifiableList(this.result.sequences_);
                }
                VirtualSequences virtualSequences = this.result;
                this.result = null;
                return virtualSequences;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof VirtualSequences) {
                    return mergeFrom((VirtualSequences) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(VirtualSequences virtualSequences) {
                if (virtualSequences != VirtualSequences.getDefaultInstance()) {
                    if (!virtualSequences.sequences_.isEmpty()) {
                        if (this.result.sequences_.isEmpty()) {
                            List unused = this.result.sequences_ = new ArrayList();
                        }
                        this.result.sequences_.addAll(virtualSequences.sequences_);
                    }
                    mergeUnknownFields(virtualSequences.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 10:
                            VirtualSequence.Builder newBuilder = VirtualSequence.newBuilder();
                            hVar.a(newBuilder, abVar);
                            addSequences(newBuilder.buildPartial());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final List<VirtualSequence> getSequencesList() {
                return Collections.unmodifiableList(this.result.sequences_);
            }

            public final int getSequencesCount() {
                return this.result.getSequencesCount();
            }

            public final VirtualSequence getSequences(int i) {
                return this.result.getSequences(i);
            }

            public final Builder setSequences(int i, VirtualSequence virtualSequence) {
                if (virtualSequence == null) {
                    throw new NullPointerException();
                }
                this.result.sequences_.set(i, virtualSequence);
                return this;
            }

            public final Builder setSequences(int i, VirtualSequence.Builder builder) {
                this.result.sequences_.set(i, builder.build());
                return this;
            }

            public final Builder addSequences(VirtualSequence virtualSequence) {
                if (virtualSequence == null) {
                    throw new NullPointerException();
                }
                if (this.result.sequences_.isEmpty()) {
                    List unused = this.result.sequences_ = new ArrayList();
                }
                this.result.sequences_.add(virtualSequence);
                return this;
            }

            public final Builder addSequences(VirtualSequence.Builder builder) {
                if (this.result.sequences_.isEmpty()) {
                    List unused = this.result.sequences_ = new ArrayList();
                }
                this.result.sequences_.add(builder.build());
                return this;
            }

            public final Builder addAllSequences(Iterable<? extends VirtualSequence> iterable) {
                if (this.result.sequences_.isEmpty()) {
                    List unused = this.result.sequences_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.sequences_);
                return this;
            }

            public final Builder clearSequences() {
                List unused = this.result.sequences_ = Collections.emptyList();
                return this;
            }
        }
    }

    public static c.b a() {
        return M;
    }

    static {
        c.b.a(new String[]{"\n\rjigsaur.proto\u0012\u0016com.ptowngames.jigsaur\"%\n\rPolygonVertex\u0012\t\n\u0001x\u0018\u0001 \u0002(\u0002\u0012\t\n\u0001y\u0018\u0002 \u0002(\u0002\"*\n\u0007Vertex3\u0012\t\n\u0001x\u0018\u0001 \u0002(\u0002\u0012\t\n\u0001y\u0018\u0002 \u0002(\u0002\u0012\t\n\u0001z\u0018\u0003 \u0002(\u0002\"©\u0001\n\nJoinCorner\u0012\f\n\u0004xoff\u0018\u0001 \u0002(\u0002\u0012\f\n\u0004yoff\u0018\u0002 \u0002(\u0002\u0012\r\n\u0005scale\u0018\u0003 \u0002(\u0002\u0012\u001b\n\u0013north_join_priority\u0018\u0004 \u0002(\r\u0012\u001a\n\u0012east_join_priority\u0018\u0005 \u0002(\r\u0012\u001b\n\u0013south_join_priority\u0018\u0006 \u0002(\r\u0012\u001a\n\u0012west_join_priority\u0018\u0007 \u0002(\r\"\u0001\n\bJoinInfo\u0012A\n\u0012join_circle_center\u0018\u0001 \u0002(\u000b2%.com.ptowngames.jigsaur.PolygonVertex\u0012\u0018\n\u0010base_orientation\u0018\u0002 \u0002(\u0002", "\u0012\u0013\n\u000bneighbor_id\u0018\u0003 \u0002(\r\u0012\u0015\n\routtie_status\u0018\u0004 \u0002(\b\"í\u0001\n\u0005Piece\u0012\n\n\u0002id\u0018\u0001 \u0002(\r\u0012\u001b\n\u000fpolygon_indices\u0018\u0002 \u0003(\rB\u0002\u0010\u0001\u0012$\n\u0018extruded_polygon_indices\u0018\u0003 \u0003(\rB\u0002\u0010\u0001\u00124\n\njoin_infos\u0018\u0004 \u0003(\u000b2 .com.ptowngames.jigsaur.JoinInfo\u0012!\n\u0015triangulation_indices\u0018\u0005 \u0003(\rB\u0002\u0010\u0001\u0012<\n\rcorner_points\u0018\u0006 \u0003(\u000b2%.com.ptowngames.jigsaur.PolygonVertex\"Æ\u0001\n\bPieceSet\u0012-\n\u0006pieces\u0018\u0001 \u0003(\u000b2\u001d.com.ptowngames.jigsaur.Piece\u0012\u0016\n\u000epiece_vertices\u0018\u0002 \u0002(\f\u0012\u0019\n\u0011num_total_indices\u0018\u0003 \u0002(\r\u0012\u001d\n\u0015m", "aximum_join_distance\u0018\u0004 \u0002(\u0002\u0012\u000f\n\u0007unused1\u0018\u0005 \u0001(\u0002\u0012\u000f\n\u0007unused2\u0018\u0006 \u0001(\u0002\u0012\u0017\n\u000fextrusion_depth\u0018\u0007 \u0002(\u0002\"J\n\fPieceSetStub\u0012\"\n\u001apiece_set_file_name_prefix\u0018\u0001 \u0002(\t\u0012\u0016\n\u000epiece_set_type\u0018\u0002 \u0002(\t\"D\n\rPieceSetStubs\u00123\n\u0005stubs\u0018\u0001 \u0003(\u000b2$.com.ptowngames.jigsaur.PieceSetStub\"\u0002\n\u0019TypeOnePrimordialPieceSet\u0012\u0015\n\rnum_verticals\u0018\u0001 \u0002(\r\u0012\u0017\n\u000fnum_horizontals\u0018\u0002 \u0002(\r\u0012:\n\u000bgrid_points\u0018\u0003 \u0003(\u000b2%.com.ptowngames.jigsaur.PolygonVertex\u00128\n\fjoin_corners\u0018\u0004 \u0003(\u000b2\".com.pt", "owngames.jigsaur.JoinCorner\u0012\u0017\n\u000fextrusion_depth\u0018\u0005 \u0002(\u0002\u0012\u0013\n\u000bboard_width\u0018\u0006 \u0002(\u0002\u0012\u0014\n\fboard_height\u0018\u0007 \u0002(\u0002\"ò\u0001\n\u0016PieceGlobConfiguration\u0012\u0011\n\tpiece_ids\u0018\u0001 \u0003(\r\u0012/\n\u0006center\u0018\u0002 \u0002(\u000b2\u001f.com.ptowngames.jigsaur.Vertex3\u0012\r\n\u0005angle\u0018\u0003 \u0002(\u0002\u0012G\n\u0005state\u0018\u0004 \u0002(\u000e28.com.ptowngames.jigsaur.PieceGlobConfiguration.GlobState\"<\n\tGlobState\u0012\u000b\n\u0007INVALID\u0010\u0000\u0012\n\n\u0006RAISED\u0010\u0001\u0012\n\n\u0006SOLVED\u0010\u0002\u0012\n\n\u0006NORMAL\u0010\u0003\"W\n\u0016WorkspaceConfiguration\u0012=\n\u0005globs\u0018\u0001 \u0003(\u000b2..com.ptowngames.j", "igsaur.PieceGlobConfiguration\"À\u0001\n\u0012SavedConfiguration\u0012\u0012\n\nnum_pieces\u0018\u0001 \u0002(\r\u0012B\n\nworkspaces\u0018\u0002 \u0003(\u000b2..com.ptowngames.jigsaur.WorkspaceConfiguration\u0012<\n\rcamera_center\u0018\u0003 \u0002(\u000b2%.com.ptowngames.jigsaur.PolygonVertex\u0012\u0014\n\fscale_factor\u0018\u0004 \u0002(\u0002\"Ê\u0002\n\u000fPuzzleImageInfo\u0012?\n\u0004type\u0018\u0001 \u0002(\u000e21.com.ptowngames.jigsaur.PuzzleImageInfo.ImageType\u0012\u0012\n\nnum_frames\u0018\u0002 \u0002(\r\u0012\u000b\n\u0003fps\u0018\u0003 \u0002(\u0002\u0012\u0015\n\rresource_name\u0018\u0004 \u0002(\t\u0012\u0013\n\u000battribution\u0018\u0005 \u0002(\t\u0012\u0013\n\u000bacquisitio", "n\u0018\u0006 \u0002(\t\u0012\u0012\n\ntime_begin\u0018\u0007 \u0001(\u0002\u0012\u0010\n\btime_end\u0018\b \u0001(\u0002\u0012\u000b\n\u0003uid\u0018\t \u0002(\r\u0012\u001a\n\u0012puzzle_image_width\u0018\n \u0002(\u0002\u0012\u001b\n\u0013puzzle_image_height\u0018\u000b \u0002(\u0002\"(\n\tImageType\u0012\t\n\u0005MOVIE\u0010\u0001\u0012\u0010\n\fMOVIE_FRAMES\u0010\u0002\"W\n\u0010PuzzleImageInfos\u0012C\n\u0012puzzle_image_infos\u0018\u0001 \u0003(\u000b2'.com.ptowngames.jigsaur.PuzzleImageInfo\"­\u0001\n\u000eLevelPredicate\u0012B\n\u0004type\u0018\u0001 \u0002(\u000e24.com.ptowngames.jigsaur.LevelPredicate.PredicateType\u0012\u0016\n\u000eprior_level_id\u0018\u0002 \u0001(\r\u0012\u001d\n\u0015completion_percentage\u0018\u0003 \u0001(\r\" \n\rPredicat", "eType\u0012\u000f\n\u000bPRIOR_LEVEL\u0010\u0001\"\u0003\n\u0005Level\u0012\u000b\n\u0003uid\u0018\u0001 \u0002(\r\u0012\u0016\n\u000epiece_set_type\u0018\u0002 \u0002(\t\u0012\u0016\n\u000eimage_info_uid\u0018\u0003 \u0002(\r\u0012J\n\u0016starting_configuration\u0018\u0004 \u0001(\u000b2*.com.ptowngames.jigsaur.SavedConfiguration\u0012\u0010\n\bmin_zoom\u0018\u0005 \u0002(\u0002\u0012\u0010\n\bmax_zoom\u0018\u0006 \u0002(\u0002\u0012\r\n\u0005min_x\u0018\u0007 \u0002(\u0002\u0012\r\n\u0005max_x\u0018\b \u0002(\u0002\u0012\r\n\u0005min_y\u0018\t \u0002(\u0002\u0012\r\n\u0005max_y\u0018\n \u0002(\u0002\u0012\u0015\n\rinternal_name\u0018\u000b \u0002(\t\u0012\u0012\n\nnext_level\u0018\f \u0002(\t\u0012\u0012\n\nprev_level\u0018\r \u0002(\t\u0012\u0019\n\u0011difficulty_family\u0018\u000e \u0003(\t\u0012\u0012\n\nis_premium\u0018d \u0002(\b\u00128\n\bpredices\u0018e \u0003(\u000b2&.com.", "ptowngames.jigsaur.LevelPredicate\"Ð\u0002\n\bSettings\u0012\u0014\n\fdetail_level\u0018\u0001 \u0001(\r\u0012\u0017\n\u000fdo_auto_joining\u0018\u0002 \u0001(\b\u0012\u0015\n\rtone_deafness\u0018\u0003 \u0001(\u0002\u0012\u0014\n\fblink_on_win\u0018\u0004 \u0001(\b\u0012\u001c\n\u0014blink_on_win_seconds\u0018\u0005 \u0001(\r\u0012\u0015\n\rshow_solution\u0018\u0006 \u0001(\b\u0012\u001c\n\u0014enable_accelerometer\u0018\u0007 \u0001(\b\u0012\u001b\n\u0013display_zoom_slider\u0018\b \u0001(\b\u0012\u000f\n\u0007user_id\u0018\t \u0001(\u0004\u0012\u0019\n\u0011last_level_played\u0018\n \u0001(\t\u0012\u001e\n\u0016timestamp_first_played\u0018\u000b \u0001(\u0004\u0012\u0015\n\rstartup_count\u0018\f \u0001(\u0004\u0012\u0015\n\rsound_enabled\u0018\r \u0001(\b\"ó\u0001\n\u000fVirtualSequence\u0012A\n\u0004type", "\u0018\u0001 \u0002(\u000e23.com.ptowngames.jigsaur.VirtualSequence.GestureType\u0012\u000f\n\u0007pointer\u0018\u0002 \u0002(\r\u0012\n\n\u0002x1\u0018\u0003 \u0002(\u0002\u0012\n\n\u0002y1\u0018\u0004 \u0002(\u0002\u0012\n\n\u0002x2\u0018\u0005 \u0002(\u0002\u0012\n\n\u0002y2\u0018\u0006 \u0002(\u0002\u0012\u0012\n\ntime_start\u0018\u0007 \u0002(\r\u0012\u0010\n\btime_end\u0018\b \u0002(\r\u0012\r\n\u0005angle\u0018\t \u0001(\u0002\"'\n\u000bGestureType\u0012\n\n\u0006LINEAR\u0010\u0001\u0012\f\n\bCIRCULAR\u0010\u0002\"N\n\u0010VirtualSequences\u0012:\n\tsequences\u0018\u0001 \u0003(\u000b2'.com.ptowngames.jigsaur.VirtualSequence"}, new c.b[0], new ai());
    }
}
