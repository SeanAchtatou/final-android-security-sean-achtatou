package com.ptowngames.jigsaur;

import com.badlogic.gdx.a.a;
import com.badlogic.gdx.g;
import com.ptowngames.jigsaur.a;
import com.ptowngames.jigsaur.a.d;
import com.ptowngames.jigsaur.c;

public final class af implements c.a {
    private static af d = null;
    private a a = g.c.a(g.e.b("data/yay.ogg"));
    private a b = g.c.a(g.e.b("data/nay.ogg"));
    private a c = g.c.a(g.e.b("data/win.ogg"));

    protected af() {
    }

    public final void a(final Object obj, c.b bVar, Object obj2) {
        switch (bVar) {
            case GLOB_JOIN_FAILURE:
                if (ar.a().f) {
                    this.b.a();
                    return;
                }
                return;
            case GLOB_JOIN_SUCCESS:
                if (ar.a().f) {
                    this.a.a();
                    return;
                }
                return;
            case PUZZLE_SOLVED:
                if (ar.a().f) {
                    this.c.a();
                }
                if (ar.a().b) {
                    ((a) obj).e();
                }
                ar.a();
                d a2 = d.a();
                AnonymousClass1 r1 = new com.ptowngames.jigsaur.a.a() {
                    public final void c() {
                        ((a) obj).f();
                        ((a) obj).d();
                        ab.a().e();
                    }
                };
                ar.a();
                a2.a(r1.a(1500));
                return;
            case GLOB_WAS_CREATED:
                ((a.C0018a) obj2).v();
                return;
            default:
                return;
        }
    }

    public static void a() {
        c.b(d, c.b.PUZZLE_SOLVED);
        c.b(d, c.b.GLOB_JOIN_SUCCESS);
        c.b(d, c.b.GLOB_JOIN_FAILURE);
        c.b(d, c.b.GLOB_WAS_CREATED);
        if (d != null) {
            d.a.b();
            d.b.b();
            d.c.b();
        }
        d = null;
    }

    public static void b() {
        a();
        af afVar = new af();
        d = afVar;
        c.a(afVar, c.b.PUZZLE_SOLVED);
        c.a(d, c.b.GLOB_JOIN_SUCCESS);
        c.a(d, c.b.GLOB_JOIN_FAILURE);
        c.a(d, c.b.GLOB_WAS_CREATED);
    }
}
