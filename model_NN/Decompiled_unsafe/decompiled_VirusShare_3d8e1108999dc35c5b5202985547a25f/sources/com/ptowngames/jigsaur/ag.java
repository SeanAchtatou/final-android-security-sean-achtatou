package com.ptowngames.jigsaur;

import android.os.AsyncTask;
import com.ptowngames.webservices.Webservices;
import com.ptowngames.webservices.c;
import com.ptowngames.webservices.d;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import org.codehaus.jackson.util.BufferRecycler;

public final class ag {

    enum a {
        JIGSAUR(1),
        CATS(1009),
        DOGS(BufferRecycler.DEFAULT_WRITE_CONCAT_BUFFER_LEN),
        CARS(3000),
        BUTTERFLIES(4000),
        FLOWERS(5000),
        LADIES(6000),
        NATURE(7000),
        PANDAS(8000),
        UNDERWATER(9000),
        KIMJONG(10000),
        LADIES2(11000),
        LADIES3(12000),
        PARIS(13000),
        BIRDS(14000),
        DOLPHINS(15000),
        MONKEYS(16000),
        MAX(17000);
        
        private int s;

        private a(int i) {
            this.s = i;
        }

        public final int a() {
            return this.s;
        }
    }

    enum b {
        INVALID(0),
        MAIN_MENU(1),
        PUZZLE_PLAYER(2),
        NUM_IDS(3);
        
        private int e;

        private b(int i) {
            this.e = i;
        }

        public final int a() {
            return this.e;
        }
    }

    public static void a(b bVar, Throwable th) {
        StringWriter stringWriter = new StringWriter();
        th.printStackTrace(new PrintWriter(stringWriter));
        a(bVar, Webservices.RewardCheckRequest.a.PROGRAM_EXCEPTION, stringWriter.toString());
    }

    public static void a(b bVar, Webservices.RewardCheckRequest.a aVar, String str) {
        if (t.a != null) {
            final Webservices.a.C0020a a2 = Webservices.a.a(new d());
            final c cVar = new c();
            Webservices.RewardCheckRequest.Builder eventCode = Webservices.RewardCheckRequest.newBuilder().setUserId(ar.a().g).setLocale(ab.a().d()).setProductId(t.a.a()).setSubproductId(bVar.a()).setEventCode(aVar.d_());
            if (str != null) {
                eventCode.setExtraData(str);
            }
            final Webservices.RewardCheckRequest build = eventCode.build();
            try {
                final b bVar2 = bVar;
                final Webservices.RewardCheckRequest.a aVar2 = aVar;
                final String str2 = str;
                new AsyncTask<Void, Void, Void>() {
                    /* access modifiers changed from: protected */
                    public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
                        return a();
                    }

                    private Void a() {
                        try {
                            a2.a(cVar, build);
                            com.flurry.android.ag.b("" + ar.a().g);
                            String str = "" + bVar2.a() + ":" + aVar2.d_();
                            if (str2 == null || str2 == "") {
                                com.flurry.android.ag.a(str);
                                return null;
                            }
                            HashMap hashMap = new HashMap();
                            hashMap.put("Extras", str2);
                            com.flurry.android.ag.a(str, hashMap);
                            return null;
                        } catch (com.google.protobuf.b e2) {
                            return null;
                        }
                    }
                }.execute(new Void[0]);
            } catch (NoClassDefFoundError e) {
                try {
                    a2.a(cVar, build);
                } catch (com.google.protobuf.b e2) {
                }
            } catch (Exception e3) {
            }
            if (cVar.b()) {
                "Reward request failed: " + cVar.a();
            }
        }
    }
}
