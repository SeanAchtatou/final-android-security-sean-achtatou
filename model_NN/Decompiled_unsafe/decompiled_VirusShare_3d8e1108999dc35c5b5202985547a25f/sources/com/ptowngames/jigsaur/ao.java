package com.ptowngames.jigsaur;

import com.badlogic.gdx.math.f;
import java.util.Iterator;

public class ao extends x {
    private static /* synthetic */ boolean f = (!ao.class.desiredAssertionStatus());
    private float b;
    private float c;
    private float d;
    private boolean e;

    public final void b(float f2) {
        if (f2 != this.d) {
            this.d = f2;
            if (!this.e) {
                return;
            }
            if (f || this.b > 2.0f * this.c) {
                int size = this.a.size();
                f c2 = c();
                float f3 = this.d * (this.b - this.c);
                Iterator it = this.a.iterator();
                int i = 0;
                while (it.hasNext()) {
                    j jVar = (j) it.next();
                    ah ahVar = (ah) jVar;
                    ahVar.a = this.c * this.d;
                    double d2 = (6.283185307179586d * ((double) i)) / ((double) size);
                    ahVar.a(c2.a + (((float) Math.cos(d2)) * f3), (((float) Math.sin(d2)) * f3) + c2.b, c2.c, jVar.k());
                    i++;
                }
                return;
            }
            throw new AssertionError();
        }
    }

    public final void a(aa aaVar, float f2) {
        Iterator it = this.a.iterator();
        while (it.hasNext()) {
            ((ah) ((j) it.next())).c(f2);
        }
        super.a(aaVar);
    }
}
