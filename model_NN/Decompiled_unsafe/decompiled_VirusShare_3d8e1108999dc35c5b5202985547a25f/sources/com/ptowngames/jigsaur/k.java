package com.ptowngames.jigsaur;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.View;
import com.ptowngames.ObservableScrollView;
import com.ptowngames.jigsaur.Jigsaur;
import java.io.IOException;

final class k extends View implements View.OnClickListener {
    private static final Paint j = new Paint();
    private static final float[] k = {0.2125f, 0.7154f, 0.0721f, 0.0f, 0.0f, 0.2125f, 0.7154f, 0.0721f, 0.0f, 0.0f, 0.2125f, 0.7154f, 0.0721f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f};
    private static final ColorMatrixColorFilter l = new ColorMatrixColorFilter(k);
    private Jigsaur.Level a;
    private Context b;
    private Bitmap c = null;
    private Bitmap d = null;
    private Bitmap e = null;
    private ObservableScrollView f;
    private ad g;
    private boolean h = false;
    private int i;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k(Context context, Jigsaur.Level level, ObservableScrollView observableScrollView, ad adVar) {
        super(context);
        int i2 = 0;
        this.b = context;
        this.f = observableScrollView;
        this.g = adVar;
        this.a = level;
        Jigsaur.PuzzleImageInfo a2 = ae.a().a(level.getImageInfoUid());
        try {
            Bitmap decodeStream = BitmapFactory.decodeStream(this.b.getAssets().open(a2.getResourceName() + "/" + o.a(a2.getNumFrames() > 10 ? 9 : i2)));
            float puzzleImageHeight = a2.getPuzzleImageHeight() / a2.getPuzzleImageWidth();
            String a3 = p.a().a(this.a.getPieceSetType(), this.a.getImageInfoUid(), ".png");
            "Creating Overlay for type: " + this.a.getPieceSetType() + " => " + a3;
            Bitmap decodeStream2 = BitmapFactory.decodeStream(this.b.getAssets().open(a3));
            int i3 = (int) (puzzleImageHeight * 180.0f);
            Bitmap createBitmap = Bitmap.createBitmap(180, i3, Bitmap.Config.RGB_565);
            Canvas canvas = new Canvas(createBitmap);
            Rect rect = new Rect(0, 0, 180, i3);
            j.reset();
            j.setColorFilter(l);
            canvas.drawBitmap(decodeStream, (Rect) null, rect, j);
            canvas.drawBitmap(decodeStream2, (Rect) null, rect, (Paint) null);
            this.c = createBitmap;
        } catch (IOException e2) {
            this.c = Bitmap.createBitmap(25, 25, Bitmap.Config.RGB_565);
        }
        this.d = this.c;
        this.i = this.d.getHeight() + 30;
        setOnClickListener(this);
    }

    public final Jigsaur.Level a() {
        return this.a;
    }

    public final void onClick(View view) {
        if (this.h) {
            this.g.f = this.a;
            this.g.dismiss();
            return;
        }
        this.f.smoothScrollTo(0, view.getTop() - 105);
    }

    /* access modifiers changed from: protected */
    public final void onDraw(Canvas canvas) {
        j.reset();
        j.setStyle(Paint.Style.FILL);
        if (this.h) {
            j.setColor(-1);
        } else {
            j.setColor(-16777216);
        }
        canvas.drawRect(0.0f, 0.0f, 210.0f, (float) this.i, j);
        RectF rectF = new RectF(15.0f, 15.0f, 195.0f, (float) (this.i - 15));
        if (this.h) {
            j.reset();
            j.setStyle(Paint.Style.STROKE);
            j.setColor(Color.rgb(86, 86, 86));
            j.setStrokeWidth(4.0f);
            canvas.drawRect(0.0f, 0.0f, 210.0f, (float) this.i, j);
        }
        j.reset();
        if (!this.h) {
            j.setColorFilter(l);
        }
        if (this.d != null) {
            canvas.drawBitmap(this.d, (Rect) null, rectF, j);
        }
        if (this.e != null) {
            canvas.drawBitmap(this.e, (Rect) null, rectF, j);
        }
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i2, int i3) {
        setMeasuredDimension(210, this.i);
    }

    public final void setSelected(boolean z) {
        if (this.h != z) {
            this.h = z;
        }
    }
}
