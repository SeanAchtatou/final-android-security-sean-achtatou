package com.ptowngames.jigsaur;

import com.badlogic.gdx.f;
import com.ptowngames.jigsaur.a.d;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.SortedSet;
import java.util.TreeSet;

public class h implements f {
    private static /* synthetic */ boolean h = (!h.class.desiredAssertionStatus());
    private ap a = new a(this);
    private Hashtable<Integer, b> b = new Hashtable<>();
    private Hashtable<Integer, ap> c = new Hashtable<>();
    private Hashtable<Integer, b> d = new Hashtable<>();
    private SortedSet<ap> e = Collections.synchronizedSortedSet(new TreeSet(new Comparator<ap>() {
        private int a = 0;
        private Hashtable<ap, Integer> b = new Hashtable<>();

        public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
            ap apVar = (ap) obj;
            ap apVar2 = (ap) obj2;
            int a2 = apVar.a() - apVar2.a();
            if (a2 != 0) {
                return a2;
            }
            if (apVar == apVar2) {
                return 0;
            }
            return a(apVar) - a(apVar2);
        }

        private int a(ap apVar) {
            if (this.b.containsKey(apVar)) {
                return this.b.get(apVar).intValue();
            }
            Hashtable<ap, Integer> hashtable = this.b;
            int i = this.a;
            this.a = i + 1;
            hashtable.put(apVar, Integer.valueOf(i));
            return this.b.get(apVar).intValue();
        }
    }));
    private com.ptowngames.jigsaur.a.a f;
    private boolean g = false;

    public h() {
        a(this.a);
        this.f = new com.ptowngames.jigsaur.a.a() {
            public final void c() {
                h.this.b();
            }
        }.a(10).a(true);
        a();
    }

    public final void a() {
        d.a().a(this.f);
    }

    private class b {
        public final int a;
        public final int b;
        public final int c;
        public final long d = System.currentTimeMillis();

        public b(int i, int i2, int i3) {
            this.a = i;
            this.b = i2;
            this.c = i3;
        }
    }

    public final synchronized boolean a(int i, int i2, int i3) {
        if (!this.b.containsKey(Integer.valueOf(i3))) {
            if (h || !this.c.containsKey(Integer.valueOf(i3))) {
                this.b.put(Integer.valueOf(i3), new b(i, i2, i3));
            } else {
                throw new AssertionError();
            }
        }
        return true;
    }

    public final synchronized boolean c(int i, int i2, int i3) {
        if (this.c.containsKey(Integer.valueOf(i3))) {
            this.c.get(Integer.valueOf(i3)).d(i, i2, i3);
        } else if (h || this.b.containsKey(Integer.valueOf(i3))) {
            b bVar = this.b.get(Integer.valueOf(i3));
            if ((System.currentTimeMillis() - bVar.d >= 350 || ((i - bVar.a) * (i - bVar.a)) + ((i2 - bVar.b) * (i2 - bVar.b)) > 49) && ((i - bVar.a) * (i - bVar.a)) + ((i2 - bVar.b) * (i2 - bVar.b)) > 9) {
                a(bVar.a, bVar.b, bVar.c, new c() {
                    public final boolean a(ap apVar, int i, int i2, int i3) {
                        return apVar.b(i, i2, i3);
                    }
                });
                if (h || this.c.containsKey(Integer.valueOf(i3))) {
                    this.c.get(Integer.valueOf(i3)).d(i, i2, i3);
                } else {
                    throw new AssertionError();
                }
            }
        } else {
            throw new AssertionError();
        }
        return true;
    }

    public final synchronized boolean b(int i, int i2, int i3) {
        if (h || this.b.containsKey(Integer.valueOf(i3))) {
            b remove = this.b.remove(Integer.valueOf(i3));
            if (!h && i3 != remove.c) {
                throw new AssertionError();
            } else if (this.c.containsKey(Integer.valueOf(i3))) {
                this.c.remove(Integer.valueOf(i3)).a(i3);
            } else {
                long currentTimeMillis = System.currentTimeMillis();
                if (currentTimeMillis - remove.d < 350 && ((i - remove.a) * (i - remove.a)) + ((i2 - remove.b) * (i2 - remove.b)) <= 49) {
                    b bVar = this.d.get(Integer.valueOf(i3));
                    if (bVar != null && currentTimeMillis - bVar.d < 300) {
                        if (((i2 - bVar.b) * (i2 - bVar.b)) + ((i - bVar.a) * (i - bVar.a)) <= 1000) {
                            a(remove.a, remove.b, remove.c, 2);
                            this.d.remove(Integer.valueOf(i3));
                        }
                    }
                    a(remove.a, remove.b, remove.c, 1);
                    this.d.put(Integer.valueOf(i3), remove);
                }
            }
        } else {
            throw new AssertionError();
        }
        return true;
    }

    private class a implements ap {
        /* synthetic */ a(h hVar) {
            this((byte) 0);
        }

        private a(byte b) {
        }

        public final void a(int i, int i2) {
        }

        public final int a() {
            return Integer.MAX_VALUE;
        }

        public final boolean a(int i, int i2, int i3) {
            return true;
        }

        public final boolean b(int i, int i2, int i3) {
            return true;
        }

        public final boolean c(int i, int i2, int i3) {
            return true;
        }

        public final void d(int i, int i2, int i3) {
        }

        public final void a(int i) {
        }
    }

    public final void a(ap apVar) {
        this.e.add(apVar);
    }

    public final void b(ap apVar) {
        this.e.remove(apVar);
    }

    private abstract class c {
        public abstract boolean a(ap apVar, int i, int i2, int i3);

        /* synthetic */ c(h hVar) {
            this((byte) 0);
        }

        private c(byte b) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r3.c.put(java.lang.Integer.valueOf(r6), r3.a);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void a(int r4, int r5, int r6, com.ptowngames.jigsaur.h.c r7) {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r0 = com.ptowngames.jigsaur.h.h     // Catch:{ all -> 0x0017 }
            if (r0 != 0) goto L_0x001a
            java.util.Hashtable<java.lang.Integer, com.ptowngames.jigsaur.ap> r0 = r3.c     // Catch:{ all -> 0x0017 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x0017 }
            boolean r0 = r0.containsKey(r1)     // Catch:{ all -> 0x0017 }
            if (r0 == 0) goto L_0x001a
            java.lang.AssertionError r0 = new java.lang.AssertionError     // Catch:{ all -> 0x0017 }
            r0.<init>()     // Catch:{ all -> 0x0017 }
            throw r0     // Catch:{ all -> 0x0017 }
        L_0x0017:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        L_0x001a:
            java.util.SortedSet<com.ptowngames.jigsaur.ap> r0 = r3.e     // Catch:{ all -> 0x0017 }
            java.util.Iterator r1 = r0.iterator()     // Catch:{ all -> 0x0017 }
        L_0x0020:
            boolean r0 = r1.hasNext()     // Catch:{ all -> 0x0017 }
            if (r0 == 0) goto L_0x003d
            java.lang.Object r0 = r1.next()     // Catch:{ all -> 0x0017 }
            com.ptowngames.jigsaur.ap r0 = (com.ptowngames.jigsaur.ap) r0     // Catch:{ all -> 0x0017 }
            boolean r2 = r7.a(r0, r4, r5, r6)     // Catch:{ all -> 0x0017 }
            if (r2 == 0) goto L_0x0020
            java.util.Hashtable<java.lang.Integer, com.ptowngames.jigsaur.ap> r1 = r3.c     // Catch:{ all -> 0x0017 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x0017 }
            r1.put(r2, r0)     // Catch:{ all -> 0x0017 }
        L_0x003b:
            monitor-exit(r3)
            return
        L_0x003d:
            java.util.Hashtable<java.lang.Integer, com.ptowngames.jigsaur.ap> r0 = r3.c     // Catch:{ all -> 0x0017 }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x0017 }
            com.ptowngames.jigsaur.ap r2 = r3.a     // Catch:{ all -> 0x0017 }
            r0.put(r1, r2)     // Catch:{ all -> 0x0017 }
            goto L_0x003b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ptowngames.jigsaur.h.a(int, int, int, com.ptowngames.jigsaur.h$c):void");
    }

    private void a(int i, int i2, int i3, final int i4) {
        a(i, i2, i3, new c() {
            public final boolean a(ap apVar, int i, int i2, int i3) {
                return apVar.a(i, i2, i3);
            }
        });
        this.c.remove(Integer.valueOf(i3));
    }

    public final synchronized void b() {
        for (Integer intValue : this.b.keySet()) {
            int intValue2 = intValue.intValue();
            if (!this.c.containsKey(Integer.valueOf(intValue2))) {
                b bVar = this.b.get(Integer.valueOf(intValue2));
                if (!h && bVar.c != intValue2) {
                    throw new AssertionError();
                } else if (System.currentTimeMillis() - bVar.d > 500) {
                    a(bVar.a, bVar.b, bVar.c, new c() {
                        public final boolean a(ap apVar, int i, int i2, int i3) {
                            return apVar.c(i, i2, i3);
                        }
                    });
                }
            }
        }
    }
}
