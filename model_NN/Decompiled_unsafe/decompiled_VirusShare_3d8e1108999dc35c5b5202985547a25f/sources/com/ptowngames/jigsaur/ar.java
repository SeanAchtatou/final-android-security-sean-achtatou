package com.ptowngames.jigsaur;

import com.ptowngames.jigsaur.Jigsaur;
import java.security.SecureRandom;

public final class ar {
    private static ar k;
    boolean a;
    boolean b;
    boolean c;
    boolean d;
    boolean e;
    boolean f;
    long g;
    boolean h = false;
    String i = "";
    long j;
    private long l = 0;

    public static ar a() {
        if (k == null) {
            k = new ar();
        }
        return k;
    }

    public final long b() {
        return this.g;
    }

    protected ar() {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        long j2;
        long j3 = 0;
        boolean z5 = false;
        boolean z6 = true;
        Jigsaur.Settings settings = null;
        try {
            settings = Jigsaur.Settings.parseFrom(ab.a().b("settings"));
        } catch (Exception e2) {
        }
        settings = settings == null ? Jigsaur.Settings.newBuilder().build() : settings;
        this.a = settings.hasDoAutoJoining() ? settings.getDoAutoJoining() : true;
        if (settings.hasBlinkOnWin()) {
            z = settings.getBlinkOnWin();
        } else {
            z = true;
        }
        this.b = z;
        if (settings.hasShowSolution()) {
            z2 = settings.getShowSolution();
        } else {
            z2 = true;
        }
        this.c = z2;
        if (settings.hasEnableAccelerometer()) {
            z3 = settings.getEnableAccelerometer();
        } else {
            z3 = false;
        }
        this.d = z3;
        if (settings.hasDisplayZoomSlider()) {
            z4 = settings.getDisplayZoomSlider();
        } else {
            z4 = true;
        }
        this.e = z4;
        this.i = settings.hasLastLevelPlayed() ? settings.getLastLevelPlayed() : "";
        if (settings.hasTimestampFirstPlayed()) {
            j2 = settings.getTimestampFirstPlayed();
        } else {
            j2 = 0;
        }
        this.l = j2;
        this.j = settings.hasStartupCount() ? settings.getStartupCount() : j3;
        if (settings.hasUserId()) {
            this.g = settings.getUserId();
        } else {
            this.g = new SecureRandom().nextLong();
            this.h = true;
            this.l = System.currentTimeMillis();
            z5 = true;
        }
        this.f = settings.hasSoundEnabled() ? settings.getSoundEnabled() : z6;
        if (z5) {
            c();
        }
    }

    public final void c() {
        try {
            Jigsaur.Settings.newBuilder().setDoAutoJoining(this.a).setShowSolution(this.c).setEnableAccelerometer(this.d).setDisplayZoomSlider(this.e).setUserId(this.g).setLastLevelPlayed(this.i).setTimestampFirstPlayed(this.l).setStartupCount(this.j).setSoundEnabled(this.f).build().writeTo(ab.a().a("settings"));
        } catch (Exception e2) {
            "Error writing settings: " + e2;
        }
    }
}
