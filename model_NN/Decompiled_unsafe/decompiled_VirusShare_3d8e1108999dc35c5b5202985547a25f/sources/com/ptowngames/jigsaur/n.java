package com.ptowngames.jigsaur;

import com.ptowngames.jigsaur.a;
import com.ptowngames.jigsaur.a.c;

public class n extends c {
    private static /* synthetic */ boolean f = (!n.class.desiredAssertionStatus());
    private float b;
    private float c;
    private float d;
    private a.C0018a e;

    public n(a.C0018a aVar) {
        super(800, 5.0f);
        this.e = aVar;
        if (f || this.e != null) {
            this.b = -0.3f;
            this.c = 0.3f;
            this.d = this.c - this.b;
            a(true);
            a(33L);
            return;
        }
        throw new AssertionError();
    }

    public final void a(float f2) {
        float abs;
        if (f || f2 >= 0.0f) {
            float f3 = f2 - ((float) ((int) f2));
            if (f3 <= 0.5f) {
                abs = 1.0f - (((float) Math.abs(0.25d - ((double) f3))) * 2.0f);
            } else {
                abs = ((float) Math.abs(0.75d - ((double) f3))) * 2.0f;
            }
            this.e.c((abs * this.d) + this.b);
            return;
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: protected */
    public final void b() {
        super.b();
        this.e.c(0.0f);
    }
}
