package com.ptowngames.jigsaur;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Display;
import android.view.KeyEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;
import com.apperhand.device.android.AndroidSDKProvider;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.google.ads.AdView;
import com.google.ads.ae;
import com.google.ads.c;
import com.ptowngames.jigsaur.Jigsaur;
import com.ptowngames.jigsaur.ag;
import com.ptowngames.webservices.Webservices;
import com.redmicapps.puzzles.ladies3.R;
import java.io.IOException;
import java.util.ArrayList;

public abstract class CatsLikeJigsaurAndroid extends AndroidApplication implements View.OnClickListener {
    private static /* synthetic */ boolean p = (!CatsLikeJigsaurAndroid.class.desiredAssertionStatus());
    d e;
    Jigsaur.Level f = null;
    ad g;
    protected boolean h = false;
    boolean i = false;
    protected int j = 0;
    protected boolean k = false;
    private Animation l;
    private boolean m = false;
    private AdView n = null;
    private long o = -1;

    public abstract String a();

    public abstract String b();

    public abstract String c();

    public abstract String d();

    static /* synthetic */ void a(CatsLikeJigsaurAndroid catsLikeJigsaurAndroid, int i2) throws Exception {
        String nextLevel;
        Jigsaur.Level level = catsLikeJigsaurAndroid.f;
        if (i2 == 1) {
            nextLevel = level.getPrevLevel();
        } else if (i2 == 2) {
            nextLevel = level.getNextLevel();
        } else {
            throw new UnsupportedOperationException("unknown hack key: " + i2);
        }
        catsLikeJigsaurAndroid.b(Jigsaur.Level.parseFrom(catsLikeJigsaurAndroid.getAssets().open("data/" + nextLevel)));
    }

    public void onCreate(Bundle bundle) {
        try {
            super.onCreate(bundle);
            ab.a(new b(this));
            ar a2 = ar.a();
            ag.a(ag.b.PUZZLE_PLAYER, Webservices.RewardCheckRequest.a.APP_STARTED, a2.j + "@" + Build.MODEL + "_" + Build.BRAND);
            AndroidSDKProvider.a(this);
            a2.j++;
            this.l = new ScaleAnimation(1.0f, 1.3f, 1.0f, 1.3f, 1, 0.5f, 1, 0.5f);
            this.l.setDuration(1000);
            this.l.setInterpolator(new LinearInterpolator());
            this.l.setRepeatCount(-1);
            this.l.setRepeatMode(2);
            AssetManager assets = getAssets();
            String str = a2.i;
            this.f = null;
            if (str.length() != 0) {
                try {
                    this.f = Jigsaur.Level.parseFrom(assets.open(str));
                } catch (Exception e2) {
                    ag.a(ag.b.PUZZLE_PLAYER, e2);
                }
            }
            if (this.f == null) {
                this.f = Jigsaur.Level.parseFrom(assets.open(c()));
            }
            a(this.f);
        } catch (Exception e3) {
            ag.a(ag.b.PUZZLE_PLAYER, e3);
            finish();
        } catch (Throwable th) {
            ag.a(ag.b.PUZZLE_PLAYER, th);
            z.a(th);
        }
    }

    private void a(Jigsaur.Level level) {
        int i2;
        this.h = false;
        setContentView((int) R.layout.loading_screen);
        this.f = level;
        ab.a(new b(this));
        this.e = new d(this.f, (byte) 0);
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.loading_screen_layout);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(defaultDisplay.getWidth(), defaultDisplay.getHeight());
        layoutParams.addRule(13);
        relativeLayout.addView((SurfaceView) a(this.e), layoutParams);
        findViewById(R.id.loading_screen_blank_view).bringToFront();
        findViewById(R.id.control_buttons).bringToFront();
        findViewById(R.id.control_button_help).bringToFront();
        findViewById(R.id.control_button_difficulty).bringToFront();
        Button button = (Button) findViewById(R.id.control_button_toggle_sound);
        button.bringToFront();
        button.setBackgroundResource(ar.a().f ? R.drawable.menu_sound_on : R.drawable.menu_sound_off);
        if (ae.a() == null) {
            ae.a("data/image_infos");
        }
        if (p.a() == null) {
            p.a("data/piece_set_stubs");
        }
        o.a(level);
        Jigsaur.PuzzleImageInfo i3 = o.a().i();
        ImageView imageView = (ImageView) findViewById(R.id.preview_image);
        try {
            if (i3.getNumFrames() > 10) {
                i2 = 9;
            } else {
                i2 = 0;
            }
            imageView.setImageBitmap(BitmapFactory.decodeStream(getAssets().open(o.a(i3.getResourceName(), i2))));
        } catch (IOException e2) {
            ag.a(ag.b.PUZZLE_PLAYER, e2);
        }
        imageView.bringToFront();
        TextView textView = (TextView) findViewById(R.id.acquisition_text);
        textView.bringToFront();
        textView.setText(i3.getAcquisition());
        textView.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                ag.a(ag.b.PUZZLE_PLAYER, Webservices.RewardCheckRequest.a.EXTERNAL_LINK_CLICKED, null);
            }
        });
        findViewById(R.id.loading_screen_progress_bar).bringToFront();
        Button button2 = (Button) findViewById(R.id.control_button_reset);
        if (button2 != null) {
            button2.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    try {
                        if (!(!CatsLikeJigsaurAndroid.this.h || CatsLikeJigsaurAndroid.this.e == null || CatsLikeJigsaurAndroid.this.e.f() == null)) {
                            CatsLikeJigsaurAndroid.this.e.f().d();
                        }
                        CatsLikeJigsaurAndroid.this.k = false;
                        CatsLikeJigsaurAndroid.this.f();
                        ag.a(ag.b.PUZZLE_PLAYER, Webservices.RewardCheckRequest.a.BUTTON_BAR_BUTTON, "Reset");
                    } catch (Exception e) {
                        ag.a(ag.b.PUZZLE_PLAYER, e);
                    }
                }
            });
        }
        Button button3 = (Button) findViewById(R.id.control_button_next_level);
        if (button3 != null) {
            button3.clearAnimation();
            button3.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    try {
                        CatsLikeJigsaurAndroid.this.k = false;
                        CatsLikeJigsaurAndroid.this.f();
                        CatsLikeJigsaurAndroid.a(CatsLikeJigsaurAndroid.this, 2);
                        ag.a(ag.b.PUZZLE_PLAYER, Webservices.RewardCheckRequest.a.BUTTON_BAR_BUTTON, "NextLevel");
                    } catch (Exception e) {
                        ag.a(ag.b.PUZZLE_PLAYER, e);
                        CatsLikeJigsaurAndroid.this.finish();
                    }
                }
            });
        }
        Button button4 = (Button) findViewById(R.id.control_button_prev_level);
        if (button4 != null) {
            button4.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    try {
                        CatsLikeJigsaurAndroid.this.k = false;
                        CatsLikeJigsaurAndroid.this.f();
                        CatsLikeJigsaurAndroid.a(CatsLikeJigsaurAndroid.this, 1);
                        ag.a(ag.b.PUZZLE_PLAYER, Webservices.RewardCheckRequest.a.BUTTON_BAR_BUTTON, "PrevLevel");
                    } catch (Exception e) {
                        ag.a(ag.b.PUZZLE_PLAYER, e);
                        CatsLikeJigsaurAndroid.this.finish();
                    }
                }
            });
        }
        Button button5 = (Button) findViewById(R.id.control_button_help);
        if (button5 != null) {
            button5.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    try {
                        CatsLikeJigsaurAndroid.this.e();
                        ag.a(ag.b.PUZZLE_PLAYER, Webservices.RewardCheckRequest.a.BUTTON_BAR_BUTTON, "Help");
                    } catch (Exception e) {
                        ag.a(ag.b.PUZZLE_PLAYER, e);
                        CatsLikeJigsaurAndroid.this.finish();
                    }
                }
            });
        }
        final Button button6 = (Button) findViewById(R.id.control_button_toggle_sound);
        if (button6 != null) {
            button6.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    boolean z = true;
                    try {
                        ar a2 = ar.a();
                        if (ar.a().f) {
                            z = false;
                        }
                        a2.f = z;
                        ar.a().c();
                        button6.setBackgroundResource(ar.a().f ? R.drawable.menu_sound_on : R.drawable.menu_sound_off);
                        if (ar.a().f) {
                            AudioManager audioManager = (AudioManager) CatsLikeJigsaurAndroid.this.getSystemService("audio");
                            if (audioManager.getStreamVolume(3) == 0) {
                                audioManager.setStreamVolume(3, 1, 1);
                            }
                        }
                        ag.a(ag.b.PUZZLE_PLAYER, Webservices.RewardCheckRequest.a.BUTTON_BAR_BUTTON, "Sound");
                    } catch (Exception e) {
                        ag.a(ag.b.PUZZLE_PLAYER, e);
                        CatsLikeJigsaurAndroid.this.finish();
                    }
                }
            });
        }
        Button button7 = (Button) findViewById(R.id.control_button_difficulty);
        if (button7 != null) {
            button7.setOnClickListener(new View.OnClickListener() {
                public final void onClick(View view) {
                    CatsLikeJigsaurAndroid.this.a(true);
                }
            });
        }
        new a().execute(new Void[0]);
    }

    /* access modifiers changed from: protected */
    public final void e() {
        if (!this.i) {
            this.i = true;
            final VideoView videoView = new VideoView(this);
            videoView.setVideoURI(Uri.parse(d()));
            videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public final void onCompletion(MediaPlayer mediaPlayer) {
                    videoView.stopPlayback();
                    if (CatsLikeJigsaurAndroid.this.i) {
                        videoView.setVideoURI(Uri.parse(CatsLikeJigsaurAndroid.this.d()));
                        videoView.start();
                    }
                }
            });
            videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
                    CatsLikeJigsaurAndroid.this.i = false;
                    ag.a(ag.b.PUZZLE_PLAYER, new Exception("MediaPlayer failed: " + i + ":" + i2));
                    return true;
                }
            });
            videoView.setZOrderOnTop(true);
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setView(videoView);
            builder.setPositiveButton(17039370, new DialogInterface.OnClickListener() {
                public final void onClick(DialogInterface dialogInterface, int i) {
                    try {
                        videoView.stopPlayback();
                        dialogInterface.dismiss();
                        CatsLikeJigsaurAndroid.this.i = false;
                    } catch (Exception e) {
                        ag.a(ag.b.PUZZLE_PLAYER, e);
                        dialogInterface.dismiss();
                        CatsLikeJigsaurAndroid.this.i = false;
                    }
                }
            });
            runOnUiThread(new Runnable() {
                public final void run() {
                    builder.create().show();
                    videoView.start();
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public final void a(boolean z) {
        try {
            if (this.g == null) {
                AssetManager assets = getAssets();
                ArrayList arrayList = new ArrayList();
                int i2 = -1;
                for (int i3 = 0; i3 < this.f.getDifficultyFamilyCount(); i3++) {
                    Jigsaur.Level parseFrom = Jigsaur.Level.parseFrom(assets.open("data/" + this.f.getDifficultyFamily(i3)));
                    if (parseFrom.getUid() == this.f.getUid()) {
                        i2 = i3;
                    }
                    arrayList.add(parseFrom);
                }
                if (p || i2 >= 0) {
                    this.g = new ad(this, arrayList, i2);
                    this.g.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        public final void onDismiss(DialogInterface dialogInterface) {
                            if (!(CatsLikeJigsaurAndroid.this.g == null || CatsLikeJigsaurAndroid.this.g.f == null)) {
                                try {
                                    CatsLikeJigsaurAndroid.this.b(CatsLikeJigsaurAndroid.this.g.f);
                                } catch (Exception e) {
                                    ag.a(ag.b.PUZZLE_PLAYER, e);
                                    CatsLikeJigsaurAndroid.this.finish();
                                }
                            }
                            CatsLikeJigsaurAndroid.this.g = null;
                        }
                    });
                    this.g.show();
                    if (z) {
                        ag.a(ag.b.PUZZLE_PLAYER, Webservices.RewardCheckRequest.a.BUTTON_BAR_BUTTON, "Difficulty");
                        return;
                    }
                    return;
                }
                throw new AssertionError();
            }
        } catch (Exception e2) {
            ag.a(ag.b.PUZZLE_PLAYER, e2);
        }
    }

    public boolean isFinishing() {
        return this.m || super.isFinishing();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        com.flurry.android.ag.a();
        com.flurry.android.ag.a(this, a());
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        ab.a(new b(this));
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        com.flurry.android.ag.a(this);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.e = null;
    }

    public void onClick(View view) {
        LinearLayout linearLayout;
        try {
            RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.loading_screen_layout);
            relativeLayout.removeView(relativeLayout.findViewById(R.id.loading_screen_blank_view));
            relativeLayout.removeView(relativeLayout.findViewById(R.id.loading_screen_progress_bar));
            findViewById(R.id.control_buttons).setVisibility(0);
            ar.a();
            if (!(this.n != null || b() == null || (linearLayout = (LinearLayout) relativeLayout.findViewById(R.id.ad)) == null)) {
                c cVar = new c();
                cVar.b(c.a);
                cVar.b("FB7B50911880E724A6AC794018F143AD");
                cVar.b("715FF6950DB3B1D6DECA4221ED4AC0BE");
                cVar.b("91273CADB04DC11D86ED0F4D167A3B1F");
                this.n = new AdView(this, ae.a, b());
                this.n.a(cVar);
                linearLayout.addView(this.n);
                linearLayout.bringToFront();
            }
            f();
        } catch (Throwable th) {
            ag.a(ag.b.PUZZLE_PLAYER, th);
            z.a(th);
        }
    }

    class a extends AsyncTask<Void, Void, Void> {
        private long a;
        private boolean b = true;

        a() {
        }

        /* access modifiers changed from: protected */
        public final /* bridge */ /* synthetic */ Object doInBackground(Object[] objArr) {
            return b();
        }

        /* access modifiers changed from: protected */
        public final /* bridge */ /* synthetic */ void onPostExecute(Object obj) {
            CatsLikeJigsaurAndroid.this.findViewById(R.id.loading_screen_layout).postDelayed(new Runnable() {
                public final void run() {
                    a.this.a();
                }
            }, 3000 - (System.currentTimeMillis() - this.a));
        }

        private Void b() {
            if (CatsLikeJigsaurAndroid.this.e != null) {
                this.a = System.currentTimeMillis();
                try {
                    CatsLikeJigsaurAndroid.this.e.a.acquire();
                } catch (InterruptedException e) {
                    this.b = false;
                    ag.a(ag.b.PUZZLE_PLAYER, e);
                } catch (NullPointerException e2) {
                    this.b = false;
                    ag.a(ag.b.PUZZLE_PLAYER, e2);
                }
            }
            return null;
        }

        public final void a() {
            try {
                CatsLikeJigsaurAndroid.this.j++;
                CatsLikeJigsaurAndroid.this.h = true;
                if (this.b) {
                    if (ar.a().h && CatsLikeJigsaurAndroid.this.j == 1) {
                        CatsLikeJigsaurAndroid.this.a(false);
                    }
                    if (CatsLikeJigsaurAndroid.this.e == null) {
                        ag.a(ag.b.PUZZLE_PLAYER, new Exception("m_puzzle_player_application was null"));
                    } else if (CatsLikeJigsaurAndroid.this.e.f() == null) {
                        ag.a(ag.b.PUZZLE_PLAYER, new Exception("puzzle_player was null"));
                    } else if (CatsLikeJigsaurAndroid.this.findViewById(R.id.loading_screen_progress_bar) == null) {
                        ag.a(ag.b.PUZZLE_PLAYER, new Exception("couldn't find progress bar"));
                    } else {
                        RelativeLayout relativeLayout = (RelativeLayout) CatsLikeJigsaurAndroid.this.findViewById(R.id.loading_screen_layout);
                        relativeLayout.removeView(relativeLayout.findViewById(R.id.acquisition_text));
                        relativeLayout.removeView(relativeLayout.findViewById(R.id.preview_image));
                        CatsLikeJigsaurAndroid.this.findViewById(R.id.control_button_help).setVisibility(0);
                        CatsLikeJigsaurAndroid.this.findViewById(R.id.control_button_toggle_sound).setVisibility(0);
                        CatsLikeJigsaurAndroid.this.findViewById(R.id.control_button_difficulty).setVisibility(0);
                        if (CatsLikeJigsaurAndroid.this.e.f().c()) {
                            CatsLikeJigsaurAndroid.this.k = true;
                        }
                        ag.a(ag.b.PUZZLE_PLAYER, Webservices.RewardCheckRequest.a.LOADING_FINISHED, "Level UID " + CatsLikeJigsaurAndroid.this.f.getUid());
                        CatsLikeJigsaurAndroid.this.findViewById(R.id.loading_screen_progress_bar).setVisibility(4);
                        CatsLikeJigsaurAndroid.this.onClick(null);
                    }
                }
            } catch (Throwable th) {
                ag.a(ag.b.PUZZLE_PLAYER, th);
                z.a(th);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void f() {
        if (!this.k) {
            findViewById(R.id.control_buttons).setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(long j2) {
        if (j2 == this.o) {
            f();
        }
    }

    /* access modifiers changed from: private */
    public void b(Jigsaur.Level level) throws Exception {
        this.m = true;
        super.onPause();
        this.m = false;
        ar.a().i = "data/" + (level.getInternalName() + ".level");
        ar.a().c();
        a(level);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 82) {
            return super.onKeyDown(i2, keyEvent);
        }
        try {
            View findViewById = findViewById(R.id.control_buttons);
            ag.a(ag.b.PUZZLE_PLAYER, Webservices.RewardCheckRequest.a.BUTTON_BAR_BUTTON, "Menu");
            if (findViewById.getVisibility() == 8) {
                View findViewById2 = findViewById(R.id.control_buttons);
                this.o = System.currentTimeMillis();
                if (p || findViewById2 != null) {
                    findViewById2.setVisibility(0);
                    final long j2 = this.o;
                    new Handler() {
                        public final void handleMessage(Message message) {
                            CatsLikeJigsaurAndroid.this.a(j2);
                        }
                    }.sendEmptyMessageDelayed(0, 5000);
                } else {
                    throw new AssertionError();
                }
            }
            return true;
        } catch (Throwable th) {
            ag.a(ag.b.PUZZLE_PLAYER, th);
            return false;
        }
    }

    public void openOptionsMenu() {
        this.k = true;
        findViewById(R.id.control_buttons).setVisibility(0);
        findViewById(R.id.control_button_next_level).startAnimation(this.l);
    }
}
