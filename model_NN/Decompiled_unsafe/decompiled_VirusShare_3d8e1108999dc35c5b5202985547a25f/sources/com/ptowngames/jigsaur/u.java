package com.ptowngames.jigsaur;

import com.badlogic.gdx.math.a.a;
import com.badlogic.gdx.math.f;
import com.badlogic.gdx.math.g;

public final class u {
    private static /* synthetic */ boolean a = (!u.class.desiredAssertionStatus());

    public static g a(a aVar, float f) {
        g gVar = new g();
        a(aVar, f, gVar);
        return gVar;
    }

    public static void a(a aVar, float f, g gVar) {
        if (a || ((aVar.a.c <= f && aVar.b.c > 0.0f) || (aVar.a.c >= f && aVar.b.c < 0.0f))) {
            float f2 = (f - aVar.a.c) / aVar.b.c;
            if (a || f2 > 0.0f) {
                gVar.a = aVar.a.a + (aVar.b.a * f2);
                gVar.b = (f2 * aVar.b.b) + aVar.a.b;
                return;
            }
            throw new AssertionError();
        }
        throw new AssertionError("r: " + aVar + ", z_plane: " + f);
    }

    public static void a(a aVar, float f, f fVar) {
        if (a || ((aVar.a.c <= f && aVar.b.c > 0.0f) || (aVar.a.c >= f && aVar.b.c < 0.0f))) {
            float f2 = (f - aVar.a.c) / aVar.b.c;
            if (a || f2 > 0.0f) {
                fVar.a = aVar.a.a + (aVar.b.a * f2);
                fVar.b = (f2 * aVar.b.b) + aVar.a.b;
                fVar.c = f;
                return;
            }
            throw new AssertionError();
        }
        throw new AssertionError("r: " + aVar + ", z_plane: " + f);
    }

    public static float a(float f) {
        return f - (((float) Math.floor((double) ((f / 6.2831855f) + 0.5f))) * 6.2831855f);
    }

    public static g a(g gVar, g gVar2, float f) {
        float cos = (float) Math.cos((double) f);
        float sin = (float) Math.sin((double) f);
        gVar.b(gVar2);
        gVar.a((gVar.a * cos) - (gVar.b * sin), (cos * gVar.b) + (sin * gVar.a));
        gVar.c(gVar2);
        return gVar;
    }

    public static f a(f fVar, f fVar2, float f) {
        float cos = (float) Math.cos((double) f);
        float sin = (float) Math.sin((double) f);
        fVar.a -= fVar2.a;
        fVar.b -= fVar2.b;
        float f2 = (fVar.a * cos) - (fVar.b * sin);
        float f3 = (cos * fVar.b) + (sin * fVar.a);
        fVar.a = fVar2.a + f2;
        fVar.b = f3 + fVar2.b;
        return fVar;
    }
}
