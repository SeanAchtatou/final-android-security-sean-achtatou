package com.ptowngames.jigsaur;

import java.util.ArrayList;

public final class c {
    private static c a = null;
    private static /* synthetic */ boolean b;

    public interface a {
        void a(Object obj, b bVar, Object obj2);
    }

    static {
        boolean z;
        if (!c.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        b = z;
    }

    public enum b {
        INVALID_CHANNEL(0),
        NO_GLOBS_VISIBLE(1),
        GLOB_WENT_DEFUNCT(2),
        GLOB_WAS_CREATED(3),
        PUZZLE_SOLVED(4),
        GLOB_JOIN_FAILURE(5),
        GLOB_JOIN_SUCCESS(6),
        DISPLAY_ZOOM_SLIDER_CHANGED(7),
        NUM_CHANNELS(8);
        
        private ArrayList<a> j = new ArrayList<>();
        private int k;

        private b(int i) {
            this.k = i;
        }

        /* access modifiers changed from: protected */
        public final void a() {
            this.j.clear();
        }
    }

    public static void a(a aVar, b bVar) {
        if (!b && bVar == null) {
            throw new AssertionError();
        } else if (b || aVar != null) {
            b.a(bVar).add(aVar);
        } else {
            throw new AssertionError();
        }
    }

    public static boolean b(a aVar, b bVar) {
        if (b || bVar != null) {
            return b.a(bVar).remove(aVar);
        }
        throw new AssertionError();
    }

    public static void a(Object obj, b bVar, Object obj2) {
        int size = b.a(bVar).size();
        for (int i = 0; i < size; i++) {
            ((a) b.a(bVar).get(i)).a(obj, bVar, obj2);
        }
    }

    public static void a() {
        a = new c();
        for (b a2 : b.values()) {
            a2.a();
        }
    }
}
