package com.ptowngames.jigsaur;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.LinearLayout;
import com.ptowngames.ObservableScrollView;
import com.ptowngames.a;
import com.ptowngames.jigsaur.Jigsaur;
import com.redmicapps.puzzles.ladies3.R;
import java.util.ArrayList;

public final class ad extends Dialog implements a {
    k a = null;
    int b = -1;
    k[] c = null;
    int d = -1;
    int e = -1;
    public Jigsaur.Level f;
    private ViewTreeObserver.OnGlobalLayoutListener g = null;
    private ArrayList<Jigsaur.Level> h = null;
    private ViewGroup.LayoutParams i = null;
    private Context j;

    public ad(Context context, ArrayList<Jigsaur.Level> arrayList, int i2) {
        super(context);
        this.j = context;
        this.h = arrayList;
        this.d = i2;
        this.e = i2;
        setTitle((int) R.string.difficulty_chooser_title);
    }

    /* access modifiers changed from: protected */
    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.difficulty_chooser);
        Button button = (Button) findViewById(R.id.difficulty_ok_button);
        button.setText(17039370);
        button.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                if (ad.this.b != ad.this.e) {
                    ad.this.f = ad.this.a.a();
                }
                ad.this.dismiss();
            }
        });
        Button button2 = (Button) findViewById(R.id.difficulty_cancel_button);
        button2.setText(17039360);
        button2.setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                ad.this.dismiss();
            }
        });
        ArrayList<Jigsaur.Level> arrayList = this.h;
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.scroll_view_layout);
        linearLayout.removeAllViews();
        final ObservableScrollView observableScrollView = (ObservableScrollView) findViewById(R.id.level_scroll_view);
        observableScrollView.a(this);
        this.c = new k[arrayList.size()];
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            Jigsaur.Level level = arrayList.get(i2);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
            layoutParams.topMargin = 20;
            layoutParams.bottomMargin = 0;
            if (i2 == arrayList.size() - 1) {
                layoutParams.bottomMargin = (int) (200.0f * this.j.getResources().getDisplayMetrics().density);
            }
            k kVar = new k(this.j, level, observableScrollView, this);
            linearLayout.addView(kVar, layoutParams);
            this.c[i2] = kVar;
        }
        if (this.c.length > 0) {
            this.b = 0;
            this.a = this.c[0];
            this.a.setSelected(true);
        }
        findViewById(R.id.difficulty_chooser_layout).requestLayout();
        this.g = new ViewTreeObserver.OnGlobalLayoutListener() {
            public final void onGlobalLayout() {
                if (ad.this.d <= 0) {
                    observableScrollView.scrollTo(0, 210);
                    observableScrollView.smoothScrollTo(0, 0);
                } else if (ad.this.c != null) {
                    observableScrollView.scrollTo(0, ad.this.c[ad.this.d].getTop() - 105);
                }
            }
        };
        observableScrollView.getViewTreeObserver().addOnGlobalLayoutListener(this.g);
    }

    public final void a(int i2) {
        if (this.c != null) {
            int length = this.c.length;
            int i3 = 0;
            while (i3 < length) {
                k kVar = this.c[i3];
                int top = kVar.getTop() - (i2 + 105);
                if (top <= -105 || top >= 105) {
                    i3++;
                } else if (this.a != kVar) {
                    if (this.a != null) {
                        this.a.setSelected(false);
                    }
                    this.b = i3;
                    this.a = kVar;
                    this.a.setSelected(true);
                    return;
                } else {
                    return;
                }
            }
        }
    }
}
