package com.ptowngames.jigsaur;

import com.badlogic.gdx.utils.f;
import com.ptowngames.jigsaur.Jigsaur;
import com.ptowngames.jigsaur.b.a;
import java.io.IOException;
import java.io.InputStream;

public class o {
    private static o a = null;
    private static char[] j = {'/', 'f', 'r', 'a', 'm', 'e', '0', '0', '0', '0', '0', '.', 'j', 'p', 'g'};
    private static StringBuffer k = new StringBuffer(100);
    private static /* synthetic */ boolean l = (!o.class.desiredAssertionStatus());
    private Jigsaur.Level b;
    private Jigsaur.PuzzleImageInfo c;
    private int d;
    private String e;
    private float f;
    private int g;
    private long h;
    private a i = null;

    public static o a() {
        return a;
    }

    private o(Jigsaur.Level level) {
        this.b = level;
        if (l || ae.a() != null) {
            this.c = ae.a().a(level.getImageInfoUid());
            if (l || this.c != null) {
                this.d = level.getUid();
                this.e = this.d + "_" + level.getInternalName();
                this.f = this.c.getFps();
                if (l || this.f > 0.0f) {
                    this.g = this.c.getNumFrames();
                    if (l || this.g > 0) {
                        this.h = System.currentTimeMillis();
                        return;
                    }
                    throw new AssertionError();
                }
                throw new AssertionError();
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    public static boolean b() {
        if (l || a != null) {
            try {
                Jigsaur.PieceSet a2 = p.a().a(a.b.getPieceSetType(), a.b.getImageInfoUid());
                if (a2 == null) {
                    throw new NullPointerException();
                }
                a.i = new a(a2, a.c.getPuzzleImageWidth(), a.c.getPuzzleImageHeight());
                return true;
            } catch (Exception e2) {
                "Failed to load PieceSet for " + a.e + ": " + e2;
                return false;
            }
        } else {
            throw new AssertionError("call Puzzle.load() before Puzzle.loadPieceset()");
        }
    }

    public final float c() {
        if (l || this.c.hasPuzzleImageWidth()) {
            return this.c.getPuzzleImageWidth();
        }
        throw new AssertionError();
    }

    public final float d() {
        if (l || this.c.hasPuzzleImageHeight()) {
            return this.c.getPuzzleImageHeight();
        }
        throw new AssertionError();
    }

    public final a e() {
        if (l || this.i != null) {
            return this.i;
        }
        throw new AssertionError("You forgot to call loadPieceset()");
    }

    public final int f() {
        return e().a();
    }

    public final int g() {
        return this.d;
    }

    public final Jigsaur.Level h() {
        return this.b;
    }

    public final Jigsaur.PuzzleImageInfo i() {
        return this.c;
    }

    public final Jigsaur.SavedConfiguration j() {
        try {
            InputStream b2 = ab.a().b(this.e);
            if (b2 != null) {
                Jigsaur.SavedConfiguration parseFrom = Jigsaur.SavedConfiguration.parseFrom(b2);
                if (parseFrom.getNumPieces() == e().a()) {
                    return parseFrom;
                }
            }
        } catch (IOException e2) {
            "Error reading configuration file: " + e2;
        }
        if (this.b.hasStartingConfiguration()) {
            return this.b.getStartingConfiguration();
        }
        return null;
    }

    public final boolean a(Jigsaur.SavedConfiguration savedConfiguration) {
        boolean z = false;
        try {
            savedConfiguration.writeTo(ab.a().a(this.e));
            z = true;
        } catch (IOException e2) {
            "Error writing to file: " + e2;
        } catch (f e3) {
            "Error writing to file: " + e3;
        }
        "Writing configuration to " + this.e + " success: " + z;
        return z;
    }

    public final int k() {
        long currentTimeMillis = System.currentTimeMillis() - this.h;
        if (l || currentTimeMillis < 2147483647L) {
            int i2 = (int) ((((float) currentTimeMillis) * this.f) / 1000.0f);
            if (l || i2 >= 0) {
                if (i2 < 0) {
                    i2 = 0;
                    this.h = System.currentTimeMillis();
                }
                return i2 % this.g;
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    private static String a(String str) {
        while (str.length() < 5) {
            str = "0" + str;
        }
        return str;
    }

    public static String a(int i2) {
        return "frame" + a("" + i2) + ".jpg";
    }

    public static String a(String str, int i2) {
        j[10] = (char) ((i2 % 10) + 48);
        int i3 = i2 / 10;
        j[9] = (char) ((i3 % 10) + 48);
        int i4 = i3 / 10;
        j[8] = (char) ((i4 % 10) + 48);
        int i5 = i4 / 10;
        j[7] = (char) ((i5 % 10) + 48);
        j[6] = (char) (((i5 / 10) % 10) + 48);
        k.setLength(0);
        return k.append(str).append(j).toString();
    }

    public static void a(Jigsaur.Level level) {
        a = null;
        a = new o(level);
    }

    public static void l() {
        a = null;
    }
}
