package com.ptowngames.jigsaur;

import com.badlogic.gdx.math.f;
import com.badlogic.gdx.math.g;
import com.ptowngames.jigsaur.a.d;
import java.util.Collection;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

public class x implements Set<j> {
    private static /* synthetic */ boolean k = (!x.class.desiredAssertionStatus());
    protected final HashSet<j> a;
    private final com.badlogic.gdx.math.a.a b;
    private boolean c;
    private final com.badlogic.gdx.math.a.a d;
    private boolean e;
    private boolean f;
    private Hashtable<Float, b> g;
    private g h;
    private g i;
    private g j;

    private class b {
        /* access modifiers changed from: private */
        public final f a;
        private final g b;
        private boolean c;
        /* access modifiers changed from: private */
        public float d;
        /* access modifiers changed from: private */
        public float e;
        /* access modifiers changed from: private */
        public boolean f;

        /* synthetic */ b(x xVar) {
            this((byte) 0);
        }

        static /* synthetic */ float c(b bVar, float f2) {
            float f3 = bVar.e - f2;
            bVar.e = f3;
            return f3;
        }

        static /* synthetic */ float d(b bVar, float f2) {
            float f3 = bVar.d + f2;
            bVar.d = f3;
            return f3;
        }

        private b(byte b2) {
            this.a = new f();
            this.b = new g();
            this.c = false;
            this.d = 0.0f;
            this.f = false;
        }
    }

    public x() {
        this.h = new g();
        this.i = new g();
        this.j = new g();
        this.b = new com.badlogic.gdx.math.a.a(new f(), new f());
        this.c = false;
        this.d = new com.badlogic.gdx.math.a.a(new f(), new f());
        this.e = false;
        this.a = new HashSet<>();
        this.g = new Hashtable<>();
        this.f = false;
    }

    public x(x xVar) {
        this.h = new g();
        this.i = new g();
        this.j = new g();
        this.b = xVar.b;
        this.c = xVar.c;
        this.d = xVar.d;
        this.e = xVar.e;
        this.a = xVar.a;
        this.g = xVar.g;
        this.f = xVar.f;
    }

    public final x a() {
        x xVar = new x();
        Iterator<j> it = this.a.iterator();
        while (it.hasNext()) {
            xVar.add(it.next());
        }
        if (this.c) {
            xVar.a(this.b);
        }
        if (this.e) {
            xVar.b(this.d);
        }
        xVar.a(this.f);
        return xVar;
    }

    /* renamed from: a */
    public boolean add(j jVar) {
        if (k || jVar != null) {
            boolean add = this.a.add(jVar);
            if (add && this.c) {
                b(jVar.j().c);
            }
            return add;
        }
        throw new AssertionError();
    }

    public boolean addAll(Collection<? extends j> collection) {
        boolean z = false;
        Iterator<? extends j> it = collection.iterator();
        while (true) {
            boolean z2 = z;
            if (!it.hasNext()) {
                return z2;
            }
            z = add((j) it.next()) | z2;
        }
    }

    public void clear() {
        this.a.clear();
    }

    public boolean contains(Object obj) {
        return this.a.contains(obj);
    }

    public boolean containsAll(Collection<?> collection) {
        return this.a.containsAll(collection);
    }

    public boolean isEmpty() {
        return this.a.isEmpty();
    }

    public Iterator<j> iterator() {
        return this.a.iterator();
    }

    public boolean remove(Object obj) {
        return this.a.remove(obj);
    }

    public boolean removeAll(Collection<?> collection) {
        boolean z = false;
        for (Object remove : collection) {
            z |= remove(remove);
        }
        return z;
    }

    public boolean retainAll(Collection<?> collection) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    public int size() {
        return this.a.size();
    }

    public Object[] toArray() {
        return this.a.toArray();
    }

    public <T> T[] toArray(T[] tArr) {
        return this.a.toArray(tArr);
    }

    public final void a(com.badlogic.gdx.math.a.a aVar) {
        this.b.a(aVar.a, aVar.b);
        this.c = true;
        this.g.clear();
        Iterator<j> it = iterator();
        while (it.hasNext()) {
            b(it.next().j().c);
        }
    }

    public final void b(com.badlogic.gdx.math.a.a aVar) {
        this.d.a(aVar.a, aVar.b);
        this.e = true;
        for (b a2 : this.g.values()) {
            a(a2);
        }
    }

    private void b(float f2) {
        if (this.g.get(Float.valueOf(f2)) == null) {
            this.g.put(Float.valueOf(f2), c(f2));
        }
    }

    private b c(float f2) {
        if (k || this.c) {
            b bVar = new b(this);
            if (!this.f) {
                u.a(this.b, f2, bVar.a);
            } else {
                bVar.a.a(c());
                bVar.a.c = f2;
            }
            if (this.e) {
                a(bVar);
            }
            return bVar;
        }
        throw new AssertionError();
    }

    private void a(b bVar) {
        if (k || this.e) {
            float unused = bVar.d = a(bVar, this.d);
            return;
        }
        throw new AssertionError();
    }

    private float a(b bVar, com.badlogic.gdx.math.a.a aVar) {
        u.a(aVar, bVar.a.c, this.h);
        this.h.a -= bVar.a.a;
        this.h.b -= bVar.a.b;
        return this.h.d() * 0.017453292f;
    }

    public final void a(boolean z) {
        if (z && !this.f) {
            e();
        }
        this.f = z;
    }

    private void e() {
        f c2 = c();
        for (b next : this.g.values()) {
            next.a.a = c2.a;
            next.a.b = c2.b;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ptowngames.jigsaur.x.b.a(com.ptowngames.jigsaur.x$b, boolean):boolean
     arg types: [com.ptowngames.jigsaur.x$b, int]
     candidates:
      com.ptowngames.jigsaur.x.b.a(com.ptowngames.jigsaur.x$b, float):float
      com.ptowngames.jigsaur.x.b.a(com.ptowngames.jigsaur.x$b, boolean):boolean */
    public final void c(com.badlogic.gdx.math.a.a aVar) {
        if (k || this.c) {
            if (this.f) {
                e();
            }
            float f2 = Float.POSITIVE_INFINITY;
            b bVar = null;
            Iterator<j> it = iterator();
            while (it.hasNext()) {
                j next = it.next();
                f l = next.l();
                float f3 = l.c;
                if (f2 != f3) {
                    bVar = this.g.get(Float.valueOf(f3));
                    if (bVar == null) {
                        bVar = c(f3);
                        this.g.put(Float.valueOf(f3), bVar);
                    }
                    if (!bVar.f) {
                        float unused = bVar.e = a(bVar, aVar);
                        b.c(bVar, bVar.d);
                        boolean unused2 = bVar.f = true;
                    }
                } else {
                    f3 = f2;
                }
                u.a(l, bVar.a, bVar.e);
                next.b(next.k() + bVar.e);
                next.m();
                f2 = f3;
            }
            this.d.a(aVar.a, aVar.b);
            if (((float) this.g.size()) >= 1.2f * ((float) size())) {
                a(this.b);
                return;
            }
            for (b next2 : this.g.values()) {
                if (!next2.f) {
                    float unused3 = next2.d = a(next2, aVar);
                } else {
                    b.d(next2, next2.e);
                    boolean unused4 = next2.f = false;
                }
            }
            return;
        }
        throw new AssertionError();
    }

    public final void a(aa aaVar) {
        Iterator<j> it = iterator();
        while (it.hasNext()) {
            it.next().a(aaVar);
        }
    }

    public final void b() {
        Iterator<j> it = iterator();
        while (it.hasNext()) {
            it.next().b(-1.0f, 1.0f, -1.0f, 1.0f);
        }
    }

    public final f c() {
        f fVar = new f(0.0f, 0.0f, 0.0f);
        Iterator<j> it = iterator();
        float f2 = 0.0f;
        while (it.hasNext()) {
            j next = it.next();
            float b2 = next.b();
            fVar.b(next.j().d(b2, b2, b2));
            f2 += b2;
        }
        if (f2 != 0.0f) {
            float f3 = 1.0f / f2;
            fVar.d(f3, f3, f3);
        }
        return fVar;
    }

    class a extends com.ptowngames.jigsaur.a.b {
        public a(float f, float f2) {
            super(200, f, f2);
        }

        public final void a(float f) {
            Iterator<j> it = x.this.iterator();
            while (it.hasNext()) {
                it.next().a(f);
            }
        }
    }

    public final void a(float f2) {
        Iterator<j> it = iterator();
        while (it.hasNext()) {
            j next = it.next();
            float k2 = (next.k() - f2) / 1.5707964f;
            float floor = (float) Math.floor((double) k2);
            if (((k2 - 440.32f) - floor) * -1.0f > 0.0f) {
                floor -= 4.0f;
            }
            d.a().a(new a(next.k(), (floor * 1.5707964f) + f2));
        }
    }

    public final j a(com.badlogic.gdx.math.a.a aVar, float f2) {
        return a(aVar, f2, null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v0, resolved type: com.ptowngames.jigsaur.x} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: com.ptowngames.jigsaur.x} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v2, resolved type: java.util.HashSet} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v22, resolved type: com.ptowngames.jigsaur.x} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.ptowngames.jigsaur.j a(com.badlogic.gdx.math.a.a r10, float r11, com.badlogic.gdx.physics.box2d.World r12) {
        /*
            r9 = this;
            r6 = 0
            if (r12 == 0) goto L_0x009d
            int r0 = r9.size()
            if (r0 <= 0) goto L_0x009d
            java.util.Iterator r2 = r9.iterator()
            r1 = r6
        L_0x000e:
            boolean r0 = r2.hasNext()
            if (r0 == 0) goto L_0x0023
            java.lang.Object r0 = r2.next()
            com.ptowngames.jigsaur.j r0 = (com.ptowngames.jigsaur.j) r0
            com.badlogic.gdx.math.f r0 = r0.l()
            float r0 = r0.c
            float r0 = r0 + r1
            r1 = r0
            goto L_0x000e
        L_0x0023:
            int r0 = r9.size()
            float r0 = (float) r0
            float r0 = r1 / r0
            r1 = 1036831949(0x3dcccccd, float:0.1)
            float r5 = r11 + r1
            com.badlogic.gdx.math.g r1 = r9.i
            com.ptowngames.jigsaur.u.a(r10, r0, r1)
            java.util.HashSet r7 = new java.util.HashSet
            r7.<init>()
            com.ptowngames.jigsaur.x$1 r1 = new com.ptowngames.jigsaur.x$1
            r1.<init>(r7)
            com.badlogic.gdx.math.g r0 = r9.i
            float r0 = r0.a
            float r2 = r0 - r5
            com.badlogic.gdx.math.g r0 = r9.i
            float r0 = r0.b
            float r3 = r0 - r5
            com.badlogic.gdx.math.g r0 = r9.i
            float r0 = r0.a
            float r4 = r0 + r5
            com.badlogic.gdx.math.g r0 = r9.i
            float r0 = r0.b
            float r5 = r5 + r0
            r0 = r12
            r0.QueryAABB(r1, r2, r3, r4, r5)
            r0 = r7
        L_0x005a:
            float r3 = r11 * r11
            r2 = 0
            r1 = 2139095040(0x7f800000, float:Infinity)
            java.util.Iterator r5 = r0.iterator()
            r8 = r1
            r1 = r2
            r2 = r3
            r3 = r8
        L_0x0067:
            boolean r0 = r5.hasNext()
            if (r0 == 0) goto L_0x00a4
            java.lang.Object r0 = r5.next()
            com.ptowngames.jigsaur.j r0 = (com.ptowngames.jigsaur.j) r0
            com.badlogic.gdx.math.f r4 = r0.j()
            float r4 = r4.c
            int r4 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r4 == 0) goto L_0x00a9
            com.badlogic.gdx.math.f r3 = r0.j()
            float r4 = r3.c
            com.badlogic.gdx.math.g r3 = r9.i
            com.ptowngames.jigsaur.u.a(r10, r4, r3)
        L_0x0088:
            com.badlogic.gdx.math.g r3 = r9.i
            float r3 = r0.a(r3)
            int r7 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
            if (r7 < 0) goto L_0x0098
            if (r1 != 0) goto L_0x00a6
            int r7 = (r3 > r2 ? 1 : (r3 == r2 ? 0 : -1))
            if (r7 != 0) goto L_0x00a6
        L_0x0098:
            int r1 = (r3 > r6 ? 1 : (r3 == r6 ? 0 : -1))
            if (r1 != 0) goto L_0x009f
        L_0x009c:
            return r0
        L_0x009d:
            r0 = r9
            goto L_0x005a
        L_0x009f:
            r1 = r3
        L_0x00a0:
            r3 = r4
            r2 = r1
            r1 = r0
            goto L_0x0067
        L_0x00a4:
            r0 = r1
            goto L_0x009c
        L_0x00a6:
            r0 = r1
            r1 = r2
            goto L_0x00a0
        L_0x00a9:
            r4 = r3
            goto L_0x0088
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ptowngames.jigsaur.x.a(com.badlogic.gdx.math.a.a, float, com.badlogic.gdx.physics.box2d.World):com.ptowngames.jigsaur.j");
    }

    public final g d() {
        g gVar = new g();
        Iterator<j> it = iterator();
        while (it.hasNext()) {
            gVar.a(it.next().a());
        }
        return gVar;
    }
}
