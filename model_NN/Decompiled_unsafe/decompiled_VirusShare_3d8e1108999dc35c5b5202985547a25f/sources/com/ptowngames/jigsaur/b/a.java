package com.ptowngames.jigsaur.b;

import com.ptowngames.jigsaur.Jigsaur;
import java.nio.ByteBuffer;

public class a {
    private static /* synthetic */ boolean e = (!a.class.desiredAssertionStatus());
    private float a;
    private float b;
    private float c;
    private Jigsaur.PieceSet d;

    public a(Jigsaur.PieceSet pieceSet, float f, float f2) {
        this.d = pieceSet;
        this.a = f;
        this.b = f2;
        this.c = (float) Math.sqrt((double) ((f * f) + (f2 * f2)));
    }

    public final int a() {
        return this.d.getPiecesCount();
    }

    public final b a(int i) {
        return new b(this.d.getPieces(i), this.a, this.b);
    }

    public final float b() {
        return this.d.getExtrusionDepth();
    }

    public final float c() {
        return this.c * this.d.getMaximumJoinDistance();
    }

    public final int d() {
        return this.d.getNumTotalIndices();
    }

    public final int e() {
        return this.d.getPieceVertices().a();
    }

    public final void a(float[] fArr) {
        int e2 = e() / 4;
        if (e || fArr.length >= e2) {
            ByteBuffer.wrap(this.d.getPieceVertices().b()).asFloatBuffer().get(fArr);
            "Scaling Piece Set Vertices by: " + this.a + ", " + this.b;
            for (int i = 0; i < e2; i += 5) {
                fArr[i] = fArr[i] * this.a;
                int i2 = i + 1;
                fArr[i2] = fArr[i2] * this.b;
            }
            return;
        }
        throw new AssertionError();
    }
}
