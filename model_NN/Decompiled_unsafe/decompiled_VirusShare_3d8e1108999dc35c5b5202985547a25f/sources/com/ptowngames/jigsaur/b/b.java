package com.ptowngames.jigsaur.b;

import com.ptowngames.jigsaur.Jigsaur;

public final class b {
    private float a;
    private float b;
    private Jigsaur.Piece c;

    public b(Jigsaur.Piece piece, float f, float f2) {
        this.c = piece;
        this.a = f;
        this.b = f2;
    }

    public final int a() {
        return this.c.getId();
    }

    public final int b() {
        return this.c.getTriangulationIndicesCount();
    }

    public final int a(int i) {
        return this.c.getTriangulationIndices(i);
    }

    public final int c() {
        return this.c.getPolygonIndicesCount();
    }

    public final int b(int i) {
        return this.c.getPolygonIndices(i);
    }

    public final int d() {
        return this.c.getExtrudedPolygonIndicesCount();
    }

    public final int c(int i) {
        return this.c.getExtrudedPolygonIndices(i);
    }

    public final int e() {
        return this.c.getJoinInfosCount();
    }

    public final Jigsaur.JoinInfo d(int i) {
        Jigsaur.JoinInfo.Builder newBuilder = Jigsaur.JoinInfo.newBuilder();
        Jigsaur.JoinInfo joinInfos = this.c.getJoinInfos(i);
        newBuilder.mergeFrom(joinInfos);
        Jigsaur.PolygonVertex.Builder newBuilder2 = Jigsaur.PolygonVertex.newBuilder();
        newBuilder2.setX(joinInfos.getJoinCircleCenter().getX() * this.a);
        newBuilder2.setY(joinInfos.getJoinCircleCenter().getY() * this.b);
        newBuilder.setJoinCircleCenter(newBuilder2);
        return newBuilder.build();
    }

    public final int f() {
        return this.c.getCornerPointsCount();
    }

    public final Jigsaur.PolygonVertex e(int i) {
        Jigsaur.PolygonVertex cornerPoints = this.c.getCornerPoints(i);
        Jigsaur.PolygonVertex.Builder newBuilder = Jigsaur.PolygonVertex.newBuilder();
        newBuilder.setX(cornerPoints.getX() * this.a);
        newBuilder.setY(cornerPoints.getY() * this.b);
        return newBuilder.build();
    }
}
