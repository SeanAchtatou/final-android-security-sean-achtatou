package com.ptowngames.jigsaur;

import com.badlogic.gdx.graphics.f;
import com.badlogic.gdx.graphics.n;
import com.badlogic.gdx.math.g;

public class ak {
    private static /* synthetic */ boolean f = (!ak.class.desiredAssertionStatus());
    private f a;
    private float b;
    private float c;
    private float d;
    private boolean e;

    private g b(g gVar) {
        return new g((-0.5f + gVar.a) * this.b, (0.5f - gVar.b) * this.c);
    }

    public final g a(g gVar) {
        g a2 = gVar.a();
        a2.a -= this.b * 0.5f;
        a2.b -= this.c * 0.5f;
        return a2;
    }

    public final float a() {
        return this.d;
    }

    public ak() {
        this.b = -1.0f;
        this.c = -1.0f;
        this.d = -1.0f;
        this.e = false;
        this.b = o.a().c();
        this.c = o.a().d();
        if (!f && this.b <= 0.0f) {
            throw new AssertionError();
        } else if (f || this.c > 0.0f) {
            g b2 = b(new g(0.0f, 1.0f));
            g b3 = b(new g(1.0f, 0.0f));
            this.d = (-5.0f - o.a().e().b()) + 0.002f;
            float f2 = b2.a;
            float f3 = b2.b;
            float f4 = this.d;
            float f5 = b3.a;
            float f6 = b3.b;
            float f7 = this.d;
            float[] fArr = {f2, f3, f4, 0.0f, 1.0f, f2, f6, f7, 0.0f, 0.0f, f5, f6, f7, 1.0f, 0.0f, f5, f6, f7, 1.0f, 0.0f, f5, f3, f4, 1.0f, 1.0f, f2, f3, f4, 0.0f, 1.0f};
            short[] sArr = new short[6];
            for (int i = 0; i < 6; i++) {
                sArr[i] = (short) i;
            }
            this.a = f.b().a(6, 6, new n(0, 3, "sol_overlay_pos"), new n(3, 2, "sol_overlay_uv"));
            this.a.a(fArr);
            this.a.a(sArr);
        } else {
            throw new AssertionError();
        }
    }

    public final void b() {
        if (f || !this.e) {
            this.a.a(4, 0, 6);
            return;
        }
        throw new AssertionError();
    }

    public final void c() {
        this.e = true;
    }
}
