package com.ptowngames.jigsaur;

import com.badlogic.gdx.graphics.c;
import com.badlogic.gdx.math.f;
import com.badlogic.gdx.math.g;
import com.ptowngames.jigsaur.Jigsaur;
import com.ptowngames.jigsaur.a.d;
import com.ptowngames.jigsaur.ag;
import com.ptowngames.jigsaur.c;
import com.ptowngames.jigsaur.w;
import com.ptowngames.webservices.Webservices;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

public class PuzzlePlayer implements ap, c.a {
    private static /* synthetic */ boolean M;
    private static float[] v = {0.05f, 0.95f, 0.95f, 0.05f};
    private static float[] w = {0.05f, 0.05f, 0.95f, 0.95f};
    private static int[] x = {0, 1, 2, 3};
    private int A = -1;
    private int B = -1;
    private boolean C = false;
    private boolean D = false;
    private g E = new g();
    private a F = new a();
    private a G = new a();
    private a H = new a();
    private a I = new a();
    private long J = 0;
    private boolean K = false;
    private v L = new v();
    public double a = 0.0d;
    private boolean b = false;
    private int c;
    private int d = 0;
    private int e = 0;
    private float f = 1.0f;
    /* access modifiers changed from: private */
    public w g;
    private Jigsaur.PolygonVertex h = null;
    private float i = -1.0f;
    private float j = -1.0f;
    private w.a k;
    private a l = null;
    private int m;
    private ArrayList<s> n = new ArrayList<>();
    private ac o = new ac();
    private com.badlogic.gdx.graphics.b[] p;
    private long q = -1;
    /* access modifiers changed from: private */
    public int r = 0;
    private boolean s = false;
    private c t = new c();
    private boolean u = false;
    private int y = -1;
    private int z = -1;

    protected static native boolean getNextFrame(ByteBuffer byteBuffer);

    protected static native void initFFMpeg();

    protected static native void loadMovie(String str);

    static {
        boolean z2;
        if (!PuzzlePlayer.class.desiredAssertionStatus()) {
            z2 = true;
        } else {
            z2 = false;
        }
        M = z2;
    }

    public final void a(Object obj, c.b bVar, Object obj2) {
        switch (bVar) {
            case NO_GLOBS_VISIBLE:
                this.g.c(this.g.g() * 0.99f);
                return;
            case PUZZLE_SOLVED:
                g c2 = this.l.c();
                g b2 = c2.b.a().b(c2.a);
                g gVar = new g(b2.a * 0.2f, b2.b * 0.2f);
                c2.a.b(gVar);
                c2.b.c(gVar);
                g b3 = c2.b.a().b(c2.a);
                float h2 = this.g.h();
                this.g.j();
                this.g.a(this.g.a.a(), -h2);
                this.g.j();
                g d2 = this.g.d();
                this.g.a(this.g.a.a(), h2);
                this.g.j();
                float g2 = this.g.g() / Math.max(b3.a / (d2.b.a - d2.a.a), b3.b / (d2.b.b - d2.a.b));
                g c3 = c2.a.a().c(b3.a().a(0.5f));
                com.badlogic.gdx.math.a.a aVar = new com.badlogic.gdx.math.a.a(new f(c3.a, c3.b, -5.0f), this.g.b.a().d(-1.0f, -1.0f, -1.0f));
                f fVar = new f();
                u.a(aVar, 0.0f, fVar);
                d.a().a(new b(fVar, g2, -h2));
                this.b = true;
                ag.a(ag.b.PUZZLE_PLAYER, Webservices.RewardCheckRequest.a.LEVEL_SOLVED, null);
                return;
            default:
                return;
        }
    }

    public final int a() {
        return this.c;
    }

    public final void a(int i2, int i3) {
        "SCREEN: " + i2 + MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR + i3;
        this.d = i2;
        this.e = i3;
        r.a().a(new g(0.0f, 0.0f), new g((float) i2, (float) i3));
        this.f = ((float) i2) / ((float) i3);
        this.g = new w(2.0f * this.f, this.g.g());
        this.g.a(this.y == -1);
        Jigsaur.Level h2 = o.a().h();
        this.g.b(h2.getMinZoom());
        this.g.a(h2.getMaxZoom());
        this.g.a(h2.getMinX(), h2.getMinY());
        this.g.b(h2.getMaxX(), h2.getMaxY());
        if (this.k != null) {
            this.g.a(this.k);
        }
        Iterator<s> it = this.n.iterator();
        while (it.hasNext()) {
            this.g.a(it.next());
        }
        this.g.a(this.l);
        this.g.b();
        this.g.a();
        this.g.i();
        if (this.h != null) {
            f b2 = a.b(new f(this.h.getX(), this.h.getY(), 0.0f));
            b2.c = 0.0f;
            this.g.a(b2.a, b2.b, b2.c);
            this.h = null;
        }
    }

    public final boolean a(int i2, int i3, int i4) {
        if (this.l.a(this.g.c((float) i2, (float) i3), this.g)) {
            return true;
        }
        this.l.h();
        return true;
    }

    public final boolean b(int i2, int i3, int i4) {
        if (!M && this.K) {
            throw new AssertionError();
        } else if (!M && i4 == this.B) {
            throw new AssertionError();
        } else if (M || i4 != this.y) {
            com.badlogic.gdx.math.a.a c2 = this.g.c((float) i2, (float) i3);
            if ((this.y == -1 || this.B >= 0) && this.l.a(c2, i4)) {
                return true;
            }
            if (this.y == -1) {
                this.y = i4;
                this.g.a(i2, i3);
                this.z = i2;
                this.A = i3;
                this.g.a(false);
                return true;
            } else if (this.B != -1) {
                return true;
            } else {
                if (M || this.g.k()) {
                    this.B = i4;
                    this.g.c(i2, i3);
                    return true;
                }
                throw new AssertionError();
            }
        } else {
            throw new AssertionError();
        }
    }

    public final boolean c(int i2, int i3, int i4) {
        this.l.c(this.g.c((float) i2, (float) i3), i4);
        return true;
    }

    public final void d(int i2, int i3, int i4) {
        com.badlogic.gdx.math.a.a c2 = this.g.c((float) i2, (float) i3);
        if (this.l.d(this.g.c((float) i2, (float) i3), i4)) {
            return;
        }
        if (this.l.b(c2, i4)) {
            if (ar.a().h && !this.D) {
                ag.a(ag.b.PUZZLE_PLAYER, Webservices.RewardCheckRequest.a.FIRST_PIECE_MOVED, null);
            }
            this.D = true;
        } else if (i4 == this.y) {
            if (ar.a().h && !this.C) {
                ag.a(ag.b.PUZZLE_PLAYER, Webservices.RewardCheckRequest.a.FIRST_CAMERA_MOVED, null);
            }
            this.C = true;
            this.g.b(i2, i3);
            this.z = i2;
            this.A = i3;
        } else if (i4 == this.B && this.y != -1) {
            this.g.d(i2, i3);
            int i5 = this.z - i2;
            int i6 = this.A - i3;
            float sqrt = (float) Math.sqrt((double) ((i5 * i5) + (i6 * i6)));
            if (this.i == -1.0f) {
                this.i = sqrt;
                this.j = this.g.g();
            }
            float f2 = sqrt / this.i;
            this.g.a(f2 * f2 * this.j, this.z, this.A, -5.0f);
        }
    }

    public final void a(int i2) {
        if (!(this.l.b(i2)) && !this.l.a(i2)) {
            if (i2 == this.y) {
                this.y = -1;
                this.z = -1;
                this.A = -1;
                this.g.a(true);
            } else if (i2 == this.B) {
                this.B = -1;
            }
            this.i = -1.0f;
        }
    }

    public final w b() {
        return this.g;
    }

    final class b extends com.ptowngames.jigsaur.a.a {
        private f b;
        private float c;
        private float d;
        private long e;
        private long f;

        public b(f fVar, float f2, float f3) {
            a(true);
            a(16);
            this.b = fVar.a().c(PuzzlePlayer.this.g.a).a(0.003125f);
            this.d = (f2 - PuzzlePlayer.this.g.g()) / 320.0f;
            this.c = f3 / 320.0f;
            long currentTimeMillis = System.currentTimeMillis();
            this.e = currentTimeMillis;
            this.f = currentTimeMillis;
        }

        public final void c() {
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis >= this.f + 320) {
                a(false);
                currentTimeMillis = this.f + 320;
            }
            float f2 = (float) (currentTimeMillis - this.e);
            this.e = currentTimeMillis;
            if (f2 > 0.0f) {
                PuzzlePlayer.this.g.j();
                PuzzlePlayer.this.g.a.b(this.b.a().a(f2));
                PuzzlePlayer.this.g.c(PuzzlePlayer.this.g.g() + (this.d * f2));
                PuzzlePlayer.this.g.i();
                if (this.c != 0.0f) {
                    PuzzlePlayer.this.g.j();
                    PuzzlePlayer.this.g.a(PuzzlePlayer.this.g.a.a(), f2 * this.c);
                }
            }
        }
    }

    private static File a(String str) {
        try {
            InputStream b2 = com.badlogic.gdx.g.e.b(str).b();
            File createTempFile = File.createTempFile("movie", ".mp4", new File("/sdcard/"));
            FileOutputStream fileOutputStream = new FileOutputStream(createTempFile);
            byte[] bArr = new byte[8192];
            while (true) {
                int read = b2.read(bArr);
                if (read != -1) {
                    fileOutputStream.write(bArr, 0, read);
                } else {
                    fileOutputStream.close();
                    b2.close();
                    return createTempFile;
                }
            }
        } catch (IOException e2) {
            return null;
        }
    }

    private void g() {
        int i2 = 0;
        long currentTimeMillis = System.currentTimeMillis();
        Jigsaur.PuzzleImageInfo i3 = o.a().i();
        this.p = new com.badlogic.gdx.graphics.b[i3.getNumFrames()];
        if (i3.getType() == Jigsaur.PuzzleImageInfo.a.MOVIE) {
            System.loadLibrary("avutil");
            System.loadLibrary("avcore");
            System.loadLibrary("avcodec");
            System.loadLibrary("avformat");
            System.loadLibrary("avdevice");
            System.loadLibrary("avfilter");
            System.loadLibrary("swscale");
            System.loadLibrary("videoframes");
            initFFMpeg();
            File a2 = a(i3.getResourceName());
            if (a2 != null) {
                "copied to " + a2.getPath();
                loadMovie(a2.getPath());
                while (i2 < this.p.length) {
                    com.badlogic.gdx.graphics.c cVar = new com.badlogic.gdx.graphics.c(256, 256, c.a.RGB888);
                    if (!getNextFrame(cVar.i())) {
                        "Error loading frame " + i2;
                    }
                    this.p[i2] = f.b().a(cVar);
                    i2++;
                }
                a2.delete();
            } else {
                return;
            }
        } else if (i3.getType() == Jigsaur.PuzzleImageInfo.a.MOVIE_FRAMES) {
            StringBuilder sb = new StringBuilder(o.a(i3.getResourceName(), 0));
            int length = i3.getResourceName().length() + 6 + 5;
            while (i2 < this.p.length) {
                sb.replace(length - String.valueOf(i2).length(), length, String.valueOf(i2));
                this.p[i2] = f.b().a(com.badlogic.gdx.g.e.b(sb.toString()));
                i2++;
            }
        } else {
            "Unknown Puzzle Image type: " + i3.getType();
            if (!M) {
                throw new AssertionError();
            }
        }
        "Done loading textures, total time: " + (System.currentTimeMillis() - currentTimeMillis);
    }

    class c extends com.ptowngames.jigsaur.a.a {
        private long b = -1;
        private long c = -1;

        public c() {
            a(true);
            a(10000);
        }

        public final void a() {
            this.b = (long) PuzzlePlayer.this.r;
            this.c = System.currentTimeMillis();
        }

        public final void c() {
            long currentTimeMillis = System.currentTimeMillis();
            long j = currentTimeMillis - this.c;
            if (j == 0) {
                PuzzlePlayer.this.a = 0.0d;
            } else {
                PuzzlePlayer.this.a = (((double) (((long) PuzzlePlayer.this.r) - this.b)) * 1000.0d) / ((double) j);
            }
            "Estimating FPS = " + PuzzlePlayer.this.a + " over last " + j + " milliseconds.";
            this.c = currentTimeMillis;
            this.b = (long) PuzzlePlayer.this.r;
        }
    }

    public final void a(aa aaVar) {
        float f2;
        if (!M && this.K) {
            throw new AssertionError();
        } else if (M || this.m == o.a().g()) {
            if (this.u) {
                h();
                i();
                this.q = -1;
                this.g.c(2.5f);
                a(this.d, this.e);
                this.u = false;
            }
            if (ar.a().d) {
                float a2 = com.badlogic.gdx.g.d.a();
                float b2 = com.badlogic.gdx.g.d.b();
                float c2 = com.badlogic.gdx.g.d.c();
                this.F.a(a2);
                this.G.a(b2);
                this.H.a(c2);
                this.I.a((float) Math.sqrt((double) ((a2 * a2) + (b2 * b2) + (c2 * c2))));
                float f3 = -a2;
                if (this.J == 0) {
                    this.J = System.currentTimeMillis();
                }
                long currentTimeMillis = System.currentTimeMillis();
                long j2 = currentTimeMillis - this.J;
                boolean z2 = f3 < 0.0f;
                float abs = Math.abs(f3);
                if (abs > 1.0f) {
                    float f4 = (abs - 1.0f) / 8.8f;
                    if (f4 > 1.0f) {
                        f4 = 1.0f;
                    }
                    if (M || f4 >= 0.0f) {
                        if (f4 > 0.0f) {
                            float pow = (float) (Math.pow(0.5d, 1.0d - ((double) f4)) - 0.5d);
                            f4 = pow * pow * pow;
                        }
                        float pow2 = (float) Math.pow((double) ((f4 * 6400.0f) + 1.0f), (double) (((float) j2) / 1000.0f));
                        if (z2) {
                            f2 = 1.0f / pow2;
                        } else {
                            f2 = pow2;
                        }
                        this.g.c(f2 * this.g.g());
                    } else {
                        throw new AssertionError();
                    }
                }
                this.J = currentTimeMillis;
            }
            long currentTimeMillis2 = System.currentTimeMillis();
            if (this.q < 0) {
                this.q = currentTimeMillis2;
                d.a().a(this.t);
            }
            this.r++;
            if (!this.s && currentTimeMillis2 - this.q > 5000) {
                this.s = true;
                "" + this.r + " frames rendered in first 3 seconds.";
            }
            this.l.a(currentTimeMillis2 - this.q);
            com.badlogic.gdx.g.b.b().glEnable(2929);
            com.badlogic.gdx.g.b.b().glDepthFunc(513);
            this.g.a(com.badlogic.gdx.g.g);
            int k2 = o.a().k();
            com.badlogic.gdx.g.f.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
            com.badlogic.gdx.g.f.glClear(16640);
            com.badlogic.gdx.g.g.b();
            ar.a();
            com.badlogic.gdx.g.g.glEnable(3553);
            if (this.l.k()) {
                com.badlogic.gdx.g.g.a(0.75f, 0.75f, 1.0f, 1.0f);
            } else {
                com.badlogic.gdx.g.g.a(1.0f, 1.0f, 1.0f, 1.0f);
            }
            this.o.a();
            com.badlogic.gdx.g.g.glDisable(3553);
            this.l.b(this.g);
            this.l.a(aaVar, this.p[k2], this.g.g());
            com.badlogic.gdx.g.b.b().glDepthFunc(519);
            aaVar.c();
            com.badlogic.gdx.g.g.a();
            this.l.a(false);
        } else {
            throw new AssertionError();
        }
    }

    private void h() {
        if (this.l != null) {
            this.l.l();
        }
        this.l = new a();
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.n.size()) {
                s sVar = this.n.get(i3);
                if (sVar.f()) {
                    sVar.i();
                }
                sVar.n = -1;
                this.n.get(i3).a(0.0f);
                this.l.a(this.n.get(i3));
                i2 = i3 + 1;
            } else {
                this.l.a(o.a().e().c());
                return;
            }
        }
    }

    public final boolean c() {
        return this.l != null && this.l.g();
    }

    public PuzzlePlayer(w.a aVar) {
        if (M || o.a() != null) {
            c.a(this, c.b.NO_GLOBS_VISIBLE);
            c.a(this, c.b.PUZZLE_SOLVED);
            this.c = 10;
            this.k = aVar;
            this.g = new w(2.0f, 2.5f);
            com.ptowngames.jigsaur.b.a e2 = o.a().e();
            int a2 = e2.a();
            this.m = o.a().g();
            for (int i2 = 0; i2 < a2; i2++) {
                this.n.add(new s(e2.a(i2), (byte) 0));
            }
            s.t();
            h();
            "added all PuzzlePieces: " + System.currentTimeMillis();
            Jigsaur.SavedConfiguration j2 = o.a().j();
            if (j2 == null) {
                i();
            } else if (M || j2.getWorkspacesCount() == 1) {
                this.l.a(j2.getWorkspaces(0));
                Jigsaur.Level h2 = o.a().h();
                this.g.c(a.b(j2.getScaleFactor(), h2.getMinZoom(), h2.getMaxZoom()));
                this.h = j2.getCameraCenter();
            } else {
                throw new AssertionError();
            }
            g();
            return;
        }
        throw new AssertionError();
    }

    private void i() {
        this.l.b().b();
    }

    public final void d() {
        this.u = true;
    }

    public final void e() {
        if (M || !this.K) {
            Jigsaur.SavedConfiguration.Builder newBuilder = Jigsaur.SavedConfiguration.newBuilder();
            newBuilder.setNumPieces(o.a().f());
            newBuilder.addWorkspaces(this.l.j());
            Jigsaur.Level h2 = o.a().h();
            newBuilder.setScaleFactor(a.a(this.g.g(), h2.getMinZoom(), h2.getMaxZoom()));
            f a2 = a.a(this.g.a);
            Jigsaur.PolygonVertex.Builder newBuilder2 = Jigsaur.PolygonVertex.newBuilder();
            newBuilder2.setX(a2.a);
            newBuilder2.setY(a2.b);
            newBuilder.setCameraCenter(newBuilder2);
            o.a().a(newBuilder.build());
            return;
        }
        throw new AssertionError();
    }

    class a {
        private static /* synthetic */ boolean f = (!PuzzlePlayer.class.desiredAssertionStatus());
        private int a = 0;
        private float[] b = new float[20];
        private float c = 0.0f;
        private float d = 0.0f;
        private int e = 0;

        public a() {
        }

        public final void a(float f2) {
            if (this.e == 20) {
                this.c -= this.b[this.a];
                this.d -= this.b[this.a] * this.b[this.a];
            }
            this.c += f2;
            this.d += f2 * f2;
            this.b[this.a] = f2;
            if (this.e < 20) {
                this.e++;
            } else if (!f && this.e != 20) {
                throw new AssertionError();
            }
            this.a = (this.a + 1) % 20;
        }
    }

    public final void f() {
        this.o.b();
        this.l.l();
        c.b(this, c.b.NO_GLOBS_VISIBLE);
        c.b(this, c.b.PUZZLE_SOLVED);
        this.K = true;
    }
}
