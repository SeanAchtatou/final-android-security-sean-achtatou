package com.ptowngames.jigsaur;

public final class g {
    public final com.badlogic.gdx.math.g a;
    public final com.badlogic.gdx.math.g b;

    public g() {
        this.a = new com.badlogic.gdx.math.g(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY);
        this.b = new com.badlogic.gdx.math.g(Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY);
    }

    private g(com.badlogic.gdx.math.g gVar, com.badlogic.gdx.math.g gVar2) {
        this.a = gVar;
        this.b = gVar2;
    }

    public final g a() {
        return new g(this.a.a(), this.b.a());
    }

    public final g a(float f, float f2) {
        this.a.a += f;
        this.a.b += f2;
        this.b.a += f;
        this.b.b += f2;
        return this;
    }

    public final g a(g gVar) {
        if (gVar.a.a <= gVar.b.a) {
            a(gVar.a);
            a(gVar.b);
        }
        return this;
    }

    public final g b(float f, float f2) {
        if (f < this.a.a) {
            this.a.a = f;
        }
        if (f2 < this.a.b) {
            this.a.b = f2;
        }
        if (f > this.b.a) {
            this.b.a = f;
        }
        if (f2 > this.b.b) {
            this.b.b = f2;
        }
        return this;
    }

    public final g a(com.badlogic.gdx.math.g gVar) {
        return b(gVar.a, gVar.b);
    }

    public final String toString() {
        return "ZPlaneBBox: Max = " + this.b + ", Min = " + this.a;
    }
}
