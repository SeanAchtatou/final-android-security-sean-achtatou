package com.ptowngames.jigsaur;

import com.badlogic.gdx.graphics.b;
import com.badlogic.gdx.graphics.n;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.f;
import com.badlogic.gdx.math.g;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.ptowngames.jigsaur.Jigsaur;
import com.ptowngames.jigsaur.w;
import java.util.Arrays;
import java.util.Collection;

public final class s extends j implements w.a {
    private static float[] A;
    private static short[] B;
    private static int C;
    private static Matrix4 H = new Matrix4();
    private static f I = new f();
    private static com.badlogic.gdx.graphics.f J = null;
    private static final float[] L = new float[20];
    private static final float[] M = new float[20];
    private static /* synthetic */ boolean N;
    public static final b l;
    public static com.badlogic.gdx.graphics.f m = null;
    private static boolean w = false;
    private static boolean x;
    private static int y = -1;
    private static int z = -1;
    private int D;
    private boolean E;
    private PolygonShape F;
    private float G;
    private g K;
    Matrix4 a;
    int f;
    int g;
    int h;
    int i;
    int j;
    int k;
    public long n;
    boolean o;
    private a[] p;
    private com.ptowngames.jigsaur.b.b q;
    private q r;
    private Matrix4 s;
    private boolean t;
    private boolean u;
    private g v;

    static {
        boolean z2;
        if (!s.class.desiredAssertionStatus()) {
            z2 = true;
        } else {
            z2 = false;
        }
        N = z2;
        if (com.badlogic.gdx.g.e != null) {
            l = new b(com.badlogic.gdx.g.e.b("data/cardboard.png"), (byte) 0);
        } else {
            l = null;
        }
        for (int i2 = 0; i2 < 20; i2++) {
            float pow = (float) Math.pow(((double) i2) / 19.0d, 2.0d);
            float f2 = pow * pow;
            L[i2] = f2;
            M[i2] = (f2 + 1.0f) - (pow * 2.0f);
        }
    }

    class a {
        boolean a;
        int b;
        private float d;
        private float e;
        private float f;

        /* synthetic */ a(s sVar, float f2, float f3, float f4, boolean z, int i) {
            this(f2, f3, f4, z, i, (byte) 0);
        }

        private a(float f2, float f3, float f4, boolean z, int i, byte b2) {
            this.d = (float) Math.sqrt((double) ((f2 * f2) + (f3 * f3)));
            this.e = (float) Math.atan2((double) f3, (double) f2);
            this.f = f4;
            this.a = z;
            this.b = i;
        }

        public final float a() {
            return s.this.j().a + (this.d * ((float) Math.cos((double) (this.e + s.this.e))));
        }

        public final float b() {
            return s.this.j().b + (this.d * ((float) Math.sin((double) (this.e + s.this.e))));
        }

        public final float c() {
            return this.f + s.this.e;
        }

        public final String toString() {
            return "[ PuzzlePiece.JoinPoint " + a() + ":" + b() + ":" + c() + ":" + this.a + ":" + this.b + ":" + s.this.s() + " ]";
        }
    }

    public final Collection<a> p() {
        return Arrays.asList(this.p);
    }

    public final g q() {
        if (this.v == null) {
            return null;
        }
        return this.v.a();
    }

    public static void a(com.ptowngames.jigsaur.b.a aVar) {
        x = false;
        w = true;
        y = (aVar.e() / 5) / 4;
        if (N || y <= 65535) {
            z = aVar.d();
            if (m != null) {
                m.a();
                m = null;
                A = null;
                B = null;
                Runtime.getRuntime().gc();
            }
            com.badlogic.gdx.graphics.f fVar = new com.badlogic.gdx.graphics.f(true, y, z, new n(0, 3, "pieces_pos"), new n(3, 2, "pieces_uv"));
            m = fVar;
            fVar.c();
            A = new float[(y * 5)];
            B = new short[z];
            C = 0;
            aVar.a(A);
            return;
        }
        throw new AssertionError();
    }

    public static void r() {
        if (!N) {
            w = true;
        }
        m.a();
        m = null;
        A = null;
        B = null;
        w = false;
        Runtime.getRuntime().gc();
    }

    private static void a(int i2) {
        if (N || i2 <= 65535) {
            short[] sArr = B;
            int i3 = C;
            C = i3 + 1;
            sArr[i3] = (short) i2;
            return;
        }
        throw new AssertionError();
    }

    public final int s() {
        return this.D;
    }

    public s(com.ptowngames.jigsaur.b.b bVar, byte b) {
        this(bVar);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private s(com.ptowngames.jigsaur.b.b bVar) {
        super((byte) 0);
        this.t = true;
        this.u = true;
        this.n = -1;
        this.G = 0.0f;
        this.o = false;
        this.K = null;
        if (N || w) {
            this.s = new Matrix4().a();
            this.a = new Matrix4().a();
            this.q = bVar;
            a(0.0f);
            this.D = bVar.a();
            this.E = false;
            this.g = C;
            int b = bVar.b();
            this.f = b;
            for (int i2 = 0; i2 < b; i2++) {
                a(bVar.a(i2));
            }
            this.j = C;
            if (N || bVar.c() == bVar.d()) {
                int c = bVar.c();
                this.k = c * 2;
                for (int i3 = 0; i3 < c; i3++) {
                    a(bVar.b(i3));
                    a(bVar.c(i3));
                }
                this.h = C;
                this.i = c;
                for (int i4 = 0; i4 < c; i4++) {
                    a(bVar.b(i4));
                }
                this.r = new q(A, B, this.h, this.i);
                this.v = this.r.b();
                c(this.v);
                b(this.v);
                this.r.a(this.v.a, this.v.b);
                a(this.v.a, this.v.b, -5.0f);
                if (!N && this.E) {
                    throw new AssertionError();
                } else if (N || !x) {
                    this.E = true;
                } else {
                    throw new AssertionError();
                }
            } else {
                throw new AssertionError();
            }
        } else {
            throw new AssertionError();
        }
    }

    public final void n() {
        super.n();
        this.t = true;
        this.K = null;
    }

    public final void a(w wVar) {
        this.u = true;
    }

    public final void c(float f2) {
        this.G = f2;
        this.t = true;
    }

    public static void a(s[] sVarArr, w wVar, float f2) {
        boolean z2;
        g gVar = new g();
        float f3 = Float.POSITIVE_INFINITY;
        for (s sVar : sVarArr) {
            if (f3 != f2) {
                wVar.a(f2, gVar);
                f3 = f2;
            }
            if (sVar.K == null) {
                sVar.v();
            }
            g gVar2 = sVar.K;
            if (!(gVar.b.a >= gVar2.a.a && gVar.b.b >= gVar2.a.b && gVar2.b.a >= gVar.a.a && gVar2.b.b >= gVar.a.b)) {
                z2 = true;
            } else {
                z2 = false;
            }
            sVar.o = z2;
            if (!sVar.o || sVar.b.c >= -4.9f) {
                if (sVar.t || sVar.n > 0) {
                    sVar.s.a(sVar.b);
                    if (sVar.n > 0) {
                        float sin = 0.04f * ((float) Math.sin((double) (((float) (System.currentTimeMillis() - sVar.n)) / 500.0f)));
                        float[] fArr = sVar.s.a;
                        fArr[14] = sin + fArr[14];
                    }
                    sVar.s.b(H.a((sVar.e + sVar.G) * 57.29578f));
                    sVar.s.b(H.b(-sVar.v.a, -sVar.v.b));
                    sVar.t = false;
                    sVar.u = true;
                }
                if (sVar.u) {
                    sVar.a.a(wVar.e.a).b(sVar.s);
                    sVar.u = false;
                }
            }
        }
    }

    private void b(g gVar) {
        g[] gVarArr = new g[this.q.f()];
        for (int i2 = 0; i2 < this.q.f(); i2++) {
            Jigsaur.PolygonVertex e = this.q.e(i2);
            gVarArr[i2] = new g(e.getX() - gVar.a, e.getY() - gVar.b);
        }
        this.F = f.b().c();
        this.F.set(gVarArr);
    }

    private void c(g gVar) {
        this.p = new a[this.q.e()];
        for (int i2 = 0; i2 < this.p.length; i2++) {
            Jigsaur.JoinInfo d = this.q.d(i2);
            this.p[i2] = new a(this, d.getJoinCircleCenter().getX() - gVar.a, d.getJoinCircleCenter().getY() - gVar.b, -d.getBaseOrientation(), !d.getOuttieStatus(), d.getNeighborId());
        }
    }

    public static void t() {
        if (N || !x) {
            m.a(A);
            "used " + C + " of " + B.length + " indices";
            com.badlogic.gdx.graphics.f fVar = m;
            short[] sArr = B;
            int i2 = C;
            int i3 = i2 + 0;
            if (i3 < 0) {
                throw new IllegalArgumentException(0 + " > " + i2);
            }
            short[] sArr2 = new short[i3];
            System.arraycopy(sArr, 0, sArr2, 0, Math.min(sArr.length + 0, i3));
            fVar.a(sArr2);
            x = true;
            return;
        }
        throw new AssertionError();
    }

    public final Shape u() {
        return this.F;
    }

    public final g a() {
        if (this.K == null) {
            v();
        }
        return this.K.a();
    }

    private void v() {
        this.K = new g();
        g a2 = this.r.a();
        g gVar = new g(0.0f, 0.0f);
        this.K.a(u.a(a2.a.a(), gVar, this.e));
        this.K.a(u.a(a2.a, gVar, -this.e));
        this.K.a(u.a(a2.b.a(), gVar, this.e));
        this.K.a(u.a(a2.b, gVar, -this.e));
        g gVar2 = this.K;
        f fVar = this.b;
        gVar2.a(fVar.a, fVar.b);
    }

    public final float a(g gVar) {
        f j2 = j();
        g gVar2 = new g(j2.a, j2.b);
        g b = u.a(gVar.a(), gVar2, -this.e).b(gVar2);
        return this.r.b(b.a, b.b);
    }
}
