package com.ptowngames.jigsaur;

import com.badlogic.gdx.graphics.i;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.f;
import com.badlogic.gdx.math.g;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.MouseJoint;
import com.badlogic.gdx.physics.box2d.joints.MouseJointDef;
import com.ptowngames.jigsaur.Jigsaur;
import com.ptowngames.jigsaur.a.b;
import com.ptowngames.jigsaur.a.c;
import com.ptowngames.jigsaur.a.d;
import com.ptowngames.jigsaur.c;
import com.ptowngames.jigsaur.f;
import com.ptowngames.jigsaur.s;
import com.ptowngames.jigsaur.w;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;

public class a implements w.a {
    private static /* synthetic */ boolean S;
    private static int l = 0;
    private static float[] u = {1.0f, 1.0f, 1.0f, 1.0f};
    private static float[] v = {0.0f, 0.0f, 0.0f, 1.0f};
    private static final Matrix4 w = new Matrix4();
    private long A;
    private x B = new x();
    private float C = 0.2f;
    private float D = 0.2f;
    private int E = -1;
    private com.badlogic.gdx.math.a.a F = null;
    private HashMap<Integer, s> G;
    private C0018a H;
    private Body I = null;
    /* access modifiers changed from: private */
    public ak J = new ak();
    private ah K = new ah(f.b().a("data/1.png"));
    private ah L = new ah(f.b().a("data/2.png"));
    private ah M = new ah(f.b().a("data/3.png"));
    private ah N = null;
    private ao O = null;
    private b P = null;
    private boolean Q = false;
    private int R = -1;
    public final Body a;
    World b = new World(new g(), true);
    Hashtable<Integer, al> c = new Hashtable<>();
    Hashtable<Integer, C0018a> d = new Hashtable<>();
    protected float e;
    HashMap<s, C0018a> f;
    x g;
    protected C0018a h;
    protected boolean i = false;
    protected float j = 1.0f;
    public boolean k = false;
    /* access modifiers changed from: private */
    public x m;
    private long n = -1;
    private y o;
    private ArrayList<c> p;
    private ArrayList<s> q;
    private s[] r = new s[0];
    /* access modifiers changed from: private */
    public C0018a[] s = new C0018a[0];
    /* access modifiers changed from: private */
    public boolean t = true;
    /* access modifiers changed from: private */
    public HashMap<Long, d> x;
    private boolean y = false;
    private Box2DDebugRenderer z = new Box2DDebugRenderer();

    public enum c {
        INITIALIZING,
        NORMAL,
        DEFUNCT
    }

    static {
        boolean z2;
        if (!a.class.desiredAssertionStatus()) {
            z2 = true;
        } else {
            z2 = false;
        }
        S = z2;
    }

    static /* synthetic */ int m() {
        int i2 = l;
        l = i2 + 1;
        return i2;
    }

    public static final long a(C0018a aVar, C0018a aVar2) {
        long a2 = (long) (aVar.r + aVar2.r);
        return ((a2 * (1 + a2)) / 2) + ((long) (aVar.r < aVar2.r ? aVar.r : aVar2.r));
    }

    public static void a() {
        l = 0;
    }

    /* renamed from: com.ptowngames.jigsaur.a$a  reason: collision with other inner class name */
    class C0018a extends i {
        private static /* synthetic */ boolean K = (!a.class.desiredAssertionStatus());
        private boolean A;
        private Hashtable<Integer, MouseJoint> B;
        private HashSet<d> C;
        /* access modifiers changed from: private */
        public int D;
        private float E;
        private boolean F;
        private int G;
        private final float H;
        private final float I;
        private float J;
        al f;
        s g;
        g h;
        d i;
        x j;
        s[] k;
        public boolean l;
        protected float m;
        C0019a n;
        protected float o;
        protected b p;
        protected int q;
        /* access modifiers changed from: private */
        public final int r;
        private c s;
        private ArrayList<s.a> t;
        private HashSet<Integer> u;
        private Body v;
        private boolean w;
        private float x;
        private float y;
        /* access modifiers changed from: private */
        public long z;

        /* synthetic */ C0018a(a aVar, C0018a aVar2, C0018a aVar3) {
            this(aVar, aVar2, aVar3, (byte) 0);
        }

        /* synthetic */ C0018a(a aVar, boolean z2) {
            this(z2, (byte) 0);
        }

        static /* synthetic */ C0018a c(C0018a aVar) {
            if (aVar.s != c.DEFUNCT) {
                if (K || aVar.s == c.NORMAL) {
                    aVar.s = c.DEFUNCT;
                    aVar.D();
                    aVar.E();
                    Iterator<j> it = a.this.m.iterator();
                    while (it.hasNext()) {
                        long a = a.a(aVar, (C0018a) it.next());
                        if (a.this.x.containsKey(Long.valueOf(a))) {
                            a.this.x.remove(Long.valueOf(a));
                        }
                    }
                    aVar.z = 0;
                    a.this.a(aVar.f);
                    if (K || !aVar.f.r()) {
                        if (a.this.g.contains(aVar)) {
                            a.this.g.remove(aVar);
                        }
                        boolean unused = a.this.t = true;
                        if (!aVar.w) {
                            if (K || a.this.m.contains(aVar)) {
                                a.this.m.remove(aVar);
                            } else {
                                throw new AssertionError();
                            }
                        }
                        c.a(aVar, c.b.GLOB_WENT_DEFUNCT, aVar);
                    } else {
                        throw new AssertionError();
                    }
                } else {
                    throw new AssertionError();
                }
            }
            return aVar;
        }

        public final boolean p() {
            return this.w;
        }

        public final Body q() {
            return this.v;
        }

        public final void a(Body body) {
            this.v = body;
            this.v.setUserData(this);
            if (this.s != c.INITIALIZING) {
                b(this.G);
            }
            if (this.w) {
                body.setActive(false);
            }
        }

        public final long r() {
            return this.z;
        }

        public final void s() {
            if (this.v != null && this.v.isAwake()) {
                this.z = System.currentTimeMillis();
                float angle = this.v.getAngle();
                g worldPoint = this.v.getWorldPoint(new g(0.0f, 0.0f));
                worldPoint.a -= this.b.a;
                worldPoint.b -= this.b.b;
                if (Math.abs(this.e - angle) >= 0.001f || worldPoint.c() >= 1.0E-6f) {
                    a(this.b.a + worldPoint.a, worldPoint.b + this.b.b, this.b.c, angle);
                }
            }
        }

        private void B() {
            if (!this.A) {
                if (this.v != null) {
                    f j2 = j();
                    this.v.setTransform(new g(j2.a, j2.b), super.k());
                    if (!this.v.isAwake()) {
                        this.v.setAwake(true);
                    }
                }
                this.l = true;
            }
        }

        public final boolean a(int i2, com.badlogic.gdx.math.a.a aVar) {
            return a(i2, u.a(aVar, j().c));
        }

        public final boolean a(int i2, g gVar) {
            if (K || !this.B.containsKey(Integer.valueOf(i2))) {
                MouseJointDef mouseJointDef = new MouseJointDef();
                mouseJointDef.maxForce = 100000.0f * this.v.getMass();
                mouseJointDef.bodyA = a.this.a;
                mouseJointDef.bodyB = this.v;
                mouseJointDef.target.a = gVar.a;
                mouseJointDef.target.b = gVar.b;
                this.B.put(Integer.valueOf(i2), (MouseJoint) a.this.b.createJoint(mouseJointDef));
                this.v.setFixedRotation(this.B.size() == 1);
                b(i2, gVar);
                this.J = 0.0f;
                return true;
            }
            throw new AssertionError();
        }

        public final void a(int i2) {
            if (this.B.containsKey(Integer.valueOf(i2))) {
                a.this.b.destroyJoint(this.B.remove(Integer.valueOf(i2)));
            }
            this.v.setFixedRotation(this.B.size() == 1);
            this.l = true;
            if (this.B.size() == 0) {
                this.J = 0.0f;
            }
        }

        public final void b(int i2, com.badlogic.gdx.math.a.a aVar) {
            b(i2, u.a(aVar, j().c));
        }

        private void b(int i2, g gVar) {
            if (K || this.B.containsKey(Integer.valueOf(i2))) {
                this.B.get(Integer.valueOf(i2)).setTarget(gVar);
                return;
            }
            throw new AssertionError();
        }

        public final float k() {
            return super.k();
        }

        public final void n() {
            super.n();
            B();
        }

        public final void o() {
            float f2;
            float f3 = 1.0f;
            float f4 = -1.0f;
            if (this.c == null || this.d == null || this.v == null) {
                super.o();
                return;
            }
            if (this.b.c < this.c.c) {
                this.b.c = this.c.c;
            }
            if (this.b.c > this.d.c) {
                this.b.c = this.d.c;
            }
            if (this.b.a < this.c.a) {
                f2 = 1.0f;
            } else {
                f2 = 0.0f;
            }
            if (this.b.b >= this.c.b) {
                f3 = 0.0f;
            }
            if (this.b.a > this.d.a) {
                f2 = -1.0f;
            }
            if (this.b.b <= this.d.b) {
                f4 = f3;
            }
            if (f2 != 0.0f || f4 != 0.0f) {
                this.v.applyForce(new g(f2 * b(), f4 * b()), this.v.getWorldCenter());
            }
        }

        public final void b(float f2, float f3, float f4, float f5) {
            super.b(f2, f3, f4, f5);
        }

        private C0018a(boolean z2, byte b) {
            this.r = a.m();
            this.g = null;
            this.h = null;
            this.k = new s[0];
            this.x = 0.001f;
            this.y = 1.0E-6f;
            this.z = 0;
            this.A = false;
            this.B = new Hashtable<>();
            this.C = new HashSet<>();
            this.l = false;
            this.m = 1.0f;
            this.D = 16;
            this.n = null;
            this.E = 0.5f;
            this.o = 0.5f;
            this.F = false;
            this.p = null;
            this.q = 0;
            this.G = -123;
            this.H = 0.0f;
            this.I = 0.0f;
            this.J = 0.0f;
            if (K || this.r >= 0) {
                this.s = c.INITIALIZING;
                this.t = new ArrayList<>();
                this.u = new HashSet<>();
                this.w = z2;
                if (!this.w) {
                    a.this.m.add(this);
                }
                boolean unused = a.this.t = true;
                this.f = al.b(this);
                this.f.a(false);
                this.j = new x();
                this.e = 0.0f;
                return;
            }
            throw new AssertionError();
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private C0018a(com.ptowngames.jigsaur.a r5, com.ptowngames.jigsaur.a.C0018a r6, com.ptowngames.jigsaur.a.C0018a r7, byte r8) {
            /*
                r4 = this;
                r1 = 0
                boolean r0 = r6.w
                if (r0 != 0) goto L_0x0009
                boolean r0 = r7.w
                if (r0 == 0) goto L_0x001b
            L_0x0009:
                r0 = 1
            L_0x000a:
                r4.<init>(r0, r1)
                boolean r0 = com.ptowngames.jigsaur.a.C0018a.K
                if (r0 != 0) goto L_0x001d
                com.ptowngames.jigsaur.a$a r0 = r5.h
                if (r6 != r0) goto L_0x001d
                java.lang.AssertionError r0 = new java.lang.AssertionError
                r0.<init>()
                throw r0
            L_0x001b:
                r0 = r1
                goto L_0x000a
            L_0x001d:
                boolean r0 = com.ptowngames.jigsaur.a.C0018a.K
                if (r0 != 0) goto L_0x002b
                com.badlogic.gdx.physics.box2d.Body r0 = r6.v
                if (r0 != 0) goto L_0x002b
                java.lang.AssertionError r0 = new java.lang.AssertionError
                r0.<init>()
                throw r0
            L_0x002b:
                com.ptowngames.jigsaur.a$a r0 = r4.f(r6)
                r0.f(r7)
                com.ptowngames.jigsaur.x r0 = r4.j
                com.badlogic.gdx.math.f r0 = r0.c()
                java.util.ArrayList r1 = new java.util.ArrayList
                com.ptowngames.jigsaur.x r2 = r6.j
                int r2 = r2.size()
                com.ptowngames.jigsaur.x r3 = r7.j
                int r3 = r3.size()
                int r2 = r2 + r3
                r1.<init>(r2)
                com.badlogic.gdx.physics.box2d.Body r2 = r6.v
                if (r2 == 0) goto L_0x0057
                com.badlogic.gdx.physics.box2d.Body r2 = r6.v
                java.util.ArrayList r2 = a(r2, r0)
                r1.addAll(r2)
            L_0x0057:
                com.badlogic.gdx.physics.box2d.Body r2 = r7.v
                if (r2 == 0) goto L_0x0064
                com.badlogic.gdx.physics.box2d.Body r2 = r7.v
                java.util.ArrayList r0 = a(r2, r0)
                r1.addAll(r0)
            L_0x0064:
                com.badlogic.gdx.physics.box2d.Body r0 = r6.v
                r4.v = r0
                com.badlogic.gdx.physics.box2d.Body r0 = r4.v
                r0.setUserData(r4)
                com.badlogic.gdx.physics.box2d.Body r0 = r7.v
                if (r0 == 0) goto L_0x0078
                com.badlogic.gdx.physics.box2d.World r0 = r5.b
                com.badlogic.gdx.physics.box2d.Body r2 = r7.v
                r0.destroyBody(r2)
            L_0x0078:
                com.badlogic.gdx.physics.box2d.Body r0 = r4.v
                java.util.ArrayList r0 = r0.getFixtureList()
                java.lang.Object r0 = r0.clone()
                java.util.ArrayList r0 = (java.util.ArrayList) r0
                java.util.Iterator r2 = r0.iterator()
            L_0x0088:
                boolean r0 = r2.hasNext()
                if (r0 == 0) goto L_0x009a
                java.lang.Object r0 = r2.next()
                com.badlogic.gdx.physics.box2d.Fixture r0 = (com.badlogic.gdx.physics.box2d.Fixture) r0
                com.badlogic.gdx.physics.box2d.Body r3 = r4.v
                r3.destroyFixture(r0)
                goto L_0x0088
            L_0x009a:
                java.util.Iterator r1 = r1.iterator()
            L_0x009e:
                boolean r0 = r1.hasNext()
                if (r0 == 0) goto L_0x00b5
                java.lang.Object r0 = r1.next()
                com.badlogic.gdx.physics.box2d.FixtureDef r0 = (com.badlogic.gdx.physics.box2d.FixtureDef) r0
                com.badlogic.gdx.physics.box2d.Body r2 = r4.v
                r2.createFixture(r0)
                com.badlogic.gdx.physics.box2d.Shape r0 = r0.shape
                r0.dispose()
                goto L_0x009e
            L_0x00b5:
                float r0 = r6.e
                r4.e = r0
                r4.F()
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ptowngames.jigsaur.a.C0018a.<init>(com.ptowngames.jigsaur.a, com.ptowngames.jigsaur.a$a, com.ptowngames.jigsaur.a$a, byte):void");
        }

        private static ArrayList<FixtureDef> a(Body body, f fVar) {
            ArrayList<FixtureDef> arrayList = new ArrayList<>();
            g localPoint = body.getLocalPoint(new g(fVar.a, fVar.b));
            localPoint.a(-1.0f);
            Iterator<Fixture> it = body.getFixtureList().iterator();
            while (it.hasNext()) {
                FixtureDef i2 = a.i();
                PolygonShape polygonShape = (PolygonShape) it.next().getShape();
                g[] gVarArr = new g[polygonShape.getVertexCount()];
                for (int i3 = 0; i3 < gVarArr.length; i3++) {
                    gVarArr[i3] = new g();
                    polygonShape.getVertex(i3, gVarArr[i3]);
                    gVarArr[i3].c(localPoint);
                }
                f.c c = f.b().c();
                c.set(gVarArr);
                c.setRadius(0.0f);
                i2.shape = c;
                arrayList.add(i2);
            }
            return arrayList;
        }

        public final Jigsaur.PieceGlobConfiguration t() {
            Jigsaur.PieceGlobConfiguration.Builder newBuilder = Jigsaur.PieceGlobConfiguration.newBuilder();
            Iterator<Integer> it = this.u.iterator();
            while (it.hasNext()) {
                newBuilder.addPieceIds(it.next().intValue());
            }
            com.badlogic.gdx.math.f a = a.a(j());
            Jigsaur.Vertex3.Builder newBuilder2 = Jigsaur.Vertex3.newBuilder();
            newBuilder2.setX(a.a);
            newBuilder2.setY(a.b);
            newBuilder2.setZ(a.c);
            newBuilder.setCenter(newBuilder2.build());
            newBuilder.setAngle(this.e);
            if (this.w) {
                newBuilder.setState(Jigsaur.PieceGlobConfiguration.a.SOLVED);
            } else if (A()) {
                newBuilder.setState(Jigsaur.PieceGlobConfiguration.a.RAISED);
            } else {
                newBuilder.setState(Jigsaur.PieceGlobConfiguration.a.NORMAL);
            }
            return newBuilder.build();
        }

        private d e(C0018a aVar) {
            C0018a aVar2 = aVar;
            C0018a aVar3 = this;
            while (true) {
                if (!K && aVar3.s != c.NORMAL) {
                    throw new AssertionError();
                } else if (!K && aVar2.s != c.NORMAL) {
                    throw new AssertionError();
                } else if (aVar3.t.size() > aVar2.t.size()) {
                    C0018a aVar4 = aVar3;
                    aVar3 = aVar2;
                    aVar2 = aVar4;
                } else {
                    Iterator<s.a> it = aVar3.t.iterator();
                    while (it.hasNext()) {
                        s.a next = it.next();
                        Iterator<s.a> it2 = aVar2.t.iterator();
                        while (true) {
                            if (it2.hasNext()) {
                                s.a next2 = it2.next();
                                if (next.b == s.this.s() && next2.b == s.this.s()) {
                                    return new d(aVar3, aVar2, next, next2);
                                }
                            }
                        }
                    }
                    return null;
                }
            }
        }

        public final void u() {
            this.l = false;
            D();
            for (C0018a aVar : a.this.s) {
                if (aVar != this) {
                    Iterator<s.a> it = this.t.iterator();
                    while (it.hasNext()) {
                        s.a next = it.next();
                        Iterator<s.a> it2 = aVar.t.iterator();
                        while (it2.hasNext()) {
                            s.a next2 = it2.next();
                            d dVar = new d(this, aVar, next, next2);
                            if (next.a != next2.a && dVar.a()) {
                                a(dVar);
                                aVar.a(dVar);
                            }
                        }
                    }
                }
            }
            if (!this.C.contains(this.i) && this.i != null && this.i.a()) {
                a(this.i);
                a.this.h.a(this.i);
            }
        }

        private void a(d dVar) {
            if (K || !this.C.contains(dVar)) {
                this.C.add(dVar);
                a(true);
                return;
            }
            throw new AssertionError();
        }

        /* renamed from: com.ptowngames.jigsaur.a$a$a  reason: collision with other inner class name */
        class C0019a extends com.ptowngames.jigsaur.a.c {
            private float b;

            public C0019a(float f) {
                super(0, 0.0f);
                this.b = f;
            }

            public final void a(float f) {
                C0018a.this.o = f;
            }

            public final void a() {
                a(C0018a.this.o, this.b);
                b((long) ((1000.0f * Math.abs(this.b - C0018a.this.o)) / 1.6f));
                super.a();
            }

            public final void b() {
                super.b();
                if (this == C0018a.this.n) {
                    C0018a.this.n = (C0019a) g();
                }
            }
        }

        public final void a(float f2, boolean z2) {
            if (K || (f2 >= 0.1f && f2 <= 0.9f)) {
                this.E = f2;
                if (this.o != this.E || this.n != null) {
                    if (this.n != null && !z2) {
                        C();
                    }
                    if (this.n == null) {
                        this.n = new C0019a(this.E);
                        d.a().a(this.n);
                    } else if (K || (z2 && this.n != null)) {
                        C0019a aVar = new C0019a(this.E);
                        this.n.a(aVar);
                        this.n = aVar;
                    } else {
                        throw new AssertionError();
                    }
                }
            } else {
                throw new AssertionError();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.jigsaur.a.a.a(float, boolean):void
         arg types: [int, int]
         candidates:
          com.ptowngames.jigsaur.a.a.a(com.ptowngames.jigsaur.a$a, int):int
          com.ptowngames.jigsaur.a.a.a(com.ptowngames.jigsaur.a$a, long):long
          com.ptowngames.jigsaur.a.a.a(com.ptowngames.jigsaur.a$a, com.ptowngames.jigsaur.s):com.ptowngames.jigsaur.a$a
          com.ptowngames.jigsaur.a.a.a(com.badlogic.gdx.physics.box2d.Body, com.badlogic.gdx.math.f):java.util.ArrayList<com.badlogic.gdx.physics.box2d.FixtureDef>
          com.ptowngames.jigsaur.a.a.a(int, com.badlogic.gdx.math.a.a):boolean
          com.ptowngames.jigsaur.a.a.a(int, com.badlogic.gdx.math.g):boolean
          com.ptowngames.jigsaur.i.a(com.ptowngames.jigsaur.x, float):void
          com.ptowngames.jigsaur.a.a.a(float, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.jigsaur.a.a.a(float, boolean):void
         arg types: [float, int]
         candidates:
          com.ptowngames.jigsaur.a.a.a(com.ptowngames.jigsaur.a$a, int):int
          com.ptowngames.jigsaur.a.a.a(com.ptowngames.jigsaur.a$a, long):long
          com.ptowngames.jigsaur.a.a.a(com.ptowngames.jigsaur.a$a, com.ptowngames.jigsaur.s):com.ptowngames.jigsaur.a$a
          com.ptowngames.jigsaur.a.a.a(com.badlogic.gdx.physics.box2d.Body, com.badlogic.gdx.math.f):java.util.ArrayList<com.badlogic.gdx.physics.box2d.FixtureDef>
          com.ptowngames.jigsaur.a.a.a(int, com.badlogic.gdx.math.a.a):boolean
          com.ptowngames.jigsaur.a.a.a(int, com.badlogic.gdx.math.g):boolean
          com.ptowngames.jigsaur.i.a(com.ptowngames.jigsaur.x, float):void
          com.ptowngames.jigsaur.a.a.a(float, boolean):void */
        public final void v() {
            float f2 = this.E;
            a(0.9f, false);
            a(f2, true);
        }

        public final void w() {
            C();
            C0019a aVar = new C0019a(0.9f);
            C0019a aVar2 = new C0019a(0.5f);
            aVar.a(aVar2);
            aVar2.a(aVar);
            this.n = aVar;
            d.a().a(this.n);
        }

        private void C() {
            if (this.n != null) {
                this.n.a((b) null);
                this.n.b(false);
                d.a().c(this.n);
                this.n = null;
            }
        }

        public final void c(float f2) {
            for (s c : this.k) {
                c.c(f2);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.jigsaur.a.a.a(float, boolean):void
         arg types: [float, int]
         candidates:
          com.ptowngames.jigsaur.a.a.a(com.ptowngames.jigsaur.a$a, int):int
          com.ptowngames.jigsaur.a.a.a(com.ptowngames.jigsaur.a$a, long):long
          com.ptowngames.jigsaur.a.a.a(com.ptowngames.jigsaur.a$a, com.ptowngames.jigsaur.s):com.ptowngames.jigsaur.a$a
          com.ptowngames.jigsaur.a.a.a(com.badlogic.gdx.physics.box2d.Body, com.badlogic.gdx.math.f):java.util.ArrayList<com.badlogic.gdx.physics.box2d.FixtureDef>
          com.ptowngames.jigsaur.a.a.a(int, com.badlogic.gdx.math.a.a):boolean
          com.ptowngames.jigsaur.a.a.a(int, com.badlogic.gdx.math.g):boolean
          com.ptowngames.jigsaur.i.a(com.ptowngames.jigsaur.x, float):void
          com.ptowngames.jigsaur.a.a.a(float, boolean):void */
        private void a(boolean z2) {
            this.F = z2;
            a(this.F ? 0.72f : 0.5f, false);
        }

        public final boolean x() {
            return this.C.size() > 0;
        }

        private void D() {
            a(false);
            Iterator<d> it = this.C.iterator();
            while (it.hasNext()) {
                d next = it.next();
                if (K || next != null) {
                    C0018a a = next.a(this);
                    boolean remove = a.C.remove(next);
                    if (!K && !remove) {
                        throw new AssertionError();
                    } else if (a.C.size() == 0) {
                        a.a(false);
                    }
                } else {
                    throw new AssertionError();
                }
            }
            this.C.clear();
            a(false);
        }

        public final C0018a y() {
            if (K || x()) {
                HashSet<C0018a> hashSet = new HashSet<>();
                Iterator<d> it = this.C.iterator();
                while (it.hasNext()) {
                    d next = it.next();
                    d dVar = (d) a.this.x.get(Long.valueOf(a.a(this, next.a(this))));
                    if (dVar != null && dVar.a()) {
                        hashSet.add(next.a(this));
                    }
                }
                if (this.i.a()) {
                    hashSet.add(a.this.h);
                }
                if (hashSet.size() == 0) {
                    D();
                    d.a().a(new n(this));
                    return null;
                }
                "executeJoinOpportunity found " + hashSet.size() + " additional globs to actually join";
                boolean A2 = A();
                C0018a aVar = this;
                boolean A3 = A();
                for (C0018a aVar2 : hashSet) {
                    A3 &= aVar2.A();
                    A2 |= aVar2.A();
                    aVar = a.this.b(aVar, aVar2);
                }
                if (aVar.w) {
                    for (s sVar : aVar.k) {
                        sVar.n = -1;
                    }
                    if (!K && aVar != a.this.h) {
                        throw new AssertionError();
                    } else if (!K && aVar.A()) {
                        throw new AssertionError();
                    } else if (K || this.s == c.DEFUNCT) {
                        return aVar;
                    } else {
                        throw new AssertionError();
                    }
                } else {
                    if (!A2 || A3) {
                        if (A3) {
                            a.this.a(aVar);
                            if (!K && !aVar.A()) {
                                throw new AssertionError();
                            }
                        }
                    } else if (aVar.A()) {
                        aVar.z();
                    } else {
                        for (s sVar2 : aVar.k) {
                            sVar2.n = -1;
                        }
                    }
                    if (K || this.s == c.DEFUNCT) {
                        return aVar;
                    }
                    throw new AssertionError();
                }
            } else {
                throw new AssertionError();
            }
        }

        /* access modifiers changed from: private */
        public C0018a a(s sVar) {
            if (!K && this.s != c.INITIALIZING) {
                throw new AssertionError();
            } else if (K || !this.j.contains(sVar)) {
                a.this.f.put(sVar, this);
                this.u.add(Integer.valueOf(sVar.s()));
                for (s.a add : sVar.p()) {
                    this.t.add(add);
                }
                this.j.add(sVar);
                return this;
            } else {
                throw new AssertionError();
            }
        }

        private C0018a f(C0018a aVar) {
            if (K || this.s == c.INITIALIZING) {
                Iterator<j> it = aVar.j.iterator();
                while (it.hasNext()) {
                    s sVar = (s) it.next();
                    if (!this.j.contains(sVar)) {
                        a(sVar);
                    }
                }
                return this;
            }
            throw new AssertionError();
        }

        private void E() {
            if (K || this.s == c.DEFUNCT) {
                Iterator<j> it = this.j.iterator();
                while (it.hasNext()) {
                    s sVar = (s) it.next();
                    if (a.this.f.get(sVar) == this) {
                        a.this.f.remove(sVar);
                    }
                }
                this.j.clear();
                this.u.clear();
                this.t.clear();
                return;
            }
            throw new AssertionError();
        }

        /* access modifiers changed from: private */
        public C0018a F() {
            float f2;
            if (K || this.s == c.INITIALIZING) {
                a(this.j, this.e);
                com.badlogic.gdx.math.f fVar = this.b;
                if (this.w) {
                    f2 = a.this.J.a() + 0.002f;
                } else {
                    f2 = this.b.c;
                }
                fVar.c = f2;
                this.s = c.NORMAL;
                ArrayList arrayList = new ArrayList();
                Iterator<s.a> it = this.t.iterator();
                while (it.hasNext()) {
                    s.a next = it.next();
                    if (this.u.contains(Integer.valueOf(next.b))) {
                        arrayList.add(next);
                    }
                }
                this.t.removeAll(arrayList);
                Iterator<j> it2 = a.this.m.iterator();
                while (it2.hasNext()) {
                    C0018a aVar = (C0018a) it2.next();
                    d e = e(aVar);
                    if (e != null) {
                        long a = a.a(this, aVar);
                        if (K || !a.this.x.containsKey(Long.valueOf(a))) {
                            a.this.x.put(Long.valueOf(a), e);
                        } else {
                            throw new AssertionError();
                        }
                    }
                }
                Jigsaur.Level h2 = o.a().h();
                a(new com.badlogic.gdx.math.f(h2.getMinX(), h2.getMinY(), a.this.J.a()));
                b(new com.badlogic.gdx.math.f(h2.getMaxX(), h2.getMaxY(), -4.85f));
                if (K || this.v != null || this.j.size() == 0) {
                    if (this.v != null) {
                        B();
                        if (this.w) {
                            this.v.setActive(false);
                        }
                    }
                    b(this.b.c > -4.9f ? 1 : 0);
                    this.k = (s[]) this.j.toArray(this.k);
                    boolean unused = a.this.t = true;
                    if (this.k.length > 0) {
                        this.g = this.k[0];
                        this.h = a.this.J.a(this.g.q());
                        this.i = new d(this);
                    } else {
                        this.h = null;
                        this.i = null;
                    }
                    c.a(this, c.b.GLOB_WAS_CREATED, this);
                    return this;
                }
                throw new AssertionError();
            }
            throw new AssertionError();
        }

        public final synchronized void z() {
            float f2;
            b(1 - this.G);
            if (this.q != 0) {
                if (K || this.p != null) {
                    d.a().c(this.p);
                    if (!K && this.p != null) {
                        throw new AssertionError();
                    }
                } else {
                    throw new AssertionError();
                }
            }
            float f3 = this.b.c;
            if (!A()) {
                f2 = -5.0f;
                this.q = -1;
            } else {
                f2 = -4.85f;
                this.q = 1;
            }
            long j2 = (long) (((((float) this.q) * (f2 - f3)) / 4.0f) * 1000.0f);
            if (K || this.p == null) {
                this.p = new b(j2, f3, f2) {
                    public final void a(float f) {
                        C0018a.this.b.c = f;
                        C0018a.this.m();
                    }

                    public final void b() {
                        C0018a.this.p = null;
                        C0018a.this.q = 0;
                    }
                };
                d.a().a(this.p);
            } else {
                throw new AssertionError();
            }
        }

        public final boolean A() {
            return this.G == 1;
        }

        private void b(int i2) {
            int i3 = 0;
            if (K || i2 == 0 || i2 == 1) {
                this.G = i2;
                if (i2 == 0) {
                    s[] sVarArr = this.k;
                    int length = sVarArr.length;
                    while (i3 < length) {
                        sVarArr[i3].n = -1;
                        i3++;
                    }
                } else {
                    s[] sVarArr2 = this.k;
                    int length2 = sVarArr2.length;
                    while (i3 < length2) {
                        sVarArr2[i3].n = System.currentTimeMillis();
                        i3++;
                    }
                }
                if (this.v != null) {
                    short s2 = (short) (i2 + 1);
                    Iterator<Fixture> it = this.v.getFixtureList().iterator();
                    while (it.hasNext()) {
                        Fixture next = it.next();
                        Filter filterData = next.getFilterData();
                        filterData.maskBits = s2;
                        filterData.categoryBits = s2;
                        next.setFilterData(filterData);
                    }
                    this.v.setAwake(true);
                    this.v.setLinearDamping(30.0f);
                    this.v.setAngularDamping(30.0f);
                    return;
                }
                return;
            }
            throw new AssertionError();
        }

        public final float a(g gVar) {
            float a = super.a(gVar);
            if (this.J == 0.0f) {
                return a;
            }
            com.badlogic.gdx.math.f j2 = j();
            float b = gVar.a().b(new g(j2.a, j2.b)).b();
            return (float) Math.min((double) a, b > this.J ? Math.pow((double) (b - this.J), 2.0d) : 0.0d);
        }

        public final void a(aa aaVar) {
            if (K || !a.this.k) {
                this.j.a(aaVar);
                if (this.J > 0.0f) {
                    aaVar.a();
                    aaVar.a(0.1f, 0.1f, 0.9f, 0.5f);
                    aaVar.a(j());
                    aaVar.b(this.J);
                    aaVar.b();
                    return;
                }
                return;
            }
            throw new AssertionError();
        }
    }

    private class d {
        private static /* synthetic */ boolean f = (!a.class.desiredAssertionStatus());
        private boolean a;
        private C0018a b;
        private C0018a c;
        private s.a d;
        private s.a e;

        public d(C0018a aVar) {
            this.b = aVar;
            this.c = null;
            this.d = null;
            this.e = null;
            this.a = true;
        }

        public d(C0018a aVar, C0018a aVar2, s.a aVar3, s.a aVar4) {
            this.b = aVar;
            this.c = aVar2;
            this.d = aVar3;
            this.e = aVar4;
            this.a = false;
        }

        public final C0018a a(C0018a aVar) {
            if (this.a && aVar != a.this.h) {
                return a.this.h;
            }
            if (aVar == this.b) {
                return this.c;
            }
            if (aVar == this.c) {
                return this.b;
            }
            if (f) {
                return null;
            }
            throw new AssertionError();
        }

        public final com.badlogic.gdx.math.f b(C0018a aVar) {
            com.badlogic.gdx.math.f fVar;
            if (f || aVar == this.b || aVar == this.c) {
                if (!this.a) {
                    fVar = new com.badlogic.gdx.math.f(this.d.a() - this.e.a(), this.d.b() - this.e.b(), s.this.j().c - s.this.j().c);
                } else if (f || this.b.g != null) {
                    fVar = this.b.g.j();
                    fVar.c(this.b.h.a, this.b.h.b, a.this.h.b.c);
                } else {
                    throw new AssertionError();
                }
                if (aVar != this.c) {
                    fVar.d(-1.0f, -1.0f, -1.0f);
                }
                return fVar;
            }
            throw new AssertionError();
        }

        public final float c(C0018a aVar) {
            float c2;
            if (f || aVar == this.b || aVar == this.c) {
                float f2 = aVar == this.c ? -1.0f : 1.0f;
                if (this.a) {
                    c2 = (f2 * (3.1415927f - this.b.k())) - 3.1415927f;
                } else {
                    c2 = f2 * ((this.e.c() - this.d.c()) - 3.1415927f);
                }
                return u.a(c2);
            }
            throw new AssertionError();
        }

        public final boolean a() {
            if (((double) Math.abs(c(this.b))) < 0.5235987755982988d) {
                float d2 = b(this.b).d(1.0f, 1.0f, 0.0f).d();
                float f2 = a.this.e;
                ar.a();
                if (d2 < f2 * 2.0f) {
                    return true;
                }
            }
            return false;
        }

        public final String toString() {
            return "{JoinDelta " + this.b.r + " : " + this.c.r + " : " + this.d + " : " + this.e;
        }
    }

    public final x b() {
        return this.o;
    }

    public final g c() {
        g d2 = this.o.d();
        d2.a(this.h.d().d());
        return d2;
    }

    public final void d() {
        if (this.p != null) {
            Iterator<com.ptowngames.jigsaur.a.c> it = this.p.iterator();
            while (it.hasNext()) {
                com.ptowngames.jigsaur.a.c next = it.next();
                next.a((b) null);
                next.b(false);
                d.a().c(next);
            }
            this.p.clear();
            this.p = null;
        }
    }

    public final void e() {
        for (C0018a w2 : this.s) {
            w2.w();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.ptowngames.jigsaur.a.a.a(float, boolean):void
     arg types: [int, int]
     candidates:
      com.ptowngames.jigsaur.a.a.a(com.ptowngames.jigsaur.a$a, int):int
      com.ptowngames.jigsaur.a.a.a(com.ptowngames.jigsaur.a$a, long):long
      com.ptowngames.jigsaur.a.a.a(com.ptowngames.jigsaur.a$a, com.ptowngames.jigsaur.s):com.ptowngames.jigsaur.a$a
      com.ptowngames.jigsaur.a.a.a(com.badlogic.gdx.physics.box2d.Body, com.badlogic.gdx.math.f):java.util.ArrayList<com.badlogic.gdx.physics.box2d.FixtureDef>
      com.ptowngames.jigsaur.a.a.a(int, com.badlogic.gdx.math.a.a):boolean
      com.ptowngames.jigsaur.a.a.a(int, com.badlogic.gdx.math.g):boolean
      com.ptowngames.jigsaur.i.a(com.ptowngames.jigsaur.x, float):void
      com.ptowngames.jigsaur.a.a.a(float, boolean):void */
    public final void f() {
        for (C0018a a2 : this.s) {
            a2.a(0.5f, false);
        }
    }

    public final void b(w wVar) {
        if (this.t) {
            this.r = new s[0];
            this.s = new C0018a[(this.m.size() + 1)];
            this.m.toArray(this.s);
            if (S || this.s[this.s.length - 1] == null) {
                this.s[this.s.length - 1] = this.h;
                this.r = (s[]) this.q.toArray(this.r);
                this.t = false;
            } else {
                throw new AssertionError();
            }
        }
        s.a(this.r, wVar, this.J.a());
    }

    public final void a(aa aaVar, com.badlogic.gdx.graphics.b bVar, float f2) {
        i b2 = com.badlogic.gdx.g.b.b();
        b2.a(1.0f, 1.0f, 1.0f, 1.0f);
        b2.glEnable(3553);
        bVar.a();
        s.m.d();
        i iVar = com.badlogic.gdx.g.g;
        iVar.b();
        iVar.b(3042.0f);
        float f3 = -1.0f;
        float f4 = -1.0f;
        for (C0018a aVar : this.s) {
            if (!(f4 == aVar.o && f3 == aVar.m)) {
                f4 = aVar.o;
                f3 = aVar.m;
                float f5 = 2.0f * f4;
                if (f4 < 0.5f) {
                    float[] fArr = v;
                    float[] fArr2 = v;
                    v[2] = f5;
                    fArr2[1] = f5;
                    fArr[0] = f5;
                    iVar.b(v);
                    iVar.a(0.0f, 0.0f, 0.0f, 0.0f);
                } else {
                    iVar.b(u);
                    float f6 = f5 - 1.0f;
                    iVar.a(f6, f6, f6, 1.0f);
                }
            }
            for (s sVar : aVar.k) {
                if (!sVar.o) {
                    iVar.a(sVar.a.a);
                    s.m.a(5, sVar.g, sVar.f);
                }
            }
        }
        iVar.b(8448.0f);
        iVar.a();
        ar.a();
        i iVar2 = com.badlogic.gdx.g.g;
        s.l.a();
        iVar2.b();
        int length = this.s.length - 1;
        float f7 = Float.POSITIVE_INFINITY;
        for (int i2 = 0; i2 < length; i2++) {
            C0018a aVar2 = this.s[i2];
            if (f7 != aVar2.m) {
                f7 = aVar2.m;
                iVar2.a(1.0f, 1.0f, 1.0f, aVar2.m);
            }
            for (s sVar2 : aVar2.k) {
                if (!sVar2.o) {
                    iVar2.a(sVar2.a.a);
                    s.m.a(5, sVar2.j, sVar2.k);
                }
            }
        }
        iVar2.a();
        b2.glDisable(3553);
        ar.a();
        i iVar3 = com.badlogic.gdx.g.g;
        iVar3.b();
        int length2 = this.s.length;
        int i3 = -1;
        for (int i4 = 0; i4 < length2; i4++) {
            C0018a aVar3 = this.s[i4];
            if (aVar3.D != i3) {
                i3 = aVar3.D;
                iVar3.a(((float) ((i3 >> 24) & 255)) / 255.0f, ((float) ((i3 >> 16) & 255)) / 255.0f, ((float) ((i3 >> 8) & 255)) / 255.0f, 1.0f);
                iVar3.glLineWidth(((float) (i3 & 255)) / 16.0f);
            }
            for (s sVar3 : aVar3.k) {
                if (!sVar3.o) {
                    float f8 = sVar3.a.a[14];
                    float[] fArr3 = sVar3.a.a;
                    fArr3[14] = fArr3[14] + 0.002f;
                    iVar3.a(sVar3.a.a);
                    sVar3.a.a[14] = f8;
                    s.m.a(3, sVar3.h, sVar3.i);
                }
            }
        }
        iVar3.a();
        b2.glEnable(3042);
        i iVar4 = com.badlogic.gdx.g.g;
        iVar4.glBlendFunc(770, 771);
        iVar4.glDepthMask(false);
        iVar4.b();
        iVar4.a(0.0f, 0.0f, 0.0f, 0.4f);
        for (C0018a aVar4 : this.s) {
            float a2 = this.J.a() + 0.002f;
            if (!aVar4.p()) {
                if (aVar4.A()) {
                    a2 = -4.998f;
                }
                for (s sVar4 : aVar4.k) {
                    if (!sVar4.o) {
                        float f9 = sVar4.a.a[14];
                        float f10 = (f9 - a2) * f2;
                        float[] fArr4 = sVar4.a.a;
                        fArr4[12] = fArr4[12] - (1.0f * f10);
                        float[] fArr5 = sVar4.a.a;
                        fArr5[13] = fArr5[13] - (0.0f * f10);
                        sVar4.a.a[14] = a2;
                        iVar4.a(sVar4.a.a);
                        s.m.a(5, sVar4.g, sVar4.f);
                        float[] fArr6 = sVar4.a.a;
                        fArr6[12] = fArr6[12] + (1.0f * f10);
                        float[] fArr7 = sVar4.a.a;
                        fArr7[13] = (f10 * 0.0f) + fArr7[13];
                        sVar4.a.a[14] = f9;
                    }
                }
            }
        }
        iVar4.a();
        iVar4.glDepthMask(true);
        b2.glBlendFunc(770, 771);
        b2.glDepthFunc(513);
        s.m.e();
        if (this.n == -1) {
            aaVar.a(1.0f, 1.0f, 1.0f, 0.5f);
        } else {
            aaVar.a(1.0f, 1.0f, 1.0f, 1.0f - (((float) Math.abs(((System.currentTimeMillis() - this.n) % 2000) - 1000)) / 1000.0f));
        }
        if (ar.a().c) {
            b2.glEnable(3553);
            bVar.a();
        }
        this.J.b();
        b2.glEnable(3553);
        aaVar.a(1.0f, 1.0f, 1.0f, 0.5f);
        com.badlogic.gdx.g.g.glDepthFunc(519);
        al.q().a();
        Iterator<j> it = this.B.iterator();
        while (it.hasNext()) {
            al alVar = (al) it.next();
            alVar.c(f2);
            alVar.a(aaVar);
        }
        if (this.i) {
            this.O.b(1.0f / f2);
            this.O.a(aaVar, this.j);
        }
        b2.glDisable(3553);
        b2.glDisable(3042);
        com.badlogic.gdx.g.g.glDepthFunc(513);
    }

    public final C0018a b(C0018a aVar, C0018a aVar2) {
        d dVar;
        C0018a aVar3 = aVar;
        while (aVar3 == this.h) {
            C0018a aVar4 = aVar3;
            aVar3 = aVar2;
            aVar2 = aVar4;
        }
        if (!S) {
            if (!((aVar3 == this.h || aVar2 == this.h) ? true : this.x.containsKey(Long.valueOf(a(aVar3, aVar2))))) {
                throw new AssertionError();
            }
        }
        if (!S && !d(aVar3)) {
            throw new AssertionError();
        } else if (S || d(aVar2)) {
            if (aVar2 == this.h) {
                dVar = aVar3.i;
            } else {
                dVar = this.x.get(Long.valueOf(a(aVar3, aVar2)));
            }
            if (S || dVar != null) {
                aVar3.a(aVar3.k() + dVar.c(aVar3));
                aVar3.c(aVar3.j().b(dVar.b(aVar3)));
                return c(aVar3, aVar2);
            }
            throw new AssertionError();
        } else {
            throw new AssertionError();
        }
    }

    private C0018a c(C0018a aVar, C0018a aVar2) {
        C0018a aVar3 = aVar;
        while (true) {
            if (!S && aVar3 == aVar2) {
                throw new AssertionError();
            } else if (aVar3 == this.h) {
                C0018a aVar4 = aVar3;
                aVar3 = aVar2;
                aVar2 = aVar4;
            } else {
                C0018a aVar5 = new C0018a(this, aVar3, aVar2);
                C0018a.c(aVar3);
                C0018a.c(aVar2);
                if (aVar5.p()) {
                    this.h = aVar5;
                }
                if (this.m.size() == 1 && this.h.j.size() == 0) {
                    return b(aVar5, this.h);
                }
                if (g() && !this.y) {
                    this.s[this.s.length - 1] = aVar5;
                    c.a(this, c.b.PUZZLE_SOLVED, null);
                }
                return aVar5;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final boolean g() {
        return this.m.size() == 0;
    }

    public final void a(long j2) {
        if (S || !this.k) {
            int i2 = 0;
            while (this.A < j2) {
                i2++;
                this.A += 16;
            }
            for (int i3 = 0; i3 < i2; i3++) {
                this.b.step(0.016666668f, 8, 3);
            }
            Iterator<j> it = this.m.iterator();
            while (it.hasNext()) {
                ((C0018a) it.next()).s();
            }
            return;
        }
        throw new AssertionError();
    }

    public final boolean a(com.badlogic.gdx.math.a.a aVar, int i2) {
        j a2;
        if (!S && this.k) {
            throw new AssertionError();
        } else if (!S && this.d.containsKey(Integer.valueOf(i2))) {
            throw new AssertionError();
        } else if (!S && this.c.containsKey(Integer.valueOf(i2))) {
            throw new AssertionError();
        } else if (this.Q && this.R == -1) {
            this.R = i2;
            Iterator<j> it = this.g.iterator();
            while (it.hasNext()) {
                j next = it.next();
                com.badlogic.gdx.math.f j2 = next.j();
                ((C0018a) next).a(i2, new g(j2.a, j2.b));
            }
            return true;
        } else if (!this.i || this.E != -1 || (a2 = this.O.a(aVar, this.D)) == null) {
            j a3 = this.B.a(aVar, this.C);
            if (a3 == null || this.c.values().contains(a3)) {
                j a4 = this.g.a(aVar, this.D, this.b);
                if (a4 == null) {
                    a4 = this.m.a(aVar, this.D, this.b);
                }
                if (a4 == null) {
                    return false;
                }
                ((C0018a) a4).a(i2, aVar);
                this.d.put(Integer.valueOf(i2), (C0018a) a4);
                b((C0018a) a4);
                return true;
            } else if (S || (a3 instanceof al)) {
                al alVar = (al) a3;
                if (S || alVar.f()) {
                    y d2 = alVar.h().d();
                    d2.a(aVar);
                    d2.a(true);
                    d2.b(aVar);
                    this.c.put(Integer.valueOf(i2), alVar);
                    return true;
                }
                throw new AssertionError();
            } else {
                throw new AssertionError();
            }
        } else {
            if (this.N != null) {
                this.N.a(false);
            }
            this.N = (ah) a2;
            this.N.a(true);
            n();
            this.i = true;
            this.j = 1.0f;
            this.E = i2;
            this.F = aVar;
            return true;
        }
    }

    private void b(C0018a aVar) {
        al alVar = aVar.f;
        if (!this.B.contains(alVar)) {
            a(true);
            if (S || !alVar.r()) {
                long unused = aVar.z = System.currentTimeMillis();
                alVar.a(true);
                this.B.add(alVar);
                return;
            }
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(al alVar) {
        int i2;
        if (this.B.contains(alVar)) {
            this.B.remove(alVar);
            if (this.c.containsValue(alVar)) {
                Iterator<Integer> it = this.c.keySet().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        i2 = -1;
                        break;
                    }
                    i2 = it.next().intValue();
                    if (this.c.get(Integer.valueOf(i2)) == alVar) {
                        break;
                    }
                }
                if (S || i2 >= 0) {
                    this.c.remove(Integer.valueOf(i2));
                } else {
                    throw new AssertionError();
                }
            }
            if (S || alVar.r()) {
                alVar.a(false);
                return;
            }
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: protected */
    public final void h() {
        for (al a2 : (al[]) this.B.toArray(new al[0])) {
            a(a2);
        }
    }

    public final void a(boolean z2) {
        HashSet hashSet = new HashSet();
        Iterator<j> it = this.B.iterator();
        while (it.hasNext()) {
            al alVar = (al) it.next();
            if (z2 || (System.currentTimeMillis() - ((C0018a) alVar.s()).r() > 2000 && !this.d.contains(alVar.s()))) {
                hashSet.add(alVar);
            }
        }
        Iterator it2 = hashSet.iterator();
        while (it2.hasNext()) {
            a((al) it2.next());
        }
    }

    public final boolean b(com.badlogic.gdx.math.a.a aVar, int i2) {
        if (i2 == this.R) {
            Iterator<j> it = this.g.iterator();
            while (it.hasNext()) {
                ((C0018a) it.next()).b(i2, aVar);
            }
            return true;
        } else if (this.c.containsKey(Integer.valueOf(i2))) {
            this.c.get(Integer.valueOf(i2)).h().d().c(aVar);
            return true;
        } else if (!this.d.containsKey(Integer.valueOf(i2))) {
            return false;
        } else {
            this.d.get(Integer.valueOf(i2)).b(i2, aVar);
            return true;
        }
    }

    public final boolean a(int i2) {
        if (i2 == this.R) {
            this.Q = false;
            this.R = -1;
            Iterator<j> it = this.g.iterator();
            while (it.hasNext()) {
                ((C0018a) it.next()).a(i2);
            }
            return true;
        } else if (this.c.containsKey(Integer.valueOf(i2))) {
            C0018a aVar = (C0018a) this.c.remove(Integer.valueOf(i2)).h();
            aVar.d().a(false);
            if (ar.a().a) {
                c(aVar);
            }
            return true;
        } else if (!this.d.containsKey(Integer.valueOf(i2))) {
            return false;
        } else {
            C0018a remove = this.d.remove(Integer.valueOf(i2));
            remove.a(i2);
            if (ar.a().a) {
                c(remove);
            }
            return true;
        }
    }

    public final void a(w wVar) {
        float g2 = wVar.g();
        this.C = 0.2f / g2;
        this.D = 0.2f / g2;
    }

    public final boolean a(com.badlogic.gdx.math.a.a aVar, w wVar) {
        if (!this.i || this.E != -1) {
            j a2 = this.g.a(aVar, 0.0f, this.b);
            if (a2 == null) {
                a2 = this.m.a(aVar, this.D, this.b);
            }
            if (a2 != null) {
                if (!S && !(a2 instanceof C0018a)) {
                    throw new AssertionError();
                } else if (c((C0018a) a2)) {
                    return true;
                }
            }
            j a3 = this.B.a(aVar, this.C);
            if (a3 != null) {
                a3.h().d().a(wVar.h());
                ((C0018a) a3.h()).u();
                return true;
            }
            C0018a aVar2 = (C0018a) this.g.a(aVar, this.D);
            if (aVar2 != null) {
                e(aVar2);
                return true;
            }
            C0018a aVar3 = (C0018a) this.m.a(aVar, this.D, this.b);
            if (aVar3 == null) {
                return false;
            }
            if (!S && this.k) {
                throw new AssertionError();
            } else if (S || !aVar3.A()) {
                aVar3.z();
                a(aVar3);
                return true;
            } else {
                throw new AssertionError();
            }
        } else {
            j a4 = this.O.a(aVar, this.D);
            if (a4 != null) {
                if (this.N != null) {
                    this.N.a(false);
                }
                this.N = (ah) a4;
                o();
            }
            return true;
        }
    }

    private boolean c(C0018a aVar) {
        if (!aVar.x()) {
            return false;
        }
        C0018a y2 = aVar.y();
        if (y2 == null) {
            c.a(this, c.b.GLOB_JOIN_FAILURE, null);
        } else if (!g()) {
            c.a(this, c.b.GLOB_JOIN_SUCCESS, y2);
        }
        return true;
    }

    public final boolean c(com.badlogic.gdx.math.a.a aVar, int i2) {
        return a(aVar, i2);
    }

    public final boolean d(com.badlogic.gdx.math.a.a aVar, int i2) {
        if (i2 != this.E) {
            return false;
        }
        this.F = aVar;
        j a2 = this.O.a(aVar, this.D);
        if (!(this.N == null || this.N == a2)) {
            this.N.a(false);
        }
        this.N = (ah) a2;
        if (this.N != null) {
            this.N.a(true);
        }
        return true;
    }

    public final boolean b(int i2) {
        if (i2 != this.E) {
            return false;
        }
        if (this.N != null) {
            o();
        } else {
            if (this.P == null) {
                this.P = new b();
            }
            this.P.g();
        }
        this.F = null;
        this.E = -1;
        return true;
    }

    public a() {
        if (S || this.J.a() <= -5.0f) {
            this.g = new x();
            this.m = new x();
            this.o = new y(this.m);
            this.f = new HashMap<>();
            this.G = new HashMap<>();
            this.x = new HashMap<>();
            this.q = new ArrayList<>();
            this.H = C0018a.c(new C0018a(this, false).F());
            this.h = new C0018a(this, true).F();
            this.a = this.b.createBody(new BodyDef());
            d.a().a(new com.ptowngames.jigsaur.a.a() {
                public final void c() {
                    for (C0018a next : a.this.d.values()) {
                        if (next.l) {
                            next.u();
                        }
                    }
                    for (al h : a.this.c.values()) {
                        C0018a aVar = (C0018a) h.h();
                        if (aVar.l) {
                            aVar.u();
                        }
                    }
                }
            }.a(200).a(true));
            f.b().a(this.b);
            return;
        }
        throw new AssertionError();
    }

    public static FixtureDef i() {
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.density = 1.0f;
        fixtureDef.friction = 0.0f;
        fixtureDef.isSensor = false;
        fixtureDef.restitution = 0.0f;
        return fixtureDef;
    }

    public final void a(s sVar) {
        if (S || !this.f.containsKey(sVar)) {
            this.q.add(sVar);
            this.t = true;
            this.G.put(Integer.valueOf(sVar.s()), sVar);
            C0018a a2 = new C0018a(this, false).a(sVar);
            BodyDef bodyDef = new BodyDef();
            bodyDef.active = true;
            bodyDef.allowSleep = true;
            bodyDef.angularDamping = 30.0f;
            bodyDef.awake = true;
            bodyDef.bullet = false;
            bodyDef.fixedRotation = false;
            bodyDef.linearDamping = 30.0f;
            bodyDef.type = BodyDef.BodyType.DynamicBody;
            bodyDef.angle = a2.k();
            com.badlogic.gdx.math.f j2 = a2.j();
            bodyDef.position.a(j2.a, j2.b);
            a2.a(this.b.createBody(bodyDef));
            FixtureDef i2 = i();
            i2.shape = sVar.u();
            if (S || !((f.c) i2.shape).a()) {
                i2.shape.setRadius(0.0f);
                a2.q().createFixture(i2);
                C0018a unused = a2.F();
                a2.b.c = -5.0f;
                a2.m();
                return;
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    public final void a(float f2) {
        this.e = f2;
    }

    private C0018a c(int i2) {
        return this.f.get(this.G.get(Integer.valueOf(i2)));
    }

    private boolean d(C0018a aVar) {
        return this.m.contains(aVar) || aVar == this.H || aVar == this.h;
    }

    public final Jigsaur.WorkspaceConfiguration j() {
        Jigsaur.WorkspaceConfiguration.Builder newBuilder = Jigsaur.WorkspaceConfiguration.newBuilder();
        for (C0018a t2 : this.s) {
            newBuilder.addGlobs(t2.t());
        }
        return newBuilder.build();
    }

    public final void a(Jigsaur.WorkspaceConfiguration workspaceConfiguration) {
        this.y = true;
        int i2 = 0;
        for (Jigsaur.PieceGlobConfiguration next : workspaceConfiguration.getGlobsList()) {
            i2++;
            int i3 = -1;
            if (next.getPieceIdsCount() > 0) {
                i3 = next.getPieceIds(0);
            }
            C0018a c2 = c(i3);
            if (c2 != null) {
                C0018a aVar = c2;
                for (int i4 = 1; i4 < next.getPieceIdsCount(); i4++) {
                    int pieceIds = next.getPieceIds(i4);
                    C0018a c3 = c(pieceIds);
                    if (c3 != null) {
                        aVar = c(aVar, c3);
                    } else {
                        "Error loading glob config, unknown piece id: " + pieceIds;
                    }
                }
                Jigsaur.Vertex3 center = next.getCenter();
                com.badlogic.gdx.math.f b2 = b(new com.badlogic.gdx.math.f(center.getX(), center.getY(), center.getZ()));
                aVar.a(b2.a, b2.b, b2.c, next.getAngle());
                if (next.getState() == Jigsaur.PieceGlobConfiguration.a.RAISED) {
                    if (!aVar.A()) {
                        aVar.b.c = -5.0f;
                        aVar.m();
                        aVar.z();
                    } else {
                        aVar.b.c = -4.85f;
                        aVar.m();
                    }
                    if (S || aVar.A()) {
                        a(aVar);
                        if (!S && !this.g.contains(aVar)) {
                            throw new AssertionError();
                        }
                    } else {
                        throw new AssertionError();
                    }
                } else if (aVar.A()) {
                    aVar.b.c = -4.85f;
                    aVar.m();
                    aVar.z();
                    if (!S && this.g.contains(aVar)) {
                        throw new AssertionError();
                    }
                } else {
                    aVar.b.c = -5.0f;
                    aVar.m();
                }
                if (next.getState() == Jigsaur.PieceGlobConfiguration.a.SOLVED && aVar != this.h) {
                    c(aVar, this.h);
                }
            } else {
                "Error loading glob config, unknown piece id: " + i3;
            }
        }
        this.y = false;
        h();
        "Read " + i2 + " glob configurations.";
    }

    /* access modifiers changed from: protected */
    public final void a(C0018a aVar) {
        if (!S && this.g.contains(aVar)) {
            throw new AssertionError();
        } else if (S || aVar.A()) {
            int unused = aVar.D = -65504;
            this.g.add(aVar);
            b(aVar);
            if (this.R != -1) {
                com.badlogic.gdx.math.f j2 = aVar.j();
                aVar.a(this.R, new g(j2.a, j2.b));
            }
            this.L.b(false);
            this.M.b(false);
        } else {
            throw new AssertionError();
        }
    }

    private void e(C0018a aVar) {
        if (!S && !aVar.A()) {
            throw new AssertionError();
        } else if (S || this.g.contains(aVar)) {
            int unused = aVar.D = 16;
            aVar.z();
            this.g.remove(aVar);
            a(aVar.f);
            if (this.R != -1) {
                aVar.a(this.R);
            }
            if (this.g.size() == 0) {
                this.L.b(true);
                this.M.b(true);
            }
        } else {
            throw new AssertionError();
        }
    }

    class b extends com.ptowngames.jigsaur.a.b {
        private static /* synthetic */ boolean i = (!a.class.desiredAssertionStatus());
        private int b = 0;
        private int c = 1;
        private int d = 2;
        private int e = 3;
        private int f = 0;
        private long g = 0;
        private long h = 0;

        public b() {
            super(0, 1.0f, 0.0f);
        }

        public final void g() {
            a.this.i = true;
            a.this.j = 1.0f;
            this.g = 800;
            this.h = 500;
            b(true);
            this.f = 0;
            if (!d.a().c(this)) {
                this.f = 1;
                d.a().a(this);
            }
        }

        public final void a() {
            if (this.f == 1) {
                b(this.g);
            } else if (this.f == 2) {
                b(this.h);
            } else if (!i) {
                throw new AssertionError();
            }
            super.a();
        }

        public final void b() {
            super.b();
            this.f++;
            "fade out => " + this.f;
            if (this.f >= 3 || !f()) {
                a.this.i = false;
            } else {
                d.a().a(this);
            }
        }

        public final void a(float f2) {
            if (this.f > 1) {
                if (this.f == 2 || i) {
                    a.this.j = f2;
                    return;
                }
                throw new AssertionError();
            }
        }
    }

    private void n() {
        if (this.P != null) {
            this.P.b(false);
            d.a().c(this.P);
            this.P = null;
        }
    }

    private void o() {
        if (S || this.N != null) {
            if (!this.N.p() && this.N != this.K) {
                if (this.N == this.L) {
                    for (C0018a e2 : (C0018a[]) this.g.toArray(new C0018a[this.g.size()])) {
                        e(e2);
                    }
                } else if (this.N == this.M) {
                    if (S || this.R == -1) {
                        this.Q = true;
                        Iterator<j> it = this.B.iterator();
                        while (it.hasNext()) {
                            a((al) it.next());
                        }
                    } else {
                        throw new AssertionError();
                    }
                }
            }
            this.N.a(false);
            this.N = null;
            n();
            this.i = false;
            this.j = 0.0f;
            return;
        }
        throw new AssertionError();
    }

    public final boolean k() {
        return this.Q;
    }

    public final void l() {
        this.z.dispose();
        this.b.dispose();
        f.b().b(this.b);
        this.J.c();
        this.k = true;
    }

    public static com.badlogic.gdx.math.f a(com.badlogic.gdx.math.f fVar) {
        Jigsaur.Level h2 = o.a().h();
        com.badlogic.gdx.math.f fVar2 = new com.badlogic.gdx.math.f();
        float f2 = fVar.a;
        float minX = h2.getMinX();
        fVar2.a = (f2 - minX) / (h2.getMaxX() - minX);
        float f3 = fVar.b;
        float minY = h2.getMinY();
        fVar2.b = (f3 - minY) / (h2.getMaxY() - minY);
        fVar2.c = (fVar.c - -5.0f) / 0.1500001f;
        return fVar2;
    }

    public static com.badlogic.gdx.math.f b(com.badlogic.gdx.math.f fVar) {
        Jigsaur.Level h2 = o.a().h();
        com.badlogic.gdx.math.f fVar2 = new com.badlogic.gdx.math.f();
        fVar2.a = b(fVar.a, h2.getMinX(), h2.getMaxX());
        fVar2.b = b(fVar.b, h2.getMinY(), h2.getMaxY());
        fVar2.c = b(fVar.c, -5.0f, -4.85f);
        return fVar2;
    }

    public static float a(float f2, float f3, float f4) {
        return (f2 - f3) / (f4 - f3);
    }

    public static float b(float f2, float f3, float f4) {
        return ((1.0f - f2) * f3) + (f4 * f2);
    }
}
