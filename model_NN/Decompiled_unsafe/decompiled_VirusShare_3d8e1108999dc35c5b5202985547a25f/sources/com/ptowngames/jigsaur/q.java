package com.ptowngames.jigsaur;

import com.badlogic.gdx.math.g;

public class q {
    private static /* synthetic */ boolean h = (!q.class.desiredAssertionStatus());
    private Float a;
    private g b;
    private g c;
    private float[] d;
    private float[] e;
    private g f;
    private float g;

    public q(float[] fArr, short[] sArr, int i, int i2) {
        if (!h && i2 <= 2) {
            throw new AssertionError();
        } else if (h || i + i2 <= sArr.length) {
            this.a = null;
            this.b = null;
            this.g = -1.0f;
            this.c = new g(0.0f, 0.0f);
            this.f = new g();
            this.d = new float[i2];
            this.e = new float[i2];
            for (int i3 = 0; i3 < i2; i3++) {
                short s = 65535 & sArr[i + i3];
                this.d[i3] = fArr[(s * 5) + 0];
                this.e[i3] = fArr[(s * 5) + 1];
                this.f.b(this.d[i3], this.e[i3]);
            }
        } else {
            throw new AssertionError();
        }
    }

    public final g a() {
        return this.f.a().a(-this.c.a, -this.c.b);
    }

    public final void a(float f2, float f3) {
        this.c.a(f2, f3);
        this.b = null;
    }

    private int a(int i) {
        int length = i % this.d.length;
        if (length < 0) {
            return length + this.d.length;
        }
        return length;
    }

    private float b(int i) {
        return this.d[a(i)] - this.c.a;
    }

    private float c(int i) {
        return this.e[a(i)] - this.c.b;
    }

    private float c() {
        if (this.a != null) {
            return this.a.floatValue();
        }
        this.a = Float.valueOf(0.0f);
        for (int i = 0; i < this.d.length; i++) {
            this.a = Float.valueOf(this.a.floatValue() + ((b(i) * c(i + 1)) - (b(i + 1) * c(i))));
        }
        this.a = Float.valueOf(this.a.floatValue() * 0.5f);
        return this.a.floatValue();
    }

    public final g b() {
        float f2 = 0.0f;
        if (this.b != null) {
            return this.b;
        }
        float f3 = 0.0f;
        for (int i = 0; i < this.d.length; i++) {
            f3 += (b(i) + b(i + 1)) * ((b(i) * c(i + 1)) - (b(i + 1) * c(i)));
            f2 += (c(i) + c(i + 1)) * ((b(i) * c(i + 1)) - (b(i + 1) * c(i)));
        }
        this.b = new g(f3 / (c() * 6.0f), f2 / (c() * 6.0f));
        return this.b;
    }

    public final float b(float f2, float f3) {
        int length = this.d.length;
        float f4 = this.c.a;
        float f5 = this.c.b;
        int i = 0;
        boolean z = false;
        int i2 = length - 1;
        while (i < length) {
            float f6 = this.e[i] - f5;
            float f7 = this.e[i2] - f5;
            float f8 = this.d[i] - f4;
            float f9 = this.d[i2] - f4;
            if ((f6 > f3) != (f7 > f3) && f2 - f8 < ((f9 - f8) * (f3 - f6)) / (f7 - f6)) {
                z = !z;
            }
            int i3 = i;
            i++;
            i2 = i3;
        }
        if (z) {
            return 0.0f;
        }
        float f10 = Float.POSITIVE_INFINITY;
        float f11 = this.c.a;
        float f12 = this.c.b;
        int i4 = 0;
        while (i4 < this.d.length) {
            float f13 = f2 - (this.d[i4] - f11);
            float f14 = f3 - (this.e[i4] - f12);
            float f15 = (f13 * f13) + (f14 * f14);
            if (f15 >= f10) {
                f15 = f10;
            }
            i4++;
            f10 = f15;
        }
        return f10;
    }
}
