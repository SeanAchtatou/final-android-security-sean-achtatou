package com.ptowngames.jigsaur;

import com.badlogic.gdx.math.g;
import java.util.Iterator;

public class i extends j {
    protected y a;
    private float f;

    public i() {
        super((byte) 0);
        this.a = null;
        this.f = -1.0f;
    }

    /* access modifiers changed from: protected */
    public final void a(x xVar, float f2) {
        this.a = new y(xVar.a());
        a(f2);
        c(xVar.c());
        this.f = 0.0f;
        Iterator<j> it = this.a.iterator();
        while (it.hasNext()) {
            j next = it.next();
            this.f += next.b();
            if (next.f()) {
                next.i();
            }
            next.a(this);
        }
    }

    public i(x xVar) {
        this();
        a(xVar, 0.0f);
    }

    public float a(g gVar) {
        float f2 = Float.POSITIVE_INFINITY;
        Iterator<j> it = this.a.iterator();
        while (true) {
            float f3 = f2;
            if (!it.hasNext()) {
                return f3;
            }
            f2 = it.next().a(gVar);
            if (f2 >= f3) {
                f2 = f3;
            }
        }
    }

    public void a(aa aaVar) {
        this.a.a(aaVar);
    }

    public final g a() {
        return this.a.d();
    }

    public final float b() {
        return this.f;
    }
}
