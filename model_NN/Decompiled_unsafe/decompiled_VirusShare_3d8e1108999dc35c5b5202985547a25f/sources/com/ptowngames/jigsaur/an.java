package com.ptowngames.jigsaur;

import com.badlogic.gdx.math.f;
import com.badlogic.gdx.math.g;

public class an extends j {
    float a;

    public an(float f) {
        super((byte) 0);
        this.a = f;
    }

    public final float a(g gVar) {
        f j = j();
        float b = gVar.a().b(new g(j.a, j.b)).b();
        if (b <= this.a) {
            return 0.0f;
        }
        return (b - this.a) * (b - this.a);
    }

    public void a(aa aaVar) {
        aaVar.a();
        aaVar.a(j());
        aaVar.b(this.a);
        aaVar.b();
    }

    public final g a() {
        f j = j();
        g gVar = new g();
        int[] iArr = {-1, 1, 1, -1};
        int[] iArr2 = {1, 1, -1, -1};
        for (int i = 0; i < 4; i++) {
            gVar.b(j.a + (((float) iArr[i]) * this.a), j.b + (((float) iArr2[i]) * this.a));
        }
        return gVar;
    }
}
