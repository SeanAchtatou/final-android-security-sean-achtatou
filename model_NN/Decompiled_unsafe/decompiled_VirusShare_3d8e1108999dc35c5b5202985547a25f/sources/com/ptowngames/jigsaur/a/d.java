package com.ptowngames.jigsaur.a;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.PriorityQueue;

public final class d {
    private static d c = null;
    private PriorityQueue<e> a = new PriorityQueue<>();
    private boolean b = true;

    private d() {
    }

    public static d a() {
        if (c == null) {
            c = new d();
        }
        return c;
    }

    public static void b() {
        if (c != null) {
            c.b = false;
            ArrayList arrayList = new ArrayList(c.a.size());
            Iterator<e> it = c.a.iterator();
            while (it.hasNext()) {
                arrayList.add(it.next());
            }
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                c.c((e) it2.next());
            }
        }
        c = null;
        e.a = 0;
    }

    public final synchronized boolean a(e eVar) {
        boolean b2;
        if (!this.b) {
            b2 = false;
        } else {
            this.a.remove(eVar);
            eVar.a();
            b2 = b(eVar);
        }
        return b2;
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean b(e eVar) {
        boolean z;
        if (!this.b) {
            z = false;
        } else {
            eVar.j();
            this.a.offer(eVar);
            z = true;
        }
        return z;
    }

    public final synchronized boolean c(e eVar) {
        boolean z;
        if (this.a.remove(eVar)) {
            eVar.b();
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    public final void c() {
        do {
        } while (a(System.currentTimeMillis()));
    }

    private synchronized boolean a(long j) {
        boolean z;
        if (this.a.size() == 0 || this.a.peek().i() > j) {
            z = false;
        } else {
            this.a.poll().h();
            z = true;
        }
        return z;
    }
}
