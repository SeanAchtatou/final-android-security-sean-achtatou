package com.ptowngames.jigsaur.a;

public abstract class e implements Comparable {
    static long a = 0;
    private static /* synthetic */ boolean d;
    private long b = -1;
    private long c;

    public abstract void c();

    public abstract long d();

    public abstract boolean e();

    static {
        boolean z;
        if (!e.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        d = z;
    }

    public int compareTo(Object obj) {
        if (d || (obj instanceof e)) {
            e eVar = (e) obj;
            if (this == eVar) {
                return 0;
            }
            long j = this.b;
            long j2 = eVar.b;
            if (j < j2) {
                return -1;
            }
            if (j2 < j) {
                return 1;
            }
            if (this.c < eVar.c) {
                return -1;
            }
            if (this.c > eVar.c) {
                return 1;
            }
            if (d) {
                return 0;
            }
            throw new AssertionError();
        }
        throw new AssertionError();
    }

    /* access modifiers changed from: package-private */
    public final void h() {
        c();
        if (e()) {
            d.a().b(this);
        } else {
            b();
        }
    }

    public final long i() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final e j() {
        this.b = System.currentTimeMillis() + d();
        return this;
    }

    protected e() {
        long j = a;
        a = 1 + j;
        this.c = j;
    }

    /* access modifiers changed from: protected */
    public void a() {
    }

    /* access modifiers changed from: protected */
    public void b() {
    }
}
