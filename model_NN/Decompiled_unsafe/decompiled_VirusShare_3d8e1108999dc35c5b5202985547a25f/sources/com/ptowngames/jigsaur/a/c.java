package com.ptowngames.jigsaur.a;

public abstract class c extends b {
    private b b = null;

    public final void a(b bVar) {
        this.b = bVar;
    }

    public final b g() {
        return this.b;
    }

    public c(long j, float f) {
        super(j, 0.0f, f);
    }

    /* access modifiers changed from: protected */
    public void b() {
        super.b();
        b bVar = this.b;
        if (bVar != null) {
            d.a().a(bVar);
        }
    }
}
