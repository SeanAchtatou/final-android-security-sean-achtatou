package com.ptowngames.jigsaur.a;

public abstract class b extends a {
    private static /* synthetic */ boolean h = (!b.class.desiredAssertionStatus());
    private long b;
    private float c;
    private float d;
    private long e;
    private long f;
    private boolean g = true;

    public abstract void a(float f2);

    public final boolean f() {
        return this.g;
    }

    public final void b(boolean z) {
        this.g = z;
    }

    private void b(float f2) {
        if (this.g) {
            a(f2);
        }
    }

    public final void c() {
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis <= this.e) {
            b(this.c);
        } else if (currentTimeMillis >= this.f) {
            b(this.d);
        } else {
            float currentTimeMillis2 = ((float) (System.currentTimeMillis() - this.e)) / ((float) (this.f - this.e));
            b((currentTimeMillis2 * (this.d - this.c)) + this.c);
        }
    }

    public final boolean e() {
        if (System.currentTimeMillis() >= this.f) {
            return false;
        }
        return super.e();
    }

    /* access modifiers changed from: protected */
    public void b() {
        b(this.d);
    }

    protected b(long j, float f2, float f3) {
        if (h || j >= 0) {
            this.b = j;
            a(true);
            this.c = f2;
            this.d = f3;
            return;
        }
        throw new AssertionError();
    }

    public void a() {
        this.e = System.currentTimeMillis();
        this.f = this.e + this.b;
    }

    public final void b(long j) {
        if (h || j >= 0) {
            this.b = j;
            this.f = this.e + this.b;
            return;
        }
        throw new AssertionError();
    }

    public final void a(float f2, float f3) {
        this.c = f2;
        this.d = f3;
    }
}
