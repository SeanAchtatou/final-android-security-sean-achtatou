package com.ptowngames.jigsaur.a;

public abstract class a extends e {
    private long b = 0;
    private boolean c = false;

    public final long d() {
        return this.b;
    }

    public final a a(long j) {
        this.b = j;
        return this;
    }

    public boolean e() {
        return this.c;
    }

    public final a a(boolean z) {
        this.c = z;
        return this;
    }

    protected a() {
    }
}
