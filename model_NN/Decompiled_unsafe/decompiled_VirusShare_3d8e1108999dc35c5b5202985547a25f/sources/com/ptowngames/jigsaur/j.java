package com.ptowngames.jigsaur;

import com.badlogic.gdx.math.f;
import com.badlogic.gdx.math.g;
import java.util.ArrayList;

public abstract class j implements Comparable {
    private static final Object a = new Object();
    private static long f = 0;
    private static /* synthetic */ boolean m = (!j.class.desiredAssertionStatus());
    protected final f b;
    protected f c;
    protected f d;
    protected float e;
    private Long g;
    private y h;
    private a i;
    private ArrayList<j> j;
    private j k;
    private g l;

    public abstract float a(g gVar);

    public static void c() {
        f = 0;
    }

    public int compareTo(Object obj) {
        if (m || (obj instanceof j)) {
            return this.g.compareTo(((j) obj).g);
        }
        throw new AssertionError();
    }

    public final y d() {
        return this.h;
    }

    protected class a {
        public float a;
        public final float b;
        public final float c;

        public a(float f, float f2, float f3) {
            this.a = f;
            this.b = f2;
            this.c = f3;
        }

        public final String toString() {
            return "" + this.a + ":" + this.b + ":" + this.c;
        }
    }

    public final a e() {
        return this.i;
    }

    public final boolean f() {
        return this.k != null;
    }

    public final int g() {
        return this.j.size();
    }

    public final j h() {
        return this.k;
    }

    public final void a(j jVar) {
        g gVar = new g(this.b.a, this.b.b);
        gVar.a -= jVar.b.a;
        gVar.b -= jVar.b.b;
        float d2 = gVar.d() * 0.017453292f;
        a aVar = new a(gVar.b(), d2 - jVar.e, this.e - d2);
        if (!m && this.j.size() != 0) {
            throw new AssertionError();
        } else if (!m && this.k != null) {
            throw new AssertionError();
        } else if (!m && jVar.k != null) {
            throw new AssertionError();
        } else if (m || jVar.j != null) {
            this.k = jVar;
            this.j = null;
            this.i = aVar;
            jVar.j.add(this);
        } else {
            throw new AssertionError();
        }
    }

    public final void i() {
        if (!m && !f()) {
            throw new AssertionError();
        } else if (!m && this.j != null) {
            throw new AssertionError();
        } else if (m || this.k != null) {
            boolean remove = this.k.j.remove(this);
            if (m || remove) {
                this.k = null;
                this.i = null;
                this.j = new ArrayList<>();
                return;
            }
            throw new AssertionError();
        } else {
            throw new AssertionError();
        }
    }

    public final void a(f fVar) {
        if (fVar == null) {
            this.c = null;
        } else {
            this.c = new f(fVar);
        }
    }

    public final void b(f fVar) {
        if (fVar == null) {
            this.d = null;
        } else {
            this.d = new f(fVar);
        }
    }

    protected j(byte b2) {
        this();
    }

    private j() {
        this.c = null;
        this.d = null;
        this.l = new g();
        this.b = new f(0.0f, 0.0f, 0.0f);
        this.e = 0.0f;
        synchronized (a) {
            long j2 = f;
            f = 1 + j2;
            this.g = new Long(j2);
        }
        this.k = null;
        this.i = null;
        this.j = new ArrayList<>();
        x xVar = new x();
        xVar.add(this);
        this.h = new y(xVar);
    }

    public final void c(f fVar) {
        a(fVar.a, fVar.b, fVar.c, this.e);
    }

    public final void a(float f2, float f3, float f4) {
        a(f2, f3, f4, this.e);
    }

    public final f j() {
        return this.b.a();
    }

    public final void a(float f2) {
        a(this.b.a, this.b.b, this.b.c, f2);
    }

    public float k() {
        return this.e;
    }

    public final f l() {
        if (m || !f()) {
            return this.b;
        }
        throw new AssertionError();
    }

    public final void b(float f2) {
        if (m || !f()) {
            this.e = f2;
            return;
        }
        throw new AssertionError();
    }

    public final void m() {
        while (true) {
            this.o();
            if (!this.f()) {
                break;
            }
            this = this.k;
        }
        int size = this.j.size();
        for (int i2 = 0; i2 < size; i2++) {
            j jVar = this.j.get(i2);
            a aVar = jVar.i;
            if (aVar.a == 0.0f) {
                jVar.b.a(this.b);
                jVar.e = aVar.c + this.e + aVar.b;
                jVar.n();
            } else {
                float f2 = this.e + aVar.b;
                jVar.b.a(this.b.a + (aVar.a * ((float) Math.cos((double) f2))), this.b.b + (aVar.a * ((float) Math.sin((double) f2))), this.b.c);
                jVar.e = aVar.c + f2;
                jVar.n();
            }
        }
        this.n();
    }

    /* access modifiers changed from: protected */
    public void n() {
    }

    public final void a(float f2, float f3, float f4, float f5) {
        float cos;
        float f6 = 0.0f;
        if (this.k != null) {
            a aVar = this.i;
            if (m || aVar != null) {
                float f7 = f5 - aVar.c;
                if (aVar.a == 0.0f) {
                    cos = 0.0f;
                } else {
                    cos = ((float) Math.cos((double) f7)) * aVar.a;
                    f6 = aVar.a * ((float) Math.sin((double) f7));
                }
                this.k.b.a(f2 - cos, f3 - f6, f4);
                this.k.e = f7 - aVar.b;
                this.k.m();
                return;
            }
            throw new AssertionError();
        }
        this.b.a(f2, f3, f4);
        this.e = f5;
        m();
    }

    /* access modifiers changed from: protected */
    public void o() {
        if (this.c != null) {
            if (m || !f()) {
                if (this.b.a < this.c.a) {
                    this.b.a = this.c.a;
                }
                if (this.b.b < this.c.b) {
                    this.b.b = this.c.b;
                }
                if (this.b.c < this.c.c) {
                    this.b.c = this.c.c;
                }
            } else {
                throw new AssertionError();
            }
        }
        if (this.d == null) {
            return;
        }
        if (m || !f()) {
            if (this.b.a > this.d.a) {
                this.b.a = this.d.a;
            }
            if (this.b.b > this.d.b) {
                this.b.b = this.d.b;
            }
            if (this.b.c > this.d.c) {
                this.b.c = this.d.c;
                return;
            }
            return;
        }
        throw new AssertionError();
    }

    public void a(aa aaVar) {
    }

    public void b(float f2, float f3, float f4, float f5) {
        this.b.a = (float) (((double) f2) + (Math.random() * ((double) (f3 - f2))));
        this.b.b = (float) (((double) f4) + (Math.random() * ((double) (f5 - f4))));
        this.e = (float) (-3.141592653589793d + (2.0d * Math.random() * 3.141592653589793d));
        m();
    }

    public g a() {
        g gVar = new g();
        f fVar = this.b;
        return gVar.b(fVar.a, fVar.b);
    }

    public float b() {
        return 1.0f;
    }
}
