package com.ptowngames.jigsaur;

import com.badlogic.gdx.g;
import com.ptowngames.jigsaur.Jigsaur;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;

public final class p {
    private static p a = null;
    private Hashtable<String, ArrayList<String>> b = new Hashtable<>();

    public static p a() {
        return a;
    }

    private p() {
    }

    public final Jigsaur.PieceSet a(String str, int i) throws Exception {
        String a2 = a(str, i, ".piece_set");
        if (a2 == null) {
            return null;
        }
        return Jigsaur.PieceSet.parseFrom(g.e.b(a2).b());
    }

    public final String a(String str, int i, String str2) {
        ArrayList arrayList = this.b.get(str);
        if (arrayList == null || arrayList.size() == 0) {
            return null;
        }
        return "data/" + ((String) arrayList.get(i % arrayList.size())) + str2;
    }

    public static boolean a(String str) {
        try {
            Jigsaur.PieceSetStubs parseFrom = Jigsaur.PieceSetStubs.parseFrom(g.e.b(str).b());
            a = null;
            p pVar = new p();
            a = pVar;
            int stubsCount = parseFrom.getStubsCount();
            for (int i = 0; i < stubsCount; i++) {
                Jigsaur.PieceSetStub stubs = parseFrom.getStubs(i);
                String pieceSetType = stubs.getPieceSetType();
                String pieceSetFileNamePrefix = stubs.getPieceSetFileNamePrefix();
                ArrayList arrayList = pVar.b.get(pieceSetType);
                if (arrayList == null) {
                    arrayList = new ArrayList(1);
                    arrayList.add(pieceSetFileNamePrefix);
                    pVar.b.put(pieceSetType, arrayList);
                }
                if (!arrayList.contains(pieceSetFileNamePrefix)) {
                    arrayList.add(pieceSetFileNamePrefix);
                    Collections.sort(arrayList);
                }
            }
            return true;
        } catch (Exception e) {
            "Failed to load '" + str + "': " + e;
            return false;
        }
    }
}
