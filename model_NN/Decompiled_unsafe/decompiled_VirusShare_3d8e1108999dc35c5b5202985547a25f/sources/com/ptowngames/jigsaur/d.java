package com.ptowngames.jigsaur;

import com.badlogic.gdx.g;
import com.badlogic.gdx.graphics.b;
import com.badlogic.gdx.graphics.c;
import com.badlogic.gdx.graphics.g2d.c;
import com.ptowngames.jigsaur.Jigsaur;
import com.ptowngames.jigsaur.ag;
import com.ptowngames.jigsaur.c;
import com.ptowngames.jigsaur.f;
import com.ptowngames.jigsaur.w;
import com.ptowngames.webservices.Webservices;
import com.redmicapps.puzzles.ladies3.R;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.Semaphore;

public class d implements com.badlogic.gdx.d {
    private static /* synthetic */ boolean t = (!d.class.desiredAssertionStatus());
    public final Semaphore a;
    private int b;
    /* access modifiers changed from: private */
    public int c;
    private float d;
    private HashSet<ap> e;
    private h f;
    private c g;
    /* access modifiers changed from: private */
    public PuzzlePlayer h;
    /* access modifiers changed from: private */
    public a i;
    private boolean j;
    private Jigsaur.Level k;
    private int l;
    private boolean m;
    private Thread n;
    private boolean o;
    private aa p;
    private boolean q;
    private com.badlogic.gdx.graphics.g2d.a r;
    private b s;

    class a implements ap, w.a {
        private static /* synthetic */ boolean k = (!d.class.desiredAssertionStatus());
        private b a = f.b().a(g.e.b("data/zoom_controls.png"), c.a.RGBA8888);
        private com.badlogic.gdx.graphics.g2d.b b = new com.badlogic.gdx.graphics.g2d.b(this.a, 3, 15);
        private com.badlogic.gdx.graphics.g2d.b c = new com.badlogic.gdx.graphics.g2d.b(this.a, 23, 233);
        private int d = 0;
        private int e = 0;
        private int f = 0;
        private float g = 1.0f;
        private float h = 1.0E-5f;
        private float i = (this.g / this.h);
        private int j;

        public final int a() {
            return this.j;
        }

        public a(int i2) {
            this.j = i2;
        }

        public final void b(int i2) {
            this.j = i2;
        }

        public final void a(int i2, int i3) {
            this.d = 5;
            this.e = (i3 - 32) - 5;
            a(d.this.h.b().g());
        }

        private void a(float f2) {
            float f3 = f2 / this.h;
            if (f3 > this.i) {
                f3 = this.i;
            }
            if (f3 < 1.0f) {
                f3 = 1.0f;
            }
            this.f = ((int) ((float) ((int) (((1.0f - (1.0f / f3)) * 155.0f) + ((float) (this.d + 35)))))) - 7;
        }

        public final void a(w wVar) {
            if (k || wVar == d.this.h.b()) {
                this.h = wVar.f();
                this.g = wVar.e();
                if (!k && this.g < this.h) {
                    throw new AssertionError();
                } else if (k || this.h > 0.0f) {
                    this.i = this.g / this.h;
                    a(wVar.g());
                } else {
                    throw new AssertionError();
                }
            } else {
                throw new AssertionError();
            }
        }

        private boolean e(int i2, int i3, int i4) {
            if (!b(i2, i3)) {
                return false;
            }
            d(i2, i3, i4);
            return true;
        }

        public final boolean a(int i2, int i3, int i4) {
            return e(i2, i3, i4);
        }

        public final boolean b(int i2, int i3, int i4) {
            return e(i2, i3, i4);
        }

        public final boolean c(int i2, int i3, int i4) {
            return e(i2, i3, i4);
        }

        public final void a(int i2) {
        }

        private boolean b(int i2, int i3) {
            int b2 = d.this.c - i3;
            return i2 > this.d + -10 && i2 < (this.d + 233) + 10 && b2 > this.e + -10 && b2 < (this.e + 32) + 10;
        }

        public final void d(int i2, int i3, int i4) {
            if (b(i2, i3) && i2 >= this.d + 35 && i2 <= this.d + 190) {
                this.f = i2 - 7;
                w b2 = d.this.h.b();
                float f2 = 1.0f / (1.0f - (((((float) i2) - ((float) this.d)) - 35.0f) / 155.0f));
                if (f2 > this.i) {
                    f2 = this.i;
                }
                b2.d(f2 * this.h);
            }
        }

        public final void a(com.badlogic.gdx.graphics.g2d.c cVar) {
            cVar.a();
            cVar.c();
            cVar.a(this.c, (float) this.d, (float) this.e);
            cVar.a(this.b, (float) this.f, (float) this.e);
            cVar.b();
        }
    }

    public final void a(ap apVar) {
        this.e.add(apVar);
        this.f.a(apVar);
    }

    public final void b(ap apVar) {
        this.e.remove(apVar);
        this.f.b(apVar);
    }

    public final void a(int i2, int i3) {
        this.b = i2;
        this.c = i3;
        Iterator<ap> it = this.e.iterator();
        while (it.hasNext()) {
            it.next().a(i2, i3);
        }
        this.g = new com.badlogic.gdx.graphics.g2d.c((byte) 0);
    }

    public final PuzzlePlayer f() {
        return this.h;
    }

    public d(Jigsaur.Level level, byte b2) {
        this(level);
    }

    private d(Jigsaur.Level level) {
        this.b = 0;
        this.c = 0;
        this.d = 1.0f;
        this.i = null;
        this.a = new Semaphore(0);
        this.l = 0;
        this.m = false;
        this.n = null;
        this.o = true;
        this.q = false;
        this.s = null;
        this.j = false;
        this.e = new HashSet<>();
        this.k = level;
    }

    public final void a() {
        int i2 = R.string.out_of_memory;
        int i3 = 1;
        this.q = true;
        AnonymousClass2 r3 = new Runnable() {
            public final void run() {
                ab.a().c();
            }
        };
        try {
            c.a();
            ae.a("data/image_infos");
            p.a("data/piece_set_stubs");
            o.a(this.k);
            if (o.b() || t) {
                com.ptowngames.jigsaur.a.d.b();
                af.b();
                j.c();
                a.a();
                s.a(o.a().e());
                al.c();
                r.b();
                f.a();
                this.p = new aj();
                if (this.j) {
                    g.d = new com.badlogic.gdx.b.a();
                }
                this.f = new h();
                g.d.a(this.f);
                this.g = new com.badlogic.gdx.graphics.g2d.c((byte) 0);
                if (!ar.a().e) {
                    i3 = 10;
                }
                this.i = new a(i3 * 3);
                this.h = new PuzzlePlayer(this.i);
                c.a(new c.a() {
                    public final void a(Object obj, c.b bVar, Object obj2) {
                        d.this.b(d.this.i);
                        d.this.i.b((ar.a().e ? 1 : 10) * 3);
                        d.this.a(d.this.i);
                    }
                }, c.b.DISPLAY_ZOOM_SLIDER_CHANGED);
                this.r = new com.badlogic.gdx.graphics.g2d.a();
                a(this.i);
                a(this.h);
                this.a.release();
                return;
            }
            throw new AssertionError();
        } catch (f.d e2) {
            System.err.println("create() failed due to: " + e2);
            if (((double) ab.a().b()) <= 0.95d * ((double) ab.a().a())) {
                i2 = R.string.unknown_error;
            }
            ab.a().a(i2, r3, r3);
            throw e2;
        } catch (OutOfMemoryError e3) {
            System.err.println("create() failed due to: " + e3);
            ab.a().a(R.string.out_of_memory, r3, r3);
            throw e3;
        } catch (Throwable th) {
            try {
                ag.a(ag.b.PUZZLE_PLAYER, th);
            } catch (Throwable th2) {
            }
            this.q = false;
        }
    }

    public final void e() {
        ag.a(ag.b.PUZZLE_PLAYER, Webservices.RewardCheckRequest.a.APP_DISPOSE, null);
        if (f.b() != null) {
            f.b().d();
        }
        try {
            b(this.h);
            this.h.f();
            if (t || f.b().e()) {
                g.d.a((com.badlogic.gdx.f) null);
                this.h = null;
                this.g = null;
                this.i = null;
                this.f = null;
                s.r();
                al.p();
                a.a();
                j.c();
                af.a();
                com.ptowngames.jigsaur.a.d.b();
                o.l();
                c.a();
                Runtime.getRuntime().gc();
                return;
            }
            throw new AssertionError();
        } catch (Throwable th) {
        }
    }

    public final void d() {
        if (this.q) {
            ag.a(ag.b.PUZZLE_PLAYER, Webservices.RewardCheckRequest.a.APP_PAUSE, "" + this.h.a);
            if (this.l < 100) {
                try {
                    g();
                } catch (Throwable th) {
                }
            } else {
                g();
            }
        }
    }

    private void g() {
        this.h.e();
        ar.a().c();
        this.m = true;
    }

    public final void c() {
        if (this.q) {
            if (this.l < 100) {
                try {
                    h();
                } catch (Throwable th) {
                }
            } else {
                h();
            }
        }
    }

    private void h() {
        com.ptowngames.jigsaur.a.d.a().c();
        this.h.a(this.p);
        aa aaVar = this.p;
        g.g.glDepthMask(false);
        if (g.d instanceof com.badlogic.gdx.b.a) {
            for (int i2 = 0; i2 < 2; i2++) {
                if (i2 == 0) {
                    aaVar.a(1.0f, 0.0f, 0.0f, 1.0f);
                } else {
                    aaVar.a(0.0f, 0.0f, 0.8f, 1.0f);
                }
                if (g.d.c(i2)) {
                    int a2 = g.d.a(i2);
                    int b2 = this.c - g.d.b(i2);
                    aaVar.b((float) (a2 - 3), (float) (b2 - 3), (float) (a2 + 3), (float) (b2 + 3));
                    aaVar.c();
                }
            }
        }
        if (ar.a().e) {
            this.i.a(this.g);
        }
        g.g.glDepthMask(true);
    }

    public final void b() {
        if (this.q) {
            ag.a(ag.b.PUZZLE_PLAYER, Webservices.RewardCheckRequest.a.APP_RESUME, null);
            if (this.l < 100) {
                try {
                    i();
                } catch (Throwable th) {
                }
            } else {
                i();
            }
        }
    }

    private void i() {
        this.f.a();
        this.m = false;
    }
}
