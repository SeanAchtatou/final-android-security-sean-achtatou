package com.ptowngames.jigsaur;

import com.badlogic.gdx.g;
import com.badlogic.gdx.graphics.b;
import com.badlogic.gdx.graphics.f;
import com.badlogic.gdx.graphics.n;
import java.util.Iterator;

public class al extends i {
    private static b f = null;
    private static f g = null;
    private static /* synthetic */ boolean n = (!al.class.desiredAssertionStatus());
    private j h;
    private float i;
    private float j;
    private float k;
    private boolean l;
    private float m = 1.0f;

    public static void c() {
        if (f != null) {
            f.d();
        }
        b bVar = new b("data/steering_wheel.png");
        f = bVar;
        bVar.a(b.C0007b.Linear, b.C0007b.Linear);
        if (g != null) {
            g.a();
        }
        g = new f(true, 4, 4, new n(0, 3, "steering_wheel_pos"), new n(3, 2, "steering_wheel_uv"));
        g.a(new float[]{-0.5f, -0.5f, 0.0f, 0.0f, 0.0f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.5f, 0.5f, 0.0f, 1.0f, 1.0f});
        g.a(new short[]{0, 1, 2, 3});
    }

    public static void p() {
        if (f != null) {
            f.d();
        }
        f = null;
        if (g != null) {
            g.a();
        }
        g = null;
    }

    public static b q() {
        return f;
    }

    private al(x xVar, j jVar, float f2) {
        super(xVar);
        Iterator<j> it = this.a.iterator();
        while (it.hasNext()) {
            it.next().i();
        }
        if (n || g() == 0) {
            c(jVar.j());
            a(jVar.k());
            this.h = jVar;
            this.i = 1.0f;
            this.j = 0.45f;
            this.k = f2;
            Iterator<j> it2 = this.a.iterator();
            while (it2.hasNext()) {
                it2.next().a(this.h);
            }
            a(this.h);
            this.l = true;
            return;
        }
        throw new AssertionError();
    }

    public final boolean r() {
        return this.l;
    }

    public final void a(boolean z) {
        if (this.l && !z) {
            Iterator<j> it = this.a.iterator();
            while (it.hasNext()) {
                j next = it.next();
                if (n || next.f()) {
                    next.i();
                } else {
                    throw new AssertionError();
                }
            }
            i();
            Iterator<j> it2 = this.a.iterator();
            while (it2.hasNext()) {
                it2.next().a(this);
            }
        } else if (!this.l && z) {
            com.badlogic.gdx.math.f j2 = this.h.j();
            a(j2.a, j2.b, j2.c, this.h.k());
            Iterator<j> it3 = this.a.iterator();
            while (it3.hasNext()) {
                j next2 = it3.next();
                if (n || next2.f()) {
                    next2.i();
                    next2.a(this.h);
                } else {
                    throw new AssertionError();
                }
            }
            a(this.h);
        }
        this.l = z;
    }

    public final void c(float f2) {
        if (!n && f2 <= 0.0f) {
            throw new AssertionError();
        } else if (f2 != this.m) {
            this.m = f2;
            float f3 = this.j / f2;
            float f4 = this.i / f2;
            Iterator<j> it = this.a.iterator();
            while (it.hasNext()) {
                an anVar = (an) it.next();
                anVar.a = f3;
                if (n || anVar.f()) {
                    anVar.e().a = f4;
                } else {
                    throw new AssertionError();
                }
            }
            if (f()) {
                h().m();
            } else {
                m();
            }
        }
    }

    public static al b(j jVar) {
        if (!n && jVar == null) {
            throw new AssertionError();
        } else if (n || !jVar.f()) {
            x xVar = new x();
            com.badlogic.gdx.math.f j2 = jVar.j();
            float random = (float) (6.283185307179586d * Math.random());
            for (int i2 = 0; i2 <= 0; i2++) {
                float k2 = 0.0f + jVar.k() + random;
                an anVar = new an(0.45f);
                anVar.a((((float) Math.cos((double) k2)) * 0.55f) + j2.a, (((float) Math.sin((double) k2)) * 0.55f) + j2.b, j2.c);
                anVar.a(k2);
                xVar.add(anVar);
            }
            return new al(xVar, jVar, random);
        } else {
            throw new AssertionError();
        }
    }

    public final j s() {
        return this.h;
    }

    public final void a(aa aaVar) {
        if (n || this.l) {
            aaVar.a();
            aaVar.a(j());
            float f2 = ((this.i * 2.0f) + (this.j * 2.0f)) / this.m;
            g.g.a(f2, f2);
            aaVar.a(k() + this.k);
            aaVar.a(1.0f, 1.0f, 1.0f, 1.0f);
            g.a(5);
            aaVar.b();
            return;
        }
        throw new AssertionError();
    }
}
