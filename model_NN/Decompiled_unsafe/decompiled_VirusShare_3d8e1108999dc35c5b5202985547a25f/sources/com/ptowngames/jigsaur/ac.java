package com.ptowngames.jigsaur;

import com.badlogic.gdx.g;
import com.badlogic.gdx.graphics.b;
import com.badlogic.gdx.graphics.f;
import com.badlogic.gdx.graphics.n;
import com.ptowngames.jigsaur.Jigsaur;

public class ac {
    private static /* synthetic */ boolean d = (!ac.class.desiredAssertionStatus());
    private f a;
    private b b = f.b().a(g.e.b("data/europlac_wood_texture.jpg"));
    private boolean c = false;

    private static void a(float[] fArr, int i, boolean z, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8) {
        float f9;
        float f10;
        float f11;
        float f12 = 1.0f;
        int i2 = i + 1;
        fArr[i] = f;
        int i3 = i2 + 1;
        fArr[i2] = f2;
        int i4 = i3 + 1;
        fArr[i3] = f3;
        int i5 = i4 + 1;
        fArr[i4] = 0.0f;
        int i6 = i5 + 1;
        fArr[i5] = f4;
        int i7 = i6 + 1;
        fArr[i6] = z ? f : f5;
        int i8 = i7 + 1;
        if (z) {
            f9 = f6;
        } else {
            f9 = f2;
        }
        fArr[i7] = f9;
        int i9 = i8 + 1;
        fArr[i8] = f7;
        int i10 = i9 + 1;
        if (z) {
            f10 = 0.0f;
        } else {
            f10 = 1.0f;
        }
        fArr[i9] = f10;
        int i11 = i10 + 1;
        if (z) {
            f11 = f8;
        } else {
            f11 = f4;
        }
        fArr[i10] = f11;
        int i12 = i11 + 1;
        fArr[i11] = f5;
        int i13 = i12 + 1;
        fArr[i12] = f6;
        int i14 = i13 + 1;
        fArr[i13] = f7;
        int i15 = i14 + 1;
        fArr[i14] = 1.0f;
        int i16 = i15 + 1;
        fArr[i15] = f8;
        int i17 = i16 + 1;
        fArr[i16] = f5;
        int i18 = i17 + 1;
        fArr[i17] = f6;
        int i19 = i18 + 1;
        fArr[i18] = f7;
        int i20 = i19 + 1;
        fArr[i19] = 1.0f;
        int i21 = i20 + 1;
        fArr[i20] = f8;
        int i22 = i21 + 1;
        if (!z) {
            f5 = f;
        }
        fArr[i21] = f5;
        int i23 = i22 + 1;
        if (z) {
            f6 = f2;
        }
        fArr[i22] = f6;
        int i24 = i23 + 1;
        fArr[i23] = f3;
        int i25 = i24 + 1;
        if (!z) {
            f12 = 0.0f;
        }
        fArr[i24] = f12;
        int i26 = i25 + 1;
        if (z) {
            f8 = f4;
        }
        fArr[i25] = f8;
        int i27 = i26 + 1;
        fArr[i26] = f;
        int i28 = i27 + 1;
        fArr[i27] = f2;
        int i29 = i28 + 1;
        fArr[i28] = f3;
        fArr[i29] = 0.0f;
        fArr[i29 + 1] = f4;
    }

    public ac() {
        this.b.a(b.a.Repeat, b.a.Repeat);
        Jigsaur.Level h = o.a().h();
        float minX = h.getMinX();
        float minY = h.getMinY();
        float maxX = h.getMaxX();
        float maxY = h.getMaxY();
        float b2 = -5.0f - o.a().e().b();
        float[] fArr = new float[150];
        a(fArr, 0, true, minX, minY, b2, 0.5f, maxX, maxY, b2, 2.5f);
        a(fArr, 30, true, minX, maxY, b2, 0.0f, maxX, maxY, b2 - 0.25f, 1.0f);
        a(fArr, 60, false, maxX, maxY, b2, 0.0f, maxX, minY, b2 - 0.25f, 1.0f);
        a(fArr, 90, true, maxX, minY, b2, 0.0f, minX, minY, b2 - 0.25f, 1.0f);
        a(fArr, 120, false, minX, minY, b2, 0.0f, minX, maxY, b2 - 0.25f, 1.0f);
        short[] sArr = new short[30];
        for (int i = 0; i < 30; i++) {
            sArr[i] = (short) i;
        }
        this.a = f.b().a(30, 30, new n(0, 3, "table_pos"), new n(3, 2, "table_uv"));
        this.a.a(fArr);
        this.a.a(sArr);
    }

    public final void a() {
        if (d || !this.c) {
            this.b.a();
            this.a.a(4, 0, 30);
            return;
        }
        throw new AssertionError();
    }

    public final void b() {
        this.c = true;
    }
}
