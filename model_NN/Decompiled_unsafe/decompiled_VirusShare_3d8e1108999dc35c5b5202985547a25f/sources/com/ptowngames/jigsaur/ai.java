package com.ptowngames.jigsaur;

import com.google.protobuf.GeneratedMessage;
import com.google.protobuf.c;
import com.google.protobuf.f;
import com.ptowngames.jigsaur.Jigsaur;

class ai implements c.b.a {
    ai() {
    }

    public final f a(c.b bVar) {
        c.b unused = Jigsaur.M = bVar;
        c.a unused2 = Jigsaur.a = Jigsaur.a().d().get(0);
        GeneratedMessage.FieldAccessorTable unused3 = Jigsaur.b = new GeneratedMessage.FieldAccessorTable(Jigsaur.a, new String[]{"X", "Y"}, Jigsaur.PolygonVertex.class, Jigsaur.PolygonVertex.Builder.class);
        c.a unused4 = Jigsaur.c = Jigsaur.a().d().get(1);
        GeneratedMessage.FieldAccessorTable unused5 = Jigsaur.d = new GeneratedMessage.FieldAccessorTable(Jigsaur.c, new String[]{"X", "Y", "Z"}, Jigsaur.Vertex3.class, Jigsaur.Vertex3.Builder.class);
        c.a unused6 = Jigsaur.e = Jigsaur.a().d().get(2);
        GeneratedMessage.FieldAccessorTable unused7 = Jigsaur.f = new GeneratedMessage.FieldAccessorTable(Jigsaur.e, new String[]{"Xoff", "Yoff", "Scale", "NorthJoinPriority", "EastJoinPriority", "SouthJoinPriority", "WestJoinPriority"}, Jigsaur.JoinCorner.class, Jigsaur.JoinCorner.Builder.class);
        c.a unused8 = Jigsaur.g = Jigsaur.a().d().get(3);
        GeneratedMessage.FieldAccessorTable unused9 = Jigsaur.h = new GeneratedMessage.FieldAccessorTable(Jigsaur.g, new String[]{"JoinCircleCenter", "BaseOrientation", "NeighborId", "OuttieStatus"}, Jigsaur.JoinInfo.class, Jigsaur.JoinInfo.Builder.class);
        c.a unused10 = Jigsaur.i = Jigsaur.a().d().get(4);
        GeneratedMessage.FieldAccessorTable unused11 = Jigsaur.j = new GeneratedMessage.FieldAccessorTable(Jigsaur.i, new String[]{"Id", "PolygonIndices", "ExtrudedPolygonIndices", "JoinInfos", "TriangulationIndices", "CornerPoints"}, Jigsaur.Piece.class, Jigsaur.Piece.Builder.class);
        c.a unused12 = Jigsaur.k = Jigsaur.a().d().get(5);
        GeneratedMessage.FieldAccessorTable unused13 = Jigsaur.l = new GeneratedMessage.FieldAccessorTable(Jigsaur.k, new String[]{"Pieces", "PieceVertices", "NumTotalIndices", "MaximumJoinDistance", "Unused1", "Unused2", "ExtrusionDepth"}, Jigsaur.PieceSet.class, Jigsaur.PieceSet.Builder.class);
        c.a unused14 = Jigsaur.m = Jigsaur.a().d().get(6);
        GeneratedMessage.FieldAccessorTable unused15 = Jigsaur.n = new GeneratedMessage.FieldAccessorTable(Jigsaur.m, new String[]{"PieceSetFileNamePrefix", "PieceSetType"}, Jigsaur.PieceSetStub.class, Jigsaur.PieceSetStub.Builder.class);
        c.a unused16 = Jigsaur.o = Jigsaur.a().d().get(7);
        GeneratedMessage.FieldAccessorTable unused17 = Jigsaur.p = new GeneratedMessage.FieldAccessorTable(Jigsaur.o, new String[]{"Stubs"}, Jigsaur.PieceSetStubs.class, Jigsaur.PieceSetStubs.Builder.class);
        c.a unused18 = Jigsaur.q = Jigsaur.a().d().get(8);
        GeneratedMessage.FieldAccessorTable unused19 = Jigsaur.r = new GeneratedMessage.FieldAccessorTable(Jigsaur.q, new String[]{"NumVerticals", "NumHorizontals", "GridPoints", "JoinCorners", "ExtrusionDepth", "BoardWidth", "BoardHeight"}, Jigsaur.TypeOnePrimordialPieceSet.class, Jigsaur.TypeOnePrimordialPieceSet.Builder.class);
        c.a unused20 = Jigsaur.s = Jigsaur.a().d().get(9);
        GeneratedMessage.FieldAccessorTable unused21 = Jigsaur.t = new GeneratedMessage.FieldAccessorTable(Jigsaur.s, new String[]{"PieceIds", "Center", "Angle", "State"}, Jigsaur.PieceGlobConfiguration.class, Jigsaur.PieceGlobConfiguration.Builder.class);
        c.a unused22 = Jigsaur.u = Jigsaur.a().d().get(10);
        GeneratedMessage.FieldAccessorTable unused23 = Jigsaur.v = new GeneratedMessage.FieldAccessorTable(Jigsaur.u, new String[]{"Globs"}, Jigsaur.WorkspaceConfiguration.class, Jigsaur.WorkspaceConfiguration.Builder.class);
        c.a unused24 = Jigsaur.w = Jigsaur.a().d().get(11);
        GeneratedMessage.FieldAccessorTable unused25 = Jigsaur.x = new GeneratedMessage.FieldAccessorTable(Jigsaur.w, new String[]{"NumPieces", "Workspaces", "CameraCenter", "ScaleFactor"}, Jigsaur.SavedConfiguration.class, Jigsaur.SavedConfiguration.Builder.class);
        c.a unused26 = Jigsaur.y = Jigsaur.a().d().get(12);
        GeneratedMessage.FieldAccessorTable unused27 = Jigsaur.z = new GeneratedMessage.FieldAccessorTable(Jigsaur.y, new String[]{"Type", "NumFrames", "Fps", "ResourceName", "Attribution", "Acquisition", "TimeBegin", "TimeEnd", "Uid", "PuzzleImageWidth", "PuzzleImageHeight"}, Jigsaur.PuzzleImageInfo.class, Jigsaur.PuzzleImageInfo.Builder.class);
        c.a unused28 = Jigsaur.A = Jigsaur.a().d().get(13);
        GeneratedMessage.FieldAccessorTable unused29 = Jigsaur.B = new GeneratedMessage.FieldAccessorTable(Jigsaur.A, new String[]{"PuzzleImageInfos"}, Jigsaur.PuzzleImageInfos.class, Jigsaur.PuzzleImageInfos.Builder.class);
        c.a unused30 = Jigsaur.C = Jigsaur.a().d().get(14);
        GeneratedMessage.FieldAccessorTable unused31 = Jigsaur.D = new GeneratedMessage.FieldAccessorTable(Jigsaur.C, new String[]{"Type", "PriorLevelId", "CompletionPercentage"}, Jigsaur.LevelPredicate.class, Jigsaur.LevelPredicate.Builder.class);
        c.a unused32 = Jigsaur.E = Jigsaur.a().d().get(15);
        GeneratedMessage.FieldAccessorTable unused33 = Jigsaur.F = new GeneratedMessage.FieldAccessorTable(Jigsaur.E, new String[]{"Uid", "PieceSetType", "ImageInfoUid", "StartingConfiguration", "MinZoom", "MaxZoom", "MinX", "MaxX", "MinY", "MaxY", "InternalName", "NextLevel", "PrevLevel", "DifficultyFamily", "IsPremium", "Predices"}, Jigsaur.Level.class, Jigsaur.Level.Builder.class);
        c.a unused34 = Jigsaur.G = Jigsaur.a().d().get(16);
        GeneratedMessage.FieldAccessorTable unused35 = Jigsaur.H = new GeneratedMessage.FieldAccessorTable(Jigsaur.G, new String[]{"DetailLevel", "DoAutoJoining", "ToneDeafness", "BlinkOnWin", "BlinkOnWinSeconds", "ShowSolution", "EnableAccelerometer", "DisplayZoomSlider", "UserId", "LastLevelPlayed", "TimestampFirstPlayed", "StartupCount", "SoundEnabled"}, Jigsaur.Settings.class, Jigsaur.Settings.Builder.class);
        c.a unused36 = Jigsaur.I = Jigsaur.a().d().get(17);
        GeneratedMessage.FieldAccessorTable unused37 = Jigsaur.J = new GeneratedMessage.FieldAccessorTable(Jigsaur.I, new String[]{"Type", "Pointer", "X1", "Y1", "X2", "Y2", "TimeStart", "TimeEnd", "Angle"}, Jigsaur.VirtualSequence.class, Jigsaur.VirtualSequence.Builder.class);
        c.a unused38 = Jigsaur.K = Jigsaur.a().d().get(18);
        GeneratedMessage.FieldAccessorTable unused39 = Jigsaur.L = new GeneratedMessage.FieldAccessorTable(Jigsaur.K, new String[]{"Sequences"}, Jigsaur.VirtualSequences.class, Jigsaur.VirtualSequences.Builder.class);
        return null;
    }
}
