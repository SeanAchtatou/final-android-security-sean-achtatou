package com.ptowngames.jigsaur;

import com.badlogic.gdx.g;
import com.badlogic.gdx.graphics.f;
import com.badlogic.gdx.graphics.n;
import org.codehaus.jackson.util.BufferRecycler;

public final class aj implements aa {
    private static /* synthetic */ boolean i = (!aj.class.desiredAssertionStatus());
    private final f a = f.b().a((int) BufferRecycler.DEFAULT_WRITE_CONCAT_BUFFER_LEN, (int) BufferRecycler.DEFAULT_WRITE_CONCAT_BUFFER_LEN, new n(0, 3, ""));
    private final float[] b = new float[6000];
    private final short[] c = new short[BufferRecycler.DEFAULT_WRITE_CONCAT_BUFFER_LEN];
    private int d = 0;
    private final f e;
    private final float[] f = new float[156];
    private final short[] g = new short[52];
    private int h = 0;

    public aj() {
        for (int i2 = 0; i2 < this.c.length; i2++) {
            this.c[i2] = (short) i2;
            this.b[i2 * 3] = 0.0f;
            this.b[(i2 * 3) + 1] = 0.0f;
            this.b[(i2 * 3) + 2] = 0.0f;
        }
        this.d = 0;
        this.a.a(this.b);
        this.a.a(this.c);
        this.e = f.b().a(52, 52, new n(0, 3, ""));
        for (int i3 = 0; i3 < this.g.length; i3++) {
            this.g[i3] = (short) i3;
            this.f[i3 * 3] = 0.0f;
            this.f[(i3 * 3) + 1] = 0.0f;
            this.f[(i3 * 3) + 2] = 0.0f;
        }
        this.h = 0;
        this.e.a(this.f);
        this.e.a(this.g);
    }

    public final void a() {
        g.g.b();
    }

    public final void b() {
        c();
        g.g.a();
    }

    public final void a(float f2, float f3) {
        c();
        g.g.a(f2, f3);
    }

    public final void a(float f2) {
        c();
        g.g.a((float) ((((double) f2) * 180.0d) / 3.141592653589793d));
    }

    public final void a(com.badlogic.gdx.math.f fVar) {
        c();
        g.g.a(fVar.a, fVar.b, fVar.c);
    }

    public final void a(float f2, float f3, float f4, float f5) {
        c();
        g.g.a(f2, f3, f4, f5);
    }

    public final void c() {
        if (this.d != 0) {
            synchronized (this.a) {
                this.a.a(this.b);
                this.a.a(1, 0, this.d / 3);
                this.d = 0;
            }
        }
    }

    private void c(float f2, float f3, float f4, float f5) {
        if (this.d >= this.b.length - 6) {
            c();
            if (!i && this.d != 0) {
                throw new AssertionError();
            }
        }
        synchronized (this.a) {
            this.b[this.d] = f2;
            this.b[this.d + 1] = f3;
            this.b[this.d + 3] = f4;
            this.b[this.d + 4] = f5;
            this.d += 6;
        }
    }

    public final void b(float f2, float f3, float f4, float f5) {
        c(f2, f3, f4, f3);
        c(f4, f3, f4, f5);
        c(f4, f5, f2, f5);
        c(f2, f5, f2, f3);
    }

    public final void b(float f2) {
        if (i || this.h == 0) {
            float[] fArr = this.f;
            int i2 = this.h;
            this.h = i2 + 1;
            fArr[i2] = 0.0f;
            float[] fArr2 = this.f;
            int i3 = this.h;
            this.h = i3 + 1;
            fArr2[i3] = 0.0f;
            this.h++;
            for (int i4 = 0; i4 <= 20; i4++) {
                double d2 = ((((double) i4) * 2.0d) * 3.141592653589793d) / 20.0d;
                double sin = Math.sin(d2);
                float[] fArr3 = this.f;
                int i5 = this.h;
                this.h = i5 + 1;
                fArr3[i5] = (float) (((double) f2) * Math.cos(d2));
                float[] fArr4 = this.f;
                int i6 = this.h;
                this.h = i6 + 1;
                fArr4[i6] = (float) (sin * ((double) f2));
                this.h++;
            }
            this.e.a(this.f);
            g.g.glDepthFunc(519);
            this.e.a(6, 0, this.h / 3);
            g.g.glDepthFunc(513);
            this.h = 0;
            return;
        }
        throw new AssertionError();
    }
}
