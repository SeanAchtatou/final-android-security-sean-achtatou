package com.ptowngames.jigsaur;

import com.badlogic.gdx.g;
import com.badlogic.gdx.graphics.b;
import com.badlogic.gdx.graphics.f;
import com.badlogic.gdx.graphics.n;

public class ah extends an {
    private static f f;
    private static float[] k = {1.0f, 1.0f, 1.0f, 1.0f};
    private static /* synthetic */ boolean l = (!ah.class.desiredAssertionStatus());
    private b g;
    private boolean h;
    private float i;
    private boolean j;

    static {
        float[] fArr = new float[260];
        short[] sArr = new short[52];
        fArr[0] = 0.0f;
        fArr[1] = 0.0f;
        fArr[2] = 0.0f;
        fArr[3] = 0.5f;
        fArr[4] = 0.5f;
        sArr[0] = 0;
        for (int i2 = 0; i2 <= 50; i2++) {
            int i3 = (i2 + 1) * 5;
            double d = ((((double) i2) * 2.0d) * 3.141592653589793d) / 50.0d;
            float cos = (float) Math.cos(d);
            float sin = (float) Math.sin(d);
            int i4 = i3 + 1;
            fArr[i3] = cos;
            int i5 = i4 + 1;
            fArr[i4] = sin;
            int i6 = i5 + 1;
            fArr[i5] = 0.0f;
            fArr[i6] = (cos * 0.5f) + 0.5f;
            fArr[i6 + 1] = 1.0f - ((sin * 0.5f) + 0.5f);
            sArr[i2 + 1] = (short) (i2 + 1);
        }
        f fVar = new f(true, 52, 52, new n(0, 3, "cid_pos"), new n(3, 2, "cid_uv"));
        fVar.a(fArr);
        fVar.a(sArr);
        f = fVar;
    }

    public ah(b bVar) {
        super(1.0f);
        if (l || bVar != null) {
            this.g = bVar;
            this.h = false;
            this.j = false;
            this.i = 1.0f;
            return;
        }
        throw new AssertionError();
    }

    public final void a(boolean z) {
        this.h = z;
    }

    public final void c(float f2) {
        if (l || (f2 >= 0.0f && f2 <= 1.0f)) {
            this.i = f2;
            return;
        }
        throw new AssertionError();
    }

    public final boolean p() {
        return this.j;
    }

    public final void b(boolean z) {
        this.j = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public final void a(aa aaVar) {
        float f2;
        aa aaVar2;
        float min;
        float f3;
        float f4;
        aa aaVar3;
        float f5 = 1.0f;
        float f6 = 0.5f;
        this.g.a();
        aaVar.a();
        aaVar.a(j());
        aaVar.a(this.a, this.a);
        if (!this.j && this.h) {
            g.g.b(3042.0f);
            g.g.b(k);
            f5 = 0.5f;
            f2 = 0.5f;
            aaVar2 = aaVar;
        } else if (this.j) {
            min = Math.min(this.i, 0.25f);
            f3 = 1.0f;
            f4 = 1.0f;
            aaVar3 = aaVar;
            aaVar3.a(f4, f3, f5, min);
            f.a(6);
            if (!this.j && this.h) {
                g.g.b(8448.0f);
            }
            aaVar.b();
        } else {
            f6 = 1.0f;
            f2 = 1.0f;
            aaVar2 = aaVar;
        }
        aaVar3 = aaVar2;
        f4 = f2;
        f3 = f5;
        f5 = f6;
        min = this.i;
        aaVar3.a(f4, f3, f5, min);
        f.a(6);
        g.g.b(8448.0f);
        aaVar.b();
    }
}
