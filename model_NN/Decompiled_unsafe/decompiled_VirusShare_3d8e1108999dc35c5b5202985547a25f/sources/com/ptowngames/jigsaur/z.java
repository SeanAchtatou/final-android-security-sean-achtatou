package com.ptowngames.jigsaur;

class z {
    private static Throwable a;

    private z() throws Throwable {
        throw a;
    }

    public static synchronized void a(Throwable th) {
        synchronized (z.class) {
            a = th;
            try {
                z.class.newInstance();
                a = null;
            } catch (InstantiationException e) {
                a = null;
            } catch (IllegalAccessException e2) {
                a = null;
            } catch (Throwable th2) {
                a = null;
                throw th2;
            }
        }
        return;
    }
}
