package com.ptowngames.jigsaur;

import java.util.Collection;

public final class y extends x {
    public final /* bridge */ /* synthetic */ boolean add(Object obj) {
        throw new UnsupportedOperationException();
    }

    public y(x xVar) {
        super(xVar);
    }

    public final boolean a(j jVar) {
        throw new UnsupportedOperationException();
    }

    public final boolean addAll(Collection<? extends j> collection) {
        throw new UnsupportedOperationException();
    }

    public final void clear() {
        throw new UnsupportedOperationException();
    }

    public final boolean remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    public final boolean removeAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }

    public final boolean retainAll(Collection<?> collection) {
        throw new UnsupportedOperationException();
    }
}
