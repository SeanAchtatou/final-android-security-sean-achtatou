package com.ptowngames.jigsaur;

import com.badlogic.gdx.g;
import com.badlogic.gdx.graphics.c;
import com.badlogic.gdx.graphics.n;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import java.util.ArrayList;
import java.util.Iterator;

public class f {
    private static f a = null;
    private static /* synthetic */ boolean f;
    private ArrayList<a> b = new ArrayList<>();
    private ArrayList<b> c = new ArrayList<>();
    private ArrayList<c> d = new ArrayList<>();
    private ArrayList<com.badlogic.gdx.utils.c> e = new ArrayList<>();

    static {
        boolean z;
        if (!f.class.desiredAssertionStatus()) {
            z = true;
        } else {
            z = false;
        }
        f = z;
    }

    public class d extends Error {
        public d(String str) {
            super(str);
        }
    }

    class a extends com.badlogic.gdx.graphics.b {
        private static /* synthetic */ boolean b = (!f.class.desiredAssertionStatus());
        private boolean a;

        /* synthetic */ a(f fVar, com.badlogic.gdx.c.a aVar, c.a aVar2) {
            this(aVar, aVar2, (byte) 0);
        }

        /* synthetic */ a(f fVar, com.badlogic.gdx.graphics.c cVar) {
            this(cVar, (byte) 0);
        }

        private a(com.badlogic.gdx.c.a aVar, c.a aVar2, byte b2) {
            super(aVar, aVar2);
            this.a = false;
        }

        private a(com.badlogic.gdx.graphics.c cVar, byte b2) {
            super(cVar);
            this.a = false;
        }

        public final void d() {
            if (b || !this.a) {
                super.d();
                this.a = true;
                return;
            }
            throw new AssertionError();
        }

        public final boolean g() {
            return this.a;
        }
    }

    class b extends com.badlogic.gdx.graphics.f {
        private static /* synthetic */ boolean b = (!f.class.desiredAssertionStatus());
        private boolean a = false;

        public b(int i, int i2, n nVar) {
            super(false, i, i2, nVar);
        }

        public b(int i, int i2, n... nVarArr) {
            super(true, i, i2, nVarArr);
        }

        public final void a() {
            if (b || !this.a) {
                super.a();
                this.a = true;
                return;
            }
            throw new AssertionError();
        }

        public final boolean b() {
            return this.a;
        }
    }

    class c extends PolygonShape implements com.badlogic.gdx.utils.c {
        private static /* synthetic */ boolean b = (!f.class.desiredAssertionStatus());
        private boolean a = false;

        public c() {
        }

        public final void dispose() {
            if (b || !this.a) {
                super.dispose();
                this.a = true;
                return;
            }
            throw new AssertionError();
        }

        public final boolean a() {
            return this.a;
        }
    }

    public static void a() {
        if (a != null && !a.e()) {
            a.d();
            if (!f && a.e.size() != 0) {
                throw new AssertionError();
            }
        }
        a = new f();
    }

    public static f b() {
        return a;
    }

    protected f() {
    }

    public final com.badlogic.gdx.graphics.b a(String str) {
        return a(g.e.b(str));
    }

    public final com.badlogic.gdx.graphics.b a(com.badlogic.gdx.c.a aVar) {
        return a(aVar, c.a.RGB565);
    }

    public final com.badlogic.gdx.graphics.b a(com.badlogic.gdx.c.a aVar, c.a aVar2) {
        try {
            a aVar3 = new a(this, aVar, aVar2);
            this.b.add(aVar3);
            return aVar3;
        } catch (com.badlogic.gdx.utils.f e2) {
            System.err.println("Ran into a GdxRuntimeException: " + e2);
            throw new d("(ran into GdxRuntimeException)");
        }
    }

    public final com.badlogic.gdx.graphics.b a(com.badlogic.gdx.graphics.c cVar) {
        try {
            a aVar = new a(this, cVar);
            this.b.add(aVar);
            return aVar;
        } catch (com.badlogic.gdx.utils.f e2) {
            System.err.println("Ran into a GdxRuntimeException: " + e2);
            throw new d("(ran into GdxRuntimeException)");
        }
    }

    public final com.badlogic.gdx.graphics.f a(int i, int i2, n nVar) {
        try {
            b bVar = new b(i, i2, nVar);
            this.c.add(bVar);
            return bVar;
        } catch (com.badlogic.gdx.utils.f e2) {
            System.err.println("Ran into a GdxRuntimeException: " + e2);
            throw new d("(ran into GdxRuntimeException)");
        }
    }

    public final com.badlogic.gdx.graphics.f a(int i, int i2, n... nVarArr) {
        try {
            b bVar = new b(i, i2, nVarArr);
            this.c.add(bVar);
            return bVar;
        } catch (com.badlogic.gdx.utils.f e2) {
            System.err.println("Ran into a GdxRuntimeException: " + e2);
            throw new d("(ran into GdxRuntimeException)");
        }
    }

    public final c c() {
        c cVar = new c();
        this.d.add(cVar);
        return cVar;
    }

    public final void d() {
        "NannyBot disposing " + this.b.size() + " textures, " + this.c.size() + " meshes, and " + this.d.size() + " PolygonShapes.";
        Iterator<a> it = this.b.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!next.g()) {
                next.d();
            }
        }
        this.b.clear();
        Iterator<b> it2 = this.c.iterator();
        while (it2.hasNext()) {
            b next2 = it2.next();
            if (!next2.b()) {
                next2.a();
            }
        }
        this.c.clear();
        Iterator<c> it3 = this.d.iterator();
        while (it3.hasNext()) {
            c next3 = it3.next();
            if (!next3.a()) {
                next3.dispose();
            }
        }
        this.d.clear();
    }

    public final void a(com.badlogic.gdx.utils.c cVar) {
        this.e.add(cVar);
    }

    public final boolean b(com.badlogic.gdx.utils.c cVar) {
        return this.e.remove(cVar);
    }

    public final boolean e() {
        return this.c.size() == 0 && this.b.size() == 0 && this.d.size() == 0 && this.e.size() == 0;
    }
}
