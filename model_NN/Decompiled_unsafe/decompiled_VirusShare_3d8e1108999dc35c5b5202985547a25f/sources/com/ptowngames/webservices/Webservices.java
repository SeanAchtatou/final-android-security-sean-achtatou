package com.ptowngames.webservices;

import com.badlogic.gdx.physics.box2d.Transform;
import com.google.protobuf.DescriptorProtos;
import com.google.protobuf.GeneratedMessage;
import com.google.protobuf.a;
import com.google.protobuf.aa;
import com.google.protobuf.ab;
import com.google.protobuf.ac;
import com.google.protobuf.c;
import com.google.protobuf.g;
import com.google.protobuf.h;
import com.google.protobuf.q;
import com.google.protobuf.s;
import com.google.protobuf.v;
import com.google.protobuf.x;
import com.google.protobuf.z;
import java.io.IOException;
import java.io.InputStream;

public final class Webservices {
    /* access modifiers changed from: private */
    public static c.a a;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable b;
    /* access modifiers changed from: private */
    public static c.a c;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable d;
    /* access modifiers changed from: private */
    public static c.a e;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable f;
    /* access modifiers changed from: private */
    public static c.a g;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable h;
    /* access modifiers changed from: private */
    public static c.a i;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable j;
    /* access modifiers changed from: private */
    public static c.a k;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable l;
    /* access modifiers changed from: private */
    public static c.b m;

    private Webservices() {
    }

    public static final class ValidateCodeRequest extends GeneratedMessage {
        public static final int CODE_FIELD_NUMBER = 1;
        public static final int PRODUCT_ID_FIELD_NUMBER = 2;
        private static final ValidateCodeRequest defaultInstance = new ValidateCodeRequest(true);
        /* access modifiers changed from: private */
        public String code_;
        /* access modifiers changed from: private */
        public boolean hasCode;
        /* access modifiers changed from: private */
        public boolean hasProductId;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public int productId_;

        /* synthetic */ ValidateCodeRequest(a aVar) {
            this();
        }

        private ValidateCodeRequest() {
            this.code_ = "";
            this.productId_ = 0;
            this.memoizedSerializedSize = -1;
        }

        private ValidateCodeRequest(boolean z) {
            this.code_ = "";
            this.productId_ = 0;
            this.memoizedSerializedSize = -1;
        }

        public static ValidateCodeRequest getDefaultInstance() {
            return defaultInstance;
        }

        public final ValidateCodeRequest getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Webservices.a;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Webservices.b;
        }

        public final boolean hasCode() {
            return this.hasCode;
        }

        public final String getCode() {
            return this.code_;
        }

        public final boolean hasProductId() {
            return this.hasProductId;
        }

        public final int getProductId() {
            return this.productId_;
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            if (this.hasCode && this.hasProductId) {
                return true;
            }
            return false;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasCode()) {
                xVar.a(1, getCode());
            }
            if (hasProductId()) {
                xVar.b(2, getProductId());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (hasCode()) {
                i2 = x.b(1, getCode()) + 0;
            }
            if (hasProductId()) {
                i2 += x.e(2, getProductId());
            }
            int serializedSize = i2 + getUnknownFields().getSerializedSize();
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static ValidateCodeRequest parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.webservices.Webservices.ValidateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.webservices.Webservices.ValidateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.ValidateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.webservices.Webservices.ValidateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.webservices.Webservices.ValidateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$ValidateCodeRequest$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static ValidateCodeRequest parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static ValidateCodeRequest parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.webservices.Webservices.ValidateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.webservices.Webservices.ValidateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.ValidateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.webservices.Webservices.ValidateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.webservices.Webservices.ValidateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$ValidateCodeRequest$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static ValidateCodeRequest parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static ValidateCodeRequest parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.webservices.Webservices.ValidateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.webservices.Webservices.ValidateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.ValidateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.webservices.Webservices.ValidateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.webservices.Webservices.ValidateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$ValidateCodeRequest$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static ValidateCodeRequest parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static ValidateCodeRequest parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static ValidateCodeRequest parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static ValidateCodeRequest parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.webservices.Webservices.ValidateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$ValidateCodeRequest$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.webservices.Webservices.ValidateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.webservices.Webservices.ValidateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.ValidateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.webservices.Webservices.ValidateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.ValidateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$ValidateCodeRequest$Builder */
        public static ValidateCodeRequest parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(ValidateCodeRequest validateCodeRequest) {
            return newBuilder().mergeFrom(validateCodeRequest);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private ValidateCodeRequest result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new ValidateCodeRequest((a) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final ValidateCodeRequest internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new ValidateCodeRequest((a) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return ValidateCodeRequest.getDescriptor();
            }

            public final ValidateCodeRequest getDefaultInstanceForType() {
                return ValidateCodeRequest.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final ValidateCodeRequest build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public ValidateCodeRequest buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final ValidateCodeRequest buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                ValidateCodeRequest validateCodeRequest = this.result;
                this.result = null;
                return validateCodeRequest;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof ValidateCodeRequest) {
                    return mergeFrom((ValidateCodeRequest) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(ValidateCodeRequest validateCodeRequest) {
                if (validateCodeRequest != ValidateCodeRequest.getDefaultInstance()) {
                    if (validateCodeRequest.hasCode()) {
                        setCode(validateCodeRequest.getCode());
                    }
                    if (validateCodeRequest.hasProductId()) {
                        setProductId(validateCodeRequest.getProductId());
                    }
                    mergeUnknownFields(validateCodeRequest.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = com.google.protobuf.a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 10:
                            setCode(hVar.j());
                            break;
                        case 16:
                            setProductId(hVar.l());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasCode() {
                return this.result.hasCode();
            }

            public final String getCode() {
                return this.result.getCode();
            }

            public final Builder setCode(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasCode = true;
                String unused2 = this.result.code_ = str;
                return this;
            }

            public final Builder clearCode() {
                boolean unused = this.result.hasCode = false;
                String unused2 = this.result.code_ = ValidateCodeRequest.getDefaultInstance().getCode();
                return this;
            }

            public final boolean hasProductId() {
                return this.result.hasProductId();
            }

            public final int getProductId() {
                return this.result.getProductId();
            }

            public final Builder setProductId(int i) {
                boolean unused = this.result.hasProductId = true;
                int unused2 = this.result.productId_ = i;
                return this;
            }

            public final Builder clearProductId() {
                boolean unused = this.result.hasProductId = false;
                int unused2 = this.result.productId_ = 0;
                return this;
            }
        }
    }

    public static final class ValidateCodeResponse extends GeneratedMessage {
        public static final int IS_VALID_FIELD_NUMBER = 1;
        private static final ValidateCodeResponse defaultInstance = new ValidateCodeResponse(true);
        /* access modifiers changed from: private */
        public boolean hasIsValid;
        /* access modifiers changed from: private */
        public boolean isValid_;
        private int memoizedSerializedSize;

        /* synthetic */ ValidateCodeResponse(a aVar) {
            this();
        }

        private ValidateCodeResponse() {
            this.isValid_ = false;
            this.memoizedSerializedSize = -1;
        }

        private ValidateCodeResponse(boolean z) {
            this.isValid_ = false;
            this.memoizedSerializedSize = -1;
        }

        public static ValidateCodeResponse getDefaultInstance() {
            return defaultInstance;
        }

        public final ValidateCodeResponse getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Webservices.c;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Webservices.d;
        }

        public final boolean hasIsValid() {
            return this.hasIsValid;
        }

        public final boolean getIsValid() {
            return this.isValid_;
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            if (!this.hasIsValid) {
                return false;
            }
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasIsValid()) {
                xVar.a(1, getIsValid());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (hasIsValid()) {
                getIsValid();
                i2 = x.e(1) + 0;
            }
            int serializedSize = i2 + getUnknownFields().getSerializedSize();
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static ValidateCodeResponse parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.webservices.Webservices.ValidateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.webservices.Webservices.ValidateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.ValidateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.webservices.Webservices.ValidateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.webservices.Webservices.ValidateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$ValidateCodeResponse$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static ValidateCodeResponse parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static ValidateCodeResponse parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.webservices.Webservices.ValidateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.webservices.Webservices.ValidateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.ValidateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.webservices.Webservices.ValidateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.webservices.Webservices.ValidateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$ValidateCodeResponse$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static ValidateCodeResponse parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static ValidateCodeResponse parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.webservices.Webservices.ValidateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.webservices.Webservices.ValidateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.ValidateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.webservices.Webservices.ValidateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.webservices.Webservices.ValidateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$ValidateCodeResponse$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static ValidateCodeResponse parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static ValidateCodeResponse parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static ValidateCodeResponse parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static ValidateCodeResponse parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.webservices.Webservices.ValidateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$ValidateCodeResponse$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.webservices.Webservices.ValidateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.webservices.Webservices.ValidateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.ValidateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.webservices.Webservices.ValidateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.ValidateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$ValidateCodeResponse$Builder */
        public static ValidateCodeResponse parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(ValidateCodeResponse validateCodeResponse) {
            return newBuilder().mergeFrom(validateCodeResponse);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private ValidateCodeResponse result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new ValidateCodeResponse((a) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final ValidateCodeResponse internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new ValidateCodeResponse((a) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return ValidateCodeResponse.getDescriptor();
            }

            public final ValidateCodeResponse getDefaultInstanceForType() {
                return ValidateCodeResponse.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final ValidateCodeResponse build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public ValidateCodeResponse buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final ValidateCodeResponse buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                ValidateCodeResponse validateCodeResponse = this.result;
                this.result = null;
                return validateCodeResponse;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof ValidateCodeResponse) {
                    return mergeFrom((ValidateCodeResponse) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(ValidateCodeResponse validateCodeResponse) {
                if (validateCodeResponse != ValidateCodeResponse.getDefaultInstance()) {
                    if (validateCodeResponse.hasIsValid()) {
                        setIsValid(validateCodeResponse.getIsValid());
                    }
                    mergeUnknownFields(validateCodeResponse.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = com.google.protobuf.a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 8:
                            setIsValid(hVar.i());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasIsValid() {
                return this.result.hasIsValid();
            }

            public final boolean getIsValid() {
                return this.result.getIsValid();
            }

            public final Builder setIsValid(boolean z) {
                boolean unused = this.result.hasIsValid = true;
                boolean unused2 = this.result.isValid_ = z;
                return this;
            }

            public final Builder clearIsValid() {
                boolean unused = this.result.hasIsValid = false;
                boolean unused2 = this.result.isValid_ = false;
                return this;
            }
        }
    }

    public static final class GenerateCodeRequest extends GeneratedMessage {
        public static final int PRODUCT_ID_FIELD_NUMBER = 1;
        private static final GenerateCodeRequest defaultInstance = new GenerateCodeRequest(true);
        /* access modifiers changed from: private */
        public boolean hasProductId;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public int productId_;

        /* synthetic */ GenerateCodeRequest(a aVar) {
            this();
        }

        private GenerateCodeRequest() {
            this.productId_ = 0;
            this.memoizedSerializedSize = -1;
        }

        private GenerateCodeRequest(boolean z) {
            this.productId_ = 0;
            this.memoizedSerializedSize = -1;
        }

        public static GenerateCodeRequest getDefaultInstance() {
            return defaultInstance;
        }

        public final GenerateCodeRequest getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Webservices.e;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Webservices.f;
        }

        public final boolean hasProductId() {
            return this.hasProductId;
        }

        public final int getProductId() {
            return this.productId_;
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            if (!this.hasProductId) {
                return false;
            }
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasProductId()) {
                xVar.b(1, getProductId());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (hasProductId()) {
                i2 = x.e(1, getProductId()) + 0;
            }
            int serializedSize = i2 + getUnknownFields().getSerializedSize();
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static GenerateCodeRequest parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.webservices.Webservices.GenerateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.webservices.Webservices.GenerateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.GenerateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.webservices.Webservices.GenerateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.webservices.Webservices.GenerateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$GenerateCodeRequest$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static GenerateCodeRequest parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static GenerateCodeRequest parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.webservices.Webservices.GenerateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.webservices.Webservices.GenerateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.GenerateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.webservices.Webservices.GenerateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.webservices.Webservices.GenerateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$GenerateCodeRequest$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static GenerateCodeRequest parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static GenerateCodeRequest parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.webservices.Webservices.GenerateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.webservices.Webservices.GenerateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.GenerateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.webservices.Webservices.GenerateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.webservices.Webservices.GenerateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$GenerateCodeRequest$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static GenerateCodeRequest parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static GenerateCodeRequest parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static GenerateCodeRequest parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static GenerateCodeRequest parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.webservices.Webservices.GenerateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$GenerateCodeRequest$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.webservices.Webservices.GenerateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.webservices.Webservices.GenerateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.GenerateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.webservices.Webservices.GenerateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.GenerateCodeRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$GenerateCodeRequest$Builder */
        public static GenerateCodeRequest parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(GenerateCodeRequest generateCodeRequest) {
            return newBuilder().mergeFrom(generateCodeRequest);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private GenerateCodeRequest result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new GenerateCodeRequest((a) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final GenerateCodeRequest internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new GenerateCodeRequest((a) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return GenerateCodeRequest.getDescriptor();
            }

            public final GenerateCodeRequest getDefaultInstanceForType() {
                return GenerateCodeRequest.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final GenerateCodeRequest build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public GenerateCodeRequest buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final GenerateCodeRequest buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                GenerateCodeRequest generateCodeRequest = this.result;
                this.result = null;
                return generateCodeRequest;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof GenerateCodeRequest) {
                    return mergeFrom((GenerateCodeRequest) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(GenerateCodeRequest generateCodeRequest) {
                if (generateCodeRequest != GenerateCodeRequest.getDefaultInstance()) {
                    if (generateCodeRequest.hasProductId()) {
                        setProductId(generateCodeRequest.getProductId());
                    }
                    mergeUnknownFields(generateCodeRequest.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = com.google.protobuf.a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 8:
                            setProductId(hVar.l());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasProductId() {
                return this.result.hasProductId();
            }

            public final int getProductId() {
                return this.result.getProductId();
            }

            public final Builder setProductId(int i) {
                boolean unused = this.result.hasProductId = true;
                int unused2 = this.result.productId_ = i;
                return this;
            }

            public final Builder clearProductId() {
                boolean unused = this.result.hasProductId = false;
                int unused2 = this.result.productId_ = 0;
                return this;
            }
        }
    }

    public static final class GenerateCodeResponse extends GeneratedMessage {
        public static final int CODE_FIELD_NUMBER = 1;
        public static final int ERROR_FIELD_NUMBER = 2;
        private static final GenerateCodeResponse defaultInstance = new GenerateCodeResponse(true);
        /* access modifiers changed from: private */
        public String code_;
        /* access modifiers changed from: private */
        public String error_;
        /* access modifiers changed from: private */
        public boolean hasCode;
        /* access modifiers changed from: private */
        public boolean hasError;
        private int memoizedSerializedSize;

        /* synthetic */ GenerateCodeResponse(a aVar) {
            this();
        }

        private GenerateCodeResponse() {
            this.code_ = "";
            this.error_ = "";
            this.memoizedSerializedSize = -1;
        }

        private GenerateCodeResponse(boolean z) {
            this.code_ = "";
            this.error_ = "";
            this.memoizedSerializedSize = -1;
        }

        public static GenerateCodeResponse getDefaultInstance() {
            return defaultInstance;
        }

        public final GenerateCodeResponse getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Webservices.g;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Webservices.h;
        }

        public final boolean hasCode() {
            return this.hasCode;
        }

        public final String getCode() {
            return this.code_;
        }

        public final boolean hasError() {
            return this.hasError;
        }

        public final String getError() {
            return this.error_;
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasCode()) {
                xVar.a(1, getCode());
            }
            if (hasError()) {
                xVar.a(2, getError());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (hasCode()) {
                i2 = x.b(1, getCode()) + 0;
            }
            if (hasError()) {
                i2 += x.b(2, getError());
            }
            int serializedSize = i2 + getUnknownFields().getSerializedSize();
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static GenerateCodeResponse parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.webservices.Webservices.GenerateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.webservices.Webservices.GenerateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.GenerateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.webservices.Webservices.GenerateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.webservices.Webservices.GenerateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$GenerateCodeResponse$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static GenerateCodeResponse parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static GenerateCodeResponse parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.webservices.Webservices.GenerateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.webservices.Webservices.GenerateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.GenerateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.webservices.Webservices.GenerateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.webservices.Webservices.GenerateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$GenerateCodeResponse$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static GenerateCodeResponse parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static GenerateCodeResponse parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.webservices.Webservices.GenerateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.webservices.Webservices.GenerateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.GenerateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.webservices.Webservices.GenerateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.webservices.Webservices.GenerateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$GenerateCodeResponse$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static GenerateCodeResponse parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static GenerateCodeResponse parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static GenerateCodeResponse parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static GenerateCodeResponse parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.webservices.Webservices.GenerateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$GenerateCodeResponse$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.webservices.Webservices.GenerateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.webservices.Webservices.GenerateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.GenerateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.webservices.Webservices.GenerateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.GenerateCodeResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$GenerateCodeResponse$Builder */
        public static GenerateCodeResponse parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(GenerateCodeResponse generateCodeResponse) {
            return newBuilder().mergeFrom(generateCodeResponse);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private GenerateCodeResponse result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new GenerateCodeResponse((a) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final GenerateCodeResponse internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new GenerateCodeResponse((a) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return GenerateCodeResponse.getDescriptor();
            }

            public final GenerateCodeResponse getDefaultInstanceForType() {
                return GenerateCodeResponse.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final GenerateCodeResponse build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public GenerateCodeResponse buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final GenerateCodeResponse buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                GenerateCodeResponse generateCodeResponse = this.result;
                this.result = null;
                return generateCodeResponse;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof GenerateCodeResponse) {
                    return mergeFrom((GenerateCodeResponse) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(GenerateCodeResponse generateCodeResponse) {
                if (generateCodeResponse != GenerateCodeResponse.getDefaultInstance()) {
                    if (generateCodeResponse.hasCode()) {
                        setCode(generateCodeResponse.getCode());
                    }
                    if (generateCodeResponse.hasError()) {
                        setError(generateCodeResponse.getError());
                    }
                    mergeUnknownFields(generateCodeResponse.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = com.google.protobuf.a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 10:
                            setCode(hVar.j());
                            break;
                        case DescriptorProtos.FileOptions.PY_GENERIC_SERVICES_FIELD_NUMBER:
                            setError(hVar.j());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasCode() {
                return this.result.hasCode();
            }

            public final String getCode() {
                return this.result.getCode();
            }

            public final Builder setCode(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasCode = true;
                String unused2 = this.result.code_ = str;
                return this;
            }

            public final Builder clearCode() {
                boolean unused = this.result.hasCode = false;
                String unused2 = this.result.code_ = GenerateCodeResponse.getDefaultInstance().getCode();
                return this;
            }

            public final boolean hasError() {
                return this.result.hasError();
            }

            public final String getError() {
                return this.result.getError();
            }

            public final Builder setError(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasError = true;
                String unused2 = this.result.error_ = str;
                return this;
            }

            public final Builder clearError() {
                boolean unused = this.result.hasError = false;
                String unused2 = this.result.error_ = GenerateCodeResponse.getDefaultInstance().getError();
                return this;
            }
        }
    }

    public static final class RewardCheckRequest extends GeneratedMessage {
        public static final int EVENT_CODE_FIELD_NUMBER = 5;
        public static final int EXTRA_DATA_FIELD_NUMBER = 6;
        public static final int LOCALE_FIELD_NUMBER = 2;
        public static final int PRODUCT_ID_FIELD_NUMBER = 3;
        public static final int SUBPRODUCT_ID_FIELD_NUMBER = 4;
        public static final int USER_ID_FIELD_NUMBER = 1;
        private static final RewardCheckRequest defaultInstance = new RewardCheckRequest(true);
        /* access modifiers changed from: private */
        public int eventCode_;
        /* access modifiers changed from: private */
        public String extraData_;
        /* access modifiers changed from: private */
        public boolean hasEventCode;
        /* access modifiers changed from: private */
        public boolean hasExtraData;
        /* access modifiers changed from: private */
        public boolean hasLocale;
        /* access modifiers changed from: private */
        public boolean hasProductId;
        /* access modifiers changed from: private */
        public boolean hasSubproductId;
        /* access modifiers changed from: private */
        public boolean hasUserId;
        /* access modifiers changed from: private */
        public String locale_;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public int productId_;
        /* access modifiers changed from: private */
        public int subproductId_;
        /* access modifiers changed from: private */
        public long userId_;

        /* synthetic */ RewardCheckRequest(a aVar) {
            this();
        }

        private RewardCheckRequest() {
            this.userId_ = 0;
            this.locale_ = "";
            this.productId_ = 0;
            this.subproductId_ = 0;
            this.eventCode_ = 0;
            this.extraData_ = "";
            this.memoizedSerializedSize = -1;
        }

        private RewardCheckRequest(boolean z) {
            this.userId_ = 0;
            this.locale_ = "";
            this.productId_ = 0;
            this.subproductId_ = 0;
            this.eventCode_ = 0;
            this.extraData_ = "";
            this.memoizedSerializedSize = -1;
        }

        public static RewardCheckRequest getDefaultInstance() {
            return defaultInstance;
        }

        public final RewardCheckRequest getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Webservices.i;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Webservices.j;
        }

        public enum a implements v {
            APP_STARTED(0, 0),
            LEVEL_SELECTED(1, 1),
            LOADING_FINISHED(2, 2),
            LEVEL_SOLVED(3, 3),
            APP_PAUSE(4, 4),
            APP_RESUME(5, 5),
            APP_DISPOSE(6, 6),
            FIRST_CAMERA_MOVED(7, 7),
            FIRST_PIECE_MOVED(8, 8),
            BUTTON_BAR_BUTTON(9, 9),
            PROGRAM_EXCEPTION(10, 10),
            EXTERNAL_LINK_CLICKED(11, 11);
            
            private static g.b<a> m = new b();
            private static final a[] n = {APP_STARTED, LEVEL_SELECTED, LOADING_FINISHED, LEVEL_SOLVED, APP_PAUSE, APP_RESUME, APP_DISPOSE, FIRST_CAMERA_MOVED, FIRST_PIECE_MOVED, BUTTON_BAR_BUTTON, PROGRAM_EXCEPTION, EXTERNAL_LINK_CLICKED};
            private final int o;
            private final int p;

            public final int d_() {
                return this.p;
            }

            public static a a(int i) {
                switch (i) {
                    case Transform.POS_X:
                        return APP_STARTED;
                    case 1:
                        return LEVEL_SELECTED;
                    case 2:
                        return LOADING_FINISHED;
                    case 3:
                        return LEVEL_SOLVED;
                    case 4:
                        return APP_PAUSE;
                    case 5:
                        return APP_RESUME;
                    case 6:
                        return APP_DISPOSE;
                    case 7:
                        return FIRST_CAMERA_MOVED;
                    case 8:
                        return FIRST_PIECE_MOVED;
                    case 9:
                        return BUTTON_BAR_BUTTON;
                    case 10:
                        return PROGRAM_EXCEPTION;
                    case 11:
                        return EXTERNAL_LINK_CLICKED;
                    default:
                        return null;
                }
            }

            private a(int i, int i2) {
                this.o = i;
                this.p = i2;
            }
        }

        public final boolean hasUserId() {
            return this.hasUserId;
        }

        public final long getUserId() {
            return this.userId_;
        }

        public final boolean hasLocale() {
            return this.hasLocale;
        }

        public final String getLocale() {
            return this.locale_;
        }

        public final boolean hasProductId() {
            return this.hasProductId;
        }

        public final int getProductId() {
            return this.productId_;
        }

        public final boolean hasSubproductId() {
            return this.hasSubproductId;
        }

        public final int getSubproductId() {
            return this.subproductId_;
        }

        public final boolean hasEventCode() {
            return this.hasEventCode;
        }

        public final int getEventCode() {
            return this.eventCode_;
        }

        public final boolean hasExtraData() {
            return this.hasExtraData;
        }

        public final String getExtraData() {
            return this.extraData_;
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            if (!this.hasUserId) {
                return false;
            }
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasUserId()) {
                xVar.a(1, getUserId());
            }
            if (hasLocale()) {
                xVar.a(2, getLocale());
            }
            if (hasProductId()) {
                xVar.b(3, getProductId());
            }
            if (hasSubproductId()) {
                xVar.b(4, getSubproductId());
            }
            if (hasEventCode()) {
                xVar.b(5, getEventCode());
            }
            if (hasExtraData()) {
                xVar.a(6, getExtraData());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (hasUserId()) {
                i2 = x.b(1, getUserId()) + 0;
            }
            if (hasLocale()) {
                i2 += x.b(2, getLocale());
            }
            if (hasProductId()) {
                i2 += x.e(3, getProductId());
            }
            if (hasSubproductId()) {
                i2 += x.e(4, getSubproductId());
            }
            if (hasEventCode()) {
                i2 += x.e(5, getEventCode());
            }
            if (hasExtraData()) {
                i2 += x.b(6, getExtraData());
            }
            int serializedSize = i2 + getUnknownFields().getSerializedSize();
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static RewardCheckRequest parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.webservices.Webservices.RewardCheckRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.webservices.Webservices.RewardCheckRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.RewardCheckRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.webservices.Webservices.RewardCheckRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.webservices.Webservices.RewardCheckRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$RewardCheckRequest$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static RewardCheckRequest parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static RewardCheckRequest parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.webservices.Webservices.RewardCheckRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.webservices.Webservices.RewardCheckRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.RewardCheckRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.webservices.Webservices.RewardCheckRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.webservices.Webservices.RewardCheckRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$RewardCheckRequest$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static RewardCheckRequest parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static RewardCheckRequest parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.webservices.Webservices.RewardCheckRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.webservices.Webservices.RewardCheckRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.RewardCheckRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.webservices.Webservices.RewardCheckRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.webservices.Webservices.RewardCheckRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$RewardCheckRequest$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static RewardCheckRequest parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static RewardCheckRequest parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static RewardCheckRequest parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static RewardCheckRequest parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.webservices.Webservices.RewardCheckRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$RewardCheckRequest$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.webservices.Webservices.RewardCheckRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.webservices.Webservices.RewardCheckRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.RewardCheckRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.webservices.Webservices.RewardCheckRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.RewardCheckRequest.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$RewardCheckRequest$Builder */
        public static RewardCheckRequest parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(RewardCheckRequest rewardCheckRequest) {
            return newBuilder().mergeFrom(rewardCheckRequest);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private RewardCheckRequest result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new RewardCheckRequest((a) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final RewardCheckRequest internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new RewardCheckRequest((a) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return RewardCheckRequest.getDescriptor();
            }

            public final RewardCheckRequest getDefaultInstanceForType() {
                return RewardCheckRequest.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final RewardCheckRequest build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public RewardCheckRequest buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final RewardCheckRequest buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                RewardCheckRequest rewardCheckRequest = this.result;
                this.result = null;
                return rewardCheckRequest;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof RewardCheckRequest) {
                    return mergeFrom((RewardCheckRequest) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(RewardCheckRequest rewardCheckRequest) {
                if (rewardCheckRequest != RewardCheckRequest.getDefaultInstance()) {
                    if (rewardCheckRequest.hasUserId()) {
                        setUserId(rewardCheckRequest.getUserId());
                    }
                    if (rewardCheckRequest.hasLocale()) {
                        setLocale(rewardCheckRequest.getLocale());
                    }
                    if (rewardCheckRequest.hasProductId()) {
                        setProductId(rewardCheckRequest.getProductId());
                    }
                    if (rewardCheckRequest.hasSubproductId()) {
                        setSubproductId(rewardCheckRequest.getSubproductId());
                    }
                    if (rewardCheckRequest.hasEventCode()) {
                        setEventCode(rewardCheckRequest.getEventCode());
                    }
                    if (rewardCheckRequest.hasExtraData()) {
                        setExtraData(rewardCheckRequest.getExtraData());
                    }
                    mergeUnknownFields(rewardCheckRequest.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = com.google.protobuf.a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 8:
                            setUserId(hVar.d());
                            break;
                        case DescriptorProtos.FileOptions.PY_GENERIC_SERVICES_FIELD_NUMBER:
                            setLocale(hVar.j());
                            break;
                        case 24:
                            setProductId(hVar.l());
                            break;
                        case 32:
                            setSubproductId(hVar.l());
                            break;
                        case 40:
                            setEventCode(hVar.l());
                            break;
                        case 50:
                            setExtraData(hVar.j());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasUserId() {
                return this.result.hasUserId();
            }

            public final long getUserId() {
                return this.result.getUserId();
            }

            public final Builder setUserId(long j) {
                boolean unused = this.result.hasUserId = true;
                long unused2 = this.result.userId_ = j;
                return this;
            }

            public final Builder clearUserId() {
                boolean unused = this.result.hasUserId = false;
                long unused2 = this.result.userId_ = 0;
                return this;
            }

            public final boolean hasLocale() {
                return this.result.hasLocale();
            }

            public final String getLocale() {
                return this.result.getLocale();
            }

            public final Builder setLocale(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasLocale = true;
                String unused2 = this.result.locale_ = str;
                return this;
            }

            public final Builder clearLocale() {
                boolean unused = this.result.hasLocale = false;
                String unused2 = this.result.locale_ = RewardCheckRequest.getDefaultInstance().getLocale();
                return this;
            }

            public final boolean hasProductId() {
                return this.result.hasProductId();
            }

            public final int getProductId() {
                return this.result.getProductId();
            }

            public final Builder setProductId(int i) {
                boolean unused = this.result.hasProductId = true;
                int unused2 = this.result.productId_ = i;
                return this;
            }

            public final Builder clearProductId() {
                boolean unused = this.result.hasProductId = false;
                int unused2 = this.result.productId_ = 0;
                return this;
            }

            public final boolean hasSubproductId() {
                return this.result.hasSubproductId();
            }

            public final int getSubproductId() {
                return this.result.getSubproductId();
            }

            public final Builder setSubproductId(int i) {
                boolean unused = this.result.hasSubproductId = true;
                int unused2 = this.result.subproductId_ = i;
                return this;
            }

            public final Builder clearSubproductId() {
                boolean unused = this.result.hasSubproductId = false;
                int unused2 = this.result.subproductId_ = 0;
                return this;
            }

            public final boolean hasEventCode() {
                return this.result.hasEventCode();
            }

            public final int getEventCode() {
                return this.result.getEventCode();
            }

            public final Builder setEventCode(int i) {
                boolean unused = this.result.hasEventCode = true;
                int unused2 = this.result.eventCode_ = i;
                return this;
            }

            public final Builder clearEventCode() {
                boolean unused = this.result.hasEventCode = false;
                int unused2 = this.result.eventCode_ = 0;
                return this;
            }

            public final boolean hasExtraData() {
                return this.result.hasExtraData();
            }

            public final String getExtraData() {
                return this.result.getExtraData();
            }

            public final Builder setExtraData(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasExtraData = true;
                String unused2 = this.result.extraData_ = str;
                return this;
            }

            public final Builder clearExtraData() {
                boolean unused = this.result.hasExtraData = false;
                String unused2 = this.result.extraData_ = RewardCheckRequest.getDefaultInstance().getExtraData();
                return this;
            }
        }
    }

    public static final class RewardCheckResponse extends GeneratedMessage {
        public static final int HAS_REWARD_FIELD_NUMBER = 1;
        private static final RewardCheckResponse defaultInstance = new RewardCheckResponse(true);
        /* access modifiers changed from: private */
        public boolean hasHasReward;
        /* access modifiers changed from: private */
        public boolean hasReward_;
        private int memoizedSerializedSize;

        /* synthetic */ RewardCheckResponse(a aVar) {
            this();
        }

        private RewardCheckResponse() {
            this.hasReward_ = false;
            this.memoizedSerializedSize = -1;
        }

        private RewardCheckResponse(boolean z) {
            this.hasReward_ = false;
            this.memoizedSerializedSize = -1;
        }

        public static RewardCheckResponse getDefaultInstance() {
            return defaultInstance;
        }

        public final RewardCheckResponse getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return Webservices.k;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return Webservices.l;
        }

        public final boolean hasHasReward() {
            return this.hasHasReward;
        }

        public final boolean getHasReward() {
            return this.hasReward_;
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            if (!this.hasHasReward) {
                return false;
            }
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasHasReward()) {
                xVar.a(1, getHasReward());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (hasHasReward()) {
                getHasReward();
                i2 = x.e(1) + 0;
            }
            int serializedSize = i2 + getUnknownFields().getSerializedSize();
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static RewardCheckResponse parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.ptowngames.webservices.Webservices.RewardCheckResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.webservices.Webservices.RewardCheckResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.RewardCheckResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.webservices.Webservices.RewardCheckResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.webservices.Webservices.RewardCheckResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$RewardCheckResponse$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static RewardCheckResponse parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static RewardCheckResponse parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.ptowngames.webservices.Webservices.RewardCheckResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.webservices.Webservices.RewardCheckResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.RewardCheckResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.webservices.Webservices.RewardCheckResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.webservices.Webservices.RewardCheckResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$RewardCheckResponse$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static RewardCheckResponse parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static RewardCheckResponse parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.ptowngames.webservices.Webservices.RewardCheckResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.webservices.Webservices.RewardCheckResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.RewardCheckResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.webservices.Webservices.RewardCheckResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.ptowngames.webservices.Webservices.RewardCheckResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$RewardCheckResponse$Builder
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static RewardCheckResponse parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static RewardCheckResponse parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static RewardCheckResponse parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static RewardCheckResponse parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.ptowngames.webservices.Webservices.RewardCheckResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$RewardCheckResponse$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.ptowngames.webservices.Webservices.RewardCheckResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.ptowngames.webservices.Webservices.RewardCheckResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.RewardCheckResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.ptowngames.webservices.Webservices.RewardCheckResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.ptowngames.webservices.Webservices.RewardCheckResponse.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.ptowngames.webservices.Webservices$RewardCheckResponse$Builder */
        public static RewardCheckResponse parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(RewardCheckResponse rewardCheckResponse) {
            return newBuilder().mergeFrom(rewardCheckResponse);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private RewardCheckResponse result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new RewardCheckResponse((a) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final RewardCheckResponse internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new RewardCheckResponse((a) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return RewardCheckResponse.getDescriptor();
            }

            public final RewardCheckResponse getDefaultInstanceForType() {
                return RewardCheckResponse.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final RewardCheckResponse build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public RewardCheckResponse buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final RewardCheckResponse buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                RewardCheckResponse rewardCheckResponse = this.result;
                this.result = null;
                return rewardCheckResponse;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof RewardCheckResponse) {
                    return mergeFrom((RewardCheckResponse) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(RewardCheckResponse rewardCheckResponse) {
                if (rewardCheckResponse != RewardCheckResponse.getDefaultInstance()) {
                    if (rewardCheckResponse.hasHasReward()) {
                        setHasReward(rewardCheckResponse.getHasReward());
                    }
                    mergeUnknownFields(rewardCheckResponse.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = com.google.protobuf.a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 8:
                            setHasReward(hVar.i());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasHasReward() {
                return this.result.hasHasReward();
            }

            public final boolean getHasReward() {
                return this.result.getHasReward();
            }

            public final Builder setHasReward(boolean z) {
                boolean unused = this.result.hasHasReward = true;
                boolean unused2 = this.result.hasReward_ = z;
                return this;
            }

            public final Builder clearHasReward() {
                boolean unused = this.result.hasHasReward = false;
                boolean unused2 = this.result.hasReward_ = false;
                return this;
            }
        }
    }

    public static abstract class a {

        /* renamed from: com.ptowngames.webservices.Webservices$a$a  reason: collision with other inner class name */
        public interface C0020a {
            RewardCheckResponse a(aa aaVar, RewardCheckRequest rewardCheckRequest) throws com.google.protobuf.b;
        }

        protected a() {
        }

        public static C0020a a(z zVar) {
            return new b(zVar);
        }

        private static final class b implements C0020a {
            private final z a;

            /* synthetic */ b(z zVar) {
                this(zVar, (byte) 0);
            }

            private b(z zVar, byte b) {
                this.a = zVar;
            }

            public final RewardCheckResponse a(aa aaVar, RewardCheckRequest rewardCheckRequest) throws com.google.protobuf.b {
                return (RewardCheckResponse) this.a.a(Webservices.a().e().get(0).d().get(2), aaVar, rewardCheckRequest, RewardCheckResponse.getDefaultInstance());
            }
        }
    }

    public static c.b a() {
        return m;
    }

    static {
        c.b.a(new String[]{"\n\u0011webservices.proto\u0012\u001acom.ptowngames.webservices\"7\n\u0013ValidateCodeRequest\u0012\f\n\u0004code\u0018\u0001 \u0002(\t\u0012\u0012\n\nproduct_id\u0018\u0002 \u0002(\r\"(\n\u0014ValidateCodeResponse\u0012\u0010\n\bis_valid\u0018\u0001 \u0002(\b\")\n\u0013GenerateCodeRequest\u0012\u0012\n\nproduct_id\u0018\u0001 \u0002(\r\"3\n\u0014GenerateCodeResponse\u0012\f\n\u0004code\u0018\u0001 \u0001(\t\u0012\r\n\u0005error\u0018\u0002 \u0001(\t\"\u0003\n\u0012RewardCheckRequest\u0012\u000f\n\u0007user_id\u0018\u0001 \u0002(\u0004\u0012\u000e\n\u0006locale\u0018\u0002 \u0001(\t\u0012\u0012\n\nproduct_id\u0018\u0003 \u0001(\r\u0012\u0015\n\rsubproduct_id\u0018\u0004 \u0001(\r\u0012\u0012\n\nevent_code\u0018\u0005 \u0001(\r\u0012\u0012\n\nextra_data\u0018\u0006 \u0001(\t\"\u0002\n\tEventCode\u0012\u000f\n\u000bA", "PP_STARTED\u0010\u0000\u0012\u0012\n\u000eLEVEL_SELECTED\u0010\u0001\u0012\u0014\n\u0010LOADING_FINISHED\u0010\u0002\u0012\u0010\n\fLEVEL_SOLVED\u0010\u0003\u0012\r\n\tAPP_PAUSE\u0010\u0004\u0012\u000e\n\nAPP_RESUME\u0010\u0005\u0012\u000f\n\u000bAPP_DISPOSE\u0010\u0006\u0012\u0016\n\u0012FIRST_CAMERA_MOVED\u0010\u0007\u0012\u0015\n\u0011FIRST_PIECE_MOVED\u0010\b\u0012\u0015\n\u0011BUTTON_BAR_BUTTON\u0010\t\u0012\u0015\n\u0011PROGRAM_EXCEPTION\u0010\n\u0012\u0019\n\u0015EXTERNAL_LINK_CLICKED\u0010\u000b\")\n\u0013RewardCheckResponse\u0012\u0012\n\nhas_reward\u0018\u0001 \u0002(\b2ç\u0002\n\fPtownService\u0012q\n\fValidateCode\u0012/.com.ptowngames.webservices.ValidateCodeRequest\u001a0.com.ptowngames.webservices.Valid", "ateCodeResponse\u0012q\n\fGenerateCode\u0012/.com.ptowngames.webservices.GenerateCodeRequest\u001a0.com.ptowngames.webservices.GenerateCodeResponse\u0012q\n\u000eCheckForReward\u0012..com.ptowngames.webservices.RewardCheckRequest\u001a/.com.ptowngames.webservices.RewardCheckResponseB\u0003\u0001\u0001"}, new c.b[0], new a());
    }
}
