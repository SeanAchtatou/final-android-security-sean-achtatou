package com.ptowngames.webservices;

import com.google.protobuf.aa;
import com.google.protobuf.c;
import com.google.protobuf.s;
import com.google.protobuf.z;
import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;

public final class d implements z {
    public final s a(c.i iVar, aa aaVar, s sVar, s sVar2) {
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 5000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 5000);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
        HttpPost httpPost = new HttpPost("http://www.jigsaur.com/index.wsgi?method=" + iVar.a());
        httpPost.setEntity(new ByteArrayEntity(sVar.toByteString().b()));
        httpPost.addHeader("Content-type", "application/octet-stream");
        try {
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            int statusCode = execute.getStatusLine().getStatusCode();
            if (statusCode != 200) {
                aaVar.a("Non-OK status code: " + statusCode);
                return sVar2;
            }
            return sVar2.toBuilder().mergeFrom(execute.getEntity().getContent()).build();
        } catch (IOException e) {
            aaVar.a("Exception: " + e);
            return sVar2;
        }
    }
}
