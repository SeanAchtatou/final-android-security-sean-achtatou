package com.ptowngames.webservices;

import com.google.protobuf.GeneratedMessage;
import com.google.protobuf.c;
import com.google.protobuf.f;
import com.ptowngames.webservices.Webservices;

class a implements c.b.a {
    a() {
    }

    public final f a(c.b bVar) {
        c.b unused = Webservices.m = bVar;
        c.a unused2 = Webservices.a = Webservices.a().d().get(0);
        GeneratedMessage.FieldAccessorTable unused3 = Webservices.b = new GeneratedMessage.FieldAccessorTable(Webservices.a, new String[]{"Code", "ProductId"}, Webservices.ValidateCodeRequest.class, Webservices.ValidateCodeRequest.Builder.class);
        c.a unused4 = Webservices.c = Webservices.a().d().get(1);
        GeneratedMessage.FieldAccessorTable unused5 = Webservices.d = new GeneratedMessage.FieldAccessorTable(Webservices.c, new String[]{"IsValid"}, Webservices.ValidateCodeResponse.class, Webservices.ValidateCodeResponse.Builder.class);
        c.a unused6 = Webservices.e = Webservices.a().d().get(2);
        GeneratedMessage.FieldAccessorTable unused7 = Webservices.f = new GeneratedMessage.FieldAccessorTable(Webservices.e, new String[]{"ProductId"}, Webservices.GenerateCodeRequest.class, Webservices.GenerateCodeRequest.Builder.class);
        c.a unused8 = Webservices.g = Webservices.a().d().get(3);
        GeneratedMessage.FieldAccessorTable unused9 = Webservices.h = new GeneratedMessage.FieldAccessorTable(Webservices.g, new String[]{"Code", "Error"}, Webservices.GenerateCodeResponse.class, Webservices.GenerateCodeResponse.Builder.class);
        c.a unused10 = Webservices.i = Webservices.a().d().get(4);
        GeneratedMessage.FieldAccessorTable unused11 = Webservices.j = new GeneratedMessage.FieldAccessorTable(Webservices.i, new String[]{"UserId", "Locale", "ProductId", "SubproductId", "EventCode", "ExtraData"}, Webservices.RewardCheckRequest.class, Webservices.RewardCheckRequest.Builder.class);
        c.a unused12 = Webservices.k = Webservices.a().d().get(5);
        GeneratedMessage.FieldAccessorTable unused13 = Webservices.l = new GeneratedMessage.FieldAccessorTable(Webservices.k, new String[]{"HasReward"}, Webservices.RewardCheckResponse.class, Webservices.RewardCheckResponse.Builder.class);
        return null;
    }
}
