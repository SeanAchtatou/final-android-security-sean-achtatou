package com.badlogic.gdx.physics.box2d.joints;

import com.badlogic.gdx.math.g;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.JointDef;

public class WeldJointDef extends JointDef {
    public final g localAnchorA = new g();
    public final g localAnchorB = new g();
    public float referenceAngle = 0.0f;

    public WeldJointDef() {
        this.type = JointDef.JointType.WeldJoint;
    }

    public void initialize(Body body, Body body2, g gVar) {
        this.bodyA = body;
        this.bodyB = body2;
        this.localAnchorA.a(body.getLocalPoint(gVar));
        this.localAnchorB.a(body2.getLocalPoint(gVar));
        this.referenceAngle = body2.getAngle() - body.getAngle();
    }
}
