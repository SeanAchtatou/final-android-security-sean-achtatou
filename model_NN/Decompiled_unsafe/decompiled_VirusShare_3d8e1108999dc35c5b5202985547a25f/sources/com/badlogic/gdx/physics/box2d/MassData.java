package com.badlogic.gdx.physics.box2d;

import com.badlogic.gdx.math.g;

public class MassData {
    public float I;
    public final g center = new g();
    public float mass;
}
