package com.badlogic.gdx.physics.box2d.joints;

import com.badlogic.gdx.math.g;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.World;

public class MouseJoint extends Joint {
    private final g target = new g();
    final float[] tmp = new float[2];

    private native float jniGetDampingRatio(long j);

    private native float jniGetFrequency(long j);

    private native float jniGetMaxForce(long j);

    private native void jniGetTarget(long j, float[] fArr);

    private native void jniSetDampingRatio(long j, float f);

    private native void jniSetFrequency(long j, float f);

    private native void jniSetMaxForce(long j, float f);

    private native void jniSetTarget(long j, float f, float f2);

    public MouseJoint(World world, long j) {
        super(world, j);
    }

    public void setTarget(g gVar) {
        jniSetTarget(this.addr, gVar.a, gVar.b);
    }

    public g getTarget() {
        jniGetTarget(this.addr, this.tmp);
        this.target.a = this.tmp[0];
        this.target.b = this.tmp[1];
        return this.target;
    }

    public void setMaxForce(float f) {
        jniSetMaxForce(this.addr, f);
    }

    public float getMaxForce() {
        return jniGetMaxForce(this.addr);
    }

    public void setFrequency(float f) {
        jniSetFrequency(this.addr, f);
    }

    public float getFrequency() {
        return jniGetFrequency(this.addr);
    }

    public void setDampingRatio(float f) {
        jniSetDampingRatio(this.addr, f);
    }

    public float getDampingRatio() {
        return jniGetDampingRatio(this.addr);
    }
}
