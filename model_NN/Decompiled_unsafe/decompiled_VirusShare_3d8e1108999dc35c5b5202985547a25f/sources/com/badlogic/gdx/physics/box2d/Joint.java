package com.badlogic.gdx.physics.box2d;

import com.badlogic.gdx.math.g;
import com.badlogic.gdx.physics.box2d.JointDef;

public abstract class Joint {
    protected long addr;
    private final g anchorA = new g();
    private final g anchorB = new g();
    protected JointEdge jointEdgeA;
    protected JointEdge jointEdgeB;
    private final g reactionForce = new g();
    private final float[] tmp = new float[2];
    private final World world;

    private native void jniGetAnchorA(long j, float[] fArr);

    private native void jniGetAnchorB(long j, float[] fArr);

    private native long jniGetBodyA(long j);

    private native long jniGetBodyB(long j);

    private native void jniGetReactionForce(long j, float f, float[] fArr);

    private native float jniGetReactionTorque(long j, float f);

    private native int jniGetType(long j);

    private native boolean jniIsActive(long j);

    protected Joint(World world2, long j) {
        this.world = world2;
        this.addr = j;
    }

    public JointDef.JointType getType() {
        int jniGetType = jniGetType(this.addr);
        if (jniGetType <= 0 || jniGetType >= JointDef.JointType.valueTypes.length) {
            return JointDef.JointType.Unknown;
        }
        return JointDef.JointType.valueTypes[jniGetType];
    }

    public Body getBodyA() {
        return this.world.bodies.a(jniGetBodyA(this.addr));
    }

    public Body getBodyB() {
        return this.world.bodies.a(jniGetBodyB(this.addr));
    }

    public g getAnchorA() {
        jniGetAnchorA(this.addr, this.tmp);
        this.anchorA.a = this.tmp[0];
        this.anchorA.b = this.tmp[1];
        return this.anchorA;
    }

    public g getAnchorB() {
        jniGetAnchorB(this.addr, this.tmp);
        this.anchorB.a = this.tmp[0];
        this.anchorB.b = this.tmp[1];
        return this.anchorB;
    }

    public g getReactionForce(float f) {
        jniGetReactionForce(this.addr, f, this.tmp);
        this.reactionForce.a = this.tmp[0];
        this.reactionForce.b = this.tmp[1];
        return this.reactionForce;
    }

    public float getReactionTorque(float f) {
        return jniGetReactionTorque(this.addr, f);
    }

    public boolean isActive() {
        return jniIsActive(this.addr);
    }
}
