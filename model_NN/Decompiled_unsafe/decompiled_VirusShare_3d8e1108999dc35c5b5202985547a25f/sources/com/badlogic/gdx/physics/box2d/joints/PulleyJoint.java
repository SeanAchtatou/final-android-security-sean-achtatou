package com.badlogic.gdx.physics.box2d.joints;

import com.badlogic.gdx.math.g;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.World;

public class PulleyJoint extends Joint {
    private final g groundAnchorA = new g();
    private final g groundAnchorB = new g();
    private final float[] tmp = new float[2];

    private native void jniGetGroundAnchorA(long j, float[] fArr);

    private native void jniGetGroundAnchorB(long j, float[] fArr);

    private native float jniGetLength1(long j);

    private native float jniGetLength2(long j);

    private native float jniGetRatio(long j);

    public PulleyJoint(World world, long j) {
        super(world, j);
    }

    public g getGroundAnchorA() {
        jniGetGroundAnchorA(this.addr, this.tmp);
        this.groundAnchorA.a(this.tmp[0], this.tmp[1]);
        return this.groundAnchorA;
    }

    public g getGroundAnchorB() {
        jniGetGroundAnchorB(this.addr, this.tmp);
        this.groundAnchorB.a(this.tmp[0], this.tmp[1]);
        return this.groundAnchorB;
    }

    public float getLength1() {
        return jniGetLength1(this.addr);
    }

    public float getLength2() {
        return jniGetLength2(this.addr);
    }

    public float getRatio() {
        return jniGetRatio(this.addr);
    }
}
