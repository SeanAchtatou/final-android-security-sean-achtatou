package com.badlogic.gdx.physics.box2d;

import com.badlogic.gdx.math.g;
import com.badlogic.gdx.physics.box2d.JointDef;
import com.badlogic.gdx.physics.box2d.joints.DistanceJoint;
import com.badlogic.gdx.physics.box2d.joints.DistanceJointDef;
import com.badlogic.gdx.physics.box2d.joints.FrictionJoint;
import com.badlogic.gdx.physics.box2d.joints.FrictionJointDef;
import com.badlogic.gdx.physics.box2d.joints.GearJoint;
import com.badlogic.gdx.physics.box2d.joints.GearJointDef;
import com.badlogic.gdx.physics.box2d.joints.LineJoint;
import com.badlogic.gdx.physics.box2d.joints.LineJointDef;
import com.badlogic.gdx.physics.box2d.joints.MouseJoint;
import com.badlogic.gdx.physics.box2d.joints.MouseJointDef;
import com.badlogic.gdx.physics.box2d.joints.PrismaticJoint;
import com.badlogic.gdx.physics.box2d.joints.PrismaticJointDef;
import com.badlogic.gdx.physics.box2d.joints.PulleyJoint;
import com.badlogic.gdx.physics.box2d.joints.PulleyJointDef;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJoint;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import com.badlogic.gdx.physics.box2d.joints.WeldJoint;
import com.badlogic.gdx.physics.box2d.joints.WeldJointDef;
import com.badlogic.gdx.utils.a;
import com.badlogic.gdx.utils.c;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class World implements c {
    private final long addr;
    protected final a<Body> bodies = new a<>((byte) 0);
    private final Contact contact = new Contact(this, 0);
    private long[] contactAddrs = new long[200];
    protected ContactFilter contactFilter = null;
    protected ContactListener contactListener = null;
    private final ArrayList<Contact> contacts = new ArrayList<>();
    protected final a<Fixture> fixtures = new a<>((byte) 0);
    private final ArrayList<Contact> freeContacts = new ArrayList<>();
    final g gravity = new g();
    protected final a<Joint> joints = new a<>((byte) 0);
    private QueryCallback queryCallback = null;
    private RayCastCallback rayCastCallback = null;
    private g rayNormal = new g();
    private g rayPoint = new g();
    final float[] tmpGravity = new float[2];

    private native void jniClearForces(long j);

    private native long jniCreateBody(long j, int i, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, float f9);

    private native long jniCreateDistanceJoint(long j, long j2, long j3, boolean z, float f, float f2, float f3, float f4, float f5, float f6, float f7);

    private native long jniCreateFrictionJoint(long j, long j2, long j3, boolean z, float f, float f2, float f3, float f4, float f5, float f6);

    private native long jniCreateGearJoint(long j, long j2, long j3, boolean z, long j4, long j5, float f);

    private native long jniCreateLineJoint(long j, long j2, long j3, boolean z, float f, float f2, float f3, float f4, float f5, float f6, boolean z2, float f7, float f8, boolean z3, float f9, float f10);

    private native long jniCreateMouseJoint(long j, long j2, long j3, boolean z, float f, float f2, float f3, float f4, float f5);

    private native long jniCreatePrismaticJoint(long j, long j2, long j3, boolean z, float f, float f2, float f3, float f4, float f5, float f6, float f7, boolean z2, float f8, float f9, boolean z3, float f10, float f11);

    private native long jniCreatePulleyJoint(long j, long j2, long j3, boolean z, float f, float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9, float f10, float f11, float f12, float f13);

    private native long jniCreateRevoluteJoint(long j, long j2, long j3, boolean z, float f, float f2, float f3, float f4, float f5, boolean z2, float f6, float f7, boolean z3, float f8, float f9);

    private native long jniCreateWeldJoint(long j, long j2, long j3, boolean z, float f, float f2, float f3, float f4, float f5);

    private native void jniDestroyBody(long j, long j2);

    private native void jniDestroyJoint(long j, long j2);

    private native void jniDispose(long j);

    private native boolean jniGetAutoClearForces(long j);

    private native int jniGetBodyCount(long j);

    private native int jniGetContactCount(long j);

    private native void jniGetContactList(long j, long[] jArr);

    private native void jniGetGravity(long j, float[] fArr);

    private native int jniGetJointcount(long j);

    private native int jniGetProxyCount(long j);

    private native boolean jniIsLocked(long j);

    private native void jniQueryAABB(long j, float f, float f2, float f3, float f4);

    private native void jniRayCast(long j, float f, float f2, float f3, float f4);

    private native void jniSetAutoClearForces(long j, boolean z);

    private native void jniSetContiousPhysics(long j, boolean z);

    private native void jniSetGravity(long j, float f, float f2);

    private native void jniSetWarmStarting(long j, boolean z);

    private native void jniStep(long j, float f, int i, int i2);

    private native long newWorld(float f, float f2, boolean z);

    public void rayCast(RayCastCallback rayCastCallback2, g gVar, g gVar2) {
        this.rayCastCallback = rayCastCallback2;
        jniRayCast(this.addr, gVar.a, gVar.b, gVar2.a, gVar2.b);
    }

    private float reportRayFixture(long j, float f, float f2, float f3, float f4, float f5) {
        if (this.rayCastCallback != null) {
            return this.rayCastCallback.reportRayFixture(this.fixtures.a(j), this.rayPoint.a(f, f2), this.rayNormal.a(f3, f4), f5);
        }
        return 0.0f;
    }

    public World(g gVar, boolean z) {
        this.addr = newWorld(gVar.a, gVar.b, z);
        for (int i = 0; i < 200; i++) {
            this.freeContacts.add(new Contact(this, 0));
        }
    }

    public void setDestructionListener(DestructionListener destructionListener) {
    }

    public void setContactFilter(ContactFilter contactFilter2) {
        this.contactFilter = contactFilter2;
    }

    public void setContactListener(ContactListener contactListener2) {
        this.contactListener = contactListener2;
    }

    public Body createBody(BodyDef bodyDef) {
        Body body = new Body(this, jniCreateBody(this.addr, bodyDef.type.getValue(), bodyDef.position.a, bodyDef.position.b, bodyDef.angle, bodyDef.linearVelocity.a, bodyDef.linearVelocity.b, bodyDef.angularVelocity, bodyDef.linearDamping, bodyDef.angularDamping, bodyDef.allowSleep, bodyDef.awake, bodyDef.fixedRotation, bodyDef.bullet, bodyDef.active, bodyDef.inertiaScale));
        this.bodies.a(body.addr, body);
        return body;
    }

    public void destroyBody(Body body) {
        this.bodies.b(body.addr);
        for (int i = 0; i < body.getFixtureList().size(); i++) {
            this.fixtures.b(body.getFixtureList().get(i).addr);
        }
        for (int i2 = 0; i2 < body.getJointList().size(); i2++) {
            this.joints.b(body.getJointList().get(i2).joint.addr);
        }
        jniDestroyBody(this.addr, body.addr);
    }

    public Joint createJoint(JointDef jointDef) {
        long createProperJoint = createProperJoint(jointDef);
        Joint joint = null;
        if (jointDef.type == JointDef.JointType.DistanceJoint) {
            joint = new DistanceJoint(this, createProperJoint);
        }
        if (jointDef.type == JointDef.JointType.FrictionJoint) {
            joint = new FrictionJoint(this, createProperJoint);
        }
        if (jointDef.type == JointDef.JointType.GearJoint) {
            joint = new GearJoint(this, createProperJoint);
        }
        if (jointDef.type == JointDef.JointType.LineJoint) {
            joint = new LineJoint(this, createProperJoint);
        }
        if (jointDef.type == JointDef.JointType.MouseJoint) {
            joint = new MouseJoint(this, createProperJoint);
        }
        if (jointDef.type == JointDef.JointType.PrismaticJoint) {
            joint = new PrismaticJoint(this, createProperJoint);
        }
        if (jointDef.type == JointDef.JointType.PulleyJoint) {
            joint = new PulleyJoint(this, createProperJoint);
        }
        if (jointDef.type == JointDef.JointType.RevoluteJoint) {
            joint = new RevoluteJoint(this, createProperJoint);
        }
        if (jointDef.type == JointDef.JointType.WeldJoint) {
            joint = new WeldJoint(this, createProperJoint);
        }
        if (joint != null) {
            this.joints.a(joint.addr, joint);
        }
        JointEdge jointEdge = new JointEdge(jointDef.bodyB, joint);
        JointEdge jointEdge2 = new JointEdge(jointDef.bodyA, joint);
        joint.jointEdgeA = jointEdge;
        joint.jointEdgeB = jointEdge2;
        jointDef.bodyA.joints.add(jointEdge);
        jointDef.bodyB.joints.add(jointEdge2);
        return joint;
    }

    private long createProperJoint(JointDef jointDef) {
        if (jointDef.type == JointDef.JointType.DistanceJoint) {
            DistanceJointDef distanceJointDef = (DistanceJointDef) jointDef;
            return jniCreateDistanceJoint(this.addr, distanceJointDef.bodyA.addr, distanceJointDef.bodyB.addr, distanceJointDef.collideConnected, distanceJointDef.localAnchorA.a, distanceJointDef.localAnchorA.b, distanceJointDef.localAnchorB.a, distanceJointDef.localAnchorB.b, distanceJointDef.length, distanceJointDef.frequencyHz, distanceJointDef.dampingRatio);
        } else if (jointDef.type == JointDef.JointType.FrictionJoint) {
            FrictionJointDef frictionJointDef = (FrictionJointDef) jointDef;
            return jniCreateFrictionJoint(this.addr, frictionJointDef.bodyA.addr, frictionJointDef.bodyB.addr, frictionJointDef.collideConnected, frictionJointDef.localAnchorA.a, frictionJointDef.localAnchorA.b, frictionJointDef.localAnchorB.a, frictionJointDef.localAnchorB.b, frictionJointDef.maxForce, frictionJointDef.maxTorque);
        } else if (jointDef.type == JointDef.JointType.GearJoint) {
            GearJointDef gearJointDef = (GearJointDef) jointDef;
            return jniCreateGearJoint(this.addr, gearJointDef.bodyA.addr, gearJointDef.bodyB.addr, gearJointDef.collideConnected, gearJointDef.joint1.addr, gearJointDef.joint2.addr, gearJointDef.ratio);
        } else if (jointDef.type == JointDef.JointType.LineJoint) {
            LineJointDef lineJointDef = (LineJointDef) jointDef;
            return jniCreateLineJoint(this.addr, lineJointDef.bodyA.addr, lineJointDef.bodyB.addr, lineJointDef.collideConnected, lineJointDef.localAnchorA.a, lineJointDef.localAnchorA.b, lineJointDef.localAnchorB.a, lineJointDef.localAnchorB.b, lineJointDef.localAxisA.a, lineJointDef.localAxisA.b, lineJointDef.enableLimit, lineJointDef.lowerTranslation, lineJointDef.upperTranslation, lineJointDef.enableMotor, lineJointDef.maxMotorForce, lineJointDef.motorSpeed);
        } else if (jointDef.type == JointDef.JointType.MouseJoint) {
            MouseJointDef mouseJointDef = (MouseJointDef) jointDef;
            return jniCreateMouseJoint(this.addr, mouseJointDef.bodyA.addr, mouseJointDef.bodyB.addr, mouseJointDef.collideConnected, mouseJointDef.target.a, mouseJointDef.target.b, mouseJointDef.maxForce, mouseJointDef.frequencyHz, mouseJointDef.dampingRatio);
        } else if (jointDef.type == JointDef.JointType.PrismaticJoint) {
            PrismaticJointDef prismaticJointDef = (PrismaticJointDef) jointDef;
            return jniCreatePrismaticJoint(this.addr, prismaticJointDef.bodyA.addr, prismaticJointDef.bodyB.addr, prismaticJointDef.collideConnected, prismaticJointDef.localAnchorA.a, prismaticJointDef.localAnchorA.b, prismaticJointDef.localAnchorB.a, prismaticJointDef.localAnchorB.b, prismaticJointDef.localAxis1.a, prismaticJointDef.localAxis1.b, prismaticJointDef.referenceAngle, prismaticJointDef.enableLimit, prismaticJointDef.lowerTranslation, prismaticJointDef.upperTranslation, prismaticJointDef.enableMotor, prismaticJointDef.maxMotorForce, prismaticJointDef.motorSpeed);
        } else if (jointDef.type == JointDef.JointType.PulleyJoint) {
            PulleyJointDef pulleyJointDef = (PulleyJointDef) jointDef;
            return jniCreatePulleyJoint(this.addr, pulleyJointDef.bodyA.addr, pulleyJointDef.bodyB.addr, pulleyJointDef.collideConnected, pulleyJointDef.groundAnchorA.a, pulleyJointDef.groundAnchorA.b, pulleyJointDef.groundAnchorB.a, pulleyJointDef.groundAnchorB.b, pulleyJointDef.localAnchorA.a, pulleyJointDef.localAnchorA.b, pulleyJointDef.localAnchorB.a, pulleyJointDef.localAnchorB.b, pulleyJointDef.lengthA, pulleyJointDef.maxLengthA, pulleyJointDef.lengthB, pulleyJointDef.maxLengthB, pulleyJointDef.ratio);
        } else if (jointDef.type == JointDef.JointType.RevoluteJoint) {
            RevoluteJointDef revoluteJointDef = (RevoluteJointDef) jointDef;
            return jniCreateRevoluteJoint(this.addr, revoluteJointDef.bodyA.addr, revoluteJointDef.bodyB.addr, revoluteJointDef.collideConnected, revoluteJointDef.localAnchorA.a, revoluteJointDef.localAnchorA.b, revoluteJointDef.localAnchorB.a, revoluteJointDef.localAnchorB.b, revoluteJointDef.referenceAngle, revoluteJointDef.enableLimit, revoluteJointDef.lowerAngle, revoluteJointDef.upperAngle, revoluteJointDef.enableMotor, revoluteJointDef.motorSpeed, revoluteJointDef.maxMotorTorque);
        } else if (jointDef.type != JointDef.JointType.WeldJoint) {
            return 0;
        } else {
            WeldJointDef weldJointDef = (WeldJointDef) jointDef;
            return jniCreateWeldJoint(this.addr, weldJointDef.bodyA.addr, weldJointDef.bodyB.addr, weldJointDef.collideConnected, weldJointDef.localAnchorA.a, weldJointDef.localAnchorA.b, weldJointDef.localAnchorB.a, weldJointDef.localAnchorB.b, weldJointDef.referenceAngle);
        }
    }

    public void destroyJoint(Joint joint) {
        this.joints.b(joint.addr);
        joint.jointEdgeA.other.joints.remove(joint.jointEdgeB);
        joint.jointEdgeB.other.joints.remove(joint.jointEdgeA);
        jniDestroyJoint(this.addr, joint.addr);
    }

    public void step(float f, int i, int i2) {
        jniStep(this.addr, f, i, i2);
    }

    public void clearForces() {
        jniClearForces(this.addr);
    }

    public void setWarmStarting(boolean z) {
        jniSetWarmStarting(this.addr, z);
    }

    public void setContinuousPhysics(boolean z) {
        jniSetContiousPhysics(this.addr, z);
    }

    public int getProxyCount() {
        return jniGetProxyCount(this.addr);
    }

    public int getBodyCount() {
        return jniGetBodyCount(this.addr);
    }

    public int getJointCount() {
        return jniGetJointcount(this.addr);
    }

    public int getContactCount() {
        return jniGetContactCount(this.addr);
    }

    public void setGravity(g gVar) {
        jniSetGravity(this.addr, gVar.a, gVar.b);
    }

    public g getGravity() {
        jniGetGravity(this.addr, this.tmpGravity);
        this.gravity.a = this.tmpGravity[0];
        this.gravity.b = this.tmpGravity[1];
        return this.gravity;
    }

    public boolean isLocked() {
        return jniIsLocked(this.addr);
    }

    public void setAutoClearForces(boolean z) {
        jniSetAutoClearForces(this.addr, z);
    }

    public boolean getAutoClearForces() {
        return jniGetAutoClearForces(this.addr);
    }

    public void QueryAABB(QueryCallback queryCallback2, float f, float f2, float f3, float f4) {
        this.queryCallback = queryCallback2;
        jniQueryAABB(this.addr, f, f2, f3, f4);
    }

    public List<Contact> getContactList() {
        int contactCount = getContactCount();
        if (contactCount > this.contactAddrs.length) {
            this.contactAddrs = new long[contactCount];
        }
        if (contactCount > this.freeContacts.size()) {
            int size = this.freeContacts.size();
            for (int i = 0; i < contactCount - size; i++) {
                this.freeContacts.add(new Contact(this, 0));
            }
        }
        jniGetContactList(this.addr, this.contactAddrs);
        this.contacts.clear();
        for (int i2 = 0; i2 < contactCount; i2++) {
            Contact contact2 = this.freeContacts.get(i2);
            contact2.addr = this.contactAddrs[i2];
            this.contacts.add(contact2);
        }
        return this.contacts;
    }

    public Iterator<Body> getBodies() {
        return this.bodies.a();
    }

    public Iterator<Joint> getJoints() {
        return this.joints.a();
    }

    public void dispose() {
        jniDispose(this.addr);
    }

    private boolean contactFilter(long j, long j2) {
        if (this.contactFilter != null) {
            return this.contactFilter.shouldCollide(this.fixtures.a(j), this.fixtures.a(j2));
        }
        Filter filterData = this.fixtures.a(j).getFilterData();
        Filter filterData2 = this.fixtures.a(j2).getFilterData();
        if (filterData.groupIndex == filterData2.groupIndex && filterData.groupIndex != 0) {
            return filterData.groupIndex > 0;
        }
        if ((filterData.maskBits & filterData2.categoryBits) != 0) {
            if ((filterData2.maskBits & filterData.categoryBits) != 0) {
                return true;
            }
        }
        return false;
    }

    private void beginContact(long j) {
        this.contact.addr = j;
        if (this.contactListener != null) {
            this.contactListener.beginContact(this.contact);
        }
    }

    private void endContact(long j) {
        this.contact.addr = j;
        this.contact.getWorldManifold();
        if (this.contactListener != null) {
            this.contactListener.endContact(this.contact);
        }
    }

    private boolean reportFixture(long j) {
        if (this.queryCallback != null) {
            return this.queryCallback.reportFixture(this.fixtures.a(j));
        }
        return false;
    }
}
