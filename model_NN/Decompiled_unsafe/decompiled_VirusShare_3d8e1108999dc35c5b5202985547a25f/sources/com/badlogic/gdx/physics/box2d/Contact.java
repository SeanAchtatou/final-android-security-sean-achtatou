package com.badlogic.gdx.physics.box2d;

import com.badlogic.gdx.math.g;

public class Contact {
    protected long addr;
    private final float[] tmp = new float[6];
    protected World world;
    protected final WorldManifold worldManifold = new WorldManifold();

    private native long jniGetFixtureA(long j);

    private native long jniGetFixtureB(long j);

    private native int jniGetWorldManifold(long j, float[] fArr);

    private native boolean jniIsEnabled(long j);

    private native boolean jniIsTouching(long j);

    private native void jniSetEnabled(long j, boolean z);

    protected Contact(World world2, long j) {
        this.addr = j;
        this.world = world2;
    }

    public WorldManifold getWorldManifold() {
        int jniGetWorldManifold = jniGetWorldManifold(this.addr, this.tmp);
        this.worldManifold.numContactPoints = jniGetWorldManifold;
        this.worldManifold.normal.a(this.tmp[0], this.tmp[1]);
        for (int i = 0; i < jniGetWorldManifold; i++) {
            g gVar = this.worldManifold.points[i];
            gVar.a = this.tmp[(i * 2) + 2];
            gVar.b = this.tmp[(i * 2) + 2 + 1];
        }
        return this.worldManifold;
    }

    public boolean isTouching() {
        return jniIsTouching(this.addr);
    }

    public void setEnabled(boolean z) {
        jniSetEnabled(this.addr, z);
    }

    public boolean isEnabled() {
        return jniIsEnabled(this.addr);
    }

    public Fixture getFixtureA() {
        return this.world.fixtures.a(jniGetFixtureA(this.addr));
    }

    public Fixture getFixtureB() {
        return this.world.fixtures.a(jniGetFixtureB(this.addr));
    }
}
