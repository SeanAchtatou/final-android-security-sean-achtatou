package com.badlogic.gdx.physics.box2d;

import com.badlogic.gdx.math.g;

public class Transform {
    public static final int COL1_X = 2;
    public static final int COL1_Y = 3;
    public static final int COL2_X = 4;
    public static final int COL2_Y = 5;
    public static final int POS_X = 0;
    public static final int POS_Y = 1;
    private g position = new g();
    public float[] vals = new float[6];

    public Transform() {
    }

    public Transform(g gVar, float f) {
        setPosition(gVar);
        setRotation(f);
    }

    public g mul(g gVar) {
        gVar.a = this.vals[0] + (this.vals[2] * gVar.a) + (this.vals[4] * gVar.b);
        gVar.b = this.vals[1] + (this.vals[3] * gVar.a) + (this.vals[5] * gVar.b);
        return gVar;
    }

    public g getPosition() {
        return this.position.a(this.vals[0], this.vals[1]);
    }

    public void setRotation(float f) {
        float cos = (float) Math.cos((double) f);
        float sin = (float) Math.sin((double) f);
        this.vals[2] = cos;
        this.vals[4] = -sin;
        this.vals[3] = sin;
        this.vals[5] = cos;
    }

    public void setPosition(g gVar) {
        this.vals[0] = gVar.a;
        this.vals[1] = gVar.b;
    }
}
