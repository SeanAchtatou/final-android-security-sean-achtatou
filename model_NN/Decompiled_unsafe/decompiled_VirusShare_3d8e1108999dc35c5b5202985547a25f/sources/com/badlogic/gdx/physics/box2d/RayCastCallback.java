package com.badlogic.gdx.physics.box2d;

import com.badlogic.gdx.math.g;

public interface RayCastCallback {
    float reportRayFixture(Fixture fixture, g gVar, g gVar2, float f);
}
