package com.badlogic.gdx.physics.box2d.joints;

import com.badlogic.gdx.math.g;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.JointDef;

public class FrictionJointDef extends JointDef {
    public final g localAnchorA = new g();
    public final g localAnchorB = new g();
    public float maxForce = 0.0f;
    public float maxTorque = 0.0f;

    public FrictionJointDef() {
        this.type = JointDef.JointType.FrictionJoint;
    }

    public void initialize(Body body, Body body2, g gVar) {
        this.bodyA = body;
        this.bodyB = body2;
        this.localAnchorA.a(body.getLocalPoint(gVar));
        this.localAnchorB.a(body2.getLocalPoint(gVar));
    }
}
