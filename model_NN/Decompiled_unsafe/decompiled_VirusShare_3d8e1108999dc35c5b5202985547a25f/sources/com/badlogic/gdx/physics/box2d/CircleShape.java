package com.badlogic.gdx.physics.box2d;

import com.badlogic.gdx.math.g;
import com.badlogic.gdx.physics.box2d.Shape;

public class CircleShape extends Shape {
    private final g position = new g();
    private final float[] tmp = new float[2];

    private native void jniGetPosition(long j, float[] fArr);

    private native void jniSetPosition(long j, float f, float f2);

    private native long newCircleShape();

    public CircleShape() {
        this.addr = newCircleShape();
    }

    protected CircleShape(long j) {
        this.addr = j;
    }

    public Shape.Type getType() {
        return Shape.Type.Circle;
    }

    public g getPosition() {
        jniGetPosition(this.addr, this.tmp);
        this.position.a = this.tmp[0];
        this.position.b = this.tmp[1];
        return this.position;
    }

    public void setPosition(g gVar) {
        jniSetPosition(this.addr, gVar.a, gVar.b);
    }
}
