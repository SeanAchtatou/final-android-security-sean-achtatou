package com.badlogic.gdx.physics.box2d.joints;

import com.badlogic.gdx.math.g;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.JointDef;

public class DistanceJointDef extends JointDef {
    public float dampingRatio = 0.0f;
    public float frequencyHz = 0.0f;
    public float length = 1.0f;
    public final g localAnchorA = new g();
    public final g localAnchorB = new g();

    public DistanceJointDef() {
        this.type = JointDef.JointType.DistanceJoint;
    }

    public void initialize(Body body, Body body2, g gVar, g gVar2) {
        this.bodyA = body;
        this.bodyB = body2;
        this.localAnchorA.a(body.getLocalPoint(gVar));
        this.localAnchorB.a(body2.getLocalPoint(gVar2));
        this.length = gVar.d(gVar2);
    }
}
