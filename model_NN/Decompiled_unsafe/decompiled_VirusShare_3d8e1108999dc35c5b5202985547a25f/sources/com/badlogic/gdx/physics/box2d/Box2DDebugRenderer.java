package com.badlogic.gdx.physics.box2d;

import com.badlogic.gdx.graphics.g2d.c;
import com.badlogic.gdx.math.g;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.JointDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.joints.PulleyJoint;
import java.util.ArrayList;
import java.util.Iterator;

public class Box2DDebugRenderer {
    private static g axis = new g();
    private static g t = new g();
    private static g[] vertices = new g[100];
    private final com.badlogic.gdx.graphics.g JOINT_COLOR = new com.badlogic.gdx.graphics.g(0.5f, 0.8f, 0.8f);
    private final com.badlogic.gdx.graphics.g SHAPE_AWAKE = new com.badlogic.gdx.graphics.g(0.9f, 0.7f, 0.7f);
    private final com.badlogic.gdx.graphics.g SHAPE_KINEMATIC = new com.badlogic.gdx.graphics.g(0.5f, 0.5f, 0.9f);
    private final com.badlogic.gdx.graphics.g SHAPE_NOT_ACTIVE = new com.badlogic.gdx.graphics.g(0.5f, 0.5f, 0.3f);
    private final com.badlogic.gdx.graphics.g SHAPE_NOT_AWAKE = new com.badlogic.gdx.graphics.g(0.6f, 0.6f, 0.6f);
    private final com.badlogic.gdx.graphics.g SHAPE_STATIC = new com.badlogic.gdx.graphics.g(0.5f, 0.9f, 0.5f);
    public c batch = new c((byte) 0);
    protected com.badlogic.gdx.graphics.a.c renderer = new com.badlogic.gdx.graphics.a.c((byte) 0);
    private final g v = new g();

    public Box2DDebugRenderer() {
        for (int i = 0; i < vertices.length; i++) {
            vertices[i] = new g();
        }
    }

    public void render(World world) {
        renderBodies(world);
    }

    private void renderBodies(World world) {
        Iterator<Body> bodies = world.getBodies();
        while (bodies.hasNext()) {
            Body next = bodies.next();
            Transform transform = next.getTransform();
            int size = next.getFixtureList().size();
            ArrayList<Fixture> fixtureList = next.getFixtureList();
            for (int i = 0; i < size; i++) {
                Fixture fixture = fixtureList.get(i);
                if (!next.isActive()) {
                    drawShape(fixture, transform, this.SHAPE_NOT_ACTIVE);
                } else if (next.getType() == BodyDef.BodyType.StaticBody) {
                    drawShape(fixture, transform, this.SHAPE_STATIC);
                } else if (next.getType() == BodyDef.BodyType.KinematicBody) {
                    drawShape(fixture, transform, this.SHAPE_KINEMATIC);
                } else if (!next.isAwake()) {
                    drawShape(fixture, transform, this.SHAPE_NOT_AWAKE);
                } else {
                    drawShape(fixture, transform, this.SHAPE_AWAKE);
                }
            }
        }
        Iterator<Joint> joints = world.getJoints();
        while (joints.hasNext()) {
            drawJoint(joints.next());
        }
        int size2 = world.getContactList().size();
        for (int i2 = 0; i2 < size2; i2++) {
            world.getContactList().get(i2);
        }
    }

    private void drawShape(Fixture fixture, Transform transform, com.badlogic.gdx.graphics.g gVar) {
        if (fixture.getType() == Shape.Type.Circle) {
            CircleShape circleShape = (CircleShape) fixture.getShape();
            t.a(circleShape.getPosition());
            transform.mul(t);
            drawSolidCircle(t, circleShape.getRadius(), axis.a(transform.vals[2], transform.vals[3]), gVar);
            return;
        }
        PolygonShape polygonShape = (PolygonShape) fixture.getShape();
        int vertexCount = polygonShape.getVertexCount();
        for (int i = 0; i < vertexCount; i++) {
            polygonShape.getVertex(i, vertices[i]);
            transform.mul(vertices[i]);
        }
        drawSolidPolygon(vertices, vertexCount, gVar);
    }

    private void drawSolidCircle(g gVar, float f, g gVar2, com.badlogic.gdx.graphics.g gVar3) {
        this.renderer.a(2);
        float f2 = 0.0f;
        int i = 0;
        while (i < 20) {
            this.v.a((((float) Math.cos((double) f2)) * f) + gVar.a, (((float) Math.sin((double) f2)) * f) + gVar.b);
            this.renderer.a(gVar3.b, gVar3.c, gVar3.d, gVar3.e);
            this.renderer.a(this.v.a, this.v.b);
            i++;
            f2 += 0.31415927f;
        }
        this.renderer.a();
        this.renderer.a(1);
        this.renderer.a(gVar3.b, gVar3.c, gVar3.d, gVar3.e);
        this.renderer.a(gVar.a, gVar.b);
        this.renderer.a(gVar3.b, gVar3.c, gVar3.d, gVar3.e);
        this.renderer.a(gVar.a + (gVar2.a * f), gVar.b + (gVar2.b * f));
        this.renderer.a();
    }

    private void drawSolidPolygon(g[] gVarArr, int i, com.badlogic.gdx.graphics.g gVar) {
        this.renderer.a(2);
        for (int i2 = 0; i2 < i; i2++) {
            g gVar2 = gVarArr[i2];
            this.renderer.a(gVar.b, gVar.c, gVar.d, gVar.e);
            this.renderer.a(gVar2.a, gVar2.b);
        }
        this.renderer.a();
    }

    private void drawJoint(Joint joint) {
        Body bodyA = joint.getBodyA();
        Body bodyB = joint.getBodyB();
        Transform transform = bodyA.getTransform();
        Transform transform2 = bodyB.getTransform();
        g position = transform.getPosition();
        g position2 = transform2.getPosition();
        g anchorA = joint.getAnchorA();
        g anchorB = joint.getAnchorB();
        if (joint.getType() == JointDef.JointType.DistanceJoint) {
            drawSegment(anchorA, anchorB, this.JOINT_COLOR);
        } else if (joint.getType() == JointDef.JointType.PulleyJoint) {
            PulleyJoint pulleyJoint = (PulleyJoint) joint;
            g groundAnchorA = pulleyJoint.getGroundAnchorA();
            g groundAnchorB = pulleyJoint.getGroundAnchorB();
            drawSegment(groundAnchorA, anchorA, this.JOINT_COLOR);
            drawSegment(groundAnchorB, anchorB, this.JOINT_COLOR);
            drawSegment(groundAnchorA, groundAnchorB, this.JOINT_COLOR);
        } else if (joint.getType() != JointDef.JointType.MouseJoint) {
            drawSegment(position, anchorA, this.JOINT_COLOR);
            drawSegment(anchorA, anchorB, this.JOINT_COLOR);
            drawSegment(position2, anchorB, this.JOINT_COLOR);
        }
    }

    private void drawSegment(g gVar, g gVar2, com.badlogic.gdx.graphics.g gVar3) {
        this.renderer.a(1);
        this.renderer.a(gVar3.b, gVar3.c, gVar3.d, gVar3.e);
        this.renderer.a(gVar.a, gVar.b);
        this.renderer.a(gVar3.b, gVar3.c, gVar3.d, gVar3.e);
        this.renderer.a(gVar2.a, gVar2.b);
        this.renderer.a();
    }

    private void drawContact(Contact contact) {
    }

    public void dispose() {
        this.batch.d();
    }
}
