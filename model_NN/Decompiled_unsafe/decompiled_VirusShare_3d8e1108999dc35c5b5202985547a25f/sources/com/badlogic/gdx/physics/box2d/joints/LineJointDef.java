package com.badlogic.gdx.physics.box2d.joints;

import com.badlogic.gdx.math.g;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.JointDef;

public class LineJointDef extends JointDef {
    public boolean enableLimit = false;
    public boolean enableMotor = false;
    public final g localAnchorA = new g();
    public final g localAnchorB = new g();
    public final g localAxisA = new g(1.0f, 0.0f);
    public float lowerTranslation = 0.0f;
    public float maxMotorForce = 0.0f;
    public float motorSpeed = 0.0f;
    public float upperTranslation = 0.0f;

    public LineJointDef() {
        this.type = JointDef.JointType.LineJoint;
    }

    public void initialize(Body body, Body body2, g gVar, g gVar2) {
        this.bodyA = body;
        this.bodyB = body2;
        this.localAnchorA.a(body.getLocalPoint(gVar));
        this.localAnchorB.a(body2.getLocalPoint(gVar));
        this.localAxisA.a(body.getLocalVector(gVar2));
    }
}
