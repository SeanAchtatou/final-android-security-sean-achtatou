package com.badlogic.gdx.physics.box2d;

import com.badlogic.gdx.math.g;
import com.badlogic.gdx.physics.box2d.BodyDef;
import java.util.ArrayList;

public class Body {
    protected final long addr;
    private ArrayList<Fixture> fixtures = new ArrayList<>(2);
    protected ArrayList<JointEdge> joints = new ArrayList<>(2);
    public final g linVelLoc = new g();
    public final g linVelWorld = new g();
    private final g linearVelocity = new g();
    private final g localCenter = new g();
    private final g localPoint = new g();
    public final g localPoint2 = new g();
    public final g localVector = new g();
    private final MassData massData = new MassData();
    private final g position = new g();
    private final float[] tmp = new float[4];
    private final Transform transform = new Transform();
    private Object userData;
    private final World world;
    private final g worldCenter = new g();
    private final g worldVector = new g();

    private native void jniApplyAngularImpulse(long j, float f);

    private native void jniApplyForce(long j, float f, float f2, float f3, float f4);

    private native void jniApplyLinearImpulse(long j, float f, float f2, float f3, float f4);

    private native void jniApplyTorque(long j, float f);

    private native long jniCreateFixture(long j, long j2, float f);

    private native long jniCreateFixture(long j, long j2, float f, float f2, float f3, boolean z, short s, short s2, short s3);

    private native void jniDestroyFixture(long j, long j2);

    private native float jniGetAngle(long j);

    private native float jniGetAngularDamping(long j);

    private native float jniGetAngularVelocity(long j);

    private native float jniGetInertia(long j);

    private native float jniGetLinearDamping(long j);

    private native void jniGetLinearVelocity(long j, float[] fArr);

    private native void jniGetLinearVelocityFromLocalPoint(long j, float f, float f2, float[] fArr);

    private native void jniGetLinearVelocityFromWorldPoint(long j, float f, float f2, float[] fArr);

    private native void jniGetLocalCenter(long j, float[] fArr);

    private native void jniGetLocalPoint(long j, float f, float f2, float[] fArr);

    private native void jniGetLocalVector(long j, float f, float f2, float[] fArr);

    private native float jniGetMass(long j);

    private native void jniGetMassData(long j, float[] fArr);

    private native void jniGetPosition(long j, float[] fArr);

    private native void jniGetTransform(long j, float[] fArr);

    private native int jniGetType(long j);

    private native void jniGetWorldCenter(long j, float[] fArr);

    private native void jniGetWorldPoint(long j, float f, float f2, float[] fArr);

    private native void jniGetWorldVector(long j, float f, float f2, float[] fArr);

    private native boolean jniIsActive(long j);

    private native boolean jniIsAwake(long j);

    private native boolean jniIsBullet(long j);

    private native boolean jniIsFixedRotation(long j);

    private native boolean jniIsSleepingAllowed(long j);

    private native void jniResetMassData(long j);

    private native void jniSetActive(long j, boolean z);

    private native void jniSetAngularDamping(long j, float f);

    private native void jniSetAngularVelocity(long j, float f);

    private native void jniSetAwake(long j, boolean z);

    private native void jniSetBullet(long j, boolean z);

    private native void jniSetFixedRotation(long j, boolean z);

    private native void jniSetLinearDamping(long j, float f);

    private native void jniSetLinearVelocity(long j, float f, float f2);

    private native void jniSetMassData(long j, float f, float f2, float f3, float f4);

    private native void jniSetSleepingAllowed(long j, boolean z);

    private native void jniSetTransform(long j, float f, float f2, float f3);

    private native void jniSetType(long j, int i);

    protected Body(World world2, long j) {
        this.world = world2;
        this.addr = j;
    }

    public Fixture createFixture(FixtureDef fixtureDef) {
        Fixture fixture = new Fixture(this, jniCreateFixture(this.addr, fixtureDef.shape.addr, fixtureDef.friction, fixtureDef.restitution, fixtureDef.density, fixtureDef.isSensor, fixtureDef.filter.categoryBits, fixtureDef.filter.maskBits, fixtureDef.filter.groupIndex));
        this.world.fixtures.a(fixture.addr, fixture);
        this.fixtures.add(fixture);
        return fixture;
    }

    public Fixture createFixture(Shape shape, float f) {
        Fixture fixture = new Fixture(this, jniCreateFixture(this.addr, shape.addr, f));
        this.world.fixtures.a(fixture.addr, fixture);
        this.fixtures.add(fixture);
        return fixture;
    }

    public void destroyFixture(Fixture fixture) {
        jniDestroyFixture(this.addr, fixture.addr);
        this.world.fixtures.b(fixture.addr);
        this.fixtures.remove(fixture);
    }

    public void setTransform(g gVar, float f) {
        jniSetTransform(this.addr, gVar.a, gVar.b, f);
    }

    public Transform getTransform() {
        jniGetTransform(this.addr, this.transform.vals);
        return this.transform;
    }

    public g getPosition() {
        jniGetPosition(this.addr, this.tmp);
        this.position.a = this.tmp[0];
        this.position.b = this.tmp[1];
        return this.position;
    }

    public float getAngle() {
        return jniGetAngle(this.addr);
    }

    public g getWorldCenter() {
        jniGetWorldCenter(this.addr, this.tmp);
        this.worldCenter.a = this.tmp[0];
        this.worldCenter.b = this.tmp[1];
        return this.worldCenter;
    }

    public g getLocalCenter() {
        jniGetLocalCenter(this.addr, this.tmp);
        this.localCenter.a = this.tmp[0];
        this.localCenter.b = this.tmp[1];
        return this.localCenter;
    }

    public void setLinearVelocity(g gVar) {
        jniSetLinearVelocity(this.addr, gVar.a, gVar.b);
    }

    public g getLinearVelocity() {
        jniGetLinearVelocity(this.addr, this.tmp);
        this.linearVelocity.a = this.tmp[0];
        this.linearVelocity.b = this.tmp[1];
        return this.linearVelocity;
    }

    public void setAngularVelocity(float f) {
        jniSetAngularVelocity(this.addr, f);
    }

    public float getAngularVelocity() {
        return jniGetAngularVelocity(this.addr);
    }

    public void applyForce(g gVar, g gVar2) {
        jniApplyForce(this.addr, gVar.a, gVar.b, gVar2.a, gVar2.b);
    }

    public void applyTorque(float f) {
        jniApplyTorque(this.addr, f);
    }

    public void applyLinearImpulse(g gVar, g gVar2) {
        jniApplyLinearImpulse(this.addr, gVar.a, gVar.b, gVar2.a, gVar2.b);
    }

    public void applyAngularImpulse(float f) {
        jniApplyAngularImpulse(this.addr, f);
    }

    public float getMass() {
        return jniGetMass(this.addr);
    }

    public float getInertia() {
        return jniGetInertia(this.addr);
    }

    public MassData getMassData() {
        jniGetMassData(this.addr, this.tmp);
        this.massData.mass = this.tmp[0];
        this.massData.center.a = this.tmp[1];
        this.massData.center.b = this.tmp[2];
        this.massData.I = this.tmp[3];
        return this.massData;
    }

    public void setMassData(MassData massData2) {
        jniSetMassData(this.addr, massData2.mass, massData2.center.a, massData2.center.b, massData2.I);
    }

    public void resetMassData() {
        jniResetMassData(this.addr);
    }

    public g getWorldPoint(g gVar) {
        jniGetWorldPoint(this.addr, gVar.a, gVar.b, this.tmp);
        this.localPoint.a = this.tmp[0];
        this.localPoint.b = this.tmp[1];
        return this.localPoint;
    }

    public g getWorldVector(g gVar) {
        jniGetWorldVector(this.addr, gVar.a, gVar.b, this.tmp);
        this.worldVector.a = this.tmp[0];
        this.worldVector.b = this.tmp[1];
        return this.worldVector;
    }

    public g getLocalPoint(g gVar) {
        jniGetLocalPoint(this.addr, gVar.a, gVar.b, this.tmp);
        this.localPoint2.a = this.tmp[0];
        this.localPoint2.b = this.tmp[1];
        return this.localPoint2;
    }

    public g getLocalVector(g gVar) {
        jniGetLocalVector(this.addr, gVar.a, gVar.b, this.tmp);
        this.localVector.a = this.tmp[0];
        this.localVector.b = this.tmp[1];
        return this.localVector;
    }

    public g getLinearVelocityFromWorldPoint(g gVar) {
        jniGetLinearVelocityFromWorldPoint(this.addr, gVar.a, gVar.b, this.tmp);
        this.linVelWorld.a = this.tmp[0];
        this.linVelWorld.b = this.tmp[1];
        return this.linVelWorld;
    }

    public g getLinearVelocityFromLocalPoint(g gVar) {
        jniGetLinearVelocityFromLocalPoint(this.addr, gVar.a, gVar.b, this.tmp);
        this.linVelLoc.a = this.tmp[0];
        this.linVelLoc.b = this.tmp[1];
        return this.linVelLoc;
    }

    public float getLinearDamping() {
        return jniGetLinearDamping(this.addr);
    }

    public void setLinearDamping(float f) {
        jniSetLinearDamping(this.addr, f);
    }

    public float getAngularDamping() {
        return jniGetAngularDamping(this.addr);
    }

    public void setAngularDamping(float f) {
        jniSetAngularDamping(this.addr, f);
    }

    public void setType(BodyDef.BodyType bodyType) {
        jniSetType(this.addr, bodyType.getValue());
    }

    public BodyDef.BodyType getType() {
        int jniGetType = jniGetType(this.addr);
        if (jniGetType == 0) {
            return BodyDef.BodyType.StaticBody;
        }
        if (jniGetType == 1) {
            return BodyDef.BodyType.KinematicBody;
        }
        if (jniGetType == 2) {
            return BodyDef.BodyType.DynamicBody;
        }
        return BodyDef.BodyType.StaticBody;
    }

    public void setBullet(boolean z) {
        jniSetBullet(this.addr, z);
    }

    public boolean isBullet() {
        return jniIsBullet(this.addr);
    }

    public void setSleepingAllowed(boolean z) {
        jniSetSleepingAllowed(this.addr, z);
    }

    public boolean isSleepingAllowed() {
        return jniIsSleepingAllowed(this.addr);
    }

    public void setAwake(boolean z) {
        jniSetAwake(this.addr, z);
    }

    public boolean isAwake() {
        return jniIsAwake(this.addr);
    }

    public void setActive(boolean z) {
        jniSetActive(this.addr, z);
    }

    public boolean isActive() {
        return jniIsActive(this.addr);
    }

    public void setFixedRotation(boolean z) {
        jniSetFixedRotation(this.addr, z);
    }

    public boolean isFixedRotation() {
        return jniIsFixedRotation(this.addr);
    }

    public ArrayList<Fixture> getFixtureList() {
        return this.fixtures;
    }

    public ArrayList<JointEdge> getJointList() {
        return this.joints;
    }

    public World getWorld() {
        return this.world;
    }

    public Object getUserData() {
        return this.userData;
    }

    public void setUserData(Object obj) {
        this.userData = obj;
    }
}
