package com.badlogic.gdx.physics.box2d;

import com.badlogic.gdx.math.g;
import com.badlogic.gdx.physics.box2d.Shape;

public class PolygonShape extends Shape {
    private static float[] verts = new float[2];

    private native void jniGetVertex(long j, int i, float[] fArr);

    private native int jniGetVertexCount(long j);

    private native void jniSet(long j, float[] fArr);

    private native void jniSetAsBox(long j, float f, float f2);

    private native void jniSetAsBox(long j, float f, float f2, float f3, float f4, float f5);

    private native void jniSetAsEdge(long j, float f, float f2, float f3, float f4);

    private native long newPolygonShape();

    public PolygonShape() {
        this.addr = newPolygonShape();
    }

    protected PolygonShape(long j) {
        this.addr = j;
    }

    public Shape.Type getType() {
        return Shape.Type.Polygon;
    }

    public void set(g[] gVarArr) {
        int i = 0;
        float[] fArr = new float[(gVarArr.length * 2)];
        int i2 = 0;
        while (i2 < gVarArr.length * 2) {
            fArr[i2] = gVarArr[i].a;
            fArr[i2 + 1] = gVarArr[i].b;
            i2 += 2;
            i++;
        }
        jniSet(this.addr, fArr);
    }

    public void setAsBox(float f, float f2) {
        jniSetAsBox(this.addr, f, f2);
    }

    public void setAsBox(float f, float f2, g gVar, float f3) {
        jniSetAsBox(this.addr, f, f2, gVar.a, gVar.b, f3);
    }

    public void setAsEdge(g gVar, g gVar2) {
        jniSetAsEdge(this.addr, gVar.a, gVar.b, gVar2.a, gVar2.b);
    }

    public int getVertexCount() {
        return jniGetVertexCount(this.addr);
    }

    public void getVertex(int i, g gVar) {
        jniGetVertex(this.addr, i, verts);
        gVar.a = verts[0];
        gVar.b = verts[1];
    }
}
