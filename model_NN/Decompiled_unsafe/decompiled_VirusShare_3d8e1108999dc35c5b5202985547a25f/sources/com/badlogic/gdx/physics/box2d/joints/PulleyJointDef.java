package com.badlogic.gdx.physics.box2d.joints;

import com.badlogic.gdx.math.g;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.JointDef;

public class PulleyJointDef extends JointDef {
    private static final float minPulleyLength = 2.0f;
    public final g groundAnchorA = new g(-1.0f, 1.0f);
    public final g groundAnchorB = new g(1.0f, 1.0f);
    public float lengthA = 0.0f;
    public float lengthB = 0.0f;
    public final g localAnchorA = new g(-1.0f, 0.0f);
    public final g localAnchorB = new g(1.0f, 0.0f);
    public float maxLengthA = 0.0f;
    public float maxLengthB = 0.0f;
    public float ratio = 1.0f;

    public PulleyJointDef() {
        this.type = JointDef.JointType.PulleyJoint;
        this.collideConnected = true;
    }

    public void initialize(Body body, Body body2, g gVar, g gVar2, g gVar3, g gVar4, float f) {
        this.bodyA = body;
        this.bodyB = body2;
        this.groundAnchorA.a(gVar);
        this.groundAnchorB.a(gVar2);
        this.localAnchorA.a(body.getLocalPoint(gVar3));
        this.localAnchorB.a(body2.getLocalPoint(gVar4));
        this.lengthA = gVar3.d(gVar);
        this.lengthB = gVar4.d(gVar2);
        this.ratio = f;
        float f2 = this.lengthA + (this.lengthB * f);
        this.maxLengthA = f2 - (f * minPulleyLength);
        this.maxLengthB = (f2 - minPulleyLength) / f;
    }
}
