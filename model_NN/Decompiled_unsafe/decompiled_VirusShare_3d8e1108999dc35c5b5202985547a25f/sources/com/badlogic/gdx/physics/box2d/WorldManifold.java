package com.badlogic.gdx.physics.box2d;

import com.badlogic.gdx.math.g;

public class WorldManifold {
    protected final g normal = new g();
    protected int numContactPoints;
    protected final g[] points = {new g(), new g()};

    protected WorldManifold() {
    }

    public g getNormal() {
        return this.normal;
    }

    public g[] getPoints() {
        return this.points;
    }

    public int getNumberOfContactPoints() {
        return this.numContactPoints;
    }
}
