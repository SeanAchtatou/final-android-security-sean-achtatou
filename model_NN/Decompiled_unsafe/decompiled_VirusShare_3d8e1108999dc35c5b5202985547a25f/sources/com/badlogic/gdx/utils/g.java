package com.badlogic.gdx.utils;

import com.badlogic.gdx.h;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.CRC32;

public class g {
    private static boolean a = false;
    private static boolean b = false;
    private static boolean c = System.getProperty("os.name").contains("Windows");
    private static boolean d = System.getProperty("os.name").contains("Linux");
    private static boolean e = System.getProperty("os.name").contains("Mac");
    private static boolean f = System.getProperty("os.arch").equals("amd64");
    private static File g = new File(System.getProperty("java.io.tmpdir") + "/libgdx/" + a("gdx.dll"));

    private static String a(String str) {
        InputStream resourceAsStream = g.class.getResourceAsStream("/" + str);
        if (resourceAsStream == null) {
            return h.a;
        }
        CRC32 crc32 = new CRC32();
        byte[] bArr = new byte[4096];
        while (true) {
            try {
                int read = resourceAsStream.read(bArr);
                if (read == -1) {
                    break;
                }
                crc32.update(bArr, 0, read);
            } catch (Exception e2) {
                try {
                    resourceAsStream.close();
                } catch (Exception e3) {
                }
            }
        }
        return Long.toString(crc32.getValue());
    }

    private static boolean a(String str, String str2) {
        String b2 = b(str, str2);
        if (b2 != null) {
            System.load(b2);
        }
        return b2 != null;
    }

    private static String b(String str, String str2) {
        if (!f) {
            str2 = str;
        }
        File file = new File(g, str2);
        try {
            InputStream resourceAsStream = g.class.getResourceAsStream("/" + str2);
            if (resourceAsStream == null) {
                return null;
            }
            g.mkdirs();
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            byte[] bArr = new byte[4096];
            while (true) {
                int read = resourceAsStream.read(bArr);
                if (read == -1) {
                    break;
                }
                fileOutputStream.write(bArr, 0, read);
            }
            resourceAsStream.close();
            fileOutputStream.close();
            if (file.exists()) {
                return file.getAbsolutePath();
            }
            return null;
        } catch (IOException e2) {
        }
    }

    public static void a() {
        if (a) {
            System.out.println("So you don't like our native lib loading? Good, you are on your own now. We don't give support from here on out");
        } else if (!b) {
            String property = System.getProperty("java.vm.name");
            if (property == null || !property.contains("Dalvik")) {
                if (c) {
                    b = a("gdx.dll", "gdx-64.dll");
                } else if (e) {
                    b = a("libgdx.dylib", "libgdx.dylib");
                } else if (d) {
                    b = a("libgdx.so", "libgdx-64.so");
                }
                if (b) {
                    return;
                }
            }
            if (!f || e) {
                System.loadLibrary("gdx");
            } else {
                System.loadLibrary("gdx-64");
            }
            b = true;
        }
    }
}
