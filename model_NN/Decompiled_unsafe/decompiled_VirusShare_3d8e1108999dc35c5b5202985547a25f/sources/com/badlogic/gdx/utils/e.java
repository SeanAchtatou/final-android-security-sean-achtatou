package com.badlogic.gdx.utils;

import java.util.Iterator;
import java.util.NoSuchElementException;

public final class e<T> implements Iterable<T> {
    public T[] a;
    public int b;
    public boolean c;
    private a d;

    public e() {
        this(true);
    }

    public e(boolean z) {
        this.c = z;
        this.a = (Object[]) new Object[16];
    }

    public final Iterator<T> iterator() {
        if (this.d == null) {
            this.d = new a(this);
        }
        this.d.a = 0;
        return this.d;
    }

    public final String toString() {
        if (this.b == 0) {
            return "[]";
        }
        T[] tArr = this.a;
        StringBuilder sb = new StringBuilder(32);
        sb.append('[');
        sb.append((Object) tArr[0]);
        for (int i = 1; i < this.b; i++) {
            sb.append(", ");
            sb.append((Object) tArr[i]);
        }
        sb.append(']');
        return sb.toString();
    }

    public static class a<T> implements Iterator<T> {
        int a;
        private final e<T> b;

        public a(e<T> eVar) {
            this.b = eVar;
        }

        public final boolean hasNext() {
            return this.a < this.b.b;
        }

        public final T next() {
            if (this.a >= this.b.b) {
                throw new NoSuchElementException(String.valueOf(this.a));
            }
            T[] tArr = this.b.a;
            int i = this.a;
            this.a = i + 1;
            return tArr[i];
        }

        public final void remove() {
            this.a--;
            e<T> eVar = this.b;
            int i = this.a;
            if (i >= eVar.b) {
                throw new IndexOutOfBoundsException(String.valueOf(i));
            }
            T[] tArr = eVar.a;
            eVar.b--;
            if (eVar.c) {
                System.arraycopy(tArr, i + 1, tArr, i, eVar.b - i);
            } else {
                tArr[i] = tArr[eVar.b];
            }
            tArr[eVar.b] = null;
        }
    }
}
