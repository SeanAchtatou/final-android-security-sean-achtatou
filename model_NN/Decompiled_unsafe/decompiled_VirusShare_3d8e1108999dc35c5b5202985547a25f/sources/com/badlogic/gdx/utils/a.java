package com.badlogic.gdx.utils;

import com.badlogic.gdx.physics.box2d.Transform;
import java.util.Iterator;

public final class a<V> {
    public int a;
    long[] b;
    V[] c;
    int d;
    int e;
    V f;
    boolean g;
    private float h;
    private int i;
    private int j;
    private int k;
    private int l;
    private int m;
    private b n;

    public a(byte b2) {
        this();
    }

    private a() {
        if (this.d > 1073741824) {
            throw new IllegalArgumentException("initialCapacity is too large: " + 100);
        }
        this.d = com.badlogic.gdx.math.b.a(100);
        this.h = 0.8f;
        this.k = (int) (((float) this.d) * 0.8f);
        this.j = this.d - 1;
        this.i = 31 - Integer.numberOfTrailingZeros(this.d);
        this.l = Math.max(3, ((int) Math.ceil(Math.log((double) this.d))) + 1);
        this.m = Math.max(Math.min(this.d, 32), ((int) Math.sqrt((double) this.d)) / 4);
        this.b = new long[(this.d + this.l)];
        this.c = (Object[]) new Object[this.b.length];
    }

    public final V a(long j2, V v) {
        if (j2 == 0) {
            V v2 = this.f;
            this.f = v;
            this.g = true;
            this.a++;
            return v2;
        }
        int i2 = (int) (((long) this.j) & j2);
        long j3 = this.b[i2];
        if (j3 == j2) {
            V v3 = this.c[i2];
            this.c[i2] = v;
            return v3;
        }
        int c2 = c(j2);
        long j4 = this.b[c2];
        if (j4 == j2) {
            V v4 = this.c[c2];
            this.c[c2] = v;
            return v4;
        }
        int d2 = d(j2);
        long j5 = this.b[d2];
        if (j5 == j2) {
            V v5 = this.c[d2];
            this.c[d2] = v;
            return v5;
        } else if (j3 == 0) {
            this.b[i2] = j2;
            this.c[i2] = v;
            int i3 = this.a;
            this.a = i3 + 1;
            if (i3 >= this.k) {
                b(this.d << 1);
            }
            return null;
        } else if (j4 == 0) {
            this.b[c2] = j2;
            this.c[c2] = v;
            int i4 = this.a;
            this.a = i4 + 1;
            if (i4 >= this.k) {
                b(this.d << 1);
            }
            return null;
        } else if (j5 == 0) {
            this.b[d2] = j2;
            this.c[d2] = v;
            int i5 = this.a;
            this.a = i5 + 1;
            if (i5 >= this.k) {
                b(this.d << 1);
            }
            return null;
        } else {
            a(j2, v, i2, j3, c2, j4, d2, j5);
            return null;
        }
    }

    private void a(long j2, V v, int i2, long j3, int i3, long j4, int i4, long j5) {
        long[] jArr = this.b;
        V[] vArr = this.c;
        int i5 = this.j;
        int i6 = 0;
        int i7 = this.m;
        do {
            switch (com.badlogic.gdx.math.b.a()) {
                case Transform.POS_X:
                    V v2 = vArr[i2];
                    jArr[i2] = j2;
                    vArr[i2] = v;
                    v = v2;
                    j2 = j3;
                    break;
                case 1:
                    V v3 = vArr[i3];
                    jArr[i3] = j2;
                    vArr[i3] = v;
                    v = v3;
                    j2 = j4;
                    break;
                default:
                    V v4 = vArr[i4];
                    jArr[i4] = j2;
                    vArr[i4] = v;
                    v = v4;
                    j2 = j5;
                    break;
            }
            i2 = (int) (((long) i5) & j2);
            j3 = jArr[i2];
            if (j3 == 0) {
                jArr[i2] = j2;
                vArr[i2] = v;
                int i8 = this.a;
                this.a = i8 + 1;
                if (i8 >= this.k) {
                    b(this.d << 1);
                    return;
                }
                return;
            }
            i3 = c(j2);
            j4 = jArr[i3];
            if (j4 == 0) {
                jArr[i3] = j2;
                vArr[i3] = v;
                int i9 = this.a;
                this.a = i9 + 1;
                if (i9 >= this.k) {
                    b(this.d << 1);
                    return;
                }
                return;
            }
            i4 = d(j2);
            j5 = jArr[i4];
            if (j5 == 0) {
                jArr[i4] = j2;
                vArr[i4] = v;
                int i10 = this.a;
                this.a = i10 + 1;
                if (i10 >= this.k) {
                    b(this.d << 1);
                    return;
                }
                return;
            }
            i6++;
        } while (i6 != i7);
        if (this.e == this.l) {
            b(this.d << 1);
            a(j2, v);
            return;
        }
        long[] jArr2 = this.b;
        int i11 = this.d;
        int i12 = this.e + i11;
        while (i11 < i12) {
            if (jArr2[i11] == j2) {
                this.c[i11] = v;
                return;
            }
            i11++;
        }
        int i13 = this.d + this.e;
        jArr2[i13] = j2;
        this.c[i13] = v;
        this.e++;
    }

    public final V a(long j2) {
        if (j2 == 0) {
            return this.f;
        }
        int i2 = (int) (((long) this.j) & j2);
        if (this.b[i2] != j2) {
            i2 = c(j2);
            if (this.b[i2] != j2) {
                i2 = d(j2);
                if (this.b[i2] != j2) {
                    long[] jArr = this.b;
                    int i3 = this.d;
                    int i4 = this.e + i3;
                    while (i3 < i4) {
                        if (jArr[i3] == j2) {
                            return this.c[i3];
                        }
                        i3++;
                    }
                    return null;
                }
            }
        }
        return this.c[i2];
    }

    public final V b(long j2) {
        if (j2 != 0) {
            int i2 = (int) (((long) this.j) & j2);
            if (this.b[i2] == j2) {
                this.b[i2] = 0;
                V v = this.c[i2];
                this.c[i2] = null;
                this.a--;
                return v;
            }
            int c2 = c(j2);
            if (this.b[c2] == j2) {
                this.b[c2] = 0;
                V v2 = this.c[c2];
                this.c[c2] = null;
                this.a--;
                return v2;
            }
            int d2 = d(j2);
            if (this.b[d2] == j2) {
                this.b[d2] = 0;
                V v3 = this.c[d2];
                this.c[d2] = null;
                this.a--;
                return v3;
            }
            long[] jArr = this.b;
            int i3 = this.d;
            int i4 = this.e + i3;
            while (i3 < i4) {
                if (jArr[i3] == j2) {
                    V v4 = this.c[i3];
                    a(i3);
                    this.a--;
                    return v4;
                }
                i3++;
            }
            return null;
        } else if (!this.g) {
            return null;
        } else {
            V v5 = this.f;
            this.f = null;
            this.g = false;
            this.a--;
            return v5;
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(int i2) {
        this.e--;
        int i3 = this.d + this.e;
        if (i2 < i3) {
            this.b[i2] = this.b[i3];
            this.c[i2] = this.c[i3];
            this.c[i3] = null;
            return;
        }
        this.c[i2] = null;
    }

    private void b(int i2) {
        int i3 = this.d + this.e;
        this.d = i2;
        this.k = (int) (((float) i2) * this.h);
        this.j = i2 - 1;
        this.i = 31 - Integer.numberOfTrailingZeros(i2);
        this.l = Math.max(3, (int) Math.ceil(Math.log((double) i2)));
        this.m = Math.max(Math.min(this.d, 32), ((int) Math.sqrt((double) this.d)) / 4);
        long[] jArr = this.b;
        V[] vArr = this.c;
        this.b = new long[(this.l + i2)];
        this.c = (Object[]) new Object[(this.l + i2)];
        this.a = this.g ? 1 : 0;
        this.e = 0;
        int i4 = 0;
        while (true) {
            int i5 = i4;
            if (i5 < i3) {
                long j2 = jArr[i5];
                if (j2 != 0) {
                    V v = vArr[i5];
                    if (j2 == 0) {
                        this.f = v;
                        this.g = true;
                    } else {
                        int i6 = (int) (((long) this.j) & j2);
                        long j3 = this.b[i6];
                        if (j3 == 0) {
                            this.b[i6] = j2;
                            this.c[i6] = v;
                            int i7 = this.a;
                            this.a = i7 + 1;
                            if (i7 >= this.k) {
                                b(this.d << 1);
                            }
                        } else {
                            int c2 = c(j2);
                            long j4 = this.b[c2];
                            if (j4 == 0) {
                                this.b[c2] = j2;
                                this.c[c2] = v;
                                int i8 = this.a;
                                this.a = i8 + 1;
                                if (i8 >= this.k) {
                                    b(this.d << 1);
                                }
                            } else {
                                int d2 = d(j2);
                                long j5 = this.b[d2];
                                if (j5 == 0) {
                                    this.b[d2] = j2;
                                    this.c[d2] = v;
                                    int i9 = this.a;
                                    this.a = i9 + 1;
                                    if (i9 >= this.k) {
                                        b(this.d << 1);
                                    }
                                } else {
                                    a(j2, v, i6, j3, c2, j4, d2, j5);
                                }
                            }
                        }
                    }
                }
                i4 = i5 + 1;
            } else {
                return;
            }
        }
    }

    private int c(long j2) {
        long j3 = -1262997959 * j2;
        return (int) ((j3 ^ (j3 >>> this.i)) & ((long) this.j));
    }

    private int d(long j2) {
        long j3 = -825114047 * j2;
        return (int) ((j3 ^ (j3 >>> this.i)) & ((long) this.j));
    }

    public final String toString() {
        if (this.a == 0) {
            return "[]";
        }
        StringBuilder sb = new StringBuilder(32);
        sb.append('[');
        long[] jArr = this.b;
        V[] vArr = this.c;
        int length = jArr.length;
        while (true) {
            int i2 = length;
            length = i2 - 1;
            if (i2 <= 0) {
                break;
            }
            long j2 = jArr[length];
            if (j2 != 0) {
                sb.append(j2);
                sb.append('=');
                sb.append((Object) vArr[length]);
                break;
            }
        }
        while (true) {
            int i3 = length - 1;
            if (length > 0) {
                long j3 = jArr[i3];
                if (j3 != 0) {
                    sb.append(", ");
                    sb.append(j3);
                    sb.append('=');
                    sb.append((Object) vArr[i3]);
                    length = i3;
                } else {
                    length = i3;
                }
            } else {
                sb.append(']');
                return sb.toString();
            }
        }
    }

    public final b<V> a() {
        if (this.n == null) {
            this.n = new b(this);
        } else {
            this.n.a();
        }
        return this.n;
    }

    /* renamed from: com.badlogic.gdx.utils.a$a  reason: collision with other inner class name */
    private static class C0009a<V> {
        public boolean a;
        final a<V> b;
        int c;
        int d;

        public C0009a(a<V> aVar) {
            this.b = aVar;
            a();
        }

        public void a() {
            this.d = -2;
            this.c = -1;
            if (this.b.g) {
                this.a = true;
            } else {
                b();
            }
        }

        /* access modifiers changed from: package-private */
        public final void b() {
            this.a = false;
            long[] jArr = this.b.b;
            int i = this.b.d + this.b.e;
            do {
                int i2 = this.c + 1;
                this.c = i2;
                if (i2 >= i) {
                    return;
                }
            } while (jArr[this.c] == 0);
            this.a = true;
        }

        public void remove() {
            if (this.d == -1 && this.b.g) {
                this.b.f = null;
                this.b.g = false;
            } else if (this.d < 0) {
                throw new IllegalStateException("next must be called before remove.");
            } else if (this.d >= this.b.d) {
                this.b.a(this.d);
            } else {
                this.b.b[this.d] = 0;
                this.b.c[this.d] = null;
            }
            this.d = -2;
            a<V> aVar = this.b;
            aVar.a--;
        }
    }

    public static class b<V> extends C0009a<V> implements Iterable<V>, Iterator<V> {
        public final /* bridge */ /* synthetic */ void a() {
            super.a();
        }

        public final /* bridge */ /* synthetic */ void remove() {
            super.remove();
        }

        public b(a<V> aVar) {
            super(aVar);
        }

        public final boolean hasNext() {
            return this.a;
        }

        public final V next() {
            V v;
            if (this.c == -1) {
                v = this.b.f;
            } else {
                v = this.b.c[this.c];
            }
            this.d = this.c;
            b();
            return v;
        }

        public final Iterator<V> iterator() {
            return this;
        }
    }
}
