package com.badlogic.gdx.utils;

public final class f extends RuntimeException {
    public f(String str) {
        super(str);
    }

    public f(String str, Throwable th) {
        super(str, th);
    }
}
