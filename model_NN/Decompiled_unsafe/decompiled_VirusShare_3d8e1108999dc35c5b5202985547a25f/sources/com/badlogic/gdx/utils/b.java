package com.badlogic.gdx.utils;

import java.lang.reflect.Array;

public abstract class b<T> {
    private int a;
    private final e<T> b;

    /* access modifiers changed from: protected */
    public abstract T a();

    public b() {
        this(Integer.MAX_VALUE);
    }

    public b(int i) {
        this.b = new e<>(false);
        this.a = i;
    }

    public final T b() {
        if (this.b.b == 0) {
            return a();
        }
        e<T> eVar = this.b;
        eVar.b--;
        T t = eVar.a[eVar.b];
        eVar.a[eVar.b] = null;
        return t;
    }

    public final void a(T t) {
        if (t == null) {
            throw new IllegalArgumentException("object cannot be null.");
        } else if (this.b.b < this.a) {
            e<T> eVar = this.b;
            T[] tArr = eVar.a;
            if (eVar.b == tArr.length) {
                int max = Math.max(8, (int) (((float) eVar.b) * 1.75f));
                T[] tArr2 = eVar.a;
                tArr = (Object[]) Array.newInstance(tArr2.getClass().getComponentType(), max);
                System.arraycopy(tArr2, 0, tArr, 0, Math.min(tArr2.length, tArr.length));
                eVar.a = tArr;
            }
            int i = eVar.b;
            eVar.b = i + 1;
            tArr[i] = t;
        }
    }
}
