package com.badlogic.gdx.utils;

import com.badlogic.gdx.math.b;

public final class d<K, V> {
    private int a;
    private K[] b;
    private V[] c;
    private int d;
    private int e;
    private float f;
    private int g;
    private int h;
    private int i;
    private int j;
    private int k;

    public d(byte b2) {
        this();
    }

    private d() {
        if (this.d > 1073741824) {
            throw new IllegalArgumentException("initialCapacity is too large: " + 32);
        }
        this.d = b.a(32);
        this.f = 0.8f;
        this.i = (int) (((float) this.d) * 0.8f);
        this.h = this.d - 1;
        this.g = 31 - Integer.numberOfTrailingZeros(this.d);
        this.j = Math.max(3, ((int) Math.ceil(Math.log((double) this.d))) + 1);
        this.k = Math.max(Math.min(this.d, 32), ((int) Math.sqrt((double) this.d)) / 4);
        this.b = (Object[]) new Object[(this.d + this.j)];
        this.c = (Object[]) new Object[this.b.length];
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    public final V a(K r11, V r12) {
        /*
            r10 = this;
            r9 = 0
            if (r11 != 0) goto L_0x000b
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "key cannot be null."
            r0.<init>(r1)
            throw r0
        L_0x000b:
            int r0 = r11.hashCode()
            int r1 = r10.h
            r3 = r0 & r1
            K[] r1 = r10.b
            r4 = r1[r3]
            boolean r1 = r11.equals(r4)
            if (r1 == 0) goto L_0x0026
            V[] r0 = r10.c
            r0 = r0[r3]
            V[] r1 = r10.c
            r1[r3] = r12
        L_0x0025:
            return r0
        L_0x0026:
            long r1 = (long) r0
            int r5 = r10.a(r1)
            K[] r1 = r10.b
            r6 = r1[r5]
            boolean r1 = r11.equals(r6)
            if (r1 == 0) goto L_0x003e
            V[] r0 = r10.c
            r0 = r0[r5]
            V[] r1 = r10.c
            r1[r5] = r12
            goto L_0x0025
        L_0x003e:
            long r0 = (long) r0
            int r7 = r10.b(r0)
            K[] r0 = r10.b
            r8 = r0[r7]
            boolean r0 = r11.equals(r8)
            if (r0 == 0) goto L_0x0056
            V[] r0 = r10.c
            r0 = r0[r7]
            V[] r1 = r10.c
            r1[r7] = r12
            goto L_0x0025
        L_0x0056:
            if (r4 != 0) goto L_0x0073
            K[] r0 = r10.b
            r0[r3] = r11
            V[] r0 = r10.c
            r0[r3] = r12
            int r0 = r10.a
            int r1 = r0 + 1
            r10.a = r1
            int r1 = r10.i
            if (r0 < r1) goto L_0x0071
            int r0 = r10.d
            int r0 = r0 << 1
            r10.a(r0)
        L_0x0071:
            r0 = r9
            goto L_0x0025
        L_0x0073:
            if (r6 != 0) goto L_0x0090
            K[] r0 = r10.b
            r0[r5] = r11
            V[] r0 = r10.c
            r0[r5] = r12
            int r0 = r10.a
            int r1 = r0 + 1
            r10.a = r1
            int r1 = r10.i
            if (r0 < r1) goto L_0x008e
            int r0 = r10.d
            int r0 = r0 << 1
            r10.a(r0)
        L_0x008e:
            r0 = r9
            goto L_0x0025
        L_0x0090:
            if (r8 != 0) goto L_0x00ae
            K[] r0 = r10.b
            r0[r7] = r11
            V[] r0 = r10.c
            r0[r7] = r12
            int r0 = r10.a
            int r1 = r0 + 1
            r10.a = r1
            int r1 = r10.i
            if (r0 < r1) goto L_0x00ab
            int r0 = r10.d
            int r0 = r0 << 1
            r10.a(r0)
        L_0x00ab:
            r0 = r9
            goto L_0x0025
        L_0x00ae:
            r0 = r10
            r1 = r11
            r2 = r12
            r0.a(r1, r2, r3, r4, r5, r6, r7, r8)
            r0 = r9
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.d.a(java.lang.Object, java.lang.Object):java.lang.Object");
    }

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: K
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:75)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    private void a(K r9, V r10, int r11, K r12, int r13, K r14, int r15, K r16) {
        /*
            r8 = this;
            K[] r2 = r8.b
            V[] r3 = r8.c
            int r4 = r8.h
            r0 = 0
            int r5 = r8.k
        L_0x0009:
            int r1 = com.badlogic.gdx.math.b.a()
            switch(r1) {
                case 0: goto L_0x0039;
                case 1: goto L_0x0042;
                default: goto L_0x0010;
            }
        L_0x0010:
            r1 = r3[r15]
            r2[r15] = r9
            r3[r15] = r10
            r10 = r1
            r9 = r16
        L_0x0019:
            int r1 = r9.hashCode()
            r11 = r1 & r4
            r12 = r2[r11]
            if (r12 != 0) goto L_0x004b
            r2[r11] = r9
            r3[r11] = r10
            int r0 = r8.a
            int r1 = r0 + 1
            r8.a = r1
            int r1 = r8.i
            if (r0 < r1) goto L_0x0038
            int r0 = r8.d
            int r0 = r0 << 1
            r8.a(r0)
        L_0x0038:
            return
        L_0x0039:
            r1 = r3[r11]
            r2[r11] = r9
            r3[r11] = r10
            r10 = r1
            r9 = r12
            goto L_0x0019
        L_0x0042:
            r1 = r3[r13]
            r2[r13] = r9
            r3[r13] = r10
            r10 = r1
            r9 = r14
            goto L_0x0019
        L_0x004b:
            long r6 = (long) r1
            int r13 = r8.a(r6)
            r14 = r2[r13]
            if (r14 != 0) goto L_0x006a
            r2[r13] = r9
            r3[r13] = r10
            int r0 = r8.a
            int r1 = r0 + 1
            r8.a = r1
            int r1 = r8.i
            if (r0 < r1) goto L_0x0038
            int r0 = r8.d
            int r0 = r0 << 1
            r8.a(r0)
            goto L_0x0038
        L_0x006a:
            long r6 = (long) r1
            int r15 = r8.b(r6)
            r16 = r2[r15]
            if (r16 != 0) goto L_0x0089
            r2[r15] = r9
            r3[r15] = r10
            int r0 = r8.a
            int r1 = r0 + 1
            r8.a = r1
            int r1 = r8.i
            if (r0 < r1) goto L_0x0038
            int r0 = r8.d
            int r0 = r0 << 1
            r8.a(r0)
            goto L_0x0038
        L_0x0089:
            int r0 = r0 + 1
            if (r0 != r5) goto L_0x0009
            int r0 = r8.e
            int r1 = r8.j
            if (r0 != r1) goto L_0x009e
            int r0 = r8.d
            int r0 = r0 << 1
            r8.a(r0)
            r8.a(r9, r10)
            goto L_0x0038
        L_0x009e:
            K[] r1 = r8.b
            int r0 = r8.d
            int r2 = r8.e
            int r2 = r2 + r0
        L_0x00a5:
            if (r0 >= r2) goto L_0x00b7
            r3 = r1[r0]
            boolean r3 = r9.equals(r3)
            if (r3 == 0) goto L_0x00b4
            V[] r1 = r8.c
            r1[r0] = r10
            goto L_0x0038
        L_0x00b4:
            int r0 = r0 + 1
            goto L_0x00a5
        L_0x00b7:
            int r0 = r8.d
            int r2 = r8.e
            int r0 = r0 + r2
            r1[r0] = r9
            V[] r1 = r8.c
            r1[r0] = r10
            int r0 = r8.e
            int r0 = r0 + 1
            r8.e = r0
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.utils.d.a(java.lang.Object, java.lang.Object, int, java.lang.Object, int, java.lang.Object, int, java.lang.Object):void");
    }

    public final V a(String str) {
        int hashCode = str.hashCode();
        int i2 = this.h & hashCode;
        if (!str.equals(this.b[i2])) {
            i2 = a((long) hashCode);
            if (!str.equals(this.b[i2])) {
                i2 = b((long) hashCode);
                if (!str.equals(this.b[i2])) {
                    K[] kArr = this.b;
                    int i3 = this.d;
                    int i4 = this.e + i3;
                    while (i3 < i4) {
                        if (str.equals(kArr[i3])) {
                            return this.c[i3];
                        }
                        i3++;
                    }
                    return null;
                }
            }
        }
        return this.c[i2];
    }

    private void a(int i2) {
        int i3 = this.d + this.e;
        this.d = i2;
        this.i = (int) (((float) i2) * this.f);
        this.h = i2 - 1;
        this.g = 31 - Integer.numberOfTrailingZeros(i2);
        this.j = Math.max(3, (int) Math.ceil(Math.log((double) i2)));
        this.k = Math.max(Math.min(this.d, 32), ((int) Math.sqrt((double) this.d)) / 4);
        K[] kArr = this.b;
        V[] vArr = this.c;
        this.b = (Object[]) new Object[(this.j + i2)];
        this.c = (Object[]) new Object[(this.j + i2)];
        this.a = 0;
        this.e = 0;
        for (int i4 = 0; i4 < i3; i4++) {
            K k2 = kArr[i4];
            if (k2 != null) {
                V v = vArr[i4];
                int hashCode = k2.hashCode();
                int i5 = this.h & hashCode;
                K k3 = this.b[i5];
                if (k3 == null) {
                    this.b[i5] = k2;
                    this.c[i5] = v;
                    int i6 = this.a;
                    this.a = i6 + 1;
                    if (i6 >= this.i) {
                        a(this.d << 1);
                    }
                } else {
                    int a2 = a((long) hashCode);
                    K k4 = this.b[a2];
                    if (k4 == null) {
                        this.b[a2] = k2;
                        this.c[a2] = v;
                        int i7 = this.a;
                        this.a = i7 + 1;
                        if (i7 >= this.i) {
                            a(this.d << 1);
                        }
                    } else {
                        int b2 = b((long) hashCode);
                        K k5 = this.b[b2];
                        if (k5 == null) {
                            this.b[b2] = k2;
                            this.c[b2] = v;
                            int i8 = this.a;
                            this.a = i8 + 1;
                            if (i8 >= this.i) {
                                a(this.d << 1);
                            }
                        } else {
                            a(k2, v, i5, k3, a2, k4, b2, k5);
                        }
                    }
                }
            }
        }
    }

    private int a(long j2) {
        long j3 = -1262997959 * j2;
        return (int) ((j3 ^ (j3 >>> this.g)) & ((long) this.h));
    }

    private int b(long j2) {
        long j3 = -825114047 * j2;
        return (int) ((j3 ^ (j3 >>> this.g)) & ((long) this.h));
    }

    public final String toString() {
        if (this.a == 0) {
            return "[]";
        }
        StringBuilder sb = new StringBuilder(32);
        sb.append('[');
        K[] kArr = this.b;
        V[] vArr = this.c;
        int length = kArr.length;
        while (true) {
            int i2 = length;
            length = i2 - 1;
            if (i2 <= 0) {
                break;
            }
            K k2 = kArr[length];
            if (k2 != null) {
                sb.append((Object) k2);
                sb.append('=');
                sb.append((Object) vArr[length]);
                break;
            }
        }
        while (true) {
            int i3 = length - 1;
            if (length > 0) {
                K k3 = kArr[i3];
                if (k3 != null) {
                    sb.append(", ");
                    sb.append((Object) k3);
                    sb.append('=');
                    sb.append((Object) vArr[i3]);
                    length = i3;
                } else {
                    length = i3;
                }
            } else {
                sb.append(']');
                return sb.toString();
            }
        }
    }
}
