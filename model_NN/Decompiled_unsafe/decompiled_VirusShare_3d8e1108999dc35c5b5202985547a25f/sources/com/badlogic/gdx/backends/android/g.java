package com.badlogic.gdx.backends.android;

import com.badlogic.gdx.graphics.j;
import java.nio.Buffer;
import java.nio.IntBuffer;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

final class g extends f implements j {
    private final GL11 a;

    public g(GL10 gl10) {
        super(gl10);
        this.a = (GL11) gl10;
    }

    public final void a(int i, int i2) {
        this.a.glBindBuffer(i, i2);
    }

    public final void a(int i, int i2, Buffer buffer, int i3) {
        this.a.glBufferData(i, i2, buffer, i3);
    }

    public final void c(int i, int i2, Buffer buffer) {
        this.a.glBufferSubData(i, 0, i2, buffer);
    }

    public final void a(IntBuffer intBuffer) {
        this.a.glDeleteBuffers(1, intBuffer);
    }

    public final void b(IntBuffer intBuffer) {
        this.a.glGenBuffers(1, intBuffer);
    }

    public final void a(int i, int i2, int i3, int i4) {
        this.a.glColorPointer(i, i2, i3, i4);
    }

    public final void b(int i, int i2) {
        this.a.glNormalPointer(5126, i, i2);
    }

    public final void b(int i, int i2, int i3) {
        this.a.glTexCoordPointer(i, 5126, i2, i3);
    }

    public final void a(int i, int i2, int i3) {
        this.a.glVertexPointer(i, 5126, i2, i3);
    }

    public final void c(int i, int i2, int i3) {
        this.a.glDrawElements(i, i2, 5123, i3);
    }
}
