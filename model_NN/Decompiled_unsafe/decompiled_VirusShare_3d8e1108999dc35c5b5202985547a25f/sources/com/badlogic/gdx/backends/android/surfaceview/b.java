package com.badlogic.gdx.backends.android.surfaceview;

import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import javax.microedition.khronos.opengles.GL;

final class b extends h {
    private boolean e;
    private boolean f;
    private Thread g;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b(GL gl, int i) {
        super(gl);
        boolean z;
        boolean z2 = true;
        if ((i & 1) != 0) {
            z = true;
        } else {
            z = false;
        }
        this.e = z;
        this.f = (i & 2) == 0 ? false : z2;
    }

    private void a() {
        if (this.f) {
            Thread currentThread = Thread.currentThread();
            if (this.g == null) {
                this.g = currentThread;
            } else if (!this.g.equals(currentThread)) {
                throw new g("OpenGL method called from wrong thread.");
            }
        }
    }

    private void b() {
        int glGetError;
        if (this.e && (glGetError = this.a.glGetError()) != 0) {
            throw new g(glGetError);
        }
    }

    public final void glActiveTexture(int i) {
        a();
        this.a.glActiveTexture(i);
        b();
    }

    public final void glAlphaFunc(int i, float f2) {
        a();
        this.a.glAlphaFunc(i, f2);
        b();
    }

    public final void glAlphaFuncx(int i, int i2) {
        a();
        this.a.glAlphaFuncx(i, i2);
        b();
    }

    public final void glBindTexture(int i, int i2) {
        a();
        this.a.glBindTexture(i, i2);
        b();
    }

    public final void glBlendFunc(int i, int i2) {
        a();
        this.a.glBlendFunc(i, i2);
        b();
    }

    public final void glClear(int i) {
        a();
        this.a.glClear(i);
        b();
    }

    public final void glClearColor(float f2, float f3, float f4, float f5) {
        a();
        this.a.glClearColor(f2, f3, f4, f5);
        b();
    }

    public final void glClearColorx(int i, int i2, int i3, int i4) {
        a();
        this.a.glClearColorx(i, i2, i3, i4);
        b();
    }

    public final void glClearDepthf(float f2) {
        a();
        this.a.glClearDepthf(f2);
        b();
    }

    public final void glClearDepthx(int i) {
        a();
        this.a.glClearDepthx(i);
        b();
    }

    public final void glClearStencil(int i) {
        a();
        this.a.glClearStencil(i);
        b();
    }

    public final void glClientActiveTexture(int i) {
        a();
        this.a.glClientActiveTexture(i);
        b();
    }

    public final void glColor4f(float f2, float f3, float f4, float f5) {
        a();
        this.a.glColor4f(f2, f3, f4, f5);
        b();
    }

    public final void glColor4x(int i, int i2, int i3, int i4) {
        a();
        this.a.glColor4x(i, i2, i3, i4);
        b();
    }

    public final void glColorMask(boolean z, boolean z2, boolean z3, boolean z4) {
        a();
        this.a.glColorMask(z, z2, z3, z4);
        b();
    }

    public final void glColorPointer(int i, int i2, int i3, Buffer buffer) {
        a();
        this.a.glColorPointer(i, i2, i3, buffer);
        b();
    }

    public final void glCompressedTexImage2D(int i, int i2, int i3, int i4, int i5, int i6, int i7, Buffer buffer) {
        a();
        this.a.glCompressedTexImage2D(i, i2, i3, i4, i5, i6, i7, buffer);
        b();
    }

    public final void glCompressedTexSubImage2D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, Buffer buffer) {
        a();
        this.a.glCompressedTexSubImage2D(i, i2, i3, i4, i5, i6, i7, i8, buffer);
        b();
    }

    public final void glCopyTexImage2D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        a();
        this.a.glCopyTexImage2D(i, i2, i3, i4, i5, i6, i7, i8);
        b();
    }

    public final void glCopyTexSubImage2D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        a();
        this.a.glCopyTexSubImage2D(i, i2, i3, i4, i5, i6, i7, i8);
        b();
    }

    public final void glCullFace(int i) {
        a();
        this.a.glCullFace(i);
        b();
    }

    public final void glDeleteTextures(int i, int[] iArr, int i2) {
        a();
        this.a.glDeleteTextures(i, iArr, i2);
        b();
    }

    public final void glDeleteTextures(int i, IntBuffer intBuffer) {
        a();
        this.a.glDeleteTextures(i, intBuffer);
        b();
    }

    public final void glDepthFunc(int i) {
        a();
        this.a.glDepthFunc(i);
        b();
    }

    public final void glDepthMask(boolean z) {
        a();
        this.a.glDepthMask(z);
        b();
    }

    public final void glDepthRangef(float f2, float f3) {
        a();
        this.a.glDepthRangef(f2, f3);
        b();
    }

    public final void glDepthRangex(int i, int i2) {
        a();
        this.a.glDepthRangex(i, i2);
        b();
    }

    public final void glDisable(int i) {
        a();
        this.a.glDisable(i);
        b();
    }

    public final void glDisableClientState(int i) {
        a();
        this.a.glDisableClientState(i);
        b();
    }

    public final void glDrawArrays(int i, int i2, int i3) {
        a();
        this.a.glDrawArrays(i, i2, i3);
        b();
    }

    public final void glDrawElements(int i, int i2, int i3, Buffer buffer) {
        a();
        this.a.glDrawElements(i, i2, i3, buffer);
        b();
    }

    public final void glEnable(int i) {
        a();
        this.a.glEnable(i);
        b();
    }

    public final void glEnableClientState(int i) {
        a();
        this.a.glEnableClientState(i);
        b();
    }

    public final void glFinish() {
        a();
        this.a.glFinish();
        b();
    }

    public final void glFlush() {
        a();
        this.a.glFlush();
        b();
    }

    public final void glFogf(int i, float f2) {
        a();
        this.a.glFogf(i, f2);
        b();
    }

    public final void glFogfv(int i, float[] fArr, int i2) {
        a();
        this.a.glFogfv(i, fArr, i2);
        b();
    }

    public final void glFogfv(int i, FloatBuffer floatBuffer) {
        a();
        this.a.glFogfv(i, floatBuffer);
        b();
    }

    public final void glFogx(int i, int i2) {
        a();
        this.a.glFogx(i, i2);
        b();
    }

    public final void glFogxv(int i, int[] iArr, int i2) {
        a();
        this.a.glFogxv(i, iArr, i2);
        b();
    }

    public final void glFogxv(int i, IntBuffer intBuffer) {
        a();
        this.a.glFogxv(i, intBuffer);
        b();
    }

    public final void glFrontFace(int i) {
        a();
        this.a.glFrontFace(i);
        b();
    }

    public final void glFrustumf(float f2, float f3, float f4, float f5, float f6, float f7) {
        a();
        this.a.glFrustumf(f2, f3, f4, f5, f6, f7);
        b();
    }

    public final void glFrustumx(int i, int i2, int i3, int i4, int i5, int i6) {
        a();
        this.a.glFrustumx(i, i2, i3, i4, i5, i6);
        b();
    }

    public final void glGenTextures(int i, int[] iArr, int i2) {
        a();
        this.a.glGenTextures(i, iArr, i2);
        b();
    }

    public final void glGenTextures(int i, IntBuffer intBuffer) {
        a();
        this.a.glGenTextures(i, intBuffer);
        b();
    }

    public final int glGetError() {
        a();
        return this.a.glGetError();
    }

    public final void glGetIntegerv(int i, int[] iArr, int i2) {
        a();
        this.a.glGetIntegerv(i, iArr, i2);
        b();
    }

    public final void glGetIntegerv(int i, IntBuffer intBuffer) {
        a();
        this.a.glGetIntegerv(i, intBuffer);
        b();
    }

    public final String glGetString(int i) {
        a();
        String glGetString = this.a.glGetString(i);
        b();
        return glGetString;
    }

    public final void glHint(int i, int i2) {
        a();
        this.a.glHint(i, i2);
        b();
    }

    public final void glLightModelf(int i, float f2) {
        a();
        this.a.glLightModelf(i, f2);
        b();
    }

    public final void glLightModelfv(int i, float[] fArr, int i2) {
        a();
        this.a.glLightModelfv(i, fArr, i2);
        b();
    }

    public final void glLightModelfv(int i, FloatBuffer floatBuffer) {
        a();
        this.a.glLightModelfv(i, floatBuffer);
        b();
    }

    public final void glLightModelx(int i, int i2) {
        a();
        this.a.glLightModelx(i, i2);
        b();
    }

    public final void glLightModelxv(int i, int[] iArr, int i2) {
        a();
        this.a.glLightModelxv(i, iArr, i2);
        b();
    }

    public final void glLightModelxv(int i, IntBuffer intBuffer) {
        a();
        this.a.glLightModelxv(i, intBuffer);
        b();
    }

    public final void glLightf(int i, int i2, float f2) {
        a();
        this.a.glLightf(i, i2, f2);
        b();
    }

    public final void glLightfv(int i, int i2, float[] fArr, int i3) {
        a();
        this.a.glLightfv(i, i2, fArr, i3);
        b();
    }

    public final void glLightfv(int i, int i2, FloatBuffer floatBuffer) {
        a();
        this.a.glLightfv(i, i2, floatBuffer);
        b();
    }

    public final void glLightx(int i, int i2, int i3) {
        a();
        this.a.glLightx(i, i2, i3);
        b();
    }

    public final void glLightxv(int i, int i2, int[] iArr, int i3) {
        a();
        this.a.glLightxv(i, i2, iArr, i3);
        b();
    }

    public final void glLightxv(int i, int i2, IntBuffer intBuffer) {
        a();
        this.a.glLightxv(i, i2, intBuffer);
        b();
    }

    public final void glLineWidth(float f2) {
        a();
        this.a.glLineWidth(f2);
        b();
    }

    public final void glLineWidthx(int i) {
        a();
        this.a.glLineWidthx(i);
        b();
    }

    public final void glLoadIdentity() {
        a();
        this.a.glLoadIdentity();
        b();
    }

    public final void glLoadMatrixf(float[] fArr, int i) {
        a();
        this.a.glLoadMatrixf(fArr, i);
        b();
    }

    public final void glLoadMatrixf(FloatBuffer floatBuffer) {
        a();
        this.a.glLoadMatrixf(floatBuffer);
        b();
    }

    public final void glLoadMatrixx(int[] iArr, int i) {
        a();
        this.a.glLoadMatrixx(iArr, i);
        b();
    }

    public final void glLoadMatrixx(IntBuffer intBuffer) {
        a();
        this.a.glLoadMatrixx(intBuffer);
        b();
    }

    public final void glLogicOp(int i) {
        a();
        this.a.glLogicOp(i);
        b();
    }

    public final void glMaterialf(int i, int i2, float f2) {
        a();
        this.a.glMaterialf(i, i2, f2);
        b();
    }

    public final void glMaterialfv(int i, int i2, float[] fArr, int i3) {
        a();
        this.a.glMaterialfv(i, i2, fArr, i3);
        b();
    }

    public final void glMaterialfv(int i, int i2, FloatBuffer floatBuffer) {
        a();
        this.a.glMaterialfv(i, i2, floatBuffer);
        b();
    }

    public final void glMaterialx(int i, int i2, int i3) {
        a();
        this.a.glMaterialx(i, i2, i3);
        b();
    }

    public final void glMaterialxv(int i, int i2, int[] iArr, int i3) {
        a();
        this.a.glMaterialxv(i, i2, iArr, i3);
        b();
    }

    public final void glMaterialxv(int i, int i2, IntBuffer intBuffer) {
        a();
        this.a.glMaterialxv(i, i2, intBuffer);
        b();
    }

    public final void glMatrixMode(int i) {
        a();
        this.a.glMatrixMode(i);
        b();
    }

    public final void glMultMatrixf(float[] fArr, int i) {
        a();
        this.a.glMultMatrixf(fArr, i);
        b();
    }

    public final void glMultMatrixf(FloatBuffer floatBuffer) {
        a();
        this.a.glMultMatrixf(floatBuffer);
        b();
    }

    public final void glMultMatrixx(int[] iArr, int i) {
        a();
        this.a.glMultMatrixx(iArr, i);
        b();
    }

    public final void glMultMatrixx(IntBuffer intBuffer) {
        a();
        this.a.glMultMatrixx(intBuffer);
        b();
    }

    public final void glMultiTexCoord4f(int i, float f2, float f3, float f4, float f5) {
        a();
        this.a.glMultiTexCoord4f(i, f2, f3, f4, f5);
        b();
    }

    public final void glMultiTexCoord4x(int i, int i2, int i3, int i4, int i5) {
        a();
        this.a.glMultiTexCoord4x(i, i2, i3, i4, i5);
        b();
    }

    public final void glNormal3f(float f2, float f3, float f4) {
        a();
        this.a.glNormal3f(f2, f3, f4);
        b();
    }

    public final void glNormal3x(int i, int i2, int i3) {
        a();
        this.a.glNormal3x(i, i2, i3);
        b();
    }

    public final void glNormalPointer(int i, int i2, Buffer buffer) {
        a();
        this.a.glNormalPointer(i, i2, buffer);
        b();
    }

    public final void glOrthof(float f2, float f3, float f4, float f5, float f6, float f7) {
        a();
        this.a.glOrthof(f2, f3, f4, f5, f6, f7);
        b();
    }

    public final void glOrthox(int i, int i2, int i3, int i4, int i5, int i6) {
        a();
        this.a.glOrthox(i, i2, i3, i4, i5, i6);
        b();
    }

    public final void glPixelStorei(int i, int i2) {
        a();
        this.a.glPixelStorei(i, i2);
        b();
    }

    public final void glPointSize(float f2) {
        a();
        this.a.glPointSize(f2);
        b();
    }

    public final void glPointSizex(int i) {
        a();
        this.a.glPointSizex(i);
        b();
    }

    public final void glPolygonOffset(float f2, float f3) {
        a();
        this.a.glPolygonOffset(f2, f3);
        b();
    }

    public final void glPolygonOffsetx(int i, int i2) {
        a();
        this.a.glPolygonOffsetx(i, i2);
        b();
    }

    public final void glPopMatrix() {
        a();
        this.a.glPopMatrix();
        b();
    }

    public final void glPushMatrix() {
        a();
        this.a.glPushMatrix();
        b();
    }

    public final void glReadPixels(int i, int i2, int i3, int i4, int i5, int i6, Buffer buffer) {
        a();
        this.a.glReadPixels(i, i2, i3, i4, i5, i6, buffer);
        b();
    }

    public final void glRotatef(float f2, float f3, float f4, float f5) {
        a();
        this.a.glRotatef(f2, f3, f4, f5);
        b();
    }

    public final void glRotatex(int i, int i2, int i3, int i4) {
        a();
        this.a.glRotatex(i, i2, i3, i4);
        b();
    }

    public final void glSampleCoverage(float f2, boolean z) {
        a();
        this.a.glSampleCoverage(f2, z);
        b();
    }

    public final void glSampleCoveragex(int i, boolean z) {
        a();
        this.a.glSampleCoveragex(i, z);
        b();
    }

    public final void glScalef(float f2, float f3, float f4) {
        a();
        this.a.glScalef(f2, f3, f4);
        b();
    }

    public final void glScalex(int i, int i2, int i3) {
        a();
        this.a.glScalex(i, i2, i3);
        b();
    }

    public final void glScissor(int i, int i2, int i3, int i4) {
        a();
        this.a.glScissor(i, i2, i3, i4);
        b();
    }

    public final void glShadeModel(int i) {
        a();
        this.a.glShadeModel(i);
        b();
    }

    public final void glStencilFunc(int i, int i2, int i3) {
        a();
        this.a.glStencilFunc(i, i2, i3);
        b();
    }

    public final void glStencilMask(int i) {
        a();
        this.a.glStencilMask(i);
        b();
    }

    public final void glStencilOp(int i, int i2, int i3) {
        a();
        this.a.glStencilOp(i, i2, i3);
        b();
    }

    public final void glTexCoordPointer(int i, int i2, int i3, Buffer buffer) {
        a();
        this.a.glTexCoordPointer(i, i2, i3, buffer);
        b();
    }

    public final void glTexEnvf(int i, int i2, float f2) {
        a();
        this.a.glTexEnvf(i, i2, f2);
        b();
    }

    public final void glTexEnvfv(int i, int i2, float[] fArr, int i3) {
        a();
        this.a.glTexEnvfv(i, i2, fArr, i3);
        b();
    }

    public final void glTexEnvfv(int i, int i2, FloatBuffer floatBuffer) {
        a();
        this.a.glTexEnvfv(i, i2, floatBuffer);
        b();
    }

    public final void glTexEnvx(int i, int i2, int i3) {
        a();
        this.a.glTexEnvx(i, i2, i3);
        b();
    }

    public final void glTexEnvxv(int i, int i2, int[] iArr, int i3) {
        a();
        this.a.glTexEnvxv(i, i2, iArr, i3);
        b();
    }

    public final void glTexEnvxv(int i, int i2, IntBuffer intBuffer) {
        a();
        this.a.glTexEnvxv(i, i2, intBuffer);
        b();
    }

    public final void glTexImage2D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, Buffer buffer) {
        a();
        this.a.glTexImage2D(i, i2, i3, i4, i5, i6, i7, i8, buffer);
        b();
    }

    public final void glTexParameterf(int i, int i2, float f2) {
        a();
        this.a.glTexParameterf(i, i2, f2);
        b();
    }

    public final void glTexParameterx(int i, int i2, int i3) {
        a();
        this.a.glTexParameterx(i, i2, i3);
        b();
    }

    public final void glTexParameteriv(int i, int i2, int[] iArr, int i3) {
        a();
        this.c.glTexParameteriv(i, i2, iArr, i3);
        b();
    }

    public final void glTexParameteriv(int i, int i2, IntBuffer intBuffer) {
        a();
        this.c.glTexParameteriv(i, i2, intBuffer);
        b();
    }

    public final void glTexSubImage2D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, Buffer buffer) {
        a();
        this.a.glTexSubImage2D(i, i2, i3, i4, i5, i6, i7, i8, buffer);
        b();
    }

    public final void glTranslatef(float f2, float f3, float f4) {
        a();
        this.a.glTranslatef(f2, f3, f4);
        b();
    }

    public final void glTranslatex(int i, int i2, int i3) {
        a();
        this.a.glTranslatex(i, i2, i3);
        b();
    }

    public final void glVertexPointer(int i, int i2, int i3, Buffer buffer) {
        a();
        this.a.glVertexPointer(i, i2, i3, buffer);
        b();
    }

    public final void glViewport(int i, int i2, int i3, int i4) {
        a();
        this.a.glViewport(i, i2, i3, i4);
        b();
    }

    public final void glClipPlanef(int i, float[] fArr, int i2) {
        a();
        this.c.glClipPlanef(i, fArr, i2);
        b();
    }

    public final void glClipPlanef(int i, FloatBuffer floatBuffer) {
        a();
        this.c.glClipPlanef(i, floatBuffer);
        b();
    }

    public final void glClipPlanex(int i, int[] iArr, int i2) {
        a();
        this.c.glClipPlanex(i, iArr, i2);
        b();
    }

    public final void glClipPlanex(int i, IntBuffer intBuffer) {
        a();
        this.c.glClipPlanex(i, intBuffer);
        b();
    }

    public final void glDrawTexfOES(float f2, float f3, float f4, float f5, float f6) {
        a();
        this.d.glDrawTexfOES(f2, f3, f4, f5, f6);
        b();
    }

    public final void glDrawTexfvOES(float[] fArr, int i) {
        a();
        this.d.glDrawTexfvOES(fArr, i);
        b();
    }

    public final void glDrawTexfvOES(FloatBuffer floatBuffer) {
        a();
        this.d.glDrawTexfvOES(floatBuffer);
        b();
    }

    public final void glDrawTexiOES(int i, int i2, int i3, int i4, int i5) {
        a();
        this.d.glDrawTexiOES(i, i2, i3, i4, i5);
        b();
    }

    public final void glDrawTexivOES(int[] iArr, int i) {
        a();
        this.d.glDrawTexivOES(iArr, i);
        b();
    }

    public final void glDrawTexivOES(IntBuffer intBuffer) {
        a();
        this.d.glDrawTexivOES(intBuffer);
        b();
    }

    public final void glDrawTexsOES(short s, short s2, short s3, short s4, short s5) {
        a();
        this.d.glDrawTexsOES(s, s2, s3, s4, s5);
        b();
    }

    public final void glDrawTexsvOES(short[] sArr, int i) {
        a();
        this.d.glDrawTexsvOES(sArr, i);
        b();
    }

    public final void glDrawTexsvOES(ShortBuffer shortBuffer) {
        a();
        this.d.glDrawTexsvOES(shortBuffer);
        b();
    }

    public final void glDrawTexxOES(int i, int i2, int i3, int i4, int i5) {
        a();
        this.d.glDrawTexxOES(i, i2, i3, i4, i5);
        b();
    }

    public final void glDrawTexxvOES(int[] iArr, int i) {
        a();
        this.d.glDrawTexxvOES(iArr, i);
        b();
    }

    public final void glDrawTexxvOES(IntBuffer intBuffer) {
        a();
        this.d.glDrawTexxvOES(intBuffer);
        b();
    }

    public final int glQueryMatrixxOES(int[] iArr, int i, int[] iArr2, int i2) {
        a();
        int glQueryMatrixxOES = this.b.glQueryMatrixxOES(iArr, i, iArr2, i2);
        b();
        return glQueryMatrixxOES;
    }

    public final int glQueryMatrixxOES(IntBuffer intBuffer, IntBuffer intBuffer2) {
        a();
        int glQueryMatrixxOES = this.b.glQueryMatrixxOES(intBuffer, intBuffer2);
        b();
        return glQueryMatrixxOES;
    }
}
