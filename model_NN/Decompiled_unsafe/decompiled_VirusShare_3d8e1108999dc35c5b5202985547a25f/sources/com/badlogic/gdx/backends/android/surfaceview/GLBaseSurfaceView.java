package com.badlogic.gdx.backends.android.surfaceview;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import java.io.Writer;
import java.util.ArrayList;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;

public class GLBaseSurfaceView extends GLSurfaceView implements SurfaceHolder.Callback {
    static final j a = new j();
    boolean b = true;
    GLSurfaceView.EGLConfigChooser c;
    f d;
    d e;
    k f;
    int g;
    private b h;

    public interface d {
        EGLSurface a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig, Object obj);

        void a(EGL10 egl10, EGLDisplay eGLDisplay, EGLSurface eGLSurface);
    }

    public interface f {
        EGLContext a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig);

        void a(EGL10 egl10, EGLDisplay eGLDisplay, EGLContext eGLContext);
    }

    public interface k {
        GL a();
    }

    public GLBaseSurfaceView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        getHolder().addCallback(this);
    }

    public void setDebugFlags(int i2) {
        this.g = i2;
    }

    public int getDebugFlags() {
        return this.g;
    }

    public void setRenderer(GLSurfaceView.Renderer renderer) {
        a();
        if (this.c == null) {
            this.c = new e(true);
        }
        if (this.d == null) {
            this.d = new a();
        }
        if (this.e == null) {
            this.e = new l();
        }
        this.h = new b(renderer);
        this.h.start();
    }

    public void setEGLConfigChooser(GLSurfaceView.EGLConfigChooser eGLConfigChooser) {
        a();
        this.c = eGLConfigChooser;
    }

    public void setEGLConfigChooser(boolean z) {
        setEGLConfigChooser(new e(z));
    }

    public void setEGLConfigChooser(int i2, int i3, int i4, int i5, int i6, int i7) {
        setEGLConfigChooser(new h(i2, i3, i4, i5, i6, i7));
    }

    public void setRenderMode(int i2) {
        this.h.a(i2);
    }

    public int getRenderMode() {
        return this.h.a();
    }

    public void requestRender() {
        this.h.b();
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        this.h.c();
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        this.h.d();
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        this.h.a(i3, i4);
    }

    public void onPause() {
        this.h.e();
    }

    public void onResume() {
        this.h.f();
    }

    public void queueEvent(Runnable runnable) {
        this.h.a(runnable);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.h.g();
    }

    static class a implements f {
        a() {
        }

        public final EGLContext a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig) {
            return egl10.eglCreateContext(eGLDisplay, eGLConfig, EGL10.EGL_NO_CONTEXT, null);
        }

        public final void a(EGL10 egl10, EGLDisplay eGLDisplay, EGLContext eGLContext) {
            egl10.eglDestroyContext(eGLDisplay, eGLContext);
        }
    }

    static class l implements d {
        l() {
        }

        public final EGLSurface a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig, Object obj) {
            return egl10.eglCreateWindowSurface(eGLDisplay, eGLConfig, obj, null);
        }

        public final void a(EGL10 egl10, EGLDisplay eGLDisplay, EGLSurface eGLSurface) {
            egl10.eglDestroySurface(eGLDisplay, eGLSurface);
        }
    }

    private static abstract class i implements GLSurfaceView.EGLConfigChooser {
        private int[] a;

        /* access modifiers changed from: package-private */
        public abstract EGLConfig a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig[] eGLConfigArr);

        public i(int[] iArr) {
            this.a = iArr;
        }

        public EGLConfig chooseConfig(EGL10 egl10, EGLDisplay eGLDisplay) {
            int[] iArr = new int[1];
            egl10.eglChooseConfig(eGLDisplay, this.a, null, 0, iArr);
            int i = iArr[0];
            if (i <= 0) {
                throw new IllegalArgumentException("No configs match configSpec");
            }
            EGLConfig[] eGLConfigArr = new EGLConfig[i];
            egl10.eglChooseConfig(eGLDisplay, this.a, eGLConfigArr, i, iArr);
            EGLConfig a2 = a(egl10, eGLDisplay, eGLConfigArr);
            if (a2 != null) {
                return a2;
            }
            throw new IllegalArgumentException("No config chosen");
        }
    }

    private static class h extends i {
        protected int a;
        protected int b;
        protected int c;
        private int[] d = new int[1];
        private int e;
        private int f;
        private int g;

        public h(int i, int i2, int i3, int i4, int i5, int i6) {
            super(new int[]{12324, i, 12323, i2, 12322, i3, 12321, i4, 12325, i5, 12326, i6, 12344});
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.e = i4;
            this.f = i5;
            this.g = i6;
        }

        public final EGLConfig a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig[] eGLConfigArr) {
            int i;
            EGLConfig eGLConfig;
            EGLConfig eGLConfig2 = null;
            int i2 = 1000;
            int length = eGLConfigArr.length;
            int i3 = 0;
            while (i3 < length) {
                EGLConfig eGLConfig3 = eGLConfigArr[i3];
                int a2 = a(egl10, eGLDisplay, eGLConfig3, 12325);
                int a3 = a(egl10, eGLDisplay, eGLConfig3, 12326);
                if (a2 >= this.f && a3 >= this.g) {
                    i = Math.abs(a(egl10, eGLDisplay, eGLConfig3, 12324) - this.a) + Math.abs(a(egl10, eGLDisplay, eGLConfig3, 12323) - this.b) + Math.abs(a(egl10, eGLDisplay, eGLConfig3, 12322) - this.c) + Math.abs(a(egl10, eGLDisplay, eGLConfig3, 12321) - this.e);
                    if (i < i2) {
                        eGLConfig = eGLConfig3;
                        i3++;
                        eGLConfig2 = eGLConfig;
                        i2 = i;
                    }
                }
                i = i2;
                eGLConfig = eGLConfig2;
                i3++;
                eGLConfig2 = eGLConfig;
                i2 = i;
            }
            return eGLConfig2;
        }

        private int a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig, int i) {
            if (egl10.eglGetConfigAttrib(eGLDisplay, eGLConfig, i, this.d)) {
                return this.d[0];
            }
            return 0;
        }
    }

    private static class e extends h {
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(boolean z) {
            super(4, 4, 4, 0, z ? 16 : 0, 0);
            this.a = 5;
            this.b = 6;
            this.c = 5;
        }
    }

    private class g {
        EGL10 a;
        EGLDisplay b;
        EGLSurface c;
        EGLConfig d;
        EGLContext e;

        public g() {
        }

        public final GL a(SurfaceHolder surfaceHolder) {
            int i;
            c cVar;
            boolean z = true;
            if (!(this.c == null || this.c == EGL10.EGL_NO_SURFACE)) {
                this.a.eglMakeCurrent(this.b, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                GLBaseSurfaceView.this.e.a(this.a, this.b, this.c);
            }
            this.c = GLBaseSurfaceView.this.e.a(this.a, this.b, this.d, surfaceHolder);
            if (this.c == null || this.c == EGL10.EGL_NO_SURFACE) {
                a("createWindowSurface");
            }
            if (!this.a.eglMakeCurrent(this.b, this.c, this.c, this.e)) {
                a("eglMakeCurrent");
            }
            GL gl = this.e.getGL();
            if (GLBaseSurfaceView.this.f != null) {
                gl = GLBaseSurfaceView.this.f.a();
            }
            if ((GLBaseSurfaceView.this.g & 3) == 0) {
                return gl;
            }
            if ((GLBaseSurfaceView.this.g & 1) != 0) {
                i = 1;
            } else {
                i = 0;
            }
            if ((GLBaseSurfaceView.this.g & 2) != 0) {
                cVar = new c();
            } else {
                cVar = null;
            }
            GL bVar = i != 0 ? new b(gl, i) : gl;
            if (cVar == null) {
                return bVar;
            }
            if ((i & 4) == 0) {
                z = false;
            }
            return new d(bVar, cVar, z);
        }

        private void a(String str) {
            throw new RuntimeException(str + " failed: " + this.a.eglGetError());
        }
    }

    class b extends Thread {
        boolean a;
        private boolean b;
        private boolean c;
        private boolean d;
        private boolean e;
        private boolean f;
        private int g = 0;
        private int h = 0;
        private int i = 1;
        private boolean j = true;
        private boolean k;
        private ArrayList<Runnable> l = new ArrayList<>();
        private GLSurfaceView.Renderer m;
        private g n;

        b(GLSurfaceView.Renderer renderer) {
            this.m = renderer;
        }

        public final void run() {
            setName("GLThread " + getId());
            try {
                i();
            } catch (InterruptedException e2) {
            } finally {
                GLBaseSurfaceView.a.a(this);
            }
        }

        private void h() {
            if (this.f) {
                this.f = false;
                g gVar = this.n;
                if (!(gVar.c == null || gVar.c == EGL10.EGL_NO_SURFACE)) {
                    gVar.a.eglMakeCurrent(gVar.b, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                    GLBaseSurfaceView.this.e.a(gVar.a, gVar.b, gVar.c);
                    gVar.c = null;
                }
                g gVar2 = this.n;
                if (gVar2.e != null) {
                    GLBaseSurfaceView.this.d.a(gVar2.a, gVar2.b, gVar2.e);
                    gVar2.e = null;
                }
                if (gVar2.b != null) {
                    gVar2.a.eglTerminate(gVar2.b);
                    gVar2.b = null;
                }
                GLBaseSurfaceView.a.c(this);
            }
        }

        /* JADX INFO: finally extract failed */
        /* JADX WARNING: Code restructure failed: missing block: B:104:0x0181, code lost:
            r7 = r0;
            r0 = r8;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:105:0x0184, code lost:
            r8 = r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x0039, code lost:
            if (r1 == null) goto L_0x0141;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
            r1.run();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x003e, code lost:
            r1 = null;
            r7 = r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:92:0x0141, code lost:
            if (r0 == false) goto L_0x0181;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:94:?, code lost:
            r0 = (javax.microedition.khronos.opengles.GL10) r13.n.a(r13.o.getHolder());
            r13.m.onSurfaceCreated(r0, r13.n.d);
            r7 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:95:0x015b, code lost:
            if (r6 == false) goto L_0x0163;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:96:0x015d, code lost:
            r13.m.onSurfaceChanged(r0, r3, r2);
            r6 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:97:0x0163, code lost:
            r13.m.onDrawFrame(r0);
            r8 = r13.n;
            r8.a.eglSwapBuffers(r8.b, r8.c);
            r8.a.eglGetError();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:98:0x0178, code lost:
            if (r5 == false) goto L_0x0184;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:99:0x017a, code lost:
            r4 = true;
            r8 = r0;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void i() throws java.lang.InterruptedException {
            /*
                r13 = this;
                com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView$g r0 = new com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView$g
                com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView r1 = com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView.this
                r0.<init>()
                r13.n = r0
                r0 = 0
                r7 = 0
                r6 = 0
                r5 = 0
                r4 = 0
                r3 = 0
                r2 = 0
                r1 = 0
                r8 = r0
            L_0x0012:
                com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView$j r9 = com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView.a     // Catch:{ all -> 0x00f8 }
                monitor-enter(r9)     // Catch:{ all -> 0x00f8 }
            L_0x0015:
                boolean r0 = r13.b     // Catch:{ all -> 0x00f5 }
                if (r0 == 0) goto L_0x0025
                monitor-exit(r9)     // Catch:{ all -> 0x00f5 }
                com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView$j r1 = com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView.a
                monitor-enter(r1)
                r13.h()     // Catch:{ all -> 0x0022 }
                monitor-exit(r1)     // Catch:{ all -> 0x0022 }
                return
            L_0x0022:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x0022 }
                throw r0
            L_0x0025:
                java.util.ArrayList<java.lang.Runnable> r0 = r13.l     // Catch:{ all -> 0x00f5 }
                boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x00f5 }
                if (r0 != 0) goto L_0x0041
                java.util.ArrayList<java.lang.Runnable> r0 = r13.l     // Catch:{ all -> 0x00f5 }
                r1 = 0
                java.lang.Object r0 = r0.remove(r1)     // Catch:{ all -> 0x00f5 }
                java.lang.Runnable r0 = (java.lang.Runnable) r0     // Catch:{ all -> 0x00f5 }
                r1 = r0
                r0 = r7
            L_0x0038:
                monitor-exit(r9)     // Catch:{ all -> 0x00f5 }
                if (r1 == 0) goto L_0x0141
                r1.run()     // Catch:{ all -> 0x00f8 }
                r1 = 0
                r7 = r0
                goto L_0x0012
            L_0x0041:
                boolean r0 = r13.f     // Catch:{ all -> 0x00f5 }
                if (r0 == 0) goto L_0x004c
                boolean r0 = r13.c     // Catch:{ all -> 0x00f5 }
                if (r0 == 0) goto L_0x004c
                r13.h()     // Catch:{ all -> 0x00f5 }
            L_0x004c:
                boolean r0 = r13.d     // Catch:{ all -> 0x00f5 }
                if (r0 != 0) goto L_0x0063
                boolean r0 = r13.e     // Catch:{ all -> 0x00f5 }
                if (r0 != 0) goto L_0x0063
                boolean r0 = r13.f     // Catch:{ all -> 0x00f5 }
                if (r0 == 0) goto L_0x005b
                r13.h()     // Catch:{ all -> 0x00f5 }
            L_0x005b:
                r0 = 1
                r13.e = r0     // Catch:{ all -> 0x00f5 }
                com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView$j r0 = com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView.a     // Catch:{ all -> 0x00f5 }
                r0.notifyAll()     // Catch:{ all -> 0x00f5 }
            L_0x0063:
                boolean r0 = r13.d     // Catch:{ all -> 0x00f5 }
                if (r0 == 0) goto L_0x0073
                boolean r0 = r13.e     // Catch:{ all -> 0x00f5 }
                if (r0 == 0) goto L_0x0073
                r0 = 0
                r13.e = r0     // Catch:{ all -> 0x00f5 }
                com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView$j r0 = com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView.a     // Catch:{ all -> 0x00f5 }
                r0.notifyAll()     // Catch:{ all -> 0x00f5 }
            L_0x0073:
                if (r4 == 0) goto L_0x007f
                r5 = 0
                r4 = 0
                r0 = 1
                r13.k = r0     // Catch:{ all -> 0x00f5 }
                com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView$j r0 = com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView.a     // Catch:{ all -> 0x00f5 }
                r0.notifyAll()     // Catch:{ all -> 0x00f5 }
            L_0x007f:
                boolean r0 = r13.c     // Catch:{ all -> 0x00f5 }
                if (r0 != 0) goto L_0x013a
                boolean r0 = r13.d     // Catch:{ all -> 0x00f5 }
                if (r0 == 0) goto L_0x013a
                int r0 = r13.g     // Catch:{ all -> 0x00f5 }
                if (r0 <= 0) goto L_0x013a
                int r0 = r13.h     // Catch:{ all -> 0x00f5 }
                if (r0 <= 0) goto L_0x013a
                boolean r0 = r13.j     // Catch:{ all -> 0x00f5 }
                if (r0 != 0) goto L_0x0098
                int r0 = r13.i     // Catch:{ all -> 0x00f5 }
                r10 = 1
                if (r0 != r10) goto L_0x013a
            L_0x0098:
                boolean r0 = r13.f     // Catch:{ all -> 0x00f5 }
                if (r0 != 0) goto L_0x010b
                com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView$j r0 = com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView.a     // Catch:{ all -> 0x00f5 }
                boolean r0 = r0.b(r13)     // Catch:{ all -> 0x00f5 }
                if (r0 == 0) goto L_0x010b
                r0 = 1
                r13.f = r0     // Catch:{ all -> 0x00f5 }
                com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView$g r6 = r13.n     // Catch:{ all -> 0x00f5 }
                javax.microedition.khronos.egl.EGL r0 = javax.microedition.khronos.egl.EGLContext.getEGL()     // Catch:{ all -> 0x00f5 }
                javax.microedition.khronos.egl.EGL10 r0 = (javax.microedition.khronos.egl.EGL10) r0     // Catch:{ all -> 0x00f5 }
                r6.a = r0     // Catch:{ all -> 0x00f5 }
                javax.microedition.khronos.egl.EGL10 r0 = r6.a     // Catch:{ all -> 0x00f5 }
                java.lang.Object r7 = javax.microedition.khronos.egl.EGL10.EGL_DEFAULT_DISPLAY     // Catch:{ all -> 0x00f5 }
                javax.microedition.khronos.egl.EGLDisplay r0 = r0.eglGetDisplay(r7)     // Catch:{ all -> 0x00f5 }
                r6.b = r0     // Catch:{ all -> 0x00f5 }
                r0 = 2
                int[] r0 = new int[r0]     // Catch:{ all -> 0x00f5 }
                javax.microedition.khronos.egl.EGL10 r7 = r6.a     // Catch:{ all -> 0x00f5 }
                javax.microedition.khronos.egl.EGLDisplay r10 = r6.b     // Catch:{ all -> 0x00f5 }
                r7.eglInitialize(r10, r0)     // Catch:{ all -> 0x00f5 }
                com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView r0 = com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView.this     // Catch:{ all -> 0x00f5 }
                android.opengl.GLSurfaceView$EGLConfigChooser r0 = r0.c     // Catch:{ all -> 0x00f5 }
                javax.microedition.khronos.egl.EGL10 r7 = r6.a     // Catch:{ all -> 0x00f5 }
                javax.microedition.khronos.egl.EGLDisplay r10 = r6.b     // Catch:{ all -> 0x00f5 }
                javax.microedition.khronos.egl.EGLConfig r0 = r0.chooseConfig(r7, r10)     // Catch:{ all -> 0x00f5 }
                r6.d = r0     // Catch:{ all -> 0x00f5 }
                com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView r0 = com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView.this     // Catch:{ all -> 0x00f5 }
                com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView$f r0 = r0.d     // Catch:{ all -> 0x00f5 }
                javax.microedition.khronos.egl.EGL10 r7 = r6.a     // Catch:{ all -> 0x00f5 }
                javax.microedition.khronos.egl.EGLDisplay r10 = r6.b     // Catch:{ all -> 0x00f5 }
                javax.microedition.khronos.egl.EGLConfig r11 = r6.d     // Catch:{ all -> 0x00f5 }
                javax.microedition.khronos.egl.EGLContext r0 = r0.a(r7, r10, r11)     // Catch:{ all -> 0x00f5 }
                r6.e = r0     // Catch:{ all -> 0x00f5 }
                javax.microedition.khronos.egl.EGLContext r0 = r6.e     // Catch:{ all -> 0x00f5 }
                if (r0 == 0) goto L_0x00ed
                javax.microedition.khronos.egl.EGLContext r0 = r6.e     // Catch:{ all -> 0x00f5 }
                javax.microedition.khronos.egl.EGLContext r7 = javax.microedition.khronos.egl.EGL10.EGL_NO_CONTEXT     // Catch:{ all -> 0x00f5 }
                if (r0 != r7) goto L_0x0101
            L_0x00ed:
                java.lang.RuntimeException r0 = new java.lang.RuntimeException     // Catch:{ all -> 0x00f5 }
                java.lang.String r1 = "createContext failed"
                r0.<init>(r1)     // Catch:{ all -> 0x00f5 }
                throw r0     // Catch:{ all -> 0x00f5 }
            L_0x00f5:
                r0 = move-exception
                monitor-exit(r9)     // Catch:{ all -> 0x00f5 }
                throw r0     // Catch:{ all -> 0x00f8 }
            L_0x00f8:
                r0 = move-exception
                com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView$j r1 = com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView.a
                monitor-enter(r1)
                r13.h()     // Catch:{ all -> 0x017e }
                monitor-exit(r1)     // Catch:{ all -> 0x017e }
                throw r0
            L_0x0101:
                r0 = 0
                r6.c = r0     // Catch:{ all -> 0x00f5 }
                r7 = 1
                r6 = 1
                com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView$j r0 = com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView.a     // Catch:{ all -> 0x00f5 }
                r0.notifyAll()     // Catch:{ all -> 0x00f5 }
            L_0x010b:
                r0 = r6
                r6 = r7
                boolean r7 = r13.f     // Catch:{ all -> 0x00f5 }
                if (r7 == 0) goto L_0x0138
                com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView r7 = com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView.this     // Catch:{ all -> 0x00f5 }
                boolean r7 = r7.b     // Catch:{ all -> 0x00f5 }
                if (r7 == 0) goto L_0x012f
                r5 = 1
                int r2 = r13.g     // Catch:{ all -> 0x00f5 }
                int r0 = r13.h     // Catch:{ all -> 0x00f5 }
                r3 = 1
                com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView r7 = com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView.this     // Catch:{ all -> 0x00f5 }
                r10 = 0
                r7.b = r10     // Catch:{ all -> 0x00f5 }
            L_0x0122:
                com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView$j r7 = com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView.a     // Catch:{ all -> 0x00f5 }
                r7.notifyAll()     // Catch:{ all -> 0x00f5 }
                r12 = r0
                r0 = r6
                r6 = r5
                r5 = r3
                r3 = r2
                r2 = r12
                goto L_0x0038
            L_0x012f:
                r7 = 0
                r13.j = r7     // Catch:{ all -> 0x00f5 }
                r12 = r2
                r2 = r3
                r3 = r5
                r5 = r0
                r0 = r12
                goto L_0x0122
            L_0x0138:
                r7 = r6
                r6 = r0
            L_0x013a:
                com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView$j r0 = com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView.a     // Catch:{ all -> 0x00f5 }
                r0.wait()     // Catch:{ all -> 0x00f5 }
                goto L_0x0015
            L_0x0141:
                if (r0 == 0) goto L_0x0181
                com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView$g r0 = r13.n     // Catch:{ all -> 0x00f8 }
                com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView r7 = com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView.this     // Catch:{ all -> 0x00f8 }
                android.view.SurfaceHolder r7 = r7.getHolder()     // Catch:{ all -> 0x00f8 }
                javax.microedition.khronos.opengles.GL r0 = r0.a(r7)     // Catch:{ all -> 0x00f8 }
                javax.microedition.khronos.opengles.GL10 r0 = (javax.microedition.khronos.opengles.GL10) r0     // Catch:{ all -> 0x00f8 }
                android.opengl.GLSurfaceView$Renderer r7 = r13.m     // Catch:{ all -> 0x00f8 }
                com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView$g r8 = r13.n     // Catch:{ all -> 0x00f8 }
                javax.microedition.khronos.egl.EGLConfig r8 = r8.d     // Catch:{ all -> 0x00f8 }
                r7.onSurfaceCreated(r0, r8)     // Catch:{ all -> 0x00f8 }
                r7 = 0
            L_0x015b:
                if (r6 == 0) goto L_0x0163
                android.opengl.GLSurfaceView$Renderer r6 = r13.m     // Catch:{ all -> 0x00f8 }
                r6.onSurfaceChanged(r0, r3, r2)     // Catch:{ all -> 0x00f8 }
                r6 = 0
            L_0x0163:
                android.opengl.GLSurfaceView$Renderer r8 = r13.m     // Catch:{ all -> 0x00f8 }
                r8.onDrawFrame(r0)     // Catch:{ all -> 0x00f8 }
                com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView$g r8 = r13.n     // Catch:{ all -> 0x00f8 }
                javax.microedition.khronos.egl.EGL10 r9 = r8.a     // Catch:{ all -> 0x00f8 }
                javax.microedition.khronos.egl.EGLDisplay r10 = r8.b     // Catch:{ all -> 0x00f8 }
                javax.microedition.khronos.egl.EGLSurface r11 = r8.c     // Catch:{ all -> 0x00f8 }
                r9.eglSwapBuffers(r10, r11)     // Catch:{ all -> 0x00f8 }
                javax.microedition.khronos.egl.EGL10 r8 = r8.a     // Catch:{ all -> 0x00f8 }
                r8.eglGetError()     // Catch:{ all -> 0x00f8 }
                if (r5 == 0) goto L_0x0184
                r4 = 1
                r8 = r0
                goto L_0x0012
            L_0x017e:
                r0 = move-exception
                monitor-exit(r1)     // Catch:{ all -> 0x017e }
                throw r0
            L_0x0181:
                r7 = r0
                r0 = r8
                goto L_0x015b
            L_0x0184:
                r8 = r0
                goto L_0x0012
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.surfaceview.GLBaseSurfaceView.b.i():void");
        }

        public final void a(int i2) {
            if (i2 < 0 || i2 > 1) {
                throw new IllegalArgumentException("renderMode");
            }
            synchronized (GLBaseSurfaceView.a) {
                this.i = i2;
                GLBaseSurfaceView.a.notifyAll();
            }
        }

        public final int a() {
            int i2;
            synchronized (GLBaseSurfaceView.a) {
                i2 = this.i;
            }
            return i2;
        }

        public final void b() {
            synchronized (GLBaseSurfaceView.a) {
                this.j = true;
                GLBaseSurfaceView.a.notifyAll();
            }
        }

        public final void c() {
            synchronized (GLBaseSurfaceView.a) {
                this.d = true;
                GLBaseSurfaceView.a.notifyAll();
            }
        }

        public final void d() {
            synchronized (GLBaseSurfaceView.a) {
                this.d = false;
                GLBaseSurfaceView.a.notifyAll();
                while (!this.e && !this.a) {
                    try {
                        GLBaseSurfaceView.a.wait();
                    } catch (InterruptedException e2) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }

        public final void e() {
            synchronized (GLBaseSurfaceView.a) {
                this.c = true;
                GLBaseSurfaceView.a.notifyAll();
            }
        }

        public final void f() {
            synchronized (GLBaseSurfaceView.a) {
                this.c = false;
                this.j = true;
                GLBaseSurfaceView.a.notifyAll();
            }
        }

        public final void a(int i2, int i3) {
            synchronized (GLBaseSurfaceView.a) {
                this.g = i2;
                this.h = i3;
                GLBaseSurfaceView.this.b = true;
                this.j = true;
                this.k = false;
                GLBaseSurfaceView.a.notifyAll();
                while (!this.a && !this.c && !this.k) {
                    try {
                        GLBaseSurfaceView.a.wait();
                    } catch (InterruptedException e2) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }

        public final void g() {
            synchronized (GLBaseSurfaceView.a) {
                this.b = true;
                GLBaseSurfaceView.a.notifyAll();
                while (!this.a) {
                    try {
                        GLBaseSurfaceView.a.wait();
                    } catch (InterruptedException e2) {
                        Thread.currentThread().interrupt();
                    }
                }
            }
        }

        public final void a(Runnable runnable) {
            if (runnable == null) {
                throw new IllegalArgumentException("r must not be null");
            }
            synchronized (GLBaseSurfaceView.a) {
                this.l.add(runnable);
                GLBaseSurfaceView.a.notifyAll();
            }
        }
    }

    static class c extends Writer {
        private StringBuilder a = new StringBuilder();

        c() {
        }

        public final void close() {
            a();
        }

        public final void flush() {
            a();
        }

        public final void write(char[] cArr, int i, int i2) {
            for (int i3 = 0; i3 < i2; i3++) {
                char c = cArr[i + i3];
                if (c == 10) {
                    a();
                } else {
                    this.a.append(c);
                }
            }
        }

        private void a() {
            if (this.a.length() > 0) {
                Log.v("GLSurfaceView", this.a.toString());
                this.a.delete(0, this.a.length());
            }
        }
    }

    private void a() {
        if (this.h != null) {
            throw new IllegalStateException("setRenderer has already been called for this instance.");
        }
    }

    static class j {
        private b a;

        j() {
        }

        public final synchronized void a(b bVar) {
            bVar.a = true;
            if (this.a == bVar) {
                this.a = null;
            }
            notifyAll();
        }

        public final boolean b(b bVar) {
            if (this.a != bVar && this.a != null) {
                return false;
            }
            this.a = bVar;
            notifyAll();
            return true;
        }

        public final void c(b bVar) {
            if (this.a == bVar) {
                this.a = null;
            }
            notifyAll();
        }
    }
}
