package com.badlogic.gdx.backends.android.surfaceview;

import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import javax.microedition.khronos.opengles.GL;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL10Ext;
import javax.microedition.khronos.opengles.GL11;
import javax.microedition.khronos.opengles.GL11Ext;
import javax.microedition.khronos.opengles.GL11ExtensionPack;

abstract class h implements GL, GL10, GL10Ext, GL11, GL11Ext {
    protected GL10 a;
    protected GL10Ext b;
    protected GL11 c;
    protected GL11Ext d;
    private GL11ExtensionPack e;

    public h(GL gl) {
        this.a = (GL10) gl;
        if (gl instanceof GL10Ext) {
            this.b = (GL10Ext) gl;
        }
        if (gl instanceof GL11) {
            this.c = (GL11) gl;
        }
        if (gl instanceof GL11Ext) {
            this.d = (GL11Ext) gl;
        }
        if (gl instanceof GL11ExtensionPack) {
            this.e = (GL11ExtensionPack) gl;
        }
    }

    public void glGetPointerv(int i, Buffer[] bufferArr) {
        throw new UnsupportedOperationException();
    }

    public void glColorPointer(int i, int i2, int i3, int i4) {
        throw new UnsupportedOperationException();
    }

    public void glNormalPointer(int i, int i2, int i3) {
        throw new UnsupportedOperationException();
    }

    public void glTexCoordPointer(int i, int i2, int i3, int i4) {
        throw new UnsupportedOperationException();
    }

    public void glVertexPointer(int i, int i2, int i3, int i4) {
        throw new UnsupportedOperationException();
    }

    public void glDrawElements(int i, int i2, int i3, int i4) {
        throw new UnsupportedOperationException();
    }

    public void glBindBuffer(int i, int i2) {
        throw new UnsupportedOperationException();
    }

    public void glBufferData(int i, int i2, Buffer buffer, int i3) {
        throw new UnsupportedOperationException();
    }

    public void glBufferSubData(int i, int i2, int i3, Buffer buffer) {
        throw new UnsupportedOperationException();
    }

    public void glColor4ub(byte b2, byte b3, byte b4, byte b5) {
        throw new UnsupportedOperationException();
    }

    public void glDeleteBuffers(int i, int[] iArr, int i2) {
        throw new UnsupportedOperationException();
    }

    public void glDeleteBuffers(int i, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public void glGenBuffers(int i, int[] iArr, int i2) {
        throw new UnsupportedOperationException();
    }

    public void glGenBuffers(int i, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public void glGetBooleanv(int i, boolean[] zArr, int i2) {
        throw new UnsupportedOperationException();
    }

    public void glGetBooleanv(int i, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public void glGetBufferParameteriv(int i, int i2, int[] iArr, int i3) {
        throw new UnsupportedOperationException();
    }

    public void glGetBufferParameteriv(int i, int i2, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public void glGetClipPlanef(int i, float[] fArr, int i2) {
        throw new UnsupportedOperationException();
    }

    public void glGetClipPlanef(int i, FloatBuffer floatBuffer) {
        throw new UnsupportedOperationException();
    }

    public void glGetClipPlanex(int i, int[] iArr, int i2) {
        throw new UnsupportedOperationException();
    }

    public void glGetClipPlanex(int i, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public void glGetFixedv(int i, int[] iArr, int i2) {
        throw new UnsupportedOperationException();
    }

    public void glGetFixedv(int i, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public void glGetFloatv(int i, float[] fArr, int i2) {
        throw new UnsupportedOperationException();
    }

    public void glGetFloatv(int i, FloatBuffer floatBuffer) {
        throw new UnsupportedOperationException();
    }

    public void glGetLightfv(int i, int i2, float[] fArr, int i3) {
        throw new UnsupportedOperationException();
    }

    public void glGetLightfv(int i, int i2, FloatBuffer floatBuffer) {
        throw new UnsupportedOperationException();
    }

    public void glGetLightxv(int i, int i2, int[] iArr, int i3) {
        throw new UnsupportedOperationException();
    }

    public void glGetLightxv(int i, int i2, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public void glGetMaterialfv(int i, int i2, float[] fArr, int i3) {
        throw new UnsupportedOperationException();
    }

    public void glGetMaterialfv(int i, int i2, FloatBuffer floatBuffer) {
        throw new UnsupportedOperationException();
    }

    public void glGetMaterialxv(int i, int i2, int[] iArr, int i3) {
        throw new UnsupportedOperationException();
    }

    public void glGetMaterialxv(int i, int i2, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public void glGetTexEnviv(int i, int i2, int[] iArr, int i3) {
        throw new UnsupportedOperationException();
    }

    public void glGetTexEnviv(int i, int i2, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public void glGetTexEnvxv(int i, int i2, int[] iArr, int i3) {
        throw new UnsupportedOperationException();
    }

    public void glGetTexEnvxv(int i, int i2, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public void glGetTexParameterfv(int i, int i2, float[] fArr, int i3) {
        throw new UnsupportedOperationException();
    }

    public void glGetTexParameterfv(int i, int i2, FloatBuffer floatBuffer) {
        throw new UnsupportedOperationException();
    }

    public void glGetTexParameteriv(int i, int i2, int[] iArr, int i3) {
        throw new UnsupportedOperationException();
    }

    public void glGetTexParameteriv(int i, int i2, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public void glGetTexParameterxv(int i, int i2, int[] iArr, int i3) {
        throw new UnsupportedOperationException();
    }

    public void glGetTexParameterxv(int i, int i2, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public boolean glIsBuffer(int i) {
        throw new UnsupportedOperationException();
    }

    public boolean glIsEnabled(int i) {
        throw new UnsupportedOperationException();
    }

    public boolean glIsTexture(int i) {
        throw new UnsupportedOperationException();
    }

    public void glPointParameterf(int i, float f) {
        throw new UnsupportedOperationException();
    }

    public void glPointParameterfv(int i, float[] fArr, int i2) {
        throw new UnsupportedOperationException();
    }

    public void glPointParameterfv(int i, FloatBuffer floatBuffer) {
        throw new UnsupportedOperationException();
    }

    public void glPointParameterx(int i, int i2) {
        throw new UnsupportedOperationException();
    }

    public void glPointParameterxv(int i, int[] iArr, int i2) {
        throw new UnsupportedOperationException();
    }

    public void glPointParameterxv(int i, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public void glPointSizePointerOES(int i, int i2, Buffer buffer) {
        throw new UnsupportedOperationException();
    }

    public void glTexEnvi(int i, int i2, int i3) {
        throw new UnsupportedOperationException();
    }

    public void glTexEnviv(int i, int i2, int[] iArr, int i3) {
        throw new UnsupportedOperationException();
    }

    public void glTexEnviv(int i, int i2, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public void glTexParameterfv(int i, int i2, float[] fArr, int i3) {
        throw new UnsupportedOperationException();
    }

    public void glTexParameterfv(int i, int i2, FloatBuffer floatBuffer) {
        throw new UnsupportedOperationException();
    }

    public void glTexParameteri(int i, int i2, int i3) {
        throw new UnsupportedOperationException();
    }

    public void glTexParameterxv(int i, int i2, int[] iArr, int i3) {
        throw new UnsupportedOperationException();
    }

    public void glTexParameterxv(int i, int i2, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public void glCurrentPaletteMatrixOES(int i) {
        throw new UnsupportedOperationException();
    }

    public void glLoadPaletteFromModelViewMatrixOES() {
        throw new UnsupportedOperationException();
    }

    public void glMatrixIndexPointerOES(int i, int i2, int i3, Buffer buffer) {
        throw new UnsupportedOperationException();
    }

    public void glMatrixIndexPointerOES(int i, int i2, int i3, int i4) {
        throw new UnsupportedOperationException();
    }

    public void glWeightPointerOES(int i, int i2, int i3, Buffer buffer) {
        throw new UnsupportedOperationException();
    }

    public void glWeightPointerOES(int i, int i2, int i3, int i4) {
        throw new UnsupportedOperationException();
    }
}
