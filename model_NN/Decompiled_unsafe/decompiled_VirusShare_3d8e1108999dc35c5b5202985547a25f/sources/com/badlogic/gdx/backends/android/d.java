package com.badlogic.gdx.backends.android;

import android.opengl.GLSurfaceView;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import com.badlogic.gdx.g;
import com.badlogic.gdx.graphics.b;
import com.badlogic.gdx.graphics.e;
import com.badlogic.gdx.graphics.f;
import com.badlogic.gdx.graphics.h;
import com.badlogic.gdx.graphics.j;
import com.badlogic.gdx.graphics.k;
import com.badlogic.gdx.i;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

public final class d implements GLSurfaceView.Renderer, i {
    final View a;
    AndroidApplication b;
    volatile boolean c = false;
    volatile boolean d = false;
    volatile boolean e = false;
    volatile boolean f = false;
    Object g = new Object();
    private int h;
    private int i;
    private e j;
    private com.badlogic.gdx.graphics.i k;
    private j l;
    private h m;
    private k n;
    private long o = System.nanoTime();
    private float p = 0.0f;
    private long q = System.nanoTime();
    private int r = 0;
    private int s;
    private com.badlogic.gdx.math.e t = new com.badlogic.gdx.math.e();
    private volatile boolean u = false;
    private float v = 0.0f;
    private float w = 0.0f;
    private float x = 0.0f;
    private float y = 0.0f;
    private int[] z = new int[1];

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v11, resolved type: com.badlogic.gdx.backends.android.surfaceview.e} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v12, resolved type: com.badlogic.gdx.backends.android.surfaceview.a} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v18, resolved type: com.badlogic.gdx.backends.android.surfaceview.i} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v20, resolved type: com.badlogic.gdx.backends.android.surfaceview.a} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v21, resolved type: com.badlogic.gdx.backends.android.surfaceview.a} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v22, resolved type: com.badlogic.gdx.backends.android.surfaceview.a} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public d(com.badlogic.gdx.backends.android.AndroidApplication r10, boolean r11, com.badlogic.gdx.backends.android.surfaceview.f r12) {
        /*
            r9 = this;
            r4 = 10
            r6 = 1
            r2 = 0
            r7 = 0
            r9.<init>()
            long r0 = java.lang.System.nanoTime()
            r9.o = r0
            r9.p = r2
            long r0 = java.lang.System.nanoTime()
            r9.q = r0
            r9.r = r7
            com.badlogic.gdx.math.e r0 = new com.badlogic.gdx.math.e
            r0.<init>()
            r9.t = r0
            r9.u = r7
            r9.c = r7
            r9.d = r7
            r9.e = r7
            r9.f = r7
            r9.v = r2
            r9.w = r2
            r9.x = r2
            r9.y = r2
            int[] r0 = new int[r6]
            r9.z = r0
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            r9.g = r0
            java.lang.String r0 = android.os.Build.DEVICE
            java.lang.String r1 = "GT-I7500"
            boolean r0 = r0.equalsIgnoreCase(r1)
            if (r0 != 0) goto L_0x0086
            r0 = 0
            r8 = r0
        L_0x0048:
            if (r11 == 0) goto L_0x008f
            javax.microedition.khronos.egl.EGL r0 = javax.microedition.khronos.egl.EGLContext.getEGL()
            javax.microedition.khronos.egl.EGL10 r0 = (javax.microedition.khronos.egl.EGL10) r0
            java.lang.Object r1 = javax.microedition.khronos.egl.EGL10.EGL_DEFAULT_DISPLAY
            javax.microedition.khronos.egl.EGLDisplay r1 = r0.eglGetDisplay(r1)
            r2 = 2
            int[] r2 = new int[r2]
            r0.eglInitialize(r1, r2)
            r2 = 9
            int[] r2 = new int[r2]
            r2 = {12324, 4, 12323, 4, 12322, 4, 12352, 4, 12344} // fill-array
            javax.microedition.khronos.egl.EGLConfig[] r3 = new javax.microedition.khronos.egl.EGLConfig[r4]
            int[] r5 = new int[r6]
            r0.eglChooseConfig(r1, r2, r3, r4, r5)
            r0.eglTerminate(r1)
            r0 = r5[r7]
            if (r0 <= 0) goto L_0x008d
            r0 = r6
        L_0x0072:
            if (r0 == 0) goto L_0x008f
            com.badlogic.gdx.backends.android.surfaceview.i r0 = new com.badlogic.gdx.backends.android.surfaceview.i
            r0.<init>(r10, r12)
            if (r8 == 0) goto L_0x007e
            r0.setEGLConfigChooser(r8)
        L_0x007e:
            r0.setRenderer(r9)
        L_0x0081:
            r9.a = r0
            r9.b = r10
            return
        L_0x0086:
            com.badlogic.gdx.backends.android.d$1 r0 = new com.badlogic.gdx.backends.android.d$1
            r0.<init>()
            r8 = r0
            goto L_0x0048
        L_0x008d:
            r0 = r7
            goto L_0x0072
        L_0x008f:
            java.lang.String r0 = android.os.Build.VERSION.SDK
            int r0 = java.lang.Integer.parseInt(r0)
            r1 = 4
            if (r0 > r1) goto L_0x00a6
            com.badlogic.gdx.backends.android.surfaceview.e r0 = new com.badlogic.gdx.backends.android.surfaceview.e
            r0.<init>(r10, r12)
            if (r8 == 0) goto L_0x00a2
            r0.a(r8)
        L_0x00a2:
            r0.a(r9)
            goto L_0x0081
        L_0x00a6:
            com.badlogic.gdx.backends.android.surfaceview.a r0 = new com.badlogic.gdx.backends.android.surfaceview.a
            r0.<init>(r10, r12)
            if (r8 == 0) goto L_0x00b0
            r0.setEGLConfigChooser(r8)
        L_0x00b0:
            r0.setRenderer(r9)
            goto L_0x0081
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.d.<init>(com.badlogic.gdx.backends.android.AndroidApplication, boolean, com.badlogic.gdx.backends.android.surfaceview.f):void");
    }

    private void f() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        this.b.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        this.v = displayMetrics.xdpi;
        this.w = displayMetrics.ydpi;
        this.x = displayMetrics.xdpi / 2.54f;
        this.y = displayMetrics.ydpi / 2.54f;
    }

    public final com.badlogic.gdx.graphics.i b() {
        return this.k;
    }

    public final h c() {
        return this.m;
    }

    public final int e() {
        return this.i;
    }

    public final int d() {
        return this.h;
    }

    public final boolean a() {
        return this.m != null;
    }

    public final void onSurfaceChanged(GL10 gl10, int i2, int i3) {
        this.h = i2;
        this.i = i3;
        f();
        gl10.glViewport(0, 0, this.h, this.i);
        if (!this.u) {
            this.b.c.a();
            this.u = true;
            synchronized (this) {
                this.c = true;
            }
        }
        this.b.c.a(i2, i3);
    }

    public final void onSurfaceCreated(GL10 gl10, EGLConfig eGLConfig) {
        if (this.k == null && this.m == null) {
            if (this.a instanceof com.badlogic.gdx.backends.android.surfaceview.i) {
                this.m = new AndroidGL20();
                this.j = this.m;
            } else {
                this.k = new f(gl10);
                this.j = this.k;
                if ((gl10 instanceof GL11) && !gl10.glGetString(7937).toLowerCase().contains("pixelflinger") && !Build.MODEL.equals("MB200") && !Build.MODEL.equals("MB220") && !Build.MODEL.contains("Behold")) {
                    this.l = new g((GL11) gl10);
                    this.k = this.l;
                }
            }
            this.n = new k();
            g.f = this.j;
            g.g = this.k;
            g.h = this.l;
            g.i = this.m;
            g.j = this.n;
            g.a.a("AndroidGraphics", "OGL renderer: " + gl10.glGetString(7937));
            g.a.a("AndroidGraphics", "OGL vendor: " + gl10.glGetString(7936));
            g.a.a("AndroidGraphics", "OGL version: " + gl10.glGetString(7938));
            g.a.a("AndroidGraphics", "OGL extensions: " + gl10.glGetString(7939));
        }
        EGL10 egl10 = (EGL10) EGLContext.getEGL();
        EGLDisplay eglGetDisplay = egl10.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        int a2 = a(egl10, eglGetDisplay, eGLConfig, 12324);
        int a3 = a(egl10, eglGetDisplay, eGLConfig, 12323);
        int a4 = a(egl10, eglGetDisplay, eGLConfig, 12322);
        int a5 = a(egl10, eglGetDisplay, eGLConfig, 12321);
        int a6 = a(egl10, eglGetDisplay, eGLConfig, 12325);
        int a7 = a(egl10, eglGetDisplay, eGLConfig, 12326);
        g.a.a("AndroidGraphics", "framebuffer: (" + a2 + ", " + a3 + ", " + a4 + ", " + a5 + ")");
        g.a.a("AndroidGraphics", "depthbuffer: (" + a6 + ")");
        g.a.a("AndroidGraphics", "stencilbuffer: (" + a7 + ")");
        f();
        f.a(this.b);
        b.b(this.b);
        com.badlogic.gdx.graphics.a.e.a(this.b);
        com.badlogic.gdx.graphics.a.f.a(this.b);
        g.a.a("AndroidGraphics", f.f());
        g.a.a("AndroidGraphics", b.f());
        g.a.a("AndroidGraphics", com.badlogic.gdx.graphics.a.e.f());
        g.a.a("AndroidGraphics", com.badlogic.gdx.graphics.a.f.a());
        Display defaultDisplay = this.b.getWindowManager().getDefaultDisplay();
        this.h = defaultDisplay.getWidth();
        this.i = defaultDisplay.getHeight();
        this.t = new com.badlogic.gdx.math.e();
        this.o = System.nanoTime();
        gl10.glViewport(0, 0, this.h, this.i);
    }

    private int a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig, int i2) {
        if (egl10.eglGetConfigAttrib(eGLDisplay, eGLConfig, i2, this.z)) {
            return this.z[0];
        }
        return 0;
    }

    public final void onDrawFrame(GL10 gl10) {
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        long nanoTime = System.nanoTime();
        this.p = ((float) (nanoTime - this.o)) / 1.0E9f;
        this.o = nanoTime;
        this.t.a(this.p);
        synchronized (this.g) {
            z2 = this.c;
            z3 = this.d;
            z4 = this.f;
            z5 = this.e;
            if (this.e) {
                this.e = false;
            }
            if (this.d) {
                this.d = false;
                this.g.notifyAll();
            }
            if (this.f) {
                this.f = false;
                this.g.notifyAll();
            }
        }
        if (z5) {
            this.b.c.b();
            g.a.a("AndroidGraphics", "resumed");
        }
        if (z2) {
            synchronized (this.b.d) {
                for (int i2 = 0; i2 < this.b.d.size(); i2++) {
                    this.b.d.get(i2).run();
                }
                this.b.d.clear();
            }
            this.b.a.d();
            this.b.c.c();
        }
        if (z3) {
            this.b.c.d();
            g.a.a("AndroidGraphics", "paused");
        }
        if (z4) {
            this.b.c.e();
            this.b.b.c();
            this.b.b = null;
            g.a.a("AndroidGraphics", "destroyed");
        }
        if (nanoTime - this.q > 1000000000) {
            this.s = this.r;
            this.r = 0;
            this.q = nanoTime;
        }
        this.r++;
    }
}
