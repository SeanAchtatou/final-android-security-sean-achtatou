package com.badlogic.gdx.backends.android.surfaceview;

import com.badlogic.gdx.physics.box2d.Transform;
import java.io.IOException;
import java.io.Writer;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;
import java.util.Arrays;
import javax.microedition.khronos.opengles.GL;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

final class d extends h {
    private Writer e;
    private boolean f;
    private int g;
    private a h;
    private a i;
    private a j;
    private a k;
    private boolean l;
    private boolean m;
    private boolean n;
    private boolean o;
    private StringBuilder p;

    public d(GL gl, Writer writer, boolean z) {
        super(gl);
        this.e = writer;
        this.f = z;
    }

    private void a() {
        int glGetError = this.a.glGetError();
        if (glGetError != 0) {
            a(("glError: " + Integer.toString(glGetError)) + 10);
        }
    }

    private void a(String str) {
        try {
            this.e.write(str);
        } catch (IOException e2) {
        }
    }

    private void b(String str) {
        a(str + '(');
        this.g = 0;
    }

    private void a(String str, String str2) {
        int i2 = this.g;
        this.g = i2 + 1;
        if (i2 > 0) {
            a(", ");
        }
        if (this.f) {
            a(str + "=");
        }
        a(str2);
    }

    private void b() {
        a(");\n");
        c();
    }

    private void c() {
        try {
            this.e.flush();
        } catch (IOException e2) {
            this.e = null;
        }
    }

    private void a(String str, boolean z) {
        a(str, Boolean.toString(z));
    }

    private void a(String str, int i2) {
        a(str, Integer.toString(i2));
    }

    private void a(String str, float f2) {
        a(str, Float.toString(f2));
    }

    private void c(String str) {
        a(") returns " + str + ";\n");
        c();
    }

    private void a(String str, int i2, int[] iArr, int i3) {
        a(str, a(i2, 0, iArr, i3));
    }

    private void a(String str, int i2, float[] fArr, int i3) {
        StringBuilder sb = new StringBuilder();
        sb.append("{\n");
        int length = fArr.length;
        for (int i4 = 0; i4 < i2; i4++) {
            int i5 = i3 + i4;
            sb.append("[" + i5 + "] = ");
            if (i5 < 0 || i5 >= length) {
                sb.append("out of bounds");
            } else {
                sb.append(fArr[i5]);
            }
            sb.append(10);
        }
        sb.append("}");
        a(str, sb.toString());
    }

    private static void a(StringBuilder sb, int i2, int i3) {
        switch (i3) {
            case Transform.POS_X /*0*/:
                sb.append(i2);
                return;
            case 1:
                sb.append(Float.intBitsToFloat(i2));
                return;
            case 2:
                sb.append(((float) i2) / 65536.0f);
                return;
            default:
                return;
        }
    }

    private static String a(int i2, int i3, int[] iArr, int i4) {
        StringBuilder sb = new StringBuilder();
        sb.append("{\n");
        int length = iArr.length;
        for (int i5 = 0; i5 < i2; i5++) {
            int i6 = i4 + i5;
            sb.append(" [" + i6 + "] = ");
            if (i6 < 0 || i6 >= length) {
                sb.append("out of bounds");
            } else {
                a(sb, iArr[i6], i3);
            }
            sb.append(10);
        }
        sb.append("}");
        return sb.toString();
    }

    private static String a(int i2, int i3, IntBuffer intBuffer) {
        StringBuilder sb = new StringBuilder();
        sb.append("{\n");
        for (int i4 = 0; i4 < i2; i4++) {
            sb.append(" [" + i4 + "] = ");
            a(sb, intBuffer.get(i4), i3);
            sb.append(10);
        }
        sb.append("}");
        return sb.toString();
    }

    private void a(String str, int i2, FloatBuffer floatBuffer) {
        StringBuilder sb = new StringBuilder();
        sb.append("{\n");
        for (int i3 = 0; i3 < i2; i3++) {
            sb.append(" [" + i3 + "] = " + floatBuffer.get(i3) + 10);
        }
        sb.append("}");
        a(str, sb.toString());
    }

    private void a(String str, int i2, IntBuffer intBuffer) {
        a(str, a(i2, 0, intBuffer));
    }

    private void a(int i2, int i3, int i4, Buffer buffer) {
        String str;
        a("size", i2);
        switch (i3) {
            case 5120:
                str = "GL_BYTE";
                break;
            case 5121:
                str = "GL_UNSIGNED_BYTE";
                break;
            case 5122:
                str = "GL_SHORT";
                break;
            case 5126:
                str = "GL_FLOAT";
                break;
            case 5132:
                str = "GL_FIXED";
                break;
            default:
                str = a(i3);
                break;
        }
        a("type", str);
        a("stride", i4);
        a("pointer", buffer.toString());
    }

    private static String a(int i2) {
        return "0x" + Integer.toHexString(i2);
    }

    private static String b(int i2) {
        switch (i2) {
            case Transform.POS_X /*0*/:
                return "GL_ZERO";
            case 1:
                return "GL_ONE";
            case 768:
                return "GL_SRC_COLOR";
            case 769:
                return "GL_ONE_MINUS_SRC_COLOR";
            case 770:
                return "GL_SRC_ALPHA";
            case 771:
                return "GL_ONE_MINUS_SRC_ALPHA";
            case 772:
                return "GL_DST_ALPHA";
            case 773:
                return "GL_ONE_MINUS_DST_ALPHA";
            case 774:
                return "GL_DST_COLOR";
            case 775:
                return "GL_ONE_MINUS_DST_COLOR";
            case 776:
                return "GL_SRC_ALPHA_SATURATE";
            default:
                return a(i2);
        }
    }

    private static String c(int i2) {
        switch (i2) {
            case 3553:
                return "GL_TEXTURE_2D";
            default:
                return a(i2);
        }
    }

    private static String d(int i2) {
        switch (i2) {
            case 8960:
                return "GL_TEXTURE_ENV";
            default:
                return a(i2);
        }
    }

    private static String e(int i2) {
        switch (i2) {
            case 8704:
                return "GL_TEXTURE_ENV_MODE";
            case 8705:
                return "GL_TEXTURE_ENV_COLOR";
            default:
                return a(i2);
        }
    }

    private static int f(int i2) {
        switch (i2) {
            case 8704:
                return 1;
            case 8705:
                return 4;
            default:
                return 0;
        }
    }

    private static String g(int i2) {
        switch (i2) {
            case 32884:
                return "GL_VERTEX_ARRAY";
            case 32885:
                return "GL_NORMAL_ARRAY";
            case 32886:
                return "GL_COLOR_ARRAY";
            case 32887:
            default:
                return a(i2);
            case 32888:
                return "GL_TEXTURE_COORD_ARRAY";
        }
    }

    private static String h(int i2) {
        switch (i2) {
            case 2832:
                return "GL_POINT_SMOOTH";
            case 2848:
                return "GL_LINE_SMOOTH";
            case 2884:
                return "GL_CULL_FACE";
            case 2896:
                return "GL_LIGHTING";
            case 2903:
                return "GL_COLOR_MATERIAL";
            case 2912:
                return "GL_FOG";
            case 2929:
                return "GL_DEPTH_TEST";
            case 2960:
                return "GL_STENCIL_TEST";
            case 2977:
                return "GL_NORMALIZE";
            case 3008:
                return "GL_ALPHA_TEST";
            case 3024:
                return "GL_DITHER";
            case 3042:
                return "GL_BLEND";
            case 3058:
                return "GL_COLOR_LOGIC_OP";
            case 3089:
                return "GL_SCISSOR_TEST";
            case 3553:
                return "GL_TEXTURE_2D";
            case 16384:
                return "GL_LIGHT0";
            case 16385:
                return "GL_LIGHT1";
            case 16386:
                return "GL_LIGHT2";
            case 16387:
                return "GL_LIGHT3";
            case 16388:
                return "GL_LIGHT4";
            case 16389:
                return "GL_LIGHT5";
            case 16390:
                return "GL_LIGHT6";
            case 16391:
                return "GL_LIGHT7";
            case 32826:
                return "GL_RESCALE_NORMAL";
            case 32884:
                return "GL_VERTEX_ARRAY";
            case 32885:
                return "GL_NORMAL_ARRAY";
            case 32886:
                return "GL_COLOR_ARRAY";
            case 32888:
                return "GL_TEXTURE_COORD_ARRAY";
            case 32925:
                return "GL_MULTISAMPLE";
            case 32926:
                return "GL_SAMPLE_ALPHA_TO_COVERAGE";
            case 32927:
                return "GL_SAMPLE_ALPHA_TO_ONE";
            case 32928:
                return "GL_SAMPLE_COVERAGE";
            default:
                return a(i2);
        }
    }

    private static String i(int i2) {
        switch (i2) {
            case 10240:
                return "GL_TEXTURE_MAG_FILTER";
            case 10241:
                return "GL_TEXTURE_MIN_FILTER";
            case 10242:
                return "GL_TEXTURE_WRAP_S";
            case 10243:
                return "GL_TEXTURE_WRAP_T";
            case 33169:
                return "GL_GENERATE_MIPMAP";
            case 35741:
                return "GL_TEXTURE_CROP_RECT_OES";
            default:
                return a(i2);
        }
    }

    private static String j(int i2) {
        switch (i2) {
            case 2914:
                return "GL_FOG_DENSITY";
            case 2915:
                return "GL_FOG_START";
            case 2916:
                return "GL_FOG_END";
            case 2917:
                return "GL_FOG_MODE";
            case 2918:
                return "GL_FOG_COLOR";
            default:
                return a(i2);
        }
    }

    private static int k(int i2) {
        switch (i2) {
            case 2914:
            case 2915:
            case 2916:
            case 2917:
                return 1;
            case 2918:
                return 4;
            default:
                return 0;
        }
    }

    private static String l(int i2) {
        switch (i2) {
            case 2834:
                return "GL_SMOOTH_POINT_SIZE_RANGE";
            case 2850:
                return "GL_SMOOTH_LINE_WIDTH_RANGE";
            case 3377:
                return "GL_MAX_LIGHTS";
            case 3379:
                return "GL_MAX_TEXTURE_SIZE";
            case 3382:
                return "GL_MAX_MODELVIEW_STACK_DEPTH";
            case 3384:
                return "GL_MAX_PROJECTION_STACK_DEPTH";
            case 3385:
                return "GL_MAX_TEXTURE_STACK_DEPTH";
            case 3386:
                return "GL_MAX_VIEWPORT_DIMS";
            case 3408:
                return "GL_SUBPIXEL_BITS";
            case 3410:
                return "GL_RED_BITS";
            case 3411:
                return "GL_GREEN_BITS";
            case 3412:
                return "GL_BLUE_BITS";
            case 3413:
                return "GL_ALPHA_BITS";
            case 3414:
                return "GL_DEPTH_BITS";
            case 3415:
                return "GL_STENCIL_BITS";
            case 33000:
                return "GL_MAX_ELEMENTS_VERTICES";
            case 33001:
                return "GL_MAX_ELEMENTS_INDICES";
            case 33901:
                return "GL_ALIASED_POINT_SIZE_RANGE";
            case 33902:
                return "GL_ALIASED_LINE_WIDTH_RANGE";
            case 34018:
                return "GL_MAX_TEXTURE_UNITS";
            case 34466:
                return "GL_NUM_COMPRESSED_TEXTURE_FORMATS";
            case 34467:
                return "GL_COMPRESSED_TEXTURE_FORMATS";
            case 35213:
                return "GL_MODELVIEW_MATRIX_FLOAT_AS_INT_BITS_OES";
            case 35214:
                return "GL_PROJECTION_MATRIX_FLOAT_AS_INT_BITS_OES";
            case 35215:
                return "GL_TEXTURE_MATRIX_FLOAT_AS_INT_BITS_OES";
            default:
                return a(i2);
        }
    }

    private int m(int i2) {
        switch (i2) {
            case 2834:
                return 2;
            case 2850:
                return 2;
            case 3377:
            case 3379:
            case 3382:
            case 3384:
            case 3385:
            case 3408:
            case 3410:
            case 3411:
            case 3412:
            case 3413:
            case 3414:
            case 3415:
            case 33000:
            case 33001:
            case 34018:
            case 34466:
                return 1;
            case 3386:
                return 2;
            case 33901:
                return 2;
            case 33902:
                return 2;
            case 34467:
                int[] iArr = new int[1];
                this.a.glGetIntegerv(34466, iArr, 0);
                return iArr[0];
            case 35213:
            case 35214:
            case 35215:
                return 16;
            default:
                return 0;
        }
    }

    private static int n(int i2) {
        switch (i2) {
            case 35213:
            case 35214:
            case 35215:
                return 1;
            default:
                return 0;
        }
    }

    private static String o(int i2) {
        switch (i2) {
            case 1032:
                return "GL_FRONT_AND_BACK";
            default:
                return a(i2);
        }
    }

    private static String p(int i2) {
        switch (i2) {
            case 4608:
                return "GL_AMBIENT";
            case 4609:
                return "GL_DIFFUSE";
            case 4610:
                return "GL_SPECULAR";
            case 5632:
                return "GL_EMISSION";
            case 5633:
                return "GL_SHININESS";
            case 5634:
                return "GL_AMBIENT_AND_DIFFUSE";
            default:
                return a(i2);
        }
    }

    private static int q(int i2) {
        switch (i2) {
            case 4608:
            case 4609:
            case 4610:
            case 5632:
            case 5634:
                return 4;
            case 5633:
                return 1;
            default:
                return 0;
        }
    }

    private static String r(int i2) {
        if (i2 < 16384 || i2 > 16391) {
            return a(i2);
        }
        return "GL_LIGHT" + Integer.toString(i2);
    }

    private static String s(int i2) {
        switch (i2) {
            case 4608:
                return "GL_AMBIENT";
            case 4609:
                return "GL_DIFFUSE";
            case 4610:
                return "GL_SPECULAR";
            case 4611:
                return "GL_POSITION";
            case 4612:
                return "GL_SPOT_DIRECTION";
            case 4613:
                return "GL_SPOT_EXPONENT";
            case 4614:
                return "GL_SPOT_CUTOFF";
            case 4615:
                return "GL_CONSTANT_ATTENUATION";
            case 4616:
                return "GL_LINEAR_ATTENUATION";
            case 4617:
                return "GL_QUADRATIC_ATTENUATION";
            default:
                return a(i2);
        }
    }

    private static int t(int i2) {
        switch (i2) {
            case 4608:
            case 4609:
            case 4610:
            case 4611:
                return 4;
            case 4612:
                return 3;
            case 4613:
                return 1;
            case 4614:
                return 1;
            case 4615:
                return 1;
            case 4616:
                return 1;
            case 4617:
                return 1;
            default:
                return 0;
        }
    }

    private static String u(int i2) {
        switch (i2) {
            case 2898:
                return "GL_LIGHT_MODEL_TWO_SIDE";
            case 2899:
                return "GL_LIGHT_MODEL_AMBIENT";
            default:
                return a(i2);
        }
    }

    private static int v(int i2) {
        switch (i2) {
            case 2898:
                return 1;
            case 2899:
                return 4;
            default:
                return 0;
        }
    }

    static ByteBuffer a(int i2, Buffer buffer) {
        ByteBuffer order;
        int i3 = 0;
        boolean z = i2 < 0;
        if (buffer instanceof ByteBuffer) {
            ByteBuffer byteBuffer = (ByteBuffer) buffer;
            if (z) {
                i2 = byteBuffer.limit();
            }
            order = ByteBuffer.allocate(i2).order(byteBuffer.order());
            int position = byteBuffer.position();
            while (i3 < i2) {
                order.put(byteBuffer.get());
                i3++;
            }
            byteBuffer.position(position);
        } else if (buffer instanceof CharBuffer) {
            CharBuffer charBuffer = (CharBuffer) buffer;
            if (z) {
                i2 = charBuffer.limit() * 2;
            }
            order = ByteBuffer.allocate(i2).order(charBuffer.order());
            CharBuffer asCharBuffer = order.asCharBuffer();
            int position2 = charBuffer.position();
            while (i3 < i2 / 2) {
                asCharBuffer.put(charBuffer.get());
                i3++;
            }
            charBuffer.position(position2);
        } else if (buffer instanceof ShortBuffer) {
            ShortBuffer shortBuffer = (ShortBuffer) buffer;
            if (z) {
                i2 = shortBuffer.limit() * 2;
            }
            order = ByteBuffer.allocate(i2).order(shortBuffer.order());
            ShortBuffer asShortBuffer = order.asShortBuffer();
            int position3 = shortBuffer.position();
            while (i3 < i2 / 2) {
                asShortBuffer.put(shortBuffer.get());
                i3++;
            }
            shortBuffer.position(position3);
        } else if (buffer instanceof IntBuffer) {
            IntBuffer intBuffer = (IntBuffer) buffer;
            if (z) {
                i2 = intBuffer.limit() * 4;
            }
            order = ByteBuffer.allocate(i2).order(intBuffer.order());
            IntBuffer asIntBuffer = order.asIntBuffer();
            int position4 = intBuffer.position();
            while (i3 < i2 / 4) {
                asIntBuffer.put(intBuffer.get());
                i3++;
            }
            intBuffer.position(position4);
        } else if (buffer instanceof FloatBuffer) {
            FloatBuffer floatBuffer = (FloatBuffer) buffer;
            if (z) {
                i2 = floatBuffer.limit() * 4;
            }
            order = ByteBuffer.allocate(i2).order(floatBuffer.order());
            FloatBuffer asFloatBuffer = order.asFloatBuffer();
            int position5 = floatBuffer.position();
            while (i3 < i2 / 4) {
                asFloatBuffer.put(floatBuffer.get());
                i3++;
            }
            floatBuffer.position(position5);
        } else if (buffer instanceof DoubleBuffer) {
            DoubleBuffer doubleBuffer = (DoubleBuffer) buffer;
            if (z) {
                i2 = doubleBuffer.limit() * 8;
            }
            order = ByteBuffer.allocate(i2).order(doubleBuffer.order());
            DoubleBuffer asDoubleBuffer = order.asDoubleBuffer();
            int position6 = doubleBuffer.position();
            while (i3 < i2 / 8) {
                asDoubleBuffer.put(doubleBuffer.get());
                i3++;
            }
            doubleBuffer.position(position6);
        } else if (buffer instanceof LongBuffer) {
            LongBuffer longBuffer = (LongBuffer) buffer;
            if (z) {
                i2 = longBuffer.limit() * 8;
            }
            order = ByteBuffer.allocate(i2).order(longBuffer.order());
            LongBuffer asLongBuffer = order.asLongBuffer();
            int position7 = longBuffer.position();
            while (i3 < i2 / 8) {
                asLongBuffer.put(longBuffer.get());
                i3++;
            }
            longBuffer.position(position7);
        } else {
            throw new RuntimeException("Unimplemented Buffer subclass.");
        }
        order.rewind();
        order.order(ByteOrder.nativeOrder());
        return order;
    }

    private static void a(StringBuilder sb, boolean z, String str, a aVar, int i2) {
        int a2;
        if (z) {
            sb.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
            sb.append(str + ":{");
            if (aVar == null) {
                sb.append("undefined");
            } else if (aVar.c < 0) {
                sb.append("invalid stride");
            } else {
                if (aVar.c > 0) {
                    a2 = aVar.c;
                } else {
                    a2 = a.a(aVar.b) * aVar.a;
                }
                ByteBuffer byteBuffer = aVar.d;
                int i3 = aVar.a;
                int i4 = aVar.b;
                int a3 = a.a(i4);
                int i5 = a2 * i2;
                for (int i6 = 0; i6 < i3; i6++) {
                    if (i6 > 0) {
                        sb.append(", ");
                    }
                    switch (i4) {
                        case 5120:
                            sb.append(Integer.toString(byteBuffer.get(i5)));
                            break;
                        case 5121:
                            sb.append(Integer.toString(byteBuffer.get(i5) & 255));
                            break;
                        case 5122:
                            sb.append(Integer.toString(byteBuffer.asShortBuffer().get(i5 / 2)));
                            break;
                        case 5126:
                            sb.append(Float.toString(byteBuffer.asFloatBuffer().get(i5 / 4)));
                            break;
                        case 5132:
                            sb.append(Integer.toString(byteBuffer.asIntBuffer().get(i5 / 4)));
                            break;
                        default:
                            sb.append("?");
                            break;
                    }
                    i5 += a3;
                }
                sb.append("}");
            }
        }
    }

    private void b(StringBuilder sb, int i2, int i3) {
        sb.append(" [" + i2 + " : " + i3 + "] =");
        a(sb, this.o, "v", this.k, i3);
        a(sb, this.m, "n", this.i, i3);
        a(sb, this.l, "c", this.h, i3);
        a(sb, this.n, "t", this.j, i3);
        sb.append("\n");
    }

    private void d() {
        this.p = new StringBuilder();
        this.p.append("\n");
        if (this.l) {
            this.h.a();
        }
        if (this.m) {
            this.i.a();
        }
        if (this.n) {
            this.j.a();
        }
        if (this.o) {
            this.k.a();
        }
    }

    private void e() {
        a(this.p.toString());
        if (this.l) {
            this.h.d = null;
        }
        if (this.m) {
            this.i.d = null;
        }
        if (this.n) {
            this.j.d = null;
        }
        if (this.o) {
            this.k.d = null;
        }
    }

    public final void glActiveTexture(int i2) {
        b("glActiveTexture");
        a("texture", i2);
        b();
        this.a.glActiveTexture(i2);
        a();
    }

    public final void glAlphaFunc(int i2, float f2) {
        b("glAlphaFunc");
        a("func", i2);
        a("ref", f2);
        b();
        this.a.glAlphaFunc(i2, f2);
        a();
    }

    public final void glAlphaFuncx(int i2, int i3) {
        b("glAlphaFuncx");
        a("func", i2);
        a("ref", i3);
        b();
        this.a.glAlphaFuncx(i2, i3);
        a();
    }

    public final void glBindTexture(int i2, int i3) {
        b("glBindTexture");
        a("target", c(i2));
        a("texture", i3);
        b();
        this.a.glBindTexture(i2, i3);
        a();
    }

    public final void glBlendFunc(int i2, int i3) {
        b("glBlendFunc");
        a("sfactor", b(i2));
        a("dfactor", b(i3));
        b();
        this.a.glBlendFunc(i2, i3);
        a();
    }

    public final void glClear(int i2) {
        int i3;
        b("glClear");
        StringBuilder sb = new StringBuilder();
        if ((i2 & 256) != 0) {
            sb.append("GL_DEPTH_BUFFER_BIT");
            i3 = i2 & -257;
        } else {
            i3 = i2;
        }
        if ((i3 & 1024) != 0) {
            if (sb.length() > 0) {
                sb.append(" | ");
            }
            sb.append("GL_STENCIL_BUFFER_BIT");
            i3 &= -1025;
        }
        if ((i3 & 16384) != 0) {
            if (sb.length() > 0) {
                sb.append(" | ");
            }
            sb.append("GL_COLOR_BUFFER_BIT");
            i3 &= -16385;
        }
        if (i3 != 0) {
            if (sb.length() > 0) {
                sb.append(" | ");
            }
            sb.append(a(i3));
        }
        a("mask", sb.toString());
        b();
        this.a.glClear(i2);
        a();
    }

    public final void glClearColor(float f2, float f3, float f4, float f5) {
        b("glClearColor");
        a("red", f2);
        a("green", f3);
        a("blue", f4);
        a("alpha", f5);
        b();
        this.a.glClearColor(f2, f3, f4, f5);
        a();
    }

    public final void glClearColorx(int i2, int i3, int i4, int i5) {
        b("glClearColor");
        a("red", i2);
        a("green", i3);
        a("blue", i4);
        a("alpha", i5);
        b();
        this.a.glClearColorx(i2, i3, i4, i5);
        a();
    }

    public final void glClearDepthf(float f2) {
        b("glClearDepthf");
        a("depth", f2);
        b();
        this.a.glClearDepthf(f2);
        a();
    }

    public final void glClearDepthx(int i2) {
        b("glClearDepthx");
        a("depth", i2);
        b();
        this.a.glClearDepthx(i2);
        a();
    }

    public final void glClearStencil(int i2) {
        b("glClearStencil");
        a("s", i2);
        b();
        this.a.glClearStencil(i2);
        a();
    }

    public final void glClientActiveTexture(int i2) {
        b("glClientActiveTexture");
        a("texture", i2);
        b();
        this.a.glClientActiveTexture(i2);
        a();
    }

    public final void glColor4f(float f2, float f3, float f4, float f5) {
        b("glColor4f");
        a("red", f2);
        a("green", f3);
        a("blue", f4);
        a("alpha", f5);
        b();
        this.a.glColor4f(f2, f3, f4, f5);
        a();
    }

    public final void glColor4x(int i2, int i3, int i4, int i5) {
        b("glColor4x");
        a("red", i2);
        a("green", i3);
        a("blue", i4);
        a("alpha", i5);
        b();
        this.a.glColor4x(i2, i3, i4, i5);
        a();
    }

    public final void glColorMask(boolean z, boolean z2, boolean z3, boolean z4) {
        b("glColorMask");
        a("red", z);
        a("green", z2);
        a("blue", z3);
        a("alpha", z4);
        b();
        this.a.glColorMask(z, z2, z3, z4);
        a();
    }

    public final void glColorPointer(int i2, int i3, int i4, Buffer buffer) {
        b("glColorPointer");
        a(i2, i3, i4, buffer);
        b();
        this.h = new a(i2, i3, i4, buffer);
        this.a.glColorPointer(i2, i3, i4, buffer);
        a();
    }

    public final void glCompressedTexImage2D(int i2, int i3, int i4, int i5, int i6, int i7, int i8, Buffer buffer) {
        b("glCompressedTexImage2D");
        a("target", c(i2));
        a("level", i3);
        a("internalformat", i4);
        a("width", i5);
        a("height", i6);
        a("border", i7);
        a("imageSize", i8);
        a("data", buffer.toString());
        b();
        this.a.glCompressedTexImage2D(i2, i3, i4, i5, i6, i7, i8, buffer);
        a();
    }

    public final void glCompressedTexSubImage2D(int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, Buffer buffer) {
        b("glCompressedTexSubImage2D");
        a("target", c(i2));
        a("level", i3);
        a("xoffset", i4);
        a("yoffset", i5);
        a("width", i6);
        a("height", i7);
        a("format", i8);
        a("imageSize", i9);
        a("data", buffer.toString());
        b();
        this.a.glCompressedTexSubImage2D(i2, i3, i4, i5, i6, i7, i8, i9, buffer);
        a();
    }

    public final void glCopyTexImage2D(int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
        b("glCopyTexImage2D");
        a("target", c(i2));
        a("level", i3);
        a("internalformat", i4);
        a("x", i5);
        a("y", i6);
        a("width", i7);
        a("height", i8);
        a("border", i9);
        b();
        this.a.glCopyTexImage2D(i2, i3, i4, i5, i6, i7, i8, i9);
        a();
    }

    public final void glCopyTexSubImage2D(int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
        b("glCopyTexSubImage2D");
        a("target", c(i2));
        a("level", i3);
        a("xoffset", i4);
        a("yoffset", i5);
        a("x", i6);
        a("y", i7);
        a("width", i8);
        a("height", i9);
        b();
        this.a.glCopyTexSubImage2D(i2, i3, i4, i5, i6, i7, i8, i9);
        a();
    }

    public final void glCullFace(int i2) {
        b("glCullFace");
        a("mode", i2);
        b();
        this.a.glCullFace(i2);
        a();
    }

    public final void glDeleteTextures(int i2, int[] iArr, int i3) {
        b("glDeleteTextures");
        a("n", i2);
        a("textures", i2, iArr, i3);
        a("offset", i3);
        b();
        this.a.glDeleteTextures(i2, iArr, i3);
        a();
    }

    public final void glDeleteTextures(int i2, IntBuffer intBuffer) {
        b("glDeleteTextures");
        a("n", i2);
        a("textures", i2, intBuffer);
        b();
        this.a.glDeleteTextures(i2, intBuffer);
        a();
    }

    public final void glDepthFunc(int i2) {
        b("glDepthFunc");
        a("func", i2);
        b();
        this.a.glDepthFunc(i2);
        a();
    }

    public final void glDepthMask(boolean z) {
        b("glDepthMask");
        a("flag", z);
        b();
        this.a.glDepthMask(z);
        a();
    }

    public final void glDepthRangef(float f2, float f3) {
        b("glDepthRangef");
        a("near", f2);
        a("far", f3);
        b();
        this.a.glDepthRangef(f2, f3);
        a();
    }

    public final void glDepthRangex(int i2, int i3) {
        b("glDepthRangex");
        a("near", i2);
        a("far", i3);
        b();
        this.a.glDepthRangex(i2, i3);
        a();
    }

    public final void glDisable(int i2) {
        b("glDisable");
        a("cap", h(i2));
        b();
        this.a.glDisable(i2);
        a();
    }

    public final void glDisableClientState(int i2) {
        b("glDisableClientState");
        a("array", g(i2));
        b();
        switch (i2) {
            case 32884:
                this.o = false;
                break;
            case 32885:
                this.m = false;
                break;
            case 32886:
                this.l = false;
                break;
            case 32888:
                this.n = false;
                break;
        }
        this.a.glDisableClientState(i2);
        a();
    }

    public final void glDrawArrays(int i2, int i3, int i4) {
        b("glDrawArrays");
        a("mode", i2);
        a("first", i3);
        a("count", i4);
        d();
        for (int i5 = 0; i5 < i4; i5++) {
            b(this.p, i5, i3 + i5);
        }
        e();
        b();
        this.a.glDrawArrays(i2, i3, i4);
        a();
    }

    public final void glDrawElements(int i2, int i3, int i4, Buffer buffer) {
        String str;
        String str2;
        b("glDrawElements");
        switch (i2) {
            case Transform.POS_X /*0*/:
                str = "GL_POINTS";
                break;
            case 1:
                str = "GL_LINES";
                break;
            case 2:
                str = "GL_LINE_LOOP";
                break;
            case 3:
                str = "GL_LINE_STRIP";
                break;
            case 4:
                str = "GL_TRIANGLES";
                break;
            case 5:
                str = "GL_TRIANGLE_STRIP";
                break;
            case 6:
                str = "GL_TRIANGLE_FAN";
                break;
            default:
                str = a(i2);
                break;
        }
        a("mode", str);
        a("count", i3);
        switch (i4) {
            case 5121:
                str2 = "GL_UNSIGNED_BYTE";
                break;
            case 5122:
            default:
                str2 = a(i4);
                break;
            case 5123:
                str2 = "GL_UNSIGNED_SHORT";
                break;
        }
        a("type", str2);
        char[] cArr = new char[i3];
        switch (i4) {
            case 5121:
                ByteBuffer a2 = a(i3, buffer);
                byte[] array = a2.array();
                int arrayOffset = a2.arrayOffset();
                for (int i5 = 0; i5 < i3; i5++) {
                    cArr[i5] = (char) (array[arrayOffset + i5] & 255);
                }
                break;
            case 5123:
                CharBuffer asCharBuffer = buffer instanceof CharBuffer ? (CharBuffer) buffer : a(i3 * 2, buffer).asCharBuffer();
                int position = asCharBuffer.position();
                asCharBuffer.position(0);
                asCharBuffer.get(cArr);
                asCharBuffer.position(position);
                break;
        }
        int length = cArr.length;
        d();
        for (int i6 = 0; i6 < length; i6++) {
            b(this.p, i6, cArr[i6]);
        }
        e();
        b();
        this.a.glDrawElements(i2, i3, i4, buffer);
        a();
    }

    public final void glEnable(int i2) {
        b("glEnable");
        a("cap", h(i2));
        b();
        this.a.glEnable(i2);
        a();
    }

    public final void glEnableClientState(int i2) {
        b("glEnableClientState");
        a("array", g(i2));
        b();
        switch (i2) {
            case 32884:
                this.o = true;
                break;
            case 32885:
                this.m = true;
                break;
            case 32886:
                this.l = true;
                break;
            case 32888:
                this.n = true;
                break;
        }
        this.a.glEnableClientState(i2);
        a();
    }

    public final void glFinish() {
        b("glFinish");
        b();
        this.a.glFinish();
        a();
    }

    public final void glFlush() {
        b("glFlush");
        b();
        this.a.glFlush();
        a();
    }

    public final void glFogf(int i2, float f2) {
        b("glFogf");
        a("pname", i2);
        a("param", f2);
        b();
        this.a.glFogf(i2, f2);
        a();
    }

    public final void glFogfv(int i2, float[] fArr, int i3) {
        b("glFogfv");
        a("pname", j(i2));
        a("params", k(i2), fArr, i3);
        a("offset", i3);
        b();
        this.a.glFogfv(i2, fArr, i3);
        a();
    }

    public final void glFogfv(int i2, FloatBuffer floatBuffer) {
        b("glFogfv");
        a("pname", j(i2));
        a("params", k(i2), floatBuffer);
        b();
        this.a.glFogfv(i2, floatBuffer);
        a();
    }

    public final void glFogx(int i2, int i3) {
        b("glFogx");
        a("pname", j(i2));
        a("param", i3);
        b();
        this.a.glFogx(i2, i3);
        a();
    }

    public final void glFogxv(int i2, int[] iArr, int i3) {
        b("glFogxv");
        a("pname", j(i2));
        a("params", k(i2), iArr, i3);
        a("offset", i3);
        b();
        this.a.glFogxv(i2, iArr, i3);
        a();
    }

    public final void glFogxv(int i2, IntBuffer intBuffer) {
        b("glFogxv");
        a("pname", j(i2));
        a("params", k(i2), intBuffer);
        b();
        this.a.glFogxv(i2, intBuffer);
        a();
    }

    public final void glFrontFace(int i2) {
        b("glFrontFace");
        a("mode", i2);
        b();
        this.a.glFrontFace(i2);
        a();
    }

    public final void glFrustumf(float f2, float f3, float f4, float f5, float f6, float f7) {
        b("glFrustumf");
        a("left", f2);
        a("right", f3);
        a("bottom", f4);
        a("top", f5);
        a("near", f6);
        a("far", f7);
        b();
        this.a.glFrustumf(f2, f3, f4, f5, f6, f7);
        a();
    }

    public final void glFrustumx(int i2, int i3, int i4, int i5, int i6, int i7) {
        b("glFrustumx");
        a("left", i2);
        a("right", i3);
        a("bottom", i4);
        a("top", i5);
        a("near", i6);
        a("far", i7);
        b();
        this.a.glFrustumx(i2, i3, i4, i5, i6, i7);
        a();
    }

    public final void glGenTextures(int i2, int[] iArr, int i3) {
        b("glGenTextures");
        a("n", i2);
        a("textures", Arrays.toString(iArr));
        a("offset", i3);
        this.a.glGenTextures(i2, iArr, i3);
        c(a(i2, 0, iArr, i3));
        a();
    }

    public final void glGenTextures(int i2, IntBuffer intBuffer) {
        b("glGenTextures");
        a("n", i2);
        a("textures", intBuffer.toString());
        this.a.glGenTextures(i2, intBuffer);
        c(a(i2, 0, intBuffer));
        a();
    }

    public final int glGetError() {
        b("glGetError");
        int glGetError = this.a.glGetError();
        c(Integer.toString(glGetError));
        return glGetError;
    }

    public final void glGetIntegerv(int i2, int[] iArr, int i3) {
        b("glGetIntegerv");
        a("pname", l(i2));
        a("params", Arrays.toString(iArr));
        a("offset", i3);
        this.a.glGetIntegerv(i2, iArr, i3);
        c(a(m(i2), n(i2), iArr, i3));
        a();
    }

    public final void glGetIntegerv(int i2, IntBuffer intBuffer) {
        b("glGetIntegerv");
        a("pname", l(i2));
        a("params", intBuffer.toString());
        this.a.glGetIntegerv(i2, intBuffer);
        c(a(m(i2), n(i2), intBuffer));
        a();
    }

    public final String glGetString(int i2) {
        b("glGetString");
        a("name", i2);
        String glGetString = this.a.glGetString(i2);
        c(glGetString);
        a();
        return glGetString;
    }

    public final void glHint(int i2, int i3) {
        String str;
        String str2;
        b("glHint");
        switch (i2) {
            case 3152:
                str = "GL_PERSPECTIVE_CORRECTION_HINT";
                break;
            case 3153:
                str = "GL_POINT_SMOOTH_HINT";
                break;
            case 3154:
                str = "GL_LINE_SMOOTH_HINT";
                break;
            case 3155:
                str = "GL_POLYGON_SMOOTH_HINT";
                break;
            case 3156:
                str = "GL_FOG_HINT";
                break;
            case 33170:
                str = "GL_GENERATE_MIPMAP_HINT";
                break;
            default:
                str = a(i2);
                break;
        }
        a("target", str);
        switch (i3) {
            case 4352:
                str2 = "GL_DONT_CARE";
                break;
            case 4353:
                str2 = "GL_FASTEST";
                break;
            case 4354:
                str2 = "GL_NICEST";
                break;
            default:
                str2 = a(i3);
                break;
        }
        a("mode", str2);
        b();
        this.a.glHint(i2, i3);
        a();
    }

    public final void glLightModelf(int i2, float f2) {
        b("glLightModelf");
        a("pname", u(i2));
        a("param", f2);
        b();
        this.a.glLightModelf(i2, f2);
        a();
    }

    public final void glLightModelfv(int i2, float[] fArr, int i3) {
        b("glLightModelfv");
        a("pname", u(i2));
        a("params", v(i2), fArr, i3);
        a("offset", i3);
        b();
        this.a.glLightModelfv(i2, fArr, i3);
        a();
    }

    public final void glLightModelfv(int i2, FloatBuffer floatBuffer) {
        b("glLightModelfv");
        a("pname", u(i2));
        a("params", v(i2), floatBuffer);
        b();
        this.a.glLightModelfv(i2, floatBuffer);
        a();
    }

    public final void glLightModelx(int i2, int i3) {
        b("glLightModelx");
        a("pname", u(i2));
        a("param", i3);
        b();
        this.a.glLightModelx(i2, i3);
        a();
    }

    public final void glLightModelxv(int i2, int[] iArr, int i3) {
        b("glLightModelxv");
        a("pname", u(i2));
        a("params", v(i2), iArr, i3);
        a("offset", i3);
        b();
        this.a.glLightModelxv(i2, iArr, i3);
        a();
    }

    public final void glLightModelxv(int i2, IntBuffer intBuffer) {
        b("glLightModelfv");
        a("pname", u(i2));
        a("params", v(i2), intBuffer);
        b();
        this.a.glLightModelxv(i2, intBuffer);
        a();
    }

    public final void glLightf(int i2, int i3, float f2) {
        b("glLightf");
        a("light", r(i2));
        a("pname", s(i3));
        a("param", f2);
        b();
        this.a.glLightf(i2, i3, f2);
        a();
    }

    public final void glLightfv(int i2, int i3, float[] fArr, int i4) {
        b("glLightfv");
        a("light", r(i2));
        a("pname", s(i3));
        a("params", t(i3), fArr, i4);
        a("offset", i4);
        b();
        this.a.glLightfv(i2, i3, fArr, i4);
        a();
    }

    public final void glLightfv(int i2, int i3, FloatBuffer floatBuffer) {
        b("glLightfv");
        a("light", r(i2));
        a("pname", s(i3));
        a("params", t(i3), floatBuffer);
        b();
        this.a.glLightfv(i2, i3, floatBuffer);
        a();
    }

    public final void glLightx(int i2, int i3, int i4) {
        b("glLightx");
        a("light", r(i2));
        a("pname", s(i3));
        a("param", i4);
        b();
        this.a.glLightx(i2, i3, i4);
        a();
    }

    public final void glLightxv(int i2, int i3, int[] iArr, int i4) {
        b("glLightxv");
        a("light", r(i2));
        a("pname", s(i3));
        a("params", t(i3), iArr, i4);
        a("offset", i4);
        b();
        this.a.glLightxv(i2, i3, iArr, i4);
        a();
    }

    public final void glLightxv(int i2, int i3, IntBuffer intBuffer) {
        b("glLightxv");
        a("light", r(i2));
        a("pname", s(i3));
        a("params", t(i3), intBuffer);
        b();
        this.a.glLightxv(i2, i3, intBuffer);
        a();
    }

    public final void glLineWidth(float f2) {
        b("glLineWidth");
        a("width", f2);
        b();
        this.a.glLineWidth(f2);
        a();
    }

    public final void glLineWidthx(int i2) {
        b("glLineWidthx");
        a("width", i2);
        b();
        this.a.glLineWidthx(i2);
        a();
    }

    public final void glLoadIdentity() {
        b("glLoadIdentity");
        b();
        this.a.glLoadIdentity();
        a();
    }

    public final void glLoadMatrixf(float[] fArr, int i2) {
        b("glLoadMatrixf");
        a("m", 16, fArr, i2);
        a("offset", i2);
        b();
        this.a.glLoadMatrixf(fArr, i2);
        a();
    }

    public final void glLoadMatrixf(FloatBuffer floatBuffer) {
        b("glLoadMatrixf");
        a("m", 16, floatBuffer);
        b();
        this.a.glLoadMatrixf(floatBuffer);
        a();
    }

    public final void glLoadMatrixx(int[] iArr, int i2) {
        b("glLoadMatrixx");
        a("m", 16, iArr, i2);
        a("offset", i2);
        b();
        this.a.glLoadMatrixx(iArr, i2);
        a();
    }

    public final void glLoadMatrixx(IntBuffer intBuffer) {
        b("glLoadMatrixx");
        a("m", 16, intBuffer);
        b();
        this.a.glLoadMatrixx(intBuffer);
        a();
    }

    public final void glLogicOp(int i2) {
        b("glLogicOp");
        a("opcode", i2);
        b();
        this.a.glLogicOp(i2);
        a();
    }

    public final void glMaterialf(int i2, int i3, float f2) {
        b("glMaterialf");
        a("face", o(i2));
        a("pname", p(i3));
        a("param", f2);
        b();
        this.a.glMaterialf(i2, i3, f2);
        a();
    }

    public final void glMaterialfv(int i2, int i3, float[] fArr, int i4) {
        b("glMaterialfv");
        a("face", o(i2));
        a("pname", p(i3));
        a("params", q(i3), fArr, i4);
        a("offset", i4);
        b();
        this.a.glMaterialfv(i2, i3, fArr, i4);
        a();
    }

    public final void glMaterialfv(int i2, int i3, FloatBuffer floatBuffer) {
        b("glMaterialfv");
        a("face", o(i2));
        a("pname", p(i3));
        a("params", q(i3), floatBuffer);
        b();
        this.a.glMaterialfv(i2, i3, floatBuffer);
        a();
    }

    public final void glMaterialx(int i2, int i3, int i4) {
        b("glMaterialx");
        a("face", o(i2));
        a("pname", p(i3));
        a("param", i4);
        b();
        this.a.glMaterialx(i2, i3, i4);
        a();
    }

    public final void glMaterialxv(int i2, int i3, int[] iArr, int i4) {
        b("glMaterialxv");
        a("face", o(i2));
        a("pname", p(i3));
        a("params", q(i3), iArr, i4);
        a("offset", i4);
        b();
        this.a.glMaterialxv(i2, i3, iArr, i4);
        a();
    }

    public final void glMaterialxv(int i2, int i3, IntBuffer intBuffer) {
        b("glMaterialxv");
        a("face", o(i2));
        a("pname", p(i3));
        a("params", q(i3), intBuffer);
        b();
        this.a.glMaterialxv(i2, i3, intBuffer);
        a();
    }

    public final void glMatrixMode(int i2) {
        String str;
        b("glMatrixMode");
        switch (i2) {
            case 5888:
                str = "GL_MODELVIEW";
                break;
            case 5889:
                str = "GL_PROJECTION";
                break;
            case 5890:
                str = "GL_TEXTURE";
                break;
            default:
                str = a(i2);
                break;
        }
        a("mode", str);
        b();
        this.a.glMatrixMode(i2);
        a();
    }

    public final void glMultMatrixf(float[] fArr, int i2) {
        b("glMultMatrixf");
        a("m", 16, fArr, i2);
        a("offset", i2);
        b();
        this.a.glMultMatrixf(fArr, i2);
        a();
    }

    public final void glMultMatrixf(FloatBuffer floatBuffer) {
        b("glMultMatrixf");
        a("m", 16, floatBuffer);
        b();
        this.a.glMultMatrixf(floatBuffer);
        a();
    }

    public final void glMultMatrixx(int[] iArr, int i2) {
        b("glMultMatrixx");
        a("m", 16, iArr, i2);
        a("offset", i2);
        b();
        this.a.glMultMatrixx(iArr, i2);
        a();
    }

    public final void glMultMatrixx(IntBuffer intBuffer) {
        b("glMultMatrixx");
        a("m", 16, intBuffer);
        b();
        this.a.glMultMatrixx(intBuffer);
        a();
    }

    public final void glMultiTexCoord4f(int i2, float f2, float f3, float f4, float f5) {
        b("glMultiTexCoord4f");
        a("target", i2);
        a("s", f2);
        a("t", f3);
        a("r", f4);
        a("q", f5);
        b();
        this.a.glMultiTexCoord4f(i2, f2, f3, f4, f5);
        a();
    }

    public final void glMultiTexCoord4x(int i2, int i3, int i4, int i5, int i6) {
        b("glMultiTexCoord4x");
        a("target", i2);
        a("s", i3);
        a("t", i4);
        a("r", i5);
        a("q", i6);
        b();
        this.a.glMultiTexCoord4x(i2, i3, i4, i5, i6);
        a();
    }

    public final void glNormal3f(float f2, float f3, float f4) {
        b("glNormal3f");
        a("nx", f2);
        a("ny", f3);
        a("nz", f4);
        b();
        this.a.glNormal3f(f2, f3, f4);
        a();
    }

    public final void glNormal3x(int i2, int i3, int i4) {
        b("glNormal3x");
        a("nx", i2);
        a("ny", i3);
        a("nz", i4);
        b();
        this.a.glNormal3x(i2, i3, i4);
        a();
    }

    public final void glNormalPointer(int i2, int i3, Buffer buffer) {
        b("glNormalPointer");
        a("type", i2);
        a("stride", i3);
        a("pointer", buffer.toString());
        b();
        this.i = new a(3, i2, i3, buffer);
        this.a.glNormalPointer(i2, i3, buffer);
        a();
    }

    public final void glOrthof(float f2, float f3, float f4, float f5, float f6, float f7) {
        b("glOrthof");
        a("left", f2);
        a("right", f3);
        a("bottom", f4);
        a("top", f5);
        a("near", f6);
        a("far", f7);
        b();
        this.a.glOrthof(f2, f3, f4, f5, f6, f7);
        a();
    }

    public final void glOrthox(int i2, int i3, int i4, int i5, int i6, int i7) {
        b("glOrthox");
        a("left", i2);
        a("right", i3);
        a("bottom", i4);
        a("top", i5);
        a("near", i6);
        a("far", i7);
        b();
        this.a.glOrthox(i2, i3, i4, i5, i6, i7);
        a();
    }

    public final void glPixelStorei(int i2, int i3) {
        b("glPixelStorei");
        a("pname", i2);
        a("param", i3);
        b();
        this.a.glPixelStorei(i2, i3);
        a();
    }

    public final void glPointSize(float f2) {
        b("glPointSize");
        a("size", f2);
        b();
        this.a.glPointSize(f2);
        a();
    }

    public final void glPointSizex(int i2) {
        b("glPointSizex");
        a("size", i2);
        b();
        this.a.glPointSizex(i2);
        a();
    }

    public final void glPolygonOffset(float f2, float f3) {
        b("glPolygonOffset");
        a("factor", f2);
        a("units", f3);
        b();
        this.a.glPolygonOffset(f2, f3);
        a();
    }

    public final void glPolygonOffsetx(int i2, int i3) {
        b("glPolygonOffsetx");
        a("factor", i2);
        a("units", i3);
        b();
        this.a.glPolygonOffsetx(i2, i3);
        a();
    }

    public final void glPopMatrix() {
        b("glPopMatrix");
        b();
        this.a.glPopMatrix();
        a();
    }

    public final void glPushMatrix() {
        b("glPushMatrix");
        b();
        this.a.glPushMatrix();
        a();
    }

    public final void glReadPixels(int i2, int i3, int i4, int i5, int i6, int i7, Buffer buffer) {
        b("glReadPixels");
        a("x", i2);
        a("y", i3);
        a("width", i4);
        a("height", i5);
        a("format", i6);
        a("type", i7);
        a("pixels", buffer.toString());
        b();
        this.a.glReadPixels(i2, i3, i4, i5, i6, i7, buffer);
        a();
    }

    public final void glRotatef(float f2, float f3, float f4, float f5) {
        b("glRotatef");
        a("angle", f2);
        a("x", f3);
        a("y", f4);
        a("z", f5);
        b();
        this.a.glRotatef(f2, f3, f4, f5);
        a();
    }

    public final void glRotatex(int i2, int i3, int i4, int i5) {
        b("glRotatex");
        a("angle", i2);
        a("x", i3);
        a("y", i4);
        a("z", i5);
        b();
        this.a.glRotatex(i2, i3, i4, i5);
        a();
    }

    public final void glSampleCoverage(float f2, boolean z) {
        b("glSampleCoveragex");
        a("value", f2);
        a("invert", z);
        b();
        this.a.glSampleCoverage(f2, z);
        a();
    }

    public final void glSampleCoveragex(int i2, boolean z) {
        b("glSampleCoveragex");
        a("value", i2);
        a("invert", z);
        b();
        this.a.glSampleCoveragex(i2, z);
        a();
    }

    public final void glScalef(float f2, float f3, float f4) {
        b("glScalef");
        a("x", f2);
        a("y", f3);
        a("z", f4);
        b();
        this.a.glScalef(f2, f3, f4);
        a();
    }

    public final void glScalex(int i2, int i3, int i4) {
        b("glScalex");
        a("x", i2);
        a("y", i3);
        a("z", i4);
        b();
        this.a.glScalex(i2, i3, i4);
        a();
    }

    public final void glScissor(int i2, int i3, int i4, int i5) {
        b("glScissor");
        a("x", i2);
        a("y", i3);
        a("width", i4);
        a("height", i5);
        b();
        this.a.glScissor(i2, i3, i4, i5);
        a();
    }

    public final void glShadeModel(int i2) {
        String str;
        b("glShadeModel");
        switch (i2) {
            case 7424:
                str = "GL_FLAT";
                break;
            case 7425:
                str = "GL_SMOOTH";
                break;
            default:
                str = a(i2);
                break;
        }
        a("mode", str);
        b();
        this.a.glShadeModel(i2);
        a();
    }

    public final void glStencilFunc(int i2, int i3, int i4) {
        b("glStencilFunc");
        a("func", i2);
        a("ref", i3);
        a("mask", i4);
        b();
        this.a.glStencilFunc(i2, i3, i4);
        a();
    }

    public final void glStencilMask(int i2) {
        b("glStencilMask");
        a("mask", i2);
        b();
        this.a.glStencilMask(i2);
        a();
    }

    public final void glStencilOp(int i2, int i3, int i4) {
        b("glStencilOp");
        a("fail", i2);
        a("zfail", i3);
        a("zpass", i4);
        b();
        this.a.glStencilOp(i2, i3, i4);
        a();
    }

    public final void glTexCoordPointer(int i2, int i3, int i4, Buffer buffer) {
        b("glTexCoordPointer");
        a(i2, i3, i4, buffer);
        b();
        this.j = new a(i2, i3, i4, buffer);
        this.a.glTexCoordPointer(i2, i3, i4, buffer);
        a();
    }

    public final void glTexEnvf(int i2, int i3, float f2) {
        String f3;
        b("glTexEnvf");
        a("target", d(i2));
        a("pname", e(i3));
        int i4 = (int) f2;
        if (f2 == ((float) i4)) {
            switch (i4) {
                case 260:
                    f3 = "GL_ADD";
                    break;
                case 3042:
                    f3 = "GL_BLEND";
                    break;
                case 7681:
                    f3 = "GL_REPLACE";
                    break;
                case 8448:
                    f3 = "GL_MODULATE";
                    break;
                case 8449:
                    f3 = "GL_DECAL";
                    break;
                case 34160:
                    f3 = "GL_COMBINE";
                    break;
                default:
                    f3 = a(i4);
                    break;
            }
        } else {
            f3 = Float.toString(f2);
        }
        a("param", f3);
        b();
        this.a.glTexEnvf(i2, i3, f2);
        a();
    }

    public final void glTexEnvfv(int i2, int i3, float[] fArr, int i4) {
        b("glTexEnvfv");
        a("target", d(i2));
        a("pname", e(i3));
        a("params", f(i3), fArr, i4);
        a("offset", i4);
        b();
        this.a.glTexEnvfv(i2, i3, fArr, i4);
        a();
    }

    public final void glTexEnvfv(int i2, int i3, FloatBuffer floatBuffer) {
        b("glTexEnvfv");
        a("target", d(i2));
        a("pname", e(i3));
        a("params", f(i3), floatBuffer);
        b();
        this.a.glTexEnvfv(i2, i3, floatBuffer);
        a();
    }

    public final void glTexEnvx(int i2, int i3, int i4) {
        b("glTexEnvx");
        a("target", d(i2));
        a("pname", e(i3));
        a("param", i4);
        b();
        this.a.glTexEnvx(i2, i3, i4);
        a();
    }

    public final void glTexEnvxv(int i2, int i3, int[] iArr, int i4) {
        b("glTexEnvxv");
        a("target", d(i2));
        a("pname", e(i3));
        a("params", f(i3), iArr, i4);
        a("offset", i4);
        b();
        this.a.glTexEnvxv(i2, i3, iArr, i4);
        a();
    }

    public final void glTexEnvxv(int i2, int i3, IntBuffer intBuffer) {
        b("glTexEnvxv");
        a("target", d(i2));
        a("pname", e(i3));
        a("params", f(i3), intBuffer);
        b();
        this.a.glTexEnvxv(i2, i3, intBuffer);
        a();
    }

    public final void glTexImage2D(int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, Buffer buffer) {
        b("glTexImage2D");
        a("target", i2);
        a("level", i3);
        a("internalformat", i4);
        a("width", i5);
        a("height", i6);
        a("border", i7);
        a("format", i8);
        a("type", i9);
        a("pixels", buffer.toString());
        b();
        this.a.glTexImage2D(i2, i3, i4, i5, i6, i7, i8, i9, buffer);
        a();
    }

    public final void glTexParameterf(int i2, int i3, float f2) {
        String f3;
        b("glTexParameterf");
        a("target", c(i2));
        a("pname", i(i3));
        int i4 = (int) f2;
        if (f2 == ((float) i4)) {
            switch (i4) {
                case 9728:
                    f3 = "GL_NEAREST";
                    break;
                case 9729:
                    f3 = "GL_LINEAR";
                    break;
                case 9984:
                    f3 = "GL_NEAREST_MIPMAP_NEAREST";
                    break;
                case 9985:
                    f3 = "GL_LINEAR_MIPMAP_NEAREST";
                    break;
                case 9986:
                    f3 = "GL_NEAREST_MIPMAP_LINEAR";
                    break;
                case 9987:
                    f3 = "GL_LINEAR_MIPMAP_LINEAR";
                    break;
                case 10497:
                    f3 = "GL_REPEAT";
                    break;
                case 33071:
                    f3 = "GL_CLAMP_TO_EDGE";
                    break;
                default:
                    f3 = a(i4);
                    break;
            }
        } else {
            f3 = Float.toString(f2);
        }
        a("param", f3);
        b();
        this.a.glTexParameterf(i2, i3, f2);
        a();
    }

    public final void glTexParameterx(int i2, int i3, int i4) {
        b("glTexParameterx");
        a("target", c(i2));
        a("pname", i(i3));
        a("param", i4);
        b();
        this.a.glTexParameterx(i2, i3, i4);
        a();
    }

    public final void glTexParameteriv(int i2, int i3, int[] iArr, int i4) {
        b("glTexParameteriv");
        a("target", c(i2));
        a("pname", i(i3));
        a("params", 4, iArr, i4);
        b();
        this.c.glTexParameteriv(i2, i3, iArr, i4);
        a();
    }

    public final void glTexParameteriv(int i2, int i3, IntBuffer intBuffer) {
        b("glTexParameteriv");
        a("target", c(i2));
        a("pname", i(i3));
        a("params", 4, intBuffer);
        b();
        this.c.glTexParameteriv(i2, i3, intBuffer);
        a();
    }

    public final void glTexSubImage2D(int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, Buffer buffer) {
        b("glTexSubImage2D");
        a("target", c(i2));
        a("level", i3);
        a("xoffset", i4);
        a("yoffset", i5);
        a("width", i6);
        a("height", i7);
        a("format", i8);
        a("type", i9);
        a("pixels", buffer.toString());
        b();
        this.a.glTexSubImage2D(i2, i3, i4, i5, i6, i7, i8, i9, buffer);
        a();
    }

    public final void glTranslatef(float f2, float f3, float f4) {
        b("glTranslatef");
        a("x", f2);
        a("y", f3);
        a("z", f4);
        b();
        this.a.glTranslatef(f2, f3, f4);
        a();
    }

    public final void glTranslatex(int i2, int i3, int i4) {
        b("glTranslatex");
        a("x", i2);
        a("y", i3);
        a("z", i4);
        b();
        this.a.glTranslatex(i2, i3, i4);
        a();
    }

    public final void glVertexPointer(int i2, int i3, int i4, Buffer buffer) {
        b("glVertexPointer");
        a(i2, i3, i4, buffer);
        b();
        this.k = new a(i2, i3, i4, buffer);
        this.a.glVertexPointer(i2, i3, i4, buffer);
        a();
    }

    public final void glViewport(int i2, int i3, int i4, int i5) {
        b("glViewport");
        a("x", i2);
        a("y", i3);
        a("width", i4);
        a("height", i5);
        b();
        this.a.glViewport(i2, i3, i4, i5);
        a();
    }

    public final void glClipPlanef(int i2, float[] fArr, int i3) {
        b("glClipPlanef");
        a("plane", i2);
        a("equation", 4, fArr, i3);
        a("offset", i3);
        b();
        this.c.glClipPlanef(i2, fArr, i3);
        a();
    }

    public final void glClipPlanef(int i2, FloatBuffer floatBuffer) {
        b("glClipPlanef");
        a("plane", i2);
        a("equation", 4, floatBuffer);
        b();
        this.c.glClipPlanef(i2, floatBuffer);
        a();
    }

    public final void glClipPlanex(int i2, int[] iArr, int i3) {
        b("glClipPlanex");
        a("plane", i2);
        a("equation", 4, iArr, i3);
        a("offset", i3);
        b();
        this.c.glClipPlanex(i2, iArr, i3);
        a();
    }

    public final void glClipPlanex(int i2, IntBuffer intBuffer) {
        b("glClipPlanef");
        a("plane", i2);
        a("equation", 4, intBuffer);
        b();
        this.c.glClipPlanex(i2, intBuffer);
        a();
    }

    public final void glDrawTexfOES(float f2, float f3, float f4, float f5, float f6) {
        b("glDrawTexfOES");
        a("x", f2);
        a("y", f3);
        a("z", f4);
        a("width", f5);
        a("height", f6);
        b();
        this.d.glDrawTexfOES(f2, f3, f4, f5, f6);
        a();
    }

    public final void glDrawTexfvOES(float[] fArr, int i2) {
        b("glDrawTexfvOES");
        a("coords", 5, fArr, i2);
        a("offset", i2);
        b();
        this.d.glDrawTexfvOES(fArr, i2);
        a();
    }

    public final void glDrawTexfvOES(FloatBuffer floatBuffer) {
        b("glDrawTexfvOES");
        a("coords", 5, floatBuffer);
        b();
        this.d.glDrawTexfvOES(floatBuffer);
        a();
    }

    public final void glDrawTexiOES(int i2, int i3, int i4, int i5, int i6) {
        b("glDrawTexiOES");
        a("x", i2);
        a("y", i3);
        a("z", i4);
        a("width", i5);
        a("height", i6);
        b();
        this.d.glDrawTexiOES(i2, i3, i4, i5, i6);
        a();
    }

    public final void glDrawTexivOES(int[] iArr, int i2) {
        b("glDrawTexivOES");
        a("coords", 5, iArr, i2);
        a("offset", i2);
        b();
        this.d.glDrawTexivOES(iArr, i2);
        a();
    }

    public final void glDrawTexivOES(IntBuffer intBuffer) {
        b("glDrawTexivOES");
        a("coords", 5, intBuffer);
        b();
        this.d.glDrawTexivOES(intBuffer);
        a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.backends.android.surfaceview.d.a(java.lang.String, int):void
     arg types: [java.lang.String, short]
     candidates:
      com.badlogic.gdx.backends.android.surfaceview.d.a(int, java.nio.Buffer):java.nio.ByteBuffer
      com.badlogic.gdx.backends.android.surfaceview.d.a(java.lang.String, float):void
      com.badlogic.gdx.backends.android.surfaceview.d.a(java.lang.String, java.lang.String):void
      com.badlogic.gdx.backends.android.surfaceview.d.a(java.lang.String, boolean):void
      com.badlogic.gdx.backends.android.surfaceview.d.a(java.lang.String, int):void */
    public final void glDrawTexsOES(short s, short s2, short s3, short s4, short s5) {
        b("glDrawTexsOES");
        a("x", (int) s);
        a("y", (int) s2);
        a("z", (int) s3);
        a("width", (int) s4);
        a("height", (int) s5);
        b();
        this.d.glDrawTexsOES(s, s2, s3, s4, s5);
        a();
    }

    public final void glDrawTexsvOES(short[] sArr, int i2) {
        b("glDrawTexsvOES");
        StringBuilder sb = new StringBuilder();
        sb.append("{\n");
        int length = sArr.length;
        for (int i3 = 0; i3 < 5; i3++) {
            int i4 = i2 + i3;
            sb.append(" [" + i4 + "] = ");
            if (i4 < 0 || i4 >= length) {
                sb.append("out of bounds");
            } else {
                sb.append((int) sArr[i4]);
            }
            sb.append(10);
        }
        sb.append("}");
        a("coords", sb.toString());
        a("offset", i2);
        b();
        this.d.glDrawTexsvOES(sArr, i2);
        a();
    }

    public final void glDrawTexsvOES(ShortBuffer shortBuffer) {
        b("glDrawTexsvOES");
        StringBuilder sb = new StringBuilder();
        sb.append("{\n");
        for (int i2 = 0; i2 < 5; i2++) {
            sb.append(" [" + i2 + "] = " + ((int) shortBuffer.get(i2)) + 10);
        }
        sb.append("}");
        a("coords", sb.toString());
        b();
        this.d.glDrawTexsvOES(shortBuffer);
        a();
    }

    public final void glDrawTexxOES(int i2, int i3, int i4, int i5, int i6) {
        b("glDrawTexxOES");
        a("x", i2);
        a("y", i3);
        a("z", i4);
        a("width", i5);
        a("height", i6);
        b();
        this.d.glDrawTexxOES(i2, i3, i4, i5, i6);
        a();
    }

    public final void glDrawTexxvOES(int[] iArr, int i2) {
        b("glDrawTexxvOES");
        a("coords", 5, iArr, i2);
        a("offset", i2);
        b();
        this.d.glDrawTexxvOES(iArr, i2);
        a();
    }

    public final void glDrawTexxvOES(IntBuffer intBuffer) {
        b("glDrawTexxvOES");
        a("coords", 5, intBuffer);
        b();
        this.d.glDrawTexxvOES(intBuffer);
        a();
    }

    public final int glQueryMatrixxOES(int[] iArr, int i2, int[] iArr2, int i3) {
        b("glQueryMatrixxOES");
        a("mantissa", Arrays.toString(iArr));
        a("exponent", Arrays.toString(iArr2));
        b();
        int glQueryMatrixxOES = this.b.glQueryMatrixxOES(iArr, i2, iArr2, i3);
        c(a(16, 2, iArr, i2));
        c(a(16, 0, iArr2, i3));
        a();
        return glQueryMatrixxOES;
    }

    public final int glQueryMatrixxOES(IntBuffer intBuffer, IntBuffer intBuffer2) {
        b("glQueryMatrixxOES");
        a("mantissa", intBuffer.toString());
        a("exponent", intBuffer2.toString());
        b();
        int glQueryMatrixxOES = this.b.glQueryMatrixxOES(intBuffer, intBuffer2);
        c(a(16, 2, intBuffer));
        c(a(16, 0, intBuffer2));
        a();
        return glQueryMatrixxOES;
    }

    public final void glBindBuffer(int i2, int i3) {
        throw new UnsupportedOperationException();
    }

    public final void glBufferData(int i2, int i3, Buffer buffer, int i4) {
        throw new UnsupportedOperationException();
    }

    public final void glBufferSubData(int i2, int i3, int i4, Buffer buffer) {
        throw new UnsupportedOperationException();
    }

    public final void glColor4ub(byte b, byte b2, byte b3, byte b4) {
        throw new UnsupportedOperationException();
    }

    public final void glDeleteBuffers(int i2, int[] iArr, int i3) {
        throw new UnsupportedOperationException();
    }

    public final void glDeleteBuffers(int i2, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public final void glGenBuffers(int i2, int[] iArr, int i3) {
        throw new UnsupportedOperationException();
    }

    public final void glGenBuffers(int i2, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public final void glGetBooleanv(int i2, boolean[] zArr, int i3) {
        throw new UnsupportedOperationException();
    }

    public final void glGetBooleanv(int i2, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public final void glGetBufferParameteriv(int i2, int i3, int[] iArr, int i4) {
        throw new UnsupportedOperationException();
    }

    public final void glGetBufferParameteriv(int i2, int i3, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public final void glGetClipPlanef(int i2, float[] fArr, int i3) {
        throw new UnsupportedOperationException();
    }

    public final void glGetClipPlanef(int i2, FloatBuffer floatBuffer) {
        throw new UnsupportedOperationException();
    }

    public final void glGetClipPlanex(int i2, int[] iArr, int i3) {
        throw new UnsupportedOperationException();
    }

    public final void glGetClipPlanex(int i2, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public final void glGetFixedv(int i2, int[] iArr, int i3) {
        throw new UnsupportedOperationException();
    }

    public final void glGetFixedv(int i2, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public final void glGetFloatv(int i2, float[] fArr, int i3) {
        throw new UnsupportedOperationException();
    }

    public final void glGetFloatv(int i2, FloatBuffer floatBuffer) {
        throw new UnsupportedOperationException();
    }

    public final void glGetLightfv(int i2, int i3, float[] fArr, int i4) {
        throw new UnsupportedOperationException();
    }

    public final void glGetLightfv(int i2, int i3, FloatBuffer floatBuffer) {
        throw new UnsupportedOperationException();
    }

    public final void glGetLightxv(int i2, int i3, int[] iArr, int i4) {
        throw new UnsupportedOperationException();
    }

    public final void glGetLightxv(int i2, int i3, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public final void glGetMaterialfv(int i2, int i3, float[] fArr, int i4) {
        throw new UnsupportedOperationException();
    }

    public final void glGetMaterialfv(int i2, int i3, FloatBuffer floatBuffer) {
        throw new UnsupportedOperationException();
    }

    public final void glGetMaterialxv(int i2, int i3, int[] iArr, int i4) {
        throw new UnsupportedOperationException();
    }

    public final void glGetMaterialxv(int i2, int i3, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public final void glGetTexEnviv(int i2, int i3, int[] iArr, int i4) {
        throw new UnsupportedOperationException();
    }

    public final void glGetTexEnviv(int i2, int i3, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public final void glGetTexEnvxv(int i2, int i3, int[] iArr, int i4) {
        throw new UnsupportedOperationException();
    }

    public final void glGetTexEnvxv(int i2, int i3, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public final void glGetTexParameterfv(int i2, int i3, float[] fArr, int i4) {
        throw new UnsupportedOperationException();
    }

    public final void glGetTexParameterfv(int i2, int i3, FloatBuffer floatBuffer) {
        throw new UnsupportedOperationException();
    }

    public final void glGetTexParameteriv(int i2, int i3, int[] iArr, int i4) {
        throw new UnsupportedOperationException();
    }

    public final void glGetTexParameteriv(int i2, int i3, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public final void glGetTexParameterxv(int i2, int i3, int[] iArr, int i4) {
        throw new UnsupportedOperationException();
    }

    public final void glGetTexParameterxv(int i2, int i3, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public final boolean glIsBuffer(int i2) {
        throw new UnsupportedOperationException();
    }

    public final boolean glIsEnabled(int i2) {
        throw new UnsupportedOperationException();
    }

    public final boolean glIsTexture(int i2) {
        throw new UnsupportedOperationException();
    }

    public final void glPointParameterf(int i2, float f2) {
        throw new UnsupportedOperationException();
    }

    public final void glPointParameterfv(int i2, float[] fArr, int i3) {
        throw new UnsupportedOperationException();
    }

    public final void glPointParameterfv(int i2, FloatBuffer floatBuffer) {
        throw new UnsupportedOperationException();
    }

    public final void glPointParameterx(int i2, int i3) {
        throw new UnsupportedOperationException();
    }

    public final void glPointParameterxv(int i2, int[] iArr, int i3) {
        throw new UnsupportedOperationException();
    }

    public final void glPointParameterxv(int i2, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public final void glPointSizePointerOES(int i2, int i3, Buffer buffer) {
        throw new UnsupportedOperationException();
    }

    public final void glTexEnvi(int i2, int i3, int i4) {
        throw new UnsupportedOperationException();
    }

    public final void glTexEnviv(int i2, int i3, int[] iArr, int i4) {
        throw new UnsupportedOperationException();
    }

    public final void glTexEnviv(int i2, int i3, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    public final void glTexParameterfv(int i2, int i3, float[] fArr, int i4) {
        throw new UnsupportedOperationException();
    }

    public final void glTexParameterfv(int i2, int i3, FloatBuffer floatBuffer) {
        throw new UnsupportedOperationException();
    }

    public final void glTexParameteri(int i2, int i3, int i4) {
        throw new UnsupportedOperationException();
    }

    public final void glTexParameterxv(int i2, int i3, int[] iArr, int i4) {
        throw new UnsupportedOperationException();
    }

    public final void glTexParameterxv(int i2, int i3, IntBuffer intBuffer) {
        throw new UnsupportedOperationException();
    }

    private class a {
        public int a;
        public int b;
        public int c;
        public ByteBuffer d;
        private Buffer e;

        public a(int i, int i2, int i3, Buffer buffer) {
            this.a = i;
            this.b = i2;
            this.c = i3;
            this.e = buffer;
        }

        public static int a(int i) {
            switch (i) {
                case 5120:
                case 5121:
                    return 1;
                case 5122:
                    return 2;
                case 5126:
                    return 4;
                case 5132:
                    return 4;
                default:
                    return 0;
            }
        }

        public final void a() {
            this.d = d.a(-1, this.e);
        }
    }
}
