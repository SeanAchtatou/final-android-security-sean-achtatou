package com.badlogic.gdx.backends.android.surfaceview;

public final class g extends RuntimeException {
    private final int a;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public g(int r3) {
        /*
            r2 = this;
            java.lang.String r0 = android.opengl.GLU.gluErrorString(r3)
            if (r0 != 0) goto L_0x001d
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Unknown error 0x"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = java.lang.Integer.toHexString(r3)
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
        L_0x001d:
            r2.<init>(r0)
            r2.a = r3
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.surfaceview.g.<init>(int):void");
    }

    public g(String str) {
        super(str);
        this.a = 28672;
    }
}
