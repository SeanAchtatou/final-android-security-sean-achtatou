package com.badlogic.gdx.backends.android;

import android.content.res.AssetManager;
import android.os.Environment;
import com.badlogic.gdx.c.a;
import com.badlogic.gdx.e;

public final class e implements com.badlogic.gdx.e {
    private String a = (Environment.getExternalStorageDirectory().getAbsolutePath() + "/");
    private AssetManager b;

    public e(AssetManager assetManager) {
        this.b = assetManager;
    }

    public final a a(String str) {
        return new m((AssetManager) null, str, e.a.Classpath);
    }

    public final a b(String str) {
        return new m(this.b, str, e.a.Internal);
    }

    public final String a() {
        return this.a;
    }
}
