package com.badlogic.gdx.backends.android;

import android.media.AudioManager;
import android.media.SoundPool;
import com.badlogic.gdx.a.a;

final class o implements a {
    private SoundPool a;
    private AudioManager b;
    private int c;

    o(SoundPool soundPool, AudioManager audioManager, int i) {
        this.a = soundPool;
        this.b = audioManager;
        this.c = i;
    }

    public final void b() {
        this.a.unload(this.c);
    }

    public final void a() {
        this.a.play(this.c, 1.0f, 1.0f, 1, 0, 1.0f);
    }
}
