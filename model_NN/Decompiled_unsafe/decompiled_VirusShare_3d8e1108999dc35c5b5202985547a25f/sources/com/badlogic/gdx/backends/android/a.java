package com.badlogic.gdx.backends.android;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Handler;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import com.badlogic.gdx.b;
import com.badlogic.gdx.f;
import com.badlogic.gdx.g;
import com.badlogic.gdx.physics.box2d.Transform;
import java.util.ArrayList;
import java.util.HashSet;

public final class a implements SensorEventListener, View.OnKeyListener, View.OnTouchListener, com.badlogic.gdx.b {
    private float A = 0.0f;
    private float B = 0.0f;
    private boolean C = false;
    private f D;
    private final n E;
    private boolean F = true;
    private float[] G = new float[9];
    private float[] H = new float[3];
    com.badlogic.gdx.utils.b<C0005a> a = new c(this);
    ArrayList<C0005a> b = new ArrayList<>();
    int[] c = new int[20];
    int[] d = new int[20];
    boolean[] e = new boolean[20];
    int[] f = new int[0];
    boolean g;
    private com.badlogic.gdx.utils.b<b> h = new b(this);
    private ArrayList<b> i = new ArrayList<>();
    private boolean j;
    private HashSet<Integer> k = new HashSet<>();
    private SensorManager l;
    private boolean m = false;
    private final float[] n = new float[3];
    private String o = null;
    private b.a p = null;
    private Handler q;
    private AndroidApplication r;
    private final i s;
    private int t = 0;
    private boolean u = false;
    private final Vibrator v;
    private boolean w = false;
    private final float[] x = new float[3];
    private float y = 0.0f;
    private float z = 0.0f;

    class b {
        int a;
        int b;
        char c;

        b() {
        }
    }

    /* renamed from: com.badlogic.gdx.backends.android.a$a  reason: collision with other inner class name */
    class C0005a {
        long a;
        int b;
        int c;
        int d;
        int e;

        C0005a() {
        }
    }

    public a(AndroidApplication androidApplication, View view, n nVar) {
        boolean z2 = false;
        view.setOnKeyListener(this);
        view.setOnTouchListener(this);
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.requestFocusFromTouch();
        this.E = nVar;
        for (int i2 = 0; i2 < this.f.length; i2++) {
            this.f[i2] = -1;
        }
        this.q = new Handler();
        this.r = androidApplication;
        this.t = nVar.d;
        if (Integer.parseInt(Build.VERSION.SDK) >= 5) {
            this.s = new j();
        } else {
            this.s = new l();
        }
        if ((this.s instanceof j) && androidApplication.getPackageManager().hasSystemFeature("android.hardware.touchscreen.multitouch")) {
            z2 = true;
        }
        this.j = z2;
        this.v = (Vibrator) androidApplication.getSystemService("vibrator");
    }

    public final float a() {
        return this.n[0];
    }

    public final float b() {
        return this.n[1];
    }

    public final float c() {
        return this.n[2];
    }

    public final int a(int i2) {
        int i3;
        synchronized (this) {
            i3 = this.c[i2];
        }
        return i3;
    }

    public final int b(int i2) {
        int i3;
        synchronized (this) {
            i3 = this.d[i2];
        }
        return i3;
    }

    public final boolean c(int i2) {
        boolean z2;
        synchronized (this) {
            z2 = this.e[i2];
        }
        return z2;
    }

    public final void a(f fVar) {
        synchronized (this) {
            this.D = fVar;
        }
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        synchronized (this) {
            this.C = false;
            if (this.D != null) {
                f fVar = this.D;
                int size = this.i.size();
                for (int i2 = 0; i2 < size; i2++) {
                    this.h.a(this.i.get(i2));
                }
                int size2 = this.b.size();
                for (int i3 = 0; i3 < size2; i3++) {
                    C0005a aVar = this.b.get(i3);
                    switch (aVar.b) {
                        case Transform.POS_X /*0*/:
                            fVar.a(aVar.c, aVar.d, aVar.e);
                            this.C = true;
                            break;
                        case 1:
                            fVar.b(aVar.c, aVar.d, aVar.e);
                            break;
                        case 2:
                            fVar.c(aVar.c, aVar.d, aVar.e);
                            break;
                    }
                    this.a.a(aVar);
                }
            } else {
                int size3 = this.b.size();
                for (int i4 = 0; i4 < size3; i4++) {
                    C0005a aVar2 = this.b.get(i4);
                    if (aVar2.b == 0) {
                        this.C = true;
                    }
                    this.a.a(aVar2);
                }
                int size4 = this.i.size();
                for (int i5 = 0; i5 < size4; i5++) {
                    this.h.a(this.i.get(i5));
                }
            }
            this.i.clear();
            this.b.clear();
        }
    }

    public final boolean onTouch(View view, MotionEvent motionEvent) {
        if (this.F) {
            view.requestFocus();
            view.requestFocusFromTouch();
            this.F = false;
        }
        this.s.a(motionEvent, this);
        if (this.t == 0) {
            return true;
        }
        try {
            Thread.sleep((long) this.t);
            return true;
        } catch (InterruptedException e2) {
            return true;
        }
    }

    public final boolean onKey(View view, int i2, KeyEvent keyEvent) {
        char c2;
        synchronized (this) {
            char unicodeChar = (char) keyEvent.getUnicodeChar();
            if (i2 == 67) {
                c2 = 8;
            } else {
                c2 = unicodeChar;
            }
            switch (keyEvent.getAction()) {
                case Transform.POS_X /*0*/:
                    b b2 = this.h.b();
                    b2.c = 0;
                    b2.b = keyEvent.getKeyCode();
                    b2.a = 0;
                    this.i.add(b2);
                    this.k.add(Integer.valueOf(b2.b));
                    break;
                case 1:
                    b b3 = this.h.b();
                    b3.c = 0;
                    b3.b = keyEvent.getKeyCode();
                    b3.a = 1;
                    this.i.add(b3);
                    b b4 = this.h.b();
                    b4.c = c2;
                    b4.b = 0;
                    b4.a = 2;
                    this.i.add(b4);
                    this.k.remove(Integer.valueOf(keyEvent.getKeyCode()));
                    break;
            }
        }
        if (!this.u || i2 != 4) {
            return false;
        }
        return true;
    }

    public final void onAccuracyChanged(Sensor sensor, int i2) {
    }

    public final void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == 1) {
            System.arraycopy(sensorEvent.values, 0, this.n, 0, this.n.length);
        }
        if (sensorEvent.sensor.getType() == 2) {
            System.arraycopy(sensorEvent.values, 0, this.x, 0, this.x.length);
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x003a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void e() {
        /*
            r4 = this;
            r3 = 1
            r2 = 0
            com.badlogic.gdx.backends.android.n r0 = r4.E
            boolean r0 = r0.b
            if (r0 == 0) goto L_0x006d
            com.badlogic.gdx.backends.android.AndroidApplication r0 = r4.r
            java.lang.String r1 = "sensor"
            java.lang.Object r0 = r0.getSystemService(r1)
            android.hardware.SensorManager r0 = (android.hardware.SensorManager) r0
            r4.l = r0
            android.hardware.SensorManager r0 = r4.l
            java.util.List r0 = r0.getSensorList(r3)
            int r0 = r0.size()
            if (r0 == 0) goto L_0x006d
            android.hardware.SensorManager r0 = r4.l
            java.util.List r0 = r0.getSensorList(r3)
            java.lang.Object r0 = r0.get(r2)
            android.hardware.Sensor r0 = (android.hardware.Sensor) r0
            android.hardware.SensorManager r1 = r4.l
            boolean r0 = r1.registerListener(r4, r0, r3)
            r4.m = r0
        L_0x0034:
            com.badlogic.gdx.backends.android.n r0 = r4.E
            boolean r0 = r0.c
            if (r0 == 0) goto L_0x0073
            android.hardware.SensorManager r0 = r4.l
            if (r0 != 0) goto L_0x004a
            com.badlogic.gdx.backends.android.AndroidApplication r0 = r4.r
            java.lang.String r1 = "sensor"
            java.lang.Object r0 = r0.getSystemService(r1)
            android.hardware.SensorManager r0 = (android.hardware.SensorManager) r0
            r4.l = r0
        L_0x004a:
            android.hardware.SensorManager r0 = r4.l
            r1 = 2
            android.hardware.Sensor r0 = r0.getDefaultSensor(r1)
            if (r0 == 0) goto L_0x0070
            boolean r1 = r4.m
            r4.w = r1
            boolean r1 = r4.w
            if (r1 == 0) goto L_0x0063
            android.hardware.SensorManager r1 = r4.l
            boolean r0 = r1.registerListener(r4, r0, r3)
            r4.w = r0
        L_0x0063:
            com.badlogic.gdx.c r0 = com.badlogic.gdx.g.a
            java.lang.String r1 = "AndroidInput"
            java.lang.String r2 = "sensor listener setup"
            r0.a(r1, r2)
            return
        L_0x006d:
            r4.m = r2
            goto L_0x0034
        L_0x0070:
            r4.w = r2
            goto L_0x0063
        L_0x0073:
            r4.w = r2
            goto L_0x0063
        */
        throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.a.e():void");
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        if (this.l != null) {
            this.l.unregisterListener(this);
            this.l = null;
        }
        g.a.a("AndroidInput", "sensor listener tear down");
    }

    public final int d(int i2) {
        int length = this.f.length;
        for (int i3 = 0; i3 < length; i3++) {
            if (this.f[i3] == i2) {
                return i3;
            }
        }
        return -1;
    }
}
