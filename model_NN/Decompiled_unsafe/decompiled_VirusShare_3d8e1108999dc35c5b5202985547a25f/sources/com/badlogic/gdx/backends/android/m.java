package com.badlogic.gdx.backends.android;

import android.content.res.AssetManager;
import com.badlogic.gdx.c.a;
import com.badlogic.gdx.e;
import com.badlogic.gdx.utils.f;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public final class m extends a {
    final AssetManager a;

    m(AssetManager assetManager, String str, e.a aVar) {
        super(str, aVar);
        this.a = assetManager;
    }

    private m(AssetManager assetManager, File file, e.a aVar) {
        super(file, aVar);
        this.a = assetManager;
    }

    public final a a(String str) {
        if (this.b.getPath().length() == 0) {
            return new m(this.a, new File(str), this.c);
        }
        return new m(this.a, new File(this.b, str), this.c);
    }

    public final a a() {
        File parentFile = this.b.getParentFile();
        if (parentFile == null) {
            if (this.c == e.a.Absolute) {
                parentFile = new File("/");
            } else {
                parentFile = new File("");
            }
        }
        return new m(this.a, parentFile, this.c);
    }

    public final InputStream b() {
        if (this.c != e.a.Internal) {
            return super.b();
        }
        try {
            return this.a.open(this.b.getPath());
        } catch (IOException e) {
            throw new f("Error reading file: " + this.b + " (" + this.c + ")", e);
        }
    }
}
