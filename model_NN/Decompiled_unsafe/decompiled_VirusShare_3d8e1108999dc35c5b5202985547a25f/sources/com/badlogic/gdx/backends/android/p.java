package com.badlogic.gdx.backends.android;

import android.media.MediaPlayer;
import com.badlogic.gdx.a.b;
import com.badlogic.gdx.g;
import java.io.IOException;

public final class p implements b {
    private final h a;
    private MediaPlayer b;
    private boolean c;

    public final void a() {
        if (this.b != null) {
            try {
                if (this.b.isPlaying()) {
                    this.b.stop();
                }
                this.b.release();
            } catch (Throwable th) {
                g.a.a("AndroidMusic", "error while disposing AndroidMusic instance, non-fatal");
            } finally {
                this.b = null;
                this.a.a.remove(this);
            }
        }
    }

    public final boolean b() {
        return this.b.isPlaying();
    }

    public final void c() {
        if (this.b.isPlaying()) {
            this.b.pause();
        }
    }

    public final void d() {
        if (!this.b.isPlaying()) {
            try {
                if (!this.c) {
                    this.b.prepare();
                    this.c = true;
                }
                this.b.start();
            } catch (IOException | IllegalStateException e) {
            }
        }
    }
}
