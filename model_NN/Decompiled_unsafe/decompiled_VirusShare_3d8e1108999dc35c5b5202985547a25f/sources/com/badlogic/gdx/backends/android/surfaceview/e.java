package com.badlogic.gdx.backends.android.surfaceview;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.badlogic.gdx.backends.android.surfaceview.f;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import javax.microedition.khronos.opengles.GL;

public final class e extends SurfaceView implements SurfaceHolder.Callback {
    static final Semaphore a = new Semaphore(1);
    GLSurfaceView.EGLConfigChooser b;
    f c;
    private f d;
    private C0006e e;
    private int f = 1;
    private GLSurfaceView.Renderer g;
    private int h;
    private int i;
    private boolean j;

    public interface f {
        GL a();
    }

    public e(Context context, f fVar) {
        super(context);
        this.d = fVar;
        SurfaceHolder holder = getHolder();
        holder.addCallback(this);
        holder.setType(2);
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i2, int i3) {
        f.a a2 = this.d.a(i2, i3);
        setMeasuredDimension(a2.a, a2.b);
    }

    public final void a(GLSurfaceView.Renderer renderer) {
        if (this.g != null) {
            throw new IllegalStateException("setRenderer has already been called for this instance.");
        }
        this.g = renderer;
    }

    public final void a(GLSurfaceView.EGLConfigChooser eGLConfigChooser) {
        if (this.g != null) {
            throw new IllegalStateException("setRenderer has already been called for this instance.");
        }
        this.b = eGLConfigChooser;
    }

    public final void surfaceCreated(SurfaceHolder surfaceHolder) {
        if (this.e != null) {
            this.e.a();
        }
        this.j = true;
    }

    public final void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        if (this.e != null) {
            this.e.b();
        }
        this.j = false;
    }

    public final void surfaceChanged(SurfaceHolder surfaceHolder, int i2, int i3, int i4) {
        if (this.e != null) {
            this.e.a(i3, i4);
        }
        this.h = i3;
        this.i = i4;
    }

    public final void a() {
        this.e.c();
        this.e.e();
        this.e = null;
    }

    public final void b() {
        if (this.b == null) {
            this.b = new b();
        }
        this.e = new C0006e(this.g);
        this.e.start();
        this.e.a(this.f);
        if (this.j) {
            this.e.a();
        }
        if (this.h > 0 && this.i > 0) {
            this.e.a(this.h, this.i);
        }
        this.e.d();
    }

    private static abstract class d implements GLSurfaceView.EGLConfigChooser {
        private int[] a;

        /* access modifiers changed from: package-private */
        public abstract EGLConfig a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig[] eGLConfigArr);

        public d(int[] iArr) {
            this.a = iArr;
        }

        public EGLConfig chooseConfig(EGL10 egl10, EGLDisplay eGLDisplay) {
            int[] iArr = new int[1];
            egl10.eglChooseConfig(eGLDisplay, this.a, null, 0, iArr);
            int i = iArr[0];
            if (i <= 0) {
                throw new IllegalArgumentException("No configs match configSpec");
            }
            EGLConfig[] eGLConfigArr = new EGLConfig[i];
            egl10.eglChooseConfig(eGLDisplay, this.a, eGLConfigArr, i, iArr);
            EGLConfig a2 = a(egl10, eGLDisplay, eGLConfigArr);
            if (a2 != null) {
                return a2;
            }
            throw new IllegalArgumentException("No config chosen");
        }
    }

    private static class a extends d {
        protected int a = 4;
        protected int b = 4;
        protected int c = 4;
        private int[] d = new int[1];
        private int e = 0;
        private int f = 16;
        private int g = 0;

        public a() {
            super(new int[]{12324, 4, 12323, 4, 12322, 4, 12321, 0, 12325, 16, 12326, 0, 12344});
        }

        public final EGLConfig a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig[] eGLConfigArr) {
            EGLConfig eGLConfig;
            EGLConfig eGLConfig2 = null;
            int i = 1000;
            int length = eGLConfigArr.length;
            int i2 = 0;
            while (i2 < length) {
                EGLConfig eGLConfig3 = eGLConfigArr[i2];
                int abs = Math.abs(a(egl10, eGLDisplay, eGLConfig3, 12324) - this.a) + Math.abs(a(egl10, eGLDisplay, eGLConfig3, 12323) - this.b) + Math.abs(a(egl10, eGLDisplay, eGLConfig3, 12322) - this.c) + Math.abs(a(egl10, eGLDisplay, eGLConfig3, 12321) - this.e) + Math.abs(a(egl10, eGLDisplay, eGLConfig3, 12325) - this.f) + Math.abs(a(egl10, eGLDisplay, eGLConfig3, 12326) - this.g);
                if (abs < i) {
                    eGLConfig = eGLConfig3;
                } else {
                    abs = i;
                    eGLConfig = eGLConfig2;
                }
                i2++;
                eGLConfig2 = eGLConfig;
                i = abs;
            }
            return eGLConfig2;
        }

        private int a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig, int i) {
            if (egl10.eglGetConfigAttrib(eGLDisplay, eGLConfig, i, this.d)) {
                return this.d[0];
            }
            return 0;
        }
    }

    private static class b extends a {
        public b() {
            this.a = 5;
            this.b = 6;
            this.c = 5;
        }
    }

    private class c {
        EGL10 a;
        EGLDisplay b;
        EGLSurface c;
        EGLConfig d;
        EGLContext e;

        public c() {
        }

        public final void a() {
            this.a = (EGL10) EGLContext.getEGL();
            this.b = this.a.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
            this.a.eglInitialize(this.b, new int[2]);
            this.d = e.this.b.chooseConfig(this.a, this.b);
            this.e = this.a.eglCreateContext(this.b, this.d, EGL10.EGL_NO_CONTEXT, null);
            this.c = null;
        }

        public final void b() {
            if (this.c != null) {
                this.a.eglMakeCurrent(this.b, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_SURFACE, EGL10.EGL_NO_CONTEXT);
                this.a.eglDestroySurface(this.b, this.c);
                this.c = null;
            }
            if (this.e != null) {
                this.a.eglDestroyContext(this.b, this.e);
                this.e = null;
            }
            if (this.b != null) {
                this.a.eglTerminate(this.b);
                this.b = null;
            }
        }
    }

    /* renamed from: com.badlogic.gdx.backends.android.surfaceview.e$e  reason: collision with other inner class name */
    class C0006e extends Thread {
        private boolean a = false;
        private boolean b;
        private boolean c;
        private int d = 0;
        private int e = 0;
        private int f = 1;
        private boolean g = true;
        private GLSurfaceView.Renderer h;
        private ArrayList<Runnable> i = new ArrayList<>();
        private c j;
        private boolean k;

        C0006e(GLSurfaceView.Renderer renderer) {
            this.h = renderer;
            this.k = true;
            setName("GLThread");
        }

        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0033, code lost:
            com.badlogic.gdx.backends.android.surfaceview.e.a.release();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x003b, code lost:
            if (r12.b == false) goto L_0x0123;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x003d, code lost:
            r12.j.b();
            r4 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0045, code lost:
            if (r12.a == false) goto L_0x004e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0047, code lost:
            r0 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x0048, code lost:
            if (r0 == false) goto L_0x006c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x004a, code lost:
            wait();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0050, code lost:
            if (r12.b != false) goto L_0x0056;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x0054, code lost:
            if (r12.c != false) goto L_0x0058;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x0056, code lost:
            r0 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x005a, code lost:
            if (r12.d <= 0) goto L_0x006a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:37:0x005e, code lost:
            if (r12.e <= 0) goto L_0x006a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x0062, code lost:
            if (r12.g != false) goto L_0x0068;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:0x0066, code lost:
            if (r12.f != 1) goto L_0x006a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:0x0068, code lost:
            r0 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x006a, code lost:
            r0 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:0x006e, code lost:
            if (r12.a == false) goto L_0x007c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:51:?, code lost:
            r0 = r12.k;
            r7 = r12.d;
            r8 = r12.e;
            r12.k = false;
            r12.g = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:53:0x0089, code lost:
            if (r4 == false) goto L_0x0092;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
            r12.j.a();
            r0 = true;
            r2 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:56:0x0092, code lost:
            if (r0 == false) goto L_0x0120;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:57:0x0094, code lost:
            r4 = r12.j;
            r0 = r12.l.getHolder();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:58:0x009e, code lost:
            if (r4.c == null) goto L_0x00b6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:59:0x00a0, code lost:
            r4.a.eglMakeCurrent(r4.b, javax.microedition.khronos.egl.EGL10.EGL_NO_SURFACE, javax.microedition.khronos.egl.EGL10.EGL_NO_SURFACE, javax.microedition.khronos.egl.EGL10.EGL_NO_CONTEXT);
            r4.a.eglDestroySurface(r4.b, r4.c);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:60:0x00b6, code lost:
            r4.c = r4.a.eglCreateWindowSurface(r4.b, r4.d, r0, null);
            r4.a.eglMakeCurrent(r4.b, r4.c, r4.c, r4.e);
            r0 = r4.e.getGL();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:61:0x00da, code lost:
            if (r4.f.c == null) goto L_0x00e4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:62:0x00dc, code lost:
            r0 = r4.f.c.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:63:0x00e4, code lost:
            r4 = (javax.microedition.khronos.opengles.GL10) r0;
            r0 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:64:0x00e8, code lost:
            if (r2 == false) goto L_0x00f4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:65:0x00ea, code lost:
            r12.h.onSurfaceCreated(r4, r12.j.d);
            r2 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:66:0x00f4, code lost:
            if (r0 == false) goto L_0x00fc;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:67:0x00f6, code lost:
            r12.h.onSurfaceChanged(r4, r7, r8);
            r0 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:68:0x00fc, code lost:
            if (r7 <= 0) goto L_0x0115;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:69:0x00fe, code lost:
            if (r8 <= 0) goto L_0x0115;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:70:0x0100, code lost:
            r12.h.onDrawFrame(r4);
            r5 = r12.j;
            r5.a.eglSwapBuffers(r5.b, r5.c);
            r5.a.eglGetError();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:72:0x0119, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:73:0x011a, code lost:
            com.badlogic.gdx.backends.android.surfaceview.e.a.release();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:74:0x011f, code lost:
            throw r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:75:0x0120, code lost:
            r0 = r5;
            r4 = r6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:76:0x0123, code lost:
            r4 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:84:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Removed duplicated region for block: B:72:0x0119 A[ExcHandler:  FINALLY, Splitter:B:1:0x0003] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void run() {
            /*
                r12 = this;
                r0 = 0
                r3 = 0
                r1 = 1
                java.util.concurrent.Semaphore r2 = com.badlogic.gdx.backends.android.surfaceview.e.a     // Catch:{ InterruptedException -> 0x0032, all -> 0x0119 }
                r2.acquire()     // Catch:{ InterruptedException -> 0x0032, all -> 0x0119 }
                com.badlogic.gdx.backends.android.surfaceview.e$c r2 = new com.badlogic.gdx.backends.android.surfaceview.e$c     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                com.badlogic.gdx.backends.android.surfaceview.e r4 = com.badlogic.gdx.backends.android.surfaceview.e.this     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                r2.<init>()     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                r12.j = r2     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                com.badlogic.gdx.backends.android.surfaceview.e$c r2 = r12.j     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                r2.a()     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                r5 = r1
                r2 = r1
                r6 = r0
            L_0x0019:
                boolean r0 = r12.a     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                if (r0 != 0) goto L_0x0071
                monitor-enter(r12)     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
            L_0x001e:
                java.lang.Runnable r0 = r12.f()     // Catch:{ all -> 0x0028 }
                if (r0 == 0) goto L_0x0039
                r0.run()     // Catch:{ all -> 0x0028 }
                goto L_0x001e
            L_0x0028:
                r0 = move-exception
                monitor-exit(r12)     // Catch:{ all -> 0x0028 }
                throw r0     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
            L_0x002b:
                r0 = move-exception
                java.util.concurrent.Semaphore r0 = com.badlogic.gdx.backends.android.surfaceview.e.a
                r0.release()
            L_0x0031:
                return
            L_0x0032:
                r0 = move-exception
                java.util.concurrent.Semaphore r0 = com.badlogic.gdx.backends.android.surfaceview.e.a
                r0.release()
                goto L_0x0031
            L_0x0039:
                boolean r0 = r12.b     // Catch:{ all -> 0x0028 }
                if (r0 == 0) goto L_0x0123
                com.badlogic.gdx.backends.android.surfaceview.e$c r0 = r12.j     // Catch:{ all -> 0x0028 }
                r0.b()     // Catch:{ all -> 0x0028 }
                r4 = r1
            L_0x0043:
                boolean r0 = r12.a     // Catch:{ all -> 0x0028 }
                if (r0 == 0) goto L_0x004e
                r0 = r3
            L_0x0048:
                if (r0 == 0) goto L_0x006c
                r12.wait()     // Catch:{ all -> 0x0028 }
                goto L_0x0043
            L_0x004e:
                boolean r0 = r12.b     // Catch:{ all -> 0x0028 }
                if (r0 != 0) goto L_0x0056
                boolean r0 = r12.c     // Catch:{ all -> 0x0028 }
                if (r0 != 0) goto L_0x0058
            L_0x0056:
                r0 = r1
                goto L_0x0048
            L_0x0058:
                int r0 = r12.d     // Catch:{ all -> 0x0028 }
                if (r0 <= 0) goto L_0x006a
                int r0 = r12.e     // Catch:{ all -> 0x0028 }
                if (r0 <= 0) goto L_0x006a
                boolean r0 = r12.g     // Catch:{ all -> 0x0028 }
                if (r0 != 0) goto L_0x0068
                int r0 = r12.f     // Catch:{ all -> 0x0028 }
                if (r0 != r1) goto L_0x006a
            L_0x0068:
                r0 = r3
                goto L_0x0048
            L_0x006a:
                r0 = r1
                goto L_0x0048
            L_0x006c:
                boolean r0 = r12.a     // Catch:{ all -> 0x0028 }
                if (r0 == 0) goto L_0x007c
                monitor-exit(r12)     // Catch:{ all -> 0x0028 }
            L_0x0071:
                com.badlogic.gdx.backends.android.surfaceview.e$c r0 = r12.j     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                r0.b()     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                java.util.concurrent.Semaphore r0 = com.badlogic.gdx.backends.android.surfaceview.e.a
                r0.release()
                goto L_0x0031
            L_0x007c:
                boolean r0 = r12.k     // Catch:{ all -> 0x0028 }
                int r7 = r12.d     // Catch:{ all -> 0x0028 }
                int r8 = r12.e     // Catch:{ all -> 0x0028 }
                r9 = 0
                r12.k = r9     // Catch:{ all -> 0x0028 }
                r9 = 0
                r12.g = r9     // Catch:{ all -> 0x0028 }
                monitor-exit(r12)     // Catch:{ all -> 0x0028 }
                if (r4 == 0) goto L_0x0092
                com.badlogic.gdx.backends.android.surfaceview.e$c r0 = r12.j     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                r0.a()     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                r0 = r1
                r2 = r1
            L_0x0092:
                if (r0 == 0) goto L_0x0120
                com.badlogic.gdx.backends.android.surfaceview.e$c r4 = r12.j     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                com.badlogic.gdx.backends.android.surfaceview.e r0 = com.badlogic.gdx.backends.android.surfaceview.e.this     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                android.view.SurfaceHolder r0 = r0.getHolder()     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                javax.microedition.khronos.egl.EGLSurface r5 = r4.c     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                if (r5 == 0) goto L_0x00b6
                javax.microedition.khronos.egl.EGL10 r5 = r4.a     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                javax.microedition.khronos.egl.EGLDisplay r6 = r4.b     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                javax.microedition.khronos.egl.EGLSurface r9 = javax.microedition.khronos.egl.EGL10.EGL_NO_SURFACE     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                javax.microedition.khronos.egl.EGLSurface r10 = javax.microedition.khronos.egl.EGL10.EGL_NO_SURFACE     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                javax.microedition.khronos.egl.EGLContext r11 = javax.microedition.khronos.egl.EGL10.EGL_NO_CONTEXT     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                r5.eglMakeCurrent(r6, r9, r10, r11)     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                javax.microedition.khronos.egl.EGL10 r5 = r4.a     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                javax.microedition.khronos.egl.EGLDisplay r6 = r4.b     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                javax.microedition.khronos.egl.EGLSurface r9 = r4.c     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                r5.eglDestroySurface(r6, r9)     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
            L_0x00b6:
                javax.microedition.khronos.egl.EGL10 r5 = r4.a     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                javax.microedition.khronos.egl.EGLDisplay r6 = r4.b     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                javax.microedition.khronos.egl.EGLConfig r9 = r4.d     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                r10 = 0
                javax.microedition.khronos.egl.EGLSurface r0 = r5.eglCreateWindowSurface(r6, r9, r0, r10)     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                r4.c = r0     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                javax.microedition.khronos.egl.EGL10 r0 = r4.a     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                javax.microedition.khronos.egl.EGLDisplay r5 = r4.b     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                javax.microedition.khronos.egl.EGLSurface r6 = r4.c     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                javax.microedition.khronos.egl.EGLSurface r9 = r4.c     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                javax.microedition.khronos.egl.EGLContext r10 = r4.e     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                r0.eglMakeCurrent(r5, r6, r9, r10)     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                javax.microedition.khronos.egl.EGLContext r0 = r4.e     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                javax.microedition.khronos.opengles.GL r0 = r0.getGL()     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                com.badlogic.gdx.backends.android.surfaceview.e r5 = com.badlogic.gdx.backends.android.surfaceview.e.this     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                com.badlogic.gdx.backends.android.surfaceview.e$f r5 = r5.c     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                if (r5 == 0) goto L_0x00e4
                com.badlogic.gdx.backends.android.surfaceview.e r0 = com.badlogic.gdx.backends.android.surfaceview.e.this     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                com.badlogic.gdx.backends.android.surfaceview.e$f r0 = r0.c     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                javax.microedition.khronos.opengles.GL r0 = r0.a()     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
            L_0x00e4:
                javax.microedition.khronos.opengles.GL10 r0 = (javax.microedition.khronos.opengles.GL10) r0     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                r4 = r0
                r0 = r1
            L_0x00e8:
                if (r2 == 0) goto L_0x00f4
                android.opengl.GLSurfaceView$Renderer r2 = r12.h     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                com.badlogic.gdx.backends.android.surfaceview.e$c r5 = r12.j     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                javax.microedition.khronos.egl.EGLConfig r5 = r5.d     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                r2.onSurfaceCreated(r4, r5)     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                r2 = r3
            L_0x00f4:
                if (r0 == 0) goto L_0x00fc
                android.opengl.GLSurfaceView$Renderer r0 = r12.h     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                r0.onSurfaceChanged(r4, r7, r8)     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                r0 = r3
            L_0x00fc:
                if (r7 <= 0) goto L_0x0115
                if (r8 <= 0) goto L_0x0115
                android.opengl.GLSurfaceView$Renderer r5 = r12.h     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                r5.onDrawFrame(r4)     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                com.badlogic.gdx.backends.android.surfaceview.e$c r5 = r12.j     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                javax.microedition.khronos.egl.EGL10 r6 = r5.a     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                javax.microedition.khronos.egl.EGLDisplay r7 = r5.b     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                javax.microedition.khronos.egl.EGLSurface r8 = r5.c     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                r6.eglSwapBuffers(r7, r8)     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                javax.microedition.khronos.egl.EGL10 r5 = r5.a     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
                r5.eglGetError()     // Catch:{ InterruptedException -> 0x002b, all -> 0x0119 }
            L_0x0115:
                r5 = r0
                r6 = r4
                goto L_0x0019
            L_0x0119:
                r0 = move-exception
                java.util.concurrent.Semaphore r1 = com.badlogic.gdx.backends.android.surfaceview.e.a
                r1.release()
                throw r0
            L_0x0120:
                r0 = r5
                r4 = r6
                goto L_0x00e8
            L_0x0123:
                r4 = r3
                goto L_0x0043
            */
            throw new UnsupportedOperationException("Method not decompiled: com.badlogic.gdx.backends.android.surfaceview.e.C0006e.run():void");
        }

        public final void a(int i2) {
            if (i2 < 0 || i2 > 1) {
                throw new IllegalArgumentException("renderMode");
            }
            synchronized (this) {
                this.f = i2;
                if (i2 == 1) {
                    notify();
                }
            }
        }

        public final void a() {
            synchronized (this) {
                this.c = true;
                notify();
            }
        }

        public final void b() {
            synchronized (this) {
                this.c = false;
                notify();
            }
        }

        public final void c() {
            synchronized (this) {
                this.b = true;
            }
        }

        public final void d() {
            synchronized (this) {
                this.b = false;
                notify();
            }
        }

        public final void a(int i2, int i3) {
            synchronized (this) {
                this.d = i2;
                this.e = i3;
                this.k = true;
                notify();
            }
        }

        public final void e() {
            synchronized (this) {
                this.a = true;
                notify();
            }
            try {
                join();
            } catch (InterruptedException e2) {
                Thread.currentThread().interrupt();
            }
        }

        private Runnable f() {
            synchronized (this) {
                if (this.i.size() <= 0) {
                    return null;
                }
                Runnable remove = this.i.remove(0);
                return remove;
            }
        }
    }
}
