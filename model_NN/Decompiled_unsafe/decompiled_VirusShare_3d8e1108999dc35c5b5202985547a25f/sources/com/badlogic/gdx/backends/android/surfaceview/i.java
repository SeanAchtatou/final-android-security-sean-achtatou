package com.badlogic.gdx.backends.android.surfaceview;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.Log;
import com.badlogic.gdx.backends.android.surfaceview.f;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;

public final class i extends GLSurfaceView {
    static String a = "GL2JNIView";
    private f b;

    public i(Context context, f fVar) {
        super(context);
        this.b = fVar;
        setEGLContextFactory(new a());
        setEGLConfigChooser(new b());
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        f.a a2 = this.b.a(i, i2);
        setMeasuredDimension(a2.a, a2.b);
    }

    static class a implements GLSurfaceView.EGLContextFactory {
        private static int a = 12440;

        a() {
        }

        public final EGLContext createContext(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig) {
            Log.w(i.a, "creating OpenGL ES 2.0 context");
            i.a("Before eglCreateContext", egl10);
            EGLContext eglCreateContext = egl10.eglCreateContext(eGLDisplay, eGLConfig, EGL10.EGL_NO_CONTEXT, new int[]{a, 2, 12344});
            i.a("After eglCreateContext", egl10);
            return eglCreateContext;
        }

        public final void destroyContext(EGL10 egl10, EGLDisplay eGLDisplay, EGLContext eGLContext) {
            egl10.eglDestroyContext(eGLDisplay, eGLContext);
        }
    }

    static void a(String str, EGL10 egl10) {
        while (true) {
            int eglGetError = egl10.eglGetError();
            if (eglGetError != 12288) {
                Log.e(a, String.format("%s: EGL error: 0x%x", str, Integer.valueOf(eglGetError)));
            } else {
                return;
            }
        }
    }

    private static class b implements GLSurfaceView.EGLConfigChooser {
        private static int a = 4;
        private static int[] b = {12324, 4, 12323, 4, 12322, 4, 12352, a, 12344};
        private int c = 5;
        private int d = 6;
        private int e = 5;
        private int f = 0;
        private int g = 16;
        private int h = 0;
        private int[] i = new int[1];

        public final EGLConfig chooseConfig(EGL10 egl10, EGLDisplay eGLDisplay) {
            int[] iArr = new int[1];
            egl10.eglChooseConfig(eGLDisplay, b, null, 0, iArr);
            int i2 = iArr[0];
            if (i2 <= 0) {
                throw new IllegalArgumentException("No configs match configSpec");
            }
            EGLConfig[] eGLConfigArr = new EGLConfig[i2];
            egl10.eglChooseConfig(eGLDisplay, b, eGLConfigArr, i2, iArr);
            return a(egl10, eGLDisplay, eGLConfigArr);
        }

        private EGLConfig a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig[] eGLConfigArr) {
            for (EGLConfig eGLConfig : eGLConfigArr) {
                int a2 = a(egl10, eGLDisplay, eGLConfig, 12325);
                int a3 = a(egl10, eGLDisplay, eGLConfig, 12326);
                if (a2 >= this.g && a3 >= this.h) {
                    int a4 = a(egl10, eGLDisplay, eGLConfig, 12324);
                    int a5 = a(egl10, eGLDisplay, eGLConfig, 12323);
                    int a6 = a(egl10, eGLDisplay, eGLConfig, 12322);
                    int a7 = a(egl10, eGLDisplay, eGLConfig, 12321);
                    if (a4 == this.c && a5 == this.d && a6 == this.e && a7 == this.f) {
                        return eGLConfig;
                    }
                }
            }
            return null;
        }

        private int a(EGL10 egl10, EGLDisplay eGLDisplay, EGLConfig eGLConfig, int i2) {
            if (egl10.eglGetConfigAttrib(eGLDisplay, eGLConfig, i2, this.i)) {
                return this.i[0];
            }
            return 0;
        }
    }
}
