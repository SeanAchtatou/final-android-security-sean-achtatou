package com.badlogic.gdx.backends.android;

import android.view.MotionEvent;
import com.badlogic.gdx.backends.android.a;
import com.badlogic.gdx.physics.box2d.Transform;

public final class j implements i {
    public final void a(MotionEvent motionEvent, a aVar) {
        int i = 0;
        int action = motionEvent.getAction() & 255;
        int action2 = (motionEvent.getAction() & 65280) >> 8;
        int pointerId = motionEvent.getPointerId(action2);
        synchronized (aVar) {
            switch (action) {
                case Transform.POS_X /*0*/:
                case 5:
                    int length = aVar.f.length;
                    while (true) {
                        if (i >= length) {
                            int[] iArr = new int[(aVar.f.length + 1)];
                            System.arraycopy(aVar.f, 0, iArr, 0, aVar.f.length);
                            aVar.f = iArr;
                            i = iArr.length - 1;
                        } else if (aVar.f[i] != -1) {
                            i++;
                        }
                    }
                    aVar.f[i] = pointerId;
                    int x = (int) motionEvent.getX(action2);
                    int y = (int) motionEvent.getY(action2);
                    a(aVar, 0, x, y, i);
                    aVar.c[i] = x;
                    aVar.d[i] = y;
                    aVar.e[i] = true;
                    break;
                case 1:
                case 3:
                case 4:
                case 6:
                    int d = aVar.d(pointerId);
                    aVar.f[d] = -1;
                    int x2 = (int) motionEvent.getX(action2);
                    int y2 = (int) motionEvent.getY(action2);
                    a(aVar, 1, x2, y2, d);
                    aVar.c[d] = x2;
                    aVar.d[d] = y2;
                    aVar.e[d] = false;
                    break;
                case 2:
                    int pointerCount = motionEvent.getPointerCount();
                    while (i < pointerCount) {
                        int pointerId2 = motionEvent.getPointerId(i);
                        int x3 = (int) motionEvent.getX(i);
                        int y3 = (int) motionEvent.getY(i);
                        int d2 = aVar.d(pointerId2);
                        a(aVar, 2, x3, y3, d2);
                        aVar.c[d2] = x3;
                        aVar.d[d2] = y3;
                        i++;
                    }
                    break;
            }
        }
    }

    private static void a(a aVar, int i, int i2, int i3, int i4) {
        long nanoTime = System.nanoTime();
        a.C0005a b = aVar.a.b();
        b.a = nanoTime;
        b.e = i4;
        b.c = i2;
        b.d = i3;
        b.b = i;
        aVar.b.add(b);
    }
}
