package com.badlogic.gdx.backends.android;

import com.badlogic.gdx.graphics.i;
import java.nio.Buffer;
import java.nio.IntBuffer;
import javax.microedition.khronos.opengles.GL10;

class f implements i {
    private GL10 a;

    public f(GL10 gl10) {
        this.a = gl10;
    }

    public final void glBindTexture(int i, int i2) {
        this.a.glBindTexture(i, i2);
    }

    public final void glBlendFunc(int i, int i2) {
        this.a.glBlendFunc(i, i2);
    }

    public final void glClear(int i) {
        this.a.glClear(i);
    }

    public final void glClearColor(float f, float f2, float f3, float f4) {
        this.a.glClearColor(f, f2, f3, f4);
    }

    public final void a(int i) {
        this.a.glClientActiveTexture(i);
    }

    public final void a(float f, float f2, float f3, float f4) {
        this.a.glColor4f(f, f2, f3, f4);
    }

    public final void a(int i, int i2, int i3, Buffer buffer) {
        this.a.glColorPointer(i, i2, i3, buffer);
    }

    public final void glDeleteTextures(int i, IntBuffer intBuffer) {
        this.a.glDeleteTextures(i, intBuffer);
    }

    public final void glDepthFunc(int i) {
        this.a.glDepthFunc(i);
    }

    public final void glDepthMask(boolean z) {
        this.a.glDepthMask(z);
    }

    public final void glDisable(int i) {
        this.a.glDisable(i);
    }

    public final void b(int i) {
        this.a.glDisableClientState(i);
    }

    public final void glDrawArrays(int i, int i2, int i3) {
        this.a.glDrawArrays(i, i2, i3);
    }

    public final void glDrawElements(int i, int i2, int i3, Buffer buffer) {
        this.a.glDrawElements(i, i2, i3, buffer);
    }

    public final void glEnable(int i) {
        this.a.glEnable(i);
    }

    public final void c(int i) {
        this.a.glEnableClientState(i);
    }

    public final void glGenTextures(int i, IntBuffer intBuffer) {
        this.a.glGenTextures(i, intBuffer);
    }

    public final void glLineWidth(float f) {
        this.a.glLineWidth(f);
    }

    public final void d(int i) {
        this.a.glMatrixMode(i);
    }

    public final void a(int i, Buffer buffer) {
        this.a.glNormalPointer(5126, i, buffer);
    }

    public final void a() {
        this.a.glPopMatrix();
    }

    public final void b() {
        this.a.glPushMatrix();
    }

    public final void a(float f) {
        this.a.glRotatef(f, 0.0f, 0.0f, 1.0f);
    }

    public final void a(float f, float f2) {
        this.a.glScalef(f, f2, 1.0f);
    }

    public final void a(int i, int i2, Buffer buffer) {
        this.a.glTexCoordPointer(i, 5126, i2, buffer);
    }

    public final void b(float f) {
        this.a.glTexEnvf(8960, 8704, f);
    }

    public final void glTexImage2D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, Buffer buffer) {
        this.a.glTexImage2D(i, i2, i3, i4, i5, i6, i7, i8, buffer);
    }

    public final void glTexParameterf(int i, int i2, float f) {
        this.a.glTexParameterf(i, i2, f);
    }

    public final void a(float f, float f2, float f3) {
        this.a.glTranslatef(f, f2, f3);
    }

    public final void b(int i, int i2, Buffer buffer) {
        this.a.glVertexPointer(i, 5126, i2, buffer);
    }

    public final void a(float[] fArr) {
        this.a.glLoadMatrixf(fArr, 0);
    }

    public final void b(float[] fArr) {
        this.a.glTexEnvfv(8960, 8705, fArr, 0);
    }
}
