package com.badlogic.gdx.backends.android;

import android.app.Activity;
import android.content.res.Configuration;
import android.opengl.GLSurfaceView;
import android.os.Handler;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import com.badlogic.gdx.c;
import com.badlogic.gdx.d;
import com.badlogic.gdx.graphics.a.e;
import com.badlogic.gdx.graphics.b;
import com.badlogic.gdx.graphics.f;
import com.badlogic.gdx.utils.g;
import java.util.ArrayList;
import java.util.List;

public class AndroidApplication extends Activity implements c {
    protected a a;
    protected h b;
    protected d c;
    protected final List<Runnable> d = new ArrayList();
    private d e;
    private e f;
    private Handler g;
    private boolean h = true;
    private PowerManager.WakeLock i = null;

    static {
        g.a();
    }

    public final View a(d dVar) {
        n nVar = new n();
        nVar.a = false;
        this.e = new d(this, nVar.a, nVar.f == null ? new com.badlogic.gdx.backends.android.surfaceview.c() : nVar.f);
        this.a = new a(this, this.e.a, nVar);
        this.b = new h(this);
        this.f = new e(getAssets());
        this.c = dVar;
        this.g = new Handler();
        com.badlogic.gdx.g.a = this;
        com.badlogic.gdx.g.d = this.a;
        com.badlogic.gdx.g.c = this.b;
        com.badlogic.gdx.g.e = this.f;
        com.badlogic.gdx.g.b = this.e;
        if (nVar.e) {
            this.i = ((PowerManager) getSystemService("power")).newWakeLock(26, "libgdx wakelock");
        }
        return this.e.a;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.i != null) {
            this.i.release();
        }
        d dVar = this.e;
        synchronized (dVar.g) {
            if (dVar.c) {
                dVar.c = false;
                dVar.d = true;
                while (dVar.d) {
                    try {
                        dVar.g.wait();
                    } catch (InterruptedException e2) {
                    }
                }
            }
        }
        if (this.b != null) {
            this.b.a();
        }
        this.a.f();
        if (isFinishing()) {
            d dVar2 = this.e;
            f.b(dVar2.b);
            b.a(dVar2.b);
            e.b(dVar2.b);
            com.badlogic.gdx.graphics.a.f.b(dVar2.b);
            com.badlogic.gdx.g.a.a("AndroidGraphics", f.f());
            com.badlogic.gdx.g.a.a("AndroidGraphics", b.f());
            com.badlogic.gdx.g.a.a("AndroidGraphics", e.f());
            com.badlogic.gdx.g.a.a("AndroidGraphics", com.badlogic.gdx.graphics.a.f.a());
            d dVar3 = this.e;
            synchronized (dVar3.g) {
                dVar3.c = false;
                dVar3.f = true;
                while (dVar3.f) {
                    try {
                        dVar3.g.wait();
                    } catch (InterruptedException e3) {
                    }
                }
            }
        }
        if (!(this.e == null || this.e.a == null)) {
            if (this.e.a instanceof com.badlogic.gdx.backends.android.surfaceview.e) {
                ((com.badlogic.gdx.backends.android.surfaceview.e) this.e.a).a();
            }
            if (this.e.a instanceof GLSurfaceView) {
                ((GLSurfaceView) this.e.a).onPause();
            }
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        if (this.i != null) {
            this.i.acquire();
        }
        com.badlogic.gdx.g.a = this;
        com.badlogic.gdx.g.d = this.a;
        com.badlogic.gdx.g.c = this.b;
        com.badlogic.gdx.g.e = this.f;
        com.badlogic.gdx.g.b = this.e;
        this.a.e();
        if (!(this.e == null || this.e.a == null)) {
            if (this.e.a instanceof com.badlogic.gdx.backends.android.surfaceview.e) {
                ((com.badlogic.gdx.backends.android.surfaceview.e) this.e.a).b();
            }
            if (this.e.a instanceof GLSurfaceView) {
                ((GLSurfaceView) this.e.a).onResume();
            }
        }
        if (this.b != null) {
            this.b.b();
        }
        if (!this.h) {
            d dVar = this.e;
            synchronized (dVar.g) {
                dVar.c = true;
                dVar.e = true;
            }
        } else {
            this.h = false;
        }
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public final void a(String str, String str2) {
        Log.d(str, str2);
    }

    public final void a(Runnable runnable) {
        synchronized (this.d) {
            this.d.add(runnable);
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        boolean z = true;
        super.onConfigurationChanged(configuration);
        if (configuration.keyboardHidden != 1) {
            z = false;
        }
        this.a.g = z;
    }
}
