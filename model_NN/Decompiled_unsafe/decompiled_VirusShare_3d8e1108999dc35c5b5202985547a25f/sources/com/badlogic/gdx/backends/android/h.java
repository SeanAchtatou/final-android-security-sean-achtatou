package com.badlogic.gdx.backends.android;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.SoundPool;
import com.badlogic.gdx.a;
import com.badlogic.gdx.e;
import com.badlogic.gdx.utils.f;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public final class h implements a {
    protected final List<p> a = new ArrayList();
    private SoundPool b = new SoundPool(16, 3, 100);
    private final AudioManager c;
    private List<Boolean> d = new ArrayList();

    public h(Activity activity) {
        this.c = (AudioManager) activity.getSystemService("audio");
        activity.setVolumeControlStream(3);
    }

    /* access modifiers changed from: protected */
    public final void a() {
        this.d.clear();
        for (p next : this.a) {
            if (next.b()) {
                next.c();
                this.d.add(true);
            } else {
                this.d.add(false);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void b() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.a.size()) {
                if (this.d.get(i2).booleanValue()) {
                    this.a.get(i2).d();
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public final com.badlogic.gdx.a.a a(com.badlogic.gdx.c.a aVar) {
        m mVar = (m) aVar;
        if (mVar.d() == e.a.Internal) {
            try {
                AssetFileDescriptor openFd = mVar.a.openFd(mVar.c());
                o oVar = new o(this.b, this.c, this.b.load(openFd, 1));
                openFd.close();
                return oVar;
            } catch (IOException e) {
                throw new f("Error loading audio file: " + aVar + "\nNote: Internal audio files must be placed in the assets directory.", e);
            }
        } else {
            try {
                return new o(this.b, this.c, this.b.load(mVar.c(), 1));
            } catch (Exception e2) {
                throw new f("Error loading audio file: " + aVar, e2);
            }
        }
    }

    public final void c() {
        for (p a2 : this.a) {
            a2.a();
        }
        this.b.release();
    }
}
