package com.badlogic.gdx.backends.android;

import android.view.MotionEvent;
import com.badlogic.gdx.backends.android.a;

public final class l implements i {
    public final void a(MotionEvent motionEvent, a aVar) {
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        aVar.c[0] = x;
        aVar.d[0] = y;
        if (motionEvent.getAction() == 0) {
            a(aVar, 0, x, y);
            aVar.e[0] = true;
        } else if (motionEvent.getAction() == 2) {
            a(aVar, 2, x, y);
            aVar.e[0] = true;
        } else if (motionEvent.getAction() == 1) {
            a(aVar, 1, x, y);
            aVar.e[0] = false;
        } else if (motionEvent.getAction() == 3) {
            a(aVar, 1, x, y);
            aVar.e[0] = false;
        }
    }

    private static void a(a aVar, int i, int i2, int i3) {
        long nanoTime = System.nanoTime();
        synchronized (aVar) {
            a.C0005a b = aVar.a.b();
            b.a = nanoTime;
            b.e = 0;
            b.c = i2;
            b.d = i3;
            b.b = i;
            aVar.b.add(b);
        }
    }
}
