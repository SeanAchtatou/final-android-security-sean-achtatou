package com.badlogic.gdx.backends.android;

import com.badlogic.gdx.graphics.h;
import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

public class AndroidGL20 implements h {
    private static native void init();

    public native void glAttachShader(int i, int i2);

    public native void glBindBuffer(int i, int i2);

    public native void glBindFramebuffer(int i, int i2);

    public native void glBindRenderbuffer(int i, int i2);

    public native void glBindTexture(int i, int i2);

    public native void glBlendFunc(int i, int i2);

    public native void glBufferData(int i, int i2, Buffer buffer, int i3);

    public native void glBufferSubData(int i, int i2, int i3, Buffer buffer);

    public native int glCheckFramebufferStatus(int i);

    public native void glClear(int i);

    public native void glClearColor(float f, float f2, float f3, float f4);

    public native void glCompileShader(int i);

    public native int glCreateProgram();

    public native int glCreateShader(int i);

    public native void glDeleteBuffers(int i, IntBuffer intBuffer);

    public native void glDeleteFramebuffers(int i, IntBuffer intBuffer);

    public native void glDeleteProgram(int i);

    public native void glDeleteRenderbuffers(int i, IntBuffer intBuffer);

    public native void glDeleteShader(int i);

    public native void glDeleteTextures(int i, IntBuffer intBuffer);

    public native void glDepthFunc(int i);

    public native void glDepthMask(boolean z);

    public native void glDisable(int i);

    public native void glDisableVertexAttribArray(int i);

    public native void glDrawArrays(int i, int i2, int i3);

    public native void glDrawElements(int i, int i2, int i3, int i4);

    public native void glDrawElements(int i, int i2, int i3, Buffer buffer);

    public native void glEnable(int i);

    public native void glEnableVertexAttribArray(int i);

    public native void glFramebufferRenderbuffer(int i, int i2, int i3, int i4);

    public native void glFramebufferTexture2D(int i, int i2, int i3, int i4, int i5);

    public native void glGenBuffers(int i, IntBuffer intBuffer);

    public native void glGenFramebuffers(int i, IntBuffer intBuffer);

    public native void glGenRenderbuffers(int i, IntBuffer intBuffer);

    public native void glGenTextures(int i, IntBuffer intBuffer);

    public native int glGetAttribLocation(int i, String str);

    public native String glGetProgramInfoLog(int i);

    public native void glGetProgramiv(int i, int i2, IntBuffer intBuffer);

    public native String glGetShaderInfoLog(int i);

    public native void glGetShaderiv(int i, int i2, IntBuffer intBuffer);

    public native int glGetUniformLocation(int i, String str);

    public native void glLineWidth(float f);

    public native void glLinkProgram(int i);

    public native void glRenderbufferStorage(int i, int i2, int i3, int i4);

    public native void glShaderSource(int i, String str);

    public native void glTexImage2D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, Buffer buffer);

    public native void glTexParameterf(int i, int i2, float f);

    public native void glUniform1i(int i, int i2);

    public native void glUniformMatrix4fv(int i, int i2, boolean z, FloatBuffer floatBuffer);

    public native void glUseProgram(int i);

    public native void glVertexAttribPointer(int i, int i2, int i3, boolean z, int i4, int i5);

    static {
        System.loadLibrary("androidgl20");
        init();
    }
}
