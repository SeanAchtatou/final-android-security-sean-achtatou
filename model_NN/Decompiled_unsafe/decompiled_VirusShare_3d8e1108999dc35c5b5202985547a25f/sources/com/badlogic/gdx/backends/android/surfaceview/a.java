package com.badlogic.gdx.backends.android.surfaceview;

import android.content.Context;
import android.opengl.GLSurfaceView;
import com.badlogic.gdx.backends.android.surfaceview.f;

public final class a extends GLSurfaceView {
    private f a;

    public a(Context context, f fVar) {
        super(context);
        this.a = fVar;
    }

    /* access modifiers changed from: protected */
    public final void onMeasure(int i, int i2) {
        f.a a2 = this.a.a(i, i2);
        setMeasuredDimension(a2.a, a2.b);
    }
}
