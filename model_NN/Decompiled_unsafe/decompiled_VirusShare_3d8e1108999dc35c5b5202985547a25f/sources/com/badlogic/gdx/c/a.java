package com.badlogic.gdx.c;

import com.badlogic.gdx.e;
import com.badlogic.gdx.g;
import com.badlogic.gdx.utils.f;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public abstract class a {
    protected File b;
    protected e.a c;

    public abstract a a();

    public abstract a a(String str);

    protected a() {
    }

    protected a(String str, e.a aVar) {
        this.c = aVar;
        this.b = new File(str);
    }

    protected a(File file, e.a aVar) {
        this.b = file;
        this.c = aVar;
    }

    public final String c() {
        return this.b.getPath();
    }

    public final e.a d() {
        return this.c;
    }

    private File e() {
        if (this.c == e.a.External) {
            return new File(g.e.a(), this.b.getPath());
        }
        return this.b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public InputStream b() {
        if (this.c == e.a.Classpath || (this.c == e.a.Internal && !this.b.exists())) {
            InputStream resourceAsStream = a.class.getResourceAsStream("/" + this.b.getPath().replace('\\', '/'));
            if (resourceAsStream != null) {
                return resourceAsStream;
            }
            throw new f("File not found: " + this.b + " (" + this.c + ")");
        }
        try {
            return new FileInputStream(e());
        } catch (FileNotFoundException e) {
            if (e().isDirectory()) {
                throw new f("Cannot open a stream to a directory: " + this.b + " (" + this.c + ")", e);
            }
            throw new f("Error reading file: " + this.b + " (" + this.c + ")", e);
        }
    }

    public String toString() {
        return this.b.getPath();
    }
}
