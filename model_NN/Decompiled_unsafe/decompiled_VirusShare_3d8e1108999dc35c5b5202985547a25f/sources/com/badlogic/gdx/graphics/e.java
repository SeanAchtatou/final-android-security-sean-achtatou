package com.badlogic.gdx.graphics;

import java.nio.Buffer;
import java.nio.IntBuffer;

public interface e {
    void glBindTexture(int i, int i2);

    void glBlendFunc(int i, int i2);

    void glClear(int i);

    void glClearColor(float f, float f2, float f3, float f4);

    void glDeleteTextures(int i, IntBuffer intBuffer);

    void glDepthFunc(int i);

    void glDepthMask(boolean z);

    void glDisable(int i);

    void glDrawArrays(int i, int i2, int i3);

    void glDrawElements(int i, int i2, int i3, Buffer buffer);

    void glEnable(int i);

    void glGenTextures(int i, IntBuffer intBuffer);

    void glLineWidth(float f);

    void glTexImage2D(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, Buffer buffer);

    void glTexParameterf(int i, int i2, float f);
}
