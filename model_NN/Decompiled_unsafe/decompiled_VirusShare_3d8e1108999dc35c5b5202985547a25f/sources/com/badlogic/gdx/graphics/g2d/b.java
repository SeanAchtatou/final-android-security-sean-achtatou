package com.badlogic.gdx.graphics.g2d;

public final class b {
    com.badlogic.gdx.graphics.b a;
    float b;
    float c;
    float d;
    float e;

    public b() {
    }

    public b(com.badlogic.gdx.graphics.b bVar) {
        this.a = bVar;
        a(0, bVar.b(), bVar.c());
    }

    public b(com.badlogic.gdx.graphics.b bVar, int i, int i2) {
        this.a = bVar;
        a(i, i2, 32);
    }

    private void a(int i, int i2, int i3) {
        float b2 = 1.0f / ((float) this.a.b());
        float c2 = 1.0f / ((float) this.a.c());
        float f = ((float) i) * b2;
        this.b = f;
        this.c = 0.0f * c2;
        this.d = b2 * ((float) (i + i2));
        this.e = c2 * ((float) (i3 + 0));
    }
}
