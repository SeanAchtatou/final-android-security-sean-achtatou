package com.badlogic.gdx.graphics.a;

import com.badlogic.gdx.g;
import com.badlogic.gdx.graphics.m;
import com.badlogic.gdx.graphics.n;
import com.badlogic.gdx.physics.box2d.Transform;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.f;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public final class i implements g {
    private m a;
    private FloatBuffer b;
    private ByteBuffer c;
    private boolean d;

    public i(int i, n... nVarArr) {
        this(i, new m(nVarArr));
    }

    private i(int i, m mVar) {
        this.d = false;
        this.a = mVar;
        this.c = ByteBuffer.allocateDirect(this.a.a * i);
        this.c.order(ByteOrder.nativeOrder());
        this.b = this.c.asFloatBuffer();
        this.b.flip();
        this.c.flip();
    }

    public final void e() {
    }

    public final int a() {
        return (this.b.limit() * 4) / this.a.a;
    }

    public final void a(float[] fArr, int i) {
        BufferUtils.a(fArr, this.c, i);
        this.b.position(0);
        this.b.limit(i);
    }

    public final void b() {
        int i;
        com.badlogic.gdx.graphics.i iVar = g.g;
        int a2 = this.a.a();
        this.c.limit(this.b.limit() * 4);
        int i2 = 0;
        int i3 = 0;
        while (i2 < a2) {
            n a3 = this.a.a(i2);
            switch (a3.a) {
                case Transform.POS_X /*0*/:
                    this.c.position(a3.c);
                    iVar.c(32884);
                    iVar.b(a3.b, this.a.a, this.c);
                    i = i3;
                    break;
                case 1:
                case 5:
                    int i4 = 5126;
                    if (a3.a == 5) {
                        i4 = 5121;
                    }
                    this.c.position(a3.c);
                    iVar.c(32886);
                    iVar.a(a3.b, i4, this.a.a, this.c);
                    i = i3;
                    break;
                case 2:
                    this.c.position(a3.c);
                    iVar.c(32885);
                    iVar.a(this.a.a, this.c);
                    i = i3;
                    break;
                case 3:
                    iVar.a(33984 + i3);
                    iVar.c(32888);
                    this.c.position(a3.c);
                    iVar.a(a3.b, this.a.a, this.c);
                    i = i3 + 1;
                    break;
                case 4:
                default:
                    throw new f("unkown vertex attribute type: " + a3.a);
            }
            i2++;
            i3 = i;
        }
        this.d = true;
    }

    public final void c() {
        com.badlogic.gdx.graphics.i iVar = g.g;
        int a2 = this.a.a();
        int i = 0;
        for (int i2 = 0; i2 < a2; i2++) {
            n a3 = this.a.a(i2);
            switch (a3.a) {
                case Transform.POS_X /*0*/:
                    break;
                case 1:
                case 5:
                    iVar.b(32886);
                    break;
                case 2:
                    iVar.b(32885);
                    break;
                case 3:
                    iVar.a(33984 + i);
                    iVar.b(32888);
                    i++;
                    break;
                case 4:
                default:
                    throw new f("unkown vertex attribute type: " + a3.a);
            }
        }
        this.c.position(0);
        this.d = false;
    }
}
