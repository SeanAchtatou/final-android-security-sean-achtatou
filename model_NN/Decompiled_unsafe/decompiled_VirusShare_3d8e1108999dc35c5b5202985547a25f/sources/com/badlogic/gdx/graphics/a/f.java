package com.badlogic.gdx.graphics.a;

import com.badlogic.gdx.g;
import com.badlogic.gdx.graphics.b;
import com.badlogic.gdx.graphics.c;
import com.badlogic.gdx.graphics.h;
import com.badlogic.gdx.utils.c;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

public final class f implements c {
    private static final Map<com.badlogic.gdx.c, List<f>> a = new HashMap();
    private b b;
    private int c;
    private int d;
    private final int e;
    private final int f;
    private final c.a g;

    public static void a(com.badlogic.gdx.c cVar) {
        List list;
        if (g.b.c() != null && (list = a.get(cVar)) != null) {
            for (int i = 0; i < list.size(); i++) {
                f fVar = (f) list.get(i);
                fVar.b = new b(fVar.e, fVar.f, fVar.g);
                fVar.b.a(b.C0007b.Linear, b.C0007b.Linear);
                fVar.b.a(b.a.ClampToEdge, b.a.ClampToEdge);
                h c2 = g.b.c();
                ByteBuffer allocateDirect = ByteBuffer.allocateDirect(4);
                allocateDirect.order(ByteOrder.nativeOrder());
                IntBuffer asIntBuffer = allocateDirect.asIntBuffer();
                c2.glGenFramebuffers(1, asIntBuffer);
                fVar.c = asIntBuffer.get(0);
                c2.glGenRenderbuffers(1, asIntBuffer);
                fVar.d = asIntBuffer.get(0);
                c2.glBindTexture(3553, fVar.b.e());
                c2.glBindRenderbuffer(36161, fVar.d);
                c2.glRenderbufferStorage(36161, 33189, fVar.b.b(), fVar.b.c());
                c2.glBindFramebuffer(36160, fVar.c);
                c2.glFramebufferTexture2D(36160, 36064, 3553, fVar.b.e(), 0);
                c2.glFramebufferRenderbuffer(36160, 36096, 36161, fVar.d);
                int glCheckFramebufferStatus = c2.glCheckFramebufferStatus(36160);
                c2.glBindRenderbuffer(36161, 0);
                c2.glBindTexture(3553, 0);
                c2.glBindFramebuffer(36160, 0);
                if (glCheckFramebufferStatus != 36053) {
                    fVar.b.d();
                    asIntBuffer.put(fVar.d);
                    asIntBuffer.flip();
                    c2.glDeleteRenderbuffers(1, asIntBuffer);
                    asIntBuffer.put(fVar.c);
                    asIntBuffer.flip();
                    c2.glDeleteFramebuffers(1, asIntBuffer);
                    if (glCheckFramebufferStatus == 36054) {
                        throw new IllegalStateException("frame buffer couldn't be constructed: incomplete attachment");
                    } else if (glCheckFramebufferStatus == 36057) {
                        throw new IllegalStateException("frame buffer couldn't be constructed: incomplete dimensions");
                    } else if (glCheckFramebufferStatus == 36055) {
                        throw new IllegalStateException("frame buffer couldn't be constructed: missing attachment");
                    }
                }
            }
        }
    }

    public static void b(com.badlogic.gdx.c cVar) {
        a.remove(cVar);
    }

    public static String a() {
        StringBuilder sb = new StringBuilder();
        sb.append("Managed buffers/app: { ");
        for (com.badlogic.gdx.c cVar : a.keySet()) {
            sb.append(a.get(cVar).size());
            sb.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
        }
        sb.append("}");
        return sb.toString();
    }
}
