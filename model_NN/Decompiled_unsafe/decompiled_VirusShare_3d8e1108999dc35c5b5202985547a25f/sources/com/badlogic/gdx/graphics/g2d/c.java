package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.graphics.a.e;
import com.badlogic.gdx.graphics.b;
import com.badlogic.gdx.graphics.f;
import com.badlogic.gdx.graphics.g;
import com.badlogic.gdx.graphics.h;
import com.badlogic.gdx.graphics.i;
import com.badlogic.gdx.graphics.n;
import com.badlogic.gdx.math.Matrix4;

public final class c implements com.badlogic.gdx.utils.c {
    private f a;
    private f[] b;
    private b c;
    private float d;
    private float e;
    private int f;
    private int g;
    private final float[] h;
    private final Matrix4 i;
    private final Matrix4 j;
    private final Matrix4 k;
    private boolean l;
    private boolean m;
    private int n;
    private int o;
    private e p;
    private float q;
    private g r;
    private int s;
    private int t;
    private e u;

    public c(byte b2) {
        this();
    }

    private c() {
        this.c = null;
        this.d = 0.0f;
        this.e = 0.0f;
        this.f = 0;
        this.g = 0;
        this.i = new Matrix4();
        this.j = new Matrix4();
        this.k = new Matrix4();
        this.l = false;
        this.m = false;
        this.n = 770;
        this.o = 771;
        this.q = g.a.a();
        this.r = new g(1.0f, 1.0f, 1.0f);
        this.s = 0;
        this.t = 0;
        this.u = null;
        this.b = new f[1];
        this.b[0] = new f(f.a.VertexArray, new n(0, 2, "a_position"), new n(5, 4, "a_color"), new n(3, 2, "a_texCoords"));
        this.j.a((float) com.badlogic.gdx.g.b.d(), (float) com.badlogic.gdx.g.b.e());
        this.h = new float[20000];
        short[] sArr = new short[6000];
        int i2 = 0;
        short s2 = 0;
        while (i2 < 6000) {
            sArr[i2 + 0] = (short) (s2 + 0);
            sArr[i2 + 1] = (short) (s2 + 1);
            sArr[i2 + 2] = (short) (s2 + 2);
            sArr[i2 + 3] = (short) (s2 + 2);
            sArr[i2 + 4] = (short) (s2 + 3);
            sArr[i2 + 5] = (short) (s2 + 0);
            i2 += 6;
            s2 = (short) (s2 + 4);
        }
        this.b[0].a(sArr);
        this.a = this.b[0];
        if (com.badlogic.gdx.g.b.a()) {
            this.p = new e("attribute vec4 a_position;\nattribute vec4 a_color;\nattribute vec2 a_texCoords;\nuniform mat4 u_projectionViewMatrix;\nvarying vec4 v_color;\nvarying vec2 v_texCoords;\n\nvoid main()\n{\n   v_color = a_color;\n   v_texCoords = a_texCoords;\n   gl_Position =  u_projectionViewMatrix * a_position;\n}\n", "#ifdef GL_ES\nprecision mediump float;\n#endif\nvarying vec4 v_color;\nvarying vec2 v_texCoords;\nuniform sampler2D u_texture;\nvoid main()\n{\n  gl_FragColor = v_color * texture2D(u_texture, v_texCoords);\n}");
            if (!this.p.b()) {
                throw new IllegalArgumentException("couldn't compile shader: " + this.p.a());
            }
        }
    }

    public final void a() {
        if (this.l) {
            throw new IllegalStateException("you have to call SpriteBatch.end() first");
        }
        this.s = 0;
        if (!com.badlogic.gdx.g.b.a()) {
            i iVar = com.badlogic.gdx.g.g;
            iVar.glDepthMask(false);
            iVar.glEnable(3553);
            iVar.d(5889);
            iVar.a(this.j.a);
            iVar.d(5888);
            iVar.a(this.i.a);
        } else {
            this.k.a(this.j).b(this.i);
            h hVar = com.badlogic.gdx.g.i;
            hVar.glDepthMask(false);
            hVar.glEnable(3553);
            if (this.u != null) {
                this.u.c();
                this.u.a("u_proj", this.j);
                this.u.a("u_trans", this.i);
                this.u.a("u_projTrans", this.k);
                this.u.a("u_texture");
            } else {
                this.p.c();
                this.p.a("u_projectionViewMatrix", this.k);
                this.p.a("u_texture");
            }
        }
        this.f = 0;
        this.c = null;
        this.l = true;
    }

    public final void b() {
        boolean z = true;
        if (!this.l) {
            throw new IllegalStateException("SpriteBatch.begin must be called before end.");
        }
        if (this.f > 0) {
            e();
        }
        this.c = null;
        this.f = 0;
        this.l = false;
        com.badlogic.gdx.graphics.e eVar = com.badlogic.gdx.g.f;
        eVar.glDepthMask(true);
        if (this.m) {
            z = false;
        }
        if (z) {
            eVar.glDisable(3042);
        }
        eVar.glDisable(3553);
        if (com.badlogic.gdx.g.b.a()) {
            e.d();
        }
    }

    public final void c() {
        this.q = Float.intBitsToFloat(-16777217);
    }

    public final void a(b bVar, float f2, float f3) {
        float abs = (float) Math.abs((int) ((bVar.d - bVar.b) * ((float) bVar.a.b())));
        float abs2 = (float) Math.abs((int) ((bVar.e - bVar.c) * ((float) bVar.a.c())));
        if (!this.l) {
            throw new IllegalStateException("SpriteBatch.begin must be called before draw.");
        }
        b bVar2 = bVar.a;
        if (bVar2 != this.c) {
            e();
            this.c = bVar2;
            this.d = 1.0f / ((float) bVar2.b());
            this.e = 1.0f / ((float) bVar2.c());
        } else if (this.f == this.h.length) {
            e();
        }
        float f4 = abs + f2;
        float f5 = abs2 + f3;
        float f6 = bVar.b;
        float f7 = bVar.e;
        float f8 = bVar.d;
        float f9 = bVar.c;
        float[] fArr = this.h;
        int i2 = this.f;
        this.f = i2 + 1;
        fArr[i2] = f2;
        float[] fArr2 = this.h;
        int i3 = this.f;
        this.f = i3 + 1;
        fArr2[i3] = f3;
        float[] fArr3 = this.h;
        int i4 = this.f;
        this.f = i4 + 1;
        fArr3[i4] = this.q;
        float[] fArr4 = this.h;
        int i5 = this.f;
        this.f = i5 + 1;
        fArr4[i5] = f6;
        float[] fArr5 = this.h;
        int i6 = this.f;
        this.f = i6 + 1;
        fArr5[i6] = f7;
        float[] fArr6 = this.h;
        int i7 = this.f;
        this.f = i7 + 1;
        fArr6[i7] = f2;
        float[] fArr7 = this.h;
        int i8 = this.f;
        this.f = i8 + 1;
        fArr7[i8] = f5;
        float[] fArr8 = this.h;
        int i9 = this.f;
        this.f = i9 + 1;
        fArr8[i9] = this.q;
        float[] fArr9 = this.h;
        int i10 = this.f;
        this.f = i10 + 1;
        fArr9[i10] = f6;
        float[] fArr10 = this.h;
        int i11 = this.f;
        this.f = i11 + 1;
        fArr10[i11] = f9;
        float[] fArr11 = this.h;
        int i12 = this.f;
        this.f = i12 + 1;
        fArr11[i12] = f4;
        float[] fArr12 = this.h;
        int i13 = this.f;
        this.f = i13 + 1;
        fArr12[i13] = f5;
        float[] fArr13 = this.h;
        int i14 = this.f;
        this.f = i14 + 1;
        fArr13[i14] = this.q;
        float[] fArr14 = this.h;
        int i15 = this.f;
        this.f = i15 + 1;
        fArr14[i15] = f8;
        float[] fArr15 = this.h;
        int i16 = this.f;
        this.f = i16 + 1;
        fArr15[i16] = f9;
        float[] fArr16 = this.h;
        int i17 = this.f;
        this.f = i17 + 1;
        fArr16[i17] = f4;
        float[] fArr17 = this.h;
        int i18 = this.f;
        this.f = i18 + 1;
        fArr17[i18] = f3;
        float[] fArr18 = this.h;
        int i19 = this.f;
        this.f = i19 + 1;
        fArr18[i19] = this.q;
        float[] fArr19 = this.h;
        int i20 = this.f;
        this.f = i20 + 1;
        fArr19[i20] = f8;
        float[] fArr20 = this.h;
        int i21 = this.f;
        this.f = i21 + 1;
        fArr20[i21] = f7;
    }

    private void e() {
        if (this.f != 0) {
            this.s++;
            int i2 = this.f / 20;
            if (i2 > this.t) {
                this.t = i2;
            }
            this.c.a();
            this.a.a(this.h, this.f);
            if (com.badlogic.gdx.g.b.a()) {
                if (this.m) {
                    com.badlogic.gdx.g.i.glDisable(3042);
                } else {
                    h hVar = com.badlogic.gdx.g.i;
                    hVar.glEnable(3042);
                    hVar.glBlendFunc(this.n, this.o);
                }
                if (this.u != null) {
                    this.a.a(this.u, i2 * 6);
                } else {
                    this.a.a(this.p, i2 * 6);
                }
            } else {
                if (this.m) {
                    com.badlogic.gdx.g.g.glDisable(3042);
                } else {
                    i iVar = com.badlogic.gdx.g.g;
                    iVar.glEnable(3042);
                    iVar.glBlendFunc(this.n, this.o);
                }
                this.a.a(4, 0, i2 * 6);
            }
            this.f = 0;
            this.g++;
            if (this.g == this.b.length) {
                this.g = 0;
            }
            this.a = this.b[this.g];
        }
    }

    public final void d() {
        for (f a2 : this.b) {
            a2.a();
        }
        if (this.p != null) {
            this.p.e();
        }
    }
}
