package com.badlogic.gdx.graphics;

import java.nio.Buffer;

public interface i extends e {
    void a();

    void a(float f);

    void a(float f, float f2);

    void a(float f, float f2, float f3);

    void a(float f, float f2, float f3, float f4);

    void a(int i);

    void a(int i, int i2, int i3, Buffer buffer);

    void a(int i, int i2, Buffer buffer);

    void a(int i, Buffer buffer);

    void a(float[] fArr);

    void b();

    void b(float f);

    void b(int i);

    void b(int i, int i2, Buffer buffer);

    void b(float[] fArr);

    void c(int i);

    void d(int i);
}
