package com.badlogic.gdx.graphics.a;

import com.badlogic.gdx.g;
import com.badlogic.gdx.graphics.i;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.f;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public final class c {
    private int a;
    private float[] b;
    private FloatBuffer c;
    private float[] d;
    private FloatBuffer e;
    private float[] f;
    private FloatBuffer g;
    private float[] h;
    private FloatBuffer i;
    private int j;
    private int k;
    private int l;
    private int m;
    private boolean n;
    private boolean o;
    private boolean p;

    public c(byte b2) {
        this();
    }

    private c() {
        this.j = 0;
        this.k = 0;
        this.l = 0;
        this.m = 0;
        if (g.b.a()) {
            throw new f("ImmediateModeRenderer can only be used with OpenGL ES 1.0/1.1");
        }
        this.b = new float[6000];
        this.c = b(6000);
        this.d = new float[8000];
        this.e = b(8000);
        this.f = new float[6000];
        this.g = b(6000);
        this.h = new float[4000];
        this.i = b(4000);
    }

    private static FloatBuffer b(int i2) {
        ByteBuffer allocateDirect = ByteBuffer.allocateDirect(i2 * 4);
        allocateDirect.order(ByteOrder.nativeOrder());
        return allocateDirect.asFloatBuffer();
    }

    public final void a(int i2) {
        this.a = i2;
        this.j = 0;
        this.k = 0;
        this.l = 0;
        this.m = 0;
        this.n = false;
        this.o = false;
        this.p = false;
    }

    public final void a(float f2, float f3, float f4, float f5) {
        this.d[this.k] = f2;
        this.d[this.k + 1] = f3;
        this.d[this.k + 2] = f4;
        this.d[this.k + 3] = f5;
        this.n = true;
    }

    public final void a(float f2, float f3) {
        float[] fArr = this.b;
        int i2 = this.j;
        this.j = i2 + 1;
        fArr[i2] = f2;
        float[] fArr2 = this.b;
        int i3 = this.j;
        this.j = i3 + 1;
        fArr2[i3] = f3;
        float[] fArr3 = this.b;
        int i4 = this.j;
        this.j = i4 + 1;
        fArr3[i4] = 0.0f;
        if (this.n) {
            this.k += 4;
        }
        if (this.o) {
            this.l += 3;
        }
        if (this.p) {
            this.m += 2;
        }
    }

    public final void a() {
        if (this.j != 0) {
            i iVar = g.g;
            iVar.c(32884);
            this.c.clear();
            BufferUtils.a(this.b, this.c, this.j);
            iVar.b(3, 0, this.c);
            if (this.n) {
                iVar.c(32886);
                this.e.clear();
                BufferUtils.a(this.d, this.e, this.k);
                iVar.a(4, 5126, 0, this.e);
            }
            if (this.o) {
                iVar.c(32885);
                this.g.clear();
                BufferUtils.a(this.f, this.g, this.l);
                iVar.a(0, this.g);
            }
            if (this.p) {
                iVar.a(33984);
                iVar.c(32888);
                this.i.clear();
                BufferUtils.a(this.h, this.i, this.m);
                iVar.a(2, 0, this.i);
            }
            iVar.glDrawArrays(this.a, 0, this.j / 3);
            if (this.n) {
                iVar.b(32886);
            }
            if (this.o) {
                iVar.b(32885);
            }
            if (this.p) {
                iVar.b(32888);
            }
        }
    }
}
