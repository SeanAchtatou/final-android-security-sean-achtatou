package com.badlogic.gdx.graphics;

import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

public interface h extends e {
    void glAttachShader(int i, int i2);

    void glBindBuffer(int i, int i2);

    void glBindFramebuffer(int i, int i2);

    void glBindRenderbuffer(int i, int i2);

    void glBufferData(int i, int i2, Buffer buffer, int i3);

    void glBufferSubData(int i, int i2, int i3, Buffer buffer);

    int glCheckFramebufferStatus(int i);

    void glCompileShader(int i);

    int glCreateProgram();

    int glCreateShader(int i);

    void glDeleteBuffers(int i, IntBuffer intBuffer);

    void glDeleteFramebuffers(int i, IntBuffer intBuffer);

    void glDeleteProgram(int i);

    void glDeleteRenderbuffers(int i, IntBuffer intBuffer);

    void glDeleteShader(int i);

    void glDisableVertexAttribArray(int i);

    void glDrawElements(int i, int i2, int i3, int i4);

    void glEnableVertexAttribArray(int i);

    void glFramebufferRenderbuffer(int i, int i2, int i3, int i4);

    void glFramebufferTexture2D(int i, int i2, int i3, int i4, int i5);

    void glGenBuffers(int i, IntBuffer intBuffer);

    void glGenFramebuffers(int i, IntBuffer intBuffer);

    void glGenRenderbuffers(int i, IntBuffer intBuffer);

    int glGetAttribLocation(int i, String str);

    String glGetProgramInfoLog(int i);

    void glGetProgramiv(int i, int i2, IntBuffer intBuffer);

    String glGetShaderInfoLog(int i);

    void glGetShaderiv(int i, int i2, IntBuffer intBuffer);

    int glGetUniformLocation(int i, String str);

    void glLinkProgram(int i);

    void glRenderbufferStorage(int i, int i2, int i3, int i4);

    void glShaderSource(int i, String str);

    void glUniform1i(int i, int i2);

    void glUniformMatrix4fv(int i, int i2, boolean z, FloatBuffer floatBuffer);

    void glUseProgram(int i);

    void glVertexAttribPointer(int i, int i2, int i3, boolean z, int i4, int i5);
}
