package com.badlogic.gdx.graphics.a;

import com.badlogic.gdx.g;
import com.badlogic.gdx.graphics.h;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.f;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

public final class j implements d {
    private static IntBuffer a = BufferUtils.a();
    private ShortBuffer b;
    private ByteBuffer c = ByteBuffer.allocateDirect(12000);
    private int d;
    private boolean e;
    private boolean f = true;
    private boolean g = false;
    private int h;

    public j() {
        this.c.order(ByteOrder.nativeOrder());
        this.e = true;
        this.h = 35048;
        this.b = this.c.asShortBuffer();
        this.b.flip();
        this.c.flip();
        this.d = h();
    }

    private int h() {
        if (g.i != null) {
            g.i.glGenBuffers(1, a);
            g.i.glBindBuffer(34963, a.get(0));
            g.i.glBufferData(34963, this.c.capacity(), null, this.h);
            g.i.glBindBuffer(34963, 0);
            return a.get(0);
        } else if (g.h == null) {
            return 0;
        } else {
            g.h.b(a);
            g.h.a(34963, a.get(0));
            g.h.a(34963, this.c.capacity(), (Buffer) null, this.h);
            g.h.a(34963, 0);
            return a.get(0);
        }
    }

    public final int a() {
        return this.b.limit();
    }

    public final int b() {
        return this.b.capacity();
    }

    public final void a(short[] sArr, int i) {
        this.f = true;
        this.b.clear();
        this.b.put(sArr);
        this.b.flip();
        this.c.position(0);
        this.c.limit(i << 1);
        if (this.g) {
            if (g.h != null) {
                g.h.c(34963, this.c.limit(), this.c);
            } else if (g.h != null) {
                g.i.glBufferSubData(34963, 0, this.c.limit(), this.c);
            }
            this.f = false;
        }
    }

    public final ShortBuffer c() {
        this.f = true;
        return this.b;
    }

    public final void d() {
        if (this.d == 0) {
            throw new f("buuh");
        }
        if (g.h != null) {
            com.badlogic.gdx.graphics.j jVar = g.h;
            jVar.a(34963, this.d);
            if (this.f) {
                this.c.limit(this.b.limit() * 2);
                jVar.c(34963, this.c.limit(), this.c);
                this.f = false;
            }
        } else {
            h hVar = g.i;
            hVar.glBindBuffer(34963, this.d);
            if (this.f) {
                this.c.limit(this.b.limit() * 2);
                hVar.glBufferSubData(34963, 0, this.c.limit(), this.c);
                this.f = false;
            }
        }
        this.g = true;
    }

    public final void e() {
        if (g.h != null) {
            g.h.a(34963, 0);
        } else if (g.i != null) {
            g.i.glBindBuffer(34963, 0);
        }
        this.g = false;
    }

    public final void f() {
        this.d = h();
        this.f = true;
    }

    public final void g() {
        if (g.i != null) {
            a.clear();
            a.put(this.d);
            a.flip();
            h hVar = g.i;
            hVar.glBindBuffer(34963, 0);
            hVar.glDeleteBuffers(1, a);
            this.d = 0;
        } else if (g.h != null) {
            a.clear();
            a.put(this.d);
            a.flip();
            com.badlogic.gdx.graphics.j jVar = g.h;
            jVar.a(34963, 0);
            jVar.a(a);
            this.d = 0;
        }
    }
}
