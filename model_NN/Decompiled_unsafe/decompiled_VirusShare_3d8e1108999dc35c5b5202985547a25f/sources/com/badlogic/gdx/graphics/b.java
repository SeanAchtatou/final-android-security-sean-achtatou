package com.badlogic.gdx.graphics;

import com.badlogic.gdx.g;
import com.badlogic.gdx.graphics.c;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.c;
import com.badlogic.gdx.utils.f;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

public class b implements c {
    private static boolean a = true;
    private static IntBuffer b = BufferUtils.a();
    private static Map<com.badlogic.gdx.c, List<b>> c = new HashMap();
    private int d;
    private int e;
    private boolean f;
    private boolean g;
    private int h;
    private com.badlogic.gdx.c.a i;
    private l j;
    private C0007b k;
    private C0007b l;
    private a m;
    private a n;
    private c.a o;

    public enum a {
        ClampToEdge,
        Repeat
    }

    /* renamed from: com.badlogic.gdx.graphics.b$b  reason: collision with other inner class name */
    public enum C0007b {
        Nearest,
        Linear,
        MipMap,
        MipMapNearestNearest,
        MipMapLinearNearest,
        MipMapNearestLinear,
        MipMapLinearLinear
    }

    public b(String str) {
        this(g.e.b(str), (byte) 0);
    }

    public b(com.badlogic.gdx.c.a aVar, byte b2) {
        this(aVar);
    }

    public b(com.badlogic.gdx.c.a aVar) {
        this.k = C0007b.Nearest;
        this.l = C0007b.Nearest;
        this.m = a.ClampToEdge;
        this.n = a.ClampToEdge;
        this.g = true;
        this.f = false;
        this.i = aVar;
        this.j = null;
        this.h = g();
        c cVar = new c(aVar);
        this.o = cVar.j();
        a(cVar);
        cVar.e();
        a(this.k, this.l);
        a(this.m, this.n);
        a(g.a, this);
    }

    public b(com.badlogic.gdx.c.a aVar, c.a aVar2) {
        this.k = C0007b.Nearest;
        this.l = C0007b.Nearest;
        this.m = a.ClampToEdge;
        this.n = a.ClampToEdge;
        this.g = true;
        this.f = false;
        this.i = aVar;
        this.j = null;
        this.h = g();
        c cVar = new c(aVar);
        this.o = aVar2;
        a(cVar);
        cVar.e();
        a(this.k, this.l);
        a(this.m, this.n);
        a(g.a, this);
    }

    public b(c cVar) {
        this.k = C0007b.Nearest;
        this.l = C0007b.Nearest;
        this.m = a.ClampToEdge;
        this.n = a.ClampToEdge;
        this.g = false;
        this.f = false;
        this.i = null;
        this.j = null;
        this.h = g();
        this.o = cVar.j();
        a(cVar);
        a(this.k, this.l);
        a(this.m, this.n);
    }

    public b(int i2, int i3, c.a aVar) {
        this.k = C0007b.Nearest;
        this.l = C0007b.Nearest;
        this.m = a.ClampToEdge;
        this.n = a.ClampToEdge;
        this.g = false;
        this.f = false;
        this.i = null;
        this.j = null;
        this.h = g();
        c cVar = new c(i2, i3, aVar);
        cVar.a();
        cVar.b();
        this.o = cVar.j();
        a(cVar);
        cVar.e();
        a(this.k, this.l);
        a(this.m, this.n);
    }

    private static int g() {
        b.position(0);
        b.limit(b.capacity());
        g.f.glGenTextures(1, b);
        return b.get(0);
    }

    private void a(c cVar) {
        boolean z;
        if (this.o != cVar.j()) {
            c cVar2 = new c(cVar.c(), cVar.d(), this.o);
            c.b k2 = c.k();
            c.a(c.b.None);
            cVar2.a(cVar, cVar.c(), cVar.d());
            c.a(k2);
            z = true;
            cVar = cVar2;
        } else {
            z = false;
        }
        this.d = cVar.c();
        this.e = cVar.d();
        if (!a || g.i != null || (com.badlogic.gdx.math.b.b(this.d) && com.badlogic.gdx.math.b.b(this.e))) {
            g.f.glBindTexture(3553, this.h);
            g.f.glTexImage2D(3553, 0, cVar.g(), cVar.c(), cVar.d(), 0, cVar.f(), cVar.h(), cVar.i());
            if (this.f) {
                if (g.i == null || this.d == this.e) {
                    int c2 = cVar.c() / 2;
                    int d2 = cVar.d() / 2;
                    int i2 = 1;
                    c cVar3 = cVar;
                    while (c2 > 0 && d2 > 0) {
                        c cVar4 = new c(c2, d2, cVar3.j());
                        cVar4.a(cVar3, cVar3.c(), cVar3.d(), c2, d2);
                        if (i2 > 1 || z) {
                            cVar3.e();
                        }
                        g.f.glTexImage2D(3553, i2, cVar4.g(), cVar4.c(), cVar4.d(), 0, cVar4.f(), cVar4.h(), cVar4.i());
                        c2 = cVar4.c() / 2;
                        d2 = cVar4.d() / 2;
                        i2++;
                        cVar3 = cVar4;
                    }
                    cVar3.e();
                    return;
                }
                throw new f("texture width and height must be square when using mipmapping in OpenGL ES 1.x");
            } else if (z) {
                cVar.e();
            }
        } else {
            throw new f("texture width and height must be powers of two");
        }
    }

    public final void a() {
        g.f.glBindTexture(3553, this.h);
    }

    public final int b() {
        return this.d;
    }

    public final int c() {
        return this.e;
    }

    public void d() {
        b.put(0, this.h);
        g.f.glDeleteTextures(1, b);
        if (this.g && c.get(g.a) != null) {
            c.get(g.a).remove(this);
        }
    }

    public final int e() {
        return this.h;
    }

    public final void a(a aVar, a aVar2) {
        float f2;
        float f3 = 33071.0f;
        this.m = aVar;
        this.n = aVar2;
        a();
        e eVar = g.f;
        if (aVar == a.ClampToEdge) {
            f2 = 33071.0f;
        } else {
            f2 = 10497.0f;
        }
        eVar.glTexParameterf(3553, 10242, f2);
        e eVar2 = g.f;
        if (aVar2 != a.ClampToEdge) {
            f3 = 10497.0f;
        }
        eVar2.glTexParameterf(3553, 10243, f3);
    }

    public final void a(C0007b bVar, C0007b bVar2) {
        this.k = bVar;
        this.l = bVar2;
        a();
        g.f.glTexParameterf(3553, 10241, (float) a(bVar));
        g.f.glTexParameterf(3553, 10240, (float) a(bVar2));
    }

    private static int a(C0007b bVar) {
        if (bVar == C0007b.Linear) {
            return 9729;
        }
        if (bVar == C0007b.Nearest) {
            return 9728;
        }
        if (bVar == C0007b.MipMap) {
            return 9987;
        }
        if (bVar == C0007b.MipMapNearestNearest) {
            return 9984;
        }
        if (bVar == C0007b.MipMapNearestLinear) {
            return 9986;
        }
        if (bVar == C0007b.MipMapLinearNearest) {
            return 9985;
        }
        if (bVar == C0007b.MipMapLinearLinear) {
        }
        return 9987;
    }

    private static void a(com.badlogic.gdx.c cVar, b bVar) {
        Object obj = c.get(cVar);
        if (obj == null) {
            obj = new ArrayList();
        }
        obj.add(bVar);
        c.put(cVar, obj);
    }

    public static void a(com.badlogic.gdx.c cVar) {
        c.remove(cVar);
    }

    public static void b(com.badlogic.gdx.c cVar) {
        List list = c.get(cVar);
        if (list != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < list.size()) {
                    b bVar = (b) list.get(i3);
                    bVar.h = g();
                    bVar.a(bVar.k, bVar.l);
                    bVar.a(bVar.m, bVar.n);
                    if (bVar.i != null) {
                        c cVar2 = new c(bVar.i);
                        bVar.a(cVar2);
                        cVar2.e();
                    }
                    if (bVar.j != null) {
                        g.f.glBindTexture(3553, bVar.h);
                    }
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public static String f() {
        StringBuilder sb = new StringBuilder();
        sb.append("Managed textures/app: { ");
        for (com.badlogic.gdx.c cVar : c.keySet()) {
            sb.append(c.get(cVar).size());
            sb.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
        }
        sb.append("}");
        return sb.toString();
    }
}
