package com.badlogic.gdx.graphics;

import java.nio.Buffer;
import java.nio.IntBuffer;

public interface j extends i {
    void a(int i, int i2);

    void a(int i, int i2, int i3);

    void a(int i, int i2, int i3, int i4);

    void a(int i, int i2, Buffer buffer, int i3);

    void a(IntBuffer intBuffer);

    void b(int i, int i2);

    void b(int i, int i2, int i3);

    void b(IntBuffer intBuffer);

    void c(int i, int i2, int i3);

    void c(int i, int i2, Buffer buffer);
}
