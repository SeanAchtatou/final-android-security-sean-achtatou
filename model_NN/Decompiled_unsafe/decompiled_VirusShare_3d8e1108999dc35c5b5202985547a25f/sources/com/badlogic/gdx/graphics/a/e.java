package com.badlogic.gdx.graphics.a;

import com.badlogic.gdx.g;
import com.badlogic.gdx.graphics.h;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.utils.c;
import com.badlogic.gdx.utils.d;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

public final class e implements c {
    private static boolean a = true;
    private static final Map<com.badlogic.gdx.c, List<e>> b = new HashMap();
    private String c = "";
    private boolean d;
    private final d<String, Integer> e = new d<>((byte) 0);
    private final d<String, Integer> f = new d<>((byte) 0);
    private int g;
    private int h;
    private int i;
    private final FloatBuffer j;
    private final String k;
    private final String l;
    private boolean m;
    private ByteBuffer n = null;
    private FloatBuffer o = null;
    private IntBuffer p = null;

    public e(String str, String str2) {
        if (str == null) {
            throw new IllegalArgumentException("vertex shader must not be null");
        } else if (str2 == null) {
            throw new IllegalArgumentException("fragment shader must not be null");
        } else {
            this.k = str;
            this.l = str2;
            a(str, str2);
            ByteBuffer allocateDirect = ByteBuffer.allocateDirect(64);
            allocateDirect.order(ByteOrder.nativeOrder());
            this.j = allocateDirect.asFloatBuffer();
            com.badlogic.gdx.c cVar = g.a;
            Object obj = b.get(cVar);
            obj = obj == null ? new ArrayList() : obj;
            obj.add(this);
            b.put(cVar, obj);
        }
    }

    private void a(String str, String str2) {
        this.h = a(35633, str);
        this.i = a(35632, str2);
        if (this.h == -1 || this.i == -1) {
            this.d = false;
            return;
        }
        h c2 = g.b.c();
        int glCreateProgram = c2.glCreateProgram();
        if (glCreateProgram == 0) {
            glCreateProgram = -1;
        } else {
            c2.glAttachShader(glCreateProgram, this.h);
            c2.glAttachShader(glCreateProgram, this.i);
            c2.glLinkProgram(glCreateProgram);
            ByteBuffer allocateDirect = ByteBuffer.allocateDirect(4);
            allocateDirect.order(ByteOrder.nativeOrder());
            IntBuffer asIntBuffer = allocateDirect.asIntBuffer();
            c2.glGetProgramiv(glCreateProgram, 35714, asIntBuffer);
            if (asIntBuffer.get(0) == 0) {
                c2.glGetProgramiv(glCreateProgram, 35716, asIntBuffer);
                if (asIntBuffer.get(0) > 1) {
                    this.c += c2.glGetProgramInfoLog(glCreateProgram);
                }
                glCreateProgram = -1;
            }
        }
        this.g = glCreateProgram;
        if (this.g == -1) {
            this.d = false;
        } else {
            this.d = true;
        }
    }

    private int a(int i2, String str) {
        h c2 = g.b.c();
        ByteBuffer allocateDirect = ByteBuffer.allocateDirect(4);
        allocateDirect.order(ByteOrder.nativeOrder());
        IntBuffer asIntBuffer = allocateDirect.asIntBuffer();
        int glCreateShader = c2.glCreateShader(i2);
        if (glCreateShader == 0) {
            return -1;
        }
        c2.glShaderSource(glCreateShader, str);
        c2.glCompileShader(glCreateShader);
        c2.glGetShaderiv(glCreateShader, 35713, asIntBuffer);
        if (asIntBuffer.get(0) != 0) {
            return glCreateShader;
        }
        c2.glGetShaderiv(glCreateShader, 35716, asIntBuffer);
        if (asIntBuffer.get(0) <= 1) {
            return -1;
        }
        this.c += c2.glGetShaderInfoLog(glCreateShader);
        return -1;
    }

    public final String a() {
        return this.c;
    }

    public final boolean b() {
        return this.d;
    }

    private int d(String str) {
        h c2 = g.b.c();
        Integer a2 = this.f.a(str);
        if (a2 == null) {
            a2 = Integer.valueOf(c2.glGetAttribLocation(this.g, str));
            if (a2.intValue() != -1) {
                this.f.a(str, a2);
            }
        }
        return a2.intValue();
    }

    private int e(String str) {
        h c2 = g.b.c();
        Integer a2 = this.e.a(str);
        if (a2 == null) {
            a2 = Integer.valueOf(c2.glGetUniformLocation(this.g, str));
            if (a2.intValue() != -1 || !a) {
                this.e.a(str, a2);
            } else {
                throw new IllegalArgumentException("no uniform with name '" + str + "' in shader");
            }
        }
        return a2.intValue();
    }

    public final void a(String str) {
        h c2 = g.b.c();
        g();
        c2.glUniform1i(e(str), 0);
    }

    public final void a(String str, Matrix4 matrix4) {
        h c2 = g.b.c();
        g();
        int e2 = e(str);
        this.j.put(matrix4.a);
        this.j.position(0);
        c2.glUniformMatrix4fv(e2, 1, false, this.j);
    }

    public final void a(String str, int i2, int i3, boolean z, int i4, int i5) {
        h c2 = g.b.c();
        g();
        int d2 = d(str);
        if (d2 != -1) {
            c2.glVertexAttribPointer(d2, i2, i3, z, i4, i5);
        }
    }

    public final void c() {
        h c2 = g.b.c();
        g();
        c2.glUseProgram(this.g);
    }

    public static void d() {
        g.b.c().glUseProgram(0);
    }

    public final void e() {
        h c2 = g.b.c();
        c2.glUseProgram(0);
        c2.glDeleteShader(this.h);
        c2.glDeleteShader(this.i);
        c2.glDeleteProgram(this.g);
        if (b.get(g.a) != null) {
            b.get(g.a).remove(this);
        }
    }

    public final void b(String str) {
        h c2 = g.b.c();
        g();
        int d2 = d(str);
        if (d2 != -1) {
            c2.glDisableVertexAttribArray(d2);
        }
    }

    public final void c(String str) {
        h c2 = g.b.c();
        g();
        int d2 = d(str);
        if (d2 != -1) {
            c2.glEnableVertexAttribArray(d2);
        }
    }

    private void g() {
        if (this.m) {
            a(this.k, this.l);
            this.m = false;
        }
    }

    public static void a(com.badlogic.gdx.c cVar) {
        List list;
        if (g.b.c() != null && (list = b.get(cVar)) != null) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 < list.size()) {
                    ((e) list.get(i3)).m = true;
                    ((e) list.get(i3)).g();
                    i2 = i3 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public static void b(com.badlogic.gdx.c cVar) {
        b.remove(cVar);
    }

    public static String f() {
        StringBuilder sb = new StringBuilder();
        sb.append("Managed shaders/app: { ");
        for (com.badlogic.gdx.c cVar : b.keySet()) {
            sb.append(b.get(cVar).size());
            sb.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
        }
        sb.append("}");
        return sb.toString();
    }
}
