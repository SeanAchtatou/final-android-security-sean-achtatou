package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.graphics.g;
import com.badlogic.gdx.utils.c;
import com.badlogic.gdx.utils.f;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

public final class a implements c {
    private b a;
    private float b;
    private float c;
    private float d;
    private float e;
    private float f;
    private float g;
    private final b[][] h;
    private float i;
    private float j;
    private final C0008a k;
    private float l;
    private g m;
    private boolean n;

    /* renamed from: com.badlogic.gdx.graphics.g2d.a$a  reason: collision with other inner class name */
    public static class C0008a {
    }

    public a() {
        this(com.badlogic.gdx.g.e.a("com/badlogic/gdx/utils/arial-15.fnt"), com.badlogic.gdx.g.e.a("com/badlogic/gdx/utils/arial-15.png"));
    }

    private a(com.badlogic.gdx.c.a aVar, com.badlogic.gdx.c.a aVar2) {
        this.f = 1.0f;
        this.g = 1.0f;
        this.h = new b[128][];
        this.k = new C0008a();
        this.l = g.a.a();
        this.m = new g(1.0f, 1.0f, 1.0f);
        this.a = new b(new com.badlogic.gdx.graphics.b(aVar2));
        a(aVar, this.a);
    }

    private void a(com.badlogic.gdx.c.a aVar, b bVar) {
        this.n = false;
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(aVar.b()), 512);
        try {
            bufferedReader.readLine();
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                throw new f("Invalid font file: " + aVar);
            }
            String[] split = readLine.split(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, 4);
            if (split.length < 4) {
                throw new f("Invalid font file: " + aVar);
            } else if (!split[1].startsWith("lineHeight=")) {
                throw new f("Invalid font file: " + aVar);
            } else {
                this.b = (float) Integer.parseInt(split[1].substring(11));
                if (!split[2].startsWith("base=")) {
                    throw new f("Invalid font file: " + aVar);
                }
                int parseInt = Integer.parseInt(split[2].substring(5));
                if (bVar != null) {
                    bufferedReader.readLine();
                } else {
                    String readLine2 = bufferedReader.readLine();
                    if (readLine2 == null) {
                        throw new f("Invalid font file: " + aVar);
                    }
                    String[] split2 = readLine2.split(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR, 4);
                    if (!split2[2].startsWith("file=")) {
                        throw new f("Invalid font file: " + aVar);
                    }
                    bVar = new b(new com.badlogic.gdx.graphics.b(aVar.a().a(split2[2].substring(6, split2[2].length() - 1))));
                }
                this.a = bVar;
                float b2 = 1.0f / ((float) bVar.a.b());
                float c2 = 1.0f / ((float) bVar.a.c());
                float f2 = bVar.b;
                float f3 = bVar.c;
                while (true) {
                    String readLine3 = bufferedReader.readLine();
                    if (readLine3 != null && !readLine3.startsWith("kernings ")) {
                        if (readLine3.startsWith("char ")) {
                            b bVar2 = new b();
                            StringTokenizer stringTokenizer = new StringTokenizer(readLine3, " =");
                            stringTokenizer.nextToken();
                            stringTokenizer.nextToken();
                            int parseInt2 = Integer.parseInt(stringTokenizer.nextToken());
                            if (parseInt2 <= 65535) {
                                b[] bVarArr = this.h[parseInt2 / 512];
                                if (bVarArr == null) {
                                    bVarArr = new b[512];
                                    this.h[parseInt2 / 512] = bVarArr;
                                }
                                bVarArr[parseInt2 & 511] = bVar2;
                                stringTokenizer.nextToken();
                                int parseInt3 = Integer.parseInt(stringTokenizer.nextToken());
                                stringTokenizer.nextToken();
                                int parseInt4 = Integer.parseInt(stringTokenizer.nextToken());
                                stringTokenizer.nextToken();
                                bVar2.a = Integer.parseInt(stringTokenizer.nextToken());
                                stringTokenizer.nextToken();
                                bVar2.b = Integer.parseInt(stringTokenizer.nextToken());
                                stringTokenizer.nextToken();
                                bVar2.g = Integer.parseInt(stringTokenizer.nextToken());
                                stringTokenizer.nextToken();
                                bVar2.h = -(bVar2.b + Integer.parseInt(stringTokenizer.nextToken()));
                                stringTokenizer.nextToken();
                                bVar2.i = Integer.parseInt(stringTokenizer.nextToken());
                                bVar2.c = (((float) parseInt3) * b2) + f2;
                                bVar2.e = (((float) (parseInt3 + bVar2.a)) * b2) + f2;
                                bVar2.f = (((float) parseInt4) * c2) + f3;
                                bVar2.d = (((float) (bVar2.b + parseInt4)) * c2) + f3;
                            }
                        }
                    }
                }
                while (true) {
                    String readLine4 = bufferedReader.readLine();
                    if (readLine4 == null || !readLine4.startsWith("kerning ")) {
                        b a2 = a(' ');
                    } else {
                        StringTokenizer stringTokenizer2 = new StringTokenizer(readLine4, " =");
                        stringTokenizer2.nextToken();
                        stringTokenizer2.nextToken();
                        int parseInt5 = Integer.parseInt(stringTokenizer2.nextToken());
                        stringTokenizer2.nextToken();
                        int parseInt6 = Integer.parseInt(stringTokenizer2.nextToken());
                        if (parseInt5 >= 0 && parseInt5 <= 65535 && parseInt6 >= 0 && parseInt6 <= 65535) {
                            b a3 = a((char) parseInt5);
                            stringTokenizer2.nextToken();
                            int parseInt7 = Integer.parseInt(stringTokenizer2.nextToken());
                            if (a3.j == null) {
                                a3.j = new byte[128][];
                            }
                            byte[] bArr = a3.j[parseInt6 >>> 9];
                            if (bArr == null) {
                                bArr = new byte[512];
                                a3.j[parseInt6 >>> 9] = bArr;
                            }
                            bArr[parseInt6 & 511] = (byte) parseInt7;
                        }
                    }
                }
                b a22 = a(' ');
                this.i = a22 != null ? (float) (a22.a + a22.i) : 1.0f;
                b a4 = a('x');
                this.j = a4 != null ? (float) a4.b : 1.0f;
                b a5 = a('M');
                this.c = a5 != null ? (float) a5.b : 1.0f;
                this.d = ((float) parseInt) - this.c;
                this.e = -this.b;
                try {
                    bufferedReader.close();
                } catch (IOException e2) {
                }
            }
        } catch (Exception e3) {
            throw new f("Error loading font file: " + aVar, e3);
        } catch (Throwable th) {
            try {
                bufferedReader.close();
            } catch (IOException e4) {
            }
            throw th;
        }
    }

    private b a(char c2) {
        b[] bVarArr = this.h[c2 / 512];
        if (bVarArr != null) {
            return bVarArr[c2 & 511];
        }
        return null;
    }

    static class b {
        int a;
        int b;
        float c;
        float d;
        float e;
        float f;
        int g;
        int h;
        int i;
        byte[][] j;

        b() {
        }
    }
}
