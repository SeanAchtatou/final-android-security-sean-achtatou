package com.badlogic.gdx.graphics.a;

import com.badlogic.gdx.g;
import com.badlogic.gdx.graphics.h;
import com.badlogic.gdx.graphics.j;
import com.badlogic.gdx.graphics.m;
import com.badlogic.gdx.graphics.n;
import com.badlogic.gdx.physics.box2d.Transform;
import com.badlogic.gdx.utils.BufferUtils;
import com.badlogic.gdx.utils.f;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;

public final class b implements g {
    private static IntBuffer a = BufferUtils.a();
    private m b;
    private FloatBuffer c;
    private ByteBuffer d;
    private int e;
    private boolean f;
    private boolean g = false;
    private int h;
    private boolean i = false;
    private boolean j = false;

    public b(n... nVarArr) {
        this.b = new m(nVarArr);
        this.d = ByteBuffer.allocateDirect(this.b.a * 4000);
        this.d.order(ByteOrder.nativeOrder());
        this.f = true;
        this.h = 35048;
        this.c = this.d.asFloatBuffer();
        if (g.i != null) {
            g.i.glGenBuffers(1, a);
            g.i.glBindBuffer(34962, a.get(0));
            g.i.glBufferData(34962, this.d.capacity(), null, this.h);
            g.i.glBindBuffer(34962, 0);
        } else {
            g.h.b(a);
            g.h.a(34962, a.get(0));
            g.h.a(34962, this.d.capacity(), (Buffer) null, this.h);
            g.h.a(34962, 0);
        }
        this.e = a.get(0);
        this.c.flip();
        this.d.flip();
    }

    public final int a() {
        return (this.c.limit() * 4) / this.b.a;
    }

    public final void a(float[] fArr, int i2) {
        this.i = true;
        if (this.f) {
            BufferUtils.a(fArr, this.d, i2);
            this.c.position(0);
            this.c.limit(i2);
        } else {
            this.c.clear();
            this.c.put(fArr, 0, i2);
            this.c.flip();
            this.d.position(0);
            this.d.limit(this.c.limit() << 2);
        }
        if (this.j) {
            if (g.i != null) {
                g.i.glBufferSubData(34962, 0, this.d.limit(), this.d);
            } else {
                g.h.c(34962, this.d.limit(), this.d);
            }
            this.i = false;
        }
    }

    public final void b() {
        int i2;
        j jVar = g.h;
        jVar.a(34962, this.e);
        if (this.i) {
            this.d.limit(this.c.limit() * 4);
            jVar.c(34962, this.d.limit(), this.d);
            this.i = false;
        }
        int a2 = this.b.a();
        int i3 = 0;
        int i4 = 0;
        while (i3 < a2) {
            n a3 = this.b.a(i3);
            switch (a3.a) {
                case Transform.POS_X /*0*/:
                    jVar.c(32884);
                    jVar.a(a3.b, this.b.a, a3.c);
                    i2 = i4;
                    break;
                case 1:
                case 5:
                    int i5 = 5126;
                    if (a3.a == 5) {
                        i5 = 5121;
                    }
                    jVar.c(32886);
                    jVar.a(a3.b, i5, this.b.a, a3.c);
                    i2 = i4;
                    break;
                case 2:
                    jVar.c(32885);
                    jVar.b(this.b.a, a3.c);
                    i2 = i4;
                    break;
                case 3:
                    jVar.a(33984 + i4);
                    jVar.c(32888);
                    jVar.b(a3.b, this.b.a, a3.c);
                    i2 = i4 + 1;
                    break;
                case 4:
                default:
                    throw new f("unkown vertex attribute type: " + a3.a);
            }
            i3++;
            i4 = i2;
        }
        this.j = true;
    }

    public final void c() {
        j jVar = g.h;
        int a2 = this.b.a();
        int i2 = 0;
        for (int i3 = 0; i3 < a2; i3++) {
            n a3 = this.b.a(i3);
            switch (a3.a) {
                case Transform.POS_X /*0*/:
                    break;
                case 1:
                case 5:
                    jVar.b(32886);
                    break;
                case 2:
                    jVar.b(32885);
                    break;
                case 3:
                    jVar.a(33984 + i2);
                    jVar.b(32888);
                    i2++;
                    break;
                case 4:
                default:
                    throw new f("unkown vertex attribute type: " + a3.a);
            }
        }
        jVar.a(34962, 0);
        this.j = false;
    }

    public final void e() {
        if (g.i != null) {
            a.clear();
            a.put(this.e);
            a.flip();
            h hVar = g.i;
            hVar.glBindBuffer(34962, 0);
            hVar.glDeleteBuffers(1, a);
            this.e = 0;
            return;
        }
        a.clear();
        a.put(this.e);
        a.flip();
        j jVar = g.h;
        jVar.a(34962, 0);
        jVar.a(a);
        this.e = 0;
    }
}
