package com.badlogic.gdx.graphics.g2d;

import com.badlogic.gdx.utils.c;
import com.badlogic.gdx.utils.f;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public class Gdx2DPixmap implements c {
    private static long[] f = new long[4];
    private long a;
    private int b;
    private int c;
    private int d;
    private ByteBuffer e;

    private static native void clear(long j, int i);

    private static native void drawPixmap(long j, long j2, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8);

    private static native void free(long j);

    private static native ByteBuffer load(long[] jArr, byte[] bArr, int i, int i2);

    private static native ByteBuffer newPixmap(long[] jArr, int i, int i2, int i3);

    public static native void setBlend(int i);

    public static native void setScale(int i);

    static {
        setBlend(1);
        setScale(1);
    }

    public Gdx2DPixmap(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[1024];
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                break;
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        this.e = load(f, byteArray, byteArray.length, 0);
        if (this.e == null) {
            throw new IOException("couldn't load pixmap");
        }
        this.a = f[0];
        this.b = (int) f[1];
        this.c = (int) f[2];
        this.d = (int) f[3];
    }

    public Gdx2DPixmap(int i, int i2, int i3) throws IllegalArgumentException {
        this.e = newPixmap(f, i, i2, i3);
        if (this.e == null) {
            throw new IllegalArgumentException("couldn't load pixmap");
        }
        this.a = f[0];
        this.b = (int) f[1];
        this.c = (int) f[2];
        this.d = (int) f[3];
    }

    public final void a() {
        free(this.a);
    }

    public final void a(int i) {
        clear(this.a, i);
    }

    public final void a(Gdx2DPixmap gdx2DPixmap, int i, int i2) {
        drawPixmap(gdx2DPixmap.a, this.a, 0, 0, i, i2, 0, 0, i, i2);
    }

    public final void a(Gdx2DPixmap gdx2DPixmap, int i, int i2, int i3, int i4) {
        drawPixmap(gdx2DPixmap.a, this.a, 0, 0, i, i2, 0, 0, i3, i4);
    }

    public final ByteBuffer b() {
        return this.e;
    }

    public final int c() {
        return this.c;
    }

    public final int d() {
        return this.b;
    }

    public final int e() {
        return this.d;
    }

    public final int f() {
        switch (this.d) {
            case 1:
                return 6406;
            case 2:
                return 6410;
            case 3:
            case 5:
                return 6407;
            case 4:
            case 6:
                return 6408;
            default:
                throw new f("unknown format: " + this.d);
        }
    }

    public final int g() {
        return f();
    }

    public final int h() {
        switch (this.d) {
            case 1:
            case 2:
            case 3:
            case 4:
                return 5121;
            case 5:
                return 33635;
            case 6:
                return 32819;
            default:
                throw new f("unknown format: " + this.d);
        }
    }
}
