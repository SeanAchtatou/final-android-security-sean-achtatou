package com.badlogic.gdx.graphics;

public final class m {
    public final int a;
    private final n[] b;

    public m(n... nVarArr) {
        if (nVarArr.length == 0) {
            throw new IllegalArgumentException("attributes must be >= 1");
        }
        n[] nVarArr2 = new n[nVarArr.length];
        for (int i = 0; i < nVarArr.length; i++) {
            nVarArr2[i] = nVarArr[i];
        }
        this.b = nVarArr2;
        c();
        this.a = b();
    }

    private int b() {
        int i = 0;
        for (n nVar : this.b) {
            nVar.c = i;
            if (nVar.a == 5) {
                i += 4;
            } else {
                i += nVar.b * 4;
            }
        }
        return i;
    }

    private void c() {
        boolean z = false;
        boolean z2 = false;
        for (n nVar : this.b) {
            if (nVar.a == 0) {
                if (z2) {
                    throw new IllegalArgumentException("two position attributes were specified");
                }
                z2 = true;
            }
            if (nVar.a == 1 || nVar.a == 5) {
                if (nVar.b != 4) {
                    throw new IllegalArgumentException("color attribute must have 4 components");
                } else if (z) {
                    throw new IllegalArgumentException("two color attributes were specified");
                } else {
                    z = true;
                }
            }
        }
        if (!z2) {
            throw new IllegalArgumentException("no position attribute was specified");
        }
    }

    public final int a() {
        return this.b.length;
    }

    public final n a(int i) {
        return this.b[i];
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < this.b.length; i++) {
            sb.append(this.b[i].d);
            sb.append(", ");
            sb.append(this.b[i].a);
            sb.append(", ");
            sb.append(this.b[i].b);
            sb.append(", ");
            sb.append(this.b[i].c);
            sb.append("\n");
        }
        return sb.toString();
    }
}
