package com.badlogic.gdx.graphics;

import com.badlogic.gdx.graphics.g2d.Gdx2DPixmap;
import com.badlogic.gdx.utils.f;
import java.io.InputStream;
import java.nio.ByteBuffer;

public final class c implements com.badlogic.gdx.utils.c {
    private static b a = b.SourceOver;
    private Gdx2DPixmap b;
    private int c = 0;

    public enum b {
        None,
        SourceOver
    }

    public enum a {
        Alpha,
        LuminanceAlpha,
        RGB565,
        RGBA4444,
        RGB888,
        RGBA8888;

        static int a(a aVar) {
            if (aVar == Alpha) {
                return 1;
            }
            if (aVar == LuminanceAlpha) {
                return 2;
            }
            if (aVar == RGB565) {
                return 5;
            }
            if (aVar == RGBA4444) {
                return 6;
            }
            if (aVar == RGB888) {
                return 3;
            }
            if (aVar == RGBA8888) {
                return 4;
            }
            throw new f("Unknown Format: " + aVar);
        }

        static a a(int i) {
            if (i == 1) {
                return Alpha;
            }
            if (i == 2) {
                return LuminanceAlpha;
            }
            if (i == 5) {
                return RGB565;
            }
            if (i == 6) {
                return RGBA4444;
            }
            if (i == 3) {
                return RGB888;
            }
            if (i == 4) {
                return RGBA8888;
            }
            throw new f("Unknown Gdx2DPixmap Format: " + i);
        }
    }

    public static void a(b bVar) {
        a = bVar;
        Gdx2DPixmap.setBlend(bVar == b.None ? 0 : 1);
    }

    public c(int i, int i2, a aVar) {
        this.b = new Gdx2DPixmap(i, i2, a.a(aVar));
        this.c = 0;
        b();
    }

    public c(com.badlogic.gdx.c.a aVar) {
        InputStream inputStream = null;
        try {
            inputStream = aVar.b();
            this.b = new Gdx2DPixmap(inputStream);
            try {
                inputStream.close();
            } catch (Exception e) {
            }
        } catch (Exception e2) {
            throw new f("Couldn't load file: " + aVar, e2);
        } catch (Throwable th) {
            try {
                inputStream.close();
            } catch (Exception e3) {
            }
            throw th;
        }
    }

    public final void a() {
        this.c = 0;
    }

    public final void b() {
        this.b.a(this.c);
    }

    public final void a(c cVar, int i, int i2) {
        this.b.a(cVar.b, i, i2);
    }

    public final void a(c cVar, int i, int i2, int i3, int i4) {
        this.b.a(cVar.b, i, i2, i3, i4);
    }

    public final int c() {
        return this.b.d();
    }

    public final int d() {
        return this.b.c();
    }

    public final void e() {
        this.b.a();
    }

    public final int f() {
        return this.b.g();
    }

    public final int g() {
        return this.b.f();
    }

    public final int h() {
        return this.b.h();
    }

    public final ByteBuffer i() {
        return this.b.b();
    }

    public final a j() {
        return a.a(this.b.e());
    }

    public static b k() {
        return a;
    }
}
