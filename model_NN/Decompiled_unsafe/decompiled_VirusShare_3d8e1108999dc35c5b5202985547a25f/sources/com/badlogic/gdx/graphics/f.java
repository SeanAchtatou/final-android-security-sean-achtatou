package com.badlogic.gdx.graphics;

import com.badlogic.gdx.graphics.a.b;
import com.badlogic.gdx.graphics.a.d;
import com.badlogic.gdx.graphics.a.e;
import com.badlogic.gdx.graphics.a.g;
import com.badlogic.gdx.graphics.a.h;
import com.badlogic.gdx.graphics.a.i;
import com.badlogic.gdx.graphics.a.j;
import com.badlogic.gdx.utils.c;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.util.MinimalPrettyPrinter;

public class f implements c {
    private static Map<com.badlogic.gdx.c, List<f>> a = new HashMap();
    private static boolean b = false;
    private g c;
    private d d;
    private boolean e = true;
    private boolean f;

    public enum a {
        VertexArray,
        VertexBufferObject,
        VertexBufferObjectSubData
    }

    public f(boolean z, int i, int i2, n... nVarArr) {
        if (com.badlogic.gdx.g.i == null && com.badlogic.gdx.g.h == null && !b) {
            this.c = new i(i, nVarArr);
            this.d = new h(i2);
            this.f = true;
        } else {
            this.c = new com.badlogic.gdx.graphics.a.a(z, i, nVarArr);
            this.d = new h(z, i2);
            this.f = false;
        }
        a(com.badlogic.gdx.g.a, this);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.badlogic.gdx.graphics.a.a.<init>(boolean, int, com.badlogic.gdx.graphics.n[]):void
     arg types: [int, int, com.badlogic.gdx.graphics.n[]]
     candidates:
      com.badlogic.gdx.graphics.a.a.<init>(boolean, int, com.badlogic.gdx.graphics.m):void
      com.badlogic.gdx.graphics.a.a.<init>(boolean, int, com.badlogic.gdx.graphics.n[]):void */
    public f(a aVar, n... nVarArr) {
        if (aVar == a.VertexArray && com.badlogic.gdx.g.b.a()) {
            aVar = a.VertexBufferObject;
        }
        if (aVar == a.VertexBufferObject) {
            this.c = new com.badlogic.gdx.graphics.a.a(false, 4000, nVarArr);
            this.d = new h(false, 6000);
            this.f = false;
        } else if (aVar == a.VertexBufferObjectSubData) {
            this.c = new b(nVarArr);
            this.d = new j();
            this.f = false;
        } else {
            this.c = new i(4000, nVarArr);
            this.d = new h(6000);
            this.f = true;
        }
        a(com.badlogic.gdx.g.a, this);
    }

    public final void a(float[] fArr) {
        this.c.a(fArr, fArr.length);
    }

    public final void a(float[] fArr, int i) {
        this.c.a(fArr, i);
    }

    public final void a(short[] sArr) {
        this.d.a(sArr, sArr.length);
    }

    public final void c() {
        this.e = false;
    }

    public final void d() {
        if (com.badlogic.gdx.g.b.a()) {
            throw new IllegalStateException("can't use this render method with OpenGL ES 2.0");
        }
        this.c.b();
        if (!this.f && this.d.a() > 0) {
            this.d.d();
        }
    }

    public final void e() {
        if (com.badlogic.gdx.g.b.a()) {
            throw new IllegalStateException("can't use this render method with OpenGL ES 2.0");
        }
        this.c.c();
        if (!this.f && this.d.a() > 0) {
            this.d.e();
        }
    }

    public final void a(int i) {
        a(i, 0, this.d.b() > 0 ? this.d.a() : this.c.a());
    }

    public final void a(int i, int i2, int i3) {
        if (com.badlogic.gdx.g.b.a()) {
            throw new IllegalStateException("can't use this render method with OpenGL ES 2.0");
        }
        if (this.e) {
            d();
        }
        if (this.f) {
            if (this.d.a() > 0) {
                ShortBuffer c2 = this.d.c();
                int position = c2.position();
                int limit = c2.limit();
                c2.position(i2);
                c2.limit(i2 + i3);
                com.badlogic.gdx.g.g.glDrawElements(i, i3, 5123, c2);
                c2.position(position);
                c2.limit(limit);
            } else {
                com.badlogic.gdx.g.g.glDrawArrays(i, i2, i3);
            }
        } else if (this.d.a() > 0) {
            com.badlogic.gdx.g.h.c(i, i3, i2 * 2);
        } else {
            com.badlogic.gdx.g.h.glDrawArrays(i, i2, i3);
        }
        if (this.e) {
            e();
        }
    }

    public final void a(e eVar, int i) {
        if (!com.badlogic.gdx.g.b.a()) {
            throw new IllegalStateException("can't use this render method with OpenGL ES 1.x");
        }
        if (this.e) {
            if (!com.badlogic.gdx.g.b.a()) {
                throw new IllegalStateException("can't use this render method with OpenGL ES 1.x");
            }
            ((com.badlogic.gdx.graphics.a.a) this.c).a(eVar);
            if (this.d.a() > 0) {
                this.d.d();
            }
        }
        if (this.d.a() > 0) {
            com.badlogic.gdx.g.i.glDrawElements(4, i, 5123, 0);
        } else {
            com.badlogic.gdx.g.i.glDrawArrays(4, 0, i);
        }
        if (!this.e) {
            return;
        }
        if (!com.badlogic.gdx.g.b.a()) {
            throw new IllegalStateException("can't use this render method with OpenGL ES 1.x");
        }
        ((com.badlogic.gdx.graphics.a.a) this.c).b(eVar);
        if (this.d.a() > 0) {
            this.d.e();
        }
    }

    public void a() {
        if (a.get(com.badlogic.gdx.g.a) != null) {
            a.get(com.badlogic.gdx.g.a).remove(this);
        }
        this.c.e();
        this.d.g();
    }

    private static void a(com.badlogic.gdx.c cVar, f fVar) {
        Object obj = a.get(cVar);
        if (obj == null) {
            obj = new ArrayList();
        }
        obj.add(fVar);
        a.put(cVar, obj);
    }

    public static void a(com.badlogic.gdx.c cVar) {
        List list = a.get(cVar);
        if (list != null) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < list.size()) {
                    if (((f) list.get(i2)).c instanceof com.badlogic.gdx.graphics.a.a) {
                        ((com.badlogic.gdx.graphics.a.a) ((f) list.get(i2)).c).d();
                        ((f) list.get(i2)).d.f();
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public static void b(com.badlogic.gdx.c cVar) {
        a.remove(cVar);
    }

    public static String f() {
        StringBuilder sb = new StringBuilder();
        sb.append("Managed meshes/app: { ");
        for (com.badlogic.gdx.c cVar : a.keySet()) {
            sb.append(a.get(cVar).size());
            sb.append(MinimalPrettyPrinter.DEFAULT_ROOT_VALUE_SEPARATOR);
        }
        sb.append("}");
        return sb.toString();
    }
}
