package com.badlogic.gdx.graphics;

public final class g {
    public static final g a = new g(1.0f, 1.0f, 1.0f);
    private static g f = new g(0.0f, 0.0f, 0.0f);
    private static g g = new g(1.0f, 0.0f, 0.0f);
    private static g h = new g(0.0f, 1.0f, 0.0f);
    private static g i = new g(0.0f, 0.0f, 1.0f);
    public float b;
    public float c;
    public float d;
    public float e;

    public g() {
    }

    public g(float f2, float f3, float f4) {
        this.b = f2;
        this.c = f3;
        this.d = f4;
        this.e = 1.0f;
        if (this.b < 0.0f) {
            this.b = 0.0f;
        } else if (this.b > 1.0f) {
            this.b = 1.0f;
        }
        if (this.c < 0.0f) {
            this.c = 0.0f;
        } else if (this.c > 1.0f) {
            this.c = 1.0f;
        }
        if (this.d < 0.0f) {
            this.d = 0.0f;
        } else if (this.d > 1.0f) {
            this.d = 1.0f;
        }
        if (this.e < 0.0f) {
            this.e = 0.0f;
        } else if (this.e > 1.0f) {
            this.e = 1.0f;
        }
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        g gVar = (g) obj;
        if (Float.compare(gVar.e, this.e) != 0) {
            return false;
        }
        if (Float.compare(gVar.d, this.d) != 0) {
            return false;
        }
        if (Float.compare(gVar.c, this.c) != 0) {
            return false;
        }
        if (Float.compare(gVar.b, this.b) != 0) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        int i2;
        int i3;
        int i4;
        int i5 = 0;
        if (this.b != 0.0f) {
            i2 = Float.floatToIntBits(this.b);
        } else {
            i2 = 0;
        }
        int i6 = i2 * 31;
        if (this.c != 0.0f) {
            i3 = Float.floatToIntBits(this.c);
        } else {
            i3 = 0;
        }
        int i7 = (i3 + i6) * 31;
        if (this.d != 0.0f) {
            i4 = Float.floatToIntBits(this.d);
        } else {
            i4 = 0;
        }
        int i8 = (i4 + i7) * 31;
        if (this.e != 0.0f) {
            i5 = Float.floatToIntBits(this.e);
        }
        return i8 + i5;
    }

    public final String toString() {
        return Integer.toHexString((((int) (this.e * 255.0f)) << 24) | (((int) (this.d * 255.0f)) << 16) | (((int) (this.c * 255.0f)) << 8) | ((int) (this.b * 255.0f)));
    }

    public final float a() {
        return Float.intBitsToFloat(((((int) (this.e * 255.0f)) << 24) | (((int) (this.d * 255.0f)) << 16) | (((int) (this.c * 255.0f)) << 8) | ((int) (this.b * 255.0f))) & -16777217);
    }
}
