package com.badlogic.gdx.graphics;

import com.badlogic.gdx.g;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.f;

public abstract class a {
    public final f a = new f();
    public final f b = new f(0.0f, 0.0f, -1.0f);
    public final f c = new f(0.0f, 1.0f, 0.0f);
    public final Matrix4 d = new Matrix4();
    public final Matrix4 e = new Matrix4();
    public final Matrix4 f = new Matrix4();
    public final Matrix4 g = new Matrix4();
    public float h = 1.0f;
    public float i = 100.0f;
    public float j = 0.0f;
    public float k = 0.0f;
    public final com.badlogic.gdx.math.a l = new com.badlogic.gdx.math.a();
    private final Matrix4 m = new Matrix4();
    private final f n = new f();
    private com.badlogic.gdx.math.a.a o = new com.badlogic.gdx.math.a.a(new f(), new f());

    public void a(i iVar) {
        iVar.d(5889);
        iVar.a(this.d.a);
        iVar.d(5888);
        iVar.a(this.e.a);
    }

    public final void a() {
        this.b.a(0.0f, 0.0f, -5.0f).c(this.a).e();
    }

    public final void b() {
        this.a.b(0.0f, 0.0f, 0.0f);
    }

    public final void a(f fVar, float f2, float f3) {
        float f4 = fVar.a;
        float f5 = fVar.b;
        fVar.a = ((f4 * 2.0f) / f2) - 1.0f;
        fVar.b = ((2.0f * ((((float) g.b.e()) - f5) - 1.0f)) / f3) - 1.0f;
        fVar.c = (fVar.c * 2.0f) - 1.0f;
        fVar.a(this.g);
    }
}
