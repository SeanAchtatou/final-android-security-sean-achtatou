package com.badlogic.gdx.math;

import java.io.Serializable;

public final class d implements Serializable {
    private static d e = new d((byte) 0);
    private static d f = new d((byte) 0);
    public float a;
    public float b;
    public float c;
    public float d;

    private d(byte b2) {
        a(0.0f, 0.0f, 0.0f, 0.0f);
    }

    d() {
    }

    private d a(float f2, float f3, float f4, float f5) {
        this.a = f2;
        this.b = f3;
        this.c = f4;
        this.d = f5;
        return this;
    }

    public final d a(f fVar, float f2) {
        float radians = (float) Math.toRadians((double) f2);
        float sin = (float) Math.sin((double) (radians / 2.0f));
        d a2 = a(fVar.a * sin, fVar.b * sin, sin * fVar.c, (float) Math.cos((double) (radians / 2.0f)));
        float f3 = (a2.a * a2.a) + (a2.b * a2.b) + (a2.c * a2.c) + (a2.d * a2.d);
        if (f3 != 0.0f && Math.abs(f3 - 1.0f) > 1.0E-5f) {
            float sqrt = (float) Math.sqrt((double) f3);
            a2.d /= sqrt;
            a2.a /= sqrt;
            a2.b /= sqrt;
            a2.c /= sqrt;
        }
        return a2;
    }

    public final String toString() {
        return "[" + this.a + "|" + this.b + "|" + this.c + "|" + this.d + "]";
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof d)) {
            return false;
        }
        d dVar = (d) obj;
        if (this.a == dVar.a && this.b == dVar.b && this.c == dVar.c && this.d == dVar.d) {
            return true;
        }
        return false;
    }
}
