package com.badlogic.gdx.math;

public final class a {
    private static f[] a = {new f(-1.0f, -1.0f, -1.0f), new f(1.0f, -1.0f, -1.0f), new f(1.0f, 1.0f, -1.0f), new f(-1.0f, 1.0f, -1.0f), new f(-1.0f, -1.0f, 1.0f), new f(1.0f, -1.0f, 1.0f), new f(1.0f, 1.0f, 1.0f), new f(-1.0f, 1.0f, 1.0f)};
    private static float[] b = new float[24];
    private c[] c = new c[6];
    private f[] d = {new f(), new f(), new f(), new f(), new f(), new f(), new f(), new f()};
    private float[] e = new float[24];

    static {
        int i = 0;
        for (f fVar : a) {
            int i2 = i + 1;
            b[i] = fVar.a;
            int i3 = i2 + 1;
            b[i2] = fVar.b;
            i = i3 + 1;
            b[i3] = fVar.c;
        }
    }

    public a() {
        for (int i = 0; i < 6; i++) {
            this.c[i] = new c(new f());
        }
    }

    public final void a(Matrix4 matrix4) {
        System.arraycopy(b, 0, this.e, 0, b.length);
        Matrix4.prj(matrix4.a, this.e, 0, 8, 3);
        int i = 0;
        for (int i2 = 0; i2 < 8; i2++) {
            f fVar = this.d[i2];
            int i3 = i + 1;
            fVar.a = this.e[i];
            int i4 = i3 + 1;
            fVar.b = this.e[i3];
            i = i4 + 1;
            fVar.c = this.e[i4];
        }
        this.c[0].a(this.d[1], this.d[0], this.d[2]);
        this.c[1].a(this.d[4], this.d[5], this.d[7]);
        this.c[2].a(this.d[0], this.d[4], this.d[3]);
        this.c[3].a(this.d[5], this.d[1], this.d[6]);
        this.c[4].a(this.d[2], this.d[3], this.d[6]);
        this.c[5].a(this.d[4], this.d[0], this.d[1]);
    }
}
