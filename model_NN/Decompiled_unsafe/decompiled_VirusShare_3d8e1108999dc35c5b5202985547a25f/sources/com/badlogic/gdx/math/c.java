package com.badlogic.gdx.math;

import java.io.Serializable;

public final class c implements Serializable {
    private f a = new f();
    private float b = 0.0f;

    public c(f fVar) {
        this.a.a(fVar).e();
        this.b = 0.0f;
    }

    public final void a(f fVar, f fVar2, f fVar3) {
        f e = fVar.b().c(fVar2).e(fVar2.c().c(fVar3)).e();
        this.a.a(e);
        this.b = -fVar.d(e);
    }

    public final String toString() {
        return this.a.toString() + ", " + this.b;
    }
}
