package com.badlogic.gdx.math;

import java.io.Serializable;

public class Matrix4 implements Serializable {
    private static d c = new d();
    private static f d = new f();
    private static f e = new f();
    private static f f = new f();
    private static f g = new f();
    private static f h = new f();
    private static Matrix4 i = new Matrix4();
    private static f j = new f();
    private static f k = new f();
    private static f l = new f();
    public final float[] a = new float[16];
    private float[] b = new float[16];

    public static native boolean inv(float[] fArr);

    public static native void mul(float[] fArr, float[] fArr2);

    public static native void prj(float[] fArr, float[] fArr2, int i2, int i3, int i4);

    public Matrix4() {
        this.a[0] = 1.0f;
        this.a[5] = 1.0f;
        this.a[10] = 1.0f;
        this.a[15] = 1.0f;
    }

    public final Matrix4 a(Matrix4 matrix4) {
        return a(matrix4.a);
    }

    public final Matrix4 a(float[] fArr) {
        this.a[0] = fArr[0];
        this.a[1] = fArr[1];
        this.a[2] = fArr[2];
        this.a[3] = fArr[3];
        this.a[4] = fArr[4];
        this.a[5] = fArr[5];
        this.a[6] = fArr[6];
        this.a[7] = fArr[7];
        this.a[8] = fArr[8];
        this.a[9] = fArr[9];
        this.a[10] = fArr[10];
        this.a[11] = fArr[11];
        this.a[12] = fArr[12];
        this.a[13] = fArr[13];
        this.a[14] = fArr[14];
        this.a[15] = fArr[15];
        return this;
    }

    public final Matrix4 b(Matrix4 matrix4) {
        this.b[0] = (this.a[0] * matrix4.a[0]) + (this.a[4] * matrix4.a[1]) + (this.a[8] * matrix4.a[2]) + (this.a[12] * matrix4.a[3]);
        this.b[4] = (this.a[0] * matrix4.a[4]) + (this.a[4] * matrix4.a[5]) + (this.a[8] * matrix4.a[6]) + (this.a[12] * matrix4.a[7]);
        this.b[8] = (this.a[0] * matrix4.a[8]) + (this.a[4] * matrix4.a[9]) + (this.a[8] * matrix4.a[10]) + (this.a[12] * matrix4.a[11]);
        this.b[12] = (this.a[0] * matrix4.a[12]) + (this.a[4] * matrix4.a[13]) + (this.a[8] * matrix4.a[14]) + (this.a[12] * matrix4.a[15]);
        this.b[1] = (this.a[1] * matrix4.a[0]) + (this.a[5] * matrix4.a[1]) + (this.a[9] * matrix4.a[2]) + (this.a[13] * matrix4.a[3]);
        this.b[5] = (this.a[1] * matrix4.a[4]) + (this.a[5] * matrix4.a[5]) + (this.a[9] * matrix4.a[6]) + (this.a[13] * matrix4.a[7]);
        this.b[9] = (this.a[1] * matrix4.a[8]) + (this.a[5] * matrix4.a[9]) + (this.a[9] * matrix4.a[10]) + (this.a[13] * matrix4.a[11]);
        this.b[13] = (this.a[1] * matrix4.a[12]) + (this.a[5] * matrix4.a[13]) + (this.a[9] * matrix4.a[14]) + (this.a[13] * matrix4.a[15]);
        this.b[2] = (this.a[2] * matrix4.a[0]) + (this.a[6] * matrix4.a[1]) + (this.a[10] * matrix4.a[2]) + (this.a[14] * matrix4.a[3]);
        this.b[6] = (this.a[2] * matrix4.a[4]) + (this.a[6] * matrix4.a[5]) + (this.a[10] * matrix4.a[6]) + (this.a[14] * matrix4.a[7]);
        this.b[10] = (this.a[2] * matrix4.a[8]) + (this.a[6] * matrix4.a[9]) + (this.a[10] * matrix4.a[10]) + (this.a[14] * matrix4.a[11]);
        this.b[14] = (this.a[2] * matrix4.a[12]) + (this.a[6] * matrix4.a[13]) + (this.a[10] * matrix4.a[14]) + (this.a[14] * matrix4.a[15]);
        this.b[3] = (this.a[3] * matrix4.a[0]) + (this.a[7] * matrix4.a[1]) + (this.a[11] * matrix4.a[2]) + (this.a[15] * matrix4.a[3]);
        this.b[7] = (this.a[3] * matrix4.a[4]) + (this.a[7] * matrix4.a[5]) + (this.a[11] * matrix4.a[6]) + (this.a[15] * matrix4.a[7]);
        this.b[11] = (this.a[3] * matrix4.a[8]) + (this.a[7] * matrix4.a[9]) + (this.a[11] * matrix4.a[10]) + (this.a[15] * matrix4.a[11]);
        this.b[15] = (this.a[3] * matrix4.a[12]) + (this.a[7] * matrix4.a[13]) + (this.a[11] * matrix4.a[14]) + (this.a[15] * matrix4.a[15]);
        return a(this.b);
    }

    public final Matrix4 a() {
        this.a[0] = 1.0f;
        this.a[4] = 0.0f;
        this.a[8] = 0.0f;
        this.a[12] = 0.0f;
        this.a[1] = 0.0f;
        this.a[5] = 1.0f;
        this.a[9] = 0.0f;
        this.a[13] = 0.0f;
        this.a[2] = 0.0f;
        this.a[6] = 0.0f;
        this.a[10] = 1.0f;
        this.a[14] = 0.0f;
        this.a[3] = 0.0f;
        this.a[7] = 0.0f;
        this.a[11] = 0.0f;
        this.a[15] = 1.0f;
        return this;
    }

    public final Matrix4 a(float f2, float f3, float f4, float f5) {
        a();
        float tan = (float) (1.0d / Math.tan((((double) f4) * 0.017453292519943295d) / 2.0d));
        this.a[0] = tan / f5;
        this.a[1] = 0.0f;
        this.a[2] = 0.0f;
        this.a[3] = 0.0f;
        this.a[4] = 0.0f;
        this.a[5] = tan;
        this.a[6] = 0.0f;
        this.a[7] = 0.0f;
        this.a[8] = 0.0f;
        this.a[9] = 0.0f;
        this.a[10] = (f3 + f2) / (f2 - f3);
        this.a[11] = -1.0f;
        this.a[12] = 0.0f;
        this.a[13] = 0.0f;
        this.a[14] = ((2.0f * f3) * f2) / (f2 - f3);
        this.a[15] = 0.0f;
        return this;
    }

    public final Matrix4 a(float f2, float f3) {
        float f4 = 0.0f + f2;
        float f5 = 0.0f + f3;
        a();
        float f6 = 2.0f / f4;
        float f7 = 2.0f / f5;
        this.a[0] = f6;
        this.a[1] = 0.0f;
        this.a[2] = 0.0f;
        this.a[3] = 0.0f;
        this.a[4] = 0.0f;
        this.a[5] = f7;
        this.a[6] = 0.0f;
        this.a[7] = 0.0f;
        this.a[8] = 0.0f;
        this.a[9] = 0.0f;
        this.a[10] = -2.0f;
        this.a[11] = 0.0f;
        this.a[12] = (-f4) / f4;
        this.a[13] = (-f5) / f5;
        this.a[14] = -1.0f;
        this.a[15] = 1.0f;
        return this;
    }

    public final Matrix4 a(f fVar) {
        a();
        this.a[12] = fVar.a;
        this.a[13] = fVar.b;
        this.a[14] = fVar.c;
        return this;
    }

    public final Matrix4 b(float f2, float f3) {
        a();
        this.a[12] = f2;
        this.a[13] = f3;
        this.a[14] = 0.0f;
        return this;
    }

    public final Matrix4 a(float f2) {
        a();
        if (f2 != 0.0f) {
            d a2 = c.a(d.a(0.0f, 0.0f, 1.0f), f2);
            float f3 = a2.a * a2.a;
            float f4 = a2.a * a2.b;
            float f5 = a2.a * a2.c;
            float f6 = a2.a * a2.d;
            float f7 = a2.b * a2.b;
            float f8 = a2.b * a2.c;
            float f9 = a2.b * a2.d;
            float f10 = a2.c * a2.c;
            float f11 = a2.d * a2.c;
            this.a[0] = 1.0f - (2.0f * (f7 + f10));
            this.a[4] = 2.0f * (f4 - f11);
            this.a[8] = 2.0f * (f5 + f9);
            this.a[12] = 0.0f;
            this.a[1] = (f11 + f4) * 2.0f;
            this.a[5] = 1.0f - ((f10 + f3) * 2.0f);
            this.a[9] = 2.0f * (f8 - f6);
            this.a[13] = 0.0f;
            this.a[2] = (f5 - f9) * 2.0f;
            this.a[6] = 2.0f * (f6 + f8);
            this.a[10] = 1.0f - ((f3 + f7) * 2.0f);
            this.a[14] = 0.0f;
            this.a[3] = 0.0f;
            this.a[7] = 0.0f;
            this.a[11] = 0.0f;
            this.a[15] = 1.0f;
        }
        return this;
    }

    public final Matrix4 c(float f2, float f3) {
        a();
        this.a[0] = f2;
        this.a[5] = f3;
        this.a[10] = 1.0f;
        return this;
    }

    public final Matrix4 a(f fVar, f fVar2, f fVar3) {
        h.a(fVar2).c(fVar);
        f fVar4 = h;
        e.a(fVar4).e();
        f.a(fVar4).e();
        f.e(fVar3).e();
        g.a(f).e(e).e();
        a();
        this.a[0] = f.a;
        this.a[4] = f.b;
        this.a[8] = f.c;
        this.a[1] = g.a;
        this.a[5] = g.b;
        this.a[9] = g.c;
        this.a[2] = -e.a;
        this.a[6] = -e.b;
        this.a[10] = -e.c;
        b(i.a(fVar.b().a(-1.0f)));
        return this;
    }

    public String toString() {
        return "[" + this.a[0] + "|" + this.a[4] + "|" + this.a[8] + "|" + this.a[12] + "]\n" + "[" + this.a[1] + "|" + this.a[5] + "|" + this.a[9] + "|" + this.a[13] + "]\n" + "[" + this.a[2] + "|" + this.a[6] + "|" + this.a[10] + "|" + this.a[14] + "]\n" + "[" + this.a[3] + "|" + this.a[7] + "|" + this.a[11] + "|" + this.a[15] + "]\n";
    }
}
