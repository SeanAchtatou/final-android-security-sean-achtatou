package com.badlogic.gdx.math;

import java.io.Serializable;

public final class g implements Serializable {
    private static final g c = new g();
    public float a;
    public float b;

    public g() {
    }

    public g(float f, float f2) {
        this.a = f;
        this.b = f2;
    }

    private g(g gVar) {
        a(gVar);
    }

    public final g a() {
        return new g(this);
    }

    public final float b() {
        return (float) Math.sqrt((double) ((this.a * this.a) + (this.b * this.b)));
    }

    public final float c() {
        return (this.a * this.a) + (this.b * this.b);
    }

    public final g a(g gVar) {
        this.a = gVar.a;
        this.b = gVar.b;
        return this;
    }

    public final g a(float f, float f2) {
        this.a = f;
        this.b = f2;
        return this;
    }

    public final g b(g gVar) {
        this.a -= gVar.a;
        this.b -= gVar.b;
        return this;
    }

    public final g c(g gVar) {
        this.a += gVar.a;
        this.b += gVar.b;
        return this;
    }

    public final g a(float f) {
        this.a *= f;
        this.b *= f;
        return this;
    }

    public final float d(g gVar) {
        float f = gVar.a - this.a;
        float f2 = gVar.b - this.b;
        return (float) Math.sqrt((double) ((f * f) + (f2 * f2)));
    }

    public final String toString() {
        return "[" + this.a + ":" + this.b + "]";
    }

    public final float d() {
        float atan2 = ((float) Math.atan2((double) this.b, (double) this.a)) * 57.295776f;
        if (atan2 < 0.0f) {
            return atan2 + 360.0f;
        }
        return atan2;
    }
}
