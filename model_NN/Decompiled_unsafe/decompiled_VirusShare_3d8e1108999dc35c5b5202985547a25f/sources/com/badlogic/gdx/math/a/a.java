package com.badlogic.gdx.math.a;

import com.badlogic.gdx.math.f;
import java.io.Serializable;

public final class a implements Serializable {
    private static f c = new f();
    public final f a = new f();
    public final f b = new f();

    public a(f fVar, f fVar2) {
        this.a.a(fVar);
        this.b.a(fVar2).e();
    }

    public final String toString() {
        return "ray [" + this.a + ":" + this.b + "]";
    }

    public final a a(f fVar, f fVar2) {
        this.a.a(fVar);
        this.b.a(fVar2);
        return this;
    }
}
