package com.badlogic.gdx.math;

import java.util.Random;

public final class b {
    private static float[] a = new float[8192];
    private static float[] b = new float[8192];
    private static final int c = ((int) Math.sqrt(16384.0d));
    private static final float d = (1.0f / ((float) (c - 1)));
    private static final float[] e = new float[16384];
    private static Random f = new Random();
    private static final double g = Double.longBitsToDouble(Double.doubleToLongBits(16385.0d) - 1);

    static {
        for (int i = 0; i < 8192; i++) {
            float f2 = ((((float) i) + 0.5f) / 8192.0f) * 6.2831855f;
            a[i] = (float) Math.sin((double) f2);
            b[i] = (float) Math.cos((double) f2);
        }
        for (int i2 = 0; i2 < 360; i2 += 90) {
            a[((int) (((float) i2) * 22.755556f)) & 8191] = (float) Math.sin((double) (((float) i2) * 0.017453292f));
            b[((int) (((float) i2) * 22.755556f)) & 8191] = (float) Math.cos((double) (((float) i2) * 0.017453292f));
        }
        for (int i3 = 0; i3 < c; i3++) {
            for (int i4 = 0; i4 < c; i4++) {
                e[(c * i4) + i3] = (float) Math.atan2((double) (((float) i4) / ((float) c)), (double) (((float) i3) / ((float) c)));
            }
        }
    }

    public static final int a() {
        return f.nextInt(3);
    }

    public static int a(int i) {
        if (i == 0) {
            return 1;
        }
        if (((i - 1) & i) == 0) {
            return i;
        }
        int i2 = (i >> 1) | i;
        int i3 = i2 | (i2 >> 2);
        int i4 = i3 | (i3 >> 4);
        int i5 = i4 | (i4 >> 8);
        return (i5 | (i5 >> 16)) + 1;
    }

    public static boolean b(int i) {
        return i != 0 && ((i + -1) & i) == 0;
    }
}
