package com.badlogic.gdx.math;

import java.io.Serializable;

public final class f implements Serializable {
    private static f d = new f();
    private static f e = new f();
    private static f f = new f();
    public float a;
    public float b;
    public float c;

    public f() {
    }

    public f(float f2, float f3, float f4) {
        a(f2, f3, f4);
    }

    public f(f fVar) {
        a(fVar);
    }

    public final f a(float f2, float f3, float f4) {
        this.a = f2;
        this.b = f3;
        this.c = f4;
        return this;
    }

    public final f a(f fVar) {
        return a(fVar.a, fVar.b, fVar.c);
    }

    public final f a() {
        return new f(this);
    }

    public final f b() {
        return d.a(this);
    }

    public final f c() {
        return e.a(this);
    }

    public final f b(f fVar) {
        return b(fVar.a, fVar.b, fVar.c);
    }

    public final f b(float f2, float f3, float f4) {
        return a(this.a + f2, this.b + f3, this.c + f4);
    }

    public final f c(f fVar) {
        return c(fVar.a, fVar.b, fVar.c);
    }

    public final f c(float f2, float f3, float f4) {
        return a(this.a - f2, this.b - f3, this.c - f4);
    }

    public final f a(float f2) {
        return a(this.a * f2, this.b * f2, this.c * f2);
    }

    public final float d() {
        return (float) Math.sqrt((double) ((this.a * this.a) + (this.b * this.b) + (this.c * this.c)));
    }

    public final f e() {
        if (this.a == 0.0f && this.b == 0.0f && this.c == 0.0f) {
            return this;
        }
        float d2 = 1.0f / d();
        return a(this.a * d2, this.b * d2, d2 * this.c);
    }

    public final float d(f fVar) {
        return (this.a * fVar.a) + (this.b * fVar.b) + (this.c * fVar.c);
    }

    public final f e(f fVar) {
        return a((this.b * fVar.c) - (this.c * fVar.b), (this.c * fVar.a) - (this.a * fVar.c), (this.a * fVar.b) - (this.b * fVar.a));
    }

    public final f a(Matrix4 matrix4) {
        float[] fArr = matrix4.a;
        float f2 = (this.a * fArr[3]) + (this.b * fArr[7]) + (this.c * fArr[11]) + fArr[15];
        return a(((((this.a * fArr[0]) + (this.b * fArr[4])) + (this.c * fArr[8])) + fArr[12]) / f2, ((((this.a * fArr[1]) + (this.b * fArr[5])) + (this.c * fArr[9])) + fArr[13]) / f2, (fArr[14] + (((this.a * fArr[2]) + (this.b * fArr[6])) + (this.c * fArr[10]))) / f2);
    }

    public final String toString() {
        return String.format("%.4f", Float.valueOf(this.a)) + ", " + String.format("%.4f", Float.valueOf(this.b)) + ", " + String.format("%.4f", Float.valueOf(this.c));
    }

    public final int hashCode() {
        return ((((Float.floatToIntBits(this.a) + 31) * 31) + Float.floatToIntBits(this.b)) * 31) + Float.floatToIntBits(this.c);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        f fVar = (f) obj;
        if (Float.floatToIntBits(this.a) != Float.floatToIntBits(fVar.a)) {
            return false;
        }
        if (Float.floatToIntBits(this.b) != Float.floatToIntBits(fVar.b)) {
            return false;
        }
        if (Float.floatToIntBits(this.c) != Float.floatToIntBits(fVar.c)) {
            return false;
        }
        return true;
    }

    public final f d(float f2, float f3, float f4) {
        this.a *= f2;
        this.b *= f3;
        this.c *= f4;
        return this;
    }
}
