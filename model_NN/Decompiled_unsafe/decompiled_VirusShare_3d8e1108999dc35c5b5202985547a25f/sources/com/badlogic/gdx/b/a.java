package com.badlogic.gdx.b;

import com.badlogic.gdx.f;
import com.badlogic.gdx.g;
import com.badlogic.gdx.physics.box2d.Transform;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

public final class a implements com.badlogic.gdx.b, Runnable {
    private static int g = 8190;
    Set<Integer> a;
    int[] b;
    int[] c;
    boolean[] d;
    boolean e;
    f f;
    private ServerSocket h;
    private float[] i;
    private float[] j;
    private boolean k;
    private float l;
    private float m;
    private final int n;
    private String[] o;

    /* renamed from: com.badlogic.gdx.b.a$a  reason: collision with other inner class name */
    class C0004a {
        int a;
        int b;
        char c;

        C0004a() {
        }
    }

    class c {
        int a;
        int b;
        int c;
        int d;

        c() {
        }
    }

    class b implements Runnable {
        private c a;
        private C0004a b;

        public b(c cVar, C0004a aVar) {
            this.a = cVar;
            this.b = aVar;
        }

        public final void run() {
            a.this.e = false;
            if (a.this.f != null) {
                if (this.a != null) {
                    a.this.b[this.a.d] = this.a.b;
                    a.this.c[this.a.d] = this.a.c;
                    switch (this.a.a) {
                        case Transform.POS_X /*0*/:
                            a.this.f.a(this.a.b, this.a.c, this.a.d);
                            a.this.d[this.a.d] = true;
                            a.this.e = true;
                            break;
                        case 1:
                            a.this.f.b(this.a.b, this.a.c, this.a.d);
                            a.this.d[this.a.d] = false;
                            break;
                        case 2:
                            a.this.f.c(this.a.b, this.a.c, this.a.d);
                            break;
                    }
                }
                if (this.b != null) {
                    switch (this.b.a) {
                        case Transform.POS_X /*0*/:
                            a.this.a.add(Integer.valueOf(this.b.b));
                            return;
                        case 1:
                            a.this.a.remove(Integer.valueOf(this.b.b));
                            return;
                        default:
                            return;
                    }
                }
            } else {
                if (this.a != null) {
                    a.this.b[this.a.d] = this.a.b;
                    a.this.c[this.a.d] = this.a.c;
                    if (this.a.a == 0) {
                        a.this.d[this.a.d] = true;
                        a.this.e = true;
                    }
                    if (this.a.a == 1) {
                        a.this.d[this.a.d] = false;
                    }
                }
                if (this.b != null) {
                    if (this.b.a == 0) {
                        a.this.a.add(Integer.valueOf(this.b.b));
                    }
                    if (this.b.a == 1) {
                        a.this.a.remove(Integer.valueOf(this.b.b));
                    }
                }
            }
        }
    }

    public a() {
        this(g);
    }

    private a(int i2) {
        this.i = new float[3];
        this.j = new float[3];
        this.k = false;
        this.l = 0.0f;
        this.m = 0.0f;
        this.a = new HashSet();
        this.b = new int[20];
        this.c = new int[20];
        this.d = new boolean[20];
        this.e = false;
        this.f = null;
        try {
            this.n = i2;
            this.h = new ServerSocket(i2);
            Thread thread = new Thread(this);
            thread.setDaemon(true);
            thread.start();
            InetAddress[] allByName = InetAddress.getAllByName(InetAddress.getLocalHost().getHostName());
            this.o = new String[allByName.length];
            for (int i3 = 0; i3 < allByName.length; i3++) {
                this.o[i3] = allByName[i3].getHostAddress();
            }
        } catch (Exception e2) {
            throw new com.badlogic.gdx.utils.f("Couldn't open listening socket at port '" + i2 + "'", e2);
        }
    }

    public final void run() {
        DataInputStream dataInputStream;
        c cVar;
        C0004a aVar;
        while (true) {
            try {
                System.out.println("listening, port " + this.n);
                Socket accept = this.h.accept();
                accept.setTcpNoDelay(true);
                accept.setSoTimeout(3000);
                dataInputStream = new DataInputStream(accept.getInputStream());
                this.k = dataInputStream.readBoolean();
                break;
            } catch (IOException e2) {
            }
        }
        while (true) {
            switch (dataInputStream.readInt()) {
                case Transform.POS_X /*0*/:
                    C0004a aVar2 = new C0004a();
                    aVar2.b = dataInputStream.readInt();
                    aVar2.a = 0;
                    aVar = aVar2;
                    cVar = null;
                    break;
                case 1:
                    C0004a aVar3 = new C0004a();
                    aVar3.b = dataInputStream.readInt();
                    aVar3.a = 1;
                    aVar = aVar3;
                    cVar = null;
                    break;
                case 2:
                    C0004a aVar4 = new C0004a();
                    aVar4.c = dataInputStream.readChar();
                    aVar4.a = 2;
                    aVar = aVar4;
                    cVar = null;
                    break;
                case 3:
                    cVar = new c();
                    cVar.b = (int) ((((float) dataInputStream.readInt()) / this.l) * ((float) g.b.d()));
                    cVar.c = (int) ((((float) dataInputStream.readInt()) / this.m) * ((float) g.b.e()));
                    cVar.d = dataInputStream.readInt();
                    cVar.a = 0;
                    aVar = null;
                    break;
                case 4:
                    cVar = new c();
                    cVar.b = (int) ((((float) dataInputStream.readInt()) / this.l) * ((float) g.b.d()));
                    cVar.c = (int) ((((float) dataInputStream.readInt()) / this.m) * ((float) g.b.e()));
                    cVar.d = dataInputStream.readInt();
                    cVar.a = 1;
                    aVar = null;
                    break;
                case 5:
                    cVar = new c();
                    cVar.b = (int) ((((float) dataInputStream.readInt()) / this.l) * ((float) g.b.d()));
                    cVar.c = (int) ((((float) dataInputStream.readInt()) / this.m) * ((float) g.b.e()));
                    cVar.d = dataInputStream.readInt();
                    cVar.a = 2;
                    aVar = null;
                    break;
                case 6:
                    this.i[0] = dataInputStream.readFloat();
                    this.i[1] = dataInputStream.readFloat();
                    this.i[2] = dataInputStream.readFloat();
                    cVar = null;
                    aVar = null;
                    break;
                case 7:
                    this.j[0] = dataInputStream.readFloat();
                    this.j[1] = dataInputStream.readFloat();
                    this.j[2] = dataInputStream.readFloat();
                    cVar = null;
                    aVar = null;
                    break;
                case 8:
                    this.l = dataInputStream.readFloat();
                    this.m = dataInputStream.readFloat();
                    cVar = null;
                    aVar = null;
                    break;
                default:
                    cVar = null;
                    aVar = null;
                    break;
            }
            g.a.a(new b(cVar, aVar));
        }
    }

    public final float a() {
        return this.i[0];
    }

    public final float b() {
        return this.i[1];
    }

    public final float c() {
        return this.i[2];
    }

    public final int a(int i2) {
        return this.b[i2];
    }

    public final int b(int i2) {
        return this.c[i2];
    }

    public final boolean c(int i2) {
        return this.d[i2];
    }

    public final void a(f fVar) {
        this.f = fVar;
    }
}
