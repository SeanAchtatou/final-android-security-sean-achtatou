package com.apperhand.common.dto;

import java.util.List;
import java.util.Locale;

public class ApplicationDetails extends BaseDTO {
    private static final long serialVersionUID = 6231432081614762071L;
    private List<String> abTests;
    private String applicationId;
    private Build build;
    private String developerId;
    private String deviceId;
    private DisplayMetrics displayMetrics;
    private Locale locale;
    private String protocolVersion;
    private String sourceIp;
    private String userAgent;

    public List<String> getAbTests() {
        return this.abTests;
    }

    public void setAbTests(List<String> list) {
        this.abTests = list;
    }

    public String getApplicationId() {
        return this.applicationId;
    }

    public void setApplicationId(String str) {
        this.applicationId = str;
    }

    public String getUserAgent() {
        return this.userAgent;
    }

    public void setUserAgent(String str) {
        this.userAgent = str;
    }

    public String getDeviceId() {
        return this.deviceId;
    }

    public void setDeviceId(String str) {
        this.deviceId = str;
    }

    public String getProtocolVersion() {
        return this.protocolVersion;
    }

    public void setProtocolVersion(String str) {
        this.protocolVersion = str;
    }

    public Locale getLocale() {
        return this.locale;
    }

    public void setLocale(Locale locale2) {
        this.locale = locale2;
    }

    public DisplayMetrics getDisplayMetrics() {
        return this.displayMetrics;
    }

    public void setDisplayMetrics(DisplayMetrics displayMetrics2) {
        this.displayMetrics = displayMetrics2;
    }

    public Build getBuild() {
        return this.build;
    }

    public void setBuild(Build build2) {
        this.build = build2;
    }

    public String getDeveloperId() {
        return this.developerId;
    }

    public void setDeveloperId(String str) {
        this.developerId = str;
    }

    public String getSourceIp() {
        return this.sourceIp;
    }

    public void setSourceIp(String str) {
        this.sourceIp = str;
    }

    public String toString() {
        return "ApplicationDetails [applicationId=" + this.applicationId + ", developerId=" + this.developerId + ", sourceIp=" + this.sourceIp + ", userAgent=" + this.userAgent + ", deviceId=" + this.deviceId + ", locale=" + this.locale + ", protocolVersion=" + this.protocolVersion + ", displayMetrics=" + this.displayMetrics + ", build=" + this.build + "]";
    }
}
