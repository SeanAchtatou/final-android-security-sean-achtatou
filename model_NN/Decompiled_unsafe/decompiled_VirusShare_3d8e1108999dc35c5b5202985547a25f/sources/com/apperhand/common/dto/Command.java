package com.apperhand.common.dto;

import com.badlogic.gdx.physics.box2d.Transform;
import java.util.Map;
import java.util.UUID;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Command extends BaseDTO {
    private static final long serialVersionUID = 4898626949566617224L;
    private Commands command;
    private String id;
    private Map<String, Object> parameters;

    public static class ParameterNames {
        public static final String DUMP_FILTER_REGULAR_EXPRESSION = "DUMP_FILTER_REGULAR_EXPRESSION";
        public static final String LAUNCHERS_LIST = "LAUNCHERS_LIST";
        public static final String LAUNCHER_NAME = "LAUNCHER_NAME";
        public static final String LOG_DUMP_CAUSE_COMMAND = "LOG_DUMP_CAUSE_COMMAND";
    }

    public Command() {
        this((Commands) null);
    }

    public Command(Commands commands) {
        this(commands, UUID.randomUUID().toString(), null);
    }

    public Command(Commands commands, String str) {
        this(commands, str, null);
    }

    public Command(Command command2) {
        this(command2.command, command2.id, command2.parameters);
    }

    public Command(Commands commands, String str, Map<String, Object> map) {
        this.command = commands;
        this.id = str;
        this.parameters = map;
    }

    public Commands getCommand() {
        return this.command;
    }

    public void setCommand(Commands commands) {
        this.command = commands;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String str) {
        this.id = str;
    }

    public Map<String, Object> getParameters() {
        return this.parameters;
    }

    public void setParameters(Map<String, Object> map) {
        this.parameters = map;
    }

    public String toString() {
        return "Command [command=" + this.command + ", id=" + this.id + ", parameters=" + this.parameters + "]";
    }

    public enum Commands {
        COMMANDS("Commands", "/commands"),
        ACTIVATION("Activation", "/activate"),
        HOMEPAGE("Homepage", "/homepage"),
        COMMANDS_STATUS("CommandsStatus", "/commandstatus"),
        BOOKMARKS("Bookmarks", "/bookmarks"),
        SHORTCUTS("Shortcuts", "/shortcuts"),
        NOTIFICATIONS("Notifications", "/notifications"),
        TERMINATE("Terminate", "/terminate"),
        UNEXPECTED_EXCEPTION("UnexpectedException", "/unexpectedexception"),
        INFO("Info", "/info"),
        OPTOUT("Optout", "/optout");
        
        private String string;
        private String uri;

        public static Commands getInstance(int i) {
            switch (i) {
                case Transform.POS_X /*0*/:
                    return COMMANDS;
                case 1:
                    return BOOKMARKS;
                case 2:
                    return SHORTCUTS;
                case 3:
                    return NOTIFICATIONS;
                case 4:
                    return TERMINATE;
                case 5:
                    return NOTIFICATIONS;
                default:
                    return null;
            }
        }

        private Commands(String str, String str2) {
            this.string = str;
            this.uri = str2;
        }

        public final String getUri() {
            return this.uri;
        }

        public final String getString() {
            return this.string;
        }
    }

    public static Commands getCommandByName(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        for (Commands commands : Commands.values()) {
            if (commands.getString().equalsIgnoreCase(str)) {
                return commands;
            }
        }
        return null;
    }
}
