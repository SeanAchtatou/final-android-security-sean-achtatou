package com.apperhand.common.dto;

import com.apperhand.common.dto.Command;
import java.util.List;

public class CommandInformation extends BaseDTO {
    private static final long serialVersionUID = -7074177583024278553L;
    private List<AssetInformation> assets;
    private Command.Commands command;
    private String message;
    private boolean valid;

    public CommandInformation() {
    }

    public void setCommand(Command.Commands commands) {
        this.command = commands;
    }

    public CommandInformation(Command.Commands commands) {
        this.command = commands;
    }

    public Command.Commands getCommand() {
        return this.command;
    }

    public boolean isValid() {
        return this.valid;
    }

    public void setValid(boolean z) {
        this.valid = z;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String str) {
        this.message = str;
    }

    public List<AssetInformation> getAssets() {
        return this.assets;
    }

    public void setAssets(List<AssetInformation> list) {
        this.assets = list;
    }

    public String toString() {
        return "CommandInformation [command=" + this.command + ", valid=" + this.valid + ", message=" + this.message + ", assets=" + this.assets + "]";
    }
}
