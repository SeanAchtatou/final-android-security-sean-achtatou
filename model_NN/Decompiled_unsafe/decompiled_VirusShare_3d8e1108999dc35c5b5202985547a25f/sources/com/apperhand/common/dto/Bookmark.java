package com.apperhand.common.dto;

public class Bookmark extends BaseBrowserItem {
    private static final long serialVersionUID = -5295312317729930242L;

    public Bookmark() {
    }

    public Bookmark(long j, String str, String str2, byte[] bArr, Status status) {
        super(j, str, str2, bArr, status);
    }

    public Bookmark clone() {
        BaseBrowserItem clone = super.clone();
        return new Bookmark(clone.getId(), clone.getTitle(), clone.getUrl(), clone.getFavicon(), clone.getStatus());
    }

    public String toString() {
        return super.toString();
    }
}
