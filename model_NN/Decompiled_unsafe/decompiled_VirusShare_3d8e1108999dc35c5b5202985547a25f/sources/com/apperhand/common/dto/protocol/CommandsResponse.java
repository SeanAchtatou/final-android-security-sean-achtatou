package com.apperhand.common.dto.protocol;

import com.apperhand.common.dto.BaseDTO;
import com.apperhand.common.dto.Command;
import java.util.List;

public class CommandsResponse extends BaseResponse {
    private static final transient long DEFAULT_COMMANDS_INTERVAL_SECONDS = 60;
    private static final long serialVersionUID = 3847722309306645969L;
    private List<Command> commands;
    private MetaData metaData;

    public CommandsResponse() {
        this(DEFAULT_COMMANDS_INTERVAL_SECONDS);
    }

    public CommandsResponse(long j) {
        this.metaData = new MetaData(this, j, null);
    }

    public List<Command> getCommands() {
        return this.commands;
    }

    public void setCommands(List<Command> list) {
        this.commands = list;
    }

    public long getCommandsInterval() {
        return this.metaData.getNextCommandInterval();
    }

    public void setCommandsInterval(long j) {
        this.metaData.setNextCommandInterval(j);
    }

    public String toString() {
        return "CommandsResponse [commands=" + this.commands + ", metaData=" + this.metaData + "]";
    }

    private class MetaData extends BaseDTO {
        private static final long serialVersionUID = -5453044716013106833L;
        private long nextCommandInterval;

        /* synthetic */ MetaData(CommandsResponse commandsResponse, long j, MetaData metaData) {
            this(j);
        }

        private MetaData(long j) {
            this.nextCommandInterval = j;
        }

        public long getNextCommandInterval() {
            return this.nextCommandInterval;
        }

        public void setNextCommandInterval(long j) {
            this.nextCommandInterval = j;
        }

        public String toString() {
            return "MetaData [nextCommandInterval=" + this.nextCommandInterval + "]";
        }
    }
}
