package com.apperhand.common.dto;

public class BaseBrowserItem extends BaseDTO implements Cloneable {
    private static final long serialVersionUID = 4764842093155994768L;
    private byte[] favicon;
    private long id;
    private Status status;
    private String title;
    private String url;

    public BaseBrowserItem() {
    }

    public BaseBrowserItem(long j, String str, String str2, byte[] bArr, Status status2) {
        this.id = j;
        this.title = str;
        this.url = str2;
        this.favicon = bArr;
        this.status = status2;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long j) {
        this.id = j;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String str) {
        this.title = str;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String str) {
        this.url = str;
    }

    public byte[] getFavicon() {
        return this.favicon;
    }

    public void setFavicon(byte[] bArr) {
        this.favicon = bArr;
    }

    public Status getStatus() {
        return this.status;
    }

    public void setStatus(Status status2) {
        this.status = status2;
    }

    public BaseBrowserItem clone() {
        BaseBrowserItem baseBrowserItem = new BaseBrowserItem();
        baseBrowserItem.setId(getId());
        baseBrowserItem.setTitle(getTitle());
        baseBrowserItem.setUrl(getUrl());
        baseBrowserItem.setStatus(getStatus());
        if (getFavicon() != null) {
            baseBrowserItem.setFavicon(new byte[getFavicon().length]);
            System.arraycopy(getFavicon(), 0, baseBrowserItem.getFavicon(), 0, getFavicon().length);
        }
        return baseBrowserItem;
    }

    public String toString() {
        return "BaseBrowserItem [id=" + this.id + ", title=" + this.title + ", url=" + this.url + ", status=" + this.status + "]";
    }
}
