package com.apperhand.common.dto.protocol;

import com.apperhand.common.dto.BaseDTO;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class BaseResponse extends BaseDTO {
    private static final long serialVersionUID = -1535279639837531044L;
    protected List<String> abTests;
    protected Map<String, String> parameters = new HashMap();
    protected boolean validResponse = true;

    public Map<String, String> getParameters() {
        return this.parameters;
    }

    public List<String> getAbTests() {
        return this.abTests;
    }

    public void setAbTests(List<String> list) {
        this.abTests = list;
    }

    public boolean isValidResponse() {
        return this.validResponse;
    }

    public void setValidResponse(boolean z) {
        this.validResponse = z;
    }

    public void setParameters(Map<String, String> map) {
        this.parameters = map;
    }

    public String toString() {
        return "BaseResponse [parameters=" + this.parameters + ", abTests=" + this.abTests + ", validResponse=" + this.validResponse + "]";
    }
}
