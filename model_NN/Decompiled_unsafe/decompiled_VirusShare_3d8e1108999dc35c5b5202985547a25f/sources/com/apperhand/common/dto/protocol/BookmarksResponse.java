package com.apperhand.common.dto.protocol;

import com.apperhand.common.dto.Bookmark;
import java.util.List;

public class BookmarksResponse extends BaseResponse {
    private static final long serialVersionUID = -5041274328910999390L;
    private List<Bookmark> bookmarks;

    public List<Bookmark> getBookmarks() {
        return this.bookmarks;
    }

    public void setBookmarks(List<Bookmark> list) {
        this.bookmarks = list;
    }

    public String toString() {
        return "BookmarksResponse [bookmarks=" + this.bookmarks + ", toString()=" + super.toString() + "]";
    }
}
