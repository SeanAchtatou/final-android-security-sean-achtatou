package com.apperhand.device.a.a;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class f {
    public static String a(String str) {
        int indexOf;
        if (str == null || str.equals("") || (indexOf = str.indexOf("?")) <= 0) {
            return str;
        }
        return str.substring(0, indexOf);
    }

    public static String b(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-1");
            instance.reset();
            return c.a(instance.digest(str.getBytes()));
        } catch (NoSuchAlgorithmException e) {
            return str;
        }
    }
}
