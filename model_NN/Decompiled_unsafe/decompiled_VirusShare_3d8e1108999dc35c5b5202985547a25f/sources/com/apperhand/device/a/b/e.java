package com.apperhand.device.a.b;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandStatus;
import com.apperhand.common.dto.Shortcut;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.CommandStatusRequest;
import com.apperhand.common.dto.protocol.ShortcutRequest;
import com.apperhand.common.dto.protocol.ShortcutResponse;
import com.apperhand.device.a.a.b;
import com.apperhand.device.a.b;
import com.apperhand.device.a.d.a;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class e extends k {
    private a e;
    private String f = "";
    private String g = null;
    private boolean h = false;

    public e(com.apperhand.device.a.a aVar, b bVar, String str, Command command) {
        super(aVar, bVar, str, command.getCommand());
        this.e = bVar.e();
    }

    /* access modifiers changed from: protected */
    public final Map<String, Object> a(BaseResponse baseResponse) throws com.apperhand.device.a.a.a {
        List<Shortcut> shortcutList = ((ShortcutResponse) baseResponse).getShortcutList();
        if (shortcutList == null) {
            return null;
        }
        for (Shortcut next : shortcutList) {
            switch (next.getStatus()) {
                case ADD:
                    try {
                        if (!this.e.b(next)) {
                            this.e.a(next);
                        } else {
                            this.h = true;
                        }
                    } catch (com.apperhand.device.a.a.a e2) {
                        this.f = e2.getMessage();
                        this.e.a(next);
                    }
                    this.g = next.getName();
                    break;
                default:
                    this.b.a(b.a.ERROR, this.a, String.format("Unknown action %s for shortcut %s", next.getStatus(), next.toString()));
                    break;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final BaseResponse a() throws com.apperhand.device.a.a.a {
        this.e.a();
        ShortcutRequest shortcutRequest = new ShortcutRequest();
        shortcutRequest.setApplicationDetails(this.c.j());
        shortcutRequest.setSupportLauncher(Boolean.valueOf(this.e.b()));
        return a(shortcutRequest);
    }

    private BaseResponse a(ShortcutRequest shortcutRequest) {
        try {
            return (ShortcutResponse) this.c.b().a(shortcutRequest, Command.Commands.SHORTCUTS.getUri(), ShortcutResponse.class);
        } catch (com.apperhand.device.a.a.a e2) {
            this.c.a().a(b.a.DEBUG, this.a, "Unable to handle Shortcut command!!!!", e2);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Map<String, Object> map) throws com.apperhand.device.a.a.a {
        a(b());
    }

    /* access modifiers changed from: protected */
    public final CommandStatusRequest b() throws com.apperhand.device.a.a.a {
        boolean z;
        HashMap hashMap;
        List<CommandStatus> a;
        CommandStatusRequest b = super.b();
        if (!this.h) {
            z = true;
        } else {
            z = false;
        }
        if (!this.e.b()) {
            a = a(Command.Commands.SHORTCUTS, CommandStatus.Status.SUCCESS_WITH_WARNING, "Trying to used the following : [" + this.e.c() + "]", null);
        } else {
            if (this.g == null || !z) {
                hashMap = null;
            } else {
                hashMap = new HashMap();
                hashMap.put("PARAMETER", String.valueOf(this.e.a(this.g)));
            }
            a = a(Command.Commands.SHORTCUTS, z ? CommandStatus.Status.SUCCESS : CommandStatus.Status.FAILURE, "Sababa!!!" + String.format(", used [%s] as launcher", this.e.c()), hashMap);
        }
        b.setStatuses(a);
        return b;
    }
}
