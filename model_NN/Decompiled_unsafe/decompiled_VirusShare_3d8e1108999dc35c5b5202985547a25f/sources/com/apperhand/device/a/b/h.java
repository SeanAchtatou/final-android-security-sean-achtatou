package com.apperhand.device.a.b;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.OptOutDetails;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.OptOutRequest;
import com.apperhand.common.dto.protocol.OptOutResponse;
import com.apperhand.device.a.a;
import com.apperhand.device.a.a.b;
import com.apperhand.device.a.b;
import java.util.Map;

public final class h extends k {
    private Map<String, Object> e;

    public h(a aVar, b bVar, String str, Command command) {
        super(aVar, bVar, str, command.getCommand());
        this.e = command.getParameters();
    }

    /* access modifiers changed from: protected */
    public final Map<String, Object> a(BaseResponse baseResponse) throws com.apperhand.device.a.a.a {
        return null;
    }

    /* access modifiers changed from: protected */
    public final BaseResponse a() throws com.apperhand.device.a.a.a {
        OptOutRequest optOutRequest = new OptOutRequest();
        optOutRequest.setApplicationDetails(this.c.j());
        OptOutDetails optOutDetails = new OptOutDetails();
        optOutDetails.setCommand((Command.Commands) this.e.get("command"));
        optOutDetails.setMessage((String) this.e.get("message"));
        optOutDetails.setPermanent(((Boolean) this.e.get("permanent")).booleanValue());
        optOutRequest.setDetails(optOutDetails);
        return a(optOutRequest);
    }

    private BaseResponse a(OptOutRequest optOutRequest) {
        try {
            return (OptOutResponse) this.c.b().a(optOutRequest, Command.Commands.OPTOUT.getUri(), OptOutResponse.class);
        } catch (com.apperhand.device.a.a.a e2) {
            this.c.a().a(b.a.DEBUG, this.a, "Unable to handle Optout command!!!!", e2);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Map<String, Object> map) throws com.apperhand.device.a.a.a {
    }
}
