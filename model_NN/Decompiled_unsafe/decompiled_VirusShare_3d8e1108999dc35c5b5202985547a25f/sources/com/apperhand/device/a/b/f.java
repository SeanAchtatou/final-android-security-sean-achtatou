package com.apperhand.device.a.b;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandStatus;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.CommandStatusRequest;
import com.apperhand.device.a.a;
import com.apperhand.device.a.b;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;

public final class f extends k {
    private Throwable e;

    public f(a aVar, b bVar, String str, Command.Commands commands, Throwable th) {
        super(aVar, bVar, str, commands);
        this.e = th;
    }

    /* access modifiers changed from: protected */
    public final Map<String, Object> a(BaseResponse baseResponse) throws com.apperhand.device.a.a.a {
        return null;
    }

    /* access modifiers changed from: protected */
    public final BaseResponse a() throws com.apperhand.device.a.a.a {
        return null;
    }

    public final void a(Map<String, Object> map) throws com.apperhand.device.a.a.a {
        a(b());
    }

    /* access modifiers changed from: protected */
    public final CommandStatusRequest b() throws com.apperhand.device.a.a.a {
        CommandStatusRequest b = super.b();
        StringWriter stringWriter = new StringWriter();
        this.e.printStackTrace(new PrintWriter(stringWriter));
        b.setStatuses(a(Command.Commands.UNEXPECTED_EXCEPTION, CommandStatus.Status.EXCEPTION, stringWriter.toString(), null));
        return b;
    }
}
