package com.apperhand.device.a.b;

import com.apperhand.common.dto.Activation;
import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandStatus;
import com.apperhand.common.dto.protocol.ActivationRequest;
import com.apperhand.common.dto.protocol.ActivationResponse;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.CommandStatusRequest;
import com.apperhand.device.a.a;
import com.apperhand.device.a.a.b;
import com.apperhand.device.a.b;
import com.apperhand.device.a.d.c;
import java.util.Map;

public final class j extends k {
    private c e;

    public j(a aVar, b bVar, String str, Command.Commands commands) {
        super(aVar, bVar, str, commands);
        this.e = bVar.c();
    }

    private ActivationResponse a(ActivationRequest activationRequest) {
        try {
            return (ActivationResponse) this.c.b().a(activationRequest, Command.Commands.ACTIVATION.getUri(), ActivationResponse.class);
        } catch (com.apperhand.device.a.a.a e2) {
            this.c.a().a(b.a.DEBUG, this.a, "Unable to handle Activation command!!!!", e2);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final Map<String, Object> a(BaseResponse baseResponse) throws com.apperhand.device.a.a.a {
        Activation activation = ((ActivationResponse) baseResponse).getActivation();
        Map<String, String> parameters = activation.getParameters();
        this.c.i().b("ACTIVATED", "true");
        if (parameters != null && parameters.size() > 0) {
            for (String next : parameters.keySet()) {
                this.c.i().b(next, parameters.get(next));
            }
        }
        activation.getEula();
        return null;
    }

    /* access modifiers changed from: protected */
    public final BaseResponse a() {
        ActivationRequest activationRequest = new ActivationRequest();
        activationRequest.setApplicationDetails(this.c.j());
        activationRequest.setMissingParameters(this.c.i().b());
        activationRequest.setFirstTimeActivation(!Boolean.getBoolean(this.c.i().a("ACTIVATED", "false")));
        return a(activationRequest);
    }

    /* access modifiers changed from: protected */
    public final void a(Map<String, Object> map) throws com.apperhand.device.a.a.a {
        a(b());
    }

    /* access modifiers changed from: protected */
    public final CommandStatusRequest b() throws com.apperhand.device.a.a.a {
        CommandStatusRequest b = super.b();
        b.setStatuses(a(Command.Commands.ACTIVATION, CommandStatus.Status.SUCCESS, this.c.k() + " was activated, SABABA!!!", null));
        return b;
    }
}
