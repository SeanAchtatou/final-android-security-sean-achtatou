package com.apperhand.device.a.b;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandInformation;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.InfoRequest;
import com.apperhand.common.dto.protocol.InfoResponse;
import com.apperhand.device.a.a.b;
import com.apperhand.device.a.b;
import com.apperhand.device.a.d.a;
import com.apperhand.device.a.d.d;
import com.apperhand.device.a.d.e;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class c extends k {
    private a e;
    private d f;
    private e g;
    private Map<String, Object> h;

    public c(com.apperhand.device.a.a aVar, b bVar, String str, Command command) {
        super(aVar, bVar, str, command.getCommand());
        this.e = bVar.e();
        this.f = bVar.d();
        this.g = bVar.g();
        this.h = command.getParameters();
    }

    /* access modifiers changed from: protected */
    public final Map<String, Object> a(BaseResponse baseResponse) throws com.apperhand.device.a.a.a {
        return null;
    }

    /* access modifiers changed from: protected */
    public final BaseResponse a() throws com.apperhand.device.a.a.a {
        CommandInformation a;
        InfoRequest infoRequest = new InfoRequest();
        ArrayList arrayList = new ArrayList();
        infoRequest.setApplicationDetails(this.c.j());
        infoRequest.setInformation(arrayList);
        for (String str : this.h.keySet()) {
            List list = (List) this.h.get(str);
            if (list != null) {
                switch (Command.getCommandByName(str)) {
                    case SHORTCUTS:
                        this.e.a();
                        a = this.e.a(list);
                        break;
                    case BOOKMARKS:
                        a = this.f.a(list);
                        break;
                    case NOTIFICATIONS:
                        a = this.g.a();
                        break;
                    default:
                        a = null;
                        break;
                }
                arrayList.add(a);
            }
        }
        return a(infoRequest);
    }

    private BaseResponse a(InfoRequest infoRequest) {
        try {
            return (InfoResponse) this.c.b().a(infoRequest, Command.Commands.INFO.getUri(), InfoResponse.class);
        } catch (com.apperhand.device.a.a.a e2) {
            this.c.a().a(b.a.DEBUG, this.a, "Unable to handle Info command!!!!", e2);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Map<String, Object> map) throws com.apperhand.device.a.a.a {
    }
}
