package com.apperhand.device.a.a;

import com.apperhand.common.dto.protocol.BaseResponse;
import java.util.List;

public final class d {
    public static String a(BaseResponse baseResponse) {
        if (baseResponse == null || baseResponse.getAbTests() == null) {
            return null;
        }
        List<String> abTests = baseResponse.getAbTests();
        StringBuffer stringBuffer = new StringBuffer();
        for (String next : abTests) {
            if (stringBuffer.length() > 0) {
                stringBuffer.append(',');
            }
            stringBuffer.append(next);
        }
        return stringBuffer.toString();
    }
}
