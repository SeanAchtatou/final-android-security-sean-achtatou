package com.apperhand.device.a.b;

import com.apperhand.common.dto.Command;
import com.apperhand.device.a.a;

public final class b {
    private b() {
    }

    public static k a(a aVar, Command command, com.apperhand.device.a.b bVar) {
        switch (command.getCommand()) {
            case ACTIVATION:
                return new j(aVar, bVar, command.getId(), command.getCommand());
            case BOOKMARKS:
                return new d(aVar, bVar, command.getId(), command.getCommand());
            case SHORTCUTS:
                return new e(aVar, bVar, command.getId(), command);
            case NOTIFICATIONS:
                return new i(aVar, bVar, command.getId(), command.getCommand());
            case TERMINATE:
                return new a(aVar, bVar, command.getId(), command.getCommand());
            case INFO:
                return new c(aVar, bVar, command.getId(), command);
            case OPTOUT:
                return new h(aVar, bVar, command.getId(), command);
            case HOMEPAGE:
                return new g(aVar, bVar, command.getId(), command);
            default:
                return null;
        }
    }
}
