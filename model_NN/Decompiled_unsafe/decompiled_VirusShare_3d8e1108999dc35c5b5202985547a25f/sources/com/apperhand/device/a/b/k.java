package com.apperhand.device.a.b;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandStatus;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.CommandStatusRequest;
import com.apperhand.common.dto.protocol.CommandStatusResponse;
import com.apperhand.device.a.a;
import com.apperhand.device.a.a.b;
import com.apperhand.device.a.a.d;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class k {
    protected final String a = getClass().getSimpleName();
    protected final b b;
    protected com.apperhand.device.a.b c;
    protected a d;
    private String e;
    private Command.Commands f;

    /* access modifiers changed from: protected */
    public abstract BaseResponse a() throws com.apperhand.device.a.a.a;

    /* access modifiers changed from: protected */
    public abstract Map<String, Object> a(BaseResponse baseResponse) throws com.apperhand.device.a.a.a;

    /* access modifiers changed from: protected */
    public abstract void a(Map<String, Object> map) throws com.apperhand.device.a.a.a;

    public k(a aVar, com.apperhand.device.a.b bVar, String str, Command.Commands commands) {
        this.d = aVar;
        this.c = bVar;
        this.b = bVar.a();
        this.e = str;
        this.f = commands;
    }

    public final void c() {
        this.b.a(b.a.INFO, this.a, "Entering execute()");
        try {
            BaseResponse a2 = a();
            if (a2 == null || !a2.isValidResponse()) {
                this.b.a(b.a.INFO, this.a, "Server Error in " + this.f.name());
                CommandStatusRequest b2 = b();
                b2.setStatuses(a(this.f, CommandStatus.Status.FAILURE, "Got server error", null));
                a(b2);
                return;
            }
            this.d.b(d.a(a2));
            a(a(a2));
        } catch (com.apperhand.device.a.a.a e2) {
            this.b.a(b.a.ERROR, this.a, "Unable to send command", e2);
            try {
                CommandStatusRequest commandStatusRequest = new CommandStatusRequest();
                commandStatusRequest.setApplicationDetails(this.c.j());
                commandStatusRequest.setStatuses(a(this.f, CommandStatus.Status.FAILURE, e2.toString(), null));
                a(commandStatusRequest);
            } catch (com.apperhand.device.a.a.a e3) {
                this.b.a(b.a.ERROR, this.a, "Unable to send exception status command", e3);
            }
        }
    }

    /* access modifiers changed from: protected */
    public CommandStatusRequest b() throws com.apperhand.device.a.a.a {
        CommandStatusRequest commandStatusRequest = new CommandStatusRequest();
        commandStatusRequest.setApplicationDetails(this.c.j());
        return commandStatusRequest;
    }

    /* access modifiers changed from: protected */
    public final List<CommandStatus> a(Command.Commands commands, CommandStatus.Status status, String str, Map<String, Object> map) {
        ArrayList arrayList = new ArrayList(1);
        CommandStatus commandStatus = new CommandStatus();
        commandStatus.setCommand(commands);
        commandStatus.setId(this.e);
        commandStatus.setMessage(str);
        commandStatus.setStatus(status);
        commandStatus.setParameters(map);
        arrayList.add(commandStatus);
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public final void a(CommandStatusRequest commandStatusRequest) throws com.apperhand.device.a.a.a {
        try {
            CommandStatusResponse commandStatusResponse = (CommandStatusResponse) this.c.b().a(commandStatusRequest, Command.Commands.COMMANDS_STATUS.getUri(), CommandStatusResponse.class);
            this.d.a(commandStatusResponse.getNextCommandInterval());
            this.d.b(d.a(commandStatusResponse));
        } catch (com.apperhand.device.a.a.a e2) {
            this.c.a().a(b.a.DEBUG, this.a, String.format("Unable to send command status for command [%s]!!!!", this.f.getString()), e2);
        }
    }
}
