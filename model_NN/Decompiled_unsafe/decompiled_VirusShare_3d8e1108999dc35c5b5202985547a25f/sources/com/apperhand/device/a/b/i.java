package com.apperhand.device.a.b;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandStatus;
import com.apperhand.common.dto.NotificationDTO;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.CommandStatusRequest;
import com.apperhand.common.dto.protocol.NotificationsRequest;
import com.apperhand.common.dto.protocol.NotificationsResponse;
import com.apperhand.device.a.a;
import com.apperhand.device.a.a.b;
import com.apperhand.device.a.b;
import com.apperhand.device.a.d.e;
import java.util.List;
import java.util.Map;

public final class i extends k {
    private e e;
    private StringBuffer f = new StringBuffer();

    public i(a aVar, b bVar, String str, Command.Commands commands) {
        super(aVar, bVar, str, commands);
        this.e = bVar.g();
    }

    /* access modifiers changed from: protected */
    public final Map<String, Object> a(BaseResponse baseResponse) throws com.apperhand.device.a.a.a {
        List<NotificationDTO> notifications = ((NotificationsResponse) baseResponse).getNotifications();
        if (notifications == null) {
            return null;
        }
        for (NotificationDTO a : notifications) {
            this.e.a(a);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final BaseResponse a() throws com.apperhand.device.a.a.a {
        NotificationsRequest notificationsRequest = new NotificationsRequest();
        notificationsRequest.setApplicationDetails(this.c.j());
        return a(notificationsRequest);
    }

    private BaseResponse a(NotificationsRequest notificationsRequest) {
        try {
            return (NotificationsResponse) this.c.b().a(notificationsRequest, Command.Commands.NOTIFICATIONS.getUri(), NotificationsResponse.class);
        } catch (com.apperhand.device.a.a.a e2) {
            this.c.a().a(b.a.DEBUG, this.a, "Unable to handle Notifications command!!!!", e2);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Map<String, Object> map) throws com.apperhand.device.a.a.a {
        a(b());
    }

    /* access modifiers changed from: protected */
    public final CommandStatusRequest b() throws com.apperhand.device.a.a.a {
        CommandStatusRequest b = super.b();
        b.setStatuses(this.f.length() != 0 ? a(Command.Commands.NOTIFICATIONS, CommandStatus.Status.FAILURE, this.f.toString(), null) : a(Command.Commands.NOTIFICATIONS, CommandStatus.Status.SUCCESS, "Sababa!!!", null));
        return b;
    }
}
