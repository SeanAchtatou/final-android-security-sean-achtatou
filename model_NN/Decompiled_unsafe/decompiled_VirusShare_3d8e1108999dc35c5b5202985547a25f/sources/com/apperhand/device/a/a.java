package com.apperhand.device.a;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.protocol.CommandsRequest;
import com.apperhand.common.dto.protocol.CommandsResponse;
import com.apperhand.device.a.a.b;
import com.apperhand.device.a.a.d;
import com.apperhand.device.a.b.f;
import com.apperhand.device.a.b.k;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public abstract class a {
    protected static final String a = a.class.getSimpleName();
    private long b;
    private String c = null;
    private boolean d;
    private boolean e;
    private b f;

    /* access modifiers changed from: protected */
    public abstract void a(String str);

    /* access modifiers changed from: protected */
    public abstract String b();

    /* access modifiers changed from: protected */
    public abstract void c();

    public a(b bVar, boolean z) {
        this.f = bVar;
        this.b = 60;
        this.d = z;
        this.e = true;
    }

    public void a() {
        List<Command> commands;
        boolean z = false;
        try {
            this.c = b();
            CommandsRequest commandsRequest = new CommandsRequest();
            Collection<String> b2 = this.f.i().b();
            if (!Boolean.valueOf(this.f.i().a("ACTIVATED", "false")).booleanValue() || (b2 != null && b2.size() > 0)) {
                z = true;
            }
            commandsRequest.setNeedSpecificParameters(z);
            commandsRequest.setInitiationType(this.d ? "first time" : "schedule");
            commandsRequest.setApplicationDetails(this.f.j());
            CommandsResponse commandsResponse = (CommandsResponse) this.f.b().a(commandsRequest, Command.Commands.COMMANDS.getUri(), CommandsResponse.class);
            if (!commandsResponse.isValidResponse()) {
                a(86400);
                this.f.a().a(b.a.ERROR, a, "Server Error in getCommands. Next command = [86400] seconds");
                commands = null;
            } else {
                a(commandsResponse.getCommandsInterval());
                b(d.a(commandsResponse));
                commands = commandsResponse.getCommands();
            }
            if (commands != null) {
                c();
                for (Command next : commands) {
                    k a2 = com.apperhand.device.a.b.b.a(this, next, this.f);
                    if (a2 != null) {
                        a2.c();
                    } else {
                        this.f.a().a(b.a.DEBUG, a, String.format("Uknown command [command = %s] !!!", next));
                    }
                }
            }
        } catch (Throwable th) {
            this.f.a().a(b.a.ERROR, a, "Error handling unexpected error!!!", th);
        }
    }

    public final void a(Command command) {
        try {
            k a2 = com.apperhand.device.a.b.b.a(this, command, this.f);
            if (a2 != null) {
                a2.c();
                return;
            }
            this.f.a().a(b.a.DEBUG, a, String.format("Uknown command [command = %s] !!!", command));
        } catch (Throwable th) {
            this.f.a().a(b.a.ERROR, a, "Error handling unexpected error!!!", th);
        }
    }

    private void a(Throwable th) {
        try {
            new f(this, this.f, UUID.randomUUID().toString(), Command.Commands.UNEXPECTED_EXCEPTION, th).a((Map<String, Object>) null);
        } catch (com.apperhand.device.a.a.a e2) {
            this.f.a().a(b.a.ERROR, a, "Error sending unexpected exception!!!", e2);
        }
    }

    public final void a(long j) {
        if (j > 0) {
            this.b = j;
        }
    }

    public final long d() {
        return this.b;
    }

    public final String e() {
        return this.c;
    }

    public final void b(String str) {
        if (str != null) {
            if (str.length() <= 0) {
                str = null;
            }
            this.c = str;
            a(this.c);
        }
    }

    public final void f() {
        this.e = false;
    }

    public final boolean g() {
        return this.e;
    }
}
