package com.apperhand.device.a.b;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandStatus;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.CommandStatusRequest;
import com.apperhand.device.a.b;
import java.util.Map;

public final class a extends k {
    public a(com.apperhand.device.a.a aVar, b bVar, String str, Command.Commands commands) {
        super(aVar, bVar, str, commands);
    }

    /* access modifiers changed from: protected */
    public final Map<String, Object> a(BaseResponse baseResponse) throws com.apperhand.device.a.a.a {
        return null;
    }

    /* access modifiers changed from: protected */
    public final BaseResponse a() throws com.apperhand.device.a.a.a {
        this.c.f().a();
        this.d.f();
        return new BaseResponse() {
            public final String toString() {
                return new StringBuilder("DummyResponse").toString();
            }
        };
    }

    /* access modifiers changed from: protected */
    public final void a(Map<String, Object> map) throws com.apperhand.device.a.a.a {
        a(b());
    }

    /* access modifiers changed from: protected */
    public final CommandStatusRequest b() throws com.apperhand.device.a.a.a {
        CommandStatusRequest b = super.b();
        b.setStatuses(a(Command.Commands.TERMINATE, CommandStatus.Status.SUCCESS, "SABABA!!!", null));
        return b;
    }
}
