package com.apperhand.device.a.a;

public final class a extends Exception {
    private C0000a a;

    public a() {
    }

    public a(C0000a aVar, String str, Throwable th) {
        super(str, th);
        this.a = aVar;
    }

    public a(C0000a aVar, String str) {
        this(aVar, str, null);
    }

    /* renamed from: com.apperhand.device.a.a.a$a  reason: collision with other inner class name */
    public enum C0000a {
        GENERAL_ERROR(1, "general error"),
        SHORTCUT_ERROR(10, "shortcut error"),
        BOOKMARK_ERROR(20, "bookmark error"),
        HISTORY_ERROR(30, "history error"),
        NOTIFICATION_ERROR(40, "notification error");
        
        private long f;
        private String g;

        private C0000a(long j, String str) {
            this.f = j;
            this.g = str;
        }
    }
}
