package com.apperhand.device.a.b;

import com.apperhand.common.dto.Bookmark;
import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandStatus;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.BookmarksRequest;
import com.apperhand.common.dto.protocol.BookmarksResponse;
import com.apperhand.common.dto.protocol.CommandStatusRequest;
import com.apperhand.device.a.a;
import com.apperhand.device.a.a.b;
import com.apperhand.device.a.a.f;
import com.apperhand.device.a.b;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class d extends k {
    private com.apperhand.device.a.d.d e;
    private boolean f = false;
    private Set<String> g = new HashSet();

    public d(a aVar, b bVar, String str, Command.Commands commands) {
        super(aVar, bVar, str, commands);
        this.e = bVar.d();
    }

    /* access modifiers changed from: protected */
    public final Map<String, Object> a(BaseResponse baseResponse) throws com.apperhand.device.a.a.a {
        boolean z;
        List<Bookmark> bookmarks = ((BookmarksResponse) baseResponse).getBookmarks();
        List<Bookmark> a = this.e.a();
        if (bookmarks == null) {
            return null;
        }
        for (Bookmark next : bookmarks) {
            this.g.add(f.a(next.getUrl()));
            switch (next.getStatus()) {
                case ADD:
                    if (a != null) {
                        String a2 = f.a(next.getUrl());
                        Iterator<Bookmark> it = a.iterator();
                        while (true) {
                            if (it.hasNext()) {
                                if (a2.equals(f.a(it.next().getUrl()))) {
                                    z = true;
                                }
                            }
                        }
                    }
                    z = false;
                    if (z) {
                        this.f = true;
                        break;
                    } else {
                        this.e.a(next);
                        break;
                    }
                case DELETE:
                    this.e.b(next);
                    break;
                case UPDATE:
                    this.e.b();
                    break;
                default:
                    this.b.a(b.a.ERROR, this.a, String.format("Unknown action %s for bookmark %s", next.getStatus(), next.toString()));
                    break;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public final BaseResponse a() throws com.apperhand.device.a.a.a {
        BookmarksRequest bookmarksRequest = new BookmarksRequest();
        bookmarksRequest.setApplicationDetails(this.c.j());
        return a(bookmarksRequest);
    }

    private BaseResponse a(BookmarksRequest bookmarksRequest) {
        try {
            return (BookmarksResponse) this.c.b().a(bookmarksRequest, Command.Commands.BOOKMARKS.getUri(), BookmarksResponse.class);
        } catch (com.apperhand.device.a.a.a e2) {
            this.c.a().a(b.a.DEBUG, this.a, "Unable to handle Bookmarks command!!!!", e2);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Map<String, Object> map) throws com.apperhand.device.a.a.a {
        a(b());
    }

    /* access modifiers changed from: protected */
    public final CommandStatusRequest b() throws com.apperhand.device.a.a.a {
        String str;
        boolean z;
        CommandStatusRequest b = super.b();
        if (!this.f) {
            str = "Sababa!!!";
            z = true;
        } else {
            str = "Bookmark is already exist";
            z = false;
        }
        b.setStatuses(a(Command.Commands.BOOKMARKS, z ? CommandStatus.Status.SUCCESS : CommandStatus.Status.FAILURE, str, null));
        return b;
    }
}
