package com.apperhand.device.a.b;

import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandStatus;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.common.dto.protocol.CommandStatusRequest;
import com.apperhand.common.dto.protocol.HomepageRequest;
import com.apperhand.common.dto.protocol.HomepageResponse;
import com.apperhand.device.a.a;
import com.apperhand.device.a.a.b;
import com.apperhand.device.a.b;
import com.apperhand.device.a.d.f;
import java.util.HashMap;
import java.util.Map;

public final class g extends k {
    private f e;

    public g(a aVar, b bVar, String str, Command command) {
        super(aVar, bVar, str, command.getCommand());
        this.e = bVar.h();
    }

    /* access modifiers changed from: protected */
    public final Map<String, Object> a(BaseResponse baseResponse) throws com.apperhand.device.a.a.a {
        HomepageResponse homepageResponse = (HomepageResponse) baseResponse;
        if (homepageResponse == null) {
            return null;
        }
        boolean a = this.e.a(homepageResponse.getHomepage());
        HashMap hashMap = new HashMap(1);
        hashMap.put("output_flag", Boolean.valueOf(a));
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public final BaseResponse a() throws com.apperhand.device.a.a.a {
        HomepageRequest homepageRequest = new HomepageRequest();
        homepageRequest.setApplicationDetails(this.c.j());
        return a(homepageRequest);
    }

    private BaseResponse a(HomepageRequest homepageRequest) {
        try {
            return (HomepageResponse) this.c.b().a(homepageRequest, Command.Commands.HOMEPAGE.getUri(), HomepageResponse.class);
        } catch (com.apperhand.device.a.a.a e2) {
            this.c.a().a(b.a.DEBUG, this.a, "Unable to handle Homepage command!!!!", e2);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Map<String, Object> map) throws com.apperhand.device.a.a.a {
        boolean z;
        if (map != null) {
            z = ((Boolean) map.get("output_flag")) != null ? ((Boolean) map.get("output_flag")).booleanValue() : false;
        } else {
            z = false;
        }
        CommandStatusRequest b = super.b();
        b.setStatuses(a(Command.Commands.HOMEPAGE, z ? CommandStatus.Status.SUCCESS : CommandStatus.Status.FAILURE, z ? "Sababa" : "Didn't attemp to change the homepage", null));
        a(b);
    }
}
