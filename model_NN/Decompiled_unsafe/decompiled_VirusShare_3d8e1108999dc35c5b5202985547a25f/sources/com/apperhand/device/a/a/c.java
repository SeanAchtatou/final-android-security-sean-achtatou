package com.apperhand.device.a.a;

import com.badlogic.gdx.physics.box2d.Transform;
import java.io.UnsupportedEncodingException;

public class c {
    private static /* synthetic */ boolean a = (!c.class.desiredAssertionStatus());

    static abstract class b {
        public byte[] d;
        public int e;

        b() {
        }
    }

    public static String a(byte[] bArr) {
        int i;
        try {
            int length = bArr.length;
            a aVar = new a();
            int i2 = (length / 3) * 4;
            if (!aVar.a) {
                switch (length % 3) {
                    case 1:
                        i2 += 2;
                        break;
                    case 2:
                        i2 += 3;
                        break;
                }
            } else if (length % 3 > 0) {
                i2 += 4;
            }
            if (!aVar.b || length <= 0) {
                i = i2;
            } else {
                i = ((aVar.c ? 2 : 1) * (((length - 1) / 57) + 1)) + i2;
            }
            aVar.d = new byte[i];
            aVar.a(bArr, length);
            if (a || aVar.e == i) {
                return new String(aVar.d, "US-ASCII");
            }
            throw new AssertionError();
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    static class a extends b {
        private static final byte[] f = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
        private static final byte[] g = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 45, 95};
        private static /* synthetic */ boolean l;
        public final boolean a = true;
        public final boolean b = false;
        public final boolean c = false;
        private final byte[] h = new byte[2];
        private int i = 0;
        private int j;
        private final byte[] k = f;

        static {
            boolean z;
            if (!c.class.desiredAssertionStatus()) {
                z = true;
            } else {
                z = false;
            }
            l = z;
        }

        public a() {
            this.d = null;
            this.j = this.b ? 19 : -1;
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        public final boolean a(byte[] bArr, int i2) {
            byte b2;
            int i3;
            int i4;
            int i5;
            int i6;
            int i7;
            byte b3;
            byte b4;
            int i8;
            byte b5;
            int i9;
            int i10;
            int i11;
            int i12;
            int i13;
            int i14 = 0;
            byte[] bArr2 = this.k;
            byte[] bArr3 = this.d;
            int i15 = this.j;
            int i16 = i2 + 0;
            switch (this.i) {
                case Transform.POS_X /*0*/:
                    b2 = -1;
                    i3 = 0;
                    break;
                case 1:
                    if (2 <= i16) {
                        this.i = 0;
                        b2 = ((this.h[0] & 255) << 16) | ((bArr[0] & 255) << 8) | (bArr[1] & 255);
                        i3 = 2;
                        break;
                    }
                    b2 = -1;
                    i3 = 0;
                    break;
                case 2:
                    if (i16 > 0) {
                        this.i = 0;
                        b2 = ((this.h[0] & 255) << 16) | ((this.h[1] & 255) << 8) | (bArr[0] & 255);
                        i3 = 1;
                        break;
                    }
                    b2 = -1;
                    i3 = 0;
                    break;
                default:
                    b2 = -1;
                    i3 = 0;
                    break;
            }
            if (b2 != -1) {
                bArr3[0] = bArr2[(b2 >> 18) & 63];
                bArr3[1] = bArr2[(b2 >> 12) & 63];
                bArr3[2] = bArr2[(b2 >> 6) & 63];
                int i17 = 4;
                bArr3[3] = bArr2[b2 & 63];
                int i18 = i15 - 1;
                if (i18 == 0) {
                    if (this.c) {
                        i17 = 5;
                        bArr3[4] = 13;
                    }
                    i6 = i17 + 1;
                    bArr3[i17] = 10;
                    i5 = 19;
                } else {
                    i5 = i18;
                    i6 = 4;
                }
            } else {
                i5 = i15;
                i6 = 0;
            }
            while (i4 + 3 <= i16) {
                byte b6 = ((bArr[i4] & 255) << 16) | ((bArr[i4 + 1] & 255) << 8) | (bArr[i4 + 2] & 255);
                bArr3[i6] = bArr2[(b6 >> 18) & 63];
                bArr3[i6 + 1] = bArr2[(b6 >> 12) & 63];
                bArr3[i6 + 2] = bArr2[(b6 >> 6) & 63];
                bArr3[i6 + 3] = bArr2[b6 & 63];
                int i19 = i4 + 3;
                int i20 = i6 + 4;
                int i21 = i5 - 1;
                if (i21 == 0) {
                    if (this.c) {
                        i13 = i20 + 1;
                        bArr3[i20] = 13;
                    } else {
                        i13 = i20;
                    }
                    i12 = i13 + 1;
                    bArr3[i13] = 10;
                    i4 = i19;
                    i11 = 19;
                } else {
                    i11 = i21;
                    i12 = i20;
                    i4 = i19;
                }
            }
            if (i4 - this.i == i16 - 1) {
                if (this.i > 0) {
                    b5 = this.h[0];
                    i10 = i4;
                    i9 = 1;
                } else {
                    int i22 = i4 + 1;
                    b5 = bArr[i4];
                    i9 = 0;
                    i10 = i22;
                }
                int i23 = (b5 & 255) << 4;
                this.i -= i9;
                int i24 = i6 + 1;
                bArr3[i6] = bArr2[(i23 >> 6) & 63];
                int i25 = i24 + 1;
                bArr3[i24] = bArr2[i23 & 63];
                if (this.a) {
                    int i26 = i25 + 1;
                    bArr3[i25] = 61;
                    i25 = i26 + 1;
                    bArr3[i26] = 61;
                }
                if (this.b) {
                    if (this.c) {
                        bArr3[i25] = 13;
                        i25++;
                    }
                    bArr3[i25] = 10;
                    i25++;
                }
                i4 = i10;
                i6 = i25;
            } else if (i4 - this.i == i16 - 2) {
                if (this.i > 1) {
                    b3 = this.h[0];
                    i14 = 1;
                } else {
                    b3 = bArr[i4];
                    i4++;
                }
                int i27 = (b3 & 255) << 10;
                if (this.i > 0) {
                    b4 = this.h[i14];
                    i14++;
                } else {
                    b4 = bArr[i4];
                    i4++;
                }
                int i28 = ((b4 & 255) << 2) | i27;
                this.i -= i14;
                int i29 = i6 + 1;
                bArr3[i6] = bArr2[(i28 >> 12) & 63];
                int i30 = i29 + 1;
                bArr3[i29] = bArr2[(i28 >> 6) & 63];
                int i31 = i30 + 1;
                bArr3[i30] = bArr2[i28 & 63];
                if (this.a) {
                    i8 = i31 + 1;
                    bArr3[i31] = 61;
                } else {
                    i8 = i31;
                }
                if (this.b) {
                    if (this.c) {
                        bArr3[i8] = 13;
                        i8++;
                    }
                    bArr3[i8] = 10;
                    i8++;
                }
                i6 = i8;
            } else if (this.b && i6 > 0 && i5 != 19) {
                if (this.c) {
                    i7 = i6 + 1;
                    bArr3[i6] = 13;
                } else {
                    i7 = i6;
                }
                i6 = i7 + 1;
                bArr3[i7] = 10;
            }
            if (!l && this.i != 0) {
                throw new AssertionError();
            } else if (l || i4 == i16) {
                this.e = i6;
                this.j = i5;
                return true;
            } else {
                throw new AssertionError();
            }
        }
    }

    private c() {
    }
}
