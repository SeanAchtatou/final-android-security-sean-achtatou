package com.apperhand.device.android.a;

import android.content.Context;
import android.content.SharedPreferences;
import com.apperhand.device.a.d.b;

public final class c implements b {
    private Context a;

    public c(Context context) {
        this.a = context;
    }

    public final void a() {
        SharedPreferences.Editor edit = this.a.getSharedPreferences("com.apperhand.global", 0).edit();
        edit.putBoolean("TERMINATE", true);
        edit.commit();
    }
}
