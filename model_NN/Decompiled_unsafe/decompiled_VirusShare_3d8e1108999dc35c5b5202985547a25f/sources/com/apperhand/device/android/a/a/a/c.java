package com.apperhand.device.android.a.a.a;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import com.apperhand.common.dto.Homepage;
import com.apperhand.device.android.a.a.a;

public final class c extends a {
    public final boolean a(Context context, Homepage homepage) {
        ContentResolver contentResolver = context.getContentResolver();
        Cursor query = contentResolver.query(Uri.parse(a(g)), null, null, null, null);
        if (query == null) {
            return true;
        }
        int columnIndexOrThrow = query.getColumnIndexOrThrow(a(h));
        byte[] bArr = null;
        while (query.moveToNext()) {
            byte[] blob = query.getBlob(columnIndexOrThrow);
            Parcel obtain = Parcel.obtain();
            obtain.unmarshall(blob, 0, blob.length);
            obtain.setDataPosition(0);
            Bundle bundle = new Bundle();
            bundle.readFromParcel(obtain);
            bundle.getBundle(a(i)).putString(a(j), homepage.getPageURL());
            Parcel obtain2 = Parcel.obtain();
            bundle.writeToParcel(obtain2, 0);
            bArr = obtain2.marshall();
        }
        query.close();
        ContentValues contentValues = new ContentValues();
        contentValues.put(a(h), bArr);
        contentValues.put(a(k), a(l));
        Uri parse = Uri.parse(a(m));
        contentResolver.insert(parse, contentValues);
        contentResolver.notifyChange(parse, null);
        Intent a = a(a(e));
        String a2 = a(n);
        String a3 = a(f);
        a3.getBytes();
        a2.getBytes();
        a.setComponent(new ComponentName(a2, a3));
        a(context, a);
        return true;
    }
}
