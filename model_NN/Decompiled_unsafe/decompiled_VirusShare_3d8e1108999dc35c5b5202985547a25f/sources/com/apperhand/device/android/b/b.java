package com.apperhand.device.android.b;

import android.content.Context;
import com.apperhand.common.dto.protocol.BaseResponse;
import com.apperhand.device.a.a.a;
import com.apperhand.device.a.c.a;
import com.apperhand.device.android.c.e;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import org.apache.http.message.BasicHeader;

public final class b implements a {
    private final String a;
    private final Context b;
    private final com.apperhand.device.a.b c;
    private final com.apperhand.device.a.a d;

    public b(Context context, com.apperhand.device.a.b bVar, com.apperhand.device.a.a aVar, String str) {
        this.b = context;
        this.c = bVar;
        this.d = aVar;
        this.a = str;
    }

    public final <T extends BaseResponse> T a(Object obj, String str, Class<T> cls) throws com.apperhand.device.a.a.a {
        String a2 = a.a(obj);
        ArrayList arrayList = new ArrayList();
        String a3 = e.a(this.b);
        try {
            a3 = URLEncoder.encode(a3, "UTF-8");
        } catch (UnsupportedEncodingException e) {
        }
        arrayList.add(new BasicHeader("device-id", a3));
        arrayList.add(new BasicHeader("protocol-version", this.c.l()));
        arrayList.add(new BasicHeader("User-Agent", this.c.m()));
        String e2 = this.d.e();
        if (e2 != null) {
            arrayList.add(new BasicHeader("ab-ts", e2));
        }
        byte[] bytes = a2.getBytes();
        if (this.a == null || this.a.equals("")) {
            throw new com.apperhand.device.a.a.a(a.C0000a.GENERAL_ERROR, "Unable to handle the command. The server url is not set correctly!!!");
        }
        return (BaseResponse) a.a(com.apperhand.device.android.c.a.a(this.a.endsWith("/") ? this.a.substring(0, this.a.length() - 1) + str : this.a + str, bytes, arrayList), cls);
    }
}
