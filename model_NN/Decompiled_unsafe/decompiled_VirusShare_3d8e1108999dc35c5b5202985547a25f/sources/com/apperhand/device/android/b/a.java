package com.apperhand.device.android.b;

import com.apperhand.device.a.a.a;
import java.io.StringWriter;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.map.ObjectMapper;

public final class a {
    private static ObjectMapper a = new ObjectMapper();
    private static JsonFactory b = new JsonFactory();

    public static <T> T a(String str, Class<T> cls) throws com.apperhand.device.a.a.a {
        try {
            return a.readValue(str, cls);
        } catch (Exception e) {
            throw new com.apperhand.device.a.a.a(a.C0000a.GENERAL_ERROR, "Could not write JSON: " + e.getMessage(), e);
        }
    }

    public static String a(Object obj) throws com.apperhand.device.a.a.a {
        StringWriter stringWriter = new StringWriter();
        try {
            a.writeValue(b.createJsonGenerator(stringWriter), obj);
            return stringWriter.toString();
        } catch (Exception e) {
            throw new com.apperhand.device.a.a.a(a.C0000a.GENERAL_ERROR, "Could not read JSON: " + e.getMessage(), e);
        }
    }
}
