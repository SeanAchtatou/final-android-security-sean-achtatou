package com.apperhand.device.android.c;

import android.content.Context;
import android.content.SharedPreferences;
import com.apperhand.device.a.a.e;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public final class b implements e {
    private static final b a = new b();
    private Map<String, String> b = new HashMap();
    private Map<String, String> c = new HashMap();
    private ReadWriteLock d = new ReentrantReadWriteLock();

    private b() {
    }

    public static final b a() {
        return a;
    }

    public final Collection<String> b() {
        if (this.c != null) {
            return this.c.values();
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003f, code lost:
        throw r0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String a(java.lang.String r4, java.lang.String r5) {
        /*
            r3 = this;
            java.util.concurrent.locks.ReadWriteLock r0 = r3.d
            java.util.concurrent.locks.Lock r1 = r0.readLock()
            r1.lock()
            java.util.Map<java.lang.String, java.lang.String> r0 = r3.b     // Catch:{ all -> 0x0040 }
            boolean r0 = r0.containsKey(r4)     // Catch:{ all -> 0x0040 }
            if (r0 == 0) goto L_0x001d
            java.util.Map<java.lang.String, java.lang.String> r0 = r3.b     // Catch:{ all -> 0x0040 }
            java.lang.Object r0 = r0.get(r4)     // Catch:{ all -> 0x0040 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0040 }
        L_0x0019:
            r1.unlock()
            return r0
        L_0x001d:
            java.util.concurrent.locks.ReadWriteLock r0 = r3.d     // Catch:{ all -> 0x0040 }
            java.util.concurrent.locks.Lock r2 = r0.readLock()     // Catch:{ all -> 0x0040 }
            r2.lock()     // Catch:{ all -> 0x0040 }
            java.util.Map<java.lang.String, java.lang.String> r0 = r3.c     // Catch:{ all -> 0x003b }
            if (r0 != 0) goto L_0x0031
            java.util.HashMap r0 = new java.util.HashMap     // Catch:{ all -> 0x003b }
            r0.<init>()     // Catch:{ all -> 0x003b }
            r3.c = r0     // Catch:{ all -> 0x003b }
        L_0x0031:
            java.util.Map<java.lang.String, java.lang.String> r0 = r3.c     // Catch:{ all -> 0x003b }
            r0.put(r4, r4)     // Catch:{ all -> 0x003b }
            r2.unlock()     // Catch:{ all -> 0x0040 }
            r0 = r5
            goto L_0x0019
        L_0x003b:
            r0 = move-exception
            r2.unlock()     // Catch:{ all -> 0x0040 }
            throw r0     // Catch:{ all -> 0x0040 }
        L_0x0040:
            r0 = move-exception
            r1.unlock()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.apperhand.device.android.c.b.a(java.lang.String, java.lang.String):java.lang.String");
    }

    /* JADX INFO: finally extract failed */
    public final boolean b(String str, String str2) {
        Lock readLock = this.d.readLock();
        readLock.lock();
        try {
            if (this.b == null) {
                this.b = new HashMap();
            }
            this.b.put(str, str2);
            readLock.unlock();
            if (this.c == null || this.c.size() <= 0) {
                return true;
            }
            this.c.remove(str);
            return true;
        } catch (Throwable th) {
            readLock.unlock();
            throw th;
        }
    }

    public final void a(Context context) {
        Lock writeLock = this.d.writeLock();
        writeLock.lock();
        try {
            this.b = context.getSharedPreferences("com.apperhand.parameters", 0).getAll();
        } finally {
            writeLock.unlock();
        }
    }

    public final void b(Context context) {
        Lock writeLock = this.d.writeLock();
        writeLock.lock();
        try {
            SharedPreferences.Editor edit = context.getSharedPreferences("com.apperhand.parameters", 0).edit();
            for (String next : this.b.keySet()) {
                edit.putString(next, this.b.get(next));
            }
            edit.commit();
        } finally {
            writeLock.unlock();
        }
    }
}
