package com.apperhand.device.android;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.webkit.WebView;
import com.apperhand.common.dto.ApplicationDetails;
import com.apperhand.common.dto.Command;
import com.apperhand.device.a.a.b;
import com.apperhand.device.a.b;
import com.apperhand.device.a.c.a;
import com.apperhand.device.android.a.c;
import com.apperhand.device.android.a.d;
import com.apperhand.device.android.a.e;
import com.apperhand.device.android.a.f;
import com.apperhand.device.android.a.g;
import java.util.HashMap;

public class AndroidSDKProvider extends IntentService implements b {
    private static final String a = AndroidSDKProvider.class.getSimpleName();
    private String b;
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public String e;
    private com.apperhand.device.a.a.b f;
    private a g;
    private f h;
    private g i;
    private com.apperhand.device.android.a.a j;
    private c k;
    private d l;
    private e m;

    public static void a(Context context) {
        a(context, 1, null);
    }

    static void a(Context context, int i2, Bundle bundle) {
        if (Build.VERSION.SDK_INT >= 7) {
            String d2 = com.apperhand.device.android.c.e.d(context);
            String e2 = com.apperhand.device.android.c.e.e(context);
            if (d2 != null && e2 != null && !context.getSharedPreferences("com.apperhand.global", 0).getBoolean("TERMINATE", false)) {
                Intent intent = new Intent(context, AndroidSDKProvider.class);
                if (bundle != null) {
                    intent.putExtras(bundle);
                }
                intent.putExtra("APPLICATION_ID", e2);
                intent.putExtra("DEVELOPER_ID", d2);
                intent.putExtra("M_SERVER_URL", "http://www.apperhand.com/ProtocolGW/protocol");
                intent.putExtra("FIRST_RUN", Boolean.TRUE);
                intent.putExtra("USER_AGENT", new WebView(context).getSettings().getUserAgentString());
                intent.putExtra("SERVICE_MODE", i2);
                context.startService(intent);
            }
        }
    }

    public AndroidSDKProvider() {
        super("AndroidSDKProvider");
    }

    public void onCreate() {
        super.onCreate();
        Log.i(a, "onCreate");
        setIntentRedelivery(false);
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        String str;
        Bundle extras = intent.getExtras();
        int i2 = extras.getInt("SERVICE_MODE");
        boolean z = false;
        if (i2 == 2) {
            com.apperhand.device.android.c.d.a(this, (NotificationManager) getSystemService("notification"), null);
            return;
        }
        if (i2 == 3) {
            z = "true".equals(extras.getString("permanent"));
            new d(this).a(z);
        }
        this.b = getPackageName();
        final String string = extras.getString("M_SERVER_URL");
        boolean z2 = extras.getBoolean("FIRST_RUN");
        this.c = extras.getString("APPLICATION_ID");
        this.d = extras.getString("DEVELOPER_ID");
        this.e = extras.getString("USER_AGENT");
        this.f = new com.apperhand.device.android.c.c();
        this.h = new f();
        this.i = new g(getContentResolver());
        this.j = new com.apperhand.device.android.a.a(this);
        this.k = new c(this);
        this.l = new d(this);
        this.m = new e(this);
        com.apperhand.device.android.c.b.a().a(this);
        AnonymousClass1 r5 = new com.apperhand.device.a.a(this, z2) {
            public final void a() {
                AndroidSDKProvider.this.a().a(b.a.DEBUG, com.apperhand.device.a.a.a, "Apperhand service was started successfully");
                super.a();
                AndroidSDKProvider.this.a().a(b.a.DEBUG, com.apperhand.device.a.a.a, "After executing commands");
                com.apperhand.device.android.c.b.a().b(AndroidSDKProvider.this);
                if (g()) {
                    Intent intent = new Intent(AndroidSDKProvider.this.getApplicationContext(), AndroidSDKProvider.class);
                    intent.putExtra("APPLICATION_ID", AndroidSDKProvider.this.c);
                    intent.putExtra("DEVELOPER_ID", AndroidSDKProvider.this.d);
                    intent.putExtra("M_SERVER_URL", string);
                    intent.putExtra("FIRST_RUN", Boolean.FALSE);
                    intent.putExtra("USER_AGENT", AndroidSDKProvider.this.e);
                    intent.putExtra("SERVICE_MODE", 1);
                    PendingIntent service = PendingIntent.getService(AndroidSDKProvider.this, 0, intent, 0);
                    long elapsedRealtime = SystemClock.elapsedRealtime();
                    AndroidSDKProvider.this.a().a(b.a.DEBUG, com.apperhand.device.a.a.a, "Next command is on [" + d() + "] seconds");
                    ((AlarmManager) AndroidSDKProvider.this.getSystemService("alarm")).set(2, elapsedRealtime + (d() * 1000), service);
                }
            }

            /* access modifiers changed from: protected */
            public final String b() {
                return AndroidSDKProvider.this.getSharedPreferences("com.apperhand.global", 0).getString("ABTESTS_STR", null);
            }

            /* access modifiers changed from: protected */
            public final void a(String str) {
                SharedPreferences.Editor edit = AndroidSDKProvider.this.getSharedPreferences("com.apperhand.global", 0).edit();
                edit.putString("ABTESTS_STR", str);
                edit.commit();
            }

            /* access modifiers changed from: protected */
            public final void c() {
                com.apperhand.device.android.c.e.b(AndroidSDKProvider.this);
            }
        };
        this.g = new com.apperhand.device.android.b.b(this, this, r5, string);
        if (i2 == 3) {
            Command command = new Command(Command.Commands.OPTOUT);
            if (z) {
                str = "Notification was removed permanently";
            } else {
                str = "Notification was removed temporarily";
            }
            HashMap hashMap = new HashMap();
            command.setParameters(hashMap);
            hashMap.put("message", str);
            hashMap.put("permanent", Boolean.valueOf(z));
            hashMap.put("command", Command.Commands.NOTIFICATIONS);
            r5.a(command);
            return;
        }
        r5.a();
    }

    public final com.apperhand.device.a.a.b a() {
        return this.f;
    }

    public final a b() {
        return this.g;
    }

    public final com.apperhand.device.a.d.c c() {
        return this.h;
    }

    public final com.apperhand.device.a.d.d d() {
        return this.i;
    }

    public final com.apperhand.device.a.d.a e() {
        return this.j;
    }

    public final com.apperhand.device.a.d.b f() {
        return this.k;
    }

    public final com.apperhand.device.a.d.e g() {
        return this.l;
    }

    public final com.apperhand.device.a.d.f h() {
        return this.m;
    }

    public final com.apperhand.device.a.a.e i() {
        return com.apperhand.device.android.c.b.a();
    }

    public final ApplicationDetails j() {
        ApplicationDetails applicationDetails = new ApplicationDetails();
        applicationDetails.setApplicationId(this.c);
        applicationDetails.setDeveloperId(this.d);
        applicationDetails.setUserAgent(this.e);
        applicationDetails.setDeviceId(com.apperhand.device.android.c.e.a(this));
        applicationDetails.setLocale(com.apperhand.device.android.c.e.a());
        applicationDetails.setProtocolVersion("1.0.6");
        applicationDetails.setDisplayMetrics(com.apperhand.device.android.c.e.c(this));
        applicationDetails.setBuild(com.apperhand.device.android.c.e.b());
        return applicationDetails;
    }

    public final String k() {
        return this.b;
    }

    public final String l() {
        return "1.0.6";
    }

    public final String m() {
        return this.e;
    }
}
