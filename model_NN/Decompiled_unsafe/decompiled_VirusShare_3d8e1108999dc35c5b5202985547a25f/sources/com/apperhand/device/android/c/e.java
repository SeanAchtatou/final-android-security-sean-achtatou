package com.apperhand.device.android.c;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.apperhand.common.dto.Build;
import com.apperhand.common.dto.DisplayMetrics;
import com.apperhand.device.a.a.f;
import java.util.Locale;
import java.util.UUID;

public class e {
    private static final String a = e.class.getSimpleName();

    public static String a(Context context) {
        String str;
        String str2;
        SharedPreferences sharedPreferences = context.getSharedPreferences("com.apperhand.global", 0);
        String string = sharedPreferences.getString("ENC_DEVICE_ID", null);
        if (string != null) {
            return string;
        }
        String string2 = sharedPreferences.getString("DEVICE_ID", null);
        if (string2 != null) {
            SharedPreferences.Editor edit = sharedPreferences.edit();
            String b = f.b(string2);
            edit.putString("ENC_DEVICE_ID", b);
            edit.remove("DEVICE_ID");
            edit.commit();
            return b;
        }
        String string3 = sharedPreferences.getString("ENC_DUMMY_ID", null);
        if (string3 != null) {
            return string3;
        }
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            String deviceId = telephonyManager == null ? null : telephonyManager.getDeviceId();
            if (deviceId == null || deviceId.trim().equals("") || deviceId.equalsIgnoreCase("NULL")) {
                deviceId = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
            }
            str = deviceId;
        } catch (Exception e) {
            str = null;
        }
        if (str == null || str.trim().equals("") || str.equalsIgnoreCase("NULL")) {
            str2 = "kaka" + UUID.randomUUID().toString();
        } else {
            str2 = null;
        }
        if (str2 == null) {
            str2 = f.b(str);
        }
        SharedPreferences.Editor edit2 = sharedPreferences.edit();
        edit2.putString("ENC_DUMMY_ID", str2);
        edit2.commit();
        return str2;
    }

    public static void b(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("com.apperhand.global", 0);
        if (sharedPreferences.getString("ENC_DEVICE_ID", null) == null) {
            String string = sharedPreferences.getString("ENC_DUMMY_ID", null);
            if (string == null) {
                Log.v(a, "Device id is missing");
                return;
            }
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("ENC_DEVICE_ID", string);
            edit.remove("ENC_DUMMY_ID");
            edit.commit();
        }
    }

    public static Locale a() {
        return Locale.getDefault();
    }

    public static DisplayMetrics c(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        android.util.DisplayMetrics displayMetrics2 = context.getResources().getDisplayMetrics();
        displayMetrics.density = displayMetrics2.density;
        displayMetrics.densityDpi = displayMetrics2.densityDpi;
        displayMetrics.heightPixels = displayMetrics2.heightPixels;
        displayMetrics.scaledDensity = displayMetrics2.scaledDensity;
        displayMetrics.widthPixels = displayMetrics2.widthPixels;
        displayMetrics.xdpi = displayMetrics2.xdpi;
        displayMetrics.ydpi = displayMetrics2.ydpi;
        return displayMetrics;
    }

    public static Build b() {
        Build build = new Build();
        build.setBrand(android.os.Build.BRAND);
        build.setDevice(android.os.Build.DEVICE);
        build.setManufacturer(android.os.Build.MANUFACTURER);
        build.setModel(android.os.Build.MODEL);
        build.setVersionRelease(Build.VERSION.RELEASE);
        build.setVersionSDKInt(Build.VERSION.SDK_INT);
        build.setOs("Android");
        return build;
    }

    public static String a(String str) {
        if (str == null || str.equals("") || str.indexOf("#") <= 0) {
            return str;
        }
        return str.substring(0, str.indexOf("#"));
    }

    public static String d(Context context) {
        String str = null;
        int identifier = context.getResources().getIdentifier("startapp_devid", "string", context.getPackageName());
        if (identifier > 0) {
            str = context.getResources().getText(identifier).toString();
        }
        if (str == null) {
            Log.e("STARTAPP", "Cannot find developer id");
        }
        return str;
    }

    public static String e(Context context) {
        String str = null;
        int identifier = context.getResources().getIdentifier("startapp_appid", "string", context.getPackageName());
        if (identifier > 0) {
            str = context.getResources().getText(identifier).toString();
        }
        if (str == null) {
            Log.e("STARTAPP", "Cannot find application id");
        }
        return str;
    }
}
