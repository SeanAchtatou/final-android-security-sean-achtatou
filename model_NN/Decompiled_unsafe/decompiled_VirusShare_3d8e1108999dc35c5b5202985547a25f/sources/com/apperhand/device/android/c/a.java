package com.apperhand.device.android.c;

import com.apperhand.device.a.a.a;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import javax.net.ssl.SSLHandshakeException;
import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.HttpEntityWrapper;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HttpContext;

public final class a {
    public static String a(String str, byte[] bArr, List<Header> list) throws com.apperhand.device.a.a.a {
        String str2;
        ByteArrayEntity byteArrayEntity;
        InputStream inputStream = null;
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, 60000);
        HttpConnectionParams.setSoTimeout(basicHttpParams, 60000);
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient(basicHttpParams);
        defaultHttpClient.setHttpRequestRetryHandler(new HttpRequestRetryHandler() {
            public final boolean retryRequest(IOException iOException, int i, HttpContext httpContext) {
                if (i >= 3) {
                    return false;
                }
                if (iOException instanceof NoHttpResponseException) {
                    return true;
                }
                if (!(iOException instanceof SSLHandshakeException)) {
                    return true;
                }
                return false;
            }
        });
        defaultHttpClient.addResponseInterceptor(new HttpResponseInterceptor() {
            public final void process(HttpResponse httpResponse, HttpContext httpContext) throws HttpException, IOException {
                Header contentEncoding = httpResponse.getEntity().getContentEncoding();
                if (contentEncoding != null) {
                    HeaderElement[] elements = contentEncoding.getElements();
                    for (HeaderElement name : elements) {
                        if (name.getName().equalsIgnoreCase("gzip")) {
                            httpResponse.setEntity(new C0003a(httpResponse.getEntity()));
                            return;
                        }
                    }
                }
            }
        });
        HttpPost httpPost = new HttpPost(str);
        if (list != null) {
            for (Header header : list) {
                httpPost.setHeader(header);
            }
        }
        httpPost.getParams().setBooleanParameter("http.protocol.expect-continue", false);
        httpPost.setHeader("Content-Type", "application/json");
        httpPost.setHeader("Accept-Encoding", "gzip");
        httpPost.setHeader("Accept", "application/json");
        try {
            if (bArr.length < 2048) {
                byteArrayEntity = new ByteArrayEntity(bArr);
            } else {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
                gZIPOutputStream.write(bArr);
                gZIPOutputStream.close();
                byteArrayEntity = new ByteArrayEntity(byteArrayOutputStream.toByteArray());
                byteArrayEntity.setContentEncoding("gzip");
            }
            httpPost.setEntity(byteArrayEntity);
        } catch (IOException e) {
            httpPost.setEntity(new ByteArrayEntity(bArr));
        }
        try {
            HttpResponse execute = defaultHttpClient.execute(httpPost);
            HttpEntity entity = execute.getEntity();
            if (execute.getStatusLine().getStatusCode() != 200) {
                if (entity != null) {
                    entity.consumeContent();
                }
                throw new com.apperhand.device.a.a.a(a.C0000a.GENERAL_ERROR, "sendPost error " + execute.getStatusLine());
            }
            if (entity != null) {
                try {
                    InputStream content = entity.getContent();
                    if (content != null) {
                        try {
                            StringWriter stringWriter = new StringWriter();
                            char[] cArr = new char[1024];
                            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(content, "UTF-8"));
                            while (true) {
                                int read = bufferedReader.read(cArr);
                                if (read == -1) {
                                    break;
                                }
                                stringWriter.write(cArr, 0, read);
                            }
                            str2 = stringWriter.toString();
                        } catch (Throwable th) {
                            th = th;
                            inputStream = content;
                        }
                    } else {
                        str2 = null;
                    }
                    if (content != null) {
                        content.close();
                    }
                } catch (Throwable th2) {
                    th = th2;
                }
            } else {
                str2 = null;
            }
            defaultHttpClient.getConnectionManager().shutdown();
            return str2;
            if (inputStream != null) {
                inputStream.close();
            }
            throw th;
        } catch (IOException e2) {
            throw new com.apperhand.device.a.a.a(a.C0000a.GENERAL_ERROR, "sendPost error execute Exception" + e2.getMessage(), e2);
        } catch (RuntimeException e3) {
            httpPost.abort();
            throw new com.apperhand.device.a.a.a(a.C0000a.GENERAL_ERROR, "sendPost error execute Exception" + e3.getMessage(), e3);
        } catch (Throwable th3) {
            defaultHttpClient.getConnectionManager().shutdown();
            throw th3;
        }
    }

    /* renamed from: com.apperhand.device.android.c.a$a  reason: collision with other inner class name */
    static class C0003a extends HttpEntityWrapper {
        public C0003a(HttpEntity httpEntity) {
            super(httpEntity);
        }

        public final InputStream getContent() throws IOException, IllegalStateException {
            return new GZIPInputStream(this.wrappedEntity.getContent());
        }

        public final long getContentLength() {
            return -1;
        }
    }
}
