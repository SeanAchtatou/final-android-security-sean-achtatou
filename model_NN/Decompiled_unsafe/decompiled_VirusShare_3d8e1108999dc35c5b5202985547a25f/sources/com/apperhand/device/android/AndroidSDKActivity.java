package com.apperhand.device.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class AndroidSDKActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = getIntent();
        Bundle bundle2 = null;
        if (intent != null) {
            bundle2 = intent.getExtras();
        }
        AndroidSDKProvider.a(this, 3, bundle2);
        finish();
    }
}
