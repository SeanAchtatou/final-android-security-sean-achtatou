package com.apperhand.device.android.a;

import android.content.Context;
import com.apperhand.common.dto.Homepage;
import com.apperhand.device.a.d.f;
import com.apperhand.device.android.a.a.a;
import java.util.List;

public final class e implements f {
    private Context a;

    public e(Context context) {
        this.a = context;
    }

    public final boolean a(Homepage homepage) {
        try {
            List<a> a2 = a.C0002a.a();
            if (a2 == null || a2.size() <= 0) {
                return false;
            }
            boolean z = false;
            for (a a3 : a2) {
                try {
                    try {
                        a3.a(this.a, homepage);
                        z = true;
                    } catch (Throwable th) {
                    }
                } catch (Throwable th2) {
                    return z;
                }
            }
            return z;
        } catch (Throwable th3) {
            return false;
        }
    }
}
