package com.apperhand.device.android.a;

import android.app.NotificationManager;
import android.content.Context;
import com.apperhand.common.dto.AssetInformation;
import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandInformation;
import com.apperhand.common.dto.NotificationDTO;
import com.apperhand.device.a.a.a;
import com.apperhand.device.a.d.e;
import java.util.ArrayList;

public final class d implements e {
    private Context a;
    private NotificationManager b;

    public d(Context context) {
        this.a = context;
        this.b = (NotificationManager) context.getSystemService("notification");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.apperhand.device.android.c.d.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.apperhand.device.android.c.d.a(android.content.Context, com.apperhand.common.dto.NotificationDTO):java.lang.String
      com.apperhand.device.android.c.d.a(android.content.Context, boolean):void */
    public final boolean a(boolean z) {
        if (z) {
            com.apperhand.device.android.c.d.a(this.a, false);
        }
        this.b.cancel(71);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.apperhand.device.android.c.d.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.apperhand.device.android.c.d.a(android.content.Context, com.apperhand.common.dto.NotificationDTO):java.lang.String
      com.apperhand.device.android.c.d.a(android.content.Context, boolean):void */
    public final String a(NotificationDTO notificationDTO) throws a {
        com.apperhand.device.android.c.d.a(this.a, notificationDTO);
        com.apperhand.device.android.c.d.a(this.a, true);
        com.apperhand.device.android.c.d.a(this.a, this.b, notificationDTO);
        return null;
    }

    public final CommandInformation a() {
        CommandInformation commandInformation = new CommandInformation(Command.Commands.NOTIFICATIONS);
        commandInformation.setValid(true);
        if (com.apperhand.device.android.c.d.a(this.a)) {
            try {
                NotificationDTO b2 = com.apperhand.device.android.c.d.b(this.a);
                if (b2 != null) {
                    ArrayList arrayList = new ArrayList();
                    commandInformation.setAssets(arrayList);
                    AssetInformation assetInformation = new AssetInformation();
                    arrayList.add(assetInformation);
                    assetInformation.setUrl(b2.getLink());
                    assetInformation.setState(AssetInformation.State.EXIST);
                } else {
                    commandInformation.setValid(false);
                    commandInformation.setMessage("Need to show notification, but couldn't read file");
                }
            } catch (Exception e) {
                commandInformation.setValid(false);
                commandInformation.setMessage("Need to show notification, message = [" + e.getMessage() + "]");
            }
        } else {
            commandInformation.setMessage("Notification was opt out");
        }
        return commandInformation;
    }
}
