package com.apperhand.device.android.c;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.widget.RemoteViews;
import com.apperhand.common.dto.NotificationDTO;
import com.apperhand.device.a.a.a;

public class d {
    private static final String a = d.class.getSimpleName();

    public static void a(Context context, boolean z) {
        SharedPreferences.Editor edit = context.getSharedPreferences("com.apperhand.global", 0).edit();
        edit.putBoolean("SHOW_NOTIF", z);
        edit.commit();
    }

    public static boolean a(Context context) {
        return context.getSharedPreferences("com.apperhand.global", 0).getBoolean("SHOW_NOTIF", false);
    }

    public static String a(Context context, NotificationDTO notificationDTO) throws a {
        String a2 = com.apperhand.device.android.b.a.a(notificationDTO);
        try {
            SharedPreferences.Editor edit = context.getSharedPreferences("com.apperhand.global", 0).edit();
            edit.putString("NOTIF_OBJ", a2);
            edit.commit();
            return null;
        } catch (Exception e) {
            throw new a(a.C0000a.GENERAL_ERROR, "Could not save Notification: " + e.getMessage(), e);
        }
    }

    public static NotificationDTO b(Context context) throws Exception {
        return (NotificationDTO) com.apperhand.device.android.b.a.a(context.getSharedPreferences("com.apperhand.global", 0).getString("NOTIF_OBJ", null), NotificationDTO.class);
    }

    public static void a(Context context, NotificationManager notificationManager, NotificationDTO notificationDTO) {
        if (notificationDTO == null) {
            try {
                notificationDTO = b(context);
            } catch (Exception e) {
                return;
            }
        }
        if (notificationDTO != null) {
            Notification notification = new Notification();
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(notificationDTO.getLink()));
            intent.addFlags(268435456);
            intent.addFlags(67108864);
            notification.contentIntent = PendingIntent.getActivity(context, 0, intent, 0);
            notification.tickerText = notificationDTO.getTickerText();
            notification.icon = 17301583;
            notification.flags = 34;
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), context.getResources().getIdentifier("search_notif", "layout", context.getPackageName()));
            byte[] icon = notificationDTO.getIcon();
            remoteViews.setImageViewBitmap(context.getResources().getIdentifier("startapp_icon", "id", context.getPackageName()), BitmapFactory.decodeByteArray(icon, 0, icon.length));
            int identifier = context.getResources().getIdentifier("startapp_text", "id", context.getPackageName());
            String str = "";
            int identifier2 = context.getResources().getIdentifier("app_name", "string", context.getPackageName());
            if (identifier2 != 0) {
                str = context.getString(identifier2);
            }
            String bodyText = notificationDTO.getBodyText();
            if (bodyText != null) {
                str = bodyText.replaceFirst("PARAM_APPLICATION_NAME", str);
            }
            remoteViews.setTextViewText(identifier, str);
            notification.contentView = remoteViews;
            notificationManager.notify(71, notification);
        }
    }
}
