package com.redmicapps.puzzles.ladies3;

import com.ptowngames.jigsaur.CatsLikeJigsaurAndroid;
import com.ptowngames.jigsaur.ar;

public class Game extends CatsLikeJigsaurAndroid {
    public final String a() {
        return "T26CR6TL6BGNJBN95XZF";
    }

    public final String b() {
        return "a14efe5007cae48";
    }

    public final String c() {
        int b = (int) (ar.a().b() % 25);
        if (b < 0) {
            b = -b;
        }
        String str = "" + (b + 1);
        while (str.length() < 5) {
            str = "0" + str;
        }
        return "data/level" + str + "_" + "2X2" + ".level";
    }

    public final String d() {
        return "android.resource://com.redmicapps.puzzles.ladies3/2130968576";
    }
}
