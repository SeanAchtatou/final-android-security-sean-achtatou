package com.flurry.android;

import java.io.DataOutput;
import java.util.ArrayList;
import java.util.List;

final class w {
    final String a;
    ac b;
    long c;
    List d;
    private byte e;

    w(w wVar, long j) {
        this(wVar.a, wVar.e, j);
        this.b = wVar.b;
        this.c = wVar.c;
    }

    w(String str, byte b2, long j) {
        this.d = new ArrayList();
        this.a = str;
        this.e = b2;
        this.d.add(new ai((byte) 1, j));
    }

    /* access modifiers changed from: package-private */
    public final void a(ai aiVar) {
        this.d.add(aiVar);
    }

    /* access modifiers changed from: package-private */
    public final long a() {
        return ((ai) this.d.get(0)).b;
    }

    /* access modifiers changed from: package-private */
    public final void a(DataOutput dataOutput) {
        dataOutput.writeUTF(this.a);
        dataOutput.writeByte(this.e);
        if (this.b == null) {
            dataOutput.writeLong(0);
            dataOutput.writeLong(0);
            dataOutput.writeByte(0);
        } else {
            dataOutput.writeLong(this.b.a);
            dataOutput.writeLong(this.b.e);
            byte[] bArr = this.b.g;
            dataOutput.writeByte(bArr.length);
            dataOutput.write(bArr);
        }
        dataOutput.writeShort(this.d.size());
        for (ai aiVar : this.d) {
            dataOutput.writeByte(aiVar.a);
            dataOutput.writeLong(aiVar.b);
        }
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{hook: " + this.a + ", ad: " + this.b.d + ", transitions: [");
        for (ai append : this.d) {
            sb.append(append);
            sb.append(",");
        }
        sb.append("]}");
        return sb.toString();
    }
}
