package com.flurry.android;

import android.view.View;
import android.widget.TextView;

final class j implements View.OnFocusChangeListener {
    private /* synthetic */ TextView a;
    private /* synthetic */ i b;

    j(i iVar, TextView textView) {
        this.b = iVar;
        this.a = textView;
    }

    public final void onFocusChange(View view, boolean z) {
        this.a.setText(z ? this.b.b : this.b.a);
    }
}
