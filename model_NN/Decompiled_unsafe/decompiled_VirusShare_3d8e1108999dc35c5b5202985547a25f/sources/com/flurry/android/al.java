package com.flurry.android;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.NinePatch;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Build;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

final class al extends RelativeLayout {
    private ab a;
    private w b;
    private int c;

    public al(Context context, ab abVar, w wVar, aj ajVar, int i, boolean z) {
        super(context);
        this.a = abVar;
        this.b = wVar;
        ac acVar = wVar.b;
        this.c = i;
        switch (this.c) {
            case 2:
                if (z) {
                    a(context, ajVar, acVar, false);
                } else {
                    a(context, ajVar, acVar, true);
                }
            case 1:
                if (!z) {
                    a(context, ajVar, acVar, true);
                    break;
                } else {
                    a(context, ajVar, acVar, false);
                    break;
                }
        }
        setFocusable(true);
    }

    private void a(Context context, aj ajVar, ac acVar, boolean z) {
        Drawable bitmapDrawable;
        Bitmap bitmap;
        setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        ah ahVar = ajVar.d;
        ImageView imageView = new ImageView(context);
        imageView.setId(1);
        m mVar = acVar.h;
        if (mVar != null) {
            byte[] bArr = mVar.e;
            Bitmap decodeByteArray = BitmapFactory.decodeByteArray(bArr, 0, bArr.length);
            if (decodeByteArray == null) {
                g.a("FlurryAgent", "Ad with bad image: " + acVar.d + ", data: " + bArr);
            }
            if (decodeByteArray != null) {
                Bitmap createBitmap = Bitmap.createBitmap(decodeByteArray.getWidth(), decodeByteArray.getHeight(), Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(createBitmap);
                Paint paint = new Paint();
                Rect rect = new Rect(0, 0, decodeByteArray.getWidth(), decodeByteArray.getHeight());
                RectF rectF = new RectF(rect);
                float a2 = (float) y.a(context, 8);
                paint.setAntiAlias(true);
                canvas.drawARGB(0, 0, 0, 0);
                paint.setColor(-16777216);
                canvas.drawRoundRect(rectF, a2, a2, paint);
                paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
                canvas.drawBitmap(decodeByteArray, rect, rect, paint);
                if (Integer.parseInt(Build.VERSION.SDK) > 4) {
                    BlurMaskFilter blurMaskFilter = new BlurMaskFilter(3.0f, BlurMaskFilter.Blur.OUTER);
                    Paint paint2 = new Paint();
                    paint2.setMaskFilter(blurMaskFilter);
                    int[] iArr = new int[2];
                    bitmap = createBitmap.extractAlpha(paint2, iArr).copy(Bitmap.Config.ARGB_8888, true);
                    new Canvas(bitmap).drawBitmap(createBitmap, (float) (-iArr[0]), (float) (-iArr[1]), (Paint) null);
                } else {
                    bitmap = createBitmap;
                }
                imageView.setImageBitmap(bitmap);
                y.a(context, imageView, y.a(context, ahVar.m), y.a(context, ahVar.n));
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            }
        }
        m a3 = this.a.a(ahVar.c);
        if (a3 != null) {
            byte[] bArr2 = a3.e;
            Bitmap decodeByteArray2 = BitmapFactory.decodeByteArray(bArr2, 0, bArr2.length);
            if (NinePatch.isNinePatchChunk(decodeByteArray2.getNinePatchChunk())) {
                bitmapDrawable = new NinePatchDrawable(decodeByteArray2, decodeByteArray2.getNinePatchChunk(), new Rect(0, 0, 0, 0), null);
            } else {
                bitmapDrawable = new BitmapDrawable(decodeByteArray2);
            }
            setBackgroundDrawable(bitmapDrawable);
        }
        TextView textView = new TextView(context);
        textView.setId(5);
        textView.setPadding(0, 0, 0, 0);
        TextView textView2 = new TextView(context);
        textView2.setId(3);
        textView2.setPadding(0, 0, 0, 0);
        if (z) {
            textView.setTextColor(ahVar.f);
            textView.setTextSize((float) ahVar.e);
            textView.setText(new String("• " + ahVar.b));
            textView.setTypeface(Typeface.create(ahVar.d, 0));
            textView2.setTextColor(ahVar.i);
            textView2.setTextSize((float) ahVar.h);
            textView2.setTypeface(Typeface.create(ahVar.g, 0));
            textView2.setText(acVar.d);
        } else {
            textView.setId(3);
            textView.setText(acVar.d);
            textView.setTextColor(ahVar.i);
            textView.setTextSize((float) ahVar.h);
            textView.setTypeface(Typeface.create(ahVar.g, 0));
            textView2.setId(4);
            textView2.setText(acVar.c);
            textView2.setTextColor(ahVar.l);
            textView2.setTextSize((float) ahVar.k);
            textView2.setTypeface(Typeface.create(ahVar.j, 0));
        }
        setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        addView(new ImageView(context), new RelativeLayout.LayoutParams(-1, -2));
        int i = (ahVar.q - (ahVar.o << 1)) - ahVar.m;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(9);
        layoutParams.setMargins(ahVar.o, ahVar.p, i, 0);
        addView(imageView, layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(6, imageView.getId());
        layoutParams2.addRule(1, imageView.getId());
        layoutParams2.setMargins(0, 0, 0, 0);
        addView(textView, layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams3.addRule(1, imageView.getId());
        layoutParams3.addRule(3, textView.getId());
        layoutParams3.setMargins(0, -2, 0, 0);
        addView(textView2, layoutParams3);
    }

    /* access modifiers changed from: package-private */
    public final w a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final void a(w wVar) {
        this.b = wVar;
    }
}
