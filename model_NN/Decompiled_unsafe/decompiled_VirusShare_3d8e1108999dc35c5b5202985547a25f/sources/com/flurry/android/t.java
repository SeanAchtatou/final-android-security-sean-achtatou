package com.flurry.android;

final class t implements Runnable {
    private /* synthetic */ String a;
    private /* synthetic */ c b;

    t(c cVar, String str) {
        this.b = cVar;
        this.a = str;
    }

    public final void run() {
        if (this.a != null) {
            ab.a(this.b.d, this.b.b, this.a);
            this.b.c.a(new ai((byte) 8, this.b.d.i()));
            return;
        }
        String str = "Unable to launch in app market: " + this.b.a;
        g.d(ab.a, str);
        this.b.d.c(str);
    }
}
