package com.flurry.android;

import java.net.Socket;
import java.security.KeyStore;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import org.apache.http.conn.ssl.SSLSocketFactory;

final class h extends SSLSocketFactory {
    private SSLContext a = SSLContext.getInstance("TLS");

    public h(KeyStore keyStore) {
        super(keyStore);
        u uVar = new u();
        this.a.init(null, new TrustManager[]{uVar}, null);
    }

    public final Socket createSocket(Socket socket, String str, int i, boolean z) {
        return this.a.getSocketFactory().createSocket(socket, str, i, z);
    }

    public final Socket createSocket() {
        return this.a.getSocketFactory().createSocket();
    }
}
