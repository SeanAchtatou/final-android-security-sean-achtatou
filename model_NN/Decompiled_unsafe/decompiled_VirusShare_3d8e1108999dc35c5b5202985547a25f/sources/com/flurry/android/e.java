package com.flurry.android;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

final class e {
    private LinkedHashMap a = new o(this, ((int) Math.ceil(133.3333282470703d)) + 1);
    /* access modifiers changed from: private */
    public int b = 100;

    e() {
    }

    /* access modifiers changed from: package-private */
    public final synchronized Object a(Object obj) {
        return this.a.get(obj);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(Object obj, Object obj2) {
        this.a.put(obj, obj2);
    }

    /* access modifiers changed from: package-private */
    public final synchronized int a() {
        return this.a.size();
    }

    /* access modifiers changed from: package-private */
    public final synchronized List b() {
        return new ArrayList(this.a.entrySet());
    }

    /* access modifiers changed from: package-private */
    public final synchronized Set c() {
        return this.a.keySet();
    }
}
