package com.flurry.android;

import java.lang.Thread;

public final class ar implements Thread.UncaughtExceptionHandler {
    private Thread.UncaughtExceptionHandler a = Thread.getDefaultUncaughtExceptionHandler();

    ar() {
    }

    public final void uncaughtException(Thread thread, Throwable th) {
        try {
            ag.j.a(th);
        } catch (Throwable th2) {
            g.b("FlurryAgent", "", th2);
        }
        if (this.a != null) {
            this.a.uncaughtException(thread, th);
        }
    }
}
