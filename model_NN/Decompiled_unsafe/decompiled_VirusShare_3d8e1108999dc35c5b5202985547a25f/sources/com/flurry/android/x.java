package com.flurry.android;

import android.webkit.WebView;
import android.webkit.WebViewClient;

final class x extends WebViewClient {
    private /* synthetic */ CatalogActivity a;

    x(CatalogActivity catalogActivity) {
        this.a = catalogActivity;
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (str == null) {
            return false;
        }
        if (this.a.f != null) {
            this.a.f.a(new ai((byte) 6, this.a.e.i()));
        }
        this.a.e.a(webView.getContext(), this.a.f, str);
        return true;
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        g.c("FlurryAgent", "Failed to load url: " + str2 + " with an errorCode of " + i);
        webView.loadData("Cannot find Android Market information. <p>Please check your network", "text/html", "UTF-8");
    }

    public final void onPageFinished(WebView webView, String str) {
        try {
            w a2 = this.a.f;
            ai aiVar = new ai((byte) 5, this.a.e.i());
            long j = this.a.f.c;
            a2.d.add(aiVar);
            a2.c = j;
        } catch (Exception e) {
        }
    }
}
