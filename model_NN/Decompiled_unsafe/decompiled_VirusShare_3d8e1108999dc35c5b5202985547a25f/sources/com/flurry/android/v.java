package com.flurry.android;

import android.content.Context;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.Arrays;
import java.util.List;

final class v extends RelativeLayout {
    private ab a;
    private Context b;
    private String c;
    private int d;
    private boolean e;
    private boolean f;

    /* access modifiers changed from: package-private */
    public final void a() {
        if (!this.e) {
            al c2 = c();
            if (c2 != null) {
                removeAllViews();
                addView(c2, b());
                c2.a().a(new ai((byte) 3, this.a.i()));
                this.e = true;
            } else if (!this.f) {
                TextView textView = new TextView(this.b);
                textView.setText(ab.b);
                textView.setTextSize(1, 20.0f);
                addView(textView, b());
            }
            this.f = true;
        }
    }

    private static RelativeLayout.LayoutParams b() {
        return new RelativeLayout.LayoutParams(-1, -1);
    }

    private synchronized al c() {
        al alVar;
        List a2 = this.a.a(this.b, Arrays.asList(this.c), null, this.d, false);
        if (!a2.isEmpty()) {
            alVar = (al) a2.get(0);
            alVar.setOnClickListener(this.a);
        } else {
            alVar = null;
        }
        return alVar;
    }
}
