package com.flurry.android;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.Settings;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.WeakHashMap;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public final class ag implements LocationListener {
    static String a;
    private static final String[] b = {"9774d56d682e549c", "dead00beef"};
    private static volatile String c = null;
    private static volatile String d = "http://data.flurry.com/aap.do";
    private static volatile String e = "https://data.flurry.com/aap.do";
    private static volatile String f = null;
    private static volatile String g = "http://ad.flurry.com/getCanvas.do";
    private static volatile String h = null;
    private static volatile String i = "http://ad.flurry.com/getAndroidApp.do";
    /* access modifiers changed from: private */
    public static final ag j = new ag();
    /* access modifiers changed from: private */
    public static long k = 10000;
    private static boolean l = true;
    private static boolean m = false;
    private static boolean n = false;
    private static boolean o = true;
    private static Criteria p = null;
    /* access modifiers changed from: private */
    public static boolean q = false;
    private static ap r = new ap();
    private String A;
    private String B;
    private boolean C = true;
    private List D;
    private LocationManager E;
    private String F;
    private boolean G;
    private long H;
    private List I = new ArrayList();
    private long J;
    private long K;
    private long L;
    private String M = "";
    private String N = "";
    private byte O = -1;
    private String P = "";
    private byte Q = -1;
    private Long R;
    private int S;
    private Location T;
    private Map U = new HashMap();
    private List V = new ArrayList();
    private boolean W;
    private int X;
    private List Y = new ArrayList();
    private int Z;
    /* access modifiers changed from: private */
    public ab aa = new ab();
    /* access modifiers changed from: private */
    public final Handler s;
    private File t;
    private File u = null;
    private volatile boolean v = false;
    /* access modifiers changed from: private */
    public volatile boolean w = false;
    private long x;
    private Map y = new WeakHashMap();
    private String z;

    static /* synthetic */ void a(ag agVar, Context context, boolean z2) {
        Location location = null;
        if (z2) {
            try {
                location = agVar.e(context);
            } catch (Throwable th) {
                g.b("FlurryAgent", "", th);
                return;
            }
        }
        synchronized (agVar) {
            agVar.T = location;
        }
        if (q) {
            agVar.aa.b();
        }
        agVar.b(true);
    }

    static /* synthetic */ void d(ag agVar) {
        boolean z2 = false;
        try {
            synchronized (agVar) {
                long elapsedRealtime = SystemClock.elapsedRealtime() - agVar.x;
                if (!agVar.v && elapsedRealtime > k && agVar.I.size() > 0) {
                    z2 = true;
                }
            }
            if (z2) {
                agVar.b(false);
            }
        } catch (Throwable th) {
            g.b("FlurryAgent", "", th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.ag.a(android.content.Context, boolean):void
     arg types: [?[OBJECT, ARRAY], int]
     candidates:
      com.flurry.android.ag.a(android.content.Context, java.lang.String):void
      com.flurry.android.ag.a(com.flurry.android.ag, android.content.Context):void
      com.flurry.android.ag.a(java.lang.String, java.util.Map):void
      com.flurry.android.ag.a(byte[], java.lang.String):boolean
      com.flurry.android.ag.a(android.content.Context, boolean):void */
    /* access modifiers changed from: package-private */
    public final void a(Throwable th) {
        String str = "";
        StackTraceElement[] stackTrace = th.getStackTrace();
        if (stackTrace != null && stackTrace.length > 0) {
            StackTraceElement stackTraceElement = stackTrace[0];
            StringBuilder sb = new StringBuilder();
            sb.append(stackTraceElement.getClassName()).append(".").append(stackTraceElement.getMethodName()).append(":").append(stackTraceElement.getLineNumber());
            if (th.getMessage() != null) {
                sb.append(" (" + th.getMessage() + ")");
            }
            str = sb.toString();
        } else if (th.getMessage() != null) {
            str = th.getMessage();
        }
        try {
            j.a("uncaught", str, th.getClass().toString());
        } catch (Throwable th2) {
            g.b("FlurryAgent", "", th2);
        }
        this.y.clear();
        a((Context) null, true);
    }

    private ag() {
        HandlerThread handlerThread = new HandlerThread("FlurryAgent");
        handlerThread.start();
        this.s = new Handler(handlerThread.getLooper());
    }

    public static void a() {
        if (120000 < 5000) {
            g.b("FlurryAgent", "Invalid time set for session resumption: " + 120000L);
            return;
        }
        synchronized (j) {
            k = 120000;
        }
    }

    public static void a(Context context, String str) {
        if (context == null) {
            throw new NullPointerException("Null context");
        } else if (str == null || str.length() == 0) {
            throw new IllegalArgumentException("Api key not specified");
        } else {
            try {
                j.b(context, str);
            } catch (Throwable th) {
                g.b("FlurryAgent", "", th);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.flurry.android.ag.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.flurry.android.ag.a(android.content.Context, java.lang.String):void
      com.flurry.android.ag.a(com.flurry.android.ag, android.content.Context):void
      com.flurry.android.ag.a(java.lang.String, java.util.Map):void
      com.flurry.android.ag.a(byte[], java.lang.String):boolean
      com.flurry.android.ag.a(android.content.Context, boolean):void */
    public static void a(Context context) {
        if (context == null) {
            throw new NullPointerException("Null context");
        }
        try {
            j.a(context, false);
        } catch (Throwable th) {
            g.b("FlurryAgent", "", th);
        }
    }

    public static void a(String str) {
        try {
            j.b(str, (Map) null);
        } catch (Throwable th) {
            g.b("FlurryAgent", "Failed to log event: " + str, th);
        }
    }

    public static void a(String str, Map map) {
        try {
            j.b(str, map);
        } catch (Throwable th) {
            g.b("FlurryAgent", "Failed to log event: " + str, th);
        }
    }

    public static void b(String str) {
        synchronized (j) {
            j.P = y.a(str, 255);
        }
    }

    protected static boolean b() {
        return o;
    }

    static ab c() {
        return j.aa;
    }

    private synchronized void b(Context context, String str) {
        if (this.z != null && !this.z.equals(str)) {
            g.b("FlurryAgent", "onStartSession called with different api keys: " + this.z + " and " + str);
        }
        if (((Context) this.y.put(context, context)) != null) {
            g.d("FlurryAgent", "onStartSession called with duplicate context, use a specific Activity or Service as context instead of using a global context");
        }
        if (!this.v) {
            g.a("FlurryAgent", "Initializing Flurry session");
            this.z = str;
            this.u = context.getFileStreamPath(".flurryagent." + Integer.toString(this.z.hashCode(), 16));
            this.t = context.getFileStreamPath(".flurryb.");
            if (o) {
                Thread.setDefaultUncaughtExceptionHandler(new ar());
            }
            Context applicationContext = context.getApplicationContext();
            if (this.B == null) {
                this.B = d(applicationContext);
            }
            String packageName = applicationContext.getPackageName();
            if (this.A != null && !this.A.equals(packageName)) {
                g.b("FlurryAgent", "onStartSession called from different application packages: " + this.A + " and " + packageName);
            }
            this.A = packageName;
            long elapsedRealtime = SystemClock.elapsedRealtime();
            if (elapsedRealtime - this.x > k) {
                g.a("FlurryAgent", "New session");
                this.J = System.currentTimeMillis();
                this.K = elapsedRealtime;
                this.L = -1;
                this.P = "";
                this.S = 0;
                this.T = null;
                this.N = TimeZone.getDefault().getID();
                this.M = Locale.getDefault().getLanguage() + "_" + Locale.getDefault().getCountry();
                this.U = new HashMap();
                this.V = new ArrayList();
                this.W = true;
                this.Y = new ArrayList();
                this.X = 0;
                this.Z = 0;
                if (q) {
                    if (!this.aa.a()) {
                        g.a("FlurryAgent", "Initializing AppCircle");
                        ae aeVar = new ae();
                        aeVar.a = this.z;
                        aeVar.b = this.H;
                        aeVar.c = f != null ? f : g;
                        aeVar.d = h != null ? h : i;
                        aeVar.e = this.s;
                        this.aa.a(context, aeVar);
                        g.a("FlurryAgent", "AppCircle initialized");
                    }
                    this.aa.a(this.J, this.K);
                }
                a(new af(this, applicationContext, this.C));
            } else {
                g.a("FlurryAgent", "Continuing previous session");
                if (!this.I.isEmpty()) {
                    this.I.remove(this.I.size() - 1);
                }
            }
            this.v = true;
        }
    }

    private synchronized void a(Context context, boolean z2) {
        if (context != null) {
            if (((Context) this.y.remove(context)) == null) {
                g.d("FlurryAgent", "onEndSession called without context from corresponding onStartSession");
            }
        }
        if (this.v && this.y.isEmpty()) {
            g.a("FlurryAgent", "Ending session");
            k();
            Context applicationContext = context == null ? null : context.getApplicationContext();
            if (context != null) {
                String packageName = applicationContext.getPackageName();
                if (!this.A.equals(packageName)) {
                    g.b("FlurryAgent", "onEndSession called from different application package, expected: " + this.A + " actual: " + packageName);
                }
            }
            long elapsedRealtime = SystemClock.elapsedRealtime();
            this.x = elapsedRealtime;
            this.L = elapsedRealtime - this.K;
            if (this.F == null) {
                g.b("FlurryAgent", "Not creating report because of bad Android ID or generated ID is null");
            }
            a(new ad(this, z2, applicationContext));
            this.v = false;
        }
    }

    /* access modifiers changed from: private */
    public synchronized void h() {
        DataOutputStream dataOutputStream;
        DataOutputStream dataOutputStream2 = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            dataOutputStream = new DataOutputStream(byteArrayOutputStream);
            try {
                dataOutputStream.writeShort(1);
                dataOutputStream.writeUTF(this.B);
                dataOutputStream.writeLong(this.J);
                dataOutputStream.writeLong(this.L);
                dataOutputStream.writeLong(0);
                dataOutputStream.writeUTF(this.M);
                dataOutputStream.writeUTF(this.N);
                dataOutputStream.writeByte(this.O);
                dataOutputStream.writeUTF(this.P == null ? "" : this.P);
                if (this.T == null) {
                    dataOutputStream.writeBoolean(false);
                } else {
                    dataOutputStream.writeBoolean(true);
                    dataOutputStream.writeDouble(a(this.T.getLatitude()));
                    dataOutputStream.writeDouble(a(this.T.getLongitude()));
                    dataOutputStream.writeFloat(this.T.getAccuracy());
                }
                dataOutputStream.writeInt(this.Z);
                dataOutputStream.writeByte(-1);
                dataOutputStream.writeByte(-1);
                dataOutputStream.writeByte(this.Q);
                if (this.R == null) {
                    dataOutputStream.writeBoolean(false);
                } else {
                    dataOutputStream.writeBoolean(true);
                    dataOutputStream.writeLong(this.R.longValue());
                }
                dataOutputStream.writeShort(this.U.size());
                for (Map.Entry entry : this.U.entrySet()) {
                    dataOutputStream.writeUTF((String) entry.getKey());
                    dataOutputStream.writeInt(((n) entry.getValue()).a);
                }
                dataOutputStream.writeShort(this.V.size());
                for (p a2 : this.V) {
                    dataOutputStream.write(a2.a());
                }
                dataOutputStream.writeBoolean(this.W);
                dataOutputStream.writeInt(this.S);
                dataOutputStream.writeShort(this.Y.size());
                for (d dVar : this.Y) {
                    dataOutputStream.writeLong(dVar.a);
                    dataOutputStream.writeUTF(dVar.b);
                    dataOutputStream.writeUTF(dVar.c);
                    dataOutputStream.writeUTF(dVar.d);
                }
                if (q) {
                    List<w> f2 = this.aa.f();
                    dataOutputStream.writeShort(f2.size());
                    for (w a3 : f2) {
                        a3.a(dataOutputStream);
                    }
                } else {
                    dataOutputStream.writeShort(0);
                }
                this.I.add(byteArrayOutputStream.toByteArray());
                y.a(dataOutputStream);
            } catch (IOException e2) {
                e = e2;
                dataOutputStream2 = dataOutputStream;
                try {
                    g.b("FlurryAgent", "", e);
                    y.a(dataOutputStream2);
                } catch (Throwable th) {
                    th = th;
                    dataOutputStream = dataOutputStream2;
                    y.a(dataOutputStream);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                y.a(dataOutputStream);
                throw th;
            }
        } catch (IOException e3) {
            e = e3;
            g.b("FlurryAgent", "", e);
            y.a(dataOutputStream2);
        } catch (Throwable th3) {
            th = th3;
            dataOutputStream = null;
            y.a(dataOutputStream);
            throw th;
        }
    }

    private static double a(double d2) {
        return ((double) Math.round(d2 * 1000.0d)) / 1000.0d;
    }

    private void a(Runnable runnable) {
        this.s.post(runnable);
    }

    private synchronized void b(String str, Map map) {
        if (this.V == null) {
            g.b("FlurryAgent", "onEvent called before onStartSession.  Event: " + str);
        } else {
            long elapsedRealtime = SystemClock.elapsedRealtime() - this.K;
            String a2 = y.a(str, 255);
            if (a2.length() != 0) {
                n nVar = (n) this.U.get(a2);
                if (nVar != null) {
                    nVar.a++;
                    g.a("FlurryAgent", "Event count incremented: " + a2);
                } else if (this.U.size() < 100) {
                    n nVar2 = new n();
                    nVar2.a = 1;
                    this.U.put(a2, nVar2);
                    g.a("FlurryAgent", "Event count incremented: " + a2);
                } else if (g.a("FlurryAgent")) {
                    g.a("FlurryAgent", "Too many different events. Event not counted: " + a2);
                }
                if (!l || this.V.size() >= 200 || this.X >= 16000) {
                    this.W = false;
                } else {
                    if (map == null) {
                        map = Collections.emptyMap();
                    }
                    if (map.size() <= 10) {
                        p pVar = new p(a2, map, elapsedRealtime);
                        if (pVar.a().length + this.X <= 16000) {
                            this.V.add(pVar);
                            this.X = pVar.a().length + this.X;
                            g.a("FlurryAgent", "Logged event: " + a2);
                        } else {
                            this.X = 16000;
                            this.W = false;
                            g.a("FlurryAgent", "Event Log size exceeded. No more event details logged.");
                        }
                    } else if (g.a("FlurryAgent")) {
                        g.a("FlurryAgent", "MaxEventParams exceeded: " + map.size());
                    }
                }
            }
        }
    }

    private synchronized void a(String str, String str2, String str3) {
        if (this.Y == null) {
            g.b("FlurryAgent", "onError called before onStartSession.  Error: " + str);
        } else {
            this.S++;
            if (this.Y.size() < 10) {
                d dVar = new d();
                dVar.a = System.currentTimeMillis();
                dVar.b = y.a(str, 255);
                dVar.c = y.a(str2, 512);
                dVar.d = y.a(str3, 255);
                this.Y.add(dVar);
                g.a("FlurryAgent", "Error logged: " + dVar.b);
            } else {
                g.a("FlurryAgent", "Max errors logged. No more errors logged.");
            }
        }
    }

    private synchronized byte[] a(boolean z2) {
        DataOutputStream dataOutputStream;
        byte[] bArr;
        synchronized (this) {
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                dataOutputStream = new DataOutputStream(byteArrayOutputStream);
                try {
                    dataOutputStream.writeShort(15);
                    if (!q || !z2) {
                        dataOutputStream.writeShort(0);
                    } else {
                        dataOutputStream.writeShort(1);
                    }
                    if (q) {
                        dataOutputStream.writeLong(this.aa.d());
                        Set<Long> e2 = this.aa.e();
                        dataOutputStream.writeShort(e2.size());
                        for (Long longValue : e2) {
                            long longValue2 = longValue.longValue();
                            dataOutputStream.writeByte(1);
                            dataOutputStream.writeLong(longValue2);
                        }
                    } else {
                        dataOutputStream.writeLong(0);
                        dataOutputStream.writeShort(0);
                    }
                    dataOutputStream.writeShort(3);
                    dataOutputStream.writeShort(120);
                    dataOutputStream.writeLong(System.currentTimeMillis());
                    dataOutputStream.writeUTF(this.z);
                    dataOutputStream.writeUTF(this.B);
                    dataOutputStream.writeShort(0);
                    dataOutputStream.writeUTF(this.F);
                    dataOutputStream.writeLong(this.H);
                    dataOutputStream.writeLong(this.J);
                    dataOutputStream.writeShort(6);
                    dataOutputStream.writeUTF("device.model");
                    dataOutputStream.writeUTF(Build.MODEL);
                    dataOutputStream.writeUTF("build.brand");
                    dataOutputStream.writeUTF(Build.BRAND);
                    dataOutputStream.writeUTF("build.id");
                    dataOutputStream.writeUTF(Build.ID);
                    dataOutputStream.writeUTF("version.release");
                    dataOutputStream.writeUTF(Build.VERSION.RELEASE);
                    dataOutputStream.writeUTF("build.device");
                    dataOutputStream.writeUTF(Build.DEVICE);
                    dataOutputStream.writeUTF("build.product");
                    dataOutputStream.writeUTF(Build.PRODUCT);
                    int size = this.I.size();
                    dataOutputStream.writeShort(size);
                    for (int i2 = 0; i2 < size; i2++) {
                        dataOutputStream.write((byte[]) this.I.get(i2));
                    }
                    this.D = new ArrayList(this.I);
                    dataOutputStream.close();
                    bArr = byteArrayOutputStream.toByteArray();
                    y.a(dataOutputStream);
                } catch (Throwable th) {
                    th = th;
                }
            } catch (Throwable th2) {
                th = th2;
                dataOutputStream = null;
                y.a(dataOutputStream);
                throw th;
            }
        }
        return bArr;
    }

    private static String i() {
        if (c != null) {
            return c;
        }
        if (n) {
            return d;
        }
        if (m) {
            return e;
        }
        return d;
    }

    private boolean a(byte[] bArr) {
        boolean z2;
        String i2 = i();
        if (i2 == null) {
            return false;
        }
        try {
            z2 = a(bArr, i2);
        } catch (Exception e2) {
            g.a("FlurryAgent", "Sending report exception: " + e2.getMessage());
            z2 = false;
        }
        if (z2 || c != null || !m || n) {
            return z2;
        }
        synchronized (j) {
            n = true;
            String i3 = i();
            if (i3 == null) {
                return false;
            }
            try {
                return a(bArr, i3);
            } catch (Exception e3) {
                return z2;
            }
        }
    }

    private boolean a(byte[] bArr, String str) {
        boolean z2 = true;
        if (!"local".equals(str)) {
            g.a("FlurryAgent", "Sending report to: " + str);
            ByteArrayEntity byteArrayEntity = new ByteArrayEntity(bArr);
            byteArrayEntity.setContentType("application/octet-stream");
            HttpPost httpPost = new HttpPost(str);
            httpPost.setEntity(byteArrayEntity);
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
            HttpConnectionParams.setSoTimeout(basicHttpParams, 15000);
            httpPost.getParams().setBooleanParameter("http.protocol.expect-continue", false);
            HttpResponse execute = a((HttpParams) basicHttpParams).execute(httpPost);
            int statusCode = execute.getStatusLine().getStatusCode();
            synchronized (this) {
                if (statusCode == 200) {
                    g.a("FlurryAgent", "Report successful");
                    this.G = true;
                    this.I.removeAll(this.D);
                    HttpEntity entity = execute.getEntity();
                    g.a("FlurryAgent", "Processing report response");
                    if (!(entity == null || entity.getContentLength() == 0)) {
                        try {
                            a(new DataInputStream(entity.getContent()));
                        } finally {
                            entity.consumeContent();
                        }
                    }
                } else {
                    g.a("FlurryAgent", "Report failed. HTTP response: " + statusCode);
                    z2 = false;
                }
                this.D = null;
            }
        }
        return z2;
    }

    /* JADX WARN: Type inference failed for: r0v13, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.io.DataInputStream r14) {
        /*
            r13 = this;
            r7 = 0
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            java.util.HashMap r4 = new java.util.HashMap
            r4.<init>()
            java.util.HashMap r5 = new java.util.HashMap
            r5.<init>()
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            java.util.HashMap r6 = new java.util.HashMap
            r6.<init>()
        L_0x001f:
            int r9 = r14.readUnsignedShort()
            int r0 = r14.readInt()
            switch(r9) {
                case 258: goto L_0x0060;
                case 259: goto L_0x0064;
                case 260: goto L_0x002a;
                case 261: goto L_0x002a;
                case 262: goto L_0x0083;
                case 263: goto L_0x009b;
                case 264: goto L_0x0045;
                case 265: goto L_0x002a;
                case 266: goto L_0x00b3;
                case 267: goto L_0x002a;
                case 268: goto L_0x00e9;
                case 269: goto L_0x0130;
                case 270: goto L_0x00af;
                case 271: goto L_0x00cb;
                case 272: goto L_0x0106;
                case 273: goto L_0x0135;
                default: goto L_0x002a;
            }
        L_0x002a:
            java.lang.String r8 = "FlurryAgent"
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "Unknown chunkType: "
            java.lang.StringBuilder r10 = r10.append(r11)
            java.lang.StringBuilder r10 = r10.append(r9)
            java.lang.String r10 = r10.toString()
            com.flurry.android.g.a(r8, r10)
            r14.skipBytes(r0)
        L_0x0045:
            r0 = 264(0x108, float:3.7E-43)
            if (r9 != r0) goto L_0x001f
            boolean r0 = com.flurry.android.ag.q
            if (r0 == 0) goto L_0x005f
            boolean r0 = r1.isEmpty()
            if (r0 == 0) goto L_0x005a
            java.lang.String r0 = "FlurryAgent"
            java.lang.String r7 = "No ads from server"
            com.flurry.android.g.a(r0, r7)
        L_0x005a:
            com.flurry.android.ab r0 = r13.aa
            r0.a(r1, r2, r3, r4, r5, r6)
        L_0x005f:
            return
        L_0x0060:
            r14.readInt()
            goto L_0x0045
        L_0x0064:
            byte r8 = r14.readByte()
            int r10 = r14.readUnsignedShort()
            com.flurry.android.ac[] r11 = new com.flurry.android.ac[r10]
            r0 = r7
        L_0x006f:
            if (r0 >= r10) goto L_0x007b
            com.flurry.android.ac r12 = new com.flurry.android.ac
            r12.<init>(r14)
            r11[r0] = r12
            int r0 = r0 + 1
            goto L_0x006f
        L_0x007b:
            java.lang.Byte r0 = java.lang.Byte.valueOf(r8)
            r1.put(r0, r11)
            goto L_0x0045
        L_0x0083:
            int r8 = r14.readUnsignedShort()
            r0 = r7
        L_0x0088:
            if (r0 >= r8) goto L_0x0045
            com.flurry.android.m r10 = new com.flurry.android.m
            r10.<init>(r14)
            long r11 = r10.a
            java.lang.Long r11 = java.lang.Long.valueOf(r11)
            r4.put(r11, r10)
            int r0 = r0 + 1
            goto L_0x0088
        L_0x009b:
            int r8 = r14.readInt()
            r0 = r7
        L_0x00a0:
            if (r0 >= r8) goto L_0x0045
            com.flurry.android.aj r10 = new com.flurry.android.aj
            r10.<init>(r14)
            java.lang.String r11 = r10.a
            r2.put(r11, r10)
            int r0 = r0 + 1
            goto L_0x00a0
        L_0x00af:
            r14.skipBytes(r0)
            goto L_0x0045
        L_0x00b3:
            byte r8 = r14.readByte()
            r0 = r7
        L_0x00b8:
            if (r0 >= r8) goto L_0x0045
            com.flurry.android.ah r10 = new com.flurry.android.ah
            r10.<init>(r14)
            byte r11 = r10.a
            java.lang.Byte r11 = java.lang.Byte.valueOf(r11)
            r3.put(r11, r10)
            int r0 = r0 + 1
            goto L_0x00b8
        L_0x00cb:
            byte r10 = r14.readByte()
            r8 = r7
        L_0x00d0:
            if (r8 >= r10) goto L_0x0045
            byte r0 = r14.readByte()
            java.lang.Byte r0 = java.lang.Byte.valueOf(r0)
            java.lang.Object r0 = r3.get(r0)
            com.flurry.android.ah r0 = (com.flurry.android.ah) r0
            if (r0 == 0) goto L_0x00e5
            r0.a(r14)
        L_0x00e5:
            int r0 = r8 + 1
            r8 = r0
            goto L_0x00d0
        L_0x00e9:
            int r8 = r14.readInt()
            r0 = r7
        L_0x00ee:
            if (r0 >= r8) goto L_0x0045
            long r10 = r14.readLong()
            short r12 = r14.readShort()
            java.lang.Short r12 = java.lang.Short.valueOf(r12)
            java.lang.Long r10 = java.lang.Long.valueOf(r10)
            r6.put(r12, r10)
            int r0 = r0 + 1
            goto L_0x00ee
        L_0x0106:
            long r10 = r14.readLong()
            java.lang.Long r0 = java.lang.Long.valueOf(r10)
            java.lang.Object r0 = r5.get(r0)
            com.flurry.android.a r0 = (com.flurry.android.a) r0
            if (r0 != 0) goto L_0x011b
            com.flurry.android.a r0 = new com.flurry.android.a
            r0.<init>()
        L_0x011b:
            java.lang.String r8 = r14.readUTF()
            r0.a = r8
            int r8 = r14.readInt()
            r0.c = r8
            java.lang.Long r8 = java.lang.Long.valueOf(r10)
            r5.put(r8, r0)
            goto L_0x0045
        L_0x0130:
            r14.skipBytes(r0)
            goto L_0x0045
        L_0x0135:
            r14.skipBytes(r0)
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.ag.a(java.io.DataInputStream):void");
    }

    private void b(boolean z2) {
        try {
            g.a("FlurryAgent", "generating report");
            byte[] a2 = a(z2);
            if (a2 == null) {
                g.a("FlurryAgent", "Error generating report");
            } else if (a(a2)) {
                g.a("FlurryAgent", "Done sending " + (this.v ? "initial " : "") + "agent report");
                j();
            }
        } catch (IOException e2) {
            g.a("FlurryAgent", "", e2);
        } catch (Throwable th) {
            g.b("FlurryAgent", "", th);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004d A[Catch:{ Throwable -> 0x00f2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0060 A[Catch:{ Throwable -> 0x00f2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x006e A[Catch:{ Throwable -> 0x00f2 }] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:48:0x00e5=Splitter:B:48:0x00e5, B:12:0x0046=Splitter:B:12:0x0046, B:20:0x005c=Splitter:B:20:0x005c} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void b(android.content.Context r9) {
        /*
            r8 = this;
            monitor-enter(r8)
            java.lang.String r0 = r8.c(r9)     // Catch:{ all -> 0x00e0 }
            r8.F = r0     // Catch:{ all -> 0x00e0 }
            java.io.File r0 = r8.u     // Catch:{ all -> 0x00e0 }
            boolean r0 = r0.exists()     // Catch:{ all -> 0x00e0 }
            if (r0 == 0) goto L_0x00fc
            java.lang.String r0 = "FlurryAgent"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e0 }
            r1.<init>()     // Catch:{ all -> 0x00e0 }
            java.lang.String r2 = "loading persistent data: "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00e0 }
            java.io.File r2 = r8.u     // Catch:{ all -> 0x00e0 }
            java.lang.String r2 = r2.getAbsolutePath()     // Catch:{ all -> 0x00e0 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00e0 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00e0 }
            com.flurry.android.g.c(r0, r1)     // Catch:{ all -> 0x00e0 }
            r2 = 0
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x0107, all -> 0x00e3 }
            java.io.File r1 = r8.u     // Catch:{ Throwable -> 0x0107, all -> 0x00e3 }
            r0.<init>(r1)     // Catch:{ Throwable -> 0x0107, all -> 0x00e3 }
            java.io.DataInputStream r1 = new java.io.DataInputStream     // Catch:{ Throwable -> 0x0107, all -> 0x00e3 }
            r1.<init>(r0)     // Catch:{ Throwable -> 0x0107, all -> 0x00e3 }
            int r0 = r1.readUnsignedShort()     // Catch:{ Throwable -> 0x00d3 }
            r2 = 46586(0xb5fa, float:6.5281E-41)
            if (r0 != r2) goto L_0x00ca
            r8.b(r1)     // Catch:{ Throwable -> 0x00d3 }
        L_0x0046:
            com.flurry.android.y.a(r1)     // Catch:{ all -> 0x00e0 }
        L_0x0049:
            boolean r0 = r8.w     // Catch:{ Throwable -> 0x00f2 }
            if (r0 != 0) goto L_0x005c
            java.io.File r0 = r8.u     // Catch:{ Throwable -> 0x00f2 }
            boolean r0 = r0.delete()     // Catch:{ Throwable -> 0x00f2 }
            if (r0 == 0) goto L_0x00e9
            java.lang.String r0 = "FlurryAgent"
            java.lang.String r1 = "Deleted persistence file"
            com.flurry.android.g.a(r0, r1)     // Catch:{ Throwable -> 0x00f2 }
        L_0x005c:
            boolean r0 = r8.w     // Catch:{ all -> 0x00e0 }
            if (r0 != 0) goto L_0x006a
            r0 = 0
            r8.G = r0     // Catch:{ all -> 0x00e0 }
            long r0 = r8.J     // Catch:{ all -> 0x00e0 }
            r8.H = r0     // Catch:{ all -> 0x00e0 }
            r0 = 1
            r8.w = r0     // Catch:{ all -> 0x00e0 }
        L_0x006a:
            java.lang.String r0 = r8.F     // Catch:{ all -> 0x00e0 }
            if (r0 != 0) goto L_0x00aa
            double r0 = java.lang.Math.random()     // Catch:{ all -> 0x00e0 }
            long r0 = java.lang.Double.doubleToLongBits(r0)     // Catch:{ all -> 0x00e0 }
            r2 = 37
            long r4 = java.lang.System.nanoTime()     // Catch:{ all -> 0x00e0 }
            java.lang.String r6 = r8.z     // Catch:{ all -> 0x00e0 }
            int r6 = r6.hashCode()     // Catch:{ all -> 0x00e0 }
            int r6 = r6 * 37
            long r6 = (long) r6     // Catch:{ all -> 0x00e0 }
            long r4 = r4 + r6
            long r2 = r2 * r4
            long r0 = r0 + r2
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e0 }
            r2.<init>()     // Catch:{ all -> 0x00e0 }
            java.lang.String r3 = "ID"
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x00e0 }
            r3 = 16
            java.lang.String r0 = java.lang.Long.toString(r0, r3)     // Catch:{ all -> 0x00e0 }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x00e0 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00e0 }
            r8.F = r0     // Catch:{ all -> 0x00e0 }
            java.lang.String r0 = "FlurryAgent"
            java.lang.String r1 = "Generated id"
            com.flurry.android.g.c(r0, r1)     // Catch:{ all -> 0x00e0 }
        L_0x00aa:
            com.flurry.android.ab r0 = r8.aa     // Catch:{ all -> 0x00e0 }
            java.lang.String r1 = r8.F     // Catch:{ all -> 0x00e0 }
            r0.a(r1)     // Catch:{ all -> 0x00e0 }
            java.lang.String r0 = r8.F     // Catch:{ all -> 0x00e0 }
            java.lang.String r1 = "AND"
            boolean r0 = r0.startsWith(r1)     // Catch:{ all -> 0x00e0 }
            if (r0 != 0) goto L_0x00c8
            java.io.File r0 = r8.t     // Catch:{ all -> 0x00e0 }
            boolean r0 = r0.exists()     // Catch:{ all -> 0x00e0 }
            if (r0 != 0) goto L_0x00c8
            java.lang.String r0 = r8.F     // Catch:{ all -> 0x00e0 }
            r8.c(r9, r0)     // Catch:{ all -> 0x00e0 }
        L_0x00c8:
            monitor-exit(r8)
            return
        L_0x00ca:
            java.lang.String r0 = "FlurryAgent"
            java.lang.String r2 = "Unexpected file type"
            com.flurry.android.g.a(r0, r2)     // Catch:{ Throwable -> 0x00d3 }
            goto L_0x0046
        L_0x00d3:
            r0 = move-exception
        L_0x00d4:
            java.lang.String r2 = "FlurryAgent"
            java.lang.String r3 = "Error when loading persistent file"
            com.flurry.android.g.b(r2, r3, r0)     // Catch:{ all -> 0x0105 }
            com.flurry.android.y.a(r1)     // Catch:{ all -> 0x00e0 }
            goto L_0x0049
        L_0x00e0:
            r0 = move-exception
            monitor-exit(r8)
            throw r0
        L_0x00e3:
            r0 = move-exception
            r1 = r2
        L_0x00e5:
            com.flurry.android.y.a(r1)     // Catch:{ all -> 0x00e0 }
            throw r0     // Catch:{ all -> 0x00e0 }
        L_0x00e9:
            java.lang.String r0 = "FlurryAgent"
            java.lang.String r1 = "Cannot delete persistence file"
            com.flurry.android.g.b(r0, r1)     // Catch:{ Throwable -> 0x00f2 }
            goto L_0x005c
        L_0x00f2:
            r0 = move-exception
            java.lang.String r1 = "FlurryAgent"
            java.lang.String r2 = ""
            com.flurry.android.g.b(r1, r2, r0)     // Catch:{ all -> 0x00e0 }
            goto L_0x005c
        L_0x00fc:
            java.lang.String r0 = "FlurryAgent"
            java.lang.String r1 = "Agent cache file doesn't exist."
            com.flurry.android.g.c(r0, r1)     // Catch:{ all -> 0x00e0 }
            goto L_0x005c
        L_0x0105:
            r0 = move-exception
            goto L_0x00e5
        L_0x0107:
            r0 = move-exception
            r1 = r2
            goto L_0x00d4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.ag.b(android.content.Context):void");
    }

    private synchronized void b(DataInputStream dataInputStream) {
        int i2 = 0;
        synchronized (this) {
            int readUnsignedShort = dataInputStream.readUnsignedShort();
            if (readUnsignedShort > 2) {
                g.b("FlurryAgent", "Unknown agent file version: " + readUnsignedShort);
                throw new IOException("Unknown agent file version: " + readUnsignedShort);
            } else if (readUnsignedShort >= 2) {
                String readUTF = dataInputStream.readUTF();
                g.a("FlurryAgent", "Loading API key: " + this.z);
                if (readUTF.equals(this.z)) {
                    String readUTF2 = dataInputStream.readUTF();
                    if (this.F == null) {
                        g.a("FlurryAgent", "Loading phoneId: " + readUTF2);
                    }
                    this.F = readUTF2;
                    this.G = dataInputStream.readBoolean();
                    this.H = dataInputStream.readLong();
                    g.a("FlurryAgent", "Loading session reports");
                    while (true) {
                        int readUnsignedShort2 = dataInputStream.readUnsignedShort();
                        if (readUnsignedShort2 == 0) {
                            break;
                        }
                        byte[] bArr = new byte[readUnsignedShort2];
                        dataInputStream.readFully(bArr);
                        this.I.add(0, bArr);
                        i2++;
                        g.a("FlurryAgent", "Session report added: " + i2);
                    }
                    g.a("FlurryAgent", "Persistent file loaded");
                    this.w = true;
                } else {
                    g.a("FlurryAgent", "Api keys do not match, old: " + readUTF + ", new: " + this.z);
                }
            } else {
                g.d("FlurryAgent", "Deleting old file version: " + readUnsignedShort);
            }
        }
    }

    /* access modifiers changed from: private */
    public synchronized void j() {
        DataOutputStream dataOutputStream;
        try {
            if (!a(this.u)) {
                y.a((Closeable) null);
            } else {
                dataOutputStream = new DataOutputStream(new FileOutputStream(this.u));
                try {
                    dataOutputStream.writeShort(46586);
                    dataOutputStream.writeShort(2);
                    dataOutputStream.writeUTF(this.z);
                    dataOutputStream.writeUTF(this.F);
                    dataOutputStream.writeBoolean(this.G);
                    dataOutputStream.writeLong(this.H);
                    int size = this.I.size() - 1;
                    while (true) {
                        if (size < 0) {
                            break;
                        }
                        byte[] bArr = (byte[]) this.I.get(size);
                        int length = bArr.length;
                        if (length + 2 + dataOutputStream.size() > 50000) {
                            g.a("FlurryAgent", "discarded sessions: " + size);
                            break;
                        }
                        dataOutputStream.writeShort(length);
                        dataOutputStream.write(bArr);
                        size--;
                    }
                    dataOutputStream.writeShort(0);
                    y.a(dataOutputStream);
                } catch (Throwable th) {
                    th = th;
                    try {
                        g.b("FlurryAgent", "", th);
                        y.a(dataOutputStream);
                    } catch (Throwable th2) {
                        th = th2;
                        y.a(dataOutputStream);
                        throw th;
                    }
                }
            }
        } catch (Throwable th3) {
            th = th3;
            dataOutputStream = null;
            y.a(dataOutputStream);
            throw th;
        }
    }

    private static boolean a(File file) {
        File parentFile = file.getParentFile();
        if (parentFile.mkdirs() || parentFile.exists()) {
            return true;
        }
        g.b("FlurryAgent", "Unable to create persistent dir: " + parentFile);
        return false;
    }

    private synchronized void c(Context context, String str) {
        DataOutputStream dataOutputStream;
        this.t = context.getFileStreamPath(".flurryb.");
        if (a(this.t)) {
            try {
                dataOutputStream = new DataOutputStream(new FileOutputStream(this.t));
                try {
                    dataOutputStream.writeInt(1);
                    dataOutputStream.writeUTF(str);
                    y.a(dataOutputStream);
                } catch (Throwable th) {
                    th = th;
                    try {
                        g.b("FlurryAgent", "Error when saving b file", th);
                        y.a(dataOutputStream);
                    } catch (Throwable th2) {
                        th = th2;
                        y.a(dataOutputStream);
                        throw th;
                    }
                }
            } catch (Throwable th3) {
                th = th3;
                dataOutputStream = null;
                y.a(dataOutputStream);
                throw th;
            }
        }
    }

    private String c(Context context) {
        DataInputStream dataInputStream;
        Throwable th;
        boolean z2 = false;
        if (this.F != null) {
            return this.F;
        }
        String string = Settings.System.getString(context.getContentResolver(), "android_id");
        if (string != null && string.length() > 0 && !string.equals("null")) {
            String[] strArr = b;
            int length = strArr.length;
            int i2 = 0;
            while (true) {
                if (i2 < length) {
                    if (string.equals(strArr[i2])) {
                        break;
                    }
                    i2++;
                } else {
                    z2 = true;
                    break;
                }
            }
        }
        if (z2) {
            return "AND" + string;
        }
        File fileStreamPath = context.getFileStreamPath(".flurryb.");
        if (!fileStreamPath.exists()) {
            return null;
        }
        try {
            dataInputStream = new DataInputStream(new FileInputStream(fileStreamPath));
            try {
                dataInputStream.readInt();
                String readUTF = dataInputStream.readUTF();
                y.a(dataInputStream);
                return readUTF;
            } catch (Throwable th2) {
                th = th2;
                try {
                    g.b("FlurryAgent", "Error when loading b file", th);
                    y.a(dataInputStream);
                    return null;
                } catch (Throwable th3) {
                    th = th3;
                    y.a(dataInputStream);
                    throw th;
                }
            }
        } catch (Throwable th4) {
            dataInputStream = null;
            th = th4;
            y.a(dataInputStream);
            throw th;
        }
    }

    private static String d(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            if (packageInfo.versionName != null) {
                return packageInfo.versionName;
            }
            if (packageInfo.versionCode != 0) {
                return Integer.toString(packageInfo.versionCode);
            }
            return "Unknown";
        } catch (Throwable th) {
            g.b("FlurryAgent", "", th);
        }
    }

    private Location e(Context context) {
        if (context.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0 || context.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
            LocationManager locationManager = (LocationManager) context.getSystemService("location");
            synchronized (this) {
                if (this.E == null) {
                    this.E = locationManager;
                } else {
                    locationManager = this.E;
                }
            }
            Criteria criteria = p;
            if (criteria == null) {
                criteria = new Criteria();
            }
            String bestProvider = locationManager.getBestProvider(criteria, true);
            if (bestProvider != null) {
                locationManager.requestLocationUpdates(bestProvider, 0, 0.0f, this, Looper.getMainLooper());
                return locationManager.getLastKnownLocation(bestProvider);
            }
        }
        return null;
    }

    private synchronized void k() {
        if (this.E != null) {
            this.E.removeUpdates(this);
        }
    }

    static String d() {
        return j.z;
    }

    public final synchronized void onLocationChanged(Location location) {
        try {
            this.T = location;
            k();
        } catch (Throwable th) {
            g.b("FlurryAgent", "", th);
        }
        return;
    }

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i2, Bundle bundle) {
    }

    private static HttpClient a(HttpParams httpParams) {
        try {
            KeyStore instance = KeyStore.getInstance(KeyStore.getDefaultType());
            instance.load(null, null);
            h hVar = new h(instance);
            SchemeRegistry schemeRegistry = new SchemeRegistry();
            schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            schemeRegistry.register(new Scheme("https", hVar, 443));
            return new DefaultHttpClient(new ThreadSafeClientConnManager(httpParams, schemeRegistry), httpParams);
        } catch (Exception e2) {
            return new DefaultHttpClient(httpParams);
        }
    }
}
