package com.flurry.android;

import android.content.Context;
import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

final class ak {
    private Context a;
    private ab b;
    private ae c;
    private volatile long d;
    private e e = new e();
    private e f = new e();
    private Map g = new HashMap();
    private Map h = new HashMap();
    private Map i = new HashMap();
    private Map j = new HashMap();
    private volatile boolean k;

    ak() {
    }

    /* access modifiers changed from: package-private */
    public final void a(Context context, ab abVar, ae aeVar) {
        this.a = context;
        this.b = abVar;
        this.c = aeVar;
    }

    /* access modifiers changed from: package-private */
    public final synchronized ac[] a(String str) {
        ac[] acVarArr;
        acVarArr = (ac[]) this.g.get(str);
        if (acVarArr == null) {
            acVarArr = (ac[]) this.g.get("");
        }
        return acVarArr;
    }

    private synchronized a b(long j2) {
        return (a) this.f.a(Long.valueOf(j2));
    }

    /* access modifiers changed from: package-private */
    public final synchronized Set a() {
        return this.e.c();
    }

    /* access modifiers changed from: package-private */
    public final synchronized m a(long j2) {
        return (m) this.e.a(Long.valueOf(j2));
    }

    /* access modifiers changed from: package-private */
    public final synchronized m b() {
        Long l;
        l = (Long) this.j.get((short) 1);
        return l == null ? null : a(l.longValue());
    }

    /* access modifiers changed from: package-private */
    public final synchronized aj b(String str) {
        aj ajVar;
        ajVar = (aj) this.h.get(str);
        if (ajVar == null) {
            ajVar = (aj) this.h.get("");
        }
        return ajVar;
    }

    /* access modifiers changed from: package-private */
    public final boolean c() {
        return this.k;
    }

    private synchronized ah a(byte b2) {
        return (ah) this.i.get(Byte.valueOf(b2));
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(Map map, Map map2, Map map3, Map map4, Map map5, Map map6) {
        this.d = System.currentTimeMillis();
        for (Map.Entry entry : map4.entrySet()) {
            if (entry.getValue() != null) {
                this.e.a(entry.getKey(), entry.getValue());
            }
        }
        for (Map.Entry entry2 : map5.entrySet()) {
            if (entry2.getValue() != null) {
                this.f.a(entry2.getKey(), entry2.getValue());
            }
        }
        if (map2 != null && !map2.isEmpty()) {
            this.h = map2;
        }
        if (map3 != null && !map3.isEmpty()) {
            this.i = map3;
        }
        if (map6 != null && !map6.isEmpty()) {
            this.j = map6;
        }
        this.g = new HashMap();
        for (Map.Entry entry3 : map2.entrySet()) {
            aj ajVar = (aj) entry3.getValue();
            ac[] acVarArr = (ac[]) map.get(Byte.valueOf(ajVar.b));
            if (acVarArr != null) {
                this.g.put(entry3.getKey(), acVarArr);
            }
            ah ahVar = (ah) map3.get(Byte.valueOf(ajVar.c));
            if (ahVar != null) {
                ajVar.d = ahVar;
            }
        }
        g();
        a(202);
    }

    /* access modifiers changed from: package-private */
    public final long d() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:28:0x004a=Splitter:B:28:0x004a, B:11:0x002d=Splitter:B:11:0x002d} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void e() {
        /*
            r5 = this;
            monitor-enter(r5)
            android.content.Context r0 = r5.a     // Catch:{ all -> 0x0045 }
            java.lang.String r1 = r5.h()     // Catch:{ all -> 0x0045 }
            java.io.File r3 = r0.getFileStreamPath(r1)     // Catch:{ all -> 0x0045 }
            boolean r0 = r3.exists()     // Catch:{ all -> 0x0045 }
            if (r0 == 0) goto L_0x004e
            r2 = 0
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ Throwable -> 0x006d, all -> 0x0048 }
            r0.<init>(r3)     // Catch:{ Throwable -> 0x006d, all -> 0x0048 }
            java.io.DataInputStream r1 = new java.io.DataInputStream     // Catch:{ Throwable -> 0x006d, all -> 0x0048 }
            r1.<init>(r0)     // Catch:{ Throwable -> 0x006d, all -> 0x0048 }
            int r0 = r1.readUnsignedShort()     // Catch:{ Throwable -> 0x0036 }
            r2 = 46587(0xb5fb, float:6.5282E-41)
            if (r0 != r2) goto L_0x0032
            r5.a(r1)     // Catch:{ Throwable -> 0x0036 }
            r0 = 201(0xc9, float:2.82E-43)
            r5.a(r0)     // Catch:{ Throwable -> 0x0036 }
        L_0x002d:
            com.flurry.android.y.a(r1)     // Catch:{ all -> 0x0045 }
        L_0x0030:
            monitor-exit(r5)
            return
        L_0x0032:
            a(r3)     // Catch:{ Throwable -> 0x0036 }
            goto L_0x002d
        L_0x0036:
            r0 = move-exception
        L_0x0037:
            java.lang.String r2 = "FlurryAgent"
            java.lang.String r4 = "Discarding cache"
            com.flurry.android.g.a(r2, r4, r0)     // Catch:{ all -> 0x006b }
            a(r3)     // Catch:{ all -> 0x006b }
            com.flurry.android.y.a(r1)     // Catch:{ all -> 0x0045 }
            goto L_0x0030
        L_0x0045:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        L_0x0048:
            r0 = move-exception
            r1 = r2
        L_0x004a:
            com.flurry.android.y.a(r1)     // Catch:{ all -> 0x0045 }
            throw r0     // Catch:{ all -> 0x0045 }
        L_0x004e:
            java.lang.String r0 = "FlurryAgent"
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0045 }
            r1.<init>()     // Catch:{ all -> 0x0045 }
            java.lang.String r2 = "cache file does not exist, path="
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0045 }
            java.lang.String r2 = r3.getAbsolutePath()     // Catch:{ all -> 0x0045 }
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x0045 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0045 }
            com.flurry.android.g.c(r0, r1)     // Catch:{ all -> 0x0045 }
            goto L_0x0030
        L_0x006b:
            r0 = move-exception
            goto L_0x004a
        L_0x006d:
            r0 = move-exception
            r1 = r2
            goto L_0x0037
        */
        throw new UnsupportedOperationException("Method not decompiled: com.flurry.android.ak.e():void");
    }

    private static void a(File file) {
        if (!file.delete()) {
            g.b("FlurryAgent", "Cannot delete cached ads");
        }
    }

    private void g() {
        Iterator it = this.i.values().iterator();
        while (it.hasNext()) {
            it.next();
        }
        for (ac[] acVarArr : this.g.values()) {
            if (acVarArr != null) {
                for (ac acVar : acVarArr) {
                    acVar.h = a(acVar.f.longValue());
                    if (acVar.h == null) {
                        g.b("FlurryAgent", "Ad " + acVar.d + " has no image");
                    }
                    if (b(acVar.a) == null) {
                        g.b("FlurryAgent", "Ad " + acVar.d + " has no pricing");
                    }
                }
            }
        }
        for (aj ajVar : this.h.values()) {
            ajVar.d = a(ajVar.c);
            if (ajVar.d == null) {
                g.d("FlurryAgent", "No ad theme found for " + ((int) ajVar.c));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void f() {
        DataOutputStream dataOutputStream = null;
        synchronized (this) {
            try {
                File fileStreamPath = this.a.getFileStreamPath(h());
                File parentFile = fileStreamPath.getParentFile();
                if (parentFile.mkdirs() || parentFile.exists()) {
                    DataOutputStream dataOutputStream2 = new DataOutputStream(new FileOutputStream(fileStreamPath));
                    try {
                        dataOutputStream2.writeShort(46587);
                        a(dataOutputStream2);
                        y.a(dataOutputStream2);
                    } catch (Throwable th) {
                        th = th;
                        dataOutputStream = dataOutputStream2;
                        y.a(dataOutputStream);
                        throw th;
                    }
                } else {
                    g.b("FlurryAgent", "Unable to create persistent dir: " + parentFile);
                    y.a((Closeable) null);
                }
            } catch (Throwable th2) {
                th = th2;
                g.b("FlurryAgent", "", th);
                y.a(dataOutputStream);
            }
        }
    }

    private void a(DataInputStream dataInputStream) {
        g.a("FlurryAgent", "Reading cache");
        if (dataInputStream.readUnsignedShort() == 2) {
            this.d = dataInputStream.readLong();
            int readUnsignedShort = dataInputStream.readUnsignedShort();
            this.e = new e();
            for (int i2 = 0; i2 < readUnsignedShort; i2++) {
                long readLong = dataInputStream.readLong();
                m mVar = new m();
                mVar.a(dataInputStream);
                this.e.a(Long.valueOf(readLong), mVar);
            }
            int readUnsignedShort2 = dataInputStream.readUnsignedShort();
            this.f = new e();
            for (int i3 = 0; i3 < readUnsignedShort2; i3++) {
                long readLong2 = dataInputStream.readLong();
                a aVar = new a();
                if (dataInputStream.readBoolean()) {
                    aVar.a = dataInputStream.readUTF();
                }
                if (dataInputStream.readBoolean()) {
                    aVar.b = dataInputStream.readUTF();
                }
                aVar.c = dataInputStream.readInt();
                this.f.a(Long.valueOf(readLong2), aVar);
            }
            int readUnsignedShort3 = dataInputStream.readUnsignedShort();
            this.h = new HashMap(readUnsignedShort3);
            for (int i4 = 0; i4 < readUnsignedShort3; i4++) {
                this.h.put(dataInputStream.readUTF(), new aj(dataInputStream));
            }
            int readUnsignedShort4 = dataInputStream.readUnsignedShort();
            this.g = new HashMap(readUnsignedShort4);
            for (int i5 = 0; i5 < readUnsignedShort4; i5++) {
                String readUTF = dataInputStream.readUTF();
                int readUnsignedShort5 = dataInputStream.readUnsignedShort();
                ac[] acVarArr = new ac[readUnsignedShort5];
                for (int i6 = 0; i6 < readUnsignedShort5; i6++) {
                    ac acVar = new ac();
                    acVar.a(dataInputStream);
                    acVarArr[i6] = acVar;
                }
                this.g.put(readUTF, acVarArr);
            }
            int readUnsignedShort6 = dataInputStream.readUnsignedShort();
            this.i = new HashMap();
            for (int i7 = 0; i7 < readUnsignedShort6; i7++) {
                byte readByte = dataInputStream.readByte();
                ah ahVar = new ah();
                ahVar.b(dataInputStream);
                this.i.put(Byte.valueOf(readByte), ahVar);
            }
            int readUnsignedShort7 = dataInputStream.readUnsignedShort();
            this.j = new HashMap(readUnsignedShort7);
            for (int i8 = 0; i8 < readUnsignedShort7; i8++) {
                this.j.put(Short.valueOf(dataInputStream.readShort()), Long.valueOf(dataInputStream.readLong()));
            }
            g();
            g.a("FlurryAgent", "Cache read, num images: " + this.e.a());
        }
    }

    private void a(DataOutputStream dataOutputStream) {
        dataOutputStream.writeShort(2);
        dataOutputStream.writeLong(this.d);
        List<Map.Entry> b2 = this.e.b();
        dataOutputStream.writeShort(b2.size());
        for (Map.Entry entry : b2) {
            dataOutputStream.writeLong(((Long) entry.getKey()).longValue());
            m mVar = (m) entry.getValue();
            dataOutputStream.writeLong(mVar.a);
            dataOutputStream.writeInt(mVar.b);
            dataOutputStream.writeInt(mVar.c);
            dataOutputStream.writeUTF(mVar.d);
            dataOutputStream.writeInt(mVar.e.length);
            dataOutputStream.write(mVar.e);
        }
        List<Map.Entry> b3 = this.f.b();
        dataOutputStream.writeShort(b3.size());
        for (Map.Entry entry2 : b3) {
            dataOutputStream.writeLong(((Long) entry2.getKey()).longValue());
            a aVar = (a) entry2.getValue();
            boolean z = aVar.a != null;
            dataOutputStream.writeBoolean(z);
            if (z) {
                dataOutputStream.writeUTF(aVar.a);
            }
            boolean z2 = aVar.b != null;
            dataOutputStream.writeBoolean(z2);
            if (z2) {
                dataOutputStream.writeUTF(aVar.b);
            }
            dataOutputStream.writeInt(aVar.c);
        }
        dataOutputStream.writeShort(this.h.size());
        for (Map.Entry entry3 : this.h.entrySet()) {
            dataOutputStream.writeUTF((String) entry3.getKey());
            aj ajVar = (aj) entry3.getValue();
            dataOutputStream.writeUTF(ajVar.a);
            dataOutputStream.writeByte(ajVar.b);
            dataOutputStream.writeByte(ajVar.c);
        }
        dataOutputStream.writeShort(this.g.size());
        for (Map.Entry entry4 : this.g.entrySet()) {
            dataOutputStream.writeUTF((String) entry4.getKey());
            ac[] acVarArr = (ac[]) entry4.getValue();
            int length = acVarArr == null ? 0 : acVarArr.length;
            dataOutputStream.writeShort(length);
            for (int i2 = 0; i2 < length; i2++) {
                ac acVar = acVarArr[i2];
                dataOutputStream.writeLong(acVar.a);
                dataOutputStream.writeLong(acVar.b);
                dataOutputStream.writeUTF(acVar.d);
                dataOutputStream.writeUTF(acVar.c);
                dataOutputStream.writeLong(acVar.e);
                dataOutputStream.writeLong(acVar.f.longValue());
                dataOutputStream.writeByte(acVar.g.length);
                dataOutputStream.write(acVar.g);
            }
        }
        dataOutputStream.writeShort(this.i.size());
        for (Map.Entry entry5 : this.i.entrySet()) {
            dataOutputStream.writeByte(((Byte) entry5.getKey()).byteValue());
            ((ah) entry5.getValue()).a(dataOutputStream);
        }
        dataOutputStream.writeShort(this.j.size());
        for (Map.Entry entry6 : this.j.entrySet()) {
            dataOutputStream.writeShort(((Short) entry6.getKey()).shortValue());
            dataOutputStream.writeLong(((Long) entry6.getValue()).longValue());
        }
    }

    private String h() {
        return ".flurryappcircle." + Integer.toString(this.c.a.hashCode(), 16);
    }

    private void a(int i2) {
        this.k = !this.g.isEmpty();
        if (this.k) {
            this.b.a(i2);
        }
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        sb.append("adImages (" + this.e.b().size() + "),\n");
        sb.append("adBlock (" + this.g.size() + "):").append(",\n");
        for (Map.Entry entry : this.g.entrySet()) {
            sb.append("\t" + ((String) entry.getKey()) + ": " + Arrays.toString((Object[]) entry.getValue()));
        }
        sb.append("adHooks (" + this.h.size() + "):" + this.h).append(",\n");
        sb.append("adThemes (" + this.i.size() + "):" + this.i).append(",\n");
        sb.append("auxMap (" + this.j.size() + "):" + this.j).append(",\n");
        sb.append("}");
        return sb.toString();
    }
}
