package com.flurry.android;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

final class ab implements View.OnClickListener {
    private static volatile long A = 0;
    static String a = "FlurryAgent";
    static String b = "";
    private static volatile String c = "market://";
    private static volatile String d = "market://details?id=";
    private static volatile String e = "https://market.android.com/details?id=";
    private static String f = "com.flurry.android.ACTION_CATALOG";
    private static int g = 5000;
    private String h;
    private String i;
    private String j;
    private long k;
    private long l;
    private long m;
    private long n;
    private ak o = new ak();
    private boolean p = true;
    private volatile boolean q;
    private String r;
    private Map s = new HashMap();
    private Handler t;
    private boolean u;
    private transient Map v = new HashMap();
    private f w;
    private List x = new ArrayList();
    private Map y = new HashMap();
    private ao z;

    static /* synthetic */ void a(ab abVar, Context context, String str) {
        if (str.startsWith(d)) {
            String substring = str.substring(d.length());
            if (abVar.p) {
                try {
                    g.a(a, "Launching Android Market for app " + substring);
                    context.startActivity(new Intent("android.intent.action.VIEW").setData(Uri.parse(str)));
                } catch (Exception e2) {
                    g.c(a, "Cannot launch Marketplace url " + str, e2);
                }
            } else {
                g.a(a, "Launching Android Market website for app " + substring);
                context.startActivity(new Intent("android.intent.action.VIEW").setData(Uri.parse(e + substring)));
            }
        } else {
            g.d(a, "Unexpected android market url scheme: " + str);
        }
    }

    static {
        new Random(System.currentTimeMillis());
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(Context context, ae aeVar) {
        boolean z2 = true;
        synchronized (this) {
            if (!this.q) {
                this.h = aeVar.c;
                this.i = aeVar.d;
                this.j = aeVar.a;
                this.k = aeVar.b;
                this.t = aeVar.e;
                this.w = new f(this.t, g);
                context.getResources().getDisplayMetrics();
                this.y.clear();
                this.v.clear();
                this.o.a(context, this, aeVar);
                this.s.clear();
                PackageManager packageManager = context.getPackageManager();
                Intent intent = new Intent("android.intent.action.VIEW");
                intent.setData(Uri.parse(d + context.getPackageName()));
                if (packageManager.queryIntentActivities(intent, 65536).size() <= 0) {
                    z2 = false;
                }
                this.p = z2;
                this.q = true;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(long j2, long j3) {
        this.l = j2;
        this.m = j3;
        this.n = 0;
        this.x.clear();
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        return this.q;
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        this.r = str;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void b() {
        if (l()) {
            this.o.e();
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void c() {
        if (l()) {
            this.o.f();
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(Map map, Map map2, Map map3, Map map4, Map map5, Map map6) {
        if (l()) {
            this.o.a(map, map2, map3, map4, map5, map6);
            Log.i("FlurryAgent", this.o.toString());
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized long d() {
        long d2;
        if (!l()) {
            d2 = 0;
        } else {
            d2 = this.o.d();
        }
        return d2;
    }

    /* access modifiers changed from: package-private */
    public final synchronized Set e() {
        Set a2;
        if (!l()) {
            a2 = Collections.emptySet();
        } else {
            a2 = this.o.a();
        }
        return a2;
    }

    /* access modifiers changed from: package-private */
    public final synchronized m a(long j2) {
        m a2;
        if (!l()) {
            a2 = null;
        } else {
            a2 = this.o.a(j2);
        }
        return a2;
    }

    private synchronized m k() {
        m b2;
        if (!l()) {
            b2 = null;
        } else {
            b2 = this.o.b();
        }
        return b2;
    }

    /* access modifiers changed from: package-private */
    public final synchronized List f() {
        return this.x;
    }

    /* access modifiers changed from: package-private */
    public final synchronized w b(long j2) {
        return (w) this.v.get(Long.valueOf(j2));
    }

    /* access modifiers changed from: package-private */
    public final synchronized void g() {
        this.v.clear();
    }

    /* access modifiers changed from: package-private */
    public final String h() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(Context context, w wVar, String str) {
        if (l()) {
            this.t.post(new c(this, str, context, wVar));
        }
    }

    /* access modifiers changed from: private */
    public String b(String str) {
        try {
            if (str.startsWith(c)) {
                return str;
            }
            HttpResponse execute = new DefaultHttpClient().execute(new HttpGet(str));
            int statusCode = execute.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                String entityUtils = EntityUtils.toString(execute.getEntity());
                if (!entityUtils.startsWith(c)) {
                    return b(entityUtils);
                }
                return entityUtils;
            }
            g.c(a, "Cannot process with responseCode " + statusCode);
            c("Error when fetching application's android market ID, responseCode " + statusCode);
            return str;
        } catch (UnknownHostException e2) {
            g.c(a, "Unknown host: " + e2.getMessage());
            if (this.z != null) {
                c("Unknown host: " + e2.getMessage());
            }
            return null;
        } catch (Exception e3) {
            g.c(a, "Failed on url: " + str, e3);
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        a(new l(this, str));
    }

    /* access modifiers changed from: package-private */
    public final synchronized List a(Context context, List list, Long l2, int i2, boolean z2) {
        List emptyList;
        List list2;
        if (!l()) {
            emptyList = Collections.emptyList();
        } else if (!this.o.c() || list == null) {
            emptyList = Collections.emptyList();
        } else {
            if (list == null || list.isEmpty() || !this.o.c()) {
                list2 = Collections.emptyList();
            } else {
                ac[] a2 = this.o.a((String) list.get(0));
                if (a2 == null || a2.length <= 0) {
                    list2 = Collections.emptyList();
                } else {
                    ArrayList arrayList = new ArrayList(Arrays.asList(a2));
                    Collections.shuffle(arrayList);
                    if (l2 != null) {
                        Iterator it = arrayList.iterator();
                        while (true) {
                            if (it.hasNext()) {
                                if (((ac) it.next()).a == l2.longValue()) {
                                    it.remove();
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                    }
                    list2 = arrayList.subList(0, Math.min(arrayList.size(), list.size()));
                }
            }
            int min = Math.min(list.size(), list2.size());
            ArrayList arrayList2 = new ArrayList();
            for (int i3 = 0; i3 < min; i3++) {
                String str = (String) list.get(i3);
                aj b2 = this.o.b(str);
                if (b2 != null) {
                    w wVar = new w((String) list.get(i3), (byte) 1, i());
                    if (this.x.size() < 32767) {
                        this.x.add(wVar);
                        this.v.put(Long.valueOf(wVar.a()), wVar);
                    }
                    if (i3 < list2.size()) {
                        wVar.b = (ac) list2.get(i3);
                        wVar.a(new ai((byte) 2, i()));
                        arrayList2.add(new al(context, this, wVar, b2, i2, z2));
                    }
                } else {
                    g.d(a, "Cannot find hook: " + str);
                }
            }
            emptyList = arrayList2;
        }
        return emptyList;
    }

    /* access modifiers changed from: package-private */
    public final synchronized long i() {
        long elapsedRealtime = SystemClock.elapsedRealtime() - this.m;
        if (elapsedRealtime <= this.n) {
            elapsedRealtime = this.n + 1;
            this.n = elapsedRealtime;
        }
        this.n = elapsedRealtime;
        return this.n;
    }

    public final synchronized void onClick(View view) {
        w wVar;
        al alVar = (al) view;
        w a2 = alVar.a();
        if (!this.x.contains(a2)) {
            wVar = new w(a2, i());
            this.x.add(wVar);
            alVar.a(wVar);
        } else {
            wVar = a2;
        }
        wVar.a(new ai((byte) 4, i()));
        String a3 = a(wVar);
        if (this.u) {
            Context context = view.getContext();
            String str = this.h + a3;
            Intent intent = new Intent(ag.a != null ? ag.a : f);
            intent.addCategory("android.intent.category.DEFAULT");
            intent.putExtra("u", str);
            if (wVar != null) {
                intent.putExtra("o", wVar.a());
            }
            context.startActivity(intent);
        } else {
            a(view.getContext(), wVar, this.i + a3);
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized String a(w wVar) {
        StringBuilder append;
        ac acVar = wVar.b;
        append = new StringBuilder().append("?apik=").append(this.j).append("&cid=").append(acVar.e).append("&adid=").append(acVar.a).append("&pid=").append(this.r).append("&iid=").append(this.k).append("&sid=").append(this.l).append("&its=").append(wVar.a()).append("&hid=").append(y.a(wVar.a)).append("&ac=").append(a(acVar.g));
        if (this.s != null && !this.s.isEmpty()) {
            for (Map.Entry entry : this.s.entrySet()) {
                append.append("&").append("c_" + y.a((String) entry.getKey())).append("=").append(y.a((String) entry.getValue()));
            }
        }
        append.append("&ats=").append(System.currentTimeMillis());
        return append.toString();
    }

    private static String a(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < bArr.length; i2++) {
            int i3 = (bArr[i2] >> 4) & 15;
            if (i3 < 10) {
                sb.append((char) (i3 + 48));
            } else {
                sb.append((char) ((i3 + 65) - 10));
            }
            byte b2 = bArr[i2] & 15;
            if (b2 < 10) {
                sb.append((char) (b2 + 48));
            } else {
                sb.append((char) ((b2 + 65) - 10));
            }
        }
        return sb.toString();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[adLogs=").append(this.x).append("]");
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public final synchronized m j() {
        m k2;
        if (!l()) {
            k2 = null;
        } else {
            k2 = k();
        }
        return k2;
    }

    private static void a(Runnable runnable) {
        new Handler().post(runnable);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(int i2) {
        if (this.z != null) {
            a(new k(this, i2));
        }
    }

    private boolean l() {
        if (!this.q) {
            g.d(a, "AppCircle is not initialized");
        }
        if (this.r == null) {
            g.d(a, "Cannot identify UDID.");
        }
        return this.q;
    }
}
