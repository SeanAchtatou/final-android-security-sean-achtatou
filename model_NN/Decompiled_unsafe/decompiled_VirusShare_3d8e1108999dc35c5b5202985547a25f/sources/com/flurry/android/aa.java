package com.flurry.android;

import java.io.Closeable;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Map;

final class aa implements Runnable {
    private /* synthetic */ Map a;
    private /* synthetic */ InstallReceiver b;

    aa(InstallReceiver installReceiver, Map map) {
        this.b = installReceiver;
        this.a = map;
    }

    public final void run() {
        boolean z;
        DataOutputStream dataOutputStream = null;
        try {
            File parentFile = this.b.b.getParentFile();
            if (parentFile.mkdirs() || parentFile.exists()) {
                DataOutputStream dataOutputStream2 = new DataOutputStream(new FileOutputStream(this.b.b));
                try {
                    boolean z2 = true;
                    for (Map.Entry entry : this.a.entrySet()) {
                        if (z2) {
                            z = false;
                        } else {
                            dataOutputStream2.writeUTF("&");
                            z = z2;
                        }
                        dataOutputStream2.writeUTF((String) entry.getKey());
                        dataOutputStream2.writeUTF("=");
                        dataOutputStream2.writeUTF((String) entry.getValue());
                        z2 = z;
                    }
                    dataOutputStream2.writeShort(0);
                    y.a(dataOutputStream2);
                } catch (Throwable th) {
                    th = th;
                    dataOutputStream = dataOutputStream2;
                    y.a(dataOutputStream);
                    throw th;
                }
            } else {
                g.b("InstallReceiver", "Unable to create persistent dir: " + parentFile);
                y.a((Closeable) null);
            }
        } catch (Throwable th2) {
            th = th2;
            g.b("InstallReceiver", "", th);
            y.a(dataOutputStream);
        }
    }
}
