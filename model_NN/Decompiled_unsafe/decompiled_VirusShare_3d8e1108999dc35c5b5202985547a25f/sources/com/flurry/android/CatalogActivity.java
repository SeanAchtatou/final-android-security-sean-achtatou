package com.flurry.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import java.util.ArrayList;
import java.util.List;

public class CatalogActivity extends Activity implements View.OnClickListener {
    private static volatile String a = "<html><body><table height='100%' width='100%' border='0'><tr><td style='vertical-align:middle;text-align:center'>No recommendations available<p><button type='input' onClick='activity.finish()'>Back</button></td></tr></table></body></html>";
    private WebView b;
    private an c;
    private List d = new ArrayList();
    /* access modifiers changed from: private */
    public ab e;
    /* access modifiers changed from: private */
    public w f;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Long valueOf;
        setTheme(16973839);
        super.onCreate(bundle);
        this.e = ag.c();
        Intent intent = getIntent();
        if (!(intent.getExtras() == null || (valueOf = Long.valueOf(intent.getExtras().getLong("o"))) == null)) {
            this.f = this.e.b(valueOf.longValue());
        }
        i iVar = new i(this, this);
        iVar.setId(1);
        iVar.setBackgroundColor(-16777216);
        this.b = new WebView(this);
        this.b.setId(2);
        this.b.setScrollBarStyle(0);
        this.b.setBackgroundColor(-1);
        if (this.f != null) {
            this.b.setWebViewClient(new x(this));
        }
        this.b.getSettings().setJavaScriptEnabled(true);
        this.b.addJavascriptInterface(this, "activity");
        this.c = new an(this, this);
        this.c.setId(3);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(10, iVar.getId());
        relativeLayout.addView(iVar, layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams2.addRule(3, iVar.getId());
        layoutParams2.addRule(2, this.c.getId());
        relativeLayout.addView(this.b, layoutParams2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams3.addRule(12, iVar.getId());
        relativeLayout.addView(this.c, layoutParams3);
        Bundle extras = getIntent().getExtras();
        String string = extras == null ? null : extras.getString("u");
        if (string == null) {
            this.b.loadDataWithBaseURL(null, a, "text/html", "utf-8", null);
        } else {
            this.b.loadUrl(string);
        }
        setContentView(relativeLayout);
    }

    public void finish() {
        super.finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.e.g();
        super.onDestroy();
    }

    public void onClick(View view) {
        if (view instanceof al) {
            am amVar = new am();
            amVar.a = this.f;
            amVar.b = this.b.getUrl();
            amVar.c = new ArrayList(this.c.b());
            this.d.add(amVar);
            if (this.d.size() > 5) {
                this.d.remove(0);
            }
            am amVar2 = new am();
            al alVar = (al) view;
            this.f = alVar.a();
            amVar2.a = alVar.a();
            amVar2.a.a(new ai((byte) 4, this.e.i()));
            amVar2.b = this.e.h() + this.e.a(alVar.a());
            amVar2.c = this.c.a(view.getContext());
            a(amVar2);
        } else if (view.getId() == 10000) {
            finish();
        } else if (view.getId() == 10002) {
            this.c.a();
        } else if (this.d.isEmpty()) {
            finish();
        } else {
            a((am) this.d.remove(this.d.size() - 1));
        }
    }

    private void a(am amVar) {
        try {
            this.b.loadUrl(amVar.b);
            this.c.a(amVar.c);
        } catch (Exception e2) {
            g.a("FlurryAgent", "Error loading url: " + amVar.b);
        }
    }
}
