package com.flurry.android;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Map;

final class p {
    private String a;
    private Map b;
    private long c;
    private boolean d = false;
    private long e;
    private byte[] f;

    public p(String str, Map map, long j) {
        this.a = str;
        this.b = map;
        this.c = j;
    }

    public final byte[] a() {
        DataOutputStream dataOutputStream;
        Throwable th;
        if (this.f == null) {
            DataOutputStream dataOutputStream2 = null;
            try {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                dataOutputStream = new DataOutputStream(byteArrayOutputStream);
                try {
                    dataOutputStream.writeUTF(this.a);
                    if (this.b == null) {
                        dataOutputStream.writeShort(0);
                    } else {
                        dataOutputStream.writeShort(this.b.size());
                        for (Map.Entry entry : this.b.entrySet()) {
                            dataOutputStream.writeUTF(y.a((String) entry.getKey(), 255));
                            dataOutputStream.writeUTF(y.a((String) entry.getValue(), 255));
                        }
                    }
                    dataOutputStream.writeLong(this.c);
                    dataOutputStream.writeLong(this.e);
                    dataOutputStream.flush();
                    this.f = byteArrayOutputStream.toByteArray();
                    y.a(dataOutputStream);
                } catch (IOException e2) {
                    dataOutputStream2 = dataOutputStream;
                    try {
                        this.f = new byte[0];
                        y.a(dataOutputStream2);
                        return this.f;
                    } catch (Throwable th2) {
                        dataOutputStream = dataOutputStream2;
                        th = th2;
                        y.a(dataOutputStream);
                        throw th;
                    }
                } catch (Throwable th3) {
                    th = th3;
                    y.a(dataOutputStream);
                    throw th;
                }
            } catch (IOException e3) {
                this.f = new byte[0];
                y.a(dataOutputStream2);
                return this.f;
            } catch (Throwable th4) {
                dataOutputStream = null;
                th = th4;
                y.a(dataOutputStream);
                throw th;
            }
        }
        return this.f;
    }
}
