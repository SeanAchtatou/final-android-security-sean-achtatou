package com.flurry.android;

import java.io.DataInput;

final class aj extends b {
    String a;
    byte b;
    byte c;
    ah d;

    aj() {
    }

    aj(DataInput dataInput) {
        this.a = dataInput.readUTF();
        this.b = dataInput.readByte();
        this.c = dataInput.readByte();
    }

    public final String toString() {
        return "{name: " + this.a + ", blockId: " + ((int) this.b) + ", themeId: " + ((int) this.c);
    }
}
