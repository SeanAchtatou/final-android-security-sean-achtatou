package com.flurry.android;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.widget.ImageView;
import android.widget.LinearLayout;

final class z extends LinearLayout {
    public z(CatalogActivity catalogActivity, Context context) {
        super(context);
        setBackgroundColor(-1);
        m j = catalogActivity.e.j();
        if (j != null) {
            ImageView imageView = new ImageView(context);
            imageView.setId(10000);
            byte[] bArr = j.e;
            if (bArr != null) {
                imageView.setImageBitmap(BitmapFactory.decodeByteArray(bArr, 0, bArr.length));
            }
            y.a(context, imageView, y.a(context, j.b), y.a(context, j.c));
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
            layoutParams.setMargins(0, 0, 0, -3);
            setGravity(3);
            addView(imageView, layoutParams);
        }
    }
}
