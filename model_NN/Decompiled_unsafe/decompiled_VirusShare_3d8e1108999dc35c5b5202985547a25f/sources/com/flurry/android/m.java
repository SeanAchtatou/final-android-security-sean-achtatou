package com.flurry.android;

import java.io.DataInput;

public final class m extends b {
    long a;
    int b;
    int c;
    String d;
    byte[] e;

    m() {
    }

    m(DataInput dataInput) {
        a(dataInput);
    }

    /* access modifiers changed from: package-private */
    public final void a(DataInput dataInput) {
        this.a = dataInput.readLong();
        this.b = dataInput.readInt();
        this.c = dataInput.readInt();
        this.d = dataInput.readUTF();
        this.e = new byte[dataInput.readInt()];
        dataInput.readFully(this.e);
    }
}
