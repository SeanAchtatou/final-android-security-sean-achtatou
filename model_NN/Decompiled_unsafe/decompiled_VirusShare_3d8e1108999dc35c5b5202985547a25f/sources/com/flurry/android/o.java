package com.flurry.android;

import java.util.LinkedHashMap;
import java.util.Map;

final class o extends LinkedHashMap {
    private /* synthetic */ e a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    o(e eVar, int i) {
        super(i, 0.75f, true);
        this.a = eVar;
    }

    /* access modifiers changed from: protected */
    public final boolean removeEldestEntry(Map.Entry entry) {
        return size() > this.a.b;
    }
}
