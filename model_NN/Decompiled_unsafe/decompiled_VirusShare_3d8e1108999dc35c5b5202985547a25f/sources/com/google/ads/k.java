package com.google.ads;

import android.webkit.WebView;
import com.google.ads.util.b;
import java.util.HashMap;

public final class k implements o {
    public final void a(x xVar, HashMap<String, String> hashMap, WebView webView) {
        String str = hashMap.get("js");
        if (str == null) {
            b.b("Could not get the JS to evaluate.");
        }
        if (webView instanceof q) {
            AdActivity a = ((q) webView).a();
            if (a == null) {
                b.b("Could not get the AdActivity from the AdWebView.");
                return;
            }
            q b = a.b();
            if (b == null) {
                b.b("Could not get the opening WebView.");
            } else {
                v.a(b, str);
            }
        } else {
            b.b("Trying to evaluate JS in a WebView that isn't an AdWebView");
        }
    }
}
