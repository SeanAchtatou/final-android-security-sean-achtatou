package com.google.ads;

import android.webkit.WebView;
import com.google.ads.c;
import com.google.ads.util.b;
import java.util.HashMap;

public final class i implements o {
    public final void a(x xVar, HashMap<String, String> hashMap, WebView webView) {
        b.e("Invalid " + hashMap.get("type") + " request error: " + hashMap.get("errors"));
        a f = xVar.f();
        if (f != null) {
            f.a(c.b.INVALID_REQUEST);
        }
    }
}
