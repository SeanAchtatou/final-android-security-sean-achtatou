package com.google.ads;

import android.media.MediaPlayer;
import android.os.Handler;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import android.widget.MediaController;
import android.widget.VideoView;
import com.google.ads.util.b;
import java.lang.ref.WeakReference;

public final class r extends FrameLayout implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener {
    private WeakReference<AdActivity> a;
    private q b;
    private MediaController c = null;
    private long d = 0;
    private VideoView e;
    private String f = null;

    private static class a implements Runnable {
        private WeakReference<r> a;
        private Handler b = new Handler();

        public a(r rVar) {
            this.a = new WeakReference<>(rVar);
        }

        public final void run() {
            r rVar = this.a.get();
            if (rVar == null) {
                b.d("The video must be gone, so cancelling the timeupdate task.");
                return;
            }
            rVar.e();
            this.b.postDelayed(this, 250);
        }

        public final void a() {
            this.b.postDelayed(this, 250);
        }
    }

    public r(AdActivity adActivity, q qVar) {
        super(adActivity);
        this.a = new WeakReference<>(adActivity);
        this.b = qVar;
        this.e = new VideoView(adActivity);
        addView(this.e, new FrameLayout.LayoutParams(-1, -1, 17));
        new a(this).a();
        this.e.setOnCompletionListener(this);
        this.e.setOnPreparedListener(this);
        this.e.setOnErrorListener(this);
    }

    public final void a() {
        if (!TextUtils.isEmpty(this.f)) {
            this.e.setVideoPath(this.f);
        } else {
            v.a(this.b, "onVideoEvent", "{'event': 'error', 'what': 'no_src'}");
        }
    }

    public final void a(boolean z) {
        AdActivity adActivity = this.a.get();
        if (adActivity == null) {
            b.e("adActivity was null while trying to enable controls on a video.");
        } else if (z) {
            if (this.c == null) {
                this.c = new MediaController(adActivity);
            }
            this.e.setMediaController(this.c);
        } else {
            if (this.c != null) {
                this.c.hide();
            }
            this.e.setMediaController(null);
        }
    }

    public final void a(String str) {
        this.f = str;
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        v.a(this.b, "onVideoEvent", "{'event': 'ended'}");
    }

    public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        b.e("Video threw error! <what:" + i + ", extra:" + i2 + ">");
        v.a(this.b, "onVideoEvent", "{'event': 'error', 'what': '" + i + "', 'extra': '" + i2 + "'}");
        return true;
    }

    public final void onPrepared(MediaPlayer mediaPlayer) {
        v.a(this.b, "onVideoEvent", "{'event': 'canplaythrough', 'duration': '" + (((float) this.e.getDuration()) / 1000.0f) + "'}");
    }

    public final void b() {
        this.e.pause();
    }

    public final void c() {
        this.e.start();
    }

    public final void a(int i) {
        this.e.seekTo(i);
    }

    public final void a(MotionEvent motionEvent) {
        this.e.onTouchEvent(motionEvent);
    }

    public final void d() {
        this.e.stopPlayback();
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        long currentPosition = (long) this.e.getCurrentPosition();
        if (this.d != currentPosition) {
            v.a(this.b, "onVideoEvent", "{'event': 'timeupdate', 'time': " + (((float) currentPosition) / 1000.0f) + "}");
            this.d = currentPosition;
        }
    }
}
