package com.google.ads;

import android.content.Context;
import android.location.Location;
import com.google.ads.util.AdUtil;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class c {
    public static final String a = AdUtil.a("emulator");
    private a b = null;
    private String c = null;
    private Set<String> d = null;
    private boolean e = false;
    private Map<String, Object> f = null;
    private Location g = null;
    private boolean h = false;
    private boolean i = false;
    private Set<String> j = null;

    public enum a {
        UNKNOWN,
        MALE,
        FEMALE
    }

    public enum b {
        INVALID_REQUEST("Invalid Google Ad request."),
        NO_FILL("Ad request successful, but no ad returned due to lack of ad inventory."),
        NETWORK_ERROR("A network error occurred."),
        INTERNAL_ERROR("There was an internal error.");
        
        private String e;

        private b(String str) {
            this.e = str;
        }

        public final String toString() {
            return this.e;
        }
    }

    public final void a(String str) {
        if (this.d == null) {
            this.d = new HashSet();
        }
        this.d.add(str);
    }

    public final void a(String str, Object obj) {
        if (this.f == null) {
            this.f = new HashMap();
        }
        this.f.put(str, obj);
    }

    public Map<String, Object> a(Context context) {
        String str;
        String a2;
        boolean z = false;
        HashMap hashMap = new HashMap();
        if (this.d != null) {
            hashMap.put("kw", this.d);
        }
        if (this.b != null) {
            hashMap.put("cust_gender", Integer.valueOf(this.b.ordinal()));
        }
        if (this.c != null) {
            hashMap.put("cust_age", this.c);
        }
        if (this.g != null) {
            hashMap.put("uule", AdUtil.a(this.g));
        }
        if (this.h) {
            hashMap.put("testing", 1);
        }
        if (this.e) {
            hashMap.put("pto", 1);
        } else {
            hashMap.put("cipa", Integer.valueOf(ab.a(context) ? 1 : 0));
        }
        if (!(this.j == null || (a2 = AdUtil.a(context)) == null || !this.j.contains(a2))) {
            z = true;
        }
        if (z) {
            hashMap.put("adtest", "on");
        } else if (!this.i) {
            if (AdUtil.c()) {
                str = "AdRequest.TEST_EMULATOR";
            } else {
                str = "\"" + AdUtil.a(context) + "\"";
            }
            com.google.ads.util.b.c("To get test ads on this device, call adRequest.addTestDevice(" + str + ");");
            this.i = true;
        }
        if (this.f != null) {
            hashMap.put("extras", this.f);
        }
        return hashMap;
    }

    public final void b(String str) {
        if (this.j == null) {
            this.j = new HashSet();
        }
        this.j.add(str);
    }
}
