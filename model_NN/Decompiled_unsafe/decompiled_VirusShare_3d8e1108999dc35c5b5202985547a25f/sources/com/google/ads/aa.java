package com.google.ads;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.webkit.WebView;
import com.google.ads.b;
import java.util.HashMap;

public final class aa implements o {

    public enum b {
        AD("ad"),
        APP("app");
        
        public String b;

        private b(String str) {
            this.b = str;
        }
    }

    private static class c implements DialogInterface.OnClickListener {
        private x a;

        public c(x xVar) {
            this.a = xVar;
        }

        public final void onClick(DialogInterface dialogInterface, int i) {
            HashMap hashMap = new HashMap();
            hashMap.put("u", "market://details?id=com.google.android.apps.plus");
            AdActivity.a(this.a, new y("intent", hashMap));
        }
    }

    private static class a implements DialogInterface.OnClickListener {
        public final void onClick(DialogInterface dialogInterface, int i) {
        }
    }

    public final void a(x xVar, HashMap<String, String> hashMap, WebView webView) {
        String str = hashMap.get("a");
        if (str != null) {
            if (str.equals("resize")) {
                v.a(webView, "(G_resizeIframe(" + hashMap.get("u") + "))");
                return;
            } else if (str.equals("state")) {
                new Thread(new b.C0012b(xVar.d(), webView, hashMap.get("u"))).start();
                return;
            }
        }
        Intent intent = new Intent();
        intent.setComponent(new ComponentName("com.google.android.apps.plus", "com.google.android.apps.circles.platform.PlusOneActivity"));
        Activity d = xVar.d();
        if (d == null) {
            com.google.ads.util.b.e("Activity was null when responding to +1 action");
        } else if (ab.a(intent, d.getApplicationContext())) {
            AdActivity.a(xVar, new y("plusone", hashMap));
        } else if (!ab.a(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=com.google.android.apps.plus")), d.getApplicationContext())) {
        } else {
            if (TextUtils.isEmpty(hashMap.get("d")) || TextUtils.isEmpty(hashMap.get("o")) || TextUtils.isEmpty(hashMap.get("c"))) {
                HashMap hashMap2 = new HashMap();
                hashMap2.put("u", "market://details?id=com.google.android.apps.plus");
                AdActivity.a(xVar, new y("intent", hashMap2));
                return;
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(d);
            builder.setMessage(hashMap.get("d")).setPositiveButton(hashMap.get("o"), new c(xVar)).setNegativeButton(hashMap.get("c"), new a());
            builder.create().show();
        }
    }
}
