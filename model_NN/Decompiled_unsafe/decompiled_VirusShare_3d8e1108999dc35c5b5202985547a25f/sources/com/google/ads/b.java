package com.google.ads;

import android.app.Activity;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.webkit.WebView;
import java.lang.ref.WeakReference;
import java.util.Date;

public final class b {

    private static class a implements Runnable {
        private WeakReference<Activity> a;

        public a(Activity activity) {
            this.a = new WeakReference<>(activity);
        }

        public final void run() {
            String str;
            try {
                Activity activity = this.a.get();
                if (activity == null) {
                    com.google.ads.util.b.a("Activity was null while making a doritos cookie request.");
                    return;
                }
                Cursor query = activity.getContentResolver().query(s.b, s.d, null, null, null);
                if (query == null || !query.moveToFirst() || query.getColumnNames().length <= 0) {
                    com.google.ads.util.b.a("Google+ app not installed, not storing doritos cookie");
                    str = null;
                } else {
                    str = query.getString(query.getColumnIndex(query.getColumnName(0)));
                }
                SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(activity.getApplicationContext()).edit();
                if (!TextUtils.isEmpty(str)) {
                    edit.putString("drt", str);
                    edit.putLong("drt_ts", new Date().getTime());
                } else {
                    edit.putString("drt", "");
                    edit.putLong("drt_ts", 0);
                }
                edit.commit();
            } catch (Exception e) {
                com.google.ads.util.b.a("An unknown error occurred while sending a doritos request.", e);
            }
        }
    }

    /* renamed from: com.google.ads.b$b  reason: collision with other inner class name */
    private static class C0012b implements Runnable {
        private WeakReference<Activity> a;
        private WebView b;
        private String c;

        public C0012b(Activity activity, WebView webView, String str) {
            this.a = new WeakReference<>(activity);
            this.c = str;
            this.b = webView;
        }

        public final void run() {
            boolean z;
            try {
                Uri withAppendedPath = Uri.withAppendedPath(s.a, this.c);
                Activity activity = this.a.get();
                if (activity == null) {
                    com.google.ads.util.b.a("Activity was null while getting the +1 button state.");
                    return;
                }
                Cursor query = activity.getContentResolver().query(withAppendedPath, s.c, null, null, null);
                if (query == null || !query.moveToFirst()) {
                    com.google.ads.util.b.a("Google+ app not installed, showing ad as not +1'd");
                    z = false;
                } else {
                    z = query.getInt(query.getColumnIndex("has_plus1")) == 1;
                }
                this.b.post(new c(this.b, z));
            } catch (Exception e) {
                com.google.ads.util.b.a("An unknown error occurred while updating the +1 state.", e);
            }
        }
    }

    private static class c implements Runnable {
        private boolean a;
        private WebView b;

        public c(WebView webView, boolean z) {
            this.b = webView;
            this.a = z;
        }

        public final void run() {
            b.a(this.b, this.a);
        }
    }

    private b() {
    }

    public static void a(WebView webView, boolean z) {
        v.a(webView, "(G_updatePlusOne(" + z + "))");
    }
}
