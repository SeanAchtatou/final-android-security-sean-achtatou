package com.google.ads;

import com.google.ads.util.b;
import java.lang.ref.WeakReference;

public final class u implements Runnable {
    private WeakReference<x> a;

    public u(x xVar) {
        this.a = new WeakReference<>(xVar);
    }

    public final void run() {
        x xVar = this.a.get();
        if (xVar == null) {
            b.a("The ad must be gone, so cancelling the refresh timer.");
        } else {
            xVar.u();
        }
    }
}
