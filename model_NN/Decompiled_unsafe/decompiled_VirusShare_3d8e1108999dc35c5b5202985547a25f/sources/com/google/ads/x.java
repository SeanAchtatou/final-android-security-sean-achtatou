package com.google.ads;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import com.google.ads.b;
import com.google.ads.c;
import com.google.ads.util.AdUtil;
import com.google.ads.util.b;
import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;

public final class x {
    private static final Object a = new Object();
    private WeakReference<Activity> b;
    private af c;
    private ac d = null;
    private a e = null;
    private c f = null;
    private ae g;
    private z h = new z();
    private String i;
    private q j;
    private p k;
    private Handler l = new Handler();
    private long m;
    private boolean n = false;
    private boolean o = false;
    private SharedPreferences p;
    private long q = 0;
    private u r;
    private boolean s = false;
    private LinkedList<String> t;
    private LinkedList<String> u;
    private int v;

    public x(Activity activity, af afVar, ae aeVar, String str) {
        this.b = new WeakReference<>(activity);
        this.c = afVar;
        this.g = aeVar;
        this.i = str;
        synchronized (a) {
            this.p = activity.getApplicationContext().getSharedPreferences("GoogleAdMobAdsPrefs", 0);
            this.m = 60000;
        }
        this.r = new u(this);
        this.t = new LinkedList<>();
        this.u = new LinkedList<>();
        a();
        AdUtil.h(activity.getApplicationContext());
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str) {
        b.a("Adding a tracking URL: " + str);
        this.t.add(str);
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(LinkedList<String> linkedList) {
        Iterator<String> it = linkedList.iterator();
        while (it.hasNext()) {
            b.a("Adding a click tracking URL: " + it.next());
        }
        this.u = linkedList;
    }

    public final synchronized void a() {
        Activity d2 = d();
        if (d2 == null) {
            b.a("activity was null while trying to create an AdWebView.");
        } else {
            this.j = new q(d2.getApplicationContext(), this.g);
            this.j.setVisibility(8);
            if (this.c instanceof AdView) {
                this.k = new p(this, v.b, true, false);
            } else {
                this.k = new p(this, v.b, true, true);
            }
            this.j.setWebViewClient(this.k);
        }
    }

    public final synchronized void b() {
        if (this.o) {
            b.a("Disabling refreshing.");
            this.l.removeCallbacks(this.r);
            this.o = false;
        } else {
            b.a("Refreshing is already disabled.");
        }
    }

    public final synchronized void c() {
        if (!(this.c instanceof AdView)) {
            b.a("Tried to enable refreshing on something other than an AdView.");
        } else if (!this.o) {
            b.a("Enabling refreshing every " + this.q + " milliseconds.");
            this.l.postDelayed(this.r, this.q);
            this.o = true;
        } else {
            b.a("Refreshing is already enabled.");
        }
    }

    public final Activity d() {
        return this.b.get();
    }

    public final af e() {
        return this.c;
    }

    public final synchronized a f() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public final String g() {
        return this.i;
    }

    public final synchronized q h() {
        return this.j;
    }

    public final synchronized p i() {
        return this.k;
    }

    public final ae j() {
        return this.g;
    }

    public final z k() {
        return this.h;
    }

    public final synchronized void a(int i2) {
        this.v = i2;
    }

    public final synchronized int l() {
        return this.v;
    }

    public final long m() {
        return this.m;
    }

    private synchronized boolean v() {
        return this.e != null;
    }

    public final synchronized boolean n() {
        return this.o;
    }

    public final synchronized void a(c cVar) {
        boolean z = false;
        synchronized (this) {
            if (v()) {
                b.e("loadAd called while the ad is already loading, so aborting.");
            } else if (AdActivity.c()) {
                b.e("loadAd called while an interstitial or landing page is displayed, so aborting");
            } else {
                Activity d2 = d();
                if (d2 == null) {
                    b.e("activity is null while trying to load an ad.");
                } else if (AdUtil.c(d2.getApplicationContext()) && AdUtil.b(d2.getApplicationContext())) {
                    long j2 = this.p.getLong("GoogleAdMobDoritosLife", 60000);
                    SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(d2.getApplicationContext());
                    if (ab.a(d2) && (!defaultSharedPreferences.contains("drt") || !defaultSharedPreferences.contains("drt_ts") || defaultSharedPreferences.getLong("drt_ts", 0) < new Date().getTime() - j2)) {
                        z = true;
                    }
                    if (z) {
                        new Thread(new b.a(d2)).start();
                    }
                    this.n = false;
                    this.t.clear();
                    this.f = cVar;
                    this.e = new a(this);
                    this.e.a(cVar);
                }
            }
        }
    }

    public final synchronized void a(c.b bVar) {
        this.e = null;
        if (this.c instanceof ad) {
            if (bVar == c.b.NO_FILL) {
                this.h.n();
            } else if (bVar == c.b.NETWORK_ERROR) {
                this.h.l();
            }
        }
        com.google.ads.util.b.c("onFailedToReceiveAd(" + bVar + ")");
    }

    /* access modifiers changed from: package-private */
    public final synchronized void o() {
        this.e = null;
        this.n = true;
        this.j.setVisibility(0);
        this.h.c();
        if (this.c instanceof AdView) {
            w();
        }
        com.google.ads.util.b.c("onReceiveAd()");
    }

    public final synchronized void p() {
        this.h.o();
        com.google.ads.util.b.c("onDismissScreen()");
    }

    public final synchronized void q() {
        com.google.ads.util.b.c("onPresentScreen()");
    }

    public final synchronized void r() {
        com.google.ads.util.b.c("onLeaveApplication()");
    }

    public final void s() {
        this.h.b();
        x();
    }

    private synchronized void w() {
        Activity activity = this.b.get();
        if (activity == null) {
            com.google.ads.util.b.e("activity was null while trying to ping tracking URLs.");
        } else {
            Iterator<String> it = this.t.iterator();
            while (it.hasNext()) {
                new Thread(new t(it.next(), activity.getApplicationContext())).start();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final synchronized boolean t() {
        return !this.u.isEmpty();
    }

    private synchronized void x() {
        Activity activity = this.b.get();
        if (activity == null) {
            com.google.ads.util.b.e("activity was null while trying to ping click tracking URLs.");
        } else {
            Iterator<String> it = this.u.iterator();
            while (it.hasNext()) {
                new Thread(new t(it.next(), activity.getApplicationContext())).start();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(Runnable runnable) {
        this.l.post(runnable);
    }

    public final synchronized void u() {
        if (this.f == null) {
            com.google.ads.util.b.a("Tried to refresh before calling loadAd().");
        } else if (this.c instanceof AdView) {
            if (!((AdView) this.c).isShown() || !AdUtil.d()) {
                com.google.ads.util.b.a("Not refreshing because the ad is not visible.");
            } else {
                com.google.ads.util.b.c("Refreshing ad.");
                a(this.f);
            }
            this.l.postDelayed(this.r, this.q);
        } else {
            com.google.ads.util.b.a("Tried to refresh an ad that wasn't an AdView.");
        }
    }

    public final void a(long j2) {
        synchronized (a) {
            SharedPreferences.Editor edit = this.p.edit();
            edit.putLong("Timeout" + this.i, j2);
            edit.commit();
            if (this.s) {
                this.m = j2;
            }
        }
    }

    public final synchronized void a(float f2) {
        this.q = (long) (1000.0f * f2);
    }

    public final synchronized void b(long j2) {
        if (j2 > 0) {
            this.p.edit().putLong("GoogleAdMobDoritosLife", j2).commit();
        }
    }
}
