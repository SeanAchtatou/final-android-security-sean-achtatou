package com.google.ads;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.webkit.WebView;
import com.google.ads.c;
import com.google.ads.util.AdUtil;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;

public final class a implements Runnable {
    private String a = null;
    private String b = null;
    private w c;
    private x d;
    private c e;
    private WebView f;
    private String g = null;
    private LinkedList<String> h = new LinkedList<>();
    private volatile boolean i;
    private c.b j = null;
    private boolean k = false;
    private int l = -1;
    private Thread m;
    private boolean n;

    private class d extends Exception {
        public d(String str) {
            super(str);
        }
    }

    /* renamed from: com.google.ads.a$a  reason: collision with other inner class name */
    private class C0010a extends Exception {
        public C0010a(String str) {
            super(str);
        }
    }

    private class c implements Runnable {
        private final x a;
        private final WebView b;
        private final w c;
        private final c.b d;
        private final boolean e;

        public c(x xVar, WebView webView, w wVar, c.b bVar, boolean z) {
            this.a = xVar;
            this.b = webView;
            this.c = wVar;
            this.d = bVar;
            this.e = z;
        }

        public final void run() {
            if (this.b != null) {
                this.b.stopLoading();
                this.b.destroy();
            }
            if (this.c != null) {
                this.c.a();
            }
            if (this.e) {
                q h = this.a.h();
                h.stopLoading();
                h.setVisibility(8);
            }
            this.a.a(this.d);
        }
    }

    private class b implements Runnable {
        private final String a;
        private final String b;
        private final WebView c;

        public b(WebView webView, String str, String str2) {
            this.c = webView;
            this.a = str;
            this.b = str2;
        }

        public final void run() {
            if (this.b != null) {
                this.c.loadDataWithBaseURL(this.a, this.b, "text/html", "utf-8", null);
            } else {
                this.c.loadUrl(this.a);
            }
        }
    }

    private class e implements Runnable {
        private final x a;
        private final LinkedList<String> b;
        private final int c;

        public e(x xVar, LinkedList<String> linkedList, int i) {
            this.a = xVar;
            this.b = linkedList;
            this.c = i;
        }

        public final void run() {
            this.a.a(this.b);
            this.a.a(this.c);
            this.a.o();
        }
    }

    public a(x xVar) {
        this.d = xVar;
        Activity d2 = xVar.d();
        if (d2 != null) {
            this.f = new q(d2, null);
            this.f.setWebViewClient(new p(xVar, v.a, false, false));
            this.f.setVisibility(8);
            this.f.setWillNotDraw(true);
            this.c = new w(this, xVar);
            return;
        }
        this.f = null;
        this.c = null;
        com.google.ads.util.b.e("activity was null while trying to create an AdLoader.");
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str) {
        this.h.add(str);
    }

    /* access modifiers changed from: package-private */
    public final void a(c cVar) {
        this.e = cVar;
        this.i = false;
        this.m = new Thread(this);
        this.m.start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.ads.a.a(com.google.ads.c$b, boolean):void
     arg types: [com.google.ads.c$b, int]
     candidates:
      com.google.ads.a.a(com.google.ads.c, android.app.Activity):java.lang.String
      com.google.ads.a.a(java.lang.String, java.lang.String):void
      com.google.ads.a.a(com.google.ads.c$b, boolean):void */
    public final void run() {
        synchronized (this) {
            if (this.f == null || this.c == null) {
                com.google.ads.util.b.e("adRequestWebView was null while trying to load an ad.");
                a(c.b.INTERNAL_ERROR, false);
                return;
            }
            Activity d2 = this.d.d();
            if (d2 == null) {
                com.google.ads.util.b.e("activity was null while forming an ad request.");
                a(c.b.INTERNAL_ERROR, false);
                return;
            }
            long m2 = this.d.m();
            long elapsedRealtime = SystemClock.elapsedRealtime();
            Object obj = this.e.a(d2).get("extras");
            if (obj instanceof Map) {
                Map map = (Map) obj;
                Object obj2 = map.get("_adUrl");
                if (obj2 instanceof String) {
                    this.a = (String) obj2;
                }
                Object obj3 = map.get("_orientation");
                if (obj3 instanceof String) {
                    if (obj3.equals("p")) {
                        this.l = 1;
                    } else if (obj3.equals("l")) {
                        this.l = 0;
                    }
                }
            }
            if (this.a == null) {
                try {
                    this.d.a(new b(this.f, null, a(this.e, d2)));
                    long elapsedRealtime2 = m2 - (SystemClock.elapsedRealtime() - elapsedRealtime);
                    if (elapsedRealtime2 > 0) {
                        try {
                            wait(elapsedRealtime2);
                        } catch (InterruptedException e2) {
                            com.google.ads.util.b.a("AdLoader InterruptedException while getting the URL: " + e2);
                            return;
                        }
                    }
                    try {
                        if (!this.i) {
                            if (this.j != null) {
                                a(this.j, false);
                                return;
                            } else if (this.g == null) {
                                com.google.ads.util.b.c("AdLoader timed out after " + m2 + "ms while getting the URL.");
                                a(c.b.NETWORK_ERROR, false);
                                return;
                            } else {
                                this.c.a(this.n);
                                this.c.a(this.g);
                                long elapsedRealtime3 = m2 - (SystemClock.elapsedRealtime() - elapsedRealtime);
                                if (elapsedRealtime3 > 0) {
                                    try {
                                        wait(elapsedRealtime3);
                                    } catch (InterruptedException e3) {
                                        com.google.ads.util.b.a("AdLoader InterruptedException while getting the HTML: " + e3);
                                        return;
                                    }
                                }
                                if (!this.i) {
                                    if (this.j != null) {
                                        a(this.j, false);
                                        return;
                                    } else if (this.b == null) {
                                        com.google.ads.util.b.c("AdLoader timed out after " + m2 + "ms while getting the HTML.");
                                        a(c.b.NETWORK_ERROR, false);
                                        return;
                                    }
                                } else {
                                    return;
                                }
                            }
                        } else {
                            return;
                        }
                    } catch (Exception e4) {
                        com.google.ads.util.b.a("An unknown error occurred in AdLoader.", e4);
                        a(c.b.INTERNAL_ERROR, true);
                    }
                } catch (d e5) {
                    com.google.ads.util.b.c("Unable to connect to network: " + e5);
                    a(c.b.NETWORK_ERROR, false);
                    return;
                } catch (C0010a e6) {
                    com.google.ads.util.b.c("Caught internal exception: " + e6);
                    a(c.b.INTERNAL_ERROR, false);
                    return;
                }
            }
            q h2 = this.d.h();
            this.d.i().b();
            this.d.a(new b(h2, this.a, this.b));
            long elapsedRealtime4 = m2 - (SystemClock.elapsedRealtime() - elapsedRealtime);
            if (elapsedRealtime4 > 0) {
                try {
                    wait(elapsedRealtime4);
                } catch (InterruptedException e7) {
                    com.google.ads.util.b.a("AdLoader InterruptedException while loading the HTML: " + e7);
                    return;
                }
            }
            if (this.k) {
                this.d.a(new e(this.d, this.h, this.l));
            } else {
                com.google.ads.util.b.c("AdLoader timed out after " + m2 + "ms while loading the HTML.");
                a(c.b.NETWORK_ERROR, true);
            }
        }
    }

    private String a(c cVar, Activity activity) throws C0010a, d {
        Context applicationContext = activity.getApplicationContext();
        Map<String, Object> a2 = cVar.a(applicationContext);
        z k2 = this.d.k();
        long h2 = k2.h();
        if (h2 > 0) {
            a2.put("prl", Long.valueOf(h2));
        }
        String g2 = k2.g();
        if (g2 != null) {
            a2.put("ppcl", g2);
        }
        String f2 = k2.f();
        if (f2 != null) {
            a2.put("pcl", f2);
        }
        long e2 = k2.e();
        if (e2 > 0) {
            a2.put("pcc", Long.valueOf(e2));
        }
        a2.put("preqs", Long.valueOf(z.i()));
        String j2 = k2.j();
        if (j2 != null) {
            a2.put("pai", j2);
        }
        if (k2.k()) {
            a2.put("aoi_timeout", "true");
        }
        if (k2.m()) {
            a2.put("aoi_nofill", "true");
        }
        String p = k2.p();
        if (p != null) {
            a2.put("pit", p);
        }
        k2.a();
        k2.d();
        if (this.d.e() instanceof ad) {
            a2.put("format", "interstitial_mb");
        } else {
            ae j3 = this.d.j();
            String aeVar = j3.toString();
            if (aeVar != null) {
                a2.put("format", aeVar);
            } else {
                HashMap hashMap = new HashMap();
                hashMap.put("w", Integer.valueOf(j3.a()));
                hashMap.put("h", Integer.valueOf(j3.b()));
                a2.put("ad_frame", hashMap);
            }
        }
        a2.put("slotname", this.d.g());
        a2.put("js", "afma-sdk-a-v4.3.1");
        try {
            int i2 = applicationContext.getPackageManager().getPackageInfo(applicationContext.getPackageName(), 0).versionCode;
            String f3 = AdUtil.f(applicationContext);
            if (!TextUtils.isEmpty(f3)) {
                a2.put("mv", f3);
            }
            a2.put("msid", applicationContext.getPackageName());
            a2.put("app_name", i2 + ".android." + applicationContext.getPackageName());
            a2.put("isu", AdUtil.a(applicationContext));
            String d2 = AdUtil.d(applicationContext);
            if (d2 == null) {
                throw new d("NETWORK_ERROR");
            }
            a2.put("net", d2);
            String e3 = AdUtil.e(applicationContext);
            if (!(e3 == null || e3.length() == 0)) {
                a2.put("cap", e3);
            }
            a2.put("u_audio", Integer.valueOf(AdUtil.g(applicationContext).ordinal()));
            DisplayMetrics a3 = AdUtil.a(activity);
            a2.put("u_sd", Float.valueOf(a3.density));
            a2.put("u_h", Integer.valueOf(AdUtil.a(applicationContext, a3)));
            a2.put("u_w", Integer.valueOf(AdUtil.b(applicationContext, a3)));
            a2.put("hl", Locale.getDefault().getLanguage());
            if (AdUtil.c()) {
                a2.put("simulator", 1);
            }
            String str = (this.e instanceof com.google.ads.a.a ? "<html><head><script src=\"http://www.gstatic.com/safa/sdk-core-v40.js\"></script><script>" : "<html><head><script src=\"http://media.admob.com/sdk-core-v40.js\"></script><script>") + "AFMA_buildAdURL" + "(" + AdUtil.a(a2) + ");" + "</script></head><body></body></html>";
            com.google.ads.util.b.c("adRequestUrlHtml: " + str);
            return str;
        } catch (PackageManager.NameNotFoundException e4) {
            throw new C0010a("NameNotFoundException");
        }
    }

    private void a(c.b bVar, boolean z) {
        this.c.a();
        this.d.a(new c(this.d, this.f, this.c, bVar, z));
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a(String str, String str2) {
        this.a = str2;
        this.b = str;
        notify();
    }

    public final synchronized void b(String str) {
        this.g = str;
        notify();
    }

    public final synchronized void a(c.b bVar) {
        this.j = bVar;
        notify();
    }

    /* access modifiers changed from: package-private */
    public final synchronized void a() {
        this.k = true;
        notify();
    }

    public final synchronized void a(int i2) {
        this.l = i2;
    }

    public final void a(boolean z) {
        this.n = z;
    }
}
