package com.google.ads;

import android.webkit.WebView;
import com.google.ads.util.b;
import java.util.HashMap;

public final class h implements o {
    public final void a(x xVar, HashMap<String, String> hashMap, WebView webView) {
        String str = hashMap.get("url");
        boolean equals = "1".equals(hashMap.get("drt_include"));
        b.c("Received ad url: <\"url\": \"" + str + "\", \"afmaNotifyDt\": \"" + hashMap.get("afma_notify_dt") + "\">");
        a f = xVar.f();
        if (f != null) {
            f.a(equals);
            f.b(str);
        }
    }
}
