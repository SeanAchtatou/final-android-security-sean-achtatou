package com.google.ads;

import android.webkit.WebView;
import com.google.ads.util.b;
import java.util.HashMap;

public final class l implements o {
    public final void a(x xVar, HashMap<String, String> hashMap, WebView webView) {
        if (webView instanceof q) {
            AdActivity a = ((q) webView).a();
            if (a != null) {
                a.finish();
                return;
            }
            return;
        }
        b.b("Trying to close WebView that isn't an AdWebView");
    }
}
