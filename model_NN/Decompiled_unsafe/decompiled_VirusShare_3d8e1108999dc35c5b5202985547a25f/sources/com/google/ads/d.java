package com.google.ads;

import android.app.Activity;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.webkit.WebView;
import com.google.ads.util.AdUtil;
import com.google.ads.util.b;
import java.util.HashMap;

public final class d implements o {
    private static int a(HashMap<String, String> hashMap, String str, int i, DisplayMetrics displayMetrics) {
        String str2 = hashMap.get(str);
        if (str2 == null) {
            return i;
        }
        try {
            return (int) TypedValue.applyDimension(1, (float) Integer.parseInt(str2), displayMetrics);
        } catch (NumberFormatException e) {
            b.a("Could not parse \"" + str + "\" in a video gmsg: " + str2);
            return i;
        }
    }

    public final void a(x xVar, HashMap<String, String> hashMap, WebView webView) {
        String str = hashMap.get("action");
        if (str == null) {
            b.a("No \"action\" parameter in a video gmsg.");
        } else if (webView instanceof q) {
            q qVar = (q) webView;
            AdActivity a = qVar.a();
            if (a == null) {
                b.a("Could not get adActivity for a video gmsg.");
                return;
            }
            boolean equals = str.equals("new");
            boolean equals2 = str.equals("position");
            if (equals || equals2) {
                DisplayMetrics a2 = AdUtil.a((Activity) a);
                int a3 = a(hashMap, "x", 0, a2);
                int a4 = a(hashMap, "y", 0, a2);
                int a5 = a(hashMap, "w", -1, a2);
                int a6 = a(hashMap, "h", -1, a2);
                if (!equals || a.a() != null) {
                    a.a(a3, a4, a5, a6);
                } else {
                    a.b(a3, a4, a5, a6);
                }
            } else {
                r a7 = a.a();
                if (a7 == null) {
                    v.a(qVar, "onVideoEvent", "{'event': 'error', 'what': 'no_video_view'}");
                } else if (str.equals("click")) {
                    DisplayMetrics a8 = AdUtil.a((Activity) a);
                    int a9 = a(hashMap, "x", 0, a8);
                    int a10 = a(hashMap, "y", 0, a8);
                    long uptimeMillis = SystemClock.uptimeMillis();
                    a7.a(MotionEvent.obtain(uptimeMillis, uptimeMillis, 0, (float) a9, (float) a10, 0));
                } else if (str.equals("controls")) {
                    String str2 = hashMap.get("enabled");
                    if (str2 == null) {
                        b.a("No \"enabled\" parameter in a controls video gmsg.");
                    } else if (str2.equals("true")) {
                        a7.a(true);
                    } else {
                        a7.a(false);
                    }
                } else if (str.equals("currentTime")) {
                    String str3 = hashMap.get("time");
                    if (str3 == null) {
                        b.a("No \"time\" parameter in a currentTime video gmsg.");
                        return;
                    }
                    try {
                        a7.a((int) (Float.parseFloat(str3) * 1000.0f));
                    } catch (NumberFormatException e) {
                        b.a("Could not parse \"time\" parameter: " + str3);
                    }
                } else if (str.equals("hide")) {
                    a7.setVisibility(4);
                } else if (str.equals("load")) {
                    a7.a();
                } else if (str.equals("pause")) {
                    a7.b();
                } else if (str.equals("play")) {
                    a7.c();
                } else if (str.equals("show")) {
                    a7.setVisibility(0);
                } else if (str.equals("src")) {
                    a7.a(hashMap.get("src"));
                } else {
                    b.a("Unknown video action: " + str);
                }
            }
        } else {
            b.a("Could not get adWebView for a video gmsg.");
        }
    }
}
