package com.google.ads;

import android.net.Uri;
import android.webkit.WebView;
import com.google.ads.util.b;
import java.util.HashMap;
import java.util.Locale;

public final class m implements o {
    public final void a(x xVar, HashMap<String, String> hashMap, WebView webView) {
        Uri parse;
        String host;
        String str = hashMap.get("u");
        if (str == null) {
            b.e("Could not get URL from click gmsg.");
            return;
        }
        z k = xVar.k();
        if (!(k == null || (host = (parse = Uri.parse(str)).getHost()) == null || !host.toLowerCase(Locale.US).endsWith(".admob.com"))) {
            String str2 = null;
            String path = parse.getPath();
            if (path != null) {
                String[] split = path.split("/");
                if (split.length >= 4) {
                    str2 = split[2] + "/" + split[3];
                }
            }
            k.b(str2);
        }
        new Thread(new t(str, webView.getContext().getApplicationContext())).start();
    }
}
