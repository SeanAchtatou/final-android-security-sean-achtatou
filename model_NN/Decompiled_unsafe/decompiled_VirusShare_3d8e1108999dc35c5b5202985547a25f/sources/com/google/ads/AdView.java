package com.google.ads;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.google.ads.util.AdUtil;
import com.google.ads.util.b;

public class AdView extends RelativeLayout implements af {
    private x a;

    public AdView(Activity activity, ae aeVar, String str) {
        super(activity.getApplicationContext());
        a(activity, aeVar, (AttributeSet) null);
        b(activity, aeVar, null);
        a(activity, aeVar, str);
    }

    public AdView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a(context, attributeSet);
    }

    public AdView(Context context, AttributeSet attributeSet, int i) {
        this(context, attributeSet);
    }

    private void a(Context context, String str, int i, ae aeVar, AttributeSet attributeSet) {
        if (getChildCount() == 0) {
            TextView textView = attributeSet == null ? new TextView(context) : new TextView(context, attributeSet);
            textView.setGravity(17);
            textView.setText(str);
            textView.setTextColor(i);
            textView.setBackgroundColor(-16777216);
            LinearLayout linearLayout = attributeSet == null ? new LinearLayout(context) : new LinearLayout(context, attributeSet);
            linearLayout.setGravity(17);
            LinearLayout linearLayout2 = attributeSet == null ? new LinearLayout(context) : new LinearLayout(context, attributeSet);
            linearLayout2.setGravity(17);
            linearLayout2.setBackgroundColor(i);
            int applyDimension = (int) TypedValue.applyDimension(1, (float) aeVar.a(), context.getResources().getDisplayMetrics());
            int applyDimension2 = (int) TypedValue.applyDimension(1, (float) aeVar.b(), context.getResources().getDisplayMetrics());
            linearLayout.addView(textView, applyDimension - 2, applyDimension2 - 2);
            linearLayout2.addView(linearLayout);
            addView(linearLayout2, applyDimension, applyDimension2);
        }
    }

    private boolean a(Context context, ae aeVar, AttributeSet attributeSet) {
        if (AdUtil.c(context)) {
            return true;
        }
        a(context, "You must have AdActivity declared in AndroidManifest.xml with configChanges.", aeVar, attributeSet);
        return false;
    }

    private boolean b(Context context, ae aeVar, AttributeSet attributeSet) {
        if (AdUtil.b(context)) {
            return true;
        }
        a(context, "You must have INTERNET and ACCESS_NETWORK_STATE permissions in AndroidManifest.xml.", aeVar, attributeSet);
        return false;
    }

    private void a(Context context, String str, ae aeVar, AttributeSet attributeSet) {
        b.b(str);
        a(context, str, -65536, aeVar, attributeSet);
        if (isInEditMode()) {
            return;
        }
        if (context instanceof Activity) {
            a((Activity) context, aeVar, "");
        } else {
            b.b("AdView was initialized with a Context that wasn't an Activity.");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
     arg types: [java.lang.String, android.util.TypedValue, int]
     candidates:
      ClspMth{android.content.res.Resources.getValue(int, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException}
      ClspMth{android.content.res.Resources.getValue(java.lang.String, android.util.TypedValue, boolean):void throws android.content.res.Resources$NotFoundException} */
    private void a(Context context, AttributeSet attributeSet) {
        ae aeVar;
        if (attributeSet != null) {
            String attributeValue = attributeSet.getAttributeValue("http://schemas.android.com/apk/lib/com.google.ads", "adSize");
            if (attributeValue == null) {
                a(context, "AdView missing required XML attribute \"adSize\".", ae.a, attributeSet);
                return;
            }
            if ("BANNER".equals(attributeValue)) {
                aeVar = ae.a;
            } else if ("IAB_MRECT".equals(attributeValue)) {
                aeVar = ae.b;
            } else if ("IAB_BANNER".equals(attributeValue)) {
                aeVar = ae.c;
            } else if ("IAB_LEADERBOARD".equals(attributeValue)) {
                aeVar = ae.d;
            } else {
                a(context, "Invalid \"adSize\" value in XML layout: " + attributeValue + ".", ae.a, attributeSet);
                return;
            }
            String attributeValue2 = attributeSet.getAttributeValue("http://schemas.android.com/apk/lib/com.google.ads", "testDevices");
            if (attributeValue2 != null && attributeValue2.startsWith("@string/")) {
                String substring = attributeValue2.substring("@string/".length());
                String packageName = context.getPackageName();
                TypedValue typedValue = new TypedValue();
                try {
                    getResources().getValue(packageName + ":string/" + substring, typedValue, true);
                    if (typedValue.string != null) {
                        attributeValue2 = typedValue.string.toString();
                    } else {
                        a(context, "\"testDevices\" was not a string: \"" + attributeValue2 + "\".", aeVar, attributeSet);
                        return;
                    }
                } catch (Resources.NotFoundException e) {
                    a(context, "Could not find resource for \"testDevices\": \"" + attributeValue2 + "\".", aeVar, attributeSet);
                    return;
                }
            }
            String attributeValue3 = attributeSet.getAttributeValue("http://schemas.android.com/apk/lib/com.google.ads", "adUnitId");
            if (attributeValue3 == null) {
                a(context, "AdView missing required XML attribute \"adUnitId\".", aeVar, attributeSet);
            } else if (isInEditMode()) {
                a(context, "Ads by Google", -1, aeVar, attributeSet);
            } else {
                if (attributeValue3.startsWith("@string/")) {
                    String substring2 = attributeValue3.substring("@string/".length());
                    String packageName2 = context.getPackageName();
                    TypedValue typedValue2 = new TypedValue();
                    try {
                        getResources().getValue(packageName2 + ":string/" + substring2, typedValue2, true);
                        if (typedValue2.string != null) {
                            attributeValue3 = typedValue2.string.toString();
                        } else {
                            a(context, "\"adUnitId\" was not a string: \"" + attributeValue3 + "\".", aeVar, attributeSet);
                            return;
                        }
                    } catch (Resources.NotFoundException e2) {
                        a(context, "Could not find resource for \"adUnitId\": \"" + attributeValue3 + "\".", aeVar, attributeSet);
                        return;
                    }
                }
                boolean attributeBooleanValue = attributeSet.getAttributeBooleanValue("http://schemas.android.com/apk/lib/com.google.ads", "loadAdOnCreate", false);
                if (context instanceof Activity) {
                    Activity activity = (Activity) context;
                    a(activity, aeVar, attributeSet);
                    b(activity, aeVar, attributeSet);
                    a(activity, aeVar, attributeValue3);
                    if (attributeBooleanValue) {
                        c cVar = new c();
                        if (attributeValue2 != null) {
                            for (String trim : attributeValue2.split(",")) {
                                String trim2 = trim.trim();
                                if (trim2.length() != 0) {
                                    if (trim2.equals("TEST_EMULATOR")) {
                                        cVar.b(c.a);
                                    } else {
                                        cVar.b(trim2);
                                    }
                                }
                            }
                        }
                        String attributeValue4 = attributeSet.getAttributeValue("http://schemas.android.com/apk/lib/com.google.ads", "keywords");
                        if (attributeValue4 != null) {
                            for (String trim3 : attributeValue4.split(",")) {
                                String trim4 = trim3.trim();
                                if (trim4.length() != 0) {
                                    cVar.a(trim4);
                                }
                            }
                        }
                        a(cVar);
                        return;
                    }
                    return;
                }
                b.b("AdView was initialized with a Context that wasn't an Activity.");
            }
        }
    }

    private void a(Activity activity, ae aeVar, String str) {
        this.a = new x(activity, this, aeVar, str);
        setGravity(17);
        setLayoutParams(new RelativeLayout.LayoutParams(-2, -2));
        addView(this.a.h(), (int) TypedValue.applyDimension(1, (float) aeVar.a(), activity.getResources().getDisplayMetrics()), (int) TypedValue.applyDimension(1, (float) aeVar.b(), activity.getResources().getDisplayMetrics()));
    }

    public final void a(c cVar) {
        if (this.a.n()) {
            this.a.b();
        }
        this.a.a(cVar);
    }
}
