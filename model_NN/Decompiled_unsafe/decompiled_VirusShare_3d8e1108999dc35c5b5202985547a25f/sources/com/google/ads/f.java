package com.google.ads;

import android.webkit.WebView;
import com.google.ads.util.b;
import java.util.HashMap;

public final class f implements o {
    public final void a(x xVar, HashMap<String, String> hashMap, WebView webView) {
        String str = hashMap.get("a");
        if (str == null) {
            b.a("Could not get the action parameter for open GMSG.");
        } else if (str.equals("webapp")) {
            AdActivity.a(xVar, new y("webapp", hashMap));
        } else {
            AdActivity.a(xVar, new y("intent", hashMap));
        }
    }
}
