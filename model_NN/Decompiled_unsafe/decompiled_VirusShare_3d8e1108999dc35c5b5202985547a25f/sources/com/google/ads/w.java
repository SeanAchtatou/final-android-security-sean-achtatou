package com.google.ads;

import android.app.Activity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import com.google.ads.c;
import com.google.ads.util.AdUtil;
import com.google.ads.util.b;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.StringTokenizer;

public final class w implements Runnable {
    private a a;
    private x b;
    private volatile boolean c;
    private boolean d;
    private String e;

    w(a aVar, x xVar) {
        this.a = aVar;
        this.b = xVar;
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.c = true;
    }

    private void a(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("X-Afma-Tracking-Urls");
        if (headerField != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(headerField);
            while (stringTokenizer.hasMoreTokens()) {
                this.b.a(stringTokenizer.nextToken());
            }
        }
        b(httpURLConnection);
        String headerField2 = httpURLConnection.getHeaderField("X-Afma-Refresh-Rate");
        if (headerField2 != null) {
            try {
                float parseFloat = Float.parseFloat(headerField2);
                if (parseFloat > 0.0f) {
                    this.b.a(parseFloat);
                    if (!this.b.n()) {
                        this.b.c();
                    }
                } else if (this.b.n()) {
                    this.b.b();
                }
            } catch (NumberFormatException e2) {
                b.b("Could not get refresh value: " + headerField2, e2);
            }
        }
        String headerField3 = httpURLConnection.getHeaderField("X-Afma-Interstitial-Timeout");
        if (headerField3 != null) {
            try {
                this.b.a((long) (Float.parseFloat(headerField3) * 1000.0f));
            } catch (NumberFormatException e3) {
                b.b("Could not get timeout value: " + headerField3, e3);
            }
        }
        String headerField4 = httpURLConnection.getHeaderField("X-Afma-Orientation");
        if (headerField4 != null) {
            if (headerField4.equals("portrait")) {
                this.a.a(AdUtil.b());
            } else if (headerField4.equals("landscape")) {
                this.a.a(AdUtil.a());
            }
        }
        if (!TextUtils.isEmpty(httpURLConnection.getHeaderField("X-Afma-Doritos-Cache-Life"))) {
            try {
                this.b.b(Long.parseLong(httpURLConnection.getHeaderField("X-Afma-Doritos-Cache-Life")));
            } catch (NumberFormatException e4) {
                b.e("Got bad value of Doritos cookie cache life from header: " + httpURLConnection.getHeaderField("X-Afma-Doritos-Cache-Life") + ". Using default value instead.");
            }
        }
    }

    private void b(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("X-Afma-Click-Tracking-Urls");
        if (headerField != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(headerField);
            while (stringTokenizer.hasMoreTokens()) {
                this.a.a(stringTokenizer.nextToken());
            }
        }
    }

    public final void a(boolean z) {
        this.d = z;
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        this.e = str;
        this.c = false;
        new Thread(this).start();
    }

    public final void run() {
        HttpURLConnection httpURLConnection;
        String readLine;
        while (!this.c) {
            try {
                httpURLConnection = (HttpURLConnection) new URL(this.e).openConnection();
                Activity d2 = this.b.d();
                if (d2 == null) {
                    b.c("activity was null in AdHtmlLoader.");
                    this.a.a(c.b.INTERNAL_ERROR);
                    httpURLConnection.disconnect();
                    return;
                }
                SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(d2);
                if (this.d && !TextUtils.isEmpty(defaultSharedPreferences.getString("drt", ""))) {
                    if (AdUtil.a == 8) {
                        httpURLConnection.addRequestProperty("X-Afma-drt-Cookie", defaultSharedPreferences.getString("drt", ""));
                    } else {
                        httpURLConnection.addRequestProperty("Cookie", defaultSharedPreferences.getString("drt", ""));
                    }
                }
                AdUtil.a(httpURLConnection, d2.getApplicationContext());
                httpURLConnection.setInstanceFollowRedirects(false);
                httpURLConnection.connect();
                int responseCode = httpURLConnection.getResponseCode();
                if (300 <= responseCode && responseCode < 400) {
                    String headerField = httpURLConnection.getHeaderField("Location");
                    if (headerField == null) {
                        b.c("Could not get redirect location from a " + responseCode + " redirect.");
                        this.a.a(c.b.INTERNAL_ERROR);
                        httpURLConnection.disconnect();
                        return;
                    }
                    a(httpURLConnection);
                    this.e = headerField;
                    httpURLConnection.disconnect();
                } else if (responseCode == 200) {
                    a(httpURLConnection);
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()), 4096);
                    StringBuilder sb = new StringBuilder();
                    while (!this.c && (readLine = bufferedReader.readLine()) != null) {
                        sb.append(readLine);
                        sb.append("\n");
                    }
                    String sb2 = sb.toString();
                    b.a("Response content is: " + sb2);
                    if (sb2 == null || sb2.trim().length() <= 0) {
                        b.a("Response message is null or zero length: " + sb2);
                        this.a.a(c.b.NO_FILL);
                        httpURLConnection.disconnect();
                        return;
                    }
                    this.a.a(sb2, this.e);
                    httpURLConnection.disconnect();
                    return;
                } else if (responseCode == 400) {
                    b.c("Bad request");
                    this.a.a(c.b.INVALID_REQUEST);
                    httpURLConnection.disconnect();
                    return;
                } else {
                    b.c("Invalid response code: " + responseCode);
                    this.a.a(c.b.INTERNAL_ERROR);
                    httpURLConnection.disconnect();
                    return;
                }
            } catch (MalformedURLException e2) {
                b.a("Received malformed ad url from javascript.", e2);
                this.a.a(c.b.INTERNAL_ERROR);
                return;
            } catch (IOException e3) {
                b.b("IOException connecting to ad url.", e3);
                this.a.a(c.b.NETWORK_ERROR);
                return;
            } catch (Exception e4) {
                b.a("An unknown error occurred in AdHtmlLoader.", e4);
                this.a.a(c.b.INTERNAL_ERROR);
                return;
            } catch (Throwable th) {
                httpURLConnection.disconnect();
                throw th;
            }
        }
    }
}
