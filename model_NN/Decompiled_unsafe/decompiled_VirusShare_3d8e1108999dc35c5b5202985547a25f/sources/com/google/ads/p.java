package com.google.ads;

import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.google.ads.util.AdUtil;
import com.google.ads.util.b;
import java.util.HashMap;
import java.util.Map;

public final class p extends WebViewClient {
    private x a;
    private Map<String, o> b;
    private boolean c;
    private boolean d;
    private boolean e = false;
    private boolean f = false;

    public p(x xVar, Map<String, o> map, boolean z, boolean z2) {
        this.a = xVar;
        this.b = map;
        this.c = z;
        this.d = z2;
    }

    public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
        String str2;
        b.a("shouldOverrideUrlLoading(\"" + str + "\")");
        Uri parse = Uri.parse(str);
        HashMap<String, String> b2 = AdUtil.b(parse);
        if (b2 == null) {
            b.e("An error occurred while parsing the url parameters.");
            return true;
        }
        String str3 = b2.get("ai");
        if (str3 != null) {
            this.a.k().a(str3);
        }
        if (v.a(parse)) {
            v.a(this.a, this.b, parse, webView);
            return true;
        } else if (this.d) {
            if (AdUtil.a(parse)) {
                return super.shouldOverrideUrlLoading(webView, str);
            }
            HashMap hashMap = new HashMap();
            hashMap.put("u", str);
            AdActivity.a(this.a, new y("intent", hashMap));
            return true;
        } else if (this.c) {
            if (!this.a.t() || !AdUtil.a(parse)) {
                str2 = "intent";
            } else {
                str2 = "webapp";
            }
            HashMap hashMap2 = new HashMap();
            hashMap2.put("u", parse.toString());
            AdActivity.a(this.a, new y(str2, hashMap2));
            return true;
        } else {
            b.e("URL is not a GMSG and can't handle URL: " + str);
            return true;
        }
    }

    public final void onPageFinished(WebView webView, String str) {
        if (this.e) {
            a f2 = this.a.f();
            if (f2 != null) {
                f2.a();
            } else {
                b.a("adLoader was null while trying to setFinishedLoadingHtml().");
            }
            this.e = false;
        }
        if (this.f) {
            v.a(webView);
            this.f = false;
        }
    }

    public final void a() {
        this.d = false;
    }

    public final void b() {
        this.e = true;
    }

    public final void c() {
        this.f = true;
    }
}
