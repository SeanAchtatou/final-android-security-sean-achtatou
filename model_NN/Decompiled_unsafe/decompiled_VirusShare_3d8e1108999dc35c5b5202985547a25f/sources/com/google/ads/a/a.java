package com.google.ads.a;

import android.content.Context;
import android.graphics.Color;
import android.util.Pair;
import com.google.ads.c;
import java.util.Map;

public final class a extends c {
    private String b;
    private int c;
    private Pair<Integer, Integer> d;
    private int e;
    private int f;
    private int g;
    private String h;
    private int i;
    private int j;
    private C0011a k;
    private int l;

    /* renamed from: com.google.ads.a.a$a  reason: collision with other inner class name */
    public enum C0011a {
        NONE("none"),
        DASHED("dashed"),
        DOTTED("dotted"),
        SOLID("solid");
        
        private String e;

        private C0011a(String str) {
            this.e = str;
        }

        public final String toString() {
            return this.e;
        }
    }

    public final Map<String, Object> a(Context context) {
        if (this.b != null) {
            a("q", this.b);
        }
        if (Color.alpha(this.c) != 0) {
            a("bgcolor", a(this.c));
        }
        if (!(this.d == null || this.d.first == null || this.d.second == null)) {
            a("gradientfrom", a(((Integer) this.d.first).intValue()));
            a("gradientto", a(((Integer) this.d.second).intValue()));
        }
        if (Color.alpha(this.e) != 0) {
            a("hcolor", a(this.e));
        }
        if (Color.alpha(this.f) != 0) {
            a("dcolor", a(this.f));
        }
        if (Color.alpha(this.g) != 0) {
            a("acolor", a(this.g));
        }
        if (this.h != null) {
            a("font", this.h);
        }
        a("headersize", Integer.toString(this.i));
        if (Color.alpha(this.j) != 0) {
            a("bcolor", a(this.j));
        }
        if (this.k != null) {
            a("btype", this.k.toString());
        }
        a("bthick", Integer.toString(this.l));
        return super.a(context);
    }

    private static String a(int i2) {
        return String.format("#%06x", Integer.valueOf(16777215 & i2));
    }
}
