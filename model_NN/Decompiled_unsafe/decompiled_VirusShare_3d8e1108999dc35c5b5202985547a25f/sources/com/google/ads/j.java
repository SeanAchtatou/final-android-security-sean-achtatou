package com.google.ads;

import android.webkit.WebView;
import com.google.ads.util.b;
import java.util.HashMap;

public final class j implements o {
    public final void a(x xVar, HashMap<String, String> hashMap, WebView webView) {
        String str = hashMap.get("u");
        if (str == null) {
            b.e("Could not get URL from click gmsg.");
        } else {
            new Thread(new t(str, webView.getContext().getApplicationContext())).start();
        }
    }
}
