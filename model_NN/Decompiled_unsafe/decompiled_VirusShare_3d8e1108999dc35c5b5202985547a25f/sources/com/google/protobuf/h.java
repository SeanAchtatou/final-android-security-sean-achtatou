package com.google.protobuf;

import com.badlogic.gdx.physics.box2d.Transform;
import com.google.protobuf.i;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

public final class h {
    private final byte[] a;
    private int b;
    private int c;
    private int d;
    private final InputStream e;
    private int f;
    private int g;
    private int h;
    private int i;
    private int j;
    private int k;

    public static h a(InputStream inputStream) {
        return new h(inputStream);
    }

    public static h a(byte[] bArr) {
        return a(bArr, 0, bArr.length);
    }

    public static h a(byte[] bArr, int i2, int i3) {
        return new h(bArr, i2, i3);
    }

    public final int a() throws IOException {
        if (this.d == this.b && !a(false)) {
            this.f = 0;
            return 0;
        }
        this.f = n();
        if (u.b(this.f) != 0) {
            return this.f;
        }
        throw new ac("Protocol message contained an invalid tag (zero).");
    }

    public final void a(int i2) throws ac {
        if (this.f != i2) {
            throw new ac("Protocol message end-group tag did not match expected tag.");
        }
    }

    public final boolean b(int i2) throws IOException {
        int a2;
        switch (u.a(i2)) {
            case Transform.POS_X:
                n();
                return true;
            case 1:
                q();
                return true;
            case 2:
                f(n());
                return true;
            case 3:
                break;
            case 4:
                return false;
            case 5:
                p();
                return true;
            default:
                throw ac.d();
        }
        do {
            a2 = a();
            if (a2 != 0) {
            }
            a(u.a(u.b(i2), 4));
            return true;
        } while (b(a2));
        a(u.a(u.b(i2), 4));
        return true;
    }

    public final double b() throws IOException {
        return Double.longBitsToDouble(q());
    }

    public final float c() throws IOException {
        return Float.intBitsToFloat(p());
    }

    public final long d() throws IOException {
        return o();
    }

    public final long e() throws IOException {
        return o();
    }

    public final int f() throws IOException {
        return n();
    }

    public final long g() throws IOException {
        return q();
    }

    public final int h() throws IOException {
        return p();
    }

    public final boolean i() throws IOException {
        return n() != 0;
    }

    public final String j() throws IOException {
        int n = n();
        if (n > this.b - this.d || n <= 0) {
            return new String(e(n), "UTF-8");
        }
        String str = new String(this.a, this.d, n, "UTF-8");
        this.d = n + this.d;
        return str;
    }

    public final void a(int i2, i.a aVar, ab abVar) throws IOException {
        if (this.i >= this.j) {
            throw ac.e();
        }
        this.i++;
        aVar.mergeFrom(this, abVar);
        a(u.a(i2, 4));
        this.i--;
    }

    public final void a(i.a aVar, ab abVar) throws IOException {
        int n = n();
        if (this.i >= this.j) {
            throw ac.e();
        }
        int c2 = c(n);
        this.i++;
        aVar.mergeFrom(this, abVar);
        a(0);
        this.i--;
        d(c2);
    }

    public final q k() throws IOException {
        int n = n();
        if (n > this.b - this.d || n <= 0) {
            return q.a(e(n));
        }
        q a2 = q.a(this.a, this.d, n);
        this.d = n + this.d;
        return a2;
    }

    public final int l() throws IOException {
        return n();
    }

    public final int m() throws IOException {
        return n();
    }

    public final int n() throws IOException {
        byte t = t();
        if (t >= 0) {
            return t;
        }
        byte b2 = t & Byte.MAX_VALUE;
        byte t2 = t();
        if (t2 >= 0) {
            return b2 | (t2 << 7);
        }
        byte b3 = b2 | ((t2 & Byte.MAX_VALUE) << 7);
        byte t3 = t();
        if (t3 >= 0) {
            return b3 | (t3 << 14);
        }
        byte b4 = b3 | ((t3 & Byte.MAX_VALUE) << 14);
        byte t4 = t();
        if (t4 >= 0) {
            return b4 | (t4 << 21);
        }
        byte b5 = b4 | ((t4 & Byte.MAX_VALUE) << 21);
        byte t5 = t();
        byte b6 = b5 | (t5 << 28);
        if (t5 >= 0) {
            return b6;
        }
        for (int i2 = 0; i2 < 5; i2++) {
            if (t() >= 0) {
                return b6;
            }
        }
        throw ac.c();
    }

    public final long o() throws IOException {
        long j2 = 0;
        for (int i2 = 0; i2 < 64; i2 += 7) {
            byte t = t();
            j2 |= ((long) (t & Byte.MAX_VALUE)) << i2;
            if ((t & 128) == 0) {
                return j2;
            }
        }
        throw ac.c();
    }

    public final int p() throws IOException {
        return (t() & 255) | ((t() & 255) << 8) | ((t() & 255) << 16) | ((t() & 255) << 24);
    }

    public final long q() throws IOException {
        byte t = t();
        byte t2 = t();
        return ((((long) t2) & 255) << 8) | (((long) t) & 255) | ((((long) t()) & 255) << 16) | ((((long) t()) & 255) << 24) | ((((long) t()) & 255) << 32) | ((((long) t()) & 255) << 40) | ((((long) t()) & 255) << 48) | ((((long) t()) & 255) << 56);
    }

    private h(byte[] bArr, int i2, int i3) {
        this.h = Integer.MAX_VALUE;
        this.j = 64;
        this.k = 67108864;
        this.a = bArr;
        this.b = i2 + i3;
        this.d = i2;
        this.g = -i2;
        this.e = null;
    }

    private h(InputStream inputStream) {
        this.h = Integer.MAX_VALUE;
        this.j = 64;
        this.k = 67108864;
        this.a = new byte[4096];
        this.b = 0;
        this.d = 0;
        this.g = 0;
        this.e = inputStream;
    }

    public final int c(int i2) throws ac {
        if (i2 < 0) {
            throw ac.b();
        }
        int i3 = this.g + this.d + i2;
        int i4 = this.h;
        if (i3 > i4) {
            throw ac.a();
        }
        this.h = i3;
        s();
        return i4;
    }

    private void s() {
        this.b += this.c;
        int i2 = this.g + this.b;
        if (i2 > this.h) {
            this.c = i2 - this.h;
            this.b -= this.c;
            return;
        }
        this.c = 0;
    }

    public final void d(int i2) {
        this.h = i2;
        s();
    }

    public final int r() {
        if (this.h == Integer.MAX_VALUE) {
            return -1;
        }
        return this.h - (this.g + this.d);
    }

    private boolean a(boolean z) throws IOException {
        if (this.d < this.b) {
            throw new IllegalStateException("refillBuffer() called when buffer wasn't empty.");
        } else if (this.g + this.b != this.h) {
            this.g += this.b;
            this.d = 0;
            this.b = this.e == null ? -1 : this.e.read(this.a);
            if (this.b == 0 || this.b < -1) {
                throw new IllegalStateException("InputStream#read(byte[]) returned invalid result: " + this.b + "\nThe InputStream implementation is buggy.");
            } else if (this.b == -1) {
                this.b = 0;
                if (!z) {
                    return false;
                }
                throw ac.a();
            } else {
                s();
                int i2 = this.g + this.b + this.c;
                if (i2 <= this.k && i2 >= 0) {
                    return true;
                }
                throw new ac("Protocol message was too large.  May be malicious.  Use CodedInputStream.setSizeLimit() to increase the size limit.");
            }
        } else if (!z) {
            return false;
        } else {
            throw ac.a();
        }
    }

    private byte t() throws IOException {
        if (this.d == this.b) {
            a(true);
        }
        byte[] bArr = this.a;
        int i2 = this.d;
        this.d = i2 + 1;
        return bArr[i2];
    }

    private byte[] e(int i2) throws IOException {
        if (i2 < 0) {
            throw ac.b();
        } else if (this.g + this.d + i2 > this.h) {
            f((this.h - this.g) - this.d);
            throw ac.a();
        } else if (i2 <= this.b - this.d) {
            byte[] bArr = new byte[i2];
            System.arraycopy(this.a, this.d, bArr, 0, i2);
            this.d += i2;
            return bArr;
        } else if (i2 < 4096) {
            byte[] bArr2 = new byte[i2];
            int i3 = this.b - this.d;
            System.arraycopy(this.a, this.d, bArr2, 0, i3);
            this.d = this.b;
            a(true);
            while (i2 - i3 > this.b) {
                System.arraycopy(this.a, 0, bArr2, i3, this.b);
                i3 += this.b;
                this.d = this.b;
                a(true);
            }
            System.arraycopy(this.a, 0, bArr2, i3, i2 - i3);
            this.d = i2 - i3;
            return bArr2;
        } else {
            int i4 = this.d;
            int i5 = this.b;
            this.g += this.b;
            this.d = 0;
            this.b = 0;
            ArrayList arrayList = new ArrayList();
            int i6 = i2 - (i5 - i4);
            while (i6 > 0) {
                byte[] bArr3 = new byte[Math.min(i6, 4096)];
                int i7 = 0;
                while (i7 < bArr3.length) {
                    int read = this.e == null ? -1 : this.e.read(bArr3, i7, bArr3.length - i7);
                    if (read == -1) {
                        throw ac.a();
                    }
                    this.g += read;
                    i7 += read;
                }
                arrayList.add(bArr3);
                i6 -= bArr3.length;
            }
            byte[] bArr4 = new byte[i2];
            int i8 = i5 - i4;
            System.arraycopy(this.a, i4, bArr4, 0, i8);
            Iterator it = arrayList.iterator();
            while (true) {
                int i9 = i8;
                if (!it.hasNext()) {
                    return bArr4;
                }
                byte[] bArr5 = (byte[]) it.next();
                System.arraycopy(bArr5, 0, bArr4, i9, bArr5.length);
                i8 = bArr5.length + i9;
            }
        }
    }

    private void f(int i2) throws IOException {
        if (i2 < 0) {
            throw ac.b();
        } else if (this.g + this.d + i2 > this.h) {
            f((this.h - this.g) - this.d);
            throw ac.a();
        } else if (i2 <= this.b - this.d) {
            this.d += i2;
        } else {
            int i3 = this.b - this.d;
            this.g += i3;
            this.d = 0;
            this.b = 0;
            int i4 = i3;
            while (i4 < i2) {
                int skip = this.e == null ? -1 : (int) this.e.skip((long) (i2 - i4));
                if (skip <= 0) {
                    throw ac.a();
                }
                i4 += skip;
                this.g = skip + this.g;
            }
        }
    }
}
