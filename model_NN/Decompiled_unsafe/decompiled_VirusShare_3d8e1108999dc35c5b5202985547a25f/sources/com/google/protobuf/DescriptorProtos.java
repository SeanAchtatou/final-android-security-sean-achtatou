package com.google.protobuf;

import com.badlogic.gdx.physics.box2d.Transform;
import com.google.protobuf.GeneratedMessage;
import com.google.protobuf.a;
import com.google.protobuf.c;
import com.google.protobuf.g;
import com.ptowngames.jigsaur.Jigsaur;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class DescriptorProtos {
    /* access modifiers changed from: private */
    public static c.a A;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable B;
    /* access modifiers changed from: private */
    public static c.a C;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable D;
    /* access modifiers changed from: private */
    public static c.a E;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable F;
    /* access modifiers changed from: private */
    public static c.a G;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable H;
    /* access modifiers changed from: private */
    public static c.a I;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable J;
    /* access modifiers changed from: private */
    public static c.b K;
    /* access modifiers changed from: private */
    public static c.a a;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable b;
    /* access modifiers changed from: private */
    public static c.a c;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable d;
    /* access modifiers changed from: private */
    public static c.a e;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable f;
    /* access modifiers changed from: private */
    public static c.a g;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable h;
    /* access modifiers changed from: private */
    public static c.a i;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable j;
    /* access modifiers changed from: private */
    public static c.a k;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable l;
    /* access modifiers changed from: private */
    public static c.a m;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable n;
    /* access modifiers changed from: private */
    public static c.a o;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable p;
    /* access modifiers changed from: private */
    public static c.a q;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable r;
    /* access modifiers changed from: private */
    public static c.a s;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable t;
    /* access modifiers changed from: private */
    public static c.a u;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable v;
    /* access modifiers changed from: private */
    public static c.a w;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable x;
    /* access modifiers changed from: private */
    public static c.a y;
    /* access modifiers changed from: private */
    public static GeneratedMessage.FieldAccessorTable z;

    private DescriptorProtos() {
    }

    public static final class FileDescriptorSet extends GeneratedMessage {
        public static final int FILE_FIELD_NUMBER = 1;
        private static final FileDescriptorSet defaultInstance = new FileDescriptorSet(true);
        /* access modifiers changed from: private */
        public List<FileDescriptorProto> file_;
        private int memoizedSerializedSize;

        /* synthetic */ FileDescriptorSet(r rVar) {
            this();
        }

        private FileDescriptorSet() {
            this.file_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        private FileDescriptorSet(boolean z) {
            this.file_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        public static FileDescriptorSet getDefaultInstance() {
            return defaultInstance;
        }

        public final FileDescriptorSet getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return DescriptorProtos.a;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return DescriptorProtos.b;
        }

        public final List<FileDescriptorProto> getFileList() {
            return this.file_;
        }

        public final int getFileCount() {
            return this.file_.size();
        }

        public final FileDescriptorProto getFile(int i) {
            return this.file_.get(i);
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            for (FileDescriptorProto isInitialized : getFileList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            for (FileDescriptorProto b : getFileList()) {
                xVar.b(1, b);
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            Iterator<FileDescriptorProto> it = getFileList().iterator();
            while (true) {
                int i3 = i2;
                if (it.hasNext()) {
                    i2 = x.d(1, it.next()) + i3;
                } else {
                    int serializedSize = getUnknownFields().getSerializedSize() + i3;
                    this.memoizedSerializedSize = serializedSize;
                    return serializedSize;
                }
            }
        }

        public static FileDescriptorSet parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.FileDescriptorSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FileDescriptorSet$Builder
          com.google.protobuf.DescriptorProtos.FileDescriptorSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.FileDescriptorSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FileDescriptorSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.FileDescriptorSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static FileDescriptorSet parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static FileDescriptorSet parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.FileDescriptorSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FileDescriptorSet$Builder
          com.google.protobuf.DescriptorProtos.FileDescriptorSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.FileDescriptorSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FileDescriptorSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.FileDescriptorSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static FileDescriptorSet parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static FileDescriptorSet parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.FileDescriptorSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FileDescriptorSet$Builder
          com.google.protobuf.DescriptorProtos.FileDescriptorSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.FileDescriptorSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FileDescriptorSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.FileDescriptorSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static FileDescriptorSet parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static FileDescriptorSet parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static FileDescriptorSet parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static FileDescriptorSet parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.DescriptorProtos.FileDescriptorSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FileDescriptorSet$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.FileDescriptorSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.FileDescriptorSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FileDescriptorSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.FileDescriptorSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FileDescriptorSet.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FileDescriptorSet$Builder */
        public static FileDescriptorSet parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(FileDescriptorSet fileDescriptorSet) {
            return newBuilder().mergeFrom(fileDescriptorSet);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private FileDescriptorSet result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new FileDescriptorSet((r) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final FileDescriptorSet internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new FileDescriptorSet((r) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return FileDescriptorSet.getDescriptor();
            }

            public final FileDescriptorSet getDefaultInstanceForType() {
                return FileDescriptorSet.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final FileDescriptorSet build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public FileDescriptorSet buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final FileDescriptorSet buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.file_ != Collections.EMPTY_LIST) {
                    List unused = this.result.file_ = Collections.unmodifiableList(this.result.file_);
                }
                FileDescriptorSet fileDescriptorSet = this.result;
                this.result = null;
                return fileDescriptorSet;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof FileDescriptorSet) {
                    return mergeFrom((FileDescriptorSet) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(FileDescriptorSet fileDescriptorSet) {
                if (fileDescriptorSet != FileDescriptorSet.getDefaultInstance()) {
                    if (!fileDescriptorSet.file_.isEmpty()) {
                        if (this.result.file_.isEmpty()) {
                            List unused = this.result.file_ = new ArrayList();
                        }
                        this.result.file_.addAll(fileDescriptorSet.file_);
                    }
                    mergeUnknownFields(fileDescriptorSet.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 10:
                            FileDescriptorProto.Builder newBuilder = FileDescriptorProto.newBuilder();
                            hVar.a(newBuilder, abVar);
                            addFile(newBuilder.buildPartial());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final List<FileDescriptorProto> getFileList() {
                return Collections.unmodifiableList(this.result.file_);
            }

            public final int getFileCount() {
                return this.result.getFileCount();
            }

            public final FileDescriptorProto getFile(int i) {
                return this.result.getFile(i);
            }

            public final Builder setFile(int i, FileDescriptorProto fileDescriptorProto) {
                if (fileDescriptorProto == null) {
                    throw new NullPointerException();
                }
                this.result.file_.set(i, fileDescriptorProto);
                return this;
            }

            public final Builder setFile(int i, FileDescriptorProto.Builder builder) {
                this.result.file_.set(i, builder.build());
                return this;
            }

            public final Builder addFile(FileDescriptorProto fileDescriptorProto) {
                if (fileDescriptorProto == null) {
                    throw new NullPointerException();
                }
                if (this.result.file_.isEmpty()) {
                    List unused = this.result.file_ = new ArrayList();
                }
                this.result.file_.add(fileDescriptorProto);
                return this;
            }

            public final Builder addFile(FileDescriptorProto.Builder builder) {
                if (this.result.file_.isEmpty()) {
                    List unused = this.result.file_ = new ArrayList();
                }
                this.result.file_.add(builder.build());
                return this;
            }

            public final Builder addAllFile(Iterable<? extends FileDescriptorProto> iterable) {
                if (this.result.file_.isEmpty()) {
                    List unused = this.result.file_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.file_);
                return this;
            }

            public final Builder clearFile() {
                List unused = this.result.file_ = Collections.emptyList();
                return this;
            }
        }
    }

    public static final class FileDescriptorProto extends GeneratedMessage {
        public static final int DEPENDENCY_FIELD_NUMBER = 3;
        public static final int ENUM_TYPE_FIELD_NUMBER = 5;
        public static final int EXTENSION_FIELD_NUMBER = 7;
        public static final int MESSAGE_TYPE_FIELD_NUMBER = 4;
        public static final int NAME_FIELD_NUMBER = 1;
        public static final int OPTIONS_FIELD_NUMBER = 8;
        public static final int PACKAGE_FIELD_NUMBER = 2;
        public static final int SERVICE_FIELD_NUMBER = 6;
        private static final FileDescriptorProto defaultInstance;
        /* access modifiers changed from: private */
        public List<String> dependency_;
        /* access modifiers changed from: private */
        public List<EnumDescriptorProto> enumType_;
        /* access modifiers changed from: private */
        public List<FieldDescriptorProto> extension_;
        /* access modifiers changed from: private */
        public boolean hasName;
        /* access modifiers changed from: private */
        public boolean hasOptions;
        /* access modifiers changed from: private */
        public boolean hasPackage;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public List<DescriptorProto> messageType_;
        /* access modifiers changed from: private */
        public String name_;
        /* access modifiers changed from: private */
        public FileOptions options_;
        /* access modifiers changed from: private */
        public String package_;
        /* access modifiers changed from: private */
        public List<ServiceDescriptorProto> service_;

        /* synthetic */ FileDescriptorProto(r rVar) {
            this();
        }

        private FileDescriptorProto() {
            this.name_ = "";
            this.package_ = "";
            this.dependency_ = Collections.emptyList();
            this.messageType_ = Collections.emptyList();
            this.enumType_ = Collections.emptyList();
            this.service_ = Collections.emptyList();
            this.extension_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
            this.options_ = FileOptions.getDefaultInstance();
        }

        private FileDescriptorProto(boolean z) {
            this.name_ = "";
            this.package_ = "";
            this.dependency_ = Collections.emptyList();
            this.messageType_ = Collections.emptyList();
            this.enumType_ = Collections.emptyList();
            this.service_ = Collections.emptyList();
            this.extension_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        public static FileDescriptorProto getDefaultInstance() {
            return defaultInstance;
        }

        public final FileDescriptorProto getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return DescriptorProtos.c;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return DescriptorProtos.d;
        }

        public final boolean hasName() {
            return this.hasName;
        }

        public final String getName() {
            return this.name_;
        }

        public final boolean hasPackage() {
            return this.hasPackage;
        }

        public final String getPackage() {
            return this.package_;
        }

        public final List<String> getDependencyList() {
            return this.dependency_;
        }

        public final int getDependencyCount() {
            return this.dependency_.size();
        }

        public final String getDependency(int i) {
            return this.dependency_.get(i);
        }

        public final List<DescriptorProto> getMessageTypeList() {
            return this.messageType_;
        }

        public final int getMessageTypeCount() {
            return this.messageType_.size();
        }

        public final DescriptorProto getMessageType(int i) {
            return this.messageType_.get(i);
        }

        public final List<EnumDescriptorProto> getEnumTypeList() {
            return this.enumType_;
        }

        public final int getEnumTypeCount() {
            return this.enumType_.size();
        }

        public final EnumDescriptorProto getEnumType(int i) {
            return this.enumType_.get(i);
        }

        public final List<ServiceDescriptorProto> getServiceList() {
            return this.service_;
        }

        public final int getServiceCount() {
            return this.service_.size();
        }

        public final ServiceDescriptorProto getService(int i) {
            return this.service_.get(i);
        }

        public final List<FieldDescriptorProto> getExtensionList() {
            return this.extension_;
        }

        public final int getExtensionCount() {
            return this.extension_.size();
        }

        public final FieldDescriptorProto getExtension(int i) {
            return this.extension_.get(i);
        }

        public final boolean hasOptions() {
            return this.hasOptions;
        }

        public final FileOptions getOptions() {
            return this.options_;
        }

        private void initFields() {
            this.options_ = FileOptions.getDefaultInstance();
        }

        public final boolean isInitialized() {
            for (DescriptorProto isInitialized : getMessageTypeList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            for (EnumDescriptorProto isInitialized2 : getEnumTypeList()) {
                if (!isInitialized2.isInitialized()) {
                    return false;
                }
            }
            for (ServiceDescriptorProto isInitialized3 : getServiceList()) {
                if (!isInitialized3.isInitialized()) {
                    return false;
                }
            }
            for (FieldDescriptorProto isInitialized4 : getExtensionList()) {
                if (!isInitialized4.isInitialized()) {
                    return false;
                }
            }
            if (!hasOptions() || getOptions().isInitialized()) {
                return true;
            }
            return false;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasName()) {
                xVar.a(1, getName());
            }
            if (hasPackage()) {
                xVar.a(2, getPackage());
            }
            for (String a : getDependencyList()) {
                xVar.a(3, a);
            }
            for (DescriptorProto b : getMessageTypeList()) {
                xVar.b(4, b);
            }
            for (EnumDescriptorProto b2 : getEnumTypeList()) {
                xVar.b(5, b2);
            }
            for (ServiceDescriptorProto b3 : getServiceList()) {
                xVar.b(6, b3);
            }
            for (FieldDescriptorProto b4 : getExtensionList()) {
                xVar.b(7, b4);
            }
            if (hasOptions()) {
                xVar.b(8, getOptions());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i;
            int i2;
            int i3;
            int i4 = 0;
            int i5 = this.memoizedSerializedSize;
            if (i5 != -1) {
                return i5;
            }
            if (hasName()) {
                i = x.b(1, getName()) + 0;
            } else {
                i = 0;
            }
            if (hasPackage()) {
                i2 = i + x.b(2, getPackage());
            } else {
                i2 = i;
            }
            for (String b : getDependencyList()) {
                i4 += x.b(b);
            }
            int size = i2 + i4 + (getDependencyList().size() * 1);
            Iterator<DescriptorProto> it = getMessageTypeList().iterator();
            while (true) {
                i3 = size;
                if (!it.hasNext()) {
                    break;
                }
                size = x.d(4, it.next()) + i3;
            }
            for (EnumDescriptorProto d : getEnumTypeList()) {
                i3 += x.d(5, d);
            }
            for (ServiceDescriptorProto d2 : getServiceList()) {
                i3 += x.d(6, d2);
            }
            for (FieldDescriptorProto d3 : getExtensionList()) {
                i3 += x.d(7, d3);
            }
            if (hasOptions()) {
                i3 += x.d(8, getOptions());
            }
            int serializedSize = getUnknownFields().getSerializedSize() + i3;
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static FileDescriptorProto parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.FileDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FileDescriptorProto$Builder
          com.google.protobuf.DescriptorProtos.FileDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.FileDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FileDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.FileDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static FileDescriptorProto parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static FileDescriptorProto parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.FileDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FileDescriptorProto$Builder
          com.google.protobuf.DescriptorProtos.FileDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.FileDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FileDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.FileDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static FileDescriptorProto parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static FileDescriptorProto parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.FileDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FileDescriptorProto$Builder
          com.google.protobuf.DescriptorProtos.FileDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.FileDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FileDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.FileDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static FileDescriptorProto parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static FileDescriptorProto parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static FileDescriptorProto parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static FileDescriptorProto parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.DescriptorProtos.FileDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FileDescriptorProto$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.FileDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.FileDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FileDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.FileDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FileDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FileDescriptorProto$Builder */
        public static FileDescriptorProto parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(FileDescriptorProto fileDescriptorProto) {
            return newBuilder().mergeFrom(fileDescriptorProto);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private FileDescriptorProto result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new FileDescriptorProto((r) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final FileDescriptorProto internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new FileDescriptorProto((r) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return FileDescriptorProto.getDescriptor();
            }

            public final FileDescriptorProto getDefaultInstanceForType() {
                return FileDescriptorProto.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final FileDescriptorProto build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public FileDescriptorProto buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final FileDescriptorProto buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.dependency_ != Collections.EMPTY_LIST) {
                    List unused = this.result.dependency_ = Collections.unmodifiableList(this.result.dependency_);
                }
                if (this.result.messageType_ != Collections.EMPTY_LIST) {
                    List unused2 = this.result.messageType_ = Collections.unmodifiableList(this.result.messageType_);
                }
                if (this.result.enumType_ != Collections.EMPTY_LIST) {
                    List unused3 = this.result.enumType_ = Collections.unmodifiableList(this.result.enumType_);
                }
                if (this.result.service_ != Collections.EMPTY_LIST) {
                    List unused4 = this.result.service_ = Collections.unmodifiableList(this.result.service_);
                }
                if (this.result.extension_ != Collections.EMPTY_LIST) {
                    List unused5 = this.result.extension_ = Collections.unmodifiableList(this.result.extension_);
                }
                FileDescriptorProto fileDescriptorProto = this.result;
                this.result = null;
                return fileDescriptorProto;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof FileDescriptorProto) {
                    return mergeFrom((FileDescriptorProto) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(FileDescriptorProto fileDescriptorProto) {
                if (fileDescriptorProto != FileDescriptorProto.getDefaultInstance()) {
                    if (fileDescriptorProto.hasName()) {
                        setName(fileDescriptorProto.getName());
                    }
                    if (fileDescriptorProto.hasPackage()) {
                        setPackage(fileDescriptorProto.getPackage());
                    }
                    if (!fileDescriptorProto.dependency_.isEmpty()) {
                        if (this.result.dependency_.isEmpty()) {
                            List unused = this.result.dependency_ = new ArrayList();
                        }
                        this.result.dependency_.addAll(fileDescriptorProto.dependency_);
                    }
                    if (!fileDescriptorProto.messageType_.isEmpty()) {
                        if (this.result.messageType_.isEmpty()) {
                            List unused2 = this.result.messageType_ = new ArrayList();
                        }
                        this.result.messageType_.addAll(fileDescriptorProto.messageType_);
                    }
                    if (!fileDescriptorProto.enumType_.isEmpty()) {
                        if (this.result.enumType_.isEmpty()) {
                            List unused3 = this.result.enumType_ = new ArrayList();
                        }
                        this.result.enumType_.addAll(fileDescriptorProto.enumType_);
                    }
                    if (!fileDescriptorProto.service_.isEmpty()) {
                        if (this.result.service_.isEmpty()) {
                            List unused4 = this.result.service_ = new ArrayList();
                        }
                        this.result.service_.addAll(fileDescriptorProto.service_);
                    }
                    if (!fileDescriptorProto.extension_.isEmpty()) {
                        if (this.result.extension_.isEmpty()) {
                            List unused5 = this.result.extension_ = new ArrayList();
                        }
                        this.result.extension_.addAll(fileDescriptorProto.extension_);
                    }
                    if (fileDescriptorProto.hasOptions()) {
                        mergeOptions(fileDescriptorProto.getOptions());
                    }
                    mergeUnknownFields(fileDescriptorProto.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 10:
                            setName(hVar.j());
                            break;
                        case FileOptions.PY_GENERIC_SERVICES_FIELD_NUMBER /*18*/:
                            setPackage(hVar.j());
                            break;
                        case 26:
                            addDependency(hVar.j());
                            break;
                        case 34:
                            DescriptorProto.Builder newBuilder = DescriptorProto.newBuilder();
                            hVar.a(newBuilder, abVar);
                            addMessageType(newBuilder.buildPartial());
                            break;
                        case 42:
                            EnumDescriptorProto.Builder newBuilder2 = EnumDescriptorProto.newBuilder();
                            hVar.a(newBuilder2, abVar);
                            addEnumType(newBuilder2.buildPartial());
                            break;
                        case 50:
                            ServiceDescriptorProto.Builder newBuilder3 = ServiceDescriptorProto.newBuilder();
                            hVar.a(newBuilder3, abVar);
                            addService(newBuilder3.buildPartial());
                            break;
                        case 58:
                            FieldDescriptorProto.Builder newBuilder4 = FieldDescriptorProto.newBuilder();
                            hVar.a(newBuilder4, abVar);
                            addExtension(newBuilder4.buildPartial());
                            break;
                        case 66:
                            FileOptions.Builder newBuilder5 = FileOptions.newBuilder();
                            if (hasOptions()) {
                                newBuilder5.mergeFrom(getOptions());
                            }
                            hVar.a(newBuilder5, abVar);
                            setOptions(newBuilder5.buildPartial());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasName() {
                return this.result.hasName();
            }

            public final String getName() {
                return this.result.getName();
            }

            public final Builder setName(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasName = true;
                String unused2 = this.result.name_ = str;
                return this;
            }

            public final Builder clearName() {
                boolean unused = this.result.hasName = false;
                String unused2 = this.result.name_ = FileDescriptorProto.getDefaultInstance().getName();
                return this;
            }

            public final boolean hasPackage() {
                return this.result.hasPackage();
            }

            public final String getPackage() {
                return this.result.getPackage();
            }

            public final Builder setPackage(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasPackage = true;
                String unused2 = this.result.package_ = str;
                return this;
            }

            public final Builder clearPackage() {
                boolean unused = this.result.hasPackage = false;
                String unused2 = this.result.package_ = FileDescriptorProto.getDefaultInstance().getPackage();
                return this;
            }

            public final List<String> getDependencyList() {
                return Collections.unmodifiableList(this.result.dependency_);
            }

            public final int getDependencyCount() {
                return this.result.getDependencyCount();
            }

            public final String getDependency(int i) {
                return this.result.getDependency(i);
            }

            public final Builder setDependency(int i, String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                this.result.dependency_.set(i, str);
                return this;
            }

            public final Builder addDependency(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                if (this.result.dependency_.isEmpty()) {
                    List unused = this.result.dependency_ = new ArrayList();
                }
                this.result.dependency_.add(str);
                return this;
            }

            public final Builder addAllDependency(Iterable<? extends String> iterable) {
                if (this.result.dependency_.isEmpty()) {
                    List unused = this.result.dependency_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.dependency_);
                return this;
            }

            public final Builder clearDependency() {
                List unused = this.result.dependency_ = Collections.emptyList();
                return this;
            }

            public final List<DescriptorProto> getMessageTypeList() {
                return Collections.unmodifiableList(this.result.messageType_);
            }

            public final int getMessageTypeCount() {
                return this.result.getMessageTypeCount();
            }

            public final DescriptorProto getMessageType(int i) {
                return this.result.getMessageType(i);
            }

            public final Builder setMessageType(int i, DescriptorProto descriptorProto) {
                if (descriptorProto == null) {
                    throw new NullPointerException();
                }
                this.result.messageType_.set(i, descriptorProto);
                return this;
            }

            public final Builder setMessageType(int i, DescriptorProto.Builder builder) {
                this.result.messageType_.set(i, builder.build());
                return this;
            }

            public final Builder addMessageType(DescriptorProto descriptorProto) {
                if (descriptorProto == null) {
                    throw new NullPointerException();
                }
                if (this.result.messageType_.isEmpty()) {
                    List unused = this.result.messageType_ = new ArrayList();
                }
                this.result.messageType_.add(descriptorProto);
                return this;
            }

            public final Builder addMessageType(DescriptorProto.Builder builder) {
                if (this.result.messageType_.isEmpty()) {
                    List unused = this.result.messageType_ = new ArrayList();
                }
                this.result.messageType_.add(builder.build());
                return this;
            }

            public final Builder addAllMessageType(Iterable<? extends DescriptorProto> iterable) {
                if (this.result.messageType_.isEmpty()) {
                    List unused = this.result.messageType_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.messageType_);
                return this;
            }

            public final Builder clearMessageType() {
                List unused = this.result.messageType_ = Collections.emptyList();
                return this;
            }

            public final List<EnumDescriptorProto> getEnumTypeList() {
                return Collections.unmodifiableList(this.result.enumType_);
            }

            public final int getEnumTypeCount() {
                return this.result.getEnumTypeCount();
            }

            public final EnumDescriptorProto getEnumType(int i) {
                return this.result.getEnumType(i);
            }

            public final Builder setEnumType(int i, EnumDescriptorProto enumDescriptorProto) {
                if (enumDescriptorProto == null) {
                    throw new NullPointerException();
                }
                this.result.enumType_.set(i, enumDescriptorProto);
                return this;
            }

            public final Builder setEnumType(int i, EnumDescriptorProto.Builder builder) {
                this.result.enumType_.set(i, builder.build());
                return this;
            }

            public final Builder addEnumType(EnumDescriptorProto enumDescriptorProto) {
                if (enumDescriptorProto == null) {
                    throw new NullPointerException();
                }
                if (this.result.enumType_.isEmpty()) {
                    List unused = this.result.enumType_ = new ArrayList();
                }
                this.result.enumType_.add(enumDescriptorProto);
                return this;
            }

            public final Builder addEnumType(EnumDescriptorProto.Builder builder) {
                if (this.result.enumType_.isEmpty()) {
                    List unused = this.result.enumType_ = new ArrayList();
                }
                this.result.enumType_.add(builder.build());
                return this;
            }

            public final Builder addAllEnumType(Iterable<? extends EnumDescriptorProto> iterable) {
                if (this.result.enumType_.isEmpty()) {
                    List unused = this.result.enumType_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.enumType_);
                return this;
            }

            public final Builder clearEnumType() {
                List unused = this.result.enumType_ = Collections.emptyList();
                return this;
            }

            public final List<ServiceDescriptorProto> getServiceList() {
                return Collections.unmodifiableList(this.result.service_);
            }

            public final int getServiceCount() {
                return this.result.getServiceCount();
            }

            public final ServiceDescriptorProto getService(int i) {
                return this.result.getService(i);
            }

            public final Builder setService(int i, ServiceDescriptorProto serviceDescriptorProto) {
                if (serviceDescriptorProto == null) {
                    throw new NullPointerException();
                }
                this.result.service_.set(i, serviceDescriptorProto);
                return this;
            }

            public final Builder setService(int i, ServiceDescriptorProto.Builder builder) {
                this.result.service_.set(i, builder.build());
                return this;
            }

            public final Builder addService(ServiceDescriptorProto serviceDescriptorProto) {
                if (serviceDescriptorProto == null) {
                    throw new NullPointerException();
                }
                if (this.result.service_.isEmpty()) {
                    List unused = this.result.service_ = new ArrayList();
                }
                this.result.service_.add(serviceDescriptorProto);
                return this;
            }

            public final Builder addService(ServiceDescriptorProto.Builder builder) {
                if (this.result.service_.isEmpty()) {
                    List unused = this.result.service_ = new ArrayList();
                }
                this.result.service_.add(builder.build());
                return this;
            }

            public final Builder addAllService(Iterable<? extends ServiceDescriptorProto> iterable) {
                if (this.result.service_.isEmpty()) {
                    List unused = this.result.service_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.service_);
                return this;
            }

            public final Builder clearService() {
                List unused = this.result.service_ = Collections.emptyList();
                return this;
            }

            public final List<FieldDescriptorProto> getExtensionList() {
                return Collections.unmodifiableList(this.result.extension_);
            }

            public final int getExtensionCount() {
                return this.result.getExtensionCount();
            }

            public final FieldDescriptorProto getExtension(int i) {
                return this.result.getExtension(i);
            }

            public final Builder setExtension(int i, FieldDescriptorProto fieldDescriptorProto) {
                if (fieldDescriptorProto == null) {
                    throw new NullPointerException();
                }
                this.result.extension_.set(i, fieldDescriptorProto);
                return this;
            }

            public final Builder setExtension(int i, FieldDescriptorProto.Builder builder) {
                this.result.extension_.set(i, builder.build());
                return this;
            }

            public final Builder addExtension(FieldDescriptorProto fieldDescriptorProto) {
                if (fieldDescriptorProto == null) {
                    throw new NullPointerException();
                }
                if (this.result.extension_.isEmpty()) {
                    List unused = this.result.extension_ = new ArrayList();
                }
                this.result.extension_.add(fieldDescriptorProto);
                return this;
            }

            public final Builder addExtension(FieldDescriptorProto.Builder builder) {
                if (this.result.extension_.isEmpty()) {
                    List unused = this.result.extension_ = new ArrayList();
                }
                this.result.extension_.add(builder.build());
                return this;
            }

            public final Builder addAllExtension(Iterable<? extends FieldDescriptorProto> iterable) {
                if (this.result.extension_.isEmpty()) {
                    List unused = this.result.extension_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.extension_);
                return this;
            }

            public final Builder clearExtension() {
                List unused = this.result.extension_ = Collections.emptyList();
                return this;
            }

            public final boolean hasOptions() {
                return this.result.hasOptions();
            }

            public final FileOptions getOptions() {
                return this.result.getOptions();
            }

            public final Builder setOptions(FileOptions fileOptions) {
                if (fileOptions == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasOptions = true;
                FileOptions unused2 = this.result.options_ = fileOptions;
                return this;
            }

            public final Builder setOptions(FileOptions.Builder builder) {
                boolean unused = this.result.hasOptions = true;
                FileOptions unused2 = this.result.options_ = builder.build();
                return this;
            }

            public final Builder mergeOptions(FileOptions fileOptions) {
                if (!this.result.hasOptions() || this.result.options_ == FileOptions.getDefaultInstance()) {
                    FileOptions unused = this.result.options_ = fileOptions;
                } else {
                    FileOptions unused2 = this.result.options_ = FileOptions.newBuilder(this.result.options_).mergeFrom(fileOptions).buildPartial();
                }
                boolean unused3 = this.result.hasOptions = true;
                return this;
            }

            public final Builder clearOptions() {
                boolean unused = this.result.hasOptions = false;
                FileOptions unused2 = this.result.options_ = FileOptions.getDefaultInstance();
                return this;
            }
        }

        static {
            FileDescriptorProto fileDescriptorProto = new FileDescriptorProto(true);
            defaultInstance = fileDescriptorProto;
            fileDescriptorProto.options_ = FileOptions.getDefaultInstance();
        }
    }

    public static final class DescriptorProto extends GeneratedMessage {
        public static final int ENUM_TYPE_FIELD_NUMBER = 4;
        public static final int EXTENSION_FIELD_NUMBER = 6;
        public static final int EXTENSION_RANGE_FIELD_NUMBER = 5;
        public static final int FIELD_FIELD_NUMBER = 2;
        public static final int NAME_FIELD_NUMBER = 1;
        public static final int NESTED_TYPE_FIELD_NUMBER = 3;
        public static final int OPTIONS_FIELD_NUMBER = 7;
        private static final DescriptorProto defaultInstance;
        /* access modifiers changed from: private */
        public List<EnumDescriptorProto> enumType_;
        /* access modifiers changed from: private */
        public List<ExtensionRange> extensionRange_;
        /* access modifiers changed from: private */
        public List<FieldDescriptorProto> extension_;
        /* access modifiers changed from: private */
        public List<FieldDescriptorProto> field_;
        /* access modifiers changed from: private */
        public boolean hasName;
        /* access modifiers changed from: private */
        public boolean hasOptions;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public String name_;
        /* access modifiers changed from: private */
        public List<DescriptorProto> nestedType_;
        /* access modifiers changed from: private */
        public MessageOptions options_;

        /* synthetic */ DescriptorProto(r rVar) {
            this();
        }

        private DescriptorProto() {
            this.name_ = "";
            this.field_ = Collections.emptyList();
            this.extension_ = Collections.emptyList();
            this.nestedType_ = Collections.emptyList();
            this.enumType_ = Collections.emptyList();
            this.extensionRange_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
            this.options_ = MessageOptions.getDefaultInstance();
        }

        private DescriptorProto(boolean z) {
            this.name_ = "";
            this.field_ = Collections.emptyList();
            this.extension_ = Collections.emptyList();
            this.nestedType_ = Collections.emptyList();
            this.enumType_ = Collections.emptyList();
            this.extensionRange_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        public static DescriptorProto getDefaultInstance() {
            return defaultInstance;
        }

        public final DescriptorProto getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return DescriptorProtos.e;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return DescriptorProtos.f;
        }

        public static final class ExtensionRange extends GeneratedMessage {
            public static final int END_FIELD_NUMBER = 2;
            public static final int START_FIELD_NUMBER = 1;
            private static final ExtensionRange defaultInstance = new ExtensionRange(true);
            /* access modifiers changed from: private */
            public int end_;
            /* access modifiers changed from: private */
            public boolean hasEnd;
            /* access modifiers changed from: private */
            public boolean hasStart;
            private int memoizedSerializedSize;
            /* access modifiers changed from: private */
            public int start_;

            /* synthetic */ ExtensionRange(r rVar) {
                this();
            }

            private ExtensionRange() {
                this.start_ = 0;
                this.end_ = 0;
                this.memoizedSerializedSize = -1;
            }

            private ExtensionRange(boolean z) {
                this.start_ = 0;
                this.end_ = 0;
                this.memoizedSerializedSize = -1;
            }

            public static ExtensionRange getDefaultInstance() {
                return defaultInstance;
            }

            public final ExtensionRange getDefaultInstanceForType() {
                return defaultInstance;
            }

            public static final c.a getDescriptor() {
                return DescriptorProtos.g;
            }

            /* access modifiers changed from: protected */
            public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
                return DescriptorProtos.h;
            }

            public final boolean hasStart() {
                return this.hasStart;
            }

            public final int getStart() {
                return this.start_;
            }

            public final boolean hasEnd() {
                return this.hasEnd;
            }

            public final int getEnd() {
                return this.end_;
            }

            private void initFields() {
            }

            public final boolean isInitialized() {
                return true;
            }

            public final void writeTo(x xVar) throws IOException {
                getSerializedSize();
                if (hasStart()) {
                    xVar.a(1, getStart());
                }
                if (hasEnd()) {
                    xVar.a(2, getEnd());
                }
                getUnknownFields().writeTo(xVar);
            }

            public final int getSerializedSize() {
                int i = this.memoizedSerializedSize;
                if (i != -1) {
                    return i;
                }
                int i2 = 0;
                if (hasStart()) {
                    i2 = x.d(1, getStart()) + 0;
                }
                if (hasEnd()) {
                    i2 += x.d(2, getEnd());
                }
                int serializedSize = i2 + getUnknownFields().getSerializedSize();
                this.memoizedSerializedSize = serializedSize;
                return serializedSize;
            }

            public static ExtensionRange parseFrom(q qVar) throws ac {
                return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
             arg types: [com.google.protobuf.q, com.google.protobuf.ab]
             candidates:
              com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$DescriptorProto$ExtensionRange$Builder
              com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
              com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
              com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
              com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
              com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
            public static ExtensionRange parseFrom(q qVar, ab abVar) throws ac {
                return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
            }

            public static ExtensionRange parseFrom(byte[] bArr) throws ac {
                return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
             arg types: [byte[], com.google.protobuf.ab]
             candidates:
              com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$DescriptorProto$ExtensionRange$Builder
              com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
              com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
              com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
              com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
              com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
            public static ExtensionRange parseFrom(byte[] bArr, ab abVar) throws ac {
                return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
            }

            public static ExtensionRange parseFrom(InputStream inputStream) throws IOException {
                return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
             arg types: [java.io.InputStream, com.google.protobuf.ab]
             candidates:
              com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$DescriptorProto$ExtensionRange$Builder
              com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
              com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
              com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
              com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
              com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
            public static ExtensionRange parseFrom(InputStream inputStream, ab abVar) throws IOException {
                return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
            }

            public static ExtensionRange parseDelimitedFrom(InputStream inputStream) throws IOException {
                Builder newBuilder = newBuilder();
                if (newBuilder.mergeDelimitedFrom(inputStream)) {
                    return newBuilder.buildParsed();
                }
                return null;
            }

            public static ExtensionRange parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
                Builder newBuilder = newBuilder();
                if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                    return newBuilder.buildParsed();
                }
                return null;
            }

            public static ExtensionRange parseFrom(h hVar) throws IOException {
                return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$DescriptorProto$ExtensionRange$Builder
             arg types: [com.google.protobuf.h, com.google.protobuf.ab]
             candidates:
              com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
              com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
              com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
              com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
              com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.DescriptorProtos.DescriptorProto.ExtensionRange.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$DescriptorProto$ExtensionRange$Builder */
            public static ExtensionRange parseFrom(h hVar, ab abVar) throws IOException {
                return newBuilder().mergeFrom(hVar, abVar).buildParsed();
            }

            public static Builder newBuilder() {
                return Builder.create();
            }

            public final Builder newBuilderForType() {
                return newBuilder();
            }

            public static Builder newBuilder(ExtensionRange extensionRange) {
                return newBuilder().mergeFrom(extensionRange);
            }

            public final Builder toBuilder() {
                return newBuilder(this);
            }

            public static final class Builder extends GeneratedMessage.b<Builder> {
                private ExtensionRange result;

                private Builder() {
                }

                /* access modifiers changed from: private */
                public static Builder create() {
                    Builder builder = new Builder();
                    builder.result = new ExtensionRange((r) null);
                    return builder;
                }

                /* access modifiers changed from: protected */
                public final ExtensionRange internalGetResult() {
                    return this.result;
                }

                public final Builder clear() {
                    if (this.result == null) {
                        throw new IllegalStateException("Cannot call clear() after build().");
                    }
                    this.result = new ExtensionRange((r) null);
                    return this;
                }

                public final Builder clone() {
                    return create().mergeFrom(this.result);
                }

                public final c.a getDescriptorForType() {
                    return ExtensionRange.getDescriptor();
                }

                public final ExtensionRange getDefaultInstanceForType() {
                    return ExtensionRange.getDefaultInstance();
                }

                public final boolean isInitialized() {
                    return this.result.isInitialized();
                }

                public final ExtensionRange build() {
                    if (this.result == null || isInitialized()) {
                        return buildPartial();
                    }
                    throw newUninitializedMessageException((s) this.result);
                }

                /* access modifiers changed from: private */
                public ExtensionRange buildParsed() throws ac {
                    if (isInitialized()) {
                        return buildPartial();
                    }
                    throw newUninitializedMessageException((s) this.result).a();
                }

                public final ExtensionRange buildPartial() {
                    if (this.result == null) {
                        throw new IllegalStateException("build() has already been called on this Builder.");
                    }
                    ExtensionRange extensionRange = this.result;
                    this.result = null;
                    return extensionRange;
                }

                public final Builder mergeFrom(s sVar) {
                    if (sVar instanceof ExtensionRange) {
                        return mergeFrom((ExtensionRange) sVar);
                    }
                    super.mergeFrom(sVar);
                    return this;
                }

                public final Builder mergeFrom(ExtensionRange extensionRange) {
                    if (extensionRange != ExtensionRange.getDefaultInstance()) {
                        if (extensionRange.hasStart()) {
                            setStart(extensionRange.getStart());
                        }
                        if (extensionRange.hasEnd()) {
                            setEnd(extensionRange.getEnd());
                        }
                        mergeUnknownFields(extensionRange.getUnknownFields());
                    }
                    return this;
                }

                public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                    a.b a = a.a(getUnknownFields());
                    while (true) {
                        int a2 = hVar.a();
                        switch (a2) {
                            case Transform.POS_X:
                                setUnknownFields(a.build());
                                break;
                            case 8:
                                setStart(hVar.f());
                                break;
                            case 16:
                                setEnd(hVar.f());
                                break;
                            default:
                                if (parseUnknownField(hVar, a, abVar, a2)) {
                                    break;
                                } else {
                                    setUnknownFields(a.build());
                                    break;
                                }
                        }
                    }
                    return this;
                }

                public final boolean hasStart() {
                    return this.result.hasStart();
                }

                public final int getStart() {
                    return this.result.getStart();
                }

                public final Builder setStart(int i) {
                    boolean unused = this.result.hasStart = true;
                    int unused2 = this.result.start_ = i;
                    return this;
                }

                public final Builder clearStart() {
                    boolean unused = this.result.hasStart = false;
                    int unused2 = this.result.start_ = 0;
                    return this;
                }

                public final boolean hasEnd() {
                    return this.result.hasEnd();
                }

                public final int getEnd() {
                    return this.result.getEnd();
                }

                public final Builder setEnd(int i) {
                    boolean unused = this.result.hasEnd = true;
                    int unused2 = this.result.end_ = i;
                    return this;
                }

                public final Builder clearEnd() {
                    boolean unused = this.result.hasEnd = false;
                    int unused2 = this.result.end_ = 0;
                    return this;
                }
            }
        }

        public final boolean hasName() {
            return this.hasName;
        }

        public final String getName() {
            return this.name_;
        }

        public final List<FieldDescriptorProto> getFieldList() {
            return this.field_;
        }

        public final int getFieldCount() {
            return this.field_.size();
        }

        public final FieldDescriptorProto getField(int i) {
            return this.field_.get(i);
        }

        public final List<FieldDescriptorProto> getExtensionList() {
            return this.extension_;
        }

        public final int getExtensionCount() {
            return this.extension_.size();
        }

        public final FieldDescriptorProto getExtension(int i) {
            return this.extension_.get(i);
        }

        public final List<DescriptorProto> getNestedTypeList() {
            return this.nestedType_;
        }

        public final int getNestedTypeCount() {
            return this.nestedType_.size();
        }

        public final DescriptorProto getNestedType(int i) {
            return this.nestedType_.get(i);
        }

        public final List<EnumDescriptorProto> getEnumTypeList() {
            return this.enumType_;
        }

        public final int getEnumTypeCount() {
            return this.enumType_.size();
        }

        public final EnumDescriptorProto getEnumType(int i) {
            return this.enumType_.get(i);
        }

        public final List<ExtensionRange> getExtensionRangeList() {
            return this.extensionRange_;
        }

        public final int getExtensionRangeCount() {
            return this.extensionRange_.size();
        }

        public final ExtensionRange getExtensionRange(int i) {
            return this.extensionRange_.get(i);
        }

        public final boolean hasOptions() {
            return this.hasOptions;
        }

        public final MessageOptions getOptions() {
            return this.options_;
        }

        private void initFields() {
            this.options_ = MessageOptions.getDefaultInstance();
        }

        public final boolean isInitialized() {
            for (FieldDescriptorProto isInitialized : getFieldList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            for (FieldDescriptorProto isInitialized2 : getExtensionList()) {
                if (!isInitialized2.isInitialized()) {
                    return false;
                }
            }
            for (DescriptorProto isInitialized3 : getNestedTypeList()) {
                if (!isInitialized3.isInitialized()) {
                    return false;
                }
            }
            for (EnumDescriptorProto isInitialized4 : getEnumTypeList()) {
                if (!isInitialized4.isInitialized()) {
                    return false;
                }
            }
            if (!hasOptions() || getOptions().isInitialized()) {
                return true;
            }
            return false;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasName()) {
                xVar.a(1, getName());
            }
            for (FieldDescriptorProto b : getFieldList()) {
                xVar.b(2, b);
            }
            for (DescriptorProto b2 : getNestedTypeList()) {
                xVar.b(3, b2);
            }
            for (EnumDescriptorProto b3 : getEnumTypeList()) {
                xVar.b(4, b3);
            }
            for (ExtensionRange b4 : getExtensionRangeList()) {
                xVar.b(5, b4);
            }
            for (FieldDescriptorProto b5 : getExtensionList()) {
                xVar.b(6, b5);
            }
            if (hasOptions()) {
                xVar.b(7, getOptions());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i;
            int i2 = this.memoizedSerializedSize;
            if (i2 != -1) {
                return i2;
            }
            int i3 = 0;
            if (hasName()) {
                i3 = x.b(1, getName()) + 0;
            }
            Iterator<FieldDescriptorProto> it = getFieldList().iterator();
            while (true) {
                i = i3;
                if (!it.hasNext()) {
                    break;
                }
                i3 = x.d(2, it.next()) + i;
            }
            for (DescriptorProto d : getNestedTypeList()) {
                i += x.d(3, d);
            }
            for (EnumDescriptorProto d2 : getEnumTypeList()) {
                i += x.d(4, d2);
            }
            for (ExtensionRange d3 : getExtensionRangeList()) {
                i += x.d(5, d3);
            }
            for (FieldDescriptorProto d4 : getExtensionList()) {
                i += x.d(6, d4);
            }
            if (hasOptions()) {
                i += x.d(7, getOptions());
            }
            int serializedSize = getUnknownFields().getSerializedSize() + i;
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static DescriptorProto parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.DescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$DescriptorProto$Builder
          com.google.protobuf.DescriptorProtos.DescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.DescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.DescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.DescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static DescriptorProto parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static DescriptorProto parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.DescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$DescriptorProto$Builder
          com.google.protobuf.DescriptorProtos.DescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.DescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.DescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.DescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static DescriptorProto parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static DescriptorProto parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.DescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$DescriptorProto$Builder
          com.google.protobuf.DescriptorProtos.DescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.DescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.DescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.DescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static DescriptorProto parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static DescriptorProto parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static DescriptorProto parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static DescriptorProto parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.DescriptorProtos.DescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$DescriptorProto$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.DescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.DescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.DescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.DescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.DescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$DescriptorProto$Builder */
        public static DescriptorProto parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(DescriptorProto descriptorProto) {
            return newBuilder().mergeFrom(descriptorProto);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private DescriptorProto result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new DescriptorProto((r) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final DescriptorProto internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new DescriptorProto((r) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return DescriptorProto.getDescriptor();
            }

            public final DescriptorProto getDefaultInstanceForType() {
                return DescriptorProto.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final DescriptorProto build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public DescriptorProto buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final DescriptorProto buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.field_ != Collections.EMPTY_LIST) {
                    List unused = this.result.field_ = Collections.unmodifiableList(this.result.field_);
                }
                if (this.result.extension_ != Collections.EMPTY_LIST) {
                    List unused2 = this.result.extension_ = Collections.unmodifiableList(this.result.extension_);
                }
                if (this.result.nestedType_ != Collections.EMPTY_LIST) {
                    List unused3 = this.result.nestedType_ = Collections.unmodifiableList(this.result.nestedType_);
                }
                if (this.result.enumType_ != Collections.EMPTY_LIST) {
                    List unused4 = this.result.enumType_ = Collections.unmodifiableList(this.result.enumType_);
                }
                if (this.result.extensionRange_ != Collections.EMPTY_LIST) {
                    List unused5 = this.result.extensionRange_ = Collections.unmodifiableList(this.result.extensionRange_);
                }
                DescriptorProto descriptorProto = this.result;
                this.result = null;
                return descriptorProto;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof DescriptorProto) {
                    return mergeFrom((DescriptorProto) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(DescriptorProto descriptorProto) {
                if (descriptorProto != DescriptorProto.getDefaultInstance()) {
                    if (descriptorProto.hasName()) {
                        setName(descriptorProto.getName());
                    }
                    if (!descriptorProto.field_.isEmpty()) {
                        if (this.result.field_.isEmpty()) {
                            List unused = this.result.field_ = new ArrayList();
                        }
                        this.result.field_.addAll(descriptorProto.field_);
                    }
                    if (!descriptorProto.extension_.isEmpty()) {
                        if (this.result.extension_.isEmpty()) {
                            List unused2 = this.result.extension_ = new ArrayList();
                        }
                        this.result.extension_.addAll(descriptorProto.extension_);
                    }
                    if (!descriptorProto.nestedType_.isEmpty()) {
                        if (this.result.nestedType_.isEmpty()) {
                            List unused3 = this.result.nestedType_ = new ArrayList();
                        }
                        this.result.nestedType_.addAll(descriptorProto.nestedType_);
                    }
                    if (!descriptorProto.enumType_.isEmpty()) {
                        if (this.result.enumType_.isEmpty()) {
                            List unused4 = this.result.enumType_ = new ArrayList();
                        }
                        this.result.enumType_.addAll(descriptorProto.enumType_);
                    }
                    if (!descriptorProto.extensionRange_.isEmpty()) {
                        if (this.result.extensionRange_.isEmpty()) {
                            List unused5 = this.result.extensionRange_ = new ArrayList();
                        }
                        this.result.extensionRange_.addAll(descriptorProto.extensionRange_);
                    }
                    if (descriptorProto.hasOptions()) {
                        mergeOptions(descriptorProto.getOptions());
                    }
                    mergeUnknownFields(descriptorProto.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 10:
                            setName(hVar.j());
                            break;
                        case FileOptions.PY_GENERIC_SERVICES_FIELD_NUMBER /*18*/:
                            FieldDescriptorProto.Builder newBuilder = FieldDescriptorProto.newBuilder();
                            hVar.a(newBuilder, abVar);
                            addField(newBuilder.buildPartial());
                            break;
                        case 26:
                            Builder newBuilder2 = DescriptorProto.newBuilder();
                            hVar.a(newBuilder2, abVar);
                            addNestedType(newBuilder2.buildPartial());
                            break;
                        case 34:
                            EnumDescriptorProto.Builder newBuilder3 = EnumDescriptorProto.newBuilder();
                            hVar.a(newBuilder3, abVar);
                            addEnumType(newBuilder3.buildPartial());
                            break;
                        case 42:
                            ExtensionRange.Builder newBuilder4 = ExtensionRange.newBuilder();
                            hVar.a(newBuilder4, abVar);
                            addExtensionRange(newBuilder4.buildPartial());
                            break;
                        case 50:
                            FieldDescriptorProto.Builder newBuilder5 = FieldDescriptorProto.newBuilder();
                            hVar.a(newBuilder5, abVar);
                            addExtension(newBuilder5.buildPartial());
                            break;
                        case 58:
                            MessageOptions.Builder newBuilder6 = MessageOptions.newBuilder();
                            if (hasOptions()) {
                                newBuilder6.mergeFrom(getOptions());
                            }
                            hVar.a(newBuilder6, abVar);
                            setOptions(newBuilder6.buildPartial());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasName() {
                return this.result.hasName();
            }

            public final String getName() {
                return this.result.getName();
            }

            public final Builder setName(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasName = true;
                String unused2 = this.result.name_ = str;
                return this;
            }

            public final Builder clearName() {
                boolean unused = this.result.hasName = false;
                String unused2 = this.result.name_ = DescriptorProto.getDefaultInstance().getName();
                return this;
            }

            public final List<FieldDescriptorProto> getFieldList() {
                return Collections.unmodifiableList(this.result.field_);
            }

            public final int getFieldCount() {
                return this.result.getFieldCount();
            }

            public final FieldDescriptorProto getField(int i) {
                return this.result.getField(i);
            }

            public final Builder setField(int i, FieldDescriptorProto fieldDescriptorProto) {
                if (fieldDescriptorProto == null) {
                    throw new NullPointerException();
                }
                this.result.field_.set(i, fieldDescriptorProto);
                return this;
            }

            public final Builder setField(int i, FieldDescriptorProto.Builder builder) {
                this.result.field_.set(i, builder.build());
                return this;
            }

            public final Builder addField(FieldDescriptorProto fieldDescriptorProto) {
                if (fieldDescriptorProto == null) {
                    throw new NullPointerException();
                }
                if (this.result.field_.isEmpty()) {
                    List unused = this.result.field_ = new ArrayList();
                }
                this.result.field_.add(fieldDescriptorProto);
                return this;
            }

            public final Builder addField(FieldDescriptorProto.Builder builder) {
                if (this.result.field_.isEmpty()) {
                    List unused = this.result.field_ = new ArrayList();
                }
                this.result.field_.add(builder.build());
                return this;
            }

            public final Builder addAllField(Iterable<? extends FieldDescriptorProto> iterable) {
                if (this.result.field_.isEmpty()) {
                    List unused = this.result.field_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.field_);
                return this;
            }

            public final Builder clearField() {
                List unused = this.result.field_ = Collections.emptyList();
                return this;
            }

            public final List<FieldDescriptorProto> getExtensionList() {
                return Collections.unmodifiableList(this.result.extension_);
            }

            public final int getExtensionCount() {
                return this.result.getExtensionCount();
            }

            public final FieldDescriptorProto getExtension(int i) {
                return this.result.getExtension(i);
            }

            public final Builder setExtension(int i, FieldDescriptorProto fieldDescriptorProto) {
                if (fieldDescriptorProto == null) {
                    throw new NullPointerException();
                }
                this.result.extension_.set(i, fieldDescriptorProto);
                return this;
            }

            public final Builder setExtension(int i, FieldDescriptorProto.Builder builder) {
                this.result.extension_.set(i, builder.build());
                return this;
            }

            public final Builder addExtension(FieldDescriptorProto fieldDescriptorProto) {
                if (fieldDescriptorProto == null) {
                    throw new NullPointerException();
                }
                if (this.result.extension_.isEmpty()) {
                    List unused = this.result.extension_ = new ArrayList();
                }
                this.result.extension_.add(fieldDescriptorProto);
                return this;
            }

            public final Builder addExtension(FieldDescriptorProto.Builder builder) {
                if (this.result.extension_.isEmpty()) {
                    List unused = this.result.extension_ = new ArrayList();
                }
                this.result.extension_.add(builder.build());
                return this;
            }

            public final Builder addAllExtension(Iterable<? extends FieldDescriptorProto> iterable) {
                if (this.result.extension_.isEmpty()) {
                    List unused = this.result.extension_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.extension_);
                return this;
            }

            public final Builder clearExtension() {
                List unused = this.result.extension_ = Collections.emptyList();
                return this;
            }

            public final List<DescriptorProto> getNestedTypeList() {
                return Collections.unmodifiableList(this.result.nestedType_);
            }

            public final int getNestedTypeCount() {
                return this.result.getNestedTypeCount();
            }

            public final DescriptorProto getNestedType(int i) {
                return this.result.getNestedType(i);
            }

            public final Builder setNestedType(int i, DescriptorProto descriptorProto) {
                if (descriptorProto == null) {
                    throw new NullPointerException();
                }
                this.result.nestedType_.set(i, descriptorProto);
                return this;
            }

            public final Builder setNestedType(int i, Builder builder) {
                this.result.nestedType_.set(i, builder.build());
                return this;
            }

            public final Builder addNestedType(DescriptorProto descriptorProto) {
                if (descriptorProto == null) {
                    throw new NullPointerException();
                }
                if (this.result.nestedType_.isEmpty()) {
                    List unused = this.result.nestedType_ = new ArrayList();
                }
                this.result.nestedType_.add(descriptorProto);
                return this;
            }

            public final Builder addNestedType(Builder builder) {
                if (this.result.nestedType_.isEmpty()) {
                    List unused = this.result.nestedType_ = new ArrayList();
                }
                this.result.nestedType_.add(builder.build());
                return this;
            }

            public final Builder addAllNestedType(Iterable<? extends DescriptorProto> iterable) {
                if (this.result.nestedType_.isEmpty()) {
                    List unused = this.result.nestedType_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.nestedType_);
                return this;
            }

            public final Builder clearNestedType() {
                List unused = this.result.nestedType_ = Collections.emptyList();
                return this;
            }

            public final List<EnumDescriptorProto> getEnumTypeList() {
                return Collections.unmodifiableList(this.result.enumType_);
            }

            public final int getEnumTypeCount() {
                return this.result.getEnumTypeCount();
            }

            public final EnumDescriptorProto getEnumType(int i) {
                return this.result.getEnumType(i);
            }

            public final Builder setEnumType(int i, EnumDescriptorProto enumDescriptorProto) {
                if (enumDescriptorProto == null) {
                    throw new NullPointerException();
                }
                this.result.enumType_.set(i, enumDescriptorProto);
                return this;
            }

            public final Builder setEnumType(int i, EnumDescriptorProto.Builder builder) {
                this.result.enumType_.set(i, builder.build());
                return this;
            }

            public final Builder addEnumType(EnumDescriptorProto enumDescriptorProto) {
                if (enumDescriptorProto == null) {
                    throw new NullPointerException();
                }
                if (this.result.enumType_.isEmpty()) {
                    List unused = this.result.enumType_ = new ArrayList();
                }
                this.result.enumType_.add(enumDescriptorProto);
                return this;
            }

            public final Builder addEnumType(EnumDescriptorProto.Builder builder) {
                if (this.result.enumType_.isEmpty()) {
                    List unused = this.result.enumType_ = new ArrayList();
                }
                this.result.enumType_.add(builder.build());
                return this;
            }

            public final Builder addAllEnumType(Iterable<? extends EnumDescriptorProto> iterable) {
                if (this.result.enumType_.isEmpty()) {
                    List unused = this.result.enumType_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.enumType_);
                return this;
            }

            public final Builder clearEnumType() {
                List unused = this.result.enumType_ = Collections.emptyList();
                return this;
            }

            public final List<ExtensionRange> getExtensionRangeList() {
                return Collections.unmodifiableList(this.result.extensionRange_);
            }

            public final int getExtensionRangeCount() {
                return this.result.getExtensionRangeCount();
            }

            public final ExtensionRange getExtensionRange(int i) {
                return this.result.getExtensionRange(i);
            }

            public final Builder setExtensionRange(int i, ExtensionRange extensionRange) {
                if (extensionRange == null) {
                    throw new NullPointerException();
                }
                this.result.extensionRange_.set(i, extensionRange);
                return this;
            }

            public final Builder setExtensionRange(int i, ExtensionRange.Builder builder) {
                this.result.extensionRange_.set(i, builder.build());
                return this;
            }

            public final Builder addExtensionRange(ExtensionRange extensionRange) {
                if (extensionRange == null) {
                    throw new NullPointerException();
                }
                if (this.result.extensionRange_.isEmpty()) {
                    List unused = this.result.extensionRange_ = new ArrayList();
                }
                this.result.extensionRange_.add(extensionRange);
                return this;
            }

            public final Builder addExtensionRange(ExtensionRange.Builder builder) {
                if (this.result.extensionRange_.isEmpty()) {
                    List unused = this.result.extensionRange_ = new ArrayList();
                }
                this.result.extensionRange_.add(builder.build());
                return this;
            }

            public final Builder addAllExtensionRange(Iterable<? extends ExtensionRange> iterable) {
                if (this.result.extensionRange_.isEmpty()) {
                    List unused = this.result.extensionRange_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.extensionRange_);
                return this;
            }

            public final Builder clearExtensionRange() {
                List unused = this.result.extensionRange_ = Collections.emptyList();
                return this;
            }

            public final boolean hasOptions() {
                return this.result.hasOptions();
            }

            public final MessageOptions getOptions() {
                return this.result.getOptions();
            }

            public final Builder setOptions(MessageOptions messageOptions) {
                if (messageOptions == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasOptions = true;
                MessageOptions unused2 = this.result.options_ = messageOptions;
                return this;
            }

            public final Builder setOptions(MessageOptions.Builder builder) {
                boolean unused = this.result.hasOptions = true;
                MessageOptions unused2 = this.result.options_ = builder.build();
                return this;
            }

            public final Builder mergeOptions(MessageOptions messageOptions) {
                if (!this.result.hasOptions() || this.result.options_ == MessageOptions.getDefaultInstance()) {
                    MessageOptions unused = this.result.options_ = messageOptions;
                } else {
                    MessageOptions unused2 = this.result.options_ = MessageOptions.newBuilder(this.result.options_).mergeFrom(messageOptions).buildPartial();
                }
                boolean unused3 = this.result.hasOptions = true;
                return this;
            }

            public final Builder clearOptions() {
                boolean unused = this.result.hasOptions = false;
                MessageOptions unused2 = this.result.options_ = MessageOptions.getDefaultInstance();
                return this;
            }
        }

        static {
            DescriptorProto descriptorProto = new DescriptorProto(true);
            defaultInstance = descriptorProto;
            descriptorProto.options_ = MessageOptions.getDefaultInstance();
        }
    }

    public static final class FieldDescriptorProto extends GeneratedMessage {
        public static final int DEFAULT_VALUE_FIELD_NUMBER = 7;
        public static final int EXTENDEE_FIELD_NUMBER = 2;
        public static final int LABEL_FIELD_NUMBER = 4;
        public static final int NAME_FIELD_NUMBER = 1;
        public static final int NUMBER_FIELD_NUMBER = 3;
        public static final int OPTIONS_FIELD_NUMBER = 8;
        public static final int TYPE_FIELD_NUMBER = 5;
        public static final int TYPE_NAME_FIELD_NUMBER = 6;
        private static final FieldDescriptorProto defaultInstance;
        /* access modifiers changed from: private */
        public String defaultValue_;
        /* access modifiers changed from: private */
        public String extendee_;
        /* access modifiers changed from: private */
        public boolean hasDefaultValue;
        /* access modifiers changed from: private */
        public boolean hasExtendee;
        /* access modifiers changed from: private */
        public boolean hasLabel;
        /* access modifiers changed from: private */
        public boolean hasName;
        /* access modifiers changed from: private */
        public boolean hasNumber;
        /* access modifiers changed from: private */
        public boolean hasOptions;
        /* access modifiers changed from: private */
        public boolean hasType;
        /* access modifiers changed from: private */
        public boolean hasTypeName;
        /* access modifiers changed from: private */
        public a label_;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public String name_;
        /* access modifiers changed from: private */
        public int number_;
        /* access modifiers changed from: private */
        public FieldOptions options_;
        /* access modifiers changed from: private */
        public String typeName_;
        /* access modifiers changed from: private */
        public b type_;

        /* synthetic */ FieldDescriptorProto(r rVar) {
            this();
        }

        private FieldDescriptorProto() {
            this.name_ = "";
            this.number_ = 0;
            this.typeName_ = "";
            this.extendee_ = "";
            this.defaultValue_ = "";
            this.memoizedSerializedSize = -1;
            initFields();
        }

        private FieldDescriptorProto(boolean z) {
            this.name_ = "";
            this.number_ = 0;
            this.typeName_ = "";
            this.extendee_ = "";
            this.defaultValue_ = "";
            this.memoizedSerializedSize = -1;
        }

        public static FieldDescriptorProto getDefaultInstance() {
            return defaultInstance;
        }

        public final FieldDescriptorProto getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return DescriptorProtos.i;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return DescriptorProtos.j;
        }

        public enum b implements v {
            TYPE_DOUBLE(0, 1),
            TYPE_FLOAT(1, 2),
            TYPE_INT64(2, 3),
            TYPE_UINT64(3, 4),
            TYPE_INT32(4, 5),
            TYPE_FIXED64(5, 6),
            TYPE_FIXED32(6, 7),
            TYPE_BOOL(7, 8),
            TYPE_STRING(8, 9),
            TYPE_GROUP(9, 10),
            TYPE_MESSAGE(10, 11),
            TYPE_BYTES(11, 12),
            TYPE_UINT32(12, 13),
            TYPE_ENUM(13, 14),
            TYPE_SFIXED32(14, 15),
            TYPE_SFIXED64(15, 16),
            TYPE_SINT32(16, 17),
            TYPE_SINT64(17, 18);
            
            private static g.b<b> s = new ad();
            private static final b[] t = {TYPE_DOUBLE, TYPE_FLOAT, TYPE_INT64, TYPE_UINT64, TYPE_INT32, TYPE_FIXED64, TYPE_FIXED32, TYPE_BOOL, TYPE_STRING, TYPE_GROUP, TYPE_MESSAGE, TYPE_BYTES, TYPE_UINT32, TYPE_ENUM, TYPE_SFIXED32, TYPE_SFIXED64, TYPE_SINT32, TYPE_SINT64};
            private final int u;
            private final int v;

            public final int d_() {
                return this.v;
            }

            public static b a(int i) {
                switch (i) {
                    case 1:
                        return TYPE_DOUBLE;
                    case 2:
                        return TYPE_FLOAT;
                    case 3:
                        return TYPE_INT64;
                    case 4:
                        return TYPE_UINT64;
                    case 5:
                        return TYPE_INT32;
                    case 6:
                        return TYPE_FIXED64;
                    case 7:
                        return TYPE_FIXED32;
                    case 8:
                        return TYPE_BOOL;
                    case 9:
                        return TYPE_STRING;
                    case 10:
                        return TYPE_GROUP;
                    case 11:
                        return TYPE_MESSAGE;
                    case 12:
                        return TYPE_BYTES;
                    case 13:
                        return TYPE_UINT32;
                    case Jigsaur.Level.DIFFICULTY_FAMILY_FIELD_NUMBER /*14*/:
                        return TYPE_ENUM;
                    case 15:
                        return TYPE_SFIXED32;
                    case 16:
                        return TYPE_SFIXED64;
                    case FileOptions.JAVA_GENERIC_SERVICES_FIELD_NUMBER /*17*/:
                        return TYPE_SINT32;
                    case FileOptions.PY_GENERIC_SERVICES_FIELD_NUMBER /*18*/:
                        return TYPE_SINT64;
                    default:
                        return null;
                }
            }

            private b(int i, int i2) {
                this.u = i;
                this.v = i2;
            }
        }

        public enum a implements v {
            LABEL_OPTIONAL(0, 1),
            LABEL_REQUIRED(1, 2),
            LABEL_REPEATED(2, 3);
            
            private static g.b<a> d = new y();
            private static final a[] e = {LABEL_OPTIONAL, LABEL_REQUIRED, LABEL_REPEATED};
            private final int f;
            private final int g;

            public final int d_() {
                return this.g;
            }

            public static a a(int i) {
                switch (i) {
                    case 1:
                        return LABEL_OPTIONAL;
                    case 2:
                        return LABEL_REQUIRED;
                    case 3:
                        return LABEL_REPEATED;
                    default:
                        return null;
                }
            }

            private a(int i, int i2) {
                this.f = i;
                this.g = i2;
            }
        }

        public final boolean hasName() {
            return this.hasName;
        }

        public final String getName() {
            return this.name_;
        }

        public final boolean hasNumber() {
            return this.hasNumber;
        }

        public final int getNumber() {
            return this.number_;
        }

        public final boolean hasLabel() {
            return this.hasLabel;
        }

        public final a getLabel() {
            return this.label_;
        }

        public final boolean hasType() {
            return this.hasType;
        }

        public final b getType() {
            return this.type_;
        }

        public final boolean hasTypeName() {
            return this.hasTypeName;
        }

        public final String getTypeName() {
            return this.typeName_;
        }

        public final boolean hasExtendee() {
            return this.hasExtendee;
        }

        public final String getExtendee() {
            return this.extendee_;
        }

        public final boolean hasDefaultValue() {
            return this.hasDefaultValue;
        }

        public final String getDefaultValue() {
            return this.defaultValue_;
        }

        public final boolean hasOptions() {
            return this.hasOptions;
        }

        public final FieldOptions getOptions() {
            return this.options_;
        }

        private void initFields() {
            this.label_ = a.LABEL_OPTIONAL;
            this.type_ = b.TYPE_DOUBLE;
            this.options_ = FieldOptions.getDefaultInstance();
        }

        public final boolean isInitialized() {
            if (!hasOptions() || getOptions().isInitialized()) {
                return true;
            }
            return false;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasName()) {
                xVar.a(1, getName());
            }
            if (hasExtendee()) {
                xVar.a(2, getExtendee());
            }
            if (hasNumber()) {
                xVar.a(3, getNumber());
            }
            if (hasLabel()) {
                xVar.c(4, getLabel().d_());
            }
            if (hasType()) {
                xVar.c(5, getType().d_());
            }
            if (hasTypeName()) {
                xVar.a(6, getTypeName());
            }
            if (hasDefaultValue()) {
                xVar.a(7, getDefaultValue());
            }
            if (hasOptions()) {
                xVar.b(8, getOptions());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (hasName()) {
                i2 = x.b(1, getName()) + 0;
            }
            if (hasExtendee()) {
                i2 += x.b(2, getExtendee());
            }
            if (hasNumber()) {
                i2 += x.d(3, getNumber());
            }
            if (hasLabel()) {
                i2 += x.f(4, getLabel().d_());
            }
            if (hasType()) {
                i2 += x.f(5, getType().d_());
            }
            if (hasTypeName()) {
                i2 += x.b(6, getTypeName());
            }
            if (hasDefaultValue()) {
                i2 += x.b(7, getDefaultValue());
            }
            if (hasOptions()) {
                i2 += x.d(8, getOptions());
            }
            int serializedSize = i2 + getUnknownFields().getSerializedSize();
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static FieldDescriptorProto parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.FieldDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FieldDescriptorProto$Builder
          com.google.protobuf.DescriptorProtos.FieldDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.FieldDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FieldDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.FieldDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static FieldDescriptorProto parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static FieldDescriptorProto parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.FieldDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FieldDescriptorProto$Builder
          com.google.protobuf.DescriptorProtos.FieldDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.FieldDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FieldDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.FieldDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static FieldDescriptorProto parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static FieldDescriptorProto parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.FieldDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FieldDescriptorProto$Builder
          com.google.protobuf.DescriptorProtos.FieldDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.FieldDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FieldDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.FieldDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static FieldDescriptorProto parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static FieldDescriptorProto parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static FieldDescriptorProto parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static FieldDescriptorProto parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.DescriptorProtos.FieldDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FieldDescriptorProto$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.FieldDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.FieldDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FieldDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.FieldDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FieldDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FieldDescriptorProto$Builder */
        public static FieldDescriptorProto parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(FieldDescriptorProto fieldDescriptorProto) {
            return newBuilder().mergeFrom(fieldDescriptorProto);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private FieldDescriptorProto result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new FieldDescriptorProto((r) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final FieldDescriptorProto internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new FieldDescriptorProto((r) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return FieldDescriptorProto.getDescriptor();
            }

            public final FieldDescriptorProto getDefaultInstanceForType() {
                return FieldDescriptorProto.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final FieldDescriptorProto build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public FieldDescriptorProto buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final FieldDescriptorProto buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                FieldDescriptorProto fieldDescriptorProto = this.result;
                this.result = null;
                return fieldDescriptorProto;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof FieldDescriptorProto) {
                    return mergeFrom((FieldDescriptorProto) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(FieldDescriptorProto fieldDescriptorProto) {
                if (fieldDescriptorProto != FieldDescriptorProto.getDefaultInstance()) {
                    if (fieldDescriptorProto.hasName()) {
                        setName(fieldDescriptorProto.getName());
                    }
                    if (fieldDescriptorProto.hasNumber()) {
                        setNumber(fieldDescriptorProto.getNumber());
                    }
                    if (fieldDescriptorProto.hasLabel()) {
                        setLabel(fieldDescriptorProto.getLabel());
                    }
                    if (fieldDescriptorProto.hasType()) {
                        setType(fieldDescriptorProto.getType());
                    }
                    if (fieldDescriptorProto.hasTypeName()) {
                        setTypeName(fieldDescriptorProto.getTypeName());
                    }
                    if (fieldDescriptorProto.hasExtendee()) {
                        setExtendee(fieldDescriptorProto.getExtendee());
                    }
                    if (fieldDescriptorProto.hasDefaultValue()) {
                        setDefaultValue(fieldDescriptorProto.getDefaultValue());
                    }
                    if (fieldDescriptorProto.hasOptions()) {
                        mergeOptions(fieldDescriptorProto.getOptions());
                    }
                    mergeUnknownFields(fieldDescriptorProto.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 10:
                            setName(hVar.j());
                            break;
                        case FileOptions.PY_GENERIC_SERVICES_FIELD_NUMBER /*18*/:
                            setExtendee(hVar.j());
                            break;
                        case 24:
                            setNumber(hVar.f());
                            break;
                        case 32:
                            int m = hVar.m();
                            a a3 = a.a(m);
                            if (a3 != null) {
                                setLabel(a3);
                                break;
                            } else {
                                a.a(4, m);
                                break;
                            }
                        case 40:
                            int m2 = hVar.m();
                            b a4 = b.a(m2);
                            if (a4 != null) {
                                setType(a4);
                                break;
                            } else {
                                a.a(5, m2);
                                break;
                            }
                        case 50:
                            setTypeName(hVar.j());
                            break;
                        case 58:
                            setDefaultValue(hVar.j());
                            break;
                        case 66:
                            FieldOptions.Builder newBuilder = FieldOptions.newBuilder();
                            if (hasOptions()) {
                                newBuilder.mergeFrom(getOptions());
                            }
                            hVar.a(newBuilder, abVar);
                            setOptions(newBuilder.buildPartial());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasName() {
                return this.result.hasName();
            }

            public final String getName() {
                return this.result.getName();
            }

            public final Builder setName(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasName = true;
                String unused2 = this.result.name_ = str;
                return this;
            }

            public final Builder clearName() {
                boolean unused = this.result.hasName = false;
                String unused2 = this.result.name_ = FieldDescriptorProto.getDefaultInstance().getName();
                return this;
            }

            public final boolean hasNumber() {
                return this.result.hasNumber();
            }

            public final int getNumber() {
                return this.result.getNumber();
            }

            public final Builder setNumber(int i) {
                boolean unused = this.result.hasNumber = true;
                int unused2 = this.result.number_ = i;
                return this;
            }

            public final Builder clearNumber() {
                boolean unused = this.result.hasNumber = false;
                int unused2 = this.result.number_ = 0;
                return this;
            }

            public final boolean hasLabel() {
                return this.result.hasLabel();
            }

            public final a getLabel() {
                return this.result.getLabel();
            }

            public final Builder setLabel(a aVar) {
                if (aVar == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasLabel = true;
                a unused2 = this.result.label_ = aVar;
                return this;
            }

            public final Builder clearLabel() {
                boolean unused = this.result.hasLabel = false;
                a unused2 = this.result.label_ = a.LABEL_OPTIONAL;
                return this;
            }

            public final boolean hasType() {
                return this.result.hasType();
            }

            public final b getType() {
                return this.result.getType();
            }

            public final Builder setType(b bVar) {
                if (bVar == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasType = true;
                b unused2 = this.result.type_ = bVar;
                return this;
            }

            public final Builder clearType() {
                boolean unused = this.result.hasType = false;
                b unused2 = this.result.type_ = b.TYPE_DOUBLE;
                return this;
            }

            public final boolean hasTypeName() {
                return this.result.hasTypeName();
            }

            public final String getTypeName() {
                return this.result.getTypeName();
            }

            public final Builder setTypeName(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasTypeName = true;
                String unused2 = this.result.typeName_ = str;
                return this;
            }

            public final Builder clearTypeName() {
                boolean unused = this.result.hasTypeName = false;
                String unused2 = this.result.typeName_ = FieldDescriptorProto.getDefaultInstance().getTypeName();
                return this;
            }

            public final boolean hasExtendee() {
                return this.result.hasExtendee();
            }

            public final String getExtendee() {
                return this.result.getExtendee();
            }

            public final Builder setExtendee(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasExtendee = true;
                String unused2 = this.result.extendee_ = str;
                return this;
            }

            public final Builder clearExtendee() {
                boolean unused = this.result.hasExtendee = false;
                String unused2 = this.result.extendee_ = FieldDescriptorProto.getDefaultInstance().getExtendee();
                return this;
            }

            public final boolean hasDefaultValue() {
                return this.result.hasDefaultValue();
            }

            public final String getDefaultValue() {
                return this.result.getDefaultValue();
            }

            public final Builder setDefaultValue(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasDefaultValue = true;
                String unused2 = this.result.defaultValue_ = str;
                return this;
            }

            public final Builder clearDefaultValue() {
                boolean unused = this.result.hasDefaultValue = false;
                String unused2 = this.result.defaultValue_ = FieldDescriptorProto.getDefaultInstance().getDefaultValue();
                return this;
            }

            public final boolean hasOptions() {
                return this.result.hasOptions();
            }

            public final FieldOptions getOptions() {
                return this.result.getOptions();
            }

            public final Builder setOptions(FieldOptions fieldOptions) {
                if (fieldOptions == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasOptions = true;
                FieldOptions unused2 = this.result.options_ = fieldOptions;
                return this;
            }

            public final Builder setOptions(FieldOptions.Builder builder) {
                boolean unused = this.result.hasOptions = true;
                FieldOptions unused2 = this.result.options_ = builder.build();
                return this;
            }

            public final Builder mergeOptions(FieldOptions fieldOptions) {
                if (!this.result.hasOptions() || this.result.options_ == FieldOptions.getDefaultInstance()) {
                    FieldOptions unused = this.result.options_ = fieldOptions;
                } else {
                    FieldOptions unused2 = this.result.options_ = FieldOptions.newBuilder(this.result.options_).mergeFrom(fieldOptions).buildPartial();
                }
                boolean unused3 = this.result.hasOptions = true;
                return this;
            }

            public final Builder clearOptions() {
                boolean unused = this.result.hasOptions = false;
                FieldOptions unused2 = this.result.options_ = FieldOptions.getDefaultInstance();
                return this;
            }
        }

        static {
            FieldDescriptorProto fieldDescriptorProto = new FieldDescriptorProto(true);
            defaultInstance = fieldDescriptorProto;
            fieldDescriptorProto.initFields();
        }
    }

    public static final class EnumDescriptorProto extends GeneratedMessage {
        public static final int NAME_FIELD_NUMBER = 1;
        public static final int OPTIONS_FIELD_NUMBER = 3;
        public static final int VALUE_FIELD_NUMBER = 2;
        private static final EnumDescriptorProto defaultInstance;
        /* access modifiers changed from: private */
        public boolean hasName;
        /* access modifiers changed from: private */
        public boolean hasOptions;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public String name_;
        /* access modifiers changed from: private */
        public EnumOptions options_;
        /* access modifiers changed from: private */
        public List<EnumValueDescriptorProto> value_;

        /* synthetic */ EnumDescriptorProto(r rVar) {
            this();
        }

        private EnumDescriptorProto() {
            this.name_ = "";
            this.value_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
            this.options_ = EnumOptions.getDefaultInstance();
        }

        private EnumDescriptorProto(boolean z) {
            this.name_ = "";
            this.value_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        public static EnumDescriptorProto getDefaultInstance() {
            return defaultInstance;
        }

        public final EnumDescriptorProto getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return DescriptorProtos.k;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return DescriptorProtos.l;
        }

        public final boolean hasName() {
            return this.hasName;
        }

        public final String getName() {
            return this.name_;
        }

        public final List<EnumValueDescriptorProto> getValueList() {
            return this.value_;
        }

        public final int getValueCount() {
            return this.value_.size();
        }

        public final EnumValueDescriptorProto getValue(int i) {
            return this.value_.get(i);
        }

        public final boolean hasOptions() {
            return this.hasOptions;
        }

        public final EnumOptions getOptions() {
            return this.options_;
        }

        private void initFields() {
            this.options_ = EnumOptions.getDefaultInstance();
        }

        public final boolean isInitialized() {
            for (EnumValueDescriptorProto isInitialized : getValueList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            if (!hasOptions() || getOptions().isInitialized()) {
                return true;
            }
            return false;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasName()) {
                xVar.a(1, getName());
            }
            for (EnumValueDescriptorProto b : getValueList()) {
                xVar.b(2, b);
            }
            if (hasOptions()) {
                xVar.b(3, getOptions());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i;
            int i2 = this.memoizedSerializedSize;
            if (i2 != -1) {
                return i2;
            }
            int i3 = 0;
            if (hasName()) {
                i3 = x.b(1, getName()) + 0;
            }
            Iterator<EnumValueDescriptorProto> it = getValueList().iterator();
            while (true) {
                i = i3;
                if (!it.hasNext()) {
                    break;
                }
                i3 = x.d(2, it.next()) + i;
            }
            if (hasOptions()) {
                i += x.d(3, getOptions());
            }
            int serializedSize = getUnknownFields().getSerializedSize() + i;
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static EnumDescriptorProto parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.EnumDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$EnumDescriptorProto$Builder
          com.google.protobuf.DescriptorProtos.EnumDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.EnumDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.EnumDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.EnumDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static EnumDescriptorProto parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static EnumDescriptorProto parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.EnumDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$EnumDescriptorProto$Builder
          com.google.protobuf.DescriptorProtos.EnumDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.EnumDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.EnumDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.EnumDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static EnumDescriptorProto parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static EnumDescriptorProto parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.EnumDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$EnumDescriptorProto$Builder
          com.google.protobuf.DescriptorProtos.EnumDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.EnumDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.EnumDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.EnumDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static EnumDescriptorProto parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static EnumDescriptorProto parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static EnumDescriptorProto parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static EnumDescriptorProto parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.DescriptorProtos.EnumDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$EnumDescriptorProto$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.EnumDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.EnumDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.EnumDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.EnumDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.EnumDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$EnumDescriptorProto$Builder */
        public static EnumDescriptorProto parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(EnumDescriptorProto enumDescriptorProto) {
            return newBuilder().mergeFrom(enumDescriptorProto);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private EnumDescriptorProto result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new EnumDescriptorProto((r) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final EnumDescriptorProto internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new EnumDescriptorProto((r) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return EnumDescriptorProto.getDescriptor();
            }

            public final EnumDescriptorProto getDefaultInstanceForType() {
                return EnumDescriptorProto.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final EnumDescriptorProto build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public EnumDescriptorProto buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final EnumDescriptorProto buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.value_ != Collections.EMPTY_LIST) {
                    List unused = this.result.value_ = Collections.unmodifiableList(this.result.value_);
                }
                EnumDescriptorProto enumDescriptorProto = this.result;
                this.result = null;
                return enumDescriptorProto;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof EnumDescriptorProto) {
                    return mergeFrom((EnumDescriptorProto) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(EnumDescriptorProto enumDescriptorProto) {
                if (enumDescriptorProto != EnumDescriptorProto.getDefaultInstance()) {
                    if (enumDescriptorProto.hasName()) {
                        setName(enumDescriptorProto.getName());
                    }
                    if (!enumDescriptorProto.value_.isEmpty()) {
                        if (this.result.value_.isEmpty()) {
                            List unused = this.result.value_ = new ArrayList();
                        }
                        this.result.value_.addAll(enumDescriptorProto.value_);
                    }
                    if (enumDescriptorProto.hasOptions()) {
                        mergeOptions(enumDescriptorProto.getOptions());
                    }
                    mergeUnknownFields(enumDescriptorProto.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 10:
                            setName(hVar.j());
                            break;
                        case FileOptions.PY_GENERIC_SERVICES_FIELD_NUMBER /*18*/:
                            EnumValueDescriptorProto.Builder newBuilder = EnumValueDescriptorProto.newBuilder();
                            hVar.a(newBuilder, abVar);
                            addValue(newBuilder.buildPartial());
                            break;
                        case 26:
                            EnumOptions.Builder newBuilder2 = EnumOptions.newBuilder();
                            if (hasOptions()) {
                                newBuilder2.mergeFrom(getOptions());
                            }
                            hVar.a(newBuilder2, abVar);
                            setOptions(newBuilder2.buildPartial());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasName() {
                return this.result.hasName();
            }

            public final String getName() {
                return this.result.getName();
            }

            public final Builder setName(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasName = true;
                String unused2 = this.result.name_ = str;
                return this;
            }

            public final Builder clearName() {
                boolean unused = this.result.hasName = false;
                String unused2 = this.result.name_ = EnumDescriptorProto.getDefaultInstance().getName();
                return this;
            }

            public final List<EnumValueDescriptorProto> getValueList() {
                return Collections.unmodifiableList(this.result.value_);
            }

            public final int getValueCount() {
                return this.result.getValueCount();
            }

            public final EnumValueDescriptorProto getValue(int i) {
                return this.result.getValue(i);
            }

            public final Builder setValue(int i, EnumValueDescriptorProto enumValueDescriptorProto) {
                if (enumValueDescriptorProto == null) {
                    throw new NullPointerException();
                }
                this.result.value_.set(i, enumValueDescriptorProto);
                return this;
            }

            public final Builder setValue(int i, EnumValueDescriptorProto.Builder builder) {
                this.result.value_.set(i, builder.build());
                return this;
            }

            public final Builder addValue(EnumValueDescriptorProto enumValueDescriptorProto) {
                if (enumValueDescriptorProto == null) {
                    throw new NullPointerException();
                }
                if (this.result.value_.isEmpty()) {
                    List unused = this.result.value_ = new ArrayList();
                }
                this.result.value_.add(enumValueDescriptorProto);
                return this;
            }

            public final Builder addValue(EnumValueDescriptorProto.Builder builder) {
                if (this.result.value_.isEmpty()) {
                    List unused = this.result.value_ = new ArrayList();
                }
                this.result.value_.add(builder.build());
                return this;
            }

            public final Builder addAllValue(Iterable<? extends EnumValueDescriptorProto> iterable) {
                if (this.result.value_.isEmpty()) {
                    List unused = this.result.value_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.value_);
                return this;
            }

            public final Builder clearValue() {
                List unused = this.result.value_ = Collections.emptyList();
                return this;
            }

            public final boolean hasOptions() {
                return this.result.hasOptions();
            }

            public final EnumOptions getOptions() {
                return this.result.getOptions();
            }

            public final Builder setOptions(EnumOptions enumOptions) {
                if (enumOptions == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasOptions = true;
                EnumOptions unused2 = this.result.options_ = enumOptions;
                return this;
            }

            public final Builder setOptions(EnumOptions.Builder builder) {
                boolean unused = this.result.hasOptions = true;
                EnumOptions unused2 = this.result.options_ = builder.build();
                return this;
            }

            public final Builder mergeOptions(EnumOptions enumOptions) {
                if (!this.result.hasOptions() || this.result.options_ == EnumOptions.getDefaultInstance()) {
                    EnumOptions unused = this.result.options_ = enumOptions;
                } else {
                    EnumOptions unused2 = this.result.options_ = EnumOptions.newBuilder(this.result.options_).mergeFrom(enumOptions).buildPartial();
                }
                boolean unused3 = this.result.hasOptions = true;
                return this;
            }

            public final Builder clearOptions() {
                boolean unused = this.result.hasOptions = false;
                EnumOptions unused2 = this.result.options_ = EnumOptions.getDefaultInstance();
                return this;
            }
        }

        static {
            EnumDescriptorProto enumDescriptorProto = new EnumDescriptorProto(true);
            defaultInstance = enumDescriptorProto;
            enumDescriptorProto.options_ = EnumOptions.getDefaultInstance();
        }
    }

    public static final class EnumValueDescriptorProto extends GeneratedMessage {
        public static final int NAME_FIELD_NUMBER = 1;
        public static final int NUMBER_FIELD_NUMBER = 2;
        public static final int OPTIONS_FIELD_NUMBER = 3;
        private static final EnumValueDescriptorProto defaultInstance;
        /* access modifiers changed from: private */
        public boolean hasName;
        /* access modifiers changed from: private */
        public boolean hasNumber;
        /* access modifiers changed from: private */
        public boolean hasOptions;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public String name_;
        /* access modifiers changed from: private */
        public int number_;
        /* access modifiers changed from: private */
        public EnumValueOptions options_;

        /* synthetic */ EnumValueDescriptorProto(r rVar) {
            this();
        }

        private EnumValueDescriptorProto() {
            this.name_ = "";
            this.number_ = 0;
            this.memoizedSerializedSize = -1;
            this.options_ = EnumValueOptions.getDefaultInstance();
        }

        private EnumValueDescriptorProto(boolean z) {
            this.name_ = "";
            this.number_ = 0;
            this.memoizedSerializedSize = -1;
        }

        public static EnumValueDescriptorProto getDefaultInstance() {
            return defaultInstance;
        }

        public final EnumValueDescriptorProto getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return DescriptorProtos.m;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return DescriptorProtos.n;
        }

        public final boolean hasName() {
            return this.hasName;
        }

        public final String getName() {
            return this.name_;
        }

        public final boolean hasNumber() {
            return this.hasNumber;
        }

        public final int getNumber() {
            return this.number_;
        }

        public final boolean hasOptions() {
            return this.hasOptions;
        }

        public final EnumValueOptions getOptions() {
            return this.options_;
        }

        private void initFields() {
            this.options_ = EnumValueOptions.getDefaultInstance();
        }

        public final boolean isInitialized() {
            if (!hasOptions() || getOptions().isInitialized()) {
                return true;
            }
            return false;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasName()) {
                xVar.a(1, getName());
            }
            if (hasNumber()) {
                xVar.a(2, getNumber());
            }
            if (hasOptions()) {
                xVar.b(3, getOptions());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (hasName()) {
                i2 = x.b(1, getName()) + 0;
            }
            if (hasNumber()) {
                i2 += x.d(2, getNumber());
            }
            if (hasOptions()) {
                i2 += x.d(3, getOptions());
            }
            int serializedSize = i2 + getUnknownFields().getSerializedSize();
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static EnumValueDescriptorProto parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$EnumValueDescriptorProto$Builder
          com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static EnumValueDescriptorProto parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static EnumValueDescriptorProto parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$EnumValueDescriptorProto$Builder
          com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static EnumValueDescriptorProto parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static EnumValueDescriptorProto parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$EnumValueDescriptorProto$Builder
          com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static EnumValueDescriptorProto parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static EnumValueDescriptorProto parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static EnumValueDescriptorProto parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static EnumValueDescriptorProto parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$EnumValueDescriptorProto$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.EnumValueDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$EnumValueDescriptorProto$Builder */
        public static EnumValueDescriptorProto parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(EnumValueDescriptorProto enumValueDescriptorProto) {
            return newBuilder().mergeFrom(enumValueDescriptorProto);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private EnumValueDescriptorProto result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new EnumValueDescriptorProto((r) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final EnumValueDescriptorProto internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new EnumValueDescriptorProto((r) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return EnumValueDescriptorProto.getDescriptor();
            }

            public final EnumValueDescriptorProto getDefaultInstanceForType() {
                return EnumValueDescriptorProto.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final EnumValueDescriptorProto build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public EnumValueDescriptorProto buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final EnumValueDescriptorProto buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                EnumValueDescriptorProto enumValueDescriptorProto = this.result;
                this.result = null;
                return enumValueDescriptorProto;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof EnumValueDescriptorProto) {
                    return mergeFrom((EnumValueDescriptorProto) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(EnumValueDescriptorProto enumValueDescriptorProto) {
                if (enumValueDescriptorProto != EnumValueDescriptorProto.getDefaultInstance()) {
                    if (enumValueDescriptorProto.hasName()) {
                        setName(enumValueDescriptorProto.getName());
                    }
                    if (enumValueDescriptorProto.hasNumber()) {
                        setNumber(enumValueDescriptorProto.getNumber());
                    }
                    if (enumValueDescriptorProto.hasOptions()) {
                        mergeOptions(enumValueDescriptorProto.getOptions());
                    }
                    mergeUnknownFields(enumValueDescriptorProto.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 10:
                            setName(hVar.j());
                            break;
                        case 16:
                            setNumber(hVar.n());
                            break;
                        case 26:
                            EnumValueOptions.Builder newBuilder = EnumValueOptions.newBuilder();
                            if (hasOptions()) {
                                newBuilder.mergeFrom(getOptions());
                            }
                            hVar.a(newBuilder, abVar);
                            setOptions(newBuilder.buildPartial());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasName() {
                return this.result.hasName();
            }

            public final String getName() {
                return this.result.getName();
            }

            public final Builder setName(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasName = true;
                String unused2 = this.result.name_ = str;
                return this;
            }

            public final Builder clearName() {
                boolean unused = this.result.hasName = false;
                String unused2 = this.result.name_ = EnumValueDescriptorProto.getDefaultInstance().getName();
                return this;
            }

            public final boolean hasNumber() {
                return this.result.hasNumber();
            }

            public final int getNumber() {
                return this.result.getNumber();
            }

            public final Builder setNumber(int i) {
                boolean unused = this.result.hasNumber = true;
                int unused2 = this.result.number_ = i;
                return this;
            }

            public final Builder clearNumber() {
                boolean unused = this.result.hasNumber = false;
                int unused2 = this.result.number_ = 0;
                return this;
            }

            public final boolean hasOptions() {
                return this.result.hasOptions();
            }

            public final EnumValueOptions getOptions() {
                return this.result.getOptions();
            }

            public final Builder setOptions(EnumValueOptions enumValueOptions) {
                if (enumValueOptions == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasOptions = true;
                EnumValueOptions unused2 = this.result.options_ = enumValueOptions;
                return this;
            }

            public final Builder setOptions(EnumValueOptions.Builder builder) {
                boolean unused = this.result.hasOptions = true;
                EnumValueOptions unused2 = this.result.options_ = builder.build();
                return this;
            }

            public final Builder mergeOptions(EnumValueOptions enumValueOptions) {
                if (!this.result.hasOptions() || this.result.options_ == EnumValueOptions.getDefaultInstance()) {
                    EnumValueOptions unused = this.result.options_ = enumValueOptions;
                } else {
                    EnumValueOptions unused2 = this.result.options_ = EnumValueOptions.newBuilder(this.result.options_).mergeFrom(enumValueOptions).buildPartial();
                }
                boolean unused3 = this.result.hasOptions = true;
                return this;
            }

            public final Builder clearOptions() {
                boolean unused = this.result.hasOptions = false;
                EnumValueOptions unused2 = this.result.options_ = EnumValueOptions.getDefaultInstance();
                return this;
            }
        }

        static {
            EnumValueDescriptorProto enumValueDescriptorProto = new EnumValueDescriptorProto(true);
            defaultInstance = enumValueDescriptorProto;
            enumValueDescriptorProto.options_ = EnumValueOptions.getDefaultInstance();
        }
    }

    public static final class ServiceDescriptorProto extends GeneratedMessage {
        public static final int METHOD_FIELD_NUMBER = 2;
        public static final int NAME_FIELD_NUMBER = 1;
        public static final int OPTIONS_FIELD_NUMBER = 3;
        private static final ServiceDescriptorProto defaultInstance;
        /* access modifiers changed from: private */
        public boolean hasName;
        /* access modifiers changed from: private */
        public boolean hasOptions;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public List<MethodDescriptorProto> method_;
        /* access modifiers changed from: private */
        public String name_;
        /* access modifiers changed from: private */
        public ServiceOptions options_;

        /* synthetic */ ServiceDescriptorProto(r rVar) {
            this();
        }

        private ServiceDescriptorProto() {
            this.name_ = "";
            this.method_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
            this.options_ = ServiceOptions.getDefaultInstance();
        }

        private ServiceDescriptorProto(boolean z) {
            this.name_ = "";
            this.method_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        public static ServiceDescriptorProto getDefaultInstance() {
            return defaultInstance;
        }

        public final ServiceDescriptorProto getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return DescriptorProtos.o;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return DescriptorProtos.p;
        }

        public final boolean hasName() {
            return this.hasName;
        }

        public final String getName() {
            return this.name_;
        }

        public final List<MethodDescriptorProto> getMethodList() {
            return this.method_;
        }

        public final int getMethodCount() {
            return this.method_.size();
        }

        public final MethodDescriptorProto getMethod(int i) {
            return this.method_.get(i);
        }

        public final boolean hasOptions() {
            return this.hasOptions;
        }

        public final ServiceOptions getOptions() {
            return this.options_;
        }

        private void initFields() {
            this.options_ = ServiceOptions.getDefaultInstance();
        }

        public final boolean isInitialized() {
            for (MethodDescriptorProto isInitialized : getMethodList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            if (!hasOptions() || getOptions().isInitialized()) {
                return true;
            }
            return false;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasName()) {
                xVar.a(1, getName());
            }
            for (MethodDescriptorProto b : getMethodList()) {
                xVar.b(2, b);
            }
            if (hasOptions()) {
                xVar.b(3, getOptions());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i;
            int i2 = this.memoizedSerializedSize;
            if (i2 != -1) {
                return i2;
            }
            int i3 = 0;
            if (hasName()) {
                i3 = x.b(1, getName()) + 0;
            }
            Iterator<MethodDescriptorProto> it = getMethodList().iterator();
            while (true) {
                i = i3;
                if (!it.hasNext()) {
                    break;
                }
                i3 = x.d(2, it.next()) + i;
            }
            if (hasOptions()) {
                i += x.d(3, getOptions());
            }
            int serializedSize = getUnknownFields().getSerializedSize() + i;
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static ServiceDescriptorProto parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.ServiceDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$ServiceDescriptorProto$Builder
          com.google.protobuf.DescriptorProtos.ServiceDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.ServiceDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.ServiceDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.ServiceDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static ServiceDescriptorProto parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static ServiceDescriptorProto parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.ServiceDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$ServiceDescriptorProto$Builder
          com.google.protobuf.DescriptorProtos.ServiceDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.ServiceDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.ServiceDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.ServiceDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static ServiceDescriptorProto parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static ServiceDescriptorProto parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.ServiceDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$ServiceDescriptorProto$Builder
          com.google.protobuf.DescriptorProtos.ServiceDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.ServiceDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.ServiceDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.ServiceDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static ServiceDescriptorProto parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static ServiceDescriptorProto parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static ServiceDescriptorProto parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static ServiceDescriptorProto parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.DescriptorProtos.ServiceDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$ServiceDescriptorProto$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.ServiceDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.ServiceDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.ServiceDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.ServiceDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.ServiceDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$ServiceDescriptorProto$Builder */
        public static ServiceDescriptorProto parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(ServiceDescriptorProto serviceDescriptorProto) {
            return newBuilder().mergeFrom(serviceDescriptorProto);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private ServiceDescriptorProto result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new ServiceDescriptorProto((r) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final ServiceDescriptorProto internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new ServiceDescriptorProto((r) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return ServiceDescriptorProto.getDescriptor();
            }

            public final ServiceDescriptorProto getDefaultInstanceForType() {
                return ServiceDescriptorProto.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final ServiceDescriptorProto build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public ServiceDescriptorProto buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final ServiceDescriptorProto buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.method_ != Collections.EMPTY_LIST) {
                    List unused = this.result.method_ = Collections.unmodifiableList(this.result.method_);
                }
                ServiceDescriptorProto serviceDescriptorProto = this.result;
                this.result = null;
                return serviceDescriptorProto;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof ServiceDescriptorProto) {
                    return mergeFrom((ServiceDescriptorProto) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(ServiceDescriptorProto serviceDescriptorProto) {
                if (serviceDescriptorProto != ServiceDescriptorProto.getDefaultInstance()) {
                    if (serviceDescriptorProto.hasName()) {
                        setName(serviceDescriptorProto.getName());
                    }
                    if (!serviceDescriptorProto.method_.isEmpty()) {
                        if (this.result.method_.isEmpty()) {
                            List unused = this.result.method_ = new ArrayList();
                        }
                        this.result.method_.addAll(serviceDescriptorProto.method_);
                    }
                    if (serviceDescriptorProto.hasOptions()) {
                        mergeOptions(serviceDescriptorProto.getOptions());
                    }
                    mergeUnknownFields(serviceDescriptorProto.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 10:
                            setName(hVar.j());
                            break;
                        case FileOptions.PY_GENERIC_SERVICES_FIELD_NUMBER /*18*/:
                            MethodDescriptorProto.Builder newBuilder = MethodDescriptorProto.newBuilder();
                            hVar.a(newBuilder, abVar);
                            addMethod(newBuilder.buildPartial());
                            break;
                        case 26:
                            ServiceOptions.Builder newBuilder2 = ServiceOptions.newBuilder();
                            if (hasOptions()) {
                                newBuilder2.mergeFrom(getOptions());
                            }
                            hVar.a(newBuilder2, abVar);
                            setOptions(newBuilder2.buildPartial());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasName() {
                return this.result.hasName();
            }

            public final String getName() {
                return this.result.getName();
            }

            public final Builder setName(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasName = true;
                String unused2 = this.result.name_ = str;
                return this;
            }

            public final Builder clearName() {
                boolean unused = this.result.hasName = false;
                String unused2 = this.result.name_ = ServiceDescriptorProto.getDefaultInstance().getName();
                return this;
            }

            public final List<MethodDescriptorProto> getMethodList() {
                return Collections.unmodifiableList(this.result.method_);
            }

            public final int getMethodCount() {
                return this.result.getMethodCount();
            }

            public final MethodDescriptorProto getMethod(int i) {
                return this.result.getMethod(i);
            }

            public final Builder setMethod(int i, MethodDescriptorProto methodDescriptorProto) {
                if (methodDescriptorProto == null) {
                    throw new NullPointerException();
                }
                this.result.method_.set(i, methodDescriptorProto);
                return this;
            }

            public final Builder setMethod(int i, MethodDescriptorProto.Builder builder) {
                this.result.method_.set(i, builder.build());
                return this;
            }

            public final Builder addMethod(MethodDescriptorProto methodDescriptorProto) {
                if (methodDescriptorProto == null) {
                    throw new NullPointerException();
                }
                if (this.result.method_.isEmpty()) {
                    List unused = this.result.method_ = new ArrayList();
                }
                this.result.method_.add(methodDescriptorProto);
                return this;
            }

            public final Builder addMethod(MethodDescriptorProto.Builder builder) {
                if (this.result.method_.isEmpty()) {
                    List unused = this.result.method_ = new ArrayList();
                }
                this.result.method_.add(builder.build());
                return this;
            }

            public final Builder addAllMethod(Iterable<? extends MethodDescriptorProto> iterable) {
                if (this.result.method_.isEmpty()) {
                    List unused = this.result.method_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.method_);
                return this;
            }

            public final Builder clearMethod() {
                List unused = this.result.method_ = Collections.emptyList();
                return this;
            }

            public final boolean hasOptions() {
                return this.result.hasOptions();
            }

            public final ServiceOptions getOptions() {
                return this.result.getOptions();
            }

            public final Builder setOptions(ServiceOptions serviceOptions) {
                if (serviceOptions == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasOptions = true;
                ServiceOptions unused2 = this.result.options_ = serviceOptions;
                return this;
            }

            public final Builder setOptions(ServiceOptions.Builder builder) {
                boolean unused = this.result.hasOptions = true;
                ServiceOptions unused2 = this.result.options_ = builder.build();
                return this;
            }

            public final Builder mergeOptions(ServiceOptions serviceOptions) {
                if (!this.result.hasOptions() || this.result.options_ == ServiceOptions.getDefaultInstance()) {
                    ServiceOptions unused = this.result.options_ = serviceOptions;
                } else {
                    ServiceOptions unused2 = this.result.options_ = ServiceOptions.newBuilder(this.result.options_).mergeFrom(serviceOptions).buildPartial();
                }
                boolean unused3 = this.result.hasOptions = true;
                return this;
            }

            public final Builder clearOptions() {
                boolean unused = this.result.hasOptions = false;
                ServiceOptions unused2 = this.result.options_ = ServiceOptions.getDefaultInstance();
                return this;
            }
        }

        static {
            ServiceDescriptorProto serviceDescriptorProto = new ServiceDescriptorProto(true);
            defaultInstance = serviceDescriptorProto;
            serviceDescriptorProto.options_ = ServiceOptions.getDefaultInstance();
        }
    }

    public static final class MethodDescriptorProto extends GeneratedMessage {
        public static final int INPUT_TYPE_FIELD_NUMBER = 2;
        public static final int NAME_FIELD_NUMBER = 1;
        public static final int OPTIONS_FIELD_NUMBER = 4;
        public static final int OUTPUT_TYPE_FIELD_NUMBER = 3;
        private static final MethodDescriptorProto defaultInstance;
        /* access modifiers changed from: private */
        public boolean hasInputType;
        /* access modifiers changed from: private */
        public boolean hasName;
        /* access modifiers changed from: private */
        public boolean hasOptions;
        /* access modifiers changed from: private */
        public boolean hasOutputType;
        /* access modifiers changed from: private */
        public String inputType_;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public String name_;
        /* access modifiers changed from: private */
        public MethodOptions options_;
        /* access modifiers changed from: private */
        public String outputType_;

        /* synthetic */ MethodDescriptorProto(r rVar) {
            this();
        }

        private MethodDescriptorProto() {
            this.name_ = "";
            this.inputType_ = "";
            this.outputType_ = "";
            this.memoizedSerializedSize = -1;
            this.options_ = MethodOptions.getDefaultInstance();
        }

        private MethodDescriptorProto(boolean z) {
            this.name_ = "";
            this.inputType_ = "";
            this.outputType_ = "";
            this.memoizedSerializedSize = -1;
        }

        public static MethodDescriptorProto getDefaultInstance() {
            return defaultInstance;
        }

        public final MethodDescriptorProto getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return DescriptorProtos.q;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return DescriptorProtos.r;
        }

        public final boolean hasName() {
            return this.hasName;
        }

        public final String getName() {
            return this.name_;
        }

        public final boolean hasInputType() {
            return this.hasInputType;
        }

        public final String getInputType() {
            return this.inputType_;
        }

        public final boolean hasOutputType() {
            return this.hasOutputType;
        }

        public final String getOutputType() {
            return this.outputType_;
        }

        public final boolean hasOptions() {
            return this.hasOptions;
        }

        public final MethodOptions getOptions() {
            return this.options_;
        }

        private void initFields() {
            this.options_ = MethodOptions.getDefaultInstance();
        }

        public final boolean isInitialized() {
            if (!hasOptions() || getOptions().isInitialized()) {
                return true;
            }
            return false;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            if (hasName()) {
                xVar.a(1, getName());
            }
            if (hasInputType()) {
                xVar.a(2, getInputType());
            }
            if (hasOutputType()) {
                xVar.a(3, getOutputType());
            }
            if (hasOptions()) {
                xVar.b(4, getOptions());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (hasName()) {
                i2 = x.b(1, getName()) + 0;
            }
            if (hasInputType()) {
                i2 += x.b(2, getInputType());
            }
            if (hasOutputType()) {
                i2 += x.b(3, getOutputType());
            }
            if (hasOptions()) {
                i2 += x.d(4, getOptions());
            }
            int serializedSize = i2 + getUnknownFields().getSerializedSize();
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static MethodDescriptorProto parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.MethodDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$MethodDescriptorProto$Builder
          com.google.protobuf.DescriptorProtos.MethodDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.MethodDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.MethodDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.MethodDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static MethodDescriptorProto parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static MethodDescriptorProto parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.MethodDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$MethodDescriptorProto$Builder
          com.google.protobuf.DescriptorProtos.MethodDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.MethodDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.MethodDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.MethodDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static MethodDescriptorProto parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static MethodDescriptorProto parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.MethodDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$MethodDescriptorProto$Builder
          com.google.protobuf.DescriptorProtos.MethodDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.MethodDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.MethodDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.MethodDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static MethodDescriptorProto parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static MethodDescriptorProto parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static MethodDescriptorProto parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static MethodDescriptorProto parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.DescriptorProtos.MethodDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$MethodDescriptorProto$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.MethodDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.MethodDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.MethodDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.MethodDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.MethodDescriptorProto.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$MethodDescriptorProto$Builder */
        public static MethodDescriptorProto parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(MethodDescriptorProto methodDescriptorProto) {
            return newBuilder().mergeFrom(methodDescriptorProto);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private MethodDescriptorProto result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new MethodDescriptorProto((r) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final MethodDescriptorProto internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new MethodDescriptorProto((r) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return MethodDescriptorProto.getDescriptor();
            }

            public final MethodDescriptorProto getDefaultInstanceForType() {
                return MethodDescriptorProto.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final MethodDescriptorProto build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public MethodDescriptorProto buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final MethodDescriptorProto buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                MethodDescriptorProto methodDescriptorProto = this.result;
                this.result = null;
                return methodDescriptorProto;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof MethodDescriptorProto) {
                    return mergeFrom((MethodDescriptorProto) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(MethodDescriptorProto methodDescriptorProto) {
                if (methodDescriptorProto != MethodDescriptorProto.getDefaultInstance()) {
                    if (methodDescriptorProto.hasName()) {
                        setName(methodDescriptorProto.getName());
                    }
                    if (methodDescriptorProto.hasInputType()) {
                        setInputType(methodDescriptorProto.getInputType());
                    }
                    if (methodDescriptorProto.hasOutputType()) {
                        setOutputType(methodDescriptorProto.getOutputType());
                    }
                    if (methodDescriptorProto.hasOptions()) {
                        mergeOptions(methodDescriptorProto.getOptions());
                    }
                    mergeUnknownFields(methodDescriptorProto.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 10:
                            setName(hVar.j());
                            break;
                        case FileOptions.PY_GENERIC_SERVICES_FIELD_NUMBER /*18*/:
                            setInputType(hVar.j());
                            break;
                        case 26:
                            setOutputType(hVar.j());
                            break;
                        case 34:
                            MethodOptions.Builder newBuilder = MethodOptions.newBuilder();
                            if (hasOptions()) {
                                newBuilder.mergeFrom(getOptions());
                            }
                            hVar.a(newBuilder, abVar);
                            setOptions(newBuilder.buildPartial());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasName() {
                return this.result.hasName();
            }

            public final String getName() {
                return this.result.getName();
            }

            public final Builder setName(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasName = true;
                String unused2 = this.result.name_ = str;
                return this;
            }

            public final Builder clearName() {
                boolean unused = this.result.hasName = false;
                String unused2 = this.result.name_ = MethodDescriptorProto.getDefaultInstance().getName();
                return this;
            }

            public final boolean hasInputType() {
                return this.result.hasInputType();
            }

            public final String getInputType() {
                return this.result.getInputType();
            }

            public final Builder setInputType(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasInputType = true;
                String unused2 = this.result.inputType_ = str;
                return this;
            }

            public final Builder clearInputType() {
                boolean unused = this.result.hasInputType = false;
                String unused2 = this.result.inputType_ = MethodDescriptorProto.getDefaultInstance().getInputType();
                return this;
            }

            public final boolean hasOutputType() {
                return this.result.hasOutputType();
            }

            public final String getOutputType() {
                return this.result.getOutputType();
            }

            public final Builder setOutputType(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasOutputType = true;
                String unused2 = this.result.outputType_ = str;
                return this;
            }

            public final Builder clearOutputType() {
                boolean unused = this.result.hasOutputType = false;
                String unused2 = this.result.outputType_ = MethodDescriptorProto.getDefaultInstance().getOutputType();
                return this;
            }

            public final boolean hasOptions() {
                return this.result.hasOptions();
            }

            public final MethodOptions getOptions() {
                return this.result.getOptions();
            }

            public final Builder setOptions(MethodOptions methodOptions) {
                if (methodOptions == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasOptions = true;
                MethodOptions unused2 = this.result.options_ = methodOptions;
                return this;
            }

            public final Builder setOptions(MethodOptions.Builder builder) {
                boolean unused = this.result.hasOptions = true;
                MethodOptions unused2 = this.result.options_ = builder.build();
                return this;
            }

            public final Builder mergeOptions(MethodOptions methodOptions) {
                if (!this.result.hasOptions() || this.result.options_ == MethodOptions.getDefaultInstance()) {
                    MethodOptions unused = this.result.options_ = methodOptions;
                } else {
                    MethodOptions unused2 = this.result.options_ = MethodOptions.newBuilder(this.result.options_).mergeFrom(methodOptions).buildPartial();
                }
                boolean unused3 = this.result.hasOptions = true;
                return this;
            }

            public final Builder clearOptions() {
                boolean unused = this.result.hasOptions = false;
                MethodOptions unused2 = this.result.options_ = MethodOptions.getDefaultInstance();
                return this;
            }
        }

        static {
            MethodDescriptorProto methodDescriptorProto = new MethodDescriptorProto(true);
            defaultInstance = methodDescriptorProto;
            methodDescriptorProto.options_ = MethodOptions.getDefaultInstance();
        }
    }

    public static final class FileOptions extends GeneratedMessage.ExtendableMessage<FileOptions> {
        public static final int CC_GENERIC_SERVICES_FIELD_NUMBER = 16;
        public static final int JAVA_GENERIC_SERVICES_FIELD_NUMBER = 17;
        public static final int JAVA_MULTIPLE_FILES_FIELD_NUMBER = 10;
        public static final int JAVA_OUTER_CLASSNAME_FIELD_NUMBER = 8;
        public static final int JAVA_PACKAGE_FIELD_NUMBER = 1;
        public static final int OPTIMIZE_FOR_FIELD_NUMBER = 9;
        public static final int PY_GENERIC_SERVICES_FIELD_NUMBER = 18;
        public static final int UNINTERPRETED_OPTION_FIELD_NUMBER = 999;
        private static final FileOptions defaultInstance;
        /* access modifiers changed from: private */
        public boolean ccGenericServices_;
        /* access modifiers changed from: private */
        public boolean hasCcGenericServices;
        /* access modifiers changed from: private */
        public boolean hasJavaGenericServices;
        /* access modifiers changed from: private */
        public boolean hasJavaMultipleFiles;
        /* access modifiers changed from: private */
        public boolean hasJavaOuterClassname;
        /* access modifiers changed from: private */
        public boolean hasJavaPackage;
        /* access modifiers changed from: private */
        public boolean hasOptimizeFor;
        /* access modifiers changed from: private */
        public boolean hasPyGenericServices;
        /* access modifiers changed from: private */
        public boolean javaGenericServices_;
        /* access modifiers changed from: private */
        public boolean javaMultipleFiles_;
        /* access modifiers changed from: private */
        public String javaOuterClassname_;
        /* access modifiers changed from: private */
        public String javaPackage_;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public a optimizeFor_;
        /* access modifiers changed from: private */
        public boolean pyGenericServices_;
        /* access modifiers changed from: private */
        public List<UninterpretedOption> uninterpretedOption_;

        /* synthetic */ FileOptions(r rVar) {
            this();
        }

        private FileOptions() {
            this.javaPackage_ = "";
            this.javaOuterClassname_ = "";
            this.javaMultipleFiles_ = false;
            this.ccGenericServices_ = true;
            this.javaGenericServices_ = true;
            this.pyGenericServices_ = true;
            this.uninterpretedOption_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
            this.optimizeFor_ = a.SPEED;
        }

        private FileOptions(boolean z) {
            this.javaPackage_ = "";
            this.javaOuterClassname_ = "";
            this.javaMultipleFiles_ = false;
            this.ccGenericServices_ = true;
            this.javaGenericServices_ = true;
            this.pyGenericServices_ = true;
            this.uninterpretedOption_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        public static FileOptions getDefaultInstance() {
            return defaultInstance;
        }

        public final FileOptions getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return DescriptorProtos.s;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return DescriptorProtos.t;
        }

        public enum a implements v {
            SPEED(0, 1),
            CODE_SIZE(1, 2),
            LITE_RUNTIME(2, 3);
            
            private static g.b<a> d = new d();
            private static final a[] e = {SPEED, CODE_SIZE, LITE_RUNTIME};
            private final int f;
            private final int g;

            public final int d_() {
                return this.g;
            }

            public static a a(int i) {
                switch (i) {
                    case 1:
                        return SPEED;
                    case 2:
                        return CODE_SIZE;
                    case 3:
                        return LITE_RUNTIME;
                    default:
                        return null;
                }
            }

            private a(int i, int i2) {
                this.f = i;
                this.g = i2;
            }
        }

        public final boolean hasJavaPackage() {
            return this.hasJavaPackage;
        }

        public final String getJavaPackage() {
            return this.javaPackage_;
        }

        public final boolean hasJavaOuterClassname() {
            return this.hasJavaOuterClassname;
        }

        public final String getJavaOuterClassname() {
            return this.javaOuterClassname_;
        }

        public final boolean hasJavaMultipleFiles() {
            return this.hasJavaMultipleFiles;
        }

        public final boolean getJavaMultipleFiles() {
            return this.javaMultipleFiles_;
        }

        public final boolean hasOptimizeFor() {
            return this.hasOptimizeFor;
        }

        public final a getOptimizeFor() {
            return this.optimizeFor_;
        }

        public final boolean hasCcGenericServices() {
            return this.hasCcGenericServices;
        }

        public final boolean getCcGenericServices() {
            return this.ccGenericServices_;
        }

        public final boolean hasJavaGenericServices() {
            return this.hasJavaGenericServices;
        }

        public final boolean getJavaGenericServices() {
            return this.javaGenericServices_;
        }

        public final boolean hasPyGenericServices() {
            return this.hasPyGenericServices;
        }

        public final boolean getPyGenericServices() {
            return this.pyGenericServices_;
        }

        public final List<UninterpretedOption> getUninterpretedOptionList() {
            return this.uninterpretedOption_;
        }

        public final int getUninterpretedOptionCount() {
            return this.uninterpretedOption_.size();
        }

        public final UninterpretedOption getUninterpretedOption(int i) {
            return this.uninterpretedOption_.get(i);
        }

        private void initFields() {
            this.optimizeFor_ = a.SPEED;
        }

        public final boolean isInitialized() {
            for (UninterpretedOption isInitialized : getUninterpretedOptionList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            if (!extensionsAreInitialized()) {
                return false;
            }
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            GeneratedMessage.ExtendableMessage<MessageType>.a newExtensionWriter = newExtensionWriter();
            if (hasJavaPackage()) {
                xVar.a(1, getJavaPackage());
            }
            if (hasJavaOuterClassname()) {
                xVar.a(8, getJavaOuterClassname());
            }
            if (hasOptimizeFor()) {
                xVar.c(9, getOptimizeFor().d_());
            }
            if (hasJavaMultipleFiles()) {
                xVar.a(10, getJavaMultipleFiles());
            }
            if (hasCcGenericServices()) {
                xVar.a(16, getCcGenericServices());
            }
            if (hasJavaGenericServices()) {
                xVar.a(17, getJavaGenericServices());
            }
            if (hasPyGenericServices()) {
                xVar.a(18, getPyGenericServices());
            }
            for (UninterpretedOption b : getUninterpretedOptionList()) {
                xVar.b(999, b);
            }
            newExtensionWriter.a(xVar);
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (hasJavaPackage()) {
                i2 = x.b(1, getJavaPackage()) + 0;
            }
            if (hasJavaOuterClassname()) {
                i2 += x.b(8, getJavaOuterClassname());
            }
            if (hasOptimizeFor()) {
                i2 += x.f(9, getOptimizeFor().d_());
            }
            if (hasJavaMultipleFiles()) {
                getJavaMultipleFiles();
                i2 += x.g(10) + 1;
            }
            if (hasCcGenericServices()) {
                getCcGenericServices();
                i2 += x.g(16) + 1;
            }
            if (hasJavaGenericServices()) {
                getJavaGenericServices();
                i2 += x.g(17) + 1;
            }
            if (hasPyGenericServices()) {
                getPyGenericServices();
                i2 += x.g(18) + 1;
            }
            Iterator<UninterpretedOption> it = getUninterpretedOptionList().iterator();
            while (true) {
                int i3 = i2;
                if (it.hasNext()) {
                    i2 = x.d(999, it.next()) + i3;
                } else {
                    int extensionsSerializedSize = extensionsSerializedSize() + i3 + getUnknownFields().getSerializedSize();
                    this.memoizedSerializedSize = extensionsSerializedSize;
                    return extensionsSerializedSize;
                }
            }
        }

        public static FileOptions parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.FileOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FileOptions$Builder
          com.google.protobuf.DescriptorProtos.FileOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.FileOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FileOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.FileOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static FileOptions parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static FileOptions parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.FileOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FileOptions$Builder
          com.google.protobuf.DescriptorProtos.FileOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.FileOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FileOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.FileOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static FileOptions parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static FileOptions parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.FileOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FileOptions$Builder
          com.google.protobuf.DescriptorProtos.FileOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.FileOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FileOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.FileOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static FileOptions parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static FileOptions parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static FileOptions parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static FileOptions parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.DescriptorProtos.FileOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FileOptions$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.FileOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.FileOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FileOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.FileOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FileOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FileOptions$Builder */
        public static FileOptions parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(FileOptions fileOptions) {
            return newBuilder().mergeFrom(fileOptions);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.ExtendableBuilder<FileOptions, Builder> {
            private FileOptions result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new FileOptions((r) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final FileOptions internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new FileOptions((r) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return FileOptions.getDescriptor();
            }

            public final FileOptions getDefaultInstanceForType() {
                return FileOptions.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final FileOptions build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public FileOptions buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final FileOptions buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.uninterpretedOption_ != Collections.EMPTY_LIST) {
                    List unused = this.result.uninterpretedOption_ = Collections.unmodifiableList(this.result.uninterpretedOption_);
                }
                FileOptions fileOptions = this.result;
                this.result = null;
                return fileOptions;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof FileOptions) {
                    return mergeFrom((FileOptions) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(FileOptions fileOptions) {
                if (fileOptions != FileOptions.getDefaultInstance()) {
                    if (fileOptions.hasJavaPackage()) {
                        setJavaPackage(fileOptions.getJavaPackage());
                    }
                    if (fileOptions.hasJavaOuterClassname()) {
                        setJavaOuterClassname(fileOptions.getJavaOuterClassname());
                    }
                    if (fileOptions.hasJavaMultipleFiles()) {
                        setJavaMultipleFiles(fileOptions.getJavaMultipleFiles());
                    }
                    if (fileOptions.hasOptimizeFor()) {
                        setOptimizeFor(fileOptions.getOptimizeFor());
                    }
                    if (fileOptions.hasCcGenericServices()) {
                        setCcGenericServices(fileOptions.getCcGenericServices());
                    }
                    if (fileOptions.hasJavaGenericServices()) {
                        setJavaGenericServices(fileOptions.getJavaGenericServices());
                    }
                    if (fileOptions.hasPyGenericServices()) {
                        setPyGenericServices(fileOptions.getPyGenericServices());
                    }
                    if (!fileOptions.uninterpretedOption_.isEmpty()) {
                        if (this.result.uninterpretedOption_.isEmpty()) {
                            List unused = this.result.uninterpretedOption_ = new ArrayList();
                        }
                        this.result.uninterpretedOption_.addAll(fileOptions.uninterpretedOption_);
                    }
                    mergeExtensionFields(fileOptions);
                    mergeUnknownFields(fileOptions.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 10:
                            setJavaPackage(hVar.j());
                            break;
                        case 66:
                            setJavaOuterClassname(hVar.j());
                            break;
                        case 72:
                            int m = hVar.m();
                            a a3 = a.a(m);
                            if (a3 != null) {
                                setOptimizeFor(a3);
                                break;
                            } else {
                                a.a(9, m);
                                break;
                            }
                        case 80:
                            setJavaMultipleFiles(hVar.i());
                            break;
                        case 128:
                            setCcGenericServices(hVar.i());
                            break;
                        case 136:
                            setJavaGenericServices(hVar.i());
                            break;
                        case 144:
                            setPyGenericServices(hVar.i());
                            break;
                        case 7994:
                            UninterpretedOption.Builder newBuilder = UninterpretedOption.newBuilder();
                            hVar.a(newBuilder, abVar);
                            addUninterpretedOption(newBuilder.buildPartial());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasJavaPackage() {
                return this.result.hasJavaPackage();
            }

            public final String getJavaPackage() {
                return this.result.getJavaPackage();
            }

            public final Builder setJavaPackage(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasJavaPackage = true;
                String unused2 = this.result.javaPackage_ = str;
                return this;
            }

            public final Builder clearJavaPackage() {
                boolean unused = this.result.hasJavaPackage = false;
                String unused2 = this.result.javaPackage_ = FileOptions.getDefaultInstance().getJavaPackage();
                return this;
            }

            public final boolean hasJavaOuterClassname() {
                return this.result.hasJavaOuterClassname();
            }

            public final String getJavaOuterClassname() {
                return this.result.getJavaOuterClassname();
            }

            public final Builder setJavaOuterClassname(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasJavaOuterClassname = true;
                String unused2 = this.result.javaOuterClassname_ = str;
                return this;
            }

            public final Builder clearJavaOuterClassname() {
                boolean unused = this.result.hasJavaOuterClassname = false;
                String unused2 = this.result.javaOuterClassname_ = FileOptions.getDefaultInstance().getJavaOuterClassname();
                return this;
            }

            public final boolean hasJavaMultipleFiles() {
                return this.result.hasJavaMultipleFiles();
            }

            public final boolean getJavaMultipleFiles() {
                return this.result.getJavaMultipleFiles();
            }

            public final Builder setJavaMultipleFiles(boolean z) {
                boolean unused = this.result.hasJavaMultipleFiles = true;
                boolean unused2 = this.result.javaMultipleFiles_ = z;
                return this;
            }

            public final Builder clearJavaMultipleFiles() {
                boolean unused = this.result.hasJavaMultipleFiles = false;
                boolean unused2 = this.result.javaMultipleFiles_ = false;
                return this;
            }

            public final boolean hasOptimizeFor() {
                return this.result.hasOptimizeFor();
            }

            public final a getOptimizeFor() {
                return this.result.getOptimizeFor();
            }

            public final Builder setOptimizeFor(a aVar) {
                if (aVar == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasOptimizeFor = true;
                a unused2 = this.result.optimizeFor_ = aVar;
                return this;
            }

            public final Builder clearOptimizeFor() {
                boolean unused = this.result.hasOptimizeFor = false;
                a unused2 = this.result.optimizeFor_ = a.SPEED;
                return this;
            }

            public final boolean hasCcGenericServices() {
                return this.result.hasCcGenericServices();
            }

            public final boolean getCcGenericServices() {
                return this.result.getCcGenericServices();
            }

            public final Builder setCcGenericServices(boolean z) {
                boolean unused = this.result.hasCcGenericServices = true;
                boolean unused2 = this.result.ccGenericServices_ = z;
                return this;
            }

            public final Builder clearCcGenericServices() {
                boolean unused = this.result.hasCcGenericServices = false;
                boolean unused2 = this.result.ccGenericServices_ = true;
                return this;
            }

            public final boolean hasJavaGenericServices() {
                return this.result.hasJavaGenericServices();
            }

            public final boolean getJavaGenericServices() {
                return this.result.getJavaGenericServices();
            }

            public final Builder setJavaGenericServices(boolean z) {
                boolean unused = this.result.hasJavaGenericServices = true;
                boolean unused2 = this.result.javaGenericServices_ = z;
                return this;
            }

            public final Builder clearJavaGenericServices() {
                boolean unused = this.result.hasJavaGenericServices = false;
                boolean unused2 = this.result.javaGenericServices_ = true;
                return this;
            }

            public final boolean hasPyGenericServices() {
                return this.result.hasPyGenericServices();
            }

            public final boolean getPyGenericServices() {
                return this.result.getPyGenericServices();
            }

            public final Builder setPyGenericServices(boolean z) {
                boolean unused = this.result.hasPyGenericServices = true;
                boolean unused2 = this.result.pyGenericServices_ = z;
                return this;
            }

            public final Builder clearPyGenericServices() {
                boolean unused = this.result.hasPyGenericServices = false;
                boolean unused2 = this.result.pyGenericServices_ = true;
                return this;
            }

            public final List<UninterpretedOption> getUninterpretedOptionList() {
                return Collections.unmodifiableList(this.result.uninterpretedOption_);
            }

            public final int getUninterpretedOptionCount() {
                return this.result.getUninterpretedOptionCount();
            }

            public final UninterpretedOption getUninterpretedOption(int i) {
                return this.result.getUninterpretedOption(i);
            }

            public final Builder setUninterpretedOption(int i, UninterpretedOption uninterpretedOption) {
                if (uninterpretedOption == null) {
                    throw new NullPointerException();
                }
                this.result.uninterpretedOption_.set(i, uninterpretedOption);
                return this;
            }

            public final Builder setUninterpretedOption(int i, UninterpretedOption.Builder builder) {
                this.result.uninterpretedOption_.set(i, builder.build());
                return this;
            }

            public final Builder addUninterpretedOption(UninterpretedOption uninterpretedOption) {
                if (uninterpretedOption == null) {
                    throw new NullPointerException();
                }
                if (this.result.uninterpretedOption_.isEmpty()) {
                    List unused = this.result.uninterpretedOption_ = new ArrayList();
                }
                this.result.uninterpretedOption_.add(uninterpretedOption);
                return this;
            }

            public final Builder addUninterpretedOption(UninterpretedOption.Builder builder) {
                if (this.result.uninterpretedOption_.isEmpty()) {
                    List unused = this.result.uninterpretedOption_ = new ArrayList();
                }
                this.result.uninterpretedOption_.add(builder.build());
                return this;
            }

            public final Builder addAllUninterpretedOption(Iterable<? extends UninterpretedOption> iterable) {
                if (this.result.uninterpretedOption_.isEmpty()) {
                    List unused = this.result.uninterpretedOption_ = new ArrayList();
                }
                GeneratedMessage.ExtendableBuilder.addAll(iterable, this.result.uninterpretedOption_);
                return this;
            }

            public final Builder clearUninterpretedOption() {
                List unused = this.result.uninterpretedOption_ = Collections.emptyList();
                return this;
            }
        }

        static {
            FileOptions fileOptions = new FileOptions(true);
            defaultInstance = fileOptions;
            fileOptions.optimizeFor_ = a.SPEED;
        }
    }

    public static final class MessageOptions extends GeneratedMessage.ExtendableMessage<MessageOptions> {
        public static final int MESSAGE_SET_WIRE_FORMAT_FIELD_NUMBER = 1;
        public static final int NO_STANDARD_DESCRIPTOR_ACCESSOR_FIELD_NUMBER = 2;
        public static final int UNINTERPRETED_OPTION_FIELD_NUMBER = 999;
        private static final MessageOptions defaultInstance = new MessageOptions(true);
        /* access modifiers changed from: private */
        public boolean hasMessageSetWireFormat;
        /* access modifiers changed from: private */
        public boolean hasNoStandardDescriptorAccessor;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public boolean messageSetWireFormat_;
        /* access modifiers changed from: private */
        public boolean noStandardDescriptorAccessor_;
        /* access modifiers changed from: private */
        public List<UninterpretedOption> uninterpretedOption_;

        /* synthetic */ MessageOptions(r rVar) {
            this();
        }

        private MessageOptions() {
            this.messageSetWireFormat_ = false;
            this.noStandardDescriptorAccessor_ = false;
            this.uninterpretedOption_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        private MessageOptions(boolean z) {
            this.messageSetWireFormat_ = false;
            this.noStandardDescriptorAccessor_ = false;
            this.uninterpretedOption_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        public static MessageOptions getDefaultInstance() {
            return defaultInstance;
        }

        public final MessageOptions getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return DescriptorProtos.u;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return DescriptorProtos.v;
        }

        public final boolean hasMessageSetWireFormat() {
            return this.hasMessageSetWireFormat;
        }

        public final boolean getMessageSetWireFormat() {
            return this.messageSetWireFormat_;
        }

        public final boolean hasNoStandardDescriptorAccessor() {
            return this.hasNoStandardDescriptorAccessor;
        }

        public final boolean getNoStandardDescriptorAccessor() {
            return this.noStandardDescriptorAccessor_;
        }

        public final List<UninterpretedOption> getUninterpretedOptionList() {
            return this.uninterpretedOption_;
        }

        public final int getUninterpretedOptionCount() {
            return this.uninterpretedOption_.size();
        }

        public final UninterpretedOption getUninterpretedOption(int i) {
            return this.uninterpretedOption_.get(i);
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            for (UninterpretedOption isInitialized : getUninterpretedOptionList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            if (!extensionsAreInitialized()) {
                return false;
            }
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            GeneratedMessage.ExtendableMessage<MessageType>.a newExtensionWriter = newExtensionWriter();
            if (hasMessageSetWireFormat()) {
                xVar.a(1, getMessageSetWireFormat());
            }
            if (hasNoStandardDescriptorAccessor()) {
                xVar.a(2, getNoStandardDescriptorAccessor());
            }
            for (UninterpretedOption b : getUninterpretedOptionList()) {
                xVar.b(999, b);
            }
            newExtensionWriter.a(xVar);
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (hasMessageSetWireFormat()) {
                getMessageSetWireFormat();
                i2 = x.g(1) + 1 + 0;
            }
            if (hasNoStandardDescriptorAccessor()) {
                getNoStandardDescriptorAccessor();
                i2 += x.g(2) + 1;
            }
            Iterator<UninterpretedOption> it = getUninterpretedOptionList().iterator();
            while (true) {
                int i3 = i2;
                if (it.hasNext()) {
                    i2 = x.d(999, it.next()) + i3;
                } else {
                    int extensionsSerializedSize = extensionsSerializedSize() + i3 + getUnknownFields().getSerializedSize();
                    this.memoizedSerializedSize = extensionsSerializedSize;
                    return extensionsSerializedSize;
                }
            }
        }

        public static MessageOptions parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.MessageOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$MessageOptions$Builder
          com.google.protobuf.DescriptorProtos.MessageOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.MessageOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.MessageOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.MessageOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static MessageOptions parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static MessageOptions parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.MessageOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$MessageOptions$Builder
          com.google.protobuf.DescriptorProtos.MessageOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.MessageOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.MessageOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.MessageOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static MessageOptions parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static MessageOptions parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.MessageOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$MessageOptions$Builder
          com.google.protobuf.DescriptorProtos.MessageOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.MessageOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.MessageOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.MessageOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static MessageOptions parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static MessageOptions parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static MessageOptions parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static MessageOptions parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.DescriptorProtos.MessageOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$MessageOptions$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.MessageOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.MessageOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.MessageOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.MessageOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.MessageOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$MessageOptions$Builder */
        public static MessageOptions parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(MessageOptions messageOptions) {
            return newBuilder().mergeFrom(messageOptions);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.ExtendableBuilder<MessageOptions, Builder> {
            private MessageOptions result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new MessageOptions((r) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final MessageOptions internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new MessageOptions((r) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return MessageOptions.getDescriptor();
            }

            public final MessageOptions getDefaultInstanceForType() {
                return MessageOptions.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final MessageOptions build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public MessageOptions buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final MessageOptions buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.uninterpretedOption_ != Collections.EMPTY_LIST) {
                    List unused = this.result.uninterpretedOption_ = Collections.unmodifiableList(this.result.uninterpretedOption_);
                }
                MessageOptions messageOptions = this.result;
                this.result = null;
                return messageOptions;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof MessageOptions) {
                    return mergeFrom((MessageOptions) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(MessageOptions messageOptions) {
                if (messageOptions != MessageOptions.getDefaultInstance()) {
                    if (messageOptions.hasMessageSetWireFormat()) {
                        setMessageSetWireFormat(messageOptions.getMessageSetWireFormat());
                    }
                    if (messageOptions.hasNoStandardDescriptorAccessor()) {
                        setNoStandardDescriptorAccessor(messageOptions.getNoStandardDescriptorAccessor());
                    }
                    if (!messageOptions.uninterpretedOption_.isEmpty()) {
                        if (this.result.uninterpretedOption_.isEmpty()) {
                            List unused = this.result.uninterpretedOption_ = new ArrayList();
                        }
                        this.result.uninterpretedOption_.addAll(messageOptions.uninterpretedOption_);
                    }
                    mergeExtensionFields(messageOptions);
                    mergeUnknownFields(messageOptions.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 8:
                            setMessageSetWireFormat(hVar.i());
                            break;
                        case 16:
                            setNoStandardDescriptorAccessor(hVar.i());
                            break;
                        case 7994:
                            UninterpretedOption.Builder newBuilder = UninterpretedOption.newBuilder();
                            hVar.a(newBuilder, abVar);
                            addUninterpretedOption(newBuilder.buildPartial());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasMessageSetWireFormat() {
                return this.result.hasMessageSetWireFormat();
            }

            public final boolean getMessageSetWireFormat() {
                return this.result.getMessageSetWireFormat();
            }

            public final Builder setMessageSetWireFormat(boolean z) {
                boolean unused = this.result.hasMessageSetWireFormat = true;
                boolean unused2 = this.result.messageSetWireFormat_ = z;
                return this;
            }

            public final Builder clearMessageSetWireFormat() {
                boolean unused = this.result.hasMessageSetWireFormat = false;
                boolean unused2 = this.result.messageSetWireFormat_ = false;
                return this;
            }

            public final boolean hasNoStandardDescriptorAccessor() {
                return this.result.hasNoStandardDescriptorAccessor();
            }

            public final boolean getNoStandardDescriptorAccessor() {
                return this.result.getNoStandardDescriptorAccessor();
            }

            public final Builder setNoStandardDescriptorAccessor(boolean z) {
                boolean unused = this.result.hasNoStandardDescriptorAccessor = true;
                boolean unused2 = this.result.noStandardDescriptorAccessor_ = z;
                return this;
            }

            public final Builder clearNoStandardDescriptorAccessor() {
                boolean unused = this.result.hasNoStandardDescriptorAccessor = false;
                boolean unused2 = this.result.noStandardDescriptorAccessor_ = false;
                return this;
            }

            public final List<UninterpretedOption> getUninterpretedOptionList() {
                return Collections.unmodifiableList(this.result.uninterpretedOption_);
            }

            public final int getUninterpretedOptionCount() {
                return this.result.getUninterpretedOptionCount();
            }

            public final UninterpretedOption getUninterpretedOption(int i) {
                return this.result.getUninterpretedOption(i);
            }

            public final Builder setUninterpretedOption(int i, UninterpretedOption uninterpretedOption) {
                if (uninterpretedOption == null) {
                    throw new NullPointerException();
                }
                this.result.uninterpretedOption_.set(i, uninterpretedOption);
                return this;
            }

            public final Builder setUninterpretedOption(int i, UninterpretedOption.Builder builder) {
                this.result.uninterpretedOption_.set(i, builder.build());
                return this;
            }

            public final Builder addUninterpretedOption(UninterpretedOption uninterpretedOption) {
                if (uninterpretedOption == null) {
                    throw new NullPointerException();
                }
                if (this.result.uninterpretedOption_.isEmpty()) {
                    List unused = this.result.uninterpretedOption_ = new ArrayList();
                }
                this.result.uninterpretedOption_.add(uninterpretedOption);
                return this;
            }

            public final Builder addUninterpretedOption(UninterpretedOption.Builder builder) {
                if (this.result.uninterpretedOption_.isEmpty()) {
                    List unused = this.result.uninterpretedOption_ = new ArrayList();
                }
                this.result.uninterpretedOption_.add(builder.build());
                return this;
            }

            public final Builder addAllUninterpretedOption(Iterable<? extends UninterpretedOption> iterable) {
                if (this.result.uninterpretedOption_.isEmpty()) {
                    List unused = this.result.uninterpretedOption_ = new ArrayList();
                }
                GeneratedMessage.ExtendableBuilder.addAll(iterable, this.result.uninterpretedOption_);
                return this;
            }

            public final Builder clearUninterpretedOption() {
                List unused = this.result.uninterpretedOption_ = Collections.emptyList();
                return this;
            }
        }
    }

    public static final class FieldOptions extends GeneratedMessage.ExtendableMessage<FieldOptions> {
        public static final int CTYPE_FIELD_NUMBER = 1;
        public static final int DEPRECATED_FIELD_NUMBER = 3;
        public static final int EXPERIMENTAL_MAP_KEY_FIELD_NUMBER = 9;
        public static final int PACKED_FIELD_NUMBER = 2;
        public static final int UNINTERPRETED_OPTION_FIELD_NUMBER = 999;
        private static final FieldOptions defaultInstance;
        /* access modifiers changed from: private */
        public a ctype_;
        /* access modifiers changed from: private */
        public boolean deprecated_;
        /* access modifiers changed from: private */
        public String experimentalMapKey_;
        /* access modifiers changed from: private */
        public boolean hasCtype;
        /* access modifiers changed from: private */
        public boolean hasDeprecated;
        /* access modifiers changed from: private */
        public boolean hasExperimentalMapKey;
        /* access modifiers changed from: private */
        public boolean hasPacked;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public boolean packed_;
        /* access modifiers changed from: private */
        public List<UninterpretedOption> uninterpretedOption_;

        /* synthetic */ FieldOptions(r rVar) {
            this();
        }

        private FieldOptions() {
            this.packed_ = false;
            this.deprecated_ = false;
            this.experimentalMapKey_ = "";
            this.uninterpretedOption_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
            this.ctype_ = a.STRING;
        }

        private FieldOptions(boolean z) {
            this.packed_ = false;
            this.deprecated_ = false;
            this.experimentalMapKey_ = "";
            this.uninterpretedOption_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        public static FieldOptions getDefaultInstance() {
            return defaultInstance;
        }

        public final FieldOptions getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return DescriptorProtos.w;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return DescriptorProtos.x;
        }

        public enum a implements v {
            STRING(0, 0),
            CORD(1, 1),
            STRING_PIECE(2, 2);
            
            private static g.b<a> d = new w();
            private static final a[] e = {STRING, CORD, STRING_PIECE};
            private final int f;
            private final int g;

            public final int d_() {
                return this.g;
            }

            public static a a(int i) {
                switch (i) {
                    case Transform.POS_X:
                        return STRING;
                    case 1:
                        return CORD;
                    case 2:
                        return STRING_PIECE;
                    default:
                        return null;
                }
            }

            private a(int i, int i2) {
                this.f = i;
                this.g = i2;
            }
        }

        public final boolean hasCtype() {
            return this.hasCtype;
        }

        public final a getCtype() {
            return this.ctype_;
        }

        public final boolean hasPacked() {
            return this.hasPacked;
        }

        public final boolean getPacked() {
            return this.packed_;
        }

        public final boolean hasDeprecated() {
            return this.hasDeprecated;
        }

        public final boolean getDeprecated() {
            return this.deprecated_;
        }

        public final boolean hasExperimentalMapKey() {
            return this.hasExperimentalMapKey;
        }

        public final String getExperimentalMapKey() {
            return this.experimentalMapKey_;
        }

        public final List<UninterpretedOption> getUninterpretedOptionList() {
            return this.uninterpretedOption_;
        }

        public final int getUninterpretedOptionCount() {
            return this.uninterpretedOption_.size();
        }

        public final UninterpretedOption getUninterpretedOption(int i) {
            return this.uninterpretedOption_.get(i);
        }

        private void initFields() {
            this.ctype_ = a.STRING;
        }

        public final boolean isInitialized() {
            for (UninterpretedOption isInitialized : getUninterpretedOptionList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            if (!extensionsAreInitialized()) {
                return false;
            }
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            GeneratedMessage.ExtendableMessage<MessageType>.a newExtensionWriter = newExtensionWriter();
            if (hasCtype()) {
                xVar.c(1, getCtype().d_());
            }
            if (hasPacked()) {
                xVar.a(2, getPacked());
            }
            if (hasDeprecated()) {
                xVar.a(3, getDeprecated());
            }
            if (hasExperimentalMapKey()) {
                xVar.a(9, getExperimentalMapKey());
            }
            for (UninterpretedOption b : getUninterpretedOptionList()) {
                xVar.b(999, b);
            }
            newExtensionWriter.a(xVar);
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            if (hasCtype()) {
                i2 = x.f(1, getCtype().d_()) + 0;
            }
            if (hasPacked()) {
                getPacked();
                i2 += x.g(2) + 1;
            }
            if (hasDeprecated()) {
                getDeprecated();
                i2 += x.g(3) + 1;
            }
            if (hasExperimentalMapKey()) {
                i2 += x.b(9, getExperimentalMapKey());
            }
            Iterator<UninterpretedOption> it = getUninterpretedOptionList().iterator();
            while (true) {
                int i3 = i2;
                if (it.hasNext()) {
                    i2 = x.d(999, it.next()) + i3;
                } else {
                    int extensionsSerializedSize = extensionsSerializedSize() + i3 + getUnknownFields().getSerializedSize();
                    this.memoizedSerializedSize = extensionsSerializedSize;
                    return extensionsSerializedSize;
                }
            }
        }

        public static FieldOptions parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.FieldOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FieldOptions$Builder
          com.google.protobuf.DescriptorProtos.FieldOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.FieldOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FieldOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.FieldOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static FieldOptions parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static FieldOptions parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.FieldOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FieldOptions$Builder
          com.google.protobuf.DescriptorProtos.FieldOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.FieldOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FieldOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.FieldOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static FieldOptions parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static FieldOptions parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.FieldOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FieldOptions$Builder
          com.google.protobuf.DescriptorProtos.FieldOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.FieldOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FieldOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.FieldOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static FieldOptions parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static FieldOptions parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static FieldOptions parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static FieldOptions parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.DescriptorProtos.FieldOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FieldOptions$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.FieldOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.FieldOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FieldOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.FieldOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.FieldOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$FieldOptions$Builder */
        public static FieldOptions parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(FieldOptions fieldOptions) {
            return newBuilder().mergeFrom(fieldOptions);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.ExtendableBuilder<FieldOptions, Builder> {
            private FieldOptions result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new FieldOptions((r) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final FieldOptions internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new FieldOptions((r) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return FieldOptions.getDescriptor();
            }

            public final FieldOptions getDefaultInstanceForType() {
                return FieldOptions.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final FieldOptions build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public FieldOptions buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final FieldOptions buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.uninterpretedOption_ != Collections.EMPTY_LIST) {
                    List unused = this.result.uninterpretedOption_ = Collections.unmodifiableList(this.result.uninterpretedOption_);
                }
                FieldOptions fieldOptions = this.result;
                this.result = null;
                return fieldOptions;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof FieldOptions) {
                    return mergeFrom((FieldOptions) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(FieldOptions fieldOptions) {
                if (fieldOptions != FieldOptions.getDefaultInstance()) {
                    if (fieldOptions.hasCtype()) {
                        setCtype(fieldOptions.getCtype());
                    }
                    if (fieldOptions.hasPacked()) {
                        setPacked(fieldOptions.getPacked());
                    }
                    if (fieldOptions.hasDeprecated()) {
                        setDeprecated(fieldOptions.getDeprecated());
                    }
                    if (fieldOptions.hasExperimentalMapKey()) {
                        setExperimentalMapKey(fieldOptions.getExperimentalMapKey());
                    }
                    if (!fieldOptions.uninterpretedOption_.isEmpty()) {
                        if (this.result.uninterpretedOption_.isEmpty()) {
                            List unused = this.result.uninterpretedOption_ = new ArrayList();
                        }
                        this.result.uninterpretedOption_.addAll(fieldOptions.uninterpretedOption_);
                    }
                    mergeExtensionFields(fieldOptions);
                    mergeUnknownFields(fieldOptions.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 8:
                            int m = hVar.m();
                            a a3 = a.a(m);
                            if (a3 != null) {
                                setCtype(a3);
                                break;
                            } else {
                                a.a(1, m);
                                break;
                            }
                        case 16:
                            setPacked(hVar.i());
                            break;
                        case 24:
                            setDeprecated(hVar.i());
                            break;
                        case 74:
                            setExperimentalMapKey(hVar.j());
                            break;
                        case 7994:
                            UninterpretedOption.Builder newBuilder = UninterpretedOption.newBuilder();
                            hVar.a(newBuilder, abVar);
                            addUninterpretedOption(newBuilder.buildPartial());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final boolean hasCtype() {
                return this.result.hasCtype();
            }

            public final a getCtype() {
                return this.result.getCtype();
            }

            public final Builder setCtype(a aVar) {
                if (aVar == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasCtype = true;
                a unused2 = this.result.ctype_ = aVar;
                return this;
            }

            public final Builder clearCtype() {
                boolean unused = this.result.hasCtype = false;
                a unused2 = this.result.ctype_ = a.STRING;
                return this;
            }

            public final boolean hasPacked() {
                return this.result.hasPacked();
            }

            public final boolean getPacked() {
                return this.result.getPacked();
            }

            public final Builder setPacked(boolean z) {
                boolean unused = this.result.hasPacked = true;
                boolean unused2 = this.result.packed_ = z;
                return this;
            }

            public final Builder clearPacked() {
                boolean unused = this.result.hasPacked = false;
                boolean unused2 = this.result.packed_ = false;
                return this;
            }

            public final boolean hasDeprecated() {
                return this.result.hasDeprecated();
            }

            public final boolean getDeprecated() {
                return this.result.getDeprecated();
            }

            public final Builder setDeprecated(boolean z) {
                boolean unused = this.result.hasDeprecated = true;
                boolean unused2 = this.result.deprecated_ = z;
                return this;
            }

            public final Builder clearDeprecated() {
                boolean unused = this.result.hasDeprecated = false;
                boolean unused2 = this.result.deprecated_ = false;
                return this;
            }

            public final boolean hasExperimentalMapKey() {
                return this.result.hasExperimentalMapKey();
            }

            public final String getExperimentalMapKey() {
                return this.result.getExperimentalMapKey();
            }

            public final Builder setExperimentalMapKey(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasExperimentalMapKey = true;
                String unused2 = this.result.experimentalMapKey_ = str;
                return this;
            }

            public final Builder clearExperimentalMapKey() {
                boolean unused = this.result.hasExperimentalMapKey = false;
                String unused2 = this.result.experimentalMapKey_ = FieldOptions.getDefaultInstance().getExperimentalMapKey();
                return this;
            }

            public final List<UninterpretedOption> getUninterpretedOptionList() {
                return Collections.unmodifiableList(this.result.uninterpretedOption_);
            }

            public final int getUninterpretedOptionCount() {
                return this.result.getUninterpretedOptionCount();
            }

            public final UninterpretedOption getUninterpretedOption(int i) {
                return this.result.getUninterpretedOption(i);
            }

            public final Builder setUninterpretedOption(int i, UninterpretedOption uninterpretedOption) {
                if (uninterpretedOption == null) {
                    throw new NullPointerException();
                }
                this.result.uninterpretedOption_.set(i, uninterpretedOption);
                return this;
            }

            public final Builder setUninterpretedOption(int i, UninterpretedOption.Builder builder) {
                this.result.uninterpretedOption_.set(i, builder.build());
                return this;
            }

            public final Builder addUninterpretedOption(UninterpretedOption uninterpretedOption) {
                if (uninterpretedOption == null) {
                    throw new NullPointerException();
                }
                if (this.result.uninterpretedOption_.isEmpty()) {
                    List unused = this.result.uninterpretedOption_ = new ArrayList();
                }
                this.result.uninterpretedOption_.add(uninterpretedOption);
                return this;
            }

            public final Builder addUninterpretedOption(UninterpretedOption.Builder builder) {
                if (this.result.uninterpretedOption_.isEmpty()) {
                    List unused = this.result.uninterpretedOption_ = new ArrayList();
                }
                this.result.uninterpretedOption_.add(builder.build());
                return this;
            }

            public final Builder addAllUninterpretedOption(Iterable<? extends UninterpretedOption> iterable) {
                if (this.result.uninterpretedOption_.isEmpty()) {
                    List unused = this.result.uninterpretedOption_ = new ArrayList();
                }
                GeneratedMessage.ExtendableBuilder.addAll(iterable, this.result.uninterpretedOption_);
                return this;
            }

            public final Builder clearUninterpretedOption() {
                List unused = this.result.uninterpretedOption_ = Collections.emptyList();
                return this;
            }
        }

        static {
            FieldOptions fieldOptions = new FieldOptions(true);
            defaultInstance = fieldOptions;
            fieldOptions.ctype_ = a.STRING;
        }
    }

    public static final class EnumOptions extends GeneratedMessage.ExtendableMessage<EnumOptions> {
        public static final int UNINTERPRETED_OPTION_FIELD_NUMBER = 999;
        private static final EnumOptions defaultInstance = new EnumOptions(true);
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public List<UninterpretedOption> uninterpretedOption_;

        /* synthetic */ EnumOptions(r rVar) {
            this();
        }

        private EnumOptions() {
            this.uninterpretedOption_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        private EnumOptions(boolean z) {
            this.uninterpretedOption_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        public static EnumOptions getDefaultInstance() {
            return defaultInstance;
        }

        public final EnumOptions getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return DescriptorProtos.y;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return DescriptorProtos.z;
        }

        public final List<UninterpretedOption> getUninterpretedOptionList() {
            return this.uninterpretedOption_;
        }

        public final int getUninterpretedOptionCount() {
            return this.uninterpretedOption_.size();
        }

        public final UninterpretedOption getUninterpretedOption(int i) {
            return this.uninterpretedOption_.get(i);
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            for (UninterpretedOption isInitialized : getUninterpretedOptionList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            if (!extensionsAreInitialized()) {
                return false;
            }
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            GeneratedMessage.ExtendableMessage<MessageType>.a newExtensionWriter = newExtensionWriter();
            for (UninterpretedOption b : getUninterpretedOptionList()) {
                xVar.b(999, b);
            }
            newExtensionWriter.a(xVar);
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            Iterator<UninterpretedOption> it = getUninterpretedOptionList().iterator();
            while (true) {
                int i3 = i2;
                if (it.hasNext()) {
                    i2 = x.d(999, it.next()) + i3;
                } else {
                    int extensionsSerializedSize = extensionsSerializedSize() + i3 + getUnknownFields().getSerializedSize();
                    this.memoizedSerializedSize = extensionsSerializedSize;
                    return extensionsSerializedSize;
                }
            }
        }

        public static EnumOptions parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.EnumOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$EnumOptions$Builder
          com.google.protobuf.DescriptorProtos.EnumOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.EnumOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.EnumOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.EnumOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static EnumOptions parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static EnumOptions parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.EnumOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$EnumOptions$Builder
          com.google.protobuf.DescriptorProtos.EnumOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.EnumOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.EnumOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.EnumOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static EnumOptions parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static EnumOptions parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.EnumOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$EnumOptions$Builder
          com.google.protobuf.DescriptorProtos.EnumOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.EnumOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.EnumOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.EnumOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static EnumOptions parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static EnumOptions parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static EnumOptions parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static EnumOptions parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.DescriptorProtos.EnumOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$EnumOptions$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.EnumOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.EnumOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.EnumOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.EnumOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.EnumOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$EnumOptions$Builder */
        public static EnumOptions parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(EnumOptions enumOptions) {
            return newBuilder().mergeFrom(enumOptions);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.ExtendableBuilder<EnumOptions, Builder> {
            private EnumOptions result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new EnumOptions((r) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final EnumOptions internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new EnumOptions((r) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return EnumOptions.getDescriptor();
            }

            public final EnumOptions getDefaultInstanceForType() {
                return EnumOptions.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final EnumOptions build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public EnumOptions buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final EnumOptions buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.uninterpretedOption_ != Collections.EMPTY_LIST) {
                    List unused = this.result.uninterpretedOption_ = Collections.unmodifiableList(this.result.uninterpretedOption_);
                }
                EnumOptions enumOptions = this.result;
                this.result = null;
                return enumOptions;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof EnumOptions) {
                    return mergeFrom((EnumOptions) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(EnumOptions enumOptions) {
                if (enumOptions != EnumOptions.getDefaultInstance()) {
                    if (!enumOptions.uninterpretedOption_.isEmpty()) {
                        if (this.result.uninterpretedOption_.isEmpty()) {
                            List unused = this.result.uninterpretedOption_ = new ArrayList();
                        }
                        this.result.uninterpretedOption_.addAll(enumOptions.uninterpretedOption_);
                    }
                    mergeExtensionFields(enumOptions);
                    mergeUnknownFields(enumOptions.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 7994:
                            UninterpretedOption.Builder newBuilder = UninterpretedOption.newBuilder();
                            hVar.a(newBuilder, abVar);
                            addUninterpretedOption(newBuilder.buildPartial());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final List<UninterpretedOption> getUninterpretedOptionList() {
                return Collections.unmodifiableList(this.result.uninterpretedOption_);
            }

            public final int getUninterpretedOptionCount() {
                return this.result.getUninterpretedOptionCount();
            }

            public final UninterpretedOption getUninterpretedOption(int i) {
                return this.result.getUninterpretedOption(i);
            }

            public final Builder setUninterpretedOption(int i, UninterpretedOption uninterpretedOption) {
                if (uninterpretedOption == null) {
                    throw new NullPointerException();
                }
                this.result.uninterpretedOption_.set(i, uninterpretedOption);
                return this;
            }

            public final Builder setUninterpretedOption(int i, UninterpretedOption.Builder builder) {
                this.result.uninterpretedOption_.set(i, builder.build());
                return this;
            }

            public final Builder addUninterpretedOption(UninterpretedOption uninterpretedOption) {
                if (uninterpretedOption == null) {
                    throw new NullPointerException();
                }
                if (this.result.uninterpretedOption_.isEmpty()) {
                    List unused = this.result.uninterpretedOption_ = new ArrayList();
                }
                this.result.uninterpretedOption_.add(uninterpretedOption);
                return this;
            }

            public final Builder addUninterpretedOption(UninterpretedOption.Builder builder) {
                if (this.result.uninterpretedOption_.isEmpty()) {
                    List unused = this.result.uninterpretedOption_ = new ArrayList();
                }
                this.result.uninterpretedOption_.add(builder.build());
                return this;
            }

            public final Builder addAllUninterpretedOption(Iterable<? extends UninterpretedOption> iterable) {
                if (this.result.uninterpretedOption_.isEmpty()) {
                    List unused = this.result.uninterpretedOption_ = new ArrayList();
                }
                GeneratedMessage.ExtendableBuilder.addAll(iterable, this.result.uninterpretedOption_);
                return this;
            }

            public final Builder clearUninterpretedOption() {
                List unused = this.result.uninterpretedOption_ = Collections.emptyList();
                return this;
            }
        }
    }

    public static final class EnumValueOptions extends GeneratedMessage.ExtendableMessage<EnumValueOptions> {
        public static final int UNINTERPRETED_OPTION_FIELD_NUMBER = 999;
        private static final EnumValueOptions defaultInstance = new EnumValueOptions(true);
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public List<UninterpretedOption> uninterpretedOption_;

        /* synthetic */ EnumValueOptions(r rVar) {
            this();
        }

        private EnumValueOptions() {
            this.uninterpretedOption_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        private EnumValueOptions(boolean z) {
            this.uninterpretedOption_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        public static EnumValueOptions getDefaultInstance() {
            return defaultInstance;
        }

        public final EnumValueOptions getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return DescriptorProtos.A;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return DescriptorProtos.B;
        }

        public final List<UninterpretedOption> getUninterpretedOptionList() {
            return this.uninterpretedOption_;
        }

        public final int getUninterpretedOptionCount() {
            return this.uninterpretedOption_.size();
        }

        public final UninterpretedOption getUninterpretedOption(int i) {
            return this.uninterpretedOption_.get(i);
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            for (UninterpretedOption isInitialized : getUninterpretedOptionList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            if (!extensionsAreInitialized()) {
                return false;
            }
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            GeneratedMessage.ExtendableMessage<MessageType>.a newExtensionWriter = newExtensionWriter();
            for (UninterpretedOption b : getUninterpretedOptionList()) {
                xVar.b(999, b);
            }
            newExtensionWriter.a(xVar);
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            Iterator<UninterpretedOption> it = getUninterpretedOptionList().iterator();
            while (true) {
                int i3 = i2;
                if (it.hasNext()) {
                    i2 = x.d(999, it.next()) + i3;
                } else {
                    int extensionsSerializedSize = extensionsSerializedSize() + i3 + getUnknownFields().getSerializedSize();
                    this.memoizedSerializedSize = extensionsSerializedSize;
                    return extensionsSerializedSize;
                }
            }
        }

        public static EnumValueOptions parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.EnumValueOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$EnumValueOptions$Builder
          com.google.protobuf.DescriptorProtos.EnumValueOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.EnumValueOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.EnumValueOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.EnumValueOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static EnumValueOptions parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static EnumValueOptions parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.EnumValueOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$EnumValueOptions$Builder
          com.google.protobuf.DescriptorProtos.EnumValueOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.EnumValueOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.EnumValueOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.EnumValueOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static EnumValueOptions parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static EnumValueOptions parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.EnumValueOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$EnumValueOptions$Builder
          com.google.protobuf.DescriptorProtos.EnumValueOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.EnumValueOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.EnumValueOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.EnumValueOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static EnumValueOptions parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static EnumValueOptions parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static EnumValueOptions parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static EnumValueOptions parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.DescriptorProtos.EnumValueOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$EnumValueOptions$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.EnumValueOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.EnumValueOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.EnumValueOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.EnumValueOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.EnumValueOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$EnumValueOptions$Builder */
        public static EnumValueOptions parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(EnumValueOptions enumValueOptions) {
            return newBuilder().mergeFrom(enumValueOptions);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.ExtendableBuilder<EnumValueOptions, Builder> {
            private EnumValueOptions result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new EnumValueOptions((r) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final EnumValueOptions internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new EnumValueOptions((r) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return EnumValueOptions.getDescriptor();
            }

            public final EnumValueOptions getDefaultInstanceForType() {
                return EnumValueOptions.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final EnumValueOptions build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public EnumValueOptions buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final EnumValueOptions buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.uninterpretedOption_ != Collections.EMPTY_LIST) {
                    List unused = this.result.uninterpretedOption_ = Collections.unmodifiableList(this.result.uninterpretedOption_);
                }
                EnumValueOptions enumValueOptions = this.result;
                this.result = null;
                return enumValueOptions;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof EnumValueOptions) {
                    return mergeFrom((EnumValueOptions) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(EnumValueOptions enumValueOptions) {
                if (enumValueOptions != EnumValueOptions.getDefaultInstance()) {
                    if (!enumValueOptions.uninterpretedOption_.isEmpty()) {
                        if (this.result.uninterpretedOption_.isEmpty()) {
                            List unused = this.result.uninterpretedOption_ = new ArrayList();
                        }
                        this.result.uninterpretedOption_.addAll(enumValueOptions.uninterpretedOption_);
                    }
                    mergeExtensionFields(enumValueOptions);
                    mergeUnknownFields(enumValueOptions.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 7994:
                            UninterpretedOption.Builder newBuilder = UninterpretedOption.newBuilder();
                            hVar.a(newBuilder, abVar);
                            addUninterpretedOption(newBuilder.buildPartial());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final List<UninterpretedOption> getUninterpretedOptionList() {
                return Collections.unmodifiableList(this.result.uninterpretedOption_);
            }

            public final int getUninterpretedOptionCount() {
                return this.result.getUninterpretedOptionCount();
            }

            public final UninterpretedOption getUninterpretedOption(int i) {
                return this.result.getUninterpretedOption(i);
            }

            public final Builder setUninterpretedOption(int i, UninterpretedOption uninterpretedOption) {
                if (uninterpretedOption == null) {
                    throw new NullPointerException();
                }
                this.result.uninterpretedOption_.set(i, uninterpretedOption);
                return this;
            }

            public final Builder setUninterpretedOption(int i, UninterpretedOption.Builder builder) {
                this.result.uninterpretedOption_.set(i, builder.build());
                return this;
            }

            public final Builder addUninterpretedOption(UninterpretedOption uninterpretedOption) {
                if (uninterpretedOption == null) {
                    throw new NullPointerException();
                }
                if (this.result.uninterpretedOption_.isEmpty()) {
                    List unused = this.result.uninterpretedOption_ = new ArrayList();
                }
                this.result.uninterpretedOption_.add(uninterpretedOption);
                return this;
            }

            public final Builder addUninterpretedOption(UninterpretedOption.Builder builder) {
                if (this.result.uninterpretedOption_.isEmpty()) {
                    List unused = this.result.uninterpretedOption_ = new ArrayList();
                }
                this.result.uninterpretedOption_.add(builder.build());
                return this;
            }

            public final Builder addAllUninterpretedOption(Iterable<? extends UninterpretedOption> iterable) {
                if (this.result.uninterpretedOption_.isEmpty()) {
                    List unused = this.result.uninterpretedOption_ = new ArrayList();
                }
                GeneratedMessage.ExtendableBuilder.addAll(iterable, this.result.uninterpretedOption_);
                return this;
            }

            public final Builder clearUninterpretedOption() {
                List unused = this.result.uninterpretedOption_ = Collections.emptyList();
                return this;
            }
        }
    }

    public static final class ServiceOptions extends GeneratedMessage.ExtendableMessage<ServiceOptions> {
        public static final int UNINTERPRETED_OPTION_FIELD_NUMBER = 999;
        private static final ServiceOptions defaultInstance = new ServiceOptions(true);
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public List<UninterpretedOption> uninterpretedOption_;

        /* synthetic */ ServiceOptions(r rVar) {
            this();
        }

        private ServiceOptions() {
            this.uninterpretedOption_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        private ServiceOptions(boolean z) {
            this.uninterpretedOption_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        public static ServiceOptions getDefaultInstance() {
            return defaultInstance;
        }

        public final ServiceOptions getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return DescriptorProtos.C;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return DescriptorProtos.D;
        }

        public final List<UninterpretedOption> getUninterpretedOptionList() {
            return this.uninterpretedOption_;
        }

        public final int getUninterpretedOptionCount() {
            return this.uninterpretedOption_.size();
        }

        public final UninterpretedOption getUninterpretedOption(int i) {
            return this.uninterpretedOption_.get(i);
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            for (UninterpretedOption isInitialized : getUninterpretedOptionList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            if (!extensionsAreInitialized()) {
                return false;
            }
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            GeneratedMessage.ExtendableMessage<MessageType>.a newExtensionWriter = newExtensionWriter();
            for (UninterpretedOption b : getUninterpretedOptionList()) {
                xVar.b(999, b);
            }
            newExtensionWriter.a(xVar);
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            Iterator<UninterpretedOption> it = getUninterpretedOptionList().iterator();
            while (true) {
                int i3 = i2;
                if (it.hasNext()) {
                    i2 = x.d(999, it.next()) + i3;
                } else {
                    int extensionsSerializedSize = extensionsSerializedSize() + i3 + getUnknownFields().getSerializedSize();
                    this.memoizedSerializedSize = extensionsSerializedSize;
                    return extensionsSerializedSize;
                }
            }
        }

        public static ServiceOptions parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.ServiceOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$ServiceOptions$Builder
          com.google.protobuf.DescriptorProtos.ServiceOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.ServiceOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.ServiceOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.ServiceOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static ServiceOptions parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static ServiceOptions parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.ServiceOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$ServiceOptions$Builder
          com.google.protobuf.DescriptorProtos.ServiceOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.ServiceOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.ServiceOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.ServiceOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static ServiceOptions parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static ServiceOptions parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.ServiceOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$ServiceOptions$Builder
          com.google.protobuf.DescriptorProtos.ServiceOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.ServiceOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.ServiceOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.ServiceOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static ServiceOptions parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static ServiceOptions parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static ServiceOptions parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static ServiceOptions parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.DescriptorProtos.ServiceOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$ServiceOptions$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.ServiceOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.ServiceOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.ServiceOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.ServiceOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.ServiceOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$ServiceOptions$Builder */
        public static ServiceOptions parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(ServiceOptions serviceOptions) {
            return newBuilder().mergeFrom(serviceOptions);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.ExtendableBuilder<ServiceOptions, Builder> {
            private ServiceOptions result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new ServiceOptions((r) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final ServiceOptions internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new ServiceOptions((r) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return ServiceOptions.getDescriptor();
            }

            public final ServiceOptions getDefaultInstanceForType() {
                return ServiceOptions.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final ServiceOptions build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public ServiceOptions buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final ServiceOptions buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.uninterpretedOption_ != Collections.EMPTY_LIST) {
                    List unused = this.result.uninterpretedOption_ = Collections.unmodifiableList(this.result.uninterpretedOption_);
                }
                ServiceOptions serviceOptions = this.result;
                this.result = null;
                return serviceOptions;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof ServiceOptions) {
                    return mergeFrom((ServiceOptions) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(ServiceOptions serviceOptions) {
                if (serviceOptions != ServiceOptions.getDefaultInstance()) {
                    if (!serviceOptions.uninterpretedOption_.isEmpty()) {
                        if (this.result.uninterpretedOption_.isEmpty()) {
                            List unused = this.result.uninterpretedOption_ = new ArrayList();
                        }
                        this.result.uninterpretedOption_.addAll(serviceOptions.uninterpretedOption_);
                    }
                    mergeExtensionFields(serviceOptions);
                    mergeUnknownFields(serviceOptions.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 7994:
                            UninterpretedOption.Builder newBuilder = UninterpretedOption.newBuilder();
                            hVar.a(newBuilder, abVar);
                            addUninterpretedOption(newBuilder.buildPartial());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final List<UninterpretedOption> getUninterpretedOptionList() {
                return Collections.unmodifiableList(this.result.uninterpretedOption_);
            }

            public final int getUninterpretedOptionCount() {
                return this.result.getUninterpretedOptionCount();
            }

            public final UninterpretedOption getUninterpretedOption(int i) {
                return this.result.getUninterpretedOption(i);
            }

            public final Builder setUninterpretedOption(int i, UninterpretedOption uninterpretedOption) {
                if (uninterpretedOption == null) {
                    throw new NullPointerException();
                }
                this.result.uninterpretedOption_.set(i, uninterpretedOption);
                return this;
            }

            public final Builder setUninterpretedOption(int i, UninterpretedOption.Builder builder) {
                this.result.uninterpretedOption_.set(i, builder.build());
                return this;
            }

            public final Builder addUninterpretedOption(UninterpretedOption uninterpretedOption) {
                if (uninterpretedOption == null) {
                    throw new NullPointerException();
                }
                if (this.result.uninterpretedOption_.isEmpty()) {
                    List unused = this.result.uninterpretedOption_ = new ArrayList();
                }
                this.result.uninterpretedOption_.add(uninterpretedOption);
                return this;
            }

            public final Builder addUninterpretedOption(UninterpretedOption.Builder builder) {
                if (this.result.uninterpretedOption_.isEmpty()) {
                    List unused = this.result.uninterpretedOption_ = new ArrayList();
                }
                this.result.uninterpretedOption_.add(builder.build());
                return this;
            }

            public final Builder addAllUninterpretedOption(Iterable<? extends UninterpretedOption> iterable) {
                if (this.result.uninterpretedOption_.isEmpty()) {
                    List unused = this.result.uninterpretedOption_ = new ArrayList();
                }
                GeneratedMessage.ExtendableBuilder.addAll(iterable, this.result.uninterpretedOption_);
                return this;
            }

            public final Builder clearUninterpretedOption() {
                List unused = this.result.uninterpretedOption_ = Collections.emptyList();
                return this;
            }
        }
    }

    public static final class MethodOptions extends GeneratedMessage.ExtendableMessage<MethodOptions> {
        public static final int UNINTERPRETED_OPTION_FIELD_NUMBER = 999;
        private static final MethodOptions defaultInstance = new MethodOptions(true);
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public List<UninterpretedOption> uninterpretedOption_;

        /* synthetic */ MethodOptions(r rVar) {
            this();
        }

        private MethodOptions() {
            this.uninterpretedOption_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        private MethodOptions(boolean z) {
            this.uninterpretedOption_ = Collections.emptyList();
            this.memoizedSerializedSize = -1;
        }

        public static MethodOptions getDefaultInstance() {
            return defaultInstance;
        }

        public final MethodOptions getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return DescriptorProtos.E;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return DescriptorProtos.F;
        }

        public final List<UninterpretedOption> getUninterpretedOptionList() {
            return this.uninterpretedOption_;
        }

        public final int getUninterpretedOptionCount() {
            return this.uninterpretedOption_.size();
        }

        public final UninterpretedOption getUninterpretedOption(int i) {
            return this.uninterpretedOption_.get(i);
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            for (UninterpretedOption isInitialized : getUninterpretedOptionList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            if (!extensionsAreInitialized()) {
                return false;
            }
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            GeneratedMessage.ExtendableMessage<MessageType>.a newExtensionWriter = newExtensionWriter();
            for (UninterpretedOption b : getUninterpretedOptionList()) {
                xVar.b(999, b);
            }
            newExtensionWriter.a(xVar);
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i = this.memoizedSerializedSize;
            if (i != -1) {
                return i;
            }
            int i2 = 0;
            Iterator<UninterpretedOption> it = getUninterpretedOptionList().iterator();
            while (true) {
                int i3 = i2;
                if (it.hasNext()) {
                    i2 = x.d(999, it.next()) + i3;
                } else {
                    int extensionsSerializedSize = extensionsSerializedSize() + i3 + getUnknownFields().getSerializedSize();
                    this.memoizedSerializedSize = extensionsSerializedSize;
                    return extensionsSerializedSize;
                }
            }
        }

        public static MethodOptions parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.MethodOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$MethodOptions$Builder
          com.google.protobuf.DescriptorProtos.MethodOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.MethodOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.MethodOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.MethodOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static MethodOptions parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static MethodOptions parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.MethodOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$MethodOptions$Builder
          com.google.protobuf.DescriptorProtos.MethodOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.MethodOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.MethodOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.MethodOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static MethodOptions parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static MethodOptions parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.MethodOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$MethodOptions$Builder
          com.google.protobuf.DescriptorProtos.MethodOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.MethodOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.MethodOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.MethodOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static MethodOptions parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static MethodOptions parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static MethodOptions parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static MethodOptions parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.DescriptorProtos.MethodOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$MethodOptions$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.MethodOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.MethodOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.MethodOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.MethodOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.MethodOptions.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$MethodOptions$Builder */
        public static MethodOptions parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(MethodOptions methodOptions) {
            return newBuilder().mergeFrom(methodOptions);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.ExtendableBuilder<MethodOptions, Builder> {
            private MethodOptions result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new MethodOptions((r) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final MethodOptions internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new MethodOptions((r) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return MethodOptions.getDescriptor();
            }

            public final MethodOptions getDefaultInstanceForType() {
                return MethodOptions.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final MethodOptions build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public MethodOptions buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final MethodOptions buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.uninterpretedOption_ != Collections.EMPTY_LIST) {
                    List unused = this.result.uninterpretedOption_ = Collections.unmodifiableList(this.result.uninterpretedOption_);
                }
                MethodOptions methodOptions = this.result;
                this.result = null;
                return methodOptions;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof MethodOptions) {
                    return mergeFrom((MethodOptions) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(MethodOptions methodOptions) {
                if (methodOptions != MethodOptions.getDefaultInstance()) {
                    if (!methodOptions.uninterpretedOption_.isEmpty()) {
                        if (this.result.uninterpretedOption_.isEmpty()) {
                            List unused = this.result.uninterpretedOption_ = new ArrayList();
                        }
                        this.result.uninterpretedOption_.addAll(methodOptions.uninterpretedOption_);
                    }
                    mergeExtensionFields(methodOptions);
                    mergeUnknownFields(methodOptions.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case 7994:
                            UninterpretedOption.Builder newBuilder = UninterpretedOption.newBuilder();
                            hVar.a(newBuilder, abVar);
                            addUninterpretedOption(newBuilder.buildPartial());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final List<UninterpretedOption> getUninterpretedOptionList() {
                return Collections.unmodifiableList(this.result.uninterpretedOption_);
            }

            public final int getUninterpretedOptionCount() {
                return this.result.getUninterpretedOptionCount();
            }

            public final UninterpretedOption getUninterpretedOption(int i) {
                return this.result.getUninterpretedOption(i);
            }

            public final Builder setUninterpretedOption(int i, UninterpretedOption uninterpretedOption) {
                if (uninterpretedOption == null) {
                    throw new NullPointerException();
                }
                this.result.uninterpretedOption_.set(i, uninterpretedOption);
                return this;
            }

            public final Builder setUninterpretedOption(int i, UninterpretedOption.Builder builder) {
                this.result.uninterpretedOption_.set(i, builder.build());
                return this;
            }

            public final Builder addUninterpretedOption(UninterpretedOption uninterpretedOption) {
                if (uninterpretedOption == null) {
                    throw new NullPointerException();
                }
                if (this.result.uninterpretedOption_.isEmpty()) {
                    List unused = this.result.uninterpretedOption_ = new ArrayList();
                }
                this.result.uninterpretedOption_.add(uninterpretedOption);
                return this;
            }

            public final Builder addUninterpretedOption(UninterpretedOption.Builder builder) {
                if (this.result.uninterpretedOption_.isEmpty()) {
                    List unused = this.result.uninterpretedOption_ = new ArrayList();
                }
                this.result.uninterpretedOption_.add(builder.build());
                return this;
            }

            public final Builder addAllUninterpretedOption(Iterable<? extends UninterpretedOption> iterable) {
                if (this.result.uninterpretedOption_.isEmpty()) {
                    List unused = this.result.uninterpretedOption_ = new ArrayList();
                }
                GeneratedMessage.ExtendableBuilder.addAll(iterable, this.result.uninterpretedOption_);
                return this;
            }

            public final Builder clearUninterpretedOption() {
                List unused = this.result.uninterpretedOption_ = Collections.emptyList();
                return this;
            }
        }
    }

    public static final class UninterpretedOption extends GeneratedMessage {
        public static final int DOUBLE_VALUE_FIELD_NUMBER = 6;
        public static final int IDENTIFIER_VALUE_FIELD_NUMBER = 3;
        public static final int NAME_FIELD_NUMBER = 2;
        public static final int NEGATIVE_INT_VALUE_FIELD_NUMBER = 5;
        public static final int POSITIVE_INT_VALUE_FIELD_NUMBER = 4;
        public static final int STRING_VALUE_FIELD_NUMBER = 7;
        private static final UninterpretedOption defaultInstance = new UninterpretedOption(true);
        /* access modifiers changed from: private */
        public double doubleValue_;
        /* access modifiers changed from: private */
        public boolean hasDoubleValue;
        /* access modifiers changed from: private */
        public boolean hasIdentifierValue;
        /* access modifiers changed from: private */
        public boolean hasNegativeIntValue;
        /* access modifiers changed from: private */
        public boolean hasPositiveIntValue;
        /* access modifiers changed from: private */
        public boolean hasStringValue;
        /* access modifiers changed from: private */
        public String identifierValue_;
        private int memoizedSerializedSize;
        /* access modifiers changed from: private */
        public List<NamePart> name_;
        /* access modifiers changed from: private */
        public long negativeIntValue_;
        /* access modifiers changed from: private */
        public long positiveIntValue_;
        /* access modifiers changed from: private */
        public q stringValue_;

        /* synthetic */ UninterpretedOption(r rVar) {
            this();
        }

        private UninterpretedOption() {
            this.name_ = Collections.emptyList();
            this.identifierValue_ = "";
            this.positiveIntValue_ = 0;
            this.negativeIntValue_ = 0;
            this.doubleValue_ = 0.0d;
            this.stringValue_ = q.a;
            this.memoizedSerializedSize = -1;
        }

        private UninterpretedOption(boolean z) {
            this.name_ = Collections.emptyList();
            this.identifierValue_ = "";
            this.positiveIntValue_ = 0;
            this.negativeIntValue_ = 0;
            this.doubleValue_ = 0.0d;
            this.stringValue_ = q.a;
            this.memoizedSerializedSize = -1;
        }

        public static UninterpretedOption getDefaultInstance() {
            return defaultInstance;
        }

        public final UninterpretedOption getDefaultInstanceForType() {
            return defaultInstance;
        }

        public static final c.a getDescriptor() {
            return DescriptorProtos.G;
        }

        /* access modifiers changed from: protected */
        public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
            return DescriptorProtos.H;
        }

        public static final class NamePart extends GeneratedMessage {
            public static final int IS_EXTENSION_FIELD_NUMBER = 2;
            public static final int NAME_PART_FIELD_NUMBER = 1;
            private static final NamePart defaultInstance = new NamePart(true);
            /* access modifiers changed from: private */
            public boolean hasIsExtension;
            /* access modifiers changed from: private */
            public boolean hasNamePart;
            /* access modifiers changed from: private */
            public boolean isExtension_;
            private int memoizedSerializedSize;
            /* access modifiers changed from: private */
            public String namePart_;

            /* synthetic */ NamePart(r rVar) {
                this();
            }

            private NamePart() {
                this.namePart_ = "";
                this.isExtension_ = false;
                this.memoizedSerializedSize = -1;
            }

            private NamePart(boolean z) {
                this.namePart_ = "";
                this.isExtension_ = false;
                this.memoizedSerializedSize = -1;
            }

            public static NamePart getDefaultInstance() {
                return defaultInstance;
            }

            public final NamePart getDefaultInstanceForType() {
                return defaultInstance;
            }

            public static final c.a getDescriptor() {
                return DescriptorProtos.I;
            }

            /* access modifiers changed from: protected */
            public final GeneratedMessage.FieldAccessorTable internalGetFieldAccessorTable() {
                return DescriptorProtos.J;
            }

            public final boolean hasNamePart() {
                return this.hasNamePart;
            }

            public final String getNamePart() {
                return this.namePart_;
            }

            public final boolean hasIsExtension() {
                return this.hasIsExtension;
            }

            public final boolean getIsExtension() {
                return this.isExtension_;
            }

            private void initFields() {
            }

            public final boolean isInitialized() {
                if (this.hasNamePart && this.hasIsExtension) {
                    return true;
                }
                return false;
            }

            public final void writeTo(x xVar) throws IOException {
                getSerializedSize();
                if (hasNamePart()) {
                    xVar.a(1, getNamePart());
                }
                if (hasIsExtension()) {
                    xVar.a(2, getIsExtension());
                }
                getUnknownFields().writeTo(xVar);
            }

            public final int getSerializedSize() {
                int i = this.memoizedSerializedSize;
                if (i != -1) {
                    return i;
                }
                int i2 = 0;
                if (hasNamePart()) {
                    i2 = x.b(1, getNamePart()) + 0;
                }
                if (hasIsExtension()) {
                    getIsExtension();
                    i2 += x.g(2) + 1;
                }
                int serializedSize = i2 + getUnknownFields().getSerializedSize();
                this.memoizedSerializedSize = serializedSize;
                return serializedSize;
            }

            public static NamePart parseFrom(q qVar) throws ac {
                return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
             arg types: [com.google.protobuf.q, com.google.protobuf.ab]
             candidates:
              com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$UninterpretedOption$NamePart$Builder
              com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
              com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
              com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
              com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
              com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
            public static NamePart parseFrom(q qVar, ab abVar) throws ac {
                return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
            }

            public static NamePart parseFrom(byte[] bArr) throws ac {
                return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
             arg types: [byte[], com.google.protobuf.ab]
             candidates:
              com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$UninterpretedOption$NamePart$Builder
              com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
              com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
              com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
              com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
              com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
            public static NamePart parseFrom(byte[] bArr, ab abVar) throws ac {
                return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
            }

            public static NamePart parseFrom(InputStream inputStream) throws IOException {
                return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
             arg types: [java.io.InputStream, com.google.protobuf.ab]
             candidates:
              com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$UninterpretedOption$NamePart$Builder
              com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
              com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
              com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
              com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
              com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
            public static NamePart parseFrom(InputStream inputStream, ab abVar) throws IOException {
                return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
            }

            public static NamePart parseDelimitedFrom(InputStream inputStream) throws IOException {
                Builder newBuilder = newBuilder();
                if (newBuilder.mergeDelimitedFrom(inputStream)) {
                    return newBuilder.buildParsed();
                }
                return null;
            }

            public static NamePart parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
                Builder newBuilder = newBuilder();
                if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                    return newBuilder.buildParsed();
                }
                return null;
            }

            public static NamePart parseFrom(h hVar) throws IOException {
                return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$UninterpretedOption$NamePart$Builder
             arg types: [com.google.protobuf.h, com.google.protobuf.ab]
             candidates:
              com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
              com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
              com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
              com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
              com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
              com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
              com.google.protobuf.DescriptorProtos.UninterpretedOption.NamePart.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$UninterpretedOption$NamePart$Builder */
            public static NamePart parseFrom(h hVar, ab abVar) throws IOException {
                return newBuilder().mergeFrom(hVar, abVar).buildParsed();
            }

            public static Builder newBuilder() {
                return Builder.create();
            }

            public final Builder newBuilderForType() {
                return newBuilder();
            }

            public static Builder newBuilder(NamePart namePart) {
                return newBuilder().mergeFrom(namePart);
            }

            public final Builder toBuilder() {
                return newBuilder(this);
            }

            public static final class Builder extends GeneratedMessage.b<Builder> {
                private NamePart result;

                private Builder() {
                }

                /* access modifiers changed from: private */
                public static Builder create() {
                    Builder builder = new Builder();
                    builder.result = new NamePart((r) null);
                    return builder;
                }

                /* access modifiers changed from: protected */
                public final NamePart internalGetResult() {
                    return this.result;
                }

                public final Builder clear() {
                    if (this.result == null) {
                        throw new IllegalStateException("Cannot call clear() after build().");
                    }
                    this.result = new NamePart((r) null);
                    return this;
                }

                public final Builder clone() {
                    return create().mergeFrom(this.result);
                }

                public final c.a getDescriptorForType() {
                    return NamePart.getDescriptor();
                }

                public final NamePart getDefaultInstanceForType() {
                    return NamePart.getDefaultInstance();
                }

                public final boolean isInitialized() {
                    return this.result.isInitialized();
                }

                public final NamePart build() {
                    if (this.result == null || isInitialized()) {
                        return buildPartial();
                    }
                    throw newUninitializedMessageException((s) this.result);
                }

                /* access modifiers changed from: private */
                public NamePart buildParsed() throws ac {
                    if (isInitialized()) {
                        return buildPartial();
                    }
                    throw newUninitializedMessageException((s) this.result).a();
                }

                public final NamePart buildPartial() {
                    if (this.result == null) {
                        throw new IllegalStateException("build() has already been called on this Builder.");
                    }
                    NamePart namePart = this.result;
                    this.result = null;
                    return namePart;
                }

                public final Builder mergeFrom(s sVar) {
                    if (sVar instanceof NamePart) {
                        return mergeFrom((NamePart) sVar);
                    }
                    super.mergeFrom(sVar);
                    return this;
                }

                public final Builder mergeFrom(NamePart namePart) {
                    if (namePart != NamePart.getDefaultInstance()) {
                        if (namePart.hasNamePart()) {
                            setNamePart(namePart.getNamePart());
                        }
                        if (namePart.hasIsExtension()) {
                            setIsExtension(namePart.getIsExtension());
                        }
                        mergeUnknownFields(namePart.getUnknownFields());
                    }
                    return this;
                }

                public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                    a.b a = a.a(getUnknownFields());
                    while (true) {
                        int a2 = hVar.a();
                        switch (a2) {
                            case Transform.POS_X:
                                setUnknownFields(a.build());
                                break;
                            case 10:
                                setNamePart(hVar.j());
                                break;
                            case 16:
                                setIsExtension(hVar.i());
                                break;
                            default:
                                if (parseUnknownField(hVar, a, abVar, a2)) {
                                    break;
                                } else {
                                    setUnknownFields(a.build());
                                    break;
                                }
                        }
                    }
                    return this;
                }

                public final boolean hasNamePart() {
                    return this.result.hasNamePart();
                }

                public final String getNamePart() {
                    return this.result.getNamePart();
                }

                public final Builder setNamePart(String str) {
                    if (str == null) {
                        throw new NullPointerException();
                    }
                    boolean unused = this.result.hasNamePart = true;
                    String unused2 = this.result.namePart_ = str;
                    return this;
                }

                public final Builder clearNamePart() {
                    boolean unused = this.result.hasNamePart = false;
                    String unused2 = this.result.namePart_ = NamePart.getDefaultInstance().getNamePart();
                    return this;
                }

                public final boolean hasIsExtension() {
                    return this.result.hasIsExtension();
                }

                public final boolean getIsExtension() {
                    return this.result.getIsExtension();
                }

                public final Builder setIsExtension(boolean z) {
                    boolean unused = this.result.hasIsExtension = true;
                    boolean unused2 = this.result.isExtension_ = z;
                    return this;
                }

                public final Builder clearIsExtension() {
                    boolean unused = this.result.hasIsExtension = false;
                    boolean unused2 = this.result.isExtension_ = false;
                    return this;
                }
            }
        }

        public final List<NamePart> getNameList() {
            return this.name_;
        }

        public final int getNameCount() {
            return this.name_.size();
        }

        public final NamePart getName(int i) {
            return this.name_.get(i);
        }

        public final boolean hasIdentifierValue() {
            return this.hasIdentifierValue;
        }

        public final String getIdentifierValue() {
            return this.identifierValue_;
        }

        public final boolean hasPositiveIntValue() {
            return this.hasPositiveIntValue;
        }

        public final long getPositiveIntValue() {
            return this.positiveIntValue_;
        }

        public final boolean hasNegativeIntValue() {
            return this.hasNegativeIntValue;
        }

        public final long getNegativeIntValue() {
            return this.negativeIntValue_;
        }

        public final boolean hasDoubleValue() {
            return this.hasDoubleValue;
        }

        public final double getDoubleValue() {
            return this.doubleValue_;
        }

        public final boolean hasStringValue() {
            return this.hasStringValue;
        }

        public final q getStringValue() {
            return this.stringValue_;
        }

        private void initFields() {
        }

        public final boolean isInitialized() {
            for (NamePart isInitialized : getNameList()) {
                if (!isInitialized.isInitialized()) {
                    return false;
                }
            }
            return true;
        }

        public final void writeTo(x xVar) throws IOException {
            getSerializedSize();
            for (NamePart b : getNameList()) {
                xVar.b(2, b);
            }
            if (hasIdentifierValue()) {
                xVar.a(3, getIdentifierValue());
            }
            if (hasPositiveIntValue()) {
                xVar.a(4, getPositiveIntValue());
            }
            if (hasNegativeIntValue()) {
                long negativeIntValue = getNegativeIntValue();
                xVar.g(5, 0);
                xVar.a(negativeIntValue);
            }
            if (hasDoubleValue()) {
                double doubleValue = getDoubleValue();
                xVar.g(6, 1);
                xVar.a(doubleValue);
            }
            if (hasStringValue()) {
                xVar.a(7, getStringValue());
            }
            getUnknownFields().writeTo(xVar);
        }

        public final int getSerializedSize() {
            int i;
            int i2 = this.memoizedSerializedSize;
            if (i2 != -1) {
                return i2;
            }
            int i3 = 0;
            Iterator<NamePart> it = getNameList().iterator();
            while (true) {
                i = i3;
                if (!it.hasNext()) {
                    break;
                }
                i3 = x.d(2, it.next()) + i;
            }
            if (hasIdentifierValue()) {
                i += x.b(3, getIdentifierValue());
            }
            if (hasPositiveIntValue()) {
                i += x.b(4, getPositiveIntValue());
            }
            if (hasNegativeIntValue()) {
                i += x.g(5) + x.b(getNegativeIntValue());
            }
            if (hasDoubleValue()) {
                getDoubleValue();
                i += x.g(6) + 8;
            }
            if (hasStringValue()) {
                i += x.b(7, getStringValue());
            }
            int serializedSize = getUnknownFields().getSerializedSize() + i;
            this.memoizedSerializedSize = serializedSize;
            return serializedSize;
        }

        public static UninterpretedOption parseFrom(q qVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.UninterpretedOption.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$UninterpretedOption$Builder
          com.google.protobuf.DescriptorProtos.UninterpretedOption.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.UninterpretedOption.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.UninterpretedOption.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.UninterpretedOption.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public static UninterpretedOption parseFrom(q qVar, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(qVar, abVar)).buildParsed();
        }

        public static UninterpretedOption parseFrom(byte[] bArr) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.UninterpretedOption.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$UninterpretedOption$Builder
          com.google.protobuf.DescriptorProtos.UninterpretedOption.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.UninterpretedOption.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.UninterpretedOption.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.UninterpretedOption.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public static UninterpretedOption parseFrom(byte[] bArr, ab abVar) throws ac {
            return ((Builder) newBuilder().mergeFrom(bArr, abVar)).buildParsed();
        }

        public static UninterpretedOption parseFrom(InputStream inputStream) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.UninterpretedOption.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$UninterpretedOption$Builder
          com.google.protobuf.DescriptorProtos.UninterpretedOption.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.UninterpretedOption.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.UninterpretedOption.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.UninterpretedOption.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public static UninterpretedOption parseFrom(InputStream inputStream, ab abVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(inputStream, abVar)).buildParsed();
        }

        public static UninterpretedOption parseDelimitedFrom(InputStream inputStream) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static UninterpretedOption parseDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            Builder newBuilder = newBuilder();
            if (newBuilder.mergeDelimitedFrom(inputStream, abVar)) {
                return newBuilder.buildParsed();
            }
            return null;
        }

        public static UninterpretedOption parseFrom(h hVar) throws IOException {
            return ((Builder) newBuilder().mergeFrom(hVar)).buildParsed();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.DescriptorProtos.UninterpretedOption.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$UninterpretedOption$Builder
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.DescriptorProtos.UninterpretedOption.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.ae$a
          com.google.protobuf.DescriptorProtos.UninterpretedOption.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.UninterpretedOption.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.DescriptorProtos.UninterpretedOption.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.DescriptorProtos.UninterpretedOption.Builder.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.DescriptorProtos$UninterpretedOption$Builder */
        public static UninterpretedOption parseFrom(h hVar, ab abVar) throws IOException {
            return newBuilder().mergeFrom(hVar, abVar).buildParsed();
        }

        public static Builder newBuilder() {
            return Builder.create();
        }

        public final Builder newBuilderForType() {
            return newBuilder();
        }

        public static Builder newBuilder(UninterpretedOption uninterpretedOption) {
            return newBuilder().mergeFrom(uninterpretedOption);
        }

        public final Builder toBuilder() {
            return newBuilder(this);
        }

        public static final class Builder extends GeneratedMessage.b<Builder> {
            private UninterpretedOption result;

            private Builder() {
            }

            /* access modifiers changed from: private */
            public static Builder create() {
                Builder builder = new Builder();
                builder.result = new UninterpretedOption((r) null);
                return builder;
            }

            /* access modifiers changed from: protected */
            public final UninterpretedOption internalGetResult() {
                return this.result;
            }

            public final Builder clear() {
                if (this.result == null) {
                    throw new IllegalStateException("Cannot call clear() after build().");
                }
                this.result = new UninterpretedOption((r) null);
                return this;
            }

            public final Builder clone() {
                return create().mergeFrom(this.result);
            }

            public final c.a getDescriptorForType() {
                return UninterpretedOption.getDescriptor();
            }

            public final UninterpretedOption getDefaultInstanceForType() {
                return UninterpretedOption.getDefaultInstance();
            }

            public final boolean isInitialized() {
                return this.result.isInitialized();
            }

            public final UninterpretedOption build() {
                if (this.result == null || isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result);
            }

            /* access modifiers changed from: private */
            public UninterpretedOption buildParsed() throws ac {
                if (isInitialized()) {
                    return buildPartial();
                }
                throw newUninitializedMessageException((s) this.result).a();
            }

            public final UninterpretedOption buildPartial() {
                if (this.result == null) {
                    throw new IllegalStateException("build() has already been called on this Builder.");
                }
                if (this.result.name_ != Collections.EMPTY_LIST) {
                    List unused = this.result.name_ = Collections.unmodifiableList(this.result.name_);
                }
                UninterpretedOption uninterpretedOption = this.result;
                this.result = null;
                return uninterpretedOption;
            }

            public final Builder mergeFrom(s sVar) {
                if (sVar instanceof UninterpretedOption) {
                    return mergeFrom((UninterpretedOption) sVar);
                }
                super.mergeFrom(sVar);
                return this;
            }

            public final Builder mergeFrom(UninterpretedOption uninterpretedOption) {
                if (uninterpretedOption != UninterpretedOption.getDefaultInstance()) {
                    if (!uninterpretedOption.name_.isEmpty()) {
                        if (this.result.name_.isEmpty()) {
                            List unused = this.result.name_ = new ArrayList();
                        }
                        this.result.name_.addAll(uninterpretedOption.name_);
                    }
                    if (uninterpretedOption.hasIdentifierValue()) {
                        setIdentifierValue(uninterpretedOption.getIdentifierValue());
                    }
                    if (uninterpretedOption.hasPositiveIntValue()) {
                        setPositiveIntValue(uninterpretedOption.getPositiveIntValue());
                    }
                    if (uninterpretedOption.hasNegativeIntValue()) {
                        setNegativeIntValue(uninterpretedOption.getNegativeIntValue());
                    }
                    if (uninterpretedOption.hasDoubleValue()) {
                        setDoubleValue(uninterpretedOption.getDoubleValue());
                    }
                    if (uninterpretedOption.hasStringValue()) {
                        setStringValue(uninterpretedOption.getStringValue());
                    }
                    mergeUnknownFields(uninterpretedOption.getUnknownFields());
                }
                return this;
            }

            public final Builder mergeFrom(h hVar, ab abVar) throws IOException {
                a.b a = a.a(getUnknownFields());
                while (true) {
                    int a2 = hVar.a();
                    switch (a2) {
                        case Transform.POS_X:
                            setUnknownFields(a.build());
                            break;
                        case FileOptions.PY_GENERIC_SERVICES_FIELD_NUMBER /*18*/:
                            NamePart.Builder newBuilder = NamePart.newBuilder();
                            hVar.a(newBuilder, abVar);
                            addName(newBuilder.buildPartial());
                            break;
                        case 26:
                            setIdentifierValue(hVar.j());
                            break;
                        case 32:
                            setPositiveIntValue(hVar.d());
                            break;
                        case 40:
                            setNegativeIntValue(hVar.e());
                            break;
                        case 49:
                            setDoubleValue(hVar.b());
                            break;
                        case 58:
                            setStringValue(hVar.k());
                            break;
                        default:
                            if (parseUnknownField(hVar, a, abVar, a2)) {
                                break;
                            } else {
                                setUnknownFields(a.build());
                                break;
                            }
                    }
                }
                return this;
            }

            public final List<NamePart> getNameList() {
                return Collections.unmodifiableList(this.result.name_);
            }

            public final int getNameCount() {
                return this.result.getNameCount();
            }

            public final NamePart getName(int i) {
                return this.result.getName(i);
            }

            public final Builder setName(int i, NamePart namePart) {
                if (namePart == null) {
                    throw new NullPointerException();
                }
                this.result.name_.set(i, namePart);
                return this;
            }

            public final Builder setName(int i, NamePart.Builder builder) {
                this.result.name_.set(i, builder.build());
                return this;
            }

            public final Builder addName(NamePart namePart) {
                if (namePart == null) {
                    throw new NullPointerException();
                }
                if (this.result.name_.isEmpty()) {
                    List unused = this.result.name_ = new ArrayList();
                }
                this.result.name_.add(namePart);
                return this;
            }

            public final Builder addName(NamePart.Builder builder) {
                if (this.result.name_.isEmpty()) {
                    List unused = this.result.name_ = new ArrayList();
                }
                this.result.name_.add(builder.build());
                return this;
            }

            public final Builder addAllName(Iterable<? extends NamePart> iterable) {
                if (this.result.name_.isEmpty()) {
                    List unused = this.result.name_ = new ArrayList();
                }
                GeneratedMessage.b.addAll(iterable, this.result.name_);
                return this;
            }

            public final Builder clearName() {
                List unused = this.result.name_ = Collections.emptyList();
                return this;
            }

            public final boolean hasIdentifierValue() {
                return this.result.hasIdentifierValue();
            }

            public final String getIdentifierValue() {
                return this.result.getIdentifierValue();
            }

            public final Builder setIdentifierValue(String str) {
                if (str == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasIdentifierValue = true;
                String unused2 = this.result.identifierValue_ = str;
                return this;
            }

            public final Builder clearIdentifierValue() {
                boolean unused = this.result.hasIdentifierValue = false;
                String unused2 = this.result.identifierValue_ = UninterpretedOption.getDefaultInstance().getIdentifierValue();
                return this;
            }

            public final boolean hasPositiveIntValue() {
                return this.result.hasPositiveIntValue();
            }

            public final long getPositiveIntValue() {
                return this.result.getPositiveIntValue();
            }

            public final Builder setPositiveIntValue(long j) {
                boolean unused = this.result.hasPositiveIntValue = true;
                long unused2 = this.result.positiveIntValue_ = j;
                return this;
            }

            public final Builder clearPositiveIntValue() {
                boolean unused = this.result.hasPositiveIntValue = false;
                long unused2 = this.result.positiveIntValue_ = 0;
                return this;
            }

            public final boolean hasNegativeIntValue() {
                return this.result.hasNegativeIntValue();
            }

            public final long getNegativeIntValue() {
                return this.result.getNegativeIntValue();
            }

            public final Builder setNegativeIntValue(long j) {
                boolean unused = this.result.hasNegativeIntValue = true;
                long unused2 = this.result.negativeIntValue_ = j;
                return this;
            }

            public final Builder clearNegativeIntValue() {
                boolean unused = this.result.hasNegativeIntValue = false;
                long unused2 = this.result.negativeIntValue_ = 0;
                return this;
            }

            public final boolean hasDoubleValue() {
                return this.result.hasDoubleValue();
            }

            public final double getDoubleValue() {
                return this.result.getDoubleValue();
            }

            public final Builder setDoubleValue(double d) {
                boolean unused = this.result.hasDoubleValue = true;
                double unused2 = this.result.doubleValue_ = d;
                return this;
            }

            public final Builder clearDoubleValue() {
                boolean unused = this.result.hasDoubleValue = false;
                double unused2 = this.result.doubleValue_ = 0.0d;
                return this;
            }

            public final boolean hasStringValue() {
                return this.result.hasStringValue();
            }

            public final q getStringValue() {
                return this.result.getStringValue();
            }

            public final Builder setStringValue(q qVar) {
                if (qVar == null) {
                    throw new NullPointerException();
                }
                boolean unused = this.result.hasStringValue = true;
                q unused2 = this.result.stringValue_ = qVar;
                return this;
            }

            public final Builder clearStringValue() {
                boolean unused = this.result.hasStringValue = false;
                q unused2 = this.result.stringValue_ = UninterpretedOption.getDefaultInstance().getStringValue();
                return this;
            }
        }
    }

    public static c.b a() {
        return K;
    }

    static {
        c.b.a(new String[]{"\n google/protobuf/descriptor.proto\u0012\u000fgoogle.protobuf\"G\n\u0011FileDescriptorSet\u00122\n\u0004file\u0018\u0001 \u0003(\u000b2$.google.protobuf.FileDescriptorProto\"Ü\u0002\n\u0013FileDescriptorProto\u0012\f\n\u0004name\u0018\u0001 \u0001(\t\u0012\u000f\n\u0007package\u0018\u0002 \u0001(\t\u0012\u0012\n\ndependency\u0018\u0003 \u0003(\t\u00126\n\fmessage_type\u0018\u0004 \u0003(\u000b2 .google.protobuf.DescriptorProto\u00127\n\tenum_type\u0018\u0005 \u0003(\u000b2$.google.protobuf.EnumDescriptorProto\u00128\n\u0007service\u0018\u0006 \u0003(\u000b2'.google.protobuf.ServiceDescriptorProto\u00128\n\textension\u0018\u0007 \u0003(\u000b2%.google.p", "rotobuf.FieldDescriptorProto\u0012-\n\u0007options\u0018\b \u0001(\u000b2\u001c.google.protobuf.FileOptions\"©\u0003\n\u000fDescriptorProto\u0012\f\n\u0004name\u0018\u0001 \u0001(\t\u00124\n\u0005field\u0018\u0002 \u0003(\u000b2%.google.protobuf.FieldDescriptorProto\u00128\n\textension\u0018\u0006 \u0003(\u000b2%.google.protobuf.FieldDescriptorProto\u00125\n\u000bnested_type\u0018\u0003 \u0003(\u000b2 .google.protobuf.DescriptorProto\u00127\n\tenum_type\u0018\u0004 \u0003(\u000b2$.google.protobuf.EnumDescriptorProto\u0012H\n\u000fextension_range\u0018\u0005 \u0003(\u000b2/.google.protobuf.DescriptorProto.Extensi", "onRange\u00120\n\u0007options\u0018\u0007 \u0001(\u000b2\u001f.google.protobuf.MessageOptions\u001a,\n\u000eExtensionRange\u0012\r\n\u0005start\u0018\u0001 \u0001(\u0005\u0012\u000b\n\u0003end\u0018\u0002 \u0001(\u0005\"\u0005\n\u0014FieldDescriptorProto\u0012\f\n\u0004name\u0018\u0001 \u0001(\t\u0012\u000e\n\u0006number\u0018\u0003 \u0001(\u0005\u0012:\n\u0005label\u0018\u0004 \u0001(\u000e2+.google.protobuf.FieldDescriptorProto.Label\u00128\n\u0004type\u0018\u0005 \u0001(\u000e2*.google.protobuf.FieldDescriptorProto.Type\u0012\u0011\n\ttype_name\u0018\u0006 \u0001(\t\u0012\u0010\n\bextendee\u0018\u0002 \u0001(\t\u0012\u0015\n\rdefault_value\u0018\u0007 \u0001(\t\u0012.\n\u0007options\u0018\b \u0001(\u000b2\u001d.google.protobuf.FieldOptions\"¶\u0002\n\u0004Type\u0012\u000f\n\u000bTYP", "E_DOUBLE\u0010\u0001\u0012\u000e\n\nTYPE_FLOAT\u0010\u0002\u0012\u000e\n\nTYPE_INT64\u0010\u0003\u0012\u000f\n\u000bTYPE_UINT64\u0010\u0004\u0012\u000e\n\nTYPE_INT32\u0010\u0005\u0012\u0010\n\fTYPE_FIXED64\u0010\u0006\u0012\u0010\n\fTYPE_FIXED32\u0010\u0007\u0012\r\n\tTYPE_BOOL\u0010\b\u0012\u000f\n\u000bTYPE_STRING\u0010\t\u0012\u000e\n\nTYPE_GROUP\u0010\n\u0012\u0010\n\fTYPE_MESSAGE\u0010\u000b\u0012\u000e\n\nTYPE_BYTES\u0010\f\u0012\u000f\n\u000bTYPE_UINT32\u0010\r\u0012\r\n\tTYPE_ENUM\u0010\u000e\u0012\u0011\n\rTYPE_SFIXED32\u0010\u000f\u0012\u0011\n\rTYPE_SFIXED64\u0010\u0010\u0012\u000f\n\u000bTYPE_SINT32\u0010\u0011\u0012\u000f\n\u000bTYPE_SINT64\u0010\u0012\"C\n\u0005Label\u0012\u0012\n\u000eLABEL_OPTIONAL\u0010\u0001\u0012\u0012\n\u000eLABEL_REQUIRED\u0010\u0002\u0012\u0012\n\u000eLABEL_REPEATED\u0010\u0003\"\u0001\n\u0013EnumDescriptorProto\u0012\f\n\u0004name\u0018\u0001", " \u0001(\t\u00128\n\u0005value\u0018\u0002 \u0003(\u000b2).google.protobuf.EnumValueDescriptorProto\u0012-\n\u0007options\u0018\u0003 \u0001(\u000b2\u001c.google.protobuf.EnumOptions\"l\n\u0018EnumValueDescriptorProto\u0012\f\n\u0004name\u0018\u0001 \u0001(\t\u0012\u000e\n\u0006number\u0018\u0002 \u0001(\u0005\u00122\n\u0007options\u0018\u0003 \u0001(\u000b2!.google.protobuf.EnumValueOptions\"\u0001\n\u0016ServiceDescriptorProto\u0012\f\n\u0004name\u0018\u0001 \u0001(\t\u00126\n\u0006method\u0018\u0002 \u0003(\u000b2&.google.protobuf.MethodDescriptorProto\u00120\n\u0007options\u0018\u0003 \u0001(\u000b2\u001f.google.protobuf.ServiceOptions\"\n\u0015MethodDescriptorProto\u0012\f\n\u0004name\u0018", "\u0001 \u0001(\t\u0012\u0012\n\ninput_type\u0018\u0002 \u0001(\t\u0012\u0013\n\u000boutput_type\u0018\u0003 \u0001(\t\u0012/\n\u0007options\u0018\u0004 \u0001(\u000b2\u001e.google.protobuf.MethodOptions\"¤\u0003\n\u000bFileOptions\u0012\u0014\n\fjava_package\u0018\u0001 \u0001(\t\u0012\u001c\n\u0014java_outer_classname\u0018\b \u0001(\t\u0012\"\n\u0013java_multiple_files\u0018\n \u0001(\b:\u0005false\u0012F\n\foptimize_for\u0018\t \u0001(\u000e2).google.protobuf.FileOptions.OptimizeMode:\u0005SPEED\u0012!\n\u0013cc_generic_services\u0018\u0010 \u0001(\b:\u0004true\u0012#\n\u0015java_generic_services\u0018\u0011 \u0001(\b:\u0004true\u0012!\n\u0013py_generic_services\u0018\u0012 \u0001(\b:\u0004true\u0012C\n\u0014uninterpreted_opti", "on\u0018ç\u0007 \u0003(\u000b2$.google.protobuf.UninterpretedOption\":\n\fOptimizeMode\u0012\t\n\u0005SPEED\u0010\u0001\u0012\r\n\tCODE_SIZE\u0010\u0002\u0012\u0010\n\fLITE_RUNTIME\u0010\u0003*\t\bè\u0007\u0010\u0002\"¸\u0001\n\u000eMessageOptions\u0012&\n\u0017message_set_wire_format\u0018\u0001 \u0001(\b:\u0005false\u0012.\n\u001fno_standard_descriptor_accessor\u0018\u0002 \u0001(\b:\u0005false\u0012C\n\u0014uninterpreted_option\u0018ç\u0007 \u0003(\u000b2$.google.protobuf.UninterpretedOption*\t\bè\u0007\u0010\u0002\"\u0002\n\fFieldOptions\u0012:\n\u0005ctype\u0018\u0001 \u0001(\u000e2#.google.protobuf.FieldOptions.CType:\u0006STRING\u0012\u000e\n\u0006packed\u0018\u0002 \u0001(\b\u0012\u0019", "\n\ndeprecated\u0018\u0003 \u0001(\b:\u0005false\u0012\u001c\n\u0014experimental_map_key\u0018\t \u0001(\t\u0012C\n\u0014uninterpreted_option\u0018ç\u0007 \u0003(\u000b2$.google.protobuf.UninterpretedOption\"/\n\u0005CType\u0012\n\n\u0006STRING\u0010\u0000\u0012\b\n\u0004CORD\u0010\u0001\u0012\u0010\n\fSTRING_PIECE\u0010\u0002*\t\bè\u0007\u0010\u0002\"]\n\u000bEnumOptions\u0012C\n\u0014uninterpreted_option\u0018ç\u0007 \u0003(\u000b2$.google.protobuf.UninterpretedOption*\t\bè\u0007\u0010\u0002\"b\n\u0010EnumValueOptions\u0012C\n\u0014uninterpreted_option\u0018ç\u0007 \u0003(\u000b2$.google.protobuf.UninterpretedOption*\t\bè\u0007\u0010\u0002\"`\n\u000eServiceOptions\u0012C\n", "\u0014uninterpreted_option\u0018ç\u0007 \u0003(\u000b2$.google.protobuf.UninterpretedOption*\t\bè\u0007\u0010\u0002\"_\n\rMethodOptions\u0012C\n\u0014uninterpreted_option\u0018ç\u0007 \u0003(\u000b2$.google.protobuf.UninterpretedOption*\t\bè\u0007\u0010\u0002\"\u0002\n\u0013UninterpretedOption\u0012;\n\u0004name\u0018\u0002 \u0003(\u000b2-.google.protobuf.UninterpretedOption.NamePart\u0012\u0018\n\u0010identifier_value\u0018\u0003 \u0001(\t\u0012\u001a\n\u0012positive_int_value\u0018\u0004 \u0001(\u0004\u0012\u001a\n\u0012negative_int_value\u0018\u0005 \u0001(\u0003\u0012\u0014\n\fdouble_value\u0018\u0006 \u0001(\u0001\u0012\u0014\n\fstring_value\u0018\u0007 \u0001(\f\u001a3\n\bNamePart\u0012\u0011\n", "\tname_part\u0018\u0001 \u0002(\t\u0012\u0014\n\fis_extension\u0018\u0002 \u0002(\bB)\n\u0013com.google.protobufB\u0010DescriptorProtosH\u0001"}, new c.b[0], new r());
    }
}
