package com.google.protobuf;

import java.util.List;

public final class e extends RuntimeException {
    private final List<String> a;

    public e() {
        super("Message was missing required fields.  (Lite runtime could not determine which fields were missing).");
        this.a = null;
    }

    public e(List<String> list) {
        super(a(list));
        this.a = list;
    }

    public final ac a() {
        return new ac(getMessage());
    }

    private static String a(List<String> list) {
        StringBuilder sb = new StringBuilder("Message missing required fields: ");
        boolean z = true;
        for (String next : list) {
            if (z) {
                z = false;
            } else {
                sb.append(", ");
            }
            sb.append(next);
        }
        return sb.toString();
    }
}
