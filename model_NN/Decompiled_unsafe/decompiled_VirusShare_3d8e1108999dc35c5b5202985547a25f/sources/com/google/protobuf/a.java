package com.google.protobuf;

import com.badlogic.gdx.physics.box2d.Transform;
import com.google.protobuf.i;
import com.google.protobuf.q;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public final class a implements i {
    private static final a a = new a(Collections.emptyMap(), (byte) 0);
    /* access modifiers changed from: private */
    public Map<Integer, C0014a> b;

    /* synthetic */ a(Map map) {
        this(map, (byte) 0);
    }

    public final /* bridge */ /* synthetic */ i getDefaultInstanceForType() {
        return a;
    }

    public final /* bridge */ /* synthetic */ i.a newBuilderForType() {
        return b.b();
    }

    public final /* bridge */ /* synthetic */ i.a toBuilder() {
        return b.b().a(this);
    }

    private a() {
    }

    public static b a() {
        return b.b();
    }

    public static b a(a aVar) {
        return b.b().a(aVar);
    }

    public static a b() {
        return a;
    }

    private a(Map<Integer, C0014a> map, byte b2) {
        this.b = map;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof a) || !this.b.equals(((a) obj).b)) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return this.b.hashCode();
    }

    public final Map<Integer, C0014a> c() {
        return this.b;
    }

    public final void writeTo(x xVar) throws IOException {
        for (Map.Entry next : this.b.entrySet()) {
            ((C0014a) next.getValue()).a(((Integer) next.getKey()).intValue(), xVar);
        }
    }

    public final String toString() {
        return p.a(this);
    }

    public final q toByteString() {
        try {
            q.a b2 = q.b(getSerializedSize());
            writeTo(b2.b());
            return b2.a();
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a ByteString threw an IOException (should never happen).", e);
        }
    }

    public final int getSerializedSize() {
        int i = 0;
        Iterator<Map.Entry<Integer, C0014a>> it = this.b.entrySet().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            Map.Entry next = it.next();
            i = ((C0014a) next.getValue()).a(((Integer) next.getKey()).intValue()) + i2;
        }
    }

    public final void a(x xVar) throws IOException {
        for (Map.Entry next : this.b.entrySet()) {
            ((C0014a) next.getValue()).b(((Integer) next.getKey()).intValue(), xVar);
        }
    }

    public final int d() {
        int i = 0;
        Iterator<Map.Entry<Integer, C0014a>> it = this.b.entrySet().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            Map.Entry next = it.next();
            i = ((C0014a) next.getValue()).b(((Integer) next.getKey()).intValue()) + i2;
        }
    }

    public final boolean isInitialized() {
        return true;
    }

    public static final class b implements i.a {
        private Map<Integer, C0014a> a;
        private int b;
        private C0014a.C0015a c;

        static /* synthetic */ b b() {
            b bVar = new b();
            bVar.a = Collections.emptyMap();
            bVar.b = 0;
            bVar.c = null;
            return bVar;
        }

        public final /* bridge */ /* synthetic */ Object clone() throws CloneNotSupportedException {
            a(0);
            return a.a().a(new a(this.a));
        }

        public final /* bridge */ /* synthetic */ i.a mergeFrom(h hVar, ab abVar) throws IOException {
            int a2;
            do {
                a2 = hVar.a();
                if (a2 == 0) {
                    break;
                }
            } while (a(a2, hVar));
            return this;
        }

        private b() {
        }

        private C0014a.C0015a a(int i) {
            if (this.c != null) {
                if (i == this.b) {
                    return this.c;
                }
                b(this.b, this.c.a());
            }
            if (i == 0) {
                return null;
            }
            C0014a aVar = this.a.get(Integer.valueOf(i));
            this.b = i;
            this.c = C0014a.a();
            if (aVar != null) {
                this.c.a(aVar);
            }
            return this.c;
        }

        /* renamed from: a */
        public final a build() {
            a aVar;
            a(0);
            if (this.a.isEmpty()) {
                aVar = a.b();
            } else {
                aVar = new a(Collections.unmodifiableMap(this.a));
            }
            this.a = null;
            return aVar;
        }

        public final b a(a aVar) {
            if (aVar != a.b()) {
                for (Map.Entry entry : aVar.b.entrySet()) {
                    a(((Integer) entry.getKey()).intValue(), (C0014a) entry.getValue());
                }
            }
            return this;
        }

        public final b a(int i, C0014a aVar) {
            if (i == 0) {
                throw new IllegalArgumentException("Zero is not a valid field number.");
            } else if (i == 0) {
                throw new IllegalArgumentException("Zero is not a valid field number.");
            } else {
                if (i == this.b || this.a.containsKey(Integer.valueOf(i))) {
                    a(i).a(aVar);
                } else {
                    b(i, aVar);
                }
                return this;
            }
        }

        public final b a(int i, int i2) {
            if (i == 0) {
                throw new IllegalArgumentException("Zero is not a valid field number.");
            }
            a(i).a((long) i2);
            return this;
        }

        private b b(int i, C0014a aVar) {
            if (i == 0) {
                throw new IllegalArgumentException("Zero is not a valid field number.");
            }
            if (this.c != null && this.b == i) {
                this.c = null;
                this.b = 0;
            }
            if (this.a.isEmpty()) {
                this.a = new TreeMap();
            }
            this.a.put(Integer.valueOf(i), aVar);
            return this;
        }

        public final boolean a(int i, h hVar) throws IOException {
            int b2 = u.b(i);
            switch (u.a(i)) {
                case Transform.POS_X:
                    a(b2).a(hVar.o());
                    return true;
                case 1:
                    a(b2).b(hVar.q());
                    return true;
                case 2:
                    a(b2).a(hVar.k());
                    return true;
                case 3:
                    b a2 = a.a();
                    hVar.a(b2, a2, f.a());
                    a(b2).a(a2.build());
                    return true;
                case 4:
                    return false;
                case 5:
                    a(b2).a(hVar.p());
                    return true;
                default:
                    throw ac.d();
            }
        }
    }

    /* renamed from: com.google.protobuf.a$a  reason: collision with other inner class name */
    public static final class C0014a {
        private static final C0014a a = C0015a.b().a();
        /* access modifiers changed from: private */
        public List<Long> b;
        /* access modifiers changed from: private */
        public List<Integer> c;
        /* access modifiers changed from: private */
        public List<Long> d;
        /* access modifiers changed from: private */
        public List<q> e;
        /* access modifiers changed from: private */
        public List<a> f;

        /* synthetic */ C0014a() {
            this((byte) 0);
        }

        private C0014a(byte b2) {
        }

        public static C0015a a() {
            return C0015a.b();
        }

        public final List<Long> b() {
            return this.b;
        }

        public final List<Integer> c() {
            return this.c;
        }

        public final List<Long> d() {
            return this.d;
        }

        public final List<q> e() {
            return this.e;
        }

        public final List<a> f() {
            return this.f;
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof C0014a)) {
                return false;
            }
            return Arrays.equals(g(), ((C0014a) obj).g());
        }

        public final int hashCode() {
            return Arrays.hashCode(g());
        }

        private Object[] g() {
            return new Object[]{this.b, this.c, this.d, this.e, this.f};
        }

        public final void a(int i, x xVar) throws IOException {
            for (Long longValue : this.b) {
                xVar.a(i, longValue.longValue());
            }
            for (Integer intValue : this.c) {
                int intValue2 = intValue.intValue();
                xVar.g(i, 5);
                xVar.j(intValue2);
            }
            for (Long longValue2 : this.d) {
                long longValue3 = longValue2.longValue();
                xVar.g(i, 1);
                xVar.c(longValue3);
            }
            for (q a2 : this.e) {
                xVar.a(i, a2);
            }
            for (a a3 : this.f) {
                xVar.a(i, a3);
            }
        }

        public final int a(int i) {
            int i2;
            int i3 = 0;
            Iterator<Long> it = this.b.iterator();
            while (true) {
                i2 = i3;
                if (!it.hasNext()) {
                    break;
                }
                i3 = x.b(i, it.next().longValue()) + i2;
            }
            for (Integer intValue : this.c) {
                intValue.intValue();
                i2 += x.g(i) + 4;
            }
            for (Long longValue : this.d) {
                longValue.longValue();
                i2 += x.g(i) + 8;
            }
            for (q b2 : this.e) {
                i2 += x.b(i, b2);
            }
            for (a serializedSize : this.f) {
                i2 += serializedSize.getSerializedSize() + (x.g(i) * 2);
            }
            return i2;
        }

        public final void b(int i, x xVar) throws IOException {
            for (q a2 : this.e) {
                xVar.g(1, 3);
                xVar.b(2, i);
                xVar.a(3, a2);
                xVar.g(1, 4);
            }
        }

        public final int b(int i) {
            int i2 = 0;
            Iterator<q> it = this.e.iterator();
            while (true) {
                int i3 = i2;
                if (!it.hasNext()) {
                    return i3;
                }
                i2 = x.b(3, it.next()) + (x.g(1) * 2) + x.e(2, i) + i3;
            }
        }

        /* renamed from: com.google.protobuf.a$a$a  reason: collision with other inner class name */
        public static final class C0015a {
            private C0014a a;

            static /* synthetic */ C0015a b() {
                C0015a aVar = new C0015a();
                aVar.a = new C0014a();
                return aVar;
            }

            private C0015a() {
            }

            public final C0014a a() {
                if (this.a.b == null) {
                    List unused = this.a.b = Collections.emptyList();
                } else {
                    List unused2 = this.a.b = Collections.unmodifiableList(this.a.b);
                }
                if (this.a.c == null) {
                    List unused3 = this.a.c = Collections.emptyList();
                } else {
                    List unused4 = this.a.c = Collections.unmodifiableList(this.a.c);
                }
                if (this.a.d == null) {
                    List unused5 = this.a.d = Collections.emptyList();
                } else {
                    List unused6 = this.a.d = Collections.unmodifiableList(this.a.d);
                }
                if (this.a.e == null) {
                    List unused7 = this.a.e = Collections.emptyList();
                } else {
                    List unused8 = this.a.e = Collections.unmodifiableList(this.a.e);
                }
                if (this.a.f == null) {
                    List unused9 = this.a.f = Collections.emptyList();
                } else {
                    List unused10 = this.a.f = Collections.unmodifiableList(this.a.f);
                }
                C0014a aVar = this.a;
                this.a = null;
                return aVar;
            }

            public final C0015a a(C0014a aVar) {
                if (!aVar.b.isEmpty()) {
                    if (this.a.b == null) {
                        List unused = this.a.b = new ArrayList();
                    }
                    this.a.b.addAll(aVar.b);
                }
                if (!aVar.c.isEmpty()) {
                    if (this.a.c == null) {
                        List unused2 = this.a.c = new ArrayList();
                    }
                    this.a.c.addAll(aVar.c);
                }
                if (!aVar.d.isEmpty()) {
                    if (this.a.d == null) {
                        List unused3 = this.a.d = new ArrayList();
                    }
                    this.a.d.addAll(aVar.d);
                }
                if (!aVar.e.isEmpty()) {
                    if (this.a.e == null) {
                        List unused4 = this.a.e = new ArrayList();
                    }
                    this.a.e.addAll(aVar.e);
                }
                if (!aVar.f.isEmpty()) {
                    if (this.a.f == null) {
                        List unused5 = this.a.f = new ArrayList();
                    }
                    this.a.f.addAll(aVar.f);
                }
                return this;
            }

            public final C0015a a(long j) {
                if (this.a.b == null) {
                    List unused = this.a.b = new ArrayList();
                }
                this.a.b.add(Long.valueOf(j));
                return this;
            }

            public final C0015a a(int i) {
                if (this.a.c == null) {
                    List unused = this.a.c = new ArrayList();
                }
                this.a.c.add(Integer.valueOf(i));
                return this;
            }

            public final C0015a b(long j) {
                if (this.a.d == null) {
                    List unused = this.a.d = new ArrayList();
                }
                this.a.d.add(Long.valueOf(j));
                return this;
            }

            public final C0015a a(q qVar) {
                if (this.a.e == null) {
                    List unused = this.a.e = new ArrayList();
                }
                this.a.e.add(qVar);
                return this;
            }

            public final C0015a a(a aVar) {
                if (this.a.f == null) {
                    List unused = this.a.f = new ArrayList();
                }
                this.a.f.add(aVar);
                return this;
            }
        }
    }
}
