package com.google.protobuf;

import com.google.protobuf.c;
import java.util.Collections;
import java.util.Map;

public final class f extends ab {
    private static final f c = new f();
    private final Map<String, a> a = Collections.emptyMap();
    private final Map<b, a> b = Collections.emptyMap();

    public static final class a {
        public final c.f a;
        public final s b;
    }

    public static f a() {
        return c;
    }

    public final a a(c.a aVar, int i) {
        return this.b.get(new b(aVar, i));
    }

    private f() {
        super(ab.b());
    }

    private static final class b {
        private final c.a a;
        private final int b;

        b(c.a aVar, int i) {
            this.a = aVar;
            this.b = i;
        }

        public final int hashCode() {
            return (this.a.hashCode() * 65535) + this.b;
        }

        public final boolean equals(Object obj) {
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            if (this.a == bVar.a && this.b == bVar.b) {
                return true;
            }
            return false;
        }
    }
}
