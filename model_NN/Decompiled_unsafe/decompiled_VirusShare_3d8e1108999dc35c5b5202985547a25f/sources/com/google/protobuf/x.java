package com.google.protobuf;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;

public final class x {
    private final byte[] a;
    private final int b;
    private int c;
    private final OutputStream d;

    static int a(int i) {
        if (i > 4096) {
            return 4096;
        }
        return i;
    }

    private x(byte[] bArr, int i) {
        this.d = null;
        this.a = bArr;
        this.c = 0;
        this.b = i + 0;
    }

    private x(OutputStream outputStream, byte[] bArr) {
        this.d = outputStream;
        this.a = bArr;
        this.c = 0;
        this.b = bArr.length;
    }

    public static x a(OutputStream outputStream, int i) {
        return new x(outputStream, new byte[i]);
    }

    public static x a(byte[] bArr) {
        return new x(bArr, bArr.length);
    }

    public final void a(int i, float f) throws IOException {
        g(i, 5);
        a(f);
    }

    public final void a(int i, long j) throws IOException {
        g(i, 0);
        a(j);
    }

    public final void a(int i, int i2) throws IOException {
        g(i, 0);
        b(i2);
    }

    public final void a(int i, boolean z) throws IOException {
        g(i, 0);
        a(z);
    }

    public final void a(int i, String str) throws IOException {
        g(i, 2);
        a(str);
    }

    public final void a(int i, i iVar) throws IOException {
        g(i, 3);
        iVar.writeTo(this);
        g(i, 4);
    }

    public final void b(int i, i iVar) throws IOException {
        g(i, 2);
        a(iVar);
    }

    public final void a(int i, q qVar) throws IOException {
        g(i, 2);
        a(qVar);
    }

    public final void b(int i, int i2) throws IOException {
        g(i, 0);
        h(i2);
    }

    public final void c(int i, int i2) throws IOException {
        g(i, 0);
        h(i2);
    }

    public final void c(int i, i iVar) throws IOException {
        g(1, 3);
        b(2, i);
        b(3, iVar);
        g(1, 4);
    }

    public final void a(double d2) throws IOException {
        c(Double.doubleToRawLongBits(d2));
    }

    public final void a(float f) throws IOException {
        j(Float.floatToRawIntBits(f));
    }

    public final void b(int i) throws IOException {
        if (i >= 0) {
            h(i);
        } else {
            a((long) i);
        }
    }

    public final void a(boolean z) throws IOException {
        l(z ? 1 : 0);
    }

    public final void a(String str) throws IOException {
        byte[] bytes = str.getBytes("UTF-8");
        h(bytes.length);
        b(bytes);
    }

    public final void a(i iVar) throws IOException {
        h(iVar.getSerializedSize());
        iVar.writeTo(this);
    }

    public final void a(q qVar) throws IOException {
        byte[] b2 = qVar.b();
        h(b2.length);
        b(b2);
    }

    public final void c(int i) throws IOException {
        h(i);
    }

    public static int d(int i) {
        return g(i) + 4;
    }

    public static int b(int i, long j) {
        return g(i) + b(j);
    }

    public static int d(int i, int i2) {
        return g(i) + f(i2);
    }

    public static int e(int i) {
        return g(i) + 1;
    }

    public static int b(int i, String str) {
        return g(i) + b(str);
    }

    public static int d(int i, i iVar) {
        return g(i) + b(iVar);
    }

    public static int b(int i, q qVar) {
        return g(i) + b(qVar);
    }

    public static int e(int i, int i2) {
        return g(i) + i(i2);
    }

    public static int f(int i, int i2) {
        return g(i) + i(i2);
    }

    public static int e(int i, i iVar) {
        return (g(1) * 2) + e(2, i) + d(3, iVar);
    }

    public static int f(int i) {
        if (i >= 0) {
            return i(i);
        }
        return 10;
    }

    public static int b(String str) {
        try {
            byte[] bytes = str.getBytes("UTF-8");
            return bytes.length + i(bytes.length);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("UTF-8 not supported.", e);
        }
    }

    public static int b(i iVar) {
        int serializedSize = iVar.getSerializedSize();
        return serializedSize + i(serializedSize);
    }

    public static int b(q qVar) {
        return i(qVar.a()) + qVar.a();
    }

    private void c() throws IOException {
        if (this.d == null) {
            throw new a();
        }
        this.d.write(this.a, 0, this.c);
        this.c = 0;
    }

    public final void a() throws IOException {
        if (this.d != null) {
            c();
        }
    }

    public final void b() {
        if (this.d != null) {
            throw new UnsupportedOperationException("spaceLeft() can only be called on CodedOutputStreams that are writing to a flat array.");
        } else if (this.b - this.c != 0) {
            throw new IllegalStateException("Did not write as much data as expected.");
        }
    }

    public static class a extends IOException {
        a() {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.");
        }
    }

    private void l(int i) throws IOException {
        byte b2 = (byte) i;
        if (this.c == this.b) {
            c();
        }
        byte[] bArr = this.a;
        int i2 = this.c;
        this.c = i2 + 1;
        bArr[i2] = b2;
    }

    private void b(byte[] bArr) throws IOException {
        int length = bArr.length;
        if (this.b - this.c >= length) {
            System.arraycopy(bArr, 0, this.a, this.c, length);
            this.c = length + this.c;
            return;
        }
        int i = this.b - this.c;
        System.arraycopy(bArr, 0, this.a, this.c, i);
        int i2 = i + 0;
        int i3 = length - i;
        this.c = this.b;
        c();
        if (i3 <= this.b) {
            System.arraycopy(bArr, i2, this.a, 0, i3);
            this.c = i3;
            return;
        }
        this.d.write(bArr, i2, i3);
    }

    public final void g(int i, int i2) throws IOException {
        h(u.a(i, i2));
    }

    public static int g(int i) {
        return i(u.a(i, 0));
    }

    public final void h(int i) throws IOException {
        while ((i & -128) != 0) {
            l((i & 127) | 128);
            i >>>= 7;
        }
        l(i);
    }

    public static int i(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        if ((-268435456 & i) == 0) {
            return 4;
        }
        return 5;
    }

    public final void a(long j) throws IOException {
        while ((-128 & j) != 0) {
            l((((int) j) & 127) | 128);
            j >>>= 7;
        }
        l((int) j);
    }

    public static int b(long j) {
        if ((-128 & j) == 0) {
            return 1;
        }
        if ((-16384 & j) == 0) {
            return 2;
        }
        if ((-2097152 & j) == 0) {
            return 3;
        }
        if ((-268435456 & j) == 0) {
            return 4;
        }
        if ((-34359738368L & j) == 0) {
            return 5;
        }
        if ((-4398046511104L & j) == 0) {
            return 6;
        }
        if ((-562949953421312L & j) == 0) {
            return 7;
        }
        if ((-72057594037927936L & j) == 0) {
            return 8;
        }
        if ((Long.MIN_VALUE & j) == 0) {
            return 9;
        }
        return 10;
    }

    public final void j(int i) throws IOException {
        l(i & 255);
        l((i >> 8) & 255);
        l((i >> 16) & 255);
        l((i >> 24) & 255);
    }

    public final void c(long j) throws IOException {
        l(((int) j) & 255);
        l(((int) (j >> 8)) & 255);
        l(((int) (j >> 16)) & 255);
        l(((int) (j >> 24)) & 255);
        l(((int) (j >> 32)) & 255);
        l(((int) (j >> 40)) & 255);
        l(((int) (j >> 48)) & 255);
        l(((int) (j >> 56)) & 255);
    }

    public static int k(int i) {
        return (i << 1) ^ (i >> 31);
    }

    public static long d(long j) {
        return (j << 1) ^ (j >> 63);
    }
}
