package com.google.protobuf;

import com.google.protobuf.ae;
import com.google.protobuf.c;
import com.google.protobuf.s;
import java.io.IOException;
import java.util.Map;

public final class t extends ae {
    /* access modifiers changed from: private */
    public final c.a a;
    /* access modifiers changed from: private */
    public final o<c.f> b;
    /* access modifiers changed from: private */
    public final a c;
    private int d;

    /* synthetic */ t(c.a aVar, o oVar, a aVar2) {
        this(aVar, oVar, aVar2, (byte) 0);
    }

    private t(c.a aVar, o<c.f> oVar, a aVar2, byte b2) {
        this.d = -1;
        this.a = aVar;
        this.b = oVar;
        this.c = aVar2;
    }

    public static t a(c.a aVar) {
        return new t(aVar, o.b(), a.b(), (byte) 0);
    }

    public final c.a getDescriptorForType() {
        return this.a;
    }

    public final Map<c.f, Object> getAllFields() {
        return this.b.e();
    }

    public final boolean hasField(c.f fVar) {
        if (fVar.o() == this.a) {
            return this.b.a(fVar);
        }
        throw new IllegalArgumentException("FieldDescriptor does not match message type.");
    }

    public final a getUnknownFields() {
        return this.c;
    }

    /* access modifiers changed from: private */
    public static boolean b(c.a aVar, o<c.f> oVar) {
        for (c.f next : aVar.e()) {
            if (next.j() && !oVar.a(next)) {
                return false;
            }
        }
        return oVar.g();
    }

    public final boolean isInitialized() {
        return b(this.a, this.b);
    }

    public final void writeTo(x xVar) throws IOException {
        if (this.a.d().getMessageSetWireFormat()) {
            this.b.b(xVar);
            this.c.a(xVar);
            return;
        }
        this.b.a(xVar);
        this.c.writeTo(xVar);
    }

    public final int getSerializedSize() {
        int i = this.d;
        if (i == -1) {
            if (this.a.d().getMessageSetWireFormat()) {
                i = this.b.i() + this.c.d();
            } else {
                i = this.b.h() + this.c.getSerializedSize();
            }
            this.d = i;
        }
        return i;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public a newBuilderForType() {
        return new a(this.a);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public a toBuilder() {
        return newBuilderForType().mergeFrom(this);
    }

    public static final class a extends ae.a<a> {
        private final c.a a;
        private o<c.f> b;
        private a c;

        /* synthetic */ a(c.a aVar) {
            this(aVar, (byte) 0);
        }

        public final /* bridge */ /* synthetic */ s.a addRepeatedField(c.f fVar, Object obj) {
            a(fVar);
            this.b.b(fVar, obj);
            return this;
        }

        public final /* bridge */ /* synthetic */ s.a clearField(c.f fVar) {
            a(fVar);
            this.b.c(fVar);
            return this;
        }

        public final /* bridge */ /* synthetic */ s.a newBuilderForField(c.f fVar) {
            a(fVar);
            if (fVar.h() == c.f.b.MESSAGE) {
                return new a(fVar.q(), (byte) 0);
            }
            throw new IllegalArgumentException("newBuilderForField is only valid for fields with message type.");
        }

        public final /* bridge */ /* synthetic */ s.a setField(c.f fVar, Object obj) {
            a(fVar);
            this.b.a(fVar, obj);
            return this;
        }

        public final /* bridge */ /* synthetic */ s.a setUnknownFields(a aVar) {
            this.c = aVar;
            return this;
        }

        private a(c.a aVar, byte b2) {
            this.a = aVar;
            this.b = o.a();
            this.c = a.b();
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public a clear() {
            if (this.b == null) {
                throw new IllegalStateException("Cannot call clear() after build().");
            }
            this.b.d();
            return this;
        }

        /* renamed from: a */
        public final a mergeFrom(s sVar) {
            if (!(sVar instanceof t)) {
                return (a) super.mergeFrom(sVar);
            }
            t tVar = (t) sVar;
            if (tVar.a != this.a) {
                throw new IllegalArgumentException("mergeFrom(Message) can only merge messages of the same type.");
            }
            this.b.a(tVar.b);
            mergeUnknownFields(tVar.c);
            return this;
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public t build() {
            if (this.b != null && !t.b(this.a, this.b)) {
                throw newUninitializedMessageException((s) new t(this.a, this.b, this.c));
            } else if (this.b == null) {
                throw new IllegalStateException("build() has already been called on this Builder.");
            } else {
                this.b.c();
                t tVar = new t(this.a, this.b, this.c);
                this.b = null;
                this.c = null;
                return tVar;
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: c */
        public a clone() {
            a aVar = new a(this.a, (byte) 0);
            aVar.b.a(this.b);
            return aVar;
        }

        public final c.a getDescriptorForType() {
            return this.a;
        }

        public final Map<c.f, Object> getAllFields() {
            return this.b.e();
        }

        public final Object getField(c.f fVar) {
            a(fVar);
            Object b2 = this.b.b(fVar);
            if (b2 != null) {
                return b2;
            }
            if (fVar.h() == c.f.b.MESSAGE) {
                return t.a(fVar.q());
            }
            return fVar.m();
        }

        public final a getUnknownFields() {
            return this.c;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public a mergeUnknownFields(a aVar) {
            this.c = a.a(this.c).a(aVar).build();
            return this;
        }

        private void a(c.f fVar) {
            if (fVar.o() != this.a) {
                throw new IllegalArgumentException("FieldDescriptor does not match message type.");
            }
        }
    }
}
