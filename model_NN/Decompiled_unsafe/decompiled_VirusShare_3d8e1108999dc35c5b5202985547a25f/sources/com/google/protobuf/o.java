package com.google.protobuf;

import com.google.protobuf.DescriptorProtos;
import com.google.protobuf.c;
import com.google.protobuf.g;
import com.google.protobuf.i;
import com.google.protobuf.o.a;
import com.google.protobuf.u;
import com.ptowngames.jigsaur.Jigsaur;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

final class o<FieldDescriptorType extends a<FieldDescriptorType>> {
    private static final o b = new o();
    private Map<FieldDescriptorType, Object> a;

    public interface a<T extends a<T>> extends Comparable<T> {
        i.a a(i.a aVar, i iVar);

        int a_();

        u.b b_();

        u.a c_();

        boolean d();

        boolean e();
    }

    private o(byte b2) {
        this.a = new TreeMap();
    }

    private o() {
        this.a = Collections.emptyMap();
    }

    public static <T extends a<T>> o<T> a() {
        return new o<>((byte) 0);
    }

    public static <T extends a<T>> o<T> b() {
        return b;
    }

    public final void c() {
        for (Map.Entry next : this.a.entrySet()) {
            if (((a) next.getKey()).d()) {
                this.a.put(next.getKey(), Collections.unmodifiableList((List) next.getValue()));
            }
        }
        this.a = Collections.unmodifiableMap(this.a);
    }

    public final void d() {
        this.a.clear();
    }

    public final Map<FieldDescriptorType, Object> e() {
        return Collections.unmodifiableMap(this.a);
    }

    public final Iterator<Map.Entry<FieldDescriptorType, Object>> f() {
        return this.a.entrySet().iterator();
    }

    public final boolean a(a aVar) {
        if (!aVar.d()) {
            return this.a.get(aVar) != null;
        }
        throw new IllegalArgumentException("hasField() can only be called on non-repeated fields.");
    }

    public final Object b(c.f fVar) {
        return this.a.get(fVar);
    }

    public final void a(a aVar, Object obj) {
        if (!aVar.d()) {
            a(aVar.c_(), obj);
        } else if (!(obj instanceof List)) {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        } else {
            ArrayList<Object> arrayList = new ArrayList<>();
            arrayList.addAll((List) obj);
            for (Object a2 : arrayList) {
                a(aVar.c_(), a2);
            }
            obj = arrayList;
        }
        this.a.put(aVar, obj);
    }

    public final void c(FieldDescriptorType fielddescriptortype) {
        this.a.remove(fielddescriptortype);
    }

    public final int d(FieldDescriptorType fielddescriptortype) {
        if (!fielddescriptortype.d()) {
            throw new IllegalArgumentException("getRepeatedField() can only be called on repeated fields.");
        }
        Object obj = this.a.get(fielddescriptortype);
        if (obj == null) {
            return 0;
        }
        return ((List) obj).size();
    }

    public final Object a(a aVar, int i) {
        if (!aVar.d()) {
            throw new IllegalArgumentException("getRepeatedField() can only be called on repeated fields.");
        }
        Object obj = this.a.get(aVar);
        if (obj != null) {
            return ((List) obj).get(i);
        }
        throw new IndexOutOfBoundsException();
    }

    public final void a(a aVar, int i, Object obj) {
        if (!aVar.d()) {
            throw new IllegalArgumentException("getRepeatedField() can only be called on repeated fields.");
        }
        Object obj2 = this.a.get(aVar);
        if (obj2 == null) {
            throw new IndexOutOfBoundsException();
        }
        a(aVar.c_(), obj);
        ((List) obj2).set(i, obj);
    }

    public final void b(a aVar, Object obj) {
        Object obj2;
        if (!aVar.d()) {
            throw new IllegalArgumentException("addRepeatedField() can only be called on repeated fields.");
        }
        a(aVar.c_(), obj);
        Object obj3 = this.a.get(aVar);
        if (obj3 == null) {
            obj2 = new ArrayList();
            this.a.put(aVar, obj2);
        } else {
            obj2 = (List) obj3;
        }
        obj2.add(obj);
    }

    private static void a(u.a aVar, Object obj) {
        if (obj == null) {
            throw new NullPointerException();
        }
        boolean z = false;
        switch (aVar.b()) {
            case INT:
                z = obj instanceof Integer;
                break;
            case LONG:
                z = obj instanceof Long;
                break;
            case FLOAT:
                z = obj instanceof Float;
                break;
            case DOUBLE:
                z = obj instanceof Double;
                break;
            case BOOLEAN:
                z = obj instanceof Boolean;
                break;
            case STRING:
                z = obj instanceof String;
                break;
            case BYTE_STRING:
                z = obj instanceof q;
                break;
            case ENUM:
                z = obj instanceof g.a;
                break;
            case MESSAGE:
                z = obj instanceof i;
                break;
        }
        if (!z) {
            throw new IllegalArgumentException("Wrong object type used with protocol message reflection.");
        }
    }

    public final boolean g() {
        for (Map.Entry next : this.a.entrySet()) {
            a aVar = (a) next.getKey();
            if (aVar.b_() == u.b.MESSAGE) {
                if (aVar.d()) {
                    for (i isInitialized : (List) next.getValue()) {
                        if (!isInitialized.isInitialized()) {
                            return false;
                        }
                    }
                    continue;
                } else if (!((i) next.getValue()).isInitialized()) {
                    return false;
                }
            }
        }
        return true;
    }

    static int a(u.a aVar, boolean z) {
        if (z) {
            return 2;
        }
        return aVar.c();
    }

    public final void a(o oVar) {
        for (Map.Entry next : oVar.a.entrySet()) {
            a aVar = (a) next.getKey();
            Object value = next.getValue();
            if (aVar.d()) {
                Object obj = this.a.get(aVar);
                if (obj == null) {
                    this.a.put(aVar, new ArrayList((List) value));
                } else {
                    ((List) obj).addAll((List) value);
                }
            } else if (aVar.b_() == u.b.MESSAGE) {
                Object obj2 = this.a.get(aVar);
                if (obj2 == null) {
                    this.a.put(aVar, value);
                } else {
                    this.a.put(aVar, aVar.a(((i) obj2).toBuilder(), (i) value).build());
                }
            } else {
                this.a.put(aVar, value);
            }
        }
    }

    /* renamed from: com.google.protobuf.o$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] b = new int[u.a.values().length];

        static {
            try {
                b[u.a.DOUBLE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                b[u.a.FLOAT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                b[u.a.INT64.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                b[u.a.UINT64.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                b[u.a.INT32.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                b[u.a.FIXED64.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                b[u.a.FIXED32.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                b[u.a.BOOL.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                b[u.a.STRING.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                b[u.a.BYTES.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                b[u.a.UINT32.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                b[u.a.SFIXED32.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            try {
                b[u.a.SFIXED64.ordinal()] = 13;
            } catch (NoSuchFieldError e13) {
            }
            try {
                b[u.a.SINT32.ordinal()] = 14;
            } catch (NoSuchFieldError e14) {
            }
            try {
                b[u.a.SINT64.ordinal()] = 15;
            } catch (NoSuchFieldError e15) {
            }
            try {
                b[u.a.GROUP.ordinal()] = 16;
            } catch (NoSuchFieldError e16) {
            }
            try {
                b[u.a.MESSAGE.ordinal()] = 17;
            } catch (NoSuchFieldError e17) {
            }
            try {
                b[u.a.ENUM.ordinal()] = 18;
            } catch (NoSuchFieldError e18) {
            }
            a = new int[u.b.values().length];
            try {
                a[u.b.INT.ordinal()] = 1;
            } catch (NoSuchFieldError e19) {
            }
            try {
                a[u.b.LONG.ordinal()] = 2;
            } catch (NoSuchFieldError e20) {
            }
            try {
                a[u.b.FLOAT.ordinal()] = 3;
            } catch (NoSuchFieldError e21) {
            }
            try {
                a[u.b.DOUBLE.ordinal()] = 4;
            } catch (NoSuchFieldError e22) {
            }
            try {
                a[u.b.BOOLEAN.ordinal()] = 5;
            } catch (NoSuchFieldError e23) {
            }
            try {
                a[u.b.STRING.ordinal()] = 6;
            } catch (NoSuchFieldError e24) {
            }
            try {
                a[u.b.BYTE_STRING.ordinal()] = 7;
            } catch (NoSuchFieldError e25) {
            }
            try {
                a[u.b.ENUM.ordinal()] = 8;
            } catch (NoSuchFieldError e26) {
            }
            try {
                a[u.b.MESSAGE.ordinal()] = 9;
            } catch (NoSuchFieldError e27) {
            }
        }
    }

    public static Object a(h hVar, u.a aVar) throws IOException {
        switch (AnonymousClass1.b[aVar.ordinal()]) {
            case 1:
                return Double.valueOf(hVar.b());
            case 2:
                return Float.valueOf(hVar.c());
            case 3:
                return Long.valueOf(hVar.e());
            case 4:
                return Long.valueOf(hVar.d());
            case 5:
                return Integer.valueOf(hVar.f());
            case 6:
                return Long.valueOf(hVar.g());
            case 7:
                return Integer.valueOf(hVar.h());
            case 8:
                return Boolean.valueOf(hVar.i());
            case 9:
                return hVar.j();
            case 10:
                return hVar.k();
            case 11:
                return Integer.valueOf(hVar.l());
            case 12:
                return Integer.valueOf(hVar.p());
            case 13:
                return Long.valueOf(hVar.q());
            case Jigsaur.Level.DIFFICULTY_FAMILY_FIELD_NUMBER /*14*/:
                int n = hVar.n();
                return Integer.valueOf((-(n & 1)) ^ (n >>> 1));
            case 15:
                long o = hVar.o();
                return Long.valueOf((-(o & 1)) ^ (o >>> 1));
            case 16:
                throw new IllegalArgumentException("readPrimitiveField() cannot handle nested groups.");
            case DescriptorProtos.FileOptions.JAVA_GENERIC_SERVICES_FIELD_NUMBER /*17*/:
                throw new IllegalArgumentException("readPrimitiveField() cannot handle embedded messages.");
            case DescriptorProtos.FileOptions.PY_GENERIC_SERVICES_FIELD_NUMBER /*18*/:
                throw new IllegalArgumentException("readPrimitiveField() cannot handle enums.");
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
    }

    public final void a(x xVar) throws IOException {
        for (Map.Entry next : this.a.entrySet()) {
            a((a) next.getKey(), next.getValue(), xVar);
        }
    }

    public final void b(x xVar) throws IOException {
        for (Map.Entry next : this.a.entrySet()) {
            a aVar = (a) next.getKey();
            if (aVar.b_() != u.b.MESSAGE || aVar.d() || aVar.e()) {
                a(aVar, next.getValue(), xVar);
            } else {
                xVar.c(((a) next.getKey()).a_(), (i) next.getValue());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.protobuf.o.a(com.google.protobuf.u$a, boolean):int
     arg types: [com.google.protobuf.u$a, int]
     candidates:
      com.google.protobuf.o.a(com.google.protobuf.h, com.google.protobuf.u$a):java.lang.Object
      com.google.protobuf.o.a(com.google.protobuf.u$a, java.lang.Object):void
      com.google.protobuf.o.a(com.google.protobuf.o$a, int):java.lang.Object
      com.google.protobuf.o.a(com.google.protobuf.o$a, java.lang.Object):void
      com.google.protobuf.o.a(com.google.protobuf.u$a, boolean):int */
    private static void a(x xVar, u.a aVar, int i, Object obj) throws IOException {
        if (aVar == u.a.GROUP) {
            xVar.a(i, (i) obj);
            return;
        }
        xVar.g(i, a(aVar, false));
        a(xVar, aVar, obj);
    }

    private static void a(x xVar, u.a aVar, Object obj) throws IOException {
        switch (AnonymousClass1.b[aVar.ordinal()]) {
            case 1:
                xVar.a(((Double) obj).doubleValue());
                return;
            case 2:
                xVar.a(((Float) obj).floatValue());
                return;
            case 3:
                xVar.a(((Long) obj).longValue());
                return;
            case 4:
                xVar.a(((Long) obj).longValue());
                return;
            case 5:
                xVar.b(((Integer) obj).intValue());
                return;
            case 6:
                xVar.c(((Long) obj).longValue());
                return;
            case 7:
                xVar.j(((Integer) obj).intValue());
                return;
            case 8:
                xVar.a(((Boolean) obj).booleanValue());
                return;
            case 9:
                xVar.a((String) obj);
                return;
            case 10:
                xVar.a((q) obj);
                return;
            case 11:
                xVar.h(((Integer) obj).intValue());
                return;
            case 12:
                xVar.j(((Integer) obj).intValue());
                return;
            case 13:
                xVar.c(((Long) obj).longValue());
                return;
            case Jigsaur.Level.DIFFICULTY_FAMILY_FIELD_NUMBER /*14*/:
                xVar.h(x.k(((Integer) obj).intValue()));
                return;
            case 15:
                xVar.a(x.d(((Long) obj).longValue()));
                return;
            case 16:
                ((i) obj).writeTo(xVar);
                return;
            case DescriptorProtos.FileOptions.JAVA_GENERIC_SERVICES_FIELD_NUMBER /*17*/:
                xVar.a((i) obj);
                return;
            case DescriptorProtos.FileOptions.PY_GENERIC_SERVICES_FIELD_NUMBER /*18*/:
                xVar.h(((g.a) obj).d_());
                return;
            default:
                return;
        }
    }

    public static void a(a<?> aVar, Object obj, x xVar) throws IOException {
        u.a c_ = aVar.c_();
        int a_ = aVar.a_();
        if (aVar.d()) {
            List<Object> list = (List) obj;
            if (aVar.e()) {
                xVar.g(a_, 2);
                int i = 0;
                for (Object b2 : list) {
                    i += b(c_, b2);
                }
                xVar.h(i);
                for (Object a2 : list) {
                    a(xVar, c_, a2);
                }
                return;
            }
            for (Object a3 : list) {
                a(xVar, c_, a_, a3);
            }
            return;
        }
        a(xVar, c_, a_, obj);
    }

    public final int h() {
        int i = 0;
        Iterator<Map.Entry<FieldDescriptorType, Object>> it = this.a.entrySet().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            Map.Entry next = it.next();
            i = c((a) next.getKey(), next.getValue()) + i2;
        }
    }

    public final int i() {
        int i = 0;
        Iterator<Map.Entry<FieldDescriptorType, Object>> it = this.a.entrySet().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            Map.Entry next = it.next();
            a aVar = (a) next.getKey();
            if (aVar.b_() != u.b.MESSAGE || aVar.d() || aVar.e()) {
                i = c(aVar, next.getValue()) + i2;
            } else {
                i = x.e(((a) next.getKey()).a_(), (i) next.getValue()) + i2;
            }
        }
    }

    private static int a(u.a aVar, int i, Object obj) {
        int g = x.g(i);
        if (aVar == u.a.GROUP) {
            g *= 2;
        }
        return g + b(aVar, obj);
    }

    private static int b(u.a aVar, Object obj) {
        switch (AnonymousClass1.b[aVar.ordinal()]) {
            case 1:
                ((Double) obj).doubleValue();
                return 8;
            case 2:
                ((Float) obj).floatValue();
                return 4;
            case 3:
                return x.b(((Long) obj).longValue());
            case 4:
                return x.b(((Long) obj).longValue());
            case 5:
                return x.f(((Integer) obj).intValue());
            case 6:
                ((Long) obj).longValue();
                return 8;
            case 7:
                ((Integer) obj).intValue();
                return 4;
            case 8:
                ((Boolean) obj).booleanValue();
                return 1;
            case 9:
                return x.b((String) obj);
            case 10:
                return x.b((q) obj);
            case 11:
                return x.i(((Integer) obj).intValue());
            case 12:
                ((Integer) obj).intValue();
                return 4;
            case 13:
                ((Long) obj).longValue();
                return 8;
            case Jigsaur.Level.DIFFICULTY_FAMILY_FIELD_NUMBER /*14*/:
                return x.i(x.k(((Integer) obj).intValue()));
            case 15:
                return x.b(x.d(((Long) obj).longValue()));
            case 16:
                return ((i) obj).getSerializedSize();
            case DescriptorProtos.FileOptions.JAVA_GENERIC_SERVICES_FIELD_NUMBER /*17*/:
                return x.b((i) obj);
            case DescriptorProtos.FileOptions.PY_GENERIC_SERVICES_FIELD_NUMBER /*18*/:
                return x.i(((g.a) obj).d_());
            default:
                throw new RuntimeException("There is no way to get here, but the compiler thinks otherwise.");
        }
    }

    public static int c(a<?> aVar, Object obj) {
        int i = 0;
        u.a c_ = aVar.c_();
        int a_ = aVar.a_();
        if (!aVar.d()) {
            return a(c_, a_, obj);
        }
        if (aVar.e()) {
            for (Object b2 : (List) obj) {
                i += b(c_, b2);
            }
            return x.i(i) + x.g(a_) + i;
        }
        for (Object a2 : (List) obj) {
            i += a(c_, a_, a2);
        }
        return i;
    }
}
