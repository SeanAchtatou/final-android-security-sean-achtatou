package com.google.protobuf;

import com.google.protobuf.a;
import com.google.protobuf.ae;
import com.google.protobuf.c;
import com.google.protobuf.s;
import com.google.protobuf.u;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public abstract class GeneratedMessage extends ae {
    /* access modifiers changed from: private */
    public a unknownFields = a.b();

    /* access modifiers changed from: protected */
    public abstract FieldAccessorTable internalGetFieldAccessorTable();

    protected GeneratedMessage() {
    }

    public c.a getDescriptorForType() {
        return internalGetFieldAccessorTable().a;
    }

    /* access modifiers changed from: private */
    public Map<c.f, Object> getAllFieldsMutable() {
        TreeMap treeMap = new TreeMap();
        for (c.f next : internalGetFieldAccessorTable().a.e()) {
            if (next.d()) {
                List list = (List) getField(next);
                if (!list.isEmpty()) {
                    treeMap.put(next, list);
                }
            } else if (hasField(next)) {
                treeMap.put(next, getField(next));
            }
        }
        return treeMap;
    }

    public boolean isInitialized() {
        for (c.f next : getDescriptorForType().e()) {
            if (next.j() && !hasField(next)) {
                return false;
            }
            if (next.h() == c.f.b.MESSAGE) {
                if (next.d()) {
                    for (s isInitialized : (List) getField(next)) {
                        if (!isInitialized.isInitialized()) {
                            return false;
                        }
                    }
                    continue;
                } else if (hasField(next) && !((s) getField(next)).isInitialized()) {
                    return false;
                }
            }
        }
        return true;
    }

    public Map<c.f, Object> getAllFields() {
        return Collections.unmodifiableMap(getAllFieldsMutable());
    }

    public boolean hasField(c.f fVar) {
        return FieldAccessorTable.a(internalGetFieldAccessorTable(), fVar).has(this);
    }

    public Object getField(c.f fVar) {
        return FieldAccessorTable.a(internalGetFieldAccessorTable(), fVar).get(this);
    }

    public int getRepeatedFieldCount(c.f fVar) {
        return FieldAccessorTable.a(internalGetFieldAccessorTable(), fVar).getRepeatedCount(this);
    }

    public Object getRepeatedField(c.f fVar, int i) {
        return FieldAccessorTable.a(internalGetFieldAccessorTable(), fVar).getRepeated(this, i);
    }

    public final a getUnknownFields() {
        return this.unknownFields;
    }

    public static abstract class b<BuilderType extends b> extends ae.a<BuilderType> {
        /* access modifiers changed from: protected */
        public abstract GeneratedMessage internalGetResult();

        protected b() {
        }

        public BuilderType clone() {
            throw new UnsupportedOperationException("This is supposed to be overridden by subclasses.");
        }

        private FieldAccessorTable internalGetFieldAccessorTable() {
            return internalGetResult().internalGetFieldAccessorTable();
        }

        public c.a getDescriptorForType() {
            return internalGetFieldAccessorTable().a;
        }

        public Map<c.f, Object> getAllFields() {
            return internalGetResult().getAllFields();
        }

        public s.a newBuilderForField(c.f fVar) {
            return FieldAccessorTable.a(internalGetFieldAccessorTable(), fVar).newBuilder();
        }

        public boolean hasField(c.f fVar) {
            return internalGetResult().hasField(fVar);
        }

        public Object getField(c.f fVar) {
            if (fVar.d()) {
                return Collections.unmodifiableList((List) internalGetResult().getField(fVar));
            }
            return internalGetResult().getField(fVar);
        }

        public BuilderType setField(c.f fVar, Object obj) {
            FieldAccessorTable.a(internalGetFieldAccessorTable(), fVar).set(this, obj);
            return this;
        }

        public BuilderType clearField(c.f fVar) {
            FieldAccessorTable.a(internalGetFieldAccessorTable(), fVar).clear(this);
            return this;
        }

        public int getRepeatedFieldCount(c.f fVar) {
            return internalGetResult().getRepeatedFieldCount(fVar);
        }

        public Object getRepeatedField(c.f fVar, int i) {
            return internalGetResult().getRepeatedField(fVar, i);
        }

        public BuilderType setRepeatedField(c.f fVar, int i, Object obj) {
            FieldAccessorTable.a(internalGetFieldAccessorTable(), fVar).setRepeated(this, i, obj);
            return this;
        }

        public BuilderType addRepeatedField(c.f fVar, Object obj) {
            FieldAccessorTable.a(internalGetFieldAccessorTable(), fVar).addRepeated(this, obj);
            return this;
        }

        public final a getUnknownFields() {
            return internalGetResult().unknownFields;
        }

        public final BuilderType setUnknownFields(a aVar) {
            a unused = internalGetResult().unknownFields = aVar;
            return this;
        }

        public final BuilderType mergeUnknownFields(a aVar) {
            GeneratedMessage internalGetResult = internalGetResult();
            a unused = internalGetResult.unknownFields = a.a(internalGetResult.unknownFields).a(aVar).build();
            return this;
        }

        public boolean isInitialized() {
            return internalGetResult().isInitialized();
        }

        /* access modifiers changed from: protected */
        public boolean parseUnknownField(h hVar, a.b bVar, ab abVar, int i) throws IOException {
            return bVar.a(i, hVar);
        }
    }

    public static abstract class ExtendableMessage<MessageType extends ExtendableMessage> extends GeneratedMessage {
        /* access modifiers changed from: private */
        public final o<c.f> extensions = o.a();

        protected ExtendableMessage() {
        }

        /* access modifiers changed from: private */
        public void verifyExtensionContainingType(a<MessageType, ?> aVar) {
            if (aVar.a().o() != getDescriptorForType()) {
                throw new IllegalArgumentException("Extension is for type \"" + aVar.a().o().b() + "\" which does not match message type \"" + getDescriptorForType().b() + "\".");
            }
        }

        public final boolean hasExtension(a<MessageType, ?> aVar) {
            verifyExtensionContainingType(aVar);
            return this.extensions.a(aVar.a());
        }

        public final <Type> int getExtensionCount(a<MessageType, List<Type>> aVar) {
            verifyExtensionContainingType(aVar);
            return this.extensions.d(aVar.a());
        }

        public final <Type> Type getExtension(a<MessageType, Type> aVar) {
            verifyExtensionContainingType(aVar);
            c.f a2 = aVar.a();
            Object b = this.extensions.b(a2);
            if (b != null) {
                return a.a(aVar, b);
            }
            if (a2.d()) {
                return Collections.emptyList();
            }
            if (a2.h() == c.f.b.MESSAGE) {
                return aVar.b();
            }
            return a.a(aVar, a2.m());
        }

        public final <Type> Type getExtension(a<MessageType, List<Type>> aVar, int i) {
            verifyExtensionContainingType(aVar);
            return aVar.a(this.extensions.a(aVar.a(), i));
        }

        /* access modifiers changed from: protected */
        public boolean extensionsAreInitialized() {
            return this.extensions.g();
        }

        public boolean isInitialized() {
            return GeneratedMessage.super.isInitialized() && extensionsAreInitialized();
        }

        protected class a {
            private final Iterator<Map.Entry<c.f, Object>> a;
            private Map.Entry<c.f, Object> b;
            private final boolean c;

            /* synthetic */ a(ExtendableMessage extendableMessage, boolean z) {
                this(z, (byte) 0);
            }

            private a(boolean z, byte b2) {
                this.a = ExtendableMessage.this.extensions.f();
                if (this.a.hasNext()) {
                    this.b = this.a.next();
                }
                this.c = z;
            }

            public final void a(x xVar) throws IOException {
                while (this.b != null && this.b.getKey().a_() < 536870912) {
                    c.f key = this.b.getKey();
                    if (!this.c || key.b_() != u.b.i || key.d()) {
                        o.a(key, this.b.getValue(), xVar);
                    } else {
                        xVar.c(key.a_(), (s) this.b.getValue());
                    }
                    if (this.a.hasNext()) {
                        this.b = this.a.next();
                    } else {
                        this.b = null;
                    }
                }
            }
        }

        /* access modifiers changed from: protected */
        public ExtendableMessage<MessageType>.a newExtensionWriter() {
            return new a(this, false);
        }

        /* access modifiers changed from: protected */
        public ExtendableMessage<MessageType>.a newMessageSetExtensionWriter() {
            return new a(this, true);
        }

        /* access modifiers changed from: protected */
        public int extensionsSerializedSize() {
            return this.extensions.h();
        }

        /* access modifiers changed from: protected */
        public int extensionsSerializedSizeAsMessageSet() {
            return this.extensions.i();
        }

        public Map<c.f, Object> getAllFields() {
            Map access$700 = getAllFieldsMutable();
            access$700.putAll(this.extensions.e());
            return Collections.unmodifiableMap(access$700);
        }

        public boolean hasField(c.f fVar) {
            if (!fVar.n()) {
                return GeneratedMessage.super.hasField(fVar);
            }
            verifyContainingType(fVar);
            return this.extensions.a(fVar);
        }

        public Object getField(c.f fVar) {
            if (!fVar.n()) {
                return GeneratedMessage.super.getField(fVar);
            }
            verifyContainingType(fVar);
            Object b = this.extensions.b(fVar);
            if (b != null) {
                return b;
            }
            if (fVar.h() == c.f.b.MESSAGE) {
                return t.a(fVar.q());
            }
            return fVar.m();
        }

        public int getRepeatedFieldCount(c.f fVar) {
            if (!fVar.n()) {
                return GeneratedMessage.super.getRepeatedFieldCount(fVar);
            }
            verifyContainingType(fVar);
            return this.extensions.d(fVar);
        }

        public Object getRepeatedField(c.f fVar, int i) {
            if (!fVar.n()) {
                return GeneratedMessage.super.getRepeatedField(fVar, i);
            }
            verifyContainingType(fVar);
            return this.extensions.a(fVar, i);
        }

        /* access modifiers changed from: private */
        public void verifyContainingType(c.f fVar) {
            if (fVar.o() != getDescriptorForType()) {
                throw new IllegalArgumentException("FieldDescriptor does not match message type.");
            }
        }
    }

    public static abstract class ExtendableBuilder<MessageType extends ExtendableMessage, BuilderType extends ExtendableBuilder> extends b<BuilderType> {
        /* access modifiers changed from: protected */
        public abstract ExtendableMessage<MessageType> internalGetResult();

        protected ExtendableBuilder() {
        }

        public BuilderType clone() {
            throw new UnsupportedOperationException("This is supposed to be overridden by subclasses.");
        }

        public final boolean hasExtension(a<MessageType, ?> aVar) {
            return internalGetResult().hasExtension(aVar);
        }

        public final <Type> int getExtensionCount(a<MessageType, List<Type>> aVar) {
            return internalGetResult().getExtensionCount(aVar);
        }

        public final <Type> Type getExtension(a<MessageType, Type> aVar) {
            return internalGetResult().getExtension(aVar);
        }

        /* JADX WARN: Type inference failed for: r2v0, types: [com.google.protobuf.GeneratedMessage$a<MessageType, java.util.List<Type>>, com.google.protobuf.GeneratedMessage$a] */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final <Type> Type getExtension(com.google.protobuf.GeneratedMessage.a<MessageType, java.util.List<Type>> r2, int r3) {
            /*
                r1 = this;
                com.google.protobuf.GeneratedMessage$ExtendableMessage r0 = r1.internalGetResult()
                java.lang.Object r0 = r0.getExtension(r2, r3)
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.GeneratedMessage.ExtendableBuilder.getExtension(com.google.protobuf.GeneratedMessage$a, int):java.lang.Object");
        }

        public final <Type> BuilderType setExtension(a<MessageType, Type> aVar, Type type) {
            ExtendableMessage internalGetResult = internalGetResult();
            internalGetResult.verifyExtensionContainingType(aVar);
            internalGetResult.extensions.a(aVar.a(), a.c(aVar, type));
            return this;
        }

        public final <Type> BuilderType setExtension(a<MessageType, List<Type>> aVar, int i, Type type) {
            ExtendableMessage internalGetResult = internalGetResult();
            internalGetResult.verifyExtensionContainingType(aVar);
            internalGetResult.extensions.a(aVar.a(), i, aVar.b(type));
            return this;
        }

        public final <Type> BuilderType addExtension(a<MessageType, List<Type>> aVar, Type type) {
            ExtendableMessage internalGetResult = internalGetResult();
            internalGetResult.verifyExtensionContainingType(aVar);
            internalGetResult.extensions.b(aVar.a(), aVar.b(type));
            return this;
        }

        public final <Type> BuilderType clearExtension(a<MessageType, ?> aVar) {
            ExtendableMessage internalGetResult = internalGetResult();
            internalGetResult.verifyExtensionContainingType(aVar);
            internalGetResult.extensions.c(aVar.a());
            return this;
        }

        /* access modifiers changed from: protected */
        public boolean parseUnknownField(h hVar, a.b bVar, ab abVar, int i) throws IOException {
            internalGetResult();
            return ae.a.mergeFieldFrom(hVar, bVar, abVar, this, i);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.GeneratedMessage.b.setField(com.google.protobuf.c$f, java.lang.Object):BuilderType
         arg types: [com.google.protobuf.c$f, java.lang.Object]
         candidates:
          com.google.protobuf.GeneratedMessage.ExtendableBuilder.setField(com.google.protobuf.c$f, java.lang.Object):BuilderType
          com.google.protobuf.GeneratedMessage.ExtendableBuilder.setField(com.google.protobuf.c$f, java.lang.Object):com.google.protobuf.s$a
          com.google.protobuf.GeneratedMessage.b.setField(com.google.protobuf.c$f, java.lang.Object):com.google.protobuf.s$a
          com.google.protobuf.s.a.setField(com.google.protobuf.c$f, java.lang.Object):com.google.protobuf.s$a
          com.google.protobuf.GeneratedMessage.b.setField(com.google.protobuf.c$f, java.lang.Object):BuilderType */
        public BuilderType setField(c.f fVar, Object obj) {
            if (!fVar.n()) {
                return (ExtendableBuilder) super.setField(fVar, obj);
            }
            ExtendableMessage internalGetResult = internalGetResult();
            internalGetResult.verifyContainingType(fVar);
            internalGetResult.extensions.a(fVar, obj);
            return this;
        }

        public BuilderType clearField(c.f fVar) {
            if (!fVar.n()) {
                return (ExtendableBuilder) super.clearField(fVar);
            }
            ExtendableMessage internalGetResult = internalGetResult();
            internalGetResult.verifyContainingType(fVar);
            internalGetResult.extensions.c(fVar);
            return this;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.GeneratedMessage.b.setRepeatedField(com.google.protobuf.c$f, int, java.lang.Object):BuilderType
         arg types: [com.google.protobuf.c$f, int, java.lang.Object]
         candidates:
          com.google.protobuf.GeneratedMessage.ExtendableBuilder.setRepeatedField(com.google.protobuf.c$f, int, java.lang.Object):BuilderType
          com.google.protobuf.GeneratedMessage.ExtendableBuilder.setRepeatedField(com.google.protobuf.c$f, int, java.lang.Object):com.google.protobuf.s$a
          com.google.protobuf.GeneratedMessage.b.setRepeatedField(com.google.protobuf.c$f, int, java.lang.Object):com.google.protobuf.s$a
          com.google.protobuf.GeneratedMessage.b.setRepeatedField(com.google.protobuf.c$f, int, java.lang.Object):BuilderType */
        public BuilderType setRepeatedField(c.f fVar, int i, Object obj) {
            if (!fVar.n()) {
                return (ExtendableBuilder) super.setRepeatedField(fVar, i, obj);
            }
            ExtendableMessage internalGetResult = internalGetResult();
            internalGetResult.verifyContainingType(fVar);
            internalGetResult.extensions.a(fVar, i, obj);
            return this;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.GeneratedMessage.b.addRepeatedField(com.google.protobuf.c$f, java.lang.Object):BuilderType
         arg types: [com.google.protobuf.c$f, java.lang.Object]
         candidates:
          com.google.protobuf.GeneratedMessage.ExtendableBuilder.addRepeatedField(com.google.protobuf.c$f, java.lang.Object):BuilderType
          com.google.protobuf.GeneratedMessage.ExtendableBuilder.addRepeatedField(com.google.protobuf.c$f, java.lang.Object):com.google.protobuf.s$a
          com.google.protobuf.GeneratedMessage.b.addRepeatedField(com.google.protobuf.c$f, java.lang.Object):com.google.protobuf.s$a
          com.google.protobuf.s.a.addRepeatedField(com.google.protobuf.c$f, java.lang.Object):com.google.protobuf.s$a
          com.google.protobuf.GeneratedMessage.b.addRepeatedField(com.google.protobuf.c$f, java.lang.Object):BuilderType */
        public BuilderType addRepeatedField(c.f fVar, Object obj) {
            if (!fVar.n()) {
                return (ExtendableBuilder) super.addRepeatedField(fVar, obj);
            }
            ExtendableMessage internalGetResult = internalGetResult();
            internalGetResult.verifyContainingType(fVar);
            internalGetResult.extensions.b(fVar, obj);
            return this;
        }

        /* access modifiers changed from: protected */
        public final void mergeExtensionFields(ExtendableMessage extendableMessage) {
            internalGetResult().extensions.a(extendableMessage.extensions);
        }
    }

    public static <ContainingType extends s, Type> a<ContainingType, Type> newGeneratedExtension() {
        return new a<>();
    }

    public static final class a<ContainingType extends s, Type> {
        private c.f a;
        private Class b;
        private Method c;
        private Method d;
        private s e;

        /* synthetic */ a() {
            this((byte) 0);
        }

        static /* synthetic */ Object a(a aVar, Object obj) {
            if (!aVar.a.d()) {
                return aVar.a(obj);
            }
            if (aVar.a.h() != c.f.b.MESSAGE && aVar.a.h() != c.f.b.ENUM) {
                return obj;
            }
            ArrayList arrayList = new ArrayList();
            for (Object a2 : (List) obj) {
                arrayList.add(aVar.a(a2));
            }
            return arrayList;
        }

        static /* synthetic */ Object c(a aVar, Object obj) {
            if (!aVar.a.d()) {
                return aVar.b(obj);
            }
            if (aVar.a.h() != c.f.b.ENUM) {
                return obj;
            }
            ArrayList arrayList = new ArrayList();
            for (Object b2 : (List) obj) {
                arrayList.add(aVar.b(b2));
            }
            return arrayList;
        }

        private a(byte b2) {
        }

        public final c.f a() {
            return this.a;
        }

        public final s b() {
            return this.e;
        }

        /* access modifiers changed from: private */
        public Object a(Object obj) {
            switch (this.a.h()) {
                case MESSAGE:
                    if (!this.b.isInstance(obj)) {
                        return this.e.newBuilderForType().mergeFrom((s) obj).build();
                    }
                    return obj;
                case ENUM:
                    return GeneratedMessage.invokeOrDie(this.c, null, (c.j) obj);
                default:
                    return obj;
            }
        }

        /* access modifiers changed from: private */
        public Object b(Object obj) {
            switch (this.a.h()) {
                case ENUM:
                    return GeneratedMessage.invokeOrDie(this.d, obj, new Object[0]);
                default:
                    return obj;
            }
        }
    }

    /* access modifiers changed from: private */
    public static Method getMethodOrDie(Class cls, String str, Class... clsArr) {
        try {
            return cls.getMethod(str, clsArr);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("Generated message class \"" + cls.getName() + "\" missing method \"" + str + "\".", e);
        }
    }

    /* access modifiers changed from: private */
    public static Object invokeOrDie(Method method, Object obj, Object... objArr) {
        try {
            return method.invoke(obj, objArr);
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Couldn't use Java reflection to implement protocol message reflection.", e);
        } catch (InvocationTargetException e2) {
            Throwable cause = e2.getCause();
            if (cause instanceof RuntimeException) {
                throw ((RuntimeException) cause);
            } else if (cause instanceof Error) {
                throw ((Error) cause);
            } else {
                throw new RuntimeException("Unexpected exception thrown by generated accessor method.", cause);
            }
        }
    }

    public static final class FieldAccessorTable {
        /* access modifiers changed from: private */
        public final c.a a;
        private final a[] b;

        private interface a {
            void addRepeated(b bVar, Object obj);

            void clear(b bVar);

            Object get(GeneratedMessage generatedMessage);

            Object getRepeated(GeneratedMessage generatedMessage, int i);

            int getRepeatedCount(GeneratedMessage generatedMessage);

            boolean has(GeneratedMessage generatedMessage);

            s.a newBuilder();

            void set(b bVar, Object obj);

            void setRepeated(b bVar, int i, Object obj);
        }

        static /* synthetic */ a a(FieldAccessorTable fieldAccessorTable, c.f fVar) {
            if (fVar.o() != fieldAccessorTable.a) {
                throw new IllegalArgumentException("FieldDescriptor does not match message type.");
            } else if (!fVar.n()) {
                return fieldAccessorTable.b[fVar.f()];
            } else {
                throw new IllegalArgumentException("This type does not have extensions.");
            }
        }

        public FieldAccessorTable(c.a aVar, String[] strArr, Class<? extends GeneratedMessage> cls, Class<? extends b> cls2) {
            this.a = aVar;
            this.b = new a[aVar.e().size()];
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.b.length) {
                    c.f fVar = aVar.e().get(i2);
                    if (fVar.d()) {
                        if (fVar.h() == c.f.b.MESSAGE) {
                            this.b[i2] = new RepeatedMessageFieldAccessor(fVar, strArr[i2], cls, cls2);
                        } else if (fVar.h() == c.f.b.ENUM) {
                            this.b[i2] = new RepeatedEnumFieldAccessor(fVar, strArr[i2], cls, cls2);
                        } else {
                            this.b[i2] = new RepeatedFieldAccessor(fVar, strArr[i2], cls, cls2);
                        }
                    } else if (fVar.h() == c.f.b.MESSAGE) {
                        this.b[i2] = new SingularMessageFieldAccessor(fVar, strArr[i2], cls, cls2);
                    } else if (fVar.h() == c.f.b.ENUM) {
                        this.b[i2] = new SingularEnumFieldAccessor(fVar, strArr[i2], cls, cls2);
                    } else {
                        this.b[i2] = new SingularFieldAccessor(fVar, strArr[i2], cls, cls2);
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }

        private static class SingularFieldAccessor implements a {
            protected final Method clearMethod;
            protected final Method getMethod;
            protected final Method hasMethod;
            protected final Method setMethod;
            protected final Class type = this.getMethod.getReturnType();

            SingularFieldAccessor(c.f fVar, String str, Class<? extends GeneratedMessage> cls, Class<? extends b> cls2) {
                this.getMethod = GeneratedMessage.getMethodOrDie(cls, "get" + str, new Class[0]);
                this.setMethod = GeneratedMessage.getMethodOrDie(cls2, "set" + str, this.type);
                this.hasMethod = GeneratedMessage.getMethodOrDie(cls, "has" + str, new Class[0]);
                this.clearMethod = GeneratedMessage.getMethodOrDie(cls2, "clear" + str, new Class[0]);
            }

            public Object get(GeneratedMessage generatedMessage) {
                return GeneratedMessage.invokeOrDie(this.getMethod, generatedMessage, new Object[0]);
            }

            public void set(b bVar, Object obj) {
                Object unused = GeneratedMessage.invokeOrDie(this.setMethod, bVar, obj);
            }

            public Object getRepeated(GeneratedMessage generatedMessage, int i) {
                throw new UnsupportedOperationException("getRepeatedField() called on a singular field.");
            }

            public void setRepeated(b bVar, int i, Object obj) {
                throw new UnsupportedOperationException("setRepeatedField() called on a singular field.");
            }

            public void addRepeated(b bVar, Object obj) {
                throw new UnsupportedOperationException("addRepeatedField() called on a singular field.");
            }

            public boolean has(GeneratedMessage generatedMessage) {
                return ((Boolean) GeneratedMessage.invokeOrDie(this.hasMethod, generatedMessage, new Object[0])).booleanValue();
            }

            public int getRepeatedCount(GeneratedMessage generatedMessage) {
                throw new UnsupportedOperationException("getRepeatedFieldSize() called on a singular field.");
            }

            public void clear(b bVar) {
                Object unused = GeneratedMessage.invokeOrDie(this.clearMethod, bVar, new Object[0]);
            }

            public s.a newBuilder() {
                throw new UnsupportedOperationException("newBuilderForField() called on a non-Message type.");
            }
        }

        private static class RepeatedFieldAccessor implements a {
            protected final Method addRepeatedMethod;
            protected final Method clearMethod;
            protected final Method getCountMethod;
            protected final Method getMethod;
            protected final Method getRepeatedMethod;
            protected final Method setRepeatedMethod;
            protected final Class type = this.getRepeatedMethod.getReturnType();

            RepeatedFieldAccessor(c.f fVar, String str, Class<? extends GeneratedMessage> cls, Class<? extends b> cls2) {
                this.getMethod = GeneratedMessage.getMethodOrDie(cls, "get" + str + "List", new Class[0]);
                this.getRepeatedMethod = GeneratedMessage.getMethodOrDie(cls, "get" + str, Integer.TYPE);
                this.setRepeatedMethod = GeneratedMessage.getMethodOrDie(cls2, "set" + str, Integer.TYPE, this.type);
                this.addRepeatedMethod = GeneratedMessage.getMethodOrDie(cls2, "add" + str, this.type);
                this.getCountMethod = GeneratedMessage.getMethodOrDie(cls, "get" + str + "Count", new Class[0]);
                this.clearMethod = GeneratedMessage.getMethodOrDie(cls2, "clear" + str, new Class[0]);
            }

            public Object get(GeneratedMessage generatedMessage) {
                return GeneratedMessage.invokeOrDie(this.getMethod, generatedMessage, new Object[0]);
            }

            public void set(b bVar, Object obj) {
                clear(bVar);
                for (Object addRepeated : (List) obj) {
                    addRepeated(bVar, addRepeated);
                }
            }

            public Object getRepeated(GeneratedMessage generatedMessage, int i) {
                return GeneratedMessage.invokeOrDie(this.getRepeatedMethod, generatedMessage, Integer.valueOf(i));
            }

            public void setRepeated(b bVar, int i, Object obj) {
                Object unused = GeneratedMessage.invokeOrDie(this.setRepeatedMethod, bVar, Integer.valueOf(i), obj);
            }

            public void addRepeated(b bVar, Object obj) {
                Object unused = GeneratedMessage.invokeOrDie(this.addRepeatedMethod, bVar, obj);
            }

            public boolean has(GeneratedMessage generatedMessage) {
                throw new UnsupportedOperationException("hasField() called on a singular field.");
            }

            public int getRepeatedCount(GeneratedMessage generatedMessage) {
                return ((Integer) GeneratedMessage.invokeOrDie(this.getCountMethod, generatedMessage, new Object[0])).intValue();
            }

            public void clear(b bVar) {
                Object unused = GeneratedMessage.invokeOrDie(this.clearMethod, bVar, new Object[0]);
            }

            public s.a newBuilder() {
                throw new UnsupportedOperationException("newBuilderForField() called on a non-Message type.");
            }
        }

        private static final class SingularEnumFieldAccessor extends SingularFieldAccessor {
            private Method getValueDescriptorMethod = GeneratedMessage.getMethodOrDie(this.type, "getValueDescriptor", new Class[0]);
            private Method valueOfMethod = GeneratedMessage.getMethodOrDie(this.type, "valueOf", c.j.class);

            SingularEnumFieldAccessor(c.f fVar, String str, Class<? extends GeneratedMessage> cls, Class<? extends b> cls2) {
                super(fVar, str, cls, cls2);
            }

            public final Object get(GeneratedMessage generatedMessage) {
                return GeneratedMessage.invokeOrDie(this.getValueDescriptorMethod, super.get(generatedMessage), new Object[0]);
            }

            public final void set(b bVar, Object obj) {
                super.set(bVar, GeneratedMessage.invokeOrDie(this.valueOfMethod, null, obj));
            }
        }

        private static final class RepeatedEnumFieldAccessor extends RepeatedFieldAccessor {
            private final Method getValueDescriptorMethod = GeneratedMessage.getMethodOrDie(this.type, "getValueDescriptor", new Class[0]);
            private final Method valueOfMethod = GeneratedMessage.getMethodOrDie(this.type, "valueOf", c.j.class);

            RepeatedEnumFieldAccessor(c.f fVar, String str, Class<? extends GeneratedMessage> cls, Class<? extends b> cls2) {
                super(fVar, str, cls, cls2);
            }

            public final Object get(GeneratedMessage generatedMessage) {
                ArrayList arrayList = new ArrayList();
                for (Object access$1400 : (List) super.get(generatedMessage)) {
                    arrayList.add(GeneratedMessage.invokeOrDie(this.getValueDescriptorMethod, access$1400, new Object[0]));
                }
                return Collections.unmodifiableList(arrayList);
            }

            public final Object getRepeated(GeneratedMessage generatedMessage, int i) {
                return GeneratedMessage.invokeOrDie(this.getValueDescriptorMethod, super.getRepeated(generatedMessage, i), new Object[0]);
            }

            public final void setRepeated(b bVar, int i, Object obj) {
                super.setRepeated(bVar, i, GeneratedMessage.invokeOrDie(this.valueOfMethod, null, obj));
            }

            public final void addRepeated(b bVar, Object obj) {
                super.addRepeated(bVar, GeneratedMessage.invokeOrDie(this.valueOfMethod, null, obj));
            }
        }

        private static final class SingularMessageFieldAccessor extends SingularFieldAccessor {
            private final Method newBuilderMethod = GeneratedMessage.getMethodOrDie(this.type, "newBuilder", new Class[0]);

            SingularMessageFieldAccessor(c.f fVar, String str, Class<? extends GeneratedMessage> cls, Class<? extends b> cls2) {
                super(fVar, str, cls, cls2);
            }

            private Object coerceType(Object obj) {
                return this.type.isInstance(obj) ? obj : ((s.a) GeneratedMessage.invokeOrDie(this.newBuilderMethod, null, new Object[0])).mergeFrom((s) obj).build();
            }

            public final void set(b bVar, Object obj) {
                super.set(bVar, coerceType(obj));
            }

            public final s.a newBuilder() {
                return (s.a) GeneratedMessage.invokeOrDie(this.newBuilderMethod, null, new Object[0]);
            }
        }

        private static final class RepeatedMessageFieldAccessor extends RepeatedFieldAccessor {
            private final Method newBuilderMethod = GeneratedMessage.getMethodOrDie(this.type, "newBuilder", new Class[0]);

            RepeatedMessageFieldAccessor(c.f fVar, String str, Class<? extends GeneratedMessage> cls, Class<? extends b> cls2) {
                super(fVar, str, cls, cls2);
            }

            private Object coerceType(Object obj) {
                return this.type.isInstance(obj) ? obj : ((s.a) GeneratedMessage.invokeOrDie(this.newBuilderMethod, null, new Object[0])).mergeFrom((s) obj).build();
            }

            public final void setRepeated(b bVar, int i, Object obj) {
                super.setRepeated(bVar, i, coerceType(obj));
            }

            public final void addRepeated(b bVar, Object obj) {
                super.addRepeated(bVar, coerceType(obj));
            }

            public final s.a newBuilder() {
                return (s.a) GeneratedMessage.invokeOrDie(this.newBuilderMethod, null, new Object[0]);
            }
        }
    }
}
