package com.google.protobuf;

import com.google.protobuf.GeneratedMessageLite;
import java.util.Collections;
import java.util.Map;

public class ab {
    private static final ab b = new ab();
    private final Map<a, GeneratedMessageLite.c<?, ?>> a;

    public static ab b() {
        return b;
    }

    public final <ContainingType extends i> GeneratedMessageLite.c<ContainingType, ?> a(ContainingType containingtype, int i) {
        return this.a.get(new a(containingtype, i));
    }

    ab(ab abVar) {
        if (abVar == b) {
            this.a = Collections.emptyMap();
        } else {
            this.a = Collections.unmodifiableMap(abVar.a);
        }
    }

    private ab() {
        this.a = Collections.emptyMap();
    }

    private static final class a {
        private final Object a;
        private final int b;

        a(Object obj, int i) {
            this.a = obj;
            this.b = i;
        }

        public final int hashCode() {
            return (System.identityHashCode(this.a) * 65535) + this.b;
        }

        public final boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (this.a == aVar.a && this.b == aVar.b) {
                return true;
            }
            return false;
        }
    }
}
