package com.google.protobuf;

import com.google.protobuf.c;
import com.google.protobuf.i;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public interface s extends i {

    public interface a extends i.a {
        a addRepeatedField(c.f fVar, Object obj);

        s build();

        a clearField(c.f fVar);

        Map<c.f, Object> getAllFields();

        c.a getDescriptorForType();

        Object getField(c.f fVar);

        a getUnknownFields();

        a mergeFrom(h hVar) throws IOException;

        a mergeFrom(s sVar);

        a mergeFrom(InputStream inputStream) throws IOException;

        a newBuilderForField(c.f fVar);

        a setField(c.f fVar, Object obj);

        a setUnknownFields(a aVar);
    }

    Map<c.f, Object> getAllFields();

    s getDefaultInstanceForType();

    c.a getDescriptorForType();

    a getUnknownFields();

    boolean hasField(c.f fVar);

    a newBuilderForType();

    a toBuilder();
}
