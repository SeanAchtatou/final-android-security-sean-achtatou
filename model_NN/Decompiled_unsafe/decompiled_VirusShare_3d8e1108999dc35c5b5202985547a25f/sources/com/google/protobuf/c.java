package com.google.protobuf;

import com.google.protobuf.DescriptorProtos;
import com.google.protobuf.g;
import com.google.protobuf.i;
import com.google.protobuf.o;
import com.google.protobuf.p;
import com.google.protobuf.s;
import com.google.protobuf.u;
import com.ptowngames.jigsaur.Jigsaur;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class c {

    private interface d {
        String a();

        String b();

        b c();

        s g();
    }

    static /* synthetic */ String a(b bVar, a aVar, String str) {
        if (aVar != null) {
            return aVar.b() + '.' + str;
        }
        return bVar.c().length() > 0 ? bVar.c() + '.' + str : str;
    }

    public static final class b {
        private DescriptorProtos.FileDescriptorProto a;
        private final a[] b;
        private final e[] c;
        private final h[] d;
        private final f[] e;
        private final b[] f;
        /* access modifiers changed from: private */
        public final g g;

        public interface a {
            f a(b bVar);
        }

        public final DescriptorProtos.FileDescriptorProto a() {
            return this.a;
        }

        public final String b() {
            return this.a.getName();
        }

        public final String c() {
            return this.a.getPackage();
        }

        public final List<a> d() {
            return Collections.unmodifiableList(Arrays.asList(this.b));
        }

        public final List<h> e() {
            return Collections.unmodifiableList(Arrays.asList(this.d));
        }

        private static b a(DescriptorProtos.FileDescriptorProto fileDescriptorProto, b[] bVarArr) throws C0016c {
            b bVar = new b(fileDescriptorProto, bVarArr, new g(bVarArr));
            if (bVarArr.length != fileDescriptorProto.getDependencyCount()) {
                throw new C0016c(bVar, "Dependencies passed to FileDescriptor.buildFrom() don't match those listed in the FileDescriptorProto.");
            }
            for (int i = 0; i < fileDescriptorProto.getDependencyCount(); i++) {
                if (!bVarArr[i].a.getName().equals(fileDescriptorProto.getDependency(i))) {
                    throw new C0016c(bVar, "Dependencies passed to FileDescriptor.buildFrom() don't match those listed in the FileDescriptorProto.");
                }
            }
            for (a a2 : bVar.b) {
                a2.h();
            }
            for (h a3 : bVar.d) {
                h.a(a3);
            }
            for (f a4 : bVar.e) {
                f.a(a4);
            }
            return bVar;
        }

        public static void a(String[] strArr, b[] bVarArr, a aVar) {
            DescriptorProtos.FileDescriptorProto fileDescriptorProto;
            StringBuilder sb = new StringBuilder();
            for (String append : strArr) {
                sb.append(append);
            }
            try {
                try {
                    try {
                        aVar.a(a(DescriptorProtos.FileDescriptorProto.parseFrom(sb.toString().getBytes("ISO-8859-1")), bVarArr));
                    } catch (C0016c e2) {
                        throw new IllegalArgumentException("Invalid embedded descriptor for \"" + fileDescriptorProto.getName() + "\".", e2);
                    }
                } catch (ac e3) {
                    throw new IllegalArgumentException("Failed to parse protocol buffer descriptor for generated code.", e3);
                }
            } catch (UnsupportedEncodingException e4) {
                throw new RuntimeException("Standard encoding ISO-8859-1 not supported by JVM.", e4);
            }
        }

        private b(DescriptorProtos.FileDescriptorProto fileDescriptorProto, b[] bVarArr, g gVar) throws C0016c {
            this.g = gVar;
            this.a = fileDescriptorProto;
            this.f = (b[]) bVarArr.clone();
            gVar.a(this.a.getPackage(), this);
            this.b = new a[fileDescriptorProto.getMessageTypeCount()];
            for (int i = 0; i < fileDescriptorProto.getMessageTypeCount(); i++) {
                this.b[i] = new a(fileDescriptorProto.getMessageType(i), this, i);
            }
            this.c = new e[fileDescriptorProto.getEnumTypeCount()];
            for (int i2 = 0; i2 < fileDescriptorProto.getEnumTypeCount(); i2++) {
                this.c[i2] = new e(fileDescriptorProto.getEnumType(i2), this, null, i2);
            }
            this.d = new h[fileDescriptorProto.getServiceCount()];
            for (int i3 = 0; i3 < fileDescriptorProto.getServiceCount(); i3++) {
                this.d[i3] = new h(fileDescriptorProto.getService(i3), this, i3);
            }
            this.e = new f[fileDescriptorProto.getExtensionCount()];
            for (int i4 = 0; i4 < fileDescriptorProto.getExtensionCount(); i4++) {
                this.e[i4] = new f(fileDescriptorProto.getExtension(i4), this, null, i4, true);
            }
        }
    }

    public static final class a implements d {
        private final int a;
        private DescriptorProtos.DescriptorProto b;
        private final String c;
        private final b d;
        private final a e;
        private final a[] f;
        private final e[] g;
        private final f[] h;
        private final f[] i;

        /* synthetic */ a(DescriptorProtos.DescriptorProto descriptorProto, b bVar, int i2) throws C0016c {
            this(descriptorProto, bVar, null, i2);
        }

        public final /* bridge */ /* synthetic */ s g() {
            return this.b;
        }

        public final String a() {
            return this.b.getName();
        }

        public final String b() {
            return this.c;
        }

        public final b c() {
            return this.d;
        }

        public final DescriptorProtos.MessageOptions d() {
            return this.b.getOptions();
        }

        public final List<f> e() {
            return Collections.unmodifiableList(Arrays.asList(this.h));
        }

        public final List<a> f() {
            return Collections.unmodifiableList(Arrays.asList(this.f));
        }

        public final boolean a(int i2) {
            for (DescriptorProtos.DescriptorProto.ExtensionRange next : this.b.getExtensionRangeList()) {
                if (next.getStart() <= i2 && i2 < next.getEnd()) {
                    return true;
                }
            }
            return false;
        }

        public final f b(int i2) {
            return (f) this.d.g.c.get(new g.a(this, i2));
        }

        private a(DescriptorProtos.DescriptorProto descriptorProto, b bVar, a aVar, int i2) throws C0016c {
            this.a = i2;
            this.b = descriptorProto;
            this.c = c.a(bVar, aVar, descriptorProto.getName());
            this.d = bVar;
            this.e = aVar;
            this.f = new a[descriptorProto.getNestedTypeCount()];
            for (int i3 = 0; i3 < descriptorProto.getNestedTypeCount(); i3++) {
                this.f[i3] = new a(descriptorProto.getNestedType(i3), bVar, this, i3);
            }
            this.g = new e[descriptorProto.getEnumTypeCount()];
            for (int i4 = 0; i4 < descriptorProto.getEnumTypeCount(); i4++) {
                this.g[i4] = new e(descriptorProto.getEnumType(i4), bVar, this, i4);
            }
            this.h = new f[descriptorProto.getFieldCount()];
            for (int i5 = 0; i5 < descriptorProto.getFieldCount(); i5++) {
                this.h[i5] = new f(descriptorProto.getField(i5), bVar, this, i5, false);
            }
            this.i = new f[descriptorProto.getExtensionCount()];
            for (int i6 = 0; i6 < descriptorProto.getExtensionCount(); i6++) {
                this.i[i6] = new f(descriptorProto.getExtension(i6), bVar, this, i6, true);
            }
            bVar.g.a(this);
        }

        /* access modifiers changed from: private */
        public void h() throws C0016c {
            for (a h2 : this.f) {
                h2.h();
            }
            for (f a2 : this.h) {
                f.a(a2);
            }
            for (f a3 : this.i) {
                f.a(a3);
            }
        }
    }

    public static final class f implements d, Comparable<f>, o.a<f> {
        private static final u.a[] a = u.a.values();
        private final int b;
        private DescriptorProtos.FieldDescriptorProto c;
        private final String d;
        private final b e;
        private final a f;
        private a g;
        private a h;
        private a i;
        private e j;
        private Object k;

        /* synthetic */ f(DescriptorProtos.FieldDescriptorProto fieldDescriptorProto, b bVar, a aVar, int i2, boolean z) throws C0016c {
            this(fieldDescriptorProto, bVar, aVar, i2, z, (byte) 0);
        }

        static /* synthetic */ void a(f fVar) throws C0016c {
            if (fVar.c.hasExtendee()) {
                d a2 = fVar.e.g.a(fVar.c.getExtendee(), fVar);
                if (!(a2 instanceof a)) {
                    throw new C0016c(fVar, '\"' + fVar.c.getExtendee() + "\" is not a message type.");
                }
                fVar.h = (a) a2;
                if (!fVar.h.a(fVar.c.getNumber())) {
                    throw new C0016c(fVar, '\"' + fVar.h.b() + "\" does not declare " + fVar.c.getNumber() + " as an extension number.");
                }
            }
            if (fVar.c.hasTypeName()) {
                d a3 = fVar.e.g.a(fVar.c.getTypeName(), fVar);
                if (!fVar.c.hasType()) {
                    if (a3 instanceof a) {
                        fVar.g = a.MESSAGE;
                    } else if (a3 instanceof e) {
                        fVar.g = a.ENUM;
                    } else {
                        throw new C0016c(fVar, '\"' + fVar.c.getTypeName() + "\" is not a type.");
                    }
                }
                if (fVar.g.a() == b.MESSAGE) {
                    if (!(a3 instanceof a)) {
                        throw new C0016c(fVar, '\"' + fVar.c.getTypeName() + "\" is not a message type.");
                    }
                    fVar.i = (a) a3;
                    if (fVar.c.hasDefaultValue()) {
                        throw new C0016c(fVar, "Messages can't have default values.");
                    }
                } else if (fVar.g.a() != b.ENUM) {
                    throw new C0016c(fVar, "Field with primitive type has type_name.");
                } else if (!(a3 instanceof e)) {
                    throw new C0016c(fVar, '\"' + fVar.c.getTypeName() + "\" is not an enum type.");
                } else {
                    fVar.j = (e) a3;
                }
            } else if (fVar.g.a() == b.MESSAGE || fVar.g.a() == b.ENUM) {
                throw new C0016c(fVar, "Field with message or enum type missing type_name.");
            }
            if (!fVar.c.hasDefaultValue()) {
                if (!fVar.d()) {
                    switch (fVar.g.a()) {
                        case ENUM:
                            fVar.k = fVar.j.d().get(0);
                            break;
                        case MESSAGE:
                            fVar.k = null;
                            break;
                        default:
                            fVar.k = b.a(fVar.g.a());
                            break;
                    }
                } else {
                    fVar.k = Collections.emptyList();
                }
            } else if (fVar.d()) {
                throw new C0016c(fVar, "Repeated fields cannot have default values.");
            } else {
                try {
                    switch (AnonymousClass1.a[fVar.g.ordinal()]) {
                        case 1:
                        case 2:
                        case 3:
                            fVar.k = Integer.valueOf(p.a(fVar.c.getDefaultValue()));
                            break;
                        case 4:
                        case 5:
                            fVar.k = Integer.valueOf(p.b(fVar.c.getDefaultValue()));
                            break;
                        case 6:
                        case 7:
                        case 8:
                            fVar.k = Long.valueOf(p.c(fVar.c.getDefaultValue()));
                            break;
                        case 9:
                        case 10:
                            fVar.k = Long.valueOf(p.d(fVar.c.getDefaultValue()));
                            break;
                        case 11:
                            if (!fVar.c.getDefaultValue().equals("inf")) {
                                if (!fVar.c.getDefaultValue().equals("-inf")) {
                                    if (!fVar.c.getDefaultValue().equals("nan")) {
                                        fVar.k = Float.valueOf(fVar.c.getDefaultValue());
                                        break;
                                    } else {
                                        fVar.k = Float.valueOf(Float.NaN);
                                        break;
                                    }
                                } else {
                                    fVar.k = Float.valueOf(Float.NEGATIVE_INFINITY);
                                    break;
                                }
                            } else {
                                fVar.k = Float.valueOf(Float.POSITIVE_INFINITY);
                                break;
                            }
                        case 12:
                            if (!fVar.c.getDefaultValue().equals("inf")) {
                                if (!fVar.c.getDefaultValue().equals("-inf")) {
                                    if (!fVar.c.getDefaultValue().equals("nan")) {
                                        fVar.k = Double.valueOf(fVar.c.getDefaultValue());
                                        break;
                                    } else {
                                        fVar.k = Double.valueOf(Double.NaN);
                                        break;
                                    }
                                } else {
                                    fVar.k = Double.valueOf(Double.NEGATIVE_INFINITY);
                                    break;
                                }
                            } else {
                                fVar.k = Double.valueOf(Double.POSITIVE_INFINITY);
                                break;
                            }
                        case 13:
                            fVar.k = Boolean.valueOf(fVar.c.getDefaultValue());
                            break;
                        case Jigsaur.Level.DIFFICULTY_FAMILY_FIELD_NUMBER /*14*/:
                            fVar.k = fVar.c.getDefaultValue();
                            break;
                        case 15:
                            fVar.k = p.a((CharSequence) fVar.c.getDefaultValue());
                            break;
                        case 16:
                            fVar.k = fVar.j.a(fVar.c.getDefaultValue());
                            if (fVar.k == null) {
                                throw new C0016c(fVar, "Unknown enum default value: \"" + fVar.c.getDefaultValue() + '\"');
                            }
                            break;
                        case DescriptorProtos.FileOptions.JAVA_GENERIC_SERVICES_FIELD_NUMBER /*17*/:
                        case DescriptorProtos.FileOptions.PY_GENERIC_SERVICES_FIELD_NUMBER /*18*/:
                            throw new C0016c(fVar, "Message type had default value.");
                    }
                } catch (p.b e2) {
                    throw new C0016c(fVar, "Couldn't parse default value: " + e2.getMessage(), e2);
                } catch (NumberFormatException e3) {
                    throw new C0016c(fVar, "Could not parse default value: \"" + fVar.c.getDefaultValue() + '\"', e3);
                }
            }
            if (!fVar.c.hasExtendee()) {
                fVar.e.g.a(fVar);
            }
            if (fVar.h != null && fVar.h.d().getMessageSetWireFormat()) {
                if (!fVar.c.hasExtendee()) {
                    throw new C0016c(fVar, "MessageSets cannot have fields, only extensions.");
                } else if (!fVar.k() || fVar.g != a.MESSAGE) {
                    throw new C0016c(fVar, "Extensions of MessageSets must be optional messages.");
                }
            }
        }

        public final /* bridge */ /* synthetic */ int compareTo(Object obj) {
            f fVar = (f) obj;
            if (fVar.h == this.h) {
                return this.c.getNumber() - fVar.c.getNumber();
            }
            throw new IllegalArgumentException("FieldDescriptors can only be compared to other FieldDescriptors for fields of the same message type.");
        }

        public final /* bridge */ /* synthetic */ s g() {
            return this.c;
        }

        public final int f() {
            return this.b;
        }

        public final String a() {
            return this.c.getName();
        }

        public final int a_() {
            return this.c.getNumber();
        }

        public final String b() {
            return this.d;
        }

        public final b h() {
            return this.g.a();
        }

        public final u.b b_() {
            return c_().b();
        }

        public final b c() {
            return this.e;
        }

        public final a i() {
            return this.g;
        }

        public final u.a c_() {
            return a[this.g.ordinal()];
        }

        static {
            if (a.values().length != DescriptorProtos.FieldDescriptorProto.b.values().length) {
                throw new RuntimeException("descriptor.proto has a new declared type but Desrciptors.java wasn't updated.");
            }
        }

        public final boolean j() {
            return this.c.getLabel() == DescriptorProtos.FieldDescriptorProto.a.LABEL_REQUIRED;
        }

        public final boolean k() {
            return this.c.getLabel() == DescriptorProtos.FieldDescriptorProto.a.LABEL_OPTIONAL;
        }

        public final boolean d() {
            return this.c.getLabel() == DescriptorProtos.FieldDescriptorProto.a.LABEL_REPEATED;
        }

        public final boolean e() {
            return this.c.getOptions().getPacked();
        }

        public final boolean l() {
            return d() && c_().a();
        }

        public final Object m() {
            if (this.g.a() != b.MESSAGE) {
                return this.k;
            }
            throw new UnsupportedOperationException("FieldDescriptor.getDefaultValue() called on an embedded message field.");
        }

        public final boolean n() {
            return this.c.hasExtendee();
        }

        public final a o() {
            return this.h;
        }

        public final a p() {
            if (this.c.hasExtendee()) {
                return this.f;
            }
            throw new UnsupportedOperationException("This field is not an extension.");
        }

        public final a q() {
            if (this.g.a() == b.MESSAGE) {
                return this.i;
            }
            throw new UnsupportedOperationException("This field is not of message type.");
        }

        public final e r() {
            if (this.g.a() == b.ENUM) {
                return this.j;
            }
            throw new UnsupportedOperationException("This field is not of enum type.");
        }

        public enum a {
            DOUBLE(b.DOUBLE),
            FLOAT(b.FLOAT),
            INT64(b.LONG),
            UINT64(b.LONG),
            INT32(b.INT),
            FIXED64(b.LONG),
            FIXED32(b.INT),
            BOOL(b.BOOLEAN),
            STRING(b.STRING),
            GROUP(b.MESSAGE),
            MESSAGE(b.MESSAGE),
            BYTES(b.BYTE_STRING),
            UINT32(b.INT),
            ENUM(b.ENUM),
            SFIXED32(b.INT),
            SFIXED64(b.LONG),
            SINT32(b.INT),
            SINT64(b.LONG);
            
            private b s;

            private a(b bVar) {
                this.s = bVar;
            }

            public final b a() {
                return this.s;
            }

            public static a a(DescriptorProtos.FieldDescriptorProto.b bVar) {
                return values()[bVar.d_() - 1];
            }
        }

        public enum b {
            INT(0),
            LONG(0L),
            FLOAT(Float.valueOf(0.0f)),
            DOUBLE(Double.valueOf(0.0d)),
            BOOLEAN(false),
            STRING(""),
            BYTE_STRING(q.a),
            ENUM(null),
            MESSAGE(null);
            
            private final Object j;

            private b(Object obj) {
                this.j = obj;
            }
        }

        private f(DescriptorProtos.FieldDescriptorProto fieldDescriptorProto, b bVar, a aVar, int i2, boolean z, byte b2) throws C0016c {
            this.b = i2;
            this.c = fieldDescriptorProto;
            this.d = c.a(bVar, aVar, fieldDescriptorProto.getName());
            this.e = bVar;
            if (fieldDescriptorProto.hasType()) {
                this.g = a.a(fieldDescriptorProto.getType());
            }
            if (this.c.getNumber() <= 0) {
                throw new C0016c(this, "Field numbers must be positive integers.");
            } else if (!fieldDescriptorProto.getOptions().getPacked() || l()) {
                if (z) {
                    if (!fieldDescriptorProto.hasExtendee()) {
                        throw new C0016c(this, "FieldDescriptorProto.extendee not set for extension field.");
                    }
                    this.h = null;
                    if (aVar != null) {
                        this.f = aVar;
                    } else {
                        this.f = null;
                    }
                } else if (fieldDescriptorProto.hasExtendee()) {
                    throw new C0016c(this, "FieldDescriptorProto.extendee set for non-extension field.");
                } else {
                    this.h = aVar;
                    this.f = null;
                }
                bVar.g.a((d) this);
            } else {
                throw new C0016c(this, "[packed = true] can only be specified for repeated primitive fields.");
            }
        }

        public final i.a a(i.a aVar, i iVar) {
            return ((s.a) aVar).mergeFrom((s) iVar);
        }
    }

    /* renamed from: com.google.protobuf.c$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[f.a.values().length];

        static {
            b = new int[f.b.values().length];
            try {
                b[f.b.ENUM.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                b[f.b.MESSAGE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[f.a.INT32.ordinal()] = 1;
            } catch (NoSuchFieldError e3) {
            }
            try {
                a[f.a.SINT32.ordinal()] = 2;
            } catch (NoSuchFieldError e4) {
            }
            try {
                a[f.a.SFIXED32.ordinal()] = 3;
            } catch (NoSuchFieldError e5) {
            }
            try {
                a[f.a.UINT32.ordinal()] = 4;
            } catch (NoSuchFieldError e6) {
            }
            try {
                a[f.a.FIXED32.ordinal()] = 5;
            } catch (NoSuchFieldError e7) {
            }
            try {
                a[f.a.INT64.ordinal()] = 6;
            } catch (NoSuchFieldError e8) {
            }
            try {
                a[f.a.SINT64.ordinal()] = 7;
            } catch (NoSuchFieldError e9) {
            }
            try {
                a[f.a.SFIXED64.ordinal()] = 8;
            } catch (NoSuchFieldError e10) {
            }
            try {
                a[f.a.UINT64.ordinal()] = 9;
            } catch (NoSuchFieldError e11) {
            }
            try {
                a[f.a.FIXED64.ordinal()] = 10;
            } catch (NoSuchFieldError e12) {
            }
            try {
                a[f.a.FLOAT.ordinal()] = 11;
            } catch (NoSuchFieldError e13) {
            }
            try {
                a[f.a.DOUBLE.ordinal()] = 12;
            } catch (NoSuchFieldError e14) {
            }
            try {
                a[f.a.BOOL.ordinal()] = 13;
            } catch (NoSuchFieldError e15) {
            }
            try {
                a[f.a.STRING.ordinal()] = 14;
            } catch (NoSuchFieldError e16) {
            }
            try {
                a[f.a.BYTES.ordinal()] = 15;
            } catch (NoSuchFieldError e17) {
            }
            try {
                a[f.a.ENUM.ordinal()] = 16;
            } catch (NoSuchFieldError e18) {
            }
            try {
                a[f.a.MESSAGE.ordinal()] = 17;
            } catch (NoSuchFieldError e19) {
            }
            try {
                a[f.a.GROUP.ordinal()] = 18;
            } catch (NoSuchFieldError e20) {
            }
        }
    }

    public static final class e implements d, g.b<j> {
        private final int a;
        private DescriptorProtos.EnumDescriptorProto b;
        private final String c;
        private final b d;
        private final a e;
        private j[] f;

        /* synthetic */ e(DescriptorProtos.EnumDescriptorProto enumDescriptorProto, b bVar, a aVar, int i) throws C0016c {
            this(enumDescriptorProto, bVar, aVar, i, (byte) 0);
        }

        public final /* bridge */ /* synthetic */ s g() {
            return this.b;
        }

        public final String a() {
            return this.b.getName();
        }

        public final String b() {
            return this.c;
        }

        public final b c() {
            return this.d;
        }

        public final List<j> d() {
            return Collections.unmodifiableList(Arrays.asList(this.f));
        }

        public final j a(String str) {
            d a2 = this.d.g.a(this.c + '.' + str);
            if (a2 == null || !(a2 instanceof j)) {
                return null;
            }
            return (j) a2;
        }

        /* renamed from: b */
        public final j a(int i) {
            return (j) this.d.g.d.get(new g.a(this, i));
        }

        private e(DescriptorProtos.EnumDescriptorProto enumDescriptorProto, b bVar, a aVar, int i, byte b2) throws C0016c {
            this.a = i;
            this.b = enumDescriptorProto;
            this.c = c.a(bVar, aVar, enumDescriptorProto.getName());
            this.d = bVar;
            this.e = aVar;
            if (enumDescriptorProto.getValueCount() == 0) {
                throw new C0016c(this, "Enums must contain at least one value.");
            }
            this.f = new j[enumDescriptorProto.getValueCount()];
            for (int i2 = 0; i2 < enumDescriptorProto.getValueCount(); i2++) {
                this.f[i2] = new j(enumDescriptorProto.getValue(i2), bVar, this, i2);
            }
            bVar.g.a(this);
        }
    }

    public static final class j implements d, g.a {
        private final int a;
        private DescriptorProtos.EnumValueDescriptorProto b;
        private final String c;
        private final b d;
        private final e e;

        /* synthetic */ j(DescriptorProtos.EnumValueDescriptorProto enumValueDescriptorProto, b bVar, e eVar, int i) throws C0016c {
            this(enumValueDescriptorProto, bVar, eVar, i, (byte) 0);
        }

        public final /* bridge */ /* synthetic */ s g() {
            return this.b;
        }

        public final String a() {
            return this.b.getName();
        }

        public final int d_() {
            return this.b.getNumber();
        }

        public final String b() {
            return this.c;
        }

        public final b c() {
            return this.d;
        }

        public final e d() {
            return this.e;
        }

        private j(DescriptorProtos.EnumValueDescriptorProto enumValueDescriptorProto, b bVar, e eVar, int i, byte b2) throws C0016c {
            this.a = i;
            this.b = enumValueDescriptorProto;
            this.d = bVar;
            this.e = eVar;
            this.c = eVar.b() + '.' + enumValueDescriptorProto.getName();
            bVar.g.a((d) this);
            bVar.g.a(this);
        }
    }

    public static final class h implements d {
        private final int a;
        private DescriptorProtos.ServiceDescriptorProto b;
        private final String c;
        private final b d;
        private i[] e;

        /* synthetic */ h(DescriptorProtos.ServiceDescriptorProto serviceDescriptorProto, b bVar, int i) throws C0016c {
            this(serviceDescriptorProto, bVar, i, (byte) 0);
        }

        static /* synthetic */ void a(h hVar) throws C0016c {
            for (i a2 : hVar.e) {
                i.a(a2);
            }
        }

        public final /* bridge */ /* synthetic */ s g() {
            return this.b;
        }

        public final String a() {
            return this.b.getName();
        }

        public final String b() {
            return this.c;
        }

        public final b c() {
            return this.d;
        }

        public final List<i> d() {
            return Collections.unmodifiableList(Arrays.asList(this.e));
        }

        private h(DescriptorProtos.ServiceDescriptorProto serviceDescriptorProto, b bVar, int i, byte b2) throws C0016c {
            this.a = i;
            this.b = serviceDescriptorProto;
            this.c = c.a(bVar, null, serviceDescriptorProto.getName());
            this.d = bVar;
            this.e = new i[serviceDescriptorProto.getMethodCount()];
            for (int i2 = 0; i2 < serviceDescriptorProto.getMethodCount(); i2++) {
                this.e[i2] = new i(serviceDescriptorProto.getMethod(i2), bVar, this, i2);
            }
            bVar.g.a(this);
        }
    }

    public static final class i implements d {
        private final int a;
        private DescriptorProtos.MethodDescriptorProto b;
        private final String c;
        private final b d;
        private final h e;
        private a f;
        private a g;

        /* synthetic */ i(DescriptorProtos.MethodDescriptorProto methodDescriptorProto, b bVar, h hVar, int i) throws C0016c {
            this(methodDescriptorProto, bVar, hVar, i, (byte) 0);
        }

        static /* synthetic */ void a(i iVar) throws C0016c {
            d a2 = iVar.d.g.a(iVar.b.getInputType(), iVar);
            if (!(a2 instanceof a)) {
                throw new C0016c(iVar, '\"' + iVar.b.getInputType() + "\" is not a message type.");
            }
            iVar.f = (a) a2;
            d a3 = iVar.d.g.a(iVar.b.getOutputType(), iVar);
            if (!(a3 instanceof a)) {
                throw new C0016c(iVar, '\"' + iVar.b.getOutputType() + "\" is not a message type.");
            }
            iVar.g = (a) a3;
        }

        public final /* bridge */ /* synthetic */ s g() {
            return this.b;
        }

        public final String a() {
            return this.b.getName();
        }

        public final String b() {
            return this.c;
        }

        public final b c() {
            return this.d;
        }

        private i(DescriptorProtos.MethodDescriptorProto methodDescriptorProto, b bVar, h hVar, int i, byte b2) throws C0016c {
            this.a = i;
            this.b = methodDescriptorProto;
            this.d = bVar;
            this.e = hVar;
            this.c = hVar.b() + '.' + methodDescriptorProto.getName();
            bVar.g.a(this);
        }
    }

    /* renamed from: com.google.protobuf.c$c  reason: collision with other inner class name */
    public static class C0016c extends Exception {
        private final String a;
        private final s b;
        private final String c;

        /* synthetic */ C0016c(b bVar, String str) {
            this(bVar, str, (byte) 0);
        }

        /* synthetic */ C0016c(d dVar, String str) {
            this(dVar, str, (byte) 0);
        }

        /* synthetic */ C0016c(d dVar, String str, Throwable th) {
            this(dVar, str, th, (byte) 0);
        }

        private C0016c(d dVar, String str, byte b2) {
            super(dVar.b() + ": " + str);
            this.a = dVar.b();
            this.b = dVar.g();
            this.c = str;
        }

        private C0016c(d dVar, String str, Throwable th, byte b2) {
            this(dVar, str, (byte) 0);
            initCause(th);
        }

        private C0016c(b bVar, String str, byte b2) {
            super(bVar.b() + ": " + str);
            this.a = bVar.b();
            this.b = bVar.a();
            this.c = str;
        }
    }

    private static final class g {
        private static /* synthetic */ boolean e = (!c.class.desiredAssertionStatus());
        private final g[] a;
        private final Map<String, d> b = new HashMap();
        /* access modifiers changed from: private */
        public final Map<a, f> c = new HashMap();
        /* access modifiers changed from: private */
        public final Map<a, j> d = new HashMap();

        g(b[] bVarArr) {
            this.a = new g[bVarArr.length];
            for (int i = 0; i < bVarArr.length; i++) {
                this.a[i] = bVarArr[i].g;
            }
            for (b bVar : bVarArr) {
                try {
                    a(bVar.c(), bVar);
                } catch (C0016c e2) {
                    if (!e) {
                        throw new AssertionError();
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public final d a(String str) {
            d dVar = this.b.get(str);
            if (dVar != null) {
                return dVar;
            }
            for (g gVar : this.a) {
                d dVar2 = gVar.b.get(str);
                if (dVar2 != null) {
                    return dVar2;
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        public final d a(String str, d dVar) throws C0016c {
            String substring;
            d a2;
            if (str.startsWith(".")) {
                a2 = a(str.substring(1));
            } else {
                int indexOf = str.indexOf(46);
                if (indexOf == -1) {
                    substring = str;
                } else {
                    substring = str.substring(0, indexOf);
                }
                StringBuilder sb = new StringBuilder(dVar.b());
                while (true) {
                    int lastIndexOf = sb.lastIndexOf(".");
                    if (lastIndexOf == -1) {
                        a2 = a(str);
                        break;
                    }
                    sb.setLength(lastIndexOf + 1);
                    sb.append(substring);
                    d a3 = a(sb.toString());
                    if (a3 == null) {
                        sb.setLength(lastIndexOf);
                    } else if (indexOf != -1) {
                        sb.setLength(lastIndexOf + 1);
                        sb.append(str);
                        a2 = a(sb.toString());
                    } else {
                        a2 = a3;
                    }
                }
            }
            if (a2 != null) {
                return a2;
            }
            throw new C0016c(dVar, '\"' + str + "\" is not defined.");
        }

        /* access modifiers changed from: package-private */
        public final void a(d dVar) throws C0016c {
            String a2 = dVar.a();
            if (a2.length() == 0) {
                throw new C0016c(dVar, "Missing name.");
            }
            boolean z = true;
            for (int i = 0; i < a2.length(); i++) {
                char charAt = a2.charAt(i);
                if (charAt >= 128) {
                    z = false;
                }
                if (!Character.isLetter(charAt) && charAt != '_' && (!Character.isDigit(charAt) || i <= 0)) {
                    z = false;
                }
            }
            if (!z) {
                throw new C0016c(dVar, '\"' + a2 + "\" is not a valid identifier.");
            }
            String b2 = dVar.b();
            int lastIndexOf = b2.lastIndexOf(46);
            d put = this.b.put(b2, dVar);
            if (put != null) {
                this.b.put(b2, put);
                if (dVar.c() != put.c()) {
                    throw new C0016c(dVar, '\"' + b2 + "\" is already defined in file \"" + put.c().b() + "\".");
                } else if (lastIndexOf == -1) {
                    throw new C0016c(dVar, '\"' + b2 + "\" is already defined.");
                } else {
                    throw new C0016c(dVar, '\"' + b2.substring(lastIndexOf + 1) + "\" is already defined in \"" + b2.substring(0, lastIndexOf) + "\".");
                }
            }
        }

        private static final class b implements d {
            private final String a;
            private final String b;
            private final b c;

            public final s g() {
                return this.c.a();
            }

            public final String a() {
                return this.a;
            }

            public final String b() {
                return this.b;
            }

            public final b c() {
                return this.c;
            }

            b(String str, String str2, b bVar) {
                this.c = bVar;
                this.b = str2;
                this.a = str;
            }
        }

        /* access modifiers changed from: package-private */
        public final void a(String str, b bVar) throws C0016c {
            String substring;
            int lastIndexOf = str.lastIndexOf(46);
            if (lastIndexOf == -1) {
                substring = str;
            } else {
                a(str.substring(0, lastIndexOf), bVar);
                substring = str.substring(lastIndexOf + 1);
            }
            d put = this.b.put(str, new b(substring, str, bVar));
            if (put != null) {
                this.b.put(str, put);
                if (!(put instanceof b)) {
                    throw new C0016c(bVar, '\"' + substring + "\" is already defined (as something other than a " + "package) in file \"" + put.c().b() + "\".");
                }
            }
        }

        private static final class a {
            private final d a;
            private final int b;

            a(d dVar, int i) {
                this.a = dVar;
                this.b = i;
            }

            public final int hashCode() {
                return (this.a.hashCode() * 65535) + this.b;
            }

            public final boolean equals(Object obj) {
                if (!(obj instanceof a)) {
                    return false;
                }
                a aVar = (a) obj;
                if (this.a == aVar.a && this.b == aVar.b) {
                    return true;
                }
                return false;
            }
        }

        /* access modifiers changed from: package-private */
        public final void a(f fVar) throws C0016c {
            a aVar = new a(fVar.o(), fVar.a_());
            f put = this.c.put(aVar, fVar);
            if (put != null) {
                this.c.put(aVar, put);
                throw new C0016c(fVar, "Field number " + fVar.a_() + "has already been used in \"" + fVar.o().b() + "\" by field \"" + put.a() + "\".");
            }
        }

        /* access modifiers changed from: package-private */
        public final void a(j jVar) {
            a aVar = new a(jVar.d(), jVar.d_());
            j put = this.d.put(aVar, jVar);
            if (put != null) {
                this.d.put(aVar, put);
            }
        }
    }
}
