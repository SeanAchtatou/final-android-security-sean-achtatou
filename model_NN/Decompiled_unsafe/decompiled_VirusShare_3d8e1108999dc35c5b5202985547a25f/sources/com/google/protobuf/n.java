package com.google.protobuf;

import com.google.protobuf.i;
import com.google.protobuf.q;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;

public abstract class n implements i {
    public q toByteString() {
        try {
            q.a b = q.b(getSerializedSize());
            writeTo(b.b());
            return b.a();
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a ByteString threw an IOException (should never happen).", e);
        }
    }

    public byte[] toByteArray() {
        try {
            byte[] bArr = new byte[getSerializedSize()];
            x a2 = x.a(bArr);
            writeTo(a2);
            a2.b();
            return bArr;
        } catch (IOException e) {
            throw new RuntimeException("Serializing to a byte array threw an IOException (should never happen).", e);
        }
    }

    public void writeTo(OutputStream outputStream) throws IOException {
        x a2 = x.a(outputStream, x.a(getSerializedSize()));
        writeTo(a2);
        a2.a();
    }

    public void writeDelimitedTo(OutputStream outputStream) throws IOException {
        int serializedSize = getSerializedSize();
        x a2 = x.a(outputStream, x.a(x.i(serializedSize) + serializedSize));
        a2.h(serializedSize);
        writeTo(a2);
        a2.a();
    }

    public static abstract class a<BuilderType extends a> implements i.a {
        public abstract BuilderType clone();

        public abstract BuilderType mergeFrom(h hVar, ab abVar) throws IOException;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType */
        public BuilderType mergeFrom(h hVar) throws IOException {
            return mergeFrom(hVar, ab.b());
        }

        public BuilderType mergeFrom(q qVar) throws ac {
            try {
                h d = qVar.d();
                mergeFrom(d);
                d.a(0);
                return this;
            } catch (ac e) {
                throw e;
            } catch (IOException e2) {
                throw new RuntimeException("Reading from a ByteString threw an IOException (should never happen).", e2);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType */
        public BuilderType mergeFrom(q qVar, ab abVar) throws ac {
            try {
                h d = qVar.d();
                mergeFrom(d, abVar);
                d.a(0);
                return this;
            } catch (ac e) {
                throw e;
            } catch (IOException e2) {
                throw new RuntimeException("Reading from a ByteString threw an IOException (should never happen).", e2);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.n.a.mergeFrom(byte[], int, int):BuilderType
         arg types: [byte[], int, int]
         candidates:
          com.google.protobuf.n.a.mergeFrom(byte[], int, int):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], int, int):BuilderType */
        public BuilderType mergeFrom(byte[] bArr) throws ac {
            return mergeFrom(bArr, 0, bArr.length);
        }

        public BuilderType mergeFrom(byte[] bArr, int i, int i2) throws ac {
            try {
                h a = h.a(bArr, i, i2);
                mergeFrom(a);
                a.a(0);
                return this;
            } catch (ac e) {
                throw e;
            } catch (IOException e2) {
                throw new RuntimeException("Reading from a byte array threw an IOException (should never happen).", e2);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.n.a.mergeFrom(byte[], int, int, com.google.protobuf.ab):BuilderType
         arg types: [byte[], int, int, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.n.a.mergeFrom(byte[], int, int, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], int, int, com.google.protobuf.ab):BuilderType */
        public BuilderType mergeFrom(byte[] bArr, ab abVar) throws ac {
            return mergeFrom(bArr, 0, bArr.length, abVar);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType */
        public BuilderType mergeFrom(byte[] bArr, int i, int i2, ab abVar) throws ac {
            try {
                h a = h.a(bArr, i, i2);
                mergeFrom(a, abVar);
                a.a(0);
                return this;
            } catch (ac e) {
                throw e;
            } catch (IOException e2) {
                throw new RuntimeException("Reading from a byte array threw an IOException (should never happen).", e2);
            }
        }

        public BuilderType mergeFrom(InputStream inputStream) throws IOException {
            h a = h.a(inputStream);
            mergeFrom(a);
            a.a(0);
            return this;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.h, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType */
        public BuilderType mergeFrom(InputStream inputStream, ab abVar) throws IOException {
            h a = h.a(inputStream);
            mergeFrom(a, abVar);
            a.a(0);
            return this;
        }

        /* renamed from: com.google.protobuf.n$a$a  reason: collision with other inner class name */
        static final class C0017a extends FilterInputStream {
            private int a;

            C0017a(InputStream inputStream, int i) {
                super(inputStream);
                this.a = i;
            }

            public final int available() throws IOException {
                return Math.min(super.available(), this.a);
            }

            public final int read() throws IOException {
                if (this.a <= 0) {
                    return -1;
                }
                int read = super.read();
                if (read < 0) {
                    return read;
                }
                this.a--;
                return read;
            }

            public final int read(byte[] bArr, int i, int i2) throws IOException {
                if (this.a <= 0) {
                    return -1;
                }
                int read = super.read(bArr, i, Math.min(i2, this.a));
                if (read < 0) {
                    return read;
                }
                this.a -= read;
                return read;
            }

            public final long skip(long j) throws IOException {
                long skip = super.skip(Math.min(j, (long) this.a));
                if (skip >= 0) {
                    this.a = (int) (((long) this.a) - skip);
                }
                return skip;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.n$a$a, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public boolean mergeDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            int read = inputStream.read();
            if (read == -1) {
                return false;
            }
            if ((read & 128) != 0) {
                read &= 127;
                int i = 7;
                while (true) {
                    if (i < 32) {
                        int read2 = inputStream.read();
                        if (read2 != -1) {
                            read |= (read2 & 127) << i;
                            if ((read2 & 128) == 0) {
                                break;
                            }
                            i += 7;
                        } else {
                            throw ac.a();
                        }
                    } else {
                        while (i < 64) {
                            int read3 = inputStream.read();
                            if (read3 == -1) {
                                throw ac.a();
                            } else if ((read3 & 128) != 0) {
                                i += 7;
                            }
                        }
                        throw ac.c();
                    }
                }
            }
            mergeFrom((InputStream) new C0017a(inputStream, read), abVar);
            return true;
        }

        public boolean mergeDelimitedFrom(InputStream inputStream) throws IOException {
            return mergeDelimitedFrom(inputStream, ab.b());
        }

        protected static e newUninitializedMessageException(i iVar) {
            return new e();
        }

        protected static <T> void addAll(Iterable<T> iterable, Collection<? super T> collection) {
            for (T t : iterable) {
                if (t == null) {
                    throw new NullPointerException();
                }
            }
            if (iterable instanceof Collection) {
                collection.addAll((Collection) iterable);
                return;
            }
            for (T add : iterable) {
                collection.add(add);
            }
        }
    }
}
