package com.google.protobuf;

import com.google.protobuf.c;
import com.google.protobuf.g;
import com.google.protobuf.i;
import com.google.protobuf.n;
import com.google.protobuf.o;
import com.google.protobuf.u;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public abstract class GeneratedMessageLite extends n {
    protected GeneratedMessageLite() {
    }

    public static abstract class b<MessageType extends GeneratedMessageLite, BuilderType extends b> extends n.a<BuilderType> {
        public abstract MessageType getDefaultInstanceForType();

        /* access modifiers changed from: protected */
        public abstract MessageType internalGetResult();

        public abstract BuilderType mergeFrom(MessageType messagetype);

        protected b() {
        }

        public BuilderType clone() {
            throw new UnsupportedOperationException("This is supposed to be overridden by subclasses.");
        }

        /* access modifiers changed from: protected */
        public boolean parseUnknownField(h hVar, ab abVar, int i) throws IOException {
            return hVar.b(i);
        }
    }

    public static abstract class ExtendableMessage<MessageType extends ExtendableMessage<MessageType>> extends GeneratedMessageLite {
        /* access modifiers changed from: private */
        public final o<a> extensions = o.a();

        protected ExtendableMessage() {
        }

        /* access modifiers changed from: private */
        public void verifyExtensionContainingType(c<MessageType, ?> cVar) {
            if (cVar.a() != getDefaultInstanceForType()) {
                throw new IllegalArgumentException("This extension is for a different message type.  Please make sure that you are not suppressing any generics type warnings.");
            }
        }

        public final boolean hasExtension(c<MessageType, ?> cVar) {
            verifyExtensionContainingType(cVar);
            return this.extensions.a(cVar.d);
        }

        public final <Type> int getExtensionCount(c<MessageType, List<Type>> cVar) {
            verifyExtensionContainingType(cVar);
            return this.extensions.d(cVar.d);
        }

        public final <Type> Type getExtension(c<MessageType, Type> cVar) {
            verifyExtensionContainingType(cVar);
            Type b = this.extensions.b((c.f) cVar.d);
            if (b == null) {
                return cVar.b;
            }
            return b;
        }

        public final <Type> Type getExtension(c<MessageType, List<Type>> cVar, int i) {
            verifyExtensionContainingType(cVar);
            return this.extensions.a(cVar.d, i);
        }

        /* access modifiers changed from: protected */
        public boolean extensionsAreInitialized() {
            return this.extensions.g();
        }

        protected class a {
            private final Iterator<Map.Entry<a, Object>> a;
            private Map.Entry<a, Object> b;
            private final boolean c;

            /* synthetic */ a(ExtendableMessage extendableMessage, boolean z) {
                this(z, (byte) 0);
            }

            private a(boolean z, byte b2) {
                this.a = ExtendableMessage.this.extensions.f();
                if (this.a.hasNext()) {
                    this.b = this.a.next();
                }
                this.c = z;
            }
        }

        /* access modifiers changed from: protected */
        public ExtendableMessage<MessageType>.a newExtensionWriter() {
            return new a(this, false);
        }

        /* access modifiers changed from: protected */
        public ExtendableMessage<MessageType>.a newMessageSetExtensionWriter() {
            return new a(this, true);
        }

        /* access modifiers changed from: protected */
        public int extensionsSerializedSize() {
            return this.extensions.h();
        }

        /* access modifiers changed from: protected */
        public int extensionsSerializedSizeAsMessageSet() {
            return this.extensions.i();
        }
    }

    public static abstract class ExtendableBuilder<MessageType extends ExtendableMessage<MessageType>, BuilderType extends ExtendableBuilder<MessageType, BuilderType>> extends b<MessageType, BuilderType> {
        /* access modifiers changed from: protected */
        public abstract MessageType internalGetResult();

        protected ExtendableBuilder() {
        }

        public BuilderType clone() {
            throw new UnsupportedOperationException("This is supposed to be overridden by subclasses.");
        }

        public final boolean hasExtension(c<MessageType, ?> cVar) {
            return internalGetResult().hasExtension(cVar);
        }

        public final <Type> int getExtensionCount(c<MessageType, List<Type>> cVar) {
            return internalGetResult().getExtensionCount(cVar);
        }

        public final <Type> Type getExtension(c<MessageType, Type> cVar) {
            return internalGetResult().getExtension(cVar);
        }

        /* JADX WARN: Type inference failed for: r2v0, types: [com.google.protobuf.GeneratedMessageLite$c, com.google.protobuf.GeneratedMessageLite$c<MessageType, java.util.List<Type>>] */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final <Type> Type getExtension(com.google.protobuf.GeneratedMessageLite.c<MessageType, java.util.List<Type>> r2, int r3) {
            /*
                r1 = this;
                com.google.protobuf.GeneratedMessageLite$ExtendableMessage r0 = r1.internalGetResult()
                java.lang.Object r0 = r0.getExtension(r2, r3)
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.GeneratedMessageLite.ExtendableBuilder.getExtension(com.google.protobuf.GeneratedMessageLite$c, int):java.lang.Object");
        }

        public final <Type> BuilderType setExtension(c<MessageType, Type> cVar, Type type) {
            ExtendableMessage internalGetResult = internalGetResult();
            internalGetResult.verifyExtensionContainingType(cVar);
            internalGetResult.extensions.a(cVar.d, type);
            return this;
        }

        public final <Type> BuilderType setExtension(c<MessageType, List<Type>> cVar, int i, Type type) {
            ExtendableMessage internalGetResult = internalGetResult();
            internalGetResult.verifyExtensionContainingType(cVar);
            internalGetResult.extensions.a(cVar.d, i, type);
            return this;
        }

        public final <Type> BuilderType addExtension(c<MessageType, List<Type>> cVar, Type type) {
            ExtendableMessage internalGetResult = internalGetResult();
            internalGetResult.verifyExtensionContainingType(cVar);
            internalGetResult.extensions.b(cVar.d, type);
            return this;
        }

        public final <Type> BuilderType clearExtension(c<MessageType, ?> cVar) {
            ExtendableMessage internalGetResult = internalGetResult();
            internalGetResult.verifyExtensionContainingType(cVar);
            internalGetResult.extensions.c(cVar.d);
            return this;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.o.a(com.google.protobuf.u$a, boolean):int
         arg types: [com.google.protobuf.u$a, int]
         candidates:
          com.google.protobuf.o.a(com.google.protobuf.h, com.google.protobuf.u$a):java.lang.Object
          com.google.protobuf.o.a(com.google.protobuf.u$a, java.lang.Object):void
          com.google.protobuf.o.a(com.google.protobuf.o$a, int):java.lang.Object
          com.google.protobuf.o.a(com.google.protobuf.o$a, java.lang.Object):void
          com.google.protobuf.o.a(com.google.protobuf.u$a, boolean):int */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Removed duplicated region for block: B:15:0x005d  */
        /* JADX WARNING: Removed duplicated region for block: B:6:0x002d  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean parseUnknownField(com.google.protobuf.h r7, com.google.protobuf.ab r8, int r9) throws java.io.IOException {
            /*
                r6 = this;
                r0 = 0
                r1 = 1
                com.google.protobuf.GeneratedMessageLite$ExtendableMessage r2 = r6.internalGetResult()
                com.google.protobuf.o r3 = r2.extensions
                int r2 = com.google.protobuf.u.a(r9)
                int r4 = com.google.protobuf.u.b(r9)
                com.google.protobuf.GeneratedMessageLite r5 = r6.getDefaultInstanceForType()
                com.google.protobuf.GeneratedMessageLite$c r4 = r8.a(r5, r4)
                if (r4 == 0) goto L_0x005b
                com.google.protobuf.GeneratedMessageLite$a r5 = r4.d
                com.google.protobuf.u$a r5 = r5.c_()
                int r5 = com.google.protobuf.o.a(r5, r0)
                if (r2 != r5) goto L_0x0032
                r2 = r0
            L_0x002b:
                if (r2 == 0) goto L_0x005d
                boolean r0 = r7.b(r9)
            L_0x0031:
                return r0
            L_0x0032:
                com.google.protobuf.GeneratedMessageLite$a r5 = r4.d
                boolean r5 = r5.d
                if (r5 == 0) goto L_0x005b
                com.google.protobuf.GeneratedMessageLite$a r5 = r4.d
                com.google.protobuf.u$a r5 = r5.c
                boolean r5 = r5.a()
                if (r5 == 0) goto L_0x005b
                com.google.protobuf.GeneratedMessageLite$a r5 = r4.d
                com.google.protobuf.u$a r5 = r5.c_()
                int r5 = com.google.protobuf.o.a(r5, r1)
                if (r2 != r5) goto L_0x005b
                r2 = r0
                r0 = r1
                goto L_0x002b
            L_0x005b:
                r2 = r1
                goto L_0x002b
            L_0x005d:
                if (r0 == 0) goto L_0x00b5
                int r0 = r7.n()
                int r0 = r7.c(r0)
                com.google.protobuf.GeneratedMessageLite$a r2 = r4.d
                com.google.protobuf.u$a r2 = r2.c_()
                com.google.protobuf.u$a r5 = com.google.protobuf.u.a.ENUM
                if (r2 != r5) goto L_0x0095
            L_0x0073:
                int r2 = r7.r()
                if (r2 <= 0) goto L_0x00af
                int r2 = r7.n()
                com.google.protobuf.GeneratedMessageLite$a r5 = r4.d
                com.google.protobuf.g$b r5 = r5.f()
                com.google.protobuf.g$a r2 = r5.a(r2)
                if (r2 != 0) goto L_0x008d
                r0 = r1
                goto L_0x0031
            L_0x008d:
                com.google.protobuf.GeneratedMessageLite$a r5 = r4.d
                r3.b(r5, r2)
                goto L_0x0073
            L_0x0095:
                int r2 = r7.r()
                if (r2 <= 0) goto L_0x00af
                com.google.protobuf.GeneratedMessageLite$a r2 = r4.d
                com.google.protobuf.u$a r2 = r2.c_()
                java.lang.Object r2 = com.google.protobuf.o.a(r7, r2)
                com.google.protobuf.GeneratedMessageLite$a r5 = r4.d
                r3.b(r5, r2)
                goto L_0x0095
            L_0x00af:
                r7.d(r0)
            L_0x00b2:
                r0 = r1
                goto L_0x0031
            L_0x00b5:
                int[] r0 = com.google.protobuf.GeneratedMessageLite.AnonymousClass1.a
                com.google.protobuf.GeneratedMessageLite$a r2 = r4.d
                com.google.protobuf.u$b r2 = r2.b_()
                int r2 = r2.ordinal()
                r0 = r0[r2]
                switch(r0) {
                    case 1: goto L_0x00e6;
                    case 2: goto L_0x0127;
                    default: goto L_0x00c8;
                }
            L_0x00c8:
                com.google.protobuf.GeneratedMessageLite$a r0 = r4.d
                com.google.protobuf.u$a r0 = r0.c_()
                java.lang.Object r0 = com.google.protobuf.o.a(r7, r0)
            L_0x00d4:
                com.google.protobuf.GeneratedMessageLite$a r2 = r4.d
                boolean r2 = r2.d()
                if (r2 == 0) goto L_0x013c
                com.google.protobuf.GeneratedMessageLite$a r2 = r4.d
                r3.b(r2, r0)
                goto L_0x00b2
            L_0x00e6:
                r2 = 0
                com.google.protobuf.GeneratedMessageLite$a r0 = r4.d
                boolean r0 = r0.d()
                if (r0 != 0) goto L_0x0145
                com.google.protobuf.GeneratedMessageLite$a r0 = r4.d
                java.lang.Object r0 = r3.b(r0)
                com.google.protobuf.i r0 = (com.google.protobuf.i) r0
                if (r0 == 0) goto L_0x0145
                com.google.protobuf.i$a r0 = r0.toBuilder()
            L_0x0101:
                if (r0 != 0) goto L_0x010b
                com.google.protobuf.i r0 = r4.c
                com.google.protobuf.i$a r0 = r0.newBuilderForType()
            L_0x010b:
                com.google.protobuf.GeneratedMessageLite$a r2 = r4.d
                com.google.protobuf.u$a r2 = r2.c_()
                com.google.protobuf.u$a r5 = com.google.protobuf.u.a.GROUP
                if (r2 != r5) goto L_0x0123
                int r2 = r4.b()
                r7.a(r2, r0, r8)
            L_0x011e:
                com.google.protobuf.i r0 = r0.build()
                goto L_0x00d4
            L_0x0123:
                r7.a(r0, r8)
                goto L_0x011e
            L_0x0127:
                int r0 = r7.n()
                com.google.protobuf.GeneratedMessageLite$a r2 = r4.d
                com.google.protobuf.g$b r2 = r2.f()
                com.google.protobuf.g$a r0 = r2.a(r0)
                if (r0 != 0) goto L_0x00d4
                r0 = r1
                goto L_0x0031
            L_0x013c:
                com.google.protobuf.GeneratedMessageLite$a r2 = r4.d
                r3.a(r2, r0)
                goto L_0x00b2
            L_0x0145:
                r0 = r2
                goto L_0x0101
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.GeneratedMessageLite.ExtendableBuilder.parseUnknownField(com.google.protobuf.h, com.google.protobuf.ab, int):boolean");
        }

        /* access modifiers changed from: protected */
        public final void mergeExtensionFields(MessageType messagetype) {
            internalGetResult().extensions.a(messagetype.extensions);
        }
    }

    public static <ContainingType extends i, Type> c<ContainingType, Type> newGeneratedExtension() {
        return new c<>();
    }

    private static final class a implements o.a<a> {
        private final g.b<?> a;
        private final int b;
        /* access modifiers changed from: private */
        public final u.a c;
        /* access modifiers changed from: private */
        public final boolean d;
        private final boolean e;

        public final /* bridge */ /* synthetic */ int compareTo(Object obj) {
            return this.b - ((a) obj).b;
        }

        public final int a_() {
            return this.b;
        }

        public final u.a c_() {
            return this.c;
        }

        public final u.b b_() {
            return this.c.b();
        }

        public final boolean d() {
            return this.d;
        }

        public final boolean e() {
            return this.e;
        }

        public final g.b<?> f() {
            return this.a;
        }

        public final i.a a(i.a aVar, i iVar) {
            return ((b) aVar).mergeFrom((GeneratedMessageLite) iVar);
        }
    }

    public static final class c<ContainingType extends i, Type> {
        private ContainingType a;
        /* access modifiers changed from: private */
        public Type b;
        /* access modifiers changed from: private */
        public i c;
        /* access modifiers changed from: private */
        public a d;

        /* synthetic */ c() {
            this((byte) 0);
        }

        private c(byte b2) {
        }

        public final ContainingType a() {
            return this.a;
        }

        public final int b() {
            return this.d.a_();
        }
    }
}
