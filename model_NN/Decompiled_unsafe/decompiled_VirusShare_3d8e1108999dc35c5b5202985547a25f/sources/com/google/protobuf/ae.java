package com.google.protobuf;

import com.google.protobuf.a;
import com.google.protobuf.c;
import com.google.protobuf.f;
import com.google.protobuf.n;
import com.google.protobuf.s;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public abstract class ae extends n implements s {
    private int memoizedSize = -1;

    public boolean isInitialized() {
        for (c.f next : getDescriptorForType().e()) {
            if (next.j() && !hasField(next)) {
                return false;
            }
        }
        for (Map.Entry next2 : getAllFields().entrySet()) {
            c.f fVar = (c.f) next2.getKey();
            if (fVar.h() == c.f.b.MESSAGE) {
                if (fVar.d()) {
                    for (s isInitialized : (List) next2.getValue()) {
                        if (!isInitialized.isInitialized()) {
                            return false;
                        }
                    }
                    continue;
                } else if (!((s) next2.getValue()).isInitialized()) {
                    return false;
                }
            }
        }
        return true;
    }

    public final String toString() {
        return p.a(this);
    }

    public void writeTo(x xVar) throws IOException {
        boolean messageSetWireFormat = getDescriptorForType().d().getMessageSetWireFormat();
        for (Map.Entry next : getAllFields().entrySet()) {
            c.f fVar = (c.f) next.getKey();
            Object value = next.getValue();
            if (!messageSetWireFormat || !fVar.n() || fVar.i() != c.f.a.MESSAGE || fVar.d()) {
                o.a(fVar, value, xVar);
            } else {
                xVar.c(fVar.a_(), (s) value);
            }
        }
        a unknownFields = getUnknownFields();
        if (messageSetWireFormat) {
            unknownFields.a(xVar);
        } else {
            unknownFields.writeTo(xVar);
        }
    }

    public int getSerializedSize() {
        int i;
        int i2 = this.memoizedSize;
        if (i2 == -1) {
            int i3 = 0;
            boolean messageSetWireFormat = getDescriptorForType().d().getMessageSetWireFormat();
            Iterator<Map.Entry<c.f, Object>> it = getAllFields().entrySet().iterator();
            while (true) {
                i = i3;
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry next = it.next();
                c.f fVar = (c.f) next.getKey();
                Object value = next.getValue();
                if (!messageSetWireFormat || !fVar.n() || fVar.i() != c.f.a.MESSAGE || fVar.d()) {
                    i3 = o.c(fVar, value) + i;
                } else {
                    i3 = x.e(fVar.a_(), (s) value) + i;
                }
            }
            a unknownFields = getUnknownFields();
            if (messageSetWireFormat) {
                i2 = unknownFields.d() + i;
            } else {
                i2 = unknownFields.getSerializedSize() + i;
            }
            this.memoizedSize = i2;
        }
        return i2;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof s)) {
            return false;
        }
        s sVar = (s) obj;
        if (getDescriptorForType() != sVar.getDescriptorForType()) {
            return false;
        }
        if (!getAllFields().equals(sVar.getAllFields()) || !getUnknownFields().equals(sVar.getUnknownFields())) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return ((((getDescriptorForType().hashCode() + 779) * 53) + getAllFields().hashCode()) * 29) + getUnknownFields().hashCode();
    }

    public static abstract class a<BuilderType extends a> extends n.a<BuilderType> implements s.a {
        public abstract BuilderType clone();

        public BuilderType clear() {
            for (Map.Entry<c.f, Object> key : getAllFields().entrySet()) {
                clearField((c.f) key.getKey());
            }
            return this;
        }

        public BuilderType mergeFrom(s sVar) {
            if (sVar.getDescriptorForType() != getDescriptorForType()) {
                throw new IllegalArgumentException("mergeFrom(Message) can only merge messages of the same type.");
            }
            for (Map.Entry next : sVar.getAllFields().entrySet()) {
                c.f fVar = (c.f) next.getKey();
                if (fVar.d()) {
                    for (Object addRepeatedField : (List) next.getValue()) {
                        addRepeatedField(fVar, addRepeatedField);
                    }
                } else if (fVar.h() == c.f.b.MESSAGE) {
                    s sVar2 = (s) getField(fVar);
                    if (sVar2 == sVar2.getDefaultInstanceForType()) {
                        setField(fVar, next.getValue());
                    } else {
                        setField(fVar, sVar2.newBuilderForType().mergeFrom(sVar2).mergeFrom((s) next.getValue()).build());
                    }
                } else {
                    setField(fVar, next.getValue());
                }
            }
            mergeUnknownFields(sVar.getUnknownFields());
            return this;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.h, com.google.protobuf.f]
         candidates:
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType */
        public BuilderType mergeFrom(h hVar) throws IOException {
            return mergeFrom(hVar, (ab) f.a());
        }

        public BuilderType mergeFrom(h hVar, ab abVar) throws IOException {
            int a;
            a.b a2 = a.a(getUnknownFields());
            do {
                a = hVar.a();
                if (a == 0) {
                    break;
                }
            } while (mergeFieldFrom(hVar, a2, abVar, this, a));
            setUnknownFields(a2.build());
            return this;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.o.a(com.google.protobuf.u$a, boolean):int
         arg types: [com.google.protobuf.u$a, int]
         candidates:
          com.google.protobuf.o.a(com.google.protobuf.h, com.google.protobuf.u$a):java.lang.Object
          com.google.protobuf.o.a(com.google.protobuf.u$a, java.lang.Object):void
          com.google.protobuf.o.a(com.google.protobuf.o$a, int):java.lang.Object
          com.google.protobuf.o.a(com.google.protobuf.o$a, java.lang.Object):void
          com.google.protobuf.o.a(com.google.protobuf.u$a, boolean):int */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0045  */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x0095  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        static boolean mergeFieldFrom(com.google.protobuf.h r8, com.google.protobuf.a.b r9, com.google.protobuf.ab r10, com.google.protobuf.s.a r11, int r12) throws java.io.IOException {
            /*
                r3 = 0
                r1 = 0
                r2 = 1
                com.google.protobuf.c$a r4 = r11.getDescriptorForType()
                com.google.protobuf.DescriptorProtos$MessageOptions r0 = r4.d()
                boolean r0 = r0.getMessageSetWireFormat()
                if (r0 == 0) goto L_0x001a
                int r0 = com.google.protobuf.u.a
                if (r12 != r0) goto L_0x001a
                mergeMessageSetExtensionFromCodedStream(r8, r9, r10, r11)
                r0 = r2
            L_0x0019:
                return r0
            L_0x001a:
                int r5 = com.google.protobuf.u.a(r12)
                int r6 = com.google.protobuf.u.b(r12)
                boolean r0 = r4.a(r6)
                if (r0 == 0) goto L_0x007a
                boolean r0 = r10 instanceof com.google.protobuf.f
                if (r0 == 0) goto L_0x0078
                r0 = r10
                com.google.protobuf.f r0 = (com.google.protobuf.f) r0
                com.google.protobuf.f$a r0 = r0.a(r4, r6)
                if (r0 != 0) goto L_0x004a
                r4 = r3
            L_0x0036:
                if (r4 == 0) goto L_0x0092
                com.google.protobuf.u$a r0 = r4.c_()
                int r0 = com.google.protobuf.o.a(r0, r1)
                if (r5 != r0) goto L_0x0080
                r0 = r1
            L_0x0043:
                if (r1 == 0) goto L_0x0095
                boolean r0 = r9.a(r12, r8)
                goto L_0x0019
            L_0x004a:
                com.google.protobuf.c$f r3 = r0.a
                com.google.protobuf.s r0 = r0.b
                if (r0 != 0) goto L_0x0075
                com.google.protobuf.c$f$b r4 = r3.h()
                com.google.protobuf.c$f$b r7 = com.google.protobuf.c.f.b.MESSAGE
                if (r4 != r7) goto L_0x0075
                java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "Message-typed extension lacked default instance: "
                java.lang.StringBuilder r1 = r1.append(r2)
                java.lang.String r2 = r3.b()
                java.lang.StringBuilder r1 = r1.append(r2)
                java.lang.String r1 = r1.toString()
                r0.<init>(r1)
                throw r0
            L_0x0075:
                r4 = r3
                r3 = r0
                goto L_0x0036
            L_0x0078:
                r4 = r3
                goto L_0x0036
            L_0x007a:
                com.google.protobuf.c$f r0 = r4.b(r6)
                r4 = r0
                goto L_0x0036
            L_0x0080:
                boolean r0 = r4.l()
                if (r0 == 0) goto L_0x0092
                com.google.protobuf.u$a r0 = r4.c_()
                int r0 = com.google.protobuf.o.a(r0, r2)
                if (r5 != r0) goto L_0x0092
                r0 = r2
                goto L_0x0043
            L_0x0092:
                r0 = r1
                r1 = r2
                goto L_0x0043
            L_0x0095:
                if (r0 == 0) goto L_0x00da
                int r0 = r8.n()
                int r0 = r8.c(r0)
                com.google.protobuf.u$a r1 = r4.c_()
                com.google.protobuf.u$a r3 = com.google.protobuf.u.a.ENUM
                if (r1 != r3) goto L_0x00c2
            L_0x00a7:
                int r1 = r8.r()
                if (r1 <= 0) goto L_0x00d4
                int r1 = r8.m()
                com.google.protobuf.c$e r3 = r4.r()
                com.google.protobuf.c$j r1 = r3.a(r1)
                if (r1 != 0) goto L_0x00be
                r0 = r2
                goto L_0x0019
            L_0x00be:
                r11.addRepeatedField(r4, r1)
                goto L_0x00a7
            L_0x00c2:
                int r1 = r8.r()
                if (r1 <= 0) goto L_0x00d4
                com.google.protobuf.u$a r1 = r4.c_()
                java.lang.Object r1 = com.google.protobuf.o.a(r8, r1)
                r11.addRepeatedField(r4, r1)
                goto L_0x00c2
            L_0x00d4:
                r8.d(r0)
            L_0x00d7:
                r0 = r2
                goto L_0x0019
            L_0x00da:
                int[] r0 = com.google.protobuf.ae.AnonymousClass1.a
                com.google.protobuf.c$f$a r1 = r4.i()
                int r1 = r1.ordinal()
                r0 = r0[r1]
                switch(r0) {
                    case 1: goto L_0x00fb;
                    case 2: goto L_0x0123;
                    case 3: goto L_0x0147;
                    default: goto L_0x00e9;
                }
            L_0x00e9:
                com.google.protobuf.u$a r0 = r4.c_()
                java.lang.Object r0 = com.google.protobuf.o.a(r8, r0)
            L_0x00f1:
                boolean r1 = r4.d()
                if (r1 == 0) goto L_0x015b
                r11.addRepeatedField(r4, r0)
                goto L_0x00d7
            L_0x00fb:
                if (r3 == 0) goto L_0x011d
                com.google.protobuf.s$a r0 = r3.newBuilderForType()
                r1 = r0
            L_0x0102:
                boolean r0 = r4.d()
                if (r0 != 0) goto L_0x0111
                java.lang.Object r0 = r11.getField(r4)
                com.google.protobuf.s r0 = (com.google.protobuf.s) r0
                r1.mergeFrom(r0)
            L_0x0111:
                int r0 = r4.a_()
                r8.a(r0, r1, r10)
                com.google.protobuf.s r0 = r1.build()
                goto L_0x00f1
            L_0x011d:
                com.google.protobuf.s$a r0 = r11.newBuilderForField(r4)
                r1 = r0
                goto L_0x0102
            L_0x0123:
                if (r3 == 0) goto L_0x0141
                com.google.protobuf.s$a r0 = r3.newBuilderForType()
                r1 = r0
            L_0x012a:
                boolean r0 = r4.d()
                if (r0 != 0) goto L_0x0139
                java.lang.Object r0 = r11.getField(r4)
                com.google.protobuf.s r0 = (com.google.protobuf.s) r0
                r1.mergeFrom(r0)
            L_0x0139:
                r8.a(r1, r10)
                com.google.protobuf.s r0 = r1.build()
                goto L_0x00f1
            L_0x0141:
                com.google.protobuf.s$a r0 = r11.newBuilderForField(r4)
                r1 = r0
                goto L_0x012a
            L_0x0147:
                int r1 = r8.m()
                com.google.protobuf.c$e r0 = r4.r()
                com.google.protobuf.c$j r0 = r0.a(r1)
                if (r0 != 0) goto L_0x00f1
                r9.a(r6, r1)
                r0 = r2
                goto L_0x0019
            L_0x015b:
                r11.setField(r4, r0)
                goto L_0x00d7
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.ae.a.mergeFieldFrom(com.google.protobuf.h, com.google.protobuf.a$b, com.google.protobuf.ab, com.google.protobuf.s$a, int):boolean");
        }

        private static void mergeMessageSetExtensionFromCodedStream(h hVar, a.b bVar, ab abVar, s.a aVar) throws IOException {
            f.a aVar2;
            q qVar;
            q qVar2;
            c.a descriptorForType = aVar.getDescriptorForType();
            c.f fVar = null;
            s.a aVar3 = null;
            q qVar3 = null;
            int i = 0;
            while (true) {
                int a = hVar.a();
                if (a == 0) {
                    break;
                }
                if (a != u.c) {
                    if (a != u.d) {
                        if (!hVar.b(a)) {
                            break;
                        }
                    } else if (i == 0) {
                        qVar3 = hVar.k();
                    } else if (aVar3 == null) {
                        bVar.a(i, a.C0014a.a().a(hVar.k()).a());
                    } else {
                        hVar.a(aVar3, abVar);
                    }
                } else {
                    i = hVar.l();
                    if (i != 0) {
                        if (abVar instanceof f) {
                            aVar2 = ((f) abVar).a(descriptorForType, i);
                        } else {
                            aVar2 = null;
                        }
                        if (aVar2 != null) {
                            fVar = aVar2.a;
                            aVar3 = aVar2.b.newBuilderForType();
                            s sVar = (s) aVar.getField(fVar);
                            if (sVar != null) {
                                aVar3.mergeFrom(sVar);
                            }
                            if (qVar3 != null) {
                                aVar3.mergeFrom(h.a(qVar3.c()));
                                qVar2 = null;
                            } else {
                                qVar2 = qVar3;
                            }
                            qVar3 = qVar2;
                        } else {
                            if (qVar3 != null) {
                                bVar.a(i, a.C0014a.a().a(qVar3).a());
                                qVar = null;
                            } else {
                                qVar = qVar3;
                            }
                            qVar3 = qVar;
                        }
                    }
                }
                i = i;
            }
            hVar.a(u.b);
            if (aVar3 != null) {
                aVar.setField(fVar, aVar3.build());
            }
        }

        public BuilderType mergeUnknownFields(a aVar) {
            setUnknownFields(a.a(getUnknownFields()).a(aVar).build());
            return this;
        }

        protected static e newUninitializedMessageException(s sVar) {
            return new e(findMissingFields(sVar));
        }

        private static List<String> findMissingFields(s sVar) {
            ArrayList arrayList = new ArrayList();
            findMissingFields(sVar, "", arrayList);
            return arrayList;
        }

        private static void findMissingFields(s sVar, String str, List<String> list) {
            for (c.f next : sVar.getDescriptorForType().e()) {
                if (next.j() && !sVar.hasField(next)) {
                    list.add(str + next.a());
                }
            }
            for (Map.Entry next2 : sVar.getAllFields().entrySet()) {
                c.f fVar = (c.f) next2.getKey();
                Object value = next2.getValue();
                if (fVar.h() == c.f.b.MESSAGE) {
                    if (fVar.d()) {
                        int i = 0;
                        for (s findMissingFields : (List) value) {
                            findMissingFields(findMissingFields, subMessagePrefix(str, fVar, i), list);
                            i++;
                        }
                    } else if (sVar.hasField(fVar)) {
                        findMissingFields((s) value, subMessagePrefix(str, fVar, -1), list);
                    }
                }
            }
        }

        private static String subMessagePrefix(String str, c.f fVar, int i) {
            StringBuilder sb = new StringBuilder(str);
            if (fVar.n()) {
                sb.append('(').append(fVar.b()).append(')');
            } else {
                sb.append(fVar.a());
            }
            if (i != -1) {
                sb.append('[').append(i).append(']');
            }
            sb.append('.');
            return sb.toString();
        }

        public BuilderType mergeFrom(q qVar) throws ac {
            return (a) super.mergeFrom(qVar);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
         arg types: [com.google.protobuf.q, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType */
        public BuilderType mergeFrom(q qVar, ab abVar) throws ac {
            return (a) super.mergeFrom(qVar, abVar);
        }

        public BuilderType mergeFrom(byte[] bArr) throws ac {
            return (a) super.mergeFrom(bArr);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.n.a.mergeFrom(byte[], int, int):BuilderType
         arg types: [byte[], int, int]
         candidates:
          com.google.protobuf.ae.a.mergeFrom(byte[], int, int):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], int, int):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], int, int):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(byte[], int, int):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], int, int):BuilderType */
        public BuilderType mergeFrom(byte[] bArr, int i, int i2) throws ac {
            return (a) super.mergeFrom(bArr, i, i2);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
         arg types: [byte[], com.google.protobuf.ab]
         candidates:
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType */
        public BuilderType mergeFrom(byte[] bArr, ab abVar) throws ac {
            return (a) super.mergeFrom(bArr, abVar);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.n.a.mergeFrom(byte[], int, int, com.google.protobuf.ab):BuilderType
         arg types: [byte[], int, int, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.ae.a.mergeFrom(byte[], int, int, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], int, int, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], int, int, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(byte[], int, int, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], int, int, com.google.protobuf.ab):BuilderType */
        public BuilderType mergeFrom(byte[] bArr, int i, int i2, ab abVar) throws ac {
            return (a) super.mergeFrom(bArr, i, i2, abVar);
        }

        public BuilderType mergeFrom(InputStream inputStream) throws IOException {
            return (a) super.mergeFrom(inputStream);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
         arg types: [java.io.InputStream, com.google.protobuf.ab]
         candidates:
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.n$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.ae.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.s$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(com.google.protobuf.q, com.google.protobuf.ab):BuilderType
          com.google.protobuf.n.a.mergeFrom(byte[], com.google.protobuf.ab):BuilderType
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.i.a.mergeFrom(com.google.protobuf.h, com.google.protobuf.ab):com.google.protobuf.i$a
          com.google.protobuf.n.a.mergeFrom(java.io.InputStream, com.google.protobuf.ab):BuilderType */
        public BuilderType mergeFrom(InputStream inputStream, ab abVar) throws IOException {
            return (a) super.mergeFrom(inputStream, abVar);
        }

        public boolean mergeDelimitedFrom(InputStream inputStream) throws IOException {
            return super.mergeDelimitedFrom(inputStream);
        }

        public boolean mergeDelimitedFrom(InputStream inputStream, ab abVar) throws IOException {
            return super.mergeDelimitedFrom(inputStream, abVar);
        }
    }
}
