package com.google.protobuf;

import java.io.IOException;

public final class ac extends IOException {
    public ac(String str) {
        super(str);
    }

    static ac a() {
        return new ac("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either than the input has been truncated or that an embedded message misreported its own length.");
    }

    static ac b() {
        return new ac("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    }

    static ac c() {
        return new ac("CodedInputStream encountered a malformed varint.");
    }

    static ac d() {
        return new ac("Protocol message tag had invalid wire type.");
    }

    static ac e() {
        return new ac("Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit.");
    }
}
