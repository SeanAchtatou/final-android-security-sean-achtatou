package com.google.protobuf;

import java.io.IOException;

public interface i {

    public interface a extends Cloneable {
        i build();

        a mergeFrom(h hVar, ab abVar) throws IOException;
    }

    i getDefaultInstanceForType();

    int getSerializedSize();

    boolean isInitialized();

    a newBuilderForType();

    a toBuilder();

    q toByteString();

    void writeTo(x xVar) throws IOException;
}
