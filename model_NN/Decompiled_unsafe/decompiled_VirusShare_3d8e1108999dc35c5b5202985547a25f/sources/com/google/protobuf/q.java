package com.google.protobuf;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

public final class q {
    public static final q a = new q(new byte[0], (byte) 0);
    private final byte[] b;
    private volatile int c;

    /* synthetic */ q(byte[] bArr) {
        this(bArr, (byte) 0);
    }

    private q(byte[] bArr, byte b2) {
        this.c = 0;
        this.b = bArr;
    }

    public final byte a(int i) {
        return this.b[i];
    }

    public final int a() {
        return this.b.length;
    }

    public static q a(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        System.arraycopy(bArr, i, bArr2, 0, i2);
        return new q(bArr2, (byte) 0);
    }

    public static q a(byte[] bArr) {
        return a(bArr, 0, bArr.length);
    }

    public static q a(String str) {
        try {
            return new q(str.getBytes("UTF-8"), (byte) 0);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("UTF-8 not supported?", e);
        }
    }

    public final byte[] b() {
        int length = this.b.length;
        byte[] bArr = new byte[length];
        System.arraycopy(this.b, 0, bArr, 0, length);
        return bArr;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof q)) {
            return false;
        }
        q qVar = (q) obj;
        int length = this.b.length;
        if (length != qVar.b.length) {
            return false;
        }
        byte[] bArr = this.b;
        byte[] bArr2 = qVar.b;
        for (int i = 0; i < length; i++) {
            if (bArr[i] != bArr2[i]) {
                return false;
            }
        }
        return true;
    }

    public final int hashCode() {
        int i = this.c;
        if (i == 0) {
            byte[] bArr = this.b;
            int length = this.b.length;
            int i2 = 0;
            i = length;
            while (i2 < length) {
                i2++;
                i = bArr[i2] + (i * 31);
            }
            if (i == 0) {
                i = 1;
            }
            this.c = i;
        }
        return i;
    }

    public final InputStream c() {
        return new ByteArrayInputStream(this.b);
    }

    public final h d() {
        return h.a(this.b);
    }

    static a b(int i) {
        return new a(i);
    }

    static final class a {
        private final x a;
        private final byte[] b;

        /* synthetic */ a(int i) {
            this(i, (byte) 0);
        }

        private a(int i, byte b2) {
            this.b = new byte[i];
            this.a = x.a(this.b);
        }

        public final q a() {
            this.a.b();
            return new q(this.b);
        }

        public final x b() {
            return this.a;
        }
    }
}
