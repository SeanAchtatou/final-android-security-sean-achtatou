package com.google.protobuf;

import com.google.protobuf.DescriptorProtos;
import com.google.protobuf.a;
import com.google.protobuf.c;
import com.ptowngames.jigsaur.Jigsaur;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

public final class p {
    private p() {
    }

    private static void a(s sVar, Appendable appendable) throws IOException {
        a(sVar, new a(appendable));
    }

    private static void a(a aVar, Appendable appendable) throws IOException {
        a(aVar, new a(appendable));
    }

    public static String a(s sVar) {
        try {
            StringBuilder sb = new StringBuilder();
            a(sVar, sb);
            return sb.toString();
        } catch (IOException e) {
            throw new RuntimeException("Writing to a StringBuilder threw an IOException (should never happen).", e);
        }
    }

    public static String a(a aVar) {
        try {
            StringBuilder sb = new StringBuilder();
            a(aVar, sb);
            return sb.toString();
        } catch (IOException e) {
            throw new RuntimeException("Writing to a StringBuilder threw an IOException (should never happen).", e);
        }
    }

    private static void a(s sVar, a aVar) throws IOException {
        for (Map.Entry next : sVar.getAllFields().entrySet()) {
            c.f fVar = (c.f) next.getKey();
            Object value = next.getValue();
            if (fVar.d()) {
                for (Object a2 : (List) value) {
                    a(fVar, a2, aVar);
                }
            } else {
                a(fVar, value, aVar);
            }
        }
        a(sVar.getUnknownFields(), aVar);
    }

    private static void a(c.f fVar, Object obj, a aVar) throws IOException {
        if (fVar.n()) {
            aVar.a("[");
            if (!fVar.o().d().getMessageSetWireFormat() || fVar.i() != c.f.a.MESSAGE || !fVar.k() || fVar.p() != fVar.q()) {
                aVar.a(fVar.b());
            } else {
                aVar.a(fVar.q().b());
            }
            aVar.a("]");
        } else if (fVar.i() == c.f.a.GROUP) {
            aVar.a(fVar.q().a());
        } else {
            aVar.a(fVar.a());
        }
        if (fVar.h() == c.f.b.MESSAGE) {
            aVar.a(" {\n");
            aVar.a();
        } else {
            aVar.a(": ");
        }
        switch (AnonymousClass1.a[fVar.i().ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
                aVar.a(obj.toString());
                break;
            case 10:
            case 11:
                int intValue = ((Integer) obj).intValue();
                aVar.a(intValue >= 0 ? Integer.toString(intValue) : Long.toString(((long) intValue) & 4294967295L));
                break;
            case 12:
            case 13:
                aVar.a(a(((Long) obj).longValue()));
                break;
            case Jigsaur.Level.DIFFICULTY_FAMILY_FIELD_NUMBER /*14*/:
                aVar.a("\"");
                aVar.a(a(q.a((String) obj)));
                aVar.a("\"");
                break;
            case 15:
                aVar.a("\"");
                aVar.a(a((q) obj));
                aVar.a("\"");
                break;
            case 16:
                aVar.a(((c.j) obj).a());
                break;
            case DescriptorProtos.FileOptions.JAVA_GENERIC_SERVICES_FIELD_NUMBER /*17*/:
            case DescriptorProtos.FileOptions.PY_GENERIC_SERVICES_FIELD_NUMBER /*18*/:
                a((s) obj, aVar);
                break;
        }
        if (fVar.h() == c.f.b.MESSAGE) {
            aVar.b();
            aVar.a("}");
        }
        aVar.a("\n");
    }

    /* renamed from: com.google.protobuf.p$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] a = new int[c.f.a.values().length];

        static {
            try {
                a[c.f.a.INT32.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                a[c.f.a.INT64.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                a[c.f.a.SINT32.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                a[c.f.a.SINT64.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                a[c.f.a.SFIXED32.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                a[c.f.a.SFIXED64.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                a[c.f.a.FLOAT.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                a[c.f.a.DOUBLE.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                a[c.f.a.BOOL.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                a[c.f.a.UINT32.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                a[c.f.a.FIXED32.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                a[c.f.a.UINT64.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            try {
                a[c.f.a.FIXED64.ordinal()] = 13;
            } catch (NoSuchFieldError e13) {
            }
            try {
                a[c.f.a.STRING.ordinal()] = 14;
            } catch (NoSuchFieldError e14) {
            }
            try {
                a[c.f.a.BYTES.ordinal()] = 15;
            } catch (NoSuchFieldError e15) {
            }
            try {
                a[c.f.a.ENUM.ordinal()] = 16;
            } catch (NoSuchFieldError e16) {
            }
            try {
                a[c.f.a.MESSAGE.ordinal()] = 17;
            } catch (NoSuchFieldError e17) {
            }
            try {
                a[c.f.a.GROUP.ordinal()] = 18;
            } catch (NoSuchFieldError e18) {
            }
        }
    }

    private static void a(a aVar, a aVar2) throws IOException {
        for (Map.Entry next : aVar.c().entrySet()) {
            ((Integer) next.getKey()).toString() + ": ";
            a.C0014a aVar3 = (a.C0014a) next.getValue();
            for (Long longValue : aVar3.b()) {
                long longValue2 = longValue.longValue();
                aVar2.a(((Integer) next.getKey()).toString());
                aVar2.a(": ");
                aVar2.a(a(longValue2));
                aVar2.a("\n");
            }
            for (Integer intValue : aVar3.c()) {
                int intValue2 = intValue.intValue();
                aVar2.a(((Integer) next.getKey()).toString());
                aVar2.a(": ");
                aVar2.a(String.format(null, "0x%08x", Integer.valueOf(intValue2)));
                aVar2.a("\n");
            }
            for (Long longValue3 : aVar3.d()) {
                long longValue4 = longValue3.longValue();
                aVar2.a(((Integer) next.getKey()).toString());
                aVar2.a(": ");
                aVar2.a(String.format(null, "0x%016x", Long.valueOf(longValue4)));
                aVar2.a("\n");
            }
            for (q a2 : aVar3.e()) {
                aVar2.a(((Integer) next.getKey()).toString());
                aVar2.a(": \"");
                aVar2.a(a(a2));
                aVar2.a("\"\n");
            }
            for (a a3 : aVar3.f()) {
                aVar2.a(((Integer) next.getKey()).toString());
                aVar2.a(" {\n");
                aVar2.a();
                a(a3, aVar2);
                aVar2.b();
                aVar2.a("}\n");
            }
        }
    }

    private static String a(long j) {
        if (j >= 0) {
            return Long.toString(j);
        }
        return BigInteger.valueOf(Long.MAX_VALUE & j).setBit(63).toString();
    }

    private static final class a {
        private Appendable a;
        private boolean b;
        private final StringBuilder c;

        /* synthetic */ a(Appendable appendable) {
            this(appendable, (byte) 0);
        }

        private a(Appendable appendable, byte b2) {
            this.b = true;
            this.c = new StringBuilder();
            this.a = appendable;
        }

        public final void a() {
            this.c.append("  ");
        }

        public final void b() {
            int length = this.c.length();
            if (length == 0) {
                throw new IllegalArgumentException(" Outdent() without matching Indent().");
            }
            this.c.delete(length - 2, length);
        }

        public final void a(CharSequence charSequence) throws IOException {
            int i = 0;
            int length = charSequence.length();
            for (int i2 = 0; i2 < length; i2++) {
                if (charSequence.charAt(i2) == 10) {
                    a(charSequence.subSequence(i, length), (i2 - i) + 1);
                    i = i2 + 1;
                    this.b = true;
                }
            }
            a(charSequence.subSequence(i, length), length - i);
        }

        private void a(CharSequence charSequence, int i) throws IOException {
            if (i != 0) {
                if (this.b) {
                    this.b = false;
                    this.a.append(this.c);
                }
                this.a.append(charSequence);
            }
        }
    }

    private static String a(q qVar) {
        StringBuilder sb = new StringBuilder(qVar.a());
        for (int i = 0; i < qVar.a(); i++) {
            byte a2 = qVar.a(i);
            switch (a2) {
                case 7:
                    sb.append("\\a");
                    break;
                case 8:
                    sb.append("\\b");
                    break;
                case 9:
                    sb.append("\\t");
                    break;
                case 10:
                    sb.append("\\n");
                    break;
                case 11:
                    sb.append("\\v");
                    break;
                case 12:
                    sb.append("\\f");
                    break;
                case 13:
                    sb.append("\\r");
                    break;
                case 34:
                    sb.append("\\\"");
                    break;
                case 39:
                    sb.append("\\'");
                    break;
                case 92:
                    sb.append("\\\\");
                    break;
                default:
                    if (a2 < 32) {
                        sb.append('\\');
                        sb.append((char) (((a2 >>> 6) & 3) + 48));
                        sb.append((char) (((a2 >>> 3) & 7) + 48));
                        sb.append((char) ((a2 & 7) + 48));
                        break;
                    } else {
                        sb.append((char) a2);
                        break;
                    }
            }
        }
        return sb.toString();
    }

    static q a(CharSequence charSequence) throws b {
        int i;
        byte[] bArr = new byte[charSequence.length()];
        int i2 = 0;
        for (int i3 = 0; i3 < charSequence.length(); i3 = i + 1) {
            char charAt = charSequence.charAt(i3);
            if (charAt != '\\') {
                bArr[i2] = (byte) charAt;
                i2++;
                i = i3;
            } else if (i3 + 1 < charSequence.length()) {
                i = i3 + 1;
                char charAt2 = charSequence.charAt(i);
                if (a(charAt2)) {
                    int c = c(charAt2);
                    if (i + 1 < charSequence.length() && a(charSequence.charAt(i + 1))) {
                        i++;
                        c = (c * 8) + c(charSequence.charAt(i));
                    }
                    if (i + 1 < charSequence.length() && a(charSequence.charAt(i + 1))) {
                        i++;
                        c = (c * 8) + c(charSequence.charAt(i));
                    }
                    bArr[i2] = (byte) c;
                    i2++;
                } else {
                    switch (charAt2) {
                        case '\"':
                            bArr[i2] = 34;
                            i2++;
                            continue;
                        case '\'':
                            bArr[i2] = 39;
                            i2++;
                            continue;
                        case '\\':
                            bArr[i2] = 92;
                            i2++;
                            continue;
                        case 'a':
                            bArr[i2] = 7;
                            i2++;
                            continue;
                        case 'b':
                            bArr[i2] = 8;
                            i2++;
                            continue;
                        case 'f':
                            bArr[i2] = 12;
                            i2++;
                            continue;
                        case 'n':
                            bArr[i2] = 10;
                            i2++;
                            continue;
                        case 'r':
                            bArr[i2] = 13;
                            i2++;
                            continue;
                        case 't':
                            bArr[i2] = 9;
                            i2++;
                            continue;
                        case 'v':
                            bArr[i2] = 11;
                            i2++;
                            continue;
                        case 'x':
                            if (i + 1 >= charSequence.length() || !b(charSequence.charAt(i + 1))) {
                                throw new b("Invalid escape sequence: '\\x' with no digits");
                            }
                            i++;
                            int c2 = c(charSequence.charAt(i));
                            if (i + 1 < charSequence.length() && b(charSequence.charAt(i + 1))) {
                                i++;
                                c2 = (c2 * 16) + c(charSequence.charAt(i));
                            }
                            bArr[i2] = (byte) c2;
                            i2++;
                            continue;
                        default:
                            throw new b("Invalid escape sequence: '\\" + charAt2 + '\'');
                    }
                }
            } else {
                throw new b("Invalid escape sequence: '\\' at end of string.");
            }
        }
        return q.a(bArr, 0, i2);
    }

    static class b extends IOException {
        b(String str) {
            super(str);
        }
    }

    private static boolean a(char c) {
        return '0' <= c && c <= '7';
    }

    private static boolean b(char c) {
        return ('0' <= c && c <= '9') || ('a' <= c && c <= 'f') || ('A' <= c && c <= 'F');
    }

    private static int c(char c) {
        if ('0' <= c && c <= '9') {
            return c - '0';
        }
        if ('a' > c || c > 'z') {
            return (c - 'A') + 10;
        }
        return (c - 'a') + 10;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.protobuf.p.a(java.lang.String, boolean, boolean):long
     arg types: [java.lang.String, int, int]
     candidates:
      com.google.protobuf.p.a(com.google.protobuf.c$f, java.lang.Object, com.google.protobuf.p$a):void
      com.google.protobuf.p.a(java.lang.String, boolean, boolean):long */
    static int a(String str) throws NumberFormatException {
        return (int) a(str, true, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.protobuf.p.a(java.lang.String, boolean, boolean):long
     arg types: [java.lang.String, int, int]
     candidates:
      com.google.protobuf.p.a(com.google.protobuf.c$f, java.lang.Object, com.google.protobuf.p$a):void
      com.google.protobuf.p.a(java.lang.String, boolean, boolean):long */
    static int b(String str) throws NumberFormatException {
        return (int) a(str, false, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.protobuf.p.a(java.lang.String, boolean, boolean):long
     arg types: [java.lang.String, int, int]
     candidates:
      com.google.protobuf.p.a(com.google.protobuf.c$f, java.lang.Object, com.google.protobuf.p$a):void
      com.google.protobuf.p.a(java.lang.String, boolean, boolean):long */
    static long c(String str) throws NumberFormatException {
        return a(str, true, true);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.protobuf.p.a(java.lang.String, boolean, boolean):long
     arg types: [java.lang.String, int, int]
     candidates:
      com.google.protobuf.p.a(com.google.protobuf.c$f, java.lang.Object, com.google.protobuf.p$a):void
      com.google.protobuf.p.a(java.lang.String, boolean, boolean):long */
    static long d(String str) throws NumberFormatException {
        return a(str, false, true);
    }

    private static long a(String str, boolean z, boolean z2) throws NumberFormatException {
        int i;
        int i2;
        BigInteger bigInteger;
        long j;
        boolean z3 = true;
        int i3 = 0;
        if (!str.startsWith("-", 0)) {
            z3 = false;
        } else if (!z) {
            throw new NumberFormatException("Number must be positive: " + str);
        } else {
            i3 = 1;
        }
        if (str.startsWith("0x", i3)) {
            i = i3 + 2;
            i2 = 16;
        } else if (str.startsWith("0", i3)) {
            i = i3;
            i2 = 8;
        } else {
            i = i3;
            i2 = 10;
        }
        String substring = str.substring(i);
        if (substring.length() < 16) {
            long parseLong = Long.parseLong(substring, i2);
            if (z3) {
                j = -parseLong;
            } else {
                j = parseLong;
            }
            if (z2) {
                return j;
            }
            if (z) {
                if (j <= 2147483647L && j >= -2147483648L) {
                    return j;
                }
                throw new NumberFormatException("Number out of range for 32-bit signed integer: " + str);
            } else if (j < 4294967296L && j >= 0) {
                return j;
            } else {
                throw new NumberFormatException("Number out of range for 32-bit unsigned integer: " + str);
            }
        } else {
            BigInteger bigInteger2 = new BigInteger(substring, i2);
            if (z3) {
                bigInteger = bigInteger2.negate();
            } else {
                bigInteger = bigInteger2;
            }
            if (!z2) {
                if (z) {
                    if (bigInteger.bitLength() > 31) {
                        throw new NumberFormatException("Number out of range for 32-bit signed integer: " + str);
                    }
                } else if (bigInteger.bitLength() > 32) {
                    throw new NumberFormatException("Number out of range for 32-bit unsigned integer: " + str);
                }
            } else if (z) {
                if (bigInteger.bitLength() > 63) {
                    throw new NumberFormatException("Number out of range for 64-bit signed integer: " + str);
                }
            } else if (bigInteger.bitLength() > 64) {
                throw new NumberFormatException("Number out of range for 64-bit unsigned integer: " + str);
            }
            return bigInteger.longValue();
        }
    }
}
