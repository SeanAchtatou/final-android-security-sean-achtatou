package com.google.protobuf;

public final class u {
    static final int a = 11;
    static final int b = 12;
    static final int c = 16;
    static final int d = 26;

    private u() {
    }

    static int a(int i) {
        return i & 7;
    }

    public static int b(int i) {
        return i >>> 3;
    }

    static int a(int i, int i2) {
        return (i << 3) | i2;
    }

    public enum b {
        INT(0),
        LONG(0L),
        FLOAT(Float.valueOf(0.0f)),
        DOUBLE(Double.valueOf(0.0d)),
        BOOLEAN(false),
        STRING(""),
        BYTE_STRING(q.a),
        ENUM(null),
        MESSAGE(null);
        
        private final Object j;

        private b(Object obj) {
            this.j = obj;
        }
    }

    public enum a {
        DOUBLE(b.DOUBLE, 1, (byte) 0),
        FLOAT(b.FLOAT, 5, (byte) 0),
        INT64(b.LONG, 0, (byte) 0),
        UINT64(b.LONG, 0, (byte) 0),
        INT32(b.INT, 0, (byte) 0),
        FIXED64(b.LONG, 1, (byte) 0),
        FIXED32(b.INT, 5, (byte) 0),
        BOOL(b.BOOLEAN, 0, (byte) 0),
        STRING,
        GROUP,
        MESSAGE,
        BYTES,
        UINT32(b.INT, 0, (byte) 0),
        ENUM(b.ENUM, 0, (byte) 0),
        SFIXED32(b.INT, 5, (byte) 0),
        SFIXED64(b.LONG, 1, (byte) 0),
        SINT32(b.INT, 0, (byte) 0),
        SINT64(b.LONG, 0, (byte) 0);
        
        private final b s;
        private final int t;

        /* JADX WARN: Type inference failed for: r3v0, types: [com.google.protobuf.u$b, byte] */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private a(byte r3, byte r4, byte r5) {
            /*
                r0 = this;
                r0.<init>(r1, r2)
                r0.s = r3
                r0.t = r4
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.protobuf.u.a.<init>(java.lang.String, int, com.google.protobuf.u$b, int, byte):void");
        }

        public final b b() {
            return this.s;
        }

        public final int c() {
            return this.t;
        }

        public boolean a() {
            return true;
        }
    }
}
