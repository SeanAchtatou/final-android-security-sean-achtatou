package com.google.protobuf;

import com.google.protobuf.DescriptorProtos;
import com.google.protobuf.GeneratedMessage;
import com.google.protobuf.c;

class r implements c.b.a {
    r() {
    }

    public final f a(c.b bVar) {
        c.b unused = DescriptorProtos.K = bVar;
        c.a unused2 = DescriptorProtos.a = DescriptorProtos.a().d().get(0);
        GeneratedMessage.FieldAccessorTable unused3 = DescriptorProtos.b = new GeneratedMessage.FieldAccessorTable(DescriptorProtos.a, new String[]{"File"}, DescriptorProtos.FileDescriptorSet.class, DescriptorProtos.FileDescriptorSet.Builder.class);
        c.a unused4 = DescriptorProtos.c = DescriptorProtos.a().d().get(1);
        GeneratedMessage.FieldAccessorTable unused5 = DescriptorProtos.d = new GeneratedMessage.FieldAccessorTable(DescriptorProtos.c, new String[]{"Name", "Package", "Dependency", "MessageType", "EnumType", "Service", "Extension", "Options"}, DescriptorProtos.FileDescriptorProto.class, DescriptorProtos.FileDescriptorProto.Builder.class);
        c.a unused6 = DescriptorProtos.e = DescriptorProtos.a().d().get(2);
        GeneratedMessage.FieldAccessorTable unused7 = DescriptorProtos.f = new GeneratedMessage.FieldAccessorTable(DescriptorProtos.e, new String[]{"Name", "Field", "Extension", "NestedType", "EnumType", "ExtensionRange", "Options"}, DescriptorProtos.DescriptorProto.class, DescriptorProtos.DescriptorProto.Builder.class);
        c.a unused8 = DescriptorProtos.g = DescriptorProtos.e.f().get(0);
        GeneratedMessage.FieldAccessorTable unused9 = DescriptorProtos.h = new GeneratedMessage.FieldAccessorTable(DescriptorProtos.g, new String[]{"Start", "End"}, DescriptorProtos.DescriptorProto.ExtensionRange.class, DescriptorProtos.DescriptorProto.ExtensionRange.Builder.class);
        c.a unused10 = DescriptorProtos.i = DescriptorProtos.a().d().get(3);
        GeneratedMessage.FieldAccessorTable unused11 = DescriptorProtos.j = new GeneratedMessage.FieldAccessorTable(DescriptorProtos.i, new String[]{"Name", "Number", "Label", "Type", "TypeName", "Extendee", "DefaultValue", "Options"}, DescriptorProtos.FieldDescriptorProto.class, DescriptorProtos.FieldDescriptorProto.Builder.class);
        c.a unused12 = DescriptorProtos.k = DescriptorProtos.a().d().get(4);
        GeneratedMessage.FieldAccessorTable unused13 = DescriptorProtos.l = new GeneratedMessage.FieldAccessorTable(DescriptorProtos.k, new String[]{"Name", "Value", "Options"}, DescriptorProtos.EnumDescriptorProto.class, DescriptorProtos.EnumDescriptorProto.Builder.class);
        c.a unused14 = DescriptorProtos.m = DescriptorProtos.a().d().get(5);
        GeneratedMessage.FieldAccessorTable unused15 = DescriptorProtos.n = new GeneratedMessage.FieldAccessorTable(DescriptorProtos.m, new String[]{"Name", "Number", "Options"}, DescriptorProtos.EnumValueDescriptorProto.class, DescriptorProtos.EnumValueDescriptorProto.Builder.class);
        c.a unused16 = DescriptorProtos.o = DescriptorProtos.a().d().get(6);
        GeneratedMessage.FieldAccessorTable unused17 = DescriptorProtos.p = new GeneratedMessage.FieldAccessorTable(DescriptorProtos.o, new String[]{"Name", "Method", "Options"}, DescriptorProtos.ServiceDescriptorProto.class, DescriptorProtos.ServiceDescriptorProto.Builder.class);
        c.a unused18 = DescriptorProtos.q = DescriptorProtos.a().d().get(7);
        GeneratedMessage.FieldAccessorTable unused19 = DescriptorProtos.r = new GeneratedMessage.FieldAccessorTable(DescriptorProtos.q, new String[]{"Name", "InputType", "OutputType", "Options"}, DescriptorProtos.MethodDescriptorProto.class, DescriptorProtos.MethodDescriptorProto.Builder.class);
        c.a unused20 = DescriptorProtos.s = DescriptorProtos.a().d().get(8);
        GeneratedMessage.FieldAccessorTable unused21 = DescriptorProtos.t = new GeneratedMessage.FieldAccessorTable(DescriptorProtos.s, new String[]{"JavaPackage", "JavaOuterClassname", "JavaMultipleFiles", "OptimizeFor", "CcGenericServices", "JavaGenericServices", "PyGenericServices", "UninterpretedOption"}, DescriptorProtos.FileOptions.class, DescriptorProtos.FileOptions.Builder.class);
        c.a unused22 = DescriptorProtos.u = DescriptorProtos.a().d().get(9);
        GeneratedMessage.FieldAccessorTable unused23 = DescriptorProtos.v = new GeneratedMessage.FieldAccessorTable(DescriptorProtos.u, new String[]{"MessageSetWireFormat", "NoStandardDescriptorAccessor", "UninterpretedOption"}, DescriptorProtos.MessageOptions.class, DescriptorProtos.MessageOptions.Builder.class);
        c.a unused24 = DescriptorProtos.w = DescriptorProtos.a().d().get(10);
        GeneratedMessage.FieldAccessorTable unused25 = DescriptorProtos.x = new GeneratedMessage.FieldAccessorTable(DescriptorProtos.w, new String[]{"Ctype", "Packed", "Deprecated", "ExperimentalMapKey", "UninterpretedOption"}, DescriptorProtos.FieldOptions.class, DescriptorProtos.FieldOptions.Builder.class);
        c.a unused26 = DescriptorProtos.y = DescriptorProtos.a().d().get(11);
        GeneratedMessage.FieldAccessorTable unused27 = DescriptorProtos.z = new GeneratedMessage.FieldAccessorTable(DescriptorProtos.y, new String[]{"UninterpretedOption"}, DescriptorProtos.EnumOptions.class, DescriptorProtos.EnumOptions.Builder.class);
        c.a unused28 = DescriptorProtos.A = DescriptorProtos.a().d().get(12);
        GeneratedMessage.FieldAccessorTable unused29 = DescriptorProtos.B = new GeneratedMessage.FieldAccessorTable(DescriptorProtos.A, new String[]{"UninterpretedOption"}, DescriptorProtos.EnumValueOptions.class, DescriptorProtos.EnumValueOptions.Builder.class);
        c.a unused30 = DescriptorProtos.C = DescriptorProtos.a().d().get(13);
        GeneratedMessage.FieldAccessorTable unused31 = DescriptorProtos.D = new GeneratedMessage.FieldAccessorTable(DescriptorProtos.C, new String[]{"UninterpretedOption"}, DescriptorProtos.ServiceOptions.class, DescriptorProtos.ServiceOptions.Builder.class);
        c.a unused32 = DescriptorProtos.E = DescriptorProtos.a().d().get(14);
        GeneratedMessage.FieldAccessorTable unused33 = DescriptorProtos.F = new GeneratedMessage.FieldAccessorTable(DescriptorProtos.E, new String[]{"UninterpretedOption"}, DescriptorProtos.MethodOptions.class, DescriptorProtos.MethodOptions.Builder.class);
        c.a unused34 = DescriptorProtos.G = DescriptorProtos.a().d().get(15);
        GeneratedMessage.FieldAccessorTable unused35 = DescriptorProtos.H = new GeneratedMessage.FieldAccessorTable(DescriptorProtos.G, new String[]{"Name", "IdentifierValue", "PositiveIntValue", "NegativeIntValue", "DoubleValue", "StringValue"}, DescriptorProtos.UninterpretedOption.class, DescriptorProtos.UninterpretedOption.Builder.class);
        c.a unused36 = DescriptorProtos.I = DescriptorProtos.G.f().get(0);
        GeneratedMessage.FieldAccessorTable unused37 = DescriptorProtos.J = new GeneratedMessage.FieldAccessorTable(DescriptorProtos.I, new String[]{"NamePart", "IsExtension"}, DescriptorProtos.UninterpretedOption.NamePart.class, DescriptorProtos.UninterpretedOption.NamePart.Builder.class);
        return null;
    }
}
