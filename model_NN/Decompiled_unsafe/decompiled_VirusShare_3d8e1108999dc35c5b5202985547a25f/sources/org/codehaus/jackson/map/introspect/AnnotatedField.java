package org.codehaus.jackson.map.introspect;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Type;

public final class AnnotatedField extends AnnotatedMember {
    protected final AnnotationMap _annotations;
    protected final Field _field;

    public AnnotatedField(Field field, AnnotationMap annotationMap) {
        this._field = field;
        this._annotations = annotationMap;
    }

    public final void addOrOverride(Annotation annotation) {
        this._annotations.add(annotation);
    }

    public final Field getAnnotated() {
        return this._field;
    }

    public final int getModifiers() {
        return this._field.getModifiers();
    }

    public final String getName() {
        return this._field.getName();
    }

    public final <A extends Annotation> A getAnnotation(Class<A> cls) {
        return this._annotations.get(cls);
    }

    public final Type getGenericType() {
        return this._field.getGenericType();
    }

    public final Class<?> getRawType() {
        return this._field.getType();
    }

    public final Class<?> getDeclaringClass() {
        return this._field.getDeclaringClass();
    }

    public final Member getMember() {
        return this._field;
    }

    public final String getFullName() {
        return getDeclaringClass().getName() + "#" + getName();
    }

    public final int getAnnotationCount() {
        return this._annotations.size();
    }

    public final String toString() {
        return "[field " + getName() + ", annotations: " + this._annotations + "]";
    }
}
