package org.codehaus.jackson.map.ser;

import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Time;
import java.util.Calendar;
import java.util.Date;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.JsonSerializable;
import org.codehaus.jackson.map.JsonSerializableWithType;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.TypeSerializer;
import org.codehaus.jackson.map.annotate.JacksonStdImpl;
import org.codehaus.jackson.util.TokenBuffer;

public class StdSerializers {
    protected StdSerializers() {
    }

    protected static abstract class NonTypedScalarSerializer<T> extends ScalarSerializerBase<T> {
        protected NonTypedScalarSerializer(Class<T> cls) {
            super(cls);
        }

        public final void serializeWithType(T t, JsonGenerator jsonGenerator, SerializerProvider serializerProvider, TypeSerializer typeSerializer) throws IOException, JsonGenerationException {
            serialize(t, jsonGenerator, serializerProvider);
        }
    }

    @JacksonStdImpl
    public static final class BooleanSerializer extends NonTypedScalarSerializer<Boolean> {
        final boolean _forPrimitive;

        public BooleanSerializer(boolean z) {
            super(Boolean.class);
            this._forPrimitive = z;
        }

        public final void serialize(Boolean bool, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            jsonGenerator.writeBoolean(bool.booleanValue());
        }

        public final JsonNode getSchema(SerializerProvider serializerProvider, Type type) throws JsonMappingException {
            return createSchemaNode("boolean", !this._forPrimitive);
        }
    }

    @JacksonStdImpl
    public static final class StringSerializer extends NonTypedScalarSerializer<String> {
        public final /* bridge */ /* synthetic */ void serialize(Object obj, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            jsonGenerator.writeString((String) obj);
        }

        public StringSerializer() {
            super(String.class);
        }

        public final void serialize(String str, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            jsonGenerator.writeString(str);
        }

        public final JsonNode getSchema(SerializerProvider serializerProvider, Type type) {
            return createSchemaNode("string", true);
        }
    }

    @JacksonStdImpl
    public static final class IntegerSerializer extends NonTypedScalarSerializer<Integer> {
        public IntegerSerializer() {
            super(Integer.class);
        }

        public final void serialize(Integer num, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            jsonGenerator.writeNumber(num.intValue());
        }

        public final JsonNode getSchema(SerializerProvider serializerProvider, Type type) throws JsonMappingException {
            return createSchemaNode("integer", true);
        }
    }

    @JacksonStdImpl
    public static final class IntLikeSerializer extends ScalarSerializerBase<Number> {
        static final IntLikeSerializer instance = new IntLikeSerializer();

        public IntLikeSerializer() {
            super(Number.class);
        }

        public final void serialize(Number number, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            jsonGenerator.writeNumber(number.intValue());
        }

        public final JsonNode getSchema(SerializerProvider serializerProvider, Type type) throws JsonMappingException {
            return createSchemaNode("integer", true);
        }
    }

    @JacksonStdImpl
    public static final class LongSerializer extends ScalarSerializerBase<Long> {
        static final LongSerializer instance = new LongSerializer();

        public LongSerializer() {
            super(Long.class);
        }

        public final void serialize(Long l, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            jsonGenerator.writeNumber(l.longValue());
        }

        public final JsonNode getSchema(SerializerProvider serializerProvider, Type type) {
            return createSchemaNode("number", true);
        }
    }

    @JacksonStdImpl
    public static final class FloatSerializer extends ScalarSerializerBase<Float> {
        static final FloatSerializer instance = new FloatSerializer();

        public FloatSerializer() {
            super(Float.class);
        }

        public final void serialize(Float f, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            jsonGenerator.writeNumber(f.floatValue());
        }

        public final JsonNode getSchema(SerializerProvider serializerProvider, Type type) {
            return createSchemaNode("number", true);
        }
    }

    @JacksonStdImpl
    public static final class DoubleSerializer extends NonTypedScalarSerializer<Double> {
        static final DoubleSerializer instance = new DoubleSerializer();

        public DoubleSerializer() {
            super(Double.class);
        }

        public final void serialize(Double d, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            jsonGenerator.writeNumber(d.doubleValue());
        }

        public final JsonNode getSchema(SerializerProvider serializerProvider, Type type) {
            return createSchemaNode("number", true);
        }
    }

    @JacksonStdImpl
    public static final class NumberSerializer extends ScalarSerializerBase<Number> {
        public static final NumberSerializer instance = new NumberSerializer();

        public NumberSerializer() {
            super(Number.class);
        }

        public final void serialize(Number number, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            if (number instanceof BigDecimal) {
                jsonGenerator.writeNumber((BigDecimal) number);
            } else if (number instanceof BigInteger) {
                jsonGenerator.writeNumber((BigInteger) number);
            } else if (number instanceof Double) {
                jsonGenerator.writeNumber(((Double) number).doubleValue());
            } else if (number instanceof Float) {
                jsonGenerator.writeNumber(((Float) number).floatValue());
            } else {
                jsonGenerator.writeNumber(number.toString());
            }
        }

        public final JsonNode getSchema(SerializerProvider serializerProvider, Type type) {
            return createSchemaNode("number", true);
        }
    }

    @JacksonStdImpl
    public static final class CalendarSerializer extends ScalarSerializerBase<Calendar> {
        public static final CalendarSerializer instance = new CalendarSerializer();

        public CalendarSerializer() {
            super(Calendar.class);
        }

        public final void serialize(Calendar calendar, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            serializerProvider.defaultSerializeDateValue(calendar.getTimeInMillis(), jsonGenerator);
        }

        public final JsonNode getSchema(SerializerProvider serializerProvider, Type type) {
            return createSchemaNode(serializerProvider.isEnabled(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS) ? "number" : "string", true);
        }
    }

    @JacksonStdImpl
    public static final class UtilDateSerializer extends ScalarSerializerBase<Date> {
        public static final UtilDateSerializer instance = new UtilDateSerializer();

        public final /* bridge */ /* synthetic */ void serialize(Object obj, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            serializerProvider.defaultSerializeDateValue((Date) obj, jsonGenerator);
        }

        public UtilDateSerializer() {
            super(Date.class);
        }

        public final void serialize(Date date, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            serializerProvider.defaultSerializeDateValue(date, jsonGenerator);
        }

        public final JsonNode getSchema(SerializerProvider serializerProvider, Type type) throws JsonMappingException {
            return createSchemaNode(serializerProvider.isEnabled(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS) ? "number" : "string", true);
        }
    }

    @JacksonStdImpl
    public static final class SqlDateSerializer extends ScalarSerializerBase<java.sql.Date> {
        public SqlDateSerializer() {
            super(java.sql.Date.class);
        }

        public final void serialize(java.sql.Date date, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            jsonGenerator.writeString(date.toString());
        }

        public final JsonNode getSchema(SerializerProvider serializerProvider, Type type) {
            return createSchemaNode("string", true);
        }
    }

    @JacksonStdImpl
    public static final class SqlTimeSerializer extends ScalarSerializerBase<Time> {
        public SqlTimeSerializer() {
            super(Time.class);
        }

        public final void serialize(Time time, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            jsonGenerator.writeString(time.toString());
        }

        public final JsonNode getSchema(SerializerProvider serializerProvider, Type type) {
            return createSchemaNode("string", true);
        }
    }

    @JacksonStdImpl
    public static final class SerializableSerializer extends SerializerBase<JsonSerializable> {
        protected static final SerializableSerializer instance = new SerializableSerializer();

        private SerializableSerializer() {
            super(JsonSerializable.class);
        }

        public final void serialize(JsonSerializable jsonSerializable, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            jsonSerializable.serialize(jsonGenerator, serializerProvider);
        }

        public final void serializeWithType(JsonSerializable jsonSerializable, JsonGenerator jsonGenerator, SerializerProvider serializerProvider, TypeSerializer typeSerializer) throws IOException, JsonGenerationException {
            if (jsonSerializable instanceof JsonSerializableWithType) {
                ((JsonSerializableWithType) jsonSerializable).serializeWithType(jsonGenerator, serializerProvider, typeSerializer);
            } else {
                serialize(jsonSerializable, jsonGenerator, serializerProvider);
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: org.codehaus.jackson.node.ObjectNode.put(java.lang.String, boolean):void
         arg types: [java.lang.String, int]
         candidates:
          org.codehaus.jackson.node.ObjectNode.put(java.lang.String, org.codehaus.jackson.JsonNode):org.codehaus.jackson.JsonNode
          org.codehaus.jackson.node.ObjectNode.put(java.lang.String, double):void
          org.codehaus.jackson.node.ObjectNode.put(java.lang.String, float):void
          org.codehaus.jackson.node.ObjectNode.put(java.lang.String, int):void
          org.codehaus.jackson.node.ObjectNode.put(java.lang.String, long):void
          org.codehaus.jackson.node.ObjectNode.put(java.lang.String, java.lang.String):void
          org.codehaus.jackson.node.ObjectNode.put(java.lang.String, java.math.BigDecimal):void
          org.codehaus.jackson.node.ObjectNode.put(java.lang.String, byte[]):void
          org.codehaus.jackson.node.ObjectNode.put(java.lang.String, boolean):void */
        /* JADX WARNING: Removed duplicated region for block: B:12:0x004d  */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0061  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final org.codehaus.jackson.JsonNode getSchema(org.codehaus.jackson.map.SerializerProvider r8, java.lang.reflect.Type r9) throws org.codehaus.jackson.map.JsonMappingException {
            /*
                r7 = this;
                r2 = 0
                org.codehaus.jackson.node.ObjectNode r4 = r7.createObjectNode()
                java.lang.String r0 = "any"
                if (r9 == 0) goto L_0x008c
                org.codehaus.jackson.type.JavaType r1 = org.codehaus.jackson.map.type.TypeFactory.type(r9)
                java.lang.Class r1 = r1.getRawClass()
                java.lang.Class<org.codehaus.jackson.schema.JsonSerializableSchema> r3 = org.codehaus.jackson.schema.JsonSerializableSchema.class
                boolean r3 = r1.isAnnotationPresent(r3)
                if (r3 == 0) goto L_0x008c
                java.lang.Class<org.codehaus.jackson.schema.JsonSerializableSchema> r0 = org.codehaus.jackson.schema.JsonSerializableSchema.class
                java.lang.annotation.Annotation r0 = r1.getAnnotation(r0)
                org.codehaus.jackson.schema.JsonSerializableSchema r0 = (org.codehaus.jackson.schema.JsonSerializableSchema) r0
                java.lang.String r3 = r0.schemaType()
                java.lang.String r1 = "##irrelevant"
                java.lang.String r5 = r0.schemaObjectPropertiesDefinition()
                boolean r1 = r1.equals(r5)
                if (r1 != 0) goto L_0x008a
                java.lang.String r1 = r0.schemaObjectPropertiesDefinition()
            L_0x0035:
                java.lang.String r5 = "##irrelevant"
                java.lang.String r6 = r0.schemaItemDefinition()
                boolean r5 = r5.equals(r6)
                if (r5 != 0) goto L_0x0088
                java.lang.String r2 = r0.schemaItemDefinition()
                r0 = r3
            L_0x0046:
                java.lang.String r3 = "type"
                r4.put(r3, r0)
                if (r1 == 0) goto L_0x005f
                java.lang.String r3 = "properties"
                org.codehaus.jackson.map.ObjectMapper r0 = new org.codehaus.jackson.map.ObjectMapper     // Catch:{ IOException -> 0x007a }
                r0.<init>()     // Catch:{ IOException -> 0x007a }
                java.lang.Class<org.codehaus.jackson.JsonNode> r5 = org.codehaus.jackson.JsonNode.class
                java.lang.Object r0 = r0.readValue(r1, r5)     // Catch:{ IOException -> 0x007a }
                org.codehaus.jackson.JsonNode r0 = (org.codehaus.jackson.JsonNode) r0     // Catch:{ IOException -> 0x007a }
                r4.put(r3, r0)     // Catch:{ IOException -> 0x007a }
            L_0x005f:
                if (r2 == 0) goto L_0x0073
                java.lang.String r1 = "items"
                org.codehaus.jackson.map.ObjectMapper r0 = new org.codehaus.jackson.map.ObjectMapper     // Catch:{ IOException -> 0x0081 }
                r0.<init>()     // Catch:{ IOException -> 0x0081 }
                java.lang.Class<org.codehaus.jackson.JsonNode> r3 = org.codehaus.jackson.JsonNode.class
                java.lang.Object r0 = r0.readValue(r2, r3)     // Catch:{ IOException -> 0x0081 }
                org.codehaus.jackson.JsonNode r0 = (org.codehaus.jackson.JsonNode) r0     // Catch:{ IOException -> 0x0081 }
                r4.put(r1, r0)     // Catch:{ IOException -> 0x0081 }
            L_0x0073:
                java.lang.String r0 = "optional"
                r1 = 1
                r4.put(r0, r1)
                return r4
            L_0x007a:
                r0 = move-exception
                java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
                r1.<init>(r0)
                throw r1
            L_0x0081:
                r0 = move-exception
                java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
                r1.<init>(r0)
                throw r1
            L_0x0088:
                r0 = r3
                goto L_0x0046
            L_0x008a:
                r1 = r2
                goto L_0x0035
            L_0x008c:
                r1 = r2
                goto L_0x0046
            */
            throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.map.ser.StdSerializers.SerializableSerializer.getSchema(org.codehaus.jackson.map.SerializerProvider, java.lang.reflect.Type):org.codehaus.jackson.JsonNode");
        }
    }

    @JacksonStdImpl
    public static final class SerializableWithTypeSerializer extends SerializerBase<JsonSerializableWithType> {
        protected static final SerializableWithTypeSerializer instance = new SerializableWithTypeSerializer();

        private SerializableWithTypeSerializer() {
            super(JsonSerializableWithType.class);
        }

        public final void serialize(JsonSerializableWithType jsonSerializableWithType, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            jsonSerializableWithType.serialize(jsonGenerator, serializerProvider);
        }

        public final void serializeWithType(JsonSerializableWithType jsonSerializableWithType, JsonGenerator jsonGenerator, SerializerProvider serializerProvider, TypeSerializer typeSerializer) throws IOException, JsonGenerationException {
            jsonSerializableWithType.serializeWithType(jsonGenerator, serializerProvider, typeSerializer);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: org.codehaus.jackson.node.ObjectNode.put(java.lang.String, boolean):void
         arg types: [java.lang.String, int]
         candidates:
          org.codehaus.jackson.node.ObjectNode.put(java.lang.String, org.codehaus.jackson.JsonNode):org.codehaus.jackson.JsonNode
          org.codehaus.jackson.node.ObjectNode.put(java.lang.String, double):void
          org.codehaus.jackson.node.ObjectNode.put(java.lang.String, float):void
          org.codehaus.jackson.node.ObjectNode.put(java.lang.String, int):void
          org.codehaus.jackson.node.ObjectNode.put(java.lang.String, long):void
          org.codehaus.jackson.node.ObjectNode.put(java.lang.String, java.lang.String):void
          org.codehaus.jackson.node.ObjectNode.put(java.lang.String, java.math.BigDecimal):void
          org.codehaus.jackson.node.ObjectNode.put(java.lang.String, byte[]):void
          org.codehaus.jackson.node.ObjectNode.put(java.lang.String, boolean):void */
        /* JADX WARNING: Removed duplicated region for block: B:12:0x004d  */
        /* JADX WARNING: Removed duplicated region for block: B:16:0x0061  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final org.codehaus.jackson.JsonNode getSchema(org.codehaus.jackson.map.SerializerProvider r8, java.lang.reflect.Type r9) throws org.codehaus.jackson.map.JsonMappingException {
            /*
                r7 = this;
                r2 = 0
                org.codehaus.jackson.node.ObjectNode r4 = r7.createObjectNode()
                java.lang.String r0 = "any"
                if (r9 == 0) goto L_0x008c
                org.codehaus.jackson.type.JavaType r1 = org.codehaus.jackson.map.type.TypeFactory.type(r9)
                java.lang.Class r1 = r1.getRawClass()
                java.lang.Class<org.codehaus.jackson.schema.JsonSerializableSchema> r3 = org.codehaus.jackson.schema.JsonSerializableSchema.class
                boolean r3 = r1.isAnnotationPresent(r3)
                if (r3 == 0) goto L_0x008c
                java.lang.Class<org.codehaus.jackson.schema.JsonSerializableSchema> r0 = org.codehaus.jackson.schema.JsonSerializableSchema.class
                java.lang.annotation.Annotation r0 = r1.getAnnotation(r0)
                org.codehaus.jackson.schema.JsonSerializableSchema r0 = (org.codehaus.jackson.schema.JsonSerializableSchema) r0
                java.lang.String r3 = r0.schemaType()
                java.lang.String r1 = "##irrelevant"
                java.lang.String r5 = r0.schemaObjectPropertiesDefinition()
                boolean r1 = r1.equals(r5)
                if (r1 != 0) goto L_0x008a
                java.lang.String r1 = r0.schemaObjectPropertiesDefinition()
            L_0x0035:
                java.lang.String r5 = "##irrelevant"
                java.lang.String r6 = r0.schemaItemDefinition()
                boolean r5 = r5.equals(r6)
                if (r5 != 0) goto L_0x0088
                java.lang.String r2 = r0.schemaItemDefinition()
                r0 = r3
            L_0x0046:
                java.lang.String r3 = "type"
                r4.put(r3, r0)
                if (r1 == 0) goto L_0x005f
                java.lang.String r3 = "properties"
                org.codehaus.jackson.map.ObjectMapper r0 = new org.codehaus.jackson.map.ObjectMapper     // Catch:{ IOException -> 0x007a }
                r0.<init>()     // Catch:{ IOException -> 0x007a }
                java.lang.Class<org.codehaus.jackson.JsonNode> r5 = org.codehaus.jackson.JsonNode.class
                java.lang.Object r0 = r0.readValue(r1, r5)     // Catch:{ IOException -> 0x007a }
                org.codehaus.jackson.JsonNode r0 = (org.codehaus.jackson.JsonNode) r0     // Catch:{ IOException -> 0x007a }
                r4.put(r3, r0)     // Catch:{ IOException -> 0x007a }
            L_0x005f:
                if (r2 == 0) goto L_0x0073
                java.lang.String r1 = "items"
                org.codehaus.jackson.map.ObjectMapper r0 = new org.codehaus.jackson.map.ObjectMapper     // Catch:{ IOException -> 0x0081 }
                r0.<init>()     // Catch:{ IOException -> 0x0081 }
                java.lang.Class<org.codehaus.jackson.JsonNode> r3 = org.codehaus.jackson.JsonNode.class
                java.lang.Object r0 = r0.readValue(r2, r3)     // Catch:{ IOException -> 0x0081 }
                org.codehaus.jackson.JsonNode r0 = (org.codehaus.jackson.JsonNode) r0     // Catch:{ IOException -> 0x0081 }
                r4.put(r1, r0)     // Catch:{ IOException -> 0x0081 }
            L_0x0073:
                java.lang.String r0 = "optional"
                r1 = 1
                r4.put(r0, r1)
                return r4
            L_0x007a:
                r0 = move-exception
                java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
                r1.<init>(r0)
                throw r1
            L_0x0081:
                r0 = move-exception
                java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
                r1.<init>(r0)
                throw r1
            L_0x0088:
                r0 = r3
                goto L_0x0046
            L_0x008a:
                r1 = r2
                goto L_0x0035
            L_0x008c:
                r1 = r2
                goto L_0x0046
            */
            throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.map.ser.StdSerializers.SerializableWithTypeSerializer.getSchema(org.codehaus.jackson.map.SerializerProvider, java.lang.reflect.Type):org.codehaus.jackson.JsonNode");
        }
    }

    @JacksonStdImpl
    public static final class TokenBufferSerializer extends SerializerBase<TokenBuffer> {
        public final /* bridge */ /* synthetic */ void serialize(Object obj, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            ((TokenBuffer) obj).serialize(jsonGenerator);
        }

        public TokenBufferSerializer() {
            super(TokenBuffer.class);
        }

        public final void serialize(TokenBuffer tokenBuffer, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonGenerationException {
            tokenBuffer.serialize(jsonGenerator);
        }

        public final void serializeWithType(TokenBuffer tokenBuffer, JsonGenerator jsonGenerator, SerializerProvider serializerProvider, TypeSerializer typeSerializer) throws IOException, JsonGenerationException {
            typeSerializer.writeTypePrefixForScalar(tokenBuffer, jsonGenerator);
            tokenBuffer.serialize(jsonGenerator);
            typeSerializer.writeTypeSuffixForScalar(tokenBuffer, jsonGenerator);
        }

        public final JsonNode getSchema(SerializerProvider serializerProvider, Type type) {
            return createSchemaNode("any", true);
        }
    }
}
