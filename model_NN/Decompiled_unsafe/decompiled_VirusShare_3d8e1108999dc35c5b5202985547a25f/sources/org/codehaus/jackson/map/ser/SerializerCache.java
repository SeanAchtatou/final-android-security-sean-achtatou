package org.codehaus.jackson.map.ser;

import java.util.HashMap;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.ser.impl.ReadOnlyClassToSerializerMap;
import org.codehaus.jackson.type.JavaType;

public final class SerializerCache {
    private ReadOnlyClassToSerializerMap _readOnlyMap = null;
    private HashMap<TypeKey, JsonSerializer<Object>> _sharedMap = new HashMap<>(64);

    public final ReadOnlyClassToSerializerMap getReadOnlyLookupMap() {
        ReadOnlyClassToSerializerMap readOnlyClassToSerializerMap;
        synchronized (this) {
            readOnlyClassToSerializerMap = this._readOnlyMap;
            if (readOnlyClassToSerializerMap == null) {
                readOnlyClassToSerializerMap = ReadOnlyClassToSerializerMap.from(this._sharedMap);
                this._readOnlyMap = readOnlyClassToSerializerMap;
            }
        }
        return readOnlyClassToSerializerMap.instance();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void
     arg types: [java.lang.Class<?>, int]
     candidates:
      org.codehaus.jackson.map.ser.SerializerCache.TypeKey.<init>(org.codehaus.jackson.type.JavaType, boolean):void
      org.codehaus.jackson.map.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void */
    public final JsonSerializer<Object> untypedValueSerializer(Class<?> cls) {
        JsonSerializer<Object> jsonSerializer;
        synchronized (this) {
            jsonSerializer = this._sharedMap.get(new TypeKey(cls, false));
        }
        return jsonSerializer;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.ser.SerializerCache.TypeKey.<init>(org.codehaus.jackson.type.JavaType, boolean):void
     arg types: [org.codehaus.jackson.type.JavaType, int]
     candidates:
      org.codehaus.jackson.map.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void
      org.codehaus.jackson.map.ser.SerializerCache.TypeKey.<init>(org.codehaus.jackson.type.JavaType, boolean):void */
    public final JsonSerializer<Object> untypedValueSerializer(JavaType javaType) {
        JsonSerializer<Object> jsonSerializer;
        synchronized (this) {
            jsonSerializer = this._sharedMap.get(new TypeKey(javaType, false));
        }
        return jsonSerializer;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.ser.SerializerCache.TypeKey.<init>(org.codehaus.jackson.type.JavaType, boolean):void
     arg types: [org.codehaus.jackson.type.JavaType, int]
     candidates:
      org.codehaus.jackson.map.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void
      org.codehaus.jackson.map.ser.SerializerCache.TypeKey.<init>(org.codehaus.jackson.type.JavaType, boolean):void */
    public final JsonSerializer<Object> typedValueSerializer(JavaType javaType) {
        JsonSerializer<Object> jsonSerializer;
        synchronized (this) {
            jsonSerializer = this._sharedMap.get(new TypeKey(javaType, true));
        }
        return jsonSerializer;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void
     arg types: [java.lang.Class<?>, int]
     candidates:
      org.codehaus.jackson.map.ser.SerializerCache.TypeKey.<init>(org.codehaus.jackson.type.JavaType, boolean):void
      org.codehaus.jackson.map.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void */
    public final JsonSerializer<Object> typedValueSerializer(Class<?> cls) {
        JsonSerializer<Object> jsonSerializer;
        synchronized (this) {
            jsonSerializer = this._sharedMap.get(new TypeKey(cls, true));
        }
        return jsonSerializer;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.ser.SerializerCache.TypeKey.<init>(org.codehaus.jackson.type.JavaType, boolean):void
     arg types: [org.codehaus.jackson.type.JavaType, int]
     candidates:
      org.codehaus.jackson.map.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void
      org.codehaus.jackson.map.ser.SerializerCache.TypeKey.<init>(org.codehaus.jackson.type.JavaType, boolean):void */
    public final void addTypedSerializer(JavaType javaType, JsonSerializer<Object> jsonSerializer) {
        synchronized (this) {
            if (this._sharedMap.put(new TypeKey(javaType, true), jsonSerializer) == null) {
                this._readOnlyMap = null;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void
     arg types: [java.lang.Class<?>, int]
     candidates:
      org.codehaus.jackson.map.ser.SerializerCache.TypeKey.<init>(org.codehaus.jackson.type.JavaType, boolean):void
      org.codehaus.jackson.map.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void */
    public final void addTypedSerializer(Class<?> cls, JsonSerializer<Object> jsonSerializer) {
        synchronized (this) {
            if (this._sharedMap.put(new TypeKey(cls, true), jsonSerializer) == null) {
                this._readOnlyMap = null;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void
     arg types: [java.lang.Class<?>, int]
     candidates:
      org.codehaus.jackson.map.ser.SerializerCache.TypeKey.<init>(org.codehaus.jackson.type.JavaType, boolean):void
      org.codehaus.jackson.map.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void */
    public final void addNonTypedSerializer(Class<?> cls, JsonSerializer<Object> jsonSerializer) {
        synchronized (this) {
            if (this._sharedMap.put(new TypeKey(cls, false), jsonSerializer) == null) {
                this._readOnlyMap = null;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.ser.SerializerCache.TypeKey.<init>(org.codehaus.jackson.type.JavaType, boolean):void
     arg types: [org.codehaus.jackson.type.JavaType, int]
     candidates:
      org.codehaus.jackson.map.ser.SerializerCache.TypeKey.<init>(java.lang.Class<?>, boolean):void
      org.codehaus.jackson.map.ser.SerializerCache.TypeKey.<init>(org.codehaus.jackson.type.JavaType, boolean):void */
    public final void addNonTypedSerializer(JavaType javaType, JsonSerializer<Object> jsonSerializer) {
        synchronized (this) {
            if (this._sharedMap.put(new TypeKey(javaType, false), jsonSerializer) == null) {
                this._readOnlyMap = null;
            }
        }
    }

    public final synchronized int size() {
        return this._sharedMap.size();
    }

    public final synchronized void flush() {
        this._sharedMap.clear();
    }

    public static final class TypeKey {
        protected Class<?> _class;
        protected int _hashCode;
        protected boolean _isTyped;
        protected JavaType _type;

        public TypeKey(Class<?> cls, boolean z) {
            this._class = cls;
            this._type = null;
            this._isTyped = z;
            this._hashCode = hash(cls, z);
        }

        public TypeKey(JavaType javaType, boolean z) {
            this._type = javaType;
            this._class = null;
            this._isTyped = z;
            this._hashCode = hash(javaType, z);
        }

        private static final int hash(Class<?> cls, boolean z) {
            int hashCode = cls.getName().hashCode();
            if (z) {
                return hashCode + 1;
            }
            return hashCode;
        }

        private static final int hash(JavaType javaType, boolean z) {
            int hashCode = javaType.hashCode() - 1;
            if (z) {
                return hashCode - 1;
            }
            return hashCode;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: org.codehaus.jackson.map.ser.SerializerCache.TypeKey.hash(java.lang.Class<?>, boolean):int
         arg types: [java.lang.Class<?>, int]
         candidates:
          org.codehaus.jackson.map.ser.SerializerCache.TypeKey.hash(org.codehaus.jackson.type.JavaType, boolean):int
          org.codehaus.jackson.map.ser.SerializerCache.TypeKey.hash(java.lang.Class<?>, boolean):int */
        public final void resetTyped(Class<?> cls) {
            this._type = null;
            this._class = cls;
            this._isTyped = true;
            this._hashCode = hash(cls, true);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: org.codehaus.jackson.map.ser.SerializerCache.TypeKey.hash(java.lang.Class<?>, boolean):int
         arg types: [java.lang.Class<?>, int]
         candidates:
          org.codehaus.jackson.map.ser.SerializerCache.TypeKey.hash(org.codehaus.jackson.type.JavaType, boolean):int
          org.codehaus.jackson.map.ser.SerializerCache.TypeKey.hash(java.lang.Class<?>, boolean):int */
        public final void resetUntyped(Class<?> cls) {
            this._type = null;
            this._class = cls;
            this._isTyped = false;
            this._hashCode = hash(cls, false);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: org.codehaus.jackson.map.ser.SerializerCache.TypeKey.hash(org.codehaus.jackson.type.JavaType, boolean):int
         arg types: [org.codehaus.jackson.type.JavaType, int]
         candidates:
          org.codehaus.jackson.map.ser.SerializerCache.TypeKey.hash(java.lang.Class<?>, boolean):int
          org.codehaus.jackson.map.ser.SerializerCache.TypeKey.hash(org.codehaus.jackson.type.JavaType, boolean):int */
        public final void resetTyped(JavaType javaType) {
            this._type = javaType;
            this._class = null;
            this._isTyped = true;
            this._hashCode = hash(javaType, true);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: org.codehaus.jackson.map.ser.SerializerCache.TypeKey.hash(org.codehaus.jackson.type.JavaType, boolean):int
         arg types: [org.codehaus.jackson.type.JavaType, int]
         candidates:
          org.codehaus.jackson.map.ser.SerializerCache.TypeKey.hash(java.lang.Class<?>, boolean):int
          org.codehaus.jackson.map.ser.SerializerCache.TypeKey.hash(org.codehaus.jackson.type.JavaType, boolean):int */
        public final void resetUntyped(JavaType javaType) {
            this._type = javaType;
            this._class = null;
            this._isTyped = false;
            this._hashCode = hash(javaType, false);
        }

        public final int hashCode() {
            return this._hashCode;
        }

        public final String toString() {
            if (this._class != null) {
                return "{class: " + this._class.getName() + ", typed? " + this._isTyped + "}";
            }
            return "{type: " + this._type + ", typed? " + this._isTyped + "}";
        }

        public final boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            TypeKey typeKey = (TypeKey) obj;
            if (typeKey._isTyped != this._isTyped) {
                return false;
            }
            if (this._class == null) {
                return this._type.equals(typeKey._type);
            }
            if (typeKey._class != this._class) {
                return false;
            }
            return true;
        }
    }
}
