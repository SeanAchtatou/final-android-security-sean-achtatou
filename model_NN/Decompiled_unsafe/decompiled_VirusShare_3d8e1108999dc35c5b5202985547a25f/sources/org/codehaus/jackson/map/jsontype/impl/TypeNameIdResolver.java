package org.codehaus.jackson.map.jsontype.impl;

import java.util.Collection;
import java.util.HashMap;
import org.codehaus.jackson.annotate.JsonTypeInfo;
import org.codehaus.jackson.map.jsontype.NamedType;
import org.codehaus.jackson.map.type.TypeFactory;
import org.codehaus.jackson.type.JavaType;

public class TypeNameIdResolver extends TypeIdResolverBase {
    protected final HashMap<String, JavaType> _idToType;
    protected final HashMap<String, String> _typeToId;

    protected TypeNameIdResolver(JavaType javaType, HashMap<String, String> hashMap, HashMap<String, JavaType> hashMap2) {
        super(javaType);
        this._typeToId = hashMap;
        this._idToType = hashMap2;
    }

    public static TypeNameIdResolver construct(JavaType javaType, Collection<NamedType> collection, boolean z, boolean z2) {
        HashMap hashMap;
        HashMap hashMap2;
        if (z == z2) {
            throw new IllegalArgumentException();
        }
        if (z) {
            hashMap = new HashMap();
        } else {
            hashMap = null;
        }
        if (z2) {
            hashMap2 = new HashMap();
        } else {
            hashMap2 = null;
        }
        if (collection != null) {
            for (NamedType next : collection) {
                Class<?> type = next.getType();
                String name = next.hasName() ? next.getName() : _defaultTypeId(type);
                if (z) {
                    hashMap.put(type.getName(), name);
                }
                if (z2 && !hashMap2.containsKey(name)) {
                    hashMap2.put(name, TypeFactory.type(type));
                }
            }
        }
        return new TypeNameIdResolver(javaType, hashMap, hashMap2);
    }

    public JsonTypeInfo.Id getMechanism() {
        return JsonTypeInfo.Id.NAME;
    }

    public String idFromValue(Object obj) {
        Class<?> cls = obj.getClass();
        String str = this._typeToId.get(cls.getName());
        if (str == null) {
            return _defaultTypeId(cls);
        }
        return str;
    }

    public JavaType typeFromId(String str) throws IllegalArgumentException {
        return this._idToType.get(str);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('[').append(getClass().getName());
        sb.append("; id-to-type=").append(this._idToType);
        sb.append(']');
        return sb.toString();
    }

    protected static String _defaultTypeId(Class<?> cls) {
        String name = cls.getName();
        int lastIndexOf = name.lastIndexOf(46);
        return lastIndexOf < 0 ? name : name.substring(lastIndexOf + 1);
    }
}
