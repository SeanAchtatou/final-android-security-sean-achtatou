package org.codehaus.jackson.map.jsontype.impl;

import org.codehaus.jackson.map.jsontype.TypeIdResolver;
import org.codehaus.jackson.type.JavaType;

public abstract class TypeIdResolverBase implements TypeIdResolver {
    protected final JavaType _baseType;

    protected TypeIdResolverBase(JavaType javaType) {
        this._baseType = javaType;
    }

    public void init(JavaType javaType) {
    }
}
