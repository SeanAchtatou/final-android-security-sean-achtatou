package org.codehaus.jackson.map.util;

import java.io.IOException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.BeanProperty;
import org.codehaus.jackson.map.JsonSerializableWithType;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.TypeSerializer;
import org.codehaus.jackson.map.type.TypeFactory;
import org.codehaus.jackson.type.JavaType;

public class JSONPObject implements JsonSerializableWithType {
    protected final String _function;
    protected final JavaType _serializationType;
    protected final Object _value;

    public JSONPObject(String str, Object obj) {
        this(str, obj, (JavaType) null);
    }

    public JSONPObject(String str, Object obj, JavaType javaType) {
        this._function = str;
        this._value = obj;
        this._serializationType = javaType;
    }

    public JSONPObject(String str, Object obj, Class<?> cls) {
        this._function = str;
        this._value = obj;
        this._serializationType = cls == null ? null : TypeFactory.type(cls);
    }

    public void serializeWithType(JsonGenerator jsonGenerator, SerializerProvider serializerProvider, TypeSerializer typeSerializer) throws IOException, JsonProcessingException {
        serialize(jsonGenerator, serializerProvider);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.SerializerProvider.findTypedValueSerializer(org.codehaus.jackson.type.JavaType, boolean, org.codehaus.jackson.map.BeanProperty):org.codehaus.jackson.map.JsonSerializer<java.lang.Object>
     arg types: [org.codehaus.jackson.type.JavaType, int, ?[OBJECT, ARRAY]]
     candidates:
      org.codehaus.jackson.map.SerializerProvider.findTypedValueSerializer(java.lang.Class<?>, boolean, org.codehaus.jackson.map.BeanProperty):org.codehaus.jackson.map.JsonSerializer<java.lang.Object>
      org.codehaus.jackson.map.SerializerProvider.findTypedValueSerializer(org.codehaus.jackson.type.JavaType, boolean, org.codehaus.jackson.map.BeanProperty):org.codehaus.jackson.map.JsonSerializer<java.lang.Object> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.codehaus.jackson.map.SerializerProvider.findTypedValueSerializer(java.lang.Class<?>, boolean, org.codehaus.jackson.map.BeanProperty):org.codehaus.jackson.map.JsonSerializer<java.lang.Object>
     arg types: [java.lang.Class<?>, int, ?[OBJECT, ARRAY]]
     candidates:
      org.codehaus.jackson.map.SerializerProvider.findTypedValueSerializer(org.codehaus.jackson.type.JavaType, boolean, org.codehaus.jackson.map.BeanProperty):org.codehaus.jackson.map.JsonSerializer<java.lang.Object>
      org.codehaus.jackson.map.SerializerProvider.findTypedValueSerializer(java.lang.Class<?>, boolean, org.codehaus.jackson.map.BeanProperty):org.codehaus.jackson.map.JsonSerializer<java.lang.Object> */
    public void serialize(JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeRaw(this._function);
        jsonGenerator.writeRaw('(');
        if (this._value == null) {
            serializerProvider.defaultSerializeNull(jsonGenerator);
        } else if (this._serializationType != null) {
            serializerProvider.findTypedValueSerializer(this._serializationType, true, (BeanProperty) null).serialize(this._value, jsonGenerator, serializerProvider);
        } else {
            serializerProvider.findTypedValueSerializer(this._value.getClass(), true, (BeanProperty) null).serialize(this._value, jsonGenerator, serializerProvider);
        }
        jsonGenerator.writeRaw(')');
    }

    public String getFunction() {
        return this._function;
    }

    public Object getValue() {
        return this._value;
    }

    public JavaType getSerializationType() {
        return this._serializationType;
    }
}
