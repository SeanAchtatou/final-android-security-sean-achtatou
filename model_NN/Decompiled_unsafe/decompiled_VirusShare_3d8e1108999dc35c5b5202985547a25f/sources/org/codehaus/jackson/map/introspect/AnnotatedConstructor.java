package org.codehaus.jackson.map.introspect;

import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.lang.reflect.Type;

public final class AnnotatedConstructor extends AnnotatedWithParams {
    protected final Constructor<?> _constructor;

    public AnnotatedConstructor(Constructor<?> constructor, AnnotationMap annotationMap, AnnotationMap[] annotationMapArr) {
        super(annotationMap, annotationMapArr);
        if (constructor == null) {
            throw new IllegalArgumentException("Null constructor not allowed");
        }
        this._constructor = constructor;
    }

    public final Constructor<?> getAnnotated() {
        return this._constructor;
    }

    public final int getModifiers() {
        return this._constructor.getModifiers();
    }

    public final String getName() {
        return this._constructor.getName();
    }

    public final Type getGenericType() {
        return getRawType();
    }

    public final Class<?> getRawType() {
        return this._constructor.getDeclaringClass();
    }

    public final AnnotatedParameter getParameter(int i) {
        return new AnnotatedParameter(this, getParameterType(i), this._paramAnnotations[i]);
    }

    public final int getParameterCount() {
        return this._constructor.getParameterTypes().length;
    }

    public final Class<?> getParameterClass(int i) {
        Class<?>[] parameterTypes = this._constructor.getParameterTypes();
        if (i >= parameterTypes.length) {
            return null;
        }
        return parameterTypes[i];
    }

    public final Type getParameterType(int i) {
        Type[] genericParameterTypes = this._constructor.getGenericParameterTypes();
        if (i >= genericParameterTypes.length) {
            return null;
        }
        return genericParameterTypes[i];
    }

    public final Class<?> getDeclaringClass() {
        return this._constructor.getDeclaringClass();
    }

    public final Member getMember() {
        return this._constructor;
    }

    public final String toString() {
        return "[constructor for " + getName() + ", annotations: " + this._annotations + "]";
    }
}
