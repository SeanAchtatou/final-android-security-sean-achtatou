package org.codehaus.jackson.map.introspect;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Member;
import java.lang.reflect.Type;
import org.codehaus.jackson.map.type.TypeFactory;

public final class AnnotatedParameter extends AnnotatedMember {
    protected final AnnotationMap _annotations;
    protected final AnnotatedMember _owner;
    protected final Type _type;

    public AnnotatedParameter(AnnotatedMember annotatedMember, Type type, AnnotationMap annotationMap) {
        this._owner = annotatedMember;
        this._type = type;
        this._annotations = annotationMap;
    }

    public final void addOrOverride(Annotation annotation) {
        this._annotations.add(annotation);
    }

    public final AnnotatedElement getAnnotated() {
        return null;
    }

    public final int getModifiers() {
        return this._owner.getModifiers();
    }

    public final String getName() {
        return "";
    }

    public final <A extends Annotation> A getAnnotation(Class<A> cls) {
        return this._annotations.get(cls);
    }

    public final Type getGenericType() {
        return this._type;
    }

    public final Class<?> getRawType() {
        return TypeFactory.type(this._type).getRawClass();
    }

    public final Class<?> getDeclaringClass() {
        return this._owner.getDeclaringClass();
    }

    public final Member getMember() {
        return this._owner.getMember();
    }

    public final Type getParameterType() {
        return this._type;
    }
}
