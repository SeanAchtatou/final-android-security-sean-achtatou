package org.codehaus.jackson.map.util;

import org.codehaus.jackson.io.SerializedString;
import org.codehaus.jackson.map.MapperConfig;
import org.codehaus.jackson.map.type.ClassKey;
import org.codehaus.jackson.type.JavaType;

public class RootNameLookup {
    protected LRUMap<ClassKey, SerializedString> _rootNames;

    public SerializedString findRootName(JavaType javaType, MapperConfig<?> mapperConfig) {
        return findRootName(javaType.getRawClass(), mapperConfig);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0042, code lost:
        if (r0 != null) goto L_0x0038;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized org.codehaus.jackson.io.SerializedString findRootName(java.lang.Class<?> r5, org.codehaus.jackson.map.MapperConfig<?> r6) {
        /*
            r4 = this;
            monitor-enter(r4)
            org.codehaus.jackson.map.type.ClassKey r2 = new org.codehaus.jackson.map.type.ClassKey     // Catch:{ all -> 0x0045 }
            r2.<init>(r5)     // Catch:{ all -> 0x0045 }
            org.codehaus.jackson.map.util.LRUMap<org.codehaus.jackson.map.type.ClassKey, org.codehaus.jackson.io.SerializedString> r0 = r4._rootNames     // Catch:{ all -> 0x0045 }
            if (r0 != 0) goto L_0x003a
            org.codehaus.jackson.map.util.LRUMap r0 = new org.codehaus.jackson.map.util.LRUMap     // Catch:{ all -> 0x0045 }
            r1 = 20
            r3 = 200(0xc8, float:2.8E-43)
            r0.<init>(r1, r3)     // Catch:{ all -> 0x0045 }
            r4._rootNames = r0     // Catch:{ all -> 0x0045 }
        L_0x0015:
            org.codehaus.jackson.map.BeanDescription r0 = r6.introspectClassAnnotations(r5)     // Catch:{ all -> 0x0045 }
            org.codehaus.jackson.map.introspect.BasicBeanDescription r0 = (org.codehaus.jackson.map.introspect.BasicBeanDescription) r0     // Catch:{ all -> 0x0045 }
            org.codehaus.jackson.map.AnnotationIntrospector r1 = r6.getAnnotationIntrospector()     // Catch:{ all -> 0x0045 }
            org.codehaus.jackson.map.introspect.AnnotatedClass r0 = r0.getClassInfo()     // Catch:{ all -> 0x0045 }
            java.lang.String r0 = r1.findRootName(r0)     // Catch:{ all -> 0x0045 }
            if (r0 != 0) goto L_0x0048
            java.lang.String r0 = r5.getSimpleName()     // Catch:{ all -> 0x0045 }
            r1 = r0
        L_0x002e:
            org.codehaus.jackson.io.SerializedString r0 = new org.codehaus.jackson.io.SerializedString     // Catch:{ all -> 0x0045 }
            r0.<init>(r1)     // Catch:{ all -> 0x0045 }
            org.codehaus.jackson.map.util.LRUMap<org.codehaus.jackson.map.type.ClassKey, org.codehaus.jackson.io.SerializedString> r1 = r4._rootNames     // Catch:{ all -> 0x0045 }
            r1.put(r2, r0)     // Catch:{ all -> 0x0045 }
        L_0x0038:
            monitor-exit(r4)
            return r0
        L_0x003a:
            org.codehaus.jackson.map.util.LRUMap<org.codehaus.jackson.map.type.ClassKey, org.codehaus.jackson.io.SerializedString> r0 = r4._rootNames     // Catch:{ all -> 0x0045 }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ all -> 0x0045 }
            org.codehaus.jackson.io.SerializedString r0 = (org.codehaus.jackson.io.SerializedString) r0     // Catch:{ all -> 0x0045 }
            if (r0 == 0) goto L_0x0015
            goto L_0x0038
        L_0x0045:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        L_0x0048:
            r1 = r0
            goto L_0x002e
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.map.util.RootNameLookup.findRootName(java.lang.Class, org.codehaus.jackson.map.MapperConfig):org.codehaus.jackson.io.SerializedString");
    }
}
