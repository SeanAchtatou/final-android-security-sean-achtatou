package org.codehaus.jackson.impl;

import java.io.Reader;
import org.codehaus.jackson.io.IOContext;

public abstract class ReaderBasedNumericParser extends ReaderBasedParserBase {
    public ReaderBasedNumericParser(IOContext iOContext, int i, Reader reader) {
        super(iOContext, i, reader);
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x002a  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00b3 A[EDGE_INSN: B:62:0x00b3->B:52:0x00b3 ?: BREAK  , SYNTHETIC] */
    protected final org.codehaus.jackson.JsonToken parseNumberText(int r14) throws java.io.IOException, org.codehaus.jackson.JsonParseException {
        /*
            r13 = this;
            r11 = 45
            r1 = 1
            r2 = 0
            r10 = 57
            r9 = 48
            if (r14 != r11) goto L_0x0048
            r0 = r1
        L_0x000b:
            int r4 = r13._inputPtr
            int r5 = r4 + -1
            int r7 = r13._inputEnd
            if (r0 == 0) goto L_0x00c9
            int r3 = r13._inputEnd
            if (r4 >= r3) goto L_0x00b3
            char[] r6 = r13._inputBuffer
            int r3 = r4 + 1
            char r4 = r6[r4]
            if (r4 > r10) goto L_0x0021
            if (r4 >= r9) goto L_0x0026
        L_0x0021:
            java.lang.String r6 = "expected digit (0-9) to follow minus sign, for valid numeric value"
            r13.reportUnexpectedNumberChar(r4, r6)
        L_0x0026:
            int r4 = r13._inputEnd
            if (r3 >= r4) goto L_0x00b3
            char[] r6 = r13._inputBuffer
            int r4 = r3 + 1
            char r3 = r6[r3]
            if (r3 < r9) goto L_0x004a
            if (r3 > r10) goto L_0x004a
            int r1 = r1 + 1
            r3 = 2
            if (r1 != r3) goto L_0x00c9
            char[] r3 = r13._inputBuffer
            int r6 = r4 + -2
            char r3 = r3[r6]
            if (r3 != r9) goto L_0x00c9
            java.lang.String r3 = "Leading zeroes not allowed"
            r13.reportInvalidNumber(r3)
            r3 = r4
            goto L_0x0026
        L_0x0048:
            r0 = r2
            goto L_0x000b
        L_0x004a:
            r6 = 46
            if (r3 != r6) goto L_0x00c4
            r3 = r2
            r6 = r4
        L_0x0050:
            if (r6 >= r7) goto L_0x00b3
            char[] r8 = r13._inputBuffer
            int r4 = r6 + 1
            char r6 = r8[r6]
            if (r6 < r9) goto L_0x0060
            if (r6 > r10) goto L_0x0060
            int r3 = r3 + 1
            r6 = r4
            goto L_0x0050
        L_0x0060:
            if (r3 != 0) goto L_0x0067
            java.lang.String r8 = "Decimal point not followed by a digit"
            r13.reportUnexpectedNumberChar(r6, r8)
        L_0x0067:
            r12 = r3
            r3 = r4
            r4 = r6
            r6 = r12
        L_0x006b:
            r8 = 101(0x65, float:1.42E-43)
            if (r4 == r8) goto L_0x0073
            r8 = 69
            if (r4 != r8) goto L_0x00a2
        L_0x0073:
            if (r3 >= r7) goto L_0x00b3
            char[] r8 = r13._inputBuffer
            int r4 = r3 + 1
            char r3 = r8[r3]
            if (r3 == r11) goto L_0x0081
            r8 = 43
            if (r3 != r8) goto L_0x00c0
        L_0x0081:
            if (r4 >= r7) goto L_0x00b3
            char[] r8 = r13._inputBuffer
            int r3 = r4 + 1
            char r4 = r8[r4]
        L_0x0089:
            if (r4 > r10) goto L_0x009b
            if (r4 < r9) goto L_0x009b
            int r2 = r2 + 1
            if (r3 >= r7) goto L_0x00b3
            char[] r8 = r13._inputBuffer
            int r4 = r3 + 1
            char r3 = r8[r3]
            r12 = r4
            r4 = r3
            r3 = r12
            goto L_0x0089
        L_0x009b:
            if (r2 != 0) goto L_0x00a2
            java.lang.String r7 = "Exponent indicator not followed by a digit"
            r13.reportUnexpectedNumberChar(r4, r7)
        L_0x00a2:
            int r3 = r3 + -1
            r13._inputPtr = r3
            int r3 = r3 - r5
            org.codehaus.jackson.util.TextBuffer r4 = r13._textBuffer
            char[] r7 = r13._inputBuffer
            r4.resetWithShared(r7, r5, r3)
            org.codehaus.jackson.JsonToken r0 = r13.reset(r0, r1, r6, r2)
        L_0x00b2:
            return r0
        L_0x00b3:
            if (r0 == 0) goto L_0x00be
            int r1 = r5 + 1
        L_0x00b7:
            r13._inputPtr = r1
            org.codehaus.jackson.JsonToken r0 = r13.parseNumberText2(r0)
            goto L_0x00b2
        L_0x00be:
            r1 = r5
            goto L_0x00b7
        L_0x00c0:
            r12 = r4
            r4 = r3
            r3 = r12
            goto L_0x0089
        L_0x00c4:
            r6 = r2
            r12 = r4
            r4 = r3
            r3 = r12
            goto L_0x006b
        L_0x00c9:
            r3 = r4
            goto L_0x0026
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.impl.ReaderBasedNumericParser.parseNumberText(int):org.codehaus.jackson.JsonToken");
    }

    /* JADX WARNING: Removed duplicated region for block: B:53:0x00df  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final org.codehaus.jackson.JsonToken parseNumberText2(boolean r15) throws java.io.IOException, org.codehaus.jackson.JsonParseException {
        /*
            r14 = this;
            r10 = 45
            r12 = 57
            r11 = 48
            r1 = 1
            r2 = 0
            org.codehaus.jackson.util.TextBuffer r0 = r14._textBuffer
            char[] r3 = r0.emptyAndGetCurrentSegment()
            if (r15 == 0) goto L_0x0182
            r3[r2] = r10
            r0 = r1
        L_0x0013:
            r4 = r3
            r3 = r0
            r0 = r2
        L_0x0016:
            int r5 = r14._inputPtr
            int r6 = r14._inputEnd
            if (r5 < r6) goto L_0x00f6
            boolean r5 = r14.loadMore()
            if (r5 != 0) goto L_0x00f6
            r8 = r1
            r5 = r2
        L_0x0024:
            if (r0 != 0) goto L_0x0046
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = "Missing integer part (next char "
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = _getCharDesc(r5)
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r7 = ")"
            java.lang.StringBuilder r6 = r6.append(r7)
            java.lang.String r6 = r6.toString()
            r14.reportInvalidNumber(r6)
        L_0x0046:
            r6 = 46
            if (r5 != r6) goto L_0x0177
            int r6 = r3 + 1
            r4[r3] = r5
            r3 = r2
            r13 = r5
            r5 = r6
            r6 = r4
            r4 = r13
        L_0x0053:
            int r7 = r14._inputPtr
            int r9 = r14._inputEnd
            if (r7 < r9) goto L_0x0124
            boolean r7 = r14.loadMore()
            if (r7 != 0) goto L_0x0124
            r7 = r1
        L_0x0060:
            if (r3 != 0) goto L_0x0067
            java.lang.String r8 = "Decimal point not followed by a digit"
            r14.reportUnexpectedNumberChar(r4, r8)
        L_0x0067:
            r9 = r3
            r3 = r5
            r13 = r4
            r4 = r6
            r6 = r13
        L_0x006c:
            r5 = 101(0x65, float:1.42E-43)
            if (r6 == r5) goto L_0x0074
            r5 = 69
            if (r6 != r5) goto L_0x016f
        L_0x0074:
            int r5 = r4.length
            if (r3 < r5) goto L_0x007f
            org.codehaus.jackson.util.TextBuffer r3 = r14._textBuffer
            char[] r3 = r3.finishCurrentSegment()
            r4 = r3
            r3 = r2
        L_0x007f:
            int r5 = r3 + 1
            r4[r3] = r6
            int r3 = r14._inputPtr
            int r6 = r14._inputEnd
            if (r3 >= r6) goto L_0x0144
            char[] r3 = r14._inputBuffer
            int r6 = r14._inputPtr
            int r8 = r6 + 1
            r14._inputPtr = r8
            char r6 = r3[r6]
        L_0x0093:
            if (r6 == r10) goto L_0x0099
            r3 = 43
            if (r6 != r3) goto L_0x0169
        L_0x0099:
            int r3 = r4.length
            if (r5 < r3) goto L_0x0166
            org.codehaus.jackson.util.TextBuffer r3 = r14._textBuffer
            char[] r4 = r3.finishCurrentSegment()
            r3 = r2
        L_0x00a3:
            int r5 = r3 + 1
            r4[r3] = r6
            int r3 = r14._inputPtr
            int r6 = r14._inputEnd
            if (r3 >= r6) goto L_0x014c
            char[] r3 = r14._inputBuffer
            int r6 = r14._inputPtr
            int r8 = r6 + 1
            r14._inputPtr = r8
            char r3 = r3[r6]
            r6 = r4
            r4 = r2
        L_0x00b9:
            r8 = r3
            r3 = r5
        L_0x00bb:
            if (r8 > r12) goto L_0x0162
            if (r8 < r11) goto L_0x0162
            int r4 = r4 + 1
            int r5 = r6.length
            if (r3 < r5) goto L_0x00cb
            org.codehaus.jackson.util.TextBuffer r3 = r14._textBuffer
            char[] r6 = r3.finishCurrentSegment()
            r3 = r2
        L_0x00cb:
            int r5 = r3 + 1
            r6[r3] = r8
            int r3 = r14._inputPtr
            int r10 = r14._inputEnd
            if (r3 < r10) goto L_0x0156
            boolean r3 = r14.loadMore()
            if (r3 != 0) goto L_0x0156
            r2 = r4
            r3 = r5
        L_0x00dd:
            if (r2 != 0) goto L_0x00e4
            java.lang.String r4 = "Exponent indicator not followed by a digit"
            r14.reportUnexpectedNumberChar(r8, r4)
        L_0x00e4:
            if (r1 != 0) goto L_0x00ec
            int r1 = r14._inputPtr
            int r1 = r1 + -1
            r14._inputPtr = r1
        L_0x00ec:
            org.codehaus.jackson.util.TextBuffer r1 = r14._textBuffer
            r1.setCurrentLength(r3)
            org.codehaus.jackson.JsonToken r0 = r14.reset(r15, r0, r9, r2)
            return r0
        L_0x00f6:
            char[] r5 = r14._inputBuffer
            int r6 = r14._inputPtr
            int r7 = r6 + 1
            r14._inputPtr = r7
            char r6 = r5[r6]
            if (r6 < r11) goto L_0x017e
            if (r6 > r12) goto L_0x017e
            int r0 = r0 + 1
            r5 = 2
            if (r0 != r5) goto L_0x0114
            int r5 = r3 + -1
            char r5 = r4[r5]
            if (r5 != r11) goto L_0x0114
            java.lang.String r5 = "Leading zeroes not allowed"
            r14.reportInvalidNumber(r5)
        L_0x0114:
            int r5 = r4.length
            if (r3 < r5) goto L_0x017c
            org.codehaus.jackson.util.TextBuffer r3 = r14._textBuffer
            char[] r4 = r3.finishCurrentSegment()
            r5 = r2
        L_0x011e:
            int r3 = r5 + 1
            r4[r5] = r6
            goto L_0x0016
        L_0x0124:
            char[] r4 = r14._inputBuffer
            int r7 = r14._inputPtr
            int r9 = r7 + 1
            r14._inputPtr = r9
            char r4 = r4[r7]
            if (r4 < r11) goto L_0x0174
            if (r4 > r12) goto L_0x0174
            int r3 = r3 + 1
            int r7 = r6.length
            if (r5 < r7) goto L_0x0172
            org.codehaus.jackson.util.TextBuffer r5 = r14._textBuffer
            char[] r6 = r5.finishCurrentSegment()
            r7 = r2
        L_0x013e:
            int r5 = r7 + 1
            r6[r7] = r4
            goto L_0x0053
        L_0x0144:
            java.lang.String r3 = "expected a digit for number exponent"
            char r6 = r14.getNextChar(r3)
            goto L_0x0093
        L_0x014c:
            java.lang.String r3 = "expected a digit for number exponent"
            char r3 = r14.getNextChar(r3)
            r6 = r4
            r4 = r2
            goto L_0x00b9
        L_0x0156:
            char[] r3 = r14._inputBuffer
            int r8 = r14._inputPtr
            int r10 = r8 + 1
            r14._inputPtr = r10
            char r3 = r3[r8]
            goto L_0x00b9
        L_0x0162:
            r2 = r4
            r1 = r7
            goto L_0x00dd
        L_0x0166:
            r3 = r5
            goto L_0x00a3
        L_0x0169:
            r8 = r6
            r3 = r5
            r6 = r4
            r4 = r2
            goto L_0x00bb
        L_0x016f:
            r1 = r7
            goto L_0x00e4
        L_0x0172:
            r7 = r5
            goto L_0x013e
        L_0x0174:
            r7 = r8
            goto L_0x0060
        L_0x0177:
            r9 = r2
            r7 = r8
            r6 = r5
            goto L_0x006c
        L_0x017c:
            r5 = r3
            goto L_0x011e
        L_0x017e:
            r8 = r2
            r5 = r6
            goto L_0x0024
        L_0x0182:
            r0 = r2
            goto L_0x0013
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.impl.ReaderBasedNumericParser.parseNumberText2(boolean):org.codehaus.jackson.JsonToken");
    }
}
