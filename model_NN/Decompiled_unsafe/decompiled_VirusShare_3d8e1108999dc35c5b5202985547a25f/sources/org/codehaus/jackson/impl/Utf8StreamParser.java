package org.codehaus.jackson.impl;

import java.io.IOException;
import java.io.InputStream;
import org.codehaus.jackson.Base64Variant;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.ObjectCodec;
import org.codehaus.jackson.io.IOContext;
import org.codehaus.jackson.sym.BytesToNameCanonicalizer;
import org.codehaus.jackson.sym.Name;
import org.codehaus.jackson.util.ByteArrayBuilder;
import org.codehaus.jackson.util.CharTypes;

public final class Utf8StreamParser extends StreamBasedParserBase {
    private static final byte BYTE_0 = 0;
    static final byte BYTE_LF = 10;
    private static final int[] sInputCodesLatin1 = CharTypes.getInputCodeLatin1();
    private static final int[] sInputCodesUtf8 = CharTypes.getInputCodeUtf8();
    protected ObjectCodec _objectCodec;
    private int _quad1;
    protected int[] _quadBuffer = new int[16];
    protected final BytesToNameCanonicalizer _symbols;
    protected boolean _tokenIncomplete = false;

    public Utf8StreamParser(IOContext iOContext, int i, InputStream inputStream, ObjectCodec objectCodec, BytesToNameCanonicalizer bytesToNameCanonicalizer, byte[] bArr, int i2, int i3, boolean z) {
        super(iOContext, i, inputStream, bArr, i2, i3, z);
        this._objectCodec = objectCodec;
        this._symbols = bytesToNameCanonicalizer;
        if (!JsonParser.Feature.CANONICALIZE_FIELD_NAMES.enabledIn(i)) {
            _throwInternal();
        }
    }

    public final ObjectCodec getCodec() {
        return this._objectCodec;
    }

    public final void setCodec(ObjectCodec objectCodec) {
        this._objectCodec = objectCodec;
    }

    public final String getText() throws IOException, JsonParseException {
        JsonToken jsonToken = this._currToken;
        if (jsonToken != JsonToken.VALUE_STRING) {
            return _getText2(jsonToken);
        }
        if (this._tokenIncomplete) {
            this._tokenIncomplete = false;
            _finishString();
        }
        return this._textBuffer.contentsAsString();
    }

    /* access modifiers changed from: protected */
    public final String _getText2(JsonToken jsonToken) {
        if (jsonToken == null) {
            return null;
        }
        switch (jsonToken) {
            case FIELD_NAME:
                return this._parsingContext.getCurrentName();
            case VALUE_STRING:
            case VALUE_NUMBER_INT:
            case VALUE_NUMBER_FLOAT:
                return this._textBuffer.contentsAsString();
            default:
                return jsonToken.asString();
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final char[] getTextCharacters() throws IOException, JsonParseException {
        if (this._currToken == null) {
            return null;
        }
        switch (this._currToken) {
            case FIELD_NAME:
                if (!this._nameCopied) {
                    String currentName = this._parsingContext.getCurrentName();
                    int length = currentName.length();
                    if (this._nameCopyBuffer == null) {
                        this._nameCopyBuffer = this._ioContext.allocNameCopyBuffer(length);
                    } else if (this._nameCopyBuffer.length < length) {
                        this._nameCopyBuffer = new char[length];
                    }
                    currentName.getChars(0, length, this._nameCopyBuffer, 0);
                    this._nameCopied = true;
                }
                return this._nameCopyBuffer;
            case VALUE_STRING:
                if (this._tokenIncomplete) {
                    this._tokenIncomplete = false;
                    _finishString();
                    break;
                }
                break;
            case VALUE_NUMBER_INT:
            case VALUE_NUMBER_FLOAT:
                break;
            default:
                return this._currToken.asCharArray();
        }
        return this._textBuffer.getTextBuffer();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final int getTextLength() throws IOException, JsonParseException {
        if (this._currToken == null) {
            return 0;
        }
        switch (this._currToken) {
            case FIELD_NAME:
                return this._parsingContext.getCurrentName().length();
            case VALUE_STRING:
                if (this._tokenIncomplete) {
                    this._tokenIncomplete = false;
                    _finishString();
                    break;
                }
                break;
            case VALUE_NUMBER_INT:
            case VALUE_NUMBER_FLOAT:
                break;
            default:
                return this._currToken.asCharArray().length;
        }
        return this._textBuffer.size();
    }

    public final int getTextOffset() throws IOException, JsonParseException {
        if (this._currToken == null) {
            return 0;
        }
        switch (this._currToken) {
            case FIELD_NAME:
            default:
                return 0;
            case VALUE_STRING:
                if (this._tokenIncomplete) {
                    this._tokenIncomplete = false;
                    _finishString();
                    break;
                }
                break;
            case VALUE_NUMBER_INT:
            case VALUE_NUMBER_FLOAT:
                break;
        }
        return this._textBuffer.getTextOffset();
    }

    public final byte[] getBinaryValue(Base64Variant base64Variant) throws IOException, JsonParseException {
        if (this._currToken != JsonToken.VALUE_STRING && (this._currToken != JsonToken.VALUE_EMBEDDED_OBJECT || this._binaryValue == null)) {
            _reportError("Current token (" + this._currToken + ") not VALUE_STRING or VALUE_EMBEDDED_OBJECT, can not access as binary");
        }
        if (this._tokenIncomplete) {
            try {
                this._binaryValue = _decodeBase64(base64Variant);
                this._tokenIncomplete = false;
            } catch (IllegalArgumentException e) {
                throw _constructError("Failed to decode VALUE_STRING as base64 (" + base64Variant + "): " + e.getMessage());
            }
        }
        return this._binaryValue;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final JsonToken nextToken() throws IOException, JsonParseException {
        JsonToken parseNumberText;
        if (this._currToken == JsonToken.FIELD_NAME) {
            return _nextAfterName();
        }
        if (this._tokenIncomplete) {
            _skipString();
        }
        int _skipWSOrEnd = _skipWSOrEnd();
        if (_skipWSOrEnd < 0) {
            close();
            this._currToken = null;
            return null;
        }
        this._tokenInputTotal = (this._currInputProcessed + ((long) this._inputPtr)) - 1;
        this._tokenInputRow = this._currInputRow;
        this._tokenInputCol = (this._inputPtr - this._currInputRowStart) - 1;
        this._binaryValue = null;
        if (_skipWSOrEnd == 93) {
            if (!this._parsingContext.inArray()) {
                _reportMismatchedEndMarker(_skipWSOrEnd, '}');
            }
            this._parsingContext = this._parsingContext.getParent();
            JsonToken jsonToken = JsonToken.END_ARRAY;
            this._currToken = jsonToken;
            return jsonToken;
        } else if (_skipWSOrEnd == 125) {
            if (!this._parsingContext.inObject()) {
                _reportMismatchedEndMarker(_skipWSOrEnd, ']');
            }
            this._parsingContext = this._parsingContext.getParent();
            JsonToken jsonToken2 = JsonToken.END_OBJECT;
            this._currToken = jsonToken2;
            return jsonToken2;
        } else {
            if (this._parsingContext.expectComma()) {
                if (_skipWSOrEnd != 44) {
                    _reportUnexpectedChar(_skipWSOrEnd, "was expecting comma to separate " + this._parsingContext.getTypeDesc() + " entries");
                }
                _skipWSOrEnd = _skipWS();
            }
            if (!this._parsingContext.inObject()) {
                return _nextTokenNotInObject(_skipWSOrEnd);
            }
            this._parsingContext.setCurrentName(_parseFieldName(_skipWSOrEnd).getName());
            this._currToken = JsonToken.FIELD_NAME;
            int _skipWS = _skipWS();
            if (_skipWS != 58) {
                _reportUnexpectedChar(_skipWS, "was expecting a colon to separate field name and value");
            }
            int _skipWS2 = _skipWS();
            if (_skipWS2 == 34) {
                this._tokenIncomplete = true;
                this._nextToken = JsonToken.VALUE_STRING;
                return this._currToken;
            }
            switch (_skipWS2) {
                case 45:
                case 48:
                case 49:
                case 50:
                case 51:
                case 52:
                case 53:
                case 54:
                case 55:
                case 56:
                case 57:
                    parseNumberText = parseNumberText(_skipWS2);
                    break;
                case 91:
                    parseNumberText = JsonToken.START_ARRAY;
                    break;
                case 93:
                case 125:
                    _reportUnexpectedChar(_skipWS2, "expected a value");
                    _matchToken(JsonToken.VALUE_TRUE);
                    parseNumberText = JsonToken.VALUE_TRUE;
                    break;
                case 102:
                    _matchToken(JsonToken.VALUE_FALSE);
                    parseNumberText = JsonToken.VALUE_FALSE;
                    break;
                case 110:
                    _matchToken(JsonToken.VALUE_NULL);
                    parseNumberText = JsonToken.VALUE_NULL;
                    break;
                case 116:
                    _matchToken(JsonToken.VALUE_TRUE);
                    parseNumberText = JsonToken.VALUE_TRUE;
                    break;
                case 123:
                    parseNumberText = JsonToken.START_OBJECT;
                    break;
                default:
                    parseNumberText = _handleUnexpectedValue(_skipWS2);
                    break;
            }
            this._nextToken = parseNumberText;
            return this._currToken;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private final JsonToken _nextTokenNotInObject(int i) throws IOException, JsonParseException {
        if (i == 34) {
            this._tokenIncomplete = true;
            JsonToken jsonToken = JsonToken.VALUE_STRING;
            this._currToken = jsonToken;
            return jsonToken;
        }
        switch (i) {
            case 45:
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
                JsonToken parseNumberText = parseNumberText(i);
                this._currToken = parseNumberText;
                return parseNumberText;
            case 91:
                this._parsingContext = this._parsingContext.createChildArrayContext(this._tokenInputRow, this._tokenInputCol);
                JsonToken jsonToken2 = JsonToken.START_ARRAY;
                this._currToken = jsonToken2;
                return jsonToken2;
            case 93:
            case 125:
                _reportUnexpectedChar(i, "expected a value");
                break;
            case 102:
                _matchToken(JsonToken.VALUE_FALSE);
                JsonToken jsonToken3 = JsonToken.VALUE_FALSE;
                this._currToken = jsonToken3;
                return jsonToken3;
            case 110:
                _matchToken(JsonToken.VALUE_NULL);
                JsonToken jsonToken4 = JsonToken.VALUE_NULL;
                this._currToken = jsonToken4;
                return jsonToken4;
            case 116:
                break;
            case 123:
                this._parsingContext = this._parsingContext.createChildObjectContext(this._tokenInputRow, this._tokenInputCol);
                JsonToken jsonToken5 = JsonToken.START_OBJECT;
                this._currToken = jsonToken5;
                return jsonToken5;
            default:
                JsonToken _handleUnexpectedValue = _handleUnexpectedValue(i);
                this._currToken = _handleUnexpectedValue;
                return _handleUnexpectedValue;
        }
        _matchToken(JsonToken.VALUE_TRUE);
        JsonToken jsonToken6 = JsonToken.VALUE_TRUE;
        this._currToken = jsonToken6;
        return jsonToken6;
    }

    private final JsonToken _nextAfterName() {
        this._nameCopied = false;
        JsonToken jsonToken = this._nextToken;
        this._nextToken = null;
        if (jsonToken == JsonToken.START_ARRAY) {
            this._parsingContext = this._parsingContext.createChildArrayContext(this._tokenInputRow, this._tokenInputCol);
        } else if (jsonToken == JsonToken.START_OBJECT) {
            this._parsingContext = this._parsingContext.createChildObjectContext(this._tokenInputRow, this._tokenInputCol);
        }
        this._currToken = jsonToken;
        return jsonToken;
    }

    public final void close() throws IOException {
        super.close();
        this._symbols.release();
    }

    /* access modifiers changed from: protected */
    public final JsonToken parseNumberText(int i) throws IOException, JsonParseException {
        int i2 = 1;
        int i3 = 0;
        char[] emptyAndGetCurrentSegment = this._textBuffer.emptyAndGetCurrentSegment();
        boolean z = i == 45;
        if (z) {
            emptyAndGetCurrentSegment[0] = '-';
            if (this._inputPtr >= this._inputEnd) {
                loadMoreGuaranteed();
            }
            byte[] bArr = this._inputBuffer;
            int i4 = this._inputPtr;
            this._inputPtr = i4 + 1;
            i = bArr[i4] & 255;
            if (i < 48 || i > 57) {
                reportInvalidNumber("Missing integer part (next char " + _getCharDesc(i) + ")");
            }
            i3 = 1;
        }
        if (i == 48) {
            _verifyNoLeadingZeroes();
        }
        int i5 = i3 + 1;
        emptyAndGetCurrentSegment[i3] = (char) i;
        int length = this._inputPtr + emptyAndGetCurrentSegment.length;
        if (length > this._inputEnd) {
            length = this._inputEnd;
        }
        while (this._inputPtr < length) {
            byte[] bArr2 = this._inputBuffer;
            int i6 = this._inputPtr;
            this._inputPtr = i6 + 1;
            byte b = bArr2[i6] & 255;
            if (b >= 48 && b <= 57) {
                i2++;
                emptyAndGetCurrentSegment[i5] = (char) b;
                i5++;
            } else if (b == 46 || b == 101 || b == 69) {
                return _parseFloatText(emptyAndGetCurrentSegment, i5, b, z, i2);
            } else {
                this._inputPtr--;
                this._textBuffer.setCurrentLength(i5);
                return resetInt(z, i2);
            }
        }
        return _parserNumber2(emptyAndGetCurrentSegment, i5, z, i2);
    }

    private final JsonToken _parserNumber2(char[] cArr, int i, boolean z, int i2) throws IOException, JsonParseException {
        byte b;
        int i3 = i2;
        int i4 = i;
        char[] cArr2 = cArr;
        while (true) {
            if (this._inputPtr < this._inputEnd || loadMore()) {
                byte[] bArr = this._inputBuffer;
                int i5 = this._inputPtr;
                this._inputPtr = i5 + 1;
                b = bArr[i5] & 255;
                if (b <= 57 && b >= 48) {
                    if (i4 >= cArr2.length) {
                        cArr2 = this._textBuffer.finishCurrentSegment();
                        i4 = 0;
                    }
                    int i6 = i4;
                    i4 = i6 + 1;
                    cArr2[i6] = (char) b;
                    i3++;
                }
            } else {
                this._textBuffer.setCurrentLength(i4);
                return resetInt(z, i3);
            }
        }
        if (b == 46 || b == 101 || b == 69) {
            return _parseFloatText(cArr2, i4, b, z, i3);
        }
        this._inputPtr--;
        this._textBuffer.setCurrentLength(i4);
        return resetInt(z, i3);
    }

    private final void _verifyNoLeadingZeroes() throws IOException, JsonParseException {
        if ((this._inputPtr < this._inputEnd || loadMore()) && this._inputBuffer[this._inputPtr] == 0) {
            reportInvalidNumber("Leading zeroes not allowed");
        }
    }

    private final JsonToken _parseFloatText(char[] cArr, int i, int i2, boolean z, int i3) throws IOException, JsonParseException {
        int i4;
        char[] cArr2;
        int i5;
        int i6;
        boolean z2;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11 = 0;
        boolean z3 = false;
        if (i2 == 46) {
            int i12 = i + 1;
            cArr[i] = (char) i2;
            while (true) {
                if (this._inputPtr >= this._inputEnd && !loadMore()) {
                    z3 = true;
                    break;
                }
                byte[] bArr = this._inputBuffer;
                int i13 = this._inputPtr;
                this._inputPtr = i13 + 1;
                i2 = bArr[i13] & 255;
                if (i2 < 48 || i2 > 57) {
                    break;
                }
                i11++;
                if (i12 >= cArr.length) {
                    cArr = this._textBuffer.finishCurrentSegment();
                    i12 = 0;
                }
                int i14 = i12;
                i12 = i14 + 1;
                cArr[i14] = (char) i2;
            }
            if (i11 == 0) {
                reportUnexpectedNumberChar(i2, "Decimal point not followed by a digit");
            }
            i4 = i11;
            i5 = i12;
            cArr2 = cArr;
        } else {
            i4 = 0;
            cArr2 = cArr;
            i5 = i;
        }
        if (i2 == 101 || i2 == 69) {
            if (i5 >= cArr2.length) {
                cArr2 = this._textBuffer.finishCurrentSegment();
                i5 = 0;
            }
            int i15 = i5 + 1;
            cArr2[i5] = (char) i2;
            if (this._inputPtr >= this._inputEnd) {
                loadMoreGuaranteed();
            }
            byte[] bArr2 = this._inputBuffer;
            int i16 = this._inputPtr;
            this._inputPtr = i16 + 1;
            byte b = bArr2[i16] & 255;
            if (b == 45 || b == 43) {
                if (i15 >= cArr2.length) {
                    cArr2 = this._textBuffer.finishCurrentSegment();
                    i10 = 0;
                } else {
                    i10 = i15;
                }
                int i17 = i10 + 1;
                cArr2[i10] = (char) b;
                if (this._inputPtr >= this._inputEnd) {
                    loadMoreGuaranteed();
                }
                byte[] bArr3 = this._inputBuffer;
                int i18 = this._inputPtr;
                this._inputPtr = i18 + 1;
                b = bArr3[i18] & 255;
                i9 = i17;
                i8 = 0;
            } else {
                i9 = i15;
                i8 = 0;
            }
            while (true) {
                if (b <= 57 && b >= 48) {
                    i8++;
                    if (i9 >= cArr2.length) {
                        cArr2 = this._textBuffer.finishCurrentSegment();
                        i9 = 0;
                    }
                    int i19 = i9 + 1;
                    cArr2[i9] = (char) b;
                    if (this._inputPtr >= this._inputEnd && !loadMore()) {
                        i7 = i8;
                        z2 = true;
                        i6 = i19;
                        break;
                    }
                    byte[] bArr4 = this._inputBuffer;
                    int i20 = this._inputPtr;
                    this._inputPtr = i20 + 1;
                    b = bArr4[i20] & 255;
                    i9 = i19;
                } else {
                    z2 = z3;
                    int i21 = i8;
                    i6 = i9;
                    i7 = i21;
                }
            }
            z2 = z3;
            int i212 = i8;
            i6 = i9;
            i7 = i212;
            if (i7 == 0) {
                reportUnexpectedNumberChar(b, "Exponent indicator not followed by a digit");
            }
        } else {
            z2 = z3;
            i6 = i5;
            i7 = 0;
        }
        if (!z2) {
            this._inputPtr--;
        }
        this._textBuffer.setCurrentLength(i6);
        return resetFloat(z, i3, i4, i7);
    }

    /* access modifiers changed from: protected */
    public final Name _parseFieldName(int i) throws IOException, JsonParseException {
        if (i != 34) {
            return _handleUnusualFieldName(i);
        }
        if (this._inputPtr + 9 > this._inputEnd) {
            return slowParseFieldName();
        }
        byte[] bArr = this._inputBuffer;
        int[] iArr = sInputCodesLatin1;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        byte b = bArr[i2] & 255;
        if (iArr[b] == 0) {
            int i3 = this._inputPtr;
            this._inputPtr = i3 + 1;
            byte b2 = bArr[i3] & 255;
            if (iArr[b2] == 0) {
                byte b3 = (b << 8) | b2;
                int i4 = this._inputPtr;
                this._inputPtr = i4 + 1;
                byte b4 = bArr[i4] & 255;
                if (iArr[b4] == 0) {
                    byte b5 = (b3 << 8) | b4;
                    int i5 = this._inputPtr;
                    this._inputPtr = i5 + 1;
                    byte b6 = bArr[i5] & 255;
                    if (iArr[b6] == 0) {
                        byte b7 = (b5 << 8) | b6;
                        int i6 = this._inputPtr;
                        this._inputPtr = i6 + 1;
                        byte b8 = bArr[i6] & 255;
                        if (iArr[b8] == 0) {
                            this._quad1 = b7;
                            return parseMediumFieldName(b8, iArr);
                        } else if (b8 == 34) {
                            return findName(b7, 4);
                        } else {
                            return parseFieldName(b7, b8, 4);
                        }
                    } else if (b6 == 34) {
                        return findName(b5, 3);
                    } else {
                        return parseFieldName(b5, b6, 3);
                    }
                } else if (b4 == 34) {
                    return findName(b3, 2);
                } else {
                    return parseFieldName(b3, b4, 2);
                }
            } else if (b2 == 34) {
                return findName(b, 1);
            } else {
                return parseFieldName(b, b2, 1);
            }
        } else if (b == 34) {
            return BytesToNameCanonicalizer.getEmptyName();
        } else {
            return parseFieldName(0, b, 0);
        }
    }

    /* access modifiers changed from: protected */
    public final Name parseMediumFieldName(int i, int[] iArr) throws IOException, JsonParseException {
        byte[] bArr = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        byte b = bArr[i2] & 255;
        if (iArr[b] == 0) {
            byte b2 = b | (i << 8);
            byte[] bArr2 = this._inputBuffer;
            int i3 = this._inputPtr;
            this._inputPtr = i3 + 1;
            byte b3 = bArr2[i3] & 255;
            if (iArr[b3] == 0) {
                byte b4 = (b2 << 8) | b3;
                byte[] bArr3 = this._inputBuffer;
                int i4 = this._inputPtr;
                this._inputPtr = i4 + 1;
                byte b5 = bArr3[i4] & 255;
                if (iArr[b5] == 0) {
                    int i5 = (b4 << 8) | b5;
                    byte[] bArr4 = this._inputBuffer;
                    int i6 = this._inputPtr;
                    this._inputPtr = i6 + 1;
                    byte b6 = bArr4[i6] & 255;
                    if (iArr[b6] == 0) {
                        this._quadBuffer[0] = this._quad1;
                        this._quadBuffer[1] = i5;
                        return parseLongFieldName(b6);
                    } else if (b6 == 34) {
                        return findName(this._quad1, i5, 4);
                    } else {
                        return parseFieldName(this._quad1, i5, b6, 4);
                    }
                } else if (b5 == 34) {
                    return findName(this._quad1, b4, 3);
                } else {
                    return parseFieldName(this._quad1, b4, b5, 3);
                }
            } else if (b3 == 34) {
                return findName(this._quad1, b2, 2);
            } else {
                return parseFieldName(this._quad1, b2, b3, 2);
            }
        } else if (b == 34) {
            return findName(this._quad1, i, 1);
        } else {
            return parseFieldName(this._quad1, i, b, 1);
        }
    }

    /* access modifiers changed from: protected */
    public final Name parseLongFieldName(int i) throws IOException, JsonParseException {
        int[] iArr = sInputCodesLatin1;
        int i2 = 2;
        byte b = i;
        while (this._inputEnd - this._inputPtr >= 4) {
            byte[] bArr = this._inputBuffer;
            int i3 = this._inputPtr;
            this._inputPtr = i3 + 1;
            byte b2 = bArr[i3] & 255;
            if (iArr[b2] == 0) {
                byte b3 = (b << 8) | b2;
                byte[] bArr2 = this._inputBuffer;
                int i4 = this._inputPtr;
                this._inputPtr = i4 + 1;
                byte b4 = bArr2[i4] & 255;
                if (iArr[b4] == 0) {
                    byte b5 = (b3 << 8) | b4;
                    byte[] bArr3 = this._inputBuffer;
                    int i5 = this._inputPtr;
                    this._inputPtr = i5 + 1;
                    byte b6 = bArr3[i5] & 255;
                    if (iArr[b6] == 0) {
                        int i6 = (b5 << 8) | b6;
                        byte[] bArr4 = this._inputBuffer;
                        int i7 = this._inputPtr;
                        this._inputPtr = i7 + 1;
                        b = bArr4[i7] & 255;
                        if (iArr[b] == 0) {
                            if (i2 >= this._quadBuffer.length) {
                                this._quadBuffer = growArrayBy(this._quadBuffer, i2);
                            }
                            this._quadBuffer[i2] = i6;
                            i2++;
                        } else if (b == 34) {
                            return findName(this._quadBuffer, i2, i6, 4);
                        } else {
                            return parseEscapedFieldName(this._quadBuffer, i2, i6, b, 4);
                        }
                    } else if (b6 == 34) {
                        return findName(this._quadBuffer, i2, b5, 3);
                    } else {
                        return parseEscapedFieldName(this._quadBuffer, i2, b5, b6, 3);
                    }
                } else if (b4 == 34) {
                    return findName(this._quadBuffer, i2, b3, 2);
                } else {
                    return parseEscapedFieldName(this._quadBuffer, i2, b3, b4, 2);
                }
            } else if (b2 == 34) {
                return findName(this._quadBuffer, i2, b, 1);
            } else {
                return parseEscapedFieldName(this._quadBuffer, i2, b, b2, 1);
            }
        }
        return parseEscapedFieldName(this._quadBuffer, i2, 0, b, 0);
    }

    /* access modifiers changed from: protected */
    public final Name slowParseFieldName() throws IOException, JsonParseException {
        if (this._inputPtr >= this._inputEnd && !loadMore()) {
            _reportInvalidEOF(": was expecting closing '\"' for name");
        }
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        byte b = bArr[i] & 255;
        if (b == 34) {
            return BytesToNameCanonicalizer.getEmptyName();
        }
        return parseEscapedFieldName(this._quadBuffer, 0, 0, b, 0);
    }

    private final Name parseFieldName(int i, int i2, int i3) throws IOException, JsonParseException {
        return parseEscapedFieldName(this._quadBuffer, 0, i, i2, i3);
    }

    private final Name parseFieldName(int i, int i2, int i3, int i4) throws IOException, JsonParseException {
        this._quadBuffer[0] = i;
        return parseEscapedFieldName(this._quadBuffer, 1, i2, i3, i4);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x009c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final org.codehaus.jackson.sym.Name parseEscapedFieldName(int[] r10, int r11, int r12, int r13, int r14) throws java.io.IOException, org.codehaus.jackson.JsonParseException {
        /*
            r9 = this;
            r7 = 4
            r1 = 0
            int[] r5 = org.codehaus.jackson.impl.Utf8StreamParser.sInputCodesLatin1
        L_0x0004:
            r0 = r5[r13]
            if (r0 == 0) goto L_0x00d6
            r0 = 34
            if (r13 == r0) goto L_0x00ae
            r0 = 92
            if (r13 == r0) goto L_0x006b
            java.lang.String r0 = "name"
            r9._throwUnquotedSpace(r13, r0)
        L_0x0015:
            r0 = 127(0x7f, float:1.78E-43)
            if (r13 <= r0) goto L_0x00d6
            if (r14 < r7) goto L_0x00d2
            int r0 = r10.length
            if (r11 < r0) goto L_0x0025
            int r0 = r10.length
            int[] r10 = growArrayBy(r10, r0)
            r9._quadBuffer = r10
        L_0x0025:
            int r4 = r11 + 1
            r10[r11] = r12
            r14 = r1
            r12 = r1
            r0 = r10
        L_0x002c:
            r2 = 2048(0x800, float:2.87E-42)
            if (r13 >= r2) goto L_0x0070
            int r2 = r12 << 8
            int r3 = r13 >> 6
            r3 = r3 | 192(0xc0, float:2.69E-43)
            r3 = r3 | r2
            int r2 = r14 + 1
            r8 = r2
            r2 = r3
            r3 = r0
            r0 = r8
        L_0x003d:
            r6 = r13 & 63
            r12 = r6 | 128(0x80, float:1.794E-43)
            r14 = r0
            r11 = r4
            r0 = r3
            r3 = r2
        L_0x0045:
            if (r14 >= r7) goto L_0x009c
            int r14 = r14 + 1
            int r2 = r3 << 8
            r12 = r12 | r2
            r10 = r0
        L_0x004d:
            int r0 = r9._inputPtr
            int r2 = r9._inputEnd
            if (r0 < r2) goto L_0x005e
            boolean r0 = r9.loadMore()
            if (r0 != 0) goto L_0x005e
            java.lang.String r0 = " in field name"
            r9._reportInvalidEOF(r0)
        L_0x005e:
            byte[] r0 = r9._inputBuffer
            int r2 = r9._inputPtr
            int r3 = r2 + 1
            r9._inputPtr = r3
            byte r0 = r0[r2]
            r13 = r0 & 255(0xff, float:3.57E-43)
            goto L_0x0004
        L_0x006b:
            char r13 = r9._decodeEscaped()
            goto L_0x0015
        L_0x0070:
            int r2 = r12 << 8
            int r3 = r13 >> 12
            r3 = r3 | 224(0xe0, float:3.14E-43)
            r3 = r3 | r2
            int r2 = r14 + 1
            if (r2 < r7) goto L_0x00cc
            int r2 = r0.length
            if (r4 < r2) goto L_0x0085
            int r2 = r0.length
            int[] r0 = growArrayBy(r0, r2)
            r9._quadBuffer = r0
        L_0x0085:
            int r2 = r4 + 1
            r0[r4] = r3
            r3 = r2
            r4 = r0
            r0 = r1
            r2 = r1
        L_0x008d:
            int r2 = r2 << 8
            int r6 = r13 >> 6
            r6 = r6 & 63
            r6 = r6 | 128(0x80, float:1.794E-43)
            r2 = r2 | r6
            int r0 = r0 + 1
            r8 = r3
            r3 = r4
            r4 = r8
            goto L_0x003d
        L_0x009c:
            int r2 = r0.length
            if (r11 < r2) goto L_0x00a6
            int r2 = r0.length
            int[] r0 = growArrayBy(r0, r2)
            r9._quadBuffer = r0
        L_0x00a6:
            int r2 = r11 + 1
            r0[r11] = r3
            r14 = 1
            r11 = r2
            r10 = r0
            goto L_0x004d
        L_0x00ae:
            if (r14 <= 0) goto L_0x00bf
            int r0 = r10.length
            if (r11 < r0) goto L_0x00ba
            int r0 = r10.length
            int[] r10 = growArrayBy(r10, r0)
            r9._quadBuffer = r10
        L_0x00ba:
            int r0 = r11 + 1
            r10[r11] = r12
            r11 = r0
        L_0x00bf:
            org.codehaus.jackson.sym.BytesToNameCanonicalizer r0 = r9._symbols
            org.codehaus.jackson.sym.Name r0 = r0.findName(r10, r11)
            if (r0 != 0) goto L_0x00cb
            org.codehaus.jackson.sym.Name r0 = r9.addName(r10, r11, r14)
        L_0x00cb:
            return r0
        L_0x00cc:
            r8 = r2
            r2 = r3
            r3 = r4
            r4 = r0
            r0 = r8
            goto L_0x008d
        L_0x00d2:
            r4 = r11
            r0 = r10
            goto L_0x002c
        L_0x00d6:
            r3 = r12
            r0 = r10
            r12 = r13
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.impl.Utf8StreamParser.parseEscapedFieldName(int[], int, int, int, int):org.codehaus.jackson.sym.Name");
    }

    /* access modifiers changed from: protected */
    public final Name _handleUnusualFieldName(int i) throws IOException, JsonParseException {
        int[] iArr;
        int i2;
        int i3;
        int i4;
        if (i == 39 && isEnabled(JsonParser.Feature.ALLOW_SINGLE_QUOTES)) {
            return _parseApostropheFieldName();
        }
        if (!isEnabled(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES)) {
            _reportUnexpectedChar(i, "was expecting double-quote to start field name");
        }
        int[] inputCodeUtf8JsNames = CharTypes.getInputCodeUtf8JsNames();
        if (inputCodeUtf8JsNames[i] != 0) {
            _reportUnexpectedChar(i, "was expecting either valid name character (for unquoted name) or double-quote (for quoted) to start field name");
        }
        int i5 = 0;
        int i6 = 0;
        byte b = i;
        int i7 = 0;
        int[] iArr2 = this._quadBuffer;
        while (true) {
            if (i5 < 4) {
                int i8 = i5 + 1;
                i3 = b | (i6 << 8);
                i4 = i7;
                iArr = iArr2;
                i2 = i8;
            } else {
                if (i7 >= iArr2.length) {
                    iArr2 = growArrayBy(iArr2, iArr2.length);
                    this._quadBuffer = iArr2;
                }
                int i9 = i7 + 1;
                iArr2[i7] = i6;
                iArr = iArr2;
                i2 = 1;
                i3 = b;
                i4 = i9;
            }
            if (this._inputPtr >= this._inputEnd && !loadMore()) {
                _reportInvalidEOF(" in field name");
            }
            byte b2 = this._inputBuffer[this._inputPtr] & 255;
            if (inputCodeUtf8JsNames[b2] != 0) {
                break;
            }
            this._inputPtr++;
            i6 = i3;
            i5 = i2;
            iArr2 = iArr;
            i7 = i4;
            b = b2;
        }
        if (i2 > 0) {
            if (i4 >= iArr.length) {
                iArr = growArrayBy(iArr, iArr.length);
                this._quadBuffer = iArr;
            }
            iArr[i4] = i3;
            i4++;
        }
        Name findName = this._symbols.findName(iArr, i4);
        return findName == null ? addName(iArr, i4, i2) : findName;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00d2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final org.codehaus.jackson.sym.Name _parseApostropheFieldName() throws java.io.IOException, org.codehaus.jackson.JsonParseException {
        /*
            r12 = this;
            r10 = 39
            r9 = 4
            r1 = 0
            int r0 = r12._inputPtr
            int r2 = r12._inputEnd
            if (r0 < r2) goto L_0x0015
            boolean r0 = r12.loadMore()
            if (r0 != 0) goto L_0x0015
            java.lang.String r0 = ": was expecting closing ''' for name"
            r12._reportInvalidEOF(r0)
        L_0x0015:
            byte[] r0 = r12._inputBuffer
            int r2 = r12._inputPtr
            int r3 = r2 + 1
            r12._inputPtr = r3
            byte r0 = r0[r2]
            r5 = r0 & 255(0xff, float:3.57E-43)
            if (r5 != r10) goto L_0x0028
            org.codehaus.jackson.sym.Name r0 = org.codehaus.jackson.sym.BytesToNameCanonicalizer.getEmptyName()
        L_0x0027:
            return r0
        L_0x0028:
            int[] r0 = r12._quadBuffer
            int[] r7 = org.codehaus.jackson.impl.Utf8StreamParser.sInputCodesLatin1
            r3 = r1
            r4 = r1
            r2 = r1
        L_0x002f:
            if (r5 == r10) goto L_0x00e6
            r6 = 34
            if (r5 == r6) goto L_0x0119
            r6 = r7[r5]
            if (r6 == 0) goto L_0x0119
            r6 = 92
            if (r5 == r6) goto L_0x00a1
            java.lang.String r6 = "name"
            r12._throwUnquotedSpace(r5, r6)
        L_0x0042:
            r6 = 127(0x7f, float:1.78E-43)
            if (r5 <= r6) goto L_0x0119
            if (r3 < r9) goto L_0x0113
            int r3 = r0.length
            if (r2 < r3) goto L_0x0052
            int r3 = r0.length
            int[] r0 = growArrayBy(r0, r3)
            r12._quadBuffer = r0
        L_0x0052:
            int r3 = r2 + 1
            r0[r2] = r4
            r2 = r1
            r4 = r3
            r3 = r1
        L_0x0059:
            r6 = 2048(0x800, float:2.87E-42)
            if (r5 >= r6) goto L_0x00a6
            int r3 = r3 << 8
            int r6 = r5 >> 6
            r6 = r6 | 192(0xc0, float:2.69E-43)
            r3 = r3 | r6
            int r2 = r2 + 1
            r11 = r2
            r2 = r3
            r3 = r0
            r0 = r11
        L_0x006a:
            r5 = r5 & 63
            r5 = r5 | 128(0x80, float:1.794E-43)
            r6 = r2
            r2 = r0
            r0 = r3
            r3 = r5
        L_0x0072:
            if (r2 >= r9) goto L_0x00d2
            int r2 = r2 + 1
            int r5 = r6 << 8
            r3 = r3 | r5
            r11 = r2
            r2 = r3
            r3 = r4
            r4 = r0
            r0 = r11
        L_0x007e:
            int r5 = r12._inputPtr
            int r6 = r12._inputEnd
            if (r5 < r6) goto L_0x008f
            boolean r5 = r12.loadMore()
            if (r5 != 0) goto L_0x008f
            java.lang.String r5 = " in field name"
            r12._reportInvalidEOF(r5)
        L_0x008f:
            byte[] r5 = r12._inputBuffer
            int r6 = r12._inputPtr
            int r8 = r6 + 1
            r12._inputPtr = r8
            byte r5 = r5[r6]
            r5 = r5 & 255(0xff, float:3.57E-43)
            r11 = r0
            r0 = r4
            r4 = r2
            r2 = r3
            r3 = r11
            goto L_0x002f
        L_0x00a1:
            char r5 = r12._decodeEscaped()
            goto L_0x0042
        L_0x00a6:
            int r3 = r3 << 8
            int r6 = r5 >> 12
            r6 = r6 | 224(0xe0, float:3.14E-43)
            r3 = r3 | r6
            int r2 = r2 + 1
            if (r2 < r9) goto L_0x010d
            int r2 = r0.length
            if (r4 < r2) goto L_0x00bb
            int r2 = r0.length
            int[] r0 = growArrayBy(r0, r2)
            r12._quadBuffer = r0
        L_0x00bb:
            int r2 = r4 + 1
            r0[r4] = r3
            r3 = r2
            r4 = r0
            r0 = r1
            r2 = r1
        L_0x00c3:
            int r2 = r2 << 8
            int r6 = r5 >> 6
            r6 = r6 & 63
            r6 = r6 | 128(0x80, float:1.794E-43)
            r2 = r2 | r6
            int r0 = r0 + 1
            r11 = r3
            r3 = r4
            r4 = r11
            goto L_0x006a
        L_0x00d2:
            int r2 = r0.length
            if (r4 < r2) goto L_0x00dc
            int r2 = r0.length
            int[] r0 = growArrayBy(r0, r2)
            r12._quadBuffer = r0
        L_0x00dc:
            int r5 = r4 + 1
            r0[r4] = r6
            r2 = 1
            r4 = r0
            r0 = r2
            r2 = r3
            r3 = r5
            goto L_0x007e
        L_0x00e6:
            if (r3 <= 0) goto L_0x010a
            int r1 = r0.length
            if (r2 < r1) goto L_0x00f2
            int r1 = r0.length
            int[] r0 = growArrayBy(r0, r1)
            r12._quadBuffer = r0
        L_0x00f2:
            int r1 = r2 + 1
            r0[r2] = r4
            r11 = r1
            r1 = r0
            r0 = r11
        L_0x00f9:
            org.codehaus.jackson.sym.BytesToNameCanonicalizer r2 = r12._symbols
            org.codehaus.jackson.sym.Name r2 = r2.findName(r1, r0)
            if (r2 != 0) goto L_0x0107
            org.codehaus.jackson.sym.Name r0 = r12.addName(r1, r0, r3)
            goto L_0x0027
        L_0x0107:
            r0 = r2
            goto L_0x0027
        L_0x010a:
            r1 = r0
            r0 = r2
            goto L_0x00f9
        L_0x010d:
            r11 = r2
            r2 = r3
            r3 = r4
            r4 = r0
            r0 = r11
            goto L_0x00c3
        L_0x0113:
            r11 = r3
            r3 = r4
            r4 = r2
            r2 = r11
            goto L_0x0059
        L_0x0119:
            r6 = r4
            r4 = r2
            r2 = r3
            r3 = r5
            goto L_0x0072
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.impl.Utf8StreamParser._parseApostropheFieldName():org.codehaus.jackson.sym.Name");
    }

    private final Name findName(int i, int i2) throws JsonParseException {
        Name findName = this._symbols.findName(i);
        if (findName != null) {
            return findName;
        }
        this._quadBuffer[0] = i;
        return addName(this._quadBuffer, 1, i2);
    }

    private final Name findName(int i, int i2, int i3) throws JsonParseException {
        Name findName = this._symbols.findName(i, i2);
        if (findName != null) {
            return findName;
        }
        this._quadBuffer[0] = i;
        this._quadBuffer[1] = i2;
        return addName(this._quadBuffer, 2, i3);
    }

    private final Name findName(int[] iArr, int i, int i2, int i3) throws JsonParseException {
        if (i >= iArr.length) {
            iArr = growArrayBy(iArr, iArr.length);
            this._quadBuffer = iArr;
        }
        int i4 = i + 1;
        iArr[i] = i2;
        Name findName = this._symbols.findName(iArr, i4);
        if (findName == null) {
            return addName(iArr, i4, i3);
        }
        return findName;
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00d1 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final org.codehaus.jackson.sym.Name addName(int[] r12, int r13, int r14) throws org.codehaus.jackson.JsonParseException {
        /*
            r11 = this;
            int r0 = r13 << 2
            int r0 = r0 + -4
            int r6 = r0 + r14
            r0 = 4
            if (r14 >= r0) goto L_0x00da
            int r0 = r13 + -1
            r0 = r12[r0]
            int r1 = r13 + -1
            int r2 = 4 - r14
            int r2 = r2 << 3
            int r2 = r0 << r2
            r12[r1] = r2
        L_0x0017:
            org.codehaus.jackson.util.TextBuffer r1 = r11._textBuffer
            char[] r1 = r1.emptyAndGetCurrentSegment()
            r5 = 0
            r2 = 0
            r3 = r2
        L_0x0020:
            if (r3 >= r6) goto L_0x0100
            int r2 = r3 >> 2
            r2 = r12[r2]
            r4 = r3 & 3
            int r4 = 3 - r4
            int r4 = r4 << 3
            int r2 = r2 >> r4
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r3 = r3 + 1
            r4 = 127(0x7f, float:1.78E-43)
            if (r2 <= r4) goto L_0x0114
            r4 = r2 & 224(0xe0, float:3.14E-43)
            r7 = 192(0xc0, float:2.69E-43)
            if (r4 != r7) goto L_0x00dd
            r4 = r2 & 31
            r2 = 1
            r10 = r2
            r2 = r4
            r4 = r10
        L_0x0041:
            int r7 = r3 + r4
            if (r7 <= r6) goto L_0x004a
            java.lang.String r7 = " in field name"
            r11._reportInvalidEOF(r7)
        L_0x004a:
            int r7 = r3 >> 2
            r7 = r12[r7]
            r8 = r3 & 3
            int r8 = 3 - r8
            int r8 = r8 << 3
            int r7 = r7 >> r8
            int r3 = r3 + 1
            r8 = r7 & 192(0xc0, float:2.69E-43)
            r9 = 128(0x80, float:1.794E-43)
            if (r8 == r9) goto L_0x0060
            r11._reportInvalidOther(r7)
        L_0x0060:
            int r2 = r2 << 6
            r7 = r7 & 63
            r2 = r2 | r7
            r7 = 1
            if (r4 <= r7) goto L_0x00a3
            int r7 = r3 >> 2
            r7 = r12[r7]
            r8 = r3 & 3
            int r8 = 3 - r8
            int r8 = r8 << 3
            int r7 = r7 >> r8
            int r3 = r3 + 1
            r8 = r7 & 192(0xc0, float:2.69E-43)
            r9 = 128(0x80, float:1.794E-43)
            if (r8 == r9) goto L_0x007e
            r11._reportInvalidOther(r7)
        L_0x007e:
            int r2 = r2 << 6
            r7 = r7 & 63
            r2 = r2 | r7
            r7 = 2
            if (r4 <= r7) goto L_0x00a3
            int r7 = r3 >> 2
            r7 = r12[r7]
            r8 = r3 & 3
            int r8 = 3 - r8
            int r8 = r8 << 3
            int r7 = r7 >> r8
            int r3 = r3 + 1
            r8 = r7 & 192(0xc0, float:2.69E-43)
            r9 = 128(0x80, float:1.794E-43)
            if (r8 == r9) goto L_0x009e
            r8 = r7 & 255(0xff, float:3.57E-43)
            r11._reportInvalidOther(r8)
        L_0x009e:
            int r2 = r2 << 6
            r7 = r7 & 63
            r2 = r2 | r7
        L_0x00a3:
            r7 = 2
            if (r4 <= r7) goto L_0x0114
            r4 = 65536(0x10000, float:9.18355E-41)
            int r2 = r2 - r4
            int r4 = r1.length
            if (r5 < r4) goto L_0x00b2
            org.codehaus.jackson.util.TextBuffer r1 = r11._textBuffer
            char[] r1 = r1.expandCurrentSegment()
        L_0x00b2:
            int r4 = r5 + 1
            r7 = 55296(0xd800, float:7.7486E-41)
            int r8 = r2 >> 10
            int r7 = r7 + r8
            char r7 = (char) r7
            r1[r5] = r7
            r5 = 56320(0xdc00, float:7.8921E-41)
            r2 = r2 & 1023(0x3ff, float:1.434E-42)
            r2 = r2 | r5
            r10 = r2
            r2 = r3
            r3 = r4
            r4 = r1
            r1 = r10
        L_0x00c8:
            int r5 = r4.length
            if (r3 < r5) goto L_0x00d1
            org.codehaus.jackson.util.TextBuffer r4 = r11._textBuffer
            char[] r4 = r4.expandCurrentSegment()
        L_0x00d1:
            int r5 = r3 + 1
            char r1 = (char) r1
            r4[r3] = r1
            r3 = r2
            r1 = r4
            goto L_0x0020
        L_0x00da:
            r0 = 0
            goto L_0x0017
        L_0x00dd:
            r4 = r2 & 240(0xf0, float:3.36E-43)
            r7 = 224(0xe0, float:3.14E-43)
            if (r4 != r7) goto L_0x00eb
            r4 = r2 & 15
            r2 = 2
            r10 = r2
            r2 = r4
            r4 = r10
            goto L_0x0041
        L_0x00eb:
            r4 = r2 & 248(0xf8, float:3.48E-43)
            r7 = 240(0xf0, float:3.36E-43)
            if (r4 != r7) goto L_0x00f9
            r4 = r2 & 7
            r2 = 3
            r10 = r2
            r2 = r4
            r4 = r10
            goto L_0x0041
        L_0x00f9:
            r11._reportInvalidInitial(r2)
            r2 = 1
            r4 = r2
            goto L_0x0041
        L_0x0100:
            java.lang.String r2 = new java.lang.String
            r3 = 0
            r2.<init>(r1, r3, r5)
            r1 = 4
            if (r14 >= r1) goto L_0x010d
            int r1 = r13 + -1
            r12[r1] = r0
        L_0x010d:
            org.codehaus.jackson.sym.BytesToNameCanonicalizer r0 = r11._symbols
            org.codehaus.jackson.sym.Name r0 = r0.addName(r2, r12, r13)
            return r0
        L_0x0114:
            r4 = r1
            r1 = r2
            r2 = r3
            r3 = r5
            goto L_0x00c8
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.impl.Utf8StreamParser.addName(int[], int, int):org.codehaus.jackson.sym.Name");
    }

    /* access modifiers changed from: protected */
    public final void _finishString() throws IOException, JsonParseException {
        int i = this._inputPtr;
        if (i >= this._inputEnd) {
            loadMoreGuaranteed();
            i = this._inputPtr;
        }
        char[] emptyAndGetCurrentSegment = this._textBuffer.emptyAndGetCurrentSegment();
        int[] iArr = sInputCodesUtf8;
        int min = Math.min(this._inputEnd, emptyAndGetCurrentSegment.length + i);
        byte[] bArr = this._inputBuffer;
        int i2 = i;
        int i3 = 0;
        while (true) {
            if (i2 >= min) {
                break;
            }
            byte b = bArr[i2] & 255;
            if (iArr[b] == 0) {
                emptyAndGetCurrentSegment[i3] = (char) b;
                i3++;
                i2++;
            } else if (b == 34) {
                this._inputPtr = i2 + 1;
                this._textBuffer.setCurrentLength(i3);
                return;
            }
        }
        this._inputPtr = i2;
        _finishString2(emptyAndGetCurrentSegment, i3);
    }

    private final void _finishString2(char[] cArr, int i) throws IOException, JsonParseException {
        int i2;
        int[] iArr = sInputCodesUtf8;
        byte[] bArr = this._inputBuffer;
        while (true) {
            int i3 = this._inputPtr;
            if (i3 >= this._inputEnd) {
                loadMoreGuaranteed();
                i3 = this._inputPtr;
            }
            if (i >= cArr.length) {
                cArr = this._textBuffer.finishCurrentSegment();
                i = 0;
            }
            int min = Math.min(this._inputEnd, (cArr.length - i) + i3);
            while (true) {
                if (i3 < min) {
                    int i4 = i3 + 1;
                    int i5 = bArr[i3] & 255;
                    if (iArr[i5] != 0) {
                        this._inputPtr = i4;
                        if (i5 != 34) {
                            switch (iArr[i5]) {
                                case 1:
                                    i5 = _decodeEscaped();
                                    break;
                                case 2:
                                    i5 = _decodeUtf8_2(i5);
                                    break;
                                case 3:
                                    if (this._inputEnd - this._inputPtr < 2) {
                                        i5 = _decodeUtf8_3(i5);
                                        break;
                                    } else {
                                        i5 = _decodeUtf8_3fast(i5);
                                        break;
                                    }
                                case 4:
                                    int _decodeUtf8_4 = _decodeUtf8_4(i5);
                                    int i6 = i + 1;
                                    cArr[i] = (char) (55296 | (_decodeUtf8_4 >> 10));
                                    if (i6 >= cArr.length) {
                                        cArr = this._textBuffer.finishCurrentSegment();
                                        i6 = 0;
                                    }
                                    i = i6;
                                    i5 = (_decodeUtf8_4 & 1023) | 56320;
                                    break;
                                default:
                                    if (i5 >= 32) {
                                        _reportInvalidChar(i5);
                                        break;
                                    } else {
                                        _throwUnquotedSpace(i5, "string value");
                                        break;
                                    }
                            }
                            if (i >= cArr.length) {
                                cArr = this._textBuffer.finishCurrentSegment();
                                i2 = 0;
                            } else {
                                i2 = i;
                            }
                            i = i2 + 1;
                            cArr[i2] = (char) i5;
                        } else {
                            this._textBuffer.setCurrentLength(i);
                            return;
                        }
                    } else {
                        cArr[i] = (char) i5;
                        i3 = i4;
                        i++;
                    }
                } else {
                    this._inputPtr = i3;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void _skipString() throws IOException, JsonParseException {
        this._tokenIncomplete = false;
        int[] iArr = sInputCodesUtf8;
        byte[] bArr = this._inputBuffer;
        while (true) {
            int i = this._inputPtr;
            int i2 = this._inputEnd;
            if (i >= i2) {
                loadMoreGuaranteed();
                i = this._inputPtr;
                i2 = this._inputEnd;
            }
            while (true) {
                if (i < i2) {
                    int i3 = i + 1;
                    byte b = bArr[i] & 255;
                    if (iArr[b] != 0) {
                        this._inputPtr = i3;
                        if (b != 34) {
                            switch (iArr[b]) {
                                case 1:
                                    _decodeEscaped();
                                    continue;
                                case 2:
                                    _skipUtf8_2(b);
                                    continue;
                                case 3:
                                    _skipUtf8_3(b);
                                    continue;
                                case 4:
                                    _skipUtf8_4(b);
                                    continue;
                                default:
                                    if (b >= 32) {
                                        _reportInvalidChar(b);
                                        break;
                                    } else {
                                        _throwUnquotedSpace(b, "string value");
                                        continue;
                                    }
                            }
                        } else {
                            return;
                        }
                    } else {
                        i = i3;
                    }
                } else {
                    this._inputPtr = i;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final JsonToken _handleUnexpectedValue(int i) throws IOException, JsonParseException {
        int i2;
        int i3;
        if (i != 39 || !isEnabled(JsonParser.Feature.ALLOW_SINGLE_QUOTES)) {
            _reportUnexpectedChar(i, "expected a valid value (number, String, array, object, 'true', 'false' or 'null')");
        }
        char[] emptyAndGetCurrentSegment = this._textBuffer.emptyAndGetCurrentSegment();
        int[] iArr = sInputCodesUtf8;
        byte[] bArr = this._inputBuffer;
        int i4 = 0;
        while (true) {
            if (this._inputPtr >= this._inputEnd) {
                loadMoreGuaranteed();
            }
            if (i4 >= emptyAndGetCurrentSegment.length) {
                emptyAndGetCurrentSegment = this._textBuffer.finishCurrentSegment();
                i4 = 0;
            }
            int i5 = this._inputEnd;
            int length = this._inputPtr + (emptyAndGetCurrentSegment.length - i4);
            if (length >= i5) {
                length = i5;
            }
            while (true) {
                if (this._inputPtr < length) {
                    int i6 = this._inputPtr;
                    this._inputPtr = i6 + 1;
                    byte b = bArr[i6] & 255;
                    if (b != 39 && iArr[b] == 0) {
                        emptyAndGetCurrentSegment[i4] = (char) b;
                        i4++;
                    } else if (b != 39) {
                        switch (iArr[b]) {
                            case 1:
                                if (b != 34) {
                                    i2 = _decodeEscaped();
                                    break;
                                }
                                i2 = b;
                                break;
                            case 2:
                                i2 = _decodeUtf8_2(b);
                                break;
                            case 3:
                                if (this._inputEnd - this._inputPtr < 2) {
                                    i2 = _decodeUtf8_3(b);
                                    break;
                                } else {
                                    i2 = _decodeUtf8_3fast(b);
                                    break;
                                }
                            case 4:
                                int _decodeUtf8_4 = _decodeUtf8_4(b);
                                int i7 = i4 + 1;
                                emptyAndGetCurrentSegment[i4] = (char) (55296 | (_decodeUtf8_4 >> 10));
                                if (i7 >= emptyAndGetCurrentSegment.length) {
                                    emptyAndGetCurrentSegment = this._textBuffer.finishCurrentSegment();
                                    i4 = 0;
                                } else {
                                    i4 = i7;
                                }
                                i2 = 56320 | (_decodeUtf8_4 & 1023);
                                break;
                            default:
                                if (b < 32) {
                                    _throwUnquotedSpace(b, "string value");
                                }
                                _reportInvalidChar(b);
                                i2 = b;
                                break;
                        }
                        if (i4 >= emptyAndGetCurrentSegment.length) {
                            emptyAndGetCurrentSegment = this._textBuffer.finishCurrentSegment();
                            i3 = 0;
                        } else {
                            i3 = i4;
                        }
                        i4 = i3 + 1;
                        emptyAndGetCurrentSegment[i3] = (char) i2;
                    } else {
                        this._textBuffer.setCurrentLength(i4);
                        return JsonToken.VALUE_STRING;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void _matchToken(JsonToken jsonToken) throws IOException, JsonParseException {
        byte[] asByteArray = jsonToken.asByteArray();
        int length = asByteArray.length;
        for (int i = 1; i < length; i++) {
            if (this._inputPtr >= this._inputEnd) {
                loadMoreGuaranteed();
            }
            if (asByteArray[i] != this._inputBuffer[this._inputPtr]) {
                _reportInvalidToken(jsonToken.asString().substring(0, i));
            }
            this._inputPtr++;
        }
    }

    private void _reportInvalidToken(String str) throws IOException, JsonParseException {
        StringBuilder sb = new StringBuilder(str);
        while (true) {
            if (this._inputPtr >= this._inputEnd && !loadMore()) {
                break;
            }
            byte[] bArr = this._inputBuffer;
            int i = this._inputPtr;
            this._inputPtr = i + 1;
            char _decodeCharForError = (char) _decodeCharForError(bArr[i]);
            if (!Character.isJavaIdentifierPart(_decodeCharForError)) {
                break;
            }
            this._inputPtr++;
            sb.append(_decodeCharForError);
        }
        _reportError("Unrecognized token '" + sb.toString() + "': was expecting 'null', 'true' or 'false'");
    }

    private final int _skipWS() throws IOException, JsonParseException {
        while (true) {
            if (this._inputPtr < this._inputEnd || loadMore()) {
                byte[] bArr = this._inputBuffer;
                int i = this._inputPtr;
                this._inputPtr = i + 1;
                byte b = bArr[i] & 255;
                if (b > 32) {
                    if (b != 47) {
                        return b;
                    }
                    _skipComment();
                } else if (b != 32) {
                    if (b == 10) {
                        _skipLF();
                    } else if (b == 13) {
                        _skipCR();
                    } else if (b != 9) {
                        _throwInvalidSpace(b);
                    }
                }
            } else {
                throw _constructError("Unexpected end-of-input within/between " + this._parsingContext.getTypeDesc() + " entries");
            }
        }
    }

    private final int _skipWSOrEnd() throws IOException, JsonParseException {
        while (true) {
            if (this._inputPtr < this._inputEnd || loadMore()) {
                byte[] bArr = this._inputBuffer;
                int i = this._inputPtr;
                this._inputPtr = i + 1;
                byte b = bArr[i] & 255;
                if (b > 32) {
                    if (b != 47) {
                        return b;
                    }
                    _skipComment();
                } else if (b != 32) {
                    if (b == 10) {
                        _skipLF();
                    } else if (b == 13) {
                        _skipCR();
                    } else if (b != 9) {
                        _throwInvalidSpace(b);
                    }
                }
            } else {
                _handleEOF();
                return -1;
            }
        }
    }

    private final void _skipComment() throws IOException, JsonParseException {
        if (!isEnabled(JsonParser.Feature.ALLOW_COMMENTS)) {
            _reportUnexpectedChar(47, "maybe a (non-standard) comment? (not recognized as one since Feature 'ALLOW_COMMENTS' not enabled for parser)");
        }
        if (this._inputPtr >= this._inputEnd && !loadMore()) {
            _reportInvalidEOF(" in a comment");
        }
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        byte b = bArr[i] & 255;
        if (b == 47) {
            _skipCppComment();
        } else if (b == 42) {
            _skipCComment();
        } else {
            _reportUnexpectedChar(b, "was expecting either '*' or '/' for a comment");
        }
    }

    private final void _skipCComment() throws IOException, JsonParseException {
        int[] inputCodeComment = CharTypes.getInputCodeComment();
        while (true) {
            if (this._inputPtr < this._inputEnd || loadMore()) {
                byte[] bArr = this._inputBuffer;
                int i = this._inputPtr;
                this._inputPtr = i + 1;
                byte b = bArr[i] & 255;
                int i2 = inputCodeComment[b];
                if (i2 != 0) {
                    switch (i2) {
                        case 10:
                            _skipLF();
                            continue;
                        case 13:
                            _skipCR();
                            continue;
                        case 42:
                            if (this._inputBuffer[this._inputPtr] == 47) {
                                this._inputPtr++;
                                return;
                            }
                            continue;
                        default:
                            _reportInvalidChar(b);
                            continue;
                    }
                }
            } else {
                _reportInvalidEOF(" in a comment");
                return;
            }
        }
    }

    private final void _skipCppComment() throws IOException, JsonParseException {
        int[] inputCodeComment = CharTypes.getInputCodeComment();
        while (true) {
            if (this._inputPtr < this._inputEnd || loadMore()) {
                byte[] bArr = this._inputBuffer;
                int i = this._inputPtr;
                this._inputPtr = i + 1;
                byte b = bArr[i] & 255;
                int i2 = inputCodeComment[b];
                if (i2 != 0) {
                    switch (i2) {
                        case 10:
                            _skipLF();
                            return;
                        case 13:
                            _skipCR();
                            return;
                        case 42:
                            break;
                        default:
                            _reportInvalidChar(b);
                            continue;
                    }
                }
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final char _decodeEscaped() throws IOException, JsonParseException {
        if (this._inputPtr >= this._inputEnd && !loadMore()) {
            _reportInvalidEOF(" in character escape sequence");
        }
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        byte b = bArr[i];
        switch (b) {
            case 34:
            case 47:
            case 92:
                return (char) b;
            case 98:
                return 8;
            case 102:
                return 12;
            case 110:
                return 10;
            case 114:
                return 13;
            case 116:
                return 9;
            case 117:
                int i2 = 0;
                for (int i3 = 0; i3 < 4; i3++) {
                    if (this._inputPtr >= this._inputEnd && !loadMore()) {
                        _reportInvalidEOF(" in character escape sequence");
                    }
                    byte[] bArr2 = this._inputBuffer;
                    int i4 = this._inputPtr;
                    this._inputPtr = i4 + 1;
                    byte b2 = bArr2[i4];
                    int charToHex = CharTypes.charToHex(b2);
                    if (charToHex < 0) {
                        _reportUnexpectedChar(b2, "expected a hex-digit for character escape sequence");
                    }
                    i2 = (i2 << 4) | charToHex;
                }
                return (char) i2;
            default:
                return _handleUnrecognizedCharacterEscape((char) _decodeCharForError(b));
        }
    }

    /* access modifiers changed from: protected */
    public final int _decodeCharForError(int i) throws IOException, JsonParseException {
        char c;
        if (i >= 0) {
            return i;
        }
        if ((i & 224) == 192) {
            i &= 31;
            c = 1;
        } else if ((i & 240) == 224) {
            i &= 15;
            c = 2;
        } else if ((i & 248) == 240) {
            i &= 7;
            c = 3;
        } else {
            _reportInvalidInitial(i & 255);
            c = 1;
        }
        int nextByte = nextByte();
        if ((nextByte & 192) != 128) {
            _reportInvalidOther(nextByte & 255);
        }
        int i2 = (i << 6) | (nextByte & 63);
        if (c <= 1) {
            return i2;
        }
        int nextByte2 = nextByte();
        if ((nextByte2 & 192) != 128) {
            _reportInvalidOther(nextByte2 & 255);
        }
        int i3 = (i2 << 6) | (nextByte2 & 63);
        if (c <= 2) {
            return i3;
        }
        int nextByte3 = nextByte();
        if ((nextByte3 & 192) != 128) {
            _reportInvalidOther(nextByte3 & 255);
        }
        return (i3 << 6) | (nextByte3 & 63);
    }

    private final int _decodeUtf8_2(int i) throws IOException, JsonParseException {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        byte b = bArr[i2];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & 255, this._inputPtr);
        }
        return (b & 63) | ((i & 31) << 6);
    }

    private final int _decodeUtf8_3(int i) throws IOException, JsonParseException {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        int i2 = i & 15;
        byte[] bArr = this._inputBuffer;
        int i3 = this._inputPtr;
        this._inputPtr = i3 + 1;
        byte b = bArr[i3];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & 255, this._inputPtr);
        }
        byte b2 = (i2 << 6) | (b & 63);
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr2 = this._inputBuffer;
        int i4 = this._inputPtr;
        this._inputPtr = i4 + 1;
        byte b3 = bArr2[i4];
        if ((b3 & 192) != 128) {
            _reportInvalidOther(b3 & 255, this._inputPtr);
        }
        return (b2 << 6) | (b3 & 63);
    }

    private final int _decodeUtf8_3fast(int i) throws IOException, JsonParseException {
        int i2 = i & 15;
        byte[] bArr = this._inputBuffer;
        int i3 = this._inputPtr;
        this._inputPtr = i3 + 1;
        byte b = bArr[i3];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & 255, this._inputPtr);
        }
        byte b2 = (i2 << 6) | (b & 63);
        byte[] bArr2 = this._inputBuffer;
        int i4 = this._inputPtr;
        this._inputPtr = i4 + 1;
        byte b3 = bArr2[i4];
        if ((b3 & 192) != 128) {
            _reportInvalidOther(b3 & 255, this._inputPtr);
        }
        return (b2 << 6) | (b3 & 63);
    }

    private final int _decodeUtf8_4(int i) throws IOException, JsonParseException {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        byte b = bArr[i2];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & 255, this._inputPtr);
        }
        byte b2 = (b & 63) | ((i & 7) << 6);
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr2 = this._inputBuffer;
        int i3 = this._inputPtr;
        this._inputPtr = i3 + 1;
        byte b3 = bArr2[i3];
        if ((b3 & 192) != 128) {
            _reportInvalidOther(b3 & 255, this._inputPtr);
        }
        byte b4 = (b2 << 6) | (b3 & 63);
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr3 = this._inputBuffer;
        int i4 = this._inputPtr;
        this._inputPtr = i4 + 1;
        byte b5 = bArr3[i4];
        if ((b5 & 192) != 128) {
            _reportInvalidOther(b5 & 255, this._inputPtr);
        }
        return ((b4 << 6) | (b5 & 63)) - BYTE_0;
    }

    private final void _skipUtf8_2(int i) throws IOException, JsonParseException {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        byte b = bArr[i2];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & 255, this._inputPtr);
        }
    }

    private final void _skipUtf8_3(int i) throws IOException, JsonParseException {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        byte b = bArr[i2];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & 255, this._inputPtr);
        }
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr2 = this._inputBuffer;
        int i3 = this._inputPtr;
        this._inputPtr = i3 + 1;
        byte b2 = bArr2[i3];
        if ((b2 & 192) != 128) {
            _reportInvalidOther(b2 & 255, this._inputPtr);
        }
    }

    private final void _skipUtf8_4(int i) throws IOException, JsonParseException {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i2 = this._inputPtr;
        this._inputPtr = i2 + 1;
        byte b = bArr[i2];
        if ((b & 192) != 128) {
            _reportInvalidOther(b & 255, this._inputPtr);
        }
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        if ((b & 192) != 128) {
            _reportInvalidOther(b & 255, this._inputPtr);
        }
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr2 = this._inputBuffer;
        int i3 = this._inputPtr;
        this._inputPtr = i3 + 1;
        byte b2 = bArr2[i3];
        if ((b2 & 192) != 128) {
            _reportInvalidOther(b2 & 255, this._inputPtr);
        }
    }

    /* access modifiers changed from: protected */
    public final void _skipCR() throws IOException {
        if ((this._inputPtr < this._inputEnd || loadMore()) && this._inputBuffer[this._inputPtr] == 10) {
            this._inputPtr++;
        }
        this._currInputRow++;
        this._currInputRowStart = this._inputPtr;
    }

    /* access modifiers changed from: protected */
    public final void _skipLF() throws IOException {
        this._currInputRow++;
        this._currInputRowStart = this._inputPtr;
    }

    private int nextByte() throws IOException, JsonParseException {
        if (this._inputPtr >= this._inputEnd) {
            loadMoreGuaranteed();
        }
        byte[] bArr = this._inputBuffer;
        int i = this._inputPtr;
        this._inputPtr = i + 1;
        return bArr[i] & 255;
    }

    /* access modifiers changed from: protected */
    public final void _reportInvalidChar(int i) throws JsonParseException {
        if (i < 32) {
            _throwInvalidSpace(i);
        }
        _reportInvalidInitial(i);
    }

    /* access modifiers changed from: protected */
    public final void _reportInvalidInitial(int i) throws JsonParseException {
        _reportError("Invalid UTF-8 start byte 0x" + Integer.toHexString(i));
    }

    /* access modifiers changed from: protected */
    public final void _reportInvalidOther(int i) throws JsonParseException {
        _reportError("Invalid UTF-8 middle byte 0x" + Integer.toHexString(i));
    }

    /* access modifiers changed from: protected */
    public final void _reportInvalidOther(int i, int i2) throws JsonParseException {
        this._inputPtr = i2;
        _reportInvalidOther(i);
    }

    public static int[] growArrayBy(int[] iArr, int i) {
        if (iArr == null) {
            return new int[i];
        }
        int length = iArr.length;
        int[] iArr2 = new int[(length + i)];
        System.arraycopy(iArr, 0, iArr2, 0, length);
        return iArr2;
    }

    /* access modifiers changed from: protected */
    public final byte[] _decodeBase64(Base64Variant base64Variant) throws IOException, JsonParseException {
        ByteArrayBuilder _getByteArrayBuilder = _getByteArrayBuilder();
        while (true) {
            if (this._inputPtr >= this._inputEnd) {
                loadMoreGuaranteed();
            }
            byte[] bArr = this._inputBuffer;
            int i = this._inputPtr;
            this._inputPtr = i + 1;
            byte b = bArr[i] & 255;
            if (b > 32) {
                int decodeBase64Char = base64Variant.decodeBase64Char(b);
                if (decodeBase64Char >= 0) {
                    if (this._inputPtr >= this._inputEnd) {
                        loadMoreGuaranteed();
                    }
                    byte[] bArr2 = this._inputBuffer;
                    int i2 = this._inputPtr;
                    this._inputPtr = i2 + 1;
                    byte b2 = bArr2[i2] & 255;
                    int decodeBase64Char2 = base64Variant.decodeBase64Char(b2);
                    if (decodeBase64Char2 < 0) {
                        throw reportInvalidChar(base64Variant, b2, 1);
                    }
                    int i3 = (decodeBase64Char << 6) | decodeBase64Char2;
                    if (this._inputPtr >= this._inputEnd) {
                        loadMoreGuaranteed();
                    }
                    byte[] bArr3 = this._inputBuffer;
                    int i4 = this._inputPtr;
                    this._inputPtr = i4 + 1;
                    byte b3 = bArr3[i4] & 255;
                    int decodeBase64Char3 = base64Variant.decodeBase64Char(b3);
                    if (decodeBase64Char3 >= 0) {
                        int i5 = (i3 << 6) | decodeBase64Char3;
                        if (this._inputPtr >= this._inputEnd) {
                            loadMoreGuaranteed();
                        }
                        byte[] bArr4 = this._inputBuffer;
                        int i6 = this._inputPtr;
                        this._inputPtr = i6 + 1;
                        byte b4 = bArr4[i6] & 255;
                        int decodeBase64Char4 = base64Variant.decodeBase64Char(b4);
                        if (decodeBase64Char4 >= 0) {
                            _getByteArrayBuilder.appendThreeBytes((i5 << 6) | decodeBase64Char4);
                        } else if (decodeBase64Char4 != -2) {
                            throw reportInvalidChar(base64Variant, b4, 3);
                        } else {
                            _getByteArrayBuilder.appendTwoBytes(i5 >> 2);
                        }
                    } else if (decodeBase64Char3 != -2) {
                        throw reportInvalidChar(base64Variant, b3, 2);
                    } else {
                        if (this._inputPtr >= this._inputEnd) {
                            loadMoreGuaranteed();
                        }
                        byte[] bArr5 = this._inputBuffer;
                        int i7 = this._inputPtr;
                        this._inputPtr = i7 + 1;
                        byte b5 = bArr5[i7] & 255;
                        if (!base64Variant.usesPaddingChar(b5)) {
                            throw reportInvalidChar(base64Variant, b5, 3, "expected padding character '" + base64Variant.getPaddingChar() + "'");
                        }
                        _getByteArrayBuilder.append(i3 >> 4);
                    }
                } else if (b == 34) {
                    return _getByteArrayBuilder.toByteArray();
                } else {
                    throw reportInvalidChar(base64Variant, b, 0);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final IllegalArgumentException reportInvalidChar(Base64Variant base64Variant, int i, int i2) throws IllegalArgumentException {
        return reportInvalidChar(base64Variant, i, i2, null);
    }

    /* access modifiers changed from: protected */
    public final IllegalArgumentException reportInvalidChar(Base64Variant base64Variant, int i, int i2, String str) throws IllegalArgumentException {
        String str2;
        if (i <= 32) {
            str2 = "Illegal white space character (code 0x" + Integer.toHexString(i) + ") as character #" + (i2 + 1) + " of 4-char base64 unit: can only used between units";
        } else if (base64Variant.usesPaddingChar(i)) {
            str2 = "Unexpected padding character ('" + base64Variant.getPaddingChar() + "') as character #" + (i2 + 1) + " of 4-char base64 unit: padding only legal as 3rd or 4th character";
        } else if (!Character.isDefined(i) || Character.isISOControl(i)) {
            str2 = "Illegal character (code 0x" + Integer.toHexString(i) + ") in base64 content";
        } else {
            str2 = "Illegal character '" + ((char) i) + "' (code 0x" + Integer.toHexString(i) + ") in base64 content";
        }
        if (str != null) {
            str2 = str2 + ": " + str;
        }
        return new IllegalArgumentException(str2);
    }
}
