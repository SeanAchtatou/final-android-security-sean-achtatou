package org.codehaus.jackson.impl;

import java.io.ByteArrayInputStream;
import java.io.CharConversionException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import org.codehaus.jackson.JsonEncoding;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.ObjectCodec;
import org.codehaus.jackson.io.IOContext;
import org.codehaus.jackson.io.MergedStream;
import org.codehaus.jackson.io.UTF32Reader;
import org.codehaus.jackson.sym.BytesToNameCanonicalizer;
import org.codehaus.jackson.sym.CharsToNameCanonicalizer;

public final class ByteSourceBootstrapper {
    boolean _bigEndian = true;
    private final boolean _bufferRecyclable;
    int _bytesPerChar = 0;
    final IOContext _context;
    final InputStream _in;
    final byte[] _inputBuffer;
    private int _inputEnd;
    protected int _inputProcessed;
    private int _inputPtr;

    public ByteSourceBootstrapper(IOContext iOContext, InputStream inputStream) {
        this._context = iOContext;
        this._in = inputStream;
        this._inputBuffer = iOContext.allocReadIOBuffer();
        this._inputPtr = 0;
        this._inputEnd = 0;
        this._inputProcessed = 0;
        this._bufferRecyclable = true;
    }

    public ByteSourceBootstrapper(IOContext iOContext, byte[] bArr, int i, int i2) {
        this._context = iOContext;
        this._in = null;
        this._inputBuffer = bArr;
        this._inputPtr = i;
        this._inputEnd = i + i2;
        this._inputProcessed = -i;
        this._bufferRecyclable = false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0053, code lost:
        if (checkUTF16(r2 >>> 16) != false) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0076, code lost:
        if (checkUTF16(((r7._inputBuffer[r7._inputPtr] & 255) << 8) | (r7._inputBuffer[r7._inputPtr + 1] & 255)) != false) goto L_0x003d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final org.codehaus.jackson.JsonEncoding detectEncoding() throws java.io.IOException, org.codehaus.jackson.JsonParseException {
        /*
            r7 = this;
            r6 = 4
            r5 = 2
            r0 = 1
            r1 = 0
            boolean r2 = r7.ensureLoaded(r6)
            if (r2 == 0) goto L_0x0057
            byte[] r2 = r7._inputBuffer
            int r3 = r7._inputPtr
            byte r2 = r2[r3]
            int r2 = r2 << 24
            byte[] r3 = r7._inputBuffer
            int r4 = r7._inputPtr
            int r4 = r4 + 1
            byte r3 = r3[r4]
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r3 = r3 << 16
            r2 = r2 | r3
            byte[] r3 = r7._inputBuffer
            int r4 = r7._inputPtr
            int r4 = r4 + 2
            byte r3 = r3[r4]
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r3 = r3 << 8
            r2 = r2 | r3
            byte[] r3 = r7._inputBuffer
            int r4 = r7._inputPtr
            int r4 = r4 + 3
            byte r3 = r3[r4]
            r3 = r3 & 255(0xff, float:3.57E-43)
            r2 = r2 | r3
            boolean r3 = r7.handleBOM(r2)
            if (r3 == 0) goto L_0x0047
        L_0x003d:
            if (r0 != 0) goto L_0x0079
            org.codehaus.jackson.JsonEncoding r0 = org.codehaus.jackson.JsonEncoding.UTF8
        L_0x0041:
            org.codehaus.jackson.io.IOContext r1 = r7._context
            r1.setEncoding(r0)
            return r0
        L_0x0047:
            boolean r3 = r7.checkUTF32(r2)
            if (r3 != 0) goto L_0x003d
            int r2 = r2 >>> 16
            boolean r2 = r7.checkUTF16(r2)
            if (r2 != 0) goto L_0x003d
        L_0x0055:
            r0 = r1
            goto L_0x003d
        L_0x0057:
            boolean r2 = r7.ensureLoaded(r5)
            if (r2 == 0) goto L_0x0055
            byte[] r2 = r7._inputBuffer
            int r3 = r7._inputPtr
            byte r2 = r2[r3]
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r2 = r2 << 8
            byte[] r3 = r7._inputBuffer
            int r4 = r7._inputPtr
            int r4 = r4 + 1
            byte r3 = r3[r4]
            r3 = r3 & 255(0xff, float:3.57E-43)
            r2 = r2 | r3
            boolean r2 = r7.checkUTF16(r2)
            if (r2 == 0) goto L_0x0055
            goto L_0x003d
        L_0x0079:
            int r0 = r7._bytesPerChar
            if (r0 != r5) goto L_0x0087
            boolean r0 = r7._bigEndian
            if (r0 == 0) goto L_0x0084
            org.codehaus.jackson.JsonEncoding r0 = org.codehaus.jackson.JsonEncoding.UTF16_BE
            goto L_0x0041
        L_0x0084:
            org.codehaus.jackson.JsonEncoding r0 = org.codehaus.jackson.JsonEncoding.UTF16_LE
            goto L_0x0041
        L_0x0087:
            int r0 = r7._bytesPerChar
            if (r0 != r6) goto L_0x0095
            boolean r0 = r7._bigEndian
            if (r0 == 0) goto L_0x0092
            org.codehaus.jackson.JsonEncoding r0 = org.codehaus.jackson.JsonEncoding.UTF32_BE
            goto L_0x0041
        L_0x0092:
            org.codehaus.jackson.JsonEncoding r0 = org.codehaus.jackson.JsonEncoding.UTF32_LE
            goto L_0x0041
        L_0x0095:
            java.lang.RuntimeException r0 = new java.lang.RuntimeException
            java.lang.String r1 = "Internal error"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.impl.ByteSourceBootstrapper.detectEncoding():org.codehaus.jackson.JsonEncoding");
    }

    public final Reader constructReader() throws IOException {
        InputStream mergedStream;
        JsonEncoding encoding = this._context.getEncoding();
        switch (encoding) {
            case UTF32_BE:
            case UTF32_LE:
                return new UTF32Reader(this._context, this._in, this._inputBuffer, this._inputPtr, this._inputEnd, this._context.getEncoding().isBigEndian());
            case UTF16_BE:
            case UTF16_LE:
            case UTF8:
                InputStream inputStream = this._in;
                if (inputStream == null) {
                    mergedStream = new ByteArrayInputStream(this._inputBuffer, this._inputPtr, this._inputEnd);
                } else {
                    mergedStream = this._inputPtr < this._inputEnd ? new MergedStream(this._context, inputStream, this._inputBuffer, this._inputPtr, this._inputEnd) : inputStream;
                }
                return new InputStreamReader(mergedStream, encoding.getJavaName());
            default:
                throw new RuntimeException("Internal error");
        }
    }

    public final JsonParser constructParser(int i, ObjectCodec objectCodec, BytesToNameCanonicalizer bytesToNameCanonicalizer, CharsToNameCanonicalizer charsToNameCanonicalizer) throws IOException, JsonParseException {
        JsonEncoding detectEncoding = detectEncoding();
        boolean enabledIn = JsonParser.Feature.CANONICALIZE_FIELD_NAMES.enabledIn(i);
        boolean enabledIn2 = JsonParser.Feature.INTERN_FIELD_NAMES.enabledIn(i);
        if (detectEncoding != JsonEncoding.UTF8 || !enabledIn) {
            return new ReaderBasedParser(this._context, i, constructReader(), objectCodec, charsToNameCanonicalizer.makeChild(enabledIn, enabledIn2));
        }
        BytesToNameCanonicalizer makeChild = bytesToNameCanonicalizer.makeChild(enabledIn, enabledIn2);
        return new Utf8StreamParser(this._context, i, this._in, objectCodec, makeChild, this._inputBuffer, this._inputPtr, this._inputEnd, this._bufferRecyclable);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private boolean handleBOM(int i) throws IOException {
        switch (i) {
            case -16842752:
                reportWeirdUCS4("3412");
                break;
            case -131072:
                this._inputPtr += 4;
                this._bytesPerChar = 4;
                this._bigEndian = false;
                return true;
            case 65279:
                this._bigEndian = true;
                this._inputPtr += 4;
                this._bytesPerChar = 4;
                return true;
            case 65534:
                reportWeirdUCS4("2143");
                reportWeirdUCS4("3412");
                break;
        }
        int i2 = i >>> 16;
        if (i2 == 65279) {
            this._inputPtr += 2;
            this._bytesPerChar = 2;
            this._bigEndian = true;
            return true;
        } else if (i2 == 65534) {
            this._inputPtr += 2;
            this._bytesPerChar = 2;
            this._bigEndian = false;
            return true;
        } else if ((i >>> 8) != 15711167) {
            return false;
        } else {
            this._inputPtr += 3;
            this._bytesPerChar = 1;
            this._bigEndian = true;
            return true;
        }
    }

    private boolean checkUTF32(int i) throws IOException {
        if ((i >> 8) == 0) {
            this._bigEndian = true;
        } else if ((16777215 & i) == 0) {
            this._bigEndian = false;
        } else if ((-16711681 & i) == 0) {
            reportWeirdUCS4("3412");
        } else if ((-65281 & i) != 0) {
            return false;
        } else {
            reportWeirdUCS4("2143");
        }
        this._bytesPerChar = 4;
        return true;
    }

    private boolean checkUTF16(int i) {
        if ((65280 & i) == 0) {
            this._bigEndian = true;
        } else if ((i & 255) != 0) {
            return false;
        } else {
            this._bigEndian = false;
        }
        this._bytesPerChar = 2;
        return true;
    }

    private void reportWeirdUCS4(String str) throws IOException {
        throw new CharConversionException("Unsupported UCS-4 endianness (" + str + ") detected");
    }

    /* access modifiers changed from: protected */
    public final boolean ensureLoaded(int i) throws IOException {
        int read;
        int i2 = this._inputEnd - this._inputPtr;
        while (i2 < i) {
            if (this._in == null) {
                read = -1;
            } else {
                read = this._in.read(this._inputBuffer, this._inputEnd, this._inputBuffer.length - this._inputEnd);
            }
            if (read <= 0) {
                return false;
            }
            this._inputEnd += read;
            i2 = read + i2;
        }
        return true;
    }
}
