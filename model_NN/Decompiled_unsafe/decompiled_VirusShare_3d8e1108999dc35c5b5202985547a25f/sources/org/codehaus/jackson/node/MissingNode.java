package org.codehaus.jackson.node;

import java.io.IOException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.SerializerProvider;

public final class MissingNode extends BaseJsonNode {
    private static final MissingNode instance = new MissingNode();

    private MissingNode() {
    }

    public static MissingNode getInstance() {
        return instance;
    }

    public final JsonToken asToken() {
        return JsonToken.NOT_AVAILABLE;
    }

    public final boolean isMissingNode() {
        return true;
    }

    public final String getValueAsText() {
        return null;
    }

    public final int getValueAsInt(int i) {
        return 0;
    }

    public final long getValueAsLong(long j) {
        return 0;
    }

    public final double getValueAsDouble(double d) {
        return 0.0d;
    }

    public final JsonNode path(String str) {
        return this;
    }

    public final JsonNode path(int i) {
        return this;
    }

    public final void serialize(JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
    }

    public final boolean equals(Object obj) {
        return obj == this;
    }

    public final String toString() {
        return "";
    }
}
