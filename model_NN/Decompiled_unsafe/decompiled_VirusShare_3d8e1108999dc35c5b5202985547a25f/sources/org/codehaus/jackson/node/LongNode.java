package org.codehaus.jackson.node;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.io.NumberOutput;
import org.codehaus.jackson.map.SerializerProvider;

public final class LongNode extends NumericNode {
    final long _value;

    public LongNode(long j) {
        this._value = j;
    }

    public static LongNode valueOf(long j) {
        return new LongNode(j);
    }

    public final JsonToken asToken() {
        return JsonToken.VALUE_NUMBER_INT;
    }

    public final JsonParser.NumberType getNumberType() {
        return JsonParser.NumberType.LONG;
    }

    public final boolean isIntegralNumber() {
        return true;
    }

    public final boolean isLong() {
        return true;
    }

    public final Number getNumberValue() {
        return Long.valueOf(this._value);
    }

    public final int getIntValue() {
        return (int) this._value;
    }

    public final long getLongValue() {
        return this._value;
    }

    public final double getDoubleValue() {
        return (double) this._value;
    }

    public final BigDecimal getDecimalValue() {
        return BigDecimal.valueOf(this._value);
    }

    public final BigInteger getBigIntegerValue() {
        return BigInteger.valueOf(this._value);
    }

    public final String getValueAsText() {
        return NumberOutput.toString(this._value);
    }

    public final boolean getValueAsBoolean(boolean z) {
        return this._value != 0;
    }

    public final void serialize(JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeNumber(this._value);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        if (((LongNode) obj)._value != this._value) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return ((int) this._value) ^ ((int) (this._value >> 32));
    }
}
