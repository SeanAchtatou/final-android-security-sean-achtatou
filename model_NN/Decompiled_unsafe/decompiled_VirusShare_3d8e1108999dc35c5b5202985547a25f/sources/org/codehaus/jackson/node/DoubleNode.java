package org.codehaus.jackson.node;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.io.NumberOutput;
import org.codehaus.jackson.map.SerializerProvider;

public final class DoubleNode extends NumericNode {
    protected final double _value;

    public DoubleNode(double d) {
        this._value = d;
    }

    public static DoubleNode valueOf(double d) {
        return new DoubleNode(d);
    }

    public final JsonToken asToken() {
        return JsonToken.VALUE_NUMBER_FLOAT;
    }

    public final JsonParser.NumberType getNumberType() {
        return JsonParser.NumberType.DOUBLE;
    }

    public final boolean isFloatingPointNumber() {
        return true;
    }

    public final boolean isDouble() {
        return true;
    }

    public final Number getNumberValue() {
        return Double.valueOf(this._value);
    }

    public final int getIntValue() {
        return (int) this._value;
    }

    public final long getLongValue() {
        return (long) this._value;
    }

    public final double getDoubleValue() {
        return this._value;
    }

    public final BigDecimal getDecimalValue() {
        return BigDecimal.valueOf(this._value);
    }

    public final BigInteger getBigIntegerValue() {
        return getDecimalValue().toBigInteger();
    }

    public final String getValueAsText() {
        return NumberOutput.toString(this._value);
    }

    public final void serialize(JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeNumber(this._value);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        if (((DoubleNode) obj)._value != this._value) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        long doubleToLongBits = Double.doubleToLongBits(this._value);
        return ((int) (doubleToLongBits >> 32)) ^ ((int) doubleToLongBits);
    }
}
