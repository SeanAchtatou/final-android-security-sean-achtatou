package org.codehaus.jackson.node;

import java.io.IOException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.SerializerProvider;

public final class NullNode extends ValueNode {
    public static final NullNode instance = new NullNode();

    private NullNode() {
    }

    public static NullNode getInstance() {
        return instance;
    }

    public final JsonToken asToken() {
        return JsonToken.VALUE_NULL;
    }

    public final boolean isNull() {
        return true;
    }

    public final String getValueAsText() {
        return "null";
    }

    public final int getValueAsInt(int i) {
        return 0;
    }

    public final long getValueAsLong(long j) {
        return 0;
    }

    public final double getValueAsDouble(double d) {
        return 0.0d;
    }

    public final void serialize(JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeNull();
    }

    public final boolean equals(Object obj) {
        return obj == this;
    }
}
