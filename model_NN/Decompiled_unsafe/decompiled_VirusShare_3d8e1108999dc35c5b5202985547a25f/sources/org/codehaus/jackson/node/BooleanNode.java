package org.codehaus.jackson.node;

import java.io.IOException;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.SerializerProvider;

public final class BooleanNode extends ValueNode {
    public static final BooleanNode FALSE = new BooleanNode();
    public static final BooleanNode TRUE = new BooleanNode();

    private BooleanNode() {
    }

    public static BooleanNode getTrue() {
        return TRUE;
    }

    public static BooleanNode getFalse() {
        return FALSE;
    }

    public static BooleanNode valueOf(boolean z) {
        return z ? TRUE : FALSE;
    }

    public final JsonToken asToken() {
        return this == TRUE ? JsonToken.VALUE_TRUE : JsonToken.VALUE_FALSE;
    }

    public final boolean isBoolean() {
        return true;
    }

    public final boolean getBooleanValue() {
        return this == TRUE;
    }

    public final String getValueAsText() {
        return this == TRUE ? "true" : "false";
    }

    public final boolean getValueAsBoolean() {
        return this == TRUE;
    }

    public final boolean getValueAsBoolean(boolean z) {
        return this == TRUE;
    }

    public final int getValueAsInt(int i) {
        return this == TRUE ? 1 : 0;
    }

    public final long getValueAsLong(long j) {
        return this == TRUE ? 1 : 0;
    }

    public final double getValueAsDouble(double d) {
        return this == TRUE ? 1.0d : 0.0d;
    }

    public final void serialize(JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeBoolean(this == TRUE);
    }

    public final boolean equals(Object obj) {
        return obj == this;
    }
}
