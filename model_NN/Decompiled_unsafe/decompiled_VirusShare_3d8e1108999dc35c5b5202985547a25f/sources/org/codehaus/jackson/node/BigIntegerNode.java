package org.codehaus.jackson.node;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.SerializerProvider;

public final class BigIntegerNode extends NumericNode {
    protected final BigInteger _value;

    public BigIntegerNode(BigInteger bigInteger) {
        this._value = bigInteger;
    }

    public static BigIntegerNode valueOf(BigInteger bigInteger) {
        return new BigIntegerNode(bigInteger);
    }

    public final JsonToken asToken() {
        return JsonToken.VALUE_NUMBER_INT;
    }

    public final JsonParser.NumberType getNumberType() {
        return JsonParser.NumberType.BIG_INTEGER;
    }

    public final boolean isIntegralNumber() {
        return true;
    }

    public final boolean isBigInteger() {
        return true;
    }

    public final Number getNumberValue() {
        return this._value;
    }

    public final int getIntValue() {
        return this._value.intValue();
    }

    public final long getLongValue() {
        return this._value.longValue();
    }

    public final BigInteger getBigIntegerValue() {
        return this._value;
    }

    public final double getDoubleValue() {
        return this._value.doubleValue();
    }

    public final BigDecimal getDecimalValue() {
        return new BigDecimal(this._value);
    }

    public final String getValueAsText() {
        return this._value.toString();
    }

    public final boolean getValueAsBoolean(boolean z) {
        return !BigInteger.ZERO.equals(this._value);
    }

    public final void serialize(JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeNumber(this._value);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        if (((BigIntegerNode) obj)._value != this._value) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return this._value.hashCode();
    }
}
