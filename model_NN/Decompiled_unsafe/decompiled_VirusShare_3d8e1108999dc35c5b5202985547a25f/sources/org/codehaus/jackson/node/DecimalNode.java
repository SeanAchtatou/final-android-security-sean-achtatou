package org.codehaus.jackson.node;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.SerializerProvider;

public final class DecimalNode extends NumericNode {
    protected final BigDecimal _value;

    public DecimalNode(BigDecimal bigDecimal) {
        this._value = bigDecimal;
    }

    public static DecimalNode valueOf(BigDecimal bigDecimal) {
        return new DecimalNode(bigDecimal);
    }

    public final JsonToken asToken() {
        return JsonToken.VALUE_NUMBER_FLOAT;
    }

    public final JsonParser.NumberType getNumberType() {
        return JsonParser.NumberType.BIG_DECIMAL;
    }

    public final boolean isFloatingPointNumber() {
        return true;
    }

    public final boolean isBigDecimal() {
        return true;
    }

    public final Number getNumberValue() {
        return this._value;
    }

    public final int getIntValue() {
        return this._value.intValue();
    }

    public final long getLongValue() {
        return this._value.longValue();
    }

    public final BigInteger getBigIntegerValue() {
        return this._value.toBigInteger();
    }

    public final double getDoubleValue() {
        return this._value.doubleValue();
    }

    public final BigDecimal getDecimalValue() {
        return this._value;
    }

    public final String getValueAsText() {
        return this._value.toString();
    }

    public final void serialize(JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeNumber(this._value);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        return ((DecimalNode) obj)._value.equals(this._value);
    }

    public final int hashCode() {
        return this._value.hashCode();
    }
}
