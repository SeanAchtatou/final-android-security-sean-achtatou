package org.codehaus.jackson.io;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;

public final class UTF8Writer extends Writer {
    static final int SURR1_FIRST = 55296;
    static final int SURR1_LAST = 56319;
    static final int SURR2_FIRST = 56320;
    static final int SURR2_LAST = 57343;
    protected final IOContext _context;
    OutputStream _out;
    byte[] _outBuffer;
    final int _outBufferEnd;
    int _outPtr;
    int _surrogate = 0;

    public UTF8Writer(IOContext iOContext, OutputStream outputStream) {
        this._context = iOContext;
        this._out = outputStream;
        this._outBuffer = iOContext.allocWriteEncodingBuffer();
        this._outBufferEnd = this._outBuffer.length - 4;
        this._outPtr = 0;
    }

    public final Writer append(char c) throws IOException {
        write(c);
        return this;
    }

    public final void close() throws IOException {
        if (this._out != null) {
            if (this._outPtr > 0) {
                this._out.write(this._outBuffer, 0, this._outPtr);
                this._outPtr = 0;
            }
            OutputStream outputStream = this._out;
            this._out = null;
            byte[] bArr = this._outBuffer;
            if (bArr != null) {
                this._outBuffer = null;
                this._context.releaseWriteEncodingBuffer(bArr);
            }
            outputStream.close();
            int i = this._surrogate;
            this._surrogate = 0;
            if (i > 0) {
                throwIllegal(i);
            }
        }
    }

    public final void flush() throws IOException {
        if (this._outPtr > 0) {
            this._out.write(this._outBuffer, 0, this._outPtr);
            this._outPtr = 0;
        }
        this._out.flush();
    }

    public final void write(char[] cArr) throws IOException {
        write(cArr, 0, cArr.length);
    }

    public final void write(char[] cArr, int i, int i2) throws IOException {
        if (i2 >= 2) {
            if (this._surrogate > 0) {
                i2--;
                write(convertSurrogate(cArr[i]));
                i++;
            }
            int i3 = this._outPtr;
            byte[] bArr = this._outBuffer;
            int i4 = this._outBufferEnd;
            int i5 = i2 + r13;
            while (r13 < i5) {
                if (i3 >= i4) {
                    this._out.write(bArr, 0, i3);
                    i3 = 0;
                }
                int i6 = r13 + 1;
                char c = cArr[r13];
                if (c < 128) {
                    int i7 = i3 + 1;
                    bArr[i3] = (byte) c;
                    int i8 = i5 - i6;
                    int i9 = i4 - i7;
                    if (i8 <= i9) {
                        i9 = i8;
                    }
                    int i10 = i9 + i6;
                    int i11 = i7;
                    int i12 = i6;
                    while (i12 < i10) {
                        i6 = i12 + 1;
                        char c2 = cArr[i12];
                        if (c2 < 128) {
                            bArr[i11] = (byte) c2;
                            i11++;
                            i12 = i6;
                        } else {
                            char c3 = c2;
                            i3 = i11;
                            c = c3;
                        }
                    }
                    r13 = i12;
                    i3 = i11;
                }
                if (c >= 2048) {
                    if (c >= SURR1_FIRST && c <= SURR2_LAST) {
                        if (c > SURR1_LAST) {
                            this._outPtr = i3;
                            throwIllegal(c);
                        }
                        this._surrogate = c;
                        if (i6 >= i5) {
                            break;
                        }
                        r13 = i6 + 1;
                        int convertSurrogate = convertSurrogate(cArr[i6]);
                        if (convertSurrogate > 1114111) {
                            this._outPtr = i3;
                            throwIllegal(convertSurrogate);
                        }
                        int i13 = i3 + 1;
                        bArr[i3] = (byte) ((convertSurrogate >> 18) | 240);
                        int i14 = i13 + 1;
                        bArr[i13] = (byte) (((convertSurrogate >> 12) & 63) | 128);
                        int i15 = i14 + 1;
                        bArr[i14] = (byte) (((convertSurrogate >> 6) & 63) | 128);
                        i3 = i15 + 1;
                        bArr[i15] = (byte) ((convertSurrogate & 63) | 128);
                    } else {
                        int i16 = i3 + 1;
                        bArr[i3] = (byte) ((c >> 12) | 224);
                        int i17 = i16 + 1;
                        bArr[i16] = (byte) (((c >> 6) & 63) | 128);
                        i3 = i17 + 1;
                        bArr[i17] = (byte) ((c & '?') | 128);
                        r13 = i6;
                    }
                } else {
                    int i18 = i3 + 1;
                    bArr[i3] = (byte) ((c >> 6) | 192);
                    i3 = i18 + 1;
                    bArr[i18] = (byte) ((c & '?') | 128);
                    r13 = i6;
                }
            }
            this._outPtr = i3;
        } else if (i2 == 1) {
            write(cArr[i]);
        }
    }

    public final void write(int i) throws IOException {
        int i2;
        if (this._surrogate > 0) {
            i = convertSurrogate(i);
        } else if (i >= SURR1_FIRST && i <= SURR2_LAST) {
            if (i > SURR1_LAST) {
                throwIllegal(i);
            }
            this._surrogate = i;
            return;
        }
        if (this._outPtr >= this._outBufferEnd) {
            this._out.write(this._outBuffer, 0, this._outPtr);
            this._outPtr = 0;
        }
        if (i < 128) {
            byte[] bArr = this._outBuffer;
            int i3 = this._outPtr;
            this._outPtr = i3 + 1;
            bArr[i3] = (byte) i;
            return;
        }
        int i4 = this._outPtr;
        if (i < 2048) {
            int i5 = i4 + 1;
            this._outBuffer[i4] = (byte) ((i >> 6) | 192);
            i2 = i5 + 1;
            this._outBuffer[i5] = (byte) ((i & 63) | 128);
        } else if (i <= 65535) {
            int i6 = i4 + 1;
            this._outBuffer[i4] = (byte) ((i >> 12) | 224);
            int i7 = i6 + 1;
            this._outBuffer[i6] = (byte) (((i >> 6) & 63) | 128);
            i2 = i7 + 1;
            this._outBuffer[i7] = (byte) ((i & 63) | 128);
        } else {
            if (i > 1114111) {
                throwIllegal(i);
            }
            int i8 = i4 + 1;
            this._outBuffer[i4] = (byte) ((i >> 18) | 240);
            int i9 = i8 + 1;
            this._outBuffer[i8] = (byte) (((i >> 12) & 63) | 128);
            int i10 = i9 + 1;
            this._outBuffer[i9] = (byte) (((i >> 6) & 63) | 128);
            i2 = i10 + 1;
            this._outBuffer[i10] = (byte) ((i & 63) | 128);
        }
        this._outPtr = i2;
    }

    public final void write(String str) throws IOException {
        write(str, 0, str.length());
    }

    public final void write(String str, int i, int i2) throws IOException {
        if (i2 >= 2) {
            if (this._surrogate > 0) {
                i2--;
                write(convertSurrogate(str.charAt(i)));
                i++;
            }
            int i3 = this._outPtr;
            byte[] bArr = this._outBuffer;
            int i4 = this._outBufferEnd;
            int i5 = i2 + r13;
            while (r13 < i5) {
                if (i3 >= i4) {
                    this._out.write(bArr, 0, i3);
                    i3 = 0;
                }
                int i6 = r13 + 1;
                char charAt = str.charAt(r13);
                if (charAt < 128) {
                    int i7 = i3 + 1;
                    bArr[i3] = (byte) charAt;
                    int i8 = i5 - i6;
                    int i9 = i4 - i7;
                    if (i8 <= i9) {
                        i9 = i8;
                    }
                    int i10 = i9 + i6;
                    int i11 = i7;
                    int i12 = i6;
                    while (i12 < i10) {
                        i6 = i12 + 1;
                        char charAt2 = str.charAt(i12);
                        if (charAt2 < 128) {
                            bArr[i11] = (byte) charAt2;
                            i11++;
                            i12 = i6;
                        } else {
                            char c = charAt2;
                            i3 = i11;
                            charAt = c;
                        }
                    }
                    r13 = i12;
                    i3 = i11;
                }
                if (charAt >= 2048) {
                    if (charAt >= SURR1_FIRST && charAt <= SURR2_LAST) {
                        if (charAt > SURR1_LAST) {
                            this._outPtr = i3;
                            throwIllegal(charAt);
                        }
                        this._surrogate = charAt;
                        if (i6 >= i5) {
                            break;
                        }
                        r13 = i6 + 1;
                        int convertSurrogate = convertSurrogate(str.charAt(i6));
                        if (convertSurrogate > 1114111) {
                            this._outPtr = i3;
                            throwIllegal(convertSurrogate);
                        }
                        int i13 = i3 + 1;
                        bArr[i3] = (byte) ((convertSurrogate >> 18) | 240);
                        int i14 = i13 + 1;
                        bArr[i13] = (byte) (((convertSurrogate >> 12) & 63) | 128);
                        int i15 = i14 + 1;
                        bArr[i14] = (byte) (((convertSurrogate >> 6) & 63) | 128);
                        i3 = i15 + 1;
                        bArr[i15] = (byte) ((convertSurrogate & 63) | 128);
                    } else {
                        int i16 = i3 + 1;
                        bArr[i3] = (byte) ((charAt >> 12) | 224);
                        int i17 = i16 + 1;
                        bArr[i16] = (byte) (((charAt >> 6) & 63) | 128);
                        i3 = i17 + 1;
                        bArr[i17] = (byte) ((charAt & '?') | 128);
                        r13 = i6;
                    }
                } else {
                    int i18 = i3 + 1;
                    bArr[i3] = (byte) ((charAt >> 6) | 192);
                    i3 = i18 + 1;
                    bArr[i18] = (byte) ((charAt & '?') | 128);
                    r13 = i6;
                }
            }
            this._outPtr = i3;
        } else if (i2 == 1) {
            write(str.charAt(i));
        }
    }

    private int convertSurrogate(int i) throws IOException {
        int i2 = this._surrogate;
        this._surrogate = 0;
        if (i >= SURR2_FIRST && i <= SURR2_LAST) {
            return ((i2 - SURR1_FIRST) << 10) + 65536 + (i - SURR2_FIRST);
        }
        throw new IOException("Broken surrogate pair: first char 0x" + Integer.toHexString(i2) + ", second 0x" + Integer.toHexString(i) + "; illegal combination");
    }

    private void throwIllegal(int i) throws IOException {
        if (i > 1114111) {
            throw new IOException("Illegal character point (0x" + Integer.toHexString(i) + ") to output; max is 0x10FFFF as per RFC 4627");
        } else if (i < SURR1_FIRST) {
            throw new IOException("Illegal character point (0x" + Integer.toHexString(i) + ") to output");
        } else if (i <= SURR1_LAST) {
            throw new IOException("Unmatched first part of surrogate pair (0x" + Integer.toHexString(i) + ")");
        } else {
            throw new IOException("Unmatched second part of surrogate pair (0x" + Integer.toHexString(i) + ")");
        }
    }
}
