package org.codehaus.jackson.io;

import java.lang.ref.SoftReference;
import org.codehaus.jackson.util.BufferRecycler;
import org.codehaus.jackson.util.ByteArrayBuilder;
import org.codehaus.jackson.util.CharTypes;
import org.codehaus.jackson.util.TextBuffer;

public final class JsonStringEncoder {
    private static final byte[] HEX_BYTES = CharTypes.copyHexBytes();
    private static final char[] HEX_CHARS = CharTypes.copyHexChars();
    private static final int INT_0 = 48;
    private static final int INT_BACKSLASH = 92;
    private static final int INT_U = 117;
    private static final int SURR1_FIRST = 55296;
    private static final int SURR1_LAST = 56319;
    private static final int SURR2_FIRST = 56320;
    private static final int SURR2_LAST = 57343;
    protected static final ThreadLocal<SoftReference<JsonStringEncoder>> _threadEncoder = new ThreadLocal<>();
    protected ByteArrayBuilder _byteBuilder;
    protected final char[] _quoteBuffer = new char[6];
    protected TextBuffer _textBuffer;

    public JsonStringEncoder() {
        this._quoteBuffer[0] = '\\';
        this._quoteBuffer[2] = '0';
        this._quoteBuffer[3] = '0';
    }

    public static JsonStringEncoder getInstance() {
        SoftReference softReference = _threadEncoder.get();
        JsonStringEncoder jsonStringEncoder = softReference == null ? null : (JsonStringEncoder) softReference.get();
        if (jsonStringEncoder != null) {
            return jsonStringEncoder;
        }
        JsonStringEncoder jsonStringEncoder2 = new JsonStringEncoder();
        _threadEncoder.set(new SoftReference(jsonStringEncoder2));
        return jsonStringEncoder2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0040, code lost:
        r4 = r2 + 1;
        r2 = _appendSingleEscape(r6[r12.charAt(r2)], r11._quoteBuffer);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0051, code lost:
        if ((r1 + r2) <= r3.length) goto L_0x0069;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0053, code lost:
        r9 = r3.length - r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0055, code lost:
        if (r9 <= 0) goto L_0x005c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0057, code lost:
        java.lang.System.arraycopy(r11._quoteBuffer, 0, r3, r1, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005c, code lost:
        r3 = r0.finishCurrentSegment();
        r2 = r2 - r9;
        java.lang.System.arraycopy(r11._quoteBuffer, r9, r3, r1, r2);
        r1 = r1 + r2;
        r2 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0069, code lost:
        java.lang.System.arraycopy(r11._quoteBuffer, 0, r3, r1, r2);
        r1 = r1 + r2;
        r2 = r4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final char[] quoteAsString(java.lang.String r12) {
        /*
            r11 = this;
            r5 = 0
            org.codehaus.jackson.util.TextBuffer r0 = r11._textBuffer
            if (r0 != 0) goto L_0x000d
            org.codehaus.jackson.util.TextBuffer r0 = new org.codehaus.jackson.util.TextBuffer
            r1 = 0
            r0.<init>(r1)
            r11._textBuffer = r0
        L_0x000d:
            char[] r3 = r0.emptyAndGetCurrentSegment()
            int[] r6 = org.codehaus.jackson.util.CharTypes.getOutputEscapes()
            int r7 = r6.length
            int r8 = r12.length()
            r1 = r5
            r2 = r5
        L_0x001c:
            if (r2 >= r8) goto L_0x0038
        L_0x001e:
            char r9 = r12.charAt(r2)
            if (r9 >= r7) goto L_0x0028
            r4 = r6[r9]
            if (r4 != 0) goto L_0x0040
        L_0x0028:
            int r4 = r3.length
            if (r1 < r4) goto L_0x0071
            char[] r3 = r0.finishCurrentSegment()
            r4 = r5
        L_0x0030:
            int r1 = r4 + 1
            r3[r4] = r9
            int r2 = r2 + 1
            if (r2 < r8) goto L_0x001e
        L_0x0038:
            r0.setCurrentLength(r1)
            char[] r0 = r0.contentsAsArray()
            return r0
        L_0x0040:
            int r4 = r2 + 1
            char r2 = r12.charAt(r2)
            r2 = r6[r2]
            char[] r9 = r11._quoteBuffer
            int r2 = r11._appendSingleEscape(r2, r9)
            int r9 = r1 + r2
            int r10 = r3.length
            if (r9 <= r10) goto L_0x0069
            int r9 = r3.length
            int r9 = r9 - r1
            if (r9 <= 0) goto L_0x005c
            char[] r10 = r11._quoteBuffer
            java.lang.System.arraycopy(r10, r5, r3, r1, r9)
        L_0x005c:
            char[] r3 = r0.finishCurrentSegment()
            int r2 = r2 - r9
            char[] r10 = r11._quoteBuffer
            java.lang.System.arraycopy(r10, r9, r3, r1, r2)
            int r1 = r1 + r2
            r2 = r4
            goto L_0x001c
        L_0x0069:
            char[] r9 = r11._quoteBuffer
            java.lang.System.arraycopy(r9, r5, r3, r1, r2)
            int r1 = r1 + r2
            r2 = r4
            goto L_0x001c
        L_0x0071:
            r4 = r1
            goto L_0x0030
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.io.JsonStringEncoder.quoteAsString(java.lang.String):char[]");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0042, code lost:
        if (r2 < r1.length) goto L_0x0049;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0044, code lost:
        r1 = r0.finishCurrentSegment();
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0049, code lost:
        r4 = r3 + 1;
        r8 = r12.charAt(r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x004f, code lost:
        if (r8 > 127) goto L_0x005d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0051, code lost:
        r2 = _appendByteEscape(r6[r8], r0, r2);
        r1 = r0.getCurrentSegment();
        r3 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005f, code lost:
        if (r8 > 2047) goto L_0x0082;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0061, code lost:
        r3 = r2 + 1;
        r1[r2] = (byte) ((r8 >> 6) | 192);
        r2 = r1;
        r1 = (r8 & '?') | 128;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0072, code lost:
        if (r3 < r2.length) goto L_0x0079;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0074, code lost:
        r2 = r0.finishCurrentSegment();
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0079, code lost:
        r6 = r3 + 1;
        r2[r3] = (byte) r1;
        r1 = r2;
        r3 = r4;
        r2 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0085, code lost:
        if (r8 < org.codehaus.jackson.io.JsonStringEncoder.SURR1_FIRST) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x008a, code lost:
        if (r8 <= org.codehaus.jackson.io.JsonStringEncoder.SURR2_LAST) goto L_0x00b0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x008c, code lost:
        r3 = r2 + 1;
        r1[r2] = (byte) ((r8 >> 12) | 224);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0096, code lost:
        if (r3 < r1.length) goto L_0x010c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0098, code lost:
        r1 = r0.finishCurrentSegment();
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x009d, code lost:
        r3 = r2 + 1;
        r1[r2] = (byte) (((r8 >> 6) & 63) | 128);
        r2 = r1;
        r1 = (r8 & '?') | 128;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00b3, code lost:
        if (r8 <= org.codehaus.jackson.io.JsonStringEncoder.SURR1_LAST) goto L_0x00b8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00b5, code lost:
        _throwIllegalSurrogate(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00b8, code lost:
        if (r4 < r7) goto L_0x00bd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00ba, code lost:
        _throwIllegalSurrogate(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00bd, code lost:
        r6 = r4 + 1;
        r4 = _convertSurrogate(r8, r12.charAt(r4));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00ca, code lost:
        if (r4 <= 1114111) goto L_0x00cf;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00cc, code lost:
        _throwIllegalSurrogate(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00cf, code lost:
        r3 = r2 + 1;
        r1[r2] = (byte) ((r4 >> 18) | 240);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00d9, code lost:
        if (r3 < r1.length) goto L_0x010a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00db, code lost:
        r1 = r0.finishCurrentSegment();
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00e0, code lost:
        r3 = r2 + 1;
        r1[r2] = (byte) (((r4 >> 12) & 63) | 128);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00ec, code lost:
        if (r3 < r1.length) goto L_0x0108;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00ee, code lost:
        r1 = r0.finishCurrentSegment();
        r2 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00f3, code lost:
        r3 = r2 + 1;
        r1[r2] = (byte) (((r4 >> 6) & 63) | 128);
        r4 = r6;
        r10 = r1;
        r1 = (r4 & '?') | 128;
        r2 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0108, code lost:
        r2 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x010a, code lost:
        r2 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x010c, code lost:
        r2 = r3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final byte[] quoteAsUTF8(java.lang.String r12) {
        /*
            r11 = this;
            r9 = 127(0x7f, float:1.78E-43)
            r5 = 0
            org.codehaus.jackson.util.ByteArrayBuilder r0 = r11._byteBuilder
            if (r0 != 0) goto L_0x000f
            org.codehaus.jackson.util.ByteArrayBuilder r0 = new org.codehaus.jackson.util.ByteArrayBuilder
            r1 = 0
            r0.<init>(r1)
            r11._byteBuilder = r0
        L_0x000f:
            int r7 = r12.length()
            byte[] r1 = r0.resetAndGetFirstSegment()
            r2 = r5
            r3 = r5
        L_0x0019:
            if (r3 >= r7) goto L_0x003a
            int[] r6 = org.codehaus.jackson.util.CharTypes.getOutputEscapes()
        L_0x001f:
            char r8 = r12.charAt(r3)
            if (r8 > r9) goto L_0x0041
            r4 = r6[r8]
            if (r4 != 0) goto L_0x0041
            int r4 = r1.length
            if (r2 < r4) goto L_0x010e
            byte[] r1 = r0.finishCurrentSegment()
            r4 = r5
        L_0x0031:
            int r2 = r4 + 1
            byte r8 = (byte) r8
            r1[r4] = r8
            int r3 = r3 + 1
            if (r3 < r7) goto L_0x001f
        L_0x003a:
            org.codehaus.jackson.util.ByteArrayBuilder r0 = r11._byteBuilder
            byte[] r0 = r0.completeAndCoalesce(r2)
            return r0
        L_0x0041:
            int r4 = r1.length
            if (r2 < r4) goto L_0x0049
            byte[] r1 = r0.finishCurrentSegment()
            r2 = r5
        L_0x0049:
            int r4 = r3 + 1
            char r8 = r12.charAt(r3)
            if (r8 > r9) goto L_0x005d
            r1 = r6[r8]
            int r2 = r11._appendByteEscape(r1, r0, r2)
            byte[] r1 = r0.getCurrentSegment()
            r3 = r4
            goto L_0x0019
        L_0x005d:
            r3 = 2047(0x7ff, float:2.868E-42)
            if (r8 > r3) goto L_0x0082
            int r3 = r2 + 1
            int r6 = r8 >> 6
            r6 = r6 | 192(0xc0, float:2.69E-43)
            byte r6 = (byte) r6
            r1[r2] = r6
            r2 = r8 & 63
            r2 = r2 | 128(0x80, float:1.794E-43)
            r10 = r2
            r2 = r1
            r1 = r10
        L_0x0071:
            int r6 = r2.length
            if (r3 < r6) goto L_0x0079
            byte[] r2 = r0.finishCurrentSegment()
            r3 = r5
        L_0x0079:
            int r6 = r3 + 1
            byte r1 = (byte) r1
            r2[r3] = r1
            r1 = r2
            r3 = r4
            r2 = r6
            goto L_0x0019
        L_0x0082:
            r3 = 55296(0xd800, float:7.7486E-41)
            if (r8 < r3) goto L_0x008c
            r3 = 57343(0xdfff, float:8.0355E-41)
            if (r8 <= r3) goto L_0x00b0
        L_0x008c:
            int r3 = r2 + 1
            int r6 = r8 >> 12
            r6 = r6 | 224(0xe0, float:3.14E-43)
            byte r6 = (byte) r6
            r1[r2] = r6
            int r2 = r1.length
            if (r3 < r2) goto L_0x010c
            byte[] r1 = r0.finishCurrentSegment()
            r2 = r5
        L_0x009d:
            int r3 = r2 + 1
            int r6 = r8 >> 6
            r6 = r6 & 63
            r6 = r6 | 128(0x80, float:1.794E-43)
            byte r6 = (byte) r6
            r1[r2] = r6
            r2 = r8 & 63
            r2 = r2 | 128(0x80, float:1.794E-43)
            r10 = r2
            r2 = r1
            r1 = r10
            goto L_0x0071
        L_0x00b0:
            r3 = 56319(0xdbff, float:7.892E-41)
            if (r8 <= r3) goto L_0x00b8
            r11._throwIllegalSurrogate(r8)
        L_0x00b8:
            if (r4 < r7) goto L_0x00bd
            r11._throwIllegalSurrogate(r8)
        L_0x00bd:
            int r6 = r4 + 1
            char r3 = r12.charAt(r4)
            int r4 = r11._convertSurrogate(r8, r3)
            r3 = 1114111(0x10ffff, float:1.561202E-39)
            if (r4 <= r3) goto L_0x00cf
            r11._throwIllegalSurrogate(r4)
        L_0x00cf:
            int r3 = r2 + 1
            int r8 = r4 >> 18
            r8 = r8 | 240(0xf0, float:3.36E-43)
            byte r8 = (byte) r8
            r1[r2] = r8
            int r2 = r1.length
            if (r3 < r2) goto L_0x010a
            byte[] r1 = r0.finishCurrentSegment()
            r2 = r5
        L_0x00e0:
            int r3 = r2 + 1
            int r8 = r4 >> 12
            r8 = r8 & 63
            r8 = r8 | 128(0x80, float:1.794E-43)
            byte r8 = (byte) r8
            r1[r2] = r8
            int r2 = r1.length
            if (r3 < r2) goto L_0x0108
            byte[] r1 = r0.finishCurrentSegment()
            r2 = r5
        L_0x00f3:
            int r3 = r2 + 1
            int r8 = r4 >> 6
            r8 = r8 & 63
            r8 = r8 | 128(0x80, float:1.794E-43)
            byte r8 = (byte) r8
            r1[r2] = r8
            r2 = r4 & 63
            r2 = r2 | 128(0x80, float:1.794E-43)
            r4 = r6
            r10 = r1
            r1 = r2
            r2 = r10
            goto L_0x0071
        L_0x0108:
            r2 = r3
            goto L_0x00f3
        L_0x010a:
            r2 = r3
            goto L_0x00e0
        L_0x010c:
            r2 = r3
            goto L_0x009d
        L_0x010e:
            r4 = r2
            goto L_0x0031
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.io.JsonStringEncoder.quoteAsUTF8(java.lang.String):byte[]");
    }

    public final byte[] encodeAsUTF8(String str) {
        int i;
        int i2;
        int i3;
        char c;
        int i4;
        ByteArrayBuilder byteArrayBuilder = this._byteBuilder;
        if (byteArrayBuilder == null) {
            byteArrayBuilder = new ByteArrayBuilder((BufferRecycler) null);
            this._byteBuilder = byteArrayBuilder;
        }
        int length = str.length();
        byte[] resetAndGetFirstSegment = byteArrayBuilder.resetAndGetFirstSegment();
        int length2 = resetAndGetFirstSegment.length;
        int i5 = 0;
        int i6 = 0;
        loop0:
        while (true) {
            if (i6 >= length) {
                i = i5;
                break;
            }
            int i7 = i6 + 1;
            char charAt = str.charAt(i6);
            int i8 = length2;
            byte[] bArr = resetAndGetFirstSegment;
            int i9 = i5;
            int i10 = i8;
            while (charAt <= 127) {
                if (i9 >= i10) {
                    bArr = byteArrayBuilder.finishCurrentSegment();
                    i10 = bArr.length;
                    i9 = 0;
                }
                int i11 = i9 + 1;
                bArr[i9] = (byte) charAt;
                if (i7 >= length) {
                    i = i11;
                    break loop0;
                }
                charAt = str.charAt(i7);
                i7++;
                i9 = i11;
            }
            if (i9 >= i10) {
                bArr = byteArrayBuilder.finishCurrentSegment();
                i10 = bArr.length;
                i2 = 0;
            } else {
                i2 = i9;
            }
            if (charAt < 2048) {
                i3 = i2 + 1;
                bArr[i2] = (byte) ((charAt >> 6) | 192);
                c = charAt;
                i6 = i7;
            } else if (charAt < SURR1_FIRST || charAt > SURR2_LAST) {
                int i12 = i2 + 1;
                bArr[i2] = (byte) ((charAt >> 12) | 224);
                if (i12 >= i10) {
                    bArr = byteArrayBuilder.finishCurrentSegment();
                    i10 = bArr.length;
                    i12 = 0;
                }
                bArr[i12] = (byte) (((charAt >> 6) & 63) | 128);
                i3 = i12 + 1;
                c = charAt;
                i6 = i7;
            } else {
                if (charAt > SURR1_LAST) {
                    _throwIllegalSurrogate(charAt);
                }
                if (i7 >= length) {
                    _throwIllegalSurrogate(charAt);
                }
                int i13 = i7 + 1;
                int _convertSurrogate = _convertSurrogate(charAt, str.charAt(i7));
                if (_convertSurrogate > 1114111) {
                    _throwIllegalSurrogate(_convertSurrogate);
                }
                int i14 = i2 + 1;
                bArr[i2] = (byte) ((_convertSurrogate >> 18) | 240);
                if (i14 >= i10) {
                    bArr = byteArrayBuilder.finishCurrentSegment();
                    i10 = bArr.length;
                    i14 = 0;
                }
                int i15 = i14 + 1;
                bArr[i14] = (byte) (((_convertSurrogate >> 12) & 63) | 128);
                if (i15 >= i10) {
                    bArr = byteArrayBuilder.finishCurrentSegment();
                    i10 = bArr.length;
                    i4 = 0;
                } else {
                    i4 = i15;
                }
                bArr[i4] = (byte) (((_convertSurrogate >> 6) & 63) | 128);
                i3 = i4 + 1;
                c = _convertSurrogate;
                i6 = i13;
            }
            if (i3 >= i10) {
                bArr = byteArrayBuilder.finishCurrentSegment();
                i10 = bArr.length;
                i3 = 0;
            }
            int i16 = i3 + 1;
            bArr[i3] = (byte) ((c & '?') | 128);
            resetAndGetFirstSegment = bArr;
            length2 = i10;
            i5 = i16;
        }
        return this._byteBuilder.completeAndCoalesce(i);
    }

    private int _appendSingleEscape(int i, char[] cArr) {
        if (i < 0) {
            int i2 = -(i + 1);
            cArr[1] = 'u';
            cArr[4] = HEX_CHARS[i2 >> 4];
            cArr[5] = HEX_CHARS[i2 & 15];
            return 6;
        }
        cArr[1] = (char) i;
        return 2;
    }

    private int _appendByteEscape(int i, ByteArrayBuilder byteArrayBuilder, int i2) {
        byteArrayBuilder.setCurrentSegmentLength(i2);
        byteArrayBuilder.append(INT_BACKSLASH);
        if (i < 0) {
            int i3 = -(i + 1);
            byteArrayBuilder.append(INT_U);
            byteArrayBuilder.append(INT_0);
            byteArrayBuilder.append(INT_0);
            byteArrayBuilder.append(HEX_BYTES[i3 >> 4]);
            byteArrayBuilder.append(HEX_BYTES[i3 & 15]);
        } else {
            byteArrayBuilder.append((byte) i);
        }
        return byteArrayBuilder.getCurrentSegmentLength();
    }

    private int _convertSurrogate(int i, int i2) {
        if (i2 >= SURR2_FIRST && i2 <= SURR2_LAST) {
            return 65536 + ((i - SURR1_FIRST) << 10) + (i2 - SURR2_FIRST);
        }
        throw new IllegalArgumentException("Broken surrogate pair: first char 0x" + Integer.toHexString(i) + ", second 0x" + Integer.toHexString(i2) + "; illegal combination");
    }

    private void _throwIllegalSurrogate(int i) {
        if (i > 1114111) {
            throw new IllegalArgumentException("Illegal character point (0x" + Integer.toHexString(i) + ") to output; max is 0x10FFFF as per RFC 4627");
        } else if (i < SURR1_FIRST) {
            throw new IllegalArgumentException("Illegal character point (0x" + Integer.toHexString(i) + ") to output");
        } else if (i <= SURR1_LAST) {
            throw new IllegalArgumentException("Unmatched first part of surrogate pair (0x" + Integer.toHexString(i) + ")");
        } else {
            throw new IllegalArgumentException("Unmatched second part of surrogate pair (0x" + Integer.toHexString(i) + ")");
        }
    }
}
