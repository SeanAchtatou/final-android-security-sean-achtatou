package org.codehaus.jackson.io;

import java.io.CharConversionException;
import java.io.IOException;
import java.io.InputStream;

public final class UTF32Reader extends BaseReader {
    final boolean mBigEndian;
    int mByteCount = 0;
    int mCharCount = 0;
    char mSurrogate = 0;

    public final /* bridge */ /* synthetic */ void close() throws IOException {
        super.close();
    }

    public final /* bridge */ /* synthetic */ int read() throws IOException {
        return super.read();
    }

    public UTF32Reader(IOContext iOContext, InputStream inputStream, byte[] bArr, int i, int i2, boolean z) {
        super(iOContext, inputStream, bArr, i, i2);
        this.mBigEndian = z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00e7, code lost:
        r1 = r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int read(char[] r8, int r9, int r10) throws java.io.IOException {
        /*
            r7 = this;
            r6 = 1114111(0x10ffff, float:1.561202E-39)
            r0 = -1
            byte[] r1 = r7._buffer
            if (r1 != 0) goto L_0x000a
            r10 = r0
        L_0x0009:
            return r10
        L_0x000a:
            if (r10 <= 0) goto L_0x0009
            if (r9 < 0) goto L_0x0013
            int r1 = r9 + r10
            int r2 = r8.length
            if (r1 <= r2) goto L_0x0016
        L_0x0013:
            r7.reportBounds(r8, r9, r10)
        L_0x0016:
            int r3 = r10 + r9
            char r1 = r7.mSurrogate
            if (r1 == 0) goto L_0x00a3
            int r2 = r9 + 1
            char r0 = r7.mSurrogate
            r8[r9] = r0
            r0 = 0
            r7.mSurrogate = r0
        L_0x0025:
            if (r2 >= r3) goto L_0x00e7
            int r0 = r7._ptr
            boolean r1 = r7.mBigEndian
            if (r1 == 0) goto L_0x00b4
            byte[] r1 = r7._buffer
            byte r1 = r1[r0]
            int r1 = r1 << 24
            byte[] r4 = r7._buffer
            int r5 = r0 + 1
            byte r4 = r4[r5]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << 16
            r1 = r1 | r4
            byte[] r4 = r7._buffer
            int r5 = r0 + 2
            byte r4 = r4[r5]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << 8
            r1 = r1 | r4
            byte[] r4 = r7._buffer
            int r0 = r0 + 3
            byte r0 = r4[r0]
            r0 = r0 & 255(0xff, float:3.57E-43)
            r0 = r0 | r1
        L_0x0052:
            int r1 = r7._ptr
            int r1 = r1 + 4
            r7._ptr = r1
            r1 = 65535(0xffff, float:9.1834E-41)
            if (r0 <= r1) goto L_0x00db
            if (r0 <= r6) goto L_0x0081
            int r1 = r2 - r9
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "(above "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = java.lang.Integer.toHexString(r6)
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = ") "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            r7.reportInvalid(r0, r1, r4)
        L_0x0081:
            r1 = 65536(0x10000, float:9.18355E-41)
            int r0 = r0 - r1
            int r1 = r2 + 1
            r4 = 55296(0xd800, float:7.7486E-41)
            int r5 = r0 >> 10
            int r4 = r4 + r5
            char r4 = (char) r4
            r8[r2] = r4
            r2 = 56320(0xdc00, float:7.8921E-41)
            r0 = r0 & 1023(0x3ff, float:1.434E-42)
            r0 = r0 | r2
            if (r1 < r3) goto L_0x00dc
            char r0 = (char) r0
            r7.mSurrogate = r0
        L_0x009a:
            int r10 = r1 - r9
            int r0 = r7.mCharCount
            int r0 = r0 + r10
            r7.mCharCount = r0
            goto L_0x0009
        L_0x00a3:
            int r1 = r7._length
            int r2 = r7._ptr
            int r1 = r1 - r2
            r2 = 4
            if (r1 >= r2) goto L_0x00e9
            boolean r1 = r7.loadMore(r1)
            if (r1 != 0) goto L_0x00e9
            r10 = r0
            goto L_0x0009
        L_0x00b4:
            byte[] r1 = r7._buffer
            byte r1 = r1[r0]
            r1 = r1 & 255(0xff, float:3.57E-43)
            byte[] r4 = r7._buffer
            int r5 = r0 + 1
            byte r4 = r4[r5]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << 8
            r1 = r1 | r4
            byte[] r4 = r7._buffer
            int r5 = r0 + 2
            byte r4 = r4[r5]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << 16
            r1 = r1 | r4
            byte[] r4 = r7._buffer
            int r0 = r0 + 3
            byte r0 = r4[r0]
            int r0 = r0 << 24
            r0 = r0 | r1
            goto L_0x0052
        L_0x00db:
            r1 = r2
        L_0x00dc:
            int r2 = r1 + 1
            char r0 = (char) r0
            r8[r1] = r0
            int r0 = r7._ptr
            int r1 = r7._length
            if (r0 < r1) goto L_0x0025
        L_0x00e7:
            r1 = r2
            goto L_0x009a
        L_0x00e9:
            r2 = r9
            goto L_0x0025
        */
        throw new UnsupportedOperationException("Method not decompiled: org.codehaus.jackson.io.UTF32Reader.read(char[], int, int):int");
    }

    private void reportUnexpectedEOF(int i, int i2) throws IOException {
        throw new CharConversionException("Unexpected EOF in the middle of a 4-byte UTF-32 char: got " + i + ", needed " + i2 + ", at char #" + this.mCharCount + ", byte #" + (this.mByteCount + i) + ")");
    }

    private void reportInvalid(int i, int i2, String str) throws IOException {
        throw new CharConversionException("Invalid UTF-32 character 0x" + Integer.toHexString(i) + str + " at char #" + (this.mCharCount + i2) + ", byte #" + ((this.mByteCount + this._ptr) - 1) + ")");
    }

    private boolean loadMore(int i) throws IOException {
        this.mByteCount += this._length - i;
        if (i > 0) {
            if (this._ptr > 0) {
                for (int i2 = 0; i2 < i; i2++) {
                    this._buffer[i2] = this._buffer[this._ptr + i2];
                }
                this._ptr = 0;
            }
            this._length = i;
        } else {
            this._ptr = 0;
            int read = this._in.read(this._buffer);
            if (read <= 0) {
                this._length = 0;
                if (read < 0) {
                    freeBuffers();
                    return false;
                }
                reportStrangeStream();
            }
            this._length = read;
        }
        while (this._length < 4) {
            int read2 = this._in.read(this._buffer, this._length, this._buffer.length - this._length);
            if (read2 <= 0) {
                if (read2 < 0) {
                    freeBuffers();
                    reportUnexpectedEOF(this._length, 4);
                }
                reportStrangeStream();
            }
            this._length = read2 + this._length;
        }
        return true;
    }
}
