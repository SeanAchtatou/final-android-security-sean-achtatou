package sojgp.zdga.pwrjbzetltzu;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class Test extends BroadcastReceiver {
    private static final String TAG = "BootReceiver";

    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, MyService.class));
    }
}
