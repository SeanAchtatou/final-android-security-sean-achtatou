package android.support.v4.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.ViewGroup;

public class b extends ViewGroup.MarginLayoutParams {
    public int a = 0;
    float b;
    boolean c;
    boolean d;

    public b(int i, int i2) {
        super(i, i2);
    }

    public b(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, DrawerLayout.a);
        this.a = obtainStyledAttributes.getInt(0, 0);
        obtainStyledAttributes.recycle();
    }

    public b(b bVar) {
        super((ViewGroup.MarginLayoutParams) bVar);
        this.a = bVar.a;
    }

    public b(ViewGroup.LayoutParams layoutParams) {
        super(layoutParams);
    }

    public b(ViewGroup.MarginLayoutParams marginLayoutParams) {
        super(marginLayoutParams);
    }
}
