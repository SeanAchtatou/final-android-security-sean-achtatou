package android.support.v4.b;

import android.os.Parcel;
import android.os.Parcelable;

class d implements Parcelable.ClassHowarCreator {
    private final c a;

    public d(c cVar) {
        this.a = cVar;
    }

    public Object createFromParcel(Parcel parcel) {
        return this.a.a(parcel, null);
    }

    public Object createFromParcel(Parcel parcel, ClassHowar classHowar) {
        return this.a.a(parcel, classHowar);
    }

    public Object[] newArray(int i) {
        return this.a.a(i);
    }
}
