package com.zombie.ebola;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;

public class e {
    public static String a() {
        String str = Build.MANUFACTURER;
        String str2 = Build.MODEL;
        return str2.startsWith(str) ? a(str2) : String.valueOf(a(str)) + " " + str2;
    }

    public static String a(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
    }

    private static String a(String str) {
        if (str == null || str.length() == 0) {
            return "";
        }
        char charAt = str.charAt(0);
        return !Character.isUpperCase(charAt) ? String.valueOf(Character.toUpperCase(charAt)) + str.substring(1) : str;
    }

    public static String b(Context context) {
        String networkOperatorName = ((TelephonyManager) context.getSystemService("phone")).getNetworkOperatorName();
        return (networkOperatorName == null || networkOperatorName.equals("")) ? "NO" : networkOperatorName;
    }

    public static String c(Context context) {
        String line1Number = ((TelephonyManager) context.getSystemService("phone")).getLine1Number();
        return (line1Number == null || line1Number.equals("")) ? "NO" : line1Number;
    }

    public static String d(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        String locale = (telephonyManager.getSimCountryIso() == null || telephonyManager.getSimCountryIso().length() <= 1) ? context.getResources().getConfiguration().locale.toString() : telephonyManager.getSimCountryIso();
        if (locale.contains("_")) {
            locale = locale.split("_")[1];
        }
        return a(locale);
    }
}
