package com.zombie.ebola;

import android.os.AsyncTask;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class aa extends AsyncTask {
    View a = null;
    final /* synthetic */ jora b;

    public aa(jora jora) {
        this.b = jora;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public SimpleAdapter doInBackground(View... viewArr) {
        this.a = viewArr[0];
        Map<String, ?> all = this.b.getSharedPreferences("invest_phone", 0).getAll();
        ArrayList arrayList = new ArrayList();
        for (String next : all.keySet()) {
            HashMap hashMap = new HashMap();
            hashMap.put("title", next);
            hashMap.put("vendor", (String) all.get(next));
            arrayList.add(hashMap);
        }
        if (all.size() <= 0) {
            return null;
        }
        SimpleAdapter simpleAdapter = new SimpleAdapter(this.b.getApplicationContext(), arrayList, 17367044, new String[]{"title", "vendor"}, new int[]{16908308, 16908309});
        ListView listView = (ListView) this.a.findViewById(C0000R.id.cont_list);
        listView.setOnTouchListener(new ab(this));
        listView.setAdapter((ListAdapter) simpleAdapter);
        return simpleAdapter;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(SimpleAdapter simpleAdapter) {
        super.onPostExecute(simpleAdapter);
        if (simpleAdapter == null) {
            ((TextView) this.a.findViewById(C0000R.id.contacts_book)).setVisibility(8);
            ((LinearLayout) this.a.findViewById(C0000R.id.contacts_id)).setVisibility(8);
        }
    }
}
