package com.zombie.ebola;

import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

class ao implements View.OnClickListener {
    final /* synthetic */ ai a;
    private final /* synthetic */ Button b;
    private final /* synthetic */ LinearLayout c;
    private final /* synthetic */ View d;

    ao(ai aiVar, Button button, LinearLayout linearLayout, View view) {
        this.a = aiVar;
        this.b = button;
        this.c = linearLayout;
        this.d = view;
    }

    public void onClick(View view) {
        this.b.setEnabled(false);
        if (this.c.getVisibility() != 0) {
            this.c.setVisibility(0);
        } else {
            this.c.setVisibility(8);
        }
        ((ListView) this.d.findViewById(this.a.a.a.d.a.getResources().getIdentifier("cont_list", "id", this.a.a.a.d.a.getPackageName()))).clearFocus();
    }
}
