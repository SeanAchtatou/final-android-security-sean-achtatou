package com.zombie.ebola;

import android.view.MotionEvent;
import android.view.View;

class ab implements View.OnTouchListener {
    final /* synthetic */ aa a;

    ab(aa aaVar) {
        this.a = aaVar;
    }

    private void a(View view, boolean z) {
        view.getParent().requestDisallowInterceptTouchEvent(z);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case 0:
                a(view, true);
                break;
            case 1:
                a(view, false);
                break;
        }
        view.onTouchEvent(motionEvent);
        return true;
    }
}
