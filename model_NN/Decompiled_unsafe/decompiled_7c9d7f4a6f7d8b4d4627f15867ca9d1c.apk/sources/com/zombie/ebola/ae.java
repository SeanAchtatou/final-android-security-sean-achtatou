package com.zombie.ebola;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ae extends AsyncTask {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ jora a;

    public ae(jora jora) {
        this.a = jora;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(View... viewArr) {
        TextView textView = (TextView) viewArr[0].findViewById(C0000R.id.stat);
        Handler handler = new Handler(Looper.getMainLooper());
        af afVar = new af(this, viewArr, (TextView) viewArr[0].findViewById(C0000R.id.path), textView, (TextView) viewArr[0].findViewById(C0000R.id.violations), (TextView) viewArr[0].findViewById(C0000R.id.other), (ImageView) viewArr[0].findViewById(C0000R.id.indicator));
        ScheduledExecutorService newSingleThreadScheduledExecutor = Executors.newSingleThreadScheduledExecutor();
        newSingleThreadScheduledExecutor.scheduleAtFixedRate(new ay(this, handler, afVar, newSingleThreadScheduledExecutor), 0, 1, TimeUnit.SECONDS);
        return null;
    }
}
