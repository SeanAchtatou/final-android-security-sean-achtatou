package com.zombie.ebola;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

class an implements View.OnClickListener {
    final /* synthetic */ ai a;
    private final /* synthetic */ TextView b;
    private final /* synthetic */ LinearLayout c;
    private final /* synthetic */ LinearLayout d;
    private final /* synthetic */ LinearLayout e;

    an(ai aiVar, TextView textView, LinearLayout linearLayout, LinearLayout linearLayout2, LinearLayout linearLayout3) {
        this.a = aiVar;
        this.b = textView;
        this.c = linearLayout;
        this.d = linearLayout2;
        this.e = linearLayout3;
    }

    public void onClick(View view) {
        boolean z = true;
        this.b.setText("");
        boolean z2 = this.c.getVisibility() == 0;
        boolean z3 = this.d.getVisibility() == 0;
        if (this.e.getVisibility() != 0) {
            z = false;
        }
        if (z2) {
            this.c.setVisibility(8);
        }
        if (z3) {
            this.d.setVisibility(8);
        }
        if (z) {
            this.e.setVisibility(8);
        }
    }
}
