package com.zombie.ebola;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

class ag implements View.OnClickListener {
    final /* synthetic */ af a;
    private final /* synthetic */ Button b;
    private final /* synthetic */ View c;
    private final /* synthetic */ View d;

    ag(af afVar, Button button, View view, View view2) {
        this.a = afVar;
        this.b = button;
        this.c = view;
        this.d = view2;
    }

    public void onClick(View view) {
        this.b.setEnabled(false);
        View inflate = jora.d.inflate((int) C0000R.layout.doc_2, (ViewGroup) null);
        FrameLayout frameLayout = (FrameLayout) this.c.findViewById(this.a.d.a.getResources().getIdentifier("framer", "id", this.a.d.a.getPackageName()));
        frameLayout.removeAllViews();
        frameLayout.addView(inflate);
        this.a.a(1, inflate, this.d);
    }
}
