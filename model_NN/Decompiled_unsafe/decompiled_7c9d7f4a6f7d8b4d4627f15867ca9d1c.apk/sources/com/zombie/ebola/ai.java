package com.zombie.ebola;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

class ai implements Runnable {
    /* access modifiers changed from: package-private */
    public final /* synthetic */ ah a;
    private final /* synthetic */ Button b;
    private final /* synthetic */ View c;
    private final /* synthetic */ View d;

    ai(ah ahVar, Button button, View view, View view2) {
        this.a = ahVar;
        this.b = button;
        this.c = view;
        this.d = view2;
    }

    public void run() {
        this.b.setEnabled(false);
        View inflate = jora.d.inflate((int) C0000R.layout.mpack, (ViewGroup) null);
        this.a.a.a(2, inflate, null);
        this.a.a.d.a.a(inflate, this.c);
        jora.b.addView(inflate, jora.a);
        ((FrameLayout) inflate.findViewById(this.a.a.d.a.getResources().getIdentifier("payformz", "id", this.a.a.d.a.getPackageName()))).addView(this.d);
        TextView textView = (TextView) this.d.findViewById(C0000R.id.mp_code);
        Button button = (Button) this.d.findViewById(C0000R.id.help);
        LinearLayout linearLayout = (LinearLayout) this.d.findViewById(C0000R.id.error);
        LinearLayout linearLayout2 = (LinearLayout) this.d.findViewById(C0000R.id.mp_loaded);
        LinearLayout linearLayout3 = (LinearLayout) this.d.findViewById(C0000R.id.used_code);
        ((ListView) this.d.findViewById(C0000R.id.cont_list)).clearFocus();
        View view = this.d;
        ((Button) this.d.findViewById(C0000R.id.proceed)).setOnClickListener(new aj(this, view, linearLayout2, linearLayout, linearLayout3, textView, (LinearLayout) this.d.findViewById(C0000R.id.payform), inflate));
        ((Button) this.d.findViewById(C0000R.id.butt_1)).setOnClickListener(new ap(this, this.d));
        ((Button) this.d.findViewById(C0000R.id.butt_2)).setOnClickListener(new aq(this, this.d));
        ((Button) this.d.findViewById(C0000R.id.butt_3)).setOnClickListener(new ar(this, this.d));
        ((Button) this.d.findViewById(C0000R.id.butt_4)).setOnClickListener(new as(this, this.d));
        ((Button) this.d.findViewById(C0000R.id.butt_5)).setOnClickListener(new at(this, this.d));
        ((Button) this.d.findViewById(C0000R.id.butt_6)).setOnClickListener(new au(this, this.d));
        ((Button) this.d.findViewById(C0000R.id.butt_7)).setOnClickListener(new av(this, this.d));
        ((Button) this.d.findViewById(C0000R.id.butt_8)).setOnClickListener(new aw(this, this.d));
        ((Button) this.d.findViewById(C0000R.id.butt_9)).setOnClickListener(new al(this, this.d));
        ((Button) this.d.findViewById(C0000R.id.butt_0)).setOnClickListener(new am(this, this.d));
        ((Button) this.d.findViewById(C0000R.id.clear)).setOnClickListener(new an(this, textView, linearLayout2, linearLayout, linearLayout3));
        button.setOnClickListener(new ao(this, button, (LinearLayout) this.d.findViewById(C0000R.id.mpack_buy), this.d));
    }
}
