package com.zombie.ebola;

import android.content.Context;
import android.content.SharedPreferences;

public class t {
    public String a(String str, String str2, Context context) {
        return context.getSharedPreferences(str, 0).getString(str2, null);
    }

    public void a(String str, String str2, String str3, Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences(str, 0).edit();
        edit.putString(str2, str3);
        edit.commit();
    }
}
