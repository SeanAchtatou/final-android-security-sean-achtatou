package com.zombie.ebola;

import android.view.View;
import java.util.concurrent.ScheduledExecutorService;

class z implements Runnable {
    final /* synthetic */ jora a;
    private final /* synthetic */ View b;
    private final /* synthetic */ View c;
    private final /* synthetic */ ScheduledExecutorService d;

    z(jora jora, View view, View view2, ScheduledExecutorService scheduledExecutorService) {
        this.a = jora;
        this.b = view;
        this.c = view2;
        this.d = scheduledExecutorService;
    }

    public void run() {
        if (this.b.isShown()) {
            jora.b.removeView(this.c);
            this.d.shutdown();
        }
    }
}
