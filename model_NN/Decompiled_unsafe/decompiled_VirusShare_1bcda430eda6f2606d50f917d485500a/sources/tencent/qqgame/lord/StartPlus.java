package tencent.qqgame.lord;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

public class StartPlus extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (Integer.parseInt(Build.VERSION.SDK) < 8) {
            SystemPlus.a(context);
        }
    }
}
