package com.tencent.qqgame.lord.ui.control;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Message;
import android.widget.EditText;
import com.tencent.qqgame.hall.common.AnimationPlayerV1;
import com.tencent.qqgame.hall.common.Report;
import com.tencent.qqgame.lord.common.StringItem;
import com.tencent.qqgame.lord.logic.LordLogic;
import tencent.qqgame.lord.R;

public class ChatDialog {
    private static final int LINE_HEIGHT = 24;
    public static EditText chatInput = null;
    private static String[] chatMatchText = null;
    public static String[] chatText = null;
    private Bitmap BufferedChatPanel = null;
    private StringItem[] chatItems = null;
    private int curSelectedIndex;
    private final LordLogic logic;
    private int offset;
    private int panelX;
    private int panelY;
    private AnimationPlayerV1 playerButton;
    private AnimationPlayerV1 playerPanel;
    private Rect rChatText = new Rect(82, 105, 362, 225);
    private RectF rScrollBar;
    private int sendX = 286;
    private int sendY = 65;
    private int touchDownY;

    public ChatDialog(LordLogic lordLogic) {
        this.logic = lordLogic;
        Resources resources = lordLogic.res;
        if (chatText == null) {
            chatText = new String[]{resources.getString(R.string.game_chat1), resources.getString(R.string.game_chat2), resources.getString(R.string.game_chat3), resources.getString(R.string.game_chat4), resources.getString(R.string.game_chat5), resources.getString(R.string.game_chat6), resources.getString(R.string.game_chat7), resources.getString(R.string.game_chat8), resources.getString(R.string.game_chat9), resources.getString(R.string.game_chat10), resources.getString(R.string.game_chat11), resources.getString(R.string.game_chat12)};
        }
        if (chatMatchText == null) {
            chatMatchText = new String[]{resources.getString(R.string.game_match_chat1), resources.getString(R.string.game_match_chat2), resources.getString(R.string.game_match_chat3), resources.getString(R.string.game_match_chat4), resources.getString(R.string.game_match_chat5), resources.getString(R.string.game_match_chat6), resources.getString(R.string.game_match_chat7), resources.getString(R.string.game_match_chat8), resources.getString(R.string.game_match_chat9), resources.getString(R.string.game_match_chat10), resources.getString(R.string.game_match_chat11), resources.getString(R.string.game_match_chat12)};
        }
    }

    public Bitmap createBufferedBitmap(AnimationPlayerV1 animationPlayerV1, int i, int i2, int i3) {
        Canvas canvas = new Canvas();
        Bitmap createBitmap = Bitmap.createBitmap(animationPlayerV1.getFrameWidth(i), animationPlayerV1.getFrameWidth(i), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(createBitmap);
        animationPlayerV1.drawFrame(canvas, i, i2, i3);
        return createBitmap;
    }

    public boolean doTouchEvent(int i, int i2, int i3) {
        if (!this.rChatText.contains(i2, i3)) {
            return true;
        }
        if (i == 0) {
            this.touchDownY = i3;
        } else if (i == 1) {
            this.curSelectedIndex = ((i3 - this.rChatText.top) - this.offset) / LINE_HEIGHT;
            if (this.curSelectedIndex >= 0 && this.curSelectedIndex < chatText.length) {
                Message obtainMessage = this.logic.handle.obtainMessage(112);
                obtainMessage.arg1 = 0;
                obtainMessage.obj = chatText[this.curSelectedIndex];
                this.logic.handle.sendMessage(obtainMessage);
            }
        } else if (i == 2) {
            int i4 = ((i3 - this.touchDownY) * 3) / 4;
            int i5 = (-(chatText.length - 5)) * LINE_HEIGHT;
            this.offset = (i4 - (i4 % LINE_HEIGHT)) + this.offset;
            if (this.offset >= 0) {
                this.offset = 0;
                return true;
            } else if (this.offset <= i5) {
                this.offset = i5;
                return true;
            }
        }
        return true;
    }

    public void draw(Canvas canvas, Paint paint) {
        paint.setTextSize(16.0f);
        if (this.BufferedChatPanel != null) {
            canvas.drawBitmap(this.BufferedChatPanel, (float) this.panelX, (float) this.panelY, paint);
            this.logic.ctlChatSend.draw(canvas, paint);
            this.logic.ctlChat.draw(canvas, paint);
            canvas.save();
            canvas.clipRect(this.rChatText);
            paint.setColor(-1);
            if (this.chatItems != null) {
                int i = this.rChatText.left;
                int textSize = this.rChatText.top + ((((int) paint.getTextSize()) + LINE_HEIGHT) / 2) + this.offset;
                for (int i2 = 0; i2 < chatText.length; i2++) {
                    this.chatItems[i2].setLocation(i, textSize);
                    this.chatItems[i2].draw(canvas, paint, this.rChatText);
                    textSize += LINE_HEIGHT;
                }
            }
            canvas.restore();
            if (this.rScrollBar != null) {
                paint.setColor(-256);
                float height = ((float) this.rChatText.top) + (((float) ((-this.offset) / LINE_HEIGHT)) * (((float) this.rChatText.height()) / ((float) chatText.length)));
                this.rScrollBar.set(this.rScrollBar.left, height, this.rScrollBar.right, this.rScrollBar.height() + height);
                canvas.drawRoundRect(this.rScrollBar, 3.0f, 3.0f, paint);
            }
        }
    }

    public Rect getBound() {
        return this.playerPanel.getFrameBound(0);
    }

    public String getCurSelectedText() {
        return chatInput.getText().toString();
    }

    public void initChatPanel(LordLogic lordLogic, AnimationPlayerV1 animationPlayerV1, AnimationPlayerV1 animationPlayerV12) {
        this.playerPanel = animationPlayerV1;
        this.playerButton = animationPlayerV12;
        this.panelX = animationPlayerV1.getFrameBound(0).left;
        this.panelY = animationPlayerV1.getFrameBound(0).top;
        this.offset = 0;
        if (this.chatItems == null) {
            this.chatItems = new StringItem[chatText.length];
            for (int i = 0; i < chatText.length; i++) {
                this.chatItems[i] = new StringItem(chatText[i]);
            }
            this.chatItems[5].setAutoScroll(true);
            this.chatItems[5].setPivot((this.rChatText.left + this.rChatText.width()) - 30);
        }
        this.rScrollBar = new RectF((float) (this.rChatText.right + 2), (float) this.rChatText.top, (float) (this.rChatText.right + 6), ((5.0f / ((float) this.chatItems.length)) * ((float) this.rChatText.height())) + ((float) this.rChatText.top));
        lordLogic.ctlChatSend.setLocation(this.sendX, this.sendY, this.sendX + animationPlayerV12.getFrameWidth(38), this.sendY + animationPlayerV12.getFrameHeight(38));
        lordLogic.ctlChatText.setLocation(this.rChatText.left, this.rChatText.top, this.rChatText.right, this.rChatText.bottom);
        if (this.BufferedChatPanel == null) {
            this.BufferedChatPanel = createBufferedBitmap(this.playerPanel, 0, -this.panelX, -this.panelY);
        }
        Message obtainMessage = lordLogic.handle.obtainMessage(112);
        obtainMessage.arg1 = 0;
        obtainMessage.obj = chatText[0];
        lordLogic.handle.sendMessage(obtainMessage);
    }

    public void playChatSound(String str) {
        if (str != null) {
            int i = -1;
            String[] strArr = chatMatchText;
            int i2 = 0;
            while (true) {
                if (i2 >= strArr.length) {
                    break;
                } else if (str.indexOf(strArr[i2]) >= 0) {
                    str.matches(strArr[i2]);
                    i = i2;
                    break;
                } else {
                    i2++;
                }
            }
            if (i >= 0 && this.logic.soundPlayer != null) {
                this.logic.soundPlayer.playChatSound(i + R.raw.chat1000);
            }
        }
    }

    public void remove() {
        if (this.BufferedChatPanel != null) {
            if (!this.BufferedChatPanel.isRecycled()) {
                this.BufferedChatPanel.recycle();
            }
            this.BufferedChatPanel = null;
        }
        Message obtainMessage = this.logic.handle.obtainMessage(112);
        obtainMessage.arg1 = 4;
        this.logic.handle.sendMessage(obtainMessage);
        this.logic.ctlChat.setStatus((byte) 0);
        this.logic.ctlChat.isFixed = false;
    }

    public void report() {
        int[] iArr = Report.chatTextHitNum;
        int i = this.curSelectedIndex;
        iArr[i] = iArr[i] + 1;
    }
}
