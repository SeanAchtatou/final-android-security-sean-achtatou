package com.tencent.qqgame.lord.common;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.MotionEvent;
import com.tencent.qqgame.common.Player;
import com.tencent.qqgame.common.Table;
import com.tencent.qqgame.common.Util;
import com.tencent.qqgame.hall.common.AActivity;
import com.tencent.qqgame.hall.common.IGameView;
import com.tencent.qqgame.hall.common.Tools;
import com.tencent.qqgame.hall.protocol.IReceiveGameMessageHandle;
import com.tencent.qqgame.hall.protocol.ISendGameMessageHandle;
import com.tencent.qqgame.lord.protocol.LordSendMessageHandle;
import com.tencent.qqgame.lord.ui.LordView;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Vector;
import tencent.qqgame.lord.R;

public abstract class AGameLogic {
    public static final int KEY_CONTROL_PARENT = 1;
    public static final byte MAX_THINK_TIME = 30;
    public static final byte STATUS_EXIT_GAME = -4;
    public static final byte STATUS_JUST_ENTER = 0;
    public static final byte STATUS_PLAY_ING = -2;
    public static final byte STATUS_ROUND_END = -3;
    public static long uin;
    public ControlGroup cgTipDialog = null;
    public final Vector<GameMessage> chatMessageList = new Vector<>();
    public byte continueOutTimes = 0;
    public byte continueReplayTimes = 0;
    public String continueTipMessage = null;
    public final Vector<ControlGroup> controlGroups = new Vector<>();
    public byte curStatus;
    public byte forceTrusteeTimes = 0;
    public final Activity gameActivity;
    public int gameTimeOut = 0;
    public final Handler handle;
    public boolean isCanViewCards = false;
    public boolean isChangingDesk = false;
    public boolean isExitingGame = false;
    protected boolean isPlayerCalled = false;
    public boolean isTrusteeship = false;
    public boolean isViewGame = false;
    public String locationInfo;
    public final ControlGroup mainControls;
    public Player me;
    public final Vector<Message> messageList = new Vector<>();
    public IReceiveGameMessageHandle receiveHandle;
    public final Resources res;
    public byte roomType = -1;
    protected LordSendMessageHandle sendHandle = null;
    public Table table;
    public byte talkWho = -1;
    public byte thinkTime = -1;
    public byte timerTipShow = 1;
    public String tipMessage = null;
    protected AGameView view;

    public AGameLogic(Activity activity, Handler handler, ISendGameMessageHandle iSendGameMessageHandle, Table table2, Player player, long j, boolean z) {
        this.sendHandle = new LordSendMessageHandle(iSendGameMessageHandle);
        this.mainControls = new ControlGroup("cgMain", new Rect(0, 0, LordView.SCREEN_WIDTH, LordView.SCREEN_HEIGHT));
        this.controlGroups.add(this.mainControls);
        this.gameActivity = activity;
        this.handle = handler;
        this.res = activity.getResources();
        this.isViewGame = z;
        uin = j;
        setGameInfo(table2, player);
    }

    /* access modifiers changed from: protected */
    public final void deletePlayerChatMessages(long j) {
        GameMessage gameMessage;
        while (true) {
            int size = this.chatMessageList.size();
            byte b = 0;
            while (true) {
                if (b >= size) {
                    gameMessage = null;
                    break;
                } else if (this.chatMessageList.elementAt(b).uid == j) {
                    gameMessage = this.chatMessageList.elementAt(b);
                    break;
                } else {
                    b = (byte) (b + 1);
                }
            }
            if (gameMessage != null) {
                this.chatMessageList.remove(gameMessage);
            } else {
                return;
            }
        }
    }

    public void disbandGame(boolean z) {
        if (z) {
            setTipMessage(this.res.getString(R.string.game_tips_disbandGame_system));
        } else {
            setTipMessage(this.res.getString(R.string.game_tips_disbandGame_dismiss));
        }
        Util.threadSleep(IGameView.MSG_2GAME_DIS_CONNECTED);
        if (z) {
            exitGame(true);
            return;
        }
        restartGame();
        setAllPlayerState(2);
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final void doHandleMessage() {
        boolean z;
        Player player;
        synchronized (this.messageList) {
            while (this.messageList.size() > 0) {
                Message elementAt = this.messageList.elementAt(0);
                switch (elementAt.what) {
                    case IGameView.MSG_2GAME_CHANGEDESK_FAULT:
                        resetThinkTime((byte) 10);
                        this.isChangingDesk = false;
                        z = true;
                        break;
                    case IGameView.MSG_2GAME_CHANGEDESK_SUCC:
                        if (!this.isChangingDesk && this.curStatus == -2) {
                            Tools.debug("CHANGEDESK_SUCC, but game is started!");
                            z = true;
                            break;
                        } else {
                            this.isChangingDesk = false;
                            Table table2 = (Table) elementAt.obj;
                            if (table2 != null) {
                                byte b = 0;
                                while (true) {
                                    if (b >= table2.playerList.size()) {
                                        player = null;
                                    } else if (table2.playerList.elementAt(b).uid == uin) {
                                        player = table2.playerList.elementAt(b);
                                    } else {
                                        b = (byte) (b + 1);
                                    }
                                }
                                setGameInfo(table2, player);
                                restartGame();
                            }
                            z = true;
                            break;
                        }
                        break;
                    case IGameView.MSG_2GAME_MENU_ITEM_SELECTED:
                    case IGameView.MSG_2GAME_EXIT_ACTIVITY:
                    case IGameView.MSG_2GAME_CHAT_EDITVIEW:
                    default:
                        z = true;
                        break;
                    case IGameView.MSG_2GAME_PARSE_SERVER_MSG:
                        this.receiveHandle.parseGameMessage((short) elementAt.arg1, (byte[]) elementAt.obj);
                        z = false;
                        break;
                    case IGameView.MSG_2GAME_PROGRESSDIALOG_CANCELED:
                        if (this.continueReplayTimes <= 0) {
                            if (this.isChangingDesk || this.isExitingGame) {
                                this.handle.sendEmptyMessage(103);
                                z = true;
                                break;
                            }
                            z = true;
                            break;
                        } else {
                            this.handle.sendEmptyMessage(107);
                            z = true;
                            break;
                        }
                    case IGameView.MSG_2GAME_FOREGROUND:
                        this.view.gameSurface.setForeground(((Boolean) elementAt.obj).booleanValue());
                        z = true;
                        break;
                }
                if (z) {
                    handleMessage(elementAt);
                }
                this.messageList.remove(elementAt);
            }
        }
    }

    public abstract boolean doKeyDown(int i, KeyEvent keyEvent);

    public abstract boolean doTouchEvent(MotionEvent motionEvent, int i, int i2);

    public abstract void exitGame(boolean z);

    public final GameMessage getLastPlayerChatMessage(Player player) {
        int size = this.chatMessageList.size();
        for (int i = 0; i < size; i++) {
            GameMessage elementAt = this.chatMessageList.elementAt(i);
            if (player.uid == elementAt.uid && elementAt.timerShow > 0) {
                return elementAt;
            }
        }
        return null;
    }

    public final void handUp() {
        this.me.status = 3;
        resetThinkTime((byte) -1);
        this.sendHandle.handUp();
    }

    /* access modifiers changed from: protected */
    public abstract void handleMessage(Message message);

    public void newRound(byte b) {
        if (b == 28) {
            Tools.debug("newRound! type=" + ((int) b));
            setTipMessage(this.res.getString(R.string.game_tips_nocall_restart));
            setAllPlayerState(2);
            restartGame();
            Util.threadSleep(IGameView.MSG_2GAME_DIS_CONNECTED);
        }
    }

    public final void playerHandUp(byte b, long j) {
        Player playerByAbsoluteSeatId = Table.getPlayerByAbsoluteSeatId(b, this.table.playerList);
        if (playerByAbsoluteSeatId != null && j == playerByAbsoluteSeatId.uid) {
            if (this.curStatus != -2) {
                playerByAbsoluteSeatId.status = 3;
            }
            if (j == this.me.uid) {
                resetThinkTime((byte) -1);
            }
            StringBuffer stringBuffer = new StringBuffer(this.res.getString(R.string.game_tips_player_handup));
            Util.replace(stringBuffer, "{0}", playerByAbsoluteSeatId.nickName);
            setTipMessage(stringBuffer.toString());
        }
        if (this.me.status == 2 && this.table.playerList.size() == 3) {
            byte b2 = 0;
            byte b3 = 0;
            while (b2 < 3) {
                byte b4 = this.table.playerList.elementAt(b2).status == 3 ? (byte) (b3 + 1) : b3;
                b2 = (byte) (b2 + 1);
                b3 = b4;
            }
            if (b3 == 2 && this.thinkTime > 10) {
                resetThinkTime((byte) 10);
            }
        }
    }

    public final void playerLeave(byte b, long j) {
        Player playerByAbsoluteSeatId = Table.getPlayerByAbsoluteSeatId(b, this.table.playerList);
        if (playerByAbsoluteSeatId != null && j == playerByAbsoluteSeatId.uid) {
            StringBuffer stringBuffer = this.curStatus == -2 ? new StringBuffer(this.res.getString(R.string.game_tips_player_runaway)) : new StringBuffer(this.res.getString(R.string.game_tips_player_quit));
            Util.replace(stringBuffer, "{0}", playerByAbsoluteSeatId.nickName);
            setTipMessage(stringBuffer.toString());
            this.table.playerList.removeElement(playerByAbsoluteSeatId);
            if (this.curStatus == -2) {
                this.handle.sendEmptyMessage(101);
                Util.threadSleep(IGameView.MSG_2GAME_DIS_CONNECTED);
                restartGame();
                setAllPlayerState(2);
            }
            deletePlayerChatMessages(j);
            this.view.aniManager.playFogAni(playerByAbsoluteSeatId);
        }
    }

    public final void playerSitDown(Player player) {
        if (player != null) {
            this.table.playerList.add(player);
            for (byte b = 0; b < this.table.playerList.size(); b = (byte) (b + 1)) {
                this.table.playerList.elementAt(b).foreS = 0;
                this.table.playerList.elementAt(b).continuS = 0;
            }
            StringBuffer stringBuffer = new StringBuffer(this.res.getString(R.string.game_tips_player_sitdown));
            Util.replace(stringBuffer, "{0}", player.nickName);
            setTipMessage(stringBuffer.toString());
            if (this.curStatus == 0) {
                this.view.aniManager.playFogAni(player);
            }
        }
    }

    public final void playerStandUp(byte b, long j) {
        Player playerByAbsoluteSeatId = Table.getPlayerByAbsoluteSeatId(b, this.table.playerList);
        if (playerByAbsoluteSeatId != null && j == playerByAbsoluteSeatId.uid) {
            if (j != this.me.uid) {
                if (this.curStatus == 0) {
                    this.view.aniManager.playFogAni(playerByAbsoluteSeatId);
                    deletePlayerChatMessages(j);
                }
                new StringBuffer();
                StringBuffer stringBuffer = this.curStatus == -2 ? new StringBuffer(this.res.getString(R.string.game_tips_player_runaway)) : new StringBuffer(this.res.getString(R.string.game_tips_player_quit));
                Util.replace(stringBuffer, "{0}", playerByAbsoluteSeatId.nickName);
                setTipMessage(stringBuffer.toString());
                this.table.playerList.removeElement(playerByAbsoluteSeatId);
                if (this.curStatus == -2) {
                    if (AActivity.soundPlayer != null) {
                        AActivity.soundPlayer.stopSendCardSound();
                        AActivity.soundPlayer.playRunawaySound();
                    }
                    this.handle.sendEmptyMessage(101);
                    Util.threadSleep(IGameView.MSG_2GAME_DIS_CONNECTED);
                    restartGame();
                    setAllPlayerState(2);
                    this.handle.sendEmptyMessage(109);
                }
            } else if (this.isExitingGame) {
                this.handle.sendEmptyMessage(103);
            } else {
                this.handle.sendEmptyMessage(102);
            }
        }
    }

    /* access modifiers changed from: protected */
    public abstract void resetThinkTime(byte b);

    /* access modifiers changed from: protected */
    public abstract void restartGame();

    public void roomEvenKicked(long j, short s, byte[] bArr) {
        Player player;
        int i;
        long j2;
        StringBuffer stringBuffer = new StringBuffer();
        if (j == this.me.uid) {
            this.handle.sendEmptyMessage(101);
        }
        if (!this.isViewGame && j == this.me.uid) {
            if (s == 4) {
                try {
                    j2 = Util.convertUnsigned2Long(new DataInputStream(new ByteArrayInputStream(bArr)).readInt());
                } catch (IOException e) {
                    j2 = 0;
                }
                stringBuffer.append(this.res.getString(R.string.game_tips_tick_by_player));
                Util.replace(stringBuffer, "{0}", String.valueOf(j2));
            } else if (s == 6) {
                try {
                    i = new DataInputStream(new ByteArrayInputStream(new byte[]{bArr[4], bArr[5]})).readInt();
                } catch (IOException e2) {
                    i = 0;
                }
                switch (i) {
                    case 0:
                        stringBuffer.append(this.res.getString(R.string.game_tips_tick_by_sys1));
                        break;
                    case 1:
                        stringBuffer.append(this.res.getString(R.string.game_tips_tick_by_sys2));
                        break;
                    case 2:
                        stringBuffer.append(this.res.getString(R.string.game_tips_tick_by_sys3));
                        break;
                    case 3:
                        stringBuffer.append(this.res.getString(R.string.game_tips_tick_by_sys4));
                        break;
                    case IGameView.MSG_2GAME_DIS_CONNECTED:
                        stringBuffer.append(this.res.getString(R.string.game_tips_tick_by_sys5));
                        break;
                    case 1001:
                        stringBuffer.append(this.res.getString(R.string.game_tips_tick_by_sys6));
                        break;
                    default:
                        stringBuffer.append(this.res.getString(R.string.game_tips_tick_by_sys7));
                        break;
                }
                stringBuffer.append(this.res.getString(R.string.game_tips_tick_by_sys));
            }
            setTipMessage(stringBuffer.toString());
            Message obtainMessage = this.handle.obtainMessage(114);
            obtainMessage.obj = stringBuffer.toString();
            this.handle.sendMessage(obtainMessage);
        } else if (!this.isViewGame || !(j == uin || j == this.me.uid)) {
            stringBuffer.append(this.res.getString(R.string.game_tips_player_tick));
            Util.replace(stringBuffer, "{0}", String.valueOf(j));
            this.tipMessage = stringBuffer.toString();
        } else {
            Message obtainMessage2 = this.handle.obtainMessage(105);
            if (j == uin) {
                obtainMessage2.arg1 = R.string.game_view_game_me_tick;
            } else {
                obtainMessage2.arg1 = R.string.game_view_game_player_tick;
            }
            this.handle.sendMessage(obtainMessage2);
        }
        int size = this.table.playerList.size();
        int i2 = 0;
        while (true) {
            if (i2 >= size) {
                player = null;
            } else if (this.table.playerList.elementAt(i2).uid == j) {
                player = this.table.playerList.elementAt(i2);
            } else {
                i2++;
            }
        }
        if (player != null) {
            this.view.aniManager.playFogAni(player);
        }
        deletePlayerChatMessages(j);
    }

    public void roomEvenOffLine(byte b, long j) {
        Player playerByAbsoluteSeatId = Table.getPlayerByAbsoluteSeatId(b, this.table.playerList);
        if (playerByAbsoluteSeatId != null && j == playerByAbsoluteSeatId.uid) {
            playerByAbsoluteSeatId.status = 5;
            if (this.curStatus == -2) {
                StringBuffer stringBuffer = new StringBuffer(this.res.getString(R.string.game_tips_player_offline));
                Util.replace(stringBuffer, "{0}", playerByAbsoluteSeatId.nickName);
                setTipMessage(stringBuffer.toString());
            }
        }
    }

    public void roomEvenReline(long j) {
        if (this.curStatus == -2) {
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < this.table.playerList.size()) {
                    Player elementAt = this.table.playerList.elementAt(i2);
                    if (elementAt.uid == j) {
                        StringBuffer stringBuffer = new StringBuffer(this.res.getString(R.string.game_tips_player_reline));
                        Util.replace(stringBuffer, "{0}", elementAt.nickName);
                        setTipMessage(stringBuffer.toString());
                        elementAt.status = 4;
                        return;
                    }
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }
    }

    public void roomEvenResult() {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.table.playerList.size()) {
                Player elementAt = this.table.playerList.elementAt(i2);
                if (elementAt.status == 5) {
                    StringBuffer stringBuffer = new StringBuffer(this.res.getString(R.string.game_tips_player_runaway));
                    Util.replace(stringBuffer, "{0}", elementAt.nickName);
                    setTipMessage(stringBuffer.toString());
                    this.table.playerList.removeElementAt(i2);
                    if (this.isViewGame) {
                        if (elementAt.seatId == this.me.seatId) {
                            Message obtainMessage = this.handle.obtainMessage(105);
                            obtainMessage.arg1 = R.string.game_view_game_cancel;
                            this.handle.sendMessage(obtainMessage);
                            return;
                        }
                        return;
                    } else if (this.curStatus == -2) {
                        this.handle.sendEmptyMessage(101);
                        Util.threadSleep(1500);
                        restartGame();
                        setAllPlayerState(2);
                        this.handle.sendEmptyMessage(109);
                        return;
                    } else {
                        return;
                    }
                } else {
                    i = i2 + 1;
                }
            } else {
                return;
            }
        }
    }

    public void roomEventCancelViewGame(long j) {
        if (this.isViewGame && j == uin) {
            Message obtainMessage = this.handle.obtainMessage(105);
            obtainMessage.arg1 = R.string.game_view_game_cancel;
            this.handle.sendMessage(obtainMessage);
        }
    }

    public abstract void run(short s, int i);

    public abstract void runLast(short s, int i);

    public final void scoreChange(Player player) {
        if (player != null) {
            int size = this.table.playerList.size();
            for (byte b = 0; b < size; b = (byte) (b + 1)) {
                Player elementAt = this.table.playerList.elementAt(b);
                if (elementAt.uid == player.uid) {
                    elementAt.point = player.point;
                    elementAt.money = player.money;
                    elementAt.winP = player.winP;
                    elementAt.runP = player.runP;
                    if (this.curStatus != -2) {
                        elementAt.status = 2;
                        return;
                    }
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void setAllPlayerState(short s) {
        int size = this.table.playerList.size();
        for (byte b = 0; b < size; b = (byte) (b + 1)) {
            this.table.playerList.elementAt(b).status = s;
        }
    }

    public final void setBaseInfo(DataInputStream dataInputStream) {
        try {
            dataInputStream.skipBytes(11);
            this.roomType = dataInputStream.readByte();
        } catch (IOException e) {
        }
    }

    public final void setGameInfo(Table table2, Player player) {
        this.table = table2;
        this.me = player;
        StringBuffer stringBuffer = new StringBuffer();
        if (!(this.table == null || this.table.zoneName == null || this.table.roomName == null)) {
            stringBuffer.append(this.res.getString(R.string.game_info_location));
            Util.replace(stringBuffer, "{0}", this.table.zoneName);
            Util.replace(stringBuffer, "{1}", this.table.roomName);
            Util.replace(stringBuffer, "{2}", ((this.table.tableId % 100) + 1) + "");
        }
        this.locationInfo = stringBuffer.toString();
        if (this.view != null) {
            this.view.initLocationInfoImg();
        }
    }

    public void setTipMessage(String str) {
        this.timerTipShow = 3;
        this.tipMessage = str;
    }

    public final void showNotification(String str) {
        Message obtainMessage = this.handle.obtainMessage(104);
        obtainMessage.obj = str;
        this.handle.sendMessage(obtainMessage);
    }
}
