package com.tencent.qqgame.lord.common;

import android.content.Context;
import android.media.MediaPlayer;
import com.tencent.qqgame.common.Card;
import com.tencent.qqgame.hall.common.SysSetting;
import com.tencent.qqgame.lord.logic.CLogic;
import java.util.Random;
import java.util.Vector;
import tencent.qqgame.lord.R;

public class SoundPlayer implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener {
    public static final byte TYPE_BG_EXCITING = 3;
    public static final byte TYPE_BG_NORMAL = 0;
    public static final byte TYPE_BG_NORMAL2 = 1;
    public static final byte TYPE_BG_WELCOME = 2;
    private byte alarm;
    private MediaPlayer bgPlayer;
    public byte bg_type;
    private Context context;
    private int curBackGroundSound = -1;
    private MediaPlayer hitCardPlayer;
    private boolean isDeamon;
    private boolean isTurnPlaying;
    private boolean loop;
    private MediaPlayer sendCardPlayer;
    private byte specialCard;
    private Vector<MediaPlayer> turnPlayers = new Vector<>(4);

    public SoundPlayer(Context context2) {
        this.context = context2;
        this.isDeamon = false;
        this.isTurnPlaying = false;
        this.bg_type = -1;
    }

    private void addTurnPlayer(int i) {
        if (!this.isDeamon && SysSetting.getInstance(this.context).isEffectSound()) {
            try {
                MediaPlayer create = MediaPlayer.create(this.context, i);
                create.setOnErrorListener(this);
                create.setOnCompletionListener(this);
                if (create != null) {
                    this.turnPlayers.addElement(create);
                }
            } catch (Exception e) {
            }
        }
    }

    private void playBackGroudSound(int i) {
        try {
            this.curBackGroundSound = i;
            if (!this.isDeamon && SysSetting.getInstance(this.context).isBgSound()) {
                stopSound(this.bgPlayer);
                this.bgPlayer = null;
                this.bgPlayer = MediaPlayer.create(this.context, i);
                this.bgPlayer.setOnCompletionListener(this);
                this.bgPlayer.setLooping(true);
                this.bgPlayer.start();
            }
        } catch (Exception e) {
        }
    }

    private void playCardNumAlarm(int i) {
        if (i == 2) {
            addTurnPlayer(R.raw.alert);
            addTurnPlayer(R.raw.baojing2);
        } else if (i == 1) {
            addTurnPlayer(R.raw.alert);
            addTurnPlayer(R.raw.baojing1);
        }
    }

    private void playSound(int i) {
        playSound(null, i, false);
    }

    private void playSound(MediaPlayer mediaPlayer, int i, boolean z) {
        MediaPlayer mediaPlayer2;
        if (!this.isDeamon && SysSetting.getInstance(this.context).isEffectSound()) {
            if (mediaPlayer == null) {
                try {
                    mediaPlayer2 = MediaPlayer.create(this.context, i);
                } catch (Exception e) {
                    return;
                }
            } else {
                mediaPlayer2 = mediaPlayer;
            }
            mediaPlayer2.setOnCompletionListener(this);
            mediaPlayer2.setLooping(z);
            mediaPlayer2.start();
        }
    }

    public void addDaniSound() {
        addTurnPlayer(((new Random().nextInt() >>> 1) % 3) + R.raw.dani1);
    }

    public byte getBackGroudSoundType() {
        return this.bg_type;
    }

    public MediaPlayer getBackGroundPlayer() {
        return this.bgPlayer;
    }

    public void init() {
        this.specialCard = -1;
        this.alarm = -1;
    }

    public boolean isBackGroundSoundPlaying() {
        return this.bgPlayer != null && this.bgPlayer.isPlaying();
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        stopSound(mediaPlayer);
        if (this.turnPlayers.contains(mediaPlayer)) {
            this.turnPlayers.removeElement(mediaPlayer);
            this.isTurnPlaying = false;
        }
    }

    public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        stopSound(mediaPlayer);
        if (this.turnPlayers.contains(mediaPlayer)) {
            this.turnPlayers.removeElement(mediaPlayer);
            this.isTurnPlaying = false;
        }
        return false;
    }

    public void playCallLordSound() {
        playSound(R.raw.calllord);
    }

    public void playCardSound(Vector<Card> vector, int i, boolean z, boolean z2) {
        if (vector != null && vector.size() != 0) {
            int cardType = CLogic.getCardType(vector);
            if (!(cardType == 0 || cardType == 99)) {
                playSound(R.raw.givecard);
            }
            switch (cardType) {
                case 1:
                    if (vector.elementAt(0).value != 14) {
                        if (vector.elementAt(0).value == 15) {
                            addTurnPlayer(R.raw.dawang);
                            break;
                        }
                    } else {
                        addTurnPlayer(R.raw.xiaowang);
                        break;
                    }
                    break;
                case 3:
                    if (!z2) {
                        addTurnPlayer(R.raw.sange);
                        break;
                    } else {
                        addDaniSound();
                        break;
                    }
                case 4:
                    if (!z2) {
                        addTurnPlayer(R.raw.sandaiyi);
                        break;
                    } else {
                        addDaniSound();
                        break;
                    }
                case 5:
                    if (!z2) {
                        addTurnPlayer(R.raw.sandaiyidui);
                        break;
                    } else {
                        addDaniSound();
                        break;
                    }
                case 6:
                    if (z2) {
                        addDaniSound();
                    } else {
                        addTurnPlayer(R.raw.star);
                        addTurnPlayer(R.raw.shunzi);
                    }
                    if (this.specialCard == -1) {
                        this.specialCard = 0;
                        break;
                    }
                    break;
                case 7:
                    if (z2) {
                        addDaniSound();
                    } else {
                        addTurnPlayer(R.raw.liandui);
                        addTurnPlayer(R.raw.flower);
                    }
                    if (this.specialCard == -1) {
                        this.specialCard = 0;
                        break;
                    }
                    break;
                case 8:
                case 9:
                case 10:
                    if (z2) {
                        addDaniSound();
                    } else {
                        addTurnPlayer(R.raw.feiji);
                        addTurnPlayer(R.raw.plane);
                    }
                    if (this.specialCard == -1) {
                        this.specialCard = 0;
                        break;
                    }
                    break;
                case 11:
                    if (!z2) {
                        addTurnPlayer(R.raw.sidaier);
                        break;
                    } else {
                        addDaniSound();
                        break;
                    }
                case 12:
                    if (!z2) {
                        addTurnPlayer(R.raw.sidailiangdui);
                        break;
                    } else {
                        addDaniSound();
                        break;
                    }
                case 13:
                    addTurnPlayer(R.raw.boom);
                    addTurnPlayer(R.raw.zhadan);
                    addTurnPlayer(R.raw.jiabei);
                    if (this.specialCard == -1) {
                        this.specialCard = 0;
                        break;
                    }
                    break;
                case 14:
                    addTurnPlayer(R.raw.boom);
                    addTurnPlayer(R.raw.wangzha);
                    addTurnPlayer(R.raw.jiabei);
                    if (this.specialCard == -1) {
                        this.specialCard = 0;
                        break;
                    }
                    break;
            }
            if (!z) {
                playCardNumAlarm(i);
            }
            if (this.specialCard == 0) {
                if (i > 2) {
                    playExcitingBackGroundSound();
                }
                this.specialCard = 1;
            }
            if (i > 0 && i < 3 && this.alarm == -1) {
                this.alarm = 0;
                playNormal2BackGroundSound();
            }
        }
    }

    public void playChatSound(int i) {
        playSound(i);
    }

    public void playClockRingSound() {
        playSound(R.raw.ring);
    }

    public void playCurBackGroundSound() {
        if (this.curBackGroundSound >= 0) {
            playBackGroudSound(this.curBackGroundSound);
        }
    }

    public void playExcitingBackGroundSound() {
        if (getBackGroudSoundType() != 3) {
            this.bg_type = 3;
            playBackGroudSound(R.raw.exciting);
        }
    }

    public void playHitCardSound() {
        playSound(R.raw.da);
    }

    public void playMenuSound() {
        playSound(R.raw.menu);
    }

    public void playNoOrderSound() {
        playSound(R.raw.notorder);
    }

    public void playNormal2BackGroundSound() {
        if (getBackGroudSoundType() != 1) {
            this.bg_type = 1;
            playBackGroudSound(R.raw.normal2);
        }
    }

    public void playNormalBackGroundSound() {
        if (getBackGroudSoundType() != 0) {
            this.bg_type = 0;
            playBackGroudSound(R.raw.normal);
        }
    }

    public void playPassSound() {
        int nextInt = (new Random().nextInt() >>> 1) % 3;
        if (nextInt == 0) {
            playSound(R.raw.yaobuqi);
        } else if (nextInt == 1) {
            playSound(R.raw.pass);
        } else if (nextInt == 2) {
            playSound(R.raw.buyao);
        }
    }

    public void playResultSound(int i) {
        if (i < 0) {
            addTurnPlayer(R.raw.lose);
        } else if (i > 0) {
            addTurnPlayer(R.raw.win);
        }
    }

    public void playRunawaySound() {
        playSound(R.raw.runaway);
    }

    public void playSendCardSound() {
        if (!this.isDeamon && SysSetting.getInstance(this.context).isEffectSound()) {
            try {
                stopSound(this.sendCardPlayer);
                this.sendCardPlayer = MediaPlayer.create(this.context, (int) R.raw.sendcard);
                playSound(this.sendCardPlayer, R.raw.sendcard, true);
            } catch (Exception e) {
            }
        }
    }

    public void playShowCardSound() {
        playSound(R.raw.start);
    }

    public void playTurnSound() {
        if (!this.isTurnPlaying && this.turnPlayers.size() > 0) {
            MediaPlayer elementAt = this.turnPlayers.elementAt(0);
            if (!elementAt.isPlaying() && !this.isDeamon) {
                try {
                    elementAt.start();
                } catch (Exception e) {
                    this.turnPlayers.removeElement(elementAt);
                    this.isTurnPlaying = false;
                }
                this.isTurnPlaying = true;
            }
        }
    }

    public void playWelcomeBackGroundSound() {
        if (getBackGroudSoundType() != 2) {
            this.bg_type = 2;
            playBackGroudSound(R.raw.welcome);
        }
    }

    public void setLoop(boolean z) {
        this.loop = z;
    }

    public void startBackGroundSound() {
        this.isDeamon = false;
        if (this.bgPlayer == null) {
            try {
                playBackGroudSound(this.curBackGroundSound);
            } catch (Exception e) {
            }
        }
    }

    public void stopBackGroundSound(boolean z) {
        if (z) {
            this.isDeamon = true;
        }
        if (this.bgPlayer != null && this.bgPlayer.isPlaying()) {
            try {
                stopSound(this.bgPlayer);
            } catch (Exception e) {
            } finally {
                this.bgPlayer = null;
            }
        }
    }

    public void stopSendCardSound() {
        stopSound(this.sendCardPlayer);
        this.sendCardPlayer = null;
    }

    public void stopSound(MediaPlayer mediaPlayer) {
        if (mediaPlayer != null) {
            try {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }
                mediaPlayer.release();
            } catch (Exception e) {
            }
        }
    }
}
