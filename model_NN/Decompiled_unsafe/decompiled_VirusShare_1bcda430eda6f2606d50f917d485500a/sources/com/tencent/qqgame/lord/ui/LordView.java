package com.tencent.qqgame.lord.ui;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Handler;
import com.tencent.qqgame.common.Card;
import com.tencent.qqgame.common.Player;
import com.tencent.qqgame.common.Table;
import com.tencent.qqgame.hall.common.AnimationPlayerV1;
import com.tencent.qqgame.hall.common.Tools;
import com.tencent.qqgame.hall.protocol.ISendGameMessageHandle;
import com.tencent.qqgame.hall.ui.controls.UserInfoControl;
import com.tencent.qqgame.lord.common.AGameView;
import com.tencent.qqgame.lord.common.ControlGroup;
import com.tencent.qqgame.lord.common.GameMessage;
import com.tencent.qqgame.lord.logic.LordLogic;
import com.tencent.qqgame.lord.ui.control.ChatDialog;
import java.util.Calendar;
import java.util.Vector;
import tencent.qqgame.lord.R;

public class LordView extends AGameView {
    public static final int CARDS_MARGIN_H = 13;
    private static final int CARDS_MARGIN_V = 25;
    public static final short FRAME_BACK_CARDS_LIST = 192;
    public static final byte FRAME_BUBBLE_ARROW_BOTTOM = 20;
    public static final byte FRAME_BUBBLE_ARROW_LEFT = 18;
    public static final byte FRAME_BUBBLE_ARROW_RIGHT = 19;
    public static final byte FRAME_BUBBLE_LEFT_1FEN = 2;
    public static final byte FRAME_BUBBLE_LEFT_2FEN = 3;
    public static final byte FRAME_BUBBLE_LEFT_3FEN = 4;
    public static final byte FRAME_BUBBLE_LEFT_NOCALL = 0;
    public static final byte FRAME_BUBBLE_LEFT_PASS = 1;
    public static final byte FRAME_BUBBLE_RIGHT_1FEN = 7;
    public static final byte FRAME_BUBBLE_RIGHT_2FEN = 8;
    public static final byte FRAME_BUBBLE_RIGHT_3FEN = 9;
    public static final byte FRAME_BUBBLE_RIGHT_NOCALL = 5;
    public static final byte FRAME_BUBBLE_RIGHT_PASS = 6;
    public static final byte FRAME_BUBBLE_ROUND_CENTER = 16;
    public static final byte FRAME_BUBBLE_ROUND_LEFT = 15;
    public static final byte FRAME_BUBBLE_ROUND_RIGHT = 17;
    public static final byte FRAME_BUBBLE_SELF_1FEN = 12;
    public static final byte FRAME_BUBBLE_SELF_2FEN = 13;
    public static final byte FRAME_BUBBLE_SELF_3FEN = 14;
    public static final byte FRAME_BUBBLE_SELF_NOCALL = 10;
    public static final byte FRAME_BUBBLE_SELF_PASS = 11;
    public static final byte FRAME_BUTTON_1FEN_DISABLE = 24;
    public static final byte FRAME_BUTTON_1FEN_HIGH = 23;
    public static final byte FRAME_BUTTON_1FEN_NORMAL = 22;
    public static final byte FRAME_BUTTON_2FEN_DISABLE = 21;
    public static final byte FRAME_BUTTON_2FEN_HIGH = 20;
    public static final byte FRAME_BUTTON_2FEN_NORMAL = 19;
    public static final byte FRAME_BUTTON_3FEN_DISABLE = 54;
    public static final byte FRAME_BUTTON_3FEN_HIGH = 18;
    public static final byte FRAME_BUTTON_3FEN_NORMAL = 17;
    public static final byte FRAME_BUTTON_CANCEL_HIGH = 41;
    public static final byte FRAME_BUTTON_CANCEL_NORMAL = 40;
    public static final byte FRAME_BUTTON_CHANGEDESK_DISABLE = 37;
    public static final byte FRAME_BUTTON_CHANGEDESK_HIGH = 36;
    public static final byte FRAME_BUTTON_CHANGEDESK_NORMAL = 35;
    public static final byte FRAME_BUTTON_LEAVEDESK_HIGH = 34;
    public static final byte FRAME_BUTTON_LEAVEDESK_NORMAL = 33;
    public static final byte FRAME_BUTTON_NOCALL_DISABLE = 55;
    public static final byte FRAME_BUTTON_NOCALL_HIGH = 26;
    public static final byte FRAME_BUTTON_NOCALL_NORMAL = 25;
    public static final byte FRAME_BUTTON_OK_HIGH = 43;
    public static final byte FRAME_BUTTON_OK_NORMAL = 42;
    public static final byte FRAME_BUTTON_OUT_DISABLE = 2;
    public static final byte FRAME_BUTTON_OUT_HIGH = 1;
    public static final byte FRAME_BUTTON_OUT_NORMAL = 0;
    public static final byte FRAME_BUTTON_PASS_DISABLE = 5;
    public static final byte FRAME_BUTTON_PASS_HIGH = 4;
    public static final byte FRAME_BUTTON_PASS_NORMAL = 3;
    public static final byte FRAME_BUTTON_RESET_DISABLE = 11;
    public static final byte FRAME_BUTTON_RESET_HIGH = 10;
    public static final byte FRAME_BUTTON_RESET_NORMAL = 9;
    public static final byte FRAME_BUTTON_SEND_HIGH = 39;
    public static final byte FRAME_BUTTON_SEND_NORMAL = 38;
    public static final byte FRAME_BUTTON_START_DISABLE = 56;
    public static final byte FRAME_BUTTON_START_HIGH = 13;
    public static final byte FRAME_BUTTON_START_NORMAL = 12;
    public static final byte FRAME_BUTTON_TICK_DISABLE = 29;
    public static final byte FRAME_BUTTON_TICK_HIGH = 28;
    public static final byte FRAME_BUTTON_TICK_NORMAL = 27;
    public static final byte FRAME_BUTTON_TISHI_DISABLE = 8;
    public static final byte FRAME_BUTTON_TISHI_HIGH = 7;
    public static final byte FRAME_BUTTON_TISHI_NORMAL = 6;
    public static final byte FRAME_BUTTON_WATCH_DISABLE = 32;
    public static final byte FRAME_BUTTON_WATCH_HIGH = 31;
    public static final byte FRAME_BUTTON_WATCH_NORMAL = 30;
    public static final byte FRAME_CARD_HEITAO_A_L = 0;
    public static final byte FRAME_CARD_HEITAO_A_M = 55;
    public static final byte FRAME_CARD_HEITAO_A_S = 110;
    public static final byte FRAME_PANEL_BUTTON_CONTINUE = 29;
    public static final byte FRAME_PANEL_CHAT_PANEL = 0;
    public static final byte FRAME_PANEL_PLAYER_INFO = 44;
    public static final byte FRAME_PANEL_QUIT = 4;
    public static final byte FRAME_PANEL_RECHECK = 8;
    public static final byte FRAME_PANEL_RESULT = 50;
    public static final byte FRAME_PANEL_SPEAKER_OFF = 9;
    public static final byte FRAME_PANEL_SPEAKER_ON = 8;
    public static final byte FRAME_PANEL_TAB_SETTING_BRIGHT = 2;
    public static final byte FRAME_PANEL_TAB_SETTING_HINT = 3;
    public static final byte FRAME_PANEL_TAB_SETTING_SOUND = 1;
    public static final byte FRAME_PANEL_TAB_SETTING_THUMB = 5;
    public static final byte FRAME_PANEL_TIPS = 42;
    public static final byte FRAME_PANEL_TIPS_NORMAL = 42;
    public static final byte FRAME_PANEL_TURN_OFF = 7;
    public static final byte FRAME_PANEL_TURN_ON = 6;
    public static final byte FRAME_PANEL_USER_INFO_BOTTOM_CENTER = 7;
    public static final byte FRAME_PANEL_USER_INFO_BOTTOM_LEFT = 6;
    public static final byte FRAME_PANEL_USER_INFO_LEFT = 4;
    public static final byte FRAME_PANEL_USER_INFO_RIGHT = 5;
    public static final byte FRAME_PORTRAIT_FRAMER = 7;
    public static final byte FRAME_PORTRAIT_ICON_FRAME = 15;
    public static final byte FRAME_PORTRAIT_LORD = 0;
    public static final byte FRAME_PORTRAIT_TRUSTEESHIP = 14;
    public static final byte FRAME_TABLE_LANZUAN = 5;
    private static final byte FRAME_TABLE_PAI = 2;
    public static final byte FRAME_TABLE_READY = 4;
    public static final byte FRAME_TOOLBAR_CHAT_DISABLE = 3;
    public static final byte FRAME_TOOLBAR_CHAT_HIGH = 1;
    public static final byte FRAME_TOOLBAR_CHAT_NORMAL = 0;
    private static final byte FRAME_TOOLBAR_DB = 14;
    public static final byte FRAME_TOOLBAR_QUIT_HIGH = 12;
    public static final byte FRAME_TOOLBAR_QUIT_NORMAL = 11;
    public static final byte FRAME_TOOLBAR_SET_HIGH = 9;
    public static final byte FRAME_TOOLBAR_SET_NORMAL = 8;
    public static final byte FRAME_TOOLBAR_TOOLBAR = 19;
    public static final byte FRAME_TOOLBAR_TUOGUAN_DISABLE = 7;
    public static final byte FRAME_TOOLBAR_TUOGUAN_HIGH = 5;
    public static final byte FRAME_TOOLBAR_TUOGUAN_NORMAL = 4;
    public static final int MYCARD_Y = 232;
    public static final int SCREEN_HEIGHT = 320;
    public static final int SCREEN_WIDTH = 480;
    public static Bitmap imgNums = null;
    public final byte CARD_HEIGHT_L;
    public final byte CARD_HEIGHT_M;
    private final byte CARD_HEIGHT_S;
    private final byte CARD_WIDTH_B;
    public final byte CARD_WIDTH_L;
    public final byte CARD_WIDTH_M;
    private final byte CARD_WIDTH_S;
    private final byte FRAME_SIZE;
    public final int MYOUTCARD_Y;
    public final AnimationPlayerV1 apBubble;
    public final AnimationPlayerV1 apButton;
    public final AnimationPlayerV1 apCards;
    public final AnimationPlayerV1 apClock;
    public final AnimationPlayerV1 apPPanel;
    public final AnimationPlayerV1 apPanelV1;
    public final AnimationPlayerV1 apPortrait;
    public final AnimationPlayerV1 apTable;
    public final AnimationPlayerV1 apToolbar;
    public final ChatDialog chatDlg;
    private int currMin = -1;
    private Bitmap imgBg;
    private Bitmap imgFarmer;
    private Bitmap imgFrame;
    public Bitmap[] imgLeftCardNums;
    private Bitmap imgLord;
    public Bitmap imgNumsAdd;
    public Bitmap imgNumsMinus;
    private Bitmap imgTimer = null;
    private Bitmap imgTimerNumber;
    private Bitmap imgToolbarBg;
    private Bitmap imgTuoGuan;
    private LordLogic logic;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    public LordView(Activity activity, LordSurface lordSurface, Handler handler, ISendGameMessageHandle iSendGameMessageHandle, short s, Table table, Player player, long j, boolean z) {
        super(lordSurface, iSendGameMessageHandle, s, z);
        Resources resources = activity.getResources();
        this.apToolbar = new AnimationPlayerV1(this.aniManager.toolbarSprite);
        this.apToolbar.setActionFlag(0, false);
        this.apCards = new AnimationPlayerV1(this.aniManager.cardSprite);
        this.apCards.setActionFlag(0, false);
        this.apClock = new AnimationPlayerV1(this.aniManager.clockSprite);
        this.apClock.setActionFlag(0, false);
        this.apPortrait = new AnimationPlayerV1(this.aniManager.portraitSprite);
        this.apPortrait.setActionFlag(0, false);
        this.apButton = new AnimationPlayerV1(this.aniManager.buttonSprite);
        this.apButton.setActionFlag(0, false);
        this.apButton.setID((byte) 2);
        this.apBubble = new AnimationPlayerV1(this.aniManager.bubbleSprite);
        this.apBubble.setActionFlag(0, false);
        this.apPanelV1 = new AnimationPlayerV1(this.aniManager.panelSpriteV1);
        this.apPanelV1.setActionFlag(0, false);
        this.apPPanel = new AnimationPlayerV1(this.aniManager.ppanelSpriteV1);
        this.apPPanel.setActionFlag(0, false);
        this.apTable = new AnimationPlayerV1(this.aniManager.tableSprite);
        this.apPPanel.setActionFlag(0, false);
        Bitmap decodeResource = BitmapFactory.decodeResource(resources, R.drawable.bg);
        Matrix matrix = new Matrix();
        matrix.postRotate(90.0f);
        this.imgBg = Bitmap.createBitmap(decodeResource, 0, 0, decodeResource.getWidth(), decodeResource.getHeight(), matrix, true);
        this.imgTimerNumber = BitmapFactory.decodeResource(resources, R.drawable.timer_number);
        this.imgTuoGuan = drawFrame2Bitmap(this.apPortrait, 14);
        this.imgLord = drawFrame2Bitmap(this.apPortrait, 0);
        this.imgFarmer = drawFrame2Bitmap(this.apPortrait, 7);
        this.imgFrame = drawFrame2Bitmap(this.apPortrait, 15);
        this.imgNumsAdd = BitmapFactory.decodeResource(resources, R.drawable.point_add);
        this.imgNumsMinus = BitmapFactory.decodeResource(resources, R.drawable.point_sub);
        this.imgLeftCardNums = new Bitmap[3];
        for (int i = 0; i < this.imgLeftCardNums.length; i++) {
            this.imgLeftCardNums[i] = BitmapFactory.decodeResource(resources, R.drawable.number_01 + i);
        }
        if (imgNums == null) {
            imgNums = BitmapFactory.decodeResource(resources, R.drawable.number);
        }
        this.CARD_WIDTH_B = (byte) this.apTable.getFrameWidth(2);
        this.CARD_WIDTH_S = (byte) this.apCards.getFrameWidth(110);
        this.CARD_HEIGHT_S = (byte) this.apCards.getFrameHeight(110);
        this.CARD_WIDTH_M = (byte) this.apCards.getFrameWidth(55);
        this.CARD_HEIGHT_M = (byte) this.apCards.getFrameHeight(55);
        this.CARD_WIDTH_L = (byte) this.apCards.getFrameWidth(0);
        this.CARD_HEIGHT_L = (byte) this.apCards.getFrameHeight(0);
        this.MYOUTCARD_Y = 226 - this.CARD_HEIGHT_M;
        this.FRAME_SIZE = (byte) this.apPortrait.getFrameWidth(15);
        this.logic = new LordLogic(activity, handler, iSendGameMessageHandle, this, table, player, j, z);
        this.logic = this.logic;
        this.aniManager.setLogic(this.logic);
        this.chatDlg = new ChatDialog(this.logic);
        initLocationInfoImg();
        this.imgToolbarBg = Bitmap.createBitmap(this.apToolbar.getFrameWidth(19), this.apToolbar.getFrameHeight(19), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas();
        canvas.setBitmap(this.imgToolbarBg);
        this.apToolbar.drawFrame(canvas, 19, 0, 0);
        this.aniManager.playToolBarAni();
    }

    private void drawBackCards(Canvas canvas, Paint paint) {
        byte b;
        byte b2 = 0;
        int i = 180;
        while (b2 < 3) {
            Card elementAt = this.logic.dCards.elementAt(b2);
            if (elementAt.number < 55) {
                this.apCards.drawFrame(canvas, (elementAt.number + 110) - 1, i, 3);
                b = this.CARD_WIDTH_S;
            } else {
                this.apTable.drawFrame(canvas, 2, i, 3);
                b = this.CARD_WIDTH_B;
            }
            b2 = (byte) (b2 + 1);
            i = b + 2 + i;
        }
        this.apToolbar.drawFrame(canvas, 14, 260, 7);
        int frameWidth = 260 + this.apToolbar.getFrameWidth(14) + 4;
        paint.setColor(-1);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        paint.setTextSize((float) 8);
        int i2 = 7 + 1;
        canvas.drawText(String.valueOf(this.logic.multiple), (float) frameWidth, (float) (8 + 8), paint);
        canvas.drawText(String.valueOf((int) this.logic.backPoint), (float) frameWidth, (float) (8 + 20), paint);
        paint.setTypeface(Typeface.DEFAULT);
    }

    private void drawCardList(Vector<Card> vector, int i, int i2, byte b, Canvas canvas, Paint paint) {
        int i3;
        int i4;
        int i5 = 0;
        int i6 = i2;
        int i7 = i;
        while (i5 < vector.size()) {
            this.apCards.drawFrame(canvas, (vector.elementAt(i5).number + 55) - 1, i7, i6);
            int i8 = i7 + 13;
            if ((i5 + 1) % b == 0) {
                i4 = i6 + CARDS_MARGIN_V;
                i3 = i;
            } else {
                int i9 = i6;
                i3 = i8;
                i4 = i9;
            }
            i5++;
            i7 = i3;
            i6 = i4;
        }
    }

    private void drawChatMessage(Canvas canvas, Paint paint) {
        GameMessage lastPlayerChatMessage;
        int i;
        int i2;
        int[] iArr = {450, 30, 30};
        int[] iArr2 = {100, 130, 100};
        if (this.logic.curStatus == 0) {
            iArr[1] = 210;
            iArr2[1] = 170;
        }
        boolean z = false;
        for (byte b = 0; b < 3; b = (byte) (b + 1)) {
            Player playerByOppositeSeatId = Table.getPlayerByOppositeSeatId((short) b, this.logic.table.playerList, this.logic.me.seatId);
            if (!(playerByOppositeSeatId == null || (lastPlayerChatMessage = this.logic.getLastPlayerChatMessage(playerByOppositeSeatId)) == null)) {
                if (!(this.chatDlg == null || lastPlayerChatMessage.originMsg == null)) {
                    this.chatDlg.playChatSound(lastPlayerChatMessage.originMsg);
                    lastPlayerChatMessage.originMsg = null;
                }
                if (b == 0) {
                    z = true;
                } else if (b == 2 && z) {
                    iArr2[b] = iArr2[b] + 30;
                }
                paint.setColor(-16435358);
                int i3 = iArr[b];
                int i4 = iArr2[b];
                int i5 = lastPlayerChatMessage.length;
                if (b == 0) {
                    i3 -= i5 + 20;
                }
                this.apBubble.drawFrame(canvas, 15, i3, i4);
                switch (b) {
                    case 0:
                        this.apBubble.drawFrame(canvas, 19, iArr[b] - CARDS_MARGIN_V, i4);
                        break;
                    case 1:
                        this.apBubble.drawFrame(canvas, 20, i3 + 15, i4 + 30);
                        break;
                    case 2:
                        this.apBubble.drawFrame(canvas, 18, i3 + 15, i4);
                        break;
                }
                int frameWidth = i3 + this.apBubble.getFrameWidth(15);
                int frameWidth2 = this.apBubble.getFrameWidth(16);
                int i6 = i5 / frameWidth2;
                if (i5 % frameWidth2 != 0) {
                    i6++;
                }
                int i7 = frameWidth;
                for (byte b2 = 0; b2 < i6; b2 = (byte) (b2 + 1)) {
                    this.apBubble.drawFrame(canvas, 16, i7, i4);
                    i7 += frameWidth2;
                }
                this.apBubble.drawFrame(canvas, 17, i7, i4);
                paint.setTextSize(16.0f);
                if (b == 0) {
                    int i8 = iArr2[b] + 28;
                    i = (iArr[b] - i5) - 14;
                    i2 = i8;
                } else {
                    i = (i7 - i5) - 16;
                    i2 = iArr2[b] + 28;
                }
                canvas.save();
                canvas.clipRect(i, i2 - 20, lastPlayerChatMessage.length + i + 10, i2);
                canvas.drawText(lastPlayerChatMessage.message[lastPlayerChatMessage.curLine], (float) (i + 10), (float) ((i2 - 6) + lastPlayerChatMessage.offset), paint);
                if (lastPlayerChatMessage.needScroll) {
                    canvas.drawText(lastPlayerChatMessage.message[(lastPlayerChatMessage.curLine + 1) % lastPlayerChatMessage.message.length], (float) (i + 10), (float) ((i2 - 6) + 20 + lastPlayerChatMessage.offset), paint);
                }
                canvas.restore();
                if (lastPlayerChatMessage.needScroll) {
                    lastPlayerChatMessage.timerStop -= this.SPF;
                    if (lastPlayerChatMessage.timerStop <= 0) {
                        lastPlayerChatMessage.offset -= 2;
                        if (lastPlayerChatMessage.offset < -20) {
                            lastPlayerChatMessage.offset = 0;
                            lastPlayerChatMessage.curLine = (lastPlayerChatMessage.curLine + 1) % lastPlayerChatMessage.message.length;
                            lastPlayerChatMessage.timerStop = GameMessage.TIMER_STOP_INTERVAL;
                        }
                    }
                }
            }
        }
    }

    private int drawDigitalNum(Canvas canvas, Paint paint, int i, int i2, int i3) {
        if (i < -1) {
            return 0;
        }
        Rect rect = rect1;
        Rect rect2 = rect2;
        if (i == -1) {
            rect.set(110, 0, 115, 14);
            rect2.set(i2, i3, i2 + 5, i3 + 14);
            canvas.drawBitmap(this.imgTimerNumber, rect, rect2, paint);
            return i2 + 5 + 4;
        }
        int i4 = i2;
        for (char valueOf : (i < 10 ? "0" + i : Integer.toString(i)).toCharArray()) {
            int parseInt = Integer.parseInt(String.valueOf(valueOf));
            rect.set(parseInt * 11, 0, (parseInt + 1) * 11, 14);
            rect2.set(i4, i3, i4 + 11, i3 + 14);
            canvas.drawBitmap(this.imgTimerNumber, rect, rect2, paint);
            i4 += 15;
        }
        return i4;
    }

    private void drawDigitalTimer(Canvas canvas, Paint paint, int i, int i2) {
        Calendar instance = Calendar.getInstance();
        if (instance.get(12) != this.currMin) {
            if (this.imgTimer == null) {
                this.imgTimer = Bitmap.createBitmap(65, 24, Bitmap.Config.ARGB_8888);
            }
            Canvas canvas2 = new Canvas();
            canvas2.setBitmap(this.imgTimer);
            int i3 = instance.get(11);
            int i4 = instance.get(12);
            drawDigitalNum(canvas2, paint, i4, drawDigitalNum(canvas2, paint, -1, drawDigitalNum(canvas2, paint, i3, 0, 0), 0), 0);
            this.currMin = i4;
        }
        canvas.drawBitmap(this.imgTimer, (float) i, (float) i2, paint);
    }

    private void drawPlayingView(Canvas canvas, Paint paint) {
        int i;
        byte b = this.logic.curStatus;
        boolean z = this.logic.isViewGame;
        drawBackCards(canvas, paint);
        if (this.logic.backPoint == 0 && !this.aniManager.isSendingCard()) {
            this.apCards.drawFrame(canvas, 192, 0, 0);
        }
        int[] iArr = {424, 4, 4};
        int[] iArr2 = {4, 171, 4};
        byte b2 = 0;
        while (true) {
            byte b3 = b2;
            if (b3 >= 3) {
                break;
            }
            short s = -1;
            this.logic.ctlPlayers[b3].setLocation(iArr[b3], iArr2[b3]);
            Player playerByOppositeSeatId = Table.getPlayerByOppositeSeatId((short) b3, this.logic.table.playerList, this.logic.me.seatId);
            if (playerByOppositeSeatId != null) {
                s = playerByOppositeSeatId.iconId;
            }
            if (this.logic.backPoint <= 0 || playerByOppositeSeatId == null) {
                canvas.drawBitmap(this.imgFrame, (float) iArr[b3], (float) iArr2[b3], paint);
                Tools.drawQQHead(canvas, paint, s, iArr[b3] + 5, iArr2[b3] + 5);
            } else if (!this.aniManager.isShowingLordFarmerHead()) {
                if (this.logic.isTrusteeship && b3 == 1) {
                    canvas.drawBitmap(this.imgTuoGuan, (float) iArr[b3], (float) (iArr2[b3] + 5), paint);
                } else if (this.logic.lordSeatId == b3) {
                    canvas.drawBitmap(this.imgLord, (float) iArr[b3], (float) (iArr2[b3] + 5), paint);
                } else {
                    canvas.drawBitmap(this.imgFarmer, (float) iArr[b3], (float) (iArr2[b3] + 5), paint);
                }
            }
            if (playerByOppositeSeatId == null) {
                Tools.debug("playerNum:" + this.logic.table.playerList.size());
                int i2 = 0;
                while (true) {
                    int i3 = i2;
                    if (i3 >= this.logic.table.playerList.size()) {
                        break;
                    }
                    Tools.debug("player seatId:" + ((int) this.logic.table.playerList.elementAt(i3).seatId));
                    Tools.debug("player uid:" + this.logic.table.playerList.elementAt(i3).uid);
                    Tools.debug("player status:" + ((int) this.logic.table.playerList.elementAt(i3).status));
                    i2 = i3 + 1;
                }
            } else {
                if (b3 != 1) {
                    String str = playerByOppositeSeatId.nickName == null ? " " : playerByOppositeSeatId.nickName;
                    if (str.length() > 5) {
                        str = str.substring(0, 4) + "..";
                    }
                    paint.setTextSize((float) 12);
                    paint.setColor(-1);
                    int measureText = (int) paint.measureText(str);
                    String levelName = UserInfoControl.getLevelName(playerByOppositeSeatId.point);
                    if (b3 == 0) {
                        int i4 = iArr2[b3] + this.FRAME_SIZE + 3 + 12;
                        canvas.drawText(str, (float) ((iArr[b3] + this.FRAME_SIZE) - measureText), (float) i4, paint);
                        int measureText2 = (iArr[b3] + this.FRAME_SIZE) - ((int) paint.measureText(levelName));
                        if (playerByOppositeSeatId.isSuperPlayer()) {
                            int frameWidth = measureText2 - (this.apTable.getFrameWidth(5) - 2);
                            this.apTable.drawFrame(canvas, 5, frameWidth, i4 + 5);
                            measureText2 = frameWidth + this.apTable.getFrameWidth(5) + 2;
                        }
                        paint.setColor(-11425314);
                        canvas.drawText(levelName, (float) measureText2, (float) (i4 + 3 + 12), paint);
                        int i5 = iArr[b3] - 4;
                        if (b == -2) {
                            this.apTable.drawFrame(canvas, 2, (iArr[b3] + this.FRAME_SIZE) - this.CARD_WIDTH_B, i4 + 3 + 12 + 2);
                            if (!this.aniManager.isSendingCard()) {
                                int width = imgNums.getWidth() / 11;
                                int i6 = (((iArr[b3] + this.FRAME_SIZE) - this.CARD_WIDTH_B) - 24) + width;
                                int height = i4 + 3 + 12 + 10 + (imgNums.getHeight() >> 1);
                                if (!this.aniManager.needScale(b3) || this.logic.allCardNum[b3] > 3 || this.logic.allCardNum[b3] <= 0) {
                                    Tools.drawNum(canvas, paint, imgNums, this.logic.allCardNum[b3], i6 - width, height - (imgNums.getHeight() >> 1));
                                } else {
                                    Bitmap bitmap = this.imgLeftCardNums[this.logic.allCardNum[b3] - 1];
                                    canvas.drawBitmap(bitmap, (float) (i6 - (bitmap.getWidth() >> 1)), (float) (height - (bitmap.getHeight() >> 1)), paint);
                                }
                            }
                            if (this.logic.talkWho == b3) {
                                this.apClock.drawFrame(canvas, i5 - 31, 44);
                            } else {
                                int size = this.logic.allOutCards[b3].size();
                                if (size > 10) {
                                    size = 10;
                                }
                                if (size > 0) {
                                    drawCardList(this.logic.allOutCards[b3], i5 - (((size - 1) * 13) + this.CARD_WIDTH_M), 44, (byte) 10, canvas, paint);
                                } else if (this.logic.playerSay[b3] >= 0) {
                                    this.apBubble.drawFrame(canvas, this.logic.playerSay[b3], 0, 0);
                                }
                            }
                        } else {
                            Vector<Card> vector = this.logic.allLeaveCards[b3];
                            Vector<Card> vector2 = vector.size() == 0 ? this.logic.allOutCards[b3] : vector;
                            int size2 = vector2.size();
                            if (size2 > 10) {
                                size2 = 10;
                            }
                            if (size2 > 0) {
                                drawCardList(vector2, i5 - (((size2 - 1) * 13) + this.CARD_WIDTH_M), 44, (byte) 10, canvas, paint);
                            }
                        }
                        drawScoreAnimation(canvas, paint, b3, canvas.getWidth() - 3, i4 + 3 + 12 + 2);
                    } else {
                        int i7 = iArr2[b3] + this.FRAME_SIZE + 3 + 12;
                        int i8 = iArr[b3];
                        canvas.drawText(str, (float) iArr[b3], (float) i7, paint);
                        if (playerByOppositeSeatId.isSuperPlayer()) {
                            this.apTable.drawFrame(canvas, 5, i8, i7 + 3);
                            i = this.apTable.getFrameWidth(5) + 2 + i8;
                        } else {
                            i = i8;
                        }
                        paint.setColor(-11425314);
                        canvas.drawText(levelName, (float) i, (float) (i7 + 3 + 12), paint);
                        int i9 = iArr[b3] + this.FRAME_SIZE + 4;
                        if (b == -2) {
                            this.apTable.drawFrame(canvas, 2, iArr[b3], i7 + 3 + 12 + 2);
                            if (!this.aniManager.isSendingCard()) {
                                int width2 = imgNums.getWidth() / 11;
                                int i10 = iArr[b3] + this.CARD_WIDTH_B + width2;
                                int height2 = i7 + 3 + 12 + 10 + (imgNums.getHeight() >> 1);
                                if (!this.aniManager.needScale(b3) || this.logic.allCardNum[b3] > 3 || this.logic.allCardNum[b3] <= 0) {
                                    Tools.drawNum(canvas, paint, imgNums, this.logic.allCardNum[b3], i10 - width2, height2 - (imgNums.getHeight() >> 1));
                                } else {
                                    Bitmap bitmap2 = this.imgLeftCardNums[this.logic.allCardNum[b3] - 1];
                                    canvas.drawBitmap(bitmap2, (float) (i10 - (bitmap2.getWidth() >> 1)), (float) (height2 - (bitmap2.getHeight() >> 1)), paint);
                                }
                            }
                            if (this.logic.talkWho == b3) {
                                this.apClock.drawFrame(canvas, i9, 44);
                            } else if (this.logic.allOutCards[b3].size() > 0) {
                                drawCardList(this.logic.allOutCards[b3], i9, 44, (byte) 10, canvas, paint);
                            } else if (this.logic.playerSay[b3] >= 0) {
                                this.apBubble.drawFrame(canvas, this.logic.playerSay[b3], 0, 0);
                            }
                        } else {
                            Vector<Card> vector3 = this.logic.allLeaveCards[b3];
                            Vector<Card> vector4 = vector3.size() == 0 ? this.logic.allOutCards[b3] : vector3;
                            if (vector4.size() > 0) {
                                drawCardList(vector4, i9, 44, (byte) 10, canvas, paint);
                            }
                        }
                        drawScoreAnimation(canvas, paint, b3, iArr[b3] + 3, i7 + 3 + 12 + 2);
                    }
                } else {
                    if (this.logic.talkWho == b3 && b == -2) {
                        int i11 = iArr[b3] + this.FRAME_SIZE + 5;
                        int i12 = iArr2[b3] - 10;
                        if (this.logic.isMeCall) {
                            this.logic.ctlNoCall.setLocation(i11, i12);
                            this.logic.ctlNoCall.draw(canvas, paint);
                            int frameWidth2 = i11 + this.apButton.getFrameWidth(CARDS_MARGIN_V) + 5;
                            this.apClock.drawFrame(canvas, frameWidth2, i12 + 4);
                            int i13 = 31 + 5;
                            int i14 = frameWidth2 + 36;
                            this.logic.ctl1Fen.setLocation(i14, i12);
                            this.logic.ctl1Fen.draw(canvas, paint);
                            int frameWidth3 = i14 + this.apButton.getFrameWidth(22) + 10;
                            this.logic.ctl2Fen.setLocation(frameWidth3, i12);
                            this.logic.ctl2Fen.draw(canvas, paint);
                            this.logic.ctl3Fen.setLocation(10 + this.apButton.getFrameWidth(19) + frameWidth3, i12);
                            this.logic.ctl3Fen.draw(canvas, paint);
                        } else {
                            this.logic.ctlPass.setLocation(i11, i12);
                            this.logic.ctlPass.draw(canvas, paint);
                            int frameWidth4 = i11 + this.apButton.getFrameWidth(3) + 5;
                            this.apClock.drawFrame(canvas, frameWidth4, i12 + 4);
                            int i15 = 31 + 5;
                            int i16 = frameWidth4 + 36;
                            this.logic.ctlReset.setLocation(i16, i12);
                            this.logic.ctlReset.draw(canvas, paint);
                            int frameWidth5 = i16 + this.apButton.getFrameWidth(9) + 10;
                            this.logic.ctlTiShi.setLocation(frameWidth5, i12);
                            this.logic.ctlTiShi.draw(canvas, paint);
                            this.logic.ctlOut.setLocation(10 + this.apButton.getFrameWidth(0) + frameWidth5, i12);
                            this.logic.ctlOut.draw(canvas, paint);
                        }
                    } else {
                        int size3 = this.logic.allOutCards[b3].size();
                        if (size3 > 0) {
                            for (int i17 = 0; i17 < size3; i17++) {
                                Rect location = this.logic.ctlMyOutCards[i17].getLocation();
                                this.apCards.drawFrame(canvas, (this.logic.allOutCards[b3].elementAt(i17).number + 55) - 1, location.left, location.top);
                            }
                        } else if (this.logic.playerSay[b3] >= 0 && this.logic.curStatus != -3) {
                            this.apBubble.drawFrame(canvas, this.logic.playerSay[b3], 0, 0);
                        }
                    }
                    if (this.logic.curStatus != -2 || this.aniManager.isSendingCard()) {
                        Vector<Card> vector5 = this.logic.allLeaveCards[b3];
                        int size4 = vector5.size();
                        if (size4 > 0) {
                            drawCardList(vector5, (SCREEN_WIDTH - (((size4 - 1) * 13) + this.CARD_WIDTH_M)) / 2, MYCARD_Y, (byte) 20, canvas, paint);
                        }
                    } else {
                        int size5 = this.logic.myCards.size();
                        for (int i18 = 0; i18 < size5; i18++) {
                            Card elementAt = this.logic.myCards.elementAt(i18);
                            Rect location2 = this.logic.ctlMyCards[i18].getLocation();
                            int i19 = location2.left;
                            int i20 = location2.top;
                            if (elementAt.isSelected && this.logic.isTouchOutCards) {
                                paint.setAlpha(127);
                            }
                            canvas.drawBitmap(elementAt.imgCard, (float) i19, (float) i20, paint);
                            if (elementAt.isTouched && !z) {
                                paint.setColor(1717134962);
                                paint.setStyle(Paint.Style.FILL_AND_STROKE);
                                fRect.set((float) (i19 + 2), (float) i20, (float) ((i19 + this.CARD_WIDTH_L) - 2), (float) ((i20 + this.CARD_HEIGHT_L) - 2));
                                canvas.drawRoundRect(fRect, 6.0f, 6.0f, paint);
                            } else if (elementAt.isFocused && !z) {
                                paint.setColor(1712772349);
                                paint.setStyle(Paint.Style.FILL_AND_STROKE);
                                fRect.set((float) (i19 + 2), (float) i20, (float) ((i19 + this.CARD_WIDTH_L) - 2), (float) ((i20 + this.CARD_HEIGHT_L) - 2));
                                canvas.drawRoundRect(fRect, 6.0f, 6.0f, paint);
                            }
                            paint.setColor(-1);
                        }
                    }
                    drawScoreAnimation(canvas, paint, b3, iArr[b3] + 3, iArr2[b3] + this.FRAME_SIZE + 3);
                }
                if (this.logic.curStatus == -3) {
                    int frameWidth6 = this.apTable.getFrameWidth(4);
                    if (playerByOppositeSeatId.status == 3) {
                        this.apTable.drawFrame(canvas, 4, b3 == 0 ? (iArr[b3] - 3) - frameWidth6 : 3 + iArr[b3] + this.FRAME_SIZE, ((iArr2[b3] + this.FRAME_SIZE) - frameWidth6) - 8);
                    }
                    int frameWidth7 = this.apButton.getFrameWidth(12);
                    int frameHeight = this.apButton.getFrameHeight(12);
                    int i21 = 216 - ((frameWidth7 - this.FRAME_SIZE) / 2);
                    int i22 = (213 - frameHeight) - 4;
                    this.logic.ctlStartGame.setLocation(i21, i22);
                    this.logic.ctlStartGame.draw(canvas, paint);
                    if (this.logic.thinkTime >= 0) {
                        this.apClock.drawFrame(canvas, frameWidth7 + i21 + 15, (frameHeight + i22) - 31);
                    }
                }
            }
            b2 = (byte) (b3 + 1);
        }
        if (this.logic.isTouchOutCards) {
            drawCardList(this.logic.selectedCards, this.logic.dragX - 40 >= 0 ? this.logic.dragX - 40 : 0, this.logic.dragY - 40 >= 0 ? this.logic.dragY - 40 : 0, (byte) 20, canvas, paint);
        }
    }

    private final void drawQuitPannel(Canvas canvas, Paint paint, ControlGroup controlGroup) {
        Rect frameBound = this.apPanelV1.getFrameBound(4);
        int i = frameBound.left;
        int i2 = frameBound.top;
        controlGroup.setRect(frameBound.left, frameBound.top, frameBound.right, frameBound.bottom);
        this.apPanelV1.drawFrame(canvas, 4);
        int i3 = frameBound.right - frameBound.left;
        int width = this.logic.ctlChangeDesk.getWidth();
        int height = this.logic.ctlLeaveDesk.getHeight();
        int i4 = i2 + 20;
        this.logic.ctlChangeDesk.setLocation(((i3 - width) / 2) + i, i4);
        this.logic.ctlChangeDesk.draw(canvas, paint);
        this.logic.ctlLeaveDesk.setLocation(((i3 - width) / 2) + i, i4 + height + 5);
        this.logic.ctlLeaveDesk.draw(canvas, paint);
    }

    private void drawScoreAnimation(Canvas canvas, Paint paint, int i, int i2, int i3) {
        Player playerByOppositeSeatId;
        if (this.logic.curStatus == -3 && (playerByOppositeSeatId = Table.getPlayerByOppositeSeatId((short) i, this.logic.table.playerList, this.logic.me.seatId)) != null) {
            int i4 = playerByOppositeSeatId.point;
            int numWidth = i == 0 ? i2 - Tools.numWidth(imgNums, i4, false) : i2;
            Tools.drawNum(canvas, paint, imgNums, i4, numWidth, i3);
            Bitmap bitmap = this.logic.allPlayerScore[i] > 0 ? this.imgNumsAdd : this.imgNumsMinus;
            int nextAlpha = this.aniManager.nextAlpha();
            if (nextAlpha >= 0) {
                if (i == 1 || i == 2) {
                    numWidth = Tools.numWidth(imgNums, i4, false) + numWidth;
                } else if (i == 0) {
                    numWidth -= Tools.numWidth(bitmap, this.logic.allPlayerScore[i], true);
                }
                int alpha = paint.getAlpha();
                if (nextAlpha < 127) {
                    paint.setAlpha(nextAlpha);
                }
                Tools.drawNum(canvas, paint, bitmap, this.logic.allPlayerScore[i], numWidth, i3 - ((255 - nextAlpha) / 3), true);
                paint.setAlpha(alpha);
            }
        }
    }

    private void drawSettingPannel(Canvas canvas, Paint paint, ControlGroup controlGroup, int i, int i2) {
        int i3;
        int i4;
        paint.setTextSize(16.0f);
        int color = paint.getColor();
        if (controlGroup.type == 3) {
            this.logic.ctlSettingSound.setLocation(i, i2);
            this.logic.ctlSettingSound.draw(canvas, paint);
            paint.setColor(-256);
            int setInt = this.logic.ctlSettingProgressBar.getSetInt(0, 0, false);
            this.logic.myRectF.set(188.0f, 102.0f, (float) setInt, 109.0f);
            if (this.logic.myRectF != null) {
                canvas.drawRoundRect(this.logic.myRectF, 3.0f, 3.0f, paint);
            }
            paint.setColor(-1);
            String string = this.logic.res.getString(R.string.game_panel_setting_bg_music);
            int measureText = (int) paint.measureText(string);
            int i5 = i + 115;
            int i6 = 16 + 132;
            Tools.drawArtString(canvas, paint, string, i5, i6, -1, -16777216);
            this.logic.ctlSettingBgTurnerOn.setLocation(i5 + measureText + CARDS_MARGIN_V, i6 - 16);
            this.logic.ctlSettingBgTurnerOn.draw(canvas, paint);
            this.logic.ctlSettingBgTurnerOff.setLocation(i5 + measureText + CARDS_MARGIN_V, i6 - 16);
            this.logic.ctlSettingBgTurnerOff.draw(canvas, paint);
            String string2 = this.logic.res.getString(R.string.game_panel_setting_effect);
            int measureText2 = (int) paint.measureText(string2);
            int i7 = (i + 280) - CARDS_MARGIN_V;
            int i8 = 16 + 132;
            Tools.drawArtString(canvas, paint, string2, i7, i8, -1, -16777216);
            this.logic.ctlSettingEffectTurnerOn.setLocation(i7 + measureText2 + CARDS_MARGIN_V, i8 - 16);
            this.logic.ctlSettingEffectTurnerOn.draw(canvas, paint);
            this.logic.ctlSettingEffectTurnerOff.setLocation(i7 + measureText2 + CARDS_MARGIN_V, i8 - 16);
            this.logic.ctlSettingEffectTurnerOff.draw(canvas, paint);
            i3 = setInt;
            i4 = 105;
        } else if (controlGroup.type == 4) {
            this.logic.ctlSettingBright.setLocation(i, i2);
            this.logic.ctlSettingBright.draw(canvas, paint);
            paint.setColor(-256);
            i3 = this.logic.ctlSettingProgressBar.getSetInt(2, 0, false);
            i4 = 121;
            this.logic.myRectF.set(188.0f, 117.0f, (float) i3, 124.0f);
            if (this.logic.myRectF != null) {
                canvas.drawRoundRect(this.logic.myRectF, 3.0f, 3.0f, paint);
            }
        } else {
            if (controlGroup.type == 5) {
                this.logic.ctlSettingHint.setLocation(i, i2);
                this.logic.ctlSettingHint.draw(canvas, paint);
                paint.setColor(-1);
                String string3 = this.logic.res.getString(R.string.game_panel_setting_vibrate);
                int measureText3 = (int) paint.measureText(string3);
                int i9 = (i + 234) - measureText3;
                Tools.drawArtString(canvas, paint, string3, i9, 140, -1, -16777216);
                this.logic.ctlSettingVibrateTurnerOn.setLocation(i9 + measureText3 + CARDS_MARGIN_V, 140 - 16);
                this.logic.ctlSettingVibrateTurnerOn.draw(canvas, paint);
                this.logic.ctlSettingVibrateTurnerOff.setLocation(i9 + measureText3 + CARDS_MARGIN_V, 140 - 16);
                this.logic.ctlSettingVibrateTurnerOff.draw(canvas, paint);
            }
            i3 = 0;
            i4 = 0;
        }
        this.logic.ctlTabSound.setLocation(i + 115, i2 + 48, i + 115 + 72, i2 + 48 + 38);
        this.logic.ctlTabBright.setLocation(i + 188, i2 + 48, i + 188 + 72, i2 + 48 + 38);
        this.logic.ctlTabHint.setLocation(i + 265, i2 + 48, i + 265 + 72, i2 + 48 + 38);
        paint.setColor(-1);
        String string4 = this.logic.res.getString(R.string.game_panel_setting_game_sound);
        int i10 = ((i2 + 48) + 38) - 5;
        canvas.drawText(string4, (float) (i + 115 + ((72 - ((int) paint.measureText(string4))) / 2)), (float) i10, paint);
        String string5 = this.logic.res.getString(R.string.game_panel_setting_bright);
        canvas.drawText(string5, (float) (i + 188 + ((72 - ((int) paint.measureText(string5))) / 2)), (float) i10, paint);
        String string6 = this.logic.res.getString(R.string.game_panel_setting_vibrate_switch);
        canvas.drawText(string6, (float) (i + 265 + ((72 - ((int) paint.measureText(string6))) / 2)), (float) i10, paint);
        if (controlGroup.type == 4 || controlGroup.type == 3) {
            if (i3 == 0) {
                i3 = 188;
            }
            this.apPanelV1.drawFrame(canvas, 5, i3 - (this.apPanelV1.getFrameWidth(5) / 2), i4 - (this.apPanelV1.getFrameHeight(5) / 2));
        }
        paint.setColor(color);
    }

    private void drawTipsMessage(Canvas canvas, Paint paint) {
        int i;
        String str = this.logic.tipMessage != null ? this.logic.tipMessage : this.logic.continueTipMessage != null ? this.logic.continueTipMessage : null;
        if (str != null) {
            paint.setTextSize((float) 16);
            paint.setColor(-1614161950);
            int length = str.length();
            char[] charArray = str.toCharArray();
            int length2 = charArray.length;
            int i2 = 65;
            int i3 = 0;
            for (int i4 = 0; i4 < length; i4 = i) {
                int i5 = 0;
                int i6 = i4;
                int i7 = i3;
                while (true) {
                    if (i7 >= length2) {
                        i = i6;
                        break;
                    }
                    i5 = (int) (((float) i5) + paint.measureText(String.valueOf(charArray[i7])));
                    if (i5 > 250) {
                        i = i7 - 1;
                        break;
                    } else {
                        i6 = i7;
                        i7++;
                    }
                }
                if (i3 >= i) {
                    break;
                }
                canvas.drawText(str, i3, i + 1, (float) ((SCREEN_WIDTH - ((int) paint.measureText(str, i3, i + 1))) / 2), (float) i2, paint);
                int i8 = 16 + 2;
                i3 = i + 1;
                i2 += 18;
            }
            paint.setColor(-1);
        }
    }

    private final void drawUserInfoPannel(Canvas canvas, Paint paint, ControlGroup controlGroup) {
        int i = controlGroup.arg1;
        Player playerByOppositeSeatId = Table.getPlayerByOppositeSeatId((short) i, this.logic.table.playerList, this.logic.me.seatId);
        if (playerByOppositeSeatId == null) {
            this.logic.removeControlGroup(controlGroup);
            return;
        }
        int i2 = 0;
        switch (i) {
            case 0:
                i2 = 5;
                break;
            case 1:
                if (this.logic.curStatus != -2 && this.logic.curStatus != -3) {
                    i2 = 7;
                    break;
                } else {
                    i2 = 6;
                    break;
                }
                break;
            case 2:
                i2 = 4;
                break;
        }
        Rect frameBound = this.apPPanel.getFrameBound(i2);
        int i3 = frameBound.left;
        int i4 = frameBound.top;
        controlGroup.setRect(i3, i4, frameBound.right, frameBound.bottom);
        this.apPPanel.drawFrame(canvas, i2);
        int i5 = i3 + 20;
        paint.setTextSize((float) 12);
        paint.setColor(-1342177281);
        int i6 = i4 + 20 + 12;
        canvas.drawText(playerByOppositeSeatId.nickName, (float) i5, (float) i6, paint);
        if (playerByOppositeSeatId.isSuperPlayer()) {
            this.apTable.drawFrame(canvas, 5, ((int) paint.measureText(playerByOppositeSeatId.nickName)) + i5 + 4, i6 - 12);
        }
        int i7 = 2 + 12;
        int i8 = i6 + 14;
        canvas.drawText(this.logic.res.getString(R.string.info_qq) + playerByOppositeSeatId.uid, (float) i5, (float) i8, paint);
        int i9 = 2 + 12;
        int i10 = i8 + 14;
        String str = this.logic.res.getString(R.string.info_level) + UserInfoControl.getLevelName(playerByOppositeSeatId.point);
        canvas.drawText(str, (float) i5, (float) i10, paint);
        canvas.drawText(this.logic.res.getString(R.string.info_fen) + playerByOppositeSeatId.point, (float) (((int) paint.measureText(str)) + i5 + 6), (float) i10, paint);
        int i11 = 2 + 12;
        int i12 = i10 + 14;
        String str2 = this.logic.res.getString(R.string.info_winp) + ((int) playerByOppositeSeatId.winP) + "%";
        canvas.drawText(str2, (float) i5, (float) i12, paint);
        canvas.drawText(this.logic.res.getString(R.string.info_runp) + ((int) playerByOppositeSeatId.runP) + "%", (float) (((int) paint.measureText(str2)) + i5 + 6), (float) i12, paint);
        int i13 = 12 + 8;
        int i14 = i12 + 20;
        paint.setColor(-10370836);
        canvas.drawText(this.logic.locationInfo, (float) i5, (float) i14, paint);
        if (i != 1) {
            this.logic.ctlTick.setLocation(i5, i14 + 8);
            this.logic.ctlTick.draw(canvas, paint);
        }
    }

    private void drawWaitingView(Canvas canvas, Paint paint) {
        int i;
        boolean z = this.logic.isViewGame;
        int[] iArr = {424, 216, 4};
        int[] iArr2 = {4, 213, 4};
        int frameWidth = this.apTable.getFrameWidth(4);
        int width = this.imgNotifyInfo.getWidth();
        if (width > 120) {
            int i2 = width - this.Notify_Info_offsetX > 120 ? 120 + this.Notify_Info_offsetX : width;
            rect1.set(this.Notify_Info_offsetX, 0, i2, 15);
            rect2.set(178, 8, (i2 + 178) - this.Notify_Info_offsetX, 8 + 15);
            canvas.drawBitmap(this.imgNotifyInfo, rect1, rect2, paint);
            if (this.Notify_Info_offsetX < width) {
                this.Notify_Info_offsetX = this.Notify_Info_offsetX + 1;
            } else {
                this.Notify_Info_offsetX = 0;
            }
        } else {
            canvas.drawBitmap(this.imgNotifyInfo, (float) (178 + ((120 - width) / 2)), (float) 8, paint);
        }
        byte b = 0;
        while (b < 3) {
            short s = -1;
            this.logic.ctlPlayers[b].setLocation(iArr[b], iArr2[b]);
            this.logic.ctlPlayers[b].draw(canvas, paint);
            Player playerByOppositeSeatId = Table.getPlayerByOppositeSeatId((short) b, this.logic.table.playerList, this.logic.me.seatId);
            if (playerByOppositeSeatId != null) {
                s = playerByOppositeSeatId.iconId;
            }
            Tools.drawQQHead(canvas, paint, s, iArr[b] + 7, iArr2[b] + 7);
            if (playerByOppositeSeatId != null) {
                if (playerByOppositeSeatId.status == 3) {
                    this.apTable.drawFrame(canvas, 4, b == 0 ? (iArr[b] - 3) - frameWidth : iArr[b] + this.FRAME_SIZE + 3, ((iArr2[b] + this.FRAME_SIZE) - frameWidth) - 8);
                } else if (playerByOppositeSeatId == this.logic.me) {
                    int frameHeight = (iArr2[b] - this.apButton.getFrameHeight(12)) - 4;
                    this.logic.ctlStartGame.setLocation(iArr[b] - ((this.apButton.getFrameWidth(12) - this.FRAME_SIZE) / 2), frameHeight);
                    this.logic.ctlStartGame.draw(canvas, paint);
                    this.apClock.drawFrame(canvas, iArr[b] - ((31 - this.FRAME_SIZE) / 2), frameHeight - 31);
                }
                String str = playerByOppositeSeatId.nickName == null ? " " : playerByOppositeSeatId.nickName;
                paint.setTextSize((float) 16);
                paint.setColor(-1);
                int measureText = (int) paint.measureText(str);
                String levelName = UserInfoControl.getLevelName(playerByOppositeSeatId.point);
                if (b == 0) {
                    int i3 = iArr2[b] + this.FRAME_SIZE + 3 + 16;
                    canvas.drawText(str, (float) ((iArr[b] + this.FRAME_SIZE) - measureText), (float) i3, paint);
                    int measureText2 = (iArr[b] + this.FRAME_SIZE) - ((int) paint.measureText(levelName));
                    if (playerByOppositeSeatId.isSuperPlayer()) {
                        int frameWidth2 = measureText2 - (this.apTable.getFrameWidth(5) - 2);
                        this.apTable.drawFrame(canvas, 5, frameWidth2, i3 + 5);
                        measureText2 = frameWidth2 + this.apTable.getFrameWidth(5) + 2;
                    }
                    paint.setColor(-11425314);
                    canvas.drawText(levelName, (float) measureText2, (float) (i3 + 3 + 16), paint);
                } else {
                    int i4 = iArr2[b] + this.FRAME_SIZE + 3 + 16;
                    int i5 = iArr[b];
                    canvas.drawText(str, (float) iArr[b], (float) i4, paint);
                    if (playerByOppositeSeatId.isSuperPlayer()) {
                        this.apTable.drawFrame(canvas, 5, i5, i4 + 3);
                        i = this.apTable.getFrameWidth(5) + 2 + i5;
                    } else {
                        i = i5;
                    }
                    paint.setColor(-11425314);
                    canvas.drawText(levelName, (float) i, (float) (i4 + 3 + 16), paint);
                }
            }
            b = (byte) (b + 1);
        }
    }

    public void doDraw(Canvas canvas) {
        Paint paint = paint;
        canvas.drawBitmap(this.imgBg, 0.0f, 0.0f, paint);
        if (this.aniManager.toolBarPlayer != null && !this.aniManager.toolBarPlayer.update(this.SPF)) {
            canvas.drawBitmap(this.imgToolbarBg, 60.0f, 0.0f, paint);
            this.logic.ctlChat.draw(canvas, paint);
            this.logic.ctlTrusteeship.draw(canvas, paint);
            this.logic.ctlSetting.draw(canvas, paint);
            this.logic.ctlQuit.draw(canvas, paint);
        }
        drawDigitalTimer(canvas, paint, 410, 181);
        if (this.logic.curStatus == 0) {
            drawWaitingView(canvas, paint);
        } else if (this.logic.curStatus == -2 || this.logic.curStatus == -3) {
            drawPlayingView(canvas, paint);
        }
        drawAnimation(canvas, paint);
        drawTipsMessage(canvas, paint);
        drawChatMessage(canvas, paint);
        int size = this.logic.controlGroups.size();
        for (byte b = 1; b < size; b = (byte) (b + 1)) {
            ControlGroup controlGroup = (ControlGroup) this.logic.controlGroups.elementAt(b);
            if (controlGroup != null && controlGroup.size() > 0) {
                switch (controlGroup.type) {
                    case 3:
                    case 4:
                    case 5:
                        drawSettingPannel(canvas, paint, controlGroup, 0, 0);
                        continue;
                    case 6:
                        drawQuitPannel(canvas, paint, controlGroup);
                        continue;
                    case 7:
                    case 8:
                    case 9:
                        drawCheckPannel(canvas, paint, controlGroup, 130, 90);
                        continue;
                    case 10:
                        drawUserInfoPannel(canvas, paint, controlGroup);
                        continue;
                    case 11:
                        if (this.chatDlg != null) {
                            this.chatDlg.draw(canvas, paint);
                            break;
                        } else {
                            continue;
                        }
                }
            }
        }
    }

    public void drawAnimation(Canvas canvas, Paint paint) {
        if (this.aniManager != null) {
            this.aniManager.draw(canvas, this.SPF);
        }
    }

    public void drawCheckPannel(Canvas canvas, Paint paint, ControlGroup controlGroup, int i, int i2) {
        int i3;
        controlGroup.setRect(i, i2, this.apPPanel.getFrameWidth(8) + i, this.apPPanel.getFrameHeight(8) + i2);
        this.apPPanel.drawFrame(canvas, 8, i, i2);
        paint.setColor(-1775639);
        paint.setTextSize((float) 15);
        canvas.drawText(this.logic.res.getString(R.string.game_tips_title), (float) (i + 93), (float) (i2 + 10 + 15), paint);
        String str = (String) controlGroup.obj;
        if (str != null) {
            paint.setTextSize((float) 15);
            int i4 = i + 12;
            int length = str.length();
            char[] charArray = str.toCharArray();
            int length2 = charArray.length;
            int i5 = 0;
            int i6 = i2 + 45;
            for (int i7 = 0; i7 < length; i7 = i3) {
                int i8 = 0;
                int i9 = i7;
                int i10 = i5;
                while (true) {
                    if (i10 >= length2) {
                        i3 = i9;
                        break;
                    }
                    i8 = (int) (((float) i8) + paint.measureText(String.valueOf(charArray[i10])));
                    if (i8 > 195) {
                        i3 = i10 - 1;
                        break;
                    }
                    i9 = i10;
                    i10++;
                }
                if (i5 >= i3) {
                    break;
                }
                canvas.drawText(str, i5, i3 + 1, (float) i4, (float) i6, paint);
                int i11 = 15 + 4;
                i5 = i3 + 1;
                i6 += 19;
            }
            if (controlGroup.type == 7) {
                this.logic.ctlCancel.setLocation(i + 20, i2 + 90);
                this.logic.ctlCancel.draw(canvas, paint);
                this.logic.ctlOK.setLocation(i + 116, i2 + 90);
                this.logic.ctlOK.draw(canvas, paint);
            } else if (controlGroup.type == 8) {
                this.logic.ctlOK.setLocation(i + 68, i2 + 90);
                this.logic.ctlOK.draw(canvas, paint);
            } else if (controlGroup.type == 9) {
                this.logic.ctlOK.setLocation(i + 68, i2 + 90);
                this.apButton.drawFrame(canvas, i + 68, i2 + 90);
            }
        }
    }

    public final Bitmap drawFrame2Bitmap(AnimationPlayerV1 animationPlayerV1, int i) {
        if (animationPlayerV1 == null || i < 0) {
            return null;
        }
        int frameWidth = animationPlayerV1.getFrameWidth(i);
        int frameHeight = animationPlayerV1.getFrameHeight(i);
        Canvas canvas = new Canvas();
        Bitmap createBitmap = Bitmap.createBitmap(frameWidth, frameHeight, Bitmap.Config.ARGB_4444);
        canvas.setBitmap(createBitmap);
        animationPlayerV1.drawFrame(canvas, i, 0, 0);
        return createBitmap;
    }

    public Bitmap drawMyCardImg(Card card) {
        Canvas canvas = new Canvas();
        Bitmap createBitmap = Bitmap.createBitmap(this.CARD_WIDTH_L, this.CARD_HEIGHT_L, Bitmap.Config.ARGB_8888);
        canvas.setBitmap(createBitmap);
        this.apCards.drawFrame(canvas, (card.number + 0) - 1, 0, 0);
        return createBitmap;
    }

    public void recycle() {
        if (this.imgBg != null) {
            if (!this.imgBg.isRecycled()) {
                this.imgBg.recycle();
            }
            this.imgBg = null;
        }
        if (this.imgToolbarBg != null) {
            if (!this.imgToolbarBg.isRecycled()) {
                this.imgToolbarBg.recycle();
            }
            this.imgToolbarBg = null;
        }
        if (imgNums != null) {
            if (!imgNums.isRecycled()) {
                imgNums.recycle();
            }
            imgNums = null;
        }
        if (this.imgNumsAdd != null) {
            if (!this.imgNumsAdd.isRecycled()) {
                this.imgNumsAdd.recycle();
            }
            this.imgNumsAdd = null;
        }
        if (this.imgNumsMinus != null) {
            if (!this.imgNumsMinus.isRecycled()) {
                this.imgNumsMinus.recycle();
            }
            this.imgNumsMinus = null;
        }
        if (this.imgTimerNumber != null) {
            if (!this.imgTimerNumber.isRecycled()) {
                this.imgTimerNumber.recycle();
            }
            this.imgTimerNumber = null;
        }
        if (this.imgTimer != null) {
            if (!this.imgTimer.isRecycled()) {
                this.imgTimer.recycle();
            }
            this.imgTimer = null;
        }
        if (this.imgFrame != null) {
            if (!this.imgFrame.isRecycled()) {
                this.imgFrame.recycle();
            }
            this.imgFrame = null;
        }
        if (this.imgTuoGuan != null) {
            if (!this.imgTuoGuan.isRecycled()) {
                this.imgTuoGuan.recycle();
            }
            this.imgTuoGuan = null;
        }
        if (this.imgLord != null) {
            if (!this.imgLord.isRecycled()) {
                this.imgLord.recycle();
            }
            this.imgLord = null;
        }
        if (this.imgFarmer != null) {
            if (!this.imgFarmer.isRecycled()) {
                this.imgFarmer.recycle();
            }
            this.imgFarmer = null;
        }
        if (this.imgLeftCardNums != null) {
            for (int i = 0; i < this.imgLeftCardNums.length; i++) {
                if (this.imgLeftCardNums[i] != null) {
                    if (!this.imgLeftCardNums[i].isRecycled()) {
                        this.imgLeftCardNums[i].recycle();
                    }
                    this.imgLeftCardNums[i] = null;
                }
            }
        }
    }
}
