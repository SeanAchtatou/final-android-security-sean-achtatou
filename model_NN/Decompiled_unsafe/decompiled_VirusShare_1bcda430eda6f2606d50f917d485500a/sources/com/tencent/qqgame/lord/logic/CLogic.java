package com.tencent.qqgame.lord.logic;

import com.tencent.qqgame.common.Card;
import com.tencent.qqgame.common.DDZCards;
import com.tencent.qqgame.common.LordCard;
import com.tencent.qqgame.common.SearchHint;
import com.tencent.qqgame.hall.common.Tools;
import java.util.Hashtable;
import java.util.Vector;

public class CLogic {
    public static final int BOMB = 13;
    public static final int FOUR_ONE = 11;
    public static final int FOUR_TWO = 12;
    public static final int MULTI_THREE_ONE = 10;
    public static final int MULTI_THREE_TWO = 9;
    public static final int NOCARD = 99;
    public static final int NOTHING = 0;
    public static final int ONE = 1;
    public static final int ONE_TURN = 6;
    public static final int ROCKET = 14;
    public static final int THREE = 3;
    public static final int THREE_ONE = 4;
    public static final int THREE_TURN = 8;
    public static final int THREE_TWO = 5;
    public static final int TWO = 2;
    public static final int TWO_TURN = 7;
    private static final Vector<LordCard> allLordCards = new Vector<>();
    static byte[] cardIndex;
    public static Vector[] cardIndexVector;
    public static int[] cardListIndex;
    public static int[] cardListLen;
    static byte[] cardNum;
    static byte[] cardValue;
    public static int currentMaxCardType;
    private static Hashtable<Integer, Integer> h_cardIndex = new Hashtable<>();
    private static Hashtable<Integer, Integer> h_cardType = new Hashtable<>();
    public static boolean isAutoCard = false;
    public static boolean isFirstSelect;
    static boolean isHaveBomb;
    private static final Vector<LordCard> lastPlayerOutLordCards = new Vector<>();
    static SearchHint m_SearchHint = new SearchHint();
    public static int maxCardTypeOpponent;
    public static int maxValueOpponent;
    public static int max_type;
    private static int max_type_num;
    private static final Vector<LordCard> myLordCards = new Vector<>();
    public static int outCardType;
    static byte[] rocketIndex;
    private static final Vector<LordCard> selectedLordCards = new Vector<>();
    public static int sizeCardOpponent;
    public static int typeOpponent;

    private static void Cards2LordCards(Vector<Card> vector, Vector<LordCard> vector2) {
        vector2.removeAllElements();
        int size = vector.size();
        for (int i = 0; i < size; i++) {
            LordCard lordCard = getLordCard(vector.elementAt(i).number);
            lordCard.selected = false;
            vector2.add(lordCard);
        }
    }

    private static boolean CardsJudge(Hashtable<Integer, Integer> hashtable, int i) {
        int i2;
        int i3;
        int i4;
        int[] iArr = new int[15];
        int i5 = 1;
        int i6 = 0;
        int i7 = 0;
        while (i5 < 14) {
            Integer num = new Integer(i5);
            Integer num2 = hashtable.get(num);
            if (num2 == null || num2.intValue() != i) {
                i2 = i6;
                i3 = i7;
            } else if (num.intValue() == 2) {
                return false;
            } else {
                if (1 == i5) {
                    iArr[i6] = 14;
                    i4 = 14;
                } else {
                    iArr[i6] = i5;
                    i4 = i7;
                }
                int i8 = i6 + 1;
                if (i5 > i4) {
                    i2 = i8;
                    i3 = i5;
                } else {
                    int i9 = i8;
                    i3 = i4;
                    i2 = i9;
                }
            }
            i5++;
            i7 = i3;
            i6 = i2;
        }
        boolean z = false;
        int i10 = i6;
        while (i10 > 1) {
            int i11 = i7 - 1;
            boolean z2 = false;
            for (int i12 = 0; i12 < i6; i12++) {
                if (iArr[i12] == i11) {
                    z2 = true;
                }
            }
            if (!z2) {
                return z2;
            }
            i10--;
            boolean z3 = z2;
            i7 = i11;
            z = z3;
        }
        return z;
    }

    public static Vector CardsSort(Vector vector) {
        int i;
        int i2;
        int i3;
        if (vector.size() <= 0) {
            return vector;
        }
        Vector vector2 = new Vector(vector.size());
        int i4 = 15;
        while (true) {
            int i5 = i4;
            if (i5 < 14) {
                break;
            }
            int i6 = 0;
            while (i6 < vector.size()) {
                LordCard lordCard = (LordCard) vector.elementAt(i6);
                if (lordCard.value == i5) {
                    vector2.addElement(lordCard);
                    vector.removeElementAt(i6);
                    i3 = i6 - 1;
                } else {
                    i3 = i6;
                }
                i6 = i3 + 1;
            }
            i4 = i5 - 1;
        }
        int i7 = 2;
        while (true) {
            int i8 = i7;
            if (i8 < 1) {
                break;
            }
            int i9 = 0;
            while (i9 < vector.size()) {
                LordCard lordCard2 = (LordCard) vector.elementAt(i9);
                if (lordCard2.value == i8) {
                    vector2.insertElementAt(lordCard2, vector2.size() - sortColor(lordCard2, vector2));
                    vector.removeElementAt(i9);
                    i2 = i9 - 1;
                } else {
                    i2 = i9;
                }
                i9 = i2 + 1;
            }
            i7 = i8 - 1;
        }
        int i10 = 13;
        while (true) {
            int i11 = i10;
            if (i11 < 3) {
                return vector2;
            }
            int i12 = 0;
            while (i12 < vector.size()) {
                LordCard lordCard3 = (LordCard) vector.elementAt(i12);
                if (lordCard3.value == i11) {
                    vector2.insertElementAt(lordCard3, vector2.size() - sortColor(lordCard3, vector2));
                    vector.removeElementAt(i12);
                    i = i12 - 1;
                } else {
                    i = i12;
                }
                i12 = i + 1;
            }
            i10 = i11 - 1;
        }
    }

    public static boolean Compare(Vector<Card> vector, Vector<Card> vector2) {
        if (vector2 == null || vector2.size() == 0) {
            return false;
        }
        int cardType = getCardType(vector2);
        int leadCardValue = getLeadCardValue(max_type);
        if (vector == null || vector.size() == 0) {
            return (cardType == 0 || cardType == 99) ? false : true;
        }
        int cardType2 = getCardType(vector);
        int leadCardValue2 = getLeadCardValue(max_type);
        if (cardType2 == 14) {
            return false;
        }
        if (cardType == 14) {
            return true;
        }
        if (cardType2 == cardType) {
            if (vector.size() != vector2.size()) {
                return false;
            }
            if (LeadCardsValueCompare(leadCardValue2, leadCardValue)) {
                return true;
            }
        } else if (cardType == 13) {
            return true;
        }
        return false;
    }

    public static void GetNextHint(LordLogic lordLogic, Vector<Card> vector, Vector<Card> vector2, Vector<Card> vector3) {
        int i;
        myLordCards.removeAllElements();
        int size = vector2.size();
        for (int i2 = 0; i2 < size; i2++) {
            LordCard lordCard = getLordCard(vector2.elementAt(i2).number);
            lordCard.selected = false;
            myLordCards.add(lordCard);
        }
        selectedLordCards.removeAllElements();
        int size2 = vector.size();
        for (int i3 = 0; i3 < size2; i3++) {
            LordCard lordCard2 = getLordCard(vector.elementAt(i3).number);
            lordCard2.selected = true;
            selectedLordCards.add(lordCard2);
        }
        lastPlayerOutLordCards.removeAllElements();
        if (vector3 != null) {
            int size3 = vector3.size();
            for (int i4 = 0; i4 < size3; i4++) {
                lastPlayerOutLordCards.add(getLordCard(vector3.elementAt(i4).number));
            }
        }
        if (!SearchHints(false, null, null)) {
            int size4 = vector.size();
            for (byte b = 0; b < size4; b = (byte) (b + 1)) {
                vector.elementAt(b).isSelected = false;
            }
            vector.removeAllElements();
            return;
        }
        selectedLordCards.removeAllElements();
        for (int i5 = 0; i5 < myLordCards.size(); i5++) {
            myLordCards.elementAt(i5).selected = false;
        }
        if (m_SearchHint.GetHintCount() != 0) {
            DDZCards GetNextHint = m_SearchHint.GetNextHint();
            System.out.println("tempCards.getCurrentLength()=" + GetNextHint.getCurrentLength());
            int i6 = 0;
            int i7 = 0;
            while (i7 < GetNextHint.getCurrentLength()) {
                int i8 = 0;
                while (true) {
                    if (i8 < myLordCards.size()) {
                        if (GetNextHint.getValueByIndex(i7) == myLordCards.elementAt(i8).value && !myLordCards.elementAt(i8).selected) {
                            myLordCards.elementAt(i8).selected = true;
                            i = i6 + 1;
                            break;
                        }
                        i8++;
                    } else {
                        i = i6;
                        break;
                    }
                }
                i7++;
                i6 = i;
            }
            int i9 = 0;
            while (i9 < myLordCards.size() && !myLordCards.elementAt(i9).selected) {
                i9++;
            }
            selectedLordCards.removeAllElements();
            for (int i10 = 0; i10 < myLordCards.size(); i10++) {
                LordCard elementAt = myLordCards.elementAt(i10);
                if (elementAt.selected) {
                    selectedLordCards.addElement(elementAt);
                }
            }
            int size5 = vector.size();
            for (int i11 = 0; i11 < size5; i11++) {
                vector.elementAt(i11).isSelected = false;
            }
            vector.removeAllElements();
            int size6 = selectedLordCards.size();
            boolean z = true;
            int i12 = 0;
            while (i12 < size6) {
                if (selectedLordCards.elementAt(i12).selected) {
                    Card card = lordLogic.getCard(selectedLordCards.elementAt(i12).number);
                    card.isSelected = true;
                    if (z) {
                        lordLogic.setMyCardsFocused((byte) vector2.indexOf(card));
                        z = false;
                    } else {
                        card.isFocused = false;
                    }
                    vector.add(card);
                }
                i12++;
                z = z;
            }
        }
    }

    private static boolean LeadCardsValueCompare(int i, int i2) {
        return ((i2 <= 2 || i2 >= 14) ? i2 : i2 - 100) > ((i <= 2 || i >= 14) ? i : i - 100);
    }

    static boolean SearchHints(boolean z, Vector<Card> vector, Vector<Card> vector2) {
        if (z) {
            myLordCards.removeAllElements();
            if (vector != null) {
                int size = vector.size();
                for (int i = 0; i < size; i++) {
                    LordCard lordCard = getLordCard(vector.elementAt(i).number);
                    lordCard.selected = false;
                    myLordCards.add(lordCard);
                }
            }
            lastPlayerOutLordCards.removeAllElements();
            if (!(vector2 == null || vector2 == null)) {
                int size2 = vector2.size();
                for (int i2 = 0; i2 < size2; i2++) {
                    lastPlayerOutLordCards.add(getLordCard(vector2.elementAt(i2).number));
                }
            }
        }
        try {
            if (lastPlayerOutLordCards.size() == 0) {
                System.out.println("SearchHints轮到自己打牌");
                return false;
            }
            SearchHint searchHint = m_SearchHint;
            if (!SearchHint.haveSearched) {
                new Vector();
                m_SearchHint.SearchHint(myLordCards, myLordCards.size(), lastPlayerOutLordCards, lastPlayerOutLordCards.size());
                SearchHint searchHint2 = m_SearchHint;
                SearchHint.haveSearched = true;
                System.out.println("SearchHints本轮已经搜索");
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void SelectPokerAI(LordLogic lordLogic, Vector<Card> vector, Vector<Card> vector2, Vector<Card> vector3) {
        boolean z;
        myLordCards.removeAllElements();
        selectedLordCards.removeAllElements();
        lastPlayerOutLordCards.removeAllElements();
        if (vector != null && vector.size() > 0 && vector2 != null && vector2.size() > 0) {
            int size = vector2.size();
            for (int i = 0; i < size; i++) {
                LordCard lordCard = getLordCard(vector2.elementAt(i).number);
                lordCard.selected = false;
                myLordCards.add(lordCard);
            }
            int size2 = vector.size();
            for (int i2 = 0; i2 < size2; i2++) {
                LordCard lordCard2 = getLordCard(vector.elementAt(i2).number);
                lordCard2.selected = true;
                selectedLordCards.add(lordCard2);
            }
            if (vector3 != null) {
                int size3 = vector3.size();
                for (int i3 = 0; i3 < size3; i3++) {
                    lastPlayerOutLordCards.add(getLordCard(vector3.elementAt(i3).number));
                }
            }
            if (lastPlayerOutLordCards.size() == 0) {
                SearchHint.FirstOutSelectAI(selectedLordCards, myLordCards, myLordCards.size());
            } else {
                SearchHint.SelectAI(selectedLordCards, myLordCards, myLordCards.size(), lastPlayerOutLordCards, lastPlayerOutLordCards.size());
            }
            if (m_SearchHint.FindFitPokers(myLordCards, myLordCards.size(), lastPlayerOutLordCards, lastPlayerOutLordCards.size())) {
                int size4 = selectedLordCards.size();
                for (int i4 = 0; i4 < size4; i4++) {
                    selectedLordCards.elementAt(i4).selected = false;
                }
                DDZCards GetSelectHint = m_SearchHint.GetSelectHint();
                int currentLength = GetSelectHint.getCurrentLength();
                for (int i5 = 0; i5 < currentLength; i5++) {
                    int i6 = 0;
                    while (true) {
                        if (i6 < size4) {
                            if (GetSelectHint.getValueByIndex(i5) == selectedLordCards.elementAt(i6).value && !selectedLordCards.elementAt(i6).selected) {
                                selectedLordCards.elementAt(i6).selected = true;
                                z = true;
                                break;
                            }
                            i6++;
                        } else {
                            z = false;
                            break;
                        }
                    }
                    int i7 = 0;
                    while (true) {
                        if (i7 >= myLordCards.size()) {
                            break;
                        }
                        if (!z) {
                            if (GetSelectHint.getValueByIndex(i5) == myLordCards.elementAt(i7).value && !myLordCards.elementAt(i7).selected) {
                                myLordCards.elementAt(i7).selected = true;
                                break;
                            }
                        } else if (myLordCards.elementAt(i7).number == selectedLordCards.elementAt(i6).number) {
                            myLordCards.elementAt(i7).selected = true;
                        }
                        i7++;
                    }
                }
                System.out.println("LordLayout.selectCard.size()=" + selectedLordCards.size());
                for (int i8 = 0; i8 < selectedLordCards.size(); i8++) {
                    System.out.println("number=" + selectedLordCards.elementAt(i8).value);
                }
                selectedLordCards.removeAllElements();
                for (int i9 = 0; i9 < myLordCards.size(); i9++) {
                    LordCard elementAt = myLordCards.elementAt(i9);
                    if (elementAt.selected) {
                        selectedLordCards.addElement(elementAt);
                    }
                }
            }
            Tools.debug("selectedCards size: " + vector.size());
            int size5 = selectedLordCards.size();
            int i10 = 0;
            int i11 = 0;
            while (i10 < size5) {
                if (selectedLordCards.elementAt(i10).selected) {
                    i11++;
                    Card card = lordLogic.getCard(selectedLordCards.elementAt(i10).number);
                    if (!vector.contains(card)) {
                        card.isSelected = true;
                        vector.add(card);
                    }
                }
                i10++;
                i11 = i11;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0080, code lost:
        if (r14.size() >= 4) goto L_0x0082;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00c9, code lost:
        if (r1 < 2) goto L_0x000f;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void SystemGetOutCard(java.util.Vector<com.tencent.qqgame.common.Card> r14) {
        /*
            r13 = 4
            r12 = 3
            r11 = 2
            r10 = 1
            r9 = 0
            resetOutCardIndex()
            com.tencent.qqgame.lord.logic.CLogic.outCardType = r9
            int r0 = com.tencent.qqgame.lord.logic.CLogic.typeOpponent
            switch(r0) {
                case 1: goto L_0x003f;
                case 2: goto L_0x003f;
                case 3: goto L_0x003f;
                case 4: goto L_0x006f;
                case 5: goto L_0x006f;
                case 6: goto L_0x0134;
                case 7: goto L_0x0134;
                case 8: goto L_0x0134;
                case 9: goto L_0x0134;
                case 10: goto L_0x0134;
                case 11: goto L_0x006f;
                case 12: goto L_0x006f;
                case 13: goto L_0x000f;
                case 14: goto L_0x0320;
                case 99: goto L_0x000f;
                default: goto L_0x000f;
            }
        L_0x000f:
            r0 = r9
        L_0x0010:
            byte[] r1 = com.tencent.qqgame.lord.logic.CLogic.cardValue
            int r1 = r1.length
            if (r0 >= r1) goto L_0x0320
            byte[] r1 = com.tencent.qqgame.lord.logic.CLogic.cardNum
            byte r1 = r1[r0]
            if (r1 < r13) goto L_0x02f6
            r1 = r10
            r2 = r9
        L_0x001d:
            java.util.Vector[] r3 = com.tencent.qqgame.lord.logic.CLogic.cardIndexVector
            r3 = r3[r12]
            int r3 = r3.size()
            if (r2 >= r3) goto L_0x02e4
            java.util.Vector[] r3 = com.tencent.qqgame.lord.logic.CLogic.cardIndexVector
            r3 = r3[r12]
            java.lang.Object r14 = r3.elementAt(r2)
            java.lang.Integer r14 = (java.lang.Integer) r14
            int r3 = r14.intValue()
            byte[] r4 = com.tencent.qqgame.lord.logic.CLogic.cardIndex
            byte r4 = r4[r0]
            if (r3 != r4) goto L_0x003c
            r1 = r9
        L_0x003c:
            int r2 = r2 + 1
            goto L_0x001d
        L_0x003f:
            r0 = r9
        L_0x0040:
            byte[] r1 = com.tencent.qqgame.lord.logic.CLogic.cardValue
            int r1 = r1.length
            if (r0 >= r1) goto L_0x000f
            byte[] r1 = com.tencent.qqgame.lord.logic.CLogic.cardNum
            byte r1 = r1[r0]
            int r2 = com.tencent.qqgame.lord.logic.CLogic.typeOpponent
            if (r1 < r2) goto L_0x006c
            int r1 = com.tencent.qqgame.lord.logic.CLogic.maxValueOpponent
            byte[] r2 = com.tencent.qqgame.lord.logic.CLogic.cardValue
            byte r2 = r2[r0]
            boolean r1 = LeadCardsValueCompare(r1, r2)
            if (r1 == 0) goto L_0x006c
            java.util.Vector[] r1 = com.tencent.qqgame.lord.logic.CLogic.cardIndexVector
            int r2 = com.tencent.qqgame.lord.logic.CLogic.typeOpponent
            int r2 = r2 - r10
            r1 = r1[r2]
            java.lang.Integer r2 = new java.lang.Integer
            byte[] r3 = com.tencent.qqgame.lord.logic.CLogic.cardIndex
            byte r3 = r3[r0]
            r2.<init>(r3)
            r1.addElement(r2)
        L_0x006c:
            int r0 = r0 + 1
            goto L_0x0040
        L_0x006f:
            int r0 = com.tencent.qqgame.lord.logic.CLogic.typeOpponent
            r1 = 11
            if (r0 >= r1) goto L_0x00cd
            int r0 = com.tencent.qqgame.lord.logic.CLogic.typeOpponent
            int r0 = r0 - r12
            int r1 = com.tencent.qqgame.lord.logic.CLogic.typeOpponent
            if (r1 != r13) goto L_0x00b0
            int r1 = r14.size()
            if (r1 < r13) goto L_0x000f
        L_0x0082:
            r1 = r12
        L_0x0083:
            r2 = r9
        L_0x0084:
            byte[] r3 = com.tencent.qqgame.lord.logic.CLogic.cardValue
            int r3 = r3.length
            if (r2 >= r3) goto L_0x0108
            byte[] r3 = com.tencent.qqgame.lord.logic.CLogic.cardNum
            byte r3 = r3[r2]
            if (r3 < r1) goto L_0x00ad
            int r3 = com.tencent.qqgame.lord.logic.CLogic.maxValueOpponent
            byte[] r4 = com.tencent.qqgame.lord.logic.CLogic.cardValue
            byte r4 = r4[r2]
            boolean r3 = LeadCardsValueCompare(r3, r4)
            if (r3 == 0) goto L_0x00ad
            java.util.Vector[] r3 = com.tencent.qqgame.lord.logic.CLogic.cardIndexVector
            int r4 = r1 - r10
            r3 = r3[r4]
            java.lang.Integer r4 = new java.lang.Integer
            byte[] r5 = com.tencent.qqgame.lord.logic.CLogic.cardIndex
            byte r5 = r5[r2]
            r4.<init>(r5)
            r3.addElement(r4)
        L_0x00ad:
            int r2 = r2 + 1
            goto L_0x0084
        L_0x00b0:
            int r1 = r14.size()
            r2 = 5
            if (r1 < r2) goto L_0x000f
            r1 = r9
            r2 = r9
        L_0x00b9:
            byte[] r3 = com.tencent.qqgame.lord.logic.CLogic.cardValue
            int r3 = r3.length
            if (r2 >= r3) goto L_0x00c9
            byte[] r3 = com.tencent.qqgame.lord.logic.CLogic.cardNum
            byte r3 = r3[r2]
            if (r3 < r11) goto L_0x00c6
            int r1 = r1 + 1
        L_0x00c6:
            int r2 = r2 + 1
            goto L_0x00b9
        L_0x00c9:
            if (r1 >= r11) goto L_0x0082
            goto L_0x000f
        L_0x00cd:
            int r0 = com.tencent.qqgame.lord.logic.CLogic.typeOpponent
            r1 = 10
            int r0 = r0 - r1
            int r1 = com.tencent.qqgame.lord.logic.CLogic.typeOpponent
            r2 = 11
            if (r1 != r2) goto L_0x00e5
            int r1 = r14.size()
            r2 = 6
            if (r1 < r2) goto L_0x000f
            int[] r1 = com.tencent.qqgame.lord.logic.CLogic.cardListLen
            r1[r9] = r11
            r1 = r13
            goto L_0x0083
        L_0x00e5:
            int r1 = r14.size()
            r2 = 8
            if (r1 < r2) goto L_0x000f
            r1 = r9
            r2 = r9
        L_0x00ef:
            byte[] r3 = com.tencent.qqgame.lord.logic.CLogic.cardValue
            int r3 = r3.length
            if (r2 >= r3) goto L_0x00ff
            byte[] r3 = com.tencent.qqgame.lord.logic.CLogic.cardNum
            byte r3 = r3[r2]
            if (r3 < r11) goto L_0x00fc
            int r1 = r1 + 1
        L_0x00fc:
            int r2 = r2 + 1
            goto L_0x00ef
        L_0x00ff:
            if (r1 < r12) goto L_0x000f
            int[] r1 = com.tencent.qqgame.lord.logic.CLogic.cardListLen
            r1[r10] = r11
            r1 = r13
            goto L_0x0083
        L_0x0108:
            java.util.Vector[] r2 = com.tencent.qqgame.lord.logic.CLogic.cardIndexVector
            int r1 = r1 - r10
            r1 = r2[r1]
            int r1 = r1.size()
            if (r1 <= 0) goto L_0x000f
            r1 = r9
        L_0x0114:
            byte[] r2 = com.tencent.qqgame.lord.logic.CLogic.cardValue
            int r2 = r2.length
            if (r1 >= r2) goto L_0x000f
            byte[] r2 = com.tencent.qqgame.lord.logic.CLogic.cardNum
            byte r2 = r2[r1]
            if (r2 < r0) goto L_0x0131
            java.util.Vector[] r2 = com.tencent.qqgame.lord.logic.CLogic.cardIndexVector
            int r3 = r0 - r10
            r2 = r2[r3]
            java.lang.Integer r3 = new java.lang.Integer
            byte[] r4 = com.tencent.qqgame.lord.logic.CLogic.cardIndex
            byte r4 = r4[r1]
            r3.<init>(r4)
            r2.addElement(r3)
        L_0x0131:
            int r1 = r1 + 1
            goto L_0x0114
        L_0x0134:
            int r0 = com.tencent.qqgame.lord.logic.CLogic.typeOpponent
            r1 = 9
            if (r0 < r1) goto L_0x01b0
            int[] r0 = com.tencent.qqgame.lord.logic.CLogic.cardListLen
            r0[r11] = r11
            int r0 = com.tencent.qqgame.lord.logic.CLogic.typeOpponent
            r1 = 10
            if (r0 != r1) goto L_0x0192
            int r0 = r14.size()
            r1 = 8
            if (r0 < r1) goto L_0x000f
            r1 = r12
        L_0x014d:
            byte[] r0 = com.tencent.qqgame.lord.logic.CLogic.cardValue
            int r0 = r0.length
            int[] r2 = com.tencent.qqgame.lord.logic.CLogic.cardListLen
            int r3 = r1 - r10
            r2 = r2[r3]
            if (r0 < r2) goto L_0x0278
            java.util.Vector r2 = new java.util.Vector
            r2.<init>()
            r0 = r9
        L_0x015e:
            byte[] r3 = com.tencent.qqgame.lord.logic.CLogic.cardValue
            int r3 = r3.length
            if (r0 >= r3) goto L_0x01bf
            byte[] r3 = com.tencent.qqgame.lord.logic.CLogic.cardNum
            byte r3 = r3[r0]
            if (r3 < r1) goto L_0x018f
            byte[] r3 = com.tencent.qqgame.lord.logic.CLogic.cardValue
            byte r3 = r3[r0]
            if (r3 == r11) goto L_0x018f
            byte[] r3 = com.tencent.qqgame.lord.logic.CLogic.cardValue
            byte r3 = r3[r0]
            r4 = 14
            if (r3 >= r4) goto L_0x018f
            com.tencent.qqgame.common.LordCard r3 = new com.tencent.qqgame.common.LordCard
            byte[] r4 = com.tencent.qqgame.lord.logic.CLogic.cardValue
            byte r4 = r4[r0]
            r3.<init>(r4)
            byte[] r4 = com.tencent.qqgame.lord.logic.CLogic.cardValue
            byte r4 = r4[r0]
            r3.value = r4
            byte[] r4 = com.tencent.qqgame.lord.logic.CLogic.cardIndex
            byte r4 = r4[r0]
            r3.index = r4
            r2.addElement(r3)
        L_0x018f:
            int r0 = r0 + 1
            goto L_0x015e
        L_0x0192:
            int r0 = r14.size()
            r1 = 10
            if (r0 < r1) goto L_0x000f
            r0 = r9
            r1 = r9
        L_0x019c:
            byte[] r2 = com.tencent.qqgame.lord.logic.CLogic.cardValue
            int r2 = r2.length
            if (r1 >= r2) goto L_0x01ac
            byte[] r2 = com.tencent.qqgame.lord.logic.CLogic.cardNum
            byte r2 = r2[r1]
            if (r2 < r11) goto L_0x01a9
            int r0 = r0 + 1
        L_0x01a9:
            int r1 = r1 + 1
            goto L_0x019c
        L_0x01ac:
            if (r0 < r13) goto L_0x000f
            r1 = r12
            goto L_0x014d
        L_0x01b0:
            int r0 = com.tencent.qqgame.lord.logic.CLogic.typeOpponent
            r1 = 5
            int r0 = r0 - r1
            int[] r1 = com.tencent.qqgame.lord.logic.CLogic.cardListLen
            int r2 = r0 - r10
            int r3 = com.tencent.qqgame.lord.logic.CLogic.sizeCardOpponent
            int r3 = r3 / r0
            r1[r2] = r3
            r1 = r0
            goto L_0x014d
        L_0x01bf:
            java.util.Vector r0 = CardsSort(r2)
            r2.removeAllElements()
            int r3 = r0.size()
            int r3 = r3 - r10
        L_0x01cb:
            if (r3 < 0) goto L_0x01d7
            java.lang.Object r4 = r0.elementAt(r3)
            r2.addElement(r4)
            int r3 = r3 + -1
            goto L_0x01cb
        L_0x01d7:
            int r0 = r2.size()
            int[] r3 = com.tencent.qqgame.lord.logic.CLogic.cardListLen
            int r4 = r1 - r10
            r3 = r3[r4]
            if (r0 < r3) goto L_0x0278
            r3 = r9
        L_0x01e4:
            int r0 = r2.size()
            if (r3 >= r0) goto L_0x0278
            java.lang.Object r14 = r2.elementAt(r3)
            com.tencent.qqgame.common.LordCard r14 = (com.tencent.qqgame.common.LordCard) r14
            int r0 = com.tencent.qqgame.lord.logic.CLogic.maxValueOpponent
            int r4 = r14.value
            boolean r0 = LeadCardsValueCompare(r0, r4)
            if (r0 == 0) goto L_0x0273
            int[] r0 = com.tencent.qqgame.lord.logic.CLogic.cardListLen
            int r4 = r1 - r10
            r0 = r0[r4]
            int r0 = r3 - r0
            int r0 = r0 + 1
            if (r0 < 0) goto L_0x0273
            java.util.Vector r4 = new java.util.Vector
            r4.<init>()
            r0 = r3
        L_0x020c:
            int[] r5 = com.tencent.qqgame.lord.logic.CLogic.cardListLen
            int r6 = r1 - r10
            r5 = r5[r6]
            int r5 = r3 - r5
            if (r0 <= r5) goto L_0x0222
            java.lang.Object r14 = r2.elementAt(r0)
            com.tencent.qqgame.common.LordCard r14 = (com.tencent.qqgame.common.LordCard) r14
            r4.addElement(r14)
            int r0 = r0 + -1
            goto L_0x020c
        L_0x0222:
            boolean r0 = isTurn(r4)
            if (r0 == 0) goto L_0x0273
            int r0 = r4.size()
            int r0 = r0 - r10
            r5 = r0
        L_0x022e:
            if (r5 < 0) goto L_0x0273
            java.lang.Object r14 = r4.elementAt(r5)
            com.tencent.qqgame.common.LordCard r14 = (com.tencent.qqgame.common.LordCard) r14
            r6 = r9
            r7 = r10
        L_0x0238:
            java.util.Vector[] r0 = com.tencent.qqgame.lord.logic.CLogic.cardIndexVector
            int r8 = r1 - r10
            r0 = r0[r8]
            int r0 = r0.size()
            if (r6 >= r0) goto L_0x025d
            java.util.Vector[] r0 = com.tencent.qqgame.lord.logic.CLogic.cardIndexVector
            int r8 = r1 - r10
            r0 = r0[r8]
            java.lang.Object r0 = r0.elementAt(r6)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            int r8 = r14.index
            if (r0 != r8) goto L_0x0321
            r0 = r9
        L_0x0259:
            int r6 = r6 + 1
            r7 = r0
            goto L_0x0238
        L_0x025d:
            if (r7 == 0) goto L_0x026f
            java.util.Vector[] r0 = com.tencent.qqgame.lord.logic.CLogic.cardIndexVector
            int r6 = r1 - r10
            r0 = r0[r6]
            java.lang.Integer r6 = new java.lang.Integer
            int r7 = r14.index
            r6.<init>(r7)
            r0.addElement(r6)
        L_0x026f:
            int r0 = r5 + -1
            r5 = r0
            goto L_0x022e
        L_0x0273:
            int r0 = r3 + 1
            r3 = r0
            goto L_0x01e4
        L_0x0278:
            int r0 = com.tencent.qqgame.lord.logic.CLogic.typeOpponent
            r2 = 9
            if (r0 < r2) goto L_0x000f
            int r0 = com.tencent.qqgame.lord.logic.CLogic.typeOpponent
            r2 = 10
            if (r0 != r2) goto L_0x02c1
            int[] r0 = com.tencent.qqgame.lord.logic.CLogic.cardListLen
            r0[r9] = r11
            r0 = r9
        L_0x0289:
            java.util.Vector[] r2 = com.tencent.qqgame.lord.logic.CLogic.cardIndexVector
            r2 = r2[r11]
            int r2 = r2.size()
            if (r2 <= 0) goto L_0x000f
            r2 = r9
        L_0x0294:
            byte[] r3 = com.tencent.qqgame.lord.logic.CLogic.cardValue
            int r3 = r3.length
            if (r2 >= r3) goto L_0x000f
            r3 = r10
            r4 = r9
        L_0x029b:
            java.util.Vector[] r5 = com.tencent.qqgame.lord.logic.CLogic.cardIndexVector
            int r6 = r1 - r10
            r5 = r5[r6]
            int r5 = r5.size()
            if (r4 >= r5) goto L_0x02c7
            java.util.Vector[] r5 = com.tencent.qqgame.lord.logic.CLogic.cardIndexVector
            int r6 = r1 - r10
            r5 = r5[r6]
            java.lang.Object r14 = r5.elementAt(r4)
            java.lang.Integer r14 = (java.lang.Integer) r14
            int r5 = r14.intValue()
            byte[] r6 = com.tencent.qqgame.lord.logic.CLogic.cardIndex
            byte r6 = r6[r2]
            if (r5 != r6) goto L_0x02be
            r3 = r9
        L_0x02be:
            int r4 = r4 + 1
            goto L_0x029b
        L_0x02c1:
            int[] r0 = com.tencent.qqgame.lord.logic.CLogic.cardListLen
            r0[r10] = r11
            r0 = r10
            goto L_0x0289
        L_0x02c7:
            if (r3 == 0) goto L_0x02e1
            byte[] r3 = com.tencent.qqgame.lord.logic.CLogic.cardNum
            byte r3 = r3[r2]
            int r4 = r0 + 1
            if (r3 < r4) goto L_0x02e1
            java.util.Vector[] r3 = com.tencent.qqgame.lord.logic.CLogic.cardIndexVector
            r3 = r3[r0]
            java.lang.Integer r4 = new java.lang.Integer
            byte[] r5 = com.tencent.qqgame.lord.logic.CLogic.cardIndex
            byte r5 = r5[r2]
            r4.<init>(r5)
            r3.addElement(r4)
        L_0x02e1:
            int r2 = r2 + 1
            goto L_0x0294
        L_0x02e4:
            if (r1 == 0) goto L_0x02f6
            java.util.Vector[] r1 = com.tencent.qqgame.lord.logic.CLogic.cardIndexVector
            r1 = r1[r12]
            java.lang.Integer r2 = new java.lang.Integer
            byte[] r3 = com.tencent.qqgame.lord.logic.CLogic.cardIndex
            byte r3 = r3[r0]
            r2.<init>(r3)
            r1.addElement(r2)
        L_0x02f6:
            byte[] r1 = com.tencent.qqgame.lord.logic.CLogic.cardNum
            byte r1 = r1[r0]
            if (r1 != r10) goto L_0x031c
            byte[] r1 = com.tencent.qqgame.lord.logic.CLogic.cardValue
            byte r1 = r1[r0]
            r2 = 14
            if (r1 != r2) goto L_0x030c
            byte[] r1 = com.tencent.qqgame.lord.logic.CLogic.rocketIndex
            byte[] r2 = com.tencent.qqgame.lord.logic.CLogic.cardIndex
            byte r2 = r2[r0]
            r1[r9] = r2
        L_0x030c:
            byte[] r1 = com.tencent.qqgame.lord.logic.CLogic.cardValue
            byte r1 = r1[r0]
            r2 = 15
            if (r1 != r2) goto L_0x031c
            byte[] r1 = com.tencent.qqgame.lord.logic.CLogic.rocketIndex
            byte[] r2 = com.tencent.qqgame.lord.logic.CLogic.cardIndex
            byte r2 = r2[r0]
            r1[r10] = r2
        L_0x031c:
            int r0 = r0 + 1
            goto L_0x0010
        L_0x0320:
            return
        L_0x0321:
            r0 = r7
            goto L_0x0259
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.lord.logic.CLogic.SystemGetOutCard(java.util.Vector):void");
    }

    public static boolean checkHasBiggerCard(Vector<Card> vector, Vector<Card> vector2) {
        if (!SearchHints(true, vector, vector2)) {
            return true;
        }
        return m_SearchHint.GetHintCount() != 0;
    }

    public static int getCardType(Vector<Card> vector) {
        if (vector == null || vector.isEmpty()) {
            return 99;
        }
        max_type = staple_CardList(vector);
        if (getLeadCardValue(max_type) == 0) {
            return 99;
        }
        switch (max_type) {
            case 1:
                if (h_cardType.size() == 1) {
                    return 1;
                }
                if (h_cardType.size() == 2 && h_cardType.containsKey(new Integer(14)) && h_cardType.containsKey(new Integer(15))) {
                    return 14;
                }
                if (isOne_Turn(vector) > 0) {
                    return 6;
                }
                break;
            case 2:
                if (h_cardType.contains(new Integer(1))) {
                    return 0;
                }
                if (h_cardType.size() == 1) {
                    return 2;
                }
                if (h_cardType.size() > 2) {
                    if (h_cardType.containsKey(new Integer(2)) || h_cardType.containsKey(new Integer(14)) || h_cardType.containsKey(new Integer(15))) {
                        return 0;
                    }
                    if (h_cardType.contains(new Integer(1))) {
                        return 0;
                    }
                    if (CardsJudge(h_cardType, 2)) {
                        return 7;
                    }
                }
                break;
            case 3:
                if (h_cardType.size() == 1) {
                    return 3;
                }
                if (h_cardType.size() == 2) {
                    if (h_cardType.contains(new Integer(1))) {
                        return 4;
                    }
                    if (h_cardType.contains(new Integer(2))) {
                        return 5;
                    }
                    return !CardsJudge(h_cardType, 3) ? 0 : 8;
                } else if (!CardsJudge(h_cardType, 3)) {
                    return 0;
                } else {
                    if (h_cardType.contains(new Integer(2))) {
                        if (vector.size() == max_type_num * 4) {
                            return 10;
                        }
                        return (h_cardType.contains(new Integer(1)) || vector.size() % 5 > 0) ? 0 : 9;
                    } else if (h_cardType.contains(new Integer(1))) {
                        return vector.size() != max_type_num * 4 ? 0 : 10;
                    } else {
                        return 8;
                    }
                }
            case 4:
                if (vector.size() == 4) {
                    return 13;
                }
                if (h_cardType.contains(new Integer(1))) {
                    return vector.size() == 6 ? 11 : 0;
                }
                if (h_cardType.contains(new Integer(2))) {
                    if (h_cardType.contains(new Integer(1))) {
                        return 0;
                    }
                    if (vector.size() == 6) {
                        return 11;
                    }
                    if (vector.size() == 8) {
                        return 12;
                    }
                }
                break;
        }
        return 0;
    }

    public static int getLeadCardValue(int i) {
        int i2 = 0;
        max_type_num = 0;
        Integer num = new Integer(i);
        for (int i3 = 1; i3 <= 15; i3++) {
            Integer num2 = new Integer(i3);
            if (h_cardType.containsKey(num2) && num.intValue() == h_cardType.get(num2).intValue()) {
                max_type_num++;
                if (i2 < i3) {
                    if (i2 == 0) {
                        i2 = i3;
                    } else if (i3 < 3) {
                        i2 = i3;
                    } else if (i2 > 2 && i2 < 14) {
                        i2 = i3;
                    }
                }
            }
        }
        return i2;
    }

    private static LordCard getLordCard(byte b) {
        byte b2 = (b < 1 || b > 54) ? 55 : b;
        byte size = (byte) allLordCards.size();
        for (byte b3 = 0; b3 < size; b3 = (byte) (b3 + 1)) {
            if (allLordCards.elementAt(b3).number == b2) {
                return allLordCards.elementAt(b3);
            }
        }
        LordCard lordCard = new LordCard(b2);
        allLordCards.add(lordCard);
        return lordCard;
    }

    private static int isOne_Turn(Vector<Card> vector) {
        int i;
        byte b;
        if (vector.size() < 5) {
            return 0;
        }
        byte b2 = 0;
        int i2 = 0;
        while (i2 < vector.size()) {
            Card elementAt = vector.elementAt(i2);
            if (elementAt.value == 1) {
                b = 14;
            } else if (elementAt.value <= 2 || elementAt.value >= 14) {
                return 0;
            } else {
                b = b2 < elementAt.value ? elementAt.value : b2;
            }
            i2++;
            b2 = b;
        }
        int i3 = b2 - 1;
        int i4 = 0;
        int i5 = 0;
        while (i5 < vector.size() - 1) {
            int i6 = 0;
            while (true) {
                if (i6 < vector.size()) {
                    if (i3 == vector.elementAt(i6).value) {
                        i = i3;
                        break;
                    }
                    i6++;
                } else {
                    i = i4;
                    break;
                }
            }
            if (i <= 0) {
                return 0;
            }
            i4 = 0;
            i5++;
            i3--;
        }
        return b2;
    }

    public static boolean isTurn(Vector vector) {
        for (int i = 0; i < vector.size(); i++) {
            if (i < vector.size() - 1) {
                LordCard lordCard = (LordCard) vector.elementAt(i);
                LordCard lordCard2 = (LordCard) vector.elementAt(i + 1);
                if (lordCard.value > 13 || lordCard.value == 2 || lordCard2.value > 13 || lordCard2.value == 2) {
                    return false;
                }
                if (LeadCardsValueCompare(lordCard.value, lordCard2.value)) {
                    return false;
                }
                if (lordCard.value == 1) {
                    if (lordCard2.value != 13) {
                        return false;
                    }
                } else if (lordCard.value - lordCard2.value != 1) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void resetOutCardIndex() {
        isFirstSelect = true;
        if (cardIndexVector == null) {
            cardIndexVector = new Vector[4];
            for (int i = 0; i < cardIndexVector.length; i++) {
                cardIndexVector[i] = new Vector();
            }
            cardListIndex = new int[4];
            cardListLen = new int[4];
            rocketIndex = new byte[2];
        }
        for (int i2 = 0; i2 < cardIndexVector.length; i2++) {
            cardIndexVector[i2].removeAllElements();
            cardListIndex[i2] = 0;
            cardListLen[i2] = 1;
        }
        rocketIndex[0] = -1;
        rocketIndex[1] = -1;
        isHaveBomb = false;
    }

    public static int sortColor(LordCard lordCard, Vector vector) {
        int size = vector.size() - 1;
        int i = 0;
        while (size >= 0) {
            LordCard lordCard2 = (LordCard) vector.elementAt(size);
            if (lordCard.value != lordCard2.value) {
                break;
            }
            size--;
            i = lordCard.color < lordCard2.color ? i + 1 : i;
        }
        return i;
    }

    public static int staple_CardList(Vector<Card> vector) {
        int i;
        if (!h_cardType.isEmpty()) {
            h_cardType.clear();
            h_cardIndex.clear();
        }
        Vector vector2 = new Vector();
        Vector vector3 = new Vector();
        Vector vector4 = new Vector();
        int i2 = 0;
        for (int i3 = 0; i3 < vector.size(); i3++) {
            Card elementAt = vector.elementAt(i3);
            if (h_cardType.containsKey(new Integer(elementAt.value))) {
                i = i2;
            } else {
                int i4 = i3 + 1;
                int i5 = 1;
                while (i4 < vector.size()) {
                    int i6 = elementAt.value == vector.elementAt(i4).value ? i5 + 1 : i5;
                    i4++;
                    i5 = i6;
                }
                try {
                    Integer num = new Integer(elementAt.value);
                    h_cardType.put(num, new Integer(i5));
                    h_cardIndex.put(num, new Integer(i3));
                    vector2.addElement(num);
                    vector3.addElement(new Integer(i5));
                    vector4.addElement(new Integer(i3));
                } catch (Exception e) {
                }
                i = i2 < i5 ? i5 : i2;
            }
            i2 = i;
        }
        int size = h_cardType.size();
        if (isAutoCard) {
            cardValue = new byte[size];
            cardNum = new byte[size];
            cardIndex = new byte[size];
            for (int i7 = 0; i7 < size; i7++) {
                cardValue[(size - i7) - 1] = ((Integer) vector2.elementAt(i7)).byteValue();
                cardNum[(size - i7) - 1] = ((Integer) vector3.elementAt(i7)).byteValue();
                cardIndex[(size - i7) - 1] = ((Integer) vector4.elementAt(i7)).byteValue();
            }
            isAutoCard = false;
        }
        return i2;
    }

    public static boolean updateOutCard(Vector<Card> vector, Vector<Card> vector2) {
        boolean z;
        if (vector == null) {
            return false;
        }
        for (int i = 0; i < vector.size(); i++) {
            vector.elementAt(i).isSelected = false;
        }
        vector.removeAllElements();
        int length = (typeOpponent == 11 || typeOpponent == 12) ? cardIndexVector.length : cardIndexVector.length - 1;
        if (outCardType != -1 && cardIndexVector[0].size() <= 0 && cardIndexVector[1].size() <= 0 && cardIndexVector[2].size() <= 0) {
            outCardType = 1;
        }
        if (outCardType == 0) {
            z = false;
            for (int i2 = 0; i2 < length; i2++) {
                if (cardIndexVector[i2].size() > 0) {
                    boolean z2 = true;
                    for (int i3 = 0; i3 < cardListLen[i2]; i3++) {
                        int intValue = ((Integer) cardIndexVector[i2].elementAt(cardListIndex[i2] + i3)).intValue();
                        if (((typeOpponent == 4 || typeOpponent == 5) && cardIndexVector[2].size() > 0 && cardListLen[2] < 2) || (cardIndexVector[3].size() > 0 && (typeOpponent == 11 || typeOpponent == 12))) {
                            char c = (typeOpponent == 11 || typeOpponent == 12) ? (char) 3 : 2;
                            if (intValue == ((Integer) cardIndexVector[c].elementAt(cardListIndex[c])).intValue() && i2 < 2) {
                                int[] iArr = cardListIndex;
                                iArr[i2] = iArr[i2] + 1;
                                if (cardListLen[i2] > 1) {
                                    if (cardListIndex[i2] >= (cardIndexVector[i2].size() - cardListLen[i2]) + 1) {
                                        cardListIndex[i2] = 0;
                                        z2 = false;
                                    }
                                } else if (cardListIndex[i2] >= cardIndexVector[i2].size()) {
                                    cardListIndex[i2] = 0;
                                    z2 = false;
                                }
                                intValue = ((Integer) cardIndexVector[i2].elementAt(cardListIndex[i2] + i3)).intValue();
                            }
                        }
                        for (int i4 = 0; i4 < i2 + 1; i4++) {
                            if (intValue + i4 < vector2.size()) {
                                Card elementAt = vector2.elementAt(intValue + i4);
                                elementAt.isSelected = true;
                                vector.add(elementAt);
                            }
                        }
                    }
                    if (i2 >= 2) {
                        if (cardIndexVector[0].size() > 0 && cardListIndex[0] == 0) {
                            int[] iArr2 = cardListIndex;
                            iArr2[i2] = iArr2[i2] + 1;
                        } else if (cardIndexVector[1].size() > 0 && cardListIndex[1] == 0) {
                            int[] iArr3 = cardListIndex;
                            iArr3[i2] = iArr3[i2] + 1;
                        }
                        if (cardIndexVector[0].size() <= 0 && cardIndexVector[1].size() <= 0) {
                            int[] iArr4 = cardListIndex;
                            iArr4[i2] = iArr4[i2] + 1;
                        }
                    } else if (z2) {
                        int[] iArr5 = cardListIndex;
                        iArr5[i2] = iArr5[i2] + 1;
                    }
                    if (cardListLen[i2] > 1) {
                        if (cardListIndex[i2] >= (cardIndexVector[i2].size() - cardListLen[i2]) + 1) {
                            cardListIndex[i2] = 0;
                            if (i2 > 1) {
                                outCardType = 1;
                                z = true;
                            } else if (cardIndexVector[i2].size() > 0 && cardIndexVector[2].size() <= 0 && typeOpponent != 11 && typeOpponent != 12) {
                                outCardType = 1;
                                z = true;
                            }
                        } else if (i2 < 2 && cardIndexVector[2].size() <= 0 && typeOpponent != 11 && typeOpponent != 12) {
                            return true;
                        }
                    } else if (cardListIndex[i2] >= cardIndexVector[i2].size()) {
                        cardListIndex[i2] = 0;
                        if (i2 > 1) {
                            outCardType = 1;
                            z = true;
                        } else if (cardIndexVector[i2].size() > 0 && cardIndexVector[2].size() <= 0 && typeOpponent != 11 && typeOpponent != 12) {
                            outCardType = 1;
                            z = true;
                        }
                    } else if (i2 < 2 && cardIndexVector[2].size() <= 0 && typeOpponent != 11 && typeOpponent != 12) {
                        return true;
                    }
                    z = true;
                }
            }
        } else {
            if (outCardType == 1) {
                if (cardIndexVector[3].size() <= 0) {
                    outCardType = 2;
                    z = false;
                } else {
                    int i5 = cardListIndex[3];
                    while (true) {
                        int i6 = i5;
                        if (i6 < cardIndexVector[3].size()) {
                            int intValue2 = ((Integer) cardIndexVector[3].elementAt(i6)).intValue();
                            boolean z3 = true;
                            for (int i7 = 0; i7 < cardIndex.length; i7++) {
                                if (cardIndex[i7] == intValue2 && typeOpponent == 13 && !LeadCardsValueCompare(maxValueOpponent, cardValue[i7])) {
                                    z3 = false;
                                }
                            }
                            if (z3) {
                                for (int i8 = 0; i8 < 4; i8++) {
                                    if (intValue2 + i8 < vector2.size()) {
                                        Card elementAt2 = vector2.elementAt(intValue2 + i8);
                                        elementAt2.isSelected = true;
                                        vector.add(elementAt2);
                                    }
                                }
                                int[] iArr6 = cardListIndex;
                                iArr6[3] = iArr6[3] + 1;
                                isHaveBomb = true;
                                return true;
                            }
                            i5 = i6 + 1;
                        } else {
                            cardListIndex[3] = 0;
                            if (isHaveBomb || !(rocketIndex[0] == -1 || rocketIndex[1] == -1)) {
                                outCardType = 2;
                            } else {
                                outCardType = 0;
                                return false;
                            }
                        }
                    }
                }
            }
            z = false;
        }
        if (outCardType == 2) {
            outCardType = 0;
            if (!(rocketIndex[0] == -1 || rocketIndex[1] == -1)) {
                if (rocketIndex[0] < vector2.size()) {
                    Card elementAt3 = vector2.elementAt(rocketIndex[0]);
                    elementAt3.isSelected = true;
                    vector.add(elementAt3);
                }
                if (rocketIndex[1] < vector2.size()) {
                    Card elementAt4 = vector2.elementAt(rocketIndex[1]);
                    elementAt4.isSelected = true;
                    vector.add(elementAt4);
                }
                outCardType = -1;
                return true;
            }
        } else if (outCardType == -1) {
            outCardType = 0;
            return true;
        }
        for (Vector size : cardIndexVector) {
            if (size.size() > 0) {
                return true;
            }
        }
        return z;
    }
}
