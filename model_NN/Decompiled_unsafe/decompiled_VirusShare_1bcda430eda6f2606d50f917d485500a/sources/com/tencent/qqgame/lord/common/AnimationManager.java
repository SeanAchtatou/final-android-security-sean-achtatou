package com.tencent.qqgame.lord.common;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Vibrator;
import com.tencent.qqgame.common.Card;
import com.tencent.qqgame.common.Player;
import com.tencent.qqgame.common.Table;
import com.tencent.qqgame.hall.common.AnimationPlayerV1;
import com.tencent.qqgame.hall.common.SpriteV1;
import com.tencent.qqgame.hall.common.SysSetting;
import com.tencent.qqgame.hall.common.Tools;
import com.tencent.qqgame.lord.logic.CLogic;
import com.tencent.qqgame.lord.logic.LordLogic;
import com.tencent.qqgame.lord.ui.LordView;
import java.util.Vector;
import tencent.qqgame.lord.R;

public class AnimationManager {
    private static final byte FROG_OFFSET = 0;
    public static final byte ID_ANIPLAYER_GAMEOVER = 0;
    public static final byte ID_ANIPLAYER_LORDFARMER = 1;
    public static final byte ID_ANIPLAYER_PLANE_BOMB = 3;
    public static final byte ID_ANIPLAYER_TIMER_CHECK = 2;
    public int alpha;
    private final Vector<AnimationPlayerV1> animations = new Vector<>();
    float arcRadius;
    public SpriteV1 bubbleSprite;
    public SpriteV1 buttonSprite;
    private int cardCount = -1;
    public SpriteV1 cardSprite;
    public SpriteV1 clockSprite;
    private Context context;
    private int curFrameID;
    private int curTalker;
    private AnimationPlayerV1 drawCardPlayer;
    float endX = 440.0f;
    public int frameCounter;
    private int height = 0;
    private boolean isInitedSprited = false;
    private boolean isSendingCard;
    public boolean isShowingLordFarmerHead;
    public LordLogic logic;
    float offAngle = 0.0f;
    private int offset = -1;
    float orgX;
    float orgY;
    public SpriteV1 panelSpriteV1;
    public SpriteV1 portraitSprite;
    public SpriteV1 ppanelSpriteV1;
    float radius;
    public SpriteV1 robotSprite;
    private AnimationPlayerV1 sendCardPlayer;
    float startAngle = 66.0f;
    float startX = 40.0f;
    float startY = 275.0f;
    public AnimationPlayerV1 substitutePlayer;
    public SpriteV1 tableSprite;
    public AnimationPlayerV1 tipsPlayer;
    public SpriteV1 tipsSprite;
    public AnimationPlayerV1 toolBarPlayer;
    public SpriteV1 toolbarSprite;
    private int width = 0;

    public AnimationManager(Context context2) {
        this.context = context2;
        if (!this.isInitedSprited) {
            this.buttonSprite = new SpriteV1(context2, R.raw.button_);
            this.cardSprite = new SpriteV1(context2, R.raw.poker_);
            this.robotSprite = new SpriteV1(context2, R.raw.robot_);
            this.clockSprite = new SpriteV1(context2, R.raw.clock_);
            this.portraitSprite = new SpriteV1(context2, R.raw.portrait_);
            this.bubbleSprite = new SpriteV1(context2, R.raw.bubble_);
            this.panelSpriteV1 = new SpriteV1(context2, R.raw.panel_v1_);
            this.ppanelSpriteV1 = new SpriteV1(context2, R.raw.ppanel_);
            this.toolbarSprite = new SpriteV1(context2, R.raw.toolbar_);
            this.tableSprite = new SpriteV1(context2, R.raw.table_);
            this.isInitedSprited = true;
        }
    }

    private void calOrigin() {
        this.width = this.drawCardPlayer.getFrameWidth(0);
        this.height = this.drawCardPlayer.getFrameHeight(0);
        this.arcRadius = (this.endX - this.startX) / 2.0f;
        this.orgX = (this.startX + this.endX) / 2.0f;
        this.orgY = (float) (((double) this.startY) + (((double) this.arcRadius) * Math.tan((((double) this.startAngle) * 3.141592653589793d) / 180.0d)));
        this.radius = (float) Math.sqrt((double) (((this.endX - this.orgX) * (this.endX - this.orgX)) + ((this.orgY - this.startY) * (this.orgY - this.startY))));
        this.offAngle = (float) ((Math.atan((double) (((float) (this.width / 2)) / this.radius)) * 180.0d) / 3.141592653589793d);
        this.cardCount = -1;
        this.offset = -1;
        this.curFrameID = -1;
        if (SysSetting.getInstance(this.context).isVibrate()) {
            ((Vibrator) this.context.getSystemService("vibrator")).vibrate(1000);
        }
    }

    private void drawSendCard(Canvas canvas) {
        if (this.sendCardPlayer != null) {
            Vector<Card> vector = this.logic.myCards;
            this.sendCardPlayer.drawFrame(canvas, 0, 0);
            if (this.offset < 0) {
                for (int i = 0; i <= this.cardCount; i++) {
                    canvas.save();
                    canvas.rotate(90.0f - ((this.offAngle + this.startAngle) + ((((float) (16 - i)) * (90.0f - this.startAngle)) / 8.0f)), this.orgX, this.orgY);
                    canvas.drawBitmap(vector.elementAt(i).imgCard, (float) ((int) this.orgX), (float) ((int) (this.orgY - this.radius)), AnimationPlayerV1.painter);
                    canvas.restore();
                }
            } else {
                int i2 = this.cardCount + 1;
                int i3 = 0;
                int width2 = ((canvas.getWidth() - ((i2 - 1) * this.offset)) - this.width) / 2;
                while (i3 < i2) {
                    canvas.drawBitmap(vector.elementAt(i3).imgCard, (float) width2, (float) LordView.MYCARD_Y, AnimationPlayerV1.painter);
                    i3++;
                    width2 = this.offset + width2;
                }
            }
            Tools.drawNum(canvas, AnimationPlayerV1.painter, LordView.imgNums, this.cardCount + 1, 437, 98);
            Tools.drawNum(canvas, AnimationPlayerV1.painter, LordView.imgNums, this.cardCount + 1, 21, 98);
        }
    }

    private void notify(AnimationPlayerV1 animationPlayerV1) {
        switch (animationPlayerV1.getID()) {
            case 0:
                this.alpha = 255;
                if (this.logic.isViewGame) {
                    this.logic.ctlStartGame.setStatus((byte) 1);
                } else {
                    this.logic.ctlStartGame.setVisible(true);
                    this.logic.resetThinkTime((byte) 5);
                    this.logic.talkWho = -1;
                }
                if (this.logic != null && !this.logic.soundPlayer.isBackGroundSoundPlaying()) {
                    this.logic.soundPlayer.playWelcomeBackGroundSound();
                    break;
                }
            case 1:
                this.isShowingLordFarmerHead = false;
                break;
        }
        if (animationPlayerV1 == null) {
            return;
        }
        if (animationPlayerV1.getID() == 0 || animationPlayerV1.getID() == 3) {
            animationPlayerV1.recycle(true);
        } else {
            animationPlayerV1.recycle(false);
        }
    }

    private void updateSendCard(short s, int i) {
        if (this.sendCardPlayer != null) {
            this.sendCardPlayer.update(i);
            int curFrame = this.sendCardPlayer.getCurFrame();
            if (this.sendCardPlayer.currentFrameID() != this.curFrameID) {
                this.curFrameID = this.sendCardPlayer.currentFrameID();
                if (curFrame == 1) {
                    calOrigin();
                } else if (curFrame < 2 || curFrame > 33) {
                    if (curFrame > 33) {
                        this.logic.setMyStatus();
                        stopSendCardAni();
                    }
                } else if (curFrame <= 18) {
                    this.cardCount++;
                    if (this.cardCount > 16) {
                        this.cardCount = 16;
                    }
                } else if (curFrame > 18 && curFrame <= 21) {
                    this.logic.soundPlayer.stopSendCardSound();
                    this.startAngle += 6.0f;
                } else if (curFrame >= 22 && curFrame < 27) {
                    this.offset += 6;
                } else if (curFrame >= 27 && curFrame <= 33) {
                    this.offset -= 4;
                }
            }
        }
    }

    public void draw(Canvas canvas, short s) {
        if (this.substitutePlayer != null && !this.substitutePlayer.update(s)) {
            this.substitutePlayer.draw(canvas);
        }
        drawSendCard(canvas);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.animations.size()) {
                AnimationPlayerV1 elementAt = this.animations.elementAt(i2);
                if (elementAt != null) {
                    elementAt.draw(canvas);
                }
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public SpriteV1 getTipsSprite() {
        if (this.tipsSprite == null) {
            this.tipsSprite = new SpriteV1(this.context, R.raw.tip_);
        }
        return this.tipsSprite;
    }

    public void init() {
        this.curTalker = -1;
        this.alpha = -1;
        this.isShowingLordFarmerHead = false;
    }

    public boolean isSendingCard() {
        return this.isSendingCard;
    }

    public boolean isShowingLordFarmerHead() {
        return this.isShowingLordFarmerHead;
    }

    public boolean needScale(int i) {
        if (this.curTalker != i || this.logic.curStatus == -3) {
            return false;
        }
        this.frameCounter++;
        if (this.frameCounter >= 30) {
            this.curTalker = -1;
        }
        return this.frameCounter % 10 >= 5;
    }

    public int nextAlpha() {
        if (this.alpha < 0) {
            return this.alpha;
        }
        this.alpha -= 2;
        return this.alpha;
    }

    public void playBlinkEyeAni(int i) {
        int[] iArr = {424, 4, 4};
        int[] iArr2 = {9, 176, 9};
        for (int i2 = 0; i2 < 3; i2++) {
            AnimationPlayerV1 animationPlayerV1 = new AnimationPlayerV1(this.portraitSprite);
            if (i2 == i) {
                animationPlayerV1.setActionFlag(0, false);
            } else {
                animationPlayerV1.setActionFlag(1, false);
            }
            animationPlayerV1.setLocation(iArr[i2], iArr2[i2]);
            this.animations.add(animationPlayerV1);
        }
    }

    public void playBreakRuleTips() {
        if (this.tipsPlayer == null) {
            this.tipsPlayer = new AnimationPlayerV1(getTipsSprite());
        }
        this.animations.remove(this.tipsPlayer);
        this.tipsPlayer.setActionFlag(0, false);
        this.animations.add(this.tipsPlayer);
    }

    public void playCardNumAni(int i) {
        this.curTalker = i;
        this.frameCounter = -1;
    }

    public void playFogAni(Player player) {
        if (this.logic != null) {
            int[] iArr = {424, 216, 4};
            int[] iArr2 = {4, 213, 4};
            AnimationPlayerV1 animationPlayerV1 = new AnimationPlayerV1(this.portraitSprite);
            animationPlayerV1.setActionFlag(3, false);
            for (int i = 0; i < 3; i++) {
                if (Table.getPlayerByOppositeSeatId((short) i, this.logic.table.playerList, this.logic.me.seatId) == player) {
                    animationPlayerV1.setLocation(iArr[i], iArr2[i]);
                }
            }
            this.animations.add(animationPlayerV1);
        }
    }

    public void playGameOverAni(int i) {
        AnimationPlayerV1 animationPlayerV1;
        if (i < 0) {
            animationPlayerV1 = new AnimationPlayerV1(new SpriteV1(this.context, R.raw.lose_));
            animationPlayerV1.setID((byte) 0);
            animationPlayerV1.setActionFlag(0, false);
        } else {
            animationPlayerV1 = new AnimationPlayerV1(new SpriteV1(this.context, R.raw.win_));
            animationPlayerV1.setID((byte) 0);
            animationPlayerV1.setActionFlag(0, false);
        }
        animationPlayerV1.setLocation(0, 0);
        if (i != 0) {
            this.animations.add(animationPlayerV1);
        }
    }

    public void playLordFarmerAni() {
        int[] iArr = {424, 4, 4};
        int[] iArr2 = {4, 171, 4};
        for (int i = 0; i < 3; i++) {
            AnimationPlayerV1 animationPlayerV1 = new AnimationPlayerV1(this.portraitSprite);
            animationPlayerV1.setID((byte) 1);
            animationPlayerV1.setActionFlag(2, false);
            animationPlayerV1.setLocation(iArr[i], iArr2[i]);
            this.animations.add(animationPlayerV1);
        }
        this.isShowingLordFarmerHead = true;
    }

    public void playNoLargerCardTips() {
        if (this.tipsPlayer == null) {
            this.tipsPlayer = new AnimationPlayerV1(getTipsSprite());
        }
        this.animations.remove(this.tipsPlayer);
        this.tipsPlayer.setActionFlag(1, false);
        this.animations.add(this.tipsPlayer);
    }

    public void playPlaneAndBombAni(Vector<Card> vector, int i) {
        AnimationPlayerV1 animationPlayerV1;
        if (vector != null && vector.size() != 0) {
            int cardType = CLogic.getCardType(vector);
            if (cardType == 8 || cardType == 10 || cardType == 9) {
                animationPlayerV1 = new AnimationPlayerV1(new SpriteV1(this.context, R.raw.fly_));
                animationPlayerV1.setID((byte) 3);
            } else if (cardType == 13 || cardType == 14) {
                playBlinkEyeAni(i);
                animationPlayerV1 = new AnimationPlayerV1(new SpriteV1(this.context, R.raw.bomb_));
                animationPlayerV1.setID((byte) 3);
            } else {
                return;
            }
            animationPlayerV1.setActionFlag(0, false);
            animationPlayerV1.setLocation(0, 0);
            this.animations.add(animationPlayerV1);
        }
    }

    public void playSendCardAni() {
        this.isSendingCard = true;
        this.sendCardPlayer = new AnimationPlayerV1(this.cardSprite);
        this.sendCardPlayer.setActionFlag(0, false);
        this.drawCardPlayer = new AnimationPlayerV1(this.cardSprite);
        this.drawCardPlayer.setActionFlag(0, false);
        this.startAngle = 66.0f;
        this.offAngle = 0.0f;
        this.cardCount = -1;
        this.offset = -1;
        this.curFrameID = -1;
    }

    public void playSubstituteAni(boolean z) {
        AnimationPlayerV1 animationPlayerV1;
        this.substitutePlayer = new AnimationPlayerV1(this.robotSprite);
        if (z) {
            animationPlayerV1 = this.substitutePlayer;
            animationPlayerV1.setActionFlag(0, false);
        } else {
            animationPlayerV1 = new AnimationPlayerV1(this.robotSprite);
            animationPlayerV1.setActionFlag(1, false);
            this.substitutePlayer = null;
        }
        animationPlayerV1.setLocation(0, 0);
        this.animations.add(animationPlayerV1);
        AnimationPlayerV1 animationPlayerV12 = new AnimationPlayerV1(this.portraitSprite);
        animationPlayerV12.setActionFlag(3, false);
        animationPlayerV12.setLocation(4, 171);
        this.animations.add(animationPlayerV12);
    }

    public void playToolBarAni() {
        this.toolBarPlayer = new AnimationPlayerV1(this.toolbarSprite);
        this.toolBarPlayer.setActionFlag(0, false);
        this.toolBarPlayer.setLocation(60, 0);
        this.animations.add(this.toolBarPlayer);
    }

    public void recycle() {
        reset();
        this.isInitedSprited = false;
        this.logic = null;
        this.animations.removeAllElements();
        if (this.substitutePlayer != null) {
            this.substitutePlayer.recycle(false);
            this.substitutePlayer = null;
        }
        if (this.toolBarPlayer != null) {
            this.toolBarPlayer.recycle(false);
            this.toolBarPlayer = null;
        }
        if (this.tipsPlayer != null) {
            this.tipsPlayer.recycle(false);
            this.tipsPlayer = null;
        }
        if (this.sendCardPlayer != null) {
            this.sendCardPlayer.recycle(false);
            this.sendCardPlayer = null;
        }
        if (this.drawCardPlayer != null) {
            this.drawCardPlayer.recycle(false);
            this.drawCardPlayer = null;
        }
        if (this.buttonSprite != null) {
            this.buttonSprite.recycle();
            this.buttonSprite = null;
        }
        if (this.cardSprite != null) {
            this.cardSprite.recycle();
            this.cardSprite = null;
        }
        if (this.robotSprite != null) {
            this.robotSprite.recycle();
            this.robotSprite = null;
        }
        if (this.clockSprite != null) {
            this.clockSprite.recycle();
            this.clockSprite = null;
        }
        if (this.portraitSprite != null) {
            this.portraitSprite.recycle();
            this.portraitSprite = null;
        }
        if (this.bubbleSprite != null) {
            this.bubbleSprite.recycle();
            this.bubbleSprite = null;
        }
        if (this.panelSpriteV1 != null) {
            this.panelSpriteV1.recycle();
            this.panelSpriteV1 = null;
        }
        if (this.ppanelSpriteV1 != null) {
            this.ppanelSpriteV1.recycle();
            this.ppanelSpriteV1 = null;
        }
        if (this.toolbarSprite != null) {
            this.toolbarSprite.recycle();
            this.toolbarSprite = null;
        }
        if (this.tipsSprite != null) {
            this.tipsSprite.recycle();
            this.tipsSprite = null;
        }
        if (this.tableSprite != null) {
            this.tableSprite.recycle();
            this.tableSprite = null;
        }
        System.gc();
    }

    public void reset() {
        this.animations.removeAllElements();
        this.curTalker = -1;
        this.alpha = -1;
        this.isShowingLordFarmerHead = false;
        this.substitutePlayer = null;
        stopSendCardAni();
    }

    public void setLogic(LordLogic lordLogic) {
        this.logic = lordLogic;
    }

    public void stopSendCardAni() {
        this.isSendingCard = false;
        if (this.sendCardPlayer != null) {
            this.sendCardPlayer.recycle(false);
            this.sendCardPlayer = null;
        }
        if (this.drawCardPlayer != null) {
            this.drawCardPlayer.recycle(false);
            this.drawCardPlayer = null;
        }
        this.offset = -1;
        if (this.logic != null) {
            this.logic.soundPlayer.stopSendCardSound();
        }
    }

    public void update(short s, int i) {
        updateSendCard(s, i);
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < this.animations.size()) {
                if (!this.animations.elementAt(i3).update(s)) {
                    notify(this.animations.elementAt(i3));
                    this.animations.removeElementAt(i3);
                }
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }
}
