package com.tencent.qqgame.lord.logic;

import android.app.Activity;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.EditText;
import com.tencent.qqgame.common.Card;
import com.tencent.qqgame.common.Player;
import com.tencent.qqgame.common.SearchHint;
import com.tencent.qqgame.common.Table;
import com.tencent.qqgame.hall.common.AActivity;
import com.tencent.qqgame.hall.common.AnimationPlayerV1;
import com.tencent.qqgame.hall.common.IGameView;
import com.tencent.qqgame.hall.common.Report;
import com.tencent.qqgame.hall.common.SyncData;
import com.tencent.qqgame.hall.common.SysSetting;
import com.tencent.qqgame.hall.common.Tools;
import com.tencent.qqgame.hall.protocol.ISendGameMessageHandle;
import com.tencent.qqgame.lord.common.AGameLogic;
import com.tencent.qqgame.lord.common.ControlGroup;
import com.tencent.qqgame.lord.common.FrameControlV1;
import com.tencent.qqgame.lord.common.GameMessage;
import com.tencent.qqgame.lord.common.SoundPlayer;
import com.tencent.qqgame.lord.protocol.LordReceiveMessageHandle;
import com.tencent.qqgame.lord.ui.LordView;
import com.tencent.qqgame.lord.ui.control.ChatDialog;
import java.util.Enumeration;
import java.util.Vector;
import tencent.qqgame.lord.R;

public class LordLogic extends AGameLogic {
    public static final int ATTR_CHANGE_DESK = 0;
    public static final int ATTR_CLOSE = 3;
    public static final int ATTR_EXIT_GAME = 4;
    public static final int ATTR_QUIT = 1;
    public static final int ATTR_QUIT_TO_LOGIN = 2;
    public static final int KEY_CONTROL_ATTR = 0;
    public static final int KEY_MYCARD_INDEX = 2;
    public static final int KEY_ORIGN_RECT = 1;
    public static final int MAX_LEFT_CARD_ZOOM = 3;
    public final int[] allCardNum = {0, 0, 0};
    private final Vector<Card> allCards = new Vector<>();
    public final Vector<Card>[] allLeaveCards = new Vector[3];
    public final Vector<Card>[] allOutCards = new Vector[3];
    public final int[] allPlayerScore = new int[3];
    public byte backPoint = 0;
    public FrameControlV1 ctl1Fen = null;
    public FrameControlV1 ctl2Fen = null;
    public FrameControlV1 ctl3Fen = null;
    public FrameControlV1 ctlCancel = null;
    public FrameControlV1 ctlChangeDesk = null;
    public FrameControlV1 ctlChat = null;
    public FrameControlV1 ctlChatSend = null;
    public FrameControlV1 ctlChatText = null;
    public FrameControlV1 ctlDowned = null;
    public FrameControlV1 ctlLeaveDesk = null;
    public final FrameControlV1[] ctlMyCards = new FrameControlV1[20];
    public final FrameControlV1[] ctlMyOutCards = new FrameControlV1[20];
    public FrameControlV1 ctlNoCall = null;
    public FrameControlV1 ctlOK = null;
    public FrameControlV1 ctlOut = null;
    public FrameControlV1 ctlPass = null;
    public final FrameControlV1[] ctlPlayers = new FrameControlV1[3];
    public FrameControlV1 ctlQuit = null;
    public FrameControlV1 ctlReset = null;
    public FrameControlV1 ctlSelectedV1 = null;
    public FrameControlV1 ctlSetting = null;
    public FrameControlV1 ctlSettingBgTurnerOff = null;
    public FrameControlV1 ctlSettingBgTurnerOn = null;
    public FrameControlV1 ctlSettingBright = null;
    public FrameControlV1 ctlSettingEffectTurnerOff = null;
    public FrameControlV1 ctlSettingEffectTurnerOn = null;
    public FrameControlV1 ctlSettingHint = null;
    public FrameControlV1 ctlSettingProgressBar = null;
    public FrameControlV1 ctlSettingSound = null;
    public FrameControlV1 ctlSettingVibrateTurnerOff = null;
    public FrameControlV1 ctlSettingVibrateTurnerOn = null;
    public FrameControlV1 ctlStartGame = null;
    public FrameControlV1 ctlTabBright = null;
    public FrameControlV1 ctlTabHint = null;
    public FrameControlV1 ctlTabSound = null;
    public FrameControlV1 ctlTiShi = null;
    public FrameControlV1 ctlTick = null;
    public FrameControlV1 ctlTrusteeship = null;
    public byte curFocusCardIndex = -1;
    public byte curTouchCardIndex = -1;
    Card dCard = null;
    public final Vector<Card> dCards = new Vector<>(3);
    public Card downedCard = null;
    public int dragX = 0;
    public int dragY = 0;
    public boolean isMeCall = false;
    private boolean isOutingCards = false;
    private boolean isTouchMovingdCards = false;
    public boolean isTouchOutCards = false;
    int lX = 0;
    int lY = 0;
    public byte lordSeatId = -1;
    public byte maxMyCardPerRow = 0;
    public byte maxScore = 0;
    public int multiple = 1;
    public final Vector<Card> myCards = new Vector<>();
    public RectF myRectF = new RectF();
    private byte outingStep = 0;
    public final byte[] playerSay = {-1, -1, -1};
    public final Vector<Card> selectedCards = new Vector<>();
    public final SoundPlayer soundPlayer;
    public Card touchedCard = null;
    public final LordView view;

    public LordLogic(Activity activity, Handler handler, ISendGameMessageHandle iSendGameMessageHandle, LordView lordView, Table table, Player player, long j, boolean z) {
        super(activity, handler, iSendGameMessageHandle, table, player, j, z);
        this.view = lordView;
        this.view = lordView;
        this.receiveHandle = new LordReceiveMessageHandle(this);
        this.curStatus = 0;
        Card card = getCard((byte) 55);
        for (byte b = 0; b < 3; b = (byte) (b + 1)) {
            this.allOutCards[b] = new Vector<>();
            this.allLeaveCards[b] = new Vector<>();
            this.dCards.add(card);
        }
        this.sendHandle.getGameInfo();
        this.ctlChat = this.mainControls.addFrame(new FrameControlV1(lordView.apToolbar, new byte[]{0, 3, 1}, 67, 7 - 8));
        this.ctlChat.setPadding(5, 15, 7, 15);
        this.ctlTrusteeship = this.mainControls.addFrame(new FrameControlV1(lordView.apToolbar, new byte[]{4, 7, 5}, 120, 7 - 8));
        this.ctlTrusteeship.setPadding(5, 15, 7, 15);
        this.ctlSetting = this.mainControls.addFrame(new FrameControlV1(lordView.apToolbar, new byte[]{8, -1, 9}, (int) LordView.SCREEN_HEIGHT, 7 - 8));
        this.ctlSetting.setPadding(5, 15, 7, 15);
        this.ctlQuit = this.mainControls.addFrame(new FrameControlV1(lordView.apToolbar, new byte[]{11, -1, 12}, 369, 7 - 8));
        this.ctlQuit.setPadding(5, 15, 7, 15);
        for (byte b2 = 0; b2 < 3; b2 = (byte) (b2 + 1)) {
            this.ctlPlayers[b2] = this.mainControls.addFrame(new FrameControlV1(lordView.apPortrait, new byte[]{15, -1, 15}));
        }
        this.ctlStartGame = this.mainControls.addFrame(new FrameControlV1(lordView.apButton, new byte[]{12, LordView.FRAME_BUTTON_START_DISABLE, 13}));
        this.ctlPass = this.mainControls.addFrame(new FrameControlV1(lordView.apButton, new byte[]{3, 5, 4}));
        this.ctlReset = this.mainControls.addFrame(new FrameControlV1(lordView.apButton, new byte[]{9, 11, 10}));
        this.ctlTiShi = this.mainControls.addFrame(new FrameControlV1(lordView.apButton, new byte[]{6, 8, 7}));
        this.ctlOut = this.mainControls.addFrame(new FrameControlV1(lordView.apButton, new byte[]{0, 2, 1}));
        this.ctlNoCall = this.mainControls.addFrame(new FrameControlV1(lordView.apButton, new byte[]{25, 55, LordView.FRAME_BUTTON_NOCALL_HIGH}));
        this.ctl1Fen = this.mainControls.addFrame(new FrameControlV1(lordView.apButton, new byte[]{LordView.FRAME_BUTTON_1FEN_NORMAL, LordView.FRAME_BUTTON_1FEN_DISABLE, LordView.FRAME_BUTTON_1FEN_HIGH}));
        this.ctl2Fen = this.mainControls.addFrame(new FrameControlV1(lordView.apButton, new byte[]{19, LordView.FRAME_BUTTON_2FEN_DISABLE, 20}));
        this.ctl3Fen = this.mainControls.addFrame(new FrameControlV1(lordView.apButton, new byte[]{17, LordView.FRAME_BUTTON_3FEN_DISABLE, 18}));
        this.ctlStartGame.setPadding(5, 5, 15, 10);
        this.ctlPass.setPadding(5, 5, 15, 10);
        this.ctlReset.setPadding(5, 5, 15, 10);
        this.ctlTiShi.setPadding(5, 5, 15, 10);
        this.ctlOut.setPadding(5, 5, 15, 10);
        this.ctlNoCall.setPadding(5, 5, 15, 10);
        this.ctl1Fen.setPadding(5, 5, 15, 10);
        this.ctl2Fen.setPadding(5, 5, 15, 10);
        this.ctl3Fen.setPadding(5, 5, 15, 10);
        this.ctlChangeDesk = new FrameControlV1(lordView.apButton, new byte[]{LordView.FRAME_BUTTON_CHANGEDESK_NORMAL, LordView.FRAME_BUTTON_CHANGEDESK_DISABLE, LordView.FRAME_BUTTON_CHANGEDESK_HIGH});
        this.ctlLeaveDesk = new FrameControlV1(lordView.apButton, new byte[]{LordView.FRAME_BUTTON_LEAVEDESK_NORMAL, -1, LordView.FRAME_BUTTON_LEAVEDESK_HIGH});
        this.ctlTick = new FrameControlV1(lordView.apButton, new byte[]{LordView.FRAME_BUTTON_TICK_NORMAL, 29, LordView.FRAME_BUTTON_TICK_HIGH});
        this.ctlOK = new FrameControlV1(lordView.apButton, new byte[]{42, -1, LordView.FRAME_BUTTON_OK_HIGH});
        this.ctlCancel = new FrameControlV1(lordView.apButton, new byte[]{LordView.FRAME_BUTTON_CANCEL_NORMAL, -1, LordView.FRAME_BUTTON_CANCEL_HIGH});
        for (byte b3 = 0; b3 < 20; b3 = (byte) (b3 + 1)) {
            this.ctlMyCards[b3] = this.mainControls.addFrame(new FrameControlV1("ctlMyCards" + ((int) b3), (AnimationPlayerV1) null, (byte[]) null));
            this.ctlMyCards[b3].getSetInt(2, b3, true);
            this.ctlMyOutCards[b3] = this.mainControls.addFrame(new FrameControlV1("ctlMyOutCards" + ((int) b3), (AnimationPlayerV1) null, (byte[]) null));
            this.ctlMyOutCards[b3].setVisible(false);
        }
        this.ctlStartGame.setVisible(true);
        this.ctlStartGame.setStatus((byte) 0);
        SysSetting instance = SysSetting.getInstance(activity);
        this.ctlSettingSound = new FrameControlV1(lordView.apPanelV1, new byte[]{1, -1, -1});
        this.ctlSettingBright = new FrameControlV1(lordView.apPanelV1, new byte[]{2, -1, -1});
        this.ctlSettingHint = new FrameControlV1(lordView.apPanelV1, new byte[]{3, -1, -1});
        this.ctlTabSound = new FrameControlV1(null, null);
        this.ctlTabBright = new FrameControlV1(null, null);
        this.ctlTabHint = new FrameControlV1(null, null);
        this.ctlSettingProgressBar = new FrameControlV1(null, null);
        this.ctlSettingVibrateTurnerOn = new FrameControlV1(lordView.apPanelV1, new byte[]{6, -1, -1});
        this.ctlSettingVibrateTurnerOn.setPadding(20, 20, 20, 20);
        this.ctlSettingVibrateTurnerOff = new FrameControlV1(lordView.apPanelV1, new byte[]{7, -1, -1});
        this.ctlSettingVibrateTurnerOff.setPadding(20, 20, 20, 20);
        this.ctlSettingBgTurnerOn = new FrameControlV1(lordView.apPanelV1, new byte[]{6, -1, -1});
        this.ctlSettingBgTurnerOn.setPadding(20, 20, 20, 20);
        this.ctlSettingBgTurnerOff = new FrameControlV1(lordView.apPanelV1, new byte[]{7, -1, -1});
        this.ctlSettingBgTurnerOff.setPadding(20, 20, 20, 20);
        this.ctlSettingEffectTurnerOn = new FrameControlV1(lordView.apPanelV1, new byte[]{6, -1, -1});
        this.ctlSettingEffectTurnerOn.setPadding(20, 20, 20, 20);
        this.ctlSettingEffectTurnerOff = new FrameControlV1(lordView.apPanelV1, new byte[]{7, -1, -1});
        this.ctlSettingEffectTurnerOff.setPadding(20, 20, 20, 20);
        if (!instance.isVibrate()) {
            this.ctlSettingVibrateTurnerOn.setVisible(false);
            this.ctlSettingVibrateTurnerOff.setVisible(true);
        } else {
            this.ctlSettingVibrateTurnerOn.setVisible(true);
            this.ctlSettingVibrateTurnerOff.setVisible(false);
        }
        if (!instance.isBgSound()) {
            this.ctlSettingBgTurnerOn.setVisible(false);
            this.ctlSettingBgTurnerOff.setVisible(true);
        } else {
            this.ctlSettingBgTurnerOn.setVisible(true);
            this.ctlSettingBgTurnerOff.setVisible(false);
        }
        if (!instance.isEffectSound()) {
            this.ctlSettingEffectTurnerOn.setVisible(false);
            this.ctlSettingEffectTurnerOff.setVisible(true);
        } else {
            this.ctlSettingEffectTurnerOn.setVisible(true);
            this.ctlSettingEffectTurnerOff.setVisible(false);
        }
        this.ctlChatSend = new FrameControlV1(lordView.apButton, new byte[]{LordView.FRAME_BUTTON_SEND_NORMAL, -1, LordView.FRAME_BUTTON_SEND_HIGH});
        this.ctlChatText = new FrameControlV1(null, null);
        if (z) {
            setViewGameCtlStatus();
        } else {
            setMenuStatus((byte) 0, Boolean.TRUE);
            setMenuStatus((byte) 2, Boolean.FALSE);
            this.ctlTrusteeship.setStatus((byte) 1);
        }
        this.soundPlayer = AActivity.soundPlayer;
        if (player.status == 2 && this.table.playerList.size() == 3) {
            byte b4 = 0;
            for (byte b5 = 0; b5 < 3; b5 = (byte) (b5 + 1)) {
                if (this.table.playerList.elementAt(b5).status == 3) {
                    b4 = (byte) (b4 + 1);
                }
            }
            if (b4 != 2 || this.thinkTime <= 10) {
                resetThinkTime((byte) 30);
            } else {
                resetThinkTime((byte) 10);
            }
        } else {
            resetThinkTime((byte) 30);
        }
        lordView.aniManager.init();
        if (!z) {
            SearchHint.init();
        }
    }

    private void addTabAndButton(ControlGroup controlGroup) {
        controlGroup.addFrame(this.ctlTabSound);
        controlGroup.addFrame(this.ctlTabBright);
        controlGroup.addFrame(this.ctlTabHint);
    }

    private boolean checkPlayDaniSound(int i) {
        int cardType = CLogic.getCardType(this.allOutCards[i]);
        if (cardType == 99 || cardType == 0 || cardType == 1 || cardType == 2 || cardType == 13 || cardType == 14) {
            return false;
        }
        for (int i2 = 0; i2 < 3; i2++) {
            if (i2 != i && CLogic.getCardType(this.allOutCards[i2]) == cardType) {
                return true;
            }
        }
        return false;
    }

    private Vector<Card> getLastPlayerOutCards() {
        if (this.allOutCards[2].size() > 0) {
            return this.allOutCards[2];
        }
        if (this.allOutCards[0].size() > 0) {
            return this.allOutCards[0];
        }
        return null;
    }

    private void onTouchedDownCard(byte b, int i, int i2, int i3) {
        int i4;
        Card elementAt = this.myCards.elementAt(b);
        if (!this.isTouchMovingdCards && i == 0) {
            this.lX = i2;
            if (this.curStatus == -2) {
                this.soundPlayer.playHitCardSound();
            }
            this.downedCard = elementAt;
            setMyCardsTouched(b);
            setMyCardsFocused(b);
        } else if (!this.isTouchMovingdCards && Math.abs(this.lX - i2) > 10 && this.downedCard != null && this.downedCard.offset < 42) {
            byte size = (byte) this.myCards.size();
            byte b2 = this.curFocusCardIndex;
            byte b3 = 3;
            if (size > 15) {
                b3 = 2;
            }
            int i5 = b2 - b3;
            int i6 = b2 + b3;
            if (b2 + b3 > size) {
                i4 = size - 1;
                i5 -= (i6 - size) + 1;
            } else if (b2 - b3 < 0) {
                i4 = i6 - i5;
                i5 = 0;
            } else {
                i4 = i6;
            }
            for (byte b4 = 0; b4 < size; b4 = (byte) (b4 + 1)) {
                Card elementAt2 = this.myCards.elementAt(b4);
                if (b4 >= i5 && b4 <= i4) {
                    elementAt2.offset = (byte) (elementAt2.offset + 1);
                    elementAt2.anim_type = 1;
                } else if (elementAt2.offset > 16) {
                    elementAt2.offset = (byte) (elementAt2.offset - 1);
                    elementAt2.anim_type = 2;
                }
            }
            this.downedCard = null;
            this.isTouchMovingdCards = true;
        }
    }

    private void prepareOutingAnim() {
        int size = this.selectedCards.size();
        if (!this.isTouchOutCards || this.isTrusteeship || this.isViewGame) {
            this.selectedCards.removeAllElements();
            int size2 = this.myCards.size();
            for (byte b = 0; b < size2; b = (byte) (b + 1)) {
                Card elementAt = this.myCards.elementAt(b);
                if (elementAt.isSelected) {
                    this.selectedCards.add(elementAt);
                    int size3 = this.selectedCards.size() - 1;
                    Rect location = this.ctlMyCards[b].getLocation();
                    if (location.isEmpty()) {
                        setMyCardsLayout(true);
                        setMyOutCardsFinalPosition();
                        return;
                    }
                    this.ctlMyOutCards[size3].setLocation(location.left, location.top, location.right, location.bottom);
                    this.ctlMyOutCards[size3].getSetObj(1, new Rect(this.ctlMyOutCards[size3].getLocation()), true);
                }
            }
        } else {
            int i = this.dragX - 40 > 0 ? this.dragX - 40 : 0;
            int i2 = this.dragY - 40 > 0 ? this.dragY - 40 : 0;
            byte b2 = this.view.CARD_HEIGHT_M;
            int i3 = i;
            for (byte b3 = 0; b3 < size; b3 = (byte) (b3 + 1)) {
                this.ctlMyOutCards[b3].setLocation(i3, i2, i3 + 13, i2 + b2);
                this.ctlMyOutCards[b3].getSetObj(1, new Rect(this.ctlMyOutCards[b3].getLocation()), true);
                i3 += 13;
            }
        }
        this.isOutingCards = true;
        this.outingStep = 0;
    }

    private void removeTabParent() {
        removeParentFromGroupList(this.ctlTabSound);
    }

    private void setCardsSelectStatus(Vector<Card> vector, boolean z) {
        int size = vector.size();
        for (byte b = 0; b < size; b = (byte) (b + 1)) {
            vector.elementAt(b).isSelected = z;
        }
        setMyCardsLayout(false);
    }

    private final void setMenuStatus(byte b, Boolean bool) {
        Message obtainMessage = this.handle.obtainMessage(100);
        obtainMessage.arg1 = b;
        obtainMessage.obj = bool;
        this.handle.sendMessage(obtainMessage);
    }

    private void setMyCardsLayout(boolean z) {
        int i;
        int size = this.myCards.size();
        if (z) {
            byte b = this.view.CARD_WIDTH_L;
            byte b2 = this.view.CARD_HEIGHT_L;
            if (size > 1) {
                i = (475 - b) / (size - 1);
                if (i > 42) {
                    i = 42;
                }
            } else {
                i = 0;
            }
            byte b3 = 0;
            int i2 = (LordView.SCREEN_WIDTH - (b + ((size - 1) * i))) / 2;
            while (b3 < size) {
                Card elementAt = this.myCards.elementAt(b3);
                elementAt.offset = (byte) i;
                if (b3 == size - 1) {
                    elementAt.offset = b2;
                }
                int i3 = elementAt.isSelected ? LordView.MYCARD_Y - 15 : 232;
                this.ctlMyCards[b3].setLocation(i2, i3, elementAt.offset + i2, i3 + b2);
                b3 = (byte) (b3 + 1);
                i2 = elementAt.offset + i2;
            }
            return;
        }
        for (byte b4 = 0; b4 < size; b4 = (byte) (b4 + 1)) {
            if (this.myCards.elementAt(b4).isSelected) {
                this.ctlMyCards[b4].getLocation().top = LordView.MYCARD_Y - 15;
            } else {
                this.ctlMyCards[b4].getLocation().top = LordView.MYCARD_Y;
            }
        }
    }

    private void setMyCardsTouched(byte b) {
        if (this.curTouchCardIndex >= 0 && this.curTouchCardIndex < this.myCards.size()) {
            this.myCards.elementAt(this.curTouchCardIndex).isTouched = false;
        }
        if (b >= 0 && b < this.myCards.size()) {
            this.myCards.elementAt(b).isTouched = true;
        }
        this.curTouchCardIndex = b;
    }

    private final void setMyOutCardsFinalPosition() {
        int size = this.allOutCards[1].size();
        byte b = this.view.CARD_HEIGHT_M;
        int i = this.view.MYOUTCARD_Y;
        int i2 = (LordView.SCREEN_WIDTH - (((size - 1) * 13) + this.view.CARD_WIDTH_M)) / 2;
        for (byte b2 = 0; b2 < size; b2 = (byte) (b2 + 1)) {
            this.ctlMyOutCards[b2].setLocation(i2, i, i2 + 13, i + b);
            i2 += 13;
        }
    }

    private final void setMyTurnJiaoFenControl() {
        this.ctl1Fen.setVisible(true);
        this.ctl2Fen.setVisible(true);
        this.ctl3Fen.setVisible(true);
        this.ctlNoCall.setVisible(true);
        this.ctl1Fen.setStatus((byte) 0);
        this.ctl2Fen.setStatus((byte) 0);
        this.ctl3Fen.setStatus((byte) 0);
        this.ctlNoCall.setStatus((byte) 0);
        if (this.maxScore == 2) {
            this.ctl1Fen.setStatus((byte) 1);
            this.ctl2Fen.setStatus((byte) 1);
        } else if (this.maxScore == 1) {
            this.ctl1Fen.setStatus((byte) 1);
        }
    }

    private final void setMyTurnPlayControl(Vector<Card> vector) {
        this.ctlPass.setVisible(true);
        this.ctlReset.setVisible(true);
        this.ctlTiShi.setVisible(true);
        this.ctlOut.setVisible(true);
        this.ctlPass.setStatus((byte) 0);
        if (this.selectedCards.size() > 0) {
            this.ctlReset.setStatus((byte) 0);
        } else {
            this.ctlReset.setStatus((byte) 1);
        }
        this.ctlTiShi.setStatus((byte) 0);
        this.ctlOut.setStatus((byte) 0);
        if (this.selectedCards.size() <= 0) {
            this.ctlOut.setStatus((byte) 1);
        }
        if (this.allOutCards[0].size() == 0 && this.allOutCards[2].size() == 0) {
            this.ctlPass.setStatus((byte) 1);
            this.ctlTiShi.setStatus((byte) 1);
        }
    }

    private void setViewGameCtlStatus() {
        if (this.isViewGame) {
            setMenuStatus((byte) 0, Boolean.FALSE);
            setMenuStatus((byte) 2, Boolean.FALSE);
            this.ctlChangeDesk.setStatus((byte) 1);
            this.ctlTrusteeship.setStatus((byte) 1);
            this.ctlStartGame.setStatus((byte) 1);
            this.ctlPass.setStatus((byte) 1);
            this.ctlReset.setStatus((byte) 1);
            this.ctlTiShi.setStatus((byte) 1);
            this.ctlOut.setStatus((byte) 1);
            this.ctl1Fen.setStatus((byte) 1);
            this.ctl2Fen.setStatus((byte) 1);
            this.ctl3Fen.setStatus((byte) 1);
            this.ctlNoCall.setStatus((byte) 1);
        }
    }

    private void trusteeshipPlay() {
        if (getLastPlayerOutCards() == null) {
            setCardsSelectStatus(this.selectedCards, false);
            this.selectedCards.removeAllElements();
            selectCard((byte) (this.myCards.size() - 1), false);
            outCards(false);
            return;
        }
        Vector<Card> lastPlayerOutCards = getLastPlayerOutCards();
        CLogic.sizeCardOpponent = lastPlayerOutCards == null ? 0 : lastPlayerOutCards.size();
        CLogic.typeOpponent = CLogic.getCardType(lastPlayerOutCards);
        CLogic.maxCardTypeOpponent = CLogic.max_type;
        CLogic.maxValueOpponent = CLogic.getLeadCardValue(CLogic.max_type);
        CLogic.isAutoCard = true;
        CLogic.currentMaxCardType = CLogic.staple_CardList(this.myCards);
        CLogic.SystemGetOutCard(this.myCards);
        if (!CLogic.updateOutCard(this.selectedCards, this.myCards)) {
            outCards(true);
        } else {
            outCards(false);
        }
    }

    public void call(byte b, byte b2, boolean z, byte b3) {
        Tools.debug("---------------------->caller/score:" + ((int) b) + "/" + ((int) b2));
        this.isPlayerCalled = true;
        switch (b) {
            case 0:
                this.playerSay[b] = b2 > 0 ? (byte) ((b2 + 7) - 1) : 5;
                break;
            case 1:
                this.playerSay[b] = b2 > 0 ? (byte) ((b2 + 12) - 1) : 10;
                break;
            case 2:
                this.playerSay[b] = b2 > 0 ? (byte) ((b2 + 2) - 1) : 0;
                break;
        }
        this.maxScore = b2 > this.maxScore ? b2 : this.maxScore;
        if (z) {
            this.isMeCall = false;
            if (this.maxScore > 0) {
                setMyStatus();
                this.view.aniManager.stopSendCardAni();
                for (byte b4 = 0; b4 < 3; b4 = (byte) (b4 + 1)) {
                    this.playerSay[b4] = -1;
                }
                this.backPoint = this.maxScore;
                this.talkWho = b3;
                Tools.debug("---------------------->talkWho/nextTalker:" + ((int) this.talkWho) + "/" + ((int) b3));
                if (this.talkWho == 1) {
                    setMyTurnPlayControl(null);
                    setTipMessage(null);
                    if (!this.isViewGame) {
                        SearchHint.init();
                    } else {
                        setViewGameCtlStatus();
                    }
                }
                if (b2 > 0) {
                    this.view.aniManager.playLordFarmerAni();
                    this.soundPlayer.playCallLordSound();
                }
            }
        } else {
            if (b3 == 1) {
                if (!this.isViewGame && !this.isTrusteeship) {
                    showNotification(this.res.getString(R.string.game_home_tip_call));
                    setMyTurnJiaoFenControl();
                }
                this.isMeCall = true;
            }
            this.talkWho = b3;
            if ((b != 1 || this.isViewGame) && b2 <= 0) {
                this.soundPlayer.playNoOrderSound();
            }
        }
        resetThinkTime((byte) 30);
    }

    public void callScore(byte b) {
        this.isMeCall = false;
        if (b == 3) {
            this.talkWho = -1;
            resetThinkTime((byte) -1);
        } else {
            this.talkWho = 0;
            resetThinkTime((byte) 30);
        }
        this.sendHandle.callScore(b);
        if (b == 0) {
            this.soundPlayer.playNoOrderSound();
        }
        this.playerSay[1] = b > 0 ? (byte) ((b + 12) - 1) : 10;
        this.ctl1Fen.setVisible(false);
        this.ctl2Fen.setVisible(false);
        this.ctl3Fen.setVisible(false);
        this.ctlNoCall.setVisible(false);
    }

    public void canceledAction(ControlGroup controlGroup) {
        if ("cgQuit".equals(controlGroup.id) || "cgConfirmQuit".equals(controlGroup.id) || "cgConfirmChangeDesk".equals(controlGroup.id)) {
            this.ctlQuit.setStatus((byte) 0);
            this.ctlQuit.isFixed = false;
        } else if ("cgSettingSound".equals(controlGroup.id) || "cgSettingBright".equals(controlGroup.id) || "cgSettingHint".equals(controlGroup.id)) {
            this.ctlSetting.setStatus((byte) 0);
            this.ctlSetting.isFixed = false;
        } else if ("cgChat".equals(controlGroup.id)) {
            this.ctlChat.setStatus((byte) 0);
            this.ctlChat.isFixed = false;
        } else if ("cgResult".equals(controlGroup.id)) {
            restartGame();
        }
    }

    public final void changeDesk() {
        Message obtainMessage = this.handle.obtainMessage(110);
        obtainMessage.obj = this.res.getString(R.string.game_change_desk_ing);
        this.handle.sendMessage(obtainMessage);
        this.isChangingDesk = true;
        resetThinkTime((byte) 10);
        this.sendHandle.quickPlay(4);
    }

    public void chat(long j, String str) {
        if (str != null) {
            int indexOf = str.indexOf(":");
            this.chatMessageList.add(new GameMessage((indexOf == -1 || str.length() <= indexOf + 1) ? str : str.substring(indexOf + 1), j, this.curStatus == 0 ? 15 : 20));
        }
    }

    public boolean doKeyDown(int i, KeyEvent keyEvent) {
        if (this.isViewGame) {
            return false;
        }
        int size = this.myCards.size();
        if (i == 19) {
            if (this.curFocusCardIndex < 0 || this.curFocusCardIndex >= size) {
                setMyCardsFocused((byte) 0);
            } else if (this.curFocusCardIndex - this.maxMyCardPerRow >= 0) {
                setMyCardsFocused((byte) (this.curFocusCardIndex - this.maxMyCardPerRow));
            }
        } else if (i == 20) {
            if (this.curFocusCardIndex < 0 || this.curFocusCardIndex >= size) {
                setMyCardsFocused((byte) 0);
            } else if (this.curFocusCardIndex + this.maxMyCardPerRow < size) {
                setMyCardsFocused((byte) (this.curFocusCardIndex + this.maxMyCardPerRow));
            }
        } else if (i == 21) {
            setMyCardsFocused(this.curFocusCardIndex == 0 ? (byte) (size - 1) : (this.curFocusCardIndex <= 0 || this.curFocusCardIndex >= size) ? 0 : (byte) (this.curFocusCardIndex - 1));
        } else if (i == 22) {
            Tools.debug("curFocusCardIndex:" + ((int) this.curFocusCardIndex));
            setMyCardsFocused(this.curFocusCardIndex == size - 1 ? 0 : (this.curFocusCardIndex < 0 || this.curFocusCardIndex >= size - 1) ? 0 : (byte) (this.curFocusCardIndex + 1));
        } else if (i == 23 && this.curFocusCardIndex >= 0 && this.curFocusCardIndex < size) {
            selectCard(this.curFocusCardIndex, true);
        }
        return true;
    }

    public boolean doTouchEvent(MotionEvent motionEvent, int i, int i2) {
        FrameControlV1 frameControlV1;
        ControlGroup controlGroup;
        FrameControlV1 frameControlV12;
        byte indexOf;
        int action = motionEvent.getAction();
        if (action == 2) {
            this.dragX = i;
            this.dragY = i2;
            if (!this.isTouchOutCards && this.downedCard != null && this.dragY < 232 && Math.abs(this.lY - this.dragY) > 10) {
                this.isTouchOutCards = true;
                if (!this.downedCard.isSelected && (indexOf = (byte) this.myCards.indexOf(this.downedCard)) != -1) {
                    selectCard(indexOf, false);
                }
                setMyCardsTouched((byte) -1);
            }
        }
        if (this.isTouchOutCards) {
            if (action == 1) {
                if (this.talkWho == 1 && !this.isMeCall && this.dragY < 232) {
                    outCards(false);
                }
                this.downedCard = null;
                this.isTouchOutCards = false;
            }
            return false;
        }
        int size = this.controlGroups.size();
        if (size > 0) {
            int i3 = size - 1;
            ControlGroup controlGroup2 = null;
            while (true) {
                if (i3 < 0) {
                    controlGroup = controlGroup2;
                    frameControlV1 = null;
                    break;
                }
                controlGroup2 = (ControlGroup) this.controlGroups.elementAt(i3);
                if (controlGroup2.rect.contains(i, i2)) {
                    int size2 = controlGroup2.size();
                    byte b = 0;
                    while (true) {
                        if (b < size2) {
                            if (controlGroup2.elementAt(b) != null && controlGroup2.elementAt(b).isTouched(i, i2)) {
                                frameControlV12 = controlGroup2.elementAt(b);
                                break;
                            }
                            b = (byte) (b + 1);
                        } else {
                            frameControlV12 = null;
                            break;
                        }
                    }
                    FrameControlV1 frameControlV13 = frameControlV12;
                    controlGroup = controlGroup2;
                    frameControlV1 = frameControlV13;
                } else {
                    i3--;
                }
            }
        } else {
            frameControlV1 = null;
            controlGroup = null;
        }
        if (this.cgTipDialog != null && ((action == 0 || action == 1) && controlGroup != this.cgTipDialog)) {
            removeTipDialog();
        }
        if (!(frameControlV1 == this.ctlSelectedV1 || this.ctlSelectedV1 == null)) {
            String id = this.ctlSelectedV1.getId();
            if (!(id == null || id.indexOf("ctlMyCards") == -1)) {
                setMyCardsTouched((byte) -1);
            }
            if (!this.ctlSelectedV1.isFixed) {
                this.ctlSelectedV1.setStatus((byte) 0);
            }
            this.ctlSelectedV1 = null;
        }
        if (frameControlV1 == null) {
            return false;
        }
        if (action == 0) {
            frameControlV1.setStatus((byte) 2);
            this.ctlSelectedV1 = frameControlV1;
        } else if (action == 1) {
            if (this.ctlSelectedV1 != frameControlV1) {
                return false;
            }
            frameControlV1.setStatus((byte) 0);
        }
        return onControlTouchedV1(frameControlV1, action, i, i2);
    }

    public void exitGame(boolean z) {
        if (z) {
            this.handle.sendEmptyMessage(103);
        } else if (this.curStatus != -2 || this.isViewGame) {
            Message obtainMessage = this.handle.obtainMessage(110);
            obtainMessage.obj = this.res.getString(R.string.game_levae_table_ing);
            this.handle.sendMessage(obtainMessage);
            this.isExitingGame = true;
            resetThinkTime((byte) 10);
            this.handle.sendEmptyMessage(102);
            this.ctlQuit.setStatus((byte) 0);
            this.ctlQuit.isFixed = false;
        } else if (getControlGroupById("cgConfirmQuit") == null) {
            ControlGroup controlGroup = new ControlGroup("cgConfirmQuit", (byte) 7);
            this.controlGroups.add(controlGroup);
            controlGroup.obj = this.res.getString(R.string.game_tips_qiangtui);
            this.ctlOK.getSetInt(0, 1, true);
            controlGroup.addFrame(this.ctlOK);
            controlGroup.addFrame(this.ctlCancel);
        }
    }

    public final void gameStart() {
        if (this.table.playerList.size() != 3) {
            this.handle.sendEmptyMessage(103);
            return;
        }
        this.handle.sendEmptyMessage(101);
        if (!this.isViewGame) {
            this.handle.sendEmptyMessage(108);
        }
        if (this.isChangingDesk) {
            this.isChangingDesk = false;
            this.handle.sendEmptyMessage(111);
        }
        setAllPlayerState(4);
        this.curStatus = -2;
        setTipMessage(null);
        this.ctlStartGame.setVisible(false);
        SearchHint.ClearFlags();
    }

    public Card getCard(byte b) {
        byte b2 = (b < 1 || b > 54) ? 55 : b;
        byte size = (byte) this.allCards.size();
        for (byte b3 = 0; b3 < size; b3 = (byte) (b3 + 1)) {
            if (this.allCards.elementAt(b3).number == b2) {
                return this.allCards.elementAt(b3);
            }
        }
        Card card = new Card(b2);
        this.allCards.add(card);
        return card;
    }

    /* access modifiers changed from: protected */
    public ControlGroup getControlGroupById(String str) {
        if (str == null) {
            return null;
        }
        Enumeration elements = this.controlGroups.elements();
        while (elements.hasMoreElements()) {
            ControlGroup controlGroup = (ControlGroup) elements.nextElement();
            if (controlGroup.id == str) {
                return controlGroup;
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void handleMessage(Message message) {
        Tools.debug("<=>Game handle Message, what:" + message.what);
        switch (message.what) {
            case IGameView.MSG_2GAME_DIS_CONNECTED:
            case IGameView.MSG_2GAME_REPLAY_FAULT:
                this.gameTimeOut = -1;
                if (this.continueReplayTimes > 0 || message.what == 1003) {
                    this.continueReplayTimes = (byte) (this.continueReplayTimes + 1);
                    if (this.continueReplayTimes > 3 || message.what == 1003) {
                        this.continueReplayTimes = 0;
                        this.handle.sendEmptyMessage(111);
                        if (getControlGroupById("cgConfirmQuitTimer") == null) {
                            ControlGroup controlGroup = new ControlGroup("cgConfirmQuitTimer", (byte) 9);
                            this.controlGroups.add(controlGroup);
                            controlGroup.obj = this.res.getString(R.string.game_offline_replay_fault);
                            this.ctlOK.getSetInt(0, 2, true);
                            controlGroup.addFrame(this.ctlOK);
                            this.view.apButton.reset(true);
                            return;
                        }
                        return;
                    }
                    this.handle.sendEmptyMessage(106);
                    return;
                } else if (this.isViewGame || this.curStatus != -2) {
                    this.handle.sendEmptyMessage(109);
                    ControlGroup controlGroup2 = new ControlGroup("cgConfirmQuitTimer", (byte) 9);
                    this.controlGroups.add(controlGroup2);
                    controlGroup2.obj = this.res.getString(R.string.game_offlined);
                    this.ctlOK.getSetInt(0, 2, true);
                    controlGroup2.addFrame(this.ctlOK);
                    this.view.apButton.reset(true);
                    return;
                } else {
                    this.gameTimeOut = -1;
                    this.continueReplayTimes = 1;
                    this.handle.sendEmptyMessage(106);
                    return;
                }
            case 1001:
            case IGameView.MSG_2GAME_CHANGEDESK_FAULT:
            case IGameView.MSG_2GAME_CHANGEDESK_SUCC:
            case IGameView.MSG_2GAME_PARSE_SERVER_MSG:
            case IGameView.MSG_2GAME_PROGRESSDIALOG_CANCELED:
            default:
                return;
            case IGameView.MSG_2GAME_REPLAY_SUCC:
                this.sendHandle.getGameInfo();
                this.continueReplayTimes = 0;
                this.tipMessage = this.res.getString(R.string.game_replay_succ);
                return;
            case IGameView.MSG_2GAME_NET_SLOW:
                this.continueTipMessage = this.res.getString(R.string.game_network_slow);
                return;
            case IGameView.MSG_2GAME_NET_NORMALED:
                this.continueTipMessage = null;
                return;
            case IGameView.MSG_2GAME_BACK_PRESSED:
                onPressedBack();
                return;
            case IGameView.MSG_2GAME_MENU_ITEM_SELECTED:
                menuSelected((byte) message.arg1);
                return;
            case IGameView.MSG_2GAME_EXIT_ACTIVITY:
                this.view.gameSurface.setRunning(false);
                this.view.gameSurface.joinThread();
                if (this.view.aniManager != null) {
                    this.view.aniManager.recycle();
                }
                if (this.soundPlayer != null) {
                    this.soundPlayer.stopSendCardSound();
                }
                this.view.recycle();
                return;
            case IGameView.MSG_2GAME_CHAT_EDITVIEW:
                EditText editText = ChatDialog.chatInput;
                if (editText != null) {
                    int i = message.arg1;
                    editText.setVisibility(i);
                    if (i == 0) {
                        editText.setText((String) message.obj);
                        return;
                    }
                    return;
                }
                return;
        }
    }

    public void kickFault(String str) {
        ControlGroup controlGroup = new ControlGroup("cgConfirmKickFault", (byte) 8);
        this.controlGroups.add(controlGroup);
        if (str == null || str.trim().length() <= 0) {
            controlGroup.obj = this.res.getString(R.string.game_tick_fault_default);
        } else {
            controlGroup.obj = str;
        }
        this.ctlOK.getSetInt(0, 3, true);
        controlGroup.addFrame(this.ctlOK);
    }

    public void matchCard() {
        byte[] loadConfigPoker;
        if (SyncData.matchCardFileName != null && !SyncData.matchCardFileName.equals("") && (loadConfigPoker = Tools.loadConfigPoker(SyncData.matchCardFileName)) != null) {
            this.sendHandle.configPoker(loadConfigPoker);
        }
    }

    public boolean menuSelected(byte b) {
        switch (b) {
            case 0:
                onChangeDesk();
                return true;
            case 1:
                removeTipDialog();
                onChat();
                return true;
            case 2:
                onTrusteeship();
                return true;
            case 3:
                removeTipDialog();
                onSetting();
                return true;
            case 4:
                exitGame(false);
                return true;
            default:
                return true;
        }
    }

    public void onChangeDesk() {
        if (this.curStatus != -2) {
            changeDesk();
            this.ctlQuit.setStatus((byte) 0);
            this.ctlQuit.isFixed = false;
        } else if (getControlGroupById("cgConfirmChangeDesk") == null) {
            ControlGroup controlGroup = new ControlGroup("cgConfirmChangeDesk", (byte) 7);
            this.controlGroups.add(controlGroup);
            controlGroup.obj = this.res.getString(R.string.game_tips_changedesk);
            this.ctlOK.getSetInt(0, 0, true);
            controlGroup.addFrame(this.ctlOK);
            controlGroup.addFrame(this.ctlCancel);
        }
    }

    public void onChat() {
        if (getControlGroupById("cgChat") == null) {
            this.view.chatDlg.initChatPanel(this, this.view.apPanelV1, this.view.apButton);
            ControlGroup controlGroup = new ControlGroup("cgChat", (byte) 11);
            this.controlGroups.add(controlGroup);
            Rect bound = this.view.chatDlg.getBound();
            controlGroup.setRect(bound.left, bound.top, bound.right, bound.bottom);
            controlGroup.addFrame(this.ctlChatSend);
            controlGroup.addFrame(this.ctlChatText);
            this.cgTipDialog = controlGroup;
        }
        this.ctlChat.isFixed = true;
        this.ctlChat.setStatus((byte) 2);
        this.soundPlayer.playMenuSound();
    }

    public boolean onControlTouchedV1(FrameControlV1 frameControlV1, int i, int i2, int i3) {
        Vector<Card> lastPlayerOutCards;
        String id = frameControlV1.getId();
        if (!(this.isViewGame || id == null || id.indexOf("ctlMyCards") == -1)) {
            byte setInt = (byte) frameControlV1.getSetInt(2, 0, false);
            if (i == 0 || i == 2) {
                onTouchedDownCard(setInt, i, i2, i3);
                return true;
            } else if (i == 1) {
                setMyCardsTouched((byte) -1);
                if (!this.isTouchMovingdCards && Math.abs(this.lX - i2) <= 10) {
                    setMyCardsFocused(setInt);
                    selectCard(setInt, true);
                    this.downedCard = null;
                }
                return true;
            }
        }
        if (frameControlV1.equals(this.ctlStartGame)) {
            if (i == 1) {
                if (this.curStatus == -3) {
                    restartGame();
                }
                matchCard();
                handUp();
                this.soundPlayer.playMenuSound();
            }
        } else if (frameControlV1.equals(this.ctlNoCall)) {
            if (i == 1) {
                callScore((byte) 0);
            }
        } else if (frameControlV1.equals(this.ctl1Fen)) {
            if (i == 1) {
                callScore((byte) 1);
            }
        } else if (frameControlV1.equals(this.ctl2Fen)) {
            if (i == 1) {
                callScore((byte) 2);
            }
        } else if (frameControlV1.equals(this.ctl3Fen)) {
            if (i == 1) {
                callScore((byte) 3);
            }
        } else if (frameControlV1.equals(this.ctlOut)) {
            if (i == 1) {
                outCards(false);
            }
        } else if (frameControlV1.equals(this.ctlPass)) {
            if (i == 1) {
                outCards(true);
            }
        } else if (frameControlV1.equals(this.ctlReset)) {
            if (i == 1) {
                setCardsSelectStatus(this.selectedCards, false);
                this.selectedCards.removeAllElements();
                setMyCardsLayout(false);
                this.ctlOut.setStatus((byte) 1);
                frameControlV1.setStatus((byte) 1);
                this.soundPlayer.playMenuSound();
            }
        } else if (frameControlV1.equals(this.ctlTiShi)) {
            if (i == 1) {
                this.soundPlayer.playMenuSound();
                if (!this.isTrusteeship && this.talkWho == 1 && (lastPlayerOutCards = getLastPlayerOutCards()) != null) {
                    setCardsSelectStatus(this.selectedCards, false);
                    this.selectedCards.removeAllElements();
                    CLogic.GetNextHint(this, this.selectedCards, this.myCards, lastPlayerOutCards);
                    if (this.selectedCards.size() == 0) {
                        outCards(true);
                    } else {
                        if (this.selectedCards.size() > 0) {
                            this.ctlOut.setStatus((byte) 0);
                        } else {
                            this.ctlOut.setStatus((byte) 1);
                        }
                        if (this.selectedCards.size() == 0) {
                            this.ctlReset.setStatus((byte) 1);
                        } else {
                            this.ctlReset.setStatus((byte) 0);
                        }
                        setMyCardsLayout(false);
                    }
                }
            }
        } else if (frameControlV1.equals(this.ctlQuit)) {
            if (i == 1) {
                if (getControlGroupById("cgQuit") == null) {
                    ControlGroup controlGroup = new ControlGroup("cgQuit", (byte) 6);
                    this.controlGroups.add(controlGroup);
                    controlGroup.addFrame(this.ctlLeaveDesk);
                    controlGroup.addFrame(this.ctlChangeDesk);
                    this.cgTipDialog = controlGroup;
                }
                frameControlV1.isFixed = true;
                frameControlV1.setStatus((byte) 2);
                Report.toolbarHitNum++;
                Report.toolbarExitHitNum++;
                this.soundPlayer.playMenuSound();
            }
        } else if (frameControlV1.equals(this.ctlChat)) {
            if (i == 1) {
                Report.toolbarHitNum++;
                Report.toolbarChatHitNum++;
                onChat();
            }
        } else if (frameControlV1.equals(this.ctlChatSend)) {
            if (i == 1 && this.view.chatDlg != null) {
                this.view.chatDlg.report();
                String curSelectedText = this.view.chatDlg.getCurSelectedText();
                if (curSelectedText != null && curSelectedText.length() > 0) {
                    if (!this.isViewGame) {
                        chat(this.me.uid, curSelectedText);
                    }
                    this.sendHandle.chat(curSelectedText);
                    this.view.chatDlg.remove();
                    removeParentFromGroupList(this.ctlChatSend);
                }
            }
        } else if (frameControlV1.equals(this.ctlChatText)) {
            if (this.view.chatDlg != null) {
                this.view.chatDlg.doTouchEvent(i, i2, i3);
            }
        } else if (frameControlV1.equals(this.ctlPlayers[0]) || frameControlV1.equals(this.ctlPlayers[1]) || frameControlV1.equals(this.ctlPlayers[2])) {
            if (i == 1) {
                Report.headHitNum++;
                int i4 = frameControlV1.equals(this.ctlPlayers[0]) ? 0 : frameControlV1.equals(this.ctlPlayers[1]) ? 1 : 2;
                if (Table.getPlayerByOppositeSeatId((short) i4, this.table.playerList, this.me.seatId) != null) {
                    ControlGroup controlGroupById = getControlGroupById("cgUserInfo");
                    if (controlGroupById != null) {
                        if (controlGroupById.arg1 == i4) {
                            return true;
                        }
                        removeControlGroup(controlGroupById);
                    }
                    ControlGroup controlGroup2 = new ControlGroup("cgUserInfo", (byte) 10);
                    this.controlGroups.add(controlGroup2);
                    controlGroup2.addFrame(this.ctlTick);
                    controlGroup2.arg1 = frameControlV1.equals(this.ctlPlayers[0]) ? 0 : frameControlV1.equals(this.ctlPlayers[1]) ? 1 : 2;
                    this.cgTipDialog = controlGroup2;
                    if (controlGroup2.arg1 == 1) {
                        this.ctlTick.setVisible(false);
                    } else {
                        this.ctlTick.setVisible(true);
                        if (this.curStatus == -2 || this.isViewGame) {
                            this.ctlTick.setStatus((byte) 1);
                        } else {
                            this.ctlTick.setStatus((byte) 0);
                        }
                    }
                }
            }
        } else if (frameControlV1.equals(this.ctlTick)) {
            if (i == 1) {
                ControlGroup controlGroupById2 = getControlGroupById("cgUserInfo");
                if (this.me.isSuperPlayer()) {
                    Player playerByOppositeSeatId = Table.getPlayerByOppositeSeatId((short) controlGroupById2.arg1, this.table.playerList, this.me.seatId);
                    if (playerByOppositeSeatId != null) {
                        this.sendHandle.tickPlayer(playerByOppositeSeatId.uid);
                    }
                    removeControlGroup(controlGroupById2);
                } else {
                    removeControlGroup(controlGroupById2);
                    ControlGroup controlGroup3 = new ControlGroup("cgConfirmNotSuperPlayer", (byte) 8);
                    this.controlGroups.add(controlGroup3);
                    controlGroup3.obj = this.res.getString(R.string.game_tick_fault_not_super);
                    this.ctlOK.getSetInt(0, 3, true);
                    controlGroup3.addFrame(this.ctlOK);
                }
            }
        } else if (frameControlV1.equals(this.ctlSetting)) {
            if (i == 1) {
                Report.toolbarHitNum++;
                Report.toolbarSettingHitNum++;
                onSetting();
            }
        } else if (frameControlV1.equals(this.ctlTabSound)) {
            if (i == 1) {
                removeTabParent();
                if (getControlGroupById("cgSettingSound") == null) {
                    SysSetting instance = SysSetting.getInstance(this.gameActivity);
                    this.ctlSettingProgressBar.getSetInt(0, ((int) ((((float) instance.getVolumne()) / ((float) instance.getMaxStreamVolume(3))) * 114.0f)) + 188, true);
                    ControlGroup controlGroup4 = new ControlGroup("cgSettingSound", (byte) 3);
                    Rect frameBound = this.view.apPanelV1.getFrameBound(1);
                    controlGroup4.setRect(frameBound.left, frameBound.top, frameBound.right, frameBound.bottom);
                    this.controlGroups.add(controlGroup4);
                    controlGroup4.addFrame(this.ctlSettingBgTurnerOn);
                    controlGroup4.addFrame(this.ctlSettingEffectTurnerOn);
                    controlGroup4.addFrame(this.ctlSettingBgTurnerOff);
                    controlGroup4.addFrame(this.ctlSettingEffectTurnerOff);
                    controlGroup4.addFrame(this.ctlSettingProgressBar);
                    if (!instance.isBgSound()) {
                        this.ctlSettingBgTurnerOn.setVisible(false);
                        this.ctlSettingBgTurnerOff.setVisible(true);
                    } else {
                        this.ctlSettingBgTurnerOn.setVisible(true);
                        this.ctlSettingBgTurnerOff.setVisible(false);
                    }
                    if (!instance.isEffectSound()) {
                        this.ctlSettingEffectTurnerOn.setVisible(false);
                        this.ctlSettingEffectTurnerOff.setVisible(true);
                    } else {
                        this.ctlSettingEffectTurnerOn.setVisible(true);
                        this.ctlSettingEffectTurnerOff.setVisible(false);
                    }
                    this.ctlSettingProgressBar.setLocation(188, 102, 302, 109);
                    this.ctlSettingProgressBar.setPadding(20, 20, 10, 20);
                    addTabAndButton(controlGroup4);
                    this.cgTipDialog = controlGroup4;
                }
                this.ctlSetting.isFixed = true;
                this.ctlSetting.setStatus((byte) 2);
            }
        } else if (frameControlV1.equals(this.ctlTabBright)) {
            if (i == 1) {
                removeTabParent();
                if (getControlGroupById("cgSettingBright") == null) {
                    this.ctlSettingProgressBar.getSetInt(2, ((int) (SysSetting.getInstance(this.gameActivity).getBright() * 114.0f)) + 188, true);
                    ControlGroup controlGroup5 = new ControlGroup("cgSettingBright", (byte) 4);
                    Rect frameBound2 = this.view.apPanelV1.getFrameBound(2);
                    controlGroup5.setRect(frameBound2.left, frameBound2.top, frameBound2.right, frameBound2.bottom);
                    this.controlGroups.add(controlGroup5);
                    controlGroup5.addFrame(this.ctlSettingProgressBar);
                    this.ctlSettingProgressBar.setLocation(188, 117, 302, 124);
                    this.ctlSettingProgressBar.setPadding(20, 20, 10, 10);
                    addTabAndButton(controlGroup5);
                    this.cgTipDialog = controlGroup5;
                }
                this.ctlSetting.isFixed = true;
                this.ctlSetting.setStatus((byte) 2);
            }
        } else if (frameControlV1.equals(this.ctlTabHint)) {
            if (i == 1) {
                removeTabParent();
                if (getControlGroupById("cgSettingHint") == null) {
                    ControlGroup controlGroup6 = new ControlGroup("cgSettingHint", (byte) 5);
                    Rect frameBound3 = this.view.apPanelV1.getFrameBound(3);
                    controlGroup6.setRect(frameBound3.left, frameBound3.top, frameBound3.right, frameBound3.bottom);
                    this.controlGroups.add(controlGroup6);
                    controlGroup6.addFrame(this.ctlSettingVibrateTurnerOn);
                    controlGroup6.addFrame(this.ctlSettingVibrateTurnerOff);
                    if (!SysSetting.getInstance(this.gameActivity).isVibrate()) {
                        this.ctlSettingVibrateTurnerOn.setVisible(false);
                        this.ctlSettingVibrateTurnerOff.setVisible(true);
                    } else {
                        this.ctlSettingVibrateTurnerOn.setVisible(true);
                        this.ctlSettingVibrateTurnerOff.setVisible(false);
                    }
                    addTabAndButton(controlGroup6);
                    this.cgTipDialog = controlGroup6;
                }
                this.ctlSetting.isFixed = true;
                this.ctlSetting.setStatus((byte) 2);
            }
        } else if (frameControlV1.equals(this.ctlSettingVibrateTurnerOn)) {
            if (i == 1) {
                Report.vibrateOnHitInSettingNum++;
                SysSetting.getInstance(this.gameActivity).setVibrate(false);
                this.ctlSettingVibrateTurnerOn.setVisible(false);
                this.ctlSettingVibrateTurnerOff.setVisible(true);
            }
        } else if (frameControlV1.equals(this.ctlSettingVibrateTurnerOff)) {
            if (i == 1) {
                Report.vibrateOffHitInSettingNum++;
                SysSetting.getInstance(this.gameActivity).setVibrate(true);
                this.ctlSettingVibrateTurnerOn.setVisible(true);
                this.ctlSettingVibrateTurnerOff.setVisible(false);
            }
        } else if (frameControlV1.equals(this.ctlSettingBgTurnerOn)) {
            if (i == 1) {
                Report.bgMusicOnHitInSettingNum++;
                SysSetting.getInstance(this.gameActivity).setBgSound(false);
                this.soundPlayer.stopBackGroundSound(false);
                this.ctlSettingBgTurnerOn.setVisible(false);
                this.ctlSettingBgTurnerOff.setVisible(true);
            }
        } else if (frameControlV1.equals(this.ctlSettingBgTurnerOff)) {
            if (i == 1) {
                Report.bgMusicOffHitInSettingNum++;
                SysSetting.getInstance(this.gameActivity).setBgSound(true);
                this.soundPlayer.playCurBackGroundSound();
                this.ctlSettingBgTurnerOn.setVisible(true);
                this.ctlSettingBgTurnerOff.setVisible(false);
            }
        } else if (frameControlV1.equals(this.ctlSettingEffectTurnerOn)) {
            if (i == 1) {
                Report.effectMusicOnHitInSettingNum++;
                SysSetting.getInstance(this.gameActivity).setEffectSound(false);
                this.ctlSettingEffectTurnerOn.setVisible(false);
                this.ctlSettingEffectTurnerOff.setVisible(true);
            }
        } else if (frameControlV1.equals(this.ctlSettingEffectTurnerOff)) {
            if (i == 1) {
                Report.effectMusicOffHitInSettingNum++;
                SysSetting instance2 = SysSetting.getInstance(this.gameActivity);
                this.soundPlayer.stopSendCardSound();
                instance2.setEffectSound(true);
                this.ctlSettingEffectTurnerOn.setVisible(true);
                this.ctlSettingEffectTurnerOff.setVisible(false);
            }
        } else if (frameControlV1.equals(this.ctlSettingProgressBar)) {
            if (i == 2 || i == 1) {
                int i5 = i2 > 302 ? 302 : i2;
                if (i5 < 188) {
                    i5 = 188;
                }
                SysSetting instance3 = SysSetting.getInstance(this.gameActivity);
                if (getControlGroupById("cgSettingBright") != null) {
                    this.ctlSettingProgressBar.getSetInt(2, i5, true);
                    instance3.setBright((((float) this.ctlSettingProgressBar.getSetInt(2, 0, false)) - 188.0f) / 114.0f);
                    this.handle.sendEmptyMessage(113);
                } else if (getControlGroupById("cgSettingSound") != null) {
                    this.ctlSettingProgressBar.getSetInt(0, i5, true);
                    instance3.adjustVolumne(3, (int) ((((float) (this.ctlSettingProgressBar.getSetInt(0, 0, false) - 188)) / 114.0f) * ((float) instance3.getMaxStreamVolume(3))));
                }
            }
        } else if (frameControlV1.equals(this.ctlTrusteeship)) {
            if (i == 1) {
                Report.toolbarHitNum++;
                Report.toolbarTrustShipHitNum++;
                onTrusteeship();
                this.soundPlayer.playMenuSound();
            }
        } else if (frameControlV1.equals(this.ctlLeaveDesk)) {
            if (i == 1) {
                Report.leaveDeskHitNum++;
                removeParentFromGroupList(frameControlV1);
                exitGame(false);
            }
        } else if (frameControlV1.equals(this.ctlChangeDesk)) {
            if (i == 1) {
                Report.changeDeskHitNum++;
                removeParentFromGroupList(frameControlV1);
                onChangeDesk();
            }
        } else if (frameControlV1.equals(this.ctlCancel)) {
            if (i == 1) {
                ControlGroup controlGroup7 = (ControlGroup) frameControlV1.getSetObj(1, null, false);
                if (controlGroup7 != null) {
                    canceledAction(controlGroup7);
                }
                removeParentFromGroupList(frameControlV1);
            }
        } else if (frameControlV1.equals(this.ctlChangeDesk)) {
            if (i == 1) {
                removeParentFromGroupList(frameControlV1);
                changeDesk();
            }
        } else if (frameControlV1.equals(this.ctlOK) && i == 1) {
            switch (this.ctlOK.getSetInt(0, -1, false)) {
                case 0:
                    changeDesk();
                    break;
                case 1:
                    Message obtainMessage = this.handle.obtainMessage(110);
                    obtainMessage.obj = this.res.getString(R.string.game_levae_table_ing);
                    this.handle.sendMessage(obtainMessage);
                    this.isExitingGame = true;
                    resetThinkTime((byte) 10);
                    this.handle.sendEmptyMessage(102);
                    break;
                case 2:
                    this.handle.sendEmptyMessage(107);
                    break;
                case 4:
                    exitGame(false);
                    break;
            }
            removeParentFromGroupList(frameControlV1);
        }
        return true;
    }

    public boolean onDragCards(int i) {
        return true;
    }

    public void onPressedBack() {
        if (!this.isChangingDesk && !this.isExitingGame) {
            int size = this.controlGroups.size();
            if (size > 1) {
                ControlGroup controlGroup = (ControlGroup) this.controlGroups.elementAt(size - 1);
                if ("cgChat".equals(controlGroup.id) && this.view.chatDlg != null) {
                    this.view.chatDlg.remove();
                } else if ("cgSettingSound".equals(controlGroup.id) || "cgSettingBright".equals(controlGroup.id) || "cgSettingHint".equals(controlGroup.id)) {
                    SysSetting.getInstance(this.gameActivity).save();
                }
                canceledAction(controlGroup);
                removeControlGroup(controlGroup);
                return;
            }
            exitGame(false);
        }
    }

    public void onSetting() {
        if (getControlGroupById("cgSettingSound") == null) {
            SysSetting instance = SysSetting.getInstance(this.gameActivity);
            instance.setVolumne(instance.getSysStreamVolume(3));
            this.ctlSettingProgressBar.getSetInt(0, ((int) ((((float) instance.getVolumne()) / ((float) instance.getMaxStreamVolume(3))) * 114.0f)) + 188, true);
            ControlGroup controlGroup = new ControlGroup("cgSettingSound", (byte) 3);
            this.controlGroups.add(controlGroup);
            Rect frameBound = this.view.apPanelV1.getFrameBound(1);
            controlGroup.setRect(frameBound.left, frameBound.top, frameBound.right, frameBound.bottom);
            controlGroup.addFrame(this.ctlSettingBgTurnerOn);
            controlGroup.addFrame(this.ctlSettingEffectTurnerOn);
            controlGroup.addFrame(this.ctlSettingBgTurnerOff);
            controlGroup.addFrame(this.ctlSettingEffectTurnerOff);
            controlGroup.addFrame(this.ctlSettingProgressBar);
            if (!instance.isBgSound()) {
                this.ctlSettingBgTurnerOn.setVisible(false);
                this.ctlSettingBgTurnerOff.setVisible(true);
            } else {
                this.ctlSettingBgTurnerOn.setVisible(true);
                this.ctlSettingBgTurnerOff.setVisible(false);
            }
            if (!instance.isEffectSound()) {
                this.ctlSettingEffectTurnerOn.setVisible(false);
                this.ctlSettingEffectTurnerOff.setVisible(true);
            } else {
                this.ctlSettingEffectTurnerOn.setVisible(true);
                this.ctlSettingEffectTurnerOff.setVisible(false);
            }
            this.ctlSettingProgressBar.setLocation(188, 102, 302, 109);
            this.ctlSettingProgressBar.setPadding(20, 20, 10, 10);
            addTabAndButton(controlGroup);
            this.cgTipDialog = controlGroup;
        }
        this.ctlSetting.isFixed = true;
        this.ctlSetting.setStatus((byte) 2);
        this.soundPlayer.playMenuSound();
    }

    public void onTrusteeship() {
        if (!this.isTrusteeship) {
            this.isTrusteeship = true;
            this.ctlTrusteeship.setStatus((byte) 2);
            this.ctlTrusteeship.isFixed = true;
            if (this.forceTrusteeTimes >= 2) {
                setMenuStatus((byte) 2, Boolean.FALSE);
            } else {
                Message obtainMessage = this.handle.obtainMessage(100);
                obtainMessage.arg1 = 2;
                obtainMessage.arg2 = R.string.menu_cancelTrusteeship;
                this.handle.sendMessage(obtainMessage);
            }
            this.ctlPass.setVisible(false);
            this.ctlReset.setVisible(false);
            this.ctlTiShi.setVisible(false);
            this.ctlOut.setVisible(false);
            if (this.talkWho == 1) {
                trusteeshipPlay();
            }
            this.view.aniManager.playSubstituteAni(this.isTrusteeship);
        } else if (this.forceTrusteeTimes >= 2) {
            setTipMessage(this.res.getString(R.string.game_trusteeship_always));
            this.ctlTrusteeship.isFixed = true;
            this.ctlTrusteeship.setStatus((byte) 2);
        } else {
            this.isTrusteeship = false;
            this.ctlTrusteeship.setStatus((byte) 0);
            this.ctlTrusteeship.isFixed = false;
            Message obtainMessage2 = this.handle.obtainMessage(100);
            obtainMessage2.arg1 = 2;
            obtainMessage2.arg2 = R.string.menu_trusteeship;
            this.handle.sendMessage(obtainMessage2);
            this.view.aniManager.playSubstituteAni(this.isTrusteeship);
        }
    }

    public void outCards(boolean z) {
        if (z) {
            this.playerSay[1] = 11;
            setCardsSelectStatus(this.selectedCards, false);
            this.allOutCards[1].removeAllElements();
            this.selectedCards.removeAllElements();
            this.soundPlayer.playPassSound();
            if (this.allCardNum[0] > 0) {
                resetThinkTime((byte) 30);
                this.talkWho = 0;
            }
        } else if (!CLogic.Compare(getLastPlayerOutCards(), this.selectedCards)) {
            this.view.aniManager.playBreakRuleTips();
            return;
        } else {
            prepareOutingAnim();
            int size = this.selectedCards.size();
            int size2 = this.myCards.size();
            for (int size3 = this.myCards.size() - this.selectedCards.size(); size3 < size2; size3++) {
                this.ctlMyCards[size3].setLocation(0, 0, 0, 0);
            }
            this.allOutCards[1].removeAllElements();
            for (byte b = 0; b < size; b = (byte) (b + 1)) {
                Card elementAt = this.selectedCards.elementAt(b);
                this.allOutCards[1].add(elementAt);
                this.myCards.removeElement(elementAt);
            }
            int[] iArr = this.allCardNum;
            iArr[1] = iArr[1] - size;
            resetThinkTime((byte) -1);
            this.talkWho = -1;
        }
        this.sendHandle.outCard(this.selectedCards);
        this.selectedCards.removeAllElements();
        setMyCardsFocused((byte) 0);
        setMyCardsLayout(false);
        this.ctlPass.setVisible(false);
        this.ctlReset.setVisible(false);
        this.ctlTiShi.setVisible(false);
        this.ctlOut.setVisible(false);
    }

    public void removeControlGroup(ControlGroup controlGroup) {
        Enumeration elements = this.controlGroups.elements();
        while (elements.hasMoreElements()) {
            ControlGroup controlGroup2 = (ControlGroup) elements.nextElement();
            if (controlGroup2.equals(controlGroup)) {
                if ("cgConfirmTimerFreeOut".equals(controlGroup.id)) {
                    exitGame(false);
                } else if ("cgConfirmQuitTimer".equals(controlGroup.id)) {
                    this.handle.sendEmptyMessage(107);
                }
                this.controlGroups.remove(controlGroup2);
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void removeParentFromGroupList(FrameControlV1 frameControlV1) {
        ControlGroup controlGroup = (ControlGroup) frameControlV1.getSetObj(1, null, false);
        if (controlGroup != null) {
            removeControlGroup(controlGroup);
            frameControlV1.getSetObj(1, null, true);
        }
    }

    public void removeTipDialog() {
        if (this.cgTipDialog != null) {
            if ("cgChat".equals(this.cgTipDialog.id)) {
                this.view.chatDlg.remove();
            } else if ("cgSettingSound".equals(this.cgTipDialog.id) || "cgSettingBright".equals(this.cgTipDialog.id) || "cgSettingHint".equals(this.cgTipDialog.id)) {
                SysSetting.getInstance(this.gameActivity).save();
                this.ctlSetting.setStatus((byte) 0);
                this.ctlSetting.isFixed = false;
            } else if ("cgQuit".equals(this.cgTipDialog.id) || "cgConfirmQuit".equals(this.cgTipDialog.id) || "cgConfirmChangeDesk".equals(this.cgTipDialog.id)) {
                this.ctlQuit.setStatus((byte) 0);
                this.ctlQuit.isFixed = false;
            }
            removeControlGroup(this.cgTipDialog);
            this.cgTipDialog = null;
        }
    }

    /* JADX WARN: Type inference failed for: r16v0, types: [byte[]] */
    /* JADX WARN: Type inference failed for: r4v14, types: [int[]] */
    /* JADX WARN: Type inference failed for: r6v10, types: [byte] */
    /* JADX WARN: Type inference failed for: r0v9, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void replay(byte r10, byte r11, byte r12, byte r13, int r14, byte[] r15, byte[] r16, byte[] r17, java.util.Vector<java.lang.Integer>[] r18, byte r19, byte r20) {
        /*
            r9 = this;
            android.os.Handler r2 = r9.handle
            r3 = 111(0x6f, float:1.56E-43)
            r2.sendEmptyMessage(r3)
            r2 = 0
            r9.continueReplayTimes = r2
            r9.restartGame()
            if (r10 != 0) goto L_0x0067
            r2 = 0
            r9.curStatus = r2
        L_0x0012:
            if (r10 == 0) goto L_0x0017
            r2 = 1
            if (r10 != r2) goto L_0x006b
        L_0x0017:
            com.tencent.qqgame.lord.common.FrameControlV1 r2 = r9.ctlChangeDesk
            r3 = 0
            r2.setStatus(r3)
            com.tencent.qqgame.lord.common.FrameControlV1 r2 = r9.ctlTrusteeship
            r3 = 1
            r2.setStatus(r3)
            r2 = 2
            java.lang.Boolean r3 = java.lang.Boolean.FALSE
            r9.setMenuStatus(r2, r3)
            r2 = 0
            java.lang.Boolean r3 = java.lang.Boolean.TRUE
            r9.setMenuStatus(r2, r3)
        L_0x002f:
            com.tencent.qqgame.common.Player r2 = r9.me
            r3 = 4
            r2.status = r3
            com.tencent.qqgame.common.Player r2 = r9.me
            short r2 = r2.seatId
            byte r2 = com.tencent.qqgame.common.Table.getOppositeSeatId(r11, r2)
            r9.lordSeatId = r2
            r2 = 1
            if (r10 != r2) goto L_0x0044
            r2 = -1
            r9.lordSeatId = r2
        L_0x0044:
            com.tencent.qqgame.common.Player r2 = r9.me
            short r2 = r2.seatId
            byte r2 = com.tencent.qqgame.common.Table.getOppositeSeatId(r12, r2)
            r9.talkWho = r2
            r9.backPoint = r13
            r9.multiple = r14
            if (r15 != 0) goto L_0x0084
            r2 = 0
        L_0x0055:
            r3 = 0
        L_0x0056:
            if (r3 >= r2) goto L_0x0086
            java.util.Vector<com.tencent.qqgame.common.Card> r4 = r9.dCards
            byte r5 = r15[r3]
            com.tencent.qqgame.common.Card r5 = r9.getCard(r5)
            r4.set(r3, r5)
            int r3 = r3 + 1
            byte r3 = (byte) r3
            goto L_0x0056
        L_0x0067:
            r2 = -2
            r9.curStatus = r2
            goto L_0x0012
        L_0x006b:
            r2 = 2
            java.lang.Boolean r3 = java.lang.Boolean.TRUE
            r9.setMenuStatus(r2, r3)
            r2 = 0
            java.lang.Boolean r3 = java.lang.Boolean.FALSE
            r9.setMenuStatus(r2, r3)
            com.tencent.qqgame.lord.common.FrameControlV1 r2 = r9.ctlTrusteeship
            r3 = 0
            r2.setStatus(r3)
            com.tencent.qqgame.lord.common.FrameControlV1 r2 = r9.ctlChangeDesk
            r3 = 1
            r2.setStatus(r3)
            goto L_0x002f
        L_0x0084:
            int r2 = r15.length
            goto L_0x0055
        L_0x0086:
            if (r16 == 0) goto L_0x00a1
            r0 = r16
            int r0 = r0.length
            r2 = r0
        L_0x008c:
            r3 = 0
        L_0x008d:
            if (r3 >= r2) goto L_0x00a3
            int[] r4 = r9.allCardNum
            com.tencent.qqgame.common.Player r5 = r9.me
            short r5 = r5.seatId
            byte r5 = com.tencent.qqgame.common.Table.getOppositeSeatId(r3, r5)
            byte r6 = r16[r3]
            r4[r5] = r6
            int r3 = r3 + 1
            byte r3 = (byte) r3
            goto L_0x008d
        L_0x00a1:
            r2 = 0
            goto L_0x008c
        L_0x00a3:
            if (r17 != 0) goto L_0x00de
            r2 = 0
        L_0x00a6:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "myCardList.length:"
            java.lang.StringBuilder r3 = r3.append(r4)
            r0 = r17
            int r0 = r0.length
            r4 = r0
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r3 = r3.toString()
            com.tencent.qqgame.hall.common.Tools.debug(r3)
            r3 = 0
        L_0x00c1:
            if (r3 >= r2) goto L_0x00e3
            byte r4 = r17[r3]
            com.tencent.qqgame.common.Card r4 = r9.getCard(r4)
            java.util.Vector<com.tencent.qqgame.common.Card> r5 = r9.myCards
            r5.addElement(r4)
            android.graphics.Bitmap r5 = r4.imgCard
            if (r5 != 0) goto L_0x00da
            com.tencent.qqgame.lord.ui.LordView r5 = r9.view
            android.graphics.Bitmap r5 = r5.drawMyCardImg(r4)
            r4.imgCard = r5
        L_0x00da:
            int r3 = r3 + 1
            byte r3 = (byte) r3
            goto L_0x00c1
        L_0x00de:
            r0 = r17
            int r0 = r0.length
            r2 = r0
            goto L_0x00a6
        L_0x00e3:
            java.util.Vector<com.tencent.qqgame.common.Card> r2 = r9.myCards
            int r2 = r2.size()
            int[] r3 = r9.allCardNum
            r4 = 1
            r3 = r3[r4]
            if (r2 >= r3) goto L_0x011e
            byte r2 = r9.lordSeatId
            r3 = 1
            if (r2 != r3) goto L_0x011e
            r2 = 0
        L_0x00f6:
            r3 = 3
            if (r2 >= r3) goto L_0x011e
            java.util.Vector<com.tencent.qqgame.common.Card> r3 = r9.dCards
            java.lang.Object r11 = r3.elementAt(r2)
            com.tencent.qqgame.common.Card r11 = (com.tencent.qqgame.common.Card) r11
            java.util.Vector<com.tencent.qqgame.common.Card> r3 = r9.myCards
            boolean r3 = r3.contains(r11)
            if (r3 != 0) goto L_0x011a
            java.util.Vector<com.tencent.qqgame.common.Card> r3 = r9.myCards
            r3.add(r11)
            android.graphics.Bitmap r3 = r11.imgCard
            if (r3 != 0) goto L_0x011a
            com.tencent.qqgame.lord.ui.LordView r3 = r9.view
            android.graphics.Bitmap r3 = r3.drawMyCardImg(r11)
            r11.imgCard = r3
        L_0x011a:
            int r2 = r2 + 1
            byte r2 = (byte) r2
            goto L_0x00f6
        L_0x011e:
            java.util.Vector<com.tencent.qqgame.common.Card> r2 = r9.myCards
            r3 = 1
            com.tencent.qqgame.common.Card.sortCardList(r2, r3)
            r2 = 1
            r9.setMyCardsLayout(r2)
            if (r18 != 0) goto L_0x014a
            r2 = 0
        L_0x012b:
            r3 = 3
            byte[] r3 = new byte[r3]
            r4 = 0
        L_0x012f:
            if (r4 >= r2) goto L_0x0151
            com.tencent.qqgame.common.Player r5 = r9.me
            short r5 = r5.seatId
            byte r5 = com.tencent.qqgame.common.Table.getOppositeSeatId(r4, r5)
            r6 = r18[r4]
            if (r6 == 0) goto L_0x014f
            r6 = r18[r4]
            int r6 = r6.size()
        L_0x0143:
            byte r6 = (byte) r6
            r3[r5] = r6
            int r4 = r4 + 1
            byte r4 = (byte) r4
            goto L_0x012f
        L_0x014a:
            r0 = r18
            int r0 = r0.length
            r2 = r0
            goto L_0x012b
        L_0x014f:
            r6 = 0
            goto L_0x0143
        L_0x0151:
            r3 = 0
        L_0x0152:
            if (r3 >= r2) goto L_0x0195
            com.tencent.qqgame.common.Player r4 = r9.me
            short r4 = r4.seatId
            byte r4 = com.tencent.qqgame.common.Table.getOppositeSeatId(r3, r4)
            r5 = r18[r3]
            if (r5 == 0) goto L_0x0191
            java.util.Vector<com.tencent.qqgame.common.Card>[] r5 = r9.allOutCards
            r5 = r5[r4]
            r5.removeAllElements()
            r5 = r18[r3]
            int r5 = r5.size()
            r6 = 0
        L_0x016e:
            if (r6 >= r5) goto L_0x018b
            java.util.Vector<com.tencent.qqgame.common.Card>[] r7 = r9.allOutCards
            r7 = r7[r4]
            r8 = r18[r3]
            java.lang.Object r11 = r8.elementAt(r6)
            java.lang.Integer r11 = (java.lang.Integer) r11
            byte r8 = r11.byteValue()
            com.tencent.qqgame.common.Card r8 = r9.getCard(r8)
            r7.addElement(r8)
            int r6 = r6 + 1
            byte r6 = (byte) r6
            goto L_0x016e
        L_0x018b:
            r5 = 1
            if (r4 != r5) goto L_0x0191
            r9.setMyOutCardsFinalPosition()
        L_0x0191:
            int r3 = r3 + 1
            byte r3 = (byte) r3
            goto L_0x0152
        L_0x0195:
            r0 = r19
            r1 = r9
            r1.roomType = r0
            r2 = 1
            if (r10 != r2) goto L_0x01d1
            r9.maxScore = r13
            r2 = 1
            if (r12 != r2) goto L_0x01b4
            android.content.res.Resources r2 = r9.res
            r3 = 2131165351(0x7f0700a7, float:1.7944917E38)
            java.lang.String r2 = r2.getString(r3)
            r9.showNotification(r2)
            r9.setMyTurnJiaoFenControl()
            r2 = 1
            r9.isMeCall = r2
        L_0x01b4:
            r2 = 0
        L_0x01b5:
            r3 = 3
            if (r2 >= r3) goto L_0x01c7
            java.util.Vector<com.tencent.qqgame.common.Card> r3 = r9.dCards
            r4 = 55
            com.tencent.qqgame.common.Card r4 = r9.getCard(r4)
            r3.set(r2, r4)
            int r2 = r2 + 1
            byte r2 = (byte) r2
            goto L_0x01b5
        L_0x01c7:
            r2 = -2
            r9.curStatus = r2
        L_0x01ca:
            r0 = r9
            r1 = r20
            r0.resetThinkTime(r1)
            return
        L_0x01d1:
            r2 = 2
            if (r10 != r2) goto L_0x01ca
            r2 = 1
            if (r12 != r2) goto L_0x01ca
            java.util.Vector r2 = r9.getLastPlayerOutCards()
            r9.setMyTurnPlayControl(r2)
            goto L_0x01ca
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.lord.logic.LordLogic.replay(byte, byte, byte, byte, int, byte[], byte[], byte[], java.util.Vector[], byte, byte):void");
    }

    public void resetThinkTime(byte b) {
        this.thinkTime = b;
        this.view.apClock.reset();
        if (b >= 0 && b != 30) {
            this.view.apClock.setFrame(30 - b);
        }
    }

    /* access modifiers changed from: protected */
    public void restartGame() {
        this.view.aniManager.stopSendCardAni();
        this.curStatus = 0;
        this.ctlStartGame.setStatus((byte) 0);
        this.ctlStartGame.setVisible(true);
        this.isMeCall = false;
        this.isPlayerCalled = false;
        this.talkWho = -1;
        this.maxScore = 0;
        this.backPoint = 0;
        this.multiple = 1;
        this.forceTrusteeTimes = 0;
        this.continueOutTimes = 0;
        if (this.isTrusteeship) {
            this.view.aniManager.playSubstituteAni(false);
            this.isTrusteeship = false;
        }
        resetThinkTime((byte) 30);
        this.view.apToolbar.reset();
        this.lordSeatId = -1;
        this.myCards.removeAllElements();
        this.dCards.removeAllElements();
        Card card = getCard((byte) 0);
        for (byte b = 0; b < 3; b = (byte) (b + 1)) {
            this.dCards.add(card);
        }
        this.selectedCards.removeAllElements();
        for (byte b2 = 0; b2 < 3; b2 = (byte) (b2 + 1)) {
            this.allOutCards[b2].removeAllElements();
            this.allLeaveCards[b2].removeAllElements();
            this.allCardNum[b2] = 0;
            this.playerSay[b2] = -1;
            this.allPlayerScore[b2] = 0;
        }
        int size = this.allCards.size();
        for (byte b3 = 0; b3 < size; b3 = (byte) (b3 + 1)) {
            Card elementAt = this.allCards.elementAt(b3);
            elementAt.isFocused = false;
            elementAt.isSelected = false;
            elementAt.isTouched = false;
            if (elementAt.imgCard != null) {
                if (!elementAt.imgCard.isRecycled()) {
                    elementAt.imgCard.recycle();
                }
                elementAt.imgCard = null;
            }
            elementAt.offset = 0;
        }
        for (byte b4 = 0; b4 < 20; b4 = (byte) (b4 + 1)) {
            this.ctlMyCards[b4].setLocation(0, 0, 0, 0);
            this.ctlMyOutCards[b4].setLocation(0, 0, 0, 0);
        }
        this.curFocusCardIndex = -1;
        this.me.status = 2;
        if (this.isViewGame) {
            setViewGameCtlStatus();
        } else {
            setMenuStatus((byte) 0, Boolean.TRUE);
            setMenuStatus((byte) 2, Boolean.FALSE);
            this.ctlChangeDesk.setStatus((byte) 0);
            this.ctlTrusteeship.setStatus((byte) 1);
        }
        this.view.aniManager.init();
        this.soundPlayer.playWelcomeBackGroundSound();
    }

    public void run(short s, int i) {
        if (this.soundPlayer != null) {
            this.soundPlayer.playTurnSound();
        }
        if (this.view.aniManager != null) {
            this.view.aniManager.update(s, i);
        }
        if (i % (IGameView.MSG_2GAME_DIS_CONNECTED / s) == 0) {
            if (this.thinkTime > 0) {
                this.thinkTime = (byte) (this.thinkTime - 1);
            }
            if (this.gameTimeOut >= 0) {
                this.gameTimeOut++;
            }
            if (this.gameTimeOut >= 60) {
                this.gameTimeOut = -1;
                if (this.isViewGame || this.curStatus != -2) {
                    this.handle.sendEmptyMessage(109);
                    ControlGroup controlGroup = new ControlGroup("cgConfirmQuitTimer", (byte) 9);
                    this.controlGroups.add(controlGroup);
                    controlGroup.obj = this.res.getString(R.string.game_offlined);
                    this.ctlOK.getSetInt(0, 2, true);
                    controlGroup.addFrame(this.ctlOK);
                    this.view.apButton.reset(true);
                } else {
                    this.continueReplayTimes = (byte) (this.continueReplayTimes + 1);
                    this.handle.sendEmptyMessage(106);
                }
            }
            if (!this.isChangingDesk && !this.isExitingGame) {
                if (!this.isViewGame && this.curStatus == 0 && this.thinkTime <= 3 && this.thinkTime >= 0 && this.me.status == 2 && this.continueReplayTimes <= 0 && this.thinkTime == 0) {
                    this.handle.sendEmptyMessage(101);
                    this.thinkTime = -1;
                    if (!this.isExitingGame) {
                        ControlGroup controlGroup2 = new ControlGroup("cgConfirmTimerFreeOut", (byte) 9);
                        this.controlGroups.add(controlGroup2);
                        controlGroup2.obj = this.res.getString(R.string.game_free_timeout);
                        this.ctlOK.getSetInt(0, 4, true);
                        controlGroup2.addFrame(this.ctlOK);
                        this.view.apButton.reset(true);
                    }
                }
                if (!this.isViewGame && this.curStatus == -2 && this.talkWho == 1 && this.thinkTime <= 3 && this.thinkTime >= 0) {
                    if (this.thinkTime == 0) {
                        if (this.isMeCall) {
                            callScore((byte) 0);
                        } else {
                            this.continueOutTimes = (byte) (this.continueOutTimes + 1);
                            if (this.continueOutTimes >= 2) {
                                onTrusteeship();
                                this.forceTrusteeTimes = (byte) (this.forceTrusteeTimes + 1);
                                if (this.forceTrusteeTimes >= 2) {
                                    setTipMessage(this.res.getString(R.string.game_trusteeship_always));
                                } else {
                                    setTipMessage(this.res.getString(R.string.game_force_trusteeship));
                                }
                                this.continueOutTimes = 0;
                            } else if (getLastPlayerOutCards() == null) {
                                setCardsSelectStatus(this.selectedCards, false);
                                this.selectedCards.removeAllElements();
                                if (this.myCards.size() > 0) {
                                    selectCard((byte) (this.myCards.size() - 1), false);
                                    outCards(false);
                                }
                            } else {
                                outCards(true);
                            }
                        }
                        this.thinkTime = -1;
                    } else if (!this.isTrusteeship) {
                    }
                }
                if (!this.isViewGame && this.curStatus == -3 && this.thinkTime == 0) {
                    restartGame();
                }
                if (this.tipMessage == null) {
                    return;
                }
                if (this.timerTipShow > 0) {
                    this.timerTipShow = (byte) (this.timerTipShow - 1);
                    return;
                }
                this.tipMessage = null;
                this.timerTipShow = 3;
            } else if (this.thinkTime == 0) {
                exitGame(true);
                this.thinkTime = -1;
            }
        }
    }

    public void runLast(short s, int i) {
        if (this.thinkTime >= 0) {
            this.view.apClock.update(s);
            if (this.view.apClock.getCurFrame() == 24) {
                this.soundPlayer.playClockRingSound();
            }
        }
        if (this.view.apButton.getIsReadyForUpdate() && !this.view.apButton.update(s)) {
            onControlTouchedV1(this.ctlOK, 1, 0, 0);
            this.view.apButton.reset();
        }
        int size = this.table.playerList.size();
        for (byte b = 0; b < size; b = (byte) (b + 1)) {
            GameMessage lastPlayerChatMessage = getLastPlayerChatMessage(this.table.playerList.elementAt(b));
            if (lastPlayerChatMessage != null) {
                lastPlayerChatMessage.timerShow -= s;
                if (lastPlayerChatMessage.timerShow <= 0) {
                    this.chatMessageList.remove(lastPlayerChatMessage);
                }
            }
        }
        if (this.isTouchMovingdCards) {
            int size2 = this.myCards.size();
            int i2 = 0;
            int i3 = size2 - 1;
            boolean z = true;
            for (byte b2 = 0; b2 < i3; b2 = (byte) (b2 + 1)) {
                Card elementAt = this.myCards.elementAt(b2);
                if (elementAt.anim_type == 1) {
                    if (((double) elementAt.offset) * 1.2d > 42.0d) {
                        elementAt.offset = 42;
                        elementAt.anim_type = 0;
                    } else {
                        elementAt.offset = (byte) ((int) (((double) elementAt.offset) * 1.2d));
                        z = false;
                    }
                } else if (elementAt.anim_type == 2) {
                    if (((double) elementAt.offset) * 0.8d < 16.0d) {
                        elementAt.offset = 16;
                        elementAt.anim_type = 0;
                    } else {
                        elementAt.offset = (byte) ((int) (((double) elementAt.offset) * 0.8d));
                        z = false;
                    }
                }
                i2 += elementAt.offset;
            }
            this.myCards.lastElement().offset = this.view.CARD_WIDTH_L;
            int i4 = this.view.CARD_WIDTH_L + i2;
            this.isTouchMovingdCards = !z;
            byte b3 = this.view.CARD_HEIGHT_L;
            int i5 = (LordView.SCREEN_WIDTH - i4) / 2;
            for (byte b4 = 0; b4 < size2; b4 = (byte) (b4 + 1)) {
                Card elementAt2 = this.myCards.elementAt(b4);
                int i6 = elementAt2.isSelected ? LordView.MYCARD_Y - 15 : 232;
                this.ctlMyCards[b4].setLocation(i5, i6, elementAt2.offset + i5, i6 + b3);
                i5 += elementAt2.offset;
            }
        }
        if (this.isOutingCards) {
            this.outingStep = (byte) (this.outingStep + 1);
            Tools.debug("outing cards, step:" + ((int) this.outingStep));
            int size3 = this.allOutCards[1].size();
            int i7 = (LordView.SCREEN_WIDTH - (((size3 - 1) * 13) + this.view.CARD_WIDTH_M)) / 2;
            int i8 = this.view.MYOUTCARD_Y;
            boolean z2 = true;
            for (int i9 = 0; i9 < size3; i9++) {
                Card elementAt3 = this.allOutCards[1].elementAt(i9);
                Rect location = this.ctlMyOutCards[i9].getLocation();
                Rect rect = (Rect) this.ctlMyOutCards[i9].getSetObj(1, null, false);
                int abs = Math.abs(rect.left - i7);
                int abs2 = Math.abs(rect.top - i8);
                if (abs > abs2) {
                    int i10 = abs / 4;
                    if (rect.left > i7) {
                        if (location.left - i10 < i7 || this.outingStep > 4) {
                            location.left = i7;
                        } else {
                            location.left -= i10;
                            z2 = false;
                        }
                    } else if (location.left + i10 > i7 || this.outingStep > 4) {
                        location.left = i7;
                    } else {
                        location.left += i10;
                        z2 = false;
                    }
                    if (i7 != rect.left) {
                        location.top = (((i8 - rect.top) * (location.left - i7)) / (i7 - rect.left)) + i8;
                    }
                    if ((rect.top >= i8 && location.top < i8) || (rect.top <= i8 && location.top > i8)) {
                        location.top = i8;
                    }
                } else {
                    int i11 = abs2 / 4;
                    if (rect.top > i8) {
                        if (location.top - i11 < i8 || this.outingStep > 4) {
                            location.top = i8;
                        } else {
                            location.top -= i11;
                            z2 = false;
                        }
                    } else if (location.top + i11 > i8 || this.outingStep > 4) {
                        location.top = i8;
                    } else {
                        location.top += i11;
                        z2 = false;
                    }
                    if (i8 != rect.top) {
                        location.left = (((i7 - rect.left) * (location.top - i8)) / (i8 - rect.top)) + i7;
                    }
                    if ((rect.left >= i7 && location.left < i7) || (rect.left <= i7 && location.left > i7)) {
                        location.left = i7;
                    }
                }
                i7 += 13;
            }
            this.isOutingCards = !z2;
            if (!this.isOutingCards) {
                this.outingStep = 0;
                Card.sortCardList(this.allOutCards[1], (byte) 1);
                Card.sortCardList(this.myCards, (byte) 1);
                this.view.aniManager.playPlaneAndBombAni(this.allOutCards[1], this.lordSeatId);
                this.soundPlayer.playCardSound(this.allOutCards[1], this.allCardNum[1], true, checkPlayDaniSound(1));
                if (this.myCards.size() > 0) {
                    resetThinkTime((byte) 30);
                    this.allOutCards[0].removeAllElements();
                    this.playerSay[0] = -1;
                    this.talkWho = 0;
                }
                setMyCardsLayout(true);
            }
        }
    }

    public void selectCard(byte b, boolean z) {
        boolean z2;
        if (b < this.myCards.size()) {
            Card elementAt = this.myCards.elementAt(b);
            if (elementAt.isSelected) {
                elementAt.isSelected = false;
                this.selectedCards.remove(elementAt);
                z2 = false;
            } else {
                elementAt.isSelected = true;
                this.selectedCards.add(elementAt);
                z2 = true;
            }
            if (this.talkWho == 1) {
                Vector<Card> lastPlayerOutCards = getLastPlayerOutCards();
                if (!this.isTrusteeship && z) {
                    if (z2 && this.talkWho == 1) {
                        try {
                            CLogic.SelectPokerAI(this, this.selectedCards, this.myCards, lastPlayerOutCards);
                        } catch (Exception e) {
                            Tools.debug("SelectPokerAI exception");
                            e.printStackTrace();
                        }
                    }
                    if (this.selectedCards.size() > 0) {
                        this.ctlOut.setStatus((byte) 0);
                    } else {
                        this.ctlOut.setStatus((byte) 1);
                    }
                    if (this.selectedCards.size() == 0) {
                        this.ctlReset.setStatus((byte) 1);
                    } else {
                        this.ctlReset.setStatus((byte) 0);
                    }
                }
            }
            setMyCardsLayout(false);
        }
    }

    public void setBackCards(byte b, byte[] bArr) {
        this.lordSeatId = b;
        this.isMeCall = false;
        for (byte b2 = 0; b2 < bArr.length; b2 = (byte) (b2 + 1)) {
            Card card = getCard(bArr[b2]);
            this.dCards.set(b2, card);
            if (b == 1) {
                if (!this.isViewGame || this.myCards.size() <= 0 || this.myCards.elementAt(0).number != 55) {
                    this.myCards.add(card);
                } else {
                    this.myCards.add(getCard((byte) 55));
                }
                if (card.imgCard == null) {
                    card.imgCard = this.view.drawMyCardImg(card);
                }
            }
        }
        this.talkWho = b;
        if (b == 1) {
            setMyTurnPlayControl(null);
            setTipMessage(null);
            if (!this.isViewGame) {
                SearchHint.init();
            } else {
                setViewGameCtlStatus();
            }
            Card.sortCardList(this.myCards, (byte) 1);
            setMyCardsLayout(true);
        }
        this.allCardNum[b] = 20;
        this.playerSay[0] = -1;
        this.playerSay[1] = -1;
        this.playerSay[2] = -1;
        resetThinkTime((byte) 30);
        this.ctl1Fen.setVisible(false);
        this.ctl2Fen.setVisible(false);
        this.ctl3Fen.setVisible(false);
        this.ctlNoCall.setVisible(false);
        if (!this.isViewGame) {
            setMenuStatus((byte) 2, Boolean.TRUE);
            this.ctlTrusteeship.setStatus((byte) 0);
        }
        this.soundPlayer.playShowCardSound();
    }

    public void setGameResult(byte[][] bArr, int[] iArr) {
        this.handle.sendEmptyMessage(101);
        try {
            Thread.sleep(2500);
        } catch (InterruptedException e) {
        }
        if (this.isTrusteeship) {
            this.view.aniManager.playSubstituteAni(false);
            Message obtainMessage = this.handle.obtainMessage(100);
            obtainMessage.arg1 = 2;
            obtainMessage.arg2 = R.string.menu_trusteeship;
            this.handle.sendMessage(obtainMessage);
            this.isTrusteeship = false;
        }
        if (bArr != null) {
            if (iArr != null) {
                for (byte b = 0; b < iArr.length; b = (byte) (b + 1)) {
                    this.allPlayerScore[b] = iArr[b];
                }
            }
            for (byte b2 = 0; b2 < bArr.length; b2 = (byte) (b2 + 1)) {
                int length = bArr[b2].length;
                for (byte b3 = 0; b3 < length; b3 = (byte) (b3 + 1)) {
                    byte b4 = bArr[b2][b3];
                    if (b4 > 0 && b4 < 55) {
                        this.allLeaveCards[b2].add(getCard(b4));
                    }
                }
                Card.sortCardList(this.allLeaveCards[b2], (byte) 1);
            }
            setAllPlayerState(2);
            this.curStatus = -3;
            if (this.isOutingCards) {
                this.isOutingCards = false;
                this.outingStep = 0;
                setMyOutCardsFinalPosition();
            }
            resetThinkTime((byte) -1);
            this.ctlPass.setVisible(false);
            this.ctlReset.setVisible(false);
            this.ctlTiShi.setVisible(false);
            this.ctlOut.setVisible(false);
            if (this.lordSeatId >= 0) {
                this.view.aniManager.playGameOverAni(this.allPlayerScore[this.lordSeatId]);
                this.soundPlayer.stopBackGroundSound(false);
                this.soundPlayer.playResultSound(this.allPlayerScore[this.lordSeatId]);
            }
            if (!this.isViewGame) {
                setMenuStatus((byte) 0, Boolean.TRUE);
                setMenuStatus((byte) 2, Boolean.FALSE);
                this.ctlChangeDesk.setStatus((byte) 0);
                this.ctlTrusteeship.setStatus((byte) 1);
            }
        }
    }

    public void setMyCards(byte[] bArr, byte b) {
        if (this.isViewGame) {
            restartGame();
            this.curStatus = -2;
        }
        AActivity.soundPlayer.init();
        AActivity.soundPlayer.playNormalBackGroundSound();
        this.talkWho = b;
        if (this.isPlayerCalled) {
            setTipMessage(this.res.getString(R.string.game_tips_newcardinfo));
            for (byte b2 = 0; b2 < 3; b2 = (byte) (b2 + 1)) {
                this.playerSay[b2] = -1;
            }
            this.maxScore = 0;
        }
        this.myCards.removeAllElements();
        for (byte b3 = 0; b3 < bArr.length; b3 = (byte) (b3 + 1)) {
            Card card = getCard(bArr[b3]);
            this.myCards.add(card);
            if (card.imgCard == null) {
                card.imgCard = this.view.drawMyCardImg(card);
            }
        }
        if (this.talkWho == 1) {
            if (!this.isViewGame && !this.isTrusteeship) {
                showNotification(this.res.getString(R.string.game_home_tip_call));
                setMyTurnJiaoFenControl();
            }
            this.isMeCall = true;
        }
        if (!this.isViewGame) {
            setMenuStatus((byte) 0, Boolean.FALSE);
            this.ctlChangeDesk.setStatus((byte) 1);
        }
        this.soundPlayer.playSendCardSound();
        this.view.aniManager.playSendCardAni();
    }

    public void setMyCardsFocused(byte b) {
        for (byte b2 = 0; b2 < this.myCards.size(); b2 = (byte) (b2 + 1)) {
            this.myCards.elementAt(b2).isFocused = false;
        }
        if (b >= 0 && b < this.myCards.size()) {
            this.curFocusCardIndex = b;
            this.myCards.elementAt(b).isFocused = true;
        }
    }

    public void setMyStatus() {
        for (byte b = 0; b < 3; b = (byte) (b + 1)) {
            this.allCardNum[b] = 17;
        }
        setMyCardsFocused((byte) 0);
        resetThinkTime((byte) 30);
        Card.sortCardList(this.myCards, (byte) 1);
        setMyCardsFocused((byte) -1);
        setMyCardsLayout(true);
    }

    public void setOutCards(byte b, byte b2, byte[] bArr, byte b3) {
        Tools.debug("talker:" + ((int) b) + ",nextTalker:" + ((int) b2));
        this.talkWho = b2;
        if (b3 > 0) {
            this.multiple = b3;
        }
        int length = bArr != null ? bArr.length : 0;
        if (b != 1 || this.isViewGame) {
            this.allOutCards[b].removeAllElements();
            for (byte b4 = 0; b4 < length; b4 = (byte) (b4 + 1)) {
                this.allOutCards[b].add(getCard(bArr[b4]));
            }
            Card.sortCardList(this.allOutCards[b], (byte) 1);
            if (this.isViewGame && b == 1) {
                if (!this.isCanViewCards) {
                    for (byte b5 = 0; b5 < length; b5 = (byte) (b5 + 1)) {
                        if (this.myCards.size() > 0) {
                            this.myCards.removeElementAt(0);
                        }
                    }
                    setMyCardsLayout(true);
                    setMyOutCardsFinalPosition();
                    resetThinkTime((byte) 30);
                } else {
                    this.selectedCards.removeAllElements();
                    for (byte b6 = 0; b6 < length; b6 = (byte) (b6 + 1)) {
                        Card elementAt = this.allOutCards[b].elementAt(b6);
                        elementAt.isSelected = true;
                        this.selectedCards.add(elementAt);
                    }
                    prepareOutingAnim();
                    for (byte b7 = 0; b7 < length; b7 = (byte) (b7 + 1)) {
                        this.myCards.removeElement(this.allOutCards[b].elementAt(b7));
                    }
                    Card.sortCardList(this.myCards, (byte) 1);
                }
            }
            int[] iArr = this.allCardNum;
            iArr[b] = iArr[b] - length;
            if (length == 0) {
                switch (b) {
                    case 0:
                        this.playerSay[b] = 6;
                        this.soundPlayer.playPassSound();
                        break;
                    case 1:
                        this.playerSay[b] = 11;
                        if (this.isViewGame) {
                            this.soundPlayer.playPassSound();
                            break;
                        }
                        break;
                    case 2:
                        this.playerSay[b] = 1;
                        this.soundPlayer.playPassSound();
                        break;
                }
            } else if (b != 1 || !this.isViewGame || !this.isCanViewCards) {
                if (this.allCardNum[b] <= 3 && this.allCardNum[b] > 0) {
                    Tools.debug("allCardNum[talker]: " + this.allCardNum[b]);
                    this.view.aniManager.playCardNumAni(b);
                }
                this.view.aniManager.playPlaneAndBombAni(this.allOutCards[b], this.lordSeatId);
                this.soundPlayer.playCardSound(this.allOutCards[b], this.allCardNum[b], false, checkPlayDaniSound(b));
            }
            if (this.allCardNum[b] == 0) {
                resetThinkTime((byte) -1);
                this.talkWho = -1;
            } else if (this.talkWho == 1 || this.talkWho == 2) {
                this.allOutCards[this.talkWho].removeAllElements();
                this.playerSay[this.talkWho] = -1;
                resetThinkTime((byte) 30);
            }
        } else {
            boolean z = true;
            if (length != this.allOutCards[b].size()) {
                z = false;
            } else {
                byte b8 = 0;
                while (true) {
                    if (b8 >= length) {
                        break;
                    } else if (!this.allOutCards[b].contains(getCard(bArr[b8]))) {
                        z = false;
                        break;
                    } else {
                        b8 = (byte) (b8 + 1);
                    }
                }
            }
            if (!z) {
                int size = this.allOutCards[b].size();
                int i = 0;
                for (byte b9 = 0; b9 < size; b9 = (byte) (b9 + 1)) {
                    Card elementAt2 = this.allOutCards[b].elementAt(b9);
                    if (!this.myCards.contains(elementAt2)) {
                        this.myCards.add(elementAt2);
                        i++;
                    }
                }
                int[] iArr2 = this.allCardNum;
                iArr2[b] = iArr2[b] + i;
                this.allOutCards[b].removeAllElements();
                byte b10 = this.view.CARD_HEIGHT_M;
                int i2 = this.view.MYOUTCARD_Y;
                int i3 = (LordView.SCREEN_WIDTH - (((length - 1) * 13) + this.view.CARD_WIDTH_M)) / 2;
                for (byte b11 = 0; b11 < length; b11 = (byte) (b11 + 1)) {
                    this.allOutCards[b].addElement(getCard(bArr[b11]));
                    this.ctlMyOutCards[b11].setLocation(i3, i2, i3 + 13, i2 + b10);
                    i3 += 13;
                }
                for (byte b12 = 0; b12 < length; b12 = (byte) (b12 + 1)) {
                    this.myCards.removeElement(this.allOutCards[b].elementAt(b12));
                }
                Card.sortCardList(this.allOutCards[1], (byte) 1);
                Card.sortCardList(this.myCards, (byte) 1);
                int[] iArr3 = this.allCardNum;
                iArr3[b] = iArr3[b] - length;
                if (this.isOutingCards) {
                    this.isOutingCards = false;
                    this.outingStep = 0;
                    this.view.aniManager.playPlaneAndBombAni(this.allOutCards[1], this.lordSeatId);
                    this.soundPlayer.playCardSound(this.allOutCards[1], this.allCardNum[1], true, checkPlayDaniSound(1));
                    if (this.myCards.size() > 0) {
                        resetThinkTime((byte) 30);
                        this.allOutCards[0].removeAllElements();
                        this.playerSay[0] = -1;
                        this.talkWho = 0;
                    } else {
                        this.talkWho = -1;
                    }
                }
                setMyCardsLayout(true);
            }
        }
        if (this.talkWho == 1 && this.myCards.size() > 0) {
            if (this.isViewGame) {
                setMyTurnPlayControl(null);
                setViewGameCtlStatus();
            } else if (this.isTrusteeship) {
                trusteeshipPlay();
            } else {
                this.isOutingCards = false;
                showNotification(this.res.getString(R.string.game_home_tip_turn));
                setMyTurnPlayControl(this.allOutCards[b]);
                SearchHint.init();
                Vector<Card> lastPlayerOutCards = getLastPlayerOutCards();
                if (this.myCards.size() == 1 && lastPlayerOutCards == null) {
                    setCardsSelectStatus(this.selectedCards, false);
                    this.selectedCards.removeAllElements();
                    selectCard((byte) 0, false);
                    outCards(false);
                } else if (this.myCards.size() == 1 && lastPlayerOutCards != null && lastPlayerOutCards.size() > 1) {
                    outCards(true);
                } else if (lastPlayerOutCards != null && CLogic.getCardType(lastPlayerOutCards) == 14) {
                    outCards(true);
                } else if (!CLogic.checkHasBiggerCard(this.myCards, lastPlayerOutCards)) {
                    this.view.aniManager.playNoLargerCardTips();
                    this.allOutCards[1].removeAllElements();
                    this.playerSay[1] = -1;
                    resetThinkTime((byte) 10);
                }
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v13, resolved type: int[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void viewGame(byte r7, byte r8, byte r9, byte r10, byte r11, boolean r12, byte[] r13, byte[] r14, java.util.Vector<java.lang.Integer>[] r15, byte[] r16) {
        /*
            r6 = this;
            if (r7 != 0) goto L_0x001b
            r0 = 0
            r6.curStatus = r0
        L_0x0005:
            if (r12 == 0) goto L_0x001f
            if (r13 == 0) goto L_0x001f
            r0 = 0
        L_0x000a:
            int r1 = r13.length
            if (r0 >= r1) goto L_0x001f
            java.util.Vector<com.tencent.qqgame.common.Card> r1 = r6.dCards
            byte r2 = r13[r0]
            com.tencent.qqgame.common.Card r2 = r6.getCard(r2)
            r1.set(r0, r2)
            int r0 = r0 + 1
            goto L_0x000a
        L_0x001b:
            r0 = -2
            r6.curStatus = r0
            goto L_0x0005
        L_0x001f:
            java.util.Vector<com.tencent.qqgame.common.Card> r0 = r6.myCards
            r0.removeAllElements()
            if (r14 != 0) goto L_0x0051
            r0 = 0
        L_0x0027:
            r1 = 0
            r6.isCanViewCards = r1
            r1 = 0
        L_0x002b:
            if (r1 >= r0) goto L_0x0053
            byte r2 = r14[r1]
            r3 = 55
            if (r2 == r3) goto L_0x0036
            r2 = 1
            r6.isCanViewCards = r2
        L_0x0036:
            byte r2 = r14[r1]
            com.tencent.qqgame.common.Card r2 = r6.getCard(r2)
            java.util.Vector<com.tencent.qqgame.common.Card> r3 = r6.myCards
            r3.addElement(r2)
            android.graphics.Bitmap r3 = r2.imgCard
            if (r3 != 0) goto L_0x004d
            com.tencent.qqgame.lord.ui.LordView r3 = r6.view
            android.graphics.Bitmap r3 = r3.drawMyCardImg(r2)
            r2.imgCard = r3
        L_0x004d:
            int r1 = r1 + 1
            byte r1 = (byte) r1
            goto L_0x002b
        L_0x0051:
            int r0 = r14.length
            goto L_0x0027
        L_0x0053:
            java.util.Vector<com.tencent.qqgame.common.Card> r0 = r6.myCards
            r1 = 1
            com.tencent.qqgame.common.Card.sortCardList(r0, r1)
            r0 = 1
            r6.setMyCardsLayout(r0)
            r0 = 0
        L_0x005e:
            r1 = 3
            if (r0 >= r1) goto L_0x00aa
            r1 = r15[r0]
            if (r1 == 0) goto L_0x00a7
            r1 = r15[r0]
            int r1 = r1.size()
            if (r1 <= 0) goto L_0x00a7
            byte r1 = (byte) r0
            com.tencent.qqgame.common.Player r2 = r6.me
            short r2 = r2.seatId
            byte r1 = com.tencent.qqgame.common.Table.getOppositeSeatId(r1, r2)
            java.util.Vector<com.tencent.qqgame.common.Card>[] r2 = r6.allOutCards
            r2 = r2[r1]
            r2.removeAllElements()
            r2 = r15[r0]
            int r2 = r2.size()
            r3 = 0
        L_0x0084:
            if (r3 >= r2) goto L_0x00a1
            java.util.Vector<com.tencent.qqgame.common.Card>[] r4 = r6.allOutCards
            r4 = r4[r1]
            r5 = r15[r0]
            java.lang.Object r12 = r5.elementAt(r3)
            java.lang.Integer r12 = (java.lang.Integer) r12
            byte r5 = r12.byteValue()
            com.tencent.qqgame.common.Card r5 = r6.getCard(r5)
            r4.addElement(r5)
            int r3 = r3 + 1
            byte r3 = (byte) r3
            goto L_0x0084
        L_0x00a1:
            r2 = 1
            if (r1 != r2) goto L_0x00a7
            r6.setMyOutCardsFinalPosition()
        L_0x00a7:
            int r0 = r0 + 1
            goto L_0x005e
        L_0x00aa:
            r0 = 0
        L_0x00ab:
            r1 = 3
            if (r0 >= r1) goto L_0x00eb
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "seat "
            java.lang.StringBuilder r1 = r1.append(r2)
            byte r2 = (byte) r0
            com.tencent.qqgame.common.Player r3 = r6.me
            short r3 = r3.seatId
            byte r2 = com.tencent.qqgame.common.Table.getOppositeSeatId(r2, r3)
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r2 = ": "
            java.lang.StringBuilder r1 = r1.append(r2)
            byte r2 = r16[r0]
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.tencent.qqgame.hall.common.Tools.debug(r1)
            int[] r1 = r6.allCardNum
            byte r2 = (byte) r0
            com.tencent.qqgame.common.Player r3 = r6.me
            short r3 = r3.seatId
            byte r2 = com.tencent.qqgame.common.Table.getOppositeSeatId(r2, r3)
            byte r3 = r16[r0]
            r1[r2] = r3
            int r0 = r0 + 1
            goto L_0x00ab
        L_0x00eb:
            r6.backPoint = r9
            r6.multiple = r10
            com.tencent.qqgame.common.Player r0 = r6.me
            short r0 = r0.seatId
            byte r0 = com.tencent.qqgame.common.Table.getOppositeSeatId(r11, r0)
            r6.talkWho = r0
            r0 = 1
            if (r7 != r0) goto L_0x0105
            byte r0 = r6.talkWho
            r1 = 1
            if (r0 != r1) goto L_0x0104
            r0 = 1
            r6.isMeCall = r0
        L_0x0104:
            return
        L_0x0105:
            com.tencent.qqgame.common.Player r0 = r6.me
            short r0 = r0.seatId
            byte r0 = com.tencent.qqgame.common.Table.getOppositeSeatId(r8, r0)
            r6.lordSeatId = r0
            goto L_0x0104
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.lord.logic.LordLogic.viewGame(byte, byte, byte, byte, byte, boolean, byte[], byte[], java.util.Vector[], byte[]):void");
    }
}
