package com.tencent.qqgame.lord.common;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import java.util.ArrayList;
import java.util.List;

public class ControlGroup {
    public static final byte GROUP_TYPE_CHAT_PANEL = 11;
    public static final byte GROUP_TYPE_CHECK = 8;
    public static final byte GROUP_TYPE_MAIN = 0;
    public static final byte GROUP_TYPE_QUIT = 6;
    public static final byte GROUP_TYPE_RECHECK = 7;
    public static final byte GROUP_TYPE_SETTING_BRIGHT = 4;
    public static final byte GROUP_TYPE_SETTING_HINT = 5;
    public static final byte GROUP_TYPE_SETTING_SOUND = 3;
    public static final byte GROUP_TYPE_TIMER_CHECK = 9;
    public static final byte GROUP_TYPE_USER_INFO = 10;
    public int arg1;
    public int arg2;
    public String id = null;
    private List<FrameControlV1> list;
    public Object obj;
    public final Rect rect;
    public byte type = 0;

    public ControlGroup(String str) {
        this.id = str;
        this.rect = new Rect();
        this.list = new ArrayList();
    }

    public ControlGroup(String str, byte b) {
        this.id = str;
        this.type = b;
        this.rect = new Rect();
        this.list = new ArrayList();
    }

    public ControlGroup(String str, Rect rect2) {
        this.id = str;
        this.rect = rect2 == null ? new Rect() : rect2;
        this.list = new ArrayList();
    }

    public ControlGroup(String str, Rect rect2, byte b) {
        this.id = str;
        this.type = b;
        this.rect = rect2 == null ? new Rect() : rect2;
        this.list = new ArrayList();
    }

    public FrameControlV1 addFrame(FrameControlV1 frameControlV1) {
        frameControlV1.getSetObj(1, this, true);
        this.list.add(frameControlV1);
        return frameControlV1;
    }

    public void draw(Canvas canvas, Paint paint) {
        for (FrameControlV1 draw : this.list) {
            draw.draw(canvas, paint);
        }
    }

    public FrameControlV1 elementAt(int i) {
        if (this.list.size() > i) {
            return this.list.get(i);
        }
        return null;
    }

    public void setRect(int i, int i2, int i3, int i4) {
        this.rect.set(i, i2, i3, i4);
    }

    public int size() {
        return this.list.size();
    }
}
