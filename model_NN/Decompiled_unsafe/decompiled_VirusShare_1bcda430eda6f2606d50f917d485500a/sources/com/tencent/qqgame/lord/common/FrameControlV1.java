package com.tencent.qqgame.lord.common;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import com.tencent.qqgame.hall.common.AnimationPlayerV1;

public class FrameControlV1 {
    public static final byte STATUS_CONTROL_DISABLE = 1;
    public static final byte STATUS_CONTROL_HIGH = 2;
    public static final byte STATUS_CONTROL_NORMAL = 0;
    private static final int SYSTEM_USED_INTCOUNT = 0;
    private static final int SYSTEM_USED_OBJCOUNT = 0;
    private byte curStatus = 0;
    private byte[] frameIdList;
    private String id = null;
    private Bitmap[] imgCaches = null;
    private boolean isCacheImg = false;
    public boolean isFixed = false;
    private Object[] objData = new Object[0];
    private int paddingBottom;
    private int paddingLeft;
    private int paddingRight;
    private int paddingTop;
    private AnimationPlayerV1 player;
    private int[] propData = new int[0];
    private Rect rect;
    private boolean visible;

    public FrameControlV1(AnimationPlayerV1 animationPlayerV1, byte[] bArr) {
        init(null, animationPlayerV1, bArr, false);
    }

    public FrameControlV1(AnimationPlayerV1 animationPlayerV1, byte[] bArr, int i, int i2) {
        init(null, animationPlayerV1, bArr, false);
        setLocation(i, i2);
    }

    public FrameControlV1(AnimationPlayerV1 animationPlayerV1, byte[] bArr, int i, int i2, boolean z) {
        init(null, animationPlayerV1, bArr, z);
        setLocation(i, i2);
    }

    public FrameControlV1(AnimationPlayerV1 animationPlayerV1, byte[] bArr, boolean z) {
        init(null, animationPlayerV1, bArr, z);
    }

    public FrameControlV1(String str, AnimationPlayerV1 animationPlayerV1, byte[] bArr) {
        init(str, animationPlayerV1, bArr, false);
    }

    public FrameControlV1(String str, AnimationPlayerV1 animationPlayerV1, byte[] bArr, int i, int i2) {
        init(str, animationPlayerV1, bArr, false);
        setLocation(i, i2);
    }

    public FrameControlV1(String str, AnimationPlayerV1 animationPlayerV1, byte[] bArr, int i, int i2, boolean z) {
        init(str, animationPlayerV1, bArr, z);
        setLocation(i, i2);
    }

    public FrameControlV1(String str, AnimationPlayerV1 animationPlayerV1, byte[] bArr, boolean z) {
        init(str, animationPlayerV1, bArr, z);
    }

    private void init(String str, AnimationPlayerV1 animationPlayerV1, byte[] bArr, boolean z) {
        this.id = str;
        this.frameIdList = bArr;
        this.player = animationPlayerV1;
        this.visible = true;
        this.paddingLeft = 0;
        this.paddingRight = 0;
        this.paddingTop = 0;
        this.paddingBottom = 0;
        this.rect = new Rect();
        this.isCacheImg = z;
        if (z) {
            this.imgCaches = new Bitmap[3];
        }
    }

    public boolean contains(int i, int i2) {
        return !this.rect.isEmpty() && i >= this.rect.left - this.paddingLeft && i <= this.rect.right + this.paddingRight && i2 >= this.rect.top - this.paddingTop && i2 <= this.rect.bottom + this.paddingBottom;
    }

    public void draw(Canvas canvas, Paint paint) {
        if (this.frameIdList != null && this.player != null && this.visible) {
            byte b = this.curStatus;
            if (this.frameIdList[this.curStatus] < 0) {
                if (this.frameIdList[0] >= 0) {
                    b = 0;
                } else {
                    return;
                }
            }
            if (this.isCacheImg) {
                if (this.imgCaches == null) {
                    this.imgCaches = new Bitmap[3];
                }
                if (this.imgCaches[b] == null) {
                    Canvas canvas2 = new Canvas();
                    this.imgCaches[b] = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
                    canvas2.setBitmap(this.imgCaches[b]);
                    this.player.drawFrame(canvas2, this.frameIdList[b], 0, 0);
                }
                canvas.drawBitmap(this.imgCaches[b], (float) this.rect.left, (float) this.rect.top, paint);
                return;
            }
            this.player.drawFrame(canvas, this.frameIdList[b], this.rect.left, this.rect.top);
        }
    }

    public AnimationPlayerV1 getAnimationPlayer() {
        return this.player;
    }

    public int getHeight() {
        if (this.rect.isEmpty() && this.frameIdList != null && this.frameIdList.length > 0 && this.frameIdList[0] >= 0 && this.player != null) {
            return this.player.getFrameHeight(this.frameIdList[0]);
        }
        if (this.rect != null) {
            return this.rect.bottom - this.rect.top;
        }
        return 0;
    }

    public String getId() {
        return this.id;
    }

    public Rect getLocation() {
        return this.rect;
    }

    public int getSetInt(int i, int i2, boolean z) {
        int i3 = i + 0;
        if (z) {
            if (i3 >= this.propData.length) {
                int[] iArr = new int[(i3 + 5)];
                System.arraycopy(this.propData, 0, iArr, 0, this.propData.length);
                this.propData = iArr;
            }
            this.propData[i3] = i2;
        }
        if (i3 < this.propData.length) {
            return this.propData[i3];
        }
        return 0;
    }

    public long getSetLong(int i, long j, boolean z) {
        if (z) {
            getSetInt(i, (int) (-1 & j), z);
            getSetInt(i + 1, (int) (j >>> 32), z);
            return j;
        }
        int i2 = i + 0;
        if (i2 + 1 < this.propData.length) {
            return (((long) this.propData[i2]) & 4294967295L) | ((((long) this.propData[i2 + 1]) & 4294967295L) << 32);
        }
        return 0;
    }

    public Object getSetObj(int i, Object obj, boolean z) {
        int i2 = i + 0;
        if (z) {
            if (i2 >= this.objData.length) {
                Object[] objArr = new Object[(i2 + 4)];
                System.arraycopy(this.objData, 0, objArr, 0, this.objData.length);
                this.objData = objArr;
            }
            this.objData[i2] = obj;
        }
        if (i2 < this.objData.length) {
            return this.objData[i2];
        }
        return null;
    }

    public int getWidth() {
        if (this.rect.isEmpty() && this.frameIdList != null && this.frameIdList.length > 0 && this.frameIdList[0] >= 0 && this.player != null) {
            return this.player.getFrameWidth(this.frameIdList[0]);
        }
        if (this.rect != null) {
            return this.rect.right - this.rect.left;
        }
        return 0;
    }

    public boolean isTouched(int i, int i2) {
        if (this.curStatus == 1 || !this.visible) {
            return false;
        }
        return contains(i, i2);
    }

    public boolean isVisible() {
        return this.visible;
    }

    public FrameControlV1 setAnimationPlayer(AnimationPlayerV1 animationPlayerV1) {
        this.player = animationPlayerV1;
        return this;
    }

    public FrameControlV1 setFrame(byte b, byte b2, byte b3) {
        if (this.frameIdList == null) {
            this.frameIdList = new byte[3];
        }
        this.frameIdList[0] = b;
        this.frameIdList[1] = b2;
        this.frameIdList[2] = b3;
        return this;
    }

    public FrameControlV1 setFrame(byte[] bArr) {
        this.frameIdList = bArr;
        return this;
    }

    public FrameControlV1 setLocation(int i, int i2) {
        if (this.frameIdList != null && this.frameIdList.length > 0 && this.frameIdList[0] >= 0 && this.player != null) {
            if (this.rect.isEmpty()) {
                this.rect.set(0, 0, this.player.getFrameWidth(this.frameIdList[0]), this.player.getFrameHeight(this.frameIdList[0]));
            }
            this.rect.offsetTo(i, i2);
        }
        return this;
    }

    public FrameControlV1 setLocation(int i, int i2, int i3, int i4) {
        this.rect.set(i, i2, i3, i4);
        return this;
    }

    public void setPadding(int i, int i2, int i3, int i4) {
        this.paddingLeft = i;
        this.paddingRight = i2;
        this.paddingTop = i3;
        this.paddingBottom = i4;
    }

    public void setStatus(byte b) {
        this.curStatus = b;
    }

    public FrameControlV1 setVisible(boolean z) {
        this.visible = z;
        return this;
    }
}
