package com.tencent.qqgame.lord.protocol;

import com.tencent.qqgame.common.Util;
import com.tencent.qqgame.hall.common.SyncData;
import com.tencent.qqgame.hall.common.Tools;
import com.tencent.qqgame.hall.protocol.ISendGameMessageHandle;
import com.tencent.qqgame.lord.common.AGameLogic;

public class LordSendMessageHandle {
    private static final byte CS_CONFIG_CARDS = 96;
    private static final byte CS_GIVE_CARDS = 3;
    private static final byte CS_ORDER = 2;
    private static final byte CS_REQUEST_BASE_INFO = 1;
    private ISendGameMessageHandle handle;

    public LordSendMessageHandle(ISendGameMessageHandle iSendGameMessageHandle) {
        this.handle = iSendGameMessageHandle;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
     arg types: [byte[], int, int]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
     arg types: [byte[], byte, int]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
     arg types: [byte[], byte, int]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void */
    public void callScore(byte b) {
        byte[] bArr = new byte[16];
        Util.word2Byte(bArr, 0, (short) 16);
        byte b2 = (byte) (0 + 2);
        Util.word2Byte(bArr, b2, AGameLogic.uin);
        byte b3 = (byte) (b2 + 4);
        Util.word2Byte(bArr, (int) b3, 500);
        byte b4 = (byte) (b3 + 4);
        Util.word2Byte(bArr, (int) b4, (short) 4);
        byte b5 = (byte) (b4 + 2);
        Util.word2Byte(bArr, (int) b5, (short) 4);
        byte b6 = (byte) (b5 + 2);
        bArr[14] = 2;
        bArr[15] = b;
        Tools.debug("<<callScore...");
        this.handle.sendGameMessage(bArr);
    }

    public void chat(String str) {
        this.handle.chat(str);
    }

    public void configPoker(byte[] bArr) {
        int length = bArr != null ? bArr.length : 0;
        byte[] bArr2 = new byte[(length + 15)];
        Util.word2Byte(bArr2, 0, (short) (length + 15));
        Util.word2Byte(bArr2, 2, SyncData.curUin);
        Util.word2Byte(bArr2, 6, Util.getCurrentTimeStamp());
        Util.word2Byte(bArr2, 10, (short) (length + 3));
        Util.word2Byte(bArr2, 12, (short) (length + 3));
        bArr2[14] = CS_CONFIG_CARDS;
        if (bArr != null) {
            System.arraycopy(bArr, 0, bArr2, 15, length);
        }
        this.handle.sendGameMessage(bArr2);
        Tools.debug("<<configPoker...");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
     arg types: [byte[], int, int]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void */
    public void getGameInfo() {
        byte[] bArr = new byte[15];
        Util.word2Byte(bArr, 0, (short) 15);
        Util.word2Byte(bArr, 2, AGameLogic.uin);
        Util.word2Byte(bArr, 6, 500);
        Util.word2Byte(bArr, 10, (short) 3);
        Util.word2Byte(bArr, 12, (short) 3);
        bArr[14] = 1;
        Tools.debug("<<getGameInfo...");
        this.handle.sendGameMessage(bArr);
    }

    public void handUp() {
        this.handle.handUp();
    }

    public void logout() {
        this.handle.logout();
    }

    /*  JADX ERROR: JadxRuntimeException in pass: InitCodeVariables
        jadx.core.utils.exceptions.JadxRuntimeException: Several immutable types in one variable: [byte, int], vars: [r1v0 ?, r1v1 ?, r1v3 ?]
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVarType(InitCodeVariables.java:102)
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVar(InitCodeVariables.java:78)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVar(InitCodeVariables.java:69)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVars(InitCodeVariables.java:51)
        	at jadx.core.dex.visitors.InitCodeVariables.visit(InitCodeVariables.java:32)
        */
    public void outCard(java.util.Vector<com.tencent.qqgame.common.Card> r8) {
        /*
            r7 = this;
            r6 = 0
            r0 = 0
            if (r8 == 0) goto L_0x006b
            int r1 = r8.size()
            if (r1 <= 0) goto L_0x006b
            int r0 = r8.size()
            byte r1 = (byte) r0
            byte[] r2 = new byte[r1]
            r3 = r6
        L_0x0012:
            if (r3 >= r1) goto L_0x0023
            java.lang.Object r0 = r8.elementAt(r3)
            com.tencent.qqgame.common.Card r0 = (com.tencent.qqgame.common.Card) r0
            byte r0 = r0.number
            r2[r3] = r0
            int r0 = r3 + 1
            byte r0 = (byte) r0
            r3 = r0
            goto L_0x0012
        L_0x0023:
            r0 = r2
        L_0x0024:
            int r2 = r1 + 16
            short r2 = (short) r2
            byte[] r3 = new byte[r2]
            com.tencent.qqgame.common.Util.word2Byte(r3, r6, r2)
            int r2 = r6 + 2
            byte r2 = (byte) r2
            long r4 = com.tencent.qqgame.lord.common.AGameLogic.uin
            com.tencent.qqgame.common.Util.word2Byte(r3, r2, r4)
            int r2 = r2 + 4
            byte r2 = (byte) r2
            long r4 = com.tencent.qqgame.common.Util.getCurrentTimeStamp()
            com.tencent.qqgame.common.Util.word2Byte(r3, r2, r4)
            int r2 = r2 + 4
            byte r2 = (byte) r2
            int r4 = r1 + 4
            short r4 = (short) r4
            com.tencent.qqgame.common.Util.word2Byte(r3, r2, r4)
            int r2 = r2 + 2
            byte r2 = (byte) r2
            int r4 = r1 + 4
            short r4 = (short) r4
            com.tencent.qqgame.common.Util.word2Byte(r3, r2, r4)
            r2 = 14
            r4 = 3
            r3[r2] = r4
            r2 = 15
            r3[r2] = r1
            if (r0 == 0) goto L_0x0060
            r2 = 16
            java.lang.System.arraycopy(r0, r6, r3, r2, r1)
        L_0x0060:
            java.lang.String r0 = "<<outCard..."
            com.tencent.qqgame.hall.common.Tools.debug(r0)
            com.tencent.qqgame.hall.protocol.ISendGameMessageHandle r0 = r7.handle
            r0.sendGameMessage(r3)
            return
        L_0x006b:
            r1 = r6
            goto L_0x0024
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.lord.protocol.LordSendMessageHandle.outCard(java.util.Vector):void");
    }

    public void quickPlay(short s) {
        this.handle.quickPlay(s);
    }

    public void tickPlayer(long j) {
        this.handle.tickPlayer(j);
    }
}
