package com.tencent.qqgame.lord.common;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class StringItem {
    private boolean autoScroll = false;
    private int length = 0;
    private int offsetA = 0;
    private int offsetB = -10000;
    private int pivot = 0;
    private String text;
    private int x = 0;
    private int y = 0;

    public StringItem(String str) {
        this.text = str;
    }

    public void draw(Canvas canvas, Paint paint, Rect rect) {
        if (rect.contains(this.x, this.y)) {
            if (this.length <= 0) {
                this.length = (int) paint.measureText(this.text);
            }
            if (this.autoScroll) {
                this.offsetA -= 2;
                this.offsetB -= 2;
                int i = this.x + this.offsetA;
                int i2 = this.x + this.offsetB;
                if (i >= this.pivot - this.length || this.offsetA <= (-this.length)) {
                    if (i2 < this.pivot - this.length && this.offsetB > (-this.length) && this.offsetA <= 0) {
                        this.offsetA = rect.width();
                    }
                } else if (this.offsetB <= 0) {
                    this.offsetB = rect.width();
                }
                canvas.drawText(this.text, (float) (this.x + this.offsetA), (float) this.y, paint);
                canvas.drawText(this.text, (float) (this.x + this.offsetB), (float) this.y, paint);
                return;
            }
            canvas.drawText(this.text, (float) this.x, (float) this.y, paint);
        }
    }

    public boolean isTouched(int i, int i2) {
        return false;
    }

    public void setAutoScroll(boolean z) {
        this.autoScroll = z;
    }

    public void setLocation(int i, int i2) {
        this.x = i;
        this.y = i2;
    }

    public void setPivot(int i) {
        this.pivot = i;
    }
}
