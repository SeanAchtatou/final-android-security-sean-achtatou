package com.tencent.qqgame.lord.ui;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import com.tencent.qqgame.common.Player;
import com.tencent.qqgame.common.Table;
import com.tencent.qqgame.hall.common.Tools;
import com.tencent.qqgame.hall.protocol.IReceiveGameMessageHandle;
import com.tencent.qqgame.hall.protocol.ISendGameMessageHandle;
import com.tencent.qqgame.lord.common.AGameLogic;
import com.tencent.qqgame.lord.common.AGameView;
import java.util.Vector;

public final class LordSurface extends SurfaceView implements Runnable, SurfaceHolder.Callback {
    private static final short SPF = 50;
    private final int MAX_STEP = 10000;
    private Activity gameActivity;
    private Thread gameThread;
    private Handler handle;
    public boolean isForeground = true;
    protected boolean isNeedRepain = false;
    private boolean isRunning = false;
    private final Vector<KeyEvent> keyDownList = new Vector<>();
    private MotionEvent lastMotion = null;
    private AGameLogic logic = null;
    private ISendGameMessageHandle sendGameMsgHandle;
    private int step = 0;
    private SurfaceHolder surfaceHolder = getHolder();
    private final Vector<MotionEvent> touchMotionList = new Vector<>();
    private AGameView view = null;

    public LordSurface(Activity activity, Handler handler, ISendGameMessageHandle iSendGameMessageHandle, Table table, Player player, long j, boolean z) {
        super(activity);
        this.surfaceHolder.addCallback(this);
        this.handle = handler;
        this.gameActivity = activity;
        this.sendGameMsgHandle = iSendGameMessageHandle;
        setFocusable(true);
        this.view = new LordView(activity, this, handler, iSendGameMessageHandle, SPF, table, player, j, z);
        this.logic = this.view.logic;
    }

    public IReceiveGameMessageHandle getGameReceiveMessageHandle() {
        return this.logic.receiveHandle;
    }

    public void joinThread() {
        boolean z = true;
        while (z) {
            try {
                if (this.gameThread != null) {
                    this.gameThread.join();
                }
                z = false;
            } catch (InterruptedException e) {
            }
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        synchronized (this.keyDownList) {
            this.keyDownList.add(keyEvent);
        }
        return false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.lastMotion != null && motionEvent.getAction() == this.lastMotion.getAction() && Math.abs(motionEvent.getX() - this.lastMotion.getX()) <= 2.0f && Math.abs(motionEvent.getY() - this.lastMotion.getY()) <= 2.0f) {
            return true;
        }
        this.lastMotion = MotionEvent.obtain(motionEvent);
        synchronized (this.touchMotionList) {
            this.touchMotionList.add(this.lastMotion);
        }
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:53:0x00e7  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0122  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0004 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r12 = this;
            r10 = 0
            r9 = 50
            r8 = 0
        L_0x0004:
            boolean r0 = r12.isRunning
            if (r0 == 0) goto L_0x0128
            long r1 = java.lang.System.currentTimeMillis()
            int r0 = r12.step
            r3 = 10000(0x2710, float:1.4013E-41)
            if (r0 <= r3) goto L_0x007b
            r0 = r8
        L_0x0013:
            r12.step = r0
            com.tencent.qqgame.lord.common.AGameLogic r0 = r12.logic
            r0.doHandleMessage()
            boolean r0 = r12.isForeground
            if (r0 == 0) goto L_0x00ab
            java.util.Vector<android.view.MotionEvent> r3 = r12.touchMotionList
            monitor-enter(r3)
        L_0x0021:
            java.util.Vector<android.view.MotionEvent> r0 = r12.touchMotionList     // Catch:{ all -> 0x0078 }
            int r0 = r0.size()     // Catch:{ all -> 0x0078 }
            if (r0 <= 0) goto L_0x0080
            java.util.Vector<android.view.MotionEvent> r0 = r12.touchMotionList     // Catch:{ all -> 0x0078 }
            r4 = 0
            java.lang.Object r0 = r0.elementAt(r4)     // Catch:{ all -> 0x0078 }
            android.view.MotionEvent r0 = (android.view.MotionEvent) r0     // Catch:{ all -> 0x0078 }
            if (r0 == 0) goto L_0x0071
            float r4 = r0.getX()     // Catch:{ all -> 0x0078 }
            int r4 = (int) r4     // Catch:{ all -> 0x0078 }
            float r5 = r0.getY()     // Catch:{ all -> 0x0078 }
            int r5 = (int) r5     // Catch:{ all -> 0x0078 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0078 }
            r6.<init>()     // Catch:{ all -> 0x0078 }
            java.lang.String r7 = "action:"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0078 }
            int r7 = r0.getAction()     // Catch:{ all -> 0x0078 }
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0078 }
            java.lang.String r7 = ",x:"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0078 }
            java.lang.StringBuilder r6 = r6.append(r4)     // Catch:{ all -> 0x0078 }
            java.lang.String r7 = ",y:"
            java.lang.StringBuilder r6 = r6.append(r7)     // Catch:{ all -> 0x0078 }
            java.lang.StringBuilder r6 = r6.append(r5)     // Catch:{ all -> 0x0078 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x0078 }
            com.tencent.qqgame.hall.common.Tools.debug(r6)     // Catch:{ all -> 0x0078 }
            com.tencent.qqgame.lord.common.AGameLogic r6 = r12.logic     // Catch:{ all -> 0x0078 }
            r6.doTouchEvent(r0, r4, r5)     // Catch:{ all -> 0x0078 }
        L_0x0071:
            java.util.Vector<android.view.MotionEvent> r0 = r12.touchMotionList     // Catch:{ all -> 0x0078 }
            r4 = 0
            r0.remove(r4)     // Catch:{ all -> 0x0078 }
            goto L_0x0021
        L_0x0078:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0078 }
            throw r0
        L_0x007b:
            int r0 = r12.step
            int r0 = r0 + 1
            goto L_0x0013
        L_0x0080:
            monitor-exit(r3)     // Catch:{ all -> 0x0078 }
            java.util.Vector<android.view.KeyEvent> r3 = r12.keyDownList
            monitor-enter(r3)
        L_0x0084:
            java.util.Vector<android.view.KeyEvent> r0 = r12.keyDownList     // Catch:{ all -> 0x00a7 }
            int r0 = r0.size()     // Catch:{ all -> 0x00a7 }
            if (r0 <= 0) goto L_0x00aa
            java.util.Vector<android.view.KeyEvent> r0 = r12.keyDownList     // Catch:{ all -> 0x00a7 }
            r4 = 0
            java.lang.Object r0 = r0.elementAt(r4)     // Catch:{ all -> 0x00a7 }
            android.view.KeyEvent r0 = (android.view.KeyEvent) r0     // Catch:{ all -> 0x00a7 }
            if (r0 == 0) goto L_0x00a0
            com.tencent.qqgame.lord.common.AGameLogic r4 = r12.logic     // Catch:{ all -> 0x00a7 }
            int r5 = r0.getKeyCode()     // Catch:{ all -> 0x00a7 }
            r4.doKeyDown(r5, r0)     // Catch:{ all -> 0x00a7 }
        L_0x00a0:
            java.util.Vector<android.view.KeyEvent> r0 = r12.keyDownList     // Catch:{ all -> 0x00a7 }
            r4 = 0
            r0.remove(r4)     // Catch:{ all -> 0x00a7 }
            goto L_0x0084
        L_0x00a7:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x00a7 }
            throw r0
        L_0x00aa:
            monitor-exit(r3)     // Catch:{ all -> 0x00a7 }
        L_0x00ab:
            com.tencent.qqgame.lord.common.AGameLogic r0 = r12.logic
            int r3 = r12.step
            r0.run(r9, r3)
            r0 = 1
            r12.isNeedRepain = r0
            boolean r0 = r12.isNeedRepain
            if (r0 == 0) goto L_0x00d7
            boolean r0 = r12.isForeground
            if (r0 == 0) goto L_0x00d7
            android.view.SurfaceHolder r0 = r12.surfaceHolder     // Catch:{ Exception -> 0x0131, all -> 0x011e }
            r3 = 0
            android.graphics.Canvas r0 = r0.lockCanvas(r3)     // Catch:{ Exception -> 0x0131, all -> 0x011e }
            android.view.SurfaceHolder r3 = r12.surfaceHolder     // Catch:{ Exception -> 0x00f5, all -> 0x0129 }
            monitor-enter(r3)     // Catch:{ Exception -> 0x00f5, all -> 0x0129 }
            com.tencent.qqgame.lord.common.AGameView r4 = r12.view     // Catch:{ all -> 0x00f2 }
            r4.doDraw(r0)     // Catch:{ all -> 0x00f2 }
            r4 = 0
            r12.isNeedRepain = r4     // Catch:{ all -> 0x00f2 }
            monitor-exit(r3)     // Catch:{ all -> 0x00f2 }
            if (r0 == 0) goto L_0x00d7
            android.view.SurfaceHolder r3 = r12.surfaceHolder
            r3.unlockCanvasAndPost(r0)
        L_0x00d7:
            com.tencent.qqgame.lord.common.AGameLogic r0 = r12.logic
            int r3 = r12.step
            r0.runLast(r9, r3)
            long r3 = java.lang.System.currentTimeMillis()
            long r0 = r3 - r1
            int r0 = (int) r0
            if (r0 >= r9) goto L_0x0004
            int r0 = r9 - r0
            long r0 = (long) r0
            java.lang.Thread.sleep(r0)     // Catch:{ Exception -> 0x00ef }
            goto L_0x0004
        L_0x00ef:
            r0 = move-exception
            goto L_0x0004
        L_0x00f2:
            r4 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x00f2 }
            throw r4     // Catch:{ Exception -> 0x00f5, all -> 0x0129 }
        L_0x00f5:
            r3 = move-exception
            r11 = r3
            r3 = r0
            r0 = r11
        L_0x00f9:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x012e }
            r4.<init>()     // Catch:{ all -> 0x012e }
            java.lang.String r5 = "draw exception! detail info:"
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x012e }
            java.lang.String r5 = r0.toString()     // Catch:{ all -> 0x012e }
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ all -> 0x012e }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x012e }
            com.tencent.qqgame.hall.common.Tools.debug(r4)     // Catch:{ all -> 0x012e }
            r0.printStackTrace()     // Catch:{ all -> 0x012e }
            if (r3 == 0) goto L_0x00d7
            android.view.SurfaceHolder r0 = r12.surfaceHolder
            r0.unlockCanvasAndPost(r3)
            goto L_0x00d7
        L_0x011e:
            r0 = move-exception
            r1 = r10
        L_0x0120:
            if (r1 == 0) goto L_0x0127
            android.view.SurfaceHolder r2 = r12.surfaceHolder
            r2.unlockCanvasAndPost(r1)
        L_0x0127:
            throw r0
        L_0x0128:
            return
        L_0x0129:
            r1 = move-exception
            r11 = r1
            r1 = r0
            r0 = r11
            goto L_0x0120
        L_0x012e:
            r0 = move-exception
            r1 = r3
            goto L_0x0120
        L_0x0131:
            r0 = move-exception
            r3 = r10
            goto L_0x00f9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.lord.ui.LordSurface.run():void");
    }

    public final void sendMessage(Message message, boolean z) {
        if (message != null) {
            synchronized (this.logic.messageList) {
                this.logic.messageList.add(message);
            }
            if (z) {
                this.logic.doHandleMessage();
            }
        }
    }

    public void setForeground(boolean z) {
        this.isForeground = z;
    }

    public void setRunning(boolean z) {
        this.isRunning = z;
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder2, int i, int i2, int i3) {
        Tools.debug("surfaceChanged.width:" + i2 + ",height:" + i3 + ",format:" + i);
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder2) {
        Tools.debug("start game thread...");
        this.isForeground = true;
        setRunning(true);
        if (this.gameThread == null) {
            this.gameThread = new Thread(this);
            this.gameThread.setName("game");
            this.gameThread.start();
        }
        Tools.debug("surfaceCreated");
    }

    public void surfaceDestroyed(SurfaceHolder surfaceHolder2) {
        this.isForeground = false;
        Tools.debug("surfaceDestroyed");
    }
}
