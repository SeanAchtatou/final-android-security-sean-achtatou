package com.tencent.qqgame.lord.common;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import com.tencent.qqgame.hall.protocol.ISendGameMessageHandle;
import com.tencent.qqgame.lord.ui.LordSurface;

public abstract class AGameView {
    public static final RectF fRect = new RectF();
    public static final Paint paint = new Paint();
    public static final Rect rect1 = new Rect();
    public static final Rect rect2 = new Rect();
    protected int Notify_Info_offsetX = 0;
    protected final short SPF;
    public final AnimationManager aniManager;
    public final LordSurface gameSurface;
    protected Bitmap imgNotifyInfo;
    protected boolean isNeedRepain = false;
    public AGameLogic logic = null;
    protected int startX = 0;
    protected int startY = 0;

    public AGameView(LordSurface lordSurface, ISendGameMessageHandle iSendGameMessageHandle, short s, boolean z) {
        this.gameSurface = lordSurface;
        this.SPF = s;
        paint.setAntiAlias(true);
        this.aniManager = new AnimationManager(lordSurface.getContext());
        this.aniManager.reset();
    }

    public abstract void doDraw(Canvas canvas);

    public final void initLocationInfoImg() {
        this.imgNotifyInfo = null;
        paint.setTextSize(14.0f);
        this.imgNotifyInfo = Bitmap.createBitmap((int) paint.measureText(this.logic.locationInfo), 15, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas();
        paint.setColor(-8797697);
        canvas.setBitmap(this.imgNotifyInfo);
        canvas.drawText(this.logic.locationInfo, 0.0f, 14.0f, paint);
        this.Notify_Info_offsetX = 0;
    }

    public abstract void recycle();
}
