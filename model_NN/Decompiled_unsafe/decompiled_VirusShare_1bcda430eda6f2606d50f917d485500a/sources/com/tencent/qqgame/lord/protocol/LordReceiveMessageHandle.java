package com.tencent.qqgame.lord.protocol;

import android.os.Message;
import com.tencent.qqgame.common.Table;
import com.tencent.qqgame.common.Util;
import com.tencent.qqgame.hall.common.Tools;
import com.tencent.qqgame.hall.common.data.MProxyInfoV2;
import com.tencent.qqgame.hall.protocol.IReceiveGameMessageHandle;
import com.tencent.qqgame.lord.common.AGameLogic;
import com.tencent.qqgame.lord.logic.LordLogic;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Vector;

public class LordReceiveMessageHandle implements IReceiveGameMessageHandle {
    private static final byte SC_BASE_INFO = 49;
    private static final byte SC_DEAL_CARD = 51;
    private static final byte SC_EXIT_SERVER_SET = 60;
    private static final byte SC_FINISH_ROUND = 62;
    private static final byte SC_FORCE_QUIT = 59;
    private static final byte SC_GIVE_CARD = 54;
    private static final byte SC_MSG_NEWLORD = 64;
    private static final byte SC_ORDER = 52;
    private static final byte SC_PLAYER_START = 50;
    private static final byte SC_REPLAY_MSG = 57;
    private static final byte SC_SHOW_BENEATH = 53;
    private static final byte SC_WATCHER_MSG = 58;
    private final LordLogic logic;

    public LordReceiveMessageHandle(LordLogic lordLogic) {
        this.logic = lordLogic;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v9, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v10, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v14, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void replay(java.io.DataInputStream r19) {
        /*
            r18 = this;
            r3 = 3
            byte[] r9 = new byte[r3]
            r3 = 3
            byte[] r10 = new byte[r3]
            r3 = 3
            java.util.Vector[] r12 = new java.util.Vector[r3]
            byte r4 = r19.readByte()     // Catch:{ IOException -> 0x0088 }
            r3 = 6
            r0 = r19
            r1 = r3
            r0.skipBytes(r1)     // Catch:{ IOException -> 0x0088 }
            byte r5 = r19.readByte()     // Catch:{ IOException -> 0x0088 }
            byte r6 = r19.readByte()     // Catch:{ IOException -> 0x0088 }
            byte r7 = r19.readByte()     // Catch:{ IOException -> 0x0088 }
            int r8 = r19.readInt()     // Catch:{ IOException -> 0x0088 }
            r13 = 3
            r0 = r19
            r1 = r13
            r0.skip(r1)     // Catch:{ IOException -> 0x0088 }
            r3 = 0
        L_0x002d:
            r11 = 3
            if (r3 >= r11) goto L_0x0039
            byte r11 = r19.readByte()     // Catch:{ IOException -> 0x0088 }
            r9[r3] = r11     // Catch:{ IOException -> 0x0088 }
            int r3 = r3 + 1
            goto L_0x002d
        L_0x0039:
            r13 = 5
            r0 = r19
            r1 = r13
            r0.skip(r1)     // Catch:{ IOException -> 0x0088 }
            r3 = 4
            byte[] r3 = new byte[r3]     // Catch:{ IOException -> 0x0088 }
            r11 = 0
        L_0x0045:
            r13 = 4
            if (r11 >= r13) goto L_0x0051
            byte r13 = r19.readByte()     // Catch:{ IOException -> 0x0088 }
            r3[r11] = r13     // Catch:{ IOException -> 0x0088 }
            int r11 = r11 + 1
            goto L_0x0045
        L_0x0051:
            r3 = 0
        L_0x0052:
            r11 = 3
            if (r3 >= r11) goto L_0x005e
            byte r11 = r19.readByte()     // Catch:{ IOException -> 0x0088 }
            r10[r3] = r11     // Catch:{ IOException -> 0x0088 }
            int r3 = r3 + 1
            goto L_0x0052
        L_0x005e:
            r13 = 1
            r0 = r19
            r1 = r13
            r0.skip(r1)     // Catch:{ IOException -> 0x0088 }
            r0 = r18
            com.tencent.qqgame.lord.logic.LordLogic r0 = r0.logic     // Catch:{ IOException -> 0x0088 }
            r3 = r0
            com.tencent.qqgame.common.Player r3 = r3.me     // Catch:{ IOException -> 0x0088 }
            short r3 = r3.seatId     // Catch:{ IOException -> 0x0088 }
            byte r3 = r10[r3]     // Catch:{ IOException -> 0x0088 }
            byte[] r11 = new byte[r3]     // Catch:{ IOException -> 0x0088 }
            r3 = 0
        L_0x0074:
            r13 = 33
            if (r3 >= r13) goto L_0x00a7
            int r13 = r11.length     // Catch:{ IOException -> 0x0088 }
            if (r3 >= r13) goto L_0x0084
            byte r13 = r19.readByte()     // Catch:{ IOException -> 0x0088 }
            r11[r3] = r13     // Catch:{ IOException -> 0x0088 }
        L_0x0081:
            int r3 = r3 + 1
            goto L_0x0074
        L_0x0084:
            r19.readByte()     // Catch:{ IOException -> 0x0088 }
            goto L_0x0081
        L_0x0088:
            r3 = move-exception
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "ERR in MSG:rePlay() "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = r3.toString()
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            com.tencent.qqgame.hall.common.Tools.debug(r4)
            r3.printStackTrace()
        L_0x00a6:
            return
        L_0x00a7:
            r3 = 3
            byte[] r3 = new byte[r3]     // Catch:{ IOException -> 0x0088 }
            r13 = 0
        L_0x00ab:
            r14 = 3
            if (r13 >= r14) goto L_0x00b7
            byte r14 = r19.readByte()     // Catch:{ IOException -> 0x0088 }
            r3[r13] = r14     // Catch:{ IOException -> 0x0088 }
            int r13 = r13 + 1
            goto L_0x00ab
        L_0x00b7:
            r13 = 1
            r0 = r19
            r1 = r13
            r0.skip(r1)     // Catch:{ IOException -> 0x0088 }
            r3 = 3
            byte[] r3 = new byte[r3]     // Catch:{ IOException -> 0x0088 }
            r13 = 0
        L_0x00c3:
            r14 = 3
            if (r13 >= r14) goto L_0x00cf
            byte r14 = r19.readByte()     // Catch:{ IOException -> 0x0088 }
            r3[r13] = r14     // Catch:{ IOException -> 0x0088 }
            int r13 = r13 + 1
            goto L_0x00c3
        L_0x00cf:
            r13 = 1
            r0 = r19
            r1 = r13
            r0.skip(r1)     // Catch:{ IOException -> 0x0088 }
            r13 = 0
        L_0x00d8:
            r14 = 3
            if (r13 >= r14) goto L_0x0105
            java.util.Vector r14 = new java.util.Vector     // Catch:{ IOException -> 0x0088 }
            r14.<init>()     // Catch:{ IOException -> 0x0088 }
            r12[r13] = r14     // Catch:{ IOException -> 0x0088 }
            r14 = 0
        L_0x00e3:
            r15 = 33
            if (r14 >= r15) goto L_0x0102
            byte r15 = r19.readByte()     // Catch:{ IOException -> 0x0088 }
            byte r16 = r3[r13]     // Catch:{ IOException -> 0x0088 }
            r0 = r14
            r1 = r16
            if (r0 >= r1) goto L_0x00ff
            r16 = r12[r13]     // Catch:{ IOException -> 0x0088 }
            java.lang.Integer r17 = new java.lang.Integer     // Catch:{ IOException -> 0x0088 }
            r0 = r17
            r1 = r15
            r0.<init>(r1)     // Catch:{ IOException -> 0x0088 }
            r16.addElement(r17)     // Catch:{ IOException -> 0x0088 }
        L_0x00ff:
            int r14 = r14 + 1
            goto L_0x00e3
        L_0x0102:
            int r13 = r13 + 1
            goto L_0x00d8
        L_0x0105:
            r13 = 33
            r0 = r19
            r1 = r13
            r0.skip(r1)     // Catch:{ IOException -> 0x0088 }
            byte r13 = r19.readByte()     // Catch:{ IOException -> 0x0088 }
            r14 = 5
            r0 = r19
            r1 = r14
            r0.skip(r1)     // Catch:{ IOException -> 0x0088 }
            r3 = 3
            byte[] r3 = new byte[r3]     // Catch:{ IOException -> 0x0088 }
            r14 = 0
        L_0x011d:
            r15 = 3
            if (r14 >= r15) goto L_0x0129
            byte r15 = r19.readByte()     // Catch:{ IOException -> 0x0088 }
            r3[r14] = r15     // Catch:{ IOException -> 0x0088 }
            int r14 = r14 + 1
            goto L_0x011d
        L_0x0129:
            byte r14 = r19.readByte()     // Catch:{ IOException -> 0x0088 }
            r0 = r18
            com.tencent.qqgame.lord.logic.LordLogic r0 = r0.logic
            r3 = r0
            r3.replay(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14)
            goto L_0x00a6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.lord.protocol.LordReceiveMessageHandle.replay(java.io.DataInputStream):void");
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:1:0x000d */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:5:0x0017 */
    /* JADX WARN: Type inference failed for: r0v1, types: [java.io.ByteArrayInputStream, java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r0v2 */
    /* JADX WARN: Type inference failed for: r0v3 */
    /* JADX WARN: Type inference failed for: r0v6 */
    /* JADX WARN: Type inference failed for: r0v9 */
    /* JADX WARN: Type inference failed for: r0v10 */
    /* JADX WARN: Type inference failed for: r0v11 */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x0017 A[SYNTHETIC, Splitter:B:5:0x0017] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void roomEvent(byte[] r14) {
        /*
            r13 = this;
            r12 = 0
            java.lang.String r0 = ">>player game start..."
            java.io.ByteArrayInputStream r0 = new java.io.ByteArrayInputStream
            r0.<init>(r14)
            java.io.DataInputStream r1 = new java.io.DataInputStream
            r1.<init>(r0)
            r1.readShort()     // Catch:{ IOException -> 0x004b }
            short r0 = r1.readShort()     // Catch:{ IOException -> 0x004b }
        L_0x0014:
            r2 = r12
        L_0x0015:
            if (r2 >= r0) goto L_0x0102
            byte r3 = r1.readByte()     // Catch:{ Exception -> 0x0075 }
            r1.readInt()     // Catch:{ Exception -> 0x0075 }
            int r4 = r1.readInt()     // Catch:{ Exception -> 0x0075 }
            long r4 = com.tencent.qqgame.common.Util.convertUnsigned2Long(r4)     // Catch:{ Exception -> 0x0075 }
            r1.readShort()     // Catch:{ Exception -> 0x0075 }
            byte r6 = r1.readByte()     // Catch:{ Exception -> 0x0075 }
            byte r7 = r1.readByte()     // Catch:{ Exception -> 0x0075 }
            r8 = 0
            r9 = 1
            if (r7 != r9) goto L_0x0103
            com.tencent.qqgame.common.Player r7 = com.tencent.qqgame.common.Msg.getPlayerInfo(r1)     // Catch:{ Exception -> 0x0075 }
        L_0x0039:
            short r8 = r1.readShort()     // Catch:{ Exception -> 0x0075 }
            byte[] r9 = new byte[r8]     // Catch:{ Exception -> 0x0075 }
            r10 = r12
        L_0x0040:
            if (r10 >= r8) goto L_0x004e
            byte r11 = r1.readByte()     // Catch:{ Exception -> 0x0075 }
            r9[r10] = r11     // Catch:{ Exception -> 0x0075 }
            int r10 = r10 + 1
            goto L_0x0040
        L_0x004b:
            r0 = move-exception
            r0 = r12
            goto L_0x0014
        L_0x004e:
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0075 }
            r10.<init>()     // Catch:{ Exception -> 0x0075 }
            java.lang.String r11 = "room event id: "
            java.lang.StringBuilder r10 = r10.append(r11)     // Catch:{ Exception -> 0x0075 }
            java.lang.StringBuilder r10 = r10.append(r3)     // Catch:{ Exception -> 0x0075 }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x0075 }
            com.tencent.qqgame.hall.common.Tools.debug(r10)     // Catch:{ Exception -> 0x0075 }
            switch(r3) {
                case 1: goto L_0x00ea;
                case 2: goto L_0x00bd;
                case 3: goto L_0x006a;
                case 4: goto L_0x0091;
                case 5: goto L_0x009c;
                case 6: goto L_0x0067;
                case 7: goto L_0x0067;
                case 8: goto L_0x0067;
                case 9: goto L_0x00a7;
                case 10: goto L_0x00b2;
                case 11: goto L_0x00c8;
                case 12: goto L_0x0067;
                case 13: goto L_0x00d3;
                case 14: goto L_0x0067;
                case 15: goto L_0x00f6;
                case 16: goto L_0x00de;
                case 17: goto L_0x00ea;
                default: goto L_0x0067;
            }     // Catch:{ Exception -> 0x0075 }
        L_0x0067:
            int r2 = r2 + 1
            goto L_0x0015
        L_0x006a:
            java.lang.String r3 = ">>player sitDown..."
            com.tencent.qqgame.hall.common.Tools.debug(r3)     // Catch:{ Exception -> 0x0075 }
            com.tencent.qqgame.lord.logic.LordLogic r3 = r13.logic     // Catch:{ Exception -> 0x0075 }
            r3.playerSitDown(r7)     // Catch:{ Exception -> 0x0075 }
            goto L_0x0067
        L_0x0075:
            r3 = move-exception
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "roomEvent exception, detail info:"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r3 = r3.toString()
            java.lang.StringBuilder r3 = r4.append(r3)
            java.lang.String r3 = r3.toString()
            com.tencent.qqgame.hall.common.Tools.debug(r3)
            goto L_0x0067
        L_0x0091:
            java.lang.String r3 = ">>player stand up..."
            com.tencent.qqgame.hall.common.Tools.debug(r3)     // Catch:{ Exception -> 0x0075 }
            com.tencent.qqgame.lord.logic.LordLogic r3 = r13.logic     // Catch:{ Exception -> 0x0075 }
            r3.playerStandUp(r6, r4)     // Catch:{ Exception -> 0x0075 }
            goto L_0x0067
        L_0x009c:
            java.lang.String r3 = ">>player hand up..."
            com.tencent.qqgame.hall.common.Tools.debug(r3)     // Catch:{ Exception -> 0x0075 }
            com.tencent.qqgame.lord.logic.LordLogic r3 = r13.logic     // Catch:{ Exception -> 0x0075 }
            r3.playerHandUp(r6, r4)     // Catch:{ Exception -> 0x0075 }
            goto L_0x0067
        L_0x00a7:
            java.lang.String r3 = ">>player game start..."
            com.tencent.qqgame.hall.common.Tools.debug(r3)     // Catch:{ Exception -> 0x0075 }
            com.tencent.qqgame.lord.logic.LordLogic r3 = r13.logic     // Catch:{ Exception -> 0x0075 }
            r3.gameStart()     // Catch:{ Exception -> 0x0075 }
            goto L_0x0067
        L_0x00b2:
            java.lang.String r3 = ">>player game start..."
            com.tencent.qqgame.hall.common.Tools.debug(r3)     // Catch:{ Exception -> 0x0075 }
            com.tencent.qqgame.lord.logic.LordLogic r3 = r13.logic     // Catch:{ Exception -> 0x0075 }
            r3.roomEvenResult()     // Catch:{ Exception -> 0x0075 }
            goto L_0x0067
        L_0x00bd:
            java.lang.String r3 = ">>player leave..."
            com.tencent.qqgame.hall.common.Tools.debug(r3)     // Catch:{ Exception -> 0x0075 }
            com.tencent.qqgame.lord.logic.LordLogic r3 = r13.logic     // Catch:{ Exception -> 0x0075 }
            r3.playerLeave(r6, r4)     // Catch:{ Exception -> 0x0075 }
            goto L_0x0067
        L_0x00c8:
            java.lang.String r3 = ">>score change..."
            com.tencent.qqgame.hall.common.Tools.debug(r3)     // Catch:{ Exception -> 0x0075 }
            com.tencent.qqgame.lord.logic.LordLogic r3 = r13.logic     // Catch:{ Exception -> 0x0075 }
            r3.scoreChange(r7)     // Catch:{ Exception -> 0x0075 }
            goto L_0x0067
        L_0x00d3:
            java.lang.String r3 = ">>player kicked..."
            com.tencent.qqgame.hall.common.Tools.debug(r3)     // Catch:{ Exception -> 0x0075 }
            com.tencent.qqgame.lord.logic.LordLogic r3 = r13.logic     // Catch:{ Exception -> 0x0075 }
            r3.roomEvenKicked(r4, r8, r9)     // Catch:{ Exception -> 0x0075 }
            goto L_0x0067
        L_0x00de:
            java.lang.String r3 = "player offline..."
            com.tencent.qqgame.hall.common.Tools.debug(r3)     // Catch:{ Exception -> 0x0075 }
            com.tencent.qqgame.lord.logic.LordLogic r3 = r13.logic     // Catch:{ Exception -> 0x0075 }
            r3.roomEvenOffLine(r6, r4)     // Catch:{ Exception -> 0x0075 }
            goto L_0x0067
        L_0x00ea:
            java.lang.String r3 = "player enter or relogin..."
            com.tencent.qqgame.hall.common.Tools.debug(r3)     // Catch:{ Exception -> 0x0075 }
            com.tencent.qqgame.lord.logic.LordLogic r3 = r13.logic     // Catch:{ Exception -> 0x0075 }
            r3.roomEvenReline(r4)     // Catch:{ Exception -> 0x0075 }
            goto L_0x0067
        L_0x00f6:
            java.lang.String r3 = "player canceled view game..."
            com.tencent.qqgame.hall.common.Tools.debug(r3)     // Catch:{ Exception -> 0x0075 }
            com.tencent.qqgame.lord.logic.LordLogic r3 = r13.logic     // Catch:{ Exception -> 0x0075 }
            r3.roomEventCancelViewGame(r4)     // Catch:{ Exception -> 0x0075 }
            goto L_0x0067
        L_0x0102:
            return
        L_0x0103:
            r7 = r8
            goto L_0x0039
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.lord.protocol.LordReceiveMessageHandle.roomEvent(byte[]):void");
    }

    private void viewGame(DataInputStream dataInputStream) {
        byte b;
        Vector[] vectorArr;
        byte b2;
        byte[] bArr;
        boolean z;
        byte b3;
        byte[] bArr2;
        byte b4;
        Vector[] vectorArr2;
        byte[] bArr3;
        byte[] bArr4;
        boolean z2;
        byte b5;
        byte b6;
        byte b7;
        byte b8;
        byte b9;
        byte readByte;
        byte readByte2;
        byte readByte3;
        byte b10 = 0;
        boolean z3 = false;
        byte[] bArr5 = null;
        byte[] bArr6 = new byte[3];
        try {
            dataInputStream.skipBytes(6);
            byte readByte4 = dataInputStream.readByte();
            try {
                readByte = dataInputStream.readByte();
                try {
                    readByte2 = dataInputStream.readByte();
                    try {
                        readByte3 = dataInputStream.readByte();
                    } catch (IOException e) {
                        b = readByte4;
                        vectorArr = null;
                        b2 = readByte2;
                        bArr = null;
                        z = false;
                        b3 = 0;
                        byte b11 = readByte;
                        bArr2 = null;
                        b4 = b11;
                    }
                } catch (IOException e2) {
                    b = readByte4;
                    vectorArr = null;
                    b2 = 0;
                    bArr = null;
                    z = false;
                    b3 = 0;
                    byte b12 = readByte;
                    bArr2 = null;
                    b4 = b12;
                }
            } catch (IOException e3) {
                b = readByte4;
                vectorArr = null;
                b2 = 0;
                bArr = null;
                z = false;
                b3 = 0;
                bArr2 = null;
                b4 = 0;
            }
            try {
                b10 = dataInputStream.readByte();
                dataInputStream.readByte();
                z3 = dataInputStream.readByte() == 1;
                if (z3) {
                    bArr5 = new byte[3];
                    int i = 0;
                    while (i < 3) {
                        try {
                            bArr5[i] = dataInputStream.readByte();
                            i++;
                        } catch (IOException e4) {
                            b = readByte4;
                            vectorArr = null;
                            byte[] bArr7 = bArr5;
                            b2 = readByte2;
                            bArr = bArr7;
                            byte b13 = readByte3;
                            z = z3;
                            b3 = b13;
                            byte b14 = readByte;
                            bArr2 = null;
                            b4 = b14;
                            vectorArr2 = vectorArr;
                            byte[] bArr8 = bArr2;
                            b9 = b;
                            bArr3 = bArr8;
                            boolean z4 = z;
                            b7 = b2;
                            z2 = z4;
                            byte b15 = b3;
                            b5 = b10;
                            b6 = b15;
                            byte b16 = b4;
                            bArr4 = bArr;
                            b8 = b16;
                            this.logic.viewGame(b9, b8, b7, b6, b5, z2, bArr4, bArr3, vectorArr2, bArr6);
                        }
                    }
                } else {
                    dataInputStream.skipBytes(3);
                }
                boolean z5 = dataInputStream.readByte() == 1;
                int readByte5 = dataInputStream.readByte();
                byte[] bArr9 = new byte[readByte5];
                if (z5) {
                    for (byte b17 = 0; b17 < 20; b17 = (byte) (b17 + 1)) {
                        if (b17 < readByte5) {
                            try {
                                bArr9[b17] = dataInputStream.readByte();
                            } catch (IOException e5) {
                                b = readByte4;
                                vectorArr = null;
                                byte[] bArr10 = bArr5;
                                b2 = readByte2;
                                bArr = bArr10;
                                byte b18 = readByte3;
                                z = z3;
                                b3 = b18;
                                byte b19 = readByte;
                                bArr2 = bArr9;
                                b4 = b19;
                                vectorArr2 = vectorArr;
                                byte[] bArr82 = bArr2;
                                b9 = b;
                                bArr3 = bArr82;
                                boolean z42 = z;
                                b7 = b2;
                                z2 = z42;
                                byte b152 = b3;
                                b5 = b10;
                                b6 = b152;
                                byte b162 = b4;
                                bArr4 = bArr;
                                b8 = b162;
                                this.logic.viewGame(b9, b8, b7, b6, b5, z2, bArr4, bArr3, vectorArr2, bArr6);
                            }
                        } else {
                            dataInputStream.readByte();
                        }
                    }
                } else {
                    for (byte b20 = 0; b20 < readByte5; b20 = (byte) (b20 + 1)) {
                        bArr9[b20] = 55;
                    }
                    dataInputStream.skipBytes(20);
                }
                Vector[] vectorArr3 = new Vector[3];
                int i2 = 0;
                while (i2 < 3) {
                    try {
                        if (dataInputStream.readByte() == 1) {
                            byte readByte6 = dataInputStream.readByte();
                            Vector vector = new Vector();
                            for (int i3 = 0; i3 < readByte6; i3++) {
                                vector.addElement(new Integer(dataInputStream.readByte()));
                            }
                            vectorArr3[i2] = vector;
                            dataInputStream.skipBytes(20 - readByte6);
                        } else {
                            dataInputStream.skipBytes(21);
                        }
                        i2++;
                    } catch (IOException e6) {
                        Vector[] vectorArr4 = vectorArr3;
                        b = readByte4;
                        vectorArr = vectorArr4;
                        byte[] bArr11 = bArr5;
                        b2 = readByte2;
                        bArr = bArr11;
                        byte b21 = readByte3;
                        z = z3;
                        b3 = b21;
                        byte b22 = readByte;
                        bArr2 = bArr9;
                        b4 = b22;
                        vectorArr2 = vectorArr;
                        byte[] bArr822 = bArr2;
                        b9 = b;
                        bArr3 = bArr822;
                        boolean z422 = z;
                        b7 = b2;
                        z2 = z422;
                        byte b1522 = b3;
                        b5 = b10;
                        b6 = b1522;
                        byte b1622 = b4;
                        bArr4 = bArr;
                        b8 = b1622;
                        this.logic.viewGame(b9, b8, b7, b6, b5, z2, bArr4, bArr3, vectorArr2, bArr6);
                    }
                }
                for (int i4 = 0; i4 < 3; i4++) {
                    bArr6[i4] = dataInputStream.readByte();
                }
                vectorArr2 = vectorArr3;
                bArr3 = bArr9;
                bArr4 = bArr5;
                z2 = z3;
                b5 = b10;
                b6 = readByte3;
                b7 = readByte2;
                b8 = readByte;
                b9 = readByte4;
            } catch (IOException e7) {
                b = readByte4;
                vectorArr = null;
                b2 = readByte2;
                bArr = null;
                byte b23 = readByte3;
                z = z3;
                b3 = b23;
                byte b24 = readByte;
                bArr2 = null;
                b4 = b24;
                vectorArr2 = vectorArr;
                byte[] bArr8222 = bArr2;
                b9 = b;
                bArr3 = bArr8222;
                boolean z4222 = z;
                b7 = b2;
                z2 = z4222;
                byte b15222 = b3;
                b5 = b10;
                b6 = b15222;
                byte b16222 = b4;
                bArr4 = bArr;
                b8 = b16222;
                this.logic.viewGame(b9, b8, b7, b6, b5, z2, bArr4, bArr3, vectorArr2, bArr6);
            }
        } catch (IOException e8) {
            b = 0;
            vectorArr = null;
            b2 = 0;
            bArr = null;
            z = false;
            b3 = 0;
            bArr2 = null;
            b4 = 0;
        }
        this.logic.viewGame(b9, b8, b7, b6, b5, z2, bArr4, bArr3, vectorArr2, bArr6);
    }

    public void parseGameMessage(short s, byte[] bArr) {
        String str;
        long j;
        String str2;
        this.logic.gameTimeOut = 0;
        switch (s) {
            case 112:
                Tools.debug(">>standUp...");
                this.logic.playerStandUp((byte) this.logic.me.seatId, this.logic.me.uid);
                return;
            case 114:
                Tools.debug("   >>KICK_PLARYER");
                DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(bArr));
                try {
                    dataInputStream.skipBytes(8);
                    str = Util.getString(dataInputStream, dataInputStream.readShort());
                } catch (IOException e) {
                    str = null;
                }
                if (str != null) {
                    this.logic.kickFault(str);
                    return;
                }
                return;
            case 127:
                Tools.debug(">>parsePlayMsg...");
                parsePlayMsg(bArr);
                return;
            case 129:
                Tools.debug(">>chat...");
                DataInputStream dataInputStream2 = new DataInputStream(new ByteArrayInputStream(bArr));
                try {
                    dataInputStream2.readShort();
                    short readShort = dataInputStream2.readShort();
                    if (readShort >= 1) {
                        String string = Util.getString(dataInputStream2, readShort);
                        try {
                            long convertUnsigned2Long = Util.convertUnsigned2Long(dataInputStream2.readInt());
                            try {
                                System.out.println("    >>length:" + ((int) readShort) + ",chat content:" + string);
                                j = convertUnsigned2Long;
                                str2 = string;
                            } catch (IOException e2) {
                                j = convertUnsigned2Long;
                                str2 = string;
                            }
                        } catch (IOException e3) {
                            j = 0;
                            str2 = string;
                        }
                        if (AGameLogic.uin != j && str2 != null) {
                            this.logic.chat(j, str2);
                            return;
                        }
                        return;
                    }
                    return;
                } catch (IOException e4) {
                    j = 0;
                    str2 = null;
                }
                break;
            case 410:
                Tools.debug(">>room_event...");
                roomEvent(bArr);
                return;
            default:
                return;
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: protected */
    public void parsePlayMsg(byte[] bArr) {
        byte b;
        byte[] bArr2;
        DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(bArr));
        try {
            dataInputStream.skipBytes(6);
            byte readByte = dataInputStream.readByte();
            Tools.debug("play msg id: " + ((int) readByte));
            switch (readByte) {
                case 49:
                    Tools.debug(">>get base info...");
                    this.logic.setBaseInfo(dataInputStream);
                    return;
                case 50:
                    Tools.debug(">>player start...");
                    return;
                case 51:
                    Tools.debug(">>get my cards...");
                    int readByte2 = dataInputStream.readByte();
                    byte oppositeSeatId = Table.getOppositeSeatId(dataInputStream.readByte(), this.logic.me.seatId);
                    byte[] bArr3 = new byte[readByte2];
                    for (byte b2 = 0; b2 < readByte2; b2 = (byte) (b2 + 1)) {
                        bArr3[b2] = dataInputStream.readByte();
                    }
                    this.logic.setMyCards(bArr3, oppositeSeatId);
                    return;
                case 52:
                    Tools.debug(">>call fen...");
                    byte oppositeSeatId2 = Table.getOppositeSeatId(dataInputStream.readByte(), this.logic.me.seatId);
                    byte readByte3 = dataInputStream.readByte();
                    boolean z = dataInputStream.readByte() == 0;
                    this.logic.call(oppositeSeatId2, readByte3, z, z ? oppositeSeatId2 : Table.getOppositeSeatId(dataInputStream.readByte(), this.logic.me.seatId));
                    return;
                case 53:
                    Tools.debug(">>set back cards...");
                    byte oppositeSeatId3 = Table.getOppositeSeatId(dataInputStream.readByte(), this.logic.me.seatId);
                    int readByte4 = dataInputStream.readByte();
                    byte[] bArr4 = new byte[readByte4];
                    for (byte b3 = 0; b3 < readByte4; b3 = (byte) (b3 + 1)) {
                        bArr4[b3] = dataInputStream.readByte();
                    }
                    this.logic.setBackCards(oppositeSeatId3, bArr4);
                    return;
                case 54:
                    Tools.debug(">>out cards...");
                    dataInputStream.skipBytes(1);
                    byte oppositeSeatId4 = Table.getOppositeSeatId(dataInputStream.readByte(), this.logic.me.seatId);
                    byte oppositeSeatId5 = Table.getOppositeSeatId(dataInputStream.readByte(), this.logic.me.seatId);
                    int readByte5 = dataInputStream.readByte();
                    if (readByte5 > 0) {
                        byte[] bArr5 = new byte[readByte5];
                        for (byte b4 = 0; b4 < readByte5; b4 = (byte) (b4 + 1)) {
                            bArr5[b4] = dataInputStream.readByte();
                        }
                        dataInputStream.skipBytes(53 - readByte5);
                        b = dataInputStream.readByte();
                        bArr2 = bArr5;
                    } else {
                        b = -1;
                        bArr2 = null;
                    }
                    this.logic.setOutCards(oppositeSeatId5, oppositeSeatId4, bArr2, b);
                    return;
                case 55:
                case 56:
                case 61:
                case 63:
                default:
                    return;
                case 57:
                    Tools.debug(">>replay succ...");
                    replay(dataInputStream);
                    return;
                case 58:
                    Tools.debug(">>view game msg...");
                    viewGame(dataInputStream);
                    return;
                case 59:
                    Tools.debug(">>quit game...");
                    this.logic.disbandGame(true);
                    return;
                case 60:
                    Tools.debug(">>over this round...");
                    this.logic.disbandGame(false);
                    return;
                case 62:
                    Tools.debug(">>this round finish...");
                    int[] iArr = new int[3];
                    for (byte b5 = 0; b5 < 3; b5 = (byte) (b5 + 1)) {
                        iArr[Table.getOppositeSeatId(b5, this.logic.me.seatId)] = dataInputStream.readInt();
                    }
                    dataInputStream.readInt();
                    dataInputStream.skipBytes(4);
                    dataInputStream.skipBytes(132);
                    byte[] bArr6 = new byte[3];
                    for (int i = 0; i < 4; i++) {
                        byte readByte6 = dataInputStream.readByte();
                        if (i < bArr6.length) {
                            bArr6[i] = readByte6;
                        }
                    }
                    byte[][] bArr7 = (byte[][]) Array.newInstance(Byte.TYPE, 3, 33);
                    for (byte b6 = 0; b6 < 4; b6 = (byte) (b6 + 1)) {
                        byte oppositeSeatId6 = Table.getOppositeSeatId(b6, this.logic.me.seatId);
                        for (int i2 = 0; i2 < 33; i2++) {
                            byte readByte7 = dataInputStream.readByte();
                            if (b6 < 3 && i2 < bArr6[b6]) {
                                bArr7[oppositeSeatId6][i2] = readByte7;
                            }
                        }
                    }
                    this.logic.setGameResult(bArr7, iArr);
                    return;
                case MProxyInfoV2.MPROXYTYPE_WAP:
                    Tools.debug(">>no body call fen,start new round...");
                    this.logic.newRound(dataInputStream.readByte());
                    return;
            }
        } catch (IOException e) {
            Tools.debug("parserPlayMsg exception, detail info: " + e.toString());
        }
        Tools.debug("parserPlayMsg exception, detail info: " + e.toString());
    }

    public void receiveGameMessage(Message message) {
        synchronized (this.logic.messageList) {
            this.logic.messageList.add(message);
        }
    }
}
