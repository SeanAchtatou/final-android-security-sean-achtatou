package com.tencent.qqgame.lord.ui;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import com.tencent.qqgame.common.Player;
import com.tencent.qqgame.common.Table;
import com.tencent.qqgame.hall.common.IGameView;
import com.tencent.qqgame.hall.protocol.IReceiveGameMessageHandle;
import com.tencent.qqgame.hall.protocol.ISendGameMessageHandle;
import com.tencent.qqgame.lord.ui.control.ChatDialog;

public class LordLayout extends RelativeLayout implements IGameView, View.OnFocusChangeListener {
    private EditText etChatInput = null;
    private LordSurface gameView;

    public LordLayout(Activity activity, Handler handler, ISendGameMessageHandle iSendGameMessageHandle, Table table, Player player, long j, boolean z) {
        super(activity);
        setBackgroundDrawable(null);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        this.gameView = new LordSurface(activity, handler, iSendGameMessageHandle, table, player, j, z);
        this.gameView.setId(1);
        addView(this.gameView, layoutParams);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(6, 1);
        layoutParams2.addRule(5, 1);
        layoutParams2.leftMargin = 82;
        layoutParams2.topMargin = 60;
        this.etChatInput = new EditText(activity);
        this.etChatInput.setWidth(200);
        this.etChatInput.setHeight(38);
        this.etChatInput.setVisibility(4);
        this.etChatInput.setSingleLine();
        this.etChatInput.setOnFocusChangeListener(this);
        ChatDialog.chatInput = this.etChatInput;
        addView(this.etChatInput, layoutParams2);
    }

    public IReceiveGameMessageHandle getGameReceiveMessageHandle() {
        return this.gameView.getGameReceiveMessageHandle();
    }

    public void onFocusChange(View view, boolean z) {
        if (view.equals(this.etChatInput) && z) {
            this.etChatInput.selectAll();
        }
    }

    public final void sendMessage(Message message, boolean z) {
        this.gameView.sendMessage(message, z);
    }
}
