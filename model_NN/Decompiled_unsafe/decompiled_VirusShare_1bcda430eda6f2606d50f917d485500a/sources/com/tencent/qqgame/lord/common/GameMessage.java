package com.tencent.qqgame.lord.common;

import com.tencent.qqgame.common.Util;
import com.tencent.qqgame.lord.ui.LordView;

public class GameMessage {
    public static final int TIMER_STOP_INTERVAL = 700;
    public int curLine;
    public int length;
    public String[] message = null;
    public boolean needScroll;
    public int offset;
    public String originMsg = null;
    public int timerShow = 4000;
    public int timerStop = TIMER_STOP_INTERVAL;
    public long uid = -1;

    public GameMessage(String str, long j, int i) {
        init(str, j, i);
    }

    private void init(String str, long j, int i) {
        this.uid = j;
        this.originMsg = str;
        this.message = Util.breakLine(str, i);
        LordView.paint.setTextSize(16.0f);
        this.length = 0;
        for (String measureText : this.message) {
            int measureText2 = (int) LordView.paint.measureText(measureText);
            if (measureText2 > this.length) {
                this.length = measureText2;
            }
        }
        this.needScroll = false;
        if (this.message.length > 1) {
            this.needScroll = true;
        }
        this.curLine = 0;
        this.offset = 0;
    }
}
