package com.tencent.qqgame.hall.ui.controls;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import com.tencent.qqgame.common.Player;
import com.tencent.qqgame.hall.common.AActivity;
import com.tencent.qqgame.hall.common.AnimationPlayerV1;
import com.tencent.qqgame.hall.common.IGameView;
import com.tencent.qqgame.hall.common.Report;
import com.tencent.qqgame.hall.common.SpriteV1;
import com.tencent.qqgame.hall.common.Tools;
import com.tencent.qqgame.hall.ui.TableListActivity;
import com.tencent.qqgame.lord.common.FrameControlV1;
import com.tencent.qqgame.lord.common.GameMessage;
import tencent.qqgame.lord.R;

public class UserInfoControl extends View {
    public static final byte FRAME_PANEL_USER_INFO_BOTTOM_LEFT = 1;
    public static final byte FRAME_PANEL_USER_INFO_BOTTOM_RIGHT = 0;
    public static final byte FRAME_PANEL_USER_INFO_TOP_LEFT = 3;
    public static final byte FRAME_PANEL_USER_INFO_TOP_RIGHT = 2;
    public static final byte FRAME_PANEL_WATCH_DISABLE = 32;
    public static final byte FRAME_PANEL_WATCH_HIGH = 31;
    public static final byte FRAME_PANEL_WATCH_NORMAL = 30;
    private static int[] gradeScore = {0, 10, 25, 40, 80, 140, 230, 365, 500, GameMessage.TIMER_STOP_INTERVAL, IGameView.MSG_2GAME_DIS_CONNECTED, 1500, 2200, 3000, 4000, 5500, 7700, 10000, 14000, 20000, 30000, 45000, 70000};
    private static String[] gradeString = {AActivity.res.getString(R.string.lord_level00), AActivity.res.getString(R.string.lord_level01), AActivity.res.getString(R.string.lord_level02), AActivity.res.getString(R.string.lord_level03), AActivity.res.getString(R.string.lord_level04), AActivity.res.getString(R.string.lord_level05), AActivity.res.getString(R.string.lord_level06), AActivity.res.getString(R.string.lord_level07), AActivity.res.getString(R.string.lord_level08), AActivity.res.getString(R.string.lord_level09), AActivity.res.getString(R.string.lord_level10), AActivity.res.getString(R.string.lord_level11), AActivity.res.getString(R.string.lord_level12), AActivity.res.getString(R.string.lord_level13), AActivity.res.getString(R.string.lord_level14), AActivity.res.getString(R.string.lord_level15), AActivity.res.getString(R.string.lord_level16), AActivity.res.getString(R.string.lord_level17), AActivity.res.getString(R.string.lord_level18), AActivity.res.getString(R.string.lord_level19), AActivity.res.getString(R.string.lord_level20), AActivity.res.getString(R.string.lord_level21), AActivity.res.getString(R.string.lord_level22)};
    public short HEIGHT;
    public short WIDTH;
    public AnimationPlayerV1 apButton;
    public AnimationPlayerV1 apPanel;
    public FrameControlV1 ctlWatch;
    private int panel_frame_id;
    private Player player;
    public short tableId;

    public UserInfoControl(Context context) {
        super(context);
        this.tableId = 0;
        this.panel_frame_id = 3;
        this.ctlWatch = null;
        this.apPanel = new AnimationPlayerV1(new SpriteV1(getContext(), R.raw.ppanel_));
        this.apButton = new AnimationPlayerV1(new SpriteV1(getContext(), R.raw.button_));
        this.ctlWatch = new FrameControlV1(this.apButton, new byte[]{30, 32, 31});
        this.ctlWatch.setStatus((byte) 0);
    }

    public static String getLevelName(int i) {
        if (gradeScore == null || gradeString == null || gradeScore.length != gradeString.length || gradeScore.length < 2) {
            return AActivity.res.getString(R.string.lord_no_level);
        }
        for (int length = gradeScore.length - 1; length > 0; length--) {
            if (i >= gradeScore[length]) {
                return gradeString[length];
            }
        }
        return gradeString[0];
    }

    public void draw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        this.apPanel.drawFrame(canvas, this.panel_frame_id, 0, 0);
        paint.setColor(-1);
        paint.setTextSize((float) 12);
        int i = (this.panel_frame_id == 0 || this.panel_frame_id == 1) ? 15 : 20;
        int i2 = i + 12;
        canvas.drawText(this.player.nickName, (float) 16, (float) i2, paint);
        if (this.player.isSuperPlayer()) {
            canvas.drawBitmap(((TableListActivity) AActivity.currentActivity).imgBlue, (float) (this.WIDTH - 28), (float) i, paint);
        }
        int i3 = 12 + 6;
        int i4 = i2 + 18;
        Resources resources = getContext().getResources();
        canvas.drawText(resources.getString(R.string.info_qq) + this.player.uid, (float) 16, (float) i4, paint);
        String str = resources.getString(R.string.info_level) + getLevelName(this.player.point);
        int i5 = 12 + 4;
        int i6 = i4 + 16;
        canvas.drawText(str, (float) 16, (float) i6, paint);
        canvas.drawText(resources.getString(R.string.info_fen) + this.player.point, (float) (((int) paint.measureText(str)) + 16 + 6), (float) i6, paint);
        this.ctlWatch.setLocation(16, i6 + 7);
        this.ctlWatch.draw(canvas, paint);
    }

    public void init(Player player2, short s, int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        this.player = player2;
        this.tableId = s;
        this.WIDTH = (short) this.apPanel.getFrameWidth(this.panel_frame_id);
        this.HEIGHT = (short) this.apPanel.getFrameHeight(this.panel_frame_id);
        if (player2.status == 4 || player2.status == 5) {
            this.ctlWatch.setStatus((byte) 0);
        } else {
            this.ctlWatch.setStatus((byte) 1);
        }
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        if (i < Tools.screenWidth / 2) {
            if (i2 < Tools.screenHeight / 2) {
                this.panel_frame_id = 3;
                layoutParams.topMargin = i2;
            } else {
                this.panel_frame_id = 1;
                layoutParams.topMargin = i2 - this.HEIGHT;
            }
            layoutParams.leftMargin = i;
        } else {
            if (i2 < Tools.screenHeight / 2) {
                this.panel_frame_id = 2;
                layoutParams.topMargin = i2;
            } else {
                this.panel_frame_id = 0;
                layoutParams.topMargin = i2 - this.HEIGHT;
            }
            layoutParams.leftMargin = i - this.WIDTH;
        }
        if (player2.seatId == 0) {
            i5 = 108;
            i6 = 26;
        } else if (player2.seatId == 1) {
            i5 = 60;
            i6 = 75;
        } else if (player2.seatId == 2) {
            i5 = 12;
            i6 = 26;
        } else {
            i5 = 0;
            i6 = 0;
        }
        if (this.panel_frame_id == 3 || this.panel_frame_id == 1) {
            layoutParams.leftMargin -= i3 - i5;
        } else {
            layoutParams.leftMargin = (40 - (i3 - i5)) + layoutParams.leftMargin;
        }
        if (this.panel_frame_id == 2 || this.panel_frame_id == 3) {
            layoutParams.topMargin = (50 - (i4 - i6)) + layoutParams.topMargin;
        } else {
            layoutParams.topMargin -= i4 - i6;
        }
        setLayoutParams(layoutParams);
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        setMeasuredDimension(this.WIDTH, this.HEIGHT);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.ctlWatch.isTouched((int) motionEvent.getX(), (int) motionEvent.getY())) {
            if (motionEvent.getAction() == 0) {
                this.ctlWatch.setStatus((byte) 2);
                return true;
            } else if (motionEvent.getAction() == 1) {
                Report.watchHitNum++;
                Handler handler = ((TableListActivity) AActivity.currentActivity).handle;
                Message obtainMessage = handler.obtainMessage(4);
                obtainMessage.arg1 = this.tableId;
                obtainMessage.arg2 = this.player.seatId;
                handler.sendMessage(obtainMessage);
                this.ctlWatch.setStatus((byte) 0);
            }
        }
        return super.onTouchEvent(motionEvent);
    }

    public void recycle() {
        if (this.apButton != null) {
            this.apButton.recycle(true);
            this.apButton = null;
        }
        if (this.apPanel != null) {
            this.apPanel.recycle(true);
            this.apPanel = null;
        }
    }
}
