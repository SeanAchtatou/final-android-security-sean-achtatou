package com.tencent.qqgame.hall.common;

import android.os.Message;
import com.tencent.qqgame.hall.protocol.IReceiveGameMessageHandle;

public interface IGameView {
    public static final byte MENU_ID_CHANGE_DESK = 0;
    public static final byte MENU_ID_CHAT = 1;
    public static final byte MENU_ID_EXIT = 4;
    public static final byte MENU_ID_SET = 3;
    public static final byte MENU_ID_TRUSTEESHIP = 2;
    public static final int MSG_2GAME_BACK_PRESSED = 1006;
    public static final int MSG_2GAME_CHANGEDESK_FAULT = 1007;
    public static final int MSG_2GAME_CHANGEDESK_SUCC = 1008;
    public static final int MSG_2GAME_CHAT_EDITVIEW = 1013;
    public static final int MSG_2GAME_DIS_CONNECTED = 1000;
    public static final int MSG_2GAME_EXIT_ACTIVITY = 1011;
    public static final int MSG_2GAME_FOREGROUND = 1014;
    public static final int MSG_2GAME_MENU_ITEM_SELECTED = 1009;
    public static final int MSG_2GAME_NET_NORMALED = 1005;
    public static final int MSG_2GAME_NET_SLOW = 1004;
    public static final int MSG_2GAME_PARSE_SERVER_MSG = 1010;
    public static final int MSG_2GAME_PROGRESSDIALOG_CANCELED = 1012;
    public static final int MSG_2GAME_REPLAY_FAULT = 1003;
    public static final int MSG_2GAME_REPLAY_SUCC = 1002;
    public static final byte MSG_2HALL_ADJUS_BRIGHT = 113;
    public static final byte MSG_2HALL_CANCEL_VIEW = 105;
    public static final byte MSG_2HALL_CHANGE_MENU_STATUS = 100;
    public static final byte MSG_2HALL_CREATE_EXIT_DIALOG = 114;
    public static final byte MSG_2HALL_EXIT_GAME = 102;
    public static final byte MSG_2HALL_FORCE_EXIT_GAME = 103;
    public static final byte MSG_2HALL_FORCE_EXIT_TO_LOGIN = 107;
    public static final byte MSG_2HALL_GAME_EVENT = 101;
    public static final byte MSG_2HALL_NOTIFICATION = 104;
    public static final byte MSG_2HALL_PROGRESS_DIALOG = 110;
    public static final byte MSG_2HALL_REMOVE_PROGRESS = 111;
    public static final byte MSG_2HALL_REMOVE_REPLAY_INFO = 109;
    public static final byte MSG_2HALL_REPLAY = 106;
    public static final byte MSG_2HALL_SAVE_REPLAY_INFO = 108;
    public static final byte MSG_2HALL_UI_REQUEST = 112;

    IReceiveGameMessageHandle getGameReceiveMessageHandle();

    void sendMessage(Message message, boolean z);
}
