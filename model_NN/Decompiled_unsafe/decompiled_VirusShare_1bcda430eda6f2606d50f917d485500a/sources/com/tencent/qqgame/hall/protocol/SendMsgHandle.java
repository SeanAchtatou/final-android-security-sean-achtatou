package com.tencent.qqgame.hall.protocol;

import com.tencent.qqgame.common.Msg;
import com.tencent.qqgame.common.Util;
import com.tencent.qqgame.hall.common.AActivity;
import com.tencent.qqgame.hall.common.FactoryCenter;
import com.tencent.qqgame.hall.common.SyncData;
import com.tencent.qqgame.hall.common.Tools;
import com.tencent.qqgame.hall.common.data.MProxyInfoV2;
import com.tencent.qqgame.hall.communication.SocketCommunicator;

public class SendMsgHandle implements ISendGameMessageHandle {
    public static final short LOGIN_LOGIN_PLAY = 1;
    public static final short LOGIN_NORMAL = 0;
    public static final short LOGIN_REPLAY = 2;
    protected final byte MSG_HEAD_LEN = 10;
    protected FactoryCenter factory;

    public SendMsgHandle(FactoryCenter factoryCenter) {
        this.factory = factoryCenter;
    }

    public void cancelView() {
        Msg msg = new Msg(fixMsgHead(10), 10);
        msg.playerId = SyncData.curUin;
        msg.mobileTypeId = SyncData.MOBILE_TYPE_ID;
        msg.flag = 0;
        msg.optLen = 0;
        msg.msgId = Msg.MSG_ID_CANCELVIEW;
        msg.msgType = 10;
        msg.sendIntervalTime = 500;
        msg.sendTimes = 2;
        Tools.debug("<<Sended Msg cancel view");
        doSend(msg);
    }

    public void chat(String str) {
        char[] cArr = new char[str.length()];
        str.getChars(0, str.length(), cArr, 0);
        short length = (short) ((cArr.length * 2) + 2);
        short s = (short) (length + 12);
        byte[] fixMsgHead = fixMsgHead(s);
        Util.word2Byte(fixMsgHead, 10, length);
        fixMsgHead[12] = -2;
        fixMsgHead[13] = -1;
        Util.getByteFromChars(fixMsgHead, 14, cArr, 0, cArr.length);
        Msg msg = new Msg(fixMsgHead, s);
        msg.playerId = SyncData.curUin;
        msg.mobileTypeId = SyncData.MOBILE_TYPE_ID;
        msg.flag = 0;
        msg.optLen = 0;
        msg.msgId = Msg.MSG_ID_TALK_ON_TABLE;
        msg.msgType = 10;
        msg.sendIntervalTime = 500;
        msg.sendTimes = 2;
        Tools.debug("<<Sended Msg chat");
        doSend(msg);
    }

    /* access modifiers changed from: protected */
    public void doSend(Msg msg) {
        if (!this.factory.getCommunicator().isConnected()) {
            if (SyncData.isLogined) {
                AActivity.currentActivity.handle.sendEmptyMessage(AActivity.MSG_LOGOUT_HANDLE);
                return;
            }
            this.factory.getCommunicator().connect(true);
        }
        this.factory.getCommunicator().sendMsg(msg);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
     arg types: [byte[], byte, int]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void */
    public byte[] fixMsgHead(short s) {
        byte[] bArr = new byte[s];
        Util.word2Byte(bArr, 0, s);
        byte b = (byte) (0 + 2);
        Util.word2Byte(bArr, (int) b, Util.convert2Unsigned(SyncData.curUin));
        byte b2 = (byte) (b + 4);
        Util.word2Byte(bArr, (int) b2, (int) Util.getCurrentTimeStamp());
        byte b3 = (byte) (b2 + 4);
        return bArr;
    }

    public void getGameVisionInfo(int i) {
        byte[] fixMsgHead = fixMsgHead(16);
        Util.word2Byte(fixMsgHead, 10, i);
        Msg msg = new Msg(fixMsgHead, 16);
        msg.playerId = SyncData.curUin;
        msg.mobileTypeId = SyncData.MOBILE_TYPE_ID;
        msg.flag = 0;
        msg.optLen = 0;
        msg.msgId = Msg.MSG_ID_GET_PLATFORM_GAMES_INFO;
        msg.msgType = 10;
        msg.sendIntervalTime = 500;
        msg.sendTimes = 2;
        doSend(msg);
        Tools.debug("<<getGameVisi...");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
     arg types: [byte[], int, int]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void */
    public void getRoomList(short s) {
        short s2 = (short) (0 + 12 + 2 + 2 + 2 + 2);
        byte[] fixMsgHead = fixMsgHead(s2);
        Util.word2Byte(fixMsgHead, 10, (short) 0);
        byte b = (byte) (10 + 2);
        Util.word2Byte(fixMsgHead, b + 0, s);
        byte b2 = (byte) (b + 2);
        Util.word2Byte(fixMsgHead, b2 + 0, (short) -1);
        byte b3 = (byte) (b2 + 2);
        Util.word2Byte(fixMsgHead, b3 + 0, (short) 0);
        byte b4 = (byte) (b3 + 2);
        Util.word2Byte(fixMsgHead, b4 + 0, SyncData.gameType);
        byte b5 = (byte) (b4 + 2);
        Msg msg = new Msg(fixMsgHead, s2);
        msg.playerId = SyncData.curUin;
        msg.mobileTypeId = SyncData.MOBILE_TYPE_ID;
        msg.flag = 0;
        msg.optLen = 0;
        msg.msgId = Msg.MSG_ID_LIST_ROOM;
        msg.msgType = 10;
        msg.sendIntervalTime = 500;
        msg.sendTimes = 2;
        Tools.debug("<<getRoomList... zoneId:" + ((int) s));
        doSend(msg);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
     arg types: [byte[], int, int]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
     arg types: [byte[], byte, int]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void */
    public void getServerList() {
        byte[] fixMsgHead = fixMsgHead(16);
        Util.word2Byte(fixMsgHead, 10, (short) 0);
        byte b = (byte) (10 + 2);
        Util.word2Byte(fixMsgHead, (int) b, (short) 50);
        byte b2 = (byte) (b + 2);
        Msg msg = new Msg(fixMsgHead, 16);
        msg.playerId = SyncData.curUin;
        msg.mobileTypeId = SyncData.MOBILE_TYPE_ID;
        msg.flag = 0;
        msg.optLen = 0;
        msg.msgId = Msg.MSG_ID_GET_SERVER;
        msg.msgType = 10;
        msg.sendIntervalTime = 500;
        msg.sendTimes = 2;
        Tools.debug("<<getServerList...");
        doSend(msg);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
     arg types: [byte[], int, int]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void */
    public void getTableList(short s, short s2, short s3, short s4, short s5, boolean z) {
        short s6 = (short) (0 + 12 + 2 + 2 + 2 + 2 + 2 + 2);
        byte[] fixMsgHead = fixMsgHead(s6);
        Util.word2Byte(fixMsgHead, 10, (short) 0);
        byte b = (byte) (10 + 2);
        Util.word2Byte(fixMsgHead, b + 0, s);
        byte b2 = (byte) (b + 2);
        Util.word2Byte(fixMsgHead, b2 + 0, s3);
        byte b3 = (byte) (b2 + 2);
        Util.word2Byte(fixMsgHead, b3 + 0, s2);
        byte b4 = (byte) (b3 + 2);
        Util.word2Byte(fixMsgHead, b4 + 0, s4);
        byte b5 = (byte) (b4 + 2);
        Util.word2Byte(fixMsgHead, b5 + 0, s5);
        Util.word2Byte(fixMsgHead, ((byte) (b5 + 2)) + 0, SyncData.gameType);
        Msg msg = new Msg(fixMsgHead, s6);
        msg.playerId = SyncData.curUin;
        msg.mobileTypeId = SyncData.MOBILE_TYPE_ID;
        msg.flag = 0;
        msg.optLen = 0;
        if (z) {
            msg.msgId = Msg.MSG_ID_ENTER_ROOM;
        } else {
            msg.msgId = Msg.MSG_ID_LIST_TABLE;
        }
        msg.msgType = 10;
        msg.sendIntervalTime = 500;
        msg.sendTimes = 2;
        Tools.debug("<<getTableList... roomId:" + ((int) s2));
        doSend(msg);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
     arg types: [byte[], int, int]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void */
    public void getZoneList() {
        short s = (short) (0 + 12 + 2 + 2 + 2 + 4);
        byte[] fixMsgHead = fixMsgHead(s);
        Util.word2Byte(fixMsgHead, 10, (short) 0);
        Util.word2Byte(fixMsgHead, 0 + 12, (short) -1);
        Util.word2Byte(fixMsgHead, 0 + 12 + 2, (short) 0);
        Util.word2Byte(fixMsgHead, 0 + 12 + 2 + 2, SyncData.gameType);
        Util.word2Byte(fixMsgHead, 0 + 12 + 2 + 2 + 2, (int) SyncData.VERSION_S);
        Msg msg = new Msg(fixMsgHead, s);
        msg.playerId = SyncData.curUin;
        msg.mobileTypeId = SyncData.MOBILE_TYPE_ID;
        msg.flag = 0;
        msg.optLen = 0;
        msg.msgId = Msg.MSG_ID_LIST_ZONE;
        msg.msgType = 10;
        msg.sendIntervalTime = 500;
        msg.sendTimes = 2;
        Tools.debug("<<getZoneList...");
        doSend(msg);
    }

    public void handUp() {
        Msg msg = new Msg(fixMsgHead(10), 10);
        msg.playerId = SyncData.curUin;
        msg.mobileTypeId = SyncData.MOBILE_TYPE_ID;
        msg.flag = 0;
        msg.optLen = 0;
        msg.msgId = Msg.MSG_ID_RIASE_HAND;
        msg.msgType = 10;
        msg.sendIntervalTime = 500;
        msg.sendTimes = 2;
        Tools.debug("<<raiseHand...");
        doSend(msg);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
     arg types: [byte[], int, int]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void */
    public void helloMsg() {
        byte[] fixMsgHead = fixMsgHead(12);
        Msg msg = new Msg(fixMsgHead, 12);
        Util.word2Byte(fixMsgHead, 10, (short) 0);
        msg.playerId = SyncData.curUin;
        msg.mobileTypeId = SyncData.MOBILE_TYPE_ID;
        msg.flag = 0;
        msg.optLen = 0;
        msg.msgId = Msg.MSG_ID_HELLO;
        msg.msgType = 10;
        msg.sendIntervalTime = 500;
        msg.sendTimes = 2;
        Tools.debug("<<helloMsg...");
        doSend(msg);
    }

    public void leaveRoom(short s, short s2) {
        byte[] fixMsgHead = fixMsgHead(16);
        Util.word2Byte(fixMsgHead, 10, s);
        Util.word2Byte(fixMsgHead, 12, s2);
        Util.word2Byte(fixMsgHead, 14, SyncData.gameType);
        Msg msg = new Msg(fixMsgHead, 16);
        msg.playerId = SyncData.curUin;
        msg.mobileTypeId = SyncData.MOBILE_TYPE_ID;
        msg.flag = 0;
        msg.optLen = 0;
        msg.msgId = Msg.MSG_ID_LEAVE_ROOM;
        msg.msgType = 10;
        msg.sendIntervalTime = 500;
        msg.sendTimes = 2;
        Tools.debug("<<leaveRoom...");
        doSend(msg);
    }

    public void login(long j, byte[] bArr, short s) {
        login(j, bArr, s, -1, -1, -1, -1, -1, -1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
     arg types: [byte[], byte, ?]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
     arg types: [byte[], byte, short]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
     arg types: [byte[], byte, int]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
     arg types: [byte[], byte, int]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void */
    public void login(long j, byte[] bArr, short s, short s2, short s3, short s4, short s5, short s6, short s7) {
        short s8 = 42;
        if (s == 2) {
            s8 = (short) (42 + 20);
        }
        byte[] fixMsgHead = fixMsgHead(s8);
        System.arraycopy(bArr, 0, fixMsgHead, 10, bArr.length);
        byte length = (byte) (10 + bArr.length);
        Util.word2Byte(fixMsgHead, (int) length, (int) SyncData.VERSION_S);
        byte b = (byte) (length + 4);
        Util.word2Byte(fixMsgHead, (int) b, SyncData.gameType);
        byte b2 = (byte) (b + 2);
        Util.word2Byte(fixMsgHead, (int) b2, 3);
        byte b3 = (byte) (b2 + 4);
        Util.word2Byte(fixMsgHead, (int) b3, (int) SyncData.VERSION_S);
        byte b4 = (byte) (b3 + 4);
        Util.word2Byte(fixMsgHead, (int) b4, s);
        byte b5 = (byte) (b4 + 2);
        if (s == 2) {
            Util.word2Byte(fixMsgHead, (int) b5, s2);
            byte b6 = (byte) (b5 + 2);
            Util.word2Byte(fixMsgHead, (int) b6, s3);
            byte b7 = (byte) (b6 + 2);
            Util.word2Byte(fixMsgHead, (int) b7, s4);
            byte b8 = (byte) (b7 + 2);
            Util.word2Byte(fixMsgHead, (int) b8, s5);
            byte b9 = (byte) (b8 + 2);
            Util.word2Byte(fixMsgHead, (int) b9, s6);
            byte b10 = (byte) (b9 + 2);
            Util.word2Byte(fixMsgHead, (int) b10, s7);
            byte b11 = (byte) (b10 + 2);
            Util.word2Byte(fixMsgHead, (int) b11, 500);
            byte b12 = (byte) (b11 + 4);
            Util.word2Byte(fixMsgHead, (int) b12, (short) 3);
            byte b13 = (byte) (b12 + 2);
            Util.word2Byte(fixMsgHead, (int) b13, (short) 3);
            byte b14 = (byte) (b13 + 2);
        }
        Msg msg = new Msg(fixMsgHead, s8);
        Msg.version = 1609;
        Msg.dialogId = -1;
        msg.playerId = j;
        msg.mobileTypeId = SyncData.MOBILE_TYPE_ID;
        msg.flag = 0;
        msg.optLen = 0;
        msg.msgId = 100;
        msg.msgType = 10;
        msg.sendIntervalTime = 500;
        msg.sendTimes = 2;
        Tools.debug("<<login...type: " + ((int) s));
        doSend(msg);
    }

    public void logout() {
        SyncData.isLogined = false;
        Msg msg = new Msg(fixMsgHead(10), 10);
        msg.playerId = SyncData.curUin;
        msg.mobileTypeId = SyncData.MOBILE_TYPE_ID;
        msg.flag = 0;
        msg.optLen = 0;
        msg.msgId = 101;
        msg.msgType = 10;
        msg.sendIntervalTime = 500;
        msg.sendTimes = 2;
        Tools.debug("<<logout...");
        doSend(msg);
        new Thread(new Runnable() {
            public void run() {
                SocketCommunicator communicator = SendMsgHandle.this.factory.getCommunicator();
                SendMsgHandle.this.factory._comm = null;
                communicator.disConnect(true);
            }
        }).start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
     arg types: [byte[], byte, short]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void */
    public void quickPlay(short s) {
        SyncData.lastQuickPlayMode = s;
        byte[] fixMsgHead = fixMsgHead(15);
        Util.word2Byte(fixMsgHead, 10, SyncData.gameType);
        byte b = (byte) (10 + 2);
        Util.word2Byte(fixMsgHead, (int) b, s);
        byte b2 = (byte) (b + 2);
        byte b3 = (byte) (b2 + 1);
        fixMsgHead[b2] = 1;
        Msg msg = new Msg(fixMsgHead, 15);
        msg.playerId = SyncData.curUin;
        msg.mobileTypeId = SyncData.MOBILE_TYPE_ID;
        msg.flag = 0;
        msg.optLen = 0;
        msg.msgId = Msg.MSG_ID_QUICK_PLAY;
        msg.msgType = 10;
        msg.sendIntervalTime = 500;
        msg.sendTimes = 2;
        Tools.debug("<<quickPlay...");
        doSend(msg);
    }

    public void replay(short s, short s2, short s3, short s4, short s5, short s6, short s7) {
        Tools.debug("gameType:" + ((int) s) + ";isReplay:" + ((int) s2) + ";zoneID:" + ((int) s3) + ";serverID:" + ((int) s4) + ";roomID:" + ((int) s5) + ";tableID:" + ((int) s6) + ";seatID:" + ((int) s7));
        byte[] fixMsgHead = fixMsgHead(24);
        Util.word2Byte(fixMsgHead, 10, s2);
        Util.word2Byte(fixMsgHead, 12, s);
        Util.word2Byte(fixMsgHead, 14, s3);
        Util.word2Byte(fixMsgHead, 16, s4);
        Util.word2Byte(fixMsgHead, 18, s5);
        Util.word2Byte(fixMsgHead, 20, s6);
        Util.word2Byte(fixMsgHead, 22, s7);
        Msg msg = new Msg(fixMsgHead, 24);
        msg.playerId = SyncData.curUin;
        msg.mobileTypeId = SyncData.MOBILE_TYPE_ID;
        msg.flag = 0;
        msg.optLen = 0;
        msg.msgId = Msg.MSG_ID_REPLAY;
        msg.msgType = 10;
        msg.sendIntervalTime = 500;
        msg.sendTimes = 2;
        Tools.debug("<<Sended Msg replay");
        doSend(msg);
    }

    public void sendGameMessage(byte[] bArr) {
        if (bArr != null && bArr.length > 0) {
            Msg msg = new Msg(bArr, bArr.length);
            msg.playerId = SyncData.curUin;
            msg.mobileTypeId = SyncData.MOBILE_TYPE_ID;
            msg.flag = 0;
            msg.optLen = 0;
            msg.msgId = Msg.MSG_ID_PLAY;
            msg.msgType = 10;
            msg.sendIntervalTime = 500;
            msg.sendTimes = 2;
            doSend(msg);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
     arg types: [byte[], int, int]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
     arg types: [byte[], byte, int]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void */
    public void sendSpeedTest(long j, long j2, long j3) {
        byte b;
        byte[] bArr = new byte[19];
        Util.word2Byte(bArr, 0, (short) 19);
        byte b2 = (byte) (0 + 2);
        Util.word2Byte(bArr, (int) b2, Util.convert2Unsigned(j));
        byte b3 = (byte) (b2 + 4);
        Util.word2Byte(bArr, (int) b3, Util.convert2Unsigned(j2));
        byte b4 = (byte) (b3 + 4);
        Util.word2Byte(bArr, (int) b4, (int) (System.currentTimeMillis() - j3));
        byte b5 = (byte) (b4 + 4);
        switch (MProxyInfoV2.getNetWorkType(AActivity.currentActivity)) {
            case 1:
                b = 1;
                break;
            case 2:
                b = 3;
                break;
            case 3:
            default:
                b = 0;
                break;
            case 4:
                b = 2;
                break;
        }
        byte b6 = (byte) (b5 + 1);
        bArr[b5] = b;
        Util.word2Byte(bArr, (int) b6, 0);
        byte b7 = (byte) (b6 + 4);
        Msg msg = new Msg(bArr, 19);
        msg.playerId = SyncData.curUin;
        msg.mobileTypeId = SyncData.MOBILE_TYPE_ID;
        msg.flag = 0;
        msg.optLen = 0;
        msg.msgId = Msg.MSG_ID_NOTIFY_SPEEDTEST;
        msg.msgType = 13;
        msg.sendIntervalTime = 500;
        msg.sendTimes = 2;
        Tools.debug("<<sendSpeedTest...");
        doSend(msg);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
     arg types: [byte[], byte, short]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void */
    public void sitDown(short s, short s2, short s3, short s4, short s5) {
        byte[] fixMsgHead = fixMsgHead(20);
        Util.word2Byte(fixMsgHead, 10, s);
        byte b = (byte) (10 + 2);
        Util.word2Byte(fixMsgHead, (int) b, s3);
        byte b2 = (byte) (b + 2);
        Util.word2Byte(fixMsgHead, (int) b2, s2);
        byte b3 = (byte) (b2 + 2);
        Util.word2Byte(fixMsgHead, (int) b3, s4);
        Util.word2Byte(fixMsgHead, (int) ((byte) (b3 + 2)), s5);
        Msg msg = new Msg(fixMsgHead, 20);
        msg.playerId = SyncData.curUin;
        msg.mobileTypeId = SyncData.MOBILE_TYPE_ID;
        msg.flag = 0;
        msg.optLen = 0;
        msg.msgId = Msg.MSG_ID_SITDOWN;
        msg.msgType = 10;
        msg.sendIntervalTime = 500;
        msg.sendTimes = 2;
        Tools.debug("<<sitdown... zoneId:" + ((int) s));
        doSend(msg);
    }

    public void standUp() {
        Msg msg = new Msg(fixMsgHead(10), 10);
        msg.playerId = SyncData.curUin;
        msg.mobileTypeId = SyncData.MOBILE_TYPE_ID;
        msg.flag = 0;
        msg.optLen = 0;
        msg.msgId = Msg.MSG_ID_STANDUP;
        msg.msgType = 10;
        msg.sendIntervalTime = 500;
        msg.sendTimes = 2;
        Tools.debug("<<standUp...");
        doSend(msg);
    }

    public void tickPlayer(long j) {
        byte[] fixMsgHead = fixMsgHead(14);
        Util.word2Byte(fixMsgHead, 10, Util.convert2Unsigned(j));
        Msg msg = new Msg(fixMsgHead, 14);
        msg.playerId = SyncData.curUin;
        msg.mobileTypeId = SyncData.MOBILE_TYPE_ID;
        msg.flag = 0;
        msg.optLen = 0;
        msg.msgId = Msg.MSG_ID_KICKPLARYER;
        msg.msgType = 10;
        msg.sendIntervalTime = 500;
        msg.sendTimes = 2;
        Tools.debug("<<Sended Msg Kick:" + j);
        doSend(msg);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
     arg types: [byte[], int, int]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void */
    public void verifyCode(short s, String str) {
        short length = s == 0 ? (short) (str.length() + 14) : 14;
        byte[] fixMsgHead = fixMsgHead(length);
        Util.word2Byte(fixMsgHead, 10, s);
        if (s == 0) {
            char[] cArr = new char[str.length()];
            str.getChars(0, str.length(), cArr, 0);
            Util.word2Byte(fixMsgHead, 12, (short) cArr.length);
            for (int i = 0; i < cArr.length; i++) {
                fixMsgHead[i + 14] = (byte) cArr[i];
            }
        } else {
            Util.word2Byte(fixMsgHead, 12, (short) 0);
        }
        Msg msg = new Msg(fixMsgHead, length);
        msg.playerId = SyncData.curUin;
        msg.mobileTypeId = SyncData.MOBILE_TYPE_ID;
        msg.flag = 0;
        msg.optLen = 0;
        msg.msgId = Msg.MSG_ID_VERIFYCODE;
        msg.msgType = 10;
        msg.sendIntervalTime = 500;
        msg.sendTimes = 2;
        Tools.debug("<<Sended Msg Verify code=" + str + "; type=" + ((int) s));
        doSend(msg);
    }

    public void viewGame(short s, short s2, short s3, short s4, short s5) {
        byte[] fixMsgHead = fixMsgHead(20);
        Util.word2Byte(fixMsgHead, 10, s);
        Util.word2Byte(fixMsgHead, 12, s2);
        Util.word2Byte(fixMsgHead, 14, s3);
        Util.word2Byte(fixMsgHead, 16, s4);
        Util.word2Byte(fixMsgHead, 18, s5);
        Msg msg = new Msg(fixMsgHead, 20);
        msg.playerId = SyncData.curUin;
        msg.mobileTypeId = SyncData.MOBILE_TYPE_ID;
        msg.flag = 0;
        msg.optLen = 0;
        msg.msgId = Msg.MSG_ID_VIEWGAME;
        msg.msgType = 10;
        msg.sendIntervalTime = 500;
        msg.sendTimes = 2;
        Tools.debug("<<Sended Msg to veiw, tableID:" + ((int) s4) + ", seatID:" + ((int) s5));
        doSend(msg);
    }
}
