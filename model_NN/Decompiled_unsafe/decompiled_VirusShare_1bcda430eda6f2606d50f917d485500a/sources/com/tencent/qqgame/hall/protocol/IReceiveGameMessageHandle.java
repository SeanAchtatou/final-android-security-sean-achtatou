package com.tencent.qqgame.hall.protocol;

import android.os.Message;

public interface IReceiveGameMessageHandle {
    void parseGameMessage(short s, byte[] bArr);

    void receiveGameMessage(Message message);
}
