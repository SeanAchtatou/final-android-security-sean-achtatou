package com.tencent.qqgame.hall.protocol;

import android.os.Handler;
import android.os.Message;
import com.tencent.qqgame.hall.common.AActivity;
import com.tencent.qqgame.hall.common.FactoryCenter;
import com.tencent.qqgame.hall.common.SyncData;
import com.tencent.qqgame.hall.common.Tools;
import com.tencent.qqgame.hall.common.data.MProxyInfoV2;
import java.io.DataInputStream;

public class ReceiveHallHttpMsgHandle {
    private static final short CMD_START_SC_V2 = 4113;
    private static final short CMD_START_SC_V4 = 4123;
    public static final short MSG_ID_UUID_RESP = 4110;
    FactoryCenter factory;

    public ReceiveHallHttpMsgHandle(FactoryCenter factoryCenter) {
        this.factory = factoryCenter;
    }

    private String byteArray2String(int i, DataInputStream dataInputStream) {
        byte[] bArr;
        Exception exc;
        if (i <= 0 || dataInputStream == null) {
            return "";
        }
        try {
            byte[] bArr2 = new byte[i];
            int i2 = 0;
            while (i2 < i) {
                try {
                    bArr2[i2] = dataInputStream.readByte();
                    i2++;
                } catch (Exception e) {
                    Exception exc2 = e;
                    bArr = bArr2;
                    exc = exc2;
                    exc.printStackTrace();
                    try {
                        return new String(bArr, "UTF-8");
                    } catch (Exception e2) {
                        return new String(bArr);
                    }
                }
            }
            return new String(bArr2, "utf-8");
        } catch (Exception e3) {
            Exception exc3 = e3;
            bArr = null;
            exc = exc3;
            exc.printStackTrace();
            return new String(bArr, "UTF-8");
        }
    }

    private void unpackage_UUIDSC(short s, short s2, DataInputStream dataInputStream) {
        try {
            SyncData.UUID = byteArray2String(dataInputStream.readShort(), dataInputStream);
            AActivity.currentActivity.handle.sendEmptyMessage(13);
        } catch (Exception e) {
            AActivity.currentActivity.handle.sendEmptyMessage(9);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0031  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void parseMsg(short r6, byte[] r7) {
        /*
            r5 = this;
            r3 = 0
            if (r7 != 0) goto L_0x0004
        L_0x0003:
            return
        L_0x0004:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "parseHallHttpMsg, cmd: "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r6)
            java.lang.String r0 = r0.toString()
            com.tencent.qqgame.hall.common.Tools.debug(r0)
            java.io.ByteArrayInputStream r0 = new java.io.ByteArrayInputStream
            r0.<init>(r7)
            java.io.DataInputStream r1 = new java.io.DataInputStream
            r1.<init>(r0)
            short r0 = r1.readShort()     // Catch:{ Exception -> 0x0051 }
            short r2 = r1.readShort()     // Catch:{ Exception -> 0x0062 }
            r4 = r2
            r2 = r0
            r0 = r4
        L_0x002f:
            if (r2 == 0) goto L_0x0056
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "get Hall Msg error resultId:"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.tencent.qqgame.hall.common.Tools.debug(r0)
            com.tencent.qqgame.hall.common.AActivity r0 = com.tencent.qqgame.hall.common.AActivity.currentActivity
            android.os.Handler r0 = r0.handle
            r1 = 9
            r0.sendEmptyMessage(r1)
            goto L_0x0003
        L_0x0051:
            r0 = move-exception
            r0 = r3
        L_0x0053:
            r2 = r0
            r0 = r3
            goto L_0x002f
        L_0x0056:
            switch(r6) {
                case 4110: goto L_0x005a;
                case 4113: goto L_0x005e;
                case 4123: goto L_0x005e;
                default: goto L_0x0059;
            }
        L_0x0059:
            goto L_0x0003
        L_0x005a:
            r5.unpackage_UUIDSC(r2, r0, r1)
            goto L_0x0003
        L_0x005e:
            r5.start_V4(r2, r0, r1)
            goto L_0x0003
        L_0x0062:
            r2 = move-exception
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.hall.protocol.ReceiveHallHttpMsgHandle.parseMsg(short, byte[]):void");
    }

    public void start_V4(short s, short s2, DataInputStream dataInputStream) {
        Handler handler = AActivity.currentActivity.handle;
        try {
            int readInt = dataInputStream.readInt();
            int readInt2 = dataInputStream.readInt();
            Tools.debug("MinVersion:" + readInt + ", MaxVersion:" + readInt2);
            String byteArray2String = byteArray2String(dataInputStream.readShort(), dataInputStream);
            Tools.debug("MgHallBaseUrl: " + byteArray2String);
            SyncData.Proxy_Url = byteArray2String;
            SyncData.saveProxyUrl(AActivity.currentActivity.getApplicationContext());
            String byteArray2String2 = byteArray2String(dataInputStream.readShort(), dataInputStream);
            Tools.debug("MgHallDownUrl: " + byteArray2String2);
            byteArray2String(dataInputStream.readShort(), dataInputStream);
            byteArray2String(dataInputStream.readShort(), dataInputStream);
            short readShort = dataInputStream.readShort();
            if (400001 < readInt || 400001 > readInt2) {
                Tools.debug("current version is out! must update software");
                Message obtainMessage = handler.obtainMessage(14);
                obtainMessage.arg1 = 1;
                obtainMessage.obj = byteArray2String2;
                handler.sendMessage(obtainMessage);
            } else if (400001 < readInt || 400001 >= readInt2 || SyncData.isCanceledUpdateTips) {
                for (int i = 0; i < readShort; i++) {
                    short readShort2 = dataInputStream.readShort();
                    int readInt3 = dataInputStream.readInt();
                    System.out.println("cmd=" + ((int) readShort2) + ",TimeStamp=" + readInt3);
                    CmdTimeStampInfo[] cmdTimeStampInfoArr = SendHallHttpMsgHandle.StampInfo;
                    int i2 = 0;
                    while (true) {
                        if (i2 >= (cmdTimeStampInfoArr != null ? cmdTimeStampInfoArr.length : 0)) {
                            break;
                        } else if (cmdTimeStampInfoArr[i2].cmd != readShort2) {
                            i2++;
                        } else if (cmdTimeStampInfoArr[i2].timeStamp < readInt3) {
                            System.out.println(">>> 需要发起请求 >>> " + i2 + ",cmd =" + ((int) readShort2) + "," + cmdTimeStampInfoArr[i2].timeStamp + "," + readInt3);
                            cmdTimeStampInfoArr[i2].timeStamp = readInt3;
                            cmdTimeStampInfoArr[i2].needSendReq = true;
                        }
                    }
                }
                int readShort3 = dataInputStream.readShort();
                Tools.debug("MProxyInfosLength=" + readShort3);
                SyncData.proxyList.removeAllElements();
                String[] strArr = new String[readShort3];
                for (int i3 = 0; i3 < readShort3; i3++) {
                    short readShort4 = dataInputStream.readShort();
                    String byteArray2String3 = byteArray2String(dataInputStream.readByte(), dataInputStream);
                    short readShort5 = dataInputStream.readShort();
                    short readShort6 = dataInputStream.readShort();
                    byte readByte = dataInputStream.readByte();
                    String byteArray2String4 = byteArray2String(dataInputStream.readShort(), dataInputStream);
                    int readInt4 = dataInputStream.readInt();
                    strArr[i3] = byteArray2String3 + ":" + ((int) readShort5);
                    SyncData.proxyList.add(new MProxyInfoV2(readShort4, byteArray2String3, readShort5, readShort6, readByte, byteArray2String4, readInt4));
                    Tools.debug("SvrID =" + ((int) readShort4) + ",SvrIP=" + byteArray2String3 + ",SvrPort=" + ((int) readShort5) + ",OnlineNum=" + ((int) readShort6) + ",NetFlag=" + ((int) readByte) + ",SvrName=" + byteArray2String4 + ",supportedType =" + readInt4);
                }
                if (readShort3 > 0) {
                    handler.sendEmptyMessage(4);
                } else {
                    handler.sendEmptyMessage(9);
                }
                SyncData.serverGuessNetType = dataInputStream.readByte();
                dataInputStream.readInt();
            } else {
                Tools.debug("current version is out! need update software");
                Message obtainMessage2 = handler.obtainMessage(14);
                obtainMessage2.arg1 = 0;
                obtainMessage2.obj = byteArray2String2;
                handler.sendMessage(obtainMessage2);
            }
        } catch (Exception e) {
            handler.sendEmptyMessage(9);
        }
    }
}
