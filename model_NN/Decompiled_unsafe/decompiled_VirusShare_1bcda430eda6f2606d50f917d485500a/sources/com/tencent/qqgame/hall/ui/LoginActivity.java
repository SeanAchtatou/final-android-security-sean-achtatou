package com.tencent.qqgame.hall.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.qqgame.common.MD5;
import com.tencent.qqgame.common.Util;
import com.tencent.qqgame.hall.common.AActivity;
import com.tencent.qqgame.hall.common.AccountStore;
import com.tencent.qqgame.hall.common.Report;
import com.tencent.qqgame.hall.common.SyncData;
import com.tencent.qqgame.hall.common.Tools;
import com.tencent.qqgame.hall.common.UpdateVersion;
import com.tencent.qqgame.hall.ui.controls.ViewWrapperListAccount;
import java.util.Random;
import tencent.qqgame.lord.R;

public class LoginActivity extends AActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener, View.OnFocusChangeListener {
    private static final String ACCOUNT_STORE_NAME = "QQLordACC";
    private static final int DIALOG_LIST = -100;
    private static final int FLASH_TIME = 2000;
    public static final String KEY_PIC_DATA = "PicData";
    public static final String KEY_PIC_FORMAT = "PicFormat";
    public static final String KEY_STATUS = "CurStatus";
    private static final int MAX_ACCOUNT = 10;
    private static final int MAX_SPLISH_TIME = 2;
    private static final int MAX_UPDATE_SOFT_TIME = 180;
    private static final byte MAX_VERIFY_NUM = 7;
    public static final int MSG_GET_SERVER_ERROR = 9;
    public static final int MSG_GET_SERVER_SUCC = 4;
    public static final int MSG_GET_SERVER_UUID = 13;
    public static final int MSG_GET_VERIFYCODE_ERROR = 6;
    public static final int MSG_LOADING_HEART = 5;
    public static final int MSG_LOCAL_PROXY_TRYED_3 = 11;
    public static final int MSG_LOGIN_ERROR = 2;
    public static final int MSG_LOGIN_PLAY_ERROR = 8;
    public static final int MSG_LOGIN_PLAY_SUCC = 10;
    public static final int MSG_LOGIN_SUCC = 3;
    public static final int MSG_NEED_UPDATE_SOFT = 14;
    public static final int MSG_SERVER_PROXY_TRYED_3 = 12;
    public static final int MSG_UPDATE_SOFT_FAULT = 15;
    public static final int RESULT_FAULT = 10;
    public static final byte STATUS_FLASH = 0;
    public static final byte STATUS_MAIN = 1;
    public static final byte STATUS_NONE = -1;
    public static final byte STATUS_VERIFY = 6;
    public static final byte STATUS_WAIT = 5;
    private ImageButton accountList;
    /* access modifiers changed from: private */
    public AccountStore accountStore;
    /* access modifiers changed from: private */
    public String[] accounts;
    private Button btnLogin;
    private CheckBox cbAutoLogin;
    private CheckBox cbLoginPlay;
    private CheckBox cbSavePwd;
    /* access modifiers changed from: private */
    public AccountStore.AccountInfo curAccountInfo;
    /* access modifiers changed from: private */
    public byte curStatus = 0;
    /* access modifiers changed from: private */
    public AlertDialog dialog;
    private EditText editAccount;
    private EditText editPwd;
    /* access modifiers changed from: private */
    public byte firstStatus = 0;
    private boolean isVerifyRefresh = false;
    public Runnable loginTimeOutTask = new Runnable() {
        public void run() {
            if (LoginActivity.this.curStatus == 5) {
                LoginActivity.this.showHint(AActivity.res.getString(R.string.login_error_logintimeout));
                if (LoginActivity.this.firstStatus != 6) {
                    if (LoginActivity.factory.getCommunicator().isConnected()) {
                        LoginActivity.factory.getSendMsgHandle().logout();
                    }
                    LoginActivity.this.showContentView((byte) 1);
                } else if (LoginActivity.this.firstStatus == 6) {
                    AActivity.currentActivity.setResult(0);
                    AActivity.currentActivity.finish();
                }
            }
        }
    };
    private boolean needSendReport = false;
    private int timerSplish = -1;
    protected int timerUpdateSoftWare = -1;
    private TextView tvLoadingTopTip;
    protected UpdateVersion updater;
    private byte[] verifyImage_Data = null;
    private short verifyImage_format = 0;
    private byte verifyNums = 7;

    private class IconicAdapter extends ArrayAdapter<String> {
        Activity context;

        IconicAdapter(Activity activity) {
            super(activity, (int) R.layout.login_list_account_row, LoginActivity.this.accounts);
            this.context = activity;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewWrapperListAccount viewWrapperListAccount;
            View view2;
            if (view == null) {
                View inflate = this.context.getLayoutInflater().inflate((int) R.layout.login_list_account_row, (ViewGroup) null);
                ViewWrapperListAccount viewWrapperListAccount2 = new ViewWrapperListAccount(inflate);
                viewWrapperListAccount2.getButton().setTag(viewWrapperListAccount2.getLabel());
                viewWrapperListAccount2.getButton().setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        String obj = ((TextView) view.getTag()).getText().toString();
                        if (obj != null && obj.equals(LoginActivity.this.getAccount())) {
                            LoginActivity.this.curAccountInfo.restore();
                            LoginActivity.this.updateDataToUI(LoginActivity.this.curAccountInfo);
                        }
                        LoginActivity.this.accountStore.remove(obj);
                        LoginActivity.this.accountStore.commit();
                        LoginActivity.this.removeDialog(-100);
                    }
                });
                viewWrapperListAccount2.getLabel().setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        LoginActivity.this.fireOnSelectAccountList((TextView) view);
                    }
                });
                inflate.setTag(viewWrapperListAccount2);
                ViewWrapperListAccount viewWrapperListAccount3 = viewWrapperListAccount2;
                view2 = inflate;
                viewWrapperListAccount = viewWrapperListAccount3;
            } else {
                viewWrapperListAccount = (ViewWrapperListAccount) view.getTag();
                view2 = view;
            }
            viewWrapperListAccount.getLabel().setText(LoginActivity.this.getModel(i));
            return view2;
        }
    }

    private void autoLoginLogic() {
        if (!this.curAccountInfo.isAutoLogin() || !verify(this.curAccountInfo.getName(), this.curAccountInfo.getInputPwd(), true)) {
            showContentView((byte) 1);
            return;
        }
        showContentView((byte) 5);
        login(this.curAccountInfo.getInputPwd());
    }

    private void cancel() {
        showContentView((byte) 1);
        if (factory.getCommunicator().isConnected()) {
            factory.getSendMsgHandle().logout();
        }
    }

    private void exit() {
        finish();
    }

    /* access modifiers changed from: private */
    public void fireOnSelectAccountList(TextView textView) {
        if (textView != null) {
            this.curAccountInfo.copy(this.accountStore.get(textView.getText().toString()));
            if (!isFirstTimeLogin(this.curAccountInfo)) {
                this.curAccountInfo.setInputPwd(this.curAccountInfo.getSucceedPwd());
            }
            updateDataToUI(this.curAccountInfo);
            this.accountStore.pushAndUpdate(this.curAccountInfo);
            this.accountStore.commit();
            removeDialog(-100);
        }
    }

    /* access modifiers changed from: private */
    public String getModel(int i) {
        return (String) ((IconicAdapter) this.dialog.getListView().getAdapter()).getItem(i);
    }

    private void hideSoftKeyBoard(View view) {
        if (view != null) {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void initMainScene() {
        this.editAccount = (EditText) findViewById(R.id.login_edit_account);
        this.editPwd = (EditText) findViewById(R.id.login_edit_pwd);
        this.editPwd.setOnFocusChangeListener(this);
        this.editAccount.setText(this.curAccountInfo.getName());
        if (this.curAccountInfo.isSavedPwd()) {
            this.editPwd.setText(this.curAccountInfo.getInputPwd());
        } else {
            this.editPwd.setText("");
        }
        this.accountList = (ImageButton) findViewById(R.id.login_imgview_account_list);
        this.accountList.setOnClickListener(this);
        this.btnLogin = (Button) findViewById(R.id.login_btn_login);
        this.btnLogin.setOnClickListener(this);
        this.cbSavePwd = (CheckBox) findViewById(R.id.login_cb_savepwd);
        this.cbSavePwd.setChecked(this.curAccountInfo.isSavedPwd());
        this.cbSavePwd.setOnCheckedChangeListener(this);
        this.cbAutoLogin = (CheckBox) findViewById(R.id.login_cb_autologin);
        this.cbAutoLogin.setChecked(this.curAccountInfo.isAutoLogin());
        this.cbAutoLogin.setOnCheckedChangeListener(this);
        this.cbLoginPlay = (CheckBox) findViewById(R.id.login_cb_loginplay);
        this.cbLoginPlay.setChecked(this.curAccountInfo.isLoginPlay());
        this.cbLoginPlay.setOnCheckedChangeListener(this);
    }

    private boolean isFirstTimeLogin(AccountStore.AccountInfo accountInfo) {
        return accountInfo.getSucceedPwd().equals("");
    }

    private void setAccountFromUI(AccountStore.AccountInfo accountInfo) {
        if (this.editAccount != null && this.editPwd != null) {
            if (this.accountStore.search(this.editAccount.getText().toString()) < 0) {
                accountInfo.setSucceedPwd("");
            }
            accountInfo.setName(this.editAccount.getText().toString());
            accountInfo.setInputPwd(this.editPwd.getText().toString());
            accountInfo.setSavedPwd(this.cbSavePwd.isChecked());
            accountInfo.setAutoLogin(this.cbAutoLogin.isChecked());
            accountInfo.setLoginPlay(this.cbLoginPlay.isChecked());
        }
    }

    private void setLoadingTips() {
        TextView textView = (TextView) findViewById(R.id.loading_tips);
        if (textView != null) {
            switch (new Random().nextInt() % 3) {
                case 0:
                    textView.setText((int) R.string.adtips_0);
                    break;
                case 1:
                    textView.setText((int) R.string.adtips_1);
                    break;
                case 2:
                    textView.setText((int) R.string.adtips_2);
                    break;
            }
            textView.invalidate();
        }
    }

    private void showSoftKeyBoard(View view) {
        if (view != null) {
            view.requestFocus();
            ((InputMethodManager) getSystemService("input_method")).showSoftInput(view, 0);
        }
    }

    /* access modifiers changed from: private */
    public void updateDataToUI(AccountStore.AccountInfo accountInfo) {
        this.editAccount.setText(accountInfo.getName());
        if (accountInfo.isSavedPwd()) {
            this.editPwd.setText(accountInfo.getInputPwd());
        } else {
            this.editPwd.setText("");
        }
        this.cbSavePwd.setChecked(accountInfo.isSavedPwd());
        this.cbAutoLogin.setChecked(accountInfo.isAutoLogin());
        this.cbLoginPlay.setChecked(accountInfo.isLoginPlay());
    }

    private boolean verify(String str, String str2, boolean z) {
        if (str == null || str.length() == 0) {
            if (!z) {
                findViewById(R.id.login_edit_account).startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
                showHint(res.getString(R.string.login_no_account));
            }
            return false;
        } else if (str2 == null || str2.length() == 0) {
            if (!z) {
                showHint(res.getString(R.string.login_no_pwd));
                findViewById(R.id.login_edit_pwd).startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            }
            return false;
        } else {
            long checkQQNum = Util.checkQQNum(str);
            SyncData.curUin = checkQQNum;
            if (checkQQNum != -1 && Util.isQQPsw(str2)) {
                return true;
            }
            if (!z) {
                showHint(res.getString(R.string.game_error_login12));
            }
            return false;
        }
    }

    public String getAccount() {
        String obj = this.editAccount.getText().toString();
        return obj == null ? "" : obj;
    }

    public String getPassword() {
        String obj = this.editPwd.getText().toString();
        return obj == null ? "" : obj;
    }

    /* access modifiers changed from: protected */
    public void handleMessage(Message message) {
        switch (message.what) {
            case AActivity.MSG_LOGIN_REPLAY_FAULT:
                if (this.firstStatus == 6) {
                    setResult(10);
                    finish();
                    GameActivity.back2Game(this);
                    return;
                }
                return;
            case AActivity.MSG_LOGIN_REPLAY_SUCC:
                if (this.firstStatus == 6) {
                    setResult(-1);
                    finish();
                    GameActivity.back2Game(this);
                    return;
                }
                return;
            case AActivity.MSG_GET_VERIFYCODE_SUCC:
                if (this.curStatus != 1) {
                    if (!this.isVerifyRefresh) {
                        this.verifyNums = (byte) (this.verifyNums - 1);
                    }
                    if (this.verifyNums < 6 && !this.isVerifyRefresh) {
                        Toast makeText = Toast.makeText(this, (int) R.string.login_error_verifyCode1, 1);
                        makeText.setGravity(17, 0, -200);
                        makeText.show();
                    }
                    this.verifyImage_format = (short) message.arg1;
                    this.verifyImage_Data = (byte[]) message.obj;
                    showContentView((byte) 6);
                    return;
                }
                return;
            case AActivity.MSG_CONNECT_ERROR:
                if (this.curStatus == 5) {
                    showContentView((byte) 1);
                    return;
                }
                return;
            case AActivity.MSG_CONNECT_GAME_SUCC:
                if (this.curStatus == 5) {
                    this.tvLoadingTopTip.setText((int) R.string.login_logining);
                    SyncData.readReplayInfo(getApplicationContext());
                    factory.getSendMsgHandle().login(SyncData.curUin, SyncData.pswMD5, (SyncData.myZoneID <= -1 || SyncData.myRoomID <= -1 || SyncData.myRoomSvrID <= -1 || SyncData.myTableID <= -1 || SyncData.myTableID <= -1 || SyncData.myZoneName == null || SyncData.myRoomName == null) ? this.curAccountInfo.isLoginPlay() ? (short) 1 : 0 : 0);
                    return;
                }
                return;
            case AActivity.MSG_GLOABLE_SLEEP:
                if (this.timerSplish >= 0) {
                    this.timerSplish++;
                    if (this.timerSplish > 2 && this.curStatus == 0) {
                        autoLoginLogic();
                        this.timerSplish = -1;
                    }
                }
                if (this.timerUpdateSoftWare >= 0) {
                    this.timerUpdateSoftWare++;
                    if (this.timerUpdateSoftWare > MAX_UPDATE_SOFT_TIME) {
                        this.timerUpdateSoftWare = -1;
                        new AlertDialog.Builder(this).setTitle((int) R.string.game_tips_title).setMessage(res.getString(R.string.login_error_update_fault)).setCancelable(false).setPositiveButton((int) R.string.menu_exit, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Process.killProcess(Process.myPid());
                            }
                        });
                    }
                }
                this.handle.sendMessageDelayed(this.handle.obtainMessage(AActivity.MSG_GLOABLE_SLEEP), 1000);
                return;
            case 2:
                if (this.curStatus == 5) {
                    setAccountFromUI(this.curAccountInfo);
                    if (isFirstTimeLogin(this.curAccountInfo)) {
                        this.curAccountInfo.setInputPwd("");
                        this.curAccountInfo.setSavedPwd(false);
                        this.curAccountInfo.setAutoLogin(false);
                    } else {
                        this.curAccountInfo.setAutoLogin(false);
                    }
                    this.accountStore.pushAndUpdate(this.curAccountInfo);
                    this.accountStore.commit();
                    reLogin();
                    showHint(message.obj.toString());
                    return;
                }
                return;
            case 3:
            case 8:
                if (this.curStatus == 5) {
                    if (message.what == 8) {
                        Toast.makeText(this, (int) R.string.login_error_loginplay, 1).show();
                    }
                    SyncData.isLogined = true;
                    this.curAccountInfo.setSucceedPwd(this.curAccountInfo.getInputPwd());
                    this.accountStore.pushAndUpdate(this.curAccountInfo);
                    this.accountStore.commit();
                    Intent intent = new Intent(this, HallListActivity.class);
                    intent.putExtras((Bundle) message.obj);
                    startActivity(intent);
                    return;
                }
                factory.getSendMsgHandle().logout();
                return;
            case 4:
                if (this.curStatus == 5) {
                    this.tvLoadingTopTip.setText((int) R.string.login_connecting_game_server);
                    SyncData.writeProxyList(getApplicationContext());
                    SyncData.isCurrProxyFromServer = true;
                    factory.getCommunicator().connect(true);
                }
                SyncData.UUID = SyncData.loadUUID(this);
                if (SyncData.UUID.equals("") || SyncData.UUID.trim().length() == 0 || SyncData.UUID.equals("0000000")) {
                    factory.getSendHallHttpMsgHandle().uuidReq();
                    return;
                } else if (this.needSendReport) {
                    Log.v("sendedreport", Report.loadReport());
                    factory.getSendHallHttpMsgHandle().sendReport(Report.loadReport());
                    this.needSendReport = false;
                    return;
                } else {
                    return;
                }
            case 5:
                setLoadingTips();
                this.handle.sendMessageDelayed(this.handle.obtainMessage(5), 1000);
                return;
            case 6:
                if (this.curStatus != 1 || this.firstStatus == 6) {
                    hideSoftKeyBoard((EditText) findViewById(R.id.login_verify_editText_verfiyInput));
                    Toast.makeText(this, message.obj.toString(), 1).show();
                    factory.getSendMsgHandle().logout();
                    showContentView((byte) 1);
                    return;
                }
                return;
            case 9:
                if (this.curStatus != 5) {
                    return;
                }
                if (SyncData.isTryLocalProxyList3) {
                    showExitDialog();
                    return;
                }
                this.tvLoadingTopTip.setText((int) R.string.login_readlocalservers);
                SyncData.readProxyList(getApplicationContext());
                this.tvLoadingTopTip.setText((int) R.string.login_connecting_game_server);
                SyncData.isCurrProxyFromServer = false;
                factory.getCommunicator().connect(true);
                return;
            case 10:
                if (this.curStatus == 5) {
                    SyncData.isLogined = true;
                    Intent intent2 = new Intent(this, GameActivity.class);
                    intent2.putExtra(GameActivity.KEY_GAME_FROM, (byte) 3);
                    intent2.putExtras((Bundle) message.obj);
                    startActivityIfNeeded(intent2, 0);
                    return;
                }
                factory.getSendMsgHandle().standUp();
                factory.getSendMsgHandle().logout();
                return;
            case 11:
                if (this.curStatus == 5) {
                    this.tvLoadingTopTip.setText((int) R.string.login_re_getservers);
                    factory.getSendHallHttpMsgHandle().start_V4();
                    return;
                }
                return;
            case 12:
                if (this.curStatus == 5) {
                    showExitDialog();
                    return;
                }
                return;
            case 13:
                SyncData.saveUUID(this);
                return;
            case 14:
                final String str = (String) message.obj;
                boolean z = message.arg1 != 0;
                AlertDialog.Builder positiveButton = new AlertDialog.Builder(this).setTitle((int) R.string.game_tips_title).setMessage(z ? res.getString(R.string.login_error_must_update) : res.getString(R.string.login_error_need_update)).setCancelable(!z).setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialogInterface) {
                        SyncData.isCanceledUpdateTips = true;
                        LoginActivity.this.login(null);
                    }
                }).setPositiveButton((int) R.string.cmd_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (LoginActivity.this.updater == null) {
                            LoginActivity.this.updater = new UpdateVersion(AActivity.currentActivity, str);
                        }
                        LoginActivity.this.updater.update();
                        LoginActivity.this.timerUpdateSoftWare = 0;
                        LoginActivity.this.showDialog(0, AActivity.res.getString(R.string.login_update_ing));
                    }
                });
                if (!z) {
                    positiveButton.setNegativeButton((int) R.string.cmd_cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            SyncData.isCanceledUpdateTips = true;
                            LoginActivity.this.login(null);
                        }
                    });
                }
                positiveButton.show();
                return;
            case 15:
                if (this.timerUpdateSoftWare >= 0) {
                    this.timerUpdateSoftWare = -1;
                    removeDialog(this.curr_dialog_key);
                    Toast.makeText(this, (int) R.string.login_error_update_fault, 1).show();
                    showContentView((byte) 1);
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void login(String str) {
        if (str != null) {
            SyncData.pswMD5 = MD5.toMD5(MD5.toMD5(str));
        } else if (SyncData.pswMD5 == null) {
            Tools.debug("login, but password is null");
            return;
        }
        this.tvLoadingTopTip.setText((int) R.string.login_getservers);
        factory.getSendHallHttpMsgHandle().start_V4();
    }

    public void onCancel(DialogInterface dialogInterface) {
        if (this.curr_dialog_key != 0 || this.timerUpdateSoftWare < 0) {
            super.onCancel(dialogInterface);
            return;
        }
        showContentView((byte) 1);
        this.timerUpdateSoftWare = -1;
        if (this.updater != null) {
            this.updater.interruptUpdate();
        }
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (this.dialog == null || !this.dialog.isShowing()) {
            if (compoundButton.getId() == R.id.login_cb_savepwd) {
                Report.savePwdHitNum++;
                if (this.curAccountInfo.isSavedPwd() || !(getAccount().length() == 0 || getPassword().length() == 0)) {
                    this.curAccountInfo.setSavedPwd(z);
                    if (!z) {
                        this.curAccountInfo.setSavedPwd(z);
                        this.curAccountInfo.setAutoLogin(z);
                    }
                } else {
                    this.curAccountInfo.setSavedPwd(false);
                    this.curAccountInfo.setAutoLogin(false);
                }
            } else if (compoundButton.getId() == R.id.login_cb_autologin) {
                Report.autoLoginHitNum++;
                if (!this.curAccountInfo.isSavedPwd()) {
                    this.curAccountInfo.setAutoLogin(false);
                } else {
                    this.curAccountInfo.setAutoLogin(z);
                    if (!z) {
                        this.curAccountInfo.setAutoLogin(z);
                    }
                }
            } else if (compoundButton.getId() == R.id.login_cb_loginplay) {
                Report.quickGameHitNum++;
            }
            this.cbSavePwd.setChecked(this.curAccountInfo.isSavedPwd());
            this.cbAutoLogin.setChecked(this.curAccountInfo.isAutoLogin());
            this.curAccountInfo.setSavedPwd(this.cbSavePwd.isChecked());
            this.curAccountInfo.setAutoLogin(this.cbAutoLogin.isChecked());
            this.curAccountInfo.setLoginPlay(this.cbLoginPlay.isChecked());
            this.accountStore.pushAndUpdate(this.curAccountInfo);
            this.accountStore.commit();
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_loading_btn_ok /*2131296272*/:
                Report.cancelLoginHitNum++;
                if (this.firstStatus == 6) {
                    if (this.firstStatus == 6) {
                        setResult(0);
                    }
                    finish();
                    return;
                }
                if (isFirstTimeLogin(this.curAccountInfo)) {
                    this.curAccountInfo.setInputPwd("");
                    this.curAccountInfo.setAutoLogin(false);
                    this.accountStore.pushAndUpdate(this.curAccountInfo);
                    this.accountStore.commit();
                }
                cancel();
                return;
            case R.id.login_imgview_account_list /*2131296274*/:
                Report.historyIconHitNum++;
                this.accounts = this.accountStore.toAccountArray();
                removeDialog(-100);
                showDialog(-100);
                return;
            case R.id.login_btn_login /*2131296276*/:
                if (verify(getAccount(), getPassword(), false)) {
                    hideSoftKeyBoard(this.editPwd);
                    setAccountFromUI(this.curAccountInfo);
                    this.accountStore.pushAndUpdate(this.curAccountInfo);
                    this.accountStore.commit();
                    Report.historyAccountNum = this.accountStore.getAccountSize();
                    showContentView((byte) 5);
                    login(getPassword());
                    disConnectHandler.removeCallbacks(this.loginTimeOutTask);
                    disConnectHandler.postDelayed(this.loginTimeOutTask, 60000);
                    return;
                }
                return;
            case R.id.login_verify_btn_cancel /*2131296292*/:
                hideSoftKeyBoard((EditText) findViewById(R.id.login_verify_editText_verfiyInput));
                if (this.firstStatus == 6) {
                    finish();
                    return;
                } else {
                    cancel();
                    return;
                }
            case R.id.login_verify_btn_refresh /*2131296293*/:
                this.isVerifyRefresh = true;
                factory.getSendMsgHandle().verifyCode(1, null);
                ((Button) findViewById(R.id.login_verify_btn_ok)).setEnabled(false);
                ((Button) findViewById(R.id.login_verify_btn_refresh)).setEnabled(false);
                return;
            case R.id.login_verify_btn_ok /*2131296294*/:
                EditText editText = (EditText) findViewById(R.id.login_verify_editText_verfiyInput);
                if (editText == null || editText.getText().toString().length() == 0) {
                    Toast makeText = Toast.makeText(this, (int) R.string.login_no_verifyCode, 1);
                    makeText.setGravity(17, 0, -200);
                    makeText.show();
                    return;
                } else if (editText.getText().length() != 4) {
                    Toast makeText2 = Toast.makeText(this, (int) R.string.login_error_verifyCode, 1);
                    makeText2.setGravity(17, 0, -200);
                    makeText2.show();
                    return;
                } else {
                    factory.getSendMsgHandle().verifyCode(0, editText.getText().toString());
                    hideSoftKeyBoard(editText);
                    showContentView((byte) 5);
                    this.tvLoadingTopTip.setText((int) R.string.login_verify_ing);
                    disConnectHandler.removeCallbacks(this.loginTimeOutTask);
                    disConnectHandler.postDelayed(this.loginTimeOutTask, 60000);
                    return;
                }
            default:
                return;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (getResources().getConfiguration().orientation == 2) {
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = getIntent();
        this.firstStatus = intent.getByteExtra(KEY_STATUS, (byte) 0);
        this.curStatus = this.firstStatus;
        if (this.firstStatus == 0) {
            this.timerSplish = 0;
            this.needSendReport = true;
            Report.saveReport(this.firstStatus);
            Report.resetReport();
        }
        this.accountStore = new AccountStore(this, ACCOUNT_STORE_NAME, 10);
        this.accountStore.open();
        this.curAccountInfo = new AccountStore.AccountInfo();
        this.curAccountInfo.copy(this.accountStore.peek());
        UpdateVersion.deleteDownloadFile(this);
        if (this.curStatus == 6) {
            this.verifyImage_format = intent.getShortExtra("PicFormat", 0);
            this.verifyImage_Data = intent.getByteArrayExtra("PicData");
        }
        showContentView(this.curStatus);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        if (i != -100) {
            return super.onCreateDialog(i);
        }
        this.dialog = new AlertDialog.Builder(this).setTitle(res.getString(R.string.login_selectAccount)).setNegativeButton(res.getString(R.string.cmd_cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                LoginActivity.this.removeDialog(-100);
            }
        }).setAdapter(new IconicAdapter(this), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        }).setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (dialogInterface != LoginActivity.this.dialog || i != 23) {
                    return false;
                }
                TextView textView = (TextView) ((AlertDialog) dialogInterface).getListView().getSelectedView().findViewById(R.id.tv_account_name);
                if (textView != null) {
                    LoginActivity.this.fireOnSelectAccountList(textView);
                }
                return true;
            }
        }).create();
        return this.dialog;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.createGeneralMenu(menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.firstStatus == 0) {
            if (factory.getCommunicator().isConnected()) {
                factory.getSendMsgHandle().logout();
            }
            factory._comm = null;
            if (soundPlayer != null) {
                soundPlayer.stopBackGroundSound(true);
            }
            this.accountStore.commit();
            this.accountStore.close();
        }
        super.onDestroy();
    }

    public void onFocusChange(View view, boolean z) {
        if (view.getId() == R.id.login_edit_pwd && z) {
            this.editPwd.selectAll();
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        switch (i) {
            case 4:
                Report.keyBackHitInHallNum++;
                if (this.firstStatus == 6) {
                    setResult(0);
                    finish();
                    break;
                } else {
                    if (this.dialog != null && this.dialog.isShowing()) {
                        removeDialog(-100);
                    }
                    if (this.curStatus == 5 || this.curStatus == 6 || this.curStatus == 0) {
                        if (this.curStatus == 6 || this.curStatus == 5) {
                            factory.getSendMsgHandle().logout();
                        }
                        showContentView((byte) 1);
                        return true;
                    }
                }
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        return super.onOptionsItemSelected(menuItem);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.curStatus = -1;
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        showContentView((byte) 1);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.handle.sendMessageDelayed(this.handle.obtainMessage(AActivity.MSG_GLOABLE_SLEEP), 1000);
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        super.onTouchEvent(motionEvent);
        if (this.curStatus != 0) {
            return false;
        }
        this.timerSplish = -1;
        autoLoginLogic();
        return false;
    }

    public void reLogin() {
        showContentView((byte) 1);
    }

    public void showContentView(byte b) {
        this.curStatus = b;
        if (b == 1) {
            setContentView((int) R.layout.layout_login);
            initMainScene();
            this.verifyNums = 7;
        } else if (b == 0) {
            setContentView((int) R.layout.layout_startup);
        } else if (b == 5) {
            setContentView((int) R.layout.layout_loading);
            this.tvLoadingTopTip = (TextView) findViewById(R.id.tvLoadingTopTip);
            ImageView imageView = (ImageView) findViewById(R.id.login_ani_loading);
            imageView.setBackgroundResource(R.anim.loading);
            ((AnimationDrawable) imageView.getBackground()).start();
            TextView textView = (TextView) findViewById(R.id.loading_text_qq);
            textView.setText(this.curAccountInfo.getName());
            textView.invalidate();
            setLoadingTips();
            ((Button) findViewById(R.id.login_loading_btn_ok)).setOnClickListener(this);
            this.handle.sendMessageDelayed(this.handle.obtainMessage(5), 1000);
        } else if (b == 6) {
            setContentView((int) R.layout.layout_verify);
            this.isVerifyRefresh = false;
            ImageView imageView2 = (ImageView) findViewById(R.id.login_verify_imgView_verifyCode);
            Bitmap bitmap = null;
            if (this.verifyImage_Data != null) {
                try {
                    bitmap = BitmapFactory.decodeByteArray(this.verifyImage_Data, 0, this.verifyImage_Data.length);
                } catch (Exception e) {
                }
            }
            imageView2.setImageBitmap(bitmap);
            ((Button) findViewById(R.id.login_verify_btn_refresh)).setOnClickListener(this);
            ((Button) findViewById(R.id.login_verify_btn_ok)).setOnClickListener(this);
            ((Button) findViewById(R.id.login_verify_btn_cancel)).setOnClickListener(this);
            showSoftKeyBoard((EditText) findViewById(R.id.login_verify_editText_verfiyInput));
        }
        setBackgroudDrawable(res, (ViewGroup) findViewById(R.id.lTopLayout));
    }

    public void showHint(String str) {
        showHint(str, 17, 0, -45);
    }

    public void showHint(String str, int i, int i2, int i3) {
        Toast makeText = Toast.makeText(this, str, 0);
        makeText.setGravity(i, i2, i3);
        makeText.show();
    }
}
