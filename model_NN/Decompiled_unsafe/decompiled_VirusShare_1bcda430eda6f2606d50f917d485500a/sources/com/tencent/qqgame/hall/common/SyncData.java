package com.tencent.qqgame.hall.common;

import android.content.Context;
import android.content.SharedPreferences;
import com.tencent.qqgame.common.Player;
import com.tencent.qqgame.common.Table;
import com.tencent.qqgame.hall.common.data.MProxyInfoV2;
import java.util.Vector;

public class SyncData {
    public static final byte GAME_LORD = 1;
    public static final byte MAX_HEART = 25;
    public static final byte MAX_RECONNECT_TIMES = 3;
    public static final short MOBILE_TYPE_ID = 21171;
    public static final int PLATFROM_ID = 12000000;
    public static String Proxy_Url = null;
    public static final short QUICKPLAY_CHANGEDESK = 4;
    public static final short QUICKPLAY_NORMAL = 0;
    public static final String SERVER_IP_CMNET = "219.133.41.176";
    public static final String SERVER_IP_CMWAP = "219.133.41.176";
    public static final String SERVER_IP_DEFAULT = "219.133.41.176";
    public static final String SERVER_IP_WIFI = "119.147.7.30";
    public static short SERVER_PORT_CMNET = 6666;
    public static short SERVER_PORT_CMWAP = 6666;
    public static short SERVER_PORT_DEFAULT = 6666;
    public static short SERVER_PORT_WIFI = 6666;
    public static String UA_Device = " ";
    public static final byte USER_TYPEID = -120;
    public static String UUID = "0000000";
    public static final int VERSION_P = 1609;
    public static final int VERSION_S = 400001;
    public static byte continue_reconnect_times = 0;
    public static long curUin = 0;
    public static byte currGame = 1;
    public static String deviceInfo = null;
    private static final String fileProxyList = "proxy.data";
    private static final String fileReplayInfo = "replay.data";
    public static short gameType = 69;
    public static boolean isCanceledUpdateTips = false;
    public static boolean isCurrProxyFromServer = true;
    public static boolean isLogined = false;
    public static volatile boolean isReconnecting = false;
    public static boolean isTryLocalProxyList3 = false;
    public static short lastQuickPlayMode = 0;
    public static String matchCardFileName = "";
    public static byte maxPlayerOfTable = 3;
    public static Player me;
    public static short myRoomID = -1;
    public static String myRoomName = null;
    public static short myRoomSvrID = -1;
    public static short mySeatID = 0;
    public static Table myTable = null;
    public static short myTableID = -1;
    public static short myZoneID = -1;
    public static String myZoneName = null;
    public static final Vector<MProxyInfoV2> proxyList = new Vector<>();
    public static byte[] pswMD5 = null;
    public static int serverGuessNetType = -1;
    public static volatile byte timerHeart = -1;
    public static byte timerNetSpeedMayBe = -1;
    public static int timerTryLocalProxyList = 0;
    public static int timerTryServerProxyList = 0;
    FactoryCenter factory;
    public byte quickPlayNum = 0;

    public SyncData(FactoryCenter factoryCenter) {
        this.factory = factoryCenter;
    }

    public static String loadProxyUrl(Context context) {
        Proxy_Url = context.getSharedPreferences("ProxyUrl", 0).getString("KEY_ProxyUrl", Proxy_Url);
        return Proxy_Url;
    }

    public static String loadUUID(Context context) {
        UUID = context.getSharedPreferences("UUID", 0).getString("KEY_UUID", UUID);
        return UUID;
    }

    /* JADX WARN: Type inference failed for: r0v20, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00ca A[SYNTHETIC, Splitter:B:29:0x00ca] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00cf A[Catch:{ Exception -> 0x00d3 }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00d9 A[SYNTHETIC, Splitter:B:37:0x00d9] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00de A[Catch:{ Exception -> 0x00e2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:57:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void readProxyList(android.content.Context r15) {
        /*
            r2 = 0
            r12 = 0
            java.lang.String r14 = "219.133.41.176"
            java.lang.String r13 = "fixed"
            r0 = 0
            java.lang.String r1 = "proxy.data"
            java.io.FileInputStream r8 = r15.openFileInput(r1)     // Catch:{ Exception -> 0x00c3, all -> 0x00d5 }
            if (r8 != 0) goto L_0x001a
            if (r8 == 0) goto L_0x0014
            r8.close()     // Catch:{ Exception -> 0x00f9 }
        L_0x0014:
            if (r2 == 0) goto L_0x0019
            r0.close()     // Catch:{ Exception -> 0x00f9 }
        L_0x0019:
            return
        L_0x001a:
            java.io.DataInputStream r9 = new java.io.DataInputStream     // Catch:{ Exception -> 0x00ee, all -> 0x00e4 }
            r9.<init>(r8)     // Catch:{ Exception -> 0x00ee, all -> 0x00e4 }
            java.util.Vector<com.tencent.qqgame.hall.common.data.MProxyInfoV2> r0 = com.tencent.qqgame.hall.common.SyncData.proxyList     // Catch:{ Exception -> 0x00f2, all -> 0x00e8 }
            r0.clear()     // Catch:{ Exception -> 0x00f2, all -> 0x00e8 }
            byte r10 = r9.readByte()     // Catch:{ Exception -> 0x00f2, all -> 0x00e8 }
            r11 = r12
        L_0x0029:
            if (r11 >= r10) goto L_0x0055
            short r1 = r9.readShort()     // Catch:{ Exception -> 0x00f2, all -> 0x00e8 }
            java.lang.String r2 = r9.readUTF()     // Catch:{ Exception -> 0x00f2, all -> 0x00e8 }
            short r3 = r9.readShort()     // Catch:{ Exception -> 0x00f2, all -> 0x00e8 }
            short r4 = r9.readShort()     // Catch:{ Exception -> 0x00f2, all -> 0x00e8 }
            byte r5 = r9.readByte()     // Catch:{ Exception -> 0x00f2, all -> 0x00e8 }
            java.lang.String r6 = r9.readUTF()     // Catch:{ Exception -> 0x00f2, all -> 0x00e8 }
            int r7 = r9.readInt()     // Catch:{ Exception -> 0x00f2, all -> 0x00e8 }
            com.tencent.qqgame.hall.common.data.MProxyInfoV2 r0 = new com.tencent.qqgame.hall.common.data.MProxyInfoV2     // Catch:{ Exception -> 0x00f2, all -> 0x00e8 }
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x00f2, all -> 0x00e8 }
            java.util.Vector<com.tencent.qqgame.hall.common.data.MProxyInfoV2> r1 = com.tencent.qqgame.hall.common.SyncData.proxyList     // Catch:{ Exception -> 0x00f2, all -> 0x00e8 }
            r1.add(r0)     // Catch:{ Exception -> 0x00f2, all -> 0x00e8 }
            int r0 = r11 + 1
            r11 = r0
            goto L_0x0029
        L_0x0055:
            if (r8 == 0) goto L_0x005a
            r8.close()     // Catch:{ Exception -> 0x00f6 }
        L_0x005a:
            if (r9 == 0) goto L_0x005f
            r9.close()     // Catch:{ Exception -> 0x00f6 }
        L_0x005f:
            java.util.Vector<com.tencent.qqgame.hall.common.data.MProxyInfoV2> r0 = com.tencent.qqgame.hall.common.SyncData.proxyList
            int r0 = r0.size()
            if (r0 > 0) goto L_0x0019
            com.tencent.qqgame.hall.common.data.MProxyInfoV2 r0 = new com.tencent.qqgame.hall.common.data.MProxyInfoV2
            java.lang.String r2 = "119.147.7.30"
            short r3 = com.tencent.qqgame.hall.common.SyncData.SERVER_PORT_WIFI
            java.lang.String r1 = "fixed"
            r7 = 2
            r1 = r12
            r4 = r12
            r5 = r12
            r6 = r13
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            java.util.Vector<com.tencent.qqgame.hall.common.data.MProxyInfoV2> r1 = com.tencent.qqgame.hall.common.SyncData.proxyList
            r1.add(r0)
            com.tencent.qqgame.hall.common.data.MProxyInfoV2 r0 = new com.tencent.qqgame.hall.common.data.MProxyInfoV2
            java.lang.String r1 = "219.133.41.176"
            short r3 = com.tencent.qqgame.hall.common.SyncData.SERVER_PORT_CMNET
            java.lang.String r1 = "fixed"
            r7 = 44
            r1 = r12
            r2 = r14
            r4 = r12
            r5 = r12
            r6 = r13
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            java.util.Vector<com.tencent.qqgame.hall.common.data.MProxyInfoV2> r1 = com.tencent.qqgame.hall.common.SyncData.proxyList
            r1.add(r0)
            com.tencent.qqgame.hall.common.data.MProxyInfoV2 r0 = new com.tencent.qqgame.hall.common.data.MProxyInfoV2
            java.lang.String r1 = "219.133.41.176"
            short r3 = com.tencent.qqgame.hall.common.SyncData.SERVER_PORT_CMWAP
            java.lang.String r1 = "fixed"
            r7 = 81
            r1 = r12
            r2 = r14
            r4 = r12
            r5 = r12
            r6 = r13
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            java.util.Vector<com.tencent.qqgame.hall.common.data.MProxyInfoV2> r1 = com.tencent.qqgame.hall.common.SyncData.proxyList
            r1.add(r0)
            com.tencent.qqgame.hall.common.data.MProxyInfoV2 r0 = new com.tencent.qqgame.hall.common.data.MProxyInfoV2
            java.lang.String r1 = "219.133.41.176"
            short r3 = com.tencent.qqgame.hall.common.SyncData.SERVER_PORT_DEFAULT
            java.lang.String r1 = "fixed"
            r7 = 128(0x80, float:1.794E-43)
            r1 = r12
            r2 = r14
            r4 = r12
            r5 = r12
            r6 = r13
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            java.util.Vector<com.tencent.qqgame.hall.common.data.MProxyInfoV2> r1 = com.tencent.qqgame.hall.common.SyncData.proxyList
            r1.add(r0)
            goto L_0x0019
        L_0x00c3:
            r0 = move-exception
            r1 = r2
        L_0x00c5:
            r0.printStackTrace()     // Catch:{ all -> 0x00ec }
            if (r2 == 0) goto L_0x00cd
            r2.close()     // Catch:{ Exception -> 0x00d3 }
        L_0x00cd:
            if (r1 == 0) goto L_0x005f
            r1.close()     // Catch:{ Exception -> 0x00d3 }
            goto L_0x005f
        L_0x00d3:
            r0 = move-exception
            goto L_0x005f
        L_0x00d5:
            r0 = move-exception
            r1 = r2
        L_0x00d7:
            if (r2 == 0) goto L_0x00dc
            r2.close()     // Catch:{ Exception -> 0x00e2 }
        L_0x00dc:
            if (r1 == 0) goto L_0x00e1
            r1.close()     // Catch:{ Exception -> 0x00e2 }
        L_0x00e1:
            throw r0
        L_0x00e2:
            r1 = move-exception
            goto L_0x00e1
        L_0x00e4:
            r0 = move-exception
            r1 = r2
            r2 = r8
            goto L_0x00d7
        L_0x00e8:
            r0 = move-exception
            r1 = r9
            r2 = r8
            goto L_0x00d7
        L_0x00ec:
            r0 = move-exception
            goto L_0x00d7
        L_0x00ee:
            r0 = move-exception
            r1 = r2
            r2 = r8
            goto L_0x00c5
        L_0x00f2:
            r0 = move-exception
            r1 = r9
            r2 = r8
            goto L_0x00c5
        L_0x00f6:
            r0 = move-exception
            goto L_0x005f
        L_0x00f9:
            r0 = move-exception
            goto L_0x0019
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.hall.common.SyncData.readProxyList(android.content.Context):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x00c6 A[SYNTHETIC, Splitter:B:24:0x00c6] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00cb A[Catch:{ Exception -> 0x00cf }] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00d5 A[SYNTHETIC, Splitter:B:32:0x00d5] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00da A[Catch:{ Exception -> 0x00de }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void readReplayInfo(android.content.Context r4) {
        /*
            r2 = 0
            r1 = -1
            r0 = 0
            com.tencent.qqgame.hall.common.SyncData.myZoneID = r1
            com.tencent.qqgame.hall.common.SyncData.myRoomSvrID = r1
            com.tencent.qqgame.hall.common.SyncData.myRoomID = r1
            com.tencent.qqgame.hall.common.SyncData.myTableID = r1
            com.tencent.qqgame.hall.common.SyncData.mySeatID = r1
            com.tencent.qqgame.hall.common.SyncData.myZoneName = r2
            com.tencent.qqgame.hall.common.SyncData.myRoomName = r2
            java.lang.String r1 = "replay.data"
            java.io.FileInputStream r1 = r4.openFileInput(r1)     // Catch:{ Exception -> 0x00bf, all -> 0x00d1 }
            if (r1 != 0) goto L_0x0024
            if (r1 == 0) goto L_0x001e
            r1.close()     // Catch:{ Exception -> 0x00fb }
        L_0x001e:
            if (r2 == 0) goto L_0x0023
            r0.close()     // Catch:{ Exception -> 0x00fb }
        L_0x0023:
            return
        L_0x0024:
            java.io.DataInputStream r0 = new java.io.DataInputStream     // Catch:{ Exception -> 0x00ed, all -> 0x00e0 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x00ed, all -> 0x00e0 }
            short r2 = r0.readShort()     // Catch:{ Exception -> 0x00f2, all -> 0x00e5 }
            com.tencent.qqgame.hall.common.SyncData.myZoneID = r2     // Catch:{ Exception -> 0x00f2, all -> 0x00e5 }
            short r2 = r0.readShort()     // Catch:{ Exception -> 0x00f2, all -> 0x00e5 }
            com.tencent.qqgame.hall.common.SyncData.myRoomSvrID = r2     // Catch:{ Exception -> 0x00f2, all -> 0x00e5 }
            short r2 = r0.readShort()     // Catch:{ Exception -> 0x00f2, all -> 0x00e5 }
            com.tencent.qqgame.hall.common.SyncData.myRoomID = r2     // Catch:{ Exception -> 0x00f2, all -> 0x00e5 }
            short r2 = r0.readShort()     // Catch:{ Exception -> 0x00f2, all -> 0x00e5 }
            com.tencent.qqgame.hall.common.SyncData.myTableID = r2     // Catch:{ Exception -> 0x00f2, all -> 0x00e5 }
            short r2 = r0.readShort()     // Catch:{ Exception -> 0x00f2, all -> 0x00e5 }
            com.tencent.qqgame.hall.common.SyncData.mySeatID = r2     // Catch:{ Exception -> 0x00f2, all -> 0x00e5 }
            java.lang.String r2 = r0.readUTF()     // Catch:{ Exception -> 0x00f2, all -> 0x00e5 }
            com.tencent.qqgame.hall.common.SyncData.myZoneName = r2     // Catch:{ Exception -> 0x00f2, all -> 0x00e5 }
            java.lang.String r2 = r0.readUTF()     // Catch:{ Exception -> 0x00f2, all -> 0x00e5 }
            com.tencent.qqgame.hall.common.SyncData.myRoomName = r2     // Catch:{ Exception -> 0x00f2, all -> 0x00e5 }
            if (r1 == 0) goto L_0x0058
            r1.close()     // Catch:{ Exception -> 0x00f8 }
        L_0x0058:
            if (r0 == 0) goto L_0x005d
            r0.close()     // Catch:{ Exception -> 0x00f8 }
        L_0x005d:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "readReplayInfo myZoneID:"
            java.lang.StringBuilder r0 = r0.append(r1)
            short r1 = com.tencent.qqgame.hall.common.SyncData.myZoneID
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = ",myRoomSvrID:"
            java.lang.StringBuilder r0 = r0.append(r1)
            short r1 = com.tencent.qqgame.hall.common.SyncData.myRoomSvrID
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = ",myRoomID:"
            java.lang.StringBuilder r0 = r0.append(r1)
            short r1 = com.tencent.qqgame.hall.common.SyncData.myRoomID
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = ",myTableID:"
            java.lang.StringBuilder r0 = r0.append(r1)
            short r1 = com.tencent.qqgame.hall.common.SyncData.myTableID
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = ",mySeatID:"
            java.lang.StringBuilder r0 = r0.append(r1)
            short r1 = com.tencent.qqgame.hall.common.SyncData.mySeatID
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = ",myZoneName:"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = com.tencent.qqgame.hall.common.SyncData.myZoneName
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = ",myRoomName:"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = com.tencent.qqgame.hall.common.SyncData.myRoomName
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.tencent.qqgame.hall.common.Tools.debug(r0)
            goto L_0x0023
        L_0x00bf:
            r0 = move-exception
            r1 = r2
        L_0x00c1:
            r0.printStackTrace()     // Catch:{ all -> 0x00eb }
            if (r2 == 0) goto L_0x00c9
            r2.close()     // Catch:{ Exception -> 0x00cf }
        L_0x00c9:
            if (r1 == 0) goto L_0x005d
            r1.close()     // Catch:{ Exception -> 0x00cf }
            goto L_0x005d
        L_0x00cf:
            r0 = move-exception
            goto L_0x005d
        L_0x00d1:
            r0 = move-exception
            r1 = r2
        L_0x00d3:
            if (r2 == 0) goto L_0x00d8
            r2.close()     // Catch:{ Exception -> 0x00de }
        L_0x00d8:
            if (r1 == 0) goto L_0x00dd
            r1.close()     // Catch:{ Exception -> 0x00de }
        L_0x00dd:
            throw r0
        L_0x00de:
            r1 = move-exception
            goto L_0x00dd
        L_0x00e0:
            r0 = move-exception
            r3 = r2
            r2 = r1
            r1 = r3
            goto L_0x00d3
        L_0x00e5:
            r2 = move-exception
            r3 = r2
            r2 = r1
            r1 = r0
            r0 = r3
            goto L_0x00d3
        L_0x00eb:
            r0 = move-exception
            goto L_0x00d3
        L_0x00ed:
            r0 = move-exception
            r3 = r2
            r2 = r1
            r1 = r3
            goto L_0x00c1
        L_0x00f2:
            r2 = move-exception
            r3 = r2
            r2 = r1
            r1 = r0
            r0 = r3
            goto L_0x00c1
        L_0x00f8:
            r0 = move-exception
            goto L_0x005d
        L_0x00fb:
            r0 = move-exception
            goto L_0x0023
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.hall.common.SyncData.readReplayInfo(android.content.Context):void");
    }

    public static void saveProxyUrl(Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences("ProxyUrl", 0).edit();
        edit.putString("KEY_ProxyUrl", Proxy_Url == null ? "" : Proxy_Url);
        edit.commit();
    }

    public static void saveUUID(Context context) {
        SharedPreferences.Editor edit = context.getSharedPreferences("UUID", 0).edit();
        edit.putString("KEY_UUID", UUID);
        edit.commit();
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0072 A[SYNTHETIC, Splitter:B:30:0x0072] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0077 A[Catch:{ Exception -> 0x007b }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0081 A[SYNTHETIC, Splitter:B:38:0x0081] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0086 A[Catch:{ Exception -> 0x008a }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void writeProxyList(android.content.Context r7) {
        /*
            r2 = 0
            r4 = 0
            java.lang.String r5 = ""
            java.lang.String r0 = "proxy.data"
            r1 = 0
            java.io.FileOutputStream r0 = r7.openFileOutput(r0, r1)     // Catch:{ Exception -> 0x006b, all -> 0x007d }
            java.io.DataOutputStream r1 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x0099, all -> 0x008c }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0099, all -> 0x008c }
            java.util.Vector<com.tencent.qqgame.hall.common.data.MProxyInfoV2> r2 = com.tencent.qqgame.hall.common.SyncData.proxyList     // Catch:{ Exception -> 0x009f, all -> 0x0092 }
            int r2 = r2.size()     // Catch:{ Exception -> 0x009f, all -> 0x0092 }
            byte r3 = (byte) r2     // Catch:{ Exception -> 0x009f, all -> 0x0092 }
            r1.writeByte(r3)     // Catch:{ Exception -> 0x009f, all -> 0x0092 }
            if (r2 <= 0) goto L_0x005d
            r3 = r4
        L_0x001d:
            if (r3 >= r2) goto L_0x005d
            java.util.Vector<com.tencent.qqgame.hall.common.data.MProxyInfoV2> r4 = com.tencent.qqgame.hall.common.SyncData.proxyList     // Catch:{ Exception -> 0x009f, all -> 0x0092 }
            java.lang.Object r7 = r4.elementAt(r3)     // Catch:{ Exception -> 0x009f, all -> 0x0092 }
            com.tencent.qqgame.hall.common.data.MProxyInfoV2 r7 = (com.tencent.qqgame.hall.common.data.MProxyInfoV2) r7     // Catch:{ Exception -> 0x009f, all -> 0x0092 }
            short r4 = r7.svrId     // Catch:{ Exception -> 0x009f, all -> 0x0092 }
            r1.writeShort(r4)     // Catch:{ Exception -> 0x009f, all -> 0x0092 }
            java.lang.String r4 = r7.svrIP     // Catch:{ Exception -> 0x009f, all -> 0x0092 }
            if (r4 != 0) goto L_0x0057
            java.lang.String r4 = ""
            r4 = r5
        L_0x0033:
            r1.writeUTF(r4)     // Catch:{ Exception -> 0x009f, all -> 0x0092 }
            short r4 = r7.svrPort     // Catch:{ Exception -> 0x009f, all -> 0x0092 }
            r1.writeShort(r4)     // Catch:{ Exception -> 0x009f, all -> 0x0092 }
            short r4 = r7.onlineNum     // Catch:{ Exception -> 0x009f, all -> 0x0092 }
            r1.writeShort(r4)     // Catch:{ Exception -> 0x009f, all -> 0x0092 }
            byte r4 = r7.netFlag     // Catch:{ Exception -> 0x009f, all -> 0x0092 }
            r1.writeByte(r4)     // Catch:{ Exception -> 0x009f, all -> 0x0092 }
            java.lang.String r4 = r7.svrName     // Catch:{ Exception -> 0x009f, all -> 0x0092 }
            if (r4 != 0) goto L_0x005a
            java.lang.String r4 = ""
            r4 = r5
        L_0x004c:
            r1.writeUTF(r4)     // Catch:{ Exception -> 0x009f, all -> 0x0092 }
            int r4 = r7.supportedType     // Catch:{ Exception -> 0x009f, all -> 0x0092 }
            r1.writeInt(r4)     // Catch:{ Exception -> 0x009f, all -> 0x0092 }
            int r3 = r3 + 1
            goto L_0x001d
        L_0x0057:
            java.lang.String r4 = r7.svrIP     // Catch:{ Exception -> 0x009f, all -> 0x0092 }
            goto L_0x0033
        L_0x005a:
            java.lang.String r4 = r7.svrName     // Catch:{ Exception -> 0x009f, all -> 0x0092 }
            goto L_0x004c
        L_0x005d:
            r1.flush()     // Catch:{ Exception -> 0x009f, all -> 0x0092 }
            if (r0 == 0) goto L_0x0065
            r0.close()     // Catch:{ Exception -> 0x00a4 }
        L_0x0065:
            if (r1 == 0) goto L_0x006a
            r1.close()     // Catch:{ Exception -> 0x00a4 }
        L_0x006a:
            return
        L_0x006b:
            r0 = move-exception
            r1 = r2
        L_0x006d:
            r0.printStackTrace()     // Catch:{ all -> 0x0097 }
            if (r2 == 0) goto L_0x0075
            r2.close()     // Catch:{ Exception -> 0x007b }
        L_0x0075:
            if (r1 == 0) goto L_0x006a
            r1.close()     // Catch:{ Exception -> 0x007b }
            goto L_0x006a
        L_0x007b:
            r0 = move-exception
            goto L_0x006a
        L_0x007d:
            r0 = move-exception
            r1 = r2
        L_0x007f:
            if (r2 == 0) goto L_0x0084
            r2.close()     // Catch:{ Exception -> 0x008a }
        L_0x0084:
            if (r1 == 0) goto L_0x0089
            r1.close()     // Catch:{ Exception -> 0x008a }
        L_0x0089:
            throw r0
        L_0x008a:
            r1 = move-exception
            goto L_0x0089
        L_0x008c:
            r1 = move-exception
            r6 = r1
            r1 = r2
            r2 = r0
            r0 = r6
            goto L_0x007f
        L_0x0092:
            r2 = move-exception
            r6 = r2
            r2 = r0
            r0 = r6
            goto L_0x007f
        L_0x0097:
            r0 = move-exception
            goto L_0x007f
        L_0x0099:
            r1 = move-exception
            r6 = r1
            r1 = r2
            r2 = r0
            r0 = r6
            goto L_0x006d
        L_0x009f:
            r2 = move-exception
            r6 = r2
            r2 = r0
            r0 = r6
            goto L_0x006d
        L_0x00a4:
            r0 = move-exception
            goto L_0x006a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.hall.common.SyncData.writeProxyList(android.content.Context):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:45:0x00d2 A[SYNTHETIC, Splitter:B:45:0x00d2] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00d7 A[Catch:{ Exception -> 0x00db }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00e1 A[SYNTHETIC, Splitter:B:53:0x00e1] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00e6 A[Catch:{ Exception -> 0x00ea }] */
    /* JADX WARNING: Removed duplicated region for block: B:71:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void writeReplayInfo(android.content.Context r6, boolean r7) {
        /*
            r2 = 0
            r3 = -1
            java.lang.String r4 = ""
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "writeReplayInfo isRemoveAll: "
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r7)
            java.lang.String r1 = ",myZoneID:"
            java.lang.StringBuilder r0 = r0.append(r1)
            short r1 = com.tencent.qqgame.hall.common.SyncData.myZoneID
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = ",myRoomSvrID:"
            java.lang.StringBuilder r0 = r0.append(r1)
            short r1 = com.tencent.qqgame.hall.common.SyncData.myRoomSvrID
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = ",myRoomID:"
            java.lang.StringBuilder r0 = r0.append(r1)
            short r1 = com.tencent.qqgame.hall.common.SyncData.myRoomID
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = ",myTableID:"
            java.lang.StringBuilder r0 = r0.append(r1)
            short r1 = com.tencent.qqgame.hall.common.SyncData.myTableID
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = ",mySeatID:"
            java.lang.StringBuilder r0 = r0.append(r1)
            short r1 = com.tencent.qqgame.hall.common.SyncData.mySeatID
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = ",myZoneName:"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = com.tencent.qqgame.hall.common.SyncData.myZoneName
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = ",myRoomName:"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = com.tencent.qqgame.hall.common.SyncData.myRoomName
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            com.tencent.qqgame.hall.common.Tools.debug(r0)
            java.lang.String r0 = "replay.data"
            r1 = 0
            java.io.FileOutputStream r0 = r6.openFileOutput(r0, r1)     // Catch:{ Exception -> 0x00cb, all -> 0x00dd }
            java.io.DataOutputStream r1 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x00f9, all -> 0x00ec }
            r1.<init>(r0)     // Catch:{ Exception -> 0x00f9, all -> 0x00ec }
            if (r7 == 0) goto L_0x00b6
            r2 = r3
        L_0x007d:
            r1.writeShort(r2)     // Catch:{ Exception -> 0x00ff, all -> 0x00f2 }
            if (r7 == 0) goto L_0x00b9
            r2 = r3
        L_0x0083:
            r1.writeShort(r2)     // Catch:{ Exception -> 0x00ff, all -> 0x00f2 }
            if (r7 == 0) goto L_0x00bc
            r2 = r3
        L_0x0089:
            r1.writeShort(r2)     // Catch:{ Exception -> 0x00ff, all -> 0x00f2 }
            if (r7 == 0) goto L_0x00bf
            r2 = r3
        L_0x008f:
            r1.writeShort(r2)     // Catch:{ Exception -> 0x00ff, all -> 0x00f2 }
            if (r7 == 0) goto L_0x00c2
            r2 = r3
        L_0x0095:
            r1.writeShort(r2)     // Catch:{ Exception -> 0x00ff, all -> 0x00f2 }
            if (r7 == 0) goto L_0x00c5
            java.lang.String r2 = ""
            r2 = r4
        L_0x009d:
            r1.writeUTF(r2)     // Catch:{ Exception -> 0x00ff, all -> 0x00f2 }
            if (r7 == 0) goto L_0x00c8
            java.lang.String r2 = ""
            r2 = r4
        L_0x00a5:
            r1.writeUTF(r2)     // Catch:{ Exception -> 0x00ff, all -> 0x00f2 }
            r1.flush()     // Catch:{ Exception -> 0x00ff, all -> 0x00f2 }
            if (r0 == 0) goto L_0x00b0
            r0.close()     // Catch:{ Exception -> 0x0104 }
        L_0x00b0:
            if (r1 == 0) goto L_0x00b5
            r1.close()     // Catch:{ Exception -> 0x0104 }
        L_0x00b5:
            return
        L_0x00b6:
            short r2 = com.tencent.qqgame.hall.common.SyncData.myZoneID     // Catch:{ Exception -> 0x00ff, all -> 0x00f2 }
            goto L_0x007d
        L_0x00b9:
            short r2 = com.tencent.qqgame.hall.common.SyncData.myRoomSvrID     // Catch:{ Exception -> 0x00ff, all -> 0x00f2 }
            goto L_0x0083
        L_0x00bc:
            short r2 = com.tencent.qqgame.hall.common.SyncData.myRoomID     // Catch:{ Exception -> 0x00ff, all -> 0x00f2 }
            goto L_0x0089
        L_0x00bf:
            short r2 = com.tencent.qqgame.hall.common.SyncData.myTableID     // Catch:{ Exception -> 0x00ff, all -> 0x00f2 }
            goto L_0x008f
        L_0x00c2:
            short r2 = com.tencent.qqgame.hall.common.SyncData.mySeatID     // Catch:{ Exception -> 0x00ff, all -> 0x00f2 }
            goto L_0x0095
        L_0x00c5:
            java.lang.String r2 = com.tencent.qqgame.hall.common.SyncData.myZoneName     // Catch:{ Exception -> 0x00ff, all -> 0x00f2 }
            goto L_0x009d
        L_0x00c8:
            java.lang.String r2 = com.tencent.qqgame.hall.common.SyncData.myRoomName     // Catch:{ Exception -> 0x00ff, all -> 0x00f2 }
            goto L_0x00a5
        L_0x00cb:
            r0 = move-exception
            r1 = r2
        L_0x00cd:
            r0.printStackTrace()     // Catch:{ all -> 0x00f7 }
            if (r2 == 0) goto L_0x00d5
            r2.close()     // Catch:{ Exception -> 0x00db }
        L_0x00d5:
            if (r1 == 0) goto L_0x00b5
            r1.close()     // Catch:{ Exception -> 0x00db }
            goto L_0x00b5
        L_0x00db:
            r0 = move-exception
            goto L_0x00b5
        L_0x00dd:
            r0 = move-exception
            r1 = r2
        L_0x00df:
            if (r2 == 0) goto L_0x00e4
            r2.close()     // Catch:{ Exception -> 0x00ea }
        L_0x00e4:
            if (r1 == 0) goto L_0x00e9
            r1.close()     // Catch:{ Exception -> 0x00ea }
        L_0x00e9:
            throw r0
        L_0x00ea:
            r1 = move-exception
            goto L_0x00e9
        L_0x00ec:
            r1 = move-exception
            r5 = r1
            r1 = r2
            r2 = r0
            r0 = r5
            goto L_0x00df
        L_0x00f2:
            r2 = move-exception
            r5 = r2
            r2 = r0
            r0 = r5
            goto L_0x00df
        L_0x00f7:
            r0 = move-exception
            goto L_0x00df
        L_0x00f9:
            r1 = move-exception
            r5 = r1
            r1 = r2
            r2 = r0
            r0 = r5
            goto L_0x00cd
        L_0x00ff:
            r2 = move-exception
            r5 = r2
            r2 = r0
            r0 = r5
            goto L_0x00cd
        L_0x0104:
            r0 = move-exception
            goto L_0x00b5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.hall.common.SyncData.writeReplayInfo(android.content.Context, boolean):void");
    }
}
