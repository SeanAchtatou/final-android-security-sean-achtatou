package com.tencent.qqgame.hall.ui.controls;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import tencent.qqgame.lord.R;

public class ViewWrapper {
    View base;
    TextView hallName = null;
    ImageView icon = null;
    Button label = null;
    TextView number = null;

    public ViewWrapper(View view) {
        this.base = view;
    }

    public Button getButton() {
        if (this.label == null) {
            this.label = (Button) this.base.findViewById(R.id.btn_hall);
        }
        return this.label;
    }

    public TextView getHallName() {
        if (this.hallName == null) {
            this.hallName = (TextView) this.base.findViewById(R.id.tv_hall_name);
        }
        return this.hallName;
    }

    public ImageView getIcon() {
        if (this.icon == null) {
            this.icon = (ImageView) this.base.findViewById(R.id.icon_hall);
        }
        return this.icon;
    }

    public TextView getNumber() {
        if (this.number == null) {
            this.number = (TextView) this.base.findViewById(R.id.tv_hall_number);
        }
        return this.number;
    }
}
