package com.tencent.qqgame.hall.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.tencent.qqgame.hall.common.AActivity;
import com.tencent.qqgame.hall.common.IGameView;
import com.tencent.qqgame.hall.common.Report;
import com.tencent.qqgame.hall.common.SyncData;
import com.tencent.qqgame.hall.common.SysSetting;
import com.tencent.qqgame.hall.common.Tools;
import com.tencent.qqgame.hall.ui.controls.LoadingView;
import com.tencent.qqgame.lord.ui.LordLayout;
import tencent.qqgame.lord.R;

public class GameActivity extends AActivity {
    public static final String ACTION = "com.tencent.qqgame.hall.ui.Game";
    public static final int DIALOG_GAME_PROGRESS = 100;
    private static final int DIALOG_MATCH_CARD = 101;
    public static final String KEY_GAME_FROM = "GameFrom";
    public static final String KEY_IS_VIEWGAME = "IsViewGame";
    public static final String KEY_RETURN_FROM_WHERE = "ReturnWhere";
    public static final String KEY_ZONE_INFO = "ZoneInfo";
    public static final byte MSG_CHANGE_DESK_SUCC = 1;
    public static final byte MSG_LEAVE_ROOM = 2;
    private static final int NOTIFICATION_ID_GAME_MSG = 1;
    public static final int REQUEST_CODE_VERIFY = 1;
    public static final byte RETURN_FROM_HELP = 2;
    public static final byte RETURN_FROM_NOFICATION = 1;
    public static final byte START_FROM_HALL = 1;
    public static final byte START_FROM_LOGIN = 3;
    public static final byte START_FROM_TABLE = 2;
    private static boolean isGameReplaying = false;
    public boolean changeDeskStatus = true;
    private IGameView gameView = null;
    private boolean isForeground = false;
    private boolean isViewGame = false;
    /* access modifiers changed from: private */
    public EditText matchCardInput;
    public NotificationManager notificationManager;
    private byte returnFromWhere = -1;
    private byte start_form = 2;
    public int trusteeshipResId = R.string.menu_trusteeship;
    public boolean trusteeshipStatus = false;
    private Bundle zoneBundle = null;

    public GameActivity() {
        this.screen_type = 2;
    }

    public static void back2Game(Activity activity) {
        Intent intent = new Intent(activity, GameActivity.class);
        intent.setFlags(131072);
        activity.startActivity(intent);
    }

    private PendingIntent makeGameIntent() {
        return PendingIntent.getActivity(this, 0, new Intent(this, NotificationActivity.class).putExtra(KEY_RETURN_FROM_WHERE, (byte) 1), 134217728);
    }

    private void showMatchCardInfoBox() {
        this.matchCardInput = new EditText(this);
        this.matchCardInput.requestFocus();
        showDialog(DIALOG_MATCH_CARD);
    }

    /* access modifiers changed from: protected */
    public void handleMessage(Message message) {
        Handler handler;
        Class<HallListActivity> cls = HallListActivity.class;
        Tools.debug("<=>GameHall handle Message, what:" + message.what);
        switch (message.what) {
            case AActivity.MSG_LOGIN_REPLAY_SUCC:
                SyncData.isLogined = true;
                removeDialog(this.curr_dialog_key);
                this.gameView.sendMessage(this.handle.obtainMessage(IGameView.MSG_2GAME_REPLAY_SUCC), false);
                return;
            case AActivity.MSG_NET_SPEED_NORMALED:
                this.gameView.sendMessage(this.handle.obtainMessage(IGameView.MSG_2GAME_NET_NORMALED), false);
                return;
            case AActivity.MSG_NET_SPEED_SLOW:
                this.gameView.sendMessage(this.handle.obtainMessage(IGameView.MSG_2GAME_NET_SLOW), false);
                return;
            case AActivity.MSG_GET_VERIFYCODE_SUCC:
                Intent intent = new Intent(this, LoginActivity.class);
                intent.setFlags(1073741824);
                intent.putExtra(LoginActivity.KEY_STATUS, (byte) 6);
                intent.putExtra("PicFormat", (short) message.arg1);
                intent.putExtra("PicData", (byte[]) message.obj);
                startActivityForResult(intent, 1);
                return;
            case AActivity.MSG_LOGOUT_HANDLE:
            case AActivity.MSG_CONNECT_ERROR:
            case AActivity.MSG_RECONNECT_ERROR:
                this.gameView.sendMessage(this.handle.obtainMessage(IGameView.MSG_2GAME_DIS_CONNECTED), false);
                return;
            case AActivity.MSG_CONNECT_GAME_SUCC:
                factory.getSendMsgHandle().login(SyncData.curUin, SyncData.pswMD5, 2, SyncData.gameType, SyncData.myZoneID, SyncData.myRoomSvrID, SyncData.myRoomID, SyncData.myTableID, SyncData.mySeatID);
                return;
            case AActivity.MSG_QUICK_ERROR:
                if (!this.isViewGame) {
                    removeDialog(this.curr_dialog_key);
                    this.gameView.sendMessage(this.handle.obtainMessage(IGameView.MSG_2GAME_CHANGEDESK_FAULT), false);
                    return;
                }
                return;
            case 1:
                if (!this.isViewGame) {
                    removeDialog(this.curr_dialog_key);
                    Message obtainMessage = this.handle.obtainMessage(IGameView.MSG_2GAME_CHANGEDESK_SUCC);
                    obtainMessage.obj = SyncData.myTable;
                    this.gameView.sendMessage(obtainMessage, true);
                    return;
                }
                return;
            case 2:
                finish();
                if (this.start_form == 3) {
                    SyncData.myTableID = -1;
                    SyncData.mySeatID = -1;
                    Class<HallListActivity> cls2 = HallListActivity.class;
                    Intent intent2 = new Intent(this, cls);
                    intent2.putExtras(this.zoneBundle);
                    startActivity(intent2);
                    return;
                }
                return;
            case DIALOG_GAME_PROGRESS /*100*/:
                switch (message.arg1) {
                    case 0:
                        this.changeDeskStatus = ((Boolean) message.obj).booleanValue();
                        return;
                    case 1:
                    default:
                        return;
                    case 2:
                        if (message.arg2 > 0) {
                            this.trusteeshipResId = message.arg2;
                            return;
                        } else {
                            this.trusteeshipStatus = ((Boolean) message.obj).booleanValue();
                            return;
                        }
                }
            case DIALOG_MATCH_CARD /*101*/:
                if (!this.isViewGame && !this.isForeground && (handler = AActivity.currentActivity.handle) != null && !handler.equals(this.handle)) {
                    handler.sendEmptyMessage(DIALOG_MATCH_CARD);
                    return;
                }
                return;
            case 102:
            case 103:
                if (this.isViewGame) {
                    factory.getSendMsgHandle().cancelView();
                    finish();
                    return;
                }
                this.handle.sendEmptyMessage(109);
                if (this.start_form == 1 || this.start_form == 3) {
                    factory.getSendMsgHandle().leaveRoom(SyncData.myRoomSvrID, SyncData.myRoomID);
                } else {
                    factory.getSendMsgHandle().standUp();
                }
                if (message.what == 103) {
                    finish();
                    if (this.start_form == 3) {
                        SyncData.myTableID = -1;
                        SyncData.mySeatID = -1;
                        Class<HallListActivity> cls3 = HallListActivity.class;
                        Intent intent3 = new Intent(this, cls);
                        intent3.putExtras(this.zoneBundle);
                        startActivity(intent3);
                    }
                }
                if (soundPlayer != null) {
                    soundPlayer.stopSendCardSound();
                    soundPlayer.playWelcomeBackGroundSound();
                    return;
                }
                return;
            case 104:
                if (!this.isViewGame) {
                    showNoticationInfo((String) message.obj);
                    return;
                }
                return;
            case 105:
                if (this.isViewGame) {
                    Toast.makeText(this, message.arg1, 1).show();
                    finish();
                    return;
                }
                return;
            case 106:
                showDialog(1, res.getString(R.string.game_offline_replay));
                this.handle.sendEmptyMessage(AActivity.MSG_RECONNECT);
                return;
            case 107:
                this.handle.sendEmptyMessage(AActivity.MSG_EXIT_LOGIN);
                return;
            case 108:
                SyncData.writeReplayInfo(getApplicationContext(), false);
                return;
            case 109:
                SyncData.writeReplayInfo(getApplicationContext(), true);
                return;
            case 110:
                showDialog(100, (String) message.obj);
                return;
            case 111:
                if (this.curr_dialog_key != -1) {
                    removeDialog(this.curr_dialog_key);
                    return;
                }
                return;
            case 112:
                message.what = IGameView.MSG_2GAME_CHAT_EDITVIEW;
                this.gameView.sendMessage(message, true);
                return;
            case 113:
                adjustBright(SysSetting.getInstance(this).getBright());
                return;
            case 114:
                new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.game_tips_title).setMessage((String) message.obj).setCancelable(false).setPositiveButton((int) R.string.cmd_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        GameActivity.this.handle.sendEmptyMessage(103);
                    }
                }).show();
                return;
            default:
                return;
        }
    }

    public void initGameView() {
        factory.getReceiveMsgHandle().setGameReceiveHandle(null);
        if (SyncData.myTable != null && SyncData.me != null) {
            this.gameView = null;
            this.gameView = new LordLayout(this, this.handle, factory.getSendMsgHandle(), SyncData.myTable, SyncData.me, SyncData.curUin, this.isViewGame);
            factory.getReceiveMsgHandle().setGameReceiveHandle(this.gameView.getGameReceiveMessageHandle());
            setContentView((View) this.gameView);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 1) {
            if (i2 == 0) {
                this.gameView.sendMessage(this.handle.obtainMessage(IGameView.MSG_2GAME_DIS_CONNECTED), false);
            } else if (i2 == -1) {
                this.handle.sendEmptyMessage(AActivity.MSG_LOGIN_REPLAY_SUCC);
            } else if (i2 == 10) {
                this.gameView.sendMessage(this.handle.obtainMessage(IGameView.MSG_2GAME_REPLAY_FAULT), false);
            }
        }
        super.onActivityResult(i, i2, intent);
    }

    public void onCancel(DialogInterface dialogInterface) {
        if (this.curr_dialog_key == 100) {
            this.gameView.sendMessage(this.handle.obtainMessage(IGameView.MSG_2GAME_PROGRESSDIALOG_CANCELED), false);
        } else {
            super.onCancel(dialogInterface);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getWindow().setFlags(1024, 1024);
        this.notificationManager = (NotificationManager) getSystemService("notification");
        Intent intent = getIntent();
        this.start_form = intent.getByteExtra(KEY_GAME_FROM, (byte) 2);
        this.zoneBundle = intent.getExtras();
        this.isViewGame = intent.getBooleanExtra(KEY_IS_VIEWGAME, false);
        initGameView();
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        Dialog create;
        switch (i) {
            case DIALOG_GAME_PROGRESS /*100*/:
                create = new Dialog(this, R.style.Theme_InfoDialog);
                create.setContentView(new LoadingView(this, create, this.progressDlgMsg));
                create.setCancelable(true);
                create.setOnCancelListener(this);
                create.show();
                break;
            case DIALOG_MATCH_CARD /*101*/:
                create = new AlertDialog.Builder(this).setTitle(getResources().getString(R.string.login_match_card_title)).setView(this.matchCardInput).setPositiveButton(getResources().getString(R.string.cmd_ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SyncData.matchCardFileName = GameActivity.this.matchCardInput.getText().toString();
                    }
                }).setNegativeButton(getResources().getString(R.string.cmd_cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SyncData.matchCardFileName = "";
                    }
                }).create();
                break;
            default:
                create = super.onCreateDialog(i);
                break;
        }
        this.curr_dialog_key = i;
        return create;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        menu.add(0, 4, 0, (int) R.string.menu_chat).setIcon((int) R.drawable.menu_chat);
        menu.add(0, 5, 0, this.trusteeshipResId).setIcon((int) R.drawable.menu_substitute).setEnabled(false);
        menu.add(0, 6, 0, (int) R.string.menu_huanzhuo).setIcon((int) R.drawable.menu_changedesk).setEnabled(true);
        createGeneralMenu(menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.gameView != null) {
            this.gameView.sendMessage(this.handle.obtainMessage(IGameView.MSG_2GAME_EXIT_ACTIVITY), true);
        }
        this.gameView = null;
        factory.getReceiveMsgHandle().setGameReceiveHandle(null);
        this.notificationManager.cancel(1);
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        switch (i) {
            case 4:
                Report.keyBackHitInGameNum++;
                this.gameView.sendMessage(this.handle.obtainMessage(IGameView.MSG_2GAME_BACK_PRESSED), false);
                return true;
            default:
                return super.onKeyDown(i, keyEvent);
        }
    }

    public void onLowMemory() {
        super.onLowMemory();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        Log.w(this.TAG, "onNewIntent..");
        this.returnFromWhere = intent.getByteExtra(KEY_RETURN_FROM_WHERE, (byte) -1);
        if (this.returnFromWhere == 1) {
            this.notificationManager.cancel(1);
        }
        super.onNewIntent(intent);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem == null) {
            return true;
        }
        int i = -1;
        switch (menuItem.getItemId()) {
            case 0:
                Report.menuitemSysteSettingHitNum++;
                i = 3;
                break;
            case 1:
            case 3:
            default:
                return super.onOptionsItemSelected(menuItem);
            case 2:
                Report.menuitemExitHitNum++;
                i = 4;
                break;
            case 4:
                Report.menuitemChatHitNum++;
                i = 1;
                break;
            case 5:
                Report.menuitemTrustShipHitNum++;
                i = 2;
                break;
            case 6:
                Report.menuitemChangeDeskHitNum++;
                i = 0;
                break;
            case 7:
                showMatchCardInfoBox();
                break;
        }
        Message obtainMessage = this.handle.obtainMessage(IGameView.MSG_2GAME_MENU_ITEM_SELECTED);
        obtainMessage.arg1 = i;
        this.gameView.sendMessage(obtainMessage, false);
        return true;
    }

    public void onOptionsMenuClosed(Menu menu) {
        super.onOptionsMenuClosed(menu);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.isForeground = false;
        Message obtainMessage = this.handle.obtainMessage(IGameView.MSG_2GAME_FOREGROUND);
        obtainMessage.obj = Boolean.FALSE;
        this.gameView.sendMessage(obtainMessage, true);
        super.onPause();
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.getItem(1).setEnabled(this.trusteeshipStatus);
        menu.getItem(1).setTitle(this.trusteeshipResId);
        Tools.debug("changeDeskStatus: " + this.changeDeskStatus);
        menu.getItem(2).setEnabled(this.changeDeskStatus);
        return super.onPrepareOptionsMenu(menu);
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        this.notificationManager.cancel(1);
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.isForeground = true;
        Message obtainMessage = this.handle.obtainMessage(IGameView.MSG_2GAME_FOREGROUND);
        obtainMessage.obj = Boolean.TRUE;
        this.gameView.sendMessage(obtainMessage, true);
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    public final void showNoticationInfo(String str) {
        if (!this.isForeground && str != null && this.notificationManager != null) {
            Notification notification = new Notification(R.drawable.icon, str, System.currentTimeMillis());
            notification.setLatestEventInfo(this, res.getString(R.string.game_home_tip_title), str, makeGameIntent());
            this.notificationManager.notify(1, notification);
        }
    }
}
