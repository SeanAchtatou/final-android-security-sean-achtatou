package com.tencent.qqgame.hall.ui.controls;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;
import com.tencent.qqgame.hall.ui.TableListActivity;

public class TableScrollView extends ScrollView {
    public TableListActivity act;
    private final int[] point = new int[2];

    public TableScrollView(Context context) {
        super(context, null);
    }

    public TableScrollView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public int[] getLastTouchedXY() {
        return this.point;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        this.point[0] = (int) motionEvent.getX();
        this.point[1] = (int) motionEvent.getY();
        UserInfoControl userInfoControl = this.act.ctlUserInfo;
        if (userInfoControl != null) {
            this.act.rlTableList.removeView(userInfoControl);
            if (this.act.ctlUserInfo != null) {
                this.act.ctlUserInfo.recycle();
            }
            this.act.ctlUserInfo = null;
        }
        return super.onInterceptTouchEvent(motionEvent);
    }
}
