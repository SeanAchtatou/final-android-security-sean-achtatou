package com.tencent.qqgame.hall.common;

import tencent.qqgame.lord.R;

public class ResMapV1 {
    public static final int[] MAP = {R.drawable.poker, R.drawable.table, R.drawable.button, R.drawable.panel_v1, R.drawable.ppanel, R.drawable.tip, R.drawable.bubble, R.drawable.toolbar, R.drawable.portrait, R.drawable.bomb, R.drawable.win, R.drawable.lose, R.drawable.robot, R.drawable.fly, R.drawable.clock};
}
