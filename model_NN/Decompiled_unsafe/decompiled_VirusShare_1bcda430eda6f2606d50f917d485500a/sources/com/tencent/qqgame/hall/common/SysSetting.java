package com.tencent.qqgame.hall.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;

public class SysSetting {
    private static final String KEY_BGSOUND = "key_bgsound";
    private static final String KEY_EFFECTSOUND = "key_effectsound";
    private static final String KEY_HINT = "key_hint";
    private static final String KEY_LIGHT = "key_light";
    private static final String KEY_LIGHTMODE = "key_lightmode";
    private static final String KEY_VIBRATE = "key_vibrate";
    private static final String KEY_VOLUME = "key_volumne_bg";
    public static final float MIN_LIGHT = 0.1f;
    private static final String NAME = "syssetting";
    private static Context context;
    private static SysSetting sysSetting;
    private boolean bgSound = true;
    private boolean effectSound = true;
    private boolean hint = true;
    private float light = 0.8f;
    public AudioManager mgr = ((AudioManager) context.getSystemService("audio"));
    private boolean nightMode = false;
    private boolean vibrate = true;
    private int volume = ((this.mgr.getStreamMaxVolume(3) * 2) / 5);

    public SysSetting(Context context2) {
        context = context2;
    }

    public static SysSetting getInstance(Context context2) {
        if (sysSetting == null) {
            sysSetting = new SysSetting(context2);
        }
        context = context2;
        return sysSetting;
    }

    private void setStreamVolume(int i, int i2) {
        this.mgr.setStreamVolume(i, i2, 8);
    }

    public void adjustVolumne(int i, int i2) {
        setVolumne(i2);
        setStreamVolume(i, i2);
    }

    public float getBright() {
        return this.light;
    }

    public int getMaxStreamVolume(int i) {
        return this.mgr.getStreamMaxVolume(i);
    }

    public int getSysStreamVolume(int i) {
        return this.mgr.getStreamVolume(i);
    }

    public int getVolumne() {
        return this.volume;
    }

    public boolean isBgSound() {
        return this.bgSound;
    }

    public boolean isEffectSound() {
        return this.effectSound;
    }

    public boolean isHint() {
        return this.hint;
    }

    public boolean isLightMode() {
        return this.nightMode;
    }

    public boolean isVibrate() {
        return this.vibrate;
    }

    public void load() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(NAME, 0);
        setVolumne(sharedPreferences.getInt(KEY_VOLUME, getVolumne()));
        setBgSound(sharedPreferences.getBoolean(KEY_BGSOUND, isBgSound()));
        setEffectSound(sharedPreferences.getBoolean(KEY_EFFECTSOUND, isEffectSound()));
        setBright(sharedPreferences.getFloat(KEY_LIGHT, getBright()));
        setHint(sharedPreferences.getBoolean(KEY_HINT, isHint()));
        setVibrate(sharedPreferences.getBoolean(KEY_VIBRATE, isVibrate()));
        setLightMode(sharedPreferences.getBoolean(KEY_LIGHTMODE, isLightMode()));
    }

    public void save() {
        SharedPreferences.Editor edit = context.getSharedPreferences(NAME, 0).edit();
        edit.putInt(KEY_VOLUME, getVolumne());
        edit.putBoolean(KEY_BGSOUND, isBgSound());
        edit.putBoolean(KEY_EFFECTSOUND, isEffectSound());
        edit.putFloat(KEY_LIGHT, getBright());
        edit.putBoolean(KEY_HINT, isHint());
        edit.putBoolean(KEY_VIBRATE, isVibrate());
        edit.putBoolean(KEY_LIGHTMODE, isLightMode());
        edit.commit();
    }

    public void setBgSound(boolean z) {
        this.bgSound = z;
    }

    public void setBright(float f) {
        this.light = f;
    }

    public void setEffectSound(boolean z) {
        this.effectSound = z;
    }

    public void setHint(boolean z) {
        this.hint = z;
    }

    public void setLightMode(boolean z) {
        this.nightMode = z;
    }

    public void setVibrate(boolean z) {
        this.vibrate = z;
    }

    public void setVolumne(int i) {
        this.volume = i;
    }
}
