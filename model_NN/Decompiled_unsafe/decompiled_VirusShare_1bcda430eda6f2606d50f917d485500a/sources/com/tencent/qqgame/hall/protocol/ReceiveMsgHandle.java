package com.tencent.qqgame.hall.protocol;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.tencent.qqgame.common.Msg;
import com.tencent.qqgame.common.Player;
import com.tencent.qqgame.common.Table;
import com.tencent.qqgame.common.Util;
import com.tencent.qqgame.hall.common.AActivity;
import com.tencent.qqgame.hall.common.FactoryCenter;
import com.tencent.qqgame.hall.common.IGameView;
import com.tencent.qqgame.hall.common.SyncData;
import com.tencent.qqgame.hall.common.Tools;
import com.tencent.qqgame.hall.ui.GameActivity;
import com.tencent.qqgame.hall.ui.HallListActivity;
import com.tencent.qqgame.hall.ui.TableListActivity;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import tencent.qqgame.lord.R;

public class ReceiveMsgHandle {
    private FactoryCenter factory = null;
    private IReceiveGameMessageHandle handle = null;
    /* access modifiers changed from: private */
    public Handler uiHandle = null;

    public ReceiveMsgHandle() {
    }

    public ReceiveMsgHandle(FactoryCenter factoryCenter) {
        this.factory = factoryCenter;
    }

    private void cancelView(Msg msg) {
        DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(msg.msgData));
        try {
            dataInputStream.skipBytes(2);
            short readShort = dataInputStream.readShort();
            Tools.debug("    >>result:" + ((int) readShort));
            if (readShort == 102) {
                Message obtainMessage = this.uiHandle.obtainMessage(105);
                obtainMessage.arg1 = R.string.game_view_game_cancel;
                this.uiHandle.sendMessage(obtainMessage);
            }
        } catch (IOException e) {
        }
    }

    private void enterRoom(Msg msg) {
        if (SyncData.isLogined && (AActivity.currentActivity instanceof HallListActivity)) {
            DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(msg.msgData));
            try {
                dataInputStream.readShort();
                short readShort = dataInputStream.readShort();
                if (readShort != 0) {
                    Message obtainMessage = this.uiHandle.obtainMessage();
                    obtainMessage.what = 3;
                    obtainMessage.arg1 = readShort;
                    this.uiHandle.sendMessage(obtainMessage);
                    return;
                }
            } catch (IOException e) {
            }
            Intent intent = new Intent("com.tencent.qqgame.hall.ui.TableList");
            intent.putExtra("Data", msg.msgData);
            AActivity.currentActivity.startActivity(intent);
        }
    }

    private void getTableList(Msg msg) {
        short s;
        int i;
        if (SyncData.isLogined && (AActivity.currentActivity instanceof TableListActivity)) {
            DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(msg.msgData));
            try {
                dataInputStream.readShort();
                s = dataInputStream.readShort();
                try {
                    i = dataInputStream.readShort();
                } catch (IOException e) {
                }
            } catch (IOException e2) {
                s = 0;
            }
            if (s == 0 && i > 0) {
                Table[] tableArr = new Table[i];
                int i2 = 0;
                while (i2 < i) {
                    try {
                        short readShort = dataInputStream.readShort();
                        dataInputStream.readShort();
                        short readShort2 = dataInputStream.readShort();
                        tableArr[i2] = new Table(SyncData.myZoneID, SyncData.myRoomID, SyncData.myRoomSvrID, readShort);
                        for (int i3 = 0; i3 < readShort2; i3++) {
                            tableArr[i2].playerList.addElement(Msg.getPlayerInfo(dataInputStream));
                        }
                        i2++;
                    } catch (IOException e3) {
                    }
                }
                Message obtainMessage = this.uiHandle.obtainMessage();
                obtainMessage.what = AActivity.MSG_REFRESH_DATA;
                obtainMessage.obj = tableArr;
                this.uiHandle.sendMessage(obtainMessage);
                return;
            }
        }
        return;
        i = 0;
        if (s == 0) {
        }
    }

    private void getVerifyCode(Msg msg) {
        byte[] bArr;
        DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(msg.msgData));
        System.out.println("    >>MsgType:" + ((int) msg.msgType));
        if (msg.msgType == 12) {
            try {
                dataInputStream.skipBytes(2);
                Msg.dialogId = dataInputStream.readShort();
                short readShort = dataInputStream.readShort();
                dataInputStream.skipBytes(4);
                short readShort2 = dataInputStream.readShort();
                int readShort3 = dataInputStream.readShort();
                if (readShort2 != 1 || readShort3 <= 20 || readShort3 >= 4096) {
                    bArr = null;
                } else {
                    byte[] bArr2 = new byte[readShort3];
                    for (int i = 0; i < readShort3; i++) {
                        bArr2[i] = dataInputStream.readByte();
                    }
                    bArr = bArr2;
                }
                Message obtainMessage = this.uiHandle.obtainMessage(AActivity.MSG_GET_VERIFYCODE_SUCC);
                obtainMessage.obj = bArr;
                obtainMessage.arg1 = readShort;
                this.uiHandle.sendMessage(obtainMessage);
            } catch (IOException e) {
            }
        } else if (msg.msgType == 11) {
            try {
                dataInputStream.skipBytes(2);
                dataInputStream.readShort();
                short readShort4 = dataInputStream.readShort();
                String string = readShort4 > 0 ? Util.getString(dataInputStream, readShort4) : null;
                Message obtainMessage2 = this.uiHandle.obtainMessage(6);
                obtainMessage2.obj = string;
                this.uiHandle.sendMessage(obtainMessage2);
            } catch (IOException e2) {
            }
        }
    }

    private void getZoneList(Msg msg) {
        int[] iArr;
        short[] sArr;
        String[] strArr;
        int i;
        if (SyncData.isLogined && (AActivity.currentActivity instanceof HallListActivity)) {
            DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(msg.msgData));
            try {
                dataInputStream.readShort();
                dataInputStream.readShort();
                dataInputStream.readShort();
                int readShort = dataInputStream.readShort();
                try {
                    strArr = new String[readShort];
                    try {
                        short[] sArr2 = new short[readShort];
                        try {
                            int[] iArr2 = new int[readShort];
                            int i2 = 0;
                            while (i2 <= readShort) {
                                try {
                                    strArr[i2] = Util.getString(dataInputStream, dataInputStream.readShort());
                                    sArr2[i2] = dataInputStream.readShort();
                                    iArr2[i2] = dataInputStream.readInt();
                                    strArr[i2] = strArr[i2];
                                    i2++;
                                } catch (IOException e) {
                                    sArr = sArr2;
                                    i = readShort;
                                    iArr = iArr2;
                                }
                            }
                            sArr = sArr2;
                            i = readShort;
                            iArr = iArr2;
                        } catch (IOException e2) {
                            sArr = sArr2;
                            i = readShort;
                            iArr = null;
                        }
                    } catch (IOException e3) {
                        sArr = null;
                        i = readShort;
                        iArr = null;
                    }
                } catch (IOException e4) {
                    sArr = null;
                    strArr = null;
                    i = readShort;
                    iArr = null;
                }
            } catch (IOException e5) {
                iArr = null;
                sArr = null;
                strArr = null;
                i = 0;
            }
            if (i > 0) {
                Bundle bundle = new Bundle();
                bundle.putByte(HallListActivity.KEY_HALL_TYPE, (byte) 0);
                bundle.putStringArray(HallListActivity.KEY_HALL_NAMELIST, strArr);
                bundle.putShortArray(HallListActivity.KEY_HALL_IDLIST, sArr);
                bundle.putIntArray(HallListActivity.KEY_PLAYERINHALLNUM, iArr);
                Message obtainMessage = this.uiHandle.obtainMessage();
                obtainMessage.what = 4;
                obtainMessage.setData(bundle);
                this.uiHandle.sendMessage(obtainMessage);
            }
        }
    }

    private void leaveRoom() {
        if (AActivity.currentActivity instanceof GameActivity) {
            AActivity.currentActivity.handle.sendEmptyMessage(2);
        }
    }

    private void login(Msg msg) {
        if (!SyncData.isLogined) {
            DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(msg.msgData));
            Message obtainMessage = this.uiHandle.obtainMessage();
            try {
                dataInputStream.readShort();
                short readShort = dataInputStream.readShort();
                Tools.debug("result: " + ((int) readShort));
                if (readShort != 0) {
                    String string = readShort == 82 ? AActivity.res.getString(R.string.game_error_login1) : readShort == 8 ? AActivity.res.getString(R.string.game_error_login2) : readShort == 81 ? AActivity.res.getString(R.string.game_error_login3) : readShort == 95 ? AActivity.res.getString(R.string.game_error_login4) : readShort == 101 ? AActivity.res.getString(R.string.game_error_login5) : readShort == 96 ? AActivity.res.getString(R.string.game_error_login6) : readShort == 102 ? AActivity.res.getString(R.string.game_error_login7) : readShort == 99 ? AActivity.res.getString(R.string.game_error_login8) : AActivity.res.getString(R.string.game_error_login10);
                    obtainMessage.what = 2;
                    obtainMessage.obj = string;
                    this.uiHandle.sendMessage(obtainMessage);
                    return;
                }
                Bundle bundle = new Bundle();
                Msg.dialogId = dataInputStream.readShort();
                int readShort2 = dataInputStream.readShort();
                String[] strArr = new String[readShort2];
                short[] sArr = new short[readShort2];
                int[] iArr = new int[readShort2];
                for (int i = 0; i < readShort2; i++) {
                    strArr[i] = Util.getString(dataInputStream, dataInputStream.readShort());
                    sArr[i] = dataInputStream.readShort();
                    iArr[i] = dataInputStream.readInt();
                }
                bundle.putByte(HallListActivity.KEY_HALL_TYPE, (byte) 0);
                bundle.putStringArray(HallListActivity.KEY_HALL_NAMELIST, strArr);
                bundle.putShortArray(HallListActivity.KEY_HALL_IDLIST, sArr);
                bundle.putIntArray(HallListActivity.KEY_PLAYERINHALLNUM, iArr);
                dataInputStream.readShort();
                dataInputStream.readShort();
                dataInputStream.readShort();
                dataInputStream.readShort();
                dataInputStream.readShort();
                dataInputStream.readShort();
                short readShort3 = dataInputStream.readShort();
                Tools.debug("extAction:" + ((int) readShort3));
                if (readShort3 != 1) {
                    if (readShort3 == 2) {
                        dataInputStream.readShort();
                        if (dataInputStream.readShort() != 0) {
                            this.uiHandle.sendEmptyMessage(AActivity.MSG_LOGIN_REPLAY_FAULT);
                        } else {
                            SyncData.myZoneID = dataInputStream.readShort();
                            SyncData.myRoomSvrID = dataInputStream.readShort();
                            SyncData.myRoomID = dataInputStream.readShort();
                            SyncData.myTableID = dataInputStream.readShort();
                            SyncData.mySeatID = dataInputStream.readShort();
                            short readShort4 = dataInputStream.readShort();
                            SyncData.myTable = new Table(SyncData.myZoneID, SyncData.myZoneName, SyncData.myRoomID, SyncData.myRoomName, SyncData.myRoomSvrID, SyncData.myTableID);
                            Table table = SyncData.myTable;
                            for (byte b = 0; b < readShort4; b = (byte) (b + 1)) {
                                Player playerInfo = Msg.getPlayerInfo(dataInputStream);
                                Table.addPlayer(playerInfo, table.playerList);
                                if (playerInfo.uid == SyncData.curUin) {
                                    SyncData.me = playerInfo;
                                    SyncData.mySeatID = playerInfo.seatId;
                                }
                            }
                            dataInputStream.readShort();
                            dataInputStream.readShort();
                            this.uiHandle.sendEmptyMessage(AActivity.MSG_LOGIN_REPLAY_SUCC);
                            return;
                        }
                    }
                    obtainMessage.what = 3;
                    obtainMessage.obj = bundle;
                    this.uiHandle.sendMessage(obtainMessage);
                } else if (parseQuickPlay(dataInputStream) != 0 || SyncData.myTable == null || SyncData.myTable.playerList.size() < 1 || SyncData.me == null) {
                    obtainMessage.what = 8;
                    obtainMessage.obj = bundle;
                    this.uiHandle.sendMessage(obtainMessage);
                } else {
                    obtainMessage.what = 10;
                    obtainMessage.obj = bundle;
                    this.uiHandle.sendMessage(obtainMessage);
                }
            } catch (IOException e) {
            }
        }
    }

    private void logout() {
        this.uiHandle.sendEmptyMessage(AActivity.MSG_LOGOUT_HANDLE);
    }

    private short parseQuickPlay(DataInputStream dataInputStream) {
        try {
            dataInputStream.readShort();
            short readShort = dataInputStream.readShort();
            if (readShort != 0) {
                return readShort;
            }
            try {
                this.factory.getSync().quickPlayNum = 0;
                SyncData.myZoneID = dataInputStream.readShort();
                SyncData.myRoomSvrID = dataInputStream.readShort();
                SyncData.myRoomID = dataInputStream.readShort();
                SyncData.myTableID = dataInputStream.readShort();
                SyncData.mySeatID = dataInputStream.readShort();
                short readShort2 = dataInputStream.readShort();
                SyncData.myTable = new Table(SyncData.myZoneID, SyncData.myRoomID, SyncData.myRoomSvrID, SyncData.myTableID);
                SyncData.me = null;
                for (byte b = 0; b < readShort2; b = (byte) (b + 1)) {
                    Player playerInfo = Msg.getPlayerInfo(dataInputStream);
                    if (playerInfo != null) {
                        Table.addPlayer(playerInfo, SyncData.myTable.playerList);
                    }
                    if (playerInfo.uid == SyncData.curUin) {
                        SyncData.me = playerInfo;
                    }
                }
                SyncData.myZoneName = Util.getString(dataInputStream, dataInputStream.readShort());
                SyncData.myRoomName = Util.getString(dataInputStream, dataInputStream.readShort());
                SyncData.myTable.setTableNameInfo(SyncData.myZoneName, SyncData.myRoomName);
                Tools.debug("myZoneName:" + SyncData.myZoneName + ", myRoomName:" + SyncData.myRoomName);
                return readShort;
            } catch (IOException e) {
                return readShort;
            }
        } catch (IOException e2) {
            return 0;
        }
    }

    private void quickPlay(Msg msg) {
        if (SyncData.isLogined) {
            if (SyncData.lastQuickPlayMode != 4 || (AActivity.currentActivity instanceof GameActivity)) {
                short parseQuickPlay = parseQuickPlay(new DataInputStream(new ByteArrayInputStream(msg.msgData)));
                if (parseQuickPlay != 0) {
                    if (this.factory.getSync().quickPlayNum < 3) {
                        if (parseQuickPlay == 104) {
                            this.factory.getSendMsgHandle().quickPlay(SyncData.lastQuickPlayMode);
                        } else {
                            this.factory.getSendMsgHandle().quickPlay(SyncData.lastQuickPlayMode);
                        }
                        SyncData sync = this.factory.getSync();
                        sync.quickPlayNum = (byte) (sync.quickPlayNum + 1);
                        return;
                    }
                    String string = parseQuickPlay == 30 ? AActivity.res.getString(R.string.game_error_sitDown4) : AActivity.res.getString(R.string.game_error_quickPlay1);
                    Message obtainMessage = this.uiHandle.obtainMessage();
                    obtainMessage.what = AActivity.MSG_QUICK_ERROR;
                    obtainMessage.obj = string;
                    this.uiHandle.sendMessage(obtainMessage);
                } else if (SyncData.myTable == null || SyncData.myTable.playerList.size() < 1 || SyncData.me == null) {
                    Message obtainMessage2 = this.uiHandle.obtainMessage();
                    obtainMessage2.what = AActivity.MSG_QUICK_ERROR;
                    obtainMessage2.obj = AActivity.res.getString(R.string.game_error_quickPlay2);
                    this.uiHandle.sendMessage(obtainMessage2);
                } else if (AActivity.currentActivity instanceof GameActivity) {
                    this.uiHandle.sendMessage(this.uiHandle.obtainMessage(1));
                } else {
                    Intent intent = new Intent(GameActivity.ACTION);
                    if (AActivity.currentActivity instanceof HallListActivity) {
                        intent.putExtra(GameActivity.KEY_GAME_FROM, (byte) 1);
                    } else {
                        intent.putExtra(GameActivity.KEY_GAME_FROM, (byte) 2);
                    }
                    AActivity.currentActivity.startActivityIfNeeded(intent, 0);
                }
            } else {
                this.factory.getSendMsgHandle().standUp();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x004a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void replay(com.tencent.qqgame.common.Msg r14) {
        /*
            r13 = this;
            boolean r0 = com.tencent.qqgame.hall.common.SyncData.isLogined
            if (r0 != 0) goto L_0x0005
        L_0x0004:
            return
        L_0x0005:
            java.io.ByteArrayInputStream r0 = new java.io.ByteArrayInputStream
            byte[] r1 = r14.msgData
            r0.<init>(r1)
            java.io.DataInputStream r1 = new java.io.DataInputStream
            r1.<init>(r0)
            r0 = 1
            r2 = -1
            r3 = -1
            r4 = -1
            r5 = -1
            r6 = -1
            r7 = 0
            r1.readShort()     // Catch:{ IOException -> 0x007a }
            short r8 = r1.readShort()     // Catch:{ IOException -> 0x007a }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x007a }
            r9.<init>()     // Catch:{ IOException -> 0x007a }
            java.lang.String r10 = "result:"
            java.lang.StringBuilder r9 = r9.append(r10)     // Catch:{ IOException -> 0x007a }
            java.lang.StringBuilder r9 = r9.append(r8)     // Catch:{ IOException -> 0x007a }
            java.lang.String r9 = r9.toString()     // Catch:{ IOException -> 0x007a }
            com.tencent.qqgame.hall.common.Tools.debug(r9)     // Catch:{ IOException -> 0x007a }
            if (r8 == 0) goto L_0x0054
            r0 = 0
            r1 = r6
            r6 = r0
            r0 = r7
            r11 = r4
            r4 = r3
            r3 = r11
            r12 = r2
            r2 = r5
            r5 = r12
        L_0x0041:
            r7 = r0
            r0 = r1
            r1 = r5
            r5 = r4
            r11 = r2
            r2 = r6
            r6 = r11
        L_0x0048:
            if (r2 != 0) goto L_0x008a
            r0 = 0
            com.tencent.qqgame.hall.common.SyncData.myTable = r0
            android.os.Handler r0 = r13.uiHandle
            r1 = 6
            r0.sendEmptyMessage(r1)
            goto L_0x0004
        L_0x0054:
            short r2 = r1.readShort()     // Catch:{ IOException -> 0x007a }
            short r3 = r1.readShort()     // Catch:{ IOException -> 0x00ce }
            short r4 = r1.readShort()     // Catch:{ IOException -> 0x00d7 }
            short r5 = r1.readShort()     // Catch:{ IOException -> 0x00d7 }
            short r6 = r1.readShort()     // Catch:{ IOException -> 0x00e0 }
            short r8 = r1.readShort()     // Catch:{ IOException -> 0x00e9 }
            com.tencent.qqgame.common.Player[] r7 = new com.tencent.qqgame.common.Player[r8]     // Catch:{ IOException -> 0x00e9 }
            r9 = 0
        L_0x006f:
            if (r9 >= r8) goto L_0x00fb
            com.tencent.qqgame.common.Player r10 = com.tencent.qqgame.common.Msg.getPlayerInfo(r1)     // Catch:{ IOException -> 0x00f2 }
            r7[r9] = r10     // Catch:{ IOException -> 0x00f2 }
            int r9 = r9 + 1
            goto L_0x006f
        L_0x007a:
            r1 = move-exception
            r1 = r7
            r11 = r6
            r6 = r2
            r2 = r11
            r12 = r3
            r3 = r5
            r5 = r12
        L_0x0082:
            r7 = r1
            r1 = r6
            r6 = r3
            r3 = r4
            r11 = r2
            r2 = r0
            r0 = r11
            goto L_0x0048
        L_0x008a:
            com.tencent.qqgame.hall.common.SyncData.myZoneID = r1
            com.tencent.qqgame.hall.common.SyncData.myRoomID = r3
            com.tencent.qqgame.hall.common.SyncData.myRoomSvrID = r5
            com.tencent.qqgame.hall.common.SyncData.mySeatID = r0
            com.tencent.qqgame.hall.common.SyncData.myTableID = r6
            com.tencent.qqgame.common.Table r0 = new com.tencent.qqgame.common.Table
            java.lang.String r2 = com.tencent.qqgame.hall.common.SyncData.myZoneName
            java.lang.String r4 = com.tencent.qqgame.hall.common.SyncData.myRoomName
            r0.<init>(r1, r2, r3, r4, r5, r6)
            com.tencent.qqgame.hall.common.SyncData.myTable = r0
            if (r7 == 0) goto L_0x00c6
            r0 = 0
        L_0x00a2:
            int r1 = r7.length
            if (r0 >= r1) goto L_0x00c6
            com.tencent.qqgame.common.Table r1 = com.tencent.qqgame.hall.common.SyncData.myTable
            java.util.Vector<com.tencent.qqgame.common.Player> r1 = r1.playerList
            r2 = r7[r0]
            r1.add(r2)
            r1 = r7[r0]
            long r1 = r1.uid
            long r3 = com.tencent.qqgame.hall.common.SyncData.curUin
            int r1 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r1 != 0) goto L_0x00c2
            r1 = r7[r0]
            com.tencent.qqgame.hall.common.SyncData.me = r1
            r1 = r7[r0]
            short r1 = r1.seatId
            com.tencent.qqgame.hall.common.SyncData.mySeatID = r1
        L_0x00c2:
            int r0 = r0 + 1
            byte r0 = (byte) r0
            goto L_0x00a2
        L_0x00c6:
            android.os.Handler r0 = r13.uiHandle
            r1 = 5
            r0.sendEmptyMessage(r1)
            goto L_0x0004
        L_0x00ce:
            r1 = move-exception
            r1 = r7
            r11 = r6
            r6 = r2
            r2 = r11
            r12 = r3
            r3 = r5
            r5 = r12
            goto L_0x0082
        L_0x00d7:
            r1 = move-exception
            r1 = r7
            r11 = r6
            r6 = r2
            r2 = r11
            r12 = r3
            r3 = r5
            r5 = r12
            goto L_0x0082
        L_0x00e0:
            r1 = move-exception
            r1 = r7
            r11 = r6
            r6 = r2
            r2 = r11
            r12 = r3
            r3 = r5
            r5 = r12
            goto L_0x0082
        L_0x00e9:
            r1 = move-exception
            r1 = r7
            r11 = r6
            r6 = r2
            r2 = r11
            r12 = r3
            r3 = r5
            r5 = r12
            goto L_0x0082
        L_0x00f2:
            r1 = move-exception
            r1 = r7
            r11 = r6
            r6 = r2
            r2 = r11
            r12 = r3
            r3 = r5
            r5 = r12
            goto L_0x0082
        L_0x00fb:
            r1 = r6
            r6 = r0
            r0 = r7
            r11 = r4
            r4 = r3
            r3 = r11
            r12 = r2
            r2 = r5
            r5 = r12
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.hall.protocol.ReceiveMsgHandle.replay(com.tencent.qqgame.common.Msg):void");
    }

    private void sendMsg2Game(byte[] bArr, short s) {
        Tools.debug(">>receiveGameMessage...");
        if (this.handle != null && SyncData.isLogined) {
            Message obtainMessage = this.uiHandle.obtainMessage(IGameView.MSG_2GAME_PARSE_SERVER_MSG);
            obtainMessage.obj = bArr;
            obtainMessage.arg1 = s;
            this.handle.receiveGameMessage(obtainMessage);
        }
    }

    private void sitDown(Msg msg) {
        if (SyncData.isLogined && (AActivity.currentActivity instanceof TableListActivity)) {
            DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(msg.msgData));
            try {
                dataInputStream.skipBytes(2);
                short readShort = dataInputStream.readShort();
                if (readShort != 0) {
                    String string = (readShort == 15 || readShort == 16) ? AActivity.res.getString(R.string.game_error_sitDown1) : readShort == 17 ? AActivity.res.getString(R.string.game_error_sitDown2) : (readShort == 20 || readShort == 21) ? AActivity.res.getString(R.string.game_error_sitDown3) : AActivity.res.getString(R.string.game_error_sitDown4);
                    Message obtainMessage = this.uiHandle.obtainMessage(2);
                    obtainMessage.obj = string;
                    this.uiHandle.sendMessage(obtainMessage);
                    return;
                }
                short readShort2 = dataInputStream.readShort();
                SyncData.myTable = new Table(SyncData.myZoneID, SyncData.myZoneName, SyncData.myRoomID, SyncData.myRoomName, SyncData.myRoomSvrID, SyncData.myTableID);
                Table table = SyncData.myTable;
                for (byte b = 0; b < readShort2; b = (byte) (b + 1)) {
                    Player playerInfo = Msg.getPlayerInfo(dataInputStream);
                    Table.addPlayer(playerInfo, table.playerList);
                    if (playerInfo.uid == SyncData.curUin) {
                        SyncData.me = playerInfo;
                        SyncData.mySeatID = playerInfo.seatId;
                    }
                }
                Intent intent = new Intent(GameActivity.ACTION);
                intent.putExtra(GameActivity.KEY_GAME_FROM, (byte) 2);
                AActivity.currentActivity.startActivity(intent);
            } catch (IOException e) {
            }
        }
    }

    private void speedTest(Msg msg) {
        DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(msg.msgData));
        try {
            dataInputStream.readShort();
            this.factory.getSendMsgHandle().sendSpeedTest(Util.convertUnsigned2Long(dataInputStream.readInt()), Util.convertUnsigned2Long(dataInputStream.readInt()), System.currentTimeMillis());
        } catch (Exception e) {
        }
    }

    public final void callPlayerAction(Msg msg) {
        byte b;
        DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(msg.msgData));
        try {
            dataInputStream.readShort();
            b = dataInputStream.readByte();
            try {
                System.out.println("    >>ActionID:" + ((int) b));
                Util.getString(dataInputStream, (short) dataInputStream.readByte());
            } catch (Exception e) {
            }
        } catch (Exception e2) {
            b = 0;
        }
        if (b != 3) {
            if ((b == 1 || b == 2) && b == 1) {
                new AlertDialog.Builder(AActivity.currentActivity).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.game_tips_title).setMessage((int) R.string.game_tips_player_action_mulitpe).setCancelable(false).setPositiveButton((int) R.string.cmd_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ReceiveMsgHandle.this.uiHandle.sendEmptyMessage(AActivity.MSG_EXIT_LOGIN);
                    }
                }).show();
            }
        }
    }

    public void getRoomList(Msg msg) {
        int[] iArr;
        short[] sArr;
        String[] strArr;
        short[] sArr2;
        int i;
        int i2 = 0;
        if (SyncData.isLogined && (AActivity.currentActivity instanceof HallListActivity)) {
            DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(msg.msgData));
            try {
                dataInputStream.readShort();
                dataInputStream.readShort();
                dataInputStream.readShort();
                int readShort = dataInputStream.readShort();
                try {
                    short[] sArr3 = new short[readShort];
                    try {
                        String[] strArr2 = new String[readShort];
                        try {
                            short[] sArr4 = new short[readShort];
                            try {
                                int[] iArr2 = new int[readShort];
                                while (i2 < readShort) {
                                    try {
                                        sArr3[i2] = dataInputStream.readShort();
                                        strArr2[i2] = Util.getString(dataInputStream, dataInputStream.readShort());
                                        sArr4[i2] = dataInputStream.readShort();
                                        iArr2[i2] = dataInputStream.readInt();
                                        i2++;
                                    } catch (IOException e) {
                                        sArr = sArr4;
                                        i = readShort;
                                        iArr = iArr2;
                                        short[] sArr5 = sArr3;
                                        strArr = strArr2;
                                        sArr2 = sArr5;
                                    }
                                }
                                sArr = sArr4;
                                i = readShort;
                                iArr = iArr2;
                                short[] sArr6 = sArr3;
                                strArr = strArr2;
                                sArr2 = sArr6;
                            } catch (IOException e2) {
                                sArr = sArr4;
                                i = readShort;
                                iArr = null;
                                short[] sArr7 = sArr3;
                                strArr = strArr2;
                                sArr2 = sArr7;
                            }
                        } catch (IOException e3) {
                            sArr = null;
                            i = readShort;
                            iArr = null;
                            short[] sArr8 = sArr3;
                            strArr = strArr2;
                            sArr2 = sArr8;
                        }
                    } catch (IOException e4) {
                        sArr = null;
                        sArr2 = sArr3;
                        i = readShort;
                        iArr = null;
                        strArr = null;
                    }
                } catch (IOException e5) {
                    sArr = null;
                    strArr = null;
                    sArr2 = null;
                    i = readShort;
                    iArr = null;
                }
            } catch (IOException e6) {
                iArr = null;
                sArr = null;
                strArr = null;
                sArr2 = null;
                i = 0;
            }
            if (i <= 0) {
                Message obtainMessage = this.uiHandle.obtainMessage();
                obtainMessage.what = 1;
                this.uiHandle.sendMessage(obtainMessage);
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putByte(HallListActivity.KEY_HALL_TYPE, (byte) 1);
            bundle.putStringArray(HallListActivity.KEY_HALL_NAMELIST, strArr);
            bundle.putShortArray(HallListActivity.KEY_HALL_IDLIST, sArr);
            bundle.putIntArray(HallListActivity.KEY_PLAYERINHALLNUM, iArr);
            bundle.putShortArray(HallListActivity.KEY_SERVERID, sArr2);
            Message obtainMessage2 = this.uiHandle.obtainMessage();
            obtainMessage2.what = 2;
            obtainMessage2.setData(bundle);
            this.uiHandle.sendMessage(obtainMessage2);
        }
    }

    public void getServerList(Msg msg) {
        DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(msg.msgData));
        try {
            dataInputStream.skipBytes(6);
            if (0 < dataInputStream.readShort()) {
                dataInputStream.skipBytes(2);
                int readByte = dataInputStream.readByte();
                char[] cArr = new char[readByte];
                for (int i = 0; i < readByte; i++) {
                    cArr[i] = (char) dataInputStream.readByte();
                }
                String valueOf = String.valueOf(cArr);
                short readShort = dataInputStream.readShort();
                dataInputStream.close();
                Message obtainMessage = this.uiHandle.obtainMessage();
                obtainMessage.what = 4;
                obtainMessage.arg1 = readShort;
                obtainMessage.obj = valueOf;
                this.uiHandle.sendMessage(obtainMessage);
            }
        } catch (Exception e) {
            Tools.debug("getServerList parse exception, detail info: " + e.toString());
        }
    }

    public boolean parseMsg(Msg msg) {
        Tools.debug("parseMsg, msg id: " + ((int) msg.msgId));
        this.uiHandle = AActivity.currentActivity.handle;
        switch (msg.msgId) {
            case GameActivity.DIALOG_GAME_PROGRESS /*100*/:
                Tools.debug(">>login...");
                login(msg);
                return true;
            case 101:
                Tools.debug(">>logout...");
                logout();
                return true;
            case 105:
                Tools.debug(">>leave room...");
                leaveRoom();
                return true;
            case 111:
                Tools.debug(">>sitDown...");
                sitDown(msg);
                return true;
            case 115:
                System.out.println(">>VIEWGAME");
                viewGame(msg);
                return true;
            case 116:
                Tools.debug(">>CANCEL VIEW");
                cancelView(msg);
                return true;
            case 136:
                Tools.debug(">>HALL_REPLAY");
                replay(msg);
                return true;
            case 400:
                Tools.debug(">>getZoneList...");
                getZoneList(msg);
                return true;
            case 401:
                Tools.debug(">>getRoomList...");
                getRoomList(msg);
                return true;
            case 402:
                Tools.debug(">>enterRoom...");
                enterRoom(msg);
                return true;
            case 407:
                Tools.debug(">>quickPlay...");
                quickPlay(msg);
                return true;
            case 408:
                Tools.debug(">>getTableList...");
                getTableList(msg);
                return true;
            case 411:
                Tools.debug(">>NOTIFY_CALL_PLAYER");
                callPlayerAction(msg);
                return true;
            case 415:
                Tools.debug("   >>Verify Code");
                getVerifyCode(msg);
                return true;
            case 416:
                Tools.debug(">>get speedTest...");
                speedTest(msg);
                return true;
            case 500:
                Tools.debug(">>get hello...");
                sendMsg2Game(msg.msgData, msg.msgId);
                return true;
            case 502:
                Tools.debug(">>get serverlist...");
                getServerList(msg);
                return true;
            default:
                sendMsg2Game(msg.msgData, msg.msgId);
                return true;
        }
    }

    public void setGameReceiveHandle(IReceiveGameMessageHandle iReceiveGameMessageHandle) {
        this.handle = iReceiveGameMessageHandle;
    }

    public void viewGame(Msg msg) {
        Player[] playerArr;
        boolean z;
        Player[] playerArr2;
        DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(msg.msgData));
        try {
            dataInputStream.skipBytes(2);
            short readShort = dataInputStream.readShort();
            Tools.debug("    >>result:" + ((int) readShort));
            if (readShort != 0) {
                playerArr2 = null;
                z = false;
            } else {
                dataInputStream.skipBytes(6);
                short readShort2 = dataInputStream.readShort();
                if (readShort2 > 0) {
                    dataInputStream.skipBytes(readShort2);
                }
                short readShort3 = dataInputStream.readShort();
                if (readShort3 > 0) {
                    dataInputStream.skipBytes(readShort3);
                }
                int readShort4 = dataInputStream.readShort();
                Player[] playerArr3 = new Player[readShort4];
                for (int i = 0; i < readShort4; i++) {
                    playerArr3[i] = Msg.getPlayerInfo(dataInputStream);
                }
                playerArr2 = playerArr3;
                z = true;
            }
            playerArr = playerArr2;
        } catch (IOException e) {
            playerArr = null;
            z = true;
        }
        if (z) {
            SyncData.myTable = new Table(SyncData.myZoneID, SyncData.myZoneName, SyncData.myRoomID, SyncData.myRoomName, SyncData.myRoomSvrID, SyncData.myTableID);
            if (playerArr != null) {
                for (byte b = 0; b < playerArr.length; b = (byte) (b + 1)) {
                    Player player = playerArr[b];
                    SyncData.myTable.playerList.add(player);
                    if (player.seatId == SyncData.mySeatID) {
                        SyncData.me = player;
                    }
                }
            }
            this.uiHandle.sendEmptyMessage(6);
            return;
        }
        this.uiHandle.sendEmptyMessage(5);
    }
}
