package com.tencent.qqgame.hall.common;

import android.content.Context;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.util.Vector;

public class AccountStore {
    private Vector<AccountInfo> accountList;
    private Context context;
    private String fileName;
    private final int maxSize;

    public static class AccountInfo {
        private String inputPwd;
        private boolean isAutoLogin;
        private boolean isLoginPlay;
        private boolean isSavedPwd;
        private String name;
        private String succeedPwd;

        public AccountInfo() {
            restore();
        }

        public void copy(AccountInfo accountInfo) {
            if (accountInfo != null && accountInfo != this) {
                this.name = new String(accountInfo.getName());
                this.inputPwd = new String(accountInfo.getInputPwd());
                this.succeedPwd = new String(accountInfo.getSucceedPwd());
                this.isSavedPwd = accountInfo.isSavedPwd;
                this.isAutoLogin = accountInfo.isAutoLogin;
                this.isLoginPlay = accountInfo.isLoginPlay;
            }
        }

        public String getInputPwd() {
            return this.inputPwd;
        }

        public String getName() {
            return this.name;
        }

        public String getSucceedPwd() {
            return this.succeedPwd;
        }

        public boolean isAutoLogin() {
            return this.isAutoLogin;
        }

        public boolean isLoginPlay() {
            return this.isLoginPlay;
        }

        public boolean isSavedPwd() {
            return this.isSavedPwd;
        }

        public void readStream(DataInputStream dataInputStream) throws Exception {
            this.name = dataInputStream.readUTF();
            this.inputPwd = dataInputStream.readUTF();
            this.succeedPwd = dataInputStream.readUTF();
            this.isSavedPwd = dataInputStream.readBoolean();
            this.isAutoLogin = dataInputStream.readBoolean();
            this.isLoginPlay = dataInputStream.readBoolean();
        }

        public void restore() {
            this.name = "";
            this.inputPwd = "";
            this.succeedPwd = "";
            this.isSavedPwd = false;
            this.isAutoLogin = false;
            this.isLoginPlay = true;
        }

        public AccountInfo setAutoLogin(boolean z) {
            this.isAutoLogin = z;
            return this;
        }

        public AccountInfo setInputPwd(String str) {
            this.inputPwd = str;
            return this;
        }

        public AccountInfo setLoginPlay(boolean z) {
            this.isLoginPlay = z;
            return this;
        }

        public AccountInfo setName(String str) {
            this.name = str;
            return this;
        }

        public AccountInfo setSavedPwd(boolean z) {
            this.isSavedPwd = z;
            return this;
        }

        public AccountInfo setSucceedPwd(String str) {
            this.succeedPwd = str;
            return this;
        }

        public String toString() {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("name=").append(this.name);
            stringBuffer.append("\ninputPwd=").append(this.inputPwd);
            stringBuffer.append("\nsucceedPwd=").append(this.succeedPwd);
            stringBuffer.append("\nisSavedPwd=").append(this.isSavedPwd);
            stringBuffer.append("\nisAutoLogin=").append(this.isAutoLogin);
            stringBuffer.append("\nisLoginPlay=").append(this.isLoginPlay);
            return stringBuffer.toString();
        }

        public void writeStream(DataOutputStream dataOutputStream) throws Exception {
            dataOutputStream.writeUTF(this.name);
            dataOutputStream.writeUTF(this.inputPwd);
            dataOutputStream.writeUTF(this.succeedPwd);
            dataOutputStream.writeBoolean(this.isSavedPwd);
            dataOutputStream.writeBoolean(this.isAutoLogin);
            dataOutputStream.writeBoolean(this.isLoginPlay);
            dataOutputStream.flush();
        }
    }

    public AccountStore(Context context2, String str, int i) {
        this.context = context2;
        this.fileName = str;
        this.maxSize = i;
        if (this.accountList == null) {
            this.accountList = new Vector<>();
        }
    }

    private void readStream(DataInputStream dataInputStream) throws Exception {
        this.accountList.clear();
        byte readByte = dataInputStream.readByte();
        for (int i = 0; i < readByte; i++) {
            AccountInfo accountInfo = new AccountInfo();
            accountInfo.readStream(dataInputStream);
            this.accountList.add(accountInfo);
        }
    }

    private void writeStream(DataOutputStream dataOutputStream) throws Exception {
        dataOutputStream.writeByte(this.accountList.size());
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < this.accountList.size()) {
                this.accountList.elementAt(i2).writeStream(dataOutputStream);
                i = i2 + 1;
            } else {
                dataOutputStream.flush();
                return;
            }
        }
    }

    public void close() {
        if (this.accountList != null) {
            this.accountList.clear();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x0028 A[SYNTHETIC, Splitter:B:15:0x0028] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x002d A[Catch:{ Exception -> 0x0031 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0038 A[SYNTHETIC, Splitter:B:23:0x0038] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x003d A[Catch:{ Exception -> 0x0041 }] */
    /* JADX WARNING: Removed duplicated region for block: B:40:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void commit() {
        /*
            r5 = this;
            r3 = 0
            android.content.Context r0 = r5.context     // Catch:{ Exception -> 0x0023, all -> 0x0033 }
            java.lang.String r1 = r5.fileName     // Catch:{ Exception -> 0x0023, all -> 0x0033 }
            r2 = 0
            java.io.FileOutputStream r0 = r0.openFileOutput(r1, r2)     // Catch:{ Exception -> 0x0023, all -> 0x0033 }
            java.io.DataOutputStream r1 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x004d, all -> 0x0043 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x004d, all -> 0x0043 }
            r5.writeStream(r1)     // Catch:{ Exception -> 0x0051, all -> 0x0048 }
            r0.close()     // Catch:{ Exception -> 0x0051, all -> 0x0048 }
            r1.close()     // Catch:{ Exception -> 0x0051, all -> 0x0048 }
            if (r0 == 0) goto L_0x001d
            r0.close()     // Catch:{ Exception -> 0x0056 }
        L_0x001d:
            if (r1 == 0) goto L_0x0022
            r1.close()     // Catch:{ Exception -> 0x0056 }
        L_0x0022:
            return
        L_0x0023:
            r0 = move-exception
            r0 = r3
            r1 = r3
        L_0x0026:
            if (r1 == 0) goto L_0x002b
            r1.close()     // Catch:{ Exception -> 0x0031 }
        L_0x002b:
            if (r0 == 0) goto L_0x0022
            r0.close()     // Catch:{ Exception -> 0x0031 }
            goto L_0x0022
        L_0x0031:
            r0 = move-exception
            goto L_0x0022
        L_0x0033:
            r0 = move-exception
            r1 = r3
            r2 = r3
        L_0x0036:
            if (r2 == 0) goto L_0x003b
            r2.close()     // Catch:{ Exception -> 0x0041 }
        L_0x003b:
            if (r1 == 0) goto L_0x0040
            r1.close()     // Catch:{ Exception -> 0x0041 }
        L_0x0040:
            throw r0
        L_0x0041:
            r1 = move-exception
            goto L_0x0040
        L_0x0043:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            goto L_0x0036
        L_0x0048:
            r2 = move-exception
            r4 = r2
            r2 = r0
            r0 = r4
            goto L_0x0036
        L_0x004d:
            r1 = move-exception
            r1 = r0
            r0 = r3
            goto L_0x0026
        L_0x0051:
            r2 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
            goto L_0x0026
        L_0x0056:
            r0 = move-exception
            goto L_0x0022
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.hall.common.AccountStore.commit():void");
    }

    public AccountInfo get(String str) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.accountList.size()) {
                return null;
            }
            if (this.accountList.elementAt(i2).getName().equals(str)) {
                return this.accountList.elementAt(i2);
            }
            i = i2 + 1;
        }
    }

    public int getAccountSize() {
        if (this.accountList == null) {
            return 0;
        }
        return this.accountList.size();
    }

    public boolean isEmpty() {
        return this.accountList.size() == 0;
    }

    public void open() {
        FileInputStream fileInputStream;
        DataInputStream dataInputStream;
        try {
            FileInputStream openFileInput = this.context.openFileInput(this.fileName);
            try {
                DataInputStream dataInputStream2 = new DataInputStream(openFileInput);
                try {
                    readStream(dataInputStream2);
                    openFileInput.close();
                    dataInputStream2.close();
                } catch (Exception e) {
                    DataInputStream dataInputStream3 = dataInputStream2;
                    fileInputStream = openFileInput;
                    dataInputStream = dataInputStream3;
                }
            } catch (Exception e2) {
                fileInputStream = openFileInput;
                dataInputStream = null;
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                    } catch (Exception e3) {
                        return;
                    }
                }
                if (dataInputStream != null) {
                    dataInputStream.close();
                }
            }
        } catch (Exception e4) {
            dataInputStream = null;
            fileInputStream = null;
        }
    }

    public AccountInfo peek() {
        if (this.accountList.size() == 0) {
            return null;
        }
        return this.accountList.elementAt(0);
    }

    public void pushAndUpdate(AccountInfo accountInfo) {
        if (accountInfo != null && !accountInfo.getName().equals("")) {
            AccountInfo accountInfo2 = new AccountInfo();
            accountInfo2.copy(accountInfo);
            int search = search(accountInfo2);
            if (search >= 0) {
                this.accountList.remove(search);
            }
            this.accountList.insertElementAt(accountInfo2, 0);
            if (this.accountList.size() > this.maxSize) {
                this.accountList.remove(this.accountList.lastElement());
            }
        }
    }

    public void remove(String str) {
        int i;
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 >= this.accountList.size()) {
                i = -1;
                break;
            } else if (this.accountList.elementAt(i3).getName().equals(str)) {
                i = i3;
                break;
            } else {
                i2 = i3 + 1;
            }
        }
        this.accountList.remove(i);
    }

    public int search(AccountInfo accountInfo) {
        if (accountInfo == null) {
            return -1;
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.accountList.size()) {
                return -1;
            }
            if (this.accountList.elementAt(i2).getName().equals(accountInfo.getName())) {
                return i2;
            }
            i = i2 + 1;
        }
    }

    public int search(String str) {
        if (str == null || str.equals("")) {
            return -1;
        }
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= this.accountList.size()) {
                return -1;
            }
            if (this.accountList.elementAt(i2).getName().equals(str)) {
                return i2;
            }
            i = i2 + 1;
        }
    }

    public String[] toAccountArray() {
        String[] strArr = new String[this.accountList.size()];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= strArr.length) {
                return strArr;
            }
            strArr[i2] = this.accountList.elementAt(i2).getName();
            i = i2 + 1;
        }
    }
}
