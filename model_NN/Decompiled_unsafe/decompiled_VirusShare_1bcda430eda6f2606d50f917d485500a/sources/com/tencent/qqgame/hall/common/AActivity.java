package com.tencent.qqgame.hall.common;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.qqgame.common.Msg;
import com.tencent.qqgame.hall.ui.GameActivity;
import com.tencent.qqgame.hall.ui.LoginActivity;
import com.tencent.qqgame.hall.ui.controls.LoadingView;
import com.tencent.qqgame.lord.common.SoundPlayer;
import com.tencent.qqgame.lord.ui.LordView;
import tencent.qqgame.lord.BaseAActivity;
import tencent.qqgame.lord.R;

public abstract class AActivity extends BaseAActivity implements DialogInterface.OnCancelListener {
    public static final int DIALOG_NORMAL_PROGRESS = 0;
    public static final int DIALOG_NOT_NEED_CLEAR = 1;
    private static final int MAX_BACKGROUDSOUND = 3000;
    private static final int MAX_DISCONNECT = 1800000;
    protected static final int MENUITEM_ID_CHANGEDESK = 6;
    protected static final int MENUITEM_ID_CHAT = 4;
    protected static final int MENUITEM_ID_EXIT = 2;
    protected static final int MENUITEM_ID_HELPABOUT = 1;
    protected static final int MENUITEM_ID_MATCHCARD = 7;
    protected static final int MENUITEM_ID_QUICKGAME = 3;
    protected static final int MENUITEM_ID_SYSTEMSETTING = 0;
    protected static final int MENUITEM_ID_TRUSTEESHIP = 5;
    public static final int MSG_CONNECT_ERROR = -110;
    public static final int MSG_CONNECT_GAME_SUCC = -109;
    public static final int MSG_EXIT_LOGIN = -106;
    public static final int MSG_GET_VERIFYCODE_SUCC = -112;
    public static final int MSG_GLOABLE_SLEEP = -101;
    public static final int MSG_LOGIN_REPLAY_FAULT = -116;
    public static final int MSG_LOGIN_REPLAY_SUCC = -115;
    public static final int MSG_LOGOUT_HANDLE = -111;
    public static final int MSG_NET_SPEED_NORMALED = -114;
    public static final int MSG_NET_SPEED_SLOW = -113;
    public static final int MSG_PARSE_HALL_HTTP_SERVER_MSG = -108;
    public static final int MSG_PARSE_SERVER_MSG = -100;
    public static final int MSG_QUICK_ERROR = -103;
    public static final int MSG_RECONNECT = -104;
    public static final int MSG_RECONNECT_ERROR = -105;
    public static final int MSG_REFRESH_DATA = -102;
    public static final byte SCREEN_ORIENTATION_AUTO = 3;
    public static final byte SCREEN_ORIENTATION_LANDSCAPE = 2;
    public static final byte SCREEN_ORIENTATION_PORTRAIT = 1;
    public static AActivity currentActivity = null;
    private static Runnable disBackGroundSoundTask = new Runnable() {
        public void run() {
            if (AActivity.soundPlayer != null) {
                AActivity.soundPlayer.stopBackGroundSound(true);
            }
        }
    };
    public static Handler disConnectHandler = new Handler();
    private static Runnable disConnectTask = new Runnable() {
        public void run() {
            if (AActivity.factory != null) {
                if (AActivity.factory.getCommunicator().isConnected()) {
                    AActivity.factory.getSendMsgHandle().logout();
                }
                AActivity.factory._comm = null;
            }
        }
    };
    protected static BitmapDrawable drawBg = null;
    /* access modifiers changed from: protected */
    public static FactoryCenter factory = null;
    public static boolean isExitingApp = false;
    public static Resources res = null;
    public static SoundPlayer soundPlayer;
    private final int MAX_STEP;
    protected final short SPF;
    protected final String TAG;
    public int curr_dialog_key;
    public Handler handle;
    protected String progressDlgMsg;
    protected byte screen_type;
    protected int step;
    protected ViewGroup vgTopLayout;

    public AActivity() {
        this.screen_type = 1;
        this.handle = null;
        this.SPF = Msg.KICK_offline;
        this.MAX_STEP = 10000;
        this.step = 0;
        this.curr_dialog_key = -1;
        this.progressDlgMsg = "";
        this.vgTopLayout = null;
        this.TAG = getClass().getName();
        this.handle = new Handler() {
            public void handleMessage(Message message) {
                AActivity.this.doHandleMessage(message);
            }
        };
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* access modifiers changed from: private */
    public final void doHandleMessage(Message message) {
        boolean z;
        this.step = this.step > 10000 ? 0 : this.step + 1;
        switch (message.what) {
            case MSG_LOGOUT_HANDLE /*-111*/:
                if (!(this instanceof LoginActivity) && !(this instanceof GameActivity)) {
                    new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.game_tips_title).setMessage(res.getString(R.string.base_connect_intterupted)).setCancelable(false).setPositiveButton((int) R.string.menu_exit, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            AActivity.this.handle.sendEmptyMessage(AActivity.MSG_EXIT_LOGIN);
                        }
                    }).show();
                }
                z = true;
                break;
            case MSG_CONNECT_ERROR /*-110*/:
            case MSG_RECONNECT_ERROR /*-105*/:
                SyncData.isReconnecting = false;
                SyncData.continue_reconnect_times = 0;
                if (!(this instanceof GameActivity)) {
                    Toast.makeText(this, (int) R.string.game_error_nosocket, 1).show();
                    z = true;
                    break;
                }
                z = true;
                break;
            case MSG_CONNECT_GAME_SUCC /*-109*/:
            case -107:
            case MSG_REFRESH_DATA /*-102*/:
            case MSG_GLOABLE_SLEEP /*-101*/:
            default:
                z = true;
                break;
            case MSG_PARSE_HALL_HTTP_SERVER_MSG /*-108*/:
                if (message.obj instanceof byte[]) {
                    factory.getReceiveHallHttpMsgHandle().parseMsg((short) message.arg1, (byte[]) message.obj);
                    return;
                }
                return;
            case MSG_EXIT_LOGIN /*-106*/:
                if (SyncData.isLogined) {
                    factory.getSendMsgHandle().logout();
                }
                if (!(this instanceof LoginActivity)) {
                    finish();
                    startActivity(new Intent(this, LoginActivity.class).setFlags(67108864).putExtra(LoginActivity.KEY_STATUS, (byte) 1));
                    return;
                }
                return;
            case MSG_RECONNECT /*-104*/:
                if (!(this instanceof LoginActivity) && !(this instanceof GameActivity)) {
                    this.handle.sendEmptyMessage(MSG_LOGOUT_HANDLE);
                    return;
                } else if (!SyncData.isReconnecting) {
                    SyncData.isLogined = false;
                    SyncData.isReconnecting = true;
                    factory.getCommunicator().connect(true);
                    return;
                } else {
                    return;
                }
            case MSG_QUICK_ERROR /*-103*/:
                Toast.makeText(this, message.obj.toString(), 1).show();
                removeDialog(0);
                if (this instanceof GameActivity) {
                    z = true;
                    break;
                } else {
                    return;
                }
            case MSG_PARSE_SERVER_MSG /*-100*/:
                if (message.obj instanceof Msg) {
                    factory.getReceiveMsgHandle().parseMsg((Msg) message.obj);
                    return;
                }
                return;
        }
        if (z) {
            handleMessage(message);
        }
    }

    protected static void setBackgroudDrawable(Resources resources, ViewGroup viewGroup) {
        if (viewGroup != null) {
            if (drawBg == null) {
                Bitmap decodeResource = BitmapFactory.decodeResource(resources, R.drawable.bg);
                drawBg = new BitmapDrawable(Bitmap.createBitmap(decodeResource, 0, 12, (int) LordView.SCREEN_HEIGHT, 455));
                decodeResource.recycle();
            }
            viewGroup.setBackgroundDrawable(drawBg);
        }
    }

    public void adjustBright(float f) {
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.screenBrightness = 0.1f + (0.9f * f);
        getWindow().setAttributes(attributes);
    }

    /* access modifiers changed from: protected */
    public final boolean createGeneralMenu(Menu menu) {
        menu.add(0, 0, 0, (int) R.string.menu_setting).setIcon((int) R.drawable.menu_setting);
        menu.add(0, 1, 0, (int) R.string.menu_intro).setIcon((int) R.drawable.menu_help);
        menu.add(0, 2, 0, (int) R.string.menu_exit).setIcon((int) R.drawable.menu_exit);
        return true;
    }

    /* access modifiers changed from: protected */
    public abstract void handleMessage(Message message);

    public void onCancel(DialogInterface dialogInterface) {
        this.handle.sendEmptyMessage(MSG_EXIT_LOGIN);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (res == null) {
            res = getResources();
        }
        if (factory == null) {
            factory = new FactoryCenter();
        }
        if (this.screen_type == 1) {
            setRequestedOrientation(1);
        } else if (this.screen_type == 2) {
            setRequestedOrientation(0);
        }
        requestWindowFeature(1);
        if (soundPlayer == null) {
            SysSetting.getInstance(this).load();
            soundPlayer = new SoundPlayer(getApplicationContext());
            soundPlayer.playWelcomeBackGroundSound();
        }
        Log.w(this.TAG, "onCreate..");
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        Dialog dialog = null;
        switch (i) {
            case 0:
            case 1:
                dialog = new Dialog(this, R.style.Theme_InfoDialog);
                dialog.setContentView(new LoadingView(this, dialog, this.progressDlgMsg));
                dialog.setCancelable(true);
                dialog.setOnCancelListener(this);
                dialog.show();
                break;
        }
        this.curr_dialog_key = i;
        return dialog;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Log.w(this.TAG, "onDestroy..");
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i == 3 && currentActivity != null) {
            if (!(currentActivity instanceof GameActivity)) {
                Report.keyHomeHitInHallNum++;
            } else {
                Report.keyHomeHitInGameNum++;
            }
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 0:
                Report.menuitemSysteSettingHitNum++;
                startActivity(new Intent("com.tencent.qqgame.hall.ui.SysSetting"));
                break;
            case 1:
                Report.menuitemHelpAboutHitNum++;
                startActivity(new Intent("com.tencent.qqgame.hall.ui.HelpAbout"));
                break;
            case 2:
                Report.menuitemExitHitNum++;
                isExitingApp = true;
                if (!(this instanceof LoginActivity)) {
                    finish();
                    break;
                } else {
                    Process.killProcess(Process.myPid());
                    break;
                }
            case 3:
                Report.menuitemQuickGameHitNum++;
                showDialog(0, res.getString(R.string.game_searchQuicking));
                factory.getSendMsgHandle().quickPlay(0);
                break;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        disConnectHandler.removeCallbacks(disConnectTask);
        disConnectHandler.postDelayed(disConnectTask, 1800000);
        disConnectHandler.removeCallbacks(disBackGroundSoundTask);
        disConnectHandler.postDelayed(disBackGroundSoundTask, 3000);
        if (!(this.curr_dialog_key == -1 || this.curr_dialog_key == 1)) {
            removeDialog(this.curr_dialog_key);
            this.curr_dialog_key = -1;
        }
        Log.w(this.TAG, "onPause..");
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        Log.w(this.TAG, "onRestart..");
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        Log.w(this.TAG, "onResume..");
        Log.i(this.TAG, "isTaskRoot:" + isTaskRoot());
        Log.i(this.TAG, "isExitingApp:" + isExitingApp);
        if (isExitingApp) {
            if (this instanceof LoginActivity) {
                Process.killProcess(Process.myPid());
            } else {
                finish();
            }
        }
        currentActivity = this;
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        if (!(defaultDisplay.getWidth() == Tools.screenWidth && defaultDisplay.getHeight() == Tools.screenHeight)) {
            Tools.screenWidth = defaultDisplay.getWidth();
            Tools.screenHeight = defaultDisplay.getHeight();
        }
        if (this.curr_dialog_key != -1) {
            removeDialog(this.curr_dialog_key);
            this.curr_dialog_key = -1;
        }
        disConnectHandler.removeCallbacks(disConnectTask);
        disConnectHandler.removeCallbacks(disBackGroundSoundTask);
        if (soundPlayer != null) {
            soundPlayer.startBackGroundSound();
        }
        SysSetting.getInstance(this).load();
        adjustBright(SysSetting.getInstance(this).getBright());
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        Log.w(this.TAG, "SIS called");
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        Log.w(this.TAG, "onStart..");
        super.onStart();
        this.handle.removeCallbacks(disConnectTask);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        Log.w(this.TAG, "onStop..");
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void setToolbarTitle(String str) {
        ((TextView) findViewById(R.id.toolbar_tv_title)).setText(str);
    }

    public void showDialog(int i, String str) {
        this.progressDlgMsg = str;
        showDialog(i);
    }

    /* access modifiers changed from: protected */
    public final void showExitDialog() {
        new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.game_tips_title).setMessage(res.getString(R.string.login_error_getserverlist)).setCancelable(false).setPositiveButton((int) R.string.menu_exit, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                AActivity.isExitingApp = true;
                if (AActivity.currentActivity instanceof LoginActivity) {
                    Process.killProcess(Process.myPid());
                } else {
                    AActivity.this.finish();
                }
            }
        }).show();
    }
}
