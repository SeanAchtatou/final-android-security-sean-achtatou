package com.tencent.qqgame.hall.common;

public interface IGameInfo {
    byte[] getGameCommonInfo(byte[] bArr);

    String getLevelName(int i);
}
