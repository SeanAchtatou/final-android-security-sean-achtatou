package com.tencent.qqgame.hall.protocol;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.tencent.qqgame.common.Util;
import com.tencent.qqgame.hall.common.AActivity;
import com.tencent.qqgame.hall.common.FactoryCenter;
import com.tencent.qqgame.hall.common.SyncData;
import com.tencent.qqgame.hall.common.Tools;
import com.tencent.qqgame.hall.common.data.MProxyInfoV2;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

public class SendHallHttpMsgHandle {
    public static final byte APNTYPE_CMNET = 1;
    public static final byte APNTYPE_CMWAP = 2;
    public static final byte APNTYPE_NET = 6;
    public static final byte APNTYPE_NONE = 0;
    public static final byte APNTYPE_UNINET = 4;
    public static final byte APNTYPE_UNIWAP = 5;
    public static final byte APNTYPE_WAP = 7;
    public static final byte APNTYPE_WIFI = 3;
    public static final byte ARRAY2 = 1;
    public static final byte ARRAY4 = 2;
    public static final short CMD_START_CS_V4 = 27;
    public static final short MSG_ID_GAME_UNIT_REQ = 5;
    public static final short MSG_ID_MIX_REQ = 13;
    public static final short MSG_ID_REPORT_REQ = 12;
    public static final short MSG_ID_START_V2_REQ = 17;
    public static final short MSG_ID_SYSTEM_MSG_REQ = 8;
    public static final short MSG_ID_TAB_REQ = 3;
    public static final short MSG_ID_UUID_REQ = 14;
    private static final byte PLATFORM = 65;
    private static final short PROTOCOL_VERION = 3;
    public static CmdTimeStampInfo[] StampInfo = null;
    public static final byte endFlag = 3;
    private static int msg_Head_Len = 0;
    private static byte[] reserve = new byte[1];
    private final String APNName;
    private final byte APNType;
    private final String Channel;
    private String UA_deviceName = "android";
    protected FactoryCenter factory;
    private final String netWorkType;

    public SendHallHttpMsgHandle(FactoryCenter factoryCenter) {
        byte b;
        String str;
        String str2;
        this.factory = factoryCenter;
        StampInfo = new CmdTimeStampInfo[4];
        StampInfo[0] = new CmdTimeStampInfo(5, 0);
        StampInfo[1] = new CmdTimeStampInfo(3, 0);
        StampInfo[2] = new CmdTimeStampInfo(8, 0);
        StampInfo[3] = new CmdTimeStampInfo(13, 0);
        switch (MProxyInfoV2.getNetWorkType(AActivity.currentActivity)) {
            case 1:
                str = "cmwap";
                b = 2;
                break;
            case 2:
                str = "wifi";
                b = 3;
                break;
            case 4:
                str = "cmnet";
                b = 1;
                break;
            case 8:
                str = "uninet";
                b = 4;
                break;
            case 16:
                b = 5;
                str = "uniwap";
                break;
            case MProxyInfoV2.MPROXYTYPE_NET:
                b = 6;
                str = "net";
                break;
            case MProxyInfoV2.MPROXYTYPE_WAP:
                b = 7;
                str = "wap";
                break;
            default:
                str = "unknown";
                b = 0;
                break;
        }
        this.APNType = b;
        this.APNName = str;
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) AActivity.currentActivity.getSystemService("connectivity")).getActiveNetworkInfo();
            str2 = activeNetworkInfo.getTypeName().toUpperCase().equals("WIFI") ? "wifi" : activeNetworkInfo.getSubtypeName();
        } catch (Exception e) {
            str2 = "";
        }
        this.netWorkType = str2;
        this.Channel = Integer.toString(1213621171);
        Tools.debug("Channel: " + this.Channel);
        msg_Head_Len = getMsgHeadLen();
    }

    private byte[] arrayToByte() {
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
            dataOutputStream.writeShort((short) StampInfo.length);
            for (int i = 0; i < StampInfo.length; i++) {
                dataOutputStream.writeShort(StampInfo[i].cmd);
                dataOutputStream.writeInt(StampInfo[i].timeStamp);
            }
            return byteArrayOutputStream.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            return new byte[0];
        }
    }

    private int bytesCopyToBuf(byte[] bArr, int i, byte[] bArr2, byte b) {
        if (b == 2) {
            int length = bArr2.length;
            Util.word2Byte(bArr, i, length);
            System.arraycopy(bArr2, 0, bArr, i + 4, length);
            return bArr.length + 4;
        }
        short length2 = (short) bArr2.length;
        Util.word2Byte(bArr, i, length2);
        System.arraycopy(bArr2, 0, bArr, i + 2, length2);
        return bArr.length + 2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
     arg types: [byte[], int, short]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
     arg types: [byte[], int, int]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void */
    private byte[] fixHead(short s) {
        byte[] bArr = new byte[s];
        bArr[0] = 2;
        int i = 0 + 1;
        Util.word2Byte(bArr, i, (int) s);
        int i2 = i + 4;
        Util.word2Byte(bArr, i2, (short) 3);
        int i3 = i2 + 2;
        Util.word2Byte(bArr, i3, (int) SyncData.VERSION_S);
        int i4 = i3 + 4;
        bArr[i4] = PLATFORM;
        int string2UTF8Bytes2Buf = string2UTF8Bytes2Buf(bArr, i4 + 1, this.Channel, (byte) 1) + 12;
        int string2UTF8Bytes2Buf2 = string2UTF8Bytes2Buf + string2UTF8Bytes2Buf(bArr, string2UTF8Bytes2Buf, SyncData.UUID, (byte) 1);
        int string2UTF8Bytes2Buf3 = string2UTF8Bytes2Buf2 + string2UTF8Bytes2Buf(bArr, string2UTF8Bytes2Buf2, this.UA_deviceName, (byte) 1);
        Util.word2Byte(bArr, string2UTF8Bytes2Buf3, Util.convert2Unsigned(SyncData.curUin));
        int i5 = string2UTF8Bytes2Buf3 + 4;
        Util.word2Byte(bArr, i5, (int) SyncData.gameType);
        int i6 = i5 + 4;
        int i7 = i6 + 1;
        bArr[i6] = this.APNType;
        int string2UTF8Bytes2Buf4 = string2UTF8Bytes2Buf(bArr, i7, this.netWorkType, (byte) 1) + i7;
        int string2UTF8Bytes2Buf5 = string2UTF8Bytes2Buf4 + string2UTF8Bytes2Buf(bArr, string2UTF8Bytes2Buf4, this.APNName, (byte) 1);
        return bArr;
    }

    private static int getBytesCopyLength(byte[] bArr, byte b) {
        return b == 2 ? bArr.length + 4 : ((short) bArr.length) + 2;
    }

    private int getMsgHeadLen() {
        return getUTF8BytesLengthAdd(this.Channel, (byte) 1) + 12 + getUTF8BytesLengthAdd(SyncData.UUID, (byte) 1) + getUTF8BytesLengthAdd(this.UA_deviceName, (byte) 1) + 4 + 1 + 4 + getUTF8BytesLengthAdd(this.netWorkType, (byte) 1) + getUTF8BytesLengthAdd(this.APNName, (byte) 1);
    }

    private static int getUTF8BytesLengthAdd(String str, byte b) {
        byte[] string2UTF8Bytes = string2UTF8Bytes(str);
        return b == 2 ? string2UTF8Bytes.length + 4 : ((short) string2UTF8Bytes.length) + 2;
    }

    private static byte[] string2UTF8Bytes(String str) {
        try {
            return str.getBytes("utf-8");
        } catch (Exception e) {
            try {
                return str.getBytes("UTF-8");
            } catch (Exception e2) {
                return str.getBytes();
            }
        }
    }

    private int string2UTF8Bytes2Buf(byte[] bArr, int i, String str, byte b) {
        byte[] string2UTF8Bytes = string2UTF8Bytes(str);
        if (b == 2) {
            int length = string2UTF8Bytes.length;
            Util.word2Byte(bArr, i, length);
            System.arraycopy(string2UTF8Bytes, 0, bArr, i + 4, length);
            return string2UTF8Bytes.length + 4;
        }
        short length2 = (short) string2UTF8Bytes.length;
        Util.word2Byte(bArr, i, length2);
        System.arraycopy(string2UTF8Bytes, 0, bArr, i + 2, length2);
        return string2UTF8Bytes.length + 2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
     arg types: [byte[], int, int]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void */
    public void sendReport(String str) {
        msg_Head_Len = getMsgHeadLen();
        int uTF8BytesLengthAdd = getUTF8BytesLengthAdd(str, (byte) 2);
        byte[] fixHead = fixHead((short) (msg_Head_Len + 2 + uTF8BytesLengthAdd + 1));
        Util.word2Byte(fixHead, msg_Head_Len, (short) 12);
        string2UTF8Bytes2Buf(fixHead, msg_Head_Len + 2, str, (byte) 2);
        fixHead[uTF8BytesLengthAdd + msg_Head_Len + 2] = 3;
        this.factory.getHttpCommunicator().sendMsg(fixHead);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
     arg types: [byte[], int, int]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void */
    public void start_V4() {
        msg_Head_Len = getMsgHeadLen();
        byte[] fixHead = fixHead((short) (msg_Head_Len + 2 + 2 + 4 + 1));
        Util.word2Byte(fixHead, msg_Head_Len, (short) 27);
        Util.word2Byte(fixHead, msg_Head_Len + 2, (short) 0);
        Util.word2Byte(fixHead, msg_Head_Len + 2 + 2, 0);
        fixHead[2 + msg_Head_Len + 2 + 4] = 3;
        this.factory.getHttpCommunicator().sendMsg(fixHead);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void
     arg types: [byte[], int, int]
     candidates:
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, int):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, long):void
      com.tencent.qqgame.common.Util.word2Byte(byte[], int, short):void */
    public void uuidReq() {
        msg_Head_Len = getMsgHeadLen();
        byte[] fixHead = fixHead((short) (msg_Head_Len + 2 + 0 + 1));
        Util.word2Byte(fixHead, msg_Head_Len, (short) 14);
        fixHead[msg_Head_Len + 2] = 3;
        this.factory.getHttpCommunicator().sendMsg(fixHead);
    }
}
