package com.tencent.qqgame.hall.common;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import com.tencent.qqgame.hall.ui.LoginActivity;
import com.tencent.qqgame.lord.ui.LordView;

public class SpriteV1 {
    public short[][] actionData;
    private int actionDataCount;
    public int[] alphas;
    public Bitmap bitmap;
    public Rect[] bounds;
    private Context context;
    private byte[] data;
    public short[][] frames;
    private int framesCount;
    private int imageId;
    public short[] modules;
    public short[] numOfFramesInAction;
    public int[] numOfModulesInFrame;
    private int position;

    public SpriteV1(Context context2, int i) {
        this.context = context2;
        open(i);
    }

    private void readBytes(short[] sArr, byte[] bArr, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            sArr[i2] = readUInt8(bArr);
        }
    }

    private short readInt16(byte[] bArr) {
        short s = (short) (((bArr[this.position + 1] & LoginActivity.STATUS_NONE) << 8) | (bArr[this.position + 0] & LoginActivity.STATUS_NONE));
        this.position += 2;
        return s;
    }

    private int readInt32(byte[] bArr) {
        byte b = ((bArr[this.position + 3] & LoginActivity.STATUS_NONE) << LordView.FRAME_BUTTON_1FEN_DISABLE) | ((bArr[this.position + 2] & LoginActivity.STATUS_NONE) << 16) | ((bArr[this.position + 1] & LoginActivity.STATUS_NONE) << 8) | (bArr[this.position + 0] & LoginActivity.STATUS_NONE);
        this.position += 4;
        return b;
    }

    private byte readInt8(byte[] bArr) {
        byte b = bArr[this.position];
        this.position++;
        return b;
    }

    private int readUInt16(byte[] bArr) {
        byte b = ((bArr[this.position + 1] & LoginActivity.STATUS_NONE) << 8) | (bArr[this.position + 0] & LoginActivity.STATUS_NONE);
        this.position += 2;
        return b;
    }

    private long readUInt32(byte[] bArr) {
        long j = (long) (((bArr[this.position + 3] & LoginActivity.STATUS_NONE) << LordView.FRAME_BUTTON_1FEN_DISABLE) | ((bArr[this.position + 2] & LoginActivity.STATUS_NONE) << 16) | ((bArr[this.position + 1] & LoginActivity.STATUS_NONE) << 8) | (bArr[this.position + 0] & LoginActivity.STATUS_NONE));
        this.position += 4;
        return j;
    }

    private short readUInt8(byte[] bArr) {
        short s = (short) (this.data[this.position] & LoginActivity.STATUS_NONE);
        this.position++;
        return s;
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x0040 A[SYNTHETIC, Splitter:B:30:0x0040] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0045 A[SYNTHETIC, Splitter:B:33:0x0045] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] loadData(int r7) {
        /*
            r6 = this;
            r4 = 0
            android.content.Context r0 = r6.context     // Catch:{ Exception -> 0x005f, all -> 0x003b }
            android.content.res.Resources r0 = r0.getResources()     // Catch:{ Exception -> 0x005f, all -> 0x003b }
            java.io.InputStream r0 = r0.openRawResource(r7)     // Catch:{ Exception -> 0x005f, all -> 0x003b }
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0063, all -> 0x0055 }
            r1.<init>()     // Catch:{ Exception -> 0x0063, all -> 0x0055 }
        L_0x0010:
            int r2 = r0.read()     // Catch:{ Exception -> 0x001b, all -> 0x005a }
            r3 = -1
            if (r2 == r3) goto L_0x002b
            r1.write(r2)     // Catch:{ Exception -> 0x001b, all -> 0x005a }
            goto L_0x0010
        L_0x001b:
            r2 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
        L_0x001f:
            if (r0 == 0) goto L_0x0024
            r0.close()     // Catch:{ IOException -> 0x004d }
        L_0x0024:
            if (r1 == 0) goto L_0x0029
            r1.close()     // Catch:{ IOException -> 0x004f }
        L_0x0029:
            r0 = r4
        L_0x002a:
            return r0
        L_0x002b:
            byte[] r2 = r1.toByteArray()     // Catch:{ Exception -> 0x001b, all -> 0x005a }
            if (r1 == 0) goto L_0x0034
            r1.close()     // Catch:{ IOException -> 0x0049 }
        L_0x0034:
            if (r0 == 0) goto L_0x0039
            r0.close()     // Catch:{ IOException -> 0x004b }
        L_0x0039:
            r0 = r2
            goto L_0x002a
        L_0x003b:
            r0 = move-exception
            r1 = r4
            r2 = r4
        L_0x003e:
            if (r1 == 0) goto L_0x0043
            r1.close()     // Catch:{ IOException -> 0x0051 }
        L_0x0043:
            if (r2 == 0) goto L_0x0048
            r2.close()     // Catch:{ IOException -> 0x0053 }
        L_0x0048:
            throw r0
        L_0x0049:
            r1 = move-exception
            goto L_0x0034
        L_0x004b:
            r0 = move-exception
            goto L_0x0039
        L_0x004d:
            r0 = move-exception
            goto L_0x0024
        L_0x004f:
            r0 = move-exception
            goto L_0x0029
        L_0x0051:
            r1 = move-exception
            goto L_0x0043
        L_0x0053:
            r1 = move-exception
            goto L_0x0048
        L_0x0055:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r4
            goto L_0x003e
        L_0x005a:
            r2 = move-exception
            r5 = r2
            r2 = r0
            r0 = r5
            goto L_0x003e
        L_0x005f:
            r0 = move-exception
            r0 = r4
            r1 = r4
            goto L_0x001f
        L_0x0063:
            r1 = move-exception
            r1 = r0
            r0 = r4
            goto L_0x001f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.hall.common.SpriteV1.loadData(int):byte[]");
    }

    public boolean open(int i) {
        this.data = loadData(i);
        this.position = 0;
        this.imageId = readUInt8(this.data);
        if (this.bitmap == null) {
            this.bitmap = BitmapFactory.decodeResource(this.context.getResources(), ResMapV1.MAP[this.imageId]);
        }
        int readUInt16 = readUInt16(this.data) * 8;
        this.modules = new short[readUInt16];
        readBytes(this.modules, this.data, readUInt16);
        int readUInt162 = readUInt16(this.data) >> 1;
        this.framesCount = readUInt162;
        this.frames = new short[readUInt162][];
        this.numOfModulesInFrame = new int[readUInt162];
        this.bounds = new Rect[readUInt162];
        this.alphas = new int[readUInt162];
        for (int i2 = 0; i2 < readUInt162; i2++) {
            short readInt16 = readInt16(this.data);
            short readInt162 = readInt16(this.data);
            this.bounds[i2] = new Rect(readInt16, readInt162, readInt16(this.data) + readInt16, readInt16(this.data) + readInt162);
            this.alphas[i2] = readUInt16(this.data);
            int readUInt163 = readUInt16(this.data);
            this.numOfModulesInFrame[i2] = readUInt163;
            int i3 = readUInt163 * 6;
            this.frames[i2] = new short[i3];
            readBytes(this.frames[i2], this.data, i3);
        }
        int readUInt8 = readUInt8(this.data);
        this.actionDataCount = readUInt8;
        this.actionData = new short[readUInt8][];
        this.numOfFramesInAction = new short[readUInt8];
        for (int i4 = 0; i4 < readUInt8; i4++) {
            short readUInt82 = readUInt8(this.data);
            this.numOfFramesInAction[i4] = readUInt82;
            this.actionData[i4] = new short[(readUInt82 * 3)];
            readBytes(this.actionData[i4], this.data, readUInt82 * 3);
        }
        return true;
    }

    public void recycle() {
        if (this.bitmap != null) {
            if (!this.bitmap.isRecycled()) {
                this.bitmap.recycle();
            }
            this.bitmap = null;
        }
        this.data = null;
        this.frames = null;
        this.modules = null;
        this.numOfModulesInFrame = null;
        this.bounds = null;
        this.alphas = null;
        this.actionData = null;
        this.numOfFramesInAction = null;
    }
}
