package com.tencent.qqgame.hall.common;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import java.io.File;

public class UpdateVersion implements Runnable {
    private static final String TMP_FILE_NAME = "TMPQQLord.apk";
    private final int BUFFER_SIZE = 4096;
    private final Context context;
    private boolean isRunning = false;
    private Thread thread;
    private final String updateUrl;

    public UpdateVersion(Context context2, String str) {
        this.context = context2;
        this.updateUrl = str;
    }

    public static void deleteDownloadFile(Context context2) {
        try {
            context2.deleteFile(TMP_FILE_NAME);
        } catch (Exception e) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        r1.flush();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0080, code lost:
        if (r1 == null) goto L_0x0085;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r1.close();
     */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x006e A[SYNTHETIC, Splitter:B:28:0x006e] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0073 A[SYNTHETIC, Splitter:B:31:0x0073] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0097 A[SYNTHETIC, Splitter:B:50:0x0097] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x009c A[SYNTHETIC, Splitter:B:53:0x009c] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00a1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean downloadFile() {
        /*
            r11 = this;
            r8 = 1
            r7 = 0
            r6 = 0
            r0 = 0
            r1 = 0
            r2 = 4096(0x1000, float:5.74E-42)
            byte[] r2 = new byte[r2]
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ca, all -> 0x0091 }
            r3.<init>()     // Catch:{ Exception -> 0x00ca, all -> 0x0091 }
            java.lang.String r4 = "updateUrl:"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00ca, all -> 0x0091 }
            java.lang.String r4 = r11.updateUrl     // Catch:{ Exception -> 0x00ca, all -> 0x0091 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x00ca, all -> 0x0091 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x00ca, all -> 0x0091 }
            com.tencent.qqgame.hall.common.Tools.debug(r3)     // Catch:{ Exception -> 0x00ca, all -> 0x0091 }
            java.net.URL r3 = new java.net.URL     // Catch:{ Exception -> 0x00ca, all -> 0x0091 }
            java.lang.String r4 = r11.updateUrl     // Catch:{ Exception -> 0x00ca, all -> 0x0091 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x00ca, all -> 0x0091 }
            r4 = 0
            java.net.HttpURLConnection r3 = com.tencent.qqgame.hall.communication.HttpCommunicator.getHttpConnection(r3, r4)     // Catch:{ Exception -> 0x00ca, all -> 0x0091 }
            if (r3 != 0) goto L_0x0040
            if (r7 == 0) goto L_0x0034
            r0.close()     // Catch:{ Exception -> 0x00a5 }
        L_0x0034:
            if (r7 == 0) goto L_0x0039
            r1.close()     // Catch:{ Exception -> 0x00a7 }
        L_0x0039:
            if (r3 == 0) goto L_0x003e
            r3.disconnect()
        L_0x003e:
            r0 = r6
        L_0x003f:
            return r0
        L_0x0040:
            java.io.BufferedInputStream r0 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x00cf, all -> 0x00b5 }
            java.io.InputStream r1 = r3.getInputStream()     // Catch:{ Exception -> 0x00cf, all -> 0x00b5 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x00cf, all -> 0x00b5 }
            android.content.Context r1 = r11.context     // Catch:{ Exception -> 0x00d4, all -> 0x00ba }
            java.lang.String r4 = "TMPQQLord.apk"
            r5 = 1
            java.io.FileOutputStream r1 = r1.openFileOutput(r4, r5)     // Catch:{ Exception -> 0x00d4, all -> 0x00ba }
        L_0x0052:
            int r4 = r0.read(r2)     // Catch:{ Exception -> 0x0062, all -> 0x00c0 }
            r5 = -1
            if (r4 == r5) goto L_0x007d
            boolean r5 = r11.isRunning     // Catch:{ Exception -> 0x0062, all -> 0x00c0 }
            if (r5 == 0) goto L_0x007d
            r5 = 0
            r1.write(r2, r5, r4)     // Catch:{ Exception -> 0x0062, all -> 0x00c0 }
            goto L_0x0052
        L_0x0062:
            r2 = move-exception
            r9 = r2
            r2 = r0
            r0 = r9
            r10 = r3
            r3 = r1
            r1 = r10
        L_0x0069:
            r0.printStackTrace()     // Catch:{ all -> 0x00c8 }
            if (r3 == 0) goto L_0x0071
            r3.close()     // Catch:{ Exception -> 0x00ad }
        L_0x0071:
            if (r2 == 0) goto L_0x0076
            r2.close()     // Catch:{ Exception -> 0x00af }
        L_0x0076:
            if (r1 == 0) goto L_0x007b
            r1.disconnect()
        L_0x007b:
            r0 = r6
            goto L_0x003f
        L_0x007d:
            r1.flush()     // Catch:{ Exception -> 0x0062, all -> 0x00c0 }
            if (r1 == 0) goto L_0x0085
            r1.close()     // Catch:{ Exception -> 0x00a9 }
        L_0x0085:
            if (r0 == 0) goto L_0x008a
            r0.close()     // Catch:{ Exception -> 0x00ab }
        L_0x008a:
            if (r3 == 0) goto L_0x008f
            r3.disconnect()
        L_0x008f:
            r0 = r8
            goto L_0x003f
        L_0x0091:
            r0 = move-exception
            r1 = r7
            r2 = r7
            r3 = r7
        L_0x0095:
            if (r3 == 0) goto L_0x009a
            r3.close()     // Catch:{ Exception -> 0x00b1 }
        L_0x009a:
            if (r2 == 0) goto L_0x009f
            r2.close()     // Catch:{ Exception -> 0x00b3 }
        L_0x009f:
            if (r1 == 0) goto L_0x00a4
            r1.disconnect()
        L_0x00a4:
            throw r0
        L_0x00a5:
            r0 = move-exception
            goto L_0x0034
        L_0x00a7:
            r0 = move-exception
            goto L_0x0039
        L_0x00a9:
            r1 = move-exception
            goto L_0x0085
        L_0x00ab:
            r0 = move-exception
            goto L_0x008a
        L_0x00ad:
            r0 = move-exception
            goto L_0x0071
        L_0x00af:
            r0 = move-exception
            goto L_0x0076
        L_0x00b1:
            r3 = move-exception
            goto L_0x009a
        L_0x00b3:
            r2 = move-exception
            goto L_0x009f
        L_0x00b5:
            r0 = move-exception
            r1 = r3
            r2 = r7
            r3 = r7
            goto L_0x0095
        L_0x00ba:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            r3 = r7
            goto L_0x0095
        L_0x00c0:
            r2 = move-exception
            r9 = r2
            r2 = r0
            r0 = r9
            r10 = r3
            r3 = r1
            r1 = r10
            goto L_0x0095
        L_0x00c8:
            r0 = move-exception
            goto L_0x0095
        L_0x00ca:
            r0 = move-exception
            r1 = r7
            r2 = r7
            r3 = r7
            goto L_0x0069
        L_0x00cf:
            r0 = move-exception
            r1 = r3
            r2 = r7
            r3 = r7
            goto L_0x0069
        L_0x00d4:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r3
            r3 = r7
            goto L_0x0069
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.hall.common.UpdateVersion.downloadFile():boolean");
    }

    private boolean installSoftWare() {
        File file;
        Throwable th;
        Exception exc;
        try {
            File fileStreamPath = this.context.getFileStreamPath(TMP_FILE_NAME);
            if (fileStreamPath != null) {
                try {
                    if (this.isRunning) {
                        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(fileStreamPath.getAbsolutePath()));
                        intent.addFlags(1);
                        intent.setClassName("com.android.packageinstaller", "com.android.packageinstaller.PackageInstallerActivity");
                        this.context.startActivity(intent);
                    }
                } catch (Exception e) {
                    Exception exc2 = e;
                    file = fileStreamPath;
                    exc = exc2;
                    try {
                        exc.printStackTrace();
                        if (file != null) {
                        }
                        return false;
                    } catch (Throwable th2) {
                        th = th2;
                        if (file != null) {
                        }
                        throw th;
                    }
                } catch (Throwable th3) {
                    Throwable th4 = th3;
                    file = fileStreamPath;
                    th = th4;
                    if (file != null) {
                    }
                    throw th;
                }
            }
            if (fileStreamPath != null) {
            }
            return true;
        } catch (Exception e2) {
            Exception exc3 = e2;
            file = null;
            exc = exc3;
        } catch (Throwable th5) {
            Throwable th6 = th5;
            file = null;
            th = th6;
            if (file != null) {
            }
            throw th;
        }
    }

    public void interruptUpdate() {
        this.isRunning = false;
        if (this.thread != null) {
            this.thread.interrupt();
        }
        this.thread = null;
    }

    public void run() {
        if (!this.isRunning) {
            return;
        }
        if (!downloadFile()) {
            AActivity.currentActivity.handle.sendEmptyMessage(15);
        } else if (!installSoftWare()) {
            AActivity.currentActivity.handle.sendEmptyMessage(15);
        }
    }

    public void update() {
        interruptUpdate();
        this.thread = new Thread(this);
        this.isRunning = true;
        this.thread.start();
    }
}
