package com.tencent.qqgame.hall.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Message;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;
import com.tencent.qqgame.common.Msg;
import com.tencent.qqgame.common.Player;
import com.tencent.qqgame.common.Table;
import com.tencent.qqgame.common.Util;
import com.tencent.qqgame.hall.common.AActivity;
import com.tencent.qqgame.hall.common.AnimationPlayerV1;
import com.tencent.qqgame.hall.common.IGameView;
import com.tencent.qqgame.hall.common.Report;
import com.tencent.qqgame.hall.common.SpriteV1;
import com.tencent.qqgame.hall.common.SyncData;
import com.tencent.qqgame.hall.common.Tools;
import com.tencent.qqgame.hall.ui.controls.TableCellControl;
import com.tencent.qqgame.hall.ui.controls.TableScrollView;
import com.tencent.qqgame.hall.ui.controls.UserInfoControl;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import tencent.qqgame.lord.R;

public class TableListActivity extends AActivity implements View.OnClickListener {
    public static final short DESKS_OF_PAGE = 20;
    public static final byte FRAME_TABLE_BULUE = 5;
    public static final byte FRAME_TABLE_CARDS = 3;
    public static final byte FRAME_TABLE_DARK = 1;
    public static final byte FRAME_TABLE_HAND = 4;
    public static final byte FRAME_TABLE_LIGHT = 0;
    public static final short MAX_DESKS = 100;
    public static final int MSG_CLOSE_INFO_DIALOG = 3;
    public static final int MSG_ENTER_ROOM = 1;
    public static final int MSG_SHOW_USER_INFO = 7;
    public static final int MSG_SITDOWN_ERROR = 2;
    public static final int MSG_VIEW_GAME = 4;
    public static final int MSG_VIEW_GAME_ERROR = 5;
    public static final int MSG_VIEW_GAME_SUCC = 6;
    private final short SYNC_TIME = 10000;
    public UserInfoControl ctlUserInfo;
    private short curPage;
    private int curRoomID;
    private int curRoomSvrID;
    private int curZoneID;
    public Bitmap imgBlue;
    public Bitmap imgCardBack;
    private Bitmap imgNum;
    public Bitmap imgReady;
    public Bitmap imgTable_dark;
    public Bitmap imgTable_light;
    private int pageHeight;
    private boolean[] pageRequested;
    public RelativeLayout rlTableList;
    private int scrollY;
    private TableScrollView sv;
    private Table[] tableList = null;
    private short timerOut = 120;
    private short timerSync = 10000;
    private TableLayout tl = null;
    private short totalItemInServer = 0;

    private boolean createDeskListMenu(Menu menu) {
        menu.add(0, 3, 0, (int) R.string.menu_quickGame).setIcon((int) R.drawable.menu_quickgame);
        createGeneralMenu(menu);
        return true;
    }

    private void decodeData(byte[] bArr) {
        short s;
        short s2;
        DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(bArr));
        try {
            dataInputStream.readShort();
            dataInputStream.readShort();
            long convertUnsigned2Long = Util.convertUnsigned2Long(dataInputStream.readInt());
            String string = Util.getString(dataInputStream, dataInputStream.readShort());
            byte readByte = dataInputStream.readByte();
            short readShort = dataInputStream.readShort();
            if (readShort % 3 != 0) {
                s2 = (short) (((short) (readShort / 3)) + 86);
            } else {
                short s3 = (short) (readShort / 3);
                s2 = s3 >= 100 ? 0 : s3;
            }
            SyncData.me = new Player(convertUnsigned2Long, string, readByte, 0, s2, dataInputStream.readInt(), dataInputStream.readInt(), dataInputStream.readInt(), dataInputStream.readShort(), dataInputStream.readShort(), (short) dataInputStream.readByte());
            this.totalItemInServer = dataInputStream.readShort();
            s = dataInputStream.readShort();
            Tools.debug("tableNum: " + ((int) s));
            for (int i = 0; i < s; i++) {
                short readShort2 = dataInputStream.readShort();
                dataInputStream.skipBytes(2);
                short readShort3 = dataInputStream.readShort();
                this.tableList[i] = new Table(SyncData.myZoneID, SyncData.myRoomID, SyncData.myRoomSvrID, readShort2);
                for (int i2 = 0; i2 < readShort3; i2++) {
                    Player playerInfo = Msg.getPlayerInfo(dataInputStream);
                    if (playerInfo != null) {
                        this.tableList[i].playerList.addElement(playerInfo);
                    }
                }
            }
        } catch (IOException e) {
            Tools.debug("decode enter room exception, detail info: " + e.getMessage());
            e.printStackTrace();
            s = 0;
        }
        if (s <= 0) {
        }
    }

    private int getRefreshPage(Table[] tableArr) {
        if (tableArr == null || tableArr.length == 0) {
            return -1;
        }
        int i = 0;
        while (true) {
            if (i >= this.tableList.length) {
                i = -1;
                break;
            } else if (this.tableList[i].tableId == tableArr[0].tableId) {
                break;
            } else {
                i++;
            }
        }
        return i != -1 ? i / 20 : i;
    }

    private void init() {
        this.tableList = null;
        this.tableList = new Table[100];
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < 100) {
                this.tableList[i2] = new Table(-1, -1, -1, -1);
                int i3 = 0;
                while (true) {
                    int i4 = i3;
                    if (i4 >= 3) {
                        break;
                    }
                    this.tableList[i2].playerList.addElement(new Player(-1, "", (byte) 0, -1, -1, 0, -1, 0, 0, 0, 0));
                    i3 = i4 + 1;
                }
                i = i2 + 1;
            } else {
                this.pageHeight = 1390;
                this.scrollY = this.sv.getScrollY();
                this.curPage = (short) (this.sv.getScrollY() / this.pageHeight);
                this.pageRequested = new boolean[5];
                this.pageRequested[this.curPage] = true;
                this.curZoneID = SyncData.myZoneID;
                this.curRoomID = SyncData.myRoomID;
                this.curRoomSvrID = SyncData.myRoomSvrID;
                return;
            }
        }
    }

    private void initTableItems() {
        TableRow tableRow = null;
        this.tl.removeAllViews();
        if (this.tableList != null) {
            int length = this.tableList.length;
            int i = 0;
            while (i < length) {
                if (i % 2 == 0) {
                    tableRow = new TableRow(this);
                    this.tl.addView(tableRow);
                }
                TableRow tableRow2 = tableRow;
                TableCellControl tableCellControl = new TableCellControl(this, factory, this.tableList[i], (short) (i + 1), this.imgNum);
                tableCellControl.setClickable(true);
                tableCellControl.setImage(this.imgTable_light, this.imgTable_dark, this.imgCardBack, this.imgReady);
                tableRow2.addView(tableCellControl);
                i++;
                tableRow = tableRow2;
            }
        }
    }

    public final Bitmap drawFrame2Bitmap(AnimationPlayerV1 animationPlayerV1, int i) {
        if (animationPlayerV1 == null || i < 0) {
            return null;
        }
        int frameWidth = animationPlayerV1.getFrameWidth(i);
        int frameHeight = animationPlayerV1.getFrameHeight(i);
        Canvas canvas = new Canvas();
        Bitmap createBitmap = Bitmap.createBitmap(frameWidth, frameHeight, Bitmap.Config.ARGB_8888);
        canvas.setBitmap(createBitmap);
        animationPlayerV1.drawFrame(canvas, i, 0, 0);
        return createBitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
    /* access modifiers changed from: protected */
    public void handleMessage(Message message) {
        switch (message.what) {
            case AActivity.MSG_REFRESH_DATA:
                Table[] tableArr = (Table[]) message.obj;
                if (tableArr != null && tableArr.length > 0) {
                    short refreshPage = (short) getRefreshPage(tableArr);
                    if (refreshPage < 0) {
                        refreshPage = this.curPage;
                    }
                    System.arraycopy(tableArr, 0, this.tableList, refreshPage * 20, tableArr.length);
                    initTableItems();
                    this.tl.invalidate();
                    this.sv.invalidate();
                }
                removeDialog(this.curr_dialog_key);
                return;
            case AActivity.MSG_GLOABLE_SLEEP:
                this.curPage = (short) (this.sv.getScrollY() / this.pageHeight);
                if ((this.curPage + 1) * this.pageHeight < this.sv.getScrollY() + (this.sv.getHeight() / 2)) {
                    this.curPage = (short) (this.curPage + 1);
                }
                if (this.scrollY == this.sv.getScrollY() && this.curPage < this.pageRequested.length && !this.pageRequested[this.curPage]) {
                    showDialog(0, res.getString(R.string.game_requetdesk));
                    this.timerSync = 0;
                    this.pageRequested[this.curPage] = true;
                }
                this.scrollY = this.sv.getScrollY();
                if (this.timerOut <= 0) {
                    factory.getSendMsgHandle().leaveRoom(SyncData.myRoomSvrID, SyncData.myRoomID);
                    Toast.makeText(this, (int) R.string.game_tablelist_timeout, 1).show();
                    finish();
                } else {
                    this.timerOut = (short) (this.timerOut - 1);
                }
                if (this.timerSync <= 0) {
                    factory.getSendMsgHandle().getTableList(SyncData.myZoneID, SyncData.myRoomID, SyncData.myRoomSvrID, this.curPage, 20, false);
                }
                this.timerSync = this.timerSync <= 0 ? 10000 : (short) (this.timerSync - IGameView.MSG_2GAME_DIS_CONNECTED);
                this.handle.sendMessageDelayed(this.handle.obtainMessage(AActivity.MSG_GLOABLE_SLEEP), 1000);
                return;
            case 1:
            default:
                return;
            case 2:
                removeDialog(this.curr_dialog_key);
                Toast.makeText(this, message.obj.toString(), 1).show();
                return;
            case 4:
                if (this.ctlUserInfo != null) {
                    this.rlTableList.removeView(this.ctlUserInfo);
                    this.ctlUserInfo.recycle();
                    this.ctlUserInfo = null;
                }
                showDialog(0, res.getString(R.string.game_request_view_game));
                SyncData.myTableID = (short) message.arg1;
                SyncData.mySeatID = (short) message.arg2;
                factory.getSendMsgHandle().viewGame(SyncData.myZoneID, SyncData.myRoomSvrID, SyncData.myRoomID, SyncData.myTableID, SyncData.mySeatID);
                return;
            case 5:
                Toast.makeText(this, (int) R.string.game_view_game_error, 1).show();
                removeDialog(this.curr_dialog_key);
                return;
            case 6:
                Intent intent = new Intent(this, GameActivity.class);
                intent.putExtra(GameActivity.KEY_GAME_FROM, (byte) 2);
                intent.putExtra(GameActivity.KEY_IS_VIEWGAME, true);
                startActivity(intent);
                return;
            case 7:
                Player player = (Player) message.obj;
                if (this.ctlUserInfo == null) {
                    this.ctlUserInfo = new UserInfoControl(this);
                    this.rlTableList.addView(this.ctlUserInfo);
                }
                Bundle data = message.getData();
                int[] lastTouchedXY = this.sv.getLastTouchedXY();
                this.ctlUserInfo.init(player, (short) message.arg1, lastTouchedXY[0], lastTouchedXY[1], data.getInt("X"), data.getInt("Y"));
                return;
        }
    }

    public void onClick(View view) {
        if (view.getId() == R.id.table_btn_quickplay) {
            Report.quickGameHitInDeskListNum++;
            showDialog(0, res.getString(R.string.game_searchQuicking));
            factory.getSendMsgHandle().quickPlay(0);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_table_list);
        AnimationPlayerV1 animationPlayerV1 = new AnimationPlayerV1(new SpriteV1(getApplicationContext(), R.raw.table_));
        this.imgTable_light = drawFrame2Bitmap(animationPlayerV1, 0);
        this.imgTable_dark = drawFrame2Bitmap(animationPlayerV1, 1);
        this.imgCardBack = drawFrame2Bitmap(animationPlayerV1, 3);
        this.imgReady = drawFrame2Bitmap(animationPlayerV1, 4);
        this.imgBlue = drawFrame2Bitmap(animationPlayerV1, 5);
        this.imgNum = BitmapFactory.decodeResource(getResources(), R.drawable.number);
        animationPlayerV1.recycle(true);
        this.sv = (TableScrollView) findViewById(R.id.svTableList);
        setBackgroudDrawable(res, this.sv);
        this.sv.act = this;
        this.rlTableList = (RelativeLayout) findViewById(R.id.rlTableList);
        Button button = (Button) findViewById(R.id.table_btn_quickplay);
        button.setOnClickListener(this);
        button.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    view.setBackgroundResource(R.drawable.quickstar_hghlight);
                    return false;
                } else if (motionEvent.getAction() != 1) {
                    return false;
                } else {
                    view.setBackgroundResource(R.drawable.quickstart_normal);
                    return false;
                }
            }
        });
        button.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View view, boolean z) {
                if (z) {
                    view.setBackgroundResource(R.drawable.quickstar_hghlight);
                } else {
                    view.setBackgroundResource(R.drawable.quickstart_normal);
                }
            }
        });
        init();
        byte[] byteArrayExtra = getIntent().getByteArrayExtra("Data");
        if (byteArrayExtra != null) {
            decodeData(byteArrayExtra);
        }
        this.tl = (TableLayout) findViewById(R.id.tlTableList);
        initTableItems();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return createDeskListMenu(menu);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (this.tableList != null) {
            this.tableList = null;
        }
        if (this.imgTable_light != null) {
            if (!this.imgTable_light.isRecycled()) {
                this.imgTable_light.recycle();
            }
            this.imgTable_light = null;
        }
        if (this.imgTable_dark != null) {
            if (!this.imgTable_dark.isRecycled()) {
                this.imgTable_dark.recycle();
            }
            this.imgTable_dark = null;
        }
        if (this.imgCardBack != null) {
            if (!this.imgCardBack.isRecycled()) {
                this.imgCardBack.recycle();
            }
            this.imgCardBack = null;
        }
        if (this.imgReady != null) {
            if (!this.imgReady.isRecycled()) {
                this.imgReady.recycle();
            }
            this.imgReady = null;
        }
        if (this.imgBlue != null) {
            if (!this.imgBlue.isRecycled()) {
                this.imgBlue.recycle();
            }
            this.imgBlue = null;
        }
        if (this.imgNum != null) {
            if (!this.imgNum.isRecycled()) {
                this.imgNum.recycle();
            }
            this.imgNum = null;
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        switch (i) {
            case 4:
                Report.keyBackHitInHallNum++;
                factory.getSendMsgHandle().leaveRoom(SyncData.myRoomSvrID, SyncData.myRoomID);
                break;
        }
        return super.onKeyDown(i, keyEvent);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        menuItem.getItemId();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.handle.removeMessages(AActivity.MSG_GLOABLE_SLEEP);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        if (!(this.curZoneID == SyncData.myZoneID && this.curRoomID == SyncData.myRoomID && this.curRoomSvrID == SyncData.myRoomSvrID)) {
            init();
        }
        factory.getSendMsgHandle().getTableList(SyncData.myZoneID, SyncData.myRoomID, SyncData.myRoomSvrID, this.curPage, 20, false);
        this.timerSync = 10000;
        this.handle.sendMessageDelayed(this.handle.obtainMessage(AActivity.MSG_GLOABLE_SLEEP), 1000);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.timerOut = 120;
        this.handle.sendMessageDelayed(this.handle.obtainMessage(AActivity.MSG_GLOABLE_SLEEP), 1000);
        setToolbarTitle(SyncData.myRoomName);
        super.onResume();
    }

    public void setTablesInfo(Table[] tableArr, boolean z) {
        if (z) {
        }
        if (tableArr != null) {
            this.tableList = tableArr;
        }
    }
}
