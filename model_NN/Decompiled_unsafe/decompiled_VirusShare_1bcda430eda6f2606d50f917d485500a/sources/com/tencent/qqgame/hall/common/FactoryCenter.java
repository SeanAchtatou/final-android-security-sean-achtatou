package com.tencent.qqgame.hall.common;

import com.tencent.qqgame.hall.communication.HttpCommunicator;
import com.tencent.qqgame.hall.communication.SocketCommunicator;
import com.tencent.qqgame.hall.protocol.ReceiveHallHttpMsgHandle;
import com.tencent.qqgame.hall.protocol.ReceiveMsgHandle;
import com.tencent.qqgame.hall.protocol.SendHallHttpMsgHandle;
import com.tencent.qqgame.hall.protocol.SendMsgHandle;

public class FactoryCenter {
    public SocketCommunicator _comm;
    public HttpCommunicator _httpComm;
    private ReceiveHallHttpMsgHandle _receiveHallHttpMsgHandle;
    private ReceiveMsgHandle _receiveMsgHandle;
    private SendHallHttpMsgHandle _sendHallHttpMsgHandle;
    private SendMsgHandle _sendMsgHandle;
    private SyncData _syncData;

    public SocketCommunicator getCommunicator() {
        if (this._comm == null) {
            this._comm = new SocketCommunicator(this);
        }
        return this._comm;
    }

    public HttpCommunicator getHttpCommunicator() {
        if (this._httpComm == null) {
            try {
                this._httpComm = new HttpCommunicator(this);
            } catch (Exception e) {
            }
        }
        return this._httpComm;
    }

    public ReceiveHallHttpMsgHandle getReceiveHallHttpMsgHandle() {
        if (this._receiveHallHttpMsgHandle == null) {
            this._receiveHallHttpMsgHandle = new ReceiveHallHttpMsgHandle(this);
        }
        return this._receiveHallHttpMsgHandle;
    }

    public ReceiveMsgHandle getReceiveMsgHandle() {
        if (this._receiveMsgHandle == null) {
            this._receiveMsgHandle = new ReceiveMsgHandle(this);
        }
        return this._receiveMsgHandle;
    }

    public SendHallHttpMsgHandle getSendHallHttpMsgHandle() {
        if (this._sendHallHttpMsgHandle == null) {
            this._sendHallHttpMsgHandle = new SendHallHttpMsgHandle(this);
        }
        return this._sendHallHttpMsgHandle;
    }

    public SendMsgHandle getSendMsgHandle() {
        if (this._sendMsgHandle == null) {
            this._sendMsgHandle = new SendMsgHandle(this);
        }
        return this._sendMsgHandle;
    }

    public SyncData getSync() {
        if (this._syncData == null) {
            this._syncData = new SyncData(this);
        }
        return this._syncData;
    }
}
