package com.tencent.qqgame.hall.ui.controls;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.MotionEvent;
import android.view.View;
import tencent.qqgame.lord.R;

public class LoadingView extends View {
    private static final int TEXT_MARGIN_LEFT = 50;
    private static final int TEXT_MARGIN_RIGHT = 40;
    private static final int TEXT_MARGIN_V = 10;
    private static Bitmap imgClose = null;
    static final int maxX = 32;
    static final int minX = 2;
    private Dialog dialog;
    private final RectF fRect = new RectF();
    private final int height;
    private String info;
    private boolean isDirectRight = true;
    private final Paint p = new Paint();
    private final Rect rect = new Rect();
    private final Rect rectClose = new Rect();
    int startX = 2;
    private final int textSize = 15;
    private final int width;

    public LoadingView(Context context, Dialog dialog2, String str) {
        super(context);
        this.p.setTextSize(15.0f);
        int measureText = (int) this.p.measureText(str);
        this.width = measureText + TEXT_MARGIN_LEFT + TEXT_MARGIN_RIGHT;
        this.height = 35;
        this.dialog = dialog2;
        this.info = str;
        this.rectClose.set(measureText + TEXT_MARGIN_LEFT, 0, this.width, this.height);
        if (imgClose == null) {
            imgClose = BitmapFactory.decodeResource(context.getResources(), R.drawable.connect_canel);
        }
    }

    public void draw(Canvas canvas) {
        this.p.setAntiAlias(true);
        this.p.setColor(-1);
        this.p.setStyle(Paint.Style.STROKE);
        this.fRect.set((float) 0, (float) 0, (float) this.width, (float) this.height);
        canvas.drawRoundRect(this.fRect, 10.0f, 10.0f, this.p);
        this.fRect.set((float) (0 + 1), (float) (0 + 1), (float) (this.width - 1), (float) (this.height - 1));
        canvas.drawRoundRect(this.fRect, 10.0f, 10.0f, this.p);
        this.p.setStyle(Paint.Style.FILL);
        this.p.setColor(-1090519040);
        this.fRect.set((float) (0 + 2), (float) (0 + 2), (float) (this.width - 2), (float) (this.height - 2));
        canvas.drawRoundRect(this.fRect, 8.0f, 8.0f, this.p);
        int i = this.startX;
        this.p.setColor(-14833949);
        int i2 = (this.height - 3) / 2;
        int i3 = i;
        for (byte b = 0; b < 4; b = (byte) (b + 1)) {
            this.rect.set(i3, i2, i3 + 3, i2 + 3);
            canvas.drawRect(this.rect, this.p);
            int i4 = 3 + 2;
            i3 += 5;
        }
        if (this.isDirectRight) {
            if (this.startX + 6 > 32) {
                this.isDirectRight = false;
                this.startX = 32;
            } else {
                this.startX = 6 + this.startX;
            }
        } else if (this.startX - 6 < 2) {
            this.isDirectRight = true;
            this.startX = 2;
        } else {
            this.startX -= 6;
        }
        canvas.drawBitmap(imgClose, (float) (this.width - 20), (float) ((this.height - imgClose.getHeight()) / 2), this.p);
        this.p.setColor(-1);
        canvas.drawText(this.info, (((float) this.width) - this.p.measureText(this.info)) / 2.0f, (float) (((this.height + 15) / 2) - 3), this.p);
        try {
            Thread.sleep(100);
        } catch (Exception e) {
        }
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        setMeasuredDimension(this.width, this.height);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (this.rectClose.contains((int) motionEvent.getX(), (int) motionEvent.getY()) && this.dialog != null) {
            this.dialog.cancel();
        }
        return super.onTouchEvent(motionEvent);
    }
}
