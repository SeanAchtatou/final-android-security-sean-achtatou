package com.tencent.qqgame.hall.common.data;

import android.app.Activity;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class MProxyInfoV2 {
    public static final int MPROXYTYPE_CMNET = 4;
    public static final int MPROXYTYPE_CMWAP = 1;
    public static final int MPROXYTYPE_DEFAULT = 128;
    public static final int MPROXYTYPE_NET = 32;
    public static final int MPROXYTYPE_UNINET = 8;
    public static final int MPROXYTYPE_UNIWAP = 16;
    public static final int MPROXYTYPE_WAP = 64;
    public static final int MPROXYTYPE_WIFI = 2;
    public boolean isTryed = false;
    public final byte netFlag;
    public final short onlineNum;
    public final int supportedType;
    public final String svrIP;
    public final short svrId;
    public final String svrName;
    public final short svrPort;

    public MProxyInfoV2(short s, String str, short s2, short s3, byte b, String str2, int i) {
        this.svrId = s;
        this.svrIP = str;
        this.svrPort = s2;
        this.onlineNum = s3;
        this.netFlag = b;
        this.svrName = str2;
        this.supportedType = i;
    }

    public static int getNetWorkType(Activity activity) {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) activity.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo.getTypeName().toUpperCase().equals("WIFI")) {
                return 2;
            }
            String lowerCase = activeNetworkInfo.getExtraInfo().toLowerCase();
            if (lowerCase.equals("cmwap")) {
                return 1;
            }
            if (lowerCase.equals("cmnet") || lowerCase.equals("epc.tmobile.com")) {
                return 4;
            }
            if (lowerCase.equals("uniwap")) {
                return 16;
            }
            if (lowerCase.equals("uninet")) {
                return 8;
            }
            if (lowerCase.equals("wap")) {
                return 64;
            }
            return lowerCase.equals("net") ? 32 : -1;
        } catch (Exception e) {
        }
    }

    public static boolean isCanUse(int i, int i2) {
        return (i & i2) == i2;
    }
}
