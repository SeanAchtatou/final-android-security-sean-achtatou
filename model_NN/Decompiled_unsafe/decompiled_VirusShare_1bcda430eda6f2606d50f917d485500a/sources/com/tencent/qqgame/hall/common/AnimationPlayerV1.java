package com.tencent.qqgame.hall.common;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import com.tencent.qqgame.common.Util;

public class AnimationPlayerV1 {
    public static final int[] DEVICE_TRANSFORMS = {0, 1, 2, 3, 4, 5, 6, 7};
    public static final int SPTR_BIT_FLIP_X = 1;
    public static final int SPTR_BIT_FLIP_Y = 0;
    public static final int SPTR_BIT_TRANSPOSE = 2;
    public static final int SPTR_FLIP_X = 2;
    public static final int SPTR_FLIP_Y = 1;
    public static final int SPTR_TRANSPOSE = 4;
    public static final int TRANS_MIRROR_X = 2;
    public static final int TRANS_MIRROR_X_ROT180 = 1;
    public static final int TRANS_MIRROR_X_ROT270 = 4;
    public static final int TRANS_MIRROR_X_ROT90 = 7;
    public static final int TRANS_NONE = 0;
    public static final int TRANS_ROT180 = 3;
    public static final int TRANS_ROT270 = 6;
    public static final int TRANS_ROT90 = 5;
    public static Paint painter = new Paint();
    private final Rect destRect = new Rect();
    private byte id = -1;
    public boolean m_bAutoHide = false;
    public boolean m_bEnabled = true;
    private boolean m_bFrameChanged;
    public boolean m_bLoop;
    private boolean m_bLooped;
    private boolean m_bReadyForUpdate;
    public boolean m_bReverse;
    private Rect m_bounds;
    public float m_fAlpha = 1.0f;
    private float m_fDrawDegrees;
    public float m_fScale = 1.0f;
    private int m_nActionId;
    private int m_nCurFrame;
    private int m_nDrawTransform;
    private int m_nFrameDur;
    private int m_nFrameId;
    private SpriteV1 m_pAnimation;
    private Bitmap mirror;
    private final Rect srcRect = new Rect();
    private int x = 0;
    private int y = 0;

    static {
        painter.setAntiAlias(true);
        painter.setDither(true);
    }

    public AnimationPlayerV1(SpriteV1 spriteV1) {
        this.m_pAnimation = spriteV1;
        this.m_nCurFrame = 0;
        this.m_nFrameId = 0;
        this.m_bLooped = false;
        this.m_bFrameChanged = false;
        this.m_bReadyForUpdate = false;
        this.m_bLoop = true;
        this.m_bReverse = false;
        this.m_nActionId = 0;
        this.m_fDrawDegrees = 0.0f;
        this.m_nDrawTransform = 0;
        this.m_bounds = new Rect();
        this.m_bounds.setEmpty();
    }

    private void adjustRect(Bitmap bitmap, Rect rect, Rect rect2, float f, int i) {
        if (bitmap != null && rect != null && rect2 != null) {
            rect_mirror(rect, bitmap.getWidth(), bitmap.getHeight(), 2);
            if (i == 2) {
                rect2.offset(-rect.width(), 0);
            } else if (i == 7) {
                rect2.offset(0, rect.height());
            } else if (i == 1) {
                rect2.offset(rect.width(), 0);
            } else if (i == 4) {
                rect2.offset(0, -rect.height());
            }
        }
    }

    private void drawBitmap(Canvas canvas, Rect rect, Rect rect2, int i, float f, float f2) {
        if (this.m_pAnimation.bitmap != null) {
            if (i != 0) {
                if (isMirror(i)) {
                    if (this.mirror == null) {
                        int width = this.m_pAnimation.bitmap.getWidth();
                        int height = this.m_pAnimation.bitmap.getHeight();
                        int[] iArr = new int[(width * height)];
                        this.m_pAnimation.bitmap.getPixels(iArr, 0, width, 0, 0, width, height);
                        this.mirror = Bitmap.createBitmap(Util.effect_mirror(iArr, width, height, (byte) 2), width, height, Bitmap.Config.ARGB_8888);
                    }
                    adjustRect(this.mirror, rect, rect2, f, i);
                }
                canvas.save();
                canvas.rotate(getTransformAngle(i), (float) rect2.left, (float) rect2.top);
                canvas.drawBitmap((this.mirror == null || !isMirror(i)) ? this.m_pAnimation.bitmap : this.mirror, rect, rect2, painter);
                canvas.restore();
                return;
            }
            canvas.drawBitmap(this.m_pAnimation.bitmap, rect, rect2, painter);
        }
    }

    private float getTransformAngle(int i) {
        switch (i) {
            case 1:
                return 180.0f;
            case 2:
            default:
                return 0.0f;
            case 3:
                return 180.0f;
            case 4:
                return 90.0f;
            case 5:
                return 90.0f;
            case 6:
                return 270.0f;
            case 7:
                return 270.0f;
        }
    }

    private boolean isMirror(int i) {
        return i == 2 || i == 1 || i == 4 || i == 7;
    }

    private int readInt16X(short[] sArr, int i) {
        return (sArr[i] & 255) | ((sArr[i + 1] & 255) << 8);
    }

    private void rect_mirror(Rect rect, int i, int i2, int i3) {
        if (rect != null && i3 == 2) {
            int i4 = rect.left;
            rect.left = i - rect.right;
            rect.right = i - i4;
        }
    }

    public boolean IsEnabled() {
        return this.m_bEnabled;
    }

    public void calculateBoundingBox() {
        this.m_bounds.setEmpty();
        this.m_bounds.intersect(this.m_pAnimation.bounds[this.m_nFrameId]);
    }

    public void clear() {
        this.m_pAnimation = null;
    }

    public int currentFrameID() {
        return this.m_nFrameId;
    }

    public void draw(int i, int i2) {
    }

    public void draw(Canvas canvas) {
        drawFrame(canvas, this.x, this.y);
    }

    public void drawFrame(Canvas canvas, int i) {
        drawFrame(canvas, i, null, 0, 0, 0.0f, 0);
    }

    public void drawFrame(Canvas canvas, int i, int i2) {
        drawFrame(canvas, this.m_nFrameId, null, i, i2, 0.0f, 0);
    }

    public void drawFrame(Canvas canvas, int i, int i2, int i3) {
        drawFrame(canvas, i, null, i2, i3, 0.0f, 0);
    }

    public void drawFrame(Canvas canvas, int i, Rect rect, int i2, int i3, float f, int i4) {
        if (i >= 0 && this.m_pAnimation.bitmap != null) {
            short[] sArr = this.m_pAnimation.frames[i];
            int i5 = this.m_pAnimation.numOfModulesInFrame[i];
            if (f != 0.0f) {
                canvas.save();
                canvas.rotate(f, (float) i2, (float) i3);
            }
            int i6 = 0;
            if (this.m_pAnimation.alphas[i] != 255) {
                i6 = painter.getAlpha();
                painter.setAlpha(this.m_pAnimation.alphas[i]);
            }
            int i7 = i6;
            int i8 = 0;
            int i9 = 0;
            while (i8 < i5) {
                int i10 = (sArr[i9] & 255) * 8;
                short s = sArr[i9 + 1] & 255;
                int readInt16X = readInt16X(sArr, i9 + 2);
                int readInt16X2 = readInt16X(sArr, i9 + 4);
                if (readInt16X >= 32768) {
                    readInt16X -= 65536;
                }
                if (readInt16X2 >= 32768) {
                    readInt16X2 -= 65536;
                }
                int readInt16X3 = readInt16X(this.m_pAnimation.modules, i10);
                int readInt16X4 = readInt16X(this.m_pAnimation.modules, i10 + 2);
                int readInt16X5 = readInt16X(this.m_pAnimation.modules, i10 + 4);
                int readInt16X6 = readInt16X(this.m_pAnimation.modules, i10 + 6);
                int i11 = readInt16X + i2;
                int i12 = readInt16X2 + i3;
                int i13 = DEVICE_TRANSFORMS[flipTransform(s, i4)];
                this.destRect.set(i11, i12, i11 + readInt16X5, i12 + readInt16X6);
                if (rect != null) {
                    this.destRect.intersect(rect);
                }
                this.srcRect.set(readInt16X3, readInt16X4, readInt16X3 + readInt16X5, readInt16X6 + readInt16X4);
                drawBitmap(canvas, this.srcRect, this.destRect, i13, f, this.m_fScale);
                i8++;
                i9 += 6;
            }
            if (this.m_pAnimation.alphas[i] != 255) {
                painter.setAlpha(i7);
            }
            if (f != 0.0f) {
                canvas.restore();
            }
        }
    }

    public int flipTransform(int i, int i2) {
        return (((i2 & 4) | ((i2 << (i >> 2)) & 2)) | ((i2 >> (i >> 2)) & 1)) ^ i;
    }

    public Rect getBounds(Rect rect) {
        Rect rect2 = new Rect(this.m_bounds);
        float f = this.m_fDrawDegrees;
        int i = this.m_nDrawTransform;
        int floatToIntBits = Float.floatToIntBits(f);
        if (floatToIntBits < 0) {
            floatToIntBits += 360;
        }
        if (floatToIntBits == 0 || floatToIntBits == 360) {
            new Rect(rect2);
        }
        return rect2;
    }

    public int getCurFrame() {
        return this.m_nCurFrame;
    }

    public Rect getFrameBound(int i) {
        return this.m_pAnimation.bounds[i];
    }

    public int getFrameHeight(int i) {
        if (i >= this.m_pAnimation.bounds.length) {
            return -1;
        }
        return this.m_pAnimation.bounds[i].height();
    }

    public int getFrameWidth(int i) {
        if (i >= this.m_pAnimation.bounds.length) {
            return -1;
        }
        return this.m_pAnimation.bounds[i].width();
    }

    public byte getID() {
        return this.id;
    }

    public boolean getIsReadyForUpdate() {
        return this.m_bReadyForUpdate;
    }

    public boolean isFrameOverNextTime(long j) {
        return true;
    }

    public void recycle(boolean z) {
        if (this.mirror != null) {
            if (!this.mirror.isRecycled()) {
                this.mirror.recycle();
            }
            this.mirror = null;
        }
        if (this.m_pAnimation != null && z) {
            this.m_pAnimation.recycle();
        }
    }

    public void reset() {
        reset(false);
    }

    public void reset(boolean z) {
        this.m_bReadyForUpdate = z;
        if (this.m_pAnimation != null) {
            this.m_nCurFrame = 0;
            setFrame(this.m_nCurFrame);
            this.m_bLooped = false;
            this.m_bFrameChanged = false;
        }
    }

    public void setActionFlag(int i, boolean z) {
        if (this.m_pAnimation != null) {
            this.m_nActionId = i;
            this.m_bLoop = z;
            if (this.m_bReverse) {
                this.m_nCurFrame = this.m_pAnimation.numOfFramesInAction[this.m_nActionId] - 1;
                this.m_nCurFrame = Math.max(0, this.m_nCurFrame);
            } else {
                this.m_nCurFrame = 0;
            }
            this.m_bLooped = false;
            setFrame(this.m_nCurFrame);
        }
    }

    public void setAutoHide(boolean z) {
        this.m_bAutoHide = z;
    }

    public void setEnabled(boolean z) {
        this.m_bEnabled = z;
    }

    public void setFrame(int i) {
        if (this.m_pAnimation != null) {
            short s = 0;
            if (this.m_nActionId < this.m_pAnimation.numOfFramesInAction.length) {
                s = this.m_pAnimation.numOfFramesInAction[this.m_nActionId];
            }
            if (i < s) {
                this.m_bFrameChanged = true;
                this.m_nCurFrame = i;
                this.m_nFrameId = this.m_pAnimation.actionData[this.m_nActionId][this.m_nCurFrame * 3] & 255;
                this.m_nFrameDur = readInt16X(this.m_pAnimation.actionData[this.m_nActionId], (this.m_nCurFrame * 3) + 1);
                calculateBoundingBox();
            }
        }
    }

    public void setID(byte b) {
        this.id = b;
    }

    public void setLocation(int i, int i2) {
        this.x = i;
        this.y = i2;
    }

    public boolean update(int i) {
        if (!this.m_bEnabled) {
            return false;
        }
        if (this.m_pAnimation == null) {
            return false;
        }
        if (!this.m_bLoop && this.m_bLooped) {
            return false;
        }
        this.m_nFrameDur -= i;
        this.m_bFrameChanged = true;
        this.m_bReadyForUpdate = true;
        this.m_bLooped = false;
        if (this.m_nFrameDur <= 0) {
            int i2 = this.m_nCurFrame;
            if (!this.m_bReverse) {
                if (this.m_nCurFrame >= this.m_pAnimation.numOfFramesInAction[this.m_nActionId] - 1) {
                    this.m_bLooped = true;
                    if (this.m_bLoop) {
                        this.m_nCurFrame = 0;
                    } else if (this.m_bAutoHide) {
                        this.m_bEnabled = false;
                    }
                } else {
                    this.m_nCurFrame++;
                }
            } else if (this.m_nCurFrame == 0) {
                this.m_bLooped = true;
                if (this.m_bLoop) {
                    this.m_nCurFrame = this.m_pAnimation.numOfFramesInAction[this.m_nActionId] - 1;
                } else if (this.m_bAutoHide) {
                    this.m_bEnabled = false;
                }
            } else {
                this.m_nCurFrame--;
            }
            if (this.m_nCurFrame != i2) {
                setFrame(this.m_nCurFrame);
            }
        }
        return true;
    }
}
