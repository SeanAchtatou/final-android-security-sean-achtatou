package com.tencent.qqgame.hall.communication;

import com.tencent.qqgame.hall.common.AActivity;
import com.tencent.qqgame.hall.common.FactoryCenter;
import com.tencent.qqgame.hall.common.SyncData;
import com.tencent.qqgame.hall.common.Tools;
import com.tencent.qqgame.hall.common.data.MProxyInfoV2;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Vector;

public class HttpCommunicator implements Runnable {
    public static final String METHOD_GET = "GET";
    public static final String METHOD_POST = "POST";
    public static final String PROXY_URL = "http://gamehall2.3g.qq.com/ghall/m/";
    private static final Vector<byte[]> msgList = new Vector<>();
    public static final byte startFlag = 2;
    public URL mUrl;

    public HttpCommunicator(FactoryCenter factoryCenter) throws Exception {
        SyncData.loadProxyUrl(AActivity.currentActivity);
        if (SyncData.Proxy_Url == null || SyncData.Proxy_Url.length() <= 0) {
            this.mUrl = new URL(PROXY_URL);
        } else {
            this.mUrl = new URL(SyncData.Proxy_Url);
        }
    }

    public static HttpURLConnection getHttpConnection(URL url, boolean z) {
        HttpURLConnection httpURLConnection;
        if (url == null) {
            return null;
        }
        boolean z2 = false;
        int netWorkType = MProxyInfoV2.getNetWorkType(AActivity.currentActivity);
        if (netWorkType == 1 || netWorkType == 16 || netWorkType == 64) {
            z2 = true;
        }
        Tools.debug("url:" + url.toString());
        if (z2) {
            try {
                Tools.debug("=======================================use proxy!=======================================");
                String host = url.getHost();
                String path = url.getPath();
                Tools.debug("host:" + host + ", path:" + path);
                HttpURLConnection httpURLConnection2 = (HttpURLConnection) new URL("http://10.0.0.172:80" + path).toURI().toURL().openConnection();
                httpURLConnection2.setRequestProperty("X-Online-Host", host);
                httpURLConnection = httpURLConnection2;
            } catch (Exception e) {
                return null;
            }
        } else {
            httpURLConnection = (HttpURLConnection) url.toURI().toURL().openConnection();
        }
        if (z) {
            httpURLConnection.setDoInput(true);
            httpURLConnection.setRequestMethod(METHOD_POST);
        }
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setConnectTimeout(180000);
        httpURLConnection.setRequestProperty("accept", "*/*");
        httpURLConnection.connect();
        return httpURLConnection;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x003c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
        com.tencent.qqgame.hall.common.Tools.debug("send http message error.");
        r0.printStackTrace();
        com.tencent.qqgame.hall.common.AActivity.currentActivity.handle.sendEmptyMessage(9);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0064 A[SYNTHETIC, Splitter:B:24:0x0064] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0126 A[SYNTHETIC, Splitter:B:65:0x0126] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x012b A[Catch:{ Exception -> 0x003c }] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0135 A[Catch:{ Exception -> 0x003c }] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x013a A[Catch:{ Exception -> 0x003c }] */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x0006 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r11 = this;
            r9 = 0
            java.lang.String r0 = "http://gamehall2.3g.qq.com/ghall/m/"
            java.util.Vector<byte[]> r1 = com.tencent.qqgame.hall.communication.HttpCommunicator.msgList
            monitor-enter(r1)
        L_0x0006:
            java.util.Vector<byte[]> r0 = com.tencent.qqgame.hall.communication.HttpCommunicator.msgList     // Catch:{ all -> 0x004f }
            int r0 = r0.size()     // Catch:{ all -> 0x004f }
            if (r0 <= 0) goto L_0x0149
        L_0x000e:
            java.net.URL r0 = r11.mUrl     // Catch:{ Exception -> 0x003c }
            r2 = 1
            java.net.HttpURLConnection r2 = getHttpConnection(r0, r2)     // Catch:{ Exception -> 0x003c }
            if (r2 != 0) goto L_0x0052
            java.net.URL r0 = r11.mUrl     // Catch:{ Exception -> 0x003c }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x003c }
            java.lang.String r3 = "http://gamehall2.3g.qq.com/ghall/m/"
            boolean r0 = r0.equals(r3)     // Catch:{ Exception -> 0x003c }
            if (r0 != 0) goto L_0x0052
            java.lang.String r0 = "create http connect error, use PROXY_URL retry."
            com.tencent.qqgame.hall.common.Tools.debug(r0)     // Catch:{ Exception -> 0x003c }
            java.net.URL r0 = new java.net.URL     // Catch:{ Exception -> 0x003c }
            java.lang.String r2 = "http://gamehall2.3g.qq.com/ghall/m/"
            r0.<init>(r2)     // Catch:{ Exception -> 0x003c }
            r11.mUrl = r0     // Catch:{ Exception -> 0x003c }
            r0 = 0
            com.tencent.qqgame.hall.common.SyncData.Proxy_Url = r0     // Catch:{ Exception -> 0x003c }
            com.tencent.qqgame.hall.common.AActivity r0 = com.tencent.qqgame.hall.common.AActivity.currentActivity     // Catch:{ Exception -> 0x003c }
            com.tencent.qqgame.hall.common.SyncData.saveProxyUrl(r0)     // Catch:{ Exception -> 0x003c }
            goto L_0x000e
        L_0x003c:
            r0 = move-exception
            java.lang.String r2 = "send http message error."
            com.tencent.qqgame.hall.common.Tools.debug(r2)     // Catch:{ all -> 0x004f }
            r0.printStackTrace()     // Catch:{ all -> 0x004f }
            com.tencent.qqgame.hall.common.AActivity r0 = com.tencent.qqgame.hall.common.AActivity.currentActivity     // Catch:{ all -> 0x004f }
            android.os.Handler r0 = r0.handle     // Catch:{ all -> 0x004f }
            r2 = 9
            r0.sendEmptyMessage(r2)     // Catch:{ all -> 0x004f }
            goto L_0x0006
        L_0x004f:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x004f }
            throw r0
        L_0x0052:
            if (r2 != 0) goto L_0x0064
            java.lang.String r0 = "creat http connection error!"
            com.tencent.qqgame.hall.common.Tools.debug(r0)     // Catch:{ Exception -> 0x003c }
            com.tencent.qqgame.hall.common.AActivity r0 = com.tencent.qqgame.hall.common.AActivity.currentActivity     // Catch:{ Exception -> 0x003c }
            android.os.Handler r0 = r0.handle     // Catch:{ Exception -> 0x003c }
            r2 = 9
            r0.sendEmptyMessage(r2)     // Catch:{ Exception -> 0x003c }
            monitor-exit(r1)     // Catch:{ all -> 0x004f }
        L_0x0063:
            return
        L_0x0064:
            java.io.OutputStream r3 = r2.getOutputStream()     // Catch:{ Exception -> 0x003c }
            java.util.Vector<byte[]> r0 = com.tencent.qqgame.hall.communication.HttpCommunicator.msgList     // Catch:{ Exception -> 0x003c }
            r4 = 0
            java.lang.Object r0 = r0.elementAt(r4)     // Catch:{ Exception -> 0x003c }
            byte[] r0 = (byte[]) r0     // Catch:{ Exception -> 0x003c }
            r3.write(r0)     // Catch:{ Exception -> 0x003c }
            r3.flush()     // Catch:{ Exception -> 0x003c }
            r3.close()     // Catch:{ Exception -> 0x003c }
            java.util.Vector<byte[]> r0 = com.tencent.qqgame.hall.communication.HttpCommunicator.msgList     // Catch:{ Exception -> 0x003c }
            r3 = 0
            r0.removeElementAt(r3)     // Catch:{ Exception -> 0x003c }
            int r0 = r2.getResponseCode()     // Catch:{ Exception -> 0x003c }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x003c }
            r3.<init>()     // Catch:{ Exception -> 0x003c }
            java.lang.String r4 = "responseCode: "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ Exception -> 0x003c }
            java.lang.StringBuilder r3 = r3.append(r0)     // Catch:{ Exception -> 0x003c }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x003c }
            com.tencent.qqgame.hall.common.Tools.debug(r3)     // Catch:{ Exception -> 0x003c }
            r3 = 200(0xc8, float:2.8E-43)
            if (r0 == r3) goto L_0x00a2
            r3 = 206(0xce, float:2.89E-43)
            if (r0 != r3) goto L_0x013e
        L_0x00a2:
            java.io.InputStream r0 = r2.getInputStream()     // Catch:{ Exception -> 0x0110, all -> 0x0130 }
            java.io.DataInputStream r2 = new java.io.DataInputStream     // Catch:{ Exception -> 0x0158, all -> 0x014c }
            r2.<init>(r0)     // Catch:{ Exception -> 0x0158, all -> 0x014c }
            byte r3 = r2.readByte()     // Catch:{ Exception -> 0x015d, all -> 0x0151 }
            r4 = 2
            if (r3 == r4) goto L_0x00c7
            com.tencent.qqgame.hall.common.AActivity r3 = com.tencent.qqgame.hall.common.AActivity.currentActivity     // Catch:{ Exception -> 0x015d, all -> 0x0151 }
            android.os.Handler r3 = r3.handle     // Catch:{ Exception -> 0x015d, all -> 0x0151 }
            r4 = 9
            r3.sendEmptyMessage(r4)     // Catch:{ Exception -> 0x015d, all -> 0x0151 }
            if (r2 == 0) goto L_0x00c0
            r2.close()     // Catch:{ Exception -> 0x003c }
        L_0x00c0:
            if (r0 == 0) goto L_0x00c5
            r0.close()     // Catch:{ Exception -> 0x003c }
        L_0x00c5:
            monitor-exit(r1)     // Catch:{ all -> 0x004f }
            goto L_0x0063
        L_0x00c7:
            int r3 = r2.readInt()     // Catch:{ Exception -> 0x015d, all -> 0x0151 }
            r2.readShort()     // Catch:{ Exception -> 0x015d, all -> 0x0151 }
            r2.readInt()     // Catch:{ Exception -> 0x015d, all -> 0x0151 }
            int r4 = r2.readInt()     // Catch:{ Exception -> 0x015d, all -> 0x0151 }
            long r5 = (long) r4     // Catch:{ Exception -> 0x015d, all -> 0x0151 }
            r2.skip(r5)     // Catch:{ Exception -> 0x015d, all -> 0x0151 }
            short r5 = r2.readShort()     // Catch:{ Exception -> 0x015d, all -> 0x0151 }
            r6 = 17
            int r3 = r3 - r6
            int r3 = r3 - r4
            byte[] r4 = new byte[r3]     // Catch:{ Exception -> 0x015d, all -> 0x0151 }
            r6 = r3
        L_0x00e4:
            if (r6 <= 0) goto L_0x00f1
            int r7 = r3 - r6
            int r7 = r2.read(r4, r7, r6)     // Catch:{ Exception -> 0x015d, all -> 0x0151 }
            r8 = -1
            if (r7 == r8) goto L_0x00f1
            int r6 = r6 - r7
            goto L_0x00e4
        L_0x00f1:
            com.tencent.qqgame.hall.common.AActivity r3 = com.tencent.qqgame.hall.common.AActivity.currentActivity     // Catch:{ Exception -> 0x015d, all -> 0x0151 }
            android.os.Handler r3 = r3.handle     // Catch:{ Exception -> 0x015d, all -> 0x0151 }
            if (r3 == 0) goto L_0x0104
            r6 = -108(0xffffffffffffff94, float:NaN)
            android.os.Message r6 = r3.obtainMessage(r6)     // Catch:{ Exception -> 0x015d, all -> 0x0151 }
            r6.obj = r4     // Catch:{ Exception -> 0x015d, all -> 0x0151 }
            r6.arg1 = r5     // Catch:{ Exception -> 0x015d, all -> 0x0151 }
            r3.sendMessage(r6)     // Catch:{ Exception -> 0x015d, all -> 0x0151 }
        L_0x0104:
            if (r2 == 0) goto L_0x0109
            r2.close()     // Catch:{ Exception -> 0x003c }
        L_0x0109:
            if (r0 == 0) goto L_0x0006
            r0.close()     // Catch:{ Exception -> 0x003c }
            goto L_0x0006
        L_0x0110:
            r0 = move-exception
            r2 = r9
            r3 = r9
        L_0x0113:
            java.lang.String r4 = "read http message error!"
            com.tencent.qqgame.hall.common.Tools.debug(r4)     // Catch:{ all -> 0x0156 }
            r0.printStackTrace()     // Catch:{ all -> 0x0156 }
            com.tencent.qqgame.hall.common.AActivity r0 = com.tencent.qqgame.hall.common.AActivity.currentActivity     // Catch:{ all -> 0x0156 }
            android.os.Handler r0 = r0.handle     // Catch:{ all -> 0x0156 }
            r4 = 9
            r0.sendEmptyMessage(r4)     // Catch:{ all -> 0x0156 }
            if (r2 == 0) goto L_0x0129
            r2.close()     // Catch:{ Exception -> 0x003c }
        L_0x0129:
            if (r3 == 0) goto L_0x0006
            r3.close()     // Catch:{ Exception -> 0x003c }
            goto L_0x0006
        L_0x0130:
            r0 = move-exception
            r2 = r9
            r3 = r9
        L_0x0133:
            if (r2 == 0) goto L_0x0138
            r2.close()     // Catch:{ Exception -> 0x003c }
        L_0x0138:
            if (r3 == 0) goto L_0x013d
            r3.close()     // Catch:{ Exception -> 0x003c }
        L_0x013d:
            throw r0     // Catch:{ Exception -> 0x003c }
        L_0x013e:
            com.tencent.qqgame.hall.common.AActivity r0 = com.tencent.qqgame.hall.common.AActivity.currentActivity     // Catch:{ Exception -> 0x003c }
            android.os.Handler r0 = r0.handle     // Catch:{ Exception -> 0x003c }
            r2 = 9
            r0.sendEmptyMessage(r2)     // Catch:{ Exception -> 0x003c }
            goto L_0x0006
        L_0x0149:
            monitor-exit(r1)     // Catch:{ all -> 0x004f }
            goto L_0x0063
        L_0x014c:
            r2 = move-exception
            r3 = r0
            r0 = r2
            r2 = r9
            goto L_0x0133
        L_0x0151:
            r3 = move-exception
            r10 = r3
            r3 = r0
            r0 = r10
            goto L_0x0133
        L_0x0156:
            r0 = move-exception
            goto L_0x0133
        L_0x0158:
            r2 = move-exception
            r3 = r0
            r0 = r2
            r2 = r9
            goto L_0x0113
        L_0x015d:
            r3 = move-exception
            r10 = r3
            r3 = r0
            r0 = r10
            goto L_0x0113
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.hall.communication.HttpCommunicator.run():void");
    }

    public void sendMsg(byte[] bArr) {
        synchronized (msgList) {
            msgList.add(bArr);
        }
        new Thread(this).start();
    }
}
