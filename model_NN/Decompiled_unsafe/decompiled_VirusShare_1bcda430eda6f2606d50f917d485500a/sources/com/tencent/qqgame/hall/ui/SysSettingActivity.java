package com.tencent.qqgame.hall.ui;

import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Message;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import com.tencent.qqgame.hall.common.AActivity;
import com.tencent.qqgame.hall.common.Report;
import com.tencent.qqgame.hall.common.SysSetting;
import tencent.qqgame.lord.R;

public class SysSettingActivity extends AActivity implements SeekBar.OnSeekBarChangeListener, CompoundButton.OnCheckedChangeListener {
    private CheckBox cbBgEffect;
    private CheckBox cbBgMusic;
    private CheckBox cbVibrate;
    private BitmapDrawable drawBg = null;
    private SeekBar sbLight;
    private SeekBar sbVolume;

    /* access modifiers changed from: protected */
    public void handleMessage(Message message) {
    }

    public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        if (compoundButton.getId() == R.id.syssetting_cb_bgmusic) {
            SysSetting.getInstance(this).setBgSound(z);
            if (soundPlayer != null) {
                if (z || !soundPlayer.isBackGroundSoundPlaying()) {
                    soundPlayer.playCurBackGroundSound();
                } else {
                    soundPlayer.stopBackGroundSound(false);
                }
            }
        } else if (compoundButton.getId() == R.id.syssetting_cb_bgeffect) {
            SysSetting.getInstance(this).setEffectSound(z);
        } else if (compoundButton.getId() == R.id.syssetting_cb_vibrate) {
            SysSetting.getInstance(this).setVibrate(z);
        }
        SysSetting.getInstance(this).save();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_syssetting);
        setBackgroudDrawable(res, (ViewGroup) findViewById(R.id.lTopLayout));
        SysSetting instance = SysSetting.getInstance(this);
        instance.load();
        setToolbarTitle(res.getString(R.string.menu_setting));
        this.sbVolume = (SeekBar) findViewById(R.id.syssetting_seekbar_volume);
        this.sbVolume.setMax(instance.getMaxStreamVolume(3));
        instance.setVolumne(instance.getSysStreamVolume(3));
        this.sbVolume.setProgress(instance.getVolumne());
        this.sbVolume.setOnSeekBarChangeListener(this);
        this.sbLight = (SeekBar) findViewById(R.id.syssetting_seekbar_light);
        this.sbLight.setProgress((int) (SysSetting.getInstance(this).getBright() * 100.0f));
        this.sbLight.setOnSeekBarChangeListener(this);
        this.cbBgMusic = (CheckBox) findViewById(R.id.syssetting_cb_bgmusic);
        this.cbBgMusic.setOnCheckedChangeListener(this);
        this.cbBgMusic.setChecked(SysSetting.getInstance(this).isBgSound());
        this.cbBgEffect = (CheckBox) findViewById(R.id.syssetting_cb_bgeffect);
        this.cbBgEffect.setOnCheckedChangeListener(this);
        this.cbBgEffect.setChecked(SysSetting.getInstance(this).isEffectSound());
        this.cbVibrate = (CheckBox) findViewById(R.id.syssetting_cb_vibrate);
        this.cbVibrate.setOnCheckedChangeListener(this);
        this.cbVibrate.setChecked(SysSetting.getInstance(this).isVibrate());
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        switch (i) {
            case 4:
                Report.keyBackHitInHallNum++;
                break;
        }
        return super.onKeyDown(i, keyEvent);
    }

    public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        if (seekBar == this.sbLight) {
            SysSetting.getInstance(this).setBright(((float) i) / ((float) seekBar.getMax()));
            adjustBright(SysSetting.getInstance(this).getBright());
        } else if (seekBar == this.sbVolume) {
            SysSetting.getInstance(this).adjustVolumne(3, i);
        }
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
        SysSetting.getInstance(this).save();
    }
}
