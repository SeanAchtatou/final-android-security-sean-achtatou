package com.tencent.qqgame.hall.ui.controls;

import android.graphics.Bitmap;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;
import com.tencent.qqgame.common.Player;
import com.tencent.qqgame.common.Table;
import com.tencent.qqgame.hall.common.FactoryCenter;
import com.tencent.qqgame.hall.common.SyncData;
import com.tencent.qqgame.hall.ui.TableListActivity;
import tencent.qqgame.lord.R;

public class TableCellControl extends View {
    public static final short TABLE_HEIGHT = 139;
    public static final short TABLE_WIDTH = 158;
    private static final Paint p = new Paint();
    FactoryCenter factory;
    private Bitmap imgCardBack = null;
    private Bitmap imgNum;
    private Bitmap imgReady = null;
    private Bitmap imgTable_dark = null;
    private Bitmap imgTable_light = null;
    private TableListActivity parent;
    private final Rect[] r_seats = new Rect[3];
    private Table table = null;
    private short tableID = 0;

    public TableCellControl(TableListActivity tableListActivity, FactoryCenter factoryCenter, Table table2, short s, Bitmap bitmap) {
        super(tableListActivity);
        this.parent = tableListActivity;
        this.factory = factoryCenter;
        this.table = table2;
        this.tableID = s;
        this.imgNum = bitmap;
        this.r_seats[0] = new Rect();
        this.r_seats[1] = new Rect();
        this.r_seats[2] = new Rect();
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x00c6  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0101  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0162  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x01c4  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00e9 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void draw(android.graphics.Canvas r22) {
        /*
            r21 = this;
            r0 = r21
            com.tencent.qqgame.common.Table r0 = r0.table
            r5 = r0
            if (r5 != 0) goto L_0x0008
        L_0x0007:
            return
        L_0x0008:
            android.graphics.Paint r6 = com.tencent.qqgame.hall.ui.controls.TableCellControl.p
            r5 = 1
            r6.setAntiAlias(r5)
            r0 = r21
            android.graphics.Bitmap r0 = r0.imgTable_light
            r5 = r0
            r7 = 0
            r5 = 0
            r8 = r5
        L_0x0016:
            r0 = r21
            com.tencent.qqgame.common.Table r0 = r0.table
            r5 = r0
            java.util.Vector<com.tencent.qqgame.common.Player> r5 = r5.playerList
            int r5 = r5.size()
            if (r8 >= r5) goto L_0x0037
            r0 = r21
            com.tencent.qqgame.common.Table r0 = r0.table
            r5 = r0
            java.util.Vector<com.tencent.qqgame.common.Player> r5 = r5.playerList
            java.lang.Object r5 = r5.elementAt(r8)
            com.tencent.qqgame.common.Player r5 = (com.tencent.qqgame.common.Player) r5
            short r5 = r5.status
            r9 = 4
            if (r5 != r9) goto L_0x00ee
            r5 = 1
            r7 = r5
        L_0x0037:
            if (r7 == 0) goto L_0x00f3
            r0 = r21
            android.graphics.Bitmap r0 = r0.imgTable_light
            r5 = r0
        L_0x003e:
            r8 = 1104674816(0x41d80000, float:27.0)
            r9 = 1090519040(0x41000000, float:8.0)
            r0 = r22
            r1 = r5
            r2 = r8
            r3 = r9
            r4 = r6
            r0.drawBitmap(r1, r2, r3, r4)
            r5 = -1
            r6.setColor(r5)
            r8 = 12
            float r5 = (float) r8
            r6.setTextSize(r5)
            r9 = 4
            r10 = 2
            byte r11 = com.tencent.qqgame.hall.common.Tools.headSize
            r0 = r21
            com.tencent.qqgame.common.Table r0 = r0.table
            r5 = r0
            short r5 = r5.tableId
            if (r5 < 0) goto L_0x0230
            r5 = 0
            r12 = r5
        L_0x0064:
            r5 = 3
            if (r12 >= r5) goto L_0x021c
            r13 = 0
            r14 = -1
            r0 = r21
            com.tencent.qqgame.common.Table r0 = r0.table
            r5 = r0
            java.util.Vector<com.tencent.qqgame.common.Player> r5 = r5.playerList
            if (r5 == 0) goto L_0x0245
            r0 = r21
            com.tencent.qqgame.common.Table r0 = r0.table
            r5 = r0
            java.util.Vector<com.tencent.qqgame.common.Player> r5 = r5.playerList
            int r5 = r5.size()
            if (r5 <= 0) goto L_0x0245
            r5 = 0
            r15 = r5
        L_0x0081:
            r0 = r21
            com.tencent.qqgame.common.Table r0 = r0.table
            r5 = r0
            java.util.Vector<com.tencent.qqgame.common.Player> r5 = r5.playerList
            int r5 = r5.size()
            if (r15 >= r5) goto L_0x0245
            r0 = r21
            com.tencent.qqgame.common.Table r0 = r0.table
            r5 = r0
            java.util.Vector<com.tencent.qqgame.common.Player> r5 = r5.playerList
            java.lang.Object r5 = r5.elementAt(r15)
            com.tencent.qqgame.common.Player r5 = (com.tencent.qqgame.common.Player) r5
            short r5 = r5.seatId
            if (r5 != r12) goto L_0x00fa
            r0 = r21
            com.tencent.qqgame.common.Table r0 = r0.table
            r5 = r0
            java.util.Vector<com.tencent.qqgame.common.Player> r5 = r5.playerList
            java.lang.Object r5 = r5.elementAt(r15)
            com.tencent.qqgame.common.Player r5 = (com.tencent.qqgame.common.Player) r5
            short r13 = r5.iconId
            r20 = r13
            r13 = r5
            r5 = r20
        L_0x00b3:
            if (r13 == 0) goto L_0x00b9
            java.lang.String r14 = r13.nickName
            if (r14 != 0) goto L_0x00fe
        L_0x00b9:
            java.lang.String r14 = " "
        L_0x00bb:
            int r15 = r14.length()
            r16 = 4
            r0 = r15
            r1 = r16
            if (r0 <= r1) goto L_0x00e6
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            r16 = 0
            r17 = 3
            r0 = r14
            r1 = r16
            r2 = r17
            java.lang.String r14 = r0.substring(r1, r2)
            java.lang.StringBuilder r14 = r15.append(r14)
            java.lang.String r15 = ".."
            java.lang.StringBuilder r14 = r14.append(r15)
            java.lang.String r14 = r14.toString()
        L_0x00e6:
            switch(r12) {
                case 0: goto L_0x0101;
                case 1: goto L_0x0162;
                case 2: goto L_0x01c4;
                default: goto L_0x00e9;
            }
        L_0x00e9:
            int r5 = r12 + 1
            r12 = r5
            goto L_0x0064
        L_0x00ee:
            int r5 = r8 + 1
            r8 = r5
            goto L_0x0016
        L_0x00f3:
            r0 = r21
            android.graphics.Bitmap r0 = r0.imgTable_dark
            r5 = r0
            goto L_0x003e
        L_0x00fa:
            int r5 = r15 + 1
            r15 = r5
            goto L_0x0081
        L_0x00fe:
            java.lang.String r14 = r13.nickName
            goto L_0x00bb
        L_0x0101:
            r0 = r21
            android.graphics.Rect[] r0 = r0.r_seats
            r15 = r0
            r16 = 0
            r15 = r15[r16]
            r16 = 108(0x6c, float:1.51E-43)
            r17 = 26
            int r18 = r11 + 108
            int r19 = r11 + 26
            int r19 = r19 + r9
            int r19 = r19 + r8
            r15.set(r16, r17, r18, r19)
            r15 = 108(0x6c, float:1.51E-43)
            r16 = 26
            r0 = r22
            r1 = r6
            r2 = r5
            r3 = r15
            r4 = r16
            com.tencent.qqgame.hall.common.Tools.drawQQHead(r0, r1, r2, r3, r4)
            if (r13 == 0) goto L_0x00e9
            r5 = 1121452032(0x42d80000, float:108.0)
            int r15 = r11 + 26
            int r15 = r15 + r9
            int r16 = r8 / 2
            int r15 = r15 + r16
            float r15 = (float) r15
            r0 = r22
            r1 = r14
            r2 = r5
            r3 = r15
            r4 = r6
            r0.drawText(r1, r2, r3, r4)
            if (r7 != 0) goto L_0x00e9
            short r5 = r13.status
            r13 = 3
            if (r5 != r13) goto L_0x00e9
            r0 = r21
            android.graphics.Bitmap r0 = r0.imgReady
            r5 = r0
            r13 = 108(0x6c, float:1.51E-43)
            int r13 = r13 - r10
            r0 = r21
            android.graphics.Bitmap r0 = r0.imgReady
            r14 = r0
            int r14 = r14.getWidth()
            int r13 = r13 - r14
            float r13 = (float) r13
            r14 = 1106247680(0x41f00000, float:30.0)
            r0 = r22
            r1 = r5
            r2 = r13
            r3 = r14
            r4 = r6
            r0.drawBitmap(r1, r2, r3, r4)
            goto L_0x00e9
        L_0x0162:
            r0 = r21
            android.graphics.Rect[] r0 = r0.r_seats
            r15 = r0
            r16 = 1
            r15 = r15[r16]
            r16 = 60
            r17 = 75
            int r18 = r11 + 60
            int r19 = r11 + 75
            int r19 = r19 + r9
            int r19 = r19 + r8
            r15.set(r16, r17, r18, r19)
            r15 = 60
            r16 = 75
            r0 = r22
            r1 = r6
            r2 = r5
            r3 = r15
            r4 = r16
            com.tencent.qqgame.hall.common.Tools.drawQQHead(r0, r1, r2, r3, r4)
            if (r13 == 0) goto L_0x00e9
            r5 = 1114636288(0x42700000, float:60.0)
            int r15 = r11 + 75
            int r15 = r15 + r9
            int r16 = r8 / 2
            int r15 = r15 + r16
            float r15 = (float) r15
            r0 = r22
            r1 = r14
            r2 = r5
            r3 = r15
            r4 = r6
            r0.drawText(r1, r2, r3, r4)
            if (r7 != 0) goto L_0x00e9
            short r5 = r13.status
            r13 = 3
            if (r5 != r13) goto L_0x00e9
            r0 = r21
            android.graphics.Bitmap r0 = r0.imgReady
            r5 = r0
            r13 = 1116340224(0x428a0000, float:69.0)
            r14 = 75
            int r14 = r14 - r10
            r0 = r21
            android.graphics.Bitmap r0 = r0.imgReady
            r15 = r0
            int r15 = r15.getHeight()
            int r14 = r14 - r15
            float r14 = (float) r14
            r0 = r22
            r1 = r5
            r2 = r13
            r3 = r14
            r4 = r6
            r0.drawBitmap(r1, r2, r3, r4)
            goto L_0x00e9
        L_0x01c4:
            r0 = r21
            android.graphics.Rect[] r0 = r0.r_seats
            r15 = r0
            r16 = 2
            r15 = r15[r16]
            r16 = 12
            r17 = 26
            int r18 = r11 + 12
            int r19 = r11 + 26
            int r19 = r19 + r9
            int r19 = r19 + r8
            r15.set(r16, r17, r18, r19)
            r15 = 12
            r16 = 26
            r0 = r22
            r1 = r6
            r2 = r5
            r3 = r15
            r4 = r16
            com.tencent.qqgame.hall.common.Tools.drawQQHead(r0, r1, r2, r3, r4)
            if (r13 == 0) goto L_0x00e9
            r5 = 1094713344(0x41400000, float:12.0)
            int r15 = r11 + 26
            int r15 = r15 + r9
            int r16 = r8 / 2
            int r15 = r15 + r16
            float r15 = (float) r15
            r0 = r22
            r1 = r14
            r2 = r5
            r3 = r15
            r4 = r6
            r0.drawText(r1, r2, r3, r4)
            if (r7 != 0) goto L_0x00e9
            short r5 = r13.status
            r13 = 3
            if (r5 != r13) goto L_0x00e9
            r0 = r21
            android.graphics.Bitmap r0 = r0.imgReady
            r5 = r0
            int r13 = r11 + 12
            int r13 = r13 + r10
            float r13 = (float) r13
            r14 = 1106247680(0x41f00000, float:30.0)
            r0 = r22
            r1 = r5
            r2 = r13
            r3 = r14
            r4 = r6
            r0.drawBitmap(r1, r2, r3, r4)
            goto L_0x00e9
        L_0x021c:
            if (r7 == 0) goto L_0x0230
            r0 = r21
            android.graphics.Bitmap r0 = r0.imgCardBack
            r5 = r0
            r7 = 1115422720(0x427c0000, float:63.0)
            r8 = 1108344832(0x42100000, float:36.0)
            r0 = r22
            r1 = r5
            r2 = r7
            r3 = r8
            r4 = r6
            r0.drawBitmap(r1, r2, r3, r4)
        L_0x0230:
            r0 = r21
            android.graphics.Bitmap r0 = r0.imgNum
            r7 = r0
            r0 = r21
            short r0 = r0.tableID
            r8 = r0
            r9 = 67
            r10 = 16
            r5 = r22
            com.tencent.qqgame.hall.common.Tools.drawNum(r5, r6, r7, r8, r9, r10)
            goto L_0x0007
        L_0x0245:
            r5 = r14
            goto L_0x00b3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.hall.ui.controls.TableCellControl.draw(android.graphics.Canvas):void");
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        invalidate();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        setMeasuredDimension(158, 139);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        Player player;
        if (this.table.tableId < 0) {
            return true;
        }
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        if (motionEvent.getAction() == 1) {
            for (short s = 0; s < this.r_seats.length; s = (short) (s + 1)) {
                if (this.r_seats[s].contains(x, y)) {
                    int i = 0;
                    while (true) {
                        if (i >= this.table.playerList.size()) {
                            player = null;
                            break;
                        } else if (this.table.playerList.elementAt(i).seatId == s) {
                            player = this.table.playerList.elementAt(i);
                            break;
                        } else {
                            i++;
                        }
                    }
                    if (player == null) {
                        this.parent.showDialog(0, this.parent.getString(R.string.game_sitdown));
                        SyncData.myTable = this.table;
                        SyncData.myTableID = this.table.tableId;
                        this.factory.getSendMsgHandle().sitDown(SyncData.myZoneID, SyncData.myRoomID, SyncData.myRoomSvrID, SyncData.myTableID, s);
                        return true;
                    }
                    Message obtainMessage = this.parent.handle.obtainMessage(7);
                    Bundle bundle = new Bundle();
                    bundle.putInt("X", x);
                    bundle.putInt("Y", y);
                    obtainMessage.obj = player;
                    obtainMessage.arg1 = this.table.tableId;
                    obtainMessage.setData(bundle);
                    this.parent.handle.sendMessage(obtainMessage);
                    return true;
                }
            }
        }
        return super.onTouchEvent(motionEvent);
    }

    public void setImage(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3, Bitmap bitmap4) {
        this.imgTable_dark = bitmap2;
        this.imgTable_light = bitmap;
        this.imgCardBack = bitmap3;
        this.imgReady = bitmap4;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        super.setOnClickListener(onClickListener);
    }
}
