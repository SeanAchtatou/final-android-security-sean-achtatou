package com.tencent.qqgame.hall.communication;

import android.os.Handler;
import com.tencent.qqgame.common.Msg;
import com.tencent.qqgame.common.Util;
import com.tencent.qqgame.hall.common.AActivity;
import com.tencent.qqgame.hall.common.FactoryCenter;
import com.tencent.qqgame.hall.common.IGameView;
import com.tencent.qqgame.hall.common.SyncData;
import com.tencent.qqgame.hall.common.Tools;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Vector;

public class SocketCommunicator implements Runnable {
    private static int receiveThreadCounter = 0;
    private static int sendThreadCounter = 0;
    private final byte CLOSED = 0;
    private final byte CLOSING = 1;
    private final byte OPENED = 3;
    private final byte OPENING = 2;
    /* access modifiers changed from: private */
    public byte curState = 0;
    /* access modifiers changed from: private */
    public FactoryCenter factory;
    /* access modifiers changed from: private */
    public InputStream in;
    private volatile boolean isConnected = false;
    /* access modifiers changed from: private */
    public volatile boolean isRRunning = false;
    /* access modifiers changed from: private */
    public volatile boolean isSRunning = false;
    private final Object lock = new Object();
    private OutputStream out;
    private Thread receiveThread = null;
    private final Vector<Msg> sendQueue;
    /* access modifiers changed from: private */
    public Thread sendThread = null;
    private String server_ip = SyncData.SERVER_IP_WIFI;
    private int server_port = 6667;
    private Socket socket = null;

    public SocketCommunicator(FactoryCenter factoryCenter) {
        this.factory = factoryCenter;
        this.sendQueue = new Vector<>();
    }

    static /* synthetic */ int access$608() {
        int i = sendThreadCounter;
        sendThreadCounter = i + 1;
        return i;
    }

    private void removeConnResource() {
        if (this.in != null) {
            try {
                this.in.close();
                this.in = null;
            } catch (Exception e) {
                Tools.debug("release resource exception. detail info: " + e.toString());
            }
        }
        if (this.out != null) {
            try {
                this.out.close();
                this.out = null;
            } catch (Exception e2) {
                Tools.debug("close i/o stream exception. detail info: " + e2.toString());
            }
        }
        if (this.socket != null) {
            try {
                this.socket.close();
                this.socket = null;
            } catch (Exception e3) {
                Tools.debug("close socket exception. detail info: " + e3.toString());
            }
        }
        this.isConnected = false;
    }

    public boolean connect(boolean z) {
        Tools.debug("connect...");
        if (this.isConnected && !z) {
            return false;
        }
        new Thread(new Runnable() {
            public void run() {
                if (SocketCommunicator.this.curState != 2) {
                    SocketCommunicator.this.disConnect(true);
                    boolean unused = SocketCommunicator.this.isSRunning = true;
                    Thread unused2 = SocketCommunicator.this.sendThread = null;
                    Thread unused3 = SocketCommunicator.this.sendThread = new Thread(SocketCommunicator.this.factory.getCommunicator());
                    SocketCommunicator.this.sendThread.setName("send_c:" + SocketCommunicator.access$608());
                    SocketCommunicator.this.sendThread.start();
                }
            }
        }).start();
        return true;
    }

    public boolean disConnect(boolean z) {
        Tools.debug("begin disConnect...");
        synchronized (this.lock) {
            if (!this.isConnected) {
                Tools.debug("isConnected");
                Tools.debug("end disConnect...");
                return true;
            }
            this.curState = 1;
            try {
                if (this.receiveThread != null) {
                    this.isRRunning = false;
                    this.receiveThread.interrupt();
                    if (z) {
                        try {
                            this.receiveThread.join();
                        } catch (InterruptedException e) {
                        }
                    }
                    this.receiveThread = null;
                }
                if (this.sendThread != null) {
                    if (this.sendThread.isAlive()) {
                        synchronized (this.sendQueue) {
                            this.sendQueue.notifyAll();
                        }
                    }
                    this.isSRunning = false;
                    Util.threadSleep(IGameView.MSG_2GAME_DIS_CONNECTED);
                    this.sendThread.interrupt();
                    if (z) {
                        try {
                            this.sendThread.join();
                        } catch (InterruptedException e2) {
                        }
                    }
                    this.sendThread = null;
                }
                removeConnResource();
                this.curState = 0;
                Tools.debug("end disConnect...");
                return true;
            } catch (Exception e3) {
                Tools.debug("close connect exception! detail info: " + e3.toString());
                e3.printStackTrace();
                this.curState = 0;
                return false;
            }
        }
    }

    public boolean isConnected() {
        return this.isConnected;
    }

    public void reconnect() {
        Tools.debug("reconnect...");
        Handler handler = AActivity.currentActivity.handle;
        if (handler != null) {
            handler.sendEmptyMessage(AActivity.MSG_RECONNECT);
        }
        this.isRRunning = false;
        this.isSRunning = false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:102:0x0263, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x0264, code lost:
        com.tencent.qqgame.hall.common.Tools.debug("send message exception inner! detail info: " + r0.toString());
        reconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:?, code lost:
        r13.sendQueue.wait();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x028a, code lost:
        com.tencent.qqgame.hall.common.Tools.debug("sendThread interrupted...");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x0290, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x0291, code lost:
        com.tencent.qqgame.hall.common.Tools.debug("send message exception! detail info: " + r0.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x02ad, code lost:
        if (r13.out == null) goto L_0x02af;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x02af, code lost:
        com.tencent.qqgame.hall.common.Tools.debug("out is null");
        reconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x02b9, code lost:
        removeConnResource();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:145:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x01f8, code lost:
        if (r13.receiveThread != null) goto L_0x022b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x01fa, code lost:
        r13.isRRunning = true;
        r13.receiveThread = new java.lang.Thread(new com.tencent.qqgame.hall.communication.SocketCommunicator.AnonymousClass1(r13));
        r0 = r13.receiveThread;
        r1 = new java.lang.StringBuilder().append("receive_c:");
        r2 = com.tencent.qqgame.hall.communication.SocketCommunicator.receiveThreadCounter;
        com.tencent.qqgame.hall.communication.SocketCommunicator.receiveThreadCounter = r2 + 1;
        r0.setName(r1.append(r2).toString());
        r13.receiveThread.start();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x022d, code lost:
        if (r13.isSRunning == false) goto L_0x02b9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:?, code lost:
        r1 = r13.sendQueue;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0231, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0238, code lost:
        if (r13.sendQueue.size() <= 0) goto L_0x0282;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x023a, code lost:
        r13.out.write(r13.sendQueue.elementAt(0).encode());
        r13.out.flush();
        r13.sendQueue.removeElementAt(0);
        com.tencent.qqgame.hall.common.Tools.debug("send socket msg...");
        com.tencent.qqgame.hall.common.SyncData.timerNetSpeedMayBe = 0;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r13 = this;
            r6 = 128(0x80, float:1.794E-43)
            r12 = 3
            r11 = 1
            r10 = 0
            java.lang.Object r1 = r13.lock
            monitor-enter(r1)
            r0 = 0
            r13.isConnected = r0     // Catch:{ all -> 0x00da }
            r0 = 2
            r13.curState = r0     // Catch:{ all -> 0x00da }
            r0 = 0
            r13.socket = r0     // Catch:{ all -> 0x00da }
            java.util.Vector<com.tencent.qqgame.hall.common.data.MProxyInfoV2> r0 = com.tencent.qqgame.hall.common.SyncData.proxyList     // Catch:{ all -> 0x00da }
            int r2 = r0.size()     // Catch:{ all -> 0x00da }
            com.tencent.qqgame.hall.common.AActivity r0 = com.tencent.qqgame.hall.common.AActivity.currentActivity     // Catch:{ all -> 0x00da }
            int r3 = com.tencent.qqgame.hall.common.data.MProxyInfoV2.getNetWorkType(r0)     // Catch:{ all -> 0x00da }
            r0 = -1
            if (r3 != r0) goto L_0x005b
            int r0 = com.tencent.qqgame.hall.common.SyncData.serverGuessNetType     // Catch:{ all -> 0x00da }
            if (r0 != 0) goto L_0x0058
            r0 = r6
        L_0x0025:
            r3 = -1
            if (r0 != r3) goto L_0x02c4
            r3 = r6
        L_0x0029:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00da }
            r0.<init>()     // Catch:{ all -> 0x00da }
            java.lang.String r4 = "start connect socket, netWorkType: "
            java.lang.StringBuilder r0 = r0.append(r4)     // Catch:{ all -> 0x00da }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ all -> 0x00da }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00da }
            com.tencent.qqgame.hall.common.Tools.debug(r0)     // Catch:{ all -> 0x00da }
            r4 = r10
        L_0x0040:
            java.util.Vector<com.tencent.qqgame.hall.common.data.MProxyInfoV2> r0 = com.tencent.qqgame.hall.common.SyncData.proxyList     // Catch:{ all -> 0x00da }
            int r0 = r0.size()     // Catch:{ all -> 0x00da }
            if (r4 >= r0) goto L_0x0080
            java.util.Vector<com.tencent.qqgame.hall.common.data.MProxyInfoV2> r0 = com.tencent.qqgame.hall.common.SyncData.proxyList     // Catch:{ all -> 0x00da }
            java.lang.Object r0 = r0.elementAt(r4)     // Catch:{ all -> 0x00da }
            com.tencent.qqgame.hall.common.data.MProxyInfoV2 r0 = (com.tencent.qqgame.hall.common.data.MProxyInfoV2) r0     // Catch:{ all -> 0x00da }
            r5 = 0
            r0.isTryed = r5     // Catch:{ all -> 0x00da }
            int r0 = r4 + 1
            byte r0 = (byte) r0     // Catch:{ all -> 0x00da }
            r4 = r0
            goto L_0x0040
        L_0x0058:
            int r0 = com.tencent.qqgame.hall.common.SyncData.serverGuessNetType     // Catch:{ all -> 0x00da }
            goto L_0x0025
        L_0x005b:
            r4 = r10
        L_0x005c:
            if (r4 >= r2) goto L_0x02c1
            java.util.Vector<com.tencent.qqgame.hall.common.data.MProxyInfoV2> r0 = com.tencent.qqgame.hall.common.SyncData.proxyList     // Catch:{ all -> 0x00da }
            java.lang.Object r0 = r0.elementAt(r4)     // Catch:{ all -> 0x00da }
            com.tencent.qqgame.hall.common.data.MProxyInfoV2 r0 = (com.tencent.qqgame.hall.common.data.MProxyInfoV2) r0     // Catch:{ all -> 0x00da }
            java.lang.String r5 = r0.svrIP     // Catch:{ all -> 0x00da }
            if (r5 == 0) goto L_0x007b
            short r5 = r0.svrPort     // Catch:{ all -> 0x00da }
            if (r5 <= 0) goto L_0x007b
            int r0 = r0.supportedType     // Catch:{ all -> 0x00da }
            boolean r0 = com.tencent.qqgame.hall.common.data.MProxyInfoV2.isCanUse(r0, r3)     // Catch:{ all -> 0x00da }
            if (r0 == 0) goto L_0x007b
            r0 = r11
        L_0x0077:
            if (r0 != 0) goto L_0x0029
            r3 = r6
            goto L_0x0029
        L_0x007b:
            int r0 = r4 + 1
            byte r0 = (byte) r0     // Catch:{ all -> 0x00da }
            r4 = r0
            goto L_0x005c
        L_0x0080:
            com.tencent.qqgame.hall.common.AActivity r0 = com.tencent.qqgame.hall.common.AActivity.currentActivity     // Catch:{ all -> 0x00da }
            android.os.Handler r4 = r0.handle     // Catch:{ all -> 0x00da }
            android.os.Message r5 = r4.obtainMessage()     // Catch:{ all -> 0x00da }
            r6 = r10
            r7 = r10
        L_0x008a:
            if (r6 >= r2) goto L_0x0155
            java.util.Vector<com.tencent.qqgame.hall.common.data.MProxyInfoV2> r0 = com.tencent.qqgame.hall.common.SyncData.proxyList     // Catch:{ all -> 0x00da }
            java.lang.Object r0 = r0.elementAt(r6)     // Catch:{ all -> 0x00da }
            com.tencent.qqgame.hall.common.data.MProxyInfoV2 r0 = (com.tencent.qqgame.hall.common.data.MProxyInfoV2) r0     // Catch:{ all -> 0x00da }
            java.lang.String r8 = r0.svrIP     // Catch:{ all -> 0x00da }
            if (r8 == 0) goto L_0x02be
            short r8 = r0.svrPort     // Catch:{ all -> 0x00da }
            if (r8 <= 0) goto L_0x02be
            boolean r8 = r0.isTryed     // Catch:{ all -> 0x00da }
            if (r8 != 0) goto L_0x02be
            int r8 = r0.supportedType     // Catch:{ all -> 0x00da }
            boolean r8 = com.tencent.qqgame.hall.common.data.MProxyInfoV2.isCanUse(r8, r3)     // Catch:{ all -> 0x00da }
            if (r8 == 0) goto L_0x02be
            boolean r8 = com.tencent.qqgame.hall.common.SyncData.isReconnecting     // Catch:{ all -> 0x00da }
            if (r8 == 0) goto L_0x00c0
            byte r8 = com.tencent.qqgame.hall.common.SyncData.continue_reconnect_times     // Catch:{ all -> 0x00da }
            int r8 = r8 + 1
            byte r8 = (byte) r8     // Catch:{ all -> 0x00da }
            com.tencent.qqgame.hall.common.SyncData.continue_reconnect_times = r8     // Catch:{ all -> 0x00da }
            byte r8 = com.tencent.qqgame.hall.common.SyncData.continue_reconnect_times     // Catch:{ all -> 0x00da }
            if (r8 <= r12) goto L_0x00f3
            r0 = -105(0xffffffffffffff97, float:NaN)
            r5.what = r0     // Catch:{ all -> 0x00da }
            r4.sendMessage(r5)     // Catch:{ all -> 0x00da }
            monitor-exit(r1)     // Catch:{ all -> 0x00da }
        L_0x00bf:
            return
        L_0x00c0:
            boolean r8 = com.tencent.qqgame.hall.common.SyncData.isCurrProxyFromServer     // Catch:{ all -> 0x00da }
            if (r8 == 0) goto L_0x00dd
            int r8 = com.tencent.qqgame.hall.common.SyncData.timerTryServerProxyList     // Catch:{ all -> 0x00da }
            int r8 = r8 + 1
            com.tencent.qqgame.hall.common.SyncData.timerTryServerProxyList = r8     // Catch:{ all -> 0x00da }
            int r8 = com.tencent.qqgame.hall.common.SyncData.timerTryServerProxyList     // Catch:{ all -> 0x00da }
            if (r8 <= r12) goto L_0x00f3
            r0 = 12
            r5.what = r0     // Catch:{ all -> 0x00da }
            r4.sendMessage(r5)     // Catch:{ all -> 0x00da }
            r0 = 0
            r13.curState = r0     // Catch:{ all -> 0x00da }
            monitor-exit(r1)     // Catch:{ all -> 0x00da }
            goto L_0x00bf
        L_0x00da:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00da }
            throw r0
        L_0x00dd:
            int r8 = com.tencent.qqgame.hall.common.SyncData.timerTryLocalProxyList     // Catch:{ all -> 0x00da }
            int r8 = r8 + 1
            com.tencent.qqgame.hall.common.SyncData.timerTryLocalProxyList = r8     // Catch:{ all -> 0x00da }
            int r8 = com.tencent.qqgame.hall.common.SyncData.timerTryLocalProxyList     // Catch:{ all -> 0x00da }
            if (r8 <= r12) goto L_0x00f3
            r0 = 11
            r5.what = r0     // Catch:{ all -> 0x00da }
            r4.sendMessage(r5)     // Catch:{ all -> 0x00da }
            r0 = 0
            r13.curState = r0     // Catch:{ all -> 0x00da }
            monitor-exit(r1)     // Catch:{ all -> 0x00da }
            goto L_0x00bf
        L_0x00f3:
            int r7 = r7 + 1
            r8 = 1
            r0.isTryed = r8     // Catch:{ all -> 0x00da }
            java.lang.String r8 = r0.svrIP     // Catch:{ all -> 0x00da }
            r13.server_ip = r8     // Catch:{ all -> 0x00da }
            short r8 = r0.svrPort     // Catch:{ all -> 0x00da }
            r13.server_port = r8     // Catch:{ all -> 0x00da }
            java.net.Socket r8 = new java.net.Socket     // Catch:{ Exception -> 0x0166 }
            r8.<init>()     // Catch:{ Exception -> 0x0166 }
            r13.socket = r8     // Catch:{ Exception -> 0x0166 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0166 }
            r8.<init>()     // Catch:{ Exception -> 0x0166 }
            java.lang.String r9 = "server address: "
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x0166 }
            java.lang.String r9 = r13.server_ip     // Catch:{ Exception -> 0x0166 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x0166 }
            java.lang.String r9 = ":"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x0166 }
            int r9 = r13.server_port     // Catch:{ Exception -> 0x0166 }
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x0166 }
            java.lang.String r9 = ", supportedType:"
            java.lang.StringBuilder r8 = r8.append(r9)     // Catch:{ Exception -> 0x0166 }
            int r0 = r0.supportedType     // Catch:{ Exception -> 0x0166 }
            java.lang.StringBuilder r0 = r8.append(r0)     // Catch:{ Exception -> 0x0166 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0166 }
            com.tencent.qqgame.hall.common.Tools.debug(r0)     // Catch:{ Exception -> 0x0166 }
            java.net.InetSocketAddress r0 = new java.net.InetSocketAddress     // Catch:{ Exception -> 0x0166 }
            java.lang.String r8 = r13.server_ip     // Catch:{ Exception -> 0x0166 }
            int r9 = r13.server_port     // Catch:{ Exception -> 0x0166 }
            r0.<init>(r8, r9)     // Catch:{ Exception -> 0x0166 }
            java.net.Socket r8 = r13.socket     // Catch:{ Exception -> 0x0166 }
            r9 = 30000(0x7530, float:4.2039E-41)
            r8.connect(r0, r9)     // Catch:{ Exception -> 0x0166 }
            java.net.Socket r0 = r13.socket     // Catch:{ Exception -> 0x0166 }
            r8 = 1
            r0.setTcpNoDelay(r8)     // Catch:{ Exception -> 0x0166 }
            java.net.Socket r0 = r13.socket     // Catch:{ Exception -> 0x0166 }
            r8 = 1
            r9 = 2000(0x7d0, float:2.803E-42)
            r0.setSoLinger(r8, r9)     // Catch:{ Exception -> 0x0166 }
        L_0x0155:
            java.net.Socket r0 = r13.socket     // Catch:{ all -> 0x00da }
            if (r0 != 0) goto L_0x0174
            r0 = -110(0xffffffffffffff92, float:NaN)
            r5.what = r0     // Catch:{ all -> 0x00da }
            r4.sendMessage(r5)     // Catch:{ all -> 0x00da }
            r0 = 0
            r13.curState = r0     // Catch:{ all -> 0x00da }
            monitor-exit(r1)     // Catch:{ all -> 0x00da }
            goto L_0x00bf
        L_0x0166:
            r0 = move-exception
            r0 = 0
            r13.server_ip = r0     // Catch:{ all -> 0x00da }
            r0 = 0
            r13.server_port = r0     // Catch:{ all -> 0x00da }
            r0 = r7
        L_0x016e:
            int r6 = r6 + 1
            byte r6 = (byte) r6
            r7 = r0
            goto L_0x008a
        L_0x0174:
            java.net.Socket r0 = r13.socket     // Catch:{ Exception -> 0x01b5 }
            java.io.OutputStream r0 = r0.getOutputStream()     // Catch:{ Exception -> 0x01b5 }
            r13.out = r0     // Catch:{ Exception -> 0x01b5 }
            java.net.Socket r0 = r13.socket     // Catch:{ Exception -> 0x01b5 }
            java.io.InputStream r0 = r0.getInputStream()     // Catch:{ Exception -> 0x01b5 }
            r13.in = r0     // Catch:{ Exception -> 0x01b5 }
            r0 = 1
            r13.isConnected = r0     // Catch:{ all -> 0x00da }
            r0 = 3
            r13.curState = r0     // Catch:{ all -> 0x00da }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00da }
            r0.<init>()     // Catch:{ all -> 0x00da }
            java.lang.String r3 = "curState: "
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ all -> 0x00da }
            byte r3 = r13.curState     // Catch:{ all -> 0x00da }
            java.lang.StringBuilder r0 = r0.append(r3)     // Catch:{ all -> 0x00da }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00da }
            com.tencent.qqgame.hall.common.Tools.debug(r0)     // Catch:{ all -> 0x00da }
            r3 = r10
        L_0x01a3:
            if (r3 >= r2) goto L_0x01dd
            java.util.Vector<com.tencent.qqgame.hall.common.data.MProxyInfoV2> r0 = com.tencent.qqgame.hall.common.SyncData.proxyList     // Catch:{ all -> 0x00da }
            java.lang.Object r0 = r0.elementAt(r3)     // Catch:{ all -> 0x00da }
            com.tencent.qqgame.hall.common.data.MProxyInfoV2 r0 = (com.tencent.qqgame.hall.common.data.MProxyInfoV2) r0     // Catch:{ all -> 0x00da }
            r4 = 0
            r0.isTryed = r4     // Catch:{ all -> 0x00da }
            int r0 = r3 + 1
            byte r0 = (byte) r0     // Catch:{ all -> 0x00da }
            r3 = r0
            goto L_0x01a3
        L_0x01b5:
            r0 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00da }
            r2.<init>()     // Catch:{ all -> 0x00da }
            java.lang.String r3 = "open stream exception, "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x00da }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00da }
            java.lang.StringBuilder r0 = r2.append(r0)     // Catch:{ all -> 0x00da }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00da }
            com.tencent.qqgame.hall.common.Tools.debug(r0)     // Catch:{ all -> 0x00da }
            r0 = -110(0xffffffffffffff92, float:NaN)
            r5.what = r0     // Catch:{ all -> 0x00da }
            r4.sendMessage(r5)     // Catch:{ all -> 0x00da }
            r0 = 0
            r13.curState = r0     // Catch:{ all -> 0x00da }
            monitor-exit(r1)     // Catch:{ all -> 0x00da }
            goto L_0x00bf
        L_0x01dd:
            r0 = 0
            com.tencent.qqgame.hall.common.SyncData.timerTryLocalProxyList = r0     // Catch:{ all -> 0x00da }
            r0 = 0
            com.tencent.qqgame.hall.common.SyncData.isTryLocalProxyList3 = r0     // Catch:{ all -> 0x00da }
            r0 = 0
            com.tencent.qqgame.hall.common.SyncData.timerTryServerProxyList = r0     // Catch:{ all -> 0x00da }
            r0 = 0
            com.tencent.qqgame.hall.common.SyncData.isReconnecting = r0     // Catch:{ all -> 0x00da }
            r0 = 0
            com.tencent.qqgame.hall.common.SyncData.continue_reconnect_times = r0     // Catch:{ all -> 0x00da }
            com.tencent.qqgame.hall.common.AActivity r0 = com.tencent.qqgame.hall.common.AActivity.currentActivity     // Catch:{ all -> 0x00da }
            android.os.Handler r0 = r0.handle     // Catch:{ all -> 0x00da }
            r2 = -109(0xffffffffffffff93, float:NaN)
            r0.sendEmptyMessage(r2)     // Catch:{ all -> 0x00da }
            monitor-exit(r1)     // Catch:{ all -> 0x00da }
            java.lang.Thread r0 = r13.receiveThread
            if (r0 != 0) goto L_0x022b
            r13.isRRunning = r11
            java.lang.Thread r0 = new java.lang.Thread
            com.tencent.qqgame.hall.communication.SocketCommunicator$1 r1 = new com.tencent.qqgame.hall.communication.SocketCommunicator$1
            r1.<init>()
            r0.<init>(r1)
            r13.receiveThread = r0
            java.lang.Thread r0 = r13.receiveThread
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "receive_c:"
            java.lang.StringBuilder r1 = r1.append(r2)
            int r2 = com.tencent.qqgame.hall.communication.SocketCommunicator.receiveThreadCounter
            int r3 = r2 + 1
            com.tencent.qqgame.hall.communication.SocketCommunicator.receiveThreadCounter = r3
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.setName(r1)
            java.lang.Thread r0 = r13.receiveThread
            r0.start()
        L_0x022b:
            boolean r0 = r13.isSRunning
            if (r0 == 0) goto L_0x02b9
            java.util.Vector<com.tencent.qqgame.common.Msg> r1 = r13.sendQueue     // Catch:{ IOException -> 0x0263, Exception -> 0x0290 }
            monitor-enter(r1)     // Catch:{ IOException -> 0x0263, Exception -> 0x0290 }
        L_0x0232:
            java.util.Vector<com.tencent.qqgame.common.Msg> r0 = r13.sendQueue     // Catch:{ all -> 0x0260 }
            int r0 = r0.size()     // Catch:{ all -> 0x0260 }
            if (r0 <= 0) goto L_0x0282
            java.util.Vector<com.tencent.qqgame.common.Msg> r0 = r13.sendQueue     // Catch:{ all -> 0x0260 }
            r2 = 0
            java.lang.Object r0 = r0.elementAt(r2)     // Catch:{ all -> 0x0260 }
            com.tencent.qqgame.common.Msg r0 = (com.tencent.qqgame.common.Msg) r0     // Catch:{ all -> 0x0260 }
            byte[] r0 = r0.encode()     // Catch:{ all -> 0x0260 }
            java.io.OutputStream r2 = r13.out     // Catch:{ all -> 0x0260 }
            r2.write(r0)     // Catch:{ all -> 0x0260 }
            java.io.OutputStream r0 = r13.out     // Catch:{ all -> 0x0260 }
            r0.flush()     // Catch:{ all -> 0x0260 }
            java.util.Vector<com.tencent.qqgame.common.Msg> r0 = r13.sendQueue     // Catch:{ all -> 0x0260 }
            r2 = 0
            r0.removeElementAt(r2)     // Catch:{ all -> 0x0260 }
            java.lang.String r0 = "send socket msg..."
            com.tencent.qqgame.hall.common.Tools.debug(r0)     // Catch:{ all -> 0x0260 }
            r0 = 0
            com.tencent.qqgame.hall.common.SyncData.timerNetSpeedMayBe = r0     // Catch:{ all -> 0x0260 }
            goto L_0x0232
        L_0x0260:
            r0 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0260 }
            throw r0     // Catch:{ IOException -> 0x0263, Exception -> 0x0290 }
        L_0x0263:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "send message exception inner! detail info: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.qqgame.hall.common.Tools.debug(r0)
            r13.reconnect()
            goto L_0x022b
        L_0x0282:
            java.util.Vector<com.tencent.qqgame.common.Msg> r0 = r13.sendQueue     // Catch:{ InterruptedException -> 0x0289 }
            r0.wait()     // Catch:{ InterruptedException -> 0x0289 }
        L_0x0287:
            monitor-exit(r1)     // Catch:{ all -> 0x0260 }
            goto L_0x022b
        L_0x0289:
            r0 = move-exception
            java.lang.String r0 = "sendThread interrupted..."
            com.tencent.qqgame.hall.common.Tools.debug(r0)     // Catch:{ all -> 0x0260 }
            goto L_0x0287
        L_0x0290:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "send message exception! detail info: "
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.String r0 = r0.toString()
            java.lang.StringBuilder r0 = r1.append(r0)
            java.lang.String r0 = r0.toString()
            com.tencent.qqgame.hall.common.Tools.debug(r0)
            java.io.OutputStream r0 = r13.out
            if (r0 != 0) goto L_0x022b
            java.lang.String r0 = "out is null"
            com.tencent.qqgame.hall.common.Tools.debug(r0)
            r13.reconnect()
            goto L_0x022b
        L_0x02b9:
            r13.removeConnResource()
            goto L_0x00bf
        L_0x02be:
            r0 = r7
            goto L_0x016e
        L_0x02c1:
            r0 = r10
            goto L_0x0077
        L_0x02c4:
            r3 = r0
            goto L_0x0029
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.hall.communication.SocketCommunicator.run():void");
    }

    public boolean sendMsg(Msg msg) {
        synchronized (this.sendQueue) {
            this.sendQueue.addElement(msg);
            this.sendQueue.notifyAll();
        }
        return true;
    }
}
