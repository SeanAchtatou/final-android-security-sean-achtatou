package com.tencent.qqgame.hall.ui;

import android.os.Bundle;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.tencent.qqgame.hall.common.AActivity;
import com.tencent.qqgame.hall.common.Report;
import tencent.qqgame.lord.R;

public class HelpAboutActivity extends AActivity implements AdapterView.OnItemClickListener {
    private static final int STATUS_ABOUT = 2;
    private static final int STATUS_HELP = 1;
    private static final int STATUS_MENU = 0;
    private static final String[] menu = {res.getString(R.string.helpabout_menu_help), res.getString(R.string.helpabout_menu_about)};
    private int status = 0;

    private void showContentView(int i) {
        this.status = i;
        if (i == 0) {
            setContentView((int) R.layout.layout_helpabout_menu);
            setBackgroudDrawable(res, (ViewGroup) findViewById(R.id.lTopLayout));
            setToolbarTitle(res.getString(R.string.helpabout_menu_menu));
            ListView listView = (ListView) findViewById(R.id.helpabout_lv_menu);
            listView.setAdapter((ListAdapter) new ArrayAdapter(this, 17367043, menu));
            listView.setOnItemClickListener(this);
        } else if (i == 1 || i == 2) {
            setContentView((int) R.layout.layout_help);
            WebView webView = (WebView) findViewById(R.id.help_webview_helpView);
            if (i == 1) {
                setToolbarTitle(res.getString(R.string.helpabout_menu_help));
                webView.loadUrl("file:///android_asset/help.html");
            } else if (i == 2) {
                setToolbarTitle(res.getString(R.string.helpabout_menu_about));
                webView.loadUrl("file:///android_asset/about.html");
            }
        }
    }

    /* access modifiers changed from: protected */
    public void handleMessage(Message message) {
        switch (message.what) {
            case 101:
                finish();
                GameActivity.back2Game(this);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        showContentView(0);
        setBackgroudDrawable(res, (ViewGroup) findViewById(R.id.lTopLayout));
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        if (i == 0) {
            showContentView(1);
        } else {
            showContentView(2);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        switch (i) {
            case 4:
                Report.keyBackHitInHallNum++;
                if (this.status == 1 || this.status == 2) {
                    showContentView(0);
                    return true;
                }
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }
}
