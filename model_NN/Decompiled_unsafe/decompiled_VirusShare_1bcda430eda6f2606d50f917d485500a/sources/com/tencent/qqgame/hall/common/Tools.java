package com.tencent.qqgame.hall.common;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import com.tencent.qqgame.lord.ui.LordView;
import tencent.qqgame.lord.R;

public class Tools {
    private static final String TAG_DEBUG = "debug";
    public static byte headSize = LordView.FRAME_BUTTON_CANCEL_NORMAL;
    public static Bitmap imgEmpty = null;
    public static Bitmap imgHeads = null;
    public static final Rect rect1 = new Rect();
    public static final Rect rect2 = new Rect();
    public static int screenHeight = LordView.SCREEN_WIDTH;
    public static int screenWidth = LordView.SCREEN_HEIGHT;

    public static void debug(String str) {
    }

    public static void drawArtString(Canvas canvas, Paint paint, String str, int i, int i2, int i3, int i4) {
        paint.setColor(i4);
        canvas.drawText(str, (float) (i - 1), (float) i2, paint);
        canvas.drawText(str, (float) (i + 1), (float) i2, paint);
        canvas.drawText(str, (float) i, (float) (i2 - 1), paint);
        canvas.drawText(str, (float) i, (float) (i2 + 1), paint);
        paint.setColor(i3);
        canvas.drawText(str, (float) i, (float) i2, paint);
    }

    public static void drawNum(Canvas canvas, Paint paint, Bitmap bitmap, int i, int i2, int i3) {
        if (bitmap != null) {
            drawNum(canvas, paint, bitmap, i, i2, i3, false);
        }
    }

    public static void drawNum(Canvas canvas, Paint paint, Bitmap bitmap, int i, int i2, int i3, boolean z) {
        if (bitmap != null) {
            StringBuffer stringBuffer = new StringBuffer();
            if (i < 0 || i >= 10) {
                stringBuffer.append(i);
            } else if (!z) {
                stringBuffer.append("0").append(i);
            } else {
                stringBuffer.append(i);
            }
            if (z && i >= 0) {
                stringBuffer.insert(0, "+");
            }
            int width = bitmap.getWidth() / 11;
            int height = bitmap.getHeight();
            Rect rect = rect1;
            Rect rect3 = rect2;
            int i4 = i2;
            for (char c : stringBuffer.toString().toCharArray()) {
                int i5 = c - '0';
                if (i5 == -3 || i5 == -5) {
                    i5 = 10;
                }
                rect.set(i5 * width, 0, (i5 + 1) * width, height);
                rect3.set(i4, i3, i4 + width, i3 + height);
                canvas.drawBitmap(bitmap, rect, rect3, paint);
                i4 += width;
            }
        }
    }

    public static void drawQQHead(Canvas canvas, Paint paint, short s, int i, int i2) {
        if (s >= -1) {
            if (imgHeads == null) {
                imgHeads = BitmapFactory.decodeResource(AActivity.res, R.drawable.iconall);
            }
            byte b = headSize;
            Rect rect = rect1;
            Rect rect3 = rect2;
            if (s >= 0 && s < 100) {
                rect.set((s % 25) * b, (s / 25) * b, ((s % 25) * b) + b, ((s / 25) * b) + b);
                rect3.set(i, i2, i + b, b + i2);
                canvas.drawBitmap(imgHeads, rect, rect3, paint);
            } else if (s == -1) {
                if (imgEmpty == null) {
                    imgEmpty = BitmapFactory.decodeResource(AActivity.res, R.drawable.icon_empty);
                }
                canvas.drawBitmap(imgEmpty, (float) i, (float) i2, paint);
            } else {
                rect.set(0, 0, b, b);
                rect3.set(i, i2, i + b, b + i2);
                canvas.drawBitmap(imgHeads, rect, rect3, paint);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x009a A[SYNTHETIC, Splitter:B:25:0x009a] */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00a4 A[SYNTHETIC, Splitter:B:31:0x00a4] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] loadConfigPoker(java.lang.String r7) {
        /*
            r5 = 0
            r6 = 0
            if (r7 == 0) goto L_0x000a
            int r0 = r7.length()
            if (r0 > 0) goto L_0x000c
        L_0x000a:
            r0 = r6
        L_0x000b:
            return r0
        L_0x000c:
            java.lang.String r0 = android.os.Environment.getExternalStorageState()
            java.lang.String r1 = "mounted"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x00b0
            r0 = 54
            byte[] r0 = new byte[r0]
            java.io.File r1 = android.os.Environment.getExternalStorageDirectory()
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0093, all -> 0x00a0 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0093, all -> 0x00a0 }
            r3.<init>()     // Catch:{ Exception -> 0x0093, all -> 0x00a0 }
            java.lang.String r1 = r1.getPath()     // Catch:{ Exception -> 0x0093, all -> 0x00a0 }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ Exception -> 0x0093, all -> 0x00a0 }
            java.lang.String r3 = "/cardtype/tileConfig"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0093, all -> 0x00a0 }
            java.lang.StringBuilder r1 = r1.append(r7)     // Catch:{ Exception -> 0x0093, all -> 0x00a0 }
            java.lang.String r3 = ".pai"
            java.lang.StringBuilder r1 = r1.append(r3)     // Catch:{ Exception -> 0x0093, all -> 0x00a0 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0093, all -> 0x00a0 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0093, all -> 0x00a0 }
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0093, all -> 0x00a0 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0093, all -> 0x00a0 }
            int r2 = r1.available()     // Catch:{ Exception -> 0x00ae }
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x00ae }
            r3 = 0
            int r4 = r1.available()     // Catch:{ Exception -> 0x00ae }
            r1.read(r2, r3, r4)     // Catch:{ Exception -> 0x00ae }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x00ae }
            r3.<init>(r2)     // Catch:{ Exception -> 0x00ae }
            r2 = r5
        L_0x005f:
            int r4 = r0.length     // Catch:{ Exception -> 0x00ae }
            if (r2 >= r4) goto L_0x0089
            r4 = 44
            int r4 = r3.indexOf(r4)     // Catch:{ Exception -> 0x00ae }
            r5 = 0
            java.lang.String r5 = r3.substring(r5, r4)     // Catch:{ Exception -> 0x00ae }
            int r4 = r4 + 1
            java.lang.String r3 = r3.substring(r4)     // Catch:{ Exception -> 0x00ae }
            java.lang.Integer r4 = new java.lang.Integer     // Catch:{ Exception -> 0x00ae }
            java.lang.String r5 = r5.trim()     // Catch:{ Exception -> 0x00ae }
            int r5 = java.lang.Integer.parseInt(r5)     // Catch:{ Exception -> 0x00ae }
            r4.<init>(r5)     // Catch:{ Exception -> 0x00ae }
            byte r4 = r4.byteValue()     // Catch:{ Exception -> 0x00ae }
            r0[r2] = r4     // Catch:{ Exception -> 0x00ae }
            int r2 = r2 + 1
            goto L_0x005f
        L_0x0089:
            if (r1 == 0) goto L_0x000b
            r1.close()     // Catch:{ Exception -> 0x0090 }
            goto L_0x000b
        L_0x0090:
            r1 = move-exception
            goto L_0x000b
        L_0x0093:
            r0 = move-exception
            r1 = r6
        L_0x0095:
            r0.printStackTrace()     // Catch:{ all -> 0x00ac }
            if (r1 == 0) goto L_0x009d
            r1.close()     // Catch:{ Exception -> 0x00a8 }
        L_0x009d:
            r0 = r6
            goto L_0x000b
        L_0x00a0:
            r0 = move-exception
            r1 = r6
        L_0x00a2:
            if (r1 == 0) goto L_0x00a7
            r1.close()     // Catch:{ Exception -> 0x00aa }
        L_0x00a7:
            throw r0
        L_0x00a8:
            r0 = move-exception
            goto L_0x009d
        L_0x00aa:
            r1 = move-exception
            goto L_0x00a7
        L_0x00ac:
            r0 = move-exception
            goto L_0x00a2
        L_0x00ae:
            r0 = move-exception
            goto L_0x0095
        L_0x00b0:
            r0 = r6
            goto L_0x000b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.hall.common.Tools.loadConfigPoker(java.lang.String):byte[]");
    }

    public static int numWidth(Bitmap bitmap, int i, boolean z) {
        if (bitmap == null) {
            return -1;
        }
        StringBuffer stringBuffer = new StringBuffer();
        if (i < 0 || i >= 10) {
            stringBuffer.append(i);
        } else if (!z) {
            stringBuffer.append("0").append(i);
        } else {
            stringBuffer.append(i);
        }
        if (z && i >= 0) {
            stringBuffer.insert(0, "+");
        }
        return (stringBuffer.toString().length() * bitmap.getWidth()) / 11;
    }
}
