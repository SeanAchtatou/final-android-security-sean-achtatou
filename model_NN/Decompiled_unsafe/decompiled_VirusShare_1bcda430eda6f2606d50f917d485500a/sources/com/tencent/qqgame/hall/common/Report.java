package com.tencent.qqgame.hall.common;

import android.util.Log;

public class Report {
    public static final int PAGE_DESKLIST = 3;
    public static final int PAGE_GAME = 4;
    public static final int PAGE_HALLLIST = 1;
    public static final int PAGE_LOGIN = 0;
    public static final int PAGE_MENU = 4;
    public static final int PAGE_ROOMLIST = 2;
    public static int autoLoginHitNum = 0;
    public static int bgMusicOffHitInSettingNum = 0;
    public static int bgMusicOnHitInSettingNum = 0;
    public static int cancelLoginHitNum = 0;
    public static int changeDeskHitNum = 0;
    public static int[] chatTextHitNum = new int[12];
    public static int effectMusicOffHitInSettingNum = 0;
    public static int effectMusicOnHitInSettingNum = 0;
    public static int headHitNum = 0;
    public static int historyAccountNum = 0;
    public static int historyIconHitNum = 0;
    public static int keyBackHitInGameNum = 0;
    public static int keyBackHitInHallNum = 0;
    public static int keyHomeHitInGameNum = 0;
    public static int keyHomeHitInHallNum = 0;
    public static int leaveDeskHitNum = 0;
    public static int menuitemChangeDeskHitNum = 0;
    public static int menuitemChatHitNum = 0;
    public static int menuitemExitHitNum = 0;
    public static int menuitemHelpAboutHitNum = 0;
    public static int menuitemQuickGameHitNum = 0;
    public static int menuitemSysteSettingHitNum = 0;
    public static int menuitemTrustShipHitNum = 0;
    public static int quickGameHitInDeskListNum = 0;
    public static int quickGameHitInHallListNum = 0;
    public static int quickGameHitInRoomListNum = 0;
    public static int quickGameHitNum = 0;
    public static int savePwdHitNum = 0;
    public static String savedReport = null;
    public static int toolbarChatHitNum = 0;
    public static int toolbarExitHitNum = 0;
    public static int toolbarHitNum = 0;
    public static int toolbarSettingHitNum = 0;
    public static int toolbarTrustShipHitNum = 0;
    public static int vibrateOffHitInSettingNum = 0;
    public static int vibrateOnHitInSettingNum = 0;
    public static int watchHitNum = 0;

    public static String getReport() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(historyAccountNum);
        stringBuffer.append("|");
        stringBuffer.append(historyIconHitNum);
        stringBuffer.append("|");
        stringBuffer.append(savePwdHitNum);
        stringBuffer.append("|");
        stringBuffer.append(autoLoginHitNum);
        stringBuffer.append("|");
        stringBuffer.append(quickGameHitNum);
        stringBuffer.append("|");
        stringBuffer.append(cancelLoginHitNum);
        stringBuffer.append("|");
        stringBuffer.append(quickGameHitInHallListNum);
        stringBuffer.append("|");
        stringBuffer.append(quickGameHitInRoomListNum);
        stringBuffer.append("|");
        stringBuffer.append(quickGameHitInDeskListNum);
        stringBuffer.append("|");
        stringBuffer.append(menuitemSysteSettingHitNum);
        stringBuffer.append("|");
        stringBuffer.append(menuitemHelpAboutHitNum);
        stringBuffer.append("|");
        stringBuffer.append(menuitemExitHitNum);
        stringBuffer.append("|");
        stringBuffer.append(menuitemQuickGameHitNum);
        stringBuffer.append("|");
        stringBuffer.append(menuitemChatHitNum);
        stringBuffer.append("|");
        stringBuffer.append(menuitemTrustShipHitNum);
        stringBuffer.append("|");
        stringBuffer.append(menuitemChangeDeskHitNum);
        stringBuffer.append("|");
        stringBuffer.append(watchHitNum);
        stringBuffer.append("|");
        stringBuffer.append(toolbarHitNum);
        stringBuffer.append("|");
        stringBuffer.append(toolbarChatHitNum);
        stringBuffer.append("|");
        stringBuffer.append(toolbarTrustShipHitNum);
        stringBuffer.append("|");
        stringBuffer.append(toolbarSettingHitNum);
        stringBuffer.append("|");
        stringBuffer.append(toolbarExitHitNum);
        stringBuffer.append("|");
        stringBuffer.append(headHitNum);
        stringBuffer.append("|");
        for (int i = 0; i < chatTextHitNum.length; i++) {
            stringBuffer.append(chatTextHitNum[i]);
            if (i != chatTextHitNum.length - 1) {
                stringBuffer.append(",");
            }
        }
        stringBuffer.append("|");
        stringBuffer.append(changeDeskHitNum);
        stringBuffer.append("|");
        stringBuffer.append(leaveDeskHitNum);
        stringBuffer.append("|");
        stringBuffer.append(keyBackHitInGameNum);
        stringBuffer.append("|");
        stringBuffer.append(keyHomeHitInGameNum);
        stringBuffer.append("|");
        stringBuffer.append(keyBackHitInHallNum);
        stringBuffer.append("|");
        stringBuffer.append(keyHomeHitInHallNum);
        stringBuffer.append("|");
        stringBuffer.append(bgMusicOnHitInSettingNum);
        stringBuffer.append("|");
        stringBuffer.append(bgMusicOffHitInSettingNum);
        stringBuffer.append("|");
        stringBuffer.append(effectMusicOnHitInSettingNum);
        stringBuffer.append("|");
        stringBuffer.append(effectMusicOffHitInSettingNum);
        stringBuffer.append("|");
        stringBuffer.append(vibrateOnHitInSettingNum);
        stringBuffer.append("|");
        stringBuffer.append(vibrateOffHitInSettingNum);
        stringBuffer.append("|");
        return stringBuffer.toString();
    }

    public static String loadReport() {
        return savedReport;
    }

    public static void resetReport() {
        historyAccountNum = 0;
        historyIconHitNum = 0;
        savePwdHitNum = 0;
        autoLoginHitNum = 0;
        quickGameHitNum = 0;
        cancelLoginHitNum = 0;
        quickGameHitInHallListNum = 0;
        quickGameHitInRoomListNum = 0;
        quickGameHitInDeskListNum = 0;
        menuitemSysteSettingHitNum = 0;
        menuitemHelpAboutHitNum = 0;
        menuitemExitHitNum = 0;
        menuitemQuickGameHitNum = 0;
        menuitemChatHitNum = 0;
        menuitemTrustShipHitNum = 0;
        menuitemChangeDeskHitNum = 0;
        watchHitNum = 0;
        toolbarHitNum = 0;
        toolbarChatHitNum = 0;
        toolbarTrustShipHitNum = 0;
        toolbarSettingHitNum = 0;
        toolbarExitHitNum = 0;
        headHitNum = 0;
        for (int i = 0; i < chatTextHitNum.length; i++) {
            chatTextHitNum[i] = 0;
        }
        changeDeskHitNum = 0;
        leaveDeskHitNum = 0;
        keyBackHitInGameNum = 0;
        keyHomeHitInGameNum = 0;
        keyBackHitInHallNum = 0;
        keyHomeHitInHallNum = 0;
        bgMusicOnHitInSettingNum = 0;
        bgMusicOffHitInSettingNum = 0;
        effectMusicOnHitInSettingNum = 0;
        effectMusicOffHitInSettingNum = 0;
        vibrateOnHitInSettingNum = 0;
        vibrateOffHitInSettingNum = 0;
    }

    public static void saveReport(int i) {
        savedReport = getReport();
        Log.v("savedReport:" + i, savedReport);
    }
}
