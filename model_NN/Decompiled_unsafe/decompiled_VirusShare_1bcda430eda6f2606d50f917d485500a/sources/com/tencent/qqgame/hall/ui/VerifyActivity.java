package com.tencent.qqgame.hall.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.qqgame.hall.common.AActivity;
import tencent.qqgame.lord.R;

public class VerifyActivity extends AActivity implements View.OnClickListener {
    public static final String KEY_PIC_DATA = "PicData";
    public static final String KEY_PIC_FORMAT = "PicFormat";
    private static final byte MAX_VERIFY_NUM = 7;
    public static final int MSG_GET_VERIFYCODE_ERROR = 6;
    public static final int MSG_GET_VERIFYCODE_SUCC = 7;
    private boolean isVerifyRefresh = false;
    private TextView tvLoadingTopTip;
    private byte[] verifyImage_Data = null;
    private short verifyImage_format = 0;
    private byte verifyNums = 7;

    private void hideSoftKeyBoard(View view) {
        if (view != null) {
            ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void showSoftKeyBoard(View view) {
        if (view != null) {
            view.requestFocus();
            ((InputMethodManager) getSystemService("input_method")).showSoftInput(view, 0);
        }
    }

    /* access modifiers changed from: protected */
    public void handleMessage(Message message) {
        switch (message.what) {
            case 6:
                hideSoftKeyBoard((EditText) findViewById(R.id.login_verify_editText_verfiyInput));
                Toast.makeText(this, message.obj.toString(), 1).show();
                factory.getSendMsgHandle().logout();
                finish();
                return;
            case 7:
                if (!this.isVerifyRefresh) {
                    this.verifyNums = (byte) (this.verifyNums - 1);
                }
                if (this.verifyNums < 6 && !this.isVerifyRefresh) {
                    Toast makeText = Toast.makeText(this, (int) R.string.login_error_verifyCode1, 1);
                    makeText.setGravity(17, 0, -200);
                    makeText.show();
                }
                this.verifyImage_format = (short) message.arg1;
                this.verifyImage_Data = (byte[]) message.obj;
                return;
            default:
                return;
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_loading_btn_ok /*2131296272*/:
                factory.getSendMsgHandle().logout();
                finish();
                return;
            case R.id.login_verify_btn_cancel /*2131296292*/:
                hideSoftKeyBoard((EditText) findViewById(R.id.login_verify_editText_verfiyInput));
                factory.getSendMsgHandle().logout();
                finish();
                return;
            case R.id.login_verify_btn_refresh /*2131296293*/:
                this.isVerifyRefresh = true;
                factory.getSendMsgHandle().verifyCode(1, null);
                ((Button) findViewById(R.id.login_verify_btn_ok)).setEnabled(false);
                ((Button) findViewById(R.id.login_verify_btn_refresh)).setEnabled(false);
                return;
            case R.id.login_verify_btn_ok /*2131296294*/:
                EditText editText = (EditText) findViewById(R.id.login_verify_editText_verfiyInput);
                if (editText == null || editText.getText().toString().length() == 0) {
                    Toast makeText = Toast.makeText(this, (int) R.string.login_no_verifyCode, 1);
                    makeText.setGravity(17, 0, -200);
                    makeText.show();
                    return;
                } else if (editText.getText().length() != 4) {
                    Toast makeText2 = Toast.makeText(this, (int) R.string.login_error_verifyCode, 1);
                    makeText2.setGravity(17, 0, -200);
                    makeText2.show();
                    return;
                } else {
                    factory.getSendMsgHandle().verifyCode(0, editText.getText().toString());
                    hideSoftKeyBoard(editText);
                    setContentView((int) R.layout.layout_loading);
                    this.tvLoadingTopTip = (TextView) findViewById(R.id.tvLoadingTopTip);
                    this.tvLoadingTopTip.setText((int) R.string.login_verify_ing);
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = getIntent();
        this.verifyImage_format = intent.getShortExtra("PicFormat", 0);
        this.verifyImage_Data = intent.getByteArrayExtra("PicData");
        setContentView((int) R.layout.layout_verify);
        this.isVerifyRefresh = false;
        ImageView imageView = (ImageView) findViewById(R.id.login_verify_imgView_verifyCode);
        Bitmap bitmap = null;
        if (this.verifyImage_Data != null) {
            try {
                bitmap = BitmapFactory.decodeByteArray(this.verifyImage_Data, 0, this.verifyImage_Data.length);
            } catch (Exception e) {
            }
        }
        imageView.setImageBitmap(bitmap);
        ((Button) findViewById(R.id.login_verify_btn_refresh)).setOnClickListener(this);
        ((Button) findViewById(R.id.login_verify_btn_ok)).setOnClickListener(this);
        ((Button) findViewById(R.id.login_verify_btn_cancel)).setOnClickListener(this);
        showSoftKeyBoard((EditText) findViewById(R.id.login_verify_editText_verfiyInput));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        return super.onOptionsItemSelected(menuItem);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
