package com.tencent.qqgame.hall.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.qqgame.common.Util;
import com.tencent.qqgame.hall.common.AActivity;
import com.tencent.qqgame.hall.common.IGameView;
import com.tencent.qqgame.hall.common.Report;
import com.tencent.qqgame.hall.common.SyncData;
import com.tencent.qqgame.hall.ui.controls.ViewWrapper;
import tencent.qqgame.lord.R;

public class HallListActivity extends AActivity implements View.OnClickListener, AdapterView.OnItemClickListener, View.OnKeyListener {
    private static final int ITEM_HIGH_LIGHT_COLOR = -256;
    public static final String KEY_HALL_IDLIST = "HallIdList";
    public static final String KEY_HALL_NAMELIST = "HallNameList";
    public static final String KEY_HALL_TYPE = "HallType";
    public static final String KEY_PLAYERINHALLNUM = "PlayerInHallNum";
    public static final String KEY_SERVERID = "ServerID";
    public static final int MSG_2HALL_REPLAY_FAULT = 6;
    public static final int MSG_2HALL_REPLAY_SUCC = 5;
    public static final int MSG_ASK_REPLAY = 7;
    public static final int MSG_ENTER_ROOM_ERROR = 3;
    public static final int MSG_GET_ROOMLIST_ERROR = 1;
    public static final int MSG_GET_ROOMLIST_SUCC = 2;
    public static final int MSG_GET_ZONELIST_SUCC = 4;
    public static final byte ROOM_LIST = 1;
    public static final byte ZONE_LIST = 0;
    private final short SYNC_TIME = 30000;
    /* access modifiers changed from: private */
    public byte curHallType = 0;
    private String[] hallDescription = null;
    private short[] hallIdList = null;
    /* access modifiers changed from: private */
    public String[] hallNameList;
    private ListView list;
    /* access modifiers changed from: private */
    public int[] playerInHallNum = null;
    public short[] serverId;
    private short timerSync = 30000;
    public short totalItemInServer = 0;

    private class IconicAdapter extends BaseAdapter {
        Activity context;

        IconicAdapter(Activity activity) {
            this.context = activity;
        }

        public int getCount() {
            return HallListActivity.this.hallNameList.length;
        }

        public Object getItem(int i) {
            return Integer.valueOf(i);
        }

        public long getItemId(int i) {
            return (long) i;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewWrapper viewWrapper;
            View view2;
            if (view == null) {
                view2 = this.context.getLayoutInflater().inflate((int) R.layout.hall_row, (ViewGroup) null);
                ImageView imageView = (ImageView) view2.findViewById(R.id.icon_hall);
                if (HallListActivity.this.curHallType == 0) {
                    imageView.setImageResource(R.drawable.zone);
                } else {
                    imageView.setImageResource(R.drawable.room);
                }
                Button button = (Button) view2.findViewById(R.id.btn_hall);
                button.setClickable(true);
                button.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        HallListActivity.this.fired(Util.findIndexInArray(((TextView) view.getTag()).getText().toString(), HallListActivity.this.hallNameList));
                    }
                });
                button.setOnTouchListener(new View.OnTouchListener() {
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        if (motionEvent.getAction() == 0) {
                            view.setBackgroundColor(HallListActivity.ITEM_HIGH_LIGHT_COLOR);
                            return false;
                        } else if (motionEvent.getAction() != 3 && motionEvent.getAction() != 1) {
                            return false;
                        } else {
                            view.setBackgroundColor(-1);
                            return false;
                        }
                    }
                });
                viewWrapper = new ViewWrapper(view2);
                view2.setTag(viewWrapper);
            } else {
                viewWrapper = (ViewWrapper) view.getTag();
                view2 = view;
            }
            viewWrapper.getButton().setTag(viewWrapper.getHallName());
            viewWrapper.getHallName().setText(HallListActivity.this.hallNameList[i]);
            viewWrapper.getNumber().setText(HallListActivity.this.playerInHallNum[i] + AActivity.res.getString(R.string.hall_label_people));
            return view2;
        }
    }

    private boolean createHallListMenu(Menu menu) {
        menu.add(0, 3, 0, (int) R.string.menu_quickGame).setIcon((int) R.drawable.menu_quickgame);
        createGeneralMenu(menu);
        return true;
    }

    /* access modifiers changed from: private */
    public void fired(int i) {
        if (i == -1) {
            return;
        }
        if (this.curHallType == 0) {
            SyncData.myZoneID = this.hallIdList[i];
            SyncData.myZoneName = this.hallNameList[i];
            showDialog(0, res.getString(R.string.game_getRoom));
            factory.getSendMsgHandle().getRoomList(this.hallIdList[i]);
            return;
        }
        SyncData.myRoomID = this.hallIdList[i];
        SyncData.myRoomName = this.hallNameList[i];
        SyncData.myRoomSvrID = this.serverId[i];
        showDialog(0, res.getString(R.string.game_enterRoom));
        factory.getSendMsgHandle().getTableList(SyncData.myZoneID, SyncData.myRoomID, SyncData.myRoomSvrID, 0, 20, true);
    }

    private void refreshData(Bundle bundle) {
        setHallInfo(bundle.getByte(KEY_HALL_TYPE), bundle.getStringArray(KEY_HALL_NAMELIST), bundle.getShortArray(KEY_HALL_IDLIST), bundle.getIntArray(KEY_PLAYERINHALLNUM));
        if (this.curHallType == 1) {
            this.serverId = bundle.getShortArray(KEY_SERVERID);
        }
        int selectedItemPosition = this.list.getSelectedItemPosition();
        this.list.setAdapter((ListAdapter) new IconicAdapter(this));
        this.list.setSelection(selectedItemPosition);
        this.list.invalidate();
    }

    private void setHallInfo(byte b, String[] strArr, short[] sArr, int[] iArr) {
        if (this.curHallType != b) {
            this.curHallType = b;
        }
        if (sArr != null) {
            this.hallNameList = strArr;
            this.hallIdList = sArr;
            this.playerInHallNum = iArr;
        }
    }

    /* access modifiers changed from: protected */
    public void handleMessage(Message message) {
        switch (message.what) {
            case AActivity.MSG_GLOABLE_SLEEP:
                if (this.timerSync <= 0) {
                    if (this.curHallType == 0) {
                        factory.getSendMsgHandle().getZoneList();
                    } else {
                        factory.getSendMsgHandle().getRoomList(SyncData.myZoneID);
                    }
                }
                this.timerSync = this.timerSync <= 0 ? 30000 : (short) (this.timerSync - IGameView.MSG_2GAME_DIS_CONNECTED);
                this.handle.sendMessageDelayed(this.handle.obtainMessage(AActivity.MSG_GLOABLE_SLEEP), 1000);
                return;
            case 1:
                removeDialog(this.curr_dialog_key);
                Toast.makeText(this, (int) R.string.game_error_getRoomList, 1).show();
                return;
            case 2:
                if (this.curHallType == 0) {
                    Intent intent = new Intent(this, HallListActivity.class);
                    intent.putExtras(message.getData());
                    startActivity(intent);
                    return;
                }
                refreshData(message.getData());
                return;
            case 3:
                removeDialog(this.curr_dialog_key);
                short s = (short) message.arg1;
                Toast.makeText(this, s == 31 ? res.getString(R.string.game_error_enterRoom1) : s == 8 ? res.getString(R.string.game_error_enterRoom2) : s == 84 ? res.getString(R.string.game_error_enterRoom3) : res.getString(R.string.game_error_enterRoom4), 1).show();
                return;
            case 4:
                if (this.curHallType == 0) {
                    refreshData(message.getData());
                    return;
                }
                return;
            case 5:
                Intent intent2 = new Intent(GameActivity.ACTION);
                intent2.putExtra(GameActivity.KEY_GAME_FROM, (byte) 1);
                AActivity.currentActivity.startActivityIfNeeded(intent2, 0);
                return;
            case 6:
                Toast.makeText(this, (int) R.string.login_error_loginreplay, 1).show();
                removeDialog(this.curr_dialog_key);
                SyncData.writeReplayInfo(getApplicationContext(), true);
                return;
            case 7:
                showDialog(0, res.getString(R.string.login_tip_relaying));
                factory.getSendMsgHandle().replay(SyncData.gameType, 1, SyncData.myZoneID, SyncData.myRoomSvrID, SyncData.myRoomID, SyncData.myTableID, SyncData.mySeatID);
                return;
            default:
                return;
        }
    }

    public void onClick(View view) {
        if (view.getId() == R.id.btn_hall_quickPlay) {
            if (this.curHallType == 1) {
                Report.quickGameHitInRoomListNum++;
            } else {
                Report.quickGameHitInHallListNum++;
            }
            showDialog(0, res.getString(R.string.game_searchQuicking));
            factory.getSendMsgHandle().quickPlay(0);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView((int) R.layout.layout_hall);
        this.vgTopLayout = (ViewGroup) findViewById(R.id.lTopLayout);
        Bundle extras = getIntent().getExtras();
        setHallInfo(extras.getByte(KEY_HALL_TYPE), extras.getStringArray(KEY_HALL_NAMELIST), extras.getShortArray(KEY_HALL_IDLIST), extras.getIntArray(KEY_PLAYERINHALLNUM));
        if (this.curHallType == 1) {
            this.serverId = extras.getShortArray(KEY_SERVERID);
        }
        Button button = (Button) findViewById(R.id.btn_hall_quickPlay);
        button.setOnClickListener(this);
        button.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == 0) {
                    view.setBackgroundResource(R.drawable.quickstar_hghlight);
                    return false;
                } else if (motionEvent.getAction() == 1) {
                    view.setBackgroundResource(R.drawable.quickstart_normal);
                    return false;
                } else {
                    if (motionEvent.getAction() == 3) {
                    }
                    return false;
                }
            }
        });
        button.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View view, boolean z) {
                if (z) {
                    view.setBackgroundResource(R.drawable.quickstar_hghlight);
                } else {
                    view.setBackgroundResource(R.drawable.quickstart_normal);
                }
            }
        });
        this.list = (ListView) findViewById(R.id.hall_list);
        this.list.setAdapter((ListAdapter) new IconicAdapter(this));
        this.list.setOnItemClickListener(this);
        this.list.setOnKeyListener(this);
        if (this.curHallType == 0) {
            SyncData.readReplayInfo(getApplicationContext());
            if (SyncData.myZoneID > -1 && SyncData.myRoomID > -1 && SyncData.myRoomSvrID > -1 && SyncData.myTableID > -1 && SyncData.mySeatID > -1 && SyncData.myZoneName != null && SyncData.myRoomName != null) {
                new AlertDialog.Builder(this).setIcon((int) R.drawable.alert_dialog_icon).setTitle((int) R.string.game_tips_title).setMessage(res.getString(R.string.login_tip_replay)).setCancelable(false).setPositiveButton((int) R.string.cmd_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        HallListActivity.this.handle.sendEmptyMessage(7);
                    }
                }).setNegativeButton((int) R.string.cmd_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SyncData.writeReplayInfo(HallListActivity.this.getApplicationContext(), true);
                        SyncData.readReplayInfo(HallListActivity.this.getApplicationContext());
                    }
                }).show();
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return createHallListMenu(menu);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        ((Button) view.findViewById(R.id.btn_hall)).setBackgroundColor(ITEM_HIGH_LIGHT_COLOR);
    }

    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        View selectedView;
        Button button;
        if (view.getId() != R.id.hall_list || i != 23 || (selectedView = this.list.getSelectedView()) == null || (button = (Button) selectedView.findViewById(R.id.btn_hall)) == null) {
            return false;
        }
        fired(Util.findIndexInArray(((TextView) button.getTag()).getText().toString(), this.hallNameList));
        return true;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        switch (i) {
            case 4:
                Report.keyBackHitInHallNum++;
                if (this.curHallType == 0) {
                    factory.getSendMsgHandle().logout();
                }
                finish();
                return false;
            default:
                super.onKeyDown(i, keyEvent);
                return false;
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        menuItem.getItemId();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        this.handle.removeMessages(AActivity.MSG_GLOABLE_SLEEP);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onRestart() {
        super.onRestart();
        if (this.curHallType == 0) {
            factory.getSendMsgHandle().getZoneList();
        } else {
            factory.getSendMsgHandle().getRoomList(SyncData.myZoneID);
        }
        this.timerSync = 30000;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        this.handle.sendMessageDelayed(this.handle.obtainMessage(AActivity.MSG_GLOABLE_SLEEP), 1000);
        if (this.curHallType == 1) {
            setToolbarTitle(SyncData.myZoneName);
        }
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }
}
