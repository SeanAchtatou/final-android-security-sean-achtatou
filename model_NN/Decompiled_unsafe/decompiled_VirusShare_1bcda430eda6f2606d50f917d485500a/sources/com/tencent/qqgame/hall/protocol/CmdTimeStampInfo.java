package com.tencent.qqgame.hall.protocol;

public class CmdTimeStampInfo {
    public short cmd = 0;
    public boolean needSendReq = false;
    public int timeStamp = 0;

    public CmdTimeStampInfo() {
    }

    public CmdTimeStampInfo(short s, int i) {
        this.cmd = s;
        this.timeStamp = i;
    }
}
