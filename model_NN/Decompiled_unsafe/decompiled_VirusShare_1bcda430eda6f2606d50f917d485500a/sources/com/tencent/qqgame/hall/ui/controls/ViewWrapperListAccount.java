package com.tencent.qqgame.hall.ui.controls;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import tencent.qqgame.lord.R;

public class ViewWrapperListAccount {
    View base;
    ImageButton icon = null;
    TextView label = null;

    public ViewWrapperListAccount(View view) {
        this.base = view;
    }

    public ImageButton getButton() {
        if (this.icon == null) {
            this.icon = (ImageButton) this.base.findViewById(R.id.imgView_delete);
        }
        return this.icon;
    }

    public TextView getLabel() {
        if (this.label == null) {
            this.label = (TextView) this.base.findViewById(R.id.tv_account_name);
        }
        return this.label;
    }
}
