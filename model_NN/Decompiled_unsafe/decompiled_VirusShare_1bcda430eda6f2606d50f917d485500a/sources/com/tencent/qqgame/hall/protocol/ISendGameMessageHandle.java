package com.tencent.qqgame.hall.protocol;

public interface ISendGameMessageHandle {
    void chat(String str);

    void handUp();

    void logout();

    void quickPlay(short s);

    void sendGameMessage(byte[] bArr);

    void tickPlayer(long j);
}
