package com.tencent.qqgame.hall.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import com.tencent.qqgame.hall.common.AActivity;
import com.tencent.qqgame.hall.common.SyncData;

public class NotificationActivity extends AActivity {
    /* access modifiers changed from: protected */
    public void handleMessage(Message message) {
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        finish();
        if (SyncData.isLogined) {
            Intent intent = new Intent(this, GameActivity.class);
            intent.setFlags(131072);
            intent.putExtra(GameActivity.KEY_RETURN_FROM_WHERE, (byte) 1);
            startActivity(intent);
        }
    }
}
