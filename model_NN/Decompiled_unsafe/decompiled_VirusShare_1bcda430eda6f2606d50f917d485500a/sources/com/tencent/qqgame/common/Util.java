package com.tencent.qqgame.common;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Vector;

public class Util {
    public static final byte MIRROR_X = 1;
    public static final byte MIRROR_Y = 2;

    public static String[] breakLine(String str, int i) {
        if (str == null) {
            return new String[]{""};
        } else if (i >= str.length()) {
            return new String[]{str};
        } else {
            Vector vector = new Vector();
            int i2 = 0;
            while (true) {
                if (i2 + i < str.length()) {
                    vector.add(str.substring(i2, i2 + i));
                    i2 += i;
                    if (i2 >= str.length()) {
                        break;
                    }
                } else {
                    vector.add(str.substring(i2, str.length()));
                    break;
                }
            }
            String[] strArr = new String[vector.size()];
            vector.copyInto(strArr);
            return strArr;
        }
    }

    public static long checkQQNum(long j) {
        if (j <= 10000 || j > 4294967296L) {
            return -1;
        }
        return j;
    }

    public static long checkQQNum(String str) {
        try {
            return checkQQNum(Long.parseLong(str));
        } catch (Exception e) {
            return -1;
        }
    }

    public static int convert2Unsigned(long j) {
        int i = (int) j;
        return (j >= 4294967296L || j <= 2147483647L) ? i : (int) (j - 4294967296L);
    }

    public static long convertUnsigned2Long(int i) {
        return i < 0 ? 4294967296L + ((long) i) : (long) i;
    }

    public static void copyData(byte[] bArr, int i, byte[] bArr2, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            bArr[i + i3] = bArr2[i3];
        }
    }

    public static int[] effect_mirror(int[] iArr, int i, int i2, byte b) {
        if (iArr == null || i <= 0 || i2 <= 0) {
            return null;
        }
        int length = iArr.length;
        if (b == 1) {
            int i3 = length / 2;
            for (int i4 = 0; i4 < i3; i4++) {
                int i5 = iArr[i4];
                iArr[i4] = iArr[(length - 1) - i4];
                iArr[(length - 1) - i4] = i5;
            }
        } else {
            for (int i6 = 0; i6 < i2; i6++) {
                int i7 = (i6 + 1) * i;
                for (int i8 = 0; i8 < i / 2; i8++) {
                    int i9 = iArr[(i6 * i) + i8];
                    iArr[(i6 * i) + i8] = iArr[(i7 - 1) - i8];
                    iArr[(i7 - 1) - i8] = i9;
                }
            }
        }
        return iArr;
    }

    public static int findIndexInArray(String str, String[] strArr) {
        if (strArr == null || str == null) {
            return -1;
        }
        for (int i = 0; i < strArr.length; i++) {
            if (strArr[i].equals(str)) {
                return i;
            }
        }
        return -1;
    }

    public static void getByteFromChars(byte[] bArr, int i, char[] cArr, int i2, int i3) {
        for (int i4 = 0; i4 < i3; i4++) {
            word2Byte(bArr, (i4 * 2) + i, (short) cArr[i4 + i2]);
        }
    }

    public static void getBytesData(byte[] bArr, int i, byte[] bArr2, int i2) {
        for (int i3 = 0; i3 < i2; i3++) {
            bArr2[i3] = bArr[i + i3];
        }
    }

    public static long getCurrentTimeStamp() {
        return new Date().getTime();
    }

    public static short getHShortData(byte[] bArr, int i) {
        try {
            return (short) ((int) (((bArr[i + 1] >= 0 ? (long) bArr[i + 1] : (long) (bArr[i + 1] + 256)) << 8) + (bArr[i] >= 0 ? (long) bArr[i] : (long) (bArr[i] + 256))));
        } catch (Exception e) {
            return 42;
        }
    }

    public static int getIntData(byte[] bArr, int i) {
        return (int) (((((((bArr[i] >= 0 ? (long) bArr[i] : (long) (bArr[i] + 256)) << 8) + (bArr[i + 1] >= 0 ? (long) bArr[i + 1] : (long) (bArr[i + 1] + 256))) << 8) + (bArr[i + 2] >= 0 ? (long) bArr[i + 2] : (long) (bArr[i + 2] + 256))) << 8) + (bArr[i + 3] >= 0 ? (long) bArr[i + 3] : (long) (bArr[i + 3] + 256)));
    }

    public static long getLongData(byte[] bArr, int i) {
        return ((((((bArr[i] >= 0 ? (long) bArr[i] : (long) (bArr[i] + 256)) << 8) + (bArr[i + 1] >= 0 ? (long) bArr[i + 1] : (long) (bArr[i + 1] + 256))) << 8) + (bArr[i + 2] >= 0 ? (long) bArr[i + 2] : (long) (bArr[i + 2] + 256))) << 8) + (bArr[i + 3] >= 0 ? (long) bArr[i + 3] : (long) (bArr[i + 3] + 256));
    }

    private static void getNetCharsData(byte[] bArr, int i, char[] cArr, int i2) {
        try {
            short shortData = getShortData(bArr, i);
            if (shortData == -1) {
                for (int i3 = 1; i3 < i2; i3++) {
                    cArr[i3 - 1] = (char) getShortData(bArr, (i3 * 2) + i);
                }
            } else if (shortData == -2) {
                for (int i4 = 1; i4 < i2; i4++) {
                    cArr[i4 - 1] = (char) getHShortData(bArr, (i4 * 2) + i);
                }
            } else {
                for (int i5 = 0; i5 < i2; i5++) {
                    cArr[i5] = (char) getHShortData(bArr, (i5 * 2) + i);
                }
            }
            if (i2 > 1) {
                cArr[i2 - 1] = 0;
            }
        } catch (Exception e) {
            for (int i6 = 0; i6 < i2; i6++) {
                cArr[i6] = '*';
            }
        }
    }

    public static short getShortData(byte[] bArr, int i) {
        return (short) ((int) (((bArr[i] >= 0 ? (long) bArr[i] : (long) (bArr[i] + 256)) << 8) + (bArr[i + 1] >= 0 ? (long) bArr[i + 1] : (long) (bArr[i + 1] + 256))));
    }

    public static String getString(DataInputStream dataInputStream, short s) {
        if (s <= 0) {
            return "";
        }
        byte[] bArr = new byte[s];
        int i = 0;
        while (i < s) {
            try {
                bArr[i] = dataInputStream.readByte();
                i++;
            } catch (IOException e) {
            }
        }
        StringBuffer stringBuffer = new StringBuffer();
        char[] cArr = new char[(s / 2)];
        getNetCharsData(bArr, 0, cArr, bArr.length / 2);
        for (int i2 = 0; i2 < cArr.length; i2++) {
            if (cArr[i2] != 0) {
                stringBuffer.append(cArr[i2]);
            }
        }
        return stringBuffer.toString();
    }

    public static int[] getTransColor(int i, int i2, int i3) {
        int i4 = i2 >> 24;
        int i5 = i2 >> 16;
        int i6 = (i2 >> 8) & 255;
        int i7 = i2 & 255;
        int i8 = (((i3 >> 16) - i5) * 100) / (i - 2);
        int i9 = ((((i3 >> 8) & 255) - i6) * 100) / (i - 2);
        int i10 = (((i3 & 255) - i7) * 100) / (i - 2);
        int[] iArr = new int[i];
        iArr[0] = i2;
        iArr[i - 1] = i3;
        for (int i11 = 1; i11 < i - 1; i11++) {
            iArr[i11] = (i4 << 24) | (((((i11 * i8) / 100) + i5) | 0) << 16) | (((((i11 * i9) / 100) + i6) | 0) << 8) | (((i11 * i10) / 100) + i7);
        }
        return iArr;
    }

    public static boolean isQQPsw(String str) {
        return str != null && str.length() >= 0 && str.length() <= 16;
    }

    public static StringBuffer replace(StringBuffer stringBuffer, String str, String str2) {
        if (stringBuffer == null || str == null || str2 == null) {
            return stringBuffer;
        }
        int indexOf = stringBuffer.indexOf(str);
        while (indexOf != -1) {
            stringBuffer.replace(indexOf, str.length() + indexOf, str2);
            indexOf = stringBuffer.indexOf(str);
        }
        return stringBuffer;
    }

    public static void threadSleep(int i) {
        try {
            Thread.sleep((long) i);
        } catch (InterruptedException e) {
        }
    }

    public static void word2Byte(byte[] bArr, int i, int i2) {
        bArr[i] = (byte) (i2 >> 24);
        bArr[i + 1] = (byte) (i2 >> 16);
        bArr[i + 2] = (byte) (i2 >> 8);
        bArr[i + 3] = (byte) i2;
    }

    public static void word2Byte(byte[] bArr, int i, long j) {
        bArr[i] = (byte) ((int) (j >> 56));
        bArr[i + 1] = (byte) ((int) (j >> 48));
        bArr[i + 2] = (byte) ((int) (j >> 40));
        bArr[i + 3] = (byte) ((int) (j >> 32));
        bArr[i + 4] = (byte) ((int) (j >> 24));
        bArr[i + 5] = (byte) ((int) (j >> 16));
        bArr[i + 6] = (byte) ((int) (j >> 8));
        bArr[i + 7] = (byte) ((int) j);
    }

    public static void word2Byte(byte[] bArr, int i, short s) {
        bArr[i] = (byte) (s >> 8);
        bArr[i + 1] = (byte) s;
    }
}
