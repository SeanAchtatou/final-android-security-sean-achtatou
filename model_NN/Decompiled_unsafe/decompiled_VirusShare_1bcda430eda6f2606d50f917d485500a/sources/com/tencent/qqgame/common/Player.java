package com.tencent.qqgame.common;

public class Player {
    public static final short PLAYER_STATUS_FREE = 1;
    public static final short PLAYER_STATUS_OFFLINE = 5;
    public static final short PLAYER_STATUS_PLAYING = 4;
    public static final short PLAYER_STATUS_READY = 3;
    public static final short PLAYER_STATUS_SIT = 2;
    public static final short PLAYER_STATUS_VIEW = 6;
    public int continuS = 0;
    public short foreS = 0;
    public short iconId;
    public int identity;
    public int money;
    public String nickName;
    public int point;
    public short runP;
    public short seatId;
    public byte sex;
    public short status = 0;
    public final long uid;
    public short winP;

    public Player(long j, String str, byte b, short s, short s2, int i, int i2, int i3, short s3, short s4, short s5) {
        this.uid = j;
        this.nickName = str;
        this.sex = b;
        this.seatId = s;
        this.iconId = s2;
        this.point = i;
        this.identity = i2;
        this.money = i3;
        this.status = s3;
        this.winP = s4;
        this.runP = s5;
    }

    public boolean isSuperPlayer() {
        return (this.identity & 192) != 0;
    }
}
