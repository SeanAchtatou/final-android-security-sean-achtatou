package com.tencent.qqgame.common;

import com.tencent.qqgame.lord.ui.LordView;
import java.util.Vector;

/*  JADX ERROR: NullPointerException in pass: ExtractFieldInit
    java.lang.NullPointerException
    	at jadx.core.utils.BlockUtils.isAllBlocksEmpty(BlockUtils.java:608)
    	at jadx.core.dex.visitors.ExtractFieldInit.getConstructorsList(ExtractFieldInit.java:241)
    	at jadx.core.dex.visitors.ExtractFieldInit.moveCommonFieldsInit(ExtractFieldInit.java:122)
    	at jadx.core.dex.visitors.ExtractFieldInit.visit(ExtractFieldInit.java:43)
    */
public class DDZCards {
    Vector cards;
    int nLen;
    int nLevel;
    int nType;

    /* access modifiers changed from: package-private */
    public void AddCard(byte b, int i, int i2, boolean z) {
        LordCard lordCard = new LordCard(b);
        lordCard.selected = z;
        this.cards.addElement(lordCard);
    }

    /* access modifiers changed from: package-private */
    public void AddCard(int i, int i2, boolean z) {
        AddCard(getNumberFromColorValue(i, i2), i, i2, z);
    }

    /* access modifiers changed from: package-private */
    public void AddCard(LordCard lordCard) {
        this.cards.addElement(lordCard);
    }

    /* access modifiers changed from: package-private */
    public void AddCards(DDZCards dDZCards) {
        for (int i = 0; i < dDZCards.getCurrentLength(); i++) {
            AddCard(dDZCards.getLordCardByIndex(i));
        }
    }

    /* access modifiers changed from: package-private */
    public boolean Contain(DDZCards dDZCards) {
        return Contain(dDZCards, false);
    }

    /* access modifiers changed from: package-private */
    public boolean Contain(DDZCards dDZCards, boolean z) {
        int[] iArr = new int[20];
        for (int i = 0; i < 20; i++) {
            iArr[i] = 0;
        }
        if ((z || dDZCards.getCurrentLength() > getCurrentLength()) && (!z || dDZCards.getCurrentLength() != getCurrentLength())) {
            return false;
        }
        boolean z2 = false;
        for (int i2 = 0; i2 < dDZCards.getCurrentLength(); i2++) {
            int i3 = 0;
            while (true) {
                if (i3 < getCurrentLength()) {
                    if (iArr[i3] == 0 && dDZCards.getValueByIndex(i2) == getValueByIndex(i3)) {
                        iArr[i3] = 1;
                        z2 = true;
                        break;
                    }
                    i3++;
                } else {
                    z2 = false;
                    break;
                }
            }
            if (!z2) {
                return false;
            }
        }
        return z2;
    }

    /* access modifiers changed from: package-private */
    public int GetTupleInfo(byte[] bArr) {
        for (int i = 0; i < bArr.length; i++) {
            bArr[i] = 0;
        }
        for (int i2 = 0; i2 < getCurrentLength(); i2++) {
            int valueByIndex = getValueByIndex(i2);
            bArr[valueByIndex] = (byte) (bArr[valueByIndex] + 1);
        }
        int i3 = 0;
        for (int i4 = 0; i4 < 16; i4++) {
            if (bArr[i4] > 0) {
                i3++;
            }
        }
        return i3;
    }

    /* access modifiers changed from: package-private */
    public void ReleaseAll() {
        this.cards.removeAllElements();
        this.nLen = 0;
        this.nType = 0;
        this.nLevel = 0;
    }

    /* access modifiers changed from: package-private */
    public void SortDetail() {
    }

    public DDZCards copy(DDZCards dDZCards) {
        DDZCards dDZCards2 = new DDZCards();
        Object[] objArr = new Object[dDZCards.cards.size()];
        dDZCards.cards.copyInto(objArr);
        for (Object obj : objArr) {
            dDZCards2.cards.addElement((LordCard) obj);
        }
        dDZCards2.nLen = dDZCards.nLen;
        dDZCards2.nType = dDZCards.nType;
        dDZCards2.nLevel = dDZCards.nLevel;
        return dDZCards2;
    }

    public int getCurrentLength() {
        return this.cards.size();
    }

    /* access modifiers changed from: package-private */
    public int getLen() {
        return this.cards.size();
    }

    /* access modifiers changed from: package-private */
    public LordCard getLordCardByIndex(int i) {
        if (i < 0 || i >= this.cards.size()) {
            return null;
        }
        return (LordCard) this.cards.elementAt(i);
    }

    /* access modifiers changed from: package-private */
    public int getNumberByIndex(int i) {
        if (i < 0 || i >= this.cards.size()) {
            return 0;
        }
        return ((LordCard) this.cards.elementAt(i)).number;
    }

    /* access modifiers changed from: package-private */
    public byte getNumberFromColorValue(int i, int i2) {
        if (i2 <= 13) {
            return (byte) (((i - 1) * 13) + i2);
        }
        if (i2 == 14) {
            return 53;
        }
        if (i2 == 15) {
            return LordView.FRAME_BUTTON_3FEN_DISABLE;
        }
        return 0;
    }

    public int getValueByIndex(int i) {
        if (i < 0 || i >= this.cards.size()) {
            return 0;
        }
        return ((LordCard) this.cards.elementAt(i)).value;
    }

    /* access modifiers changed from: package-private */
    public void swap(int i, int i2) {
        if (i >= 0 || i2 >= 0 || i < getCurrentLength() || i2 < getCurrentLength()) {
            LordCard lordCardByIndex = getLordCardByIndex(i);
            this.cards.setElementAt((LordCard) this.cards.elementAt(i2), i);
            this.cards.setElementAt(lordCardByIndex, i2);
        }
    }
}
