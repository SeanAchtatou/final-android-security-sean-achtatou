package com.tencent.qqgame.common;

import java.util.Vector;

public class SearchHint {
    public static final int BOMB = 15;
    public static final int CARD_TYPE_CONTINUE_3_TUPLE_PLUS_CONTINUE_2_TUPLE = 12;
    public static final int CARD_TYPE_CONTINUE_3_TUPLE_PLUS_CONTINUE_SINGLE = 11;
    public static final int FOUR_ONE = 13;
    public static final int FOUR_TWO = 14;
    static int MAX_INHAND_POKER_NUM = 20;
    public static final int MULTI_THREE_ONE = 10;
    public static final int MULTI_THREE_TWO = 9;
    public static final int NOCARD = 99;
    public static final int NOTHING = 0;
    public static final int ONE = 1;
    public static final int ONE_TURN = 6;
    public static final int ROCKET = 16;
    public static final int THREE = 3;
    public static final int THREE_ONE = 4;
    public static final int THREE_TURN = 8;
    public static final int THREE_TWO = 5;
    public static final int TWO = 2;
    public static final int TWO_TURN = 7;
    static boolean fHasBomb = true;
    static boolean fHasDuizi = true;
    static boolean fHasSan = true;
    static boolean fHasSi = true;
    public static boolean haveSearched = false;
    static DDZCards[] m_DDZCardsAIMain = new DDZCards[30];
    static DDZCards[] m_DDZCardsAISub = new DDZCards[50];
    static DDZCards m_DDZCardsAim = new DDZCards();
    static DDZCards[] m_DDZCardsFirstOut = new DDZCards[50];
    static DDZCards[] m_DDZCardsHint = new DDZCards[30];
    static int m_nAIMainCount = 0;
    static int m_nAISubCount = 0;
    static int m_nAimCount;
    static int m_nFirstOutCount = 0;
    static int m_nHintCount;
    static int m_nHintPos;
    static int m_nMainCount = 0;
    static int m_nSrcCount;
    static int m_nSubCount = 0;
    static int m_nSubNum = 0;

    static boolean Check_Card_Type_CARD_TYPE_2_JOKER(DDZCards dDZCards) {
        if (2 == dDZCards.getCurrentLength()) {
            SortByLevel(dDZCards);
            if (dDZCards.getValueByIndex(0) == 15 && dDZCards.getValueByIndex(1) == 14) {
                DDZCards copy = dDZCards.copy(dDZCards);
                copy.nLen = 1;
                copy.nLevel = GetLevel(copy.getLordCardByIndex(0));
                copy.nType = 16;
                System.out.println(">> ROCKET");
                return true;
            }
        }
        return false;
    }

    static boolean Check_Card_Type_CARD_TYPE_2_TUPLE(DDZCards dDZCards) {
        if (2 != dDZCards.getCurrentLength() || dDZCards.getValueByIndex(0) != dDZCards.getValueByIndex(1)) {
            return false;
        }
        dDZCards.nLen = 1;
        dDZCards.nLevel = GetLevel(dDZCards.getLordCardByIndex(0));
        dDZCards.nType = 2;
        System.out.println("nType = TWO");
        return true;
    }

    static boolean Check_Card_Type_CARD_TYPE_3_TUPLE(DDZCards dDZCards) {
        if (3 != dDZCards.getCurrentLength() || dDZCards.getValueByIndex(0) != dDZCards.getValueByIndex(1) || dDZCards.getValueByIndex(0) != dDZCards.getValueByIndex(2)) {
            return false;
        }
        dDZCards.nLen = 1;
        dDZCards.nLevel = GetLevel(dDZCards.getLordCardByIndex(0));
        dDZCards.nType = 3;
        return true;
    }

    static boolean Check_Card_Type_CARD_TYPE_3_TUPLE_PLUS_2_TUPLE(DDZCards dDZCards) {
        if (5 != dDZCards.getCurrentLength()) {
            return false;
        }
        SortByLevel(dDZCards);
        if (dDZCards.getValueByIndex(0) != dDZCards.getValueByIndex(1)) {
            return false;
        }
        if (dDZCards.getValueByIndex(3) != dDZCards.getValueByIndex(4)) {
            return false;
        }
        if (dDZCards.getValueByIndex(0) == dDZCards.getValueByIndex(4)) {
            return false;
        }
        if (dDZCards.getValueByIndex(0) != dDZCards.getValueByIndex(2) && dDZCards.getValueByIndex(4) != dDZCards.getValueByIndex(2)) {
            return false;
        }
        dDZCards.nLen = 1;
        dDZCards.nLevel = GetLevel(dDZCards.getLordCardByIndex(2));
        dDZCards.nType = 5;
        return true;
    }

    static boolean Check_Card_Type_CARD_TYPE_3_TUPLE_PLUS_SINGLE(DDZCards dDZCards) {
        if (4 == dDZCards.getCurrentLength()) {
            SortByLevel(dDZCards);
            if (dDZCards.getValueByIndex(2) != dDZCards.getValueByIndex(1)) {
                System.out.println("nType = THREE_ONE false");
                return false;
            } else if ((dDZCards.getValueByIndex(0) != dDZCards.getValueByIndex(1) && dDZCards.getValueByIndex(2) == dDZCards.getValueByIndex(3)) || (dDZCards.getValueByIndex(0) == dDZCards.getValueByIndex(1) && dDZCards.getValueByIndex(2) != dDZCards.getValueByIndex(3))) {
                dDZCards.nLen = 1;
                dDZCards.nLevel = GetLevel(dDZCards.getLordCardByIndex(1));
                dDZCards.nType = 4;
                return true;
            }
        }
        return false;
    }

    static boolean Check_Card_Type_CARD_TYPE_4_BOMB(DDZCards dDZCards) {
        if (4 != dDZCards.getCurrentLength()) {
            return false;
        }
        for (int i = 1; i < 4; i++) {
            if (dDZCards.getValueByIndex(0) != dDZCards.getValueByIndex(i)) {
                return false;
            }
        }
        dDZCards.nLen = 1;
        dDZCards.nLevel = GetLevel(dDZCards.getLordCardByIndex(0));
        dDZCards.nType = 15;
        return true;
    }

    static boolean Check_Card_Type_CARD_TYPE_4_TUPLE_PLUS_2_2_TUPLE(DDZCards dDZCards) {
        if (8 == dDZCards.getCurrentLength()) {
            byte[] bArr = new byte[16];
            if (3 != dDZCards.GetTupleInfo(bArr)) {
                return false;
            }
            for (int i = 0; i < dDZCards.getCurrentLength(); i++) {
                if (bArr[dDZCards.getValueByIndex(i)] % 2 != 0) {
                    return false;
                }
            }
            for (int i2 = 0; i2 < dDZCards.getCurrentLength(); i2++) {
                if (4 == bArr[dDZCards.getValueByIndex(i2)]) {
                    dDZCards.nLen = 1;
                    dDZCards.nLevel = GetLevel(dDZCards.getValueByIndex(i2));
                    dDZCards.nType = 14;
                    return true;
                }
            }
        }
        return false;
    }

    static boolean Check_Card_Type_CARD_TYPE_4_TUPLE_PLUS_2_SINGLE(DDZCards dDZCards) {
        System.out.println("------------in Check_Card_Type_CARD_TYPE_4_TUPLE_PLUS_2_SINGLE---------:");
        if (6 == dDZCards.getCurrentLength()) {
            byte[] bArr = new byte[16];
            int GetTupleInfo = dDZCards.GetTupleInfo(bArr);
            if (3 != GetTupleInfo && 2 != GetTupleInfo) {
                return false;
            }
            for (int i = 0; i < dDZCards.getCurrentLength(); i++) {
                if (4 == bArr[dDZCards.getValueByIndex(i)]) {
                    dDZCards.nLen = 1;
                    dDZCards.nLevel = GetLevel(dDZCards.getValueByIndex(i));
                    dDZCards.nType = 13;
                    return true;
                }
            }
        }
        return false;
    }

    static boolean Check_Card_Type_CARD_TYPE_CONTINUE_2_TUPLE(DDZCards dDZCards) {
        if (dDZCards.getCurrentLength() % 2 != 0 || dDZCards.getCurrentLength() <= 5) {
            return false;
        }
        SortByLevel(dDZCards);
        if (GetLevel(dDZCards.getLordCardByIndex(0)) > GetLevel(1)) {
            System.out.println("nType = TWO_TURN false");
            return false;
        }
        for (int i = 0; i < dDZCards.getCurrentLength() / 2; i++) {
            if (dDZCards.getValueByIndex(i * 2) != dDZCards.getValueByIndex((i * 2) + 1)) {
                System.out.println("nType = TWO_TURN false2");
                return false;
            }
        }
        for (int i2 = 0; i2 < (dDZCards.getCurrentLength() / 2) - 1; i2++) {
            if (GetLevel(dDZCards.getLordCardByIndex(i2 * 2)) != GetLevel(dDZCards.getLordCardByIndex((i2 * 2) + 2)) + 1) {
                System.out.println("nType = TWO_TURN false3");
                return false;
            }
        }
        dDZCards.nLen = dDZCards.getCurrentLength() / 2;
        dDZCards.nLevel = GetLevel(dDZCards.getLordCardByIndex(0));
        dDZCards.nType = 7;
        return true;
    }

    static boolean Check_Card_Type_CARD_TYPE_CONTINUE_3_TUPLE(DDZCards dDZCards) {
        if (dDZCards.getCurrentLength() % 3 != 0 || dDZCards.getCurrentLength() <= 5) {
            return false;
        }
        SortByLevel(dDZCards);
        if (GetLevel(dDZCards.getLordCardByIndex(0)) > GetLevel(1)) {
            System.out.println("nType = THREE_TURN false1");
            return false;
        }
        int i = 0;
        while (i < dDZCards.getCurrentLength() / 3) {
            if (dDZCards.getValueByIndex(i * 3) == dDZCards.getValueByIndex((i * 3) + 1) && dDZCards.getValueByIndex(i * 3) == dDZCards.getValueByIndex((i * 3) + 2)) {
                i++;
            } else {
                System.out.println("nType = THREE_TURN false2");
                return false;
            }
        }
        for (int i2 = 0; i2 < (dDZCards.getCurrentLength() / 3) - 1; i2++) {
            if (GetLevel(dDZCards.getLordCardByIndex(i2 * 3)) != GetLevel(dDZCards.getLordCardByIndex((i2 * 3) + 3)) + 1) {
                System.out.println("nType = THREE_TURN false3");
                return false;
            }
        }
        dDZCards.nLen = dDZCards.getCurrentLength() / 3;
        dDZCards.nLevel = GetLevel(dDZCards.getLordCardByIndex(0));
        dDZCards.nType = 8;
        return true;
    }

    static boolean Check_Card_Type_CARD_TYPE_CONTINUE_3_TUPLE_PLUS_CONTINUE_2_TUPLE(DDZCards dDZCards) {
        if (dDZCards.getCurrentLength() % 5 != 0 || dDZCards.getCurrentLength() <= 9) {
            return false;
        }
        byte[] bArr = new byte[16];
        if ((dDZCards.getCurrentLength() * 2) / 5 != dDZCards.GetTupleInfo(bArr)) {
            return false;
        }
        byte[] bArr2 = new byte[16];
        byte[] bArr3 = new byte[16];
        int i = 0;
        int i2 = 0;
        for (int i3 = 0; i3 < dDZCards.getCurrentLength(); i3++) {
            if (2 == bArr[dDZCards.getValueByIndex(i3)]) {
                i2++;
                bArr2[GetLevel(dDZCards.getLordCardByIndex(i3))] = 1;
            } else if (3 != bArr[dDZCards.getValueByIndex(i3)]) {
                return false;
            } else {
                i++;
                bArr3[GetLevel(dDZCards.getLordCardByIndex(i3))] = 1;
            }
        }
        int i4 = i2 / 2;
        int i5 = i / 3;
        if (i4 != i5) {
            return false;
        }
        if (i5 * 5 != dDZCards.getCurrentLength()) {
            return false;
        }
        int i6 = 0;
        int i7 = 0;
        for (int i8 = 1; i8 < 16; i8++) {
            if (!(bArr2[i8 - 1] == 0 || bArr2[i8] == 0)) {
                bArr2[i8] = (byte) (bArr2[i8] + bArr2[i8 - 1]);
                if (bArr2[i8] == i4) {
                    i7 = i8;
                }
            }
            if (!(bArr3[i8 - 1] == 0 || bArr3[i8] == 0)) {
                bArr3[i8] = (byte) (bArr3[i8] + bArr3[i8 - 1]);
                if (bArr3[i8] == i5) {
                    i6 = i8;
                }
            }
        }
        if (i7 == 0 || i7 > GetLevel(1)) {
            return false;
        }
        if (i6 == 0 || i6 > GetLevel(1)) {
            return false;
        }
        dDZCards.nLen = bArr3[i6];
        dDZCards.nLevel = i6;
        dDZCards.nType = 12;
        return true;
    }

    static boolean Check_Card_Type_CARD_TYPE_CONTINUE_3_TUPLE_PLUS_CONTINUE_SINGLE(DDZCards dDZCards) {
        if (dDZCards.getCurrentLength() % 4 != 0 || dDZCards.getCurrentLength() <= 7) {
            return false;
        }
        byte[] bArr = new byte[16];
        if (dDZCards.getCurrentLength() / 2 != dDZCards.GetTupleInfo(bArr)) {
            System.out.println("nType = CONTINUE_3_TUPLE_PLUS_CONTINUE_SINGLE false");
            return false;
        }
        byte[] bArr2 = new byte[16];
        byte[] bArr3 = new byte[16];
        int i = 0;
        int i2 = 0;
        for (int i3 = 0; i3 < dDZCards.getCurrentLength(); i3++) {
            if (1 == bArr[dDZCards.getValueByIndex(i3)]) {
                i2++;
                bArr2[GetLevel(dDZCards.getLordCardByIndex(i3))] = 1;
            } else if (3 == bArr[dDZCards.getValueByIndex(i3)]) {
                i++;
                bArr3[GetLevel(dDZCards.getLordCardByIndex(i3))] = 1;
            } else {
                System.out.println("nType = CONTINUE_3_TUPLE_PLUS_CONTINUE_SINGLE false2");
                return false;
            }
        }
        if (i2 != i) {
            System.out.println("nType = CONTINUE_3_TUPLE_PLUS_CONTINUE_SINGLE false3");
            return false;
        } else if (i * 4 != dDZCards.getCurrentLength()) {
            System.out.println("nType = CONTINUE_3_TUPLE_PLUS_CONTINUE_SINGLE false4");
            return false;
        } else {
            int i4 = 0;
            int i5 = 0;
            for (int i6 = 1; i6 < 16; i6++) {
                if (!(bArr2[i6 - 1] == 0 || bArr2[i6] == 0)) {
                    bArr2[i6] = (byte) (bArr2[i6] + bArr2[i6 - 1]);
                    if (bArr2[i6] == i2) {
                        i5 = i6;
                    }
                }
                if (!(bArr3[i6 - 1] == 0 || bArr3[i6] == 0)) {
                    bArr3[i6] = (byte) (bArr3[i6] + bArr3[i6 - 1]);
                    if (bArr3[i6] == i) {
                        i4 = i6;
                    }
                }
            }
            if (i5 == 0 || i5 > GetLevel(1)) {
                System.out.println("nType = CONTINUE_3_TUPLE_PLUS_CONTINUE_SINGLE false5");
                return false;
            } else if (i4 == 0 || i4 > GetLevel(1)) {
                System.out.println("nType = CONTINUE_3_TUPLE_PLUS_CONTINUE_SINGLE false6");
                return false;
            } else {
                dDZCards.nLen = bArr3[i4];
                dDZCards.nLevel = i4;
                dDZCards.nType = 11;
                return true;
            }
        }
    }

    static boolean Check_Card_Type_CARD_TYPE_CONTINUE_3_TUPLE_PLUS_SINGLE(DDZCards dDZCards) {
        if (dDZCards.getCurrentLength() % 4 != 0 || dDZCards.getCurrentLength() <= 7) {
            return false;
        }
        byte[] bArr = new byte[16];
        System.out.println("dd");
        dDZCards.GetTupleInfo(bArr);
        byte[] bArr2 = new byte[16];
        int i = 0;
        int i2 = 0;
        for (int i3 = 0; i3 < dDZCards.getCurrentLength(); i3++) {
            if (1 == bArr[dDZCards.getValueByIndex(i3)] || 2 == bArr[dDZCards.getValueByIndex(i3)]) {
                i2++;
            } else if (3 == bArr[dDZCards.getValueByIndex(i3)]) {
                i++;
                bArr2[GetLevel(dDZCards.getLordCardByIndex(i3))] = 1;
            } else {
                System.out.println("nType = MULTI_THREE_ONE false2");
                return false;
            }
        }
        int i4 = i / 3;
        if (i2 != i4) {
            System.out.println("nType = MULTI_THREE_ONE false3");
            return false;
        } else if (i4 * 4 != dDZCards.getCurrentLength()) {
            System.out.println("nType = MULTI_THREE_ONE false4");
            return false;
        } else {
            int i5 = 0;
            for (int i6 = 1; i6 < 16; i6++) {
                if (!(bArr2[i6 - 1] == 0 || bArr2[i6] == 0)) {
                    bArr2[i6] = (byte) (bArr2[i6] + bArr2[i6 - 1]);
                    if (bArr2[i6] == i4) {
                        i5 = i6;
                    }
                }
            }
            if (i5 == 0 || i5 > GetLevel(1)) {
                System.out.println("nType = MULTI_THREE_ONE false5");
                return false;
            }
            dDZCards.nLen = bArr2[i5];
            dDZCards.nLevel = i5;
            dDZCards.nType = 10;
            return true;
        }
    }

    static boolean Check_Card_Type_CARD_TYPE_CONTINUE_3_TUPPLE_PLUS_2_TUPLE(DDZCards dDZCards) {
        if (dDZCards.getCurrentLength() % 5 != 0 || dDZCards.getCurrentLength() <= 9) {
            return false;
        }
        byte[] bArr = new byte[16];
        if ((dDZCards.getCurrentLength() * 2) / 5 != dDZCards.GetTupleInfo(bArr)) {
            return false;
        }
        byte[] bArr2 = new byte[16];
        int i = 0;
        int i2 = 0;
        for (int i3 = 0; i3 < dDZCards.getCurrentLength(); i3++) {
            if (2 == bArr[dDZCards.getValueByIndex(i3)]) {
                i2++;
            } else if (3 != bArr[dDZCards.getValueByIndex(i3)]) {
                return false;
            } else {
                i++;
                bArr2[GetLevel(dDZCards.getLordCardByIndex(i3))] = 1;
            }
        }
        int i4 = i / 3;
        if (i2 / 2 != i4) {
            return false;
        }
        if (i4 * 5 != dDZCards.getCurrentLength()) {
            return false;
        }
        int i5 = 0;
        for (int i6 = 1; i6 < 16; i6++) {
            if (!(bArr2[i6 - 1] == 0 || bArr2[i6] == 0)) {
                bArr2[i6] = (byte) (bArr2[i6] + bArr2[i6 - 1]);
                if (bArr2[i6] == i4) {
                    i5 = i6;
                }
            }
        }
        if (i5 == 0 || i5 > GetLevel(1)) {
            return false;
        }
        dDZCards.nLen = bArr2[i5];
        dDZCards.nLevel = i5;
        dDZCards.nType = 9;
        return true;
    }

    static boolean Check_Card_Type_CARD_TYPE_CONTINUE_SINGLE(DDZCards dDZCards) {
        if (4 >= dDZCards.getCurrentLength()) {
            return false;
        }
        SortByLevel(dDZCards);
        if (GetLevel(dDZCards.getLordCardByIndex(0)) > GetLevel(1)) {
            System.out.println("nType = ONE_TURN false");
            return false;
        }
        for (int i = 1; i < dDZCards.getCurrentLength(); i++) {
            if (GetLevel(dDZCards.getLordCardByIndex(i)) + i != GetLevel(dDZCards.getLordCardByIndex(0))) {
                System.out.println("nType = ONE_TURN false2");
                return false;
            }
        }
        dDZCards.nLen = dDZCards.getCurrentLength();
        dDZCards.nLevel = GetLevel(dDZCards.getLordCardByIndex(0));
        dDZCards.nType = 6;
        System.out.println("nType = ONE_TURN");
        return true;
    }

    static boolean Check_Card_Type_CARD_TYPE_SINGLE(DDZCards dDZCards) {
        if (1 != dDZCards.getCurrentLength()) {
            return false;
        }
        dDZCards.nLen = 1;
        dDZCards.nLevel = GetLevel(dDZCards.getLordCardByIndex(0));
        dDZCards.nType = 1;
        System.out.println("nType = ONE");
        return true;
    }

    public static void ClearFlags() {
        fHasDuizi = true;
        fHasSan = true;
        fHasSi = true;
        fHasBomb = true;
    }

    static boolean Combination(int[] iArr, int i) {
        for (int i2 = 0; i2 < i - 1; i2++) {
            if (iArr[i2] == 1 && iArr[i2 + 1] == 0) {
                iArr[i2] = 0;
                iArr[i2 + 1] = 1;
                int i3 = 0;
                for (int i4 = 0; i4 < i2; i4++) {
                    if (iArr[i4] == 1) {
                        i3++;
                    }
                }
                for (int i5 = 0; i5 < i2; i5++) {
                    if (i5 < i3) {
                        iArr[i5] = 1;
                    } else {
                        iArr[i5] = 0;
                    }
                }
                return true;
            }
        }
        return false;
    }

    static void FirstOut3TuplePlus(DDZCards[] dDZCardsArr, int i, DDZCards[] dDZCardsArr2, int i2, DDZCards dDZCards) {
        DDZCards dDZCards2 = new DDZCards();
        DDZCards dDZCards3 = new DDZCards();
        int i3 = m_nFirstOutCount;
        for (int i4 = 0; i4 < i; i4++) {
            dDZCards2.ReleaseAll();
            dDZCards2.AddCards(dDZCardsArr[i4]);
            for (int i5 = 0; i5 < i2; i5++) {
                dDZCards3.ReleaseAll();
                dDZCards3.AddCards(dDZCards2);
                dDZCards3.AddCards(dDZCardsArr2[i5]);
                if (dDZCards3.Contain(dDZCards)) {
                    if (m_nFirstOutCount < 50) {
                        m_DDZCardsFirstOut[m_nFirstOutCount].ReleaseAll();
                        m_DDZCardsFirstOut[m_nFirstOutCount].AddCards(dDZCards3);
                        m_nFirstOutCount++;
                    } else {
                        return;
                    }
                }
            }
        }
    }

    static int FirstOutContinue(DDZCards[] dDZCardsArr, DDZCards dDZCards, byte[] bArr, int i, int i2) {
        boolean z;
        int i3 = 0;
        while (i < 14) {
            int i4 = i - 2;
            for (int i5 = i; i5 < 14 && i3 < 100; i5++) {
                boolean z2 = true;
                int i6 = 0;
                while (true) {
                    if (i6 >= i4) {
                        break;
                    } else if (bArr[i5 - i6] < i2) {
                        z2 = false;
                        break;
                    } else {
                        i6++;
                    }
                }
                if (z2) {
                    dDZCardsArr[i3].ReleaseAll();
                    for (int i7 = 0; i7 < i4; i7++) {
                        for (int i8 = 0; i8 < i2; i8++) {
                            dDZCardsArr[i3].AddCard(1, i5 - i7, false);
                        }
                    }
                    i3++;
                }
            }
            if (i3 < 100 && bArr[1] > i2 - 1) {
                boolean z3 = true;
                int i9 = 1;
                while (true) {
                    if (i9 >= i4) {
                        break;
                    } else if (bArr[14 - i9] < i2) {
                        z3 = false;
                        break;
                    } else {
                        i9++;
                    }
                }
                if (z3) {
                    dDZCardsArr[i3].ReleaseAll();
                    for (int i10 = 0; i10 < i2; i10++) {
                        dDZCardsArr[i3].AddCard(1, 1, false);
                    }
                    for (int i11 = 0; i11 < i4 - 1; i11++) {
                        for (int i12 = 0; i12 < i2; i12++) {
                            dDZCardsArr[i3].AddCard(1, 13 - i11, false);
                        }
                    }
                    i3++;
                }
            }
            i++;
        }
        if (i3 < 100 && bArr[1] > i2 - 1) {
            int i13 = 1;
            while (true) {
                if (i13 >= 12) {
                    z = true;
                    break;
                } else if (bArr[14 - i13] < i2) {
                    z = false;
                    break;
                } else {
                    i13++;
                }
            }
            if (z) {
                dDZCardsArr[i3].ReleaseAll();
                for (int i14 = 0; i14 < i2; i14++) {
                    dDZCardsArr[i3].AddCard(1, 1, false);
                }
                for (int i15 = 0; i15 < 11; i15++) {
                    for (int i16 = 0; i16 < i2; i16++) {
                        dDZCardsArr[i3].AddCard(1, 13 - i15, false);
                    }
                }
                return i3 + 1;
            }
        }
        return i3;
    }

    public static void FirstOutSelectAI(Vector vector, Vector vector2, int i) {
        boolean z;
        if (vector2 != null && i > 0) {
            m_nAIMainCount = 0;
            m_nAISubCount = 0;
            m_nFirstOutCount = 0;
            DDZCards dDZCards = new DDZCards();
            GetCardsFromPokers(vector2, i, dDZCards);
            int size = vector.size();
            DDZCards dDZCards2 = new DDZCards();
            GetCardsFromPokers(vector, vector.size(), dDZCards2);
            DDZCards dDZCards3 = new DDZCards();
            dDZCards3.nLen = 0;
            dDZCards3.nLevel = 0;
            if (size > 1) {
                int i2 = 0;
                while (true) {
                    if (i2 >= size - 1) {
                        z = true;
                        break;
                    } else if (dDZCards2.getValueByIndex(i2) != dDZCards2.getValueByIndex(i2 + 1)) {
                        z = false;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (!z) {
                    DDZCards[] dDZCardsArr = new DDZCards[100];
                    for (int i3 = 0; i3 < dDZCardsArr.length; i3++) {
                        dDZCardsArr[i3] = new DDZCards();
                    }
                    byte[] bArr = new byte[16];
                    dDZCards.GetTupleInfo(bArr);
                    int FirstOutContinue = FirstOutContinue(dDZCardsArr, dDZCards2, bArr, 7, 1);
                    for (int i4 = 0; i4 < FirstOutContinue; i4++) {
                        if (dDZCardsArr[i4].Contain(dDZCards2)) {
                            if (m_nFirstOutCount < 50) {
                                m_DDZCardsFirstOut[m_nFirstOutCount].ReleaseAll();
                                m_DDZCardsFirstOut[m_nFirstOutCount].AddCards(dDZCardsArr[i4]);
                                m_nFirstOutCount++;
                            } else {
                                return;
                            }
                        }
                    }
                    int FirstOutContinue2 = FirstOutContinue(dDZCardsArr, dDZCards2, bArr, 5, 2);
                    for (int i5 = 0; i5 < FirstOutContinue2; i5++) {
                        if (dDZCardsArr[i5].Contain(dDZCards2)) {
                            if (m_nFirstOutCount < 50) {
                                m_DDZCardsFirstOut[m_nFirstOutCount].ReleaseAll();
                                m_DDZCardsFirstOut[m_nFirstOutCount].AddCards(dDZCardsArr[i5]);
                                m_nFirstOutCount++;
                            } else {
                                return;
                            }
                        }
                    }
                    int FirstOutContinue3 = FirstOutContinue(dDZCardsArr, dDZCards2, bArr, 4, 3);
                    for (int i6 = 0; i6 < FirstOutContinue3; i6++) {
                        if (dDZCardsArr[i6].Contain(dDZCards2)) {
                            if (m_nFirstOutCount < 50) {
                                m_DDZCardsFirstOut[m_nFirstOutCount].ReleaseAll();
                                m_DDZCardsFirstOut[m_nFirstOutCount].AddCards(dDZCardsArr[i6]);
                                m_nFirstOutCount++;
                            } else {
                                return;
                            }
                        }
                    }
                    if (fHasSan) {
                        m_nAIMainCount = Search3TuplePlusSingle(dDZCards, dDZCards3, m_DDZCardsAIMain, bArr, false);
                        SearchPlusSingle(dDZCards);
                        FirstOut3TuplePlus(m_DDZCardsAIMain, m_nAIMainCount, m_DDZCardsAISub, m_nAISubCount, dDZCards2);
                        m_nAIMainCount = Search3TuplePlus2Tuple(dDZCards, dDZCards3, m_DDZCardsAIMain, bArr, false);
                        SearchPlus2Tuple(dDZCards);
                        FirstOut3TuplePlus(m_DDZCardsAIMain, m_nAIMainCount, m_DDZCardsAISub, m_nAISubCount, dDZCards2);
                        m_nAIMainCount = FirstOutContinue(m_DDZCardsAIMain, dDZCards2, bArr, 4, 3);
                        SearchPlusSingle(dDZCards);
                        FirstOutSubDDZCards(m_DDZCardsAIMain, m_nAIMainCount, m_DDZCardsAISub, m_nAISubCount, dDZCards2);
                        m_nAIMainCount = FirstOutContinue(m_DDZCardsAIMain, dDZCards2, bArr, 4, 3);
                        SearchPlus2Tuple(dDZCards);
                        FirstOutSubDDZCards(m_DDZCardsAIMain, m_nAIMainCount, m_DDZCardsAISub, m_nAISubCount, dDZCards2);
                    }
                    if (fHasSi) {
                        m_nAIMainCount = Search4TuplePlusSingle(dDZCards, dDZCards3, m_DDZCardsAIMain, bArr, false);
                        SearchPlusSingle(dDZCards);
                        FirstOutSubDDZCards(m_DDZCardsAIMain, m_nAIMainCount, m_DDZCardsAISub, m_nAISubCount, dDZCards2);
                        m_nAIMainCount = Search4TuplePlus2Tuple(dDZCards, dDZCards3, m_DDZCardsAIMain, bArr, false);
                        SearchPlus2Tuple(dDZCards);
                        FirstOutSubDDZCards(m_DDZCardsAIMain, m_nAIMainCount, m_DDZCardsAISub, m_nAISubCount, dDZCards2);
                    }
                }
            }
        }
    }

    static void FirstOutSubDDZCards(DDZCards[] dDZCardsArr, int i, DDZCards[] dDZCardsArr2, int i2, DDZCards dDZCards) {
        DDZCards dDZCards2 = new DDZCards();
        int[] iArr = new int[i2];
        for (int i3 = 0; i3 < i; i3++) {
            int currentLength = dDZCardsArr[i3].getCurrentLength() == 4 ? 2 : dDZCardsArr[i3].getCurrentLength() / 3;
            dDZCards2.ReleaseAll();
            dDZCards2.AddCards(dDZCardsArr[i3]);
            for (int i4 = 0; i4 < i2; i4++) {
                if (i4 < currentLength) {
                    iArr[i4] = 1;
                } else {
                    iArr[i4] = 0;
                }
            }
            GetCombinationDDZCards(dDZCards2, dDZCardsArr2, dDZCards, iArr, i2);
            while (Combination(iArr, i2)) {
                GetCombinationDDZCards(dDZCards2, dDZCardsArr2, dDZCards, iArr, i2);
            }
        }
    }

    static void GetCardsFromPokers(Vector vector, int i, DDZCards dDZCards) {
        for (int i2 = 0; i2 < i; i2++) {
            dDZCards.AddCard((LordCard) vector.elementAt(i2));
        }
    }

    static void GetCombinationDDZCards(DDZCards dDZCards, DDZCards[] dDZCardsArr, DDZCards dDZCards2, int[] iArr, int i) {
        boolean z;
        boolean z2;
        DDZCards dDZCards3 = new DDZCards();
        dDZCards3.ReleaseAll();
        dDZCards3.AddCards(dDZCards);
        int i2 = 0;
        while (true) {
            if (i2 >= i) {
                z = false;
                break;
            }
            if (iArr[i2] == 1) {
                if (dDZCards.Contain(dDZCardsArr[i2])) {
                    z = true;
                    break;
                }
                dDZCards3.AddCards(dDZCardsArr[i2]);
            }
            i2++;
        }
        if (!z && dDZCards3.Contain(dDZCards2) && m_nFirstOutCount < 50) {
            int i3 = 0;
            while (true) {
                if (i3 >= m_nFirstOutCount) {
                    z2 = false;
                    break;
                } else if (m_DDZCardsFirstOut[i3].Contain(dDZCards3, true)) {
                    z2 = true;
                    break;
                } else {
                    i3++;
                }
            }
            if (!z2) {
                m_DDZCardsFirstOut[m_nFirstOutCount].ReleaseAll();
                m_DDZCardsFirstOut[m_nFirstOutCount].AddCards(dDZCards3);
                m_nFirstOutCount++;
            }
        }
    }

    static int GetLevel(int i) {
        if (i >= 16 || i <= 0) {
            return 0;
        }
        switch (i) {
            case 1:
            case 2:
                return i + 11;
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
                return i - 2;
            case 14:
            case 15:
                return i;
            default:
                return 0;
        }
    }

    static int GetLevel(LordCard lordCard) {
        return GetLevel(lordCard.value);
    }

    static boolean IsFirstCardsBig(DDZCards dDZCards, DDZCards dDZCards2) {
        return dDZCards.nType == dDZCards2.nType ? dDZCards.nLen == dDZCards2.nLen && dDZCards.nLevel > dDZCards2.nLevel : dDZCards.nType >= 15 && dDZCards.nType > dDZCards2.nType;
    }

    static int Search2Tuple(DDZCards dDZCards, DDZCards dDZCards2, DDZCards[] dDZCardsArr, byte[] bArr, boolean z) {
        DDZCards dDZCards3 = new DDZCards();
        int i = 0;
        boolean z2 = false;
        for (int i2 = 2; i2 < 9; i2++) {
            for (int i3 = 3; i3 < 14; i3++) {
                if (bArr[i3] == i2) {
                    dDZCards3.ReleaseAll();
                    dDZCards3.AddCard(1, i3, false);
                    dDZCards3.AddCard(1, i3, false);
                    SetCardsType(dDZCards3);
                    if ((dDZCards2.getCurrentLength() == 0 || IsFirstCardsBig(dDZCards3, dDZCards2)) && i < 30) {
                        dDZCardsArr[i].ReleaseAll();
                        dDZCardsArr[i].AddCards(dDZCards3);
                        i++;
                        z2 = true;
                    } else {
                        z2 = true;
                    }
                }
            }
            for (int i4 = 1; i4 < 3; i4++) {
                if (bArr[i4] == i2) {
                    dDZCards3.ReleaseAll();
                    dDZCards3.AddCard(1, i4, false);
                    dDZCards3.AddCard(1, i4, false);
                    SetCardsType(dDZCards3);
                    if ((dDZCards2.getCurrentLength() == 0 || IsFirstCardsBig(dDZCards3, dDZCards2)) && i < 30) {
                        dDZCardsArr[i].ReleaseAll();
                        dDZCardsArr[i].AddCards(dDZCards3);
                        i++;
                        z2 = true;
                    } else {
                        z2 = true;
                    }
                }
            }
        }
        if (!z2) {
            fHasDuizi = false;
        }
        return i;
    }

    static int Search3Tuple(DDZCards dDZCards, DDZCards dDZCards2, DDZCards[] dDZCardsArr, byte[] bArr, boolean z) {
        DDZCards dDZCards3 = new DDZCards();
        boolean z2 = false;
        int i = 0;
        for (int i2 = 3; i2 < 9; i2++) {
            for (int i3 = 3; i3 < 14; i3++) {
                if (bArr[i3] == i2) {
                    dDZCards3.ReleaseAll();
                    dDZCards3.AddCard(1, i3, false);
                    dDZCards3.AddCard(1, i3, false);
                    dDZCards3.AddCard(1, i3, false);
                    SetCardsType(dDZCards3);
                    if ((dDZCards2.getCurrentLength() == 0 || IsFirstCardsBig(dDZCards3, dDZCards2)) && i < 30) {
                        dDZCardsArr[i].ReleaseAll();
                        dDZCardsArr[i].AddCards(dDZCards3);
                        i++;
                        z2 = true;
                    } else {
                        z2 = true;
                    }
                }
            }
            for (int i4 = 1; i4 < 3; i4++) {
                if (bArr[i4] == i2) {
                    dDZCards3.ReleaseAll();
                    dDZCards3.AddCard(1, i4, false);
                    dDZCards3.AddCard(1, i4, false);
                    dDZCards3.AddCard(1, i4, false);
                    SetCardsType(dDZCards3);
                    if ((dDZCards2.getCurrentLength() == 0 || IsFirstCardsBig(dDZCards3, dDZCards2)) && i < 30) {
                        dDZCardsArr[i].ReleaseAll();
                        dDZCardsArr[i].AddCards(dDZCards3);
                        i++;
                        z2 = true;
                    } else {
                        z2 = true;
                    }
                }
            }
        }
        if (!z2) {
            fHasSan = false;
        }
        return i;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v9, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v10, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v12, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v13, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v16, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v17, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v20, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v39, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v40, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v33, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v34, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v37, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v38, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v41, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v62, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v63, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static int Search3TuplePlus2Tuple(com.tencent.qqgame.common.DDZCards r9, com.tencent.qqgame.common.DDZCards r10, com.tencent.qqgame.common.DDZCards[] r11, byte[] r12, boolean r13) {
        /*
            r9 = 0
            com.tencent.qqgame.common.DDZCards r0 = new com.tencent.qqgame.common.DDZCards
            r0.<init>()
            com.tencent.qqgame.common.DDZCards r1 = new com.tencent.qqgame.common.DDZCards
            r1.<init>()
            r2 = 0
            r3 = 3
            r8 = r2
            r2 = r9
            r9 = r8
        L_0x0010:
            r4 = 9
            if (r3 >= r4) goto L_0x016d
            r4 = 3
        L_0x0015:
            r5 = 14
            if (r4 >= r5) goto L_0x00bf
            byte r5 = r12[r4]
            if (r5 != r3) goto L_0x00b1
            r9 = 1
            r0.ReleaseAll()
            r1.ReleaseAll()
            r5 = 1
            r6 = 0
            r0.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r0.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r0.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r1.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r1.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r1.AddCard(r5, r4, r6)
            r5 = 2
        L_0x0043:
            r6 = 9
            if (r5 >= r6) goto L_0x0089
            r6 = 3
            r8 = r6
            r6 = r5
            r5 = r8
        L_0x004b:
            r7 = 14
            if (r5 >= r7) goto L_0x0066
            byte r7 = r12[r5]
            if (r7 != r6) goto L_0x0063
            if (r5 == r4) goto L_0x0063
            r6 = 1
            r7 = 0
            r0.AddCard(r6, r5, r7)
            r6 = 1
            r7 = 0
            r0.AddCard(r6, r5, r7)
            r5 = 100
            r6 = 100
        L_0x0063:
            int r5 = r5 + 1
            goto L_0x004b
        L_0x0066:
            r5 = 100
            if (r6 == r5) goto L_0x0085
            r5 = 1
        L_0x006b:
            r7 = 3
            if (r5 >= r7) goto L_0x0085
            byte r7 = r12[r5]
            if (r7 != r6) goto L_0x0082
            if (r5 == r4) goto L_0x0082
            r6 = 1
            r7 = 0
            r0.AddCard(r6, r5, r7)
            r6 = 1
            r7 = 0
            r0.AddCard(r6, r5, r7)
            r5 = 100
            r6 = 100
        L_0x0082:
            int r5 = r5 + 1
            goto L_0x006b
        L_0x0085:
            r5 = r6
            int r5 = r5 + 1
            goto L_0x0043
        L_0x0089:
            int r5 = r0.getCurrentLength()
            r6 = 5
            if (r5 != r6) goto L_0x00bb
            SetCardsType(r0)
            int r5 = r10.getCurrentLength()
            if (r5 == 0) goto L_0x009f
            boolean r5 = IsFirstCardsBig(r0, r10)
            if (r5 == 0) goto L_0x00b1
        L_0x009f:
            r5 = 30
            if (r2 >= r5) goto L_0x00b1
            r5 = r11[r2]
            r5.ReleaseAll()
            if (r13 == 0) goto L_0x00b5
            r5 = r11[r2]
            r5.AddCards(r0)
        L_0x00af:
            int r2 = r2 + 1
        L_0x00b1:
            int r4 = r4 + 1
            goto L_0x0015
        L_0x00b5:
            r5 = r11[r2]
            r5.AddCards(r1)
            goto L_0x00af
        L_0x00bb:
            r5 = 0
            com.tencent.qqgame.common.SearchHint.fHasDuizi = r5
            goto L_0x00b1
        L_0x00bf:
            r4 = 1
        L_0x00c0:
            r5 = 3
            if (r4 >= r5) goto L_0x0169
            byte r5 = r12[r4]
            if (r5 != r3) goto L_0x015b
            r9 = 1
            r0.ReleaseAll()
            r1.ReleaseAll()
            r5 = 1
            r6 = 0
            r0.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r0.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r0.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r1.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r1.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r1.AddCard(r5, r4, r6)
            r5 = 2
        L_0x00ed:
            r6 = 9
            if (r5 >= r6) goto L_0x0133
            r6 = 3
            r8 = r6
            r6 = r5
            r5 = r8
        L_0x00f5:
            r7 = 14
            if (r5 >= r7) goto L_0x0110
            byte r7 = r12[r5]
            if (r7 != r6) goto L_0x010d
            if (r5 == r4) goto L_0x010d
            r6 = 1
            r7 = 0
            r0.AddCard(r6, r5, r7)
            r6 = 1
            r7 = 0
            r0.AddCard(r6, r5, r7)
            r5 = 100
            r6 = 100
        L_0x010d:
            int r5 = r5 + 1
            goto L_0x00f5
        L_0x0110:
            r5 = 100
            if (r6 == r5) goto L_0x012f
            r5 = 1
        L_0x0115:
            r7 = 3
            if (r5 >= r7) goto L_0x012f
            byte r7 = r12[r5]
            if (r7 != r6) goto L_0x012c
            if (r5 == r4) goto L_0x012c
            r6 = 1
            r7 = 0
            r0.AddCard(r6, r5, r7)
            r6 = 1
            r7 = 0
            r0.AddCard(r6, r5, r7)
            r5 = 100
            r6 = 100
        L_0x012c:
            int r5 = r5 + 1
            goto L_0x0115
        L_0x012f:
            r5 = r6
            int r5 = r5 + 1
            goto L_0x00ed
        L_0x0133:
            int r5 = r0.getCurrentLength()
            r6 = 5
            if (r5 != r6) goto L_0x0165
            SetCardsType(r0)
            int r5 = r10.getCurrentLength()
            if (r5 == 0) goto L_0x0149
            boolean r5 = IsFirstCardsBig(r0, r10)
            if (r5 == 0) goto L_0x015b
        L_0x0149:
            r5 = 30
            if (r2 >= r5) goto L_0x015b
            r5 = r11[r2]
            r5.ReleaseAll()
            if (r13 == 0) goto L_0x015f
            r5 = r11[r2]
            r5.AddCards(r0)
        L_0x0159:
            int r2 = r2 + 1
        L_0x015b:
            int r4 = r4 + 1
            goto L_0x00c0
        L_0x015f:
            r5 = r11[r2]
            r5.AddCards(r1)
            goto L_0x0159
        L_0x0165:
            r5 = 0
            com.tencent.qqgame.common.SearchHint.fHasDuizi = r5
            goto L_0x015b
        L_0x0169:
            int r3 = r3 + 1
            goto L_0x0010
        L_0x016d:
            if (r9 != 0) goto L_0x0172
            r9 = 0
            com.tencent.qqgame.common.SearchHint.fHasSan = r9
        L_0x0172:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.common.SearchHint.Search3TuplePlus2Tuple(com.tencent.qqgame.common.DDZCards, com.tencent.qqgame.common.DDZCards, com.tencent.qqgame.common.DDZCards[], byte[], boolean):int");
    }

    static int Search3TuplePlusSingle(DDZCards dDZCards, DDZCards dDZCards2, DDZCards[] dDZCardsArr, byte[] bArr, boolean z) {
        DDZCards dDZCards3 = new DDZCards();
        DDZCards dDZCards4 = new DDZCards();
        int i = 0;
        boolean z2 = false;
        for (int i2 = 3; i2 < 9; i2++) {
            for (int i3 = 3; i3 < 14; i3++) {
                if (bArr[i3] == i2) {
                    z2 = true;
                    dDZCards3.ReleaseAll();
                    dDZCards4.ReleaseAll();
                    dDZCards3.AddCard(1, i3, false);
                    dDZCards3.AddCard(1, i3, false);
                    dDZCards3.AddCard(1, i3, false);
                    dDZCards4.AddCard(1, i3, false);
                    dDZCards4.AddCard(1, i3, false);
                    dDZCards4.AddCard(1, i3, false);
                    SortByTuple(dDZCards);
                    int currentLength = dDZCards.getCurrentLength() - 1;
                    while (true) {
                        if (currentLength < 0) {
                            break;
                        } else if (dDZCards.getValueByIndex(currentLength) != i3) {
                            dDZCards3.AddCard(dDZCards.getLordCardByIndex(currentLength));
                            break;
                        } else {
                            currentLength--;
                        }
                    }
                    if (dDZCards3.getCurrentLength() == 4) {
                        SetCardsType(dDZCards3);
                        if ((dDZCards2.getCurrentLength() == 0 || IsFirstCardsBig(dDZCards3, dDZCards2)) && i < 30) {
                            dDZCardsArr[i].ReleaseAll();
                            if (z) {
                                dDZCardsArr[i].AddCards(dDZCards3);
                            } else {
                                dDZCardsArr[i].AddCards(dDZCards4);
                            }
                            i++;
                        }
                    }
                }
            }
            for (int i4 = 1; i4 < 3; i4++) {
                if (bArr[i4] == i2) {
                    z2 = true;
                    dDZCards3.ReleaseAll();
                    dDZCards4.ReleaseAll();
                    dDZCards3.AddCard(1, i4, false);
                    dDZCards3.AddCard(1, i4, false);
                    dDZCards3.AddCard(1, i4, false);
                    dDZCards4.AddCard(1, i4, false);
                    dDZCards4.AddCard(1, i4, false);
                    dDZCards4.AddCard(1, i4, false);
                    SortByTuple(dDZCards);
                    int currentLength2 = dDZCards.getCurrentLength() - 1;
                    while (true) {
                        if (currentLength2 < 0) {
                            break;
                        } else if (dDZCards.getValueByIndex(currentLength2) != i4) {
                            dDZCards3.AddCard(dDZCards.getLordCardByIndex(currentLength2));
                            break;
                        } else {
                            currentLength2--;
                        }
                    }
                    if (dDZCards3.getCurrentLength() == 4) {
                        SetCardsType(dDZCards3);
                        if ((dDZCards2.getCurrentLength() == 0 || IsFirstCardsBig(dDZCards3, dDZCards2)) && i < 30) {
                            dDZCardsArr[i].ReleaseAll();
                            if (z) {
                                dDZCardsArr[i].AddCards(dDZCards3);
                            } else {
                                dDZCardsArr[i].AddCards(dDZCards4);
                            }
                            i++;
                        }
                    }
                }
            }
        }
        if (!z2) {
            fHasSan = false;
        }
        return i;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v11, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v12, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v13, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v14, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v14, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v17, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v21, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v24, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v43, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v44, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v45, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v46, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v40, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v41, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v44, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v45, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v48, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v53, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v54, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v57, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v58, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v61, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v80, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v81, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v82, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v83, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static int Search4TuplePlus2Tuple(com.tencent.qqgame.common.DDZCards r10, com.tencent.qqgame.common.DDZCards r11, com.tencent.qqgame.common.DDZCards[] r12, byte[] r13, boolean r14) {
        /*
            r10 = 0
            com.tencent.qqgame.common.DDZCards r0 = new com.tencent.qqgame.common.DDZCards
            r0.<init>()
            com.tencent.qqgame.common.DDZCards r1 = new com.tencent.qqgame.common.DDZCards
            r1.<init>()
            r2 = 0
            r3 = 4
            r9 = r2
            r2 = r10
            r10 = r9
        L_0x0010:
            r4 = 9
            if (r3 >= r4) goto L_0x01ee
            r4 = 3
        L_0x0015:
            r5 = 14
            if (r4 >= r5) goto L_0x0126
            byte r5 = r13[r4]
            if (r5 != r3) goto L_0x011c
            r10 = 1
            r0.ReleaseAll()
            r1.ReleaseAll()
            r5 = 1
            r6 = 0
            r0.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r0.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r0.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r0.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r1.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r1.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r1.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r1.AddCard(r5, r4, r6)
            r5 = 2
        L_0x004d:
            r6 = 9
            if (r5 >= r6) goto L_0x0093
            r6 = 3
            r9 = r6
            r6 = r5
            r5 = r9
        L_0x0055:
            r7 = 14
            if (r5 >= r7) goto L_0x0070
            byte r7 = r13[r5]
            if (r7 != r6) goto L_0x006d
            if (r5 == r4) goto L_0x006d
            r6 = 1
            r7 = 0
            r0.AddCard(r6, r5, r7)
            r6 = 1
            r7 = 0
            r0.AddCard(r6, r5, r7)
            r5 = 100
            r6 = 100
        L_0x006d:
            int r5 = r5 + 1
            goto L_0x0055
        L_0x0070:
            r5 = 100
            if (r6 == r5) goto L_0x008f
            r5 = 1
        L_0x0075:
            r7 = 3
            if (r5 >= r7) goto L_0x008f
            byte r7 = r13[r5]
            if (r7 != r6) goto L_0x008c
            if (r5 == r4) goto L_0x008c
            r6 = 1
            r7 = 0
            r0.AddCard(r6, r5, r7)
            r6 = 1
            r7 = 0
            r0.AddCard(r6, r5, r7)
            r5 = 100
            r6 = 100
        L_0x008c:
            int r5 = r5 + 1
            goto L_0x0075
        L_0x008f:
            r5 = r6
            int r5 = r5 + 1
            goto L_0x004d
        L_0x0093:
            r5 = 2
        L_0x0094:
            r6 = 9
            if (r5 >= r6) goto L_0x00f3
            r6 = 1
            r9 = r6
            r6 = r5
            r5 = r9
        L_0x009c:
            r7 = 14
            if (r5 >= r7) goto L_0x00c3
            byte r7 = r13[r5]
            if (r7 != r6) goto L_0x00c0
            if (r5 == r4) goto L_0x00c0
            int r7 = r0.getCurrentLength()
            r8 = 1
            int r7 = r7 - r8
            int r7 = r0.getValueByIndex(r7)
            if (r5 == r7) goto L_0x00c0
            r6 = 1
            r7 = 0
            r0.AddCard(r6, r5, r7)
            r6 = 1
            r7 = 0
            r0.AddCard(r6, r5, r7)
            r5 = 100
            r6 = 100
        L_0x00c0:
            int r5 = r5 + 1
            goto L_0x009c
        L_0x00c3:
            r5 = 100
            if (r6 == r5) goto L_0x00ef
            r5 = 1
        L_0x00c8:
            r7 = 14
            if (r5 >= r7) goto L_0x00ef
            byte r7 = r13[r5]
            if (r7 != r6) goto L_0x00ec
            if (r5 == r4) goto L_0x00ec
            int r7 = r0.getCurrentLength()
            r8 = 1
            int r7 = r7 - r8
            int r7 = r0.getValueByIndex(r7)
            if (r5 == r7) goto L_0x00ec
            r6 = 1
            r7 = 0
            r0.AddCard(r6, r5, r7)
            r6 = 1
            r7 = 0
            r0.AddCard(r6, r5, r7)
            r5 = 100
            r6 = 100
        L_0x00ec:
            int r5 = r5 + 1
            goto L_0x00c8
        L_0x00ef:
            r5 = r6
            int r5 = r5 + 1
            goto L_0x0094
        L_0x00f3:
            int r5 = r0.getCurrentLength()
            r6 = 8
            if (r5 != r6) goto L_0x011c
            SetCardsType(r0)
            int r5 = r11.getCurrentLength()
            if (r5 == 0) goto L_0x010a
            boolean r5 = IsFirstCardsBig(r0, r11)
            if (r5 == 0) goto L_0x011c
        L_0x010a:
            r5 = 30
            if (r2 >= r5) goto L_0x011c
            r5 = r12[r2]
            r5.ReleaseAll()
            if (r14 == 0) goto L_0x0120
            r5 = r12[r2]
            r5.AddCards(r0)
        L_0x011a:
            int r2 = r2 + 1
        L_0x011c:
            int r4 = r4 + 1
            goto L_0x0015
        L_0x0120:
            r5 = r12[r2]
            r5.AddCards(r1)
            goto L_0x011a
        L_0x0126:
            r4 = 1
        L_0x0127:
            r5 = 3
            if (r4 >= r5) goto L_0x01ea
            byte r5 = r13[r4]
            if (r5 != r3) goto L_0x01e0
            r10 = 1
            r0.ReleaseAll()
            r1.ReleaseAll()
            r5 = 1
            r6 = 0
            r0.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r0.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r0.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r0.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r1.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r1.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r1.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r1.AddCard(r5, r4, r6)
            r5 = 2
        L_0x015e:
            r6 = 9
            if (r5 >= r6) goto L_0x0184
            r6 = 1
            r9 = r6
            r6 = r5
            r5 = r9
        L_0x0166:
            r7 = 14
            if (r5 >= r7) goto L_0x0181
            byte r7 = r13[r5]
            if (r7 != r6) goto L_0x017e
            if (r5 == r4) goto L_0x017e
            r6 = 1
            r7 = 0
            r0.AddCard(r6, r5, r7)
            r6 = 1
            r7 = 0
            r0.AddCard(r6, r5, r7)
            r5 = 100
            r6 = 100
        L_0x017e:
            int r5 = r5 + 1
            goto L_0x0166
        L_0x0181:
            int r5 = r6 + 1
            goto L_0x015e
        L_0x0184:
            r5 = 2
        L_0x0185:
            r6 = 9
            if (r5 >= r6) goto L_0x01b7
            r6 = 1
            r9 = r6
            r6 = r5
            r5 = r9
        L_0x018d:
            r7 = 14
            if (r5 >= r7) goto L_0x01b4
            byte r7 = r13[r5]
            if (r7 != r6) goto L_0x01b1
            if (r5 == r4) goto L_0x01b1
            int r7 = r0.getCurrentLength()
            r8 = 1
            int r7 = r7 - r8
            int r7 = r0.getValueByIndex(r7)
            if (r5 == r7) goto L_0x01b1
            r6 = 1
            r7 = 0
            r0.AddCard(r6, r5, r7)
            r6 = 1
            r7 = 0
            r0.AddCard(r6, r5, r7)
            r5 = 100
            r6 = 100
        L_0x01b1:
            int r5 = r5 + 1
            goto L_0x018d
        L_0x01b4:
            int r5 = r6 + 1
            goto L_0x0185
        L_0x01b7:
            int r5 = r0.getCurrentLength()
            r6 = 8
            if (r5 != r6) goto L_0x01e0
            SetCardsType(r0)
            int r5 = r11.getCurrentLength()
            if (r5 == 0) goto L_0x01ce
            boolean r5 = IsFirstCardsBig(r0, r11)
            if (r5 == 0) goto L_0x01e0
        L_0x01ce:
            r5 = 30
            if (r2 >= r5) goto L_0x01e0
            r5 = r12[r2]
            r5.ReleaseAll()
            if (r14 == 0) goto L_0x01e4
            r5 = r12[r2]
            r5.AddCards(r0)
        L_0x01de:
            int r2 = r2 + 1
        L_0x01e0:
            int r4 = r4 + 1
            goto L_0x0127
        L_0x01e4:
            r5 = r12[r2]
            r5.AddCards(r1)
            goto L_0x01de
        L_0x01ea:
            int r3 = r3 + 1
            goto L_0x0010
        L_0x01ee:
            if (r10 != 0) goto L_0x01f3
            r10 = 0
            com.tencent.qqgame.common.SearchHint.fHasSi = r10
        L_0x01f3:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.common.SearchHint.Search4TuplePlus2Tuple(com.tencent.qqgame.common.DDZCards, com.tencent.qqgame.common.DDZCards, com.tencent.qqgame.common.DDZCards[], byte[], boolean):int");
    }

    static int Search4TuplePlusSingle(DDZCards dDZCards, DDZCards dDZCards2, DDZCards[] dDZCardsArr, byte[] bArr, boolean z) {
        DDZCards dDZCards3 = new DDZCards();
        DDZCards dDZCards4 = new DDZCards();
        DDZCards dDZCards5 = new DDZCards();
        int i = 0;
        boolean z2 = false;
        for (int i2 = 4; i2 < 9; i2++) {
            for (int i3 = 3; i3 < 14; i3++) {
                if (bArr[i3] == i2) {
                    z2 = true;
                    dDZCards3.ReleaseAll();
                    dDZCards4.ReleaseAll();
                    dDZCards3.AddCard(1, i3, false);
                    dDZCards3.AddCard(1, i3, false);
                    dDZCards3.AddCard(1, i3, false);
                    dDZCards3.AddCard(1, i3, false);
                    dDZCards4.AddCard(1, i3, false);
                    dDZCards4.AddCard(1, i3, false);
                    dDZCards4.AddCard(1, i3, false);
                    dDZCards4.AddCard(1, i3, false);
                    SortByTuple(dDZCards);
                    dDZCards5.ReleaseAll();
                    int currentLength = dDZCards.getCurrentLength() - 1;
                    while (true) {
                        if (currentLength < 0) {
                            break;
                        } else if (dDZCards.getValueByIndex(currentLength) != i3) {
                            dDZCards5.AddCard(dDZCards.getLordCardByIndex(currentLength));
                            currentLength--;
                            break;
                        } else {
                            currentLength--;
                        }
                    }
                    while (true) {
                        if (currentLength < 0) {
                            break;
                        } else if (dDZCards.getValueByIndex(currentLength) != i3) {
                            dDZCards5.AddCard(dDZCards.getLordCardByIndex(currentLength));
                            break;
                        } else {
                            currentLength--;
                        }
                    }
                    dDZCards3.AddCards(dDZCards5);
                    if (dDZCards3.getCurrentLength() == 6) {
                        SetCardsType(dDZCards3);
                        if ((dDZCards2.getCurrentLength() == 0 || IsFirstCardsBig(dDZCards3, dDZCards2)) && i < 30) {
                            dDZCardsArr[i].ReleaseAll();
                            if (z) {
                                dDZCardsArr[i].AddCards(dDZCards3);
                            } else {
                                dDZCardsArr[i].AddCards(dDZCards4);
                            }
                            i++;
                        }
                    }
                }
            }
            for (int i4 = 1; i4 < 3; i4++) {
                if (bArr[i4] == i2) {
                    z2 = true;
                    dDZCards3.ReleaseAll();
                    dDZCards4.ReleaseAll();
                    dDZCards3.AddCard(1, i4, false);
                    dDZCards3.AddCard(1, i4, false);
                    dDZCards3.AddCard(1, i4, false);
                    dDZCards3.AddCard(1, i4, false);
                    dDZCards4.AddCard(1, i4, false);
                    dDZCards4.AddCard(1, i4, false);
                    dDZCards4.AddCard(1, i4, false);
                    dDZCards4.AddCard(1, i4, false);
                    dDZCards5.ReleaseAll();
                    int currentLength2 = dDZCards.getCurrentLength() - 1;
                    while (true) {
                        if (currentLength2 < 0) {
                            break;
                        } else if (dDZCards.getValueByIndex(currentLength2) != i4) {
                            dDZCards5.AddCard(dDZCards.getLordCardByIndex(currentLength2));
                            currentLength2--;
                            break;
                        } else {
                            currentLength2--;
                        }
                    }
                    while (true) {
                        if (currentLength2 < 0) {
                            break;
                        } else if (dDZCards.getValueByIndex(currentLength2) != i4) {
                            dDZCards5.AddCard(dDZCards.getLordCardByIndex(currentLength2));
                            break;
                        } else {
                            currentLength2--;
                        }
                    }
                    dDZCards3.AddCards(dDZCards5);
                    if (dDZCards3.getCurrentLength() == 6) {
                        SetCardsType(dDZCards3);
                        if ((dDZCards2.getCurrentLength() == 0 || IsFirstCardsBig(dDZCards3, dDZCards2)) && i < 30) {
                            dDZCardsArr[i].ReleaseAll();
                            if (z) {
                                dDZCardsArr[i].AddCards(dDZCards3);
                            } else {
                                dDZCardsArr[i].AddCards(dDZCards4);
                            }
                            i++;
                        }
                    }
                }
            }
        }
        if (!z2) {
            fHasSi = false;
        }
        return i;
    }

    /* JADX WARNING: Removed duplicated region for block: B:59:0x01f3  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0228  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static int SearchBomb(com.tencent.qqgame.common.DDZCards r12, com.tencent.qqgame.common.DDZCards r13, com.tencent.qqgame.common.DDZCards[] r14, int r15) {
        /*
            r11 = 2
            r10 = 14
            r9 = 30
            r8 = 1
            r7 = 0
            java.io.PrintStream r0 = java.lang.System.out
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = ">>> SearchBomb,nCount="
            java.lang.StringBuilder r1 = r1.append(r2)
            java.lang.StringBuilder r1 = r1.append(r15)
            java.lang.String r1 = r1.toString()
            r0.println(r1)
            r0 = 16
            byte[] r0 = new byte[r0]
            r13.GetTupleInfo(r0)
            com.tencent.qqgame.common.DDZCards r1 = new com.tencent.qqgame.common.DDZCards
            r1.<init>()
            r2 = r7
            r3 = r7
            r4 = r15
        L_0x002e:
            r5 = 16
            if (r2 >= r5) goto L_0x014f
            if (r4 >= r9) goto L_0x0068
            byte r5 = r0[r2]
            r6 = 4
            if (r5 < r6) goto L_0x0068
            r1.ReleaseAll()
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            SetCardsType(r1)
            boolean r3 = IsFirstCardsBig(r1, r12)
            if (r3 == 0) goto L_0x022f
            r3 = r14[r4]
            r3.ReleaseAll()
            r3 = r14[r4]
            r3.AddCards(r1)
            r3 = r14[r4]
            int r5 = r1.nLevel
            r3.nLevel = r5
            r1.ReleaseAll()
            int r3 = r4 + 1
            r4 = r3
            r3 = r8
        L_0x0068:
            if (r4 >= r9) goto L_0x009c
            byte r5 = r0[r2]
            r6 = 5
            if (r5 < r6) goto L_0x009c
            r1.ReleaseAll()
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            SetCardsType(r1)
            boolean r5 = IsFirstCardsBig(r1, r12)
            if (r5 == 0) goto L_0x009c
            r5 = r14[r4]
            r5.ReleaseAll()
            r5 = r14[r4]
            r5.AddCards(r1)
            r5 = r14[r4]
            int r6 = r1.nLevel
            r5.nLevel = r6
            int r4 = r4 + 1
        L_0x009c:
            if (r4 >= r9) goto L_0x00d3
            byte r5 = r0[r2]
            r6 = 6
            if (r5 < r6) goto L_0x00d3
            r1.ReleaseAll()
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            SetCardsType(r1)
            boolean r5 = IsFirstCardsBig(r1, r12)
            if (r5 == 0) goto L_0x00d3
            r5 = r14[r4]
            r5.ReleaseAll()
            r5 = r14[r4]
            r5.AddCards(r1)
            r5 = r14[r4]
            int r6 = r1.nLevel
            r5.nLevel = r6
            int r4 = r4 + 1
        L_0x00d3:
            if (r4 >= r9) goto L_0x010d
            byte r5 = r0[r2]
            r6 = 7
            if (r5 < r6) goto L_0x010d
            r1.ReleaseAll()
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            SetCardsType(r1)
            boolean r5 = IsFirstCardsBig(r1, r12)
            if (r5 == 0) goto L_0x010d
            r5 = r14[r4]
            r5.ReleaseAll()
            r5 = r14[r4]
            r5.AddCards(r1)
            r5 = r14[r4]
            int r6 = r1.nLevel
            r5.nLevel = r6
            int r4 = r4 + 1
        L_0x010d:
            if (r4 >= r9) goto L_0x014b
            byte r5 = r0[r2]
            r6 = 8
            if (r5 != r6) goto L_0x014b
            r1.ReleaseAll()
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            r1.AddCard(r8, r2, r7)
            SetCardsType(r1)
            boolean r5 = IsFirstCardsBig(r1, r12)
            if (r5 == 0) goto L_0x014b
            r5 = r14[r4]
            r5.ReleaseAll()
            r5 = r14[r4]
            r5.AddCards(r1)
            r5 = r14[r4]
            int r6 = r1.nLevel
            r5.nLevel = r6
            int r4 = r4 + 1
        L_0x014b:
            int r2 = r2 + 1
            goto L_0x002e
        L_0x014f:
            if (r3 != 0) goto L_0x0153
            com.tencent.qqgame.common.SearchHint.fHasSi = r7
        L_0x0153:
            if (r4 >= r9) goto L_0x022b
            int r0 = r13.getCurrentLength()
            if (r0 < r11) goto L_0x022b
            SortByLevel(r13)
            int r0 = r13.getValueByIndex(r7)
            if (r0 < r10) goto L_0x022b
            int r0 = r13.getValueByIndex(r8)
            if (r0 < r10) goto L_0x022b
            r0 = r14[r4]
            r0.ReleaseAll()
            r0 = r14[r4]
            int r1 = r13.getValueByIndex(r7)
            r0.AddCard(r7, r1, r7)
            r0 = r14[r4]
            int r1 = r13.getValueByIndex(r8)
            r0.AddCard(r7, r1, r7)
            r0 = r14[r4]
            int r1 = r13.getValueByIndex(r7)
            int r1 = GetLevel(r1)
            r0.nLevel = r1
            int r0 = r4 + 1
            r1 = r0
            r0 = r8
        L_0x0191:
            if (r1 >= r9) goto L_0x01ee
            int r2 = r13.getCurrentLength()
            r3 = 4
            if (r2 < r3) goto L_0x01ee
            SortByLevel(r13)
            int r2 = r13.getValueByIndex(r7)
            if (r2 < r10) goto L_0x01ee
            int r2 = r13.getValueByIndex(r8)
            if (r2 < r10) goto L_0x01ee
            int r2 = r13.getValueByIndex(r11)
            if (r2 < r10) goto L_0x01ee
            r2 = 3
            int r2 = r13.getValueByIndex(r2)
            if (r2 < r10) goto L_0x01ee
            r2 = r14[r1]
            r2.ReleaseAll()
            r2 = r14[r1]
            int r3 = r13.getValueByIndex(r7)
            r2.AddCard(r7, r3, r7)
            r2 = r14[r1]
            int r3 = r13.getValueByIndex(r8)
            r2.AddCard(r7, r3, r7)
            r2 = r14[r1]
            int r3 = r13.getValueByIndex(r11)
            r2.AddCard(r7, r3, r7)
            r2 = r14[r1]
            r3 = 3
            int r3 = r13.getValueByIndex(r3)
            r2.AddCard(r7, r3, r7)
            r2 = r14[r1]
            int r3 = r13.getValueByIndex(r7)
            int r3 = GetLevel(r3)
            r2.nLevel = r3
            int r1 = r1 + 1
        L_0x01ee:
            r2 = r15
        L_0x01ef:
            int r3 = r1 - r8
            if (r2 >= r3) goto L_0x020e
            r3 = r14[r2]
            int r3 = r3.nLevel
            int r4 = r2 + 1
            r4 = r14[r4]
            int r4 = r4.nLevel
            if (r3 <= r4) goto L_0x020b
            r3 = r14[r2]
            int r4 = r2 + 1
            r4 = r14[r4]
            r14[r2] = r4
            int r4 = r2 + 1
            r14[r4] = r3
        L_0x020b:
            int r2 = r2 + 1
            goto L_0x01ef
        L_0x020e:
            java.io.PrintStream r2 = java.lang.System.out
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "SearchBomb >> nTempCount="
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.StringBuilder r3 = r3.append(r1)
            java.lang.String r3 = r3.toString()
            r2.println(r3)
            if (r0 != 0) goto L_0x022a
            com.tencent.qqgame.common.SearchHint.fHasBomb = r7
        L_0x022a:
            return r1
        L_0x022b:
            r0 = r3
            r1 = r4
            goto L_0x0191
        L_0x022f:
            r3 = r8
            goto L_0x0068
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.common.SearchHint.SearchBomb(com.tencent.qqgame.common.DDZCards, com.tencent.qqgame.common.DDZCards, com.tencent.qqgame.common.DDZCards[], int):int");
    }

    static int SearchContinue2Tuple(DDZCards dDZCards, DDZCards dDZCards2, DDZCards[] dDZCardsArr, byte[] bArr, boolean z) {
        boolean z2;
        boolean z3;
        int i = dDZCards2.nLen;
        int i2 = dDZCards2.nLevel;
        if (i2 >= 12 || i2 <= 2 || i <= 2 || i >= 13) {
            return 0;
        }
        int i3 = 0;
        for (int i4 = i2 + 2 + 1; i4 < 14 && i3 < 30; i4++) {
            int i5 = 0;
            while (true) {
                if (i5 >= i) {
                    z3 = true;
                    break;
                } else if (bArr[i4 - i5] < 2) {
                    z3 = false;
                    break;
                } else {
                    i5++;
                }
            }
            if (z3) {
                dDZCardsArr[i3].ReleaseAll();
                for (int i6 = 0; i6 < i; i6++) {
                    dDZCardsArr[i3].AddCard(1, i4 - i6, false);
                    dDZCardsArr[i3].AddCard(1, i4 - i6, false);
                }
                i3++;
            }
        }
        if (i3 < 30 && bArr[1] > 1) {
            int i7 = 1;
            while (true) {
                if (i7 >= i) {
                    z2 = true;
                    break;
                } else if (bArr[14 - i7] < 2) {
                    z2 = false;
                    break;
                } else {
                    i7++;
                }
            }
            if (bArr[1] < 2) {
                z2 = false;
            }
            if (z2) {
                dDZCardsArr[i3].ReleaseAll();
                dDZCardsArr[i3].AddCard(1, 1, false);
                dDZCardsArr[i3].AddCard(1, 1, false);
                for (int i8 = 0; i8 < i - 1; i8++) {
                    dDZCardsArr[i3].AddCard(1, 13 - i8, false);
                    dDZCardsArr[i3].AddCard(1, 13 - i8, false);
                }
                return i3 + 1;
            }
        }
        return i3;
    }

    static int SearchContinue3Tuple(DDZCards dDZCards, DDZCards dDZCards2, DDZCards[] dDZCardsArr, byte[] bArr, boolean z) {
        boolean z2;
        boolean z3;
        int i = dDZCards2.nLen;
        int i2 = dDZCards2.nLevel;
        if (i2 >= 12 || i2 <= 1 || i <= 1 || i >= 13) {
            return 0;
        }
        int i3 = 0;
        for (int i4 = i2 + 2 + 1; i4 < 14 && i3 < 30; i4++) {
            int i5 = 0;
            while (true) {
                if (i5 >= i) {
                    z3 = true;
                    break;
                } else if (bArr[i4 - i5] < 3) {
                    z3 = false;
                    break;
                } else {
                    i5++;
                }
            }
            if (z3) {
                dDZCardsArr[i3].ReleaseAll();
                for (int i6 = 0; i6 < i; i6++) {
                    dDZCardsArr[i3].AddCard(1, i4 - i6, false);
                    dDZCardsArr[i3].AddCard(1, i4 - i6, false);
                    dDZCardsArr[i3].AddCard(1, i4 - i6, false);
                }
                i3++;
            }
        }
        if (i3 < 30 && bArr[1] > 2) {
            int i7 = 1;
            while (true) {
                if (i7 >= i) {
                    z2 = true;
                    break;
                } else if (bArr[14 - i7] < 3) {
                    z2 = false;
                    break;
                } else {
                    i7++;
                }
            }
            if (bArr[1] < 3) {
                z2 = false;
            }
            if (z2) {
                dDZCardsArr[i3].ReleaseAll();
                dDZCardsArr[i3].AddCard(1, 1, false);
                dDZCardsArr[i3].AddCard(1, 1, false);
                dDZCardsArr[i3].AddCard(1, 1, false);
                for (int i8 = 0; i8 < i - 1; i8++) {
                    dDZCardsArr[i3].AddCard(1, 13 - i8, false);
                    dDZCardsArr[i3].AddCard(1, 13 - i8, false);
                    dDZCardsArr[i3].AddCard(1, 13 - i8, false);
                }
                return i3 + 1;
            }
        }
        return i3;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v17, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v18, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v11, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v15, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v34, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v35, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v19, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v23, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v50, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v50, resolved type: byte} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static int SearchContinue3TuplePlus2Tuple(com.tencent.qqgame.common.DDZCards r11, com.tencent.qqgame.common.DDZCards r12, com.tencent.qqgame.common.DDZCards[] r13, byte[] r14, boolean r15) {
        /*
            int r11 = r12.nLen
            int r0 = r12.nLevel
            r1 = 0
            com.tencent.qqgame.common.DDZCards r2 = new com.tencent.qqgame.common.DDZCards
            r2.<init>()
            com.tencent.qqgame.common.DDZCards r3 = new com.tencent.qqgame.common.DDZCards
            r3.<init>()
            r4 = 12
            if (r0 >= r4) goto L_0x01e8
            r4 = 1
            if (r0 <= r4) goto L_0x01e8
            r4 = 1
            if (r11 <= r4) goto L_0x01e8
            r4 = 13
            if (r11 >= r4) goto L_0x01e8
            int r0 = r0 + 2
            int r0 = r0 + 1
            r10 = r1
            r1 = r0
            r0 = r10
        L_0x0024:
            r4 = 14
            if (r1 >= r4) goto L_0x0032
            r2.ReleaseAll()
            r3.ReleaseAll()
            r4 = 30
            if (r0 < r4) goto L_0x00b4
        L_0x0032:
            r2.ReleaseAll()
            r3.ReleaseAll()
            r1 = 30
            if (r0 >= r1) goto L_0x01e6
            r1 = 1
            byte r1 = r14[r1]
            if (r1 <= 0) goto L_0x01e6
            r1 = 1
            r4 = 1
        L_0x0043:
            if (r4 >= r11) goto L_0x004e
            r5 = 14
            int r5 = r5 - r4
            byte r5 = r14[r5]
            r6 = 3
            if (r5 >= r6) goto L_0x016d
            r1 = 0
        L_0x004e:
            r4 = 1
            byte r4 = r14[r4]
            r5 = 3
            if (r4 >= r5) goto L_0x0055
            r1 = 0
        L_0x0055:
            if (r1 == 0) goto L_0x01e6
            r1 = 1
            r4 = 1
            r5 = 0
            r2.AddCard(r1, r4, r5)
            r1 = 1
            r4 = 1
            r5 = 0
            r2.AddCard(r1, r4, r5)
            r1 = 1
            r4 = 1
            r5 = 0
            r2.AddCard(r1, r4, r5)
            r1 = 1
            r4 = 1
            r5 = 0
            r3.AddCard(r1, r4, r5)
            r1 = 1
            r4 = 1
            r5 = 0
            r3.AddCard(r1, r4, r5)
            r1 = 1
            r4 = 1
            r5 = 0
            r3.AddCard(r1, r4, r5)
            r1 = 0
        L_0x007c:
            r4 = 1
            int r4 = r11 - r4
            if (r1 >= r4) goto L_0x0171
            r4 = 1
            r5 = 13
            int r5 = r5 - r1
            r6 = 0
            r2.AddCard(r4, r5, r6)
            r4 = 1
            r5 = 13
            int r5 = r5 - r1
            r6 = 0
            r2.AddCard(r4, r5, r6)
            r4 = 1
            r5 = 13
            int r5 = r5 - r1
            r6 = 0
            r2.AddCard(r4, r5, r6)
            r4 = 1
            r5 = 13
            int r5 = r5 - r1
            r6 = 0
            r3.AddCard(r4, r5, r6)
            r4 = 1
            r5 = 13
            int r5 = r5 - r1
            r6 = 0
            r3.AddCard(r4, r5, r6)
            r4 = 1
            r5 = 13
            int r5 = r5 - r1
            r6 = 0
            r3.AddCard(r4, r5, r6)
            int r1 = r1 + 1
            goto L_0x007c
        L_0x00b4:
            r4 = 1
            r5 = 0
        L_0x00b6:
            if (r5 >= r11) goto L_0x00c0
            int r6 = r1 - r5
            byte r6 = r14[r6]
            r7 = 3
            if (r6 >= r7) goto L_0x00f2
            r4 = 0
        L_0x00c0:
            if (r4 == 0) goto L_0x0163
            r4 = 0
        L_0x00c3:
            if (r4 >= r11) goto L_0x00f5
            r5 = 1
            int r6 = r1 - r4
            r7 = 0
            r2.AddCard(r5, r6, r7)
            r5 = 1
            int r6 = r1 - r4
            r7 = 0
            r2.AddCard(r5, r6, r7)
            r5 = 1
            int r6 = r1 - r4
            r7 = 0
            r2.AddCard(r5, r6, r7)
            r5 = 1
            int r6 = r1 - r4
            r7 = 0
            r3.AddCard(r5, r6, r7)
            r5 = 1
            int r6 = r1 - r4
            r7 = 0
            r3.AddCard(r5, r6, r7)
            r5 = 1
            int r6 = r1 - r4
            r7 = 0
            r3.AddCard(r5, r6, r7)
            int r4 = r4 + 1
            goto L_0x00c3
        L_0x00f2:
            int r5 = r5 + 1
            goto L_0x00b6
        L_0x00f5:
            r4 = 0
        L_0x00f6:
            if (r4 >= r11) goto L_0x013a
            r5 = 2
        L_0x00f9:
            r6 = 9
            if (r5 >= r6) goto L_0x0137
            r6 = 1
            r10 = r6
            r6 = r5
            r5 = r10
        L_0x0101:
            r7 = 14
            if (r5 >= r7) goto L_0x0134
            byte r7 = r14[r5]
            if (r7 != r6) goto L_0x0131
            r7 = 1
            r8 = 0
            r10 = r8
            r8 = r7
            r7 = r10
        L_0x010e:
            int r9 = r2.getCurrentLength()
            if (r7 >= r9) goto L_0x011e
            int r9 = r2.getValueByIndex(r7)
            if (r9 != r5) goto L_0x011b
            r8 = 0
        L_0x011b:
            int r7 = r7 + 1
            goto L_0x010e
        L_0x011e:
            if (r8 == 0) goto L_0x0131
            r6 = 1
            r7 = 0
            r2.AddCard(r6, r5, r7)
            r6 = 1
            r7 = 0
            r2.AddCard(r6, r5, r7)
            r5 = 100
            r6 = 100
            r10 = r6
            r6 = r5
            r5 = r10
        L_0x0131:
            int r5 = r5 + 1
            goto L_0x0101
        L_0x0134:
            int r5 = r6 + 1
            goto L_0x00f9
        L_0x0137:
            int r4 = r4 + 1
            goto L_0x00f6
        L_0x013a:
            int r4 = r2.getCurrentLength()
            int r5 = r11 * 5
            if (r4 != r5) goto L_0x0163
            SetCardsType(r2)
            int r4 = r12.getCurrentLength()
            if (r4 == 0) goto L_0x0151
            boolean r4 = IsFirstCardsBig(r2, r12)
            if (r4 == 0) goto L_0x0163
        L_0x0151:
            r4 = 30
            if (r0 >= r4) goto L_0x0163
            r4 = r13[r0]
            r4.ReleaseAll()
            if (r15 == 0) goto L_0x0167
            r4 = r13[r0]
            r4.AddCards(r2)
        L_0x0161:
            int r0 = r0 + 1
        L_0x0163:
            int r1 = r1 + 1
            goto L_0x0024
        L_0x0167:
            r4 = r13[r0]
            r4.AddCards(r3)
            goto L_0x0161
        L_0x016d:
            int r4 = r4 + 1
            goto L_0x0043
        L_0x0171:
            r1 = 0
        L_0x0172:
            if (r1 >= r11) goto L_0x01b6
            r4 = 2
        L_0x0175:
            r5 = 9
            if (r4 >= r5) goto L_0x01b3
            r5 = 1
            r10 = r5
            r5 = r4
            r4 = r10
        L_0x017d:
            r6 = 14
            if (r4 >= r6) goto L_0x01b0
            byte r6 = r14[r4]
            if (r6 != r5) goto L_0x01ad
            r6 = 1
            r7 = 0
            r10 = r7
            r7 = r6
            r6 = r10
        L_0x018a:
            int r8 = r2.getCurrentLength()
            if (r6 >= r8) goto L_0x019a
            int r8 = r2.getValueByIndex(r6)
            if (r8 != r4) goto L_0x0197
            r7 = 0
        L_0x0197:
            int r6 = r6 + 1
            goto L_0x018a
        L_0x019a:
            if (r7 == 0) goto L_0x01ad
            r5 = 1
            r6 = 0
            r2.AddCard(r5, r4, r6)
            r5 = 1
            r6 = 0
            r2.AddCard(r5, r4, r6)
            r4 = 100
            r5 = 100
            r10 = r5
            r5 = r4
            r4 = r10
        L_0x01ad:
            int r4 = r4 + 1
            goto L_0x017d
        L_0x01b0:
            int r4 = r5 + 1
            goto L_0x0175
        L_0x01b3:
            int r1 = r1 + 1
            goto L_0x0172
        L_0x01b6:
            int r14 = r2.getCurrentLength()
            int r11 = r11 * 5
            if (r14 != r11) goto L_0x01e6
            SetCardsType(r2)
            int r11 = r12.getCurrentLength()
            if (r11 == 0) goto L_0x01cd
            boolean r11 = IsFirstCardsBig(r2, r12)
            if (r11 == 0) goto L_0x01e6
        L_0x01cd:
            r11 = 30
            if (r0 >= r11) goto L_0x01e6
            r11 = r13[r0]
            r11.ReleaseAll()
            if (r15 == 0) goto L_0x01e0
            r11 = r13[r0]
            r11.AddCards(r2)
        L_0x01dd:
            int r11 = r0 + 1
        L_0x01df:
            return r11
        L_0x01e0:
            r11 = r13[r0]
            r11.AddCards(r3)
            goto L_0x01dd
        L_0x01e6:
            r11 = r0
            goto L_0x01df
        L_0x01e8:
            r11 = r1
            goto L_0x01df
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.common.SearchHint.SearchContinue3TuplePlus2Tuple(com.tencent.qqgame.common.DDZCards, com.tencent.qqgame.common.DDZCards, com.tencent.qqgame.common.DDZCards[], byte[], boolean):int");
    }

    static int SearchContinue3TuplePlusSingle(DDZCards dDZCards, DDZCards dDZCards2, DDZCards[] dDZCardsArr, byte[] bArr, boolean z) {
        int i = dDZCards2.nLen;
        int i2 = dDZCards2.nLevel;
        DDZCards dDZCards3 = new DDZCards();
        DDZCards dDZCards4 = new DDZCards();
        DDZCards dDZCards5 = new DDZCards();
        if (i2 >= 12 || i2 <= 1 || i <= 1 || i >= 13) {
            return 0;
        }
        int i3 = 0;
        for (int i4 = i2 + 2 + 1; i4 < 14; i4++) {
            dDZCards3.ReleaseAll();
            dDZCards4.ReleaseAll();
            if (i3 >= 30) {
                break;
            }
            boolean z2 = true;
            int i5 = 0;
            while (true) {
                if (i5 >= i) {
                    break;
                } else if (bArr[i4 - i5] < 3) {
                    z2 = false;
                    break;
                } else {
                    i5++;
                }
            }
            if (z2) {
                for (int i6 = 0; i6 < i; i6++) {
                    dDZCards3.AddCard(1, i4 - i6, false);
                    dDZCards3.AddCard(1, i4 - i6, false);
                    dDZCards3.AddCard(1, i4 - i6, false);
                    dDZCards4.AddCard(1, i4 - i6, false);
                    dDZCards4.AddCard(1, i4 - i6, false);
                    dDZCards4.AddCard(1, i4 - i6, false);
                }
                SortByTuple(dDZCards);
                dDZCards.ReleaseAll();
                for (int i7 = 0; i7 < i; i7++) {
                    int currentLength = dDZCards.getCurrentLength() - 1;
                    while (true) {
                        if (currentLength < 0) {
                            break;
                        }
                        boolean z3 = true;
                        for (int i8 = 0; i8 < dDZCards3.getCurrentLength(); i8++) {
                            if (dDZCards3.getValueByIndex(i8) == dDZCards.getValueByIndex(currentLength)) {
                                z3 = false;
                            }
                        }
                        if (z3) {
                            dDZCards3.AddCard(dDZCards.getLordCardByIndex(currentLength));
                            break;
                        }
                        currentLength--;
                    }
                }
                dDZCards3.AddCards(dDZCards5);
                if (dDZCards3.getCurrentLength() == i * 4) {
                    SetCardsType(dDZCards3);
                    if ((dDZCards2.getCurrentLength() == 0 || IsFirstCardsBig(dDZCards3, dDZCards2)) && i3 < 30) {
                        dDZCardsArr[i3].ReleaseAll();
                        if (z) {
                            dDZCardsArr[i3].AddCards(dDZCards3);
                        } else {
                            dDZCardsArr[i3].AddCards(dDZCards4);
                        }
                        i3++;
                    }
                }
            }
        }
        dDZCards3.ReleaseAll();
        dDZCards4.ReleaseAll();
        if (i3 < 30 && bArr[1] > 0) {
            boolean z4 = true;
            int i9 = 1;
            while (true) {
                if (i9 >= i) {
                    break;
                } else if (bArr[14 - i9] < 3) {
                    z4 = false;
                    break;
                } else {
                    i9++;
                }
            }
            if (bArr[1] < 3 ? false : z4) {
                dDZCards3.AddCard(1, 1, false);
                dDZCards3.AddCard(1, 1, false);
                dDZCards3.AddCard(1, 1, false);
                dDZCards4.AddCard(1, 1, false);
                dDZCards4.AddCard(1, 1, false);
                dDZCards4.AddCard(1, 1, false);
                for (int i10 = 0; i10 < i - 1; i10++) {
                    dDZCards3.AddCard(1, 13 - i10, false);
                    dDZCards3.AddCard(1, 13 - i10, false);
                    dDZCards3.AddCard(1, 13 - i10, false);
                    dDZCards4.AddCard(1, 13 - i10, false);
                    dDZCards4.AddCard(1, 13 - i10, false);
                    dDZCards4.AddCard(1, 13 - i10, false);
                }
                SortByTuple(dDZCards);
                dDZCards5.ReleaseAll();
                for (int i11 = 0; i11 < i; i11++) {
                    int currentLength2 = dDZCards.getCurrentLength() - 1;
                    while (true) {
                        if (currentLength2 < 0) {
                            break;
                        }
                        boolean z5 = true;
                        for (int i12 = 0; i12 < dDZCards3.getCurrentLength(); i12++) {
                            if (dDZCards3.getValueByIndex(i12) == dDZCards.getValueByIndex(currentLength2)) {
                                z5 = false;
                            }
                        }
                        if (z5) {
                            dDZCards5.AddCard(dDZCards.getLordCardByIndex(currentLength2));
                            int i13 = currentLength2 - 1;
                            break;
                        }
                        currentLength2--;
                    }
                }
                dDZCards3.AddCards(dDZCards5);
                if (dDZCards3.getCurrentLength() == i * 4) {
                    SetCardsType(dDZCards3);
                    if ((dDZCards2.getCurrentLength() == 0 || IsFirstCardsBig(dDZCards3, dDZCards2)) && i3 < 30) {
                        dDZCardsArr[i3].ReleaseAll();
                        if (z) {
                            dDZCardsArr[i3].AddCards(dDZCards3);
                        } else {
                            dDZCardsArr[i3].AddCards(dDZCards4);
                        }
                        return i3 + 1;
                    }
                }
            }
        }
        return i3;
    }

    static int SearchContinueSingle(DDZCards dDZCards, DDZCards dDZCards2, DDZCards[] dDZCardsArr, byte[] bArr, boolean z) {
        boolean z2;
        boolean z3;
        int i = dDZCards2.nLen;
        int i2 = dDZCards2.nLevel;
        if (i2 >= 12 || i2 <= 4 || i <= 4 || i >= 13) {
            return 0;
        }
        int i3 = 0;
        for (int i4 = i2 + 2 + 1; i4 < 14 && i3 < 30; i4++) {
            int i5 = 0;
            while (true) {
                if (i5 >= i) {
                    z3 = true;
                    break;
                } else if (bArr[i4 - i5] == 0) {
                    z3 = false;
                    break;
                } else {
                    i5++;
                }
            }
            if (z3) {
                dDZCardsArr[i3].ReleaseAll();
                for (int i6 = 0; i6 < i; i6++) {
                    dDZCardsArr[i3].AddCard(1, i4 - i6, false);
                }
                i3++;
            }
        }
        if (i3 < 30 && bArr[1] > 0) {
            int i7 = 1;
            while (true) {
                if (i7 >= i) {
                    z2 = true;
                    break;
                } else if (bArr[14 - i7] == 0) {
                    z2 = false;
                    break;
                } else {
                    i7++;
                }
            }
            if (bArr[1] < 1) {
                z2 = false;
            }
            if (z2) {
                dDZCardsArr[i3].ReleaseAll();
                dDZCardsArr[i3].AddCard(1, 1, false);
                for (int i8 = 0; i8 < i - 1; i8++) {
                    dDZCardsArr[i3].AddCard(1, 13 - i8, false);
                }
                return i3 + 1;
            }
        }
        return i3;
    }

    static void SearchPlus2Tuple(DDZCards dDZCards) {
        boolean z;
        byte[] bArr = new byte[16];
        dDZCards.GetTupleInfo(bArr);
        DDZCards[] dDZCardsArr = new DDZCards[30];
        for (int i = 0; i < dDZCardsArr.length; i++) {
            dDZCardsArr[i] = new DDZCards();
        }
        DDZCards dDZCards2 = new DDZCards();
        int i2 = 2;
        int i3 = 0;
        boolean z2 = false;
        while (i2 < 9) {
            boolean z3 = z2;
            int i4 = i3;
            for (int i5 = 3; i5 < 14; i5++) {
                if (bArr[i5] == i2) {
                    dDZCards2.ReleaseAll();
                    dDZCards2.AddCard(1, i5, false);
                    dDZCards2.AddCard(1, i5, false);
                    SetCardsType(dDZCards2);
                    if (i4 < 30) {
                        dDZCardsArr[i4].ReleaseAll();
                        dDZCardsArr[i4].AddCards(dDZCards2);
                        i4++;
                        z3 = true;
                    } else {
                        z3 = true;
                    }
                }
            }
            for (int i6 = 1; i6 < 3; i6++) {
                if (bArr[i6] == i2) {
                    dDZCards2.ReleaseAll();
                    dDZCards2.AddCard(1, i6, false);
                    dDZCards2.AddCard(1, i6, false);
                    SetCardsType(dDZCards2);
                    if (i4 < 30) {
                        dDZCardsArr[i4].ReleaseAll();
                        dDZCardsArr[i4].AddCards(dDZCards2);
                        i4++;
                        z3 = true;
                    } else {
                        z3 = true;
                    }
                }
            }
            i2++;
            i3 = i4;
            z2 = z3;
        }
        if (!z2) {
            fHasDuizi = false;
        }
        int[] iArr = new int[MAX_INHAND_POKER_NUM];
        int[] iArr2 = new int[MAX_INHAND_POKER_NUM];
        for (int i7 = 0; i7 < MAX_INHAND_POKER_NUM; i7++) {
            iArr2[i7] = 0;
        }
        int currentLength = m_DDZCardsAIMain[0].getCurrentLength() / 3;
        m_nAISubCount = 0;
        for (int i8 = 0; i8 < m_nAIMainCount; i8++) {
            for (int i9 = 0; i9 < currentLength; i9++) {
                iArr[i9] = GetLevel(m_DDZCardsAIMain[i8].getLordCardByIndex(i9 * 3));
            }
            for (int i10 = i3 - 1; i10 >= 0; i10--) {
                if (iArr2[i10] == 0 && m_nAISubCount < 50) {
                    int GetLevel = GetLevel(dDZCardsArr[i10].getLordCardByIndex(0));
                    int i11 = 0;
                    while (true) {
                        if (i11 >= currentLength) {
                            z = false;
                            break;
                        } else if (iArr[i11] == GetLevel) {
                            z = true;
                            break;
                        } else {
                            i11++;
                        }
                    }
                    if (!z) {
                        iArr2[i10] = 1;
                        m_DDZCardsAISub[m_nAISubCount].ReleaseAll();
                        m_DDZCardsAISub[m_nAISubCount].AddCards(dDZCardsArr[i10]);
                        m_nAISubCount++;
                    }
                }
            }
        }
    }

    static void SearchPlusSingle(DDZCards dDZCards) {
        SearchPlusSingle(dDZCards, true);
    }

    static void SearchPlusSingle(DDZCards dDZCards, boolean z) {
        int GetLevel;
        boolean z2;
        SortByTuple(dDZCards);
        int[] iArr = new int[MAX_INHAND_POKER_NUM];
        int[] iArr2 = new int[MAX_INHAND_POKER_NUM];
        for (int i = 0; i < MAX_INHAND_POKER_NUM; i++) {
            iArr2[i] = 0;
        }
        int currentLength = m_DDZCardsAIMain[0].getCurrentLength() / 3;
        m_nAISubCount = 0;
        int i2 = 0;
        int i3 = 0;
        while (i2 < m_nAIMainCount) {
            for (int i4 = 0; i4 < currentLength; i4++) {
                iArr[i4] = GetLevel(m_DDZCardsAIMain[i2].getLordCardByIndex(i4 * 3));
            }
            int i5 = i3;
            for (int currentLength2 = dDZCards.getCurrentLength() - 1; currentLength2 >= 0; currentLength2--) {
                if (iArr2[currentLength2] == 0 && m_nAISubCount < 50 && (GetLevel = GetLevel(dDZCards.getLordCardByIndex(currentLength2))) < 14) {
                    int i6 = 0;
                    while (true) {
                        if (i6 >= currentLength) {
                            z2 = false;
                            break;
                        } else if (iArr[i6] == GetLevel) {
                            z2 = true;
                            break;
                        } else {
                            i6++;
                        }
                    }
                    if (!z2) {
                        iArr2[currentLength2] = 1;
                        if (z) {
                            m_DDZCardsAISub[m_nAISubCount].ReleaseAll();
                            m_DDZCardsAISub[m_nAISubCount].AddCard(dDZCards.getLordCardByIndex(currentLength2));
                            m_nAISubCount++;
                            i5 = GetLevel;
                        } else if (i5 != GetLevel) {
                            m_DDZCardsAISub[m_nAISubCount].ReleaseAll();
                            m_DDZCardsAISub[m_nAISubCount].AddCard(dDZCards.getLordCardByIndex(currentLength2));
                            m_nAISubCount++;
                            i5 = GetLevel;
                        }
                    }
                }
            }
            i2++;
            i3 = i5;
        }
    }

    public static void SelectAI(Vector vector, Vector vector2, int i, Vector vector3, int i2) {
        System.out.println("SelectAI.InhandPokerNum =" + i + ",LastOutPokerNum=" + i2);
        m_nFirstOutCount = 0;
        m_nMainCount = 0;
        m_nSubCount = 0;
        if (vector2 != null && vector3 != null) {
            m_nAIMainCount = 0;
            m_nAISubCount = 0;
            DDZCards dDZCards = new DDZCards();
            GetCardsFromPokers(vector2, i, dDZCards);
            System.out.println("PlayerCards.getCurrentLength=" + dDZCards.getCurrentLength());
            DDZCards dDZCards2 = new DDZCards();
            GetCardsFromPokers(vector, vector.size(), dDZCards2);
            System.out.println("SelectCards.getCurrentLength=" + dDZCards2.getCurrentLength());
            DDZCards dDZCards3 = new DDZCards();
            GetCardsFromPokers(vector3, i2, dDZCards3);
            System.out.println("LastPlayerCards.getCurrentLength=" + dDZCards3.getCurrentLength());
            int size = vector.size();
            if (!SetCardsType(dDZCards3) || size == 0) {
                System.out.println("LastPlayerCards.nLen=" + dDZCards3.nLen + ",LastPlayerCards.nLevel=" + dDZCards3.nLevel + ",LastPlayerCards.nType=" + dDZCards3.nType);
                return;
            }
            System.out.println(">LastPlayerCards.nLen=" + dDZCards3.nLen + ",LastPlayerCards.nLevel=" + dDZCards3.nLevel + ",LastPlayerCards.nType=" + dDZCards3.nType);
            byte[] bArr = new byte[16];
            System.out.println("nCardsCount=" + dDZCards.GetTupleInfo(bArr));
            for (int i3 = 0; i3 < bArr.length; i3++) {
                System.out.println("tupleInfo[" + i3 + "]=" + ((int) bArr[i3]));
            }
            switch (dDZCards3.nType) {
                case 1:
                    if (size == 1 && GetLevel(dDZCards2.getLordCardByIndex(0)) > dDZCards3.nLevel) {
                        if (m_nFirstOutCount < 50) {
                            m_DDZCardsFirstOut[m_nFirstOutCount].ReleaseAll();
                            m_DDZCardsFirstOut[m_nFirstOutCount].AddCards(dDZCards2);
                            m_nFirstOutCount++;
                            break;
                        } else {
                            return;
                        }
                    }
                case 2:
                    if (fHasDuizi) {
                        m_nAIMainCount = Search2Tuple(dDZCards, dDZCards3, m_DDZCardsAIMain, bArr, false);
                        for (int i4 = 0; i4 < m_nAIMainCount; i4++) {
                            if (m_DDZCardsAIMain[i4].Contain(dDZCards2)) {
                                if (m_nFirstOutCount < 50) {
                                    m_DDZCardsFirstOut[m_nFirstOutCount].ReleaseAll();
                                    m_DDZCardsFirstOut[m_nFirstOutCount].AddCards(m_DDZCardsAIMain[i4]);
                                    m_nFirstOutCount++;
                                } else {
                                    return;
                                }
                            }
                        }
                        break;
                    }
                    break;
                case 3:
                    if (fHasSan) {
                        m_nAIMainCount = Search3Tuple(dDZCards, dDZCards3, m_DDZCardsAIMain, bArr, false);
                        for (int i5 = 0; i5 < m_nAIMainCount; i5++) {
                            if (m_DDZCardsAIMain[i5].Contain(dDZCards2)) {
                                if (m_nFirstOutCount < 50) {
                                    m_DDZCardsFirstOut[m_nFirstOutCount].ReleaseAll();
                                    m_DDZCardsFirstOut[m_nFirstOutCount].AddCards(m_DDZCardsAIMain[i5]);
                                    m_nFirstOutCount++;
                                } else {
                                    return;
                                }
                            }
                        }
                        break;
                    }
                    break;
                case 4:
                    if (fHasSan) {
                        m_nAIMainCount = Search3TuplePlusSingle(dDZCards, dDZCards3, m_DDZCardsAIMain, bArr, false);
                        SearchPlusSingle(dDZCards);
                        FirstOut3TuplePlus(m_DDZCardsAIMain, m_nAIMainCount, m_DDZCardsAISub, m_nAISubCount, dDZCards2);
                        break;
                    }
                    break;
                case 5:
                    if (fHasSan) {
                        m_nAIMainCount = Search3TuplePlus2Tuple(dDZCards, dDZCards3, m_DDZCardsAIMain, bArr, false);
                        SearchPlus2Tuple(dDZCards);
                        FirstOut3TuplePlus(m_DDZCardsAIMain, m_nAIMainCount, m_DDZCardsAISub, m_nAISubCount, dDZCards2);
                        break;
                    }
                    break;
                case 6:
                    m_nAIMainCount = SearchContinueSingle(dDZCards, dDZCards3, m_DDZCardsAIMain, bArr, false);
                    for (int i6 = 0; i6 < m_nAIMainCount; i6++) {
                        if (m_DDZCardsAIMain[i6].Contain(dDZCards2)) {
                            if (m_nFirstOutCount < 50) {
                                m_DDZCardsFirstOut[m_nFirstOutCount].ReleaseAll();
                                m_DDZCardsFirstOut[m_nFirstOutCount].AddCards(m_DDZCardsAIMain[i6]);
                                m_nFirstOutCount++;
                            } else {
                                return;
                            }
                        }
                    }
                    break;
                case 7:
                    if (fHasDuizi) {
                        m_nAIMainCount = SearchContinue2Tuple(dDZCards, dDZCards3, m_DDZCardsAIMain, bArr, false);
                        for (int i7 = 0; i7 < m_nAIMainCount; i7++) {
                            if (m_DDZCardsAIMain[i7].Contain(dDZCards2)) {
                                if (m_nFirstOutCount < 50) {
                                    m_DDZCardsFirstOut[m_nFirstOutCount].ReleaseAll();
                                    m_DDZCardsFirstOut[m_nFirstOutCount].AddCards(m_DDZCardsAIMain[i7]);
                                    m_nFirstOutCount++;
                                } else {
                                    return;
                                }
                            }
                        }
                        break;
                    }
                    break;
                case 8:
                    if (fHasSan) {
                        m_nAIMainCount = SearchContinue3Tuple(dDZCards, dDZCards3, m_DDZCardsAIMain, bArr, false);
                        for (int i8 = 0; i8 < m_nAIMainCount; i8++) {
                            if (m_DDZCardsAIMain[i8].Contain(dDZCards2)) {
                                if (m_nFirstOutCount < 50) {
                                    m_DDZCardsFirstOut[m_nFirstOutCount].ReleaseAll();
                                    m_DDZCardsFirstOut[m_nFirstOutCount].AddCards(m_DDZCardsAIMain[i8]);
                                    m_nFirstOutCount++;
                                } else {
                                    return;
                                }
                            }
                        }
                        break;
                    }
                    break;
                case 9:
                    if (fHasSan) {
                        m_nAIMainCount = SearchContinue3TuplePlus2Tuple(dDZCards, dDZCards3, m_DDZCardsAIMain, bArr, false);
                        SearchPlus2Tuple(dDZCards);
                        FirstOutSubDDZCards(m_DDZCardsAIMain, m_nAIMainCount, m_DDZCardsAISub, m_nAISubCount, dDZCards2);
                        break;
                    }
                    break;
                case 10:
                    if (fHasSan) {
                        m_nAIMainCount = SearchContinue3TuplePlusSingle(dDZCards, dDZCards3, m_DDZCardsAIMain, bArr, false);
                        SearchPlusSingle(dDZCards);
                        FirstOutSubDDZCards(m_DDZCardsAIMain, m_nAIMainCount, m_DDZCardsAISub, m_nAISubCount, dDZCards2);
                        break;
                    }
                    break;
                case 13:
                    if (fHasSan) {
                        m_nAIMainCount = Search4TuplePlusSingle(dDZCards, dDZCards3, m_DDZCardsAIMain, bArr, false);
                        SearchPlusSingle(dDZCards);
                        FirstOutSubDDZCards(m_DDZCardsAIMain, m_nAIMainCount, m_DDZCardsAISub, m_nAISubCount, dDZCards2);
                        break;
                    }
                    break;
                case 14:
                    if (fHasSan) {
                        m_nAIMainCount = Search4TuplePlus2Tuple(dDZCards, dDZCards3, m_DDZCardsAIMain, bArr, false);
                        SearchPlus2Tuple(dDZCards);
                        FirstOutSubDDZCards(m_DDZCardsAIMain, m_nAIMainCount, m_DDZCardsAISub, m_nAISubCount, dDZCards2);
                        break;
                    }
                    break;
            }
            if (fHasBomb) {
                DDZCards[] dDZCardsArr = new DDZCards[5];
                for (int i9 = 0; i9 < dDZCardsArr.length; i9++) {
                    dDZCardsArr[i9] = new DDZCards();
                }
                int SearchBomb = SearchBomb(dDZCards3, dDZCards, dDZCardsArr, 0);
                for (int i10 = 0; i10 < SearchBomb; i10++) {
                    if (dDZCardsArr[i10].Contain(dDZCards2)) {
                        if (m_nFirstOutCount < 50) {
                            m_DDZCardsFirstOut[m_nFirstOutCount].ReleaseAll();
                            m_DDZCardsFirstOut[m_nFirstOutCount].AddCards(dDZCardsArr[i10]);
                            m_nFirstOutCount++;
                        } else {
                            return;
                        }
                    }
                }
            }
        }
    }

    static boolean SetCardsType(DDZCards dDZCards) {
        if (dDZCards.getCurrentLength() == 0) {
            return false;
        }
        dDZCards.nLen = 0;
        dDZCards.nLevel = 0;
        dDZCards.nType = 0;
        if (Check_Card_Type_CARD_TYPE_SINGLE(dDZCards)) {
            return true;
        }
        if (Check_Card_Type_CARD_TYPE_CONTINUE_SINGLE(dDZCards)) {
            return true;
        }
        if (Check_Card_Type_CARD_TYPE_2_TUPLE(dDZCards)) {
            return true;
        }
        if (Check_Card_Type_CARD_TYPE_CONTINUE_2_TUPLE(dDZCards)) {
            return true;
        }
        if (Check_Card_Type_CARD_TYPE_3_TUPLE(dDZCards)) {
            return true;
        }
        if (Check_Card_Type_CARD_TYPE_CONTINUE_3_TUPLE(dDZCards)) {
            return true;
        }
        if (Check_Card_Type_CARD_TYPE_3_TUPLE_PLUS_SINGLE(dDZCards)) {
            return true;
        }
        if (Check_Card_Type_CARD_TYPE_3_TUPLE_PLUS_2_TUPLE(dDZCards)) {
            return true;
        }
        if (Check_Card_Type_CARD_TYPE_CONTINUE_3_TUPLE_PLUS_SINGLE(dDZCards)) {
            return true;
        }
        if (Check_Card_Type_CARD_TYPE_CONTINUE_3_TUPPLE_PLUS_2_TUPLE(dDZCards)) {
            return true;
        }
        if (Check_Card_Type_CARD_TYPE_CONTINUE_3_TUPLE_PLUS_CONTINUE_SINGLE(dDZCards)) {
            return true;
        }
        if (Check_Card_Type_CARD_TYPE_CONTINUE_3_TUPLE_PLUS_CONTINUE_2_TUPLE(dDZCards)) {
            return true;
        }
        if (Check_Card_Type_CARD_TYPE_4_TUPLE_PLUS_2_SINGLE(dDZCards)) {
            return true;
        }
        if (Check_Card_Type_CARD_TYPE_4_TUPLE_PLUS_2_2_TUPLE(dDZCards)) {
            return true;
        }
        if (Check_Card_Type_CARD_TYPE_4_BOMB(dDZCards)) {
            return true;
        }
        return Check_Card_Type_CARD_TYPE_2_JOKER(dDZCards);
    }

    static void SortByLevel(DDZCards dDZCards) {
        if (2 <= dDZCards.getCurrentLength()) {
            int i = 0;
            while (i < dDZCards.getCurrentLength()) {
                if (GetLevel(dDZCards.getLordCardByIndex(i)) < GetLevel(dDZCards.getLordCardByIndex(i + 1))) {
                    dDZCards.swap(i, i + 1);
                    i = 0;
                } else {
                    i++;
                    if (i == dDZCards.getCurrentLength() - 1) {
                        i++;
                    }
                }
            }
        }
    }

    static void SortByTuple(DDZCards dDZCards) {
        if (dDZCards.getCurrentLength() >= 2) {
            DDZCards copy = dDZCards.copy(dDZCards);
            dDZCards.ReleaseAll();
            DDZCards dDZCards2 = new DDZCards();
            byte[] bArr = new byte[16];
            copy.GetTupleInfo(bArr);
            for (int i = 8; i > 0; i--) {
                if (dDZCards2 != null) {
                    dDZCards2.ReleaseAll();
                }
                for (int i2 = 0; i2 < 16; i2++) {
                    if (bArr[i2] == i) {
                        for (int i3 = 0; i3 < copy.getCurrentLength(); i3++) {
                            if (copy.getValueByIndex(i3) == i2) {
                                dDZCards2.AddCard(copy.getLordCardByIndex(i3));
                            }
                        }
                    }
                }
                if (dDZCards2.getCurrentLength() != 0) {
                    SortByLevel(dDZCards2);
                    dDZCards.AddCards(dDZCards2);
                }
            }
        }
    }

    static LordCard char2CARD(byte b) {
        return new LordCard(b);
    }

    public static void init() {
        for (int i = 0; i < m_DDZCardsFirstOut.length; i++) {
            m_DDZCardsFirstOut[i] = null;
            m_DDZCardsFirstOut[i] = new DDZCards();
        }
        for (int i2 = 0; i2 < m_DDZCardsHint.length; i2++) {
            m_DDZCardsHint[i2] = null;
            m_DDZCardsHint[i2] = new DDZCards();
        }
        for (int i3 = 0; i3 < m_DDZCardsAIMain.length; i3++) {
            m_DDZCardsAIMain[i3] = null;
            m_DDZCardsAIMain[i3] = new DDZCards();
        }
        for (int i4 = 0; i4 < m_DDZCardsAISub.length; i4++) {
            m_DDZCardsAISub[i4] = null;
            m_DDZCardsAISub[i4] = new DDZCards();
        }
        m_nFirstOutCount = 0;
        m_nHintCount = 0;
        m_nHintPos = 0;
        m_nSrcCount = 0;
        m_nAimCount = 0;
        m_nMainCount = 0;
        m_nSubCount = 0;
        m_nAIMainCount = 0;
        m_nAISubCount = 0;
        m_nSubNum = 0;
        System.out.println("SearchHint.init()");
        haveSearched = false;
    }

    public boolean FindFitPokers(Vector vector, int i, Vector vector2, int i2) {
        if (vector == null || vector2 == null) {
            return false;
        }
        if (m_nFirstOutCount <= 0) {
            return false;
        }
        m_DDZCardsAim.ReleaseAll();
        if (m_nFirstOutCount == 1) {
            m_DDZCardsAim.AddCards(m_DDZCardsFirstOut[0]);
        } else {
            GetSamePokers(m_DDZCardsAim);
        }
        return true;
    }

    public int GetHintCount() {
        return m_nHintCount;
    }

    public DDZCards GetNextHint() {
        if (m_nHintPos >= m_nHintCount) {
            m_nHintPos = 0;
        }
        DDZCards[] dDZCardsArr = m_DDZCardsHint;
        int i = m_nHintPos;
        m_nHintPos = i + 1;
        return dDZCardsArr[i];
    }

    /* access modifiers changed from: package-private */
    public void GetSamePokers(DDZCards dDZCards) {
        boolean z;
        if (m_nFirstOutCount > 0) {
            DDZCards dDZCards2 = new DDZCards();
            int[] iArr = new int[20];
            dDZCards.ReleaseAll();
            dDZCards.AddCards(m_DDZCardsFirstOut[0]);
            for (int i = 1; i < m_nFirstOutCount; i++) {
                for (int i2 = 0; i2 < 20; i2++) {
                    iArr[i2] = 0;
                }
                dDZCards2.ReleaseAll();
                dDZCards2.AddCards(dDZCards);
                dDZCards.ReleaseAll();
                for (int i3 = 0; i3 < dDZCards2.getCurrentLength(); i3++) {
                    int i4 = 0;
                    while (true) {
                        if (i4 < m_DDZCardsFirstOut[i].getCurrentLength()) {
                            if (iArr[i4] == 0 && dDZCards2.getValueByIndex(i3) == m_DDZCardsFirstOut[i].getValueByIndex(i4)) {
                                iArr[i4] = 1;
                                z = true;
                                break;
                            }
                            i4++;
                        } else {
                            z = false;
                            break;
                        }
                    }
                    if (z) {
                        dDZCards.AddCard(dDZCards2.getLordCardByIndex(i3));
                    }
                }
            }
        }
    }

    public DDZCards GetSelectHint() {
        return m_DDZCardsAim;
    }

    public void SearchHint(Vector vector, int i, Vector vector2, int i2) {
        try {
            System.out.println("pInhand=" + vector.size() + ",pLast=" + vector2.size());
            if (vector != null && vector2 != null) {
                m_nHintCount = 0;
                m_nHintPos = 0;
                DDZCards dDZCards = new DDZCards();
                GetCardsFromPokers(vector, i, dDZCards);
                DDZCards dDZCards2 = new DDZCards();
                GetCardsFromPokers(vector2, i2, dDZCards2);
                if (!SetCardsType(dDZCards2)) {
                    System.out.println("没有该牌垿");
                    return;
                }
                byte[] bArr = new byte[16];
                dDZCards.GetTupleInfo(bArr);
                switch (dDZCards2.nType) {
                    case 1:
                        int i3 = dDZCards2.nLevel;
                        SortByTuple(dDZCards);
                        int i4 = 0;
                        for (int currentLength = dDZCards.getCurrentLength() - 1; currentLength >= 0; currentLength--) {
                            if (GetLevel(dDZCards.getLordCardByIndex(currentLength)) > i3 && i4 != GetLevel(dDZCards.getLordCardByIndex(currentLength)) && m_nHintCount < 30) {
                                i4 = GetLevel(dDZCards.getLordCardByIndex(currentLength));
                                m_DDZCardsHint[m_nHintCount].ReleaseAll();
                                m_DDZCardsHint[m_nHintCount].AddCard(dDZCards.getLordCardByIndex(currentLength));
                                m_nHintCount++;
                            }
                        }
                        break;
                    case 2:
                        if (fHasDuizi) {
                            m_nHintCount = Search2Tuple(dDZCards, dDZCards2, m_DDZCardsHint, bArr, true);
                            break;
                        }
                        break;
                    case 3:
                        if (fHasSan) {
                            m_nHintCount = Search3Tuple(dDZCards, dDZCards2, m_DDZCardsHint, bArr, true);
                            break;
                        }
                        break;
                    case 4:
                        if (fHasSan) {
                            m_nHintCount = Search3TuplePlusSingle(dDZCards, dDZCards2, m_DDZCardsHint, bArr, true);
                            break;
                        }
                        break;
                    case 5:
                        if (fHasSan) {
                            m_nHintCount = Search3TuplePlus2Tuple(dDZCards, dDZCards2, m_DDZCardsHint, bArr, true);
                            break;
                        }
                        break;
                    case 6:
                        m_nHintCount = SearchContinueSingle(dDZCards, dDZCards2, m_DDZCardsHint, bArr, true);
                        break;
                    case 7:
                        if (fHasDuizi) {
                            m_nHintCount = SearchContinue2Tuple(dDZCards, dDZCards2, m_DDZCardsHint, bArr, true);
                            break;
                        }
                        break;
                    case 8:
                        if (fHasSan) {
                            m_nHintCount = SearchContinue3Tuple(dDZCards, dDZCards2, m_DDZCardsHint, bArr, true);
                            break;
                        }
                        break;
                    case 9:
                        if (fHasSan) {
                            m_nHintCount = SearchContinue3TuplePlus2Tuple(dDZCards, dDZCards2, m_DDZCardsHint, bArr, true);
                            break;
                        }
                        break;
                    case 10:
                        if (fHasSan) {
                            m_nHintCount = SearchContinue3TuplePlusSingle(dDZCards, dDZCards2, m_DDZCardsHint, bArr, true);
                            break;
                        }
                        break;
                    case 13:
                        if (fHasSi) {
                            m_nHintCount = Search4TuplePlusSingle(dDZCards, dDZCards2, m_DDZCardsHint, bArr, true);
                            break;
                        }
                        break;
                    case 14:
                        if (fHasSi) {
                            m_nHintCount = Search4TuplePlus2Tuple(dDZCards, dDZCards2, m_DDZCardsHint, bArr, true);
                            break;
                        }
                        break;
                }
                if (fHasSi || fHasBomb) {
                    m_nHintCount = SearchBomb(dDZCards2, dDZCards, m_DDZCardsHint, m_nHintCount);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
