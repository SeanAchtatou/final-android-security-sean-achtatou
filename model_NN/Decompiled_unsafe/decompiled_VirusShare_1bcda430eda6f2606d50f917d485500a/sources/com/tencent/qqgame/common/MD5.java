package com.tencent.qqgame.common;

import com.tencent.qqgame.hall.ui.LoginActivity;
import java.io.UnsupportedEncodingException;

public class MD5 {
    static final byte[] PADDING = {Byte.MIN_VALUE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    static final int[][] S = {new int[]{7, 12, 17, 22}, new int[]{5, 9, 14, 20}, new int[]{4, 11, 16, 23}, new int[]{6, 10, 15, 21}};
    private byte[] buffer = new byte[64];
    private long[] count = new long[2];
    private byte[] digest = new byte[16];
    public String digestHexStr;
    private long[] state = new long[4];

    public MD5() {
        md5Init();
    }

    private void Decode(long[] jArr, byte[] bArr, int i) {
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3 += 4) {
            jArr[i2] = b2iu(bArr[i3]) | (b2iu(bArr[i3 + 1]) << 8) | (b2iu(bArr[i3 + 2]) << 16) | (b2iu(bArr[i3 + 3]) << 24);
            i2++;
        }
    }

    private void Encode(byte[] bArr, long[] jArr, int i) {
        int i2 = 0;
        for (int i3 = 0; i3 < i; i3 += 4) {
            bArr[i3] = (byte) ((int) (jArr[i2] & 255));
            bArr[i3 + 1] = (byte) ((int) ((jArr[i2] >>> 8) & 255));
            bArr[i3 + 2] = (byte) ((int) ((jArr[i2] >>> 16) & 255));
            bArr[i3 + 3] = (byte) ((int) ((jArr[i2] >>> 24) & 255));
            i2++;
        }
    }

    private long F(long j, long j2, long j3) {
        return (j & j2) | ((-1 ^ j) & j3);
    }

    private long FF(long j, long j2, long j3, long j4, long j5, long j6, long j7) {
        long F = F(j2, j3, j4) + j5 + j7 + j;
        return ((long) ((((int) F) >>> ((int) (32 - j6))) | (((int) F) << ((int) j6)))) + j2;
    }

    private long G(long j, long j2, long j3) {
        return (j & j3) | ((-1 ^ j3) & j2);
    }

    private long GG(long j, long j2, long j3, long j4, long j5, long j6, long j7) {
        long G = G(j2, j3, j4) + j5 + j7 + j;
        return ((long) ((((int) G) >>> ((int) (32 - j6))) | (((int) G) << ((int) j6)))) + j2;
    }

    private long H(long j, long j2, long j3) {
        return (j ^ j2) ^ j3;
    }

    private long HH(long j, long j2, long j3, long j4, long j5, long j6, long j7) {
        long H = H(j2, j3, j4) + j5 + j7 + j;
        return ((long) ((((int) H) >>> ((int) (32 - j6))) | (((int) H) << ((int) j6)))) + j2;
    }

    private long I(long j, long j2, long j3) {
        return ((-1 ^ j3) | j) ^ j2;
    }

    private long II(long j, long j2, long j3, long j4, long j5, long j6, long j7) {
        long I = I(j2, j3, j4) + j5 + j7 + j;
        return ((long) ((((int) I) >>> ((int) (32 - j6))) | (((int) I) << ((int) j6)))) + j2;
    }

    public static long b2iu(byte b) {
        return b < 0 ? (long) (b & LoginActivity.STATUS_NONE) : (long) b;
    }

    public static String byteHEX(byte b) {
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        return new String(new char[]{cArr[(b >>> 4) & 15], cArr[b & 15]});
    }

    private void md5Final() {
        byte[] bArr = new byte[8];
        Encode(bArr, this.count, 8);
        int i = ((int) (this.count[0] >>> 3)) & 63;
        md5Update(PADDING, i < 56 ? 56 - i : 120 - i);
        md5Update(bArr, 8);
        Encode(this.digest, this.state, 16);
    }

    private void md5Init() {
        this.count[0] = 0;
        this.count[1] = 0;
        this.state[0] = 1732584193;
        this.state[1] = 4023233417L;
        this.state[2] = 2562383102L;
        this.state[3] = 271733878;
    }

    private void md5Memcpy(byte[] bArr, byte[] bArr2, int i, int i2, int i3) {
        for (int i4 = 0; i4 < i3; i4++) {
            bArr[i + i4] = bArr2[i2 + i4];
        }
    }

    private void md5Transform(byte[] bArr) {
        long j = this.state[0];
        long j2 = this.state[1];
        long j3 = this.state[2];
        long j4 = this.state[3];
        long[] jArr = new long[16];
        Decode(jArr, bArr, 64);
        long FF = FF(j, j2, j3, j4, jArr[0], (long) S[0][0], 3614090360L);
        long FF2 = FF(j4, FF, j2, j3, jArr[1], (long) S[0][1], 3905402710L);
        long FF3 = FF(j3, FF2, FF, j2, jArr[2], (long) S[0][2], 606105819);
        long FF4 = FF(j2, FF3, FF2, FF, jArr[3], (long) S[0][3], 3250441966L);
        long FF5 = FF(FF, FF4, FF3, FF2, jArr[4], (long) S[0][0], 4118548399L);
        long FF6 = FF(FF2, FF5, FF4, FF3, jArr[5], (long) S[0][1], 1200080426);
        long FF7 = FF(FF3, FF6, FF5, FF4, jArr[6], (long) S[0][2], 2821735955L);
        long FF8 = FF(FF4, FF7, FF6, FF5, jArr[7], (long) S[0][3], 4249261313L);
        long FF9 = FF(FF5, FF8, FF7, FF6, jArr[8], (long) S[0][0], 1770035416);
        long FF10 = FF(FF6, FF9, FF8, FF7, jArr[9], (long) S[0][1], 2336552879L);
        long FF11 = FF(FF7, FF10, FF9, FF8, jArr[10], (long) S[0][2], 4294925233L);
        long FF12 = FF(FF8, FF11, FF10, FF9, jArr[11], (long) S[0][3], 2304563134L);
        long FF13 = FF(FF9, FF12, FF11, FF10, jArr[12], (long) S[0][0], 1804603682);
        long FF14 = FF(FF10, FF13, FF12, FF11, jArr[13], (long) S[0][1], 4254626195L);
        long FF15 = FF(FF11, FF14, FF13, FF12, jArr[14], (long) S[0][2], 2792965006L);
        long FF16 = FF(FF12, FF15, FF14, FF13, jArr[15], (long) S[0][3], 1236535329);
        long GG = GG(FF13, FF16, FF15, FF14, jArr[1], (long) S[1][0], 4129170786L);
        long GG2 = GG(FF14, GG, FF16, FF15, jArr[6], (long) S[1][1], 3225465664L);
        long GG3 = GG(FF15, GG2, GG, FF16, jArr[11], (long) S[1][2], 643717713);
        long GG4 = GG(FF16, GG3, GG2, GG, jArr[0], (long) S[1][3], 3921069994L);
        long GG5 = GG(GG, GG4, GG3, GG2, jArr[5], (long) S[1][0], 3593408605L);
        long GG6 = GG(GG2, GG5, GG4, GG3, jArr[10], (long) S[1][1], 38016083);
        long GG7 = GG(GG3, GG6, GG5, GG4, jArr[15], (long) S[1][2], 3634488961L);
        long GG8 = GG(GG4, GG7, GG6, GG5, jArr[4], (long) S[1][3], 3889429448L);
        long GG9 = GG(GG5, GG8, GG7, GG6, jArr[9], (long) S[1][0], 568446438);
        long GG10 = GG(GG6, GG9, GG8, GG7, jArr[14], (long) S[1][1], 3275163606L);
        long GG11 = GG(GG7, GG10, GG9, GG8, jArr[3], (long) S[1][2], 4107603335L);
        long GG12 = GG(GG8, GG11, GG10, GG9, jArr[8], (long) S[1][3], 1163531501);
        long GG13 = GG(GG9, GG12, GG11, GG10, jArr[13], (long) S[1][0], 2850285829L);
        long GG14 = GG(GG10, GG13, GG12, GG11, jArr[2], (long) S[1][1], 4243563512L);
        long GG15 = GG(GG11, GG14, GG13, GG12, jArr[7], (long) S[1][2], 1735328473);
        long GG16 = GG(GG12, GG15, GG14, GG13, jArr[12], (long) S[1][3], 2368359562L);
        long HH = HH(GG13, GG16, GG15, GG14, jArr[5], (long) S[2][0], 4294588738L);
        long HH2 = HH(GG14, HH, GG16, GG15, jArr[8], (long) S[2][1], 2272392833L);
        long HH3 = HH(GG15, HH2, HH, GG16, jArr[11], (long) S[2][2], 1839030562);
        long HH4 = HH(GG16, HH3, HH2, HH, jArr[14], (long) S[2][3], 4259657740L);
        long HH5 = HH(HH, HH4, HH3, HH2, jArr[1], (long) S[2][0], 2763975236L);
        long HH6 = HH(HH2, HH5, HH4, HH3, jArr[4], (long) S[2][1], 1272893353);
        long HH7 = HH(HH3, HH6, HH5, HH4, jArr[7], (long) S[2][2], 4139469664L);
        long HH8 = HH(HH4, HH7, HH6, HH5, jArr[10], (long) S[2][3], 3200236656L);
        long HH9 = HH(HH5, HH8, HH7, HH6, jArr[13], (long) S[2][0], 681279174);
        long HH10 = HH(HH6, HH9, HH8, HH7, jArr[0], (long) S[2][1], 3936430074L);
        long HH11 = HH(HH7, HH10, HH9, HH8, jArr[3], (long) S[2][2], 3572445317L);
        long HH12 = HH(HH8, HH11, HH10, HH9, jArr[6], (long) S[2][3], 76029189);
        long HH13 = HH(HH9, HH12, HH11, HH10, jArr[9], (long) S[2][0], 3654602809L);
        long HH14 = HH(HH10, HH13, HH12, HH11, jArr[12], (long) S[2][1], 3873151461L);
        long HH15 = HH(HH11, HH14, HH13, HH12, jArr[15], (long) S[2][2], 530742520);
        long HH16 = HH(HH12, HH15, HH14, HH13, jArr[2], (long) S[2][3], 3299628645L);
        long II = II(HH13, HH16, HH15, HH14, jArr[0], (long) S[3][0], 4096336452L);
        long II2 = II(HH14, II, HH16, HH15, jArr[7], (long) S[3][1], 1126891415);
        long II3 = II(HH15, II2, II, HH16, jArr[14], (long) S[3][2], 2878612391L);
        long II4 = II(HH16, II3, II2, II, jArr[5], (long) S[3][3], 4237533241L);
        long II5 = II(II, II4, II3, II2, jArr[12], (long) S[3][0], 1700485571);
        long II6 = II(II2, II5, II4, II3, jArr[3], (long) S[3][1], 2399980690L);
        long II7 = II(II3, II6, II5, II4, jArr[10], (long) S[3][2], 4293915773L);
        long II8 = II(II4, II7, II6, II5, jArr[1], (long) S[3][3], 2240044497L);
        long II9 = II(II5, II8, II7, II6, jArr[8], (long) S[3][0], 1873313359);
        long II10 = II(II6, II9, II8, II7, jArr[15], (long) S[3][1], 4264355552L);
        long II11 = II(II7, II10, II9, II8, jArr[6], (long) S[3][2], 2734768916L);
        long II12 = II(II8, II11, II10, II9, jArr[13], (long) S[3][3], 1309151649);
        long II13 = II(II9, II12, II11, II10, jArr[4], (long) S[3][0], 4149444226L);
        long II14 = II(II10, II13, II12, II11, jArr[11], (long) S[3][1], 3174756917L);
        long II15 = II(II11, II14, II13, II12, jArr[2], (long) S[3][2], 718787259);
        long II16 = II(II12, II15, II14, II13, jArr[9], (long) S[3][3], 3951481745L);
        long[] jArr2 = this.state;
        jArr2[0] = jArr2[0] + II13;
        long[] jArr3 = this.state;
        jArr3[1] = II16 + jArr3[1];
        long[] jArr4 = this.state;
        jArr4[2] = jArr4[2] + II15;
        long[] jArr5 = this.state;
        jArr5[3] = jArr5[3] + II14;
    }

    private void md5Update(byte[] bArr, int i) {
        int i2 = 0;
        byte[] bArr2 = new byte[64];
        int i3 = ((int) (this.count[0] >>> 3)) & 63;
        long[] jArr = this.count;
        long j = jArr[0] + ((long) (i << 3));
        jArr[0] = j;
        if (j < ((long) (i << 3))) {
            long[] jArr2 = this.count;
            jArr2[1] = jArr2[1] + 1;
        }
        long[] jArr3 = this.count;
        jArr3[1] = jArr3[1] + ((long) (i >>> 29));
        int i4 = 64 - i3;
        if (i >= i4) {
            md5Memcpy(this.buffer, bArr, i3, 0, i4);
            md5Transform(this.buffer);
            while (i4 + 63 < i) {
                md5Memcpy(bArr2, bArr, 0, i4, 64);
                md5Transform(bArr2);
                i4 += 64;
            }
            i3 = 0;
            i2 = i4;
        }
        md5Memcpy(this.buffer, bArr, i3, i2, i - i2);
    }

    public static String strMD5(byte[] bArr) {
        byte[] md5 = toMD5(bArr);
        StringBuffer stringBuffer = new StringBuffer(16);
        for (int i = 0; i < 16; i++) {
            stringBuffer.append(byteHEX(md5[i]));
        }
        return stringBuffer.toString();
    }

    public static byte[] toMD5(String str) {
        return new MD5().getMD5(str);
    }

    public static byte[] toMD5(byte[] bArr) {
        return new MD5().getMD5(bArr);
    }

    public byte[] getMD5(String str) {
        md5Init();
        try {
            byte[] bytes = str.getBytes("ISO8859_1");
            md5Update(bytes, bytes.length);
        } catch (UnsupportedEncodingException e) {
            md5Update(str.getBytes(), str.length());
        }
        md5Final();
        return this.digest;
    }

    public byte[] getMD5(byte[] bArr) {
        md5Init();
        md5Update(bArr, bArr.length);
        md5Final();
        return this.digest;
    }

    public String getMD5ofStr(String str) {
        md5Init();
        try {
            byte[] bytes = str.getBytes("ISO8859_1");
            md5Update(bytes, bytes.length);
        } catch (UnsupportedEncodingException e) {
            md5Update(str.getBytes(), str.length());
        }
        md5Final();
        this.digestHexStr = "";
        for (int i = 0; i < 16; i++) {
            this.digestHexStr += byteHEX(this.digest[i]);
        }
        return this.digestHexStr;
    }
}
