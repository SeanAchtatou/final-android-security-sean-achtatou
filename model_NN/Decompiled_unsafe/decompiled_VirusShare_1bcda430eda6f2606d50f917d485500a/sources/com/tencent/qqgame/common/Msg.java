package com.tencent.qqgame.common;

import com.tencent.qqgame.hall.common.IGameView;
import com.tencent.qqgame.hall.common.SyncData;
import java.io.DataInputStream;
import java.io.IOException;

public class Msg {
    public static final byte CALL_PLAYER_ACTIONID_MULTILOGIN = 1;
    public static final byte CALL_PLAYER_ACTIONID_NOTIFYMSG = 3;
    public static final byte CALL_PLAYER_ACTIONID_SHUTUP = 2;
    public static final short EMP_ERR_FailNotify = 94;
    public static final short EMP_ERR_NOPLATFORM = 81;
    public static final short EMP_Err_BuyItem = 88;
    public static final short EMP_Err_ChangeMoney1 = 89;
    public static final short EMP_Err_ChangeMoney2 = 90;
    public static final short EMP_Err_Fail_GetSignature = 100;
    public static final short EMP_Err_FindNoTable = 86;
    public static final short EMP_Err_InvalidPlayer = 96;
    public static final short EMP_Err_InvalidSeat = 15;
    public static final short EMP_Err_InvalidTableKey = 17;
    public static final short EMP_Err_InvalidUin = 4;
    public static final short EMP_Err_Invalid_Conn = 98;
    public static final short EMP_Err_Invalid_Param = 99;
    public static final short EMP_Err_ItemNo = 87;
    public static final short EMP_Err_MoneyTooLow = 84;
    public static final short EMP_Err_Mutil_Login = 101;
    public static final short EMP_Err_NULL_POINT = 97;
    public static final short EMP_Err_NoSeat = 16;
    public static final short EMP_Err_NoStarLong = 93;
    public static final short EMP_Err_OUTGameTable = 91;
    public static final short EMP_Err_RefuseJoin = 85;
    public static final short EMP_Err_Refused = 102;
    public static final short EMP_Err_RoomFull = 31;
    public static final short EMP_Err_RoomID = 8;
    public static final short EMP_Err_SoftVision1 = 82;
    public static final short EMP_Err_SoftVision2 = 83;
    public static final short EMP_Err_TableFull = 32;
    public static final short EMP_Err_Time_Out = 42;
    public static final short EMP_Err_VerifyFail = 95;
    public static final short EMP_Err_other = 92;
    public static final short EMP_Succeed = 0;
    public static final byte EV_BECANCELD = 15;
    public static final byte EV_BEKICKED = 13;
    public static final byte EV_CANCELVIEW = 8;
    public static final byte EV_ENTER = 1;
    public static final byte EV_GAMECLOSE = 10;
    public static final byte EV_GAMEOPEN = 9;
    public static final byte EV_LEAVE = 2;
    public static final byte EV_LOCKTABLE = 6;
    public static final byte EV_OFFLINE = 16;
    public static final byte EV_RELOGIN = 17;
    public static final byte EV_SCORECHG = 11;
    public static final byte EV_SITDOWN = 3;
    public static final byte EV_STANDUP = 4;
    public static final byte EV_START = 5;
    public static final byte EV_UNLOCKTABLE = 14;
    public static final byte EV_VIEWGAME = 7;
    public static final short KICK_clear = 3;
    public static final short KICK_dull = 2;
    public static final short KICK_multilogin = 1;
    public static final short KICK_nomoney = 1001;
    public static final short KICK_offline = 1000;
    public static final short KICK_upgrade = 0;
    public static final short MSG_ID_CANCELVIEW = 116;
    public static final short MSG_ID_ENTER_ROOM = 402;
    public static final short MSG_ID_GET_LOCATION = 413;
    public static final short MSG_ID_GET_PLATFORM_GAMES_INFO = 503;
    public static final short MSG_ID_GET_SERVER = 502;
    public static final short MSG_ID_HELLO = 500;
    public static final short MSG_ID_KICKPLARYER = 114;
    public static final short MSG_ID_LEAVE_ROOM = 105;
    public static final short MSG_ID_LIST_ROOM = 401;
    public static final short MSG_ID_LIST_TABLE = 408;
    public static final short MSG_ID_LIST_ZONE = 400;
    public static final short MSG_ID_LOGIN = 100;
    public static final short MSG_ID_LOGOUT = 101;
    public static final short MSG_ID_NOTIFY_CALL_PLAYER = 411;
    public static final short MSG_ID_NOTIFY_PLAY = 127;
    public static final short MSG_ID_NOTIFY_ROOM_EVEN = 410;
    public static final short MSG_ID_NOTIFY_SPEEDTEST = 416;
    public static final short MSG_ID_NOTIFY_TABLE_TALK = 129;
    public static final short MSG_ID_PLAY = 123;
    public static final short MSG_ID_QUICK_PLAY = 407;
    public static final short MSG_ID_REPLAY = 136;
    public static final short MSG_ID_RIASE_HAND = 113;
    public static final short MSG_ID_SITDOWN = 111;
    public static final short MSG_ID_STANDUP = 112;
    public static final short MSG_ID_TALK_ON_TABLE = 124;
    public static final short MSG_ID_VERIFYCODE = 415;
    public static final short MSG_ID_VIEWGAME = 115;
    public static final byte MSG_TYPE_ASK = 13;
    public static final byte MSG_TYPE_REQUEST = 10;
    public static short dialogId = -1;
    public static int lastMsgSeqNo = 20100;
    public static short version = 1608;
    public byte flag = 1;
    public short length;
    public short mobileTypeId;
    public byte[] msgData;
    public short msgId;
    public int msgLen;
    public short msgType;
    public byte[] optData;
    public byte optLen;
    public long playerId;
    public int sendIntervalTime;
    public byte sendTimes;
    public int seqNo;
    public byte userTypeId = IGameView.MSG_2HALL_ADJUS_BRIGHT;

    public Msg(byte[] bArr, int i) {
        this.msgData = bArr;
        this.msgLen = i;
        this.userTypeId = SyncData.USER_TYPEID;
    }

    public static Player getPlayerInfo(DataInputStream dataInputStream) {
        short s;
        try {
            long convertUnsigned2Long = Util.convertUnsigned2Long(dataInputStream.readInt());
            String string = Util.getString(dataInputStream, dataInputStream.readShort());
            byte readByte = dataInputStream.readByte();
            short readShort = dataInputStream.readShort();
            short readShort2 = dataInputStream.readShort();
            if (readShort2 % 3 != 0) {
                s = (short) (((short) (readShort2 / 3)) + 86);
            } else {
                short s2 = (short) (readShort2 / 3);
                s = s2 >= 100 ? 0 : s2;
            }
            int readInt = dataInputStream.readInt();
            int readInt2 = dataInputStream.readInt();
            int readInt3 = dataInputStream.readInt();
            short readShort3 = dataInputStream.readShort();
            short readUnsignedByte = (short) dataInputStream.readUnsignedByte();
            short readUnsignedByte2 = (short) dataInputStream.readUnsignedByte();
            dataInputStream.readUnsignedByte();
            return new Player(convertUnsigned2Long, string, readByte, readShort, s, readInt, readInt2, readInt3, readShort3, readUnsignedByte, readUnsignedByte2);
        } catch (IOException e) {
            return null;
        }
    }

    /* JADX WARN: Type inference failed for: r0v25, types: [int, short] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public byte[] encode() {
        /*
            r6 = this;
            r5 = 0
            int r0 = r5 + 2
            short r0 = (short) r0
            int r0 = r0 + 2
            short r0 = (short) r0
            int r0 = r0 + 2
            short r0 = (short) r0
            int r0 = r0 + 2
            short r0 = (short) r0
            int r0 = r0 + 1
            short r0 = (short) r0
            int r0 = r0 + 1
            short r0 = (short) r0
            int r0 = r0 + 4
            short r0 = (short) r0
            int r0 = r0 + 1
            short r0 = (short) r0
            byte r1 = r6.optLen
            int r0 = r0 + r1
            short r0 = (short) r0
            int r0 = r0 + 2
            short r0 = (short) r0
            int r0 = r0 + 2
            short r0 = (short) r0
            int r0 = r0 + 4
            short r0 = (short) r0
            int r1 = r6.msgLen
            int r0 = r0 + r1
            short r0 = (short) r0
            byte[] r1 = new byte[r0]
            com.tencent.qqgame.common.Util.word2Byte(r1, r5, r0)
            int r2 = r5 + 2
            short r3 = com.tencent.qqgame.common.Msg.version
            com.tencent.qqgame.common.Util.word2Byte(r1, r2, r3)
            int r2 = r2 + 2
            short r3 = com.tencent.qqgame.common.Msg.dialogId
            com.tencent.qqgame.common.Util.word2Byte(r1, r2, r3)
            int r2 = r2 + 2
            short r3 = r6.mobileTypeId
            com.tencent.qqgame.common.Util.word2Byte(r1, r2, r3)
            int r2 = r2 + 2
            int r3 = r2 + 1
            byte r4 = r6.userTypeId
            r1[r2] = r4
            int r2 = r3 + 1
            byte r4 = r6.flag
            r1[r3] = r4
            long r3 = r6.playerId
            int r3 = com.tencent.qqgame.common.Util.convert2Unsigned(r3)
            com.tencent.qqgame.common.Util.word2Byte(r1, r2, r3)
            int r2 = r2 + 4
            int r3 = r2 + 1
            byte r4 = r6.optLen
            r1[r2] = r4
            byte r2 = r6.optLen
            if (r2 <= 0) goto L_0x00a4
            byte[] r2 = r6.optData
            byte r4 = r6.optLen
            java.lang.System.arraycopy(r2, r5, r1, r3, r4)
            byte r2 = r6.optLen
            int r2 = r2 + 15
        L_0x0072:
            short r3 = r6.msgId
            com.tencent.qqgame.common.Util.word2Byte(r1, r2, r3)
            int r2 = r2 + 2
            short r3 = r6.msgType
            com.tencent.qqgame.common.Util.word2Byte(r1, r2, r3)
            int r2 = r2 + 2
            int r3 = com.tencent.qqgame.common.Msg.lastMsgSeqNo
            int r3 = r3 + 1
            com.tencent.qqgame.common.Msg.lastMsgSeqNo = r3
            com.tencent.qqgame.common.Util.word2Byte(r1, r2, r3)
            int r2 = r2 + 4
            int r3 = r6.msgLen
            if (r3 <= 0) goto L_0x0099
            byte[] r3 = r6.msgData
            int r4 = r6.msgLen
            java.lang.System.arraycopy(r3, r5, r1, r2, r4)
            int r3 = r6.msgLen
            int r2 = r2 + r3
        L_0x0099:
            if (r0 == r2) goto L_0x00a2
            java.lang.String r0 = "encode error!"
            com.tencent.qqgame.hall.common.Tools.debug(r0)
            r0 = 0
        L_0x00a1:
            return r0
        L_0x00a2:
            r0 = r1
            goto L_0x00a1
        L_0x00a4:
            r2 = r3
            goto L_0x0072
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.qqgame.common.Msg.encode():byte[]");
    }
}
