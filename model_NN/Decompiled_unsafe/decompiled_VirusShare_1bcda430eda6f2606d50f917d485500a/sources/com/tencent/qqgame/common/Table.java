package com.tencent.qqgame.common;

import com.tencent.qqgame.hall.common.SyncData;
import java.util.Vector;

public class Table {
    public static final byte maxPlayerInTable = 3;
    public Vector<Player> playerList;
    public short roomId;
    public String roomName = null;
    public short serverId;
    public short tableId;
    public short zoneId;
    public String zoneName = null;

    public Table(short s, String str, short s2, String str2, short s3, short s4) {
        this.tableId = s4;
        this.zoneId = s;
        this.zoneName = str;
        this.roomId = s2;
        this.roomName = str2;
        this.serverId = s3;
        this.playerList = new Vector<>();
    }

    public Table(short s, short s2, short s3, short s4) {
        this.tableId = s4;
        this.zoneId = s;
        this.roomId = s2;
        this.serverId = s3;
        this.playerList = new Vector<>();
    }

    public static synchronized void addPlayer(Player player, Vector<Player> vector) {
        synchronized (Table.class) {
            if (vector.size() < 3) {
                vector.addElement(player);
            }
        }
    }

    public static byte getOppositeSeatId(byte b, short s) {
        if (b == s) {
            return 1;
        }
        if (SyncData.maxPlayerOfTable == 2) {
            return 0;
        }
        if (SyncData.maxPlayerOfTable == 3) {
            return (b + 1 == s || b - 2 == s) ? (byte) 0 : 2;
        }
        if (SyncData.maxPlayerOfTable != 4) {
            return 10;
        }
        int i = (4 - (b - s)) % 4;
        return (byte) (i == 0 ? 3 : i - 1);
    }

    public static Player getPlayerByAbsoluteSeatId(byte b, Vector<Player> vector) {
        if (vector == null) {
            return null;
        }
        int size = vector.size();
        for (byte b2 = 0; b2 < size; b2 = (byte) (b2 + 1)) {
            if (((byte) vector.elementAt(b2).seatId) == b) {
                return vector.elementAt(b2);
            }
        }
        return null;
    }

    public static Player getPlayerByOppositeSeatId(short s, Vector<Player> vector, short s2) {
        if (vector == null) {
            return null;
        }
        int size = vector.size();
        for (byte b = 0; b < size; b = (byte) (b + 1)) {
            if (getOppositeSeatId((byte) vector.elementAt(b).seatId, s2) == s) {
                return vector.elementAt(b);
            }
        }
        return null;
    }

    public static synchronized void removePlayerByUin(int i, Vector<Player> vector) {
        synchronized (Table.class) {
            int i2 = 0;
            while (true) {
                int i3 = i2;
                if (i3 >= vector.size()) {
                    break;
                } else if (vector.elementAt(i3).uid == ((long) i)) {
                    vector.removeElementAt(i3);
                    break;
                } else {
                    i2 = i3 + 1;
                }
            }
        }
    }

    public void reset(short s, short s2, short s3, short s4) {
        this.tableId = s4;
        this.zoneId = s;
        this.roomId = s2;
        this.serverId = s3;
        this.playerList = new Vector<>();
    }

    public void setTableNameInfo(String str, String str2) {
        this.zoneName = str;
        this.roomName = str2;
    }
}
