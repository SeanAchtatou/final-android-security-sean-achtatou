package com.tencent.qqgame.common;

import android.graphics.Bitmap;
import java.util.Vector;

public class Card {
    public static final byte MAX_MARGIN_H = 42;
    public static final byte MIN_MARGIN_H = 16;
    public byte anim_type = 0;
    public byte color;
    public Bitmap imgCard = null;
    public boolean isFocused = false;
    public boolean isSelected = false;
    public boolean isTouched = false;
    public byte number;
    public byte offset = 16;
    public byte value;

    public Card(byte b) {
        reset(b);
    }

    public static void sortCardList(Vector<Card> vector, byte b) {
        int size = vector.size();
        if (size >= 2) {
            for (byte b2 = 0; b2 < size - 1; b2 = (byte) (b2 + 1)) {
                for (byte b3 = (byte) (size - 1); b3 > b2; b3 = (byte) (b3 - 1)) {
                    if (vector.elementAt(b3).value > vector.elementAt(b3 - 1).value || (vector.elementAt(b3).value == vector.elementAt(b3 - 1).value && vector.elementAt(b3).color < vector.elementAt(b3 - 1).color)) {
                        vector.set(b3, vector.elementAt(b3 - 1));
                        vector.set(b3 - 1, vector.elementAt(b3));
                    }
                }
            }
            if (b == 1) {
                for (byte b4 = 0; b4 < size; b4 = (byte) (b4 + 1)) {
                    Card elementAt = vector.elementAt(b4);
                    if (elementAt.value == 2) {
                        byte b5 = 0;
                        while (true) {
                            if (b5 >= size) {
                                break;
                            } else if ((vector.elementAt(b5).value >= 14 || vector.elementAt(b5).value == 2) && (vector.elementAt(b5).value != 2 || vector.elementAt(b5).color <= elementAt.color)) {
                                b5 = (byte) (b5 + 1);
                            }
                        }
                        vector.remove(b4);
                        vector.insertElementAt(elementAt, b5);
                    } else if (elementAt.value == 1) {
                        byte b6 = 0;
                        while (true) {
                            if (b6 >= size) {
                                break;
                            } else if ((vector.elementAt(b6).value >= 14 || vector.elementAt(b6).value == 2 || vector.elementAt(b6).value == 1) && (vector.elementAt(b6).value != 1 || vector.elementAt(b6).color <= elementAt.color)) {
                                b6 = (byte) (b6 + 1);
                            }
                        }
                        vector.remove(b4);
                        vector.insertElementAt(elementAt, b6);
                    }
                }
            }
        }
    }

    public void reset(byte b) {
        byte b2;
        this.number = b;
        if (b == 54) {
            b2 = 15;
        } else {
            b2 = (byte) (b == 53 ? 14 : b % 13);
        }
        this.value = b2;
        if (this.value == 0) {
            this.value = 13;
        }
        this.color = (byte) (((b - 1) / 13) + 1);
    }
}
