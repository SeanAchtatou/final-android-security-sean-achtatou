package com.tencent.qqgame.common;

public class LordCard {
    public int color;
    public int drawX = 0;
    public int drawY = 0;
    public int index;
    public byte number;
    public boolean selected = false;
    public int value;

    public LordCard(byte b) {
        this.number = b;
        this.value = b == 54 ? 15 : b == 53 ? 14 : b % 13;
        if (this.value == 0) {
            this.value = 13;
        }
        this.color = ((b - 1) / 13) + 1;
    }
}
