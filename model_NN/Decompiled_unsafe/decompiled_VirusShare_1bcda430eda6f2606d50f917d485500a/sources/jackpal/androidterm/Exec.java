package jackpal.androidterm;

import java.io.FileDescriptor;

public final class Exec {
    static {
        System.loadLibrary("androidterm");
    }

    public static native void close(FileDescriptor fileDescriptor);

    public static native FileDescriptor createSubprocess(String str, String str2, String str3, int[] iArr);

    public static native void setPtyWindowSize(FileDescriptor fileDescriptor, int i, int i2, int i3, int i4);

    public static native int waitFor(int i);
}
