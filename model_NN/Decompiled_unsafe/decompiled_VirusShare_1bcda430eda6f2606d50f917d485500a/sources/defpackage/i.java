package defpackage;

import android.content.DialogInterface;
import android.view.KeyEvent;

/* renamed from: i  reason: default package */
class i implements DialogInterface.OnKeyListener {
    final /* synthetic */ n a;

    i(n nVar) {
        this.a = nVar;
    }

    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        this.a.a.f++;
        if (this.a.a.f <= 1) {
            return true;
        }
        this.a.a.f = 0;
        this.a.a.e.dismiss();
        this.a.a.e.show();
        return true;
    }
}
