package defpackage;

import android.content.Context;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;

/* renamed from: e  reason: default package */
public class e {
    public static void a(int i, String str, Context context) {
        InputStream openRawResource = context.getResources().openRawResource(i);
        FileOutputStream openFileOutput = context.openFileOutput(str, 0);
        GZIPInputStream gZIPInputStream = new GZIPInputStream(openRawResource);
        byte[] bArr = new byte[1024];
        while (true) {
            int read = gZIPInputStream.read(bArr);
            if (read < 0) {
                gZIPInputStream.close();
                openFileOutput.getChannel().force(true);
                openFileOutput.flush();
                openFileOutput.close();
                return;
            }
            openFileOutput.write(bArr, 0, read);
        }
    }

    public static void a(FileOutputStream fileOutputStream, String str) {
        fileOutputStream.write((String.valueOf(str) + "\n").getBytes());
        fileOutputStream.flush();
    }

    public static void b(int i, String str, Context context) {
        InputStream openRawResource = context.getResources().openRawResource(i);
        FileOutputStream openFileOutput = context.openFileOutput(str, 0);
        byte[] bArr = new byte[1024];
        while (true) {
            int read = openRawResource.read(bArr);
            if (read < 0) {
                openRawResource.close();
                openFileOutput.getChannel().force(true);
                openFileOutput.flush();
                openFileOutput.close();
                return;
            }
            openFileOutput.write(bArr, 0, read);
        }
    }
}
