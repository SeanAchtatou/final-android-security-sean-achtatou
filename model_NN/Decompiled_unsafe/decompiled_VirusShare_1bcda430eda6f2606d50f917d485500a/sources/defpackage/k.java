package defpackage;

import android.content.DialogInterface;

/* renamed from: k  reason: default package */
class k implements DialogInterface.OnClickListener {
    final /* synthetic */ n a;

    k(n nVar) {
        this.a = nVar;
    }

    public void onClick(DialogInterface dialogInterface, int i) {
        try {
            this.a.a.d.setCancelable(true);
            this.a.a.d.dismiss();
            this.a.a.d.cancel();
        } catch (Exception e) {
        }
    }
}
