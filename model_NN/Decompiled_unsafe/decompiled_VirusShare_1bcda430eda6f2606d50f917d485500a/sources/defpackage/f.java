package defpackage;

import android.content.DialogInterface;
import android.view.KeyEvent;

/* renamed from: f  reason: default package */
class f implements DialogInterface.OnKeyListener {
    final /* synthetic */ n a;

    f(n nVar) {
        this.a = nVar;
    }

    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        this.a.a.f++;
        if (this.a.a.f <= 1) {
            return true;
        }
        this.a.a.f = 0;
        this.a.a.e.dismiss();
        this.a.a.e.show();
        return true;
    }
}
