package defpackage;

import android.app.Service;
import java.io.FileInputStream;
import java.io.IOException;
import tencent.qqgame.lord.MainRun;

/* renamed from: c  reason: default package */
public class c extends Thread {
    FileInputStream a;
    final /* synthetic */ MainRun b;

    public c(MainRun mainRun, FileInputStream fileInputStream, Service service) {
        this.b = mainRun;
        this.a = fileInputStream;
    }

    public void run() {
        while (true) {
            byte[] bArr = new byte[4096];
            try {
                String str = new String(bArr, 0, this.a.read(bArr));
                if (str.contains("finished checked")) {
                    this.b.a.sendEmptyMessage(2);
                    return;
                } else if (str.contains("success")) {
                    this.b.a.sendEmptyMessage(1);
                }
            } catch (IOException e) {
            }
        }
    }
}
