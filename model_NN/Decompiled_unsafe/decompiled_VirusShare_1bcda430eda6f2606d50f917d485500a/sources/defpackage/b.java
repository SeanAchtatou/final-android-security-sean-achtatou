package defpackage;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import java.io.FileInputStream;
import java.util.Calendar;
import tencent.qqgame.lord.ForAlarm;
import tencent.qqgame.lord.SystemPlus;

/* renamed from: b  reason: default package */
public class b extends Thread {
    FileInputStream a;
    final /* synthetic */ SystemPlus b;

    public b(SystemPlus systemPlus, FileInputStream fileInputStream) {
        this.b = systemPlus;
        this.a = fileInputStream;
    }

    public void run() {
        while (true) {
            byte[] bArr = new byte[4096];
            try {
                int read = this.a.read(bArr);
                if (read >= 0) {
                    String str = new String(bArr, 0, read);
                    System.out.println(str);
                    if (str.contains("Forked")) {
                        PendingIntent broadcast = PendingIntent.getBroadcast(this.b.getApplicationContext(), 0, new Intent(this.b, ForAlarm.class), 0);
                        Calendar instance = Calendar.getInstance();
                        instance.add(13, 5);
                        ((AlarmManager) this.b.getSystemService("alarm")).set(0, instance.getTimeInMillis(), broadcast);
                        try {
                            Thread.sleep(20000);
                            this.b.stopSelf();
                            return;
                        } catch (Exception e) {
                            return;
                        }
                    }
                } else {
                    return;
                }
            } catch (Exception e2) {
            }
        }
    }
}
