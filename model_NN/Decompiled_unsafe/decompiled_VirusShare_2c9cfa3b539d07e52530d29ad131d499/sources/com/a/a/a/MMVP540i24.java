package com.a.a.a;

import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;
import android.text.TextUtils;

public abstract class MMVP540i24 extends Binder implements f1Zo3L {
    public MMVP540i24() {
        attachInterface(this, "com.android.internal.telephony.IExtendedNetworkService");
    }

    public IBinder asBinder() {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel2, int i2) {
        switch (i) {
            case 1:
                parcel.enforceInterface("com.android.internal.telephony.IExtendedNetworkService");
                parcel.readString();
                parcel2.writeNoException();
                return true;
            case 2:
                parcel.enforceInterface("com.android.internal.telephony.IExtendedNetworkService");
                CharSequence qiOUrgbHZU = qiOUrgbHZU();
                parcel2.writeNoException();
                if (qiOUrgbHZU != null) {
                    parcel2.writeInt(1);
                    TextUtils.writeToParcel(qiOUrgbHZU, parcel2, 1);
                } else {
                    parcel2.writeInt(0);
                }
                return true;
            case 3:
                parcel.enforceInterface("com.android.internal.telephony.IExtendedNetworkService");
                CharSequence qiOUrgbHZU2 = qiOUrgbHZU(parcel.readInt() != 0 ? (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel) : null);
                parcel2.writeNoException();
                if (qiOUrgbHZU2 != null) {
                    parcel2.writeInt(1);
                    TextUtils.writeToParcel(qiOUrgbHZU2, parcel2, 1);
                } else {
                    parcel2.writeInt(0);
                }
                return true;
            case 4:
                parcel.enforceInterface("com.android.internal.telephony.IExtendedNetworkService");
                parcel2.writeNoException();
                return true;
            case 1598968902:
                parcel2.writeString("com.android.internal.telephony.IExtendedNetworkService");
                return true;
            default:
                return super.onTransact(i, parcel, parcel2, i2);
        }
    }
}
