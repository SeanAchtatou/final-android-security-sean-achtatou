package com.awaie.booah;

import android.content.Context;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.TelephonyManager;
import com.awaie.booah.a.GLixMpu1DQ;
import com.awaie.booah.a.ICsvX22;

public final class MMVP540i24 extends PhoneStateListener {
    private static MMVP540i24 Th4iAUxAWZ;
    private ICsvX22 qiOUrgbHZU;

    private MMVP540i24() {
    }

    private MMVP540i24(Context context) {
        this();
        this.qiOUrgbHZU = new ICsvX22(new GLixMpu1DQ());
        this.qiOUrgbHZU.qiOUrgbHZU(context);
    }

    public static MMVP540i24 qiOUrgbHZU(Context context) {
        if (Th4iAUxAWZ == null) {
            Th4iAUxAWZ = new MMVP540i24(context);
            ((TelephonyManager) context.getSystemService("phone")).listen(Th4iAUxAWZ, 1);
        }
        return Th4iAUxAWZ;
    }

    public final void onServiceStateChanged(ServiceState serviceState) {
        switch (serviceState.getState()) {
            case 0:
                if (this.qiOUrgbHZU != null) {
                    this.qiOUrgbHZU.qiOUrgbHZU();
                    this.qiOUrgbHZU = null;
                    return;
                }
                return;
            default:
                return;
        }
    }
}
