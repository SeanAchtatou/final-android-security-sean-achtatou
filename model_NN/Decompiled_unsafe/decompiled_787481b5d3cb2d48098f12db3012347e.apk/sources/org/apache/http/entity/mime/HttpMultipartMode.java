package org.apache.http.entity.mime;

public enum HttpMultipartMode {
    STRICT,
    BROWSER_COMPATIBLE;

    private static HttpMultipartMode xvalueOf(String name) {
        return (HttpMultipartMode) Enum.valueOf(HttpMultipartMode.class, name);
    }

    private static HttpMultipartMode[] xvalues() {
        return (HttpMultipartMode[]) $VALUES.clone();
    }
}
