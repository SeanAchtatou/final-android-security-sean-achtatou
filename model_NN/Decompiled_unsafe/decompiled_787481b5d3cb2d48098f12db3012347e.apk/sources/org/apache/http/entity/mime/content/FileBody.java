package org.apache.http.entity.mime.content;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.http.annotation.NotThreadSafe;

@NotThreadSafe
public class FileBody extends AbstractContentBody {
    private final File file;

    public String getCharset() {
        return xgetCharset();
    }

    public long getContentLength() {
        return xgetContentLength();
    }

    public File getFile() {
        return xgetFile();
    }

    public String getFilename() {
        return xgetFilename();
    }

    public InputStream getInputStream() {
        return xgetInputStream();
    }

    public String getTransferEncoding() {
        return xgetTransferEncoding();
    }

    public void writeTo(OutputStream outputStream) {
        xwriteTo(outputStream);
    }

    public void writeTo(OutputStream outputStream, int i) {
        xwriteTo(outputStream, i);
    }

    public FileBody(File file2, String mimeType) {
        super(mimeType);
        if (file2 == null) {
            throw new IllegalArgumentException("File may not be null");
        }
        this.file = file2;
    }

    public FileBody(File file2) {
        this(file2, "application/octet-stream");
    }

    private InputStream xgetInputStream() throws IOException {
        return new FileInputStream(this.file);
    }

    @Deprecated
    private void xwriteTo(OutputStream out, int mode) throws IOException {
        writeTo(out);
    }

    private void xwriteTo(OutputStream out) throws IOException {
        if (out == null) {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        InputStream in = new FileInputStream(this.file);
        try {
            byte[] tmp = new byte[4096];
            while (true) {
                int l = in.read(tmp);
                if (l != -1) {
                    out.write(tmp, 0, l);
                } else {
                    out.flush();
                    return;
                }
            }
        } finally {
            in.close();
        }
    }

    private String xgetTransferEncoding() {
        return "binary";
    }

    private String xgetCharset() {
        return null;
    }

    private long xgetContentLength() {
        return this.file.length();
    }

    private String xgetFilename() {
        return this.file.getName();
    }

    private File xgetFile() {
        return this.file;
    }
}
