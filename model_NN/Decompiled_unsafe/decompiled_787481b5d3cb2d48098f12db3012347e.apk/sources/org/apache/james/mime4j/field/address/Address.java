package org.apache.james.mime4j.field.address;

import java.io.Serializable;
import java.io.StringReader;
import java.util.List;
import org.apache.james.mime4j.field.address.parser.AddressListParser;
import org.apache.james.mime4j.field.address.parser.ParseException;

public abstract class Address implements Serializable {
    private static final long serialVersionUID = 634090661990433426L;

    public static Address parse(String str) {
        return xparse(str);
    }

    /* access modifiers changed from: package-private */
    public final void addMailboxesTo(List list) {
        xaddMailboxesTo(list);
    }

    /* access modifiers changed from: protected */
    public abstract void doAddMailboxesTo(List<Mailbox> list);

    public final String getDisplayString() {
        return xgetDisplayString();
    }

    public abstract String getDisplayString(boolean z);

    public abstract String getEncodedString();

    public String toString() {
        return xtoString();
    }

    private final void xaddMailboxesTo(List<Mailbox> results) {
        doAddMailboxesTo(results);
    }

    private final String xgetDisplayString() {
        return getDisplayString(false);
    }

    private static Address xparse(String rawAddressString) {
        try {
            return Builder.getInstance().buildAddress(new AddressListParser(new StringReader(rawAddressString)).parseAddress());
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private String xtoString() {
        return getDisplayString(false);
    }
}
