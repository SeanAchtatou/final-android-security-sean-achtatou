package org.apache.james.mime4j.field.address.parser;

public class SimpleNode extends BaseNode implements Node {
    protected Node[] children;
    protected int id;
    protected Node parent;
    protected AddressListParser parser;

    public Object childrenAccept(AddressListParserVisitor addressListParserVisitor, Object obj) {
        return xchildrenAccept(addressListParserVisitor, obj);
    }

    public void dump(String str) {
        xdump(str);
    }

    public Object jjtAccept(AddressListParserVisitor addressListParserVisitor, Object obj) {
        return xjjtAccept(addressListParserVisitor, obj);
    }

    public void jjtAddChild(Node node, int i) {
        xjjtAddChild(node, i);
    }

    public void jjtClose() {
        xjjtClose();
    }

    public Node jjtGetChild(int i) {
        return xjjtGetChild(i);
    }

    public int jjtGetNumChildren() {
        return xjjtGetNumChildren();
    }

    public Node jjtGetParent() {
        return xjjtGetParent();
    }

    public void jjtOpen() {
        xjjtOpen();
    }

    public void jjtSetParent(Node node) {
        xjjtSetParent(node);
    }

    public String toString() {
        return xtoString();
    }

    public String toString(String str) {
        return xtoString(str);
    }

    public SimpleNode(int i) {
        this.id = i;
    }

    public SimpleNode(AddressListParser p, int i) {
        this(i);
        this.parser = p;
    }

    private void xjjtOpen() {
    }

    private void xjjtClose() {
    }

    private void xjjtSetParent(Node n) {
        this.parent = n;
    }

    private Node xjjtGetParent() {
        return this.parent;
    }

    private void xjjtAddChild(Node n, int i) {
        if (this.children == null) {
            this.children = new Node[(i + 1)];
        } else if (i >= this.children.length) {
            Node[] c = new Node[(i + 1)];
            System.arraycopy(this.children, 0, c, 0, this.children.length);
            this.children = c;
        }
        this.children[i] = n;
    }

    private Node xjjtGetChild(int i) {
        return this.children[i];
    }

    private int xjjtGetNumChildren() {
        if (this.children == null) {
            return 0;
        }
        return this.children.length;
    }

    private Object xjjtAccept(AddressListParserVisitor visitor, Object data) {
        return visitor.visit(this, data);
    }

    private Object xchildrenAccept(AddressListParserVisitor visitor, Object data) {
        if (this.children != null) {
            for (Node jjtAccept : this.children) {
                jjtAccept.jjtAccept(visitor, data);
            }
        }
        return data;
    }

    private String xtoString() {
        return AddressListParserTreeConstants.jjtNodeName[this.id];
    }

    private String xtoString(String prefix) {
        return prefix + toString();
    }

    private void xdump(String prefix) {
        System.out.println(toString(prefix));
        if (this.children != null) {
            for (Node node : this.children) {
                SimpleNode n = (SimpleNode) node;
                if (n != null) {
                    n.dump(prefix + " ");
                }
            }
        }
    }
}
