package org.apache.james.mime4j.field;

import java.io.StringReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.james.mime4j.field.contenttype.parser.ContentTypeParser;
import org.apache.james.mime4j.field.contenttype.parser.ParseException;
import org.apache.james.mime4j.field.contenttype.parser.TokenMgrError;
import org.apache.james.mime4j.util.ByteSequence;

public class ContentTypeField extends AbstractField {
    public static final String PARAM_BOUNDARY = "boundary";
    public static final String PARAM_CHARSET = "charset";
    static final FieldParser PARSER = new FieldParser() {
        public ParsedField parse(String str, String str2, ByteSequence byteSequence) {
            return xparse(str, str2, byteSequence);
        }

        private ParsedField xparse(String name, String body, ByteSequence raw) {
            return new ContentTypeField(name, body, raw);
        }
    };
    public static final String TYPE_MESSAGE_RFC822 = "message/rfc822";
    public static final String TYPE_MULTIPART_DIGEST = "multipart/digest";
    public static final String TYPE_MULTIPART_PREFIX = "multipart/";
    public static final String TYPE_TEXT_PLAIN = "text/plain";
    private static Log log = LogFactory.getLog(ContentTypeField.class);
    private String mimeType = "";
    private Map<String, String> parameters = new HashMap();
    private ParseException parseException;
    private boolean parsed = false;

    public static String getCharset(ContentTypeField contentTypeField) {
        return xgetCharset(contentTypeField);
    }

    public static String getMimeType(ContentTypeField contentTypeField, ContentTypeField contentTypeField2) {
        return xgetMimeType(contentTypeField, contentTypeField2);
    }

    private void parse() {
        xparse();
    }

    public String getBoundary() {
        return xgetBoundary();
    }

    public String getCharset() {
        return xgetCharset();
    }

    public String getMimeType() {
        return xgetMimeType();
    }

    public String getParameter(String str) {
        return xgetParameter(str);
    }

    public Map getParameters() {
        return xgetParameters();
    }

    /* renamed from: getParseException */
    public ParseException xgetParseException() {
        return xgetParseException();
    }

    public boolean isMimeType(String str) {
        return xisMimeType(str);
    }

    public boolean isMultipart() {
        return xisMultipart();
    }

    ContentTypeField(String name, String body, ByteSequence raw) {
        super(name, body, raw);
    }

    private ParseException xgetParseException() {
        if (!this.parsed) {
            parse();
        }
        return this.parseException;
    }

    private String xgetMimeType() {
        if (!this.parsed) {
            parse();
        }
        return this.mimeType;
    }

    private String xgetParameter(String name) {
        if (!this.parsed) {
            parse();
        }
        return this.parameters.get(name.toLowerCase());
    }

    private Map<String, String> xgetParameters() {
        if (!this.parsed) {
            parse();
        }
        return Collections.unmodifiableMap(this.parameters);
    }

    private boolean xisMimeType(String mimeType2) {
        if (!this.parsed) {
            parse();
        }
        return this.mimeType.equalsIgnoreCase(mimeType2);
    }

    private boolean xisMultipart() {
        if (!this.parsed) {
            parse();
        }
        return this.mimeType.startsWith(TYPE_MULTIPART_PREFIX);
    }

    private String xgetBoundary() {
        return getParameter(PARAM_BOUNDARY);
    }

    private String xgetCharset() {
        return getParameter(PARAM_CHARSET);
    }

    private static String xgetMimeType(ContentTypeField child, ContentTypeField parent) {
        if (child != null && child.getMimeType().length() != 0 && (!child.isMultipart() || child.getBoundary() != null)) {
            return child.getMimeType();
        }
        if (parent == null || !parent.isMimeType(TYPE_MULTIPART_DIGEST)) {
            return TYPE_TEXT_PLAIN;
        }
        return TYPE_MESSAGE_RFC822;
    }

    private static String xgetCharset(ContentTypeField f) {
        String charset;
        return (f == null || (charset = f.getCharset()) == null || charset.length() <= 0) ? "us-ascii" : charset;
    }

    private void xparse() {
        String body = getBody();
        ContentTypeParser parser = new ContentTypeParser(new StringReader(body));
        try {
            parser.parseAll();
        } catch (ParseException e) {
            if (log.isDebugEnabled()) {
                log.debug("Parsing value '" + body + "': " + e.getMessage());
            }
            this.parseException = e;
        } catch (TokenMgrError e2) {
            if (log.isDebugEnabled()) {
                log.debug("Parsing value '" + body + "': " + e2.getMessage());
            }
            this.parseException = new ParseException(e2.getMessage());
        }
        String type = parser.getType();
        String subType = parser.getSubType();
        if (!(type == null || subType == null)) {
            this.mimeType = (type + "/" + subType).toLowerCase();
            List<String> paramNames = parser.getParamNames();
            List<String> paramValues = parser.getParamValues();
            if (!(paramNames == null || paramValues == null)) {
                int len = Math.min(paramNames.size(), paramValues.size());
                for (int i = 0; i < len; i++) {
                    this.parameters.put(((String) paramNames.get(i)).toLowerCase(), (String) paramValues.get(i));
                }
            }
        }
        this.parsed = true;
    }
}
