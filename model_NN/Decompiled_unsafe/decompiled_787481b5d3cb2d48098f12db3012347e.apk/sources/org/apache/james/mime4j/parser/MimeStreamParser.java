package org.apache.james.mime4j.parser;

import java.io.IOException;
import java.io.InputStream;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.descriptor.BodyDescriptor;

public class MimeStreamParser {
    private boolean contentDecoding;
    private ContentHandler handler;
    private final MimeTokenStream mimeTokenStream;

    public boolean isContentDecoding() {
        return xisContentDecoding();
    }

    public boolean isRaw() {
        return xisRaw();
    }

    public void parse(InputStream inputStream) {
        xparse(inputStream);
    }

    public void setContentDecoding(boolean z) {
        xsetContentDecoding(z);
    }

    public void setContentHandler(ContentHandler contentHandler) {
        xsetContentHandler(contentHandler);
    }

    public void setRaw(boolean z) {
        xsetRaw(z);
    }

    public void stop() {
        xstop();
    }

    public MimeStreamParser(MimeEntityConfig config) {
        MimeEntityConfig localConfig;
        this.handler = null;
        if (config != null) {
            localConfig = config.clone();
        } else {
            localConfig = new MimeEntityConfig();
        }
        this.mimeTokenStream = new MimeTokenStream(localConfig);
        this.contentDecoding = false;
    }

    public MimeStreamParser() {
        this(null);
    }

    private boolean xisContentDecoding() {
        return this.contentDecoding;
    }

    private void xsetContentDecoding(boolean b) {
        this.contentDecoding = b;
    }

    private void xparse(InputStream is) throws MimeException, IOException {
        InputStream bodyContent;
        this.mimeTokenStream.parse(is);
        while (true) {
            int state = this.mimeTokenStream.getState();
            switch (state) {
                case -1:
                    return;
                case 0:
                    this.handler.startMessage();
                    break;
                case 1:
                    this.handler.endMessage();
                    break;
                case 2:
                    this.handler.raw(this.mimeTokenStream.getInputStream());
                    break;
                case 3:
                    this.handler.startHeader();
                    break;
                case 4:
                    this.handler.field(this.mimeTokenStream.getField());
                    break;
                case 5:
                    this.handler.endHeader();
                    break;
                case 6:
                    this.handler.startMultipart(this.mimeTokenStream.getBodyDescriptor());
                    break;
                case 7:
                    this.handler.endMultipart();
                    break;
                case 8:
                    this.handler.preamble(this.mimeTokenStream.getInputStream());
                    break;
                case 9:
                    this.handler.epilogue(this.mimeTokenStream.getInputStream());
                    break;
                case 10:
                    this.handler.startBodyPart();
                    break;
                case 11:
                    this.handler.endBodyPart();
                    break;
                case 12:
                    BodyDescriptor desc = this.mimeTokenStream.getBodyDescriptor();
                    if (this.contentDecoding) {
                        bodyContent = this.mimeTokenStream.getDecodedInputStream();
                    } else {
                        bodyContent = this.mimeTokenStream.getInputStream();
                    }
                    this.handler.body(desc, bodyContent);
                    break;
                default:
                    throw new IllegalStateException("Invalid state: " + state);
            }
            this.mimeTokenStream.next();
        }
    }

    private boolean xisRaw() {
        return this.mimeTokenStream.isRaw();
    }

    private void xsetRaw(boolean raw) {
        this.mimeTokenStream.setRecursionMode(2);
    }

    private void xstop() {
        this.mimeTokenStream.stop();
    }

    private void xsetContentHandler(ContentHandler h) {
        this.handler = h;
    }
}
