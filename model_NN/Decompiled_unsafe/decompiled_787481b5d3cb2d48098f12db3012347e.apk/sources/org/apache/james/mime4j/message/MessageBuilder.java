package org.apache.james.mime4j.message;

import java.io.IOException;
import java.io.InputStream;
import java.util.Stack;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.codec.Base64InputStream;
import org.apache.james.mime4j.codec.QuotedPrintableInputStream;
import org.apache.james.mime4j.descriptor.BodyDescriptor;
import org.apache.james.mime4j.field.AbstractField;
import org.apache.james.mime4j.parser.ContentHandler;
import org.apache.james.mime4j.parser.Field;
import org.apache.james.mime4j.storage.StorageProvider;
import org.apache.james.mime4j.util.ByteArrayBuffer;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.util.MimeUtil;

public class MessageBuilder implements ContentHandler {
    private final BodyFactory bodyFactory;
    private final Entity entity;
    private Stack<Object> stack = new Stack<>();

    private void expect(Class cls) {
        xexpect(cls);
    }

    private static ByteSequence loadStream(InputStream inputStream) {
        return xloadStream(inputStream);
    }

    public void body(BodyDescriptor bodyDescriptor, InputStream inputStream) {
        xbody(bodyDescriptor, inputStream);
    }

    public void endBodyPart() {
        xendBodyPart();
    }

    public void endHeader() {
        xendHeader();
    }

    public void endMessage() {
        xendMessage();
    }

    public void endMultipart() {
        xendMultipart();
    }

    public void epilogue(InputStream inputStream) {
        xepilogue(inputStream);
    }

    public void field(Field field) {
        xfield(field);
    }

    public void preamble(InputStream inputStream) {
        xpreamble(inputStream);
    }

    public void raw(InputStream inputStream) {
        xraw(inputStream);
    }

    public void startBodyPart() {
        xstartBodyPart();
    }

    public void startHeader() {
        xstartHeader();
    }

    public void startMessage() {
        xstartMessage();
    }

    public void startMultipart(BodyDescriptor bodyDescriptor) {
        xstartMultipart(bodyDescriptor);
    }

    public MessageBuilder(Entity entity2) {
        this.entity = entity2;
        this.bodyFactory = new BodyFactory();
    }

    public MessageBuilder(Entity entity2, StorageProvider storageProvider) {
        this.entity = entity2;
        this.bodyFactory = new BodyFactory(storageProvider);
    }

    private void xexpect(Class<?> c) {
        if (!c.isInstance(this.stack.peek())) {
            throw new IllegalStateException("Internal stack error: Expected '" + c.getName() + "' found '" + this.stack.peek().getClass().getName() + "'");
        }
    }

    private void xstartMessage() throws MimeException {
        if (this.stack.isEmpty()) {
            this.stack.push(this.entity);
            return;
        }
        expect(Entity.class);
        Message m = new Message();
        ((Entity) this.stack.peek()).setBody(m);
        this.stack.push(m);
    }

    private void xendMessage() throws MimeException {
        expect(Message.class);
        this.stack.pop();
    }

    private void xstartHeader() throws MimeException {
        this.stack.push(new Header());
    }

    private void xfield(Field field) throws MimeException {
        expect(Header.class);
        ((Header) this.stack.peek()).addField(AbstractField.parse(field.getRaw()));
    }

    private void xendHeader() throws MimeException {
        expect(Header.class);
        expect(Entity.class);
        ((Entity) this.stack.peek()).setHeader((Header) this.stack.pop());
    }

    private void xstartMultipart(BodyDescriptor bd) throws MimeException {
        expect(Entity.class);
        Multipart multiPart = new Multipart(bd.getSubType());
        ((Entity) this.stack.peek()).setBody(multiPart);
        this.stack.push(multiPart);
    }

    private void xbody(BodyDescriptor bd, InputStream is) throws MimeException, IOException {
        InputStream decodedStream;
        Body body;
        expect(Entity.class);
        String enc = bd.getTransferEncoding();
        if (MimeUtil.ENC_BASE64.equals(enc)) {
            decodedStream = new Base64InputStream(is);
        } else if (MimeUtil.ENC_QUOTED_PRINTABLE.equals(enc)) {
            decodedStream = new QuotedPrintableInputStream(is);
        } else {
            decodedStream = is;
        }
        if (bd.getMimeType().startsWith("text/")) {
            body = this.bodyFactory.textBody(decodedStream, bd.getCharset());
        } else {
            body = this.bodyFactory.binaryBody(decodedStream);
        }
        ((Entity) this.stack.peek()).setBody(body);
    }

    private void xendMultipart() throws MimeException {
        this.stack.pop();
    }

    private void xstartBodyPart() throws MimeException {
        expect(Multipart.class);
        BodyPart bodyPart = new BodyPart();
        ((Multipart) this.stack.peek()).addBodyPart(bodyPart);
        this.stack.push(bodyPart);
    }

    private void xendBodyPart() throws MimeException {
        expect(BodyPart.class);
        this.stack.pop();
    }

    private void xepilogue(InputStream is) throws MimeException, IOException {
        expect(Multipart.class);
        ((Multipart) this.stack.peek()).setEpilogueRaw(loadStream(is));
    }

    private void xpreamble(InputStream is) throws MimeException, IOException {
        expect(Multipart.class);
        ((Multipart) this.stack.peek()).setPreambleRaw(loadStream(is));
    }

    private void xraw(InputStream is) throws MimeException, IOException {
        throw new UnsupportedOperationException("Not supported");
    }

    private static ByteSequence xloadStream(InputStream in) throws IOException {
        ByteArrayBuffer bab = new ByteArrayBuffer(64);
        while (true) {
            int b = in.read();
            if (b == -1) {
                return bab;
            }
            bab.append(b);
        }
    }
}
