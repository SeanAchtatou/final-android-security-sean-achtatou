package org.apache.james.mime4j.field.contentdisposition.parser;

import java.io.InputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class ContentDispositionParser implements ContentDispositionParserConstants {
    private static int[] jj_la1_0;
    private String dispositionType;
    private Vector<int[]> jj_expentries;
    private int[] jj_expentry;
    private int jj_gen;
    SimpleCharStream jj_input_stream;
    private int jj_kind;
    private final int[] jj_la1;
    public Token jj_nt;
    private int jj_ntk;
    private List<String> paramNames;
    private List<String> paramValues;
    public Token token;
    public ContentDispositionParserTokenManager token_source;

    private final Token jj_consume_token(int i) {
        return xjj_consume_token(i);
    }

    private static void jj_la1_0() {
        xjj_la1_0();
    }

    private final int jj_ntk() {
        return xjj_ntk();
    }

    public static void main(String[] strArr) {
        xmain(strArr);
    }

    public void ReInit(InputStream inputStream) {
        xReInit(inputStream);
    }

    public void ReInit(InputStream inputStream, String str) {
        xReInit(inputStream, str);
    }

    public void ReInit(Reader reader) {
        xReInit(reader);
    }

    public void ReInit(ContentDispositionParserTokenManager contentDispositionParserTokenManager) {
        xReInit(contentDispositionParserTokenManager);
    }

    public final void disable_tracing() {
        xdisable_tracing();
    }

    public final void enable_tracing() {
        xenable_tracing();
    }

    public ParseException generateParseException() {
        return xgenerateParseException();
    }

    public String getDispositionType() {
        return xgetDispositionType();
    }

    public final Token getNextToken() {
        return xgetNextToken();
    }

    public List getParamNames() {
        return xgetParamNames();
    }

    public List getParamValues() {
        return xgetParamValues();
    }

    public final Token getToken(int i) {
        return xgetToken(i);
    }

    public final void parameter() {
        xparameter();
    }

    public final void parse() {
        xparse();
    }

    public final void parseAll() {
        xparseAll();
    }

    public final void parseLine() {
        xparseLine();
    }

    public final String value() {
        return xvalue();
    }

    private String xgetDispositionType() {
        return this.dispositionType;
    }

    private List<String> xgetParamNames() {
        return this.paramNames;
    }

    private List<String> xgetParamValues() {
        return this.paramValues;
    }

    private static void xmain(String[] args) throws ParseException {
        while (true) {
            try {
                new ContentDispositionParser(System.in).parseLine();
            } catch (Exception x) {
                x.printStackTrace();
                return;
            }
        }
    }

    private final void xparseLine() throws ParseException {
        parse();
        switch (this.jj_ntk == -1 ? jj_ntk() : this.jj_ntk) {
            case 1:
                jj_consume_token(1);
                break;
            default:
                this.jj_la1[0] = this.jj_gen;
                break;
        }
        jj_consume_token(2);
    }

    private final void xparseAll() throws ParseException {
        parse();
        jj_consume_token(0);
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x000f  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x001e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0021 A[FALL_THROUGH, LOOP:0: B:1:0x000a->B:8:0x0021, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x0016 A[SYNTHETIC] */
    private final void xparse() throws org.apache.james.mime4j.field.contentdisposition.parser.ParseException {
        /*
            r4 = this;
            r1 = 20
            org.apache.james.mime4j.field.contentdisposition.parser.Token r0 = r4.jj_consume_token(r1)
            java.lang.String r1 = r0.image
            r4.dispositionType = r1
        L_0x000a:
            int r1 = r4.jj_ntk
            r2 = -1
            if (r1 != r2) goto L_0x001e
            int r1 = r4.jj_ntk()
        L_0x0013:
            switch(r1) {
                case 3: goto L_0x0021;
                default: goto L_0x0016;
            }
        L_0x0016:
            int[] r1 = r4.jj_la1
            r2 = 1
            int r3 = r4.jj_gen
            r1[r2] = r3
            return
        L_0x001e:
            int r1 = r4.jj_ntk
            goto L_0x0013
        L_0x0021:
            r1 = 3
            r4.jj_consume_token(r1)
            r4.parameter()
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.field.contentdisposition.parser.ContentDispositionParser.xparse():void");
    }

    private final void xparameter() throws ParseException {
        Token attrib = jj_consume_token(20);
        jj_consume_token(4);
        String val = value();
        this.paramNames.add(attrib.image);
        this.paramValues.add(val);
    }

    private final String xvalue() throws ParseException {
        int i;
        Token t;
        if (this.jj_ntk == -1) {
            i = jj_ntk();
        } else {
            i = this.jj_ntk;
        }
        switch (i) {
            case 18:
                t = jj_consume_token(18);
                break;
            case 19:
                t = jj_consume_token(19);
                break;
            case 20:
                t = jj_consume_token(20);
                break;
            default:
                this.jj_la1[2] = this.jj_gen;
                jj_consume_token(-1);
                throw new ParseException();
        }
        return t.image;
    }

    static {
        jj_la1_0();
    }

    private static void xjj_la1_0() {
        jj_la1_0 = new int[]{2, 8, 1835008};
    }

    public ContentDispositionParser(InputStream stream) {
        this(stream, null);
    }

    public ContentDispositionParser(InputStream stream, String encoding) {
        this.paramNames = new ArrayList();
        this.paramValues = new ArrayList();
        this.jj_la1 = new int[3];
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        try {
            this.jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1);
            this.token_source = new ContentDispositionParserTokenManager(this.jj_input_stream);
            this.token = new Token();
            this.jj_ntk = -1;
            this.jj_gen = 0;
            for (int i = 0; i < 3; i++) {
                this.jj_la1[i] = -1;
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private void xReInit(InputStream stream) {
        ReInit(stream, null);
    }

    private void xReInit(InputStream stream, String encoding) {
        try {
            this.jj_input_stream.ReInit(stream, encoding, 1, 1);
            this.token_source.ReInit(this.jj_input_stream);
            this.token = new Token();
            this.jj_ntk = -1;
            this.jj_gen = 0;
            for (int i = 0; i < 3; i++) {
                this.jj_la1[i] = -1;
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public ContentDispositionParser(Reader stream) {
        this.paramNames = new ArrayList();
        this.paramValues = new ArrayList();
        this.jj_la1 = new int[3];
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        this.jj_input_stream = new SimpleCharStream(stream, 1, 1);
        this.token_source = new ContentDispositionParserTokenManager(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; i++) {
            this.jj_la1[i] = -1;
        }
    }

    private void xReInit(Reader stream) {
        this.jj_input_stream.ReInit(stream, 1, 1);
        this.token_source.ReInit(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; i++) {
            this.jj_la1[i] = -1;
        }
    }

    public ContentDispositionParser(ContentDispositionParserTokenManager tm) {
        this.paramNames = new ArrayList();
        this.paramValues = new ArrayList();
        this.jj_la1 = new int[3];
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; i++) {
            this.jj_la1[i] = -1;
        }
    }

    private void xReInit(ContentDispositionParserTokenManager tm) {
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 3; i++) {
            this.jj_la1[i] = -1;
        }
    }

    private final Token xjj_consume_token(int kind) throws ParseException {
        Token oldToken = this.token;
        if (oldToken.next != null) {
            this.token = this.token.next;
        } else {
            Token token2 = this.token;
            Token nextToken = this.token_source.getNextToken();
            token2.next = nextToken;
            this.token = nextToken;
        }
        this.jj_ntk = -1;
        if (this.token.kind == kind) {
            this.jj_gen++;
            return this.token;
        }
        this.token = oldToken;
        this.jj_kind = kind;
        throw generateParseException();
    }

    private final Token xgetNextToken() {
        if (this.token.next != null) {
            this.token = this.token.next;
        } else {
            Token token2 = this.token;
            Token nextToken = this.token_source.getNextToken();
            token2.next = nextToken;
            this.token = nextToken;
        }
        this.jj_ntk = -1;
        this.jj_gen++;
        return this.token;
    }

    private final Token xgetToken(int index) {
        Token t;
        int i = 0;
        Token t2 = this.token;
        while (i < index) {
            if (t2.next != null) {
                t = t2.next;
            } else {
                t = this.token_source.getNextToken();
                t2.next = t;
            }
            i++;
            t2 = t;
        }
        return t2;
    }

    private final int xjj_ntk() {
        Token token2 = this.token.next;
        this.jj_nt = token2;
        if (token2 == null) {
            Token token3 = this.token;
            Token nextToken = this.token_source.getNextToken();
            token3.next = nextToken;
            int i = nextToken.kind;
            this.jj_ntk = i;
            return i;
        }
        int i2 = this.jj_nt.kind;
        this.jj_ntk = i2;
        return i2;
    }

    private ParseException xgenerateParseException() {
        this.jj_expentries.removeAllElements();
        boolean[] la1tokens = new boolean[23];
        for (int i = 0; i < 23; i++) {
            la1tokens[i] = false;
        }
        if (this.jj_kind >= 0) {
            la1tokens[this.jj_kind] = true;
            this.jj_kind = -1;
        }
        for (int i2 = 0; i2 < 3; i2++) {
            if (this.jj_la1[i2] == this.jj_gen) {
                for (int j = 0; j < 32; j++) {
                    if ((jj_la1_0[i2] & (1 << j)) != 0) {
                        la1tokens[j] = true;
                    }
                }
            }
        }
        for (int i3 = 0; i3 < 23; i3++) {
            if (la1tokens[i3]) {
                this.jj_expentry = new int[1];
                this.jj_expentry[0] = i3;
                this.jj_expentries.addElement(this.jj_expentry);
            }
        }
        int[][] exptokseq = new int[this.jj_expentries.size()][];
        for (int i4 = 0; i4 < this.jj_expentries.size(); i4++) {
            exptokseq[i4] = this.jj_expentries.elementAt(i4);
        }
        return new ParseException(this.token, exptokseq, tokenImage);
    }

    private final void xenable_tracing() {
    }

    private final void xdisable_tracing() {
    }
}
