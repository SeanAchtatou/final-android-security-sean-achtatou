package org.apache.james.mime4j.storage;

import java.io.IOException;
import java.io.InputStream;

public class MultiReferenceStorage implements Storage {
    private int referenceCounter;
    private final Storage storage;

    private synchronized boolean decrementCounter() {
        return xdecrementCounter();
    }

    private synchronized void incrementCounter() {
        xincrementCounter();
    }

    public void addReference() {
        xaddReference();
    }

    public void delete() {
        xdelete();
    }

    public InputStream getInputStream() {
        return xgetInputStream();
    }

    public MultiReferenceStorage(Storage storage2) {
        if (storage2 == null) {
            throw new IllegalArgumentException();
        }
        this.storage = storage2;
        this.referenceCounter = 1;
    }

    private void xaddReference() {
        incrementCounter();
    }

    private void xdelete() {
        if (decrementCounter()) {
            this.storage.delete();
        }
    }

    private InputStream xgetInputStream() throws IOException {
        return this.storage.getInputStream();
    }

    private synchronized void xincrementCounter() {
        if (this.referenceCounter == 0) {
            throw new IllegalStateException("storage has been deleted");
        }
        this.referenceCounter++;
    }

    private synchronized boolean xdecrementCounter() {
        int i;
        if (this.referenceCounter == 0) {
            throw new IllegalStateException("storage has been deleted");
        }
        i = this.referenceCounter - 1;
        this.referenceCounter = i;
        return i == 0;
    }
}
