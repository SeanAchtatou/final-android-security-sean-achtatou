package org.apache.james.mime4j.parser;

import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.util.ContentUtil;

class RawField implements Field {
    private String body;
    private int colonIdx;
    private String name;
    private final ByteSequence raw;

    private String parseBody() {
        return xparseBody();
    }

    private String parseName() {
        return xparseName();
    }

    public String getBody() {
        return xgetBody();
    }

    public String getName() {
        return xgetName();
    }

    public ByteSequence getRaw() {
        return xgetRaw();
    }

    public String toString() {
        return xtoString();
    }

    public RawField(ByteSequence raw2, int colonIdx2) {
        this.raw = raw2;
        this.colonIdx = colonIdx2;
    }

    private String xgetName() {
        if (this.name == null) {
            this.name = parseName();
        }
        return this.name;
    }

    private String xgetBody() {
        if (this.body == null) {
            this.body = parseBody();
        }
        return this.body;
    }

    private ByteSequence xgetRaw() {
        return this.raw;
    }

    private String xtoString() {
        return getName() + ':' + getBody();
    }

    private String xparseName() {
        return ContentUtil.decode(this.raw, 0, this.colonIdx);
    }

    private String xparseBody() {
        int offset = this.colonIdx + 1;
        return ContentUtil.decode(this.raw, offset, this.raw.length() - offset);
    }
}
