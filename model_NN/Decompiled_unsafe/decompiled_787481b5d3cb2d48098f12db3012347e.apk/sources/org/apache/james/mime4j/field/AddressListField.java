package org.apache.james.mime4j.field;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.james.mime4j.field.address.AddressList;
import org.apache.james.mime4j.field.address.parser.ParseException;
import org.apache.james.mime4j.util.ByteSequence;

public class AddressListField extends AbstractField {
    static final FieldParser PARSER = new FieldParser() {
        public ParsedField parse(String str, String str2, ByteSequence byteSequence) {
            return xparse(str, str2, byteSequence);
        }

        private ParsedField xparse(String name, String body, ByteSequence raw) {
            return new AddressListField(name, body, raw);
        }
    };
    private static Log log = LogFactory.getLog(AddressListField.class);
    private AddressList addressList;
    private ParseException parseException;
    private boolean parsed = false;

    private void parse() {
        xparse();
    }

    public AddressList getAddressList() {
        return xgetAddressList();
    }

    /* renamed from: getParseException */
    public ParseException xgetParseException() {
        return xgetParseException();
    }

    AddressListField(String name, String body, ByteSequence raw) {
        super(name, body, raw);
    }

    private AddressList xgetAddressList() {
        if (!this.parsed) {
            parse();
        }
        return this.addressList;
    }

    private ParseException xgetParseException() {
        if (!this.parsed) {
            parse();
        }
        return this.parseException;
    }

    private void xparse() {
        String body = getBody();
        try {
            this.addressList = AddressList.parse(body);
        } catch (ParseException e) {
            if (log.isDebugEnabled()) {
                log.debug("Parsing value '" + body + "': " + e.getMessage());
            }
            this.parseException = e;
        }
        this.parsed = true;
    }
}
