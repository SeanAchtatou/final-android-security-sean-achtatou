package org.apache.james.mime4j.message;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.TimeZone;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.MimeIOException;
import org.apache.james.mime4j.field.AddressListField;
import org.apache.james.mime4j.field.DateTimeField;
import org.apache.james.mime4j.field.FieldName;
import org.apache.james.mime4j.field.Fields;
import org.apache.james.mime4j.field.MailboxField;
import org.apache.james.mime4j.field.MailboxListField;
import org.apache.james.mime4j.field.UnstructuredField;
import org.apache.james.mime4j.field.address.Address;
import org.apache.james.mime4j.field.address.AddressList;
import org.apache.james.mime4j.field.address.Mailbox;
import org.apache.james.mime4j.field.address.MailboxList;
import org.apache.james.mime4j.parser.Field;
import org.apache.james.mime4j.parser.MimeEntityConfig;
import org.apache.james.mime4j.parser.MimeStreamParser;
import org.apache.james.mime4j.storage.DefaultStorageProvider;
import org.apache.james.mime4j.storage.StorageProvider;

public class Message extends Entity implements Body {
    private AddressList getAddressList(String str) {
        return xgetAddressList(str);
    }

    private Mailbox getMailbox(String str) {
        return xgetMailbox(str);
    }

    private MailboxList getMailboxList(String str) {
        return xgetMailboxList(str);
    }

    private void setAddressList(String str, Collection collection) {
        xsetAddressList(str, collection);
    }

    private void setAddressList(String str, Address address) {
        xsetAddressList(str, address);
    }

    private void setAddressList(String str, Address... addressArr) {
        xsetAddressList(str, addressArr);
    }

    private void setMailbox(String str, Mailbox mailbox) {
        xsetMailbox(str, mailbox);
    }

    private void setMailboxList(String str, Collection collection) {
        xsetMailboxList(str, collection);
    }

    private void setMailboxList(String str, Mailbox mailbox) {
        xsetMailboxList(str, mailbox);
    }

    private void setMailboxList(String str, Mailbox... mailboxArr) {
        xsetMailboxList(str, mailboxArr);
    }

    public void createMessageId(String str) {
        xcreateMessageId(str);
    }

    public AddressList getBcc() {
        return xgetBcc();
    }

    public AddressList getCc() {
        return xgetCc();
    }

    public Date getDate() {
        return xgetDate();
    }

    public MailboxList getFrom() {
        return xgetFrom();
    }

    public String getMessageId() {
        return xgetMessageId();
    }

    public AddressList getReplyTo() {
        return xgetReplyTo();
    }

    public Mailbox getSender() {
        return xgetSender();
    }

    public String getSubject() {
        return xgetSubject();
    }

    public AddressList getTo() {
        return xgetTo();
    }

    public void setBcc(Collection collection) {
        xsetBcc(collection);
    }

    public void setBcc(Address address) {
        xsetBcc(address);
    }

    public void setBcc(Address... addressArr) {
        xsetBcc(addressArr);
    }

    public void setCc(Collection collection) {
        xsetCc(collection);
    }

    public void setCc(Address address) {
        xsetCc(address);
    }

    public void setCc(Address... addressArr) {
        xsetCc(addressArr);
    }

    public void setDate(Date date) {
        xsetDate(date);
    }

    public void setDate(Date date, TimeZone timeZone) {
        xsetDate(date, timeZone);
    }

    public void setFrom(Collection collection) {
        xsetFrom(collection);
    }

    public void setFrom(Mailbox mailbox) {
        xsetFrom(mailbox);
    }

    public void setFrom(Mailbox... mailboxArr) {
        xsetFrom(mailboxArr);
    }

    public void setReplyTo(Collection collection) {
        xsetReplyTo(collection);
    }

    public void setReplyTo(Address address) {
        xsetReplyTo(address);
    }

    public void setReplyTo(Address... addressArr) {
        xsetReplyTo(addressArr);
    }

    public void setSender(Mailbox mailbox) {
        xsetSender(mailbox);
    }

    public void setSubject(String str) {
        xsetSubject(str);
    }

    public void setTo(Collection collection) {
        xsetTo(collection);
    }

    public void setTo(Address address) {
        xsetTo(address);
    }

    public void setTo(Address... addressArr) {
        xsetTo(addressArr);
    }

    public void writeTo(OutputStream outputStream) {
        xwriteTo(outputStream);
    }

    public Message() {
    }

    public Message(Message other) {
        super(other);
    }

    public Message(InputStream is) throws IOException, MimeIOException {
        this(is, null, DefaultStorageProvider.getInstance());
    }

    public Message(InputStream is, MimeEntityConfig config) throws IOException, MimeIOException {
        this(is, config, DefaultStorageProvider.getInstance());
    }

    public Message(InputStream is, MimeEntityConfig config, StorageProvider storageProvider) throws IOException, MimeIOException {
        try {
            MimeStreamParser parser = new MimeStreamParser(config);
            parser.setContentHandler(new MessageBuilder(this, storageProvider));
            parser.parse(is);
        } catch (MimeException e) {
            throw new MimeIOException(e);
        }
    }

    private void xwriteTo(OutputStream out) throws IOException {
        MessageWriter.DEFAULT.writeEntity(this, out);
    }

    private String xgetMessageId() {
        Field field = obtainField(FieldName.MESSAGE_ID);
        if (field == null) {
            return null;
        }
        return field.getBody();
    }

    private void xcreateMessageId(String hostname) {
        obtainHeader().setField(Fields.messageId(hostname));
    }

    private String xgetSubject() {
        UnstructuredField field = (UnstructuredField) obtainField(FieldName.SUBJECT);
        if (field == null) {
            return null;
        }
        return field.getValue();
    }

    private void xsetSubject(String subject) {
        Header header = obtainHeader();
        if (subject == null) {
            header.removeFields(FieldName.SUBJECT);
        } else {
            header.setField(Fields.subject(subject));
        }
    }

    private Date xgetDate() {
        DateTimeField dateField = (DateTimeField) obtainField(FieldName.DATE);
        if (dateField == null) {
            return null;
        }
        return dateField.getDate();
    }

    private void xsetDate(Date date) {
        setDate(date, null);
    }

    private void xsetDate(Date date, TimeZone zone) {
        Header header = obtainHeader();
        if (date == null) {
            header.removeFields(FieldName.DATE);
        } else {
            header.setField(Fields.date(FieldName.DATE, date, zone));
        }
    }

    private Mailbox xgetSender() {
        return getMailbox(FieldName.SENDER);
    }

    private void xsetSender(Mailbox sender) {
        setMailbox(FieldName.SENDER, sender);
    }

    private MailboxList xgetFrom() {
        return getMailboxList(FieldName.FROM);
    }

    private void xsetFrom(Mailbox from) {
        setMailboxList(FieldName.FROM, from);
    }

    private void xsetFrom(Mailbox... from) {
        setMailboxList(FieldName.FROM, from);
    }

    private void xsetFrom(Collection<Mailbox> from) {
        setMailboxList(FieldName.FROM, from);
    }

    private AddressList xgetTo() {
        return getAddressList(FieldName.TO);
    }

    private void xsetTo(Address to) {
        setAddressList(FieldName.TO, to);
    }

    private void xsetTo(Address... to) {
        setAddressList(FieldName.TO, to);
    }

    private void xsetTo(Collection<Address> to) {
        setAddressList(FieldName.TO, to);
    }

    private AddressList xgetCc() {
        return getAddressList(FieldName.CC);
    }

    private void xsetCc(Address cc) {
        setAddressList(FieldName.CC, cc);
    }

    private void xsetCc(Address... cc) {
        setAddressList(FieldName.CC, cc);
    }

    private void xsetCc(Collection<Address> cc) {
        setAddressList(FieldName.CC, cc);
    }

    private AddressList xgetBcc() {
        return getAddressList(FieldName.BCC);
    }

    private void xsetBcc(Address bcc) {
        setAddressList(FieldName.BCC, bcc);
    }

    private void xsetBcc(Address... bcc) {
        setAddressList(FieldName.BCC, bcc);
    }

    private void xsetBcc(Collection<Address> bcc) {
        setAddressList(FieldName.BCC, bcc);
    }

    private AddressList xgetReplyTo() {
        return getAddressList(FieldName.REPLY_TO);
    }

    private void xsetReplyTo(Address replyTo) {
        setAddressList(FieldName.REPLY_TO, replyTo);
    }

    private void xsetReplyTo(Address... replyTo) {
        setAddressList(FieldName.REPLY_TO, replyTo);
    }

    private void xsetReplyTo(Collection<Address> replyTo) {
        setAddressList(FieldName.REPLY_TO, replyTo);
    }

    private Mailbox xgetMailbox(String fieldName) {
        MailboxField field = (MailboxField) obtainField(fieldName);
        if (field == null) {
            return null;
        }
        return field.getMailbox();
    }

    private void xsetMailbox(String fieldName, Mailbox mailbox) {
        Header header = obtainHeader();
        if (mailbox == null) {
            header.removeFields(fieldName);
        } else {
            header.setField(Fields.mailbox(fieldName, mailbox));
        }
    }

    private MailboxList xgetMailboxList(String fieldName) {
        MailboxListField field = (MailboxListField) obtainField(fieldName);
        if (field == null) {
            return null;
        }
        return field.getMailboxList();
    }

    private void xsetMailboxList(String fieldName, Mailbox mailbox) {
        setMailboxList(fieldName, mailbox == null ? null : Collections.singleton(mailbox));
    }

    private void xsetMailboxList(String fieldName, Mailbox... mailboxes) {
        setMailboxList(fieldName, mailboxes == null ? null : Arrays.asList(mailboxes));
    }

    private void xsetMailboxList(String fieldName, Collection<Mailbox> mailboxes) {
        Header header = obtainHeader();
        if (mailboxes == null || mailboxes.isEmpty()) {
            header.removeFields(fieldName);
        } else {
            header.setField(Fields.mailboxList(fieldName, mailboxes));
        }
    }

    private AddressList xgetAddressList(String fieldName) {
        AddressListField field = (AddressListField) obtainField(fieldName);
        if (field == null) {
            return null;
        }
        return field.getAddressList();
    }

    private void xsetAddressList(String fieldName, Address address) {
        setAddressList(fieldName, address == null ? null : Collections.singleton(address));
    }

    private void xsetAddressList(String fieldName, Address... addresses) {
        setAddressList(fieldName, addresses == null ? null : Arrays.asList(addresses));
    }

    private void xsetAddressList(String fieldName, Collection<Address> addresses) {
        Header header = obtainHeader();
        if (addresses == null || addresses.isEmpty()) {
            header.removeFields(fieldName);
        } else {
            header.setField(Fields.addressList(fieldName, addresses));
        }
    }
}
