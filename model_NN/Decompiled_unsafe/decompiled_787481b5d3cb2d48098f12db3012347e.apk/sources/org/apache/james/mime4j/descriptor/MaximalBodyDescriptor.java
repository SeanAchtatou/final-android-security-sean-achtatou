package org.apache.james.mime4j.descriptor;

import java.io.StringReader;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.field.datetime.DateTime;
import org.apache.james.mime4j.field.datetime.parser.DateTimeParser;
import org.apache.james.mime4j.field.datetime.parser.ParseException;
import org.apache.james.mime4j.field.language.parser.ContentLanguageParser;
import org.apache.james.mime4j.field.mimeversion.parser.MimeVersionParser;
import org.apache.james.mime4j.field.structured.parser.StructuredFieldParser;
import org.apache.james.mime4j.parser.Field;
import org.apache.james.mime4j.util.MimeUtil;

public class MaximalBodyDescriptor extends DefaultBodyDescriptor {
    private static final int DEFAULT_MAJOR_VERSION = 1;
    private static final int DEFAULT_MINOR_VERSION = 0;
    private String contentDescription;
    private DateTime contentDispositionCreationDate;
    private MimeException contentDispositionCreationDateParseException;
    private DateTime contentDispositionModificationDate;
    private MimeException contentDispositionModificationDateParseException;
    private Map<String, String> contentDispositionParameters;
    private DateTime contentDispositionReadDate;
    private MimeException contentDispositionReadDateParseException;
    private long contentDispositionSize;
    private MimeException contentDispositionSizeParseException;
    private String contentDispositionType;
    private String contentId;
    private List<String> contentLanguage;
    private MimeException contentLanguageParseException;
    private String contentLocation;
    private MimeException contentLocationParseException;
    private String contentMD5Raw;
    private boolean isContentDescriptionSet;
    private boolean isContentDispositionSet;
    private boolean isContentIdSet;
    private boolean isContentLanguageSet;
    private boolean isContentLocationSet;
    private boolean isContentMD5Set;
    private boolean isMimeVersionSet;
    private int mimeMajorVersion;
    private int mimeMinorVersion;
    private MimeException mimeVersionException;

    private void parseContentDescription(String str) {
        xparseContentDescription(str);
    }

    private void parseContentDisposition(String str) {
        xparseContentDisposition(str);
    }

    private void parseContentId(String str) {
        xparseContentId(str);
    }

    private DateTime parseDate(String str) {
        return xparseDate(str);
    }

    private void parseLanguage(String str) {
        xparseLanguage(str);
    }

    private void parseLocation(String str) {
        xparseLocation(str);
    }

    private void parseMD5(String str) {
        xparseMD5(str);
    }

    private void parseMimeVersion(String str) {
        xparseMimeVersion(str);
    }

    public void addField(Field field) {
        xaddField(field);
    }

    public String getContentDescription() {
        return xgetContentDescription();
    }

    public DateTime getContentDispositionCreationDate() {
        return xgetContentDispositionCreationDate();
    }

    public MimeException getContentDispositionCreationDateParseException() {
        return xgetContentDispositionCreationDateParseException();
    }

    public String getContentDispositionFilename() {
        return xgetContentDispositionFilename();
    }

    public DateTime getContentDispositionModificationDate() {
        return xgetContentDispositionModificationDate();
    }

    public MimeException getContentDispositionModificationDateParseException() {
        return xgetContentDispositionModificationDateParseException();
    }

    public Map getContentDispositionParameters() {
        return xgetContentDispositionParameters();
    }

    public DateTime getContentDispositionReadDate() {
        return xgetContentDispositionReadDate();
    }

    public MimeException getContentDispositionReadDateParseException() {
        return xgetContentDispositionReadDateParseException();
    }

    public long getContentDispositionSize() {
        return xgetContentDispositionSize();
    }

    public MimeException getContentDispositionSizeParseException() {
        return xgetContentDispositionSizeParseException();
    }

    public String getContentDispositionType() {
        return xgetContentDispositionType();
    }

    public String getContentId() {
        return xgetContentId();
    }

    public List getContentLanguage() {
        return xgetContentLanguage();
    }

    public MimeException getContentLanguageParseException() {
        return xgetContentLanguageParseException();
    }

    public String getContentLocation() {
        return xgetContentLocation();
    }

    public MimeException getContentLocationParseException() {
        return xgetContentLocationParseException();
    }

    public String getContentMD5Raw() {
        return xgetContentMD5Raw();
    }

    public int getMimeMajorVersion() {
        return xgetMimeMajorVersion();
    }

    public int getMimeMinorVersion() {
        return xgetMimeMinorVersion();
    }

    public MimeException getMimeVersionParseException() {
        return xgetMimeVersionParseException();
    }

    protected MaximalBodyDescriptor() {
        this(null);
    }

    public MaximalBodyDescriptor(BodyDescriptor parent) {
        super(parent);
        this.isMimeVersionSet = false;
        this.mimeMajorVersion = 1;
        this.mimeMinorVersion = 0;
        this.contentId = null;
        this.isContentIdSet = false;
        this.contentDescription = null;
        this.isContentDescriptionSet = false;
        this.contentDispositionType = null;
        this.contentDispositionParameters = Collections.emptyMap();
        this.contentDispositionModificationDate = null;
        this.contentDispositionModificationDateParseException = null;
        this.contentDispositionCreationDate = null;
        this.contentDispositionCreationDateParseException = null;
        this.contentDispositionReadDate = null;
        this.contentDispositionReadDateParseException = null;
        this.contentDispositionSize = -1;
        this.contentDispositionSizeParseException = null;
        this.isContentDispositionSet = false;
        this.contentLanguage = null;
        this.contentLanguageParseException = null;
        this.isContentIdSet = false;
        this.contentLocation = null;
        this.contentLocationParseException = null;
        this.isContentLocationSet = false;
        this.contentMD5Raw = null;
        this.isContentMD5Set = false;
    }

    private void xaddField(Field field) {
        String name = field.getName();
        String value = field.getBody();
        String name2 = name.trim().toLowerCase();
        if (MimeUtil.MIME_HEADER_MIME_VERSION.equals(name2) && !this.isMimeVersionSet) {
            parseMimeVersion(value);
        } else if (MimeUtil.MIME_HEADER_CONTENT_ID.equals(name2) && !this.isContentIdSet) {
            parseContentId(value);
        } else if (MimeUtil.MIME_HEADER_CONTENT_DESCRIPTION.equals(name2) && !this.isContentDescriptionSet) {
            parseContentDescription(value);
        } else if (MimeUtil.MIME_HEADER_CONTENT_DISPOSITION.equals(name2) && !this.isContentDispositionSet) {
            parseContentDisposition(value);
        } else if (MimeUtil.MIME_HEADER_LANGAUGE.equals(name2) && !this.isContentLanguageSet) {
            parseLanguage(value);
        } else if (MimeUtil.MIME_HEADER_LOCATION.equals(name2) && !this.isContentLocationSet) {
            parseLocation(value);
        } else if (!MimeUtil.MIME_HEADER_MD5.equals(name2) || this.isContentMD5Set) {
            super.addField(field);
        } else {
            parseMD5(value);
        }
    }

    private void xparseMD5(String value) {
        this.isContentMD5Set = true;
        if (value != null) {
            this.contentMD5Raw = value.trim();
        }
    }

    private void xparseLocation(String value) {
        this.isContentLocationSet = true;
        if (value != null) {
            StructuredFieldParser parser = new StructuredFieldParser(new StringReader(value));
            parser.setFoldingPreserved(false);
            try {
                this.contentLocation = parser.parse();
            } catch (MimeException e) {
                this.contentLocationParseException = e;
            }
        }
    }

    private void xparseLanguage(String value) {
        this.isContentLanguageSet = true;
        if (value != null) {
            try {
                this.contentLanguage = new ContentLanguageParser(new StringReader(value)).parse();
            } catch (MimeException e) {
                this.contentLanguageParseException = e;
            }
        }
    }

    private void xparseContentDisposition(String value) {
        this.isContentDispositionSet = true;
        this.contentDispositionParameters = MimeUtil.getHeaderParams(value);
        this.contentDispositionType = this.contentDispositionParameters.get("");
        String contentDispositionModificationDate2 = this.contentDispositionParameters.get("modification-date");
        if (contentDispositionModificationDate2 != null) {
            try {
                this.contentDispositionModificationDate = parseDate(contentDispositionModificationDate2);
            } catch (ParseException e) {
                this.contentDispositionModificationDateParseException = e;
            }
        }
        String contentDispositionCreationDate2 = this.contentDispositionParameters.get("creation-date");
        if (contentDispositionCreationDate2 != null) {
            try {
                this.contentDispositionCreationDate = parseDate(contentDispositionCreationDate2);
            } catch (ParseException e2) {
                this.contentDispositionCreationDateParseException = e2;
            }
        }
        String contentDispositionReadDate2 = this.contentDispositionParameters.get("read-date");
        if (contentDispositionReadDate2 != null) {
            try {
                this.contentDispositionReadDate = parseDate(contentDispositionReadDate2);
            } catch (ParseException e3) {
                this.contentDispositionReadDateParseException = e3;
            }
        }
        String size = this.contentDispositionParameters.get("size");
        if (size != null) {
            try {
                this.contentDispositionSize = Long.parseLong(size);
            } catch (NumberFormatException e4) {
                this.contentDispositionSizeParseException = (MimeException) new MimeException(e4.getMessage(), e4).fillInStackTrace();
            }
        }
        this.contentDispositionParameters.remove("");
    }

    private DateTime xparseDate(String date) throws ParseException {
        return new DateTimeParser(new StringReader(date)).date_time();
    }

    private void xparseContentDescription(String value) {
        if (value == null) {
            this.contentDescription = "";
        } else {
            this.contentDescription = value.trim();
        }
        this.isContentDescriptionSet = true;
    }

    private void xparseContentId(String value) {
        if (value == null) {
            this.contentId = "";
        } else {
            this.contentId = value.trim();
        }
        this.isContentIdSet = true;
    }

    private void xparseMimeVersion(String value) {
        MimeVersionParser parser = new MimeVersionParser(new StringReader(value));
        try {
            parser.parse();
            int major = parser.getMajorVersion();
            if (major != -1) {
                this.mimeMajorVersion = major;
            }
            int minor = parser.getMinorVersion();
            if (minor != -1) {
                this.mimeMinorVersion = minor;
            }
        } catch (MimeException e) {
            this.mimeVersionException = e;
        }
        this.isMimeVersionSet = true;
    }

    private int xgetMimeMajorVersion() {
        return this.mimeMajorVersion;
    }

    private int xgetMimeMinorVersion() {
        return this.mimeMinorVersion;
    }

    private MimeException xgetMimeVersionParseException() {
        return this.mimeVersionException;
    }

    private String xgetContentDescription() {
        return this.contentDescription;
    }

    private String xgetContentId() {
        return this.contentId;
    }

    private String xgetContentDispositionType() {
        return this.contentDispositionType;
    }

    private Map<String, String> xgetContentDispositionParameters() {
        return this.contentDispositionParameters;
    }

    private String xgetContentDispositionFilename() {
        return this.contentDispositionParameters.get("filename");
    }

    private DateTime xgetContentDispositionModificationDate() {
        return this.contentDispositionModificationDate;
    }

    private MimeException xgetContentDispositionModificationDateParseException() {
        return this.contentDispositionModificationDateParseException;
    }

    private DateTime xgetContentDispositionCreationDate() {
        return this.contentDispositionCreationDate;
    }

    private MimeException xgetContentDispositionCreationDateParseException() {
        return this.contentDispositionCreationDateParseException;
    }

    private DateTime xgetContentDispositionReadDate() {
        return this.contentDispositionReadDate;
    }

    private MimeException xgetContentDispositionReadDateParseException() {
        return this.contentDispositionReadDateParseException;
    }

    private long xgetContentDispositionSize() {
        return this.contentDispositionSize;
    }

    private MimeException xgetContentDispositionSizeParseException() {
        return this.contentDispositionSizeParseException;
    }

    private List<String> xgetContentLanguage() {
        return this.contentLanguage;
    }

    private MimeException xgetContentLanguageParseException() {
        return this.contentLanguageParseException;
    }

    private String xgetContentLocation() {
        return this.contentLocation;
    }

    private MimeException xgetContentLocationParseException() {
        return this.contentLocationParseException;
    }

    private String xgetContentMD5Raw() {
        return this.contentMD5Raw;
    }
}
