package org.apache.james.mime4j.parser;

public final class MimeEntityConfig implements Cloneable {
    private boolean countLineNumbers = false;
    private long maxContentLen = -1;
    private int maxHeaderCount = 1000;
    private int maxLineLen = 1000;
    private boolean maximalBodyDescriptor = false;
    private boolean strictParsing = false;

    /* renamed from: clone */
    public MimeEntityConfig xclone() {
        return xclone();
    }

    public long getMaxContentLen() {
        return xgetMaxContentLen();
    }

    public int getMaxHeaderCount() {
        return xgetMaxHeaderCount();
    }

    public int getMaxLineLen() {
        return xgetMaxLineLen();
    }

    public boolean isCountLineNumbers() {
        return xisCountLineNumbers();
    }

    public boolean isMaximalBodyDescriptor() {
        return xisMaximalBodyDescriptor();
    }

    public boolean isStrictParsing() {
        return xisStrictParsing();
    }

    public void setCountLineNumbers(boolean z) {
        xsetCountLineNumbers(z);
    }

    public void setMaxContentLen(long j) {
        xsetMaxContentLen(j);
    }

    public void setMaxHeaderCount(int i) {
        xsetMaxHeaderCount(i);
    }

    public void setMaxLineLen(int i) {
        xsetMaxLineLen(i);
    }

    public void setMaximalBodyDescriptor(boolean z) {
        xsetMaximalBodyDescriptor(z);
    }

    public void setStrictParsing(boolean z) {
        xsetStrictParsing(z);
    }

    public String toString() {
        return xtoString();
    }

    private boolean xisMaximalBodyDescriptor() {
        return this.maximalBodyDescriptor;
    }

    private void xsetMaximalBodyDescriptor(boolean maximalBodyDescriptor2) {
        this.maximalBodyDescriptor = maximalBodyDescriptor2;
    }

    private void xsetStrictParsing(boolean strictParsing2) {
        this.strictParsing = strictParsing2;
    }

    private boolean xisStrictParsing() {
        return this.strictParsing;
    }

    private void xsetMaxLineLen(int maxLineLen2) {
        this.maxLineLen = maxLineLen2;
    }

    private int xgetMaxLineLen() {
        return this.maxLineLen;
    }

    private void xsetMaxHeaderCount(int maxHeaderCount2) {
        this.maxHeaderCount = maxHeaderCount2;
    }

    private int xgetMaxHeaderCount() {
        return this.maxHeaderCount;
    }

    private void xsetMaxContentLen(long maxContentLen2) {
        this.maxContentLen = maxContentLen2;
    }

    private long xgetMaxContentLen() {
        return this.maxContentLen;
    }

    private void xsetCountLineNumbers(boolean countLineNumbers2) {
        this.countLineNumbers = countLineNumbers2;
    }

    private boolean xisCountLineNumbers() {
        return this.countLineNumbers;
    }

    private MimeEntityConfig xclone() {
        try {
            return (MimeEntityConfig) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new InternalError();
        }
    }

    private String xtoString() {
        return "[max body descriptor: " + this.maximalBodyDescriptor + ", strict parsing: " + this.strictParsing + ", max line length: " + this.maxLineLen + ", max header count: " + this.maxHeaderCount + ", max content length: " + this.maxContentLen + ", count line numbers: " + this.countLineNumbers + "]";
    }
}
