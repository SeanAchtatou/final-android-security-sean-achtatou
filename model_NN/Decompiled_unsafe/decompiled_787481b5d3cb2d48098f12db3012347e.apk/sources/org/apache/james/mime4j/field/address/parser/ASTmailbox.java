package org.apache.james.mime4j.field.address.parser;

public class ASTmailbox extends SimpleNode {
    public Object jjtAccept(AddressListParserVisitor addressListParserVisitor, Object obj) {
        return xjjtAccept(addressListParserVisitor, obj);
    }

    public ASTmailbox(int id) {
        super(id);
    }

    public ASTmailbox(AddressListParser p, int id) {
        super(p, id);
    }

    private Object xjjtAccept(AddressListParserVisitor visitor, Object data) {
        return visitor.visit(this, data);
    }
}
