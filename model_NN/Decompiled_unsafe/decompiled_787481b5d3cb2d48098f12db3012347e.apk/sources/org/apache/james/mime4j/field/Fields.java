package org.apache.james.mime4j.field;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Pattern;
import org.apache.james.mime4j.codec.EncoderUtil;
import org.apache.james.mime4j.field.address.Address;
import org.apache.james.mime4j.field.address.Mailbox;
import org.apache.james.mime4j.parser.Field;
import org.apache.james.mime4j.util.ContentUtil;
import org.apache.james.mime4j.util.MimeUtil;

public class Fields {
    private static final Pattern FIELD_NAME_PATTERN = Pattern.compile("[\\x21-\\x39\\x3b-\\x7e]+");

    public static AddressListField addressList(String str, Iterable iterable) {
        return xaddressList(str, iterable);
    }

    private static AddressListField addressList0(String str, Iterable iterable) {
        return xaddressList0(str, iterable);
    }

    public static AddressListField bcc(Iterable iterable) {
        return xbcc(iterable);
    }

    public static AddressListField bcc(Address address) {
        return xbcc(address);
    }

    public static AddressListField bcc(Address... addressArr) {
        return xbcc(addressArr);
    }

    public static AddressListField cc(Iterable iterable) {
        return xcc(iterable);
    }

    public static AddressListField cc(Address address) {
        return xcc(address);
    }

    public static AddressListField cc(Address... addressArr) {
        return xcc(addressArr);
    }

    private static void checkValidFieldName(String str) {
        xcheckValidFieldName(str);
    }

    public static ContentDispositionField contentDisposition(String str) {
        return xcontentDisposition(str);
    }

    public static ContentDispositionField contentDisposition(String str, String str2) {
        return xcontentDisposition(str, str2);
    }

    public static ContentDispositionField contentDisposition(String str, String str2, long j) {
        return xcontentDisposition(str, str2, j);
    }

    public static ContentDispositionField contentDisposition(String str, String str2, long j, Date date, Date date2, Date date3) {
        return xcontentDisposition(str, str2, j, date, date2, date3);
    }

    public static ContentDispositionField contentDisposition(String str, Map map) {
        return xcontentDisposition(str, map);
    }

    public static ContentTransferEncodingField contentTransferEncoding(String str) {
        return xcontentTransferEncoding(str);
    }

    public static ContentTypeField contentType(String str) {
        return xcontentType(str);
    }

    public static ContentTypeField contentType(String str, Map map) {
        return xcontentType(str, map);
    }

    public static DateTimeField date(String str, Date date) {
        return xdate(str, date);
    }

    public static DateTimeField date(String str, Date date, TimeZone timeZone) {
        return xdate(str, date, timeZone);
    }

    public static DateTimeField date(Date date) {
        return xdate(date);
    }

    private static DateTimeField date0(String str, Date date, TimeZone timeZone) {
        return xdate0(str, date, timeZone);
    }

    private static String encodeAddresses(Iterable iterable) {
        return xencodeAddresses(iterable);
    }

    public static MailboxListField from(Iterable iterable) {
        return xfrom(iterable);
    }

    public static MailboxListField from(Mailbox mailbox) {
        return xfrom(mailbox);
    }

    public static MailboxListField from(Mailbox... mailboxArr) {
        return xfrom(mailboxArr);
    }

    private static boolean isValidDispositionType(String str) {
        return xisValidDispositionType(str);
    }

    private static boolean isValidMimeType(String str) {
        return xisValidMimeType(str);
    }

    public static MailboxField mailbox(String str, Mailbox mailbox) {
        return xmailbox(str, mailbox);
    }

    private static MailboxField mailbox0(String str, Mailbox mailbox) {
        return xmailbox0(str, mailbox);
    }

    public static MailboxListField mailboxList(String str, Iterable iterable) {
        return xmailboxList(str, iterable);
    }

    private static MailboxListField mailboxList0(String str, Iterable iterable) {
        return xmailboxList0(str, iterable);
    }

    public static Field messageId(String str) {
        return xmessageId(str);
    }

    private static Field parse(FieldParser fieldParser, String str, String str2) {
        return xparse(fieldParser, str, str2);
    }

    public static AddressListField replyTo(Iterable iterable) {
        return xreplyTo(iterable);
    }

    public static AddressListField replyTo(Address address) {
        return xreplyTo(address);
    }

    public static AddressListField replyTo(Address... addressArr) {
        return xreplyTo(addressArr);
    }

    public static MailboxField sender(Mailbox mailbox) {
        return xsender(mailbox);
    }

    public static UnstructuredField subject(String str) {
        return xsubject(str);
    }

    public static AddressListField to(Iterable iterable) {
        return xto(iterable);
    }

    public static AddressListField to(Address address) {
        return xto(address);
    }

    public static AddressListField to(Address... addressArr) {
        return xto(addressArr);
    }

    private Fields() {
    }

    private static ContentTypeField xcontentType(String contentType) {
        return (ContentTypeField) parse(ContentTypeField.PARSER, "Content-Type", contentType);
    }

    private static ContentTypeField xcontentType(String mimeType, Map<String, String> parameters) {
        if (!isValidMimeType(mimeType)) {
            throw new IllegalArgumentException();
        } else if (parameters == null || parameters.isEmpty()) {
            return (ContentTypeField) parse(ContentTypeField.PARSER, "Content-Type", mimeType);
        } else {
            StringBuilder sb = new StringBuilder(mimeType);
            for (Map.Entry<String, String> entry : parameters.entrySet()) {
                sb.append("; ");
                sb.append(EncoderUtil.encodeHeaderParameter((String) entry.getKey(), (String) entry.getValue()));
            }
            return contentType(sb.toString());
        }
    }

    private static ContentTransferEncodingField xcontentTransferEncoding(String contentTransferEncoding) {
        return (ContentTransferEncodingField) parse(ContentTransferEncodingField.PARSER, "Content-Transfer-Encoding", contentTransferEncoding);
    }

    private static ContentDispositionField xcontentDisposition(String contentDisposition) {
        return (ContentDispositionField) parse(ContentDispositionField.PARSER, "Content-Disposition", contentDisposition);
    }

    private static ContentDispositionField xcontentDisposition(String dispositionType, Map<String, String> parameters) {
        if (!isValidDispositionType(dispositionType)) {
            throw new IllegalArgumentException();
        } else if (parameters == null || parameters.isEmpty()) {
            return (ContentDispositionField) parse(ContentDispositionField.PARSER, "Content-Disposition", dispositionType);
        } else {
            StringBuilder sb = new StringBuilder(dispositionType);
            for (Map.Entry<String, String> entry : parameters.entrySet()) {
                sb.append("; ");
                sb.append(EncoderUtil.encodeHeaderParameter((String) entry.getKey(), (String) entry.getValue()));
            }
            return contentDisposition(sb.toString());
        }
    }

    private static ContentDispositionField xcontentDisposition(String dispositionType, String filename) {
        return contentDisposition(dispositionType, filename, -1, null, null, null);
    }

    private static ContentDispositionField xcontentDisposition(String dispositionType, String filename, long size) {
        return contentDisposition(dispositionType, filename, size, null, null, null);
    }

    private static ContentDispositionField xcontentDisposition(String dispositionType, String filename, long size, Date creationDate, Date modificationDate, Date readDate) {
        Map<String, String> parameters = new HashMap<>();
        if (filename != null) {
            parameters.put("filename", filename);
        }
        if (size >= 0) {
            parameters.put("size", Long.toString(size));
        }
        if (creationDate != null) {
            parameters.put("creation-date", MimeUtil.formatDate(creationDate, null));
        }
        if (modificationDate != null) {
            parameters.put("modification-date", MimeUtil.formatDate(modificationDate, null));
        }
        if (readDate != null) {
            parameters.put("read-date", MimeUtil.formatDate(readDate, null));
        }
        return contentDisposition(dispositionType, parameters);
    }

    private static DateTimeField xdate(Date date) {
        return date0(FieldName.DATE, date, null);
    }

    private static DateTimeField xdate(String fieldName, Date date) {
        checkValidFieldName(fieldName);
        return date0(fieldName, date, null);
    }

    private static DateTimeField xdate(String fieldName, Date date, TimeZone zone) {
        checkValidFieldName(fieldName);
        return date0(fieldName, date, zone);
    }

    private static Field xmessageId(String hostname) {
        return parse(UnstructuredField.PARSER, FieldName.MESSAGE_ID, MimeUtil.createUniqueMessageId(hostname));
    }

    private static UnstructuredField xsubject(String subject) {
        return (UnstructuredField) parse(UnstructuredField.PARSER, FieldName.SUBJECT, EncoderUtil.encodeIfNecessary(subject, EncoderUtil.Usage.TEXT_TOKEN, FieldName.SUBJECT.length() + 2));
    }

    private static MailboxField xsender(Mailbox mailbox) {
        return mailbox0(FieldName.SENDER, mailbox);
    }

    private static MailboxListField xfrom(Mailbox mailbox) {
        return mailboxList0(FieldName.FROM, Collections.singleton(mailbox));
    }

    private static MailboxListField xfrom(Mailbox... mailboxes) {
        return mailboxList0(FieldName.FROM, Arrays.asList(mailboxes));
    }

    private static MailboxListField xfrom(Iterable<Mailbox> mailboxes) {
        return mailboxList0(FieldName.FROM, mailboxes);
    }

    private static AddressListField xto(Address address) {
        return addressList0(FieldName.TO, Collections.singleton(address));
    }

    private static AddressListField xto(Address... addresses) {
        return addressList0(FieldName.TO, Arrays.asList(addresses));
    }

    private static AddressListField xto(Iterable<Address> addresses) {
        return addressList0(FieldName.TO, addresses);
    }

    private static AddressListField xcc(Address address) {
        return addressList0(FieldName.CC, Collections.singleton(address));
    }

    private static AddressListField xcc(Address... addresses) {
        return addressList0(FieldName.CC, Arrays.asList(addresses));
    }

    private static AddressListField xcc(Iterable<Address> addresses) {
        return addressList0(FieldName.CC, addresses);
    }

    private static AddressListField xbcc(Address address) {
        return addressList0(FieldName.BCC, Collections.singleton(address));
    }

    private static AddressListField xbcc(Address... addresses) {
        return addressList0(FieldName.BCC, Arrays.asList(addresses));
    }

    private static AddressListField xbcc(Iterable<Address> addresses) {
        return addressList0(FieldName.BCC, addresses);
    }

    private static AddressListField xreplyTo(Address address) {
        return addressList0(FieldName.REPLY_TO, Collections.singleton(address));
    }

    private static AddressListField xreplyTo(Address... addresses) {
        return addressList0(FieldName.REPLY_TO, Arrays.asList(addresses));
    }

    private static AddressListField xreplyTo(Iterable<Address> addresses) {
        return addressList0(FieldName.REPLY_TO, addresses);
    }

    private static MailboxField xmailbox(String fieldName, Mailbox mailbox) {
        checkValidFieldName(fieldName);
        return mailbox0(fieldName, mailbox);
    }

    private static MailboxListField xmailboxList(String fieldName, Iterable<Mailbox> mailboxes) {
        checkValidFieldName(fieldName);
        return mailboxList0(fieldName, mailboxes);
    }

    private static AddressListField xaddressList(String fieldName, Iterable<Address> addresses) {
        checkValidFieldName(fieldName);
        return addressList0(fieldName, addresses);
    }

    private static DateTimeField xdate0(String fieldName, Date date, TimeZone zone) {
        return (DateTimeField) parse(DateTimeField.PARSER, fieldName, MimeUtil.formatDate(date, zone));
    }

    private static MailboxField xmailbox0(String fieldName, Mailbox mailbox) {
        return (MailboxField) parse(MailboxField.PARSER, fieldName, encodeAddresses(Collections.singleton(mailbox)));
    }

    private static MailboxListField xmailboxList0(String fieldName, Iterable<Mailbox> mailboxes) {
        return (MailboxListField) parse(MailboxListField.PARSER, fieldName, encodeAddresses(mailboxes));
    }

    private static AddressListField xaddressList0(String fieldName, Iterable<Address> addresses) {
        return (AddressListField) parse(AddressListField.PARSER, fieldName, encodeAddresses(addresses));
    }

    private static void xcheckValidFieldName(String fieldName) {
        if (!FIELD_NAME_PATTERN.matcher(fieldName).matches()) {
            throw new IllegalArgumentException("Invalid field name");
        }
    }

    private static boolean xisValidMimeType(String mimeType) {
        int idx;
        if (mimeType == null || (idx = mimeType.indexOf(47)) == -1) {
            return false;
        }
        String type = mimeType.substring(0, idx);
        String subType = mimeType.substring(idx + 1);
        if (!EncoderUtil.isToken(type) || !EncoderUtil.isToken(subType)) {
            return false;
        }
        return true;
    }

    private static boolean xisValidDispositionType(String dispositionType) {
        if (dispositionType == null) {
            return false;
        }
        return EncoderUtil.isToken(dispositionType);
    }

    private static <F extends Field> F xparse(FieldParser parser, String fieldName, String fieldBody) {
        return parser.parse(fieldName, fieldBody, ContentUtil.encode(MimeUtil.fold(fieldName + ": " + fieldBody, 0)));
    }

    private static String xencodeAddresses(Iterable<? extends Address> addresses) {
        StringBuilder sb = new StringBuilder();
        for (Address address : addresses) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(address.getEncodedString());
        }
        return sb.toString();
    }
}
