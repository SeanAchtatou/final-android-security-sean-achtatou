package org.apache.james.mime4j.message;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.james.mime4j.codec.CodecUtil;
import org.apache.james.mime4j.storage.MultiReferenceStorage;

class StorageBinaryBody extends BinaryBody {
    private MultiReferenceStorage storage;

    /* renamed from: copy */
    public StorageBinaryBody xcopy() {
        return xcopy();
    }

    public void dispose() {
        xdispose();
    }

    public InputStream getInputStream() {
        return xgetInputStream();
    }

    public void writeTo(OutputStream outputStream) {
        xwriteTo(outputStream);
    }

    public StorageBinaryBody(MultiReferenceStorage storage2) {
        this.storage = storage2;
    }

    private InputStream xgetInputStream() throws IOException {
        return this.storage.getInputStream();
    }

    private void xwriteTo(OutputStream out) throws IOException {
        if (out == null) {
            throw new IllegalArgumentException();
        }
        InputStream in = this.storage.getInputStream();
        CodecUtil.copy(in, out);
        in.close();
    }

    private StorageBinaryBody xcopy() {
        this.storage.addReference();
        return new StorageBinaryBody(this.storage);
    }

    private void xdispose() {
        if (this.storage != null) {
            this.storage.delete();
            this.storage = null;
        }
    }
}
