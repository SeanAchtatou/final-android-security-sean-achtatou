package org.apache.james.mime4j.field.address.parser;

public class ASTlocal_part extends SimpleNode {
    public Object jjtAccept(AddressListParserVisitor addressListParserVisitor, Object obj) {
        return xjjtAccept(addressListParserVisitor, obj);
    }

    public ASTlocal_part(int id) {
        super(id);
    }

    public ASTlocal_part(AddressListParser p, int id) {
        super(p, id);
    }

    private Object xjjtAccept(AddressListParserVisitor visitor, Object data) {
        return visitor.visit(this, data);
    }
}
