package org.apache.james.mime4j.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

public class StringArrayMap implements Serializable {
    private static final long serialVersionUID = -5833051164281786907L;
    private final Map<String, Object> map = new HashMap();

    public static Map asMap(Map map2) {
        return xasMap(map2);
    }

    public static String asString(Object obj) {
        return xasString(obj);
    }

    public static String[] asStringArray(Object obj) {
        return xasStringArray(obj);
    }

    public static Enumeration asStringEnum(Object obj) {
        return xasStringEnum(obj);
    }

    /* access modifiers changed from: protected */
    public void addMapValue(Map map2, String str, String str2) {
        xaddMapValue(map2, str, str2);
    }

    public void addValue(String str, String str2) {
        xaddValue(str, str2);
    }

    /* access modifiers changed from: protected */
    public String convertName(String str) {
        return xconvertName(str);
    }

    public Map getMap() {
        return xgetMap();
    }

    public String[] getNameArray() {
        return xgetNameArray();
    }

    public Enumeration getNames() {
        return xgetNames();
    }

    public String getValue(String str) {
        return xgetValue(str);
    }

    public Enumeration getValueEnum(String str) {
        return xgetValueEnum(str);
    }

    public String[] getValues(String str) {
        return xgetValues(str);
    }

    private static String xasString(Object pValue) {
        if (pValue == null) {
            return null;
        }
        if (pValue instanceof String) {
            return (String) pValue;
        }
        if (pValue instanceof String[]) {
            return ((String[]) pValue)[0];
        }
        if (pValue instanceof List) {
            return (String) ((List) pValue).get(0);
        }
        throw new IllegalStateException("Invalid parameter class: " + pValue.getClass().getName());
    }

    private static String[] xasStringArray(Object pValue) {
        if (pValue == null) {
            return null;
        }
        if (pValue instanceof String) {
            return new String[]{(String) pValue};
        } else if (pValue instanceof String[]) {
            return (String[]) pValue;
        } else {
            if (pValue instanceof List) {
                List<?> l = (List) pValue;
                return (String[]) l.toArray(new String[l.size()]);
            }
            throw new IllegalStateException("Invalid parameter class: " + pValue.getClass().getName());
        }
    }

    private static Enumeration<String> xasStringEnum(final Object pValue) {
        if (pValue == null) {
            return null;
        }
        if (pValue instanceof String) {
            return new Enumeration<String>() {
                private Object value = pValue;

                public boolean hasMoreElements() {
                    return xhasMoreElements();
                }

                /* renamed from: nextElement */
                public String xnextElement() {
                    return xnextElement();
                }

                private boolean xhasMoreElements() {
                    return this.value != null;
                }

                private String xnextElement() {
                    if (this.value == null) {
                        throw new NoSuchElementException();
                    }
                    String s = (String) this.value;
                    this.value = null;
                    return s;
                }
            };
        }
        if (pValue instanceof String[]) {
            final String[] values = (String[]) pValue;
            return new Enumeration<String>() {
                private int offset;

                public boolean hasMoreElements() {
                    return xhasMoreElements();
                }

                /* renamed from: nextElement */
                public String xnextElement() {
                    return xnextElement();
                }

                private boolean xhasMoreElements() {
                    return this.offset < values.length;
                }

                private String xnextElement() {
                    if (this.offset >= values.length) {
                        throw new NoSuchElementException();
                    }
                    String[] strArr = values;
                    int i = this.offset;
                    this.offset = i + 1;
                    return strArr[i];
                }
            };
        } else if (pValue instanceof List) {
            return Collections.enumeration((List) pValue);
        } else {
            throw new IllegalStateException("Invalid parameter class: " + pValue.getClass().getName());
        }
    }

    private static Map<String, String[]> xasMap(Map<String, Object> pMap) {
        Map<String, String[]> result = new HashMap<>(pMap.size());
        for (Map.Entry<String, Object> entry : pMap.entrySet()) {
            result.put(entry.getKey(), asStringArray(entry.getValue()));
        }
        return Collections.unmodifiableMap(result);
    }

    private void xaddMapValue(Map<String, Object> pMap, String pName, String pValue) {
        Object o = pMap.get(pName);
        if (o == null) {
            o = pValue;
        } else if (o instanceof String) {
            List<Object> list = new ArrayList<>();
            list.add(o);
            list.add(pValue);
            o = list;
        } else if (o instanceof List) {
            ((List) o).add(pValue);
        } else if (o instanceof String[]) {
            List<String> list2 = new ArrayList<>();
            for (String str : (String[]) o) {
                list2.add(str);
            }
            list2.add(pValue);
            o = list2;
        } else {
            throw new IllegalStateException("Invalid object type: " + o.getClass().getName());
        }
        pMap.put(pName, o);
    }

    private String xconvertName(String pName) {
        return pName.toLowerCase();
    }

    private String xgetValue(String pName) {
        return asString(this.map.get(convertName(pName)));
    }

    private String[] xgetValues(String pName) {
        return asStringArray(this.map.get(convertName(pName)));
    }

    private Enumeration<String> xgetValueEnum(String pName) {
        return asStringEnum(this.map.get(convertName(pName)));
    }

    private Enumeration<String> xgetNames() {
        return Collections.enumeration(this.map.keySet());
    }

    private Map<String, String[]> xgetMap() {
        return asMap(this.map);
    }

    private void xaddValue(String pName, String pValue) {
        addMapValue(this.map, convertName(pName), pValue);
    }

    private String[] xgetNameArray() {
        Collection<String> c = this.map.keySet();
        return (String[]) c.toArray(new String[c.size()]);
    }
}
