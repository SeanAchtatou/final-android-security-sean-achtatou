package org.apache.james.mime4j.codec;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class QuotedPrintableOutputStream extends FilterOutputStream {
    private boolean closed = false;
    private QuotedPrintableEncoder encoder;

    public void close() {
        xclose();
    }

    public void flush() {
        xflush();
    }

    public void write(int i) {
        xwrite(i);
    }

    public void write(byte[] bArr, int i, int i2) {
        xwrite(bArr, i, i2);
    }

    public QuotedPrintableOutputStream(OutputStream out, boolean binary) {
        super(out);
        this.encoder = new QuotedPrintableEncoder(1024, binary);
        this.encoder.initEncoding(out);
    }

    private void xclose() throws IOException {
        if (!this.closed) {
            try {
                this.encoder.completeEncoding();
            } finally {
                this.closed = true;
            }
        }
    }

    private void xflush() throws IOException {
        this.encoder.flushOutput();
    }

    private void xwrite(int b) throws IOException {
        write(new byte[]{(byte) b}, 0, 1);
    }

    private void xwrite(byte[] b, int off, int len) throws IOException {
        if (this.closed) {
            throw new IOException("QuotedPrintableOutputStream has been closed");
        }
        this.encoder.encodeChunk(b, off, len);
    }
}
