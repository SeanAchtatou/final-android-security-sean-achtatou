package org.apache.james.mime4j.field.structured.parser;

import java.io.InputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.Vector;

public class StructuredFieldParser implements StructuredFieldParserConstants {
    private static int[] jj_la1_0;
    private Vector<int[]> jj_expentries;
    private int[] jj_expentry;
    private int jj_gen;
    SimpleCharStream jj_input_stream;
    private int jj_kind;
    private final int[] jj_la1;
    public Token jj_nt;
    private int jj_ntk;
    private boolean preserveFolding;
    public Token token;
    public StructuredFieldParserTokenManager token_source;

    private final String doParse() {
        return xdoParse();
    }

    private final Token jj_consume_token(int i) {
        return xjj_consume_token(i);
    }

    private static void jj_la1_0() {
        xjj_la1_0();
    }

    private final int jj_ntk() {
        return xjj_ntk();
    }

    public void ReInit(InputStream inputStream) {
        xReInit(inputStream);
    }

    public void ReInit(InputStream inputStream, String str) {
        xReInit(inputStream, str);
    }

    public void ReInit(Reader reader) {
        xReInit(reader);
    }

    public void ReInit(StructuredFieldParserTokenManager structuredFieldParserTokenManager) {
        xReInit(structuredFieldParserTokenManager);
    }

    public final void disable_tracing() {
        xdisable_tracing();
    }

    public final void enable_tracing() {
        xenable_tracing();
    }

    public ParseException generateParseException() {
        return xgenerateParseException();
    }

    public final Token getNextToken() {
        return xgetNextToken();
    }

    public final Token getToken(int i) {
        return xgetToken(i);
    }

    public boolean isFoldingPreserved() {
        return xisFoldingPreserved();
    }

    public String parse() {
        return xparse();
    }

    public void setFoldingPreserved(boolean z) {
        xsetFoldingPreserved(z);
    }

    private boolean xisFoldingPreserved() {
        return this.preserveFolding;
    }

    private void xsetFoldingPreserved(boolean preserveFolding2) {
        this.preserveFolding = preserveFolding2;
    }

    private String xparse() throws ParseException {
        try {
            return doParse();
        } catch (TokenMgrError e) {
            throw new ParseException(e);
        }
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0015 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:3:0x000e  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024 A[FALL_THROUGH] */
    private final java.lang.String xdoParse() throws org.apache.james.mime4j.field.structured.parser.ParseException {
        /*
            r8 = this;
            r7 = -1
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r4 = 50
            r0.<init>(r4)
            r3 = 0
            r1 = 1
        L_0x000a:
            int r4 = r8.jj_ntk
            if (r4 != r7) goto L_0x0021
            int r4 = r8.jj_ntk()
        L_0x0012:
            switch(r4) {
                case 11: goto L_0x0024;
                case 12: goto L_0x0024;
                case 13: goto L_0x0024;
                case 14: goto L_0x0024;
                case 15: goto L_0x0024;
                default: goto L_0x0015;
            }
        L_0x0015:
            int[] r4 = r8.jj_la1
            r5 = 0
            int r6 = r8.jj_gen
            r4[r5] = r6
            java.lang.String r4 = r0.toString()
            return r4
        L_0x0021:
            int r4 = r8.jj_ntk
            goto L_0x0012
        L_0x0024:
            int r4 = r8.jj_ntk
            if (r4 != r7) goto L_0x003f
            int r4 = r8.jj_ntk()
        L_0x002c:
            switch(r4) {
                case 11: goto L_0x005a;
                case 12: goto L_0x007e;
                case 13: goto L_0x0066;
                case 14: goto L_0x008f;
                case 15: goto L_0x0042;
                default: goto L_0x002f;
            }
        L_0x002f:
            int[] r4 = r8.jj_la1
            r5 = 1
            int r6 = r8.jj_gen
            r4[r5] = r6
            r8.jj_consume_token(r7)
            org.apache.james.mime4j.field.structured.parser.ParseException r4 = new org.apache.james.mime4j.field.structured.parser.ParseException
            r4.<init>()
            throw r4
        L_0x003f:
            int r4 = r8.jj_ntk
            goto L_0x002c
        L_0x0042:
            r4 = 15
            org.apache.james.mime4j.field.structured.parser.Token r2 = r8.jj_consume_token(r4)
            if (r1 == 0) goto L_0x0051
            r1 = 0
        L_0x004b:
            java.lang.String r4 = r2.image
            r0.append(r4)
            goto L_0x000a
        L_0x0051:
            if (r3 == 0) goto L_0x004b
            java.lang.String r4 = " "
            r0.append(r4)
            r3 = 0
            goto L_0x004b
        L_0x005a:
            r4 = 11
            org.apache.james.mime4j.field.structured.parser.Token r2 = r8.jj_consume_token(r4)
            java.lang.String r4 = r2.image
            r0.append(r4)
            goto L_0x000a
        L_0x0066:
            r4 = 13
            org.apache.james.mime4j.field.structured.parser.Token r2 = r8.jj_consume_token(r4)
            if (r1 == 0) goto L_0x0075
            r1 = 0
        L_0x006f:
            java.lang.String r4 = r2.image
            r0.append(r4)
            goto L_0x000a
        L_0x0075:
            if (r3 == 0) goto L_0x006f
            java.lang.String r4 = " "
            r0.append(r4)
            r3 = 0
            goto L_0x006f
        L_0x007e:
            r4 = 12
            org.apache.james.mime4j.field.structured.parser.Token r2 = r8.jj_consume_token(r4)
            boolean r4 = r8.preserveFolding
            if (r4 == 0) goto L_0x000a
            java.lang.String r4 = "\r\n"
            r0.append(r4)
            goto L_0x000a
        L_0x008f:
            r4 = 14
            org.apache.james.mime4j.field.structured.parser.Token r2 = r8.jj_consume_token(r4)
            r3 = 1
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.field.structured.parser.StructuredFieldParser.xdoParse():java.lang.String");
    }

    static {
        jj_la1_0();
    }

    private static void xjj_la1_0() {
        jj_la1_0 = new int[]{63488, 63488};
    }

    public StructuredFieldParser(InputStream stream) {
        this(stream, null);
    }

    public StructuredFieldParser(InputStream stream, String encoding) {
        this.preserveFolding = false;
        this.jj_la1 = new int[2];
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        try {
            this.jj_input_stream = new SimpleCharStream(stream, encoding, 1, 1);
            this.token_source = new StructuredFieldParserTokenManager(this.jj_input_stream);
            this.token = new Token();
            this.jj_ntk = -1;
            this.jj_gen = 0;
            for (int i = 0; i < 2; i++) {
                this.jj_la1[i] = -1;
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private void xReInit(InputStream stream) {
        ReInit(stream, null);
    }

    private void xReInit(InputStream stream, String encoding) {
        try {
            this.jj_input_stream.ReInit(stream, encoding, 1, 1);
            this.token_source.ReInit(this.jj_input_stream);
            this.token = new Token();
            this.jj_ntk = -1;
            this.jj_gen = 0;
            for (int i = 0; i < 2; i++) {
                this.jj_la1[i] = -1;
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public StructuredFieldParser(Reader stream) {
        this.preserveFolding = false;
        this.jj_la1 = new int[2];
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        this.jj_input_stream = new SimpleCharStream(stream, 1, 1);
        this.token_source = new StructuredFieldParserTokenManager(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 2; i++) {
            this.jj_la1[i] = -1;
        }
    }

    private void xReInit(Reader stream) {
        this.jj_input_stream.ReInit(stream, 1, 1);
        this.token_source.ReInit(this.jj_input_stream);
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 2; i++) {
            this.jj_la1[i] = -1;
        }
    }

    public StructuredFieldParser(StructuredFieldParserTokenManager tm) {
        this.preserveFolding = false;
        this.jj_la1 = new int[2];
        this.jj_expentries = new Vector<>();
        this.jj_kind = -1;
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 2; i++) {
            this.jj_la1[i] = -1;
        }
    }

    private void xReInit(StructuredFieldParserTokenManager tm) {
        this.token_source = tm;
        this.token = new Token();
        this.jj_ntk = -1;
        this.jj_gen = 0;
        for (int i = 0; i < 2; i++) {
            this.jj_la1[i] = -1;
        }
    }

    private final Token xjj_consume_token(int kind) throws ParseException {
        Token oldToken = this.token;
        if (oldToken.next != null) {
            this.token = this.token.next;
        } else {
            Token token2 = this.token;
            Token nextToken = this.token_source.getNextToken();
            token2.next = nextToken;
            this.token = nextToken;
        }
        this.jj_ntk = -1;
        if (this.token.kind == kind) {
            this.jj_gen++;
            return this.token;
        }
        this.token = oldToken;
        this.jj_kind = kind;
        throw generateParseException();
    }

    private final Token xgetNextToken() {
        if (this.token.next != null) {
            this.token = this.token.next;
        } else {
            Token token2 = this.token;
            Token nextToken = this.token_source.getNextToken();
            token2.next = nextToken;
            this.token = nextToken;
        }
        this.jj_ntk = -1;
        this.jj_gen++;
        return this.token;
    }

    private final Token xgetToken(int index) {
        Token t;
        int i = 0;
        Token t2 = this.token;
        while (i < index) {
            if (t2.next != null) {
                t = t2.next;
            } else {
                t = this.token_source.getNextToken();
                t2.next = t;
            }
            i++;
            t2 = t;
        }
        return t2;
    }

    private final int xjj_ntk() {
        Token token2 = this.token.next;
        this.jj_nt = token2;
        if (token2 == null) {
            Token token3 = this.token;
            Token nextToken = this.token_source.getNextToken();
            token3.next = nextToken;
            int i = nextToken.kind;
            this.jj_ntk = i;
            return i;
        }
        int i2 = this.jj_nt.kind;
        this.jj_ntk = i2;
        return i2;
    }

    private ParseException xgenerateParseException() {
        this.jj_expentries.removeAllElements();
        boolean[] la1tokens = new boolean[18];
        for (int i = 0; i < 18; i++) {
            la1tokens[i] = false;
        }
        if (this.jj_kind >= 0) {
            la1tokens[this.jj_kind] = true;
            this.jj_kind = -1;
        }
        for (int i2 = 0; i2 < 2; i2++) {
            if (this.jj_la1[i2] == this.jj_gen) {
                for (int j = 0; j < 32; j++) {
                    if ((jj_la1_0[i2] & (1 << j)) != 0) {
                        la1tokens[j] = true;
                    }
                }
            }
        }
        for (int i3 = 0; i3 < 18; i3++) {
            if (la1tokens[i3]) {
                this.jj_expentry = new int[1];
                this.jj_expentry[0] = i3;
                this.jj_expentries.addElement(this.jj_expentry);
            }
        }
        int[][] exptokseq = new int[this.jj_expentries.size()][];
        for (int i4 = 0; i4 < this.jj_expentries.size(); i4++) {
            exptokseq[i4] = this.jj_expentries.elementAt(i4);
        }
        return new ParseException(this.token, exptokseq, tokenImage);
    }

    private final void xenable_tracing() {
    }

    private final void xdisable_tracing() {
    }
}
