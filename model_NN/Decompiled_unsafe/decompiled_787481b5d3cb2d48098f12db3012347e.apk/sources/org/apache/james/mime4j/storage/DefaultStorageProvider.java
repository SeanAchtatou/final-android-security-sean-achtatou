package org.apache.james.mime4j.storage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DefaultStorageProvider {
    public static final String DEFAULT_STORAGE_PROVIDER_PROPERTY = "org.apache.james.mime4j.defaultStorageProvider";
    private static volatile StorageProvider instance = null;
    private static Log log = LogFactory.getLog(DefaultStorageProvider.class);

    public static StorageProvider getInstance() {
        return xgetInstance();
    }

    private static void initialize() {
        xinitialize();
    }

    static void reset() {
        xreset();
    }

    public static void setInstance(StorageProvider storageProvider) {
        xsetInstance(storageProvider);
    }

    static {
        initialize();
    }

    private DefaultStorageProvider() {
    }

    private static StorageProvider xgetInstance() {
        return instance;
    }

    private static void xsetInstance(StorageProvider instance2) {
        if (instance2 == null) {
            throw new IllegalArgumentException();
        }
        instance = instance2;
    }

    private static void xinitialize() {
        String clazz = System.getProperty(DEFAULT_STORAGE_PROVIDER_PROPERTY);
        if (clazz != null) {
            try {
                instance = (StorageProvider) Class.forName(clazz).newInstance();
            } catch (Exception e) {
                log.warn("Unable to create or instantiate StorageProvider class '" + clazz + "'. Using default instead.", e);
            }
        }
        if (instance == null) {
            instance = new ThresholdStorageProvider(new TempFileStorageProvider(), 1024);
        }
    }

    private static void xreset() {
        instance = null;
        initialize();
    }
}
