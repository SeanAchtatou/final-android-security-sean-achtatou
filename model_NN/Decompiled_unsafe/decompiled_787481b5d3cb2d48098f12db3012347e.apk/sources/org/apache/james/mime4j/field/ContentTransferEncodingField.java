package org.apache.james.mime4j.field;

import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.util.MimeUtil;

public class ContentTransferEncodingField extends AbstractField {
    static final FieldParser PARSER = new FieldParser() {
        public ParsedField parse(String str, String str2, ByteSequence byteSequence) {
            return xparse(str, str2, byteSequence);
        }

        private ParsedField xparse(String name, String body, ByteSequence raw) {
            return new ContentTransferEncodingField(name, body, raw);
        }
    };
    private String encoding;

    public static String getEncoding(ContentTransferEncodingField contentTransferEncodingField) {
        return xgetEncoding(contentTransferEncodingField);
    }

    public String getEncoding() {
        return xgetEncoding();
    }

    ContentTransferEncodingField(String name, String body, ByteSequence raw) {
        super(name, body, raw);
        this.encoding = body.trim().toLowerCase();
    }

    private String xgetEncoding() {
        return this.encoding;
    }

    private static String xgetEncoding(ContentTransferEncodingField f) {
        if (f == null || f.getEncoding().length() == 0) {
            return MimeUtil.ENC_7BIT;
        }
        return f.getEncoding();
    }
}
