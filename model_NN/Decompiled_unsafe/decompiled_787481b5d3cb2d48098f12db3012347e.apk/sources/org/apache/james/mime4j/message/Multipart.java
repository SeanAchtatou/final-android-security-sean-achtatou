package org.apache.james.mime4j.message;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.util.ContentUtil;

public class Multipart implements Body {
    private List<BodyPart> bodyParts;
    private ByteSequence epilogue;
    private transient String epilogueStrCache;
    private Entity parent;
    private ByteSequence preamble;
    private transient String preambleStrCache;
    private String subType;

    public void addBodyPart(BodyPart bodyPart) {
        xaddBodyPart(bodyPart);
    }

    public void addBodyPart(BodyPart bodyPart, int i) {
        xaddBodyPart(bodyPart, i);
    }

    public void dispose() {
        xdispose();
    }

    public List getBodyParts() {
        return xgetBodyParts();
    }

    public int getCount() {
        return xgetCount();
    }

    public String getEpilogue() {
        return xgetEpilogue();
    }

    /* access modifiers changed from: package-private */
    public ByteSequence getEpilogueRaw() {
        return xgetEpilogueRaw();
    }

    public Entity getParent() {
        return xgetParent();
    }

    public String getPreamble() {
        return xgetPreamble();
    }

    /* access modifiers changed from: package-private */
    public ByteSequence getPreambleRaw() {
        return xgetPreambleRaw();
    }

    public String getSubType() {
        return xgetSubType();
    }

    public BodyPart removeBodyPart(int i) {
        return xremoveBodyPart(i);
    }

    public BodyPart replaceBodyPart(BodyPart bodyPart, int i) {
        return xreplaceBodyPart(bodyPart, i);
    }

    public void setBodyParts(List list) {
        xsetBodyParts(list);
    }

    public void setEpilogue(String str) {
        xsetEpilogue(str);
    }

    /* access modifiers changed from: package-private */
    public void setEpilogueRaw(ByteSequence byteSequence) {
        xsetEpilogueRaw(byteSequence);
    }

    public void setParent(Entity entity) {
        xsetParent(entity);
    }

    public void setPreamble(String str) {
        xsetPreamble(str);
    }

    /* access modifiers changed from: package-private */
    public void setPreambleRaw(ByteSequence byteSequence) {
        xsetPreambleRaw(byteSequence);
    }

    public void setSubType(String str) {
        xsetSubType(str);
    }

    public Multipart(String subType2) {
        this.bodyParts = new LinkedList();
        this.parent = null;
        this.preamble = ByteSequence.EMPTY;
        this.preambleStrCache = "";
        this.epilogue = ByteSequence.EMPTY;
        this.epilogueStrCache = "";
        this.subType = subType2;
    }

    public Multipart(Multipart other) {
        this.bodyParts = new LinkedList();
        this.parent = null;
        this.preamble = other.preamble;
        this.preambleStrCache = other.preambleStrCache;
        this.epilogue = other.epilogue;
        this.epilogueStrCache = other.epilogueStrCache;
        for (BodyPart otherBodyPart : other.bodyParts) {
            addBodyPart(new BodyPart(otherBodyPart));
        }
        this.subType = other.subType;
    }

    private String xgetSubType() {
        return this.subType;
    }

    private void xsetSubType(String subType2) {
        this.subType = subType2;
    }

    private Entity xgetParent() {
        return this.parent;
    }

    private void xsetParent(Entity parent2) {
        this.parent = parent2;
        for (BodyPart bodyPart : this.bodyParts) {
            bodyPart.setParent(parent2);
        }
    }

    private int xgetCount() {
        return this.bodyParts.size();
    }

    private List<BodyPart> xgetBodyParts() {
        return Collections.unmodifiableList(this.bodyParts);
    }

    private void xsetBodyParts(List<BodyPart> bodyParts2) {
        this.bodyParts = bodyParts2;
        for (BodyPart bodyPart : bodyParts2) {
            bodyPart.setParent(this.parent);
        }
    }

    private void xaddBodyPart(BodyPart bodyPart) {
        if (bodyPart == null) {
            throw new IllegalArgumentException();
        }
        this.bodyParts.add(bodyPart);
        bodyPart.setParent(this.parent);
    }

    private void xaddBodyPart(BodyPart bodyPart, int index) {
        if (bodyPart == null) {
            throw new IllegalArgumentException();
        }
        this.bodyParts.add(index, bodyPart);
        bodyPart.setParent(this.parent);
    }

    private BodyPart xremoveBodyPart(int index) {
        BodyPart bodyPart = this.bodyParts.remove(index);
        bodyPart.setParent(null);
        return bodyPart;
    }

    private BodyPart xreplaceBodyPart(BodyPart bodyPart, int index) {
        if (bodyPart == null) {
            throw new IllegalArgumentException();
        }
        BodyPart replacedBodyPart = this.bodyParts.set(index, bodyPart);
        if (bodyPart == replacedBodyPart) {
            throw new IllegalArgumentException("Cannot replace body part with itself");
        }
        bodyPart.setParent(this.parent);
        replacedBodyPart.setParent(null);
        return replacedBodyPart;
    }

    private ByteSequence xgetPreambleRaw() {
        return this.preamble;
    }

    private void xsetPreambleRaw(ByteSequence preamble2) {
        this.preamble = preamble2;
        this.preambleStrCache = null;
    }

    private String xgetPreamble() {
        if (this.preambleStrCache == null) {
            this.preambleStrCache = ContentUtil.decode(this.preamble);
        }
        return this.preambleStrCache;
    }

    private void xsetPreamble(String preamble2) {
        this.preamble = ContentUtil.encode(preamble2);
        this.preambleStrCache = preamble2;
    }

    private ByteSequence xgetEpilogueRaw() {
        return this.epilogue;
    }

    private void xsetEpilogueRaw(ByteSequence epilogue2) {
        this.epilogue = epilogue2;
        this.epilogueStrCache = null;
    }

    private String xgetEpilogue() {
        if (this.epilogueStrCache == null) {
            this.epilogueStrCache = ContentUtil.decode(this.epilogue);
        }
        return this.epilogueStrCache;
    }

    private void xsetEpilogue(String epilogue2) {
        this.epilogue = ContentUtil.encode(epilogue2);
        this.epilogueStrCache = epilogue2;
    }

    private void xdispose() {
        for (BodyPart bodyPart : this.bodyParts) {
            bodyPart.dispose();
        }
    }
}
