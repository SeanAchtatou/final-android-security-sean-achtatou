package org.apache.james.mime4j.field;

import java.util.HashMap;
import java.util.Map;
import org.apache.james.mime4j.util.ByteSequence;

public class DelegatingFieldParser implements FieldParser {
    private FieldParser defaultParser = UnstructuredField.PARSER;
    private Map<String, FieldParser> parsers = new HashMap();

    public FieldParser getParser(String str) {
        return xgetParser(str);
    }

    public ParsedField parse(String str, String str2, ByteSequence byteSequence) {
        return xparse(str, str2, byteSequence);
    }

    public void setFieldParser(String str, FieldParser fieldParser) {
        xsetFieldParser(str, fieldParser);
    }

    private void xsetFieldParser(String name, FieldParser parser) {
        this.parsers.put(name.toLowerCase(), parser);
    }

    private FieldParser xgetParser(String name) {
        FieldParser field = this.parsers.get(name.toLowerCase());
        if (field == null) {
            return this.defaultParser;
        }
        return field;
    }

    private ParsedField xparse(String name, String body, ByteSequence raw) {
        return getParser(name).parse(name, body, raw);
    }
}
