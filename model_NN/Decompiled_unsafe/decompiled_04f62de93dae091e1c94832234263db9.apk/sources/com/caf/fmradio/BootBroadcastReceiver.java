package com.caf.fmradio;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootBroadcastReceiver extends BroadcastReceiver {
    static final String action_boot = "android.intent.action.BOOT_COMPLETED";

    /* renamed from: com.caf.fmradio.BootBroadcastReceiver$BootBroadcastReceiver  reason: collision with other inner class name */
    public class C0000BootBroadcastReceiver extends BroadcastReceiver {
        static final String action_boot = "android.intent.action.BOOT_COMPLETED";
        private final BootBroadcastReceiver this$0;

        public C0000BootBroadcastReceiver(BootBroadcastReceiver bootBroadcastReceiver) {
            this.this$0 = bootBroadcastReceiver;
        }

        static BootBroadcastReceiver access$0(C0000BootBroadcastReceiver bootBroadcastReceiver) {
            return bootBroadcastReceiver.this$0;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            Intent intent2;
            Throwable th;
            Context context2 = context;
            if (intent.getAction().equals(action_boot)) {
                Intent intent3 = intent2;
                try {
                    new Intent(context2, Class.forName("com.caf.fmradio.ClockService"));
                    Intent intent4 = intent3;
                    Intent addFlags = intent4.addFlags(268435456);
                    context2.startActivity(intent4);
                } catch (ClassNotFoundException e) {
                    ClassNotFoundException classNotFoundException = e;
                    Throwable th2 = th;
                    new NoClassDefFoundError(classNotFoundException.getMessage());
                    throw th2;
                }
            }
        }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            context.startService(new Intent(context, Class.forName("com.caf.fmradio.MainActivity")));
        } catch (ClassNotFoundException e) {
            throw new NoClassDefFoundError(e.getMessage());
        }
    }
}
