package com.imadpush.ad.model;

public class JmUser {
    private long mId;
    private double mJmMoney;
    private String mName;
    private String mPetName;
    private String mUserUNI;

    public long getmId() {
        return this.mId;
    }

    public void setmId(long mId2) {
        this.mId = mId2;
    }

    public String getmName() {
        return this.mName;
    }

    public void setmName(String mName2) {
        this.mName = mName2;
    }

    public String getmUserUNI() {
        return this.mUserUNI;
    }

    public void setmUserUNI(String mUserUNI2) {
        this.mUserUNI = mUserUNI2;
    }

    public String getmPetName() {
        return this.mPetName;
    }

    public void setmPetName(String mPetName2) {
        this.mPetName = mPetName2;
    }

    public double getmJmMoney() {
        return this.mJmMoney;
    }

    public void setmJmMoney(double mJmMoney2) {
        this.mJmMoney = mJmMoney2;
    }
}
