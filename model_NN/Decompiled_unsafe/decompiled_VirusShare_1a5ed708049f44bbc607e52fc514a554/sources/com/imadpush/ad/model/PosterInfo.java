package com.imadpush.ad.model;

import java.io.Serializable;

public class PosterInfo implements Serializable {
    private static final long serialVersionUID = 1;
    private int condition;
    private String content;
    private String description;
    private int file_size;
    private String file_url;
    private Long id;
    private String img_url;
    private String imgs_url1;
    private String imgs_url2;
    private String imgs_url3;
    private String imgs_url4;
    private String imgs_url5;
    private String name;
    private String net_url;
    private String packagename;
    private String play_url;
    private int point;
    private int type;

    public String getName() {
        return this.name;
    }

    public void setName(String name2) {
        this.name = name2;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description2) {
        this.description = description2;
    }

    public int getCondition() {
        return this.condition;
    }

    public String getNet_url() {
        return this.net_url;
    }

    public void setNet_url(String net_url2) {
        this.net_url = net_url2;
    }

    public void setCondition(int condition2) {
        this.condition = condition2;
    }

    public int getFile_size() {
        return this.file_size;
    }

    public void setFile_size(int file_size2) {
        this.file_size = file_size2;
    }

    public String getImg_url() {
        return this.img_url;
    }

    public void setImg_url(String img_url2) {
        this.img_url = img_url2;
    }

    public String getImgs_url1() {
        return this.imgs_url1;
    }

    public void setImgs_url1(String imgs_url12) {
        this.imgs_url1 = imgs_url12;
    }

    public String getImgs_url2() {
        return this.imgs_url2;
    }

    public void setImgs_url2(String imgs_url22) {
        this.imgs_url2 = imgs_url22;
    }

    public String getImgs_url3() {
        return this.imgs_url3;
    }

    public void setImgs_url3(String imgs_url32) {
        this.imgs_url3 = imgs_url32;
    }

    public String getImgs_url4() {
        return this.imgs_url4;
    }

    public void setImgs_url4(String imgs_url42) {
        this.imgs_url4 = imgs_url42;
    }

    public String getImgs_url5() {
        return this.imgs_url5;
    }

    public void setImgs_url5(String imgs_url52) {
        this.imgs_url5 = imgs_url52;
    }

    public String getFile_url() {
        return this.file_url;
    }

    public void setFile_url(String file_url2) {
        this.file_url = file_url2;
    }

    public int getPoint() {
        return this.point;
    }

    public void setPoint(int point2) {
        this.point = point2;
    }

    public int getType() {
        return this.type;
    }

    public void setType(int type2) {
        this.type = type2;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id2) {
        this.id = id2;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content2) {
        this.content = content2;
    }

    public String getPlay_url() {
        return this.play_url;
    }

    public void setPlay_url(String play_url2) {
        this.play_url = play_url2;
    }

    public String getPackagename() {
        return this.packagename;
    }

    public void setPackagename(String packagename2) {
        this.packagename = packagename2;
    }
}
