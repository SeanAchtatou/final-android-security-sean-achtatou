package com.imadpush.ad.util;

public class Base64 {
    private static byte[] base64DecodeChars;
    private static char[] base64EncodeChars = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

    static {
        byte[] bArr = new byte[128];
        bArr[0] = -1;
        bArr[1] = -1;
        bArr[2] = -1;
        bArr[3] = -1;
        bArr[4] = -1;
        bArr[5] = -1;
        bArr[6] = -1;
        bArr[7] = -1;
        bArr[8] = -1;
        bArr[9] = -1;
        bArr[10] = -1;
        bArr[11] = -1;
        bArr[12] = -1;
        bArr[13] = -1;
        bArr[14] = -1;
        bArr[15] = -1;
        bArr[16] = -1;
        bArr[17] = -1;
        bArr[18] = -1;
        bArr[19] = -1;
        bArr[20] = -1;
        bArr[21] = -1;
        bArr[22] = -1;
        bArr[23] = -1;
        bArr[24] = -1;
        bArr[25] = -1;
        bArr[26] = -1;
        bArr[27] = -1;
        bArr[28] = -1;
        bArr[29] = -1;
        bArr[30] = -1;
        bArr[31] = -1;
        bArr[32] = -1;
        bArr[33] = -1;
        bArr[34] = -1;
        bArr[35] = -1;
        bArr[36] = -1;
        bArr[37] = -1;
        bArr[38] = -1;
        bArr[39] = -1;
        bArr[40] = -1;
        bArr[41] = -1;
        bArr[42] = -1;
        bArr[43] = 62;
        bArr[44] = -1;
        bArr[45] = -1;
        bArr[46] = -1;
        bArr[47] = 63;
        bArr[48] = 52;
        bArr[49] = 53;
        bArr[50] = 54;
        bArr[51] = 55;
        bArr[52] = 56;
        bArr[53] = 57;
        bArr[54] = 58;
        bArr[55] = 59;
        bArr[56] = 60;
        bArr[57] = 61;
        bArr[58] = -1;
        bArr[59] = -1;
        bArr[60] = -1;
        bArr[61] = -1;
        bArr[62] = -1;
        bArr[63] = -1;
        bArr[64] = -1;
        bArr[66] = 1;
        bArr[67] = 2;
        bArr[68] = 3;
        bArr[69] = 4;
        bArr[70] = 5;
        bArr[71] = 6;
        bArr[72] = 7;
        bArr[73] = 8;
        bArr[74] = 9;
        bArr[75] = 10;
        bArr[76] = 11;
        bArr[77] = 12;
        bArr[78] = 13;
        bArr[79] = 14;
        bArr[80] = 15;
        bArr[81] = 16;
        bArr[82] = 17;
        bArr[83] = 18;
        bArr[84] = 19;
        bArr[85] = 20;
        bArr[86] = 21;
        bArr[87] = 22;
        bArr[88] = 23;
        bArr[89] = 24;
        bArr[90] = 25;
        bArr[91] = -1;
        bArr[92] = -1;
        bArr[93] = -1;
        bArr[94] = -1;
        bArr[95] = -1;
        bArr[96] = -1;
        bArr[97] = 26;
        bArr[98] = 27;
        bArr[99] = 28;
        bArr[100] = 29;
        bArr[101] = 30;
        bArr[102] = 31;
        bArr[103] = 32;
        bArr[104] = 33;
        bArr[105] = 34;
        bArr[106] = 35;
        bArr[107] = 36;
        bArr[108] = 37;
        bArr[109] = 38;
        bArr[110] = 39;
        bArr[111] = 40;
        bArr[112] = 41;
        bArr[113] = 42;
        bArr[114] = 43;
        bArr[115] = 44;
        bArr[116] = 45;
        bArr[117] = 46;
        bArr[118] = 47;
        bArr[119] = 48;
        bArr[120] = 49;
        bArr[121] = 50;
        bArr[122] = 51;
        bArr[123] = -1;
        bArr[124] = -1;
        bArr[125] = -1;
        bArr[126] = -1;
        bArr[127] = -1;
        base64DecodeChars = bArr;
    }

    private Base64() {
    }

    public static String encode(byte[] data) {
        if (data == null) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        int len = data.length;
        int i = 0;
        while (true) {
            if (i >= len) {
                break;
            }
            int i2 = i + 1;
            int b1 = data[i] & 255;
            if (i2 == len) {
                sb.append(base64EncodeChars[b1 >>> 2]);
                sb.append(base64EncodeChars[(b1 & 3) << 4]);
                sb.append("==");
                break;
            }
            int i3 = i2 + 1;
            int b2 = data[i2] & 255;
            if (i3 == len) {
                sb.append(base64EncodeChars[b1 >>> 2]);
                sb.append(base64EncodeChars[((b1 & 3) << 4) | ((b2 & 240) >>> 4)]);
                sb.append(base64EncodeChars[(b2 & 15) << 2]);
                sb.append("=");
                break;
            }
            int b3 = data[i3] & 255;
            sb.append(base64EncodeChars[b1 >>> 2]);
            sb.append(base64EncodeChars[((b1 & 3) << 4) | ((b2 & 240) >>> 4)]);
            sb.append(base64EncodeChars[((b2 & 15) << 2) | ((b3 & 192) >>> 6)]);
            sb.append(base64EncodeChars[b3 & 63]);
            i = i3 + 1;
        }
        return sb.toString();
    }

    public static byte[] decode(String str) {
        if (str == null) {
            return null;
        }
        return decode(str.getBytes());
    }

    /* JADX WARNING: Removed duplicated region for block: B:15:0x002a A[LOOP:2: B:15:0x002a->B:17:0x0035, LOOP_START, PHI: r6 
      PHI: (r6v1 'i' int) = (r6v0 'i' int), (r6v2 'i' int) binds: [B:13:0x0026, B:17:0x0035] A[DONT_GENERATE, DONT_INLINE]] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0028 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static byte[] decode(byte[] r13) {
        /*
            r12 = 61
            r11 = -1
            if (r13 != 0) goto L_0x0007
            r8 = 0
        L_0x0006:
            return r8
        L_0x0007:
            int r7 = r13.length
            java.io.ByteArrayOutputStream r4 = new java.io.ByteArrayOutputStream
            r4.<init>(r7)
            r5 = 0
        L_0x000e:
            if (r5 < r7) goto L_0x001a
        L_0x0010:
            byte[] r8 = r4.toByteArray()
            r4.close()     // Catch:{ IOException -> 0x008b }
            r4 = 0
            goto L_0x0006
        L_0x0019:
            r5 = r6
        L_0x001a:
            byte[] r9 = com.imadpush.ad.util.Base64.base64DecodeChars
            int r6 = r5 + 1
            byte r10 = r13[r5]
            byte r0 = r9[r10]
            if (r6 >= r7) goto L_0x0026
            if (r0 == r11) goto L_0x0019
        L_0x0026:
            if (r0 != r11) goto L_0x002a
            r5 = r6
            goto L_0x0010
        L_0x002a:
            r5 = r6
            byte[] r9 = com.imadpush.ad.util.Base64.base64DecodeChars
            int r6 = r5 + 1
            byte r10 = r13[r5]
            byte r1 = r9[r10]
            if (r6 >= r7) goto L_0x0037
            if (r1 == r11) goto L_0x002a
        L_0x0037:
            if (r1 != r11) goto L_0x003b
            r5 = r6
            goto L_0x0010
        L_0x003b:
            int r9 = r0 << 2
            r10 = r1 & 48
            int r10 = r10 >>> 4
            r9 = r9 | r10
            r4.write(r9)
        L_0x0045:
            r5 = r6
            int r6 = r5 + 1
            byte r2 = r13[r5]
            if (r2 != r12) goto L_0x0051
            byte[] r8 = r4.toByteArray()
            goto L_0x0006
        L_0x0051:
            byte[] r9 = com.imadpush.ad.util.Base64.base64DecodeChars
            byte r2 = r9[r2]
            if (r6 >= r7) goto L_0x0059
            if (r2 == r11) goto L_0x0045
        L_0x0059:
            if (r2 != r11) goto L_0x005d
            r5 = r6
            goto L_0x0010
        L_0x005d:
            r9 = r1 & 15
            int r9 = r9 << 4
            r10 = r2 & 60
            int r10 = r10 >>> 2
            r9 = r9 | r10
            r4.write(r9)
        L_0x0069:
            r5 = r6
            int r6 = r5 + 1
            byte r3 = r13[r5]
            if (r3 != r12) goto L_0x0075
            byte[] r8 = r4.toByteArray()
            goto L_0x0006
        L_0x0075:
            byte[] r9 = com.imadpush.ad.util.Base64.base64DecodeChars
            byte r3 = r9[r3]
            if (r6 >= r7) goto L_0x007d
            if (r3 == r11) goto L_0x0069
        L_0x007d:
            if (r3 != r11) goto L_0x0081
            r5 = r6
            goto L_0x0010
        L_0x0081:
            r9 = r2 & 3
            int r9 = r9 << 6
            r9 = r9 | r3
            r4.write(r9)
            r5 = r6
            goto L_0x000e
        L_0x008b:
            r9 = move-exception
            goto L_0x0006
        */
        throw new UnsupportedOperationException("Method not decompiled: com.imadpush.ad.util.Base64.decode(byte[]):byte[]");
    }
}
