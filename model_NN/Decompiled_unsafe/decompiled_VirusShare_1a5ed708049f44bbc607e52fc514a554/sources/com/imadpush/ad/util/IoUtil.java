package com.imadpush.ad.util;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.SoftReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class IoUtil {
    public static Map<String, SoftReference<Bitmap>> mImageCache = new HashMap();

    public static Bitmap getFromLocTemp(String mPath) {
        Bitmap mBitmap = null;
        Println.I(mPath);
        String[] mFileName = mPath.split("/");
        if (mFileName[mFileName.length - 1] != null) {
            mBitmap = getImageFromCache(mFileName[mFileName.length - 1]);
            if (!mImageCache.containsKey(mFileName[mFileName.length - 1]) || mBitmap == null) {
                File mFile = new File(String.valueOf(Constant.SAVE_PATH) + "/image/" + mFileName[mFileName.length - 1]);
                BitmapFactory.Options mOpt = new BitmapFactory.Options();
                mOpt.inTempStorage = new byte[16384];
                mOpt.inSampleSize = 1;
                try {
                    mBitmap = BitmapFactory.decodeStream(new FileInputStream(mFile), null, mOpt);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                putImageToCache(mFileName[mFileName.length - 1], mBitmap);
                Println.I("缓存不存在 新建");
            }
        }
        return mBitmap;
    }

    public static HttpURLConnection getStream(String mParam, String mStartNum) {
        URL mUrl = null;
        String mUrlStr = mParam;
        Println.I("下载的url=" + mUrlStr);
        try {
            mUrl = new URL(mUrlStr);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        if (mUrl == null) {
            return null;
        }
        try {
            HttpURLConnection mConnection = (HttpURLConnection) mUrl.openConnection();
            mConnection.setReadTimeout(10000);
            mConnection.setRequestProperty("User-Agent", "NetFox");
            if (mStartNum != null) {
                mConnection.setRequestProperty("RANGE", "bytes=" + mStartNum);
            }
            Println.I("file range=" + mConnection.getRequestProperty("Range"));
            Println.I("file content length" + mConnection.getContentLength());
            return mConnection;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static String getPathFromLoc(String mPath) {
        String[] mFileName = mPath.split("/");
        if (mFileName[mFileName.length - 1] != null) {
            File mFile = new File(String.valueOf(Constant.SAVE_PATH) + "/image/" + mFileName[mFileName.length - 1]);
            if (mFile.exists()) {
                return mFile.getAbsolutePath();
            }
        }
        return "";
    }

    public static String getFileName(String mFileName) {
        return mFileName.substring(mFileName.lastIndexOf("/") + 1, mFileName.lastIndexOf("."));
    }

    public static Bitmap getImageFromCache(String mKey) {
        SoftReference<Bitmap> mBitmapR = mImageCache.get(mKey);
        if (mBitmapR == null || mBitmapR.get() == null) {
            return null;
        }
        return (Bitmap) mBitmapR.get();
    }

    public static void putImageToCache(String mKey, Bitmap mBitmap) {
        mImageCache.put(mKey, new SoftReference(mBitmap));
    }

    public static boolean isLocTempExist(String mPath) {
        if (Equipment.isSDCardAvailable() && !mPath.equals("")) {
            String[] mFileName = mPath.split("/");
            if (mFileName[mFileName.length - 1] == null || !new File(String.valueOf(Constant.SAVE_PATH) + "/image/" + mFileName[mFileName.length - 1]).exists()) {
                return false;
            }
            return true;
        }
        return false;
    }

    public static boolean writeFile(Bitmap mBitmap, String mPath) {
        if (Equipment.isSDCardAvailable()) {
            String[] mFileName = mPath.split("/");
            if (mFileName[mFileName.length - 1] != null) {
                File mFile = new File(String.valueOf(Constant.SAVE_PATH) + "/image/" + mFileName[mFileName.length - 1]);
                if (!mFile.getParentFile().exists()) {
                    mFile.getParentFile().mkdirs();
                }
                if (!mFile.getParentFile().exists() || mFile.exists()) {
                    return true;
                }
                try {
                    mFile.createNewFile();
                    BufferedOutputStream mBos = new BufferedOutputStream(new FileOutputStream(mFile));
                    try {
                        mBitmap.compress(Bitmap.CompressFormat.PNG, 100, mBos);
                        mBos.flush();
                        mBos.close();
                        return true;
                    } catch (FileNotFoundException e) {
                        e = e;
                    } catch (IOException e2) {
                        e = e2;
                        e.printStackTrace();
                        return false;
                    }
                } catch (FileNotFoundException e3) {
                    e = e3;
                    e.printStackTrace();
                    return false;
                } catch (IOException e4) {
                    e = e4;
                    e.printStackTrace();
                    return false;
                }
            }
        }
        return false;
    }

    public static boolean deleteFile(String mPath) {
        if (!Equipment.isSDCardAvailable()) {
            return false;
        }
        File mFile = new File(mPath);
        if (!mFile.exists()) {
            return false;
        }
        mFile.delete();
        return true;
    }

    public static File createFile(String mFilePath) {
        if (!Equipment.isSDCardAvailable()) {
            return null;
        }
        File mFile = new File(mFilePath);
        if (!mFile.exists()) {
            mFile.delete();
        }
        try {
            mFile.createNewFile();
            return mFile;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static File createApkFile(String mFileName) {
        String mFilePath = String.valueOf(Constant.SAVE_PATH) + "/" + mFileName;
        Println.I("-----" + mFilePath);
        File mFile = createFile(mFilePath);
        if (mFile != null) {
            return mFile;
        }
        try {
            return File.createTempFile("temp_" + mFileName, "apk");
        } catch (IOException e) {
            e.printStackTrace();
            return mFile;
        }
    }

    public static File isExistApk(String mFileName) {
        File mFile = new File(String.valueOf(Constant.SAVE_PATH) + "/" + mFileName);
        if (mFile.exists()) {
            return mFile;
        }
        return null;
    }

    public static void openFile(Activity mActivity, File mFile) {
        Intent mIntent = new Intent();
        mIntent.addFlags(268435456);
        mIntent.setAction("android.intent.action.VIEW");
        mIntent.setDataAndType(Uri.fromFile(mFile), "application/vnd.android.package-archive");
        mActivity.startActivity(mIntent);
    }
}
