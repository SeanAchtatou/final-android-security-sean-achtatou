package com.imadpush.ad.util;

import android.util.Log;

public class Println {
    public static boolean DEBUG = true;
    private static String TAG = "JM_APPPOSTER";

    public static void I(String mMessage) {
        if (DEBUG) {
            Log.i(TAG, mMessage);
        }
    }

    public static void W(String mMessage) {
        if (DEBUG) {
            Log.w(TAG, mMessage);
        }
    }

    public static void E(String mMessage) {
        if (DEBUG) {
            Log.e(TAG, mMessage);
        }
    }
}
