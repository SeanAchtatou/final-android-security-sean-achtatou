package com.imadpush.ad.util;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.widget.Button;
import android.widget.ProgressBar;
import com.imadpush.ad.model.PosterInfo;

public class AppDownLoad implements Runnable {
    public static final int RESULT_FILEERRO = 3;
    public static final int RESULT_NETERRO = 0;
    public static final int RESULT_SDCARDERRO = 2;
    public static final int RESULT_SUCCESS = 1;
    private Long dId;
    private String filename;
    private Activity mActivity;
    private boolean mCancle = false;
    private String mDownloadUrl;
    private Handler mHandler;
    private byte[] mLock = new byte[0];
    private Message mMessage;
    /* access modifiers changed from: private */
    public ProgressBar pb;
    private Long psId;
    private PosterInfo psInfo;
    private String userId;

    public AppDownLoad(Activity mActivity2, Handler mHandler2, PosterInfo psInfo2, ProgressBar pb2, String userId2, Long dId2, String filename2, Button btn) {
        this.mActivity = mActivity2;
        this.mHandler = mHandler2;
        this.psInfo = psInfo2;
        this.pb = pb2;
        this.userId = userId2;
        this.dId = dId2;
        String mUserIDStr = "&userid=" + userId2;
        String mDevIDStr = "&dId=" + dId2;
        this.filename = filename2;
        btn.setEnabled(false);
        if (psInfo2.getType() == 1) {
            this.mDownloadUrl = "http://ad.imadpush.com:7500/AppManager/index.php/AppPoster/mgPdownLog/downMore?appid=" + psInfo2.getId() + mUserIDStr + "&imei=" + Equipment.getImei(mActivity2) + mDevIDStr + "&type=1";
        } else if (psInfo2.getType() == 2) {
            this.mDownloadUrl = "http://ad.imadpush.com:7500/AppManager/index.php/AppPoster/mgPdownLog/downMore?appid=" + psInfo2.getId() + mUserIDStr + "&imei=" + Equipment.getImei(mActivity2) + mDevIDStr + "&type=2";
        }
        Println.I(this.mDownloadUrl);
        this.mMessage = new Message();
        this.mMessage.obj = pb2;
    }

    public void cancel() {
        synchronized (this.mLock) {
            this.mCancle = true;
        }
    }

    /* JADX WARN: Type inference failed for: r17v53, types: [java.net.URLConnection] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r21 = this;
            r0 = r21
            android.os.Handler r0 = r0.mHandler
            r17 = r0
            com.imadpush.ad.util.AppDownLoad$1 r18 = new com.imadpush.ad.util.AppDownLoad$1
            r0 = r18
            r1 = r21
            r0.<init>()
            r17.post(r18)
            java.lang.StringBuilder r17 = new java.lang.StringBuilder
            java.lang.String r18 = "下载文件地址:"
            r17.<init>(r18)
            r0 = r21
            java.lang.String r0 = r0.mDownloadUrl
            r18 = r0
            java.lang.StringBuilder r17 = r17.append(r18)
            java.lang.String r17 = r17.toString()
            com.imadpush.ad.util.Println.I(r17)
            r17 = 1024(0x400, float:1.435E-42)
            r0 = r17
            byte[] r4 = new byte[r0]
            r6 = 0
            r9 = 0
            r5 = 0
            r14 = 0
        L_0x0034:
            if (r5 != 0) goto L_0x003c
            r17 = 10
            r0 = r17
            if (r14 < r0) goto L_0x0129
        L_0x003c:
            if (r5 == 0) goto L_0x0289
            java.lang.String r17 = "mConnection 连接成功"
            com.imadpush.ad.util.Println.I(r17)
            java.io.InputStream r9 = r5.getInputStream()     // Catch:{ IOException -> 0x0178 }
            java.lang.String r17 = "Content-Length"
            r0 = r17
            java.lang.String r17 = r5.getHeaderField(r0)     // Catch:{ IOException -> 0x0178 }
            int r17 = java.lang.Integer.parseInt(r17)     // Catch:{ IOException -> 0x0178 }
            java.lang.Integer r15 = java.lang.Integer.valueOf(r17)     // Catch:{ IOException -> 0x0178 }
            if (r15 == 0) goto L_0x0155
            java.lang.StringBuilder r17 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0178 }
            java.lang.String r18 = "mTotalSize(获取的文件长度):"
            r17.<init>(r18)     // Catch:{ IOException -> 0x0178 }
            r0 = r17
            java.lang.StringBuilder r17 = r0.append(r15)     // Catch:{ IOException -> 0x0178 }
            java.lang.String r17 = r17.toString()     // Catch:{ IOException -> 0x0178 }
            com.imadpush.ad.util.Println.I(r17)     // Catch:{ IOException -> 0x0178 }
            r0 = r21
            android.os.Handler r0 = r0.mHandler     // Catch:{ IOException -> 0x0178 }
            r17 = r0
            com.imadpush.ad.util.AppDownLoad$2 r18 = new com.imadpush.ad.util.AppDownLoad$2     // Catch:{ IOException -> 0x0178 }
            r0 = r18
            r1 = r21
            r0.<init>(r15)     // Catch:{ IOException -> 0x0178 }
            r17.post(r18)     // Catch:{ IOException -> 0x0178 }
            if (r9 == 0) goto L_0x0260
            java.io.InputStreamReader r10 = new java.io.InputStreamReader
            r10.<init>(r9)
            r8 = 0
            r0 = r21
            java.lang.String r0 = r0.filename
            r17 = r0
            java.io.File r8 = com.imadpush.ad.util.IoUtil.createApkFile(r17)
            if (r8 == 0) goto L_0x0237
            if (r10 == 0) goto L_0x0237
            r12 = 0
            java.io.FileOutputStream r13 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x019f }
            r13.<init>(r8)     // Catch:{ FileNotFoundException -> 0x019f }
            r12 = r13
        L_0x009c:
            if (r12 == 0) goto L_0x020e
            r11 = 0
            r0 = r21
            byte[] r0 = r0.mLock     // Catch:{ IOException -> 0x01ce }
            r18 = r0
            monitor-enter(r18)     // Catch:{ IOException -> 0x01ce }
        L_0x00a6:
            int r11 = r9.read(r4)     // Catch:{ all -> 0x01cb }
            r17 = -1
            r0 = r17
            if (r11 == r0) goto L_0x00b8
            r0 = r21
            boolean r0 = r0.mCancle     // Catch:{ all -> 0x01cb }
            r17 = r0
            if (r17 == 0) goto L_0x01aa
        L_0x00b8:
            monitor-exit(r18)     // Catch:{ all -> 0x01cb }
            if (r12 == 0) goto L_0x00c1
            r12.flush()     // Catch:{ IOException -> 0x01ce }
            r12.close()     // Catch:{ IOException -> 0x01ce }
        L_0x00c1:
            if (r9 == 0) goto L_0x00c6
            r9.close()     // Catch:{ IOException -> 0x01ce }
        L_0x00c6:
            r0 = r21
            android.os.Handler r0 = r0.mHandler     // Catch:{ IOException -> 0x01ce }
            r17 = r0
            com.imadpush.ad.util.AppDownLoad$4 r18 = new com.imadpush.ad.util.AppDownLoad$4     // Catch:{ IOException -> 0x01ce }
            r0 = r18
            r1 = r21
            r0.<init>()     // Catch:{ IOException -> 0x01ce }
            r19 = 500(0x1f4, double:2.47E-321)
            r17.postDelayed(r18, r19)     // Catch:{ IOException -> 0x01ce }
            r0 = r21
            byte[] r0 = r0.mLock     // Catch:{ IOException -> 0x01ce }
            r18 = r0
            monitor-enter(r18)     // Catch:{ IOException -> 0x01ce }
            r0 = r21
            boolean r0 = r0.mCancle     // Catch:{ all -> 0x0206 }
            r17 = r0
            if (r17 != 0) goto L_0x0127
            r0 = r21
            android.os.Message r0 = r0.mMessage     // Catch:{ all -> 0x0206 }
            r17 = r0
            r19 = 1
            r0 = r19
            r1 = r17
            r1.what = r0     // Catch:{ all -> 0x0206 }
            r0 = r21
            android.os.Handler r0 = r0.mHandler     // Catch:{ all -> 0x0206 }
            r17 = r0
            r0 = r21
            android.os.Message r0 = r0.mMessage     // Catch:{ all -> 0x0206 }
            r19 = r0
            r0 = r17
            r1 = r19
            r0.sendMessage(r1)     // Catch:{ all -> 0x0206 }
            r0 = r21
            com.imadpush.ad.model.PosterInfo r0 = r0.psInfo     // Catch:{ all -> 0x0206 }
            r17 = r0
            int r17 = r17.getType()     // Catch:{ all -> 0x0206 }
            r19 = 1
            r0 = r17
            r1 = r19
            if (r0 != r1) goto L_0x0127
            r0 = r21
            android.app.Activity r0 = r0.mActivity     // Catch:{ all -> 0x0206 }
            r17 = r0
            r0 = r17
            com.imadpush.ad.util.IoUtil.openFile(r0, r8)     // Catch:{ all -> 0x0206 }
        L_0x0127:
            monitor-exit(r18)     // Catch:{ all -> 0x0206 }
        L_0x0128:
            return
        L_0x0129:
            java.net.URL r16 = new java.net.URL     // Catch:{ MalformedURLException -> 0x0141, IOException -> 0x014b }
            r0 = r21
            java.lang.String r0 = r0.mDownloadUrl     // Catch:{ MalformedURLException -> 0x0141, IOException -> 0x014b }
            r17 = r0
            r16.<init>(r17)     // Catch:{ MalformedURLException -> 0x0141, IOException -> 0x014b }
            java.net.URLConnection r17 = r16.openConnection()     // Catch:{ MalformedURLException -> 0x0141, IOException -> 0x014b }
            r0 = r17
            java.net.HttpURLConnection r0 = (java.net.HttpURLConnection) r0     // Catch:{ MalformedURLException -> 0x0141, IOException -> 0x014b }
            r5 = r0
        L_0x013d:
            int r14 = r14 + 1
            goto L_0x0034
        L_0x0141:
            r2 = move-exception
            java.lang.String r17 = "AppDownLoad MalformedURLException"
            com.imadpush.ad.util.Println.E(r17)
            r2.printStackTrace()
            goto L_0x013d
        L_0x014b:
            r2 = move-exception
            java.lang.String r17 = "AppDownLoad IOException"
            com.imadpush.ad.util.Println.E(r17)
            r2.printStackTrace()
            goto L_0x013d
        L_0x0155:
            java.lang.String r17 = "mTotalSize null 获取的文件长度为null"
            com.imadpush.ad.util.Println.E(r17)     // Catch:{ IOException -> 0x0178 }
            r0 = r21
            android.os.Message r0 = r0.mMessage     // Catch:{ IOException -> 0x0178 }
            r17 = r0
            r18 = 0
            r0 = r18
            r1 = r17
            r1.what = r0     // Catch:{ IOException -> 0x0178 }
            r0 = r21
            android.os.Handler r0 = r0.mHandler     // Catch:{ IOException -> 0x0178 }
            r17 = r0
            r0 = r21
            android.os.Message r0 = r0.mMessage     // Catch:{ IOException -> 0x0178 }
            r18 = r0
            r17.sendMessage(r18)     // Catch:{ IOException -> 0x0178 }
            goto L_0x0128
        L_0x0178:
            r2 = move-exception
            java.lang.String r17 = "mInputStream is null 获取输入流为null"
            com.imadpush.ad.util.Println.E(r17)
            r2.printStackTrace()
            r0 = r21
            android.os.Message r0 = r0.mMessage
            r17 = r0
            r18 = 0
            r0 = r18
            r1 = r17
            r1.what = r0
            r0 = r21
            android.os.Handler r0 = r0.mHandler
            r17 = r0
            r0 = r21
            android.os.Message r0 = r0.mMessage
            r18 = r0
            r17.sendMessage(r18)
            goto L_0x0128
        L_0x019f:
            r2 = move-exception
            java.lang.String r17 = "下载文件未发现"
            com.imadpush.ad.util.Println.E(r17)
            r2.printStackTrace()
            goto L_0x009c
        L_0x01aa:
            r17 = 0
            r0 = r17
            r12.write(r4, r0, r11)     // Catch:{ all -> 0x01cb }
            int r6 = r6 + r11
            r7 = r6
            r0 = r21
            android.os.Handler r0 = r0.mHandler     // Catch:{ all -> 0x01cb }
            r17 = r0
            com.imadpush.ad.util.AppDownLoad$3 r19 = new com.imadpush.ad.util.AppDownLoad$3     // Catch:{ all -> 0x01cb }
            r0 = r19
            r1 = r21
            r0.<init>(r7)     // Catch:{ all -> 0x01cb }
            r0 = r17
            r1 = r19
            r0.post(r1)     // Catch:{ all -> 0x01cb }
            goto L_0x00a6
        L_0x01cb:
            r17 = move-exception
            monitor-exit(r18)     // Catch:{ all -> 0x01cb }
            throw r17     // Catch:{ IOException -> 0x01ce }
        L_0x01ce:
            r2 = move-exception
            java.lang.String r17 = "读写本地文件出现IO异常 IOException"
            com.imadpush.ad.util.Println.E(r17)
            r2.printStackTrace()
            if (r9 == 0) goto L_0x01e7
            r12.flush()     // Catch:{ IOException -> 0x0209 }
            r12.close()     // Catch:{ IOException -> 0x0209 }
            r9.close()     // Catch:{ IOException -> 0x0209 }
        L_0x01e2:
            if (r5 == 0) goto L_0x01e7
            r5.disconnect()
        L_0x01e7:
            r0 = r21
            android.os.Message r0 = r0.mMessage
            r17 = r0
            r18 = 3
            r0 = r18
            r1 = r17
            r1.what = r0
            r0 = r21
            android.os.Handler r0 = r0.mHandler
            r17 = r0
            r0 = r21
            android.os.Message r0 = r0.mMessage
            r18 = r0
            r17.sendMessage(r18)
            goto L_0x0128
        L_0x0206:
            r17 = move-exception
            monitor-exit(r18)     // Catch:{ all -> 0x0206 }
            throw r17     // Catch:{ IOException -> 0x01ce }
        L_0x0209:
            r3 = move-exception
            r3.printStackTrace()
            goto L_0x01e2
        L_0x020e:
            java.lang.String r17 = "处理获取本地流的文件失败"
            com.imadpush.ad.util.Println.E(r17)
            if (r5 == 0) goto L_0x0218
            r5.disconnect()
        L_0x0218:
            r0 = r21
            android.os.Message r0 = r0.mMessage
            r17 = r0
            r18 = 3
            r0 = r18
            r1 = r17
            r1.what = r0
            r0 = r21
            android.os.Handler r0 = r0.mHandler
            r17 = r0
            r0 = r21
            android.os.Message r0 = r0.mMessage
            r18 = r0
            r17.sendMessage(r18)
            goto L_0x0128
        L_0x0237:
            java.lang.String r17 = "处理获取文件以及新建文件失败"
            com.imadpush.ad.util.Println.E(r17)
            if (r5 == 0) goto L_0x0241
            r5.disconnect()
        L_0x0241:
            r0 = r21
            android.os.Message r0 = r0.mMessage
            r17 = r0
            r18 = 3
            r0 = r18
            r1 = r17
            r1.what = r0
            r0 = r21
            android.os.Handler r0 = r0.mHandler
            r17 = r0
            r0 = r21
            android.os.Message r0 = r0.mMessage
            r18 = r0
            r17.sendMessage(r18)
            goto L_0x0128
        L_0x0260:
            java.lang.String r17 = "获取连接的数据流错误"
            com.imadpush.ad.util.Println.E(r17)
            if (r5 == 0) goto L_0x026a
            r5.disconnect()
        L_0x026a:
            r0 = r21
            android.os.Message r0 = r0.mMessage
            r17 = r0
            r18 = 0
            r0 = r18
            r1 = r17
            r1.what = r0
            r0 = r21
            android.os.Handler r0 = r0.mHandler
            r17 = r0
            r0 = r21
            android.os.Message r0 = r0.mMessage
            r18 = r0
            r17.sendMessage(r18)
            goto L_0x0128
        L_0x0289:
            java.lang.String r17 = "mConnection连接为null"
            com.imadpush.ad.util.Println.E(r17)
            r0 = r21
            android.os.Message r0 = r0.mMessage
            r17 = r0
            r18 = 0
            r0 = r18
            r1 = r17
            r1.what = r0
            r0 = r21
            android.os.Handler r0 = r0.mHandler
            r17 = r0
            r0 = r21
            android.os.Message r0 = r0.mMessage
            r18 = r0
            r17.sendMessage(r18)
            goto L_0x0128
        */
        throw new UnsupportedOperationException("Method not decompiled: com.imadpush.ad.util.AppDownLoad.run():void");
    }
}
