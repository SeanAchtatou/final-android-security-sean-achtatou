package com.imadpush.ad.util;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

public class AppPackageInfo {
    public static String getPackageName(Activity mActivity) {
        return mActivity.getPackageName();
    }

    public static String getVersionName(Activity mActivity, String mPackageName) {
        try {
            PackageInfo mPackageInfo = mActivity.getPackageManager().getPackageInfo(mPackageName, 16384);
            Println.E("getVersionName:" + mPackageInfo.versionName);
            return mPackageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Println.W("获取包" + mPackageName + "的VersionName失败");
            e.printStackTrace();
            return "";
        }
    }

    public static int getVersionCode(Activity mActivity, String mPackageName) {
        try {
            PackageInfo mPackageInfo = mActivity.getPackageManager().getPackageInfo(mPackageName, 16384);
            Println.E("getVersionCode:" + mPackageInfo.versionCode);
            return mPackageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            Println.W("获取包" + mPackageName + "的VersionCode失败");
            e.printStackTrace();
            return 0;
        }
    }

    public static Long getChannel(Context mActivity) {
        long channel = 123456L;
        try {
            Object value = mActivity.getPackageManager().getApplicationInfo(mActivity.getPackageName(), 128).metaData.get("com.imadpush.ad");
            if (value != null) {
                channel = Long.valueOf(Long.parseLong(value.toString()));
            }
        } catch (Exception e) {
            e.printStackTrace();
            channel = 123456L;
        }
        Println.E("getChannel:" + channel);
        return channel;
    }
}
