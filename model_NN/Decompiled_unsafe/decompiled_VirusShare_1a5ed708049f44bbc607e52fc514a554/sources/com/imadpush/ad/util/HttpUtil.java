package com.imadpush.ad.util;

import android.app.Activity;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

public class HttpUtil {
    private static List<NameValuePair> mParams;

    public static String getHttpJsonString(String mAction, List<NameValuePair> mParams2) {
        String mUrl = URLBuilder.getFormalUrl(mAction);
        if (mUrl != null) {
            Println.I(mUrl);
        }
        if (mUrl != null) {
            HttpPost mHttpPost = new HttpPost(mUrl);
            synchronized (mHttpPost) {
                try {
                    mHttpPost.setEntity(new UrlEncodedFormEntity(mParams2, "UTF-8"));
                    HttpResponse mHttpResponse = new DefaultHttpClient().execute(mHttpPost);
                    if (mHttpResponse.getStatusLine().getStatusCode() == 200) {
                        String mResult = EntityUtils.toString(mHttpResponse.getEntity(), "UTF-8");
                        return mResult;
                    }
                    Println.E("connect server fail statuscode=" + mHttpResponse.getStatusLine().getStatusCode());
                } catch (UnsupportedEncodingException e) {
                    Println.E("HttpUtil 不支持的编码异常");
                    e.printStackTrace();
                } catch (ClientProtocolException e2) {
                    Println.E("HttpUtil 客户端协议异常");
                    e2.printStackTrace();
                } catch (IOException e3) {
                    Println.E("HttpUtil 输入输出异常");
                    e3.printStackTrace();
                }
            }
        }
        return "";
    }

    public static List<NameValuePair> getHttpParams(Activity mActivity) {
        if (mParams == null) {
            mParams = new ArrayList();
            mParams.add(new BasicNameValuePair(Constant.PARAMS_IMEI, Equipment.getImei(mActivity)));
            mParams.add(new BasicNameValuePair(Constant.PARAMS_PACKAGENAME, AppPackageInfo.getPackageName(mActivity)));
            mParams.add(new BasicNameValuePair(Constant.PARAMS_VERSIONNAME, AppPackageInfo.getVersionName(mActivity, AppPackageInfo.getPackageName(mActivity))));
            mParams.add(new BasicNameValuePair(Constant.PARAMS_VERSIONCODE, String.valueOf(AppPackageInfo.getVersionCode(mActivity, AppPackageInfo.getPackageName(mActivity)))));
        }
        return mParams;
    }
}
