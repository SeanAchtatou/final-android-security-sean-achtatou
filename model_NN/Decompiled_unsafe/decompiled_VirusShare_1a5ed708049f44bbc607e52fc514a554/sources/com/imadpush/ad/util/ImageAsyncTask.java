package com.imadpush.ad.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ImageAsyncTask extends AsyncTask<ImageView, Void, Bitmap> {
    private Bitmap mBitmap;
    private ImageView mImageView;

    /* access modifiers changed from: protected */
    public void onPreExecute() {
    }

    /* access modifiers changed from: protected */
    public Bitmap doInBackground(ImageView... params) {
        this.mImageView = params[0];
        if (this.mImageView.getTag() != null) {
            Println.I(this.mImageView.getTag().toString());
            try {
                HttpURLConnection mConnection = (HttpURLConnection) new URL(this.mImageView.getTag().toString()).openConnection();
                mConnection.setDoInput(true);
                mConnection.setConnectTimeout(5000);
                mConnection.connect();
                InputStream mStream = mConnection.getInputStream();
                int mLength = mConnection.getContentLength();
                if (mLength != -1) {
                    int mDestPos = 0;
                    byte[] mBuffer = new byte[512];
                    byte[] mDate = new byte[mLength];
                    while (true) {
                        int mReadPos = mStream.read(mBuffer);
                        if (mReadPos <= 0) {
                            break;
                        }
                        System.arraycopy(mBuffer, 0, mDate, mDestPos, mReadPos);
                        mDestPos += mReadPos;
                    }
                    this.mBitmap = BitmapFactory.decodeByteArray(mDate, 0, mDate.length);
                    IoUtil.writeFile(this.mBitmap, this.mImageView.getTag().toString());
                }
                mStream.close();
            } catch (IOException e) {
                e.printStackTrace();
                Println.E("获取网络图片异常->ImageAsyncTask");
                return null;
            }
        }
        return this.mBitmap;
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Void... progress) {
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Bitmap result) {
        if (this.mBitmap != null) {
            this.mImageView.setImageBitmap(this.mBitmap);
        }
    }
}
