package com.imadpush.ad.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.util.Enumeration;

public class Equipment {
    private static String mImei = null;
    private static String mImsi = null;
    private static String mNetIpAddress = null;
    private static String mNetType = null;

    public static boolean isNetWorkAvailable(Context mContext) {
        ConnectivityManager manager = (ConnectivityManager) mContext.getSystemService("connectivity");
        if (manager.getActiveNetworkInfo() != null) {
            return manager.getActiveNetworkInfo().isAvailable();
        }
        return false;
    }

    public static boolean checkNetWiFi(Context context) {
        if (((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(1).isAvailable()) {
            return true;
        }
        return false;
    }

    public static String getLocalIpAddress() {
        try {
            Enumeration<NetworkInterface> mEnumeration = NetworkInterface.getNetworkInterfaces();
            while (mEnumeration.hasMoreElements()) {
                Enumeration<InetAddress> mEnumeration2 = mEnumeration.nextElement().getInetAddresses();
                while (true) {
                    if (mEnumeration2.hasMoreElements()) {
                        InetAddress mInetAddress = mEnumeration2.nextElement();
                        if (!mInetAddress.isLoopbackAddress()) {
                            Println.I("获取到的内网IP:" + mInetAddress.getHostAddress().toString());
                            return mInetAddress.getHostAddress().toString();
                        }
                    }
                }
            }
        } catch (SocketException ex) {
            Println.W("WifiPreference IpAddress  " + ex.toString());
        }
        return "";
    }

    public static String getNetIpAddress() {
        if (mNetIpAddress != null) {
            return mNetIpAddress;
        }
        try {
            try {
                HttpURLConnection mHttpURLConnection = (HttpURLConnection) new URL("http://fw.qq.com/ipaddress").openConnection();
                if (mHttpURLConnection.getResponseCode() != 200) {
                    return "";
                }
                InputStream mInputStream = mHttpURLConnection.getInputStream();
                BufferedReader mBufferedReader = new BufferedReader(new InputStreamReader(mInputStream, "UTF-8"));
                StringBuilder mStringBuilder = new StringBuilder();
                while (true) {
                    String mLine = mBufferedReader.readLine();
                    if (mLine == null) {
                        break;
                    }
                    mStringBuilder.append(String.valueOf(mLine) + "\n");
                }
                mInputStream.close();
                String[] mIpInfo = mStringBuilder.toString().split(String.valueOf('\"'));
                if (mIpInfo.length < 2) {
                    return "";
                }
                mNetIpAddress = mIpInfo[1];
                Println.I("获取到的外网IP:" + mNetIpAddress);
                return mNetIpAddress;
            } catch (MalformedURLException e) {
                e = e;
                e.printStackTrace();
                return "";
            } catch (IOException e2) {
                e = e2;
                e.printStackTrace();
                return "";
            }
        } catch (MalformedURLException e3) {
            e = e3;
            e.printStackTrace();
            return "";
        } catch (IOException e4) {
            e = e4;
            e.printStackTrace();
            return "";
        }
    }

    public static boolean getAppIsInstalled(Context mContext, String mPackageName) {
        try {
            PackageInfo packageInfo = mContext.getPackageManager().getPackageInfo(mPackageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            Println.W("未安装" + mPackageName);
            return false;
        }
    }

    public static double[] getLoc(Context mContext) {
        double[] mLoc = new double[3];
        LocationManager mLocationManager = (LocationManager) mContext.getSystemService("location");
        Criteria mCriteria = new Criteria();
        mCriteria.setAccuracy(1);
        mCriteria.setAltitudeRequired(true);
        mCriteria.setBearingRequired(false);
        mCriteria.setCostAllowed(true);
        mCriteria.setPowerRequirement(1);
        Location mLocation = mLocationManager.getLastKnownLocation(mLocationManager.getBestProvider(mCriteria, true));
        if (mLocation != null) {
            mLoc[0] = mLocation.getLongitude();
            mLoc[1] = mLocation.getLatitude();
            mLoc[2] = mLocation.getAltitude();
        }
        Println.I("获取的真实地址 longitude=" + mLoc[0] + "  latitude=" + mLoc[1] + "   altitude=" + mLoc[2]);
        return mLoc;
    }

    public static String getImei(Context mContext) {
        if (mImei == null) {
            mImei = ((TelephonyManager) mContext.getSystemService("phone")).getDeviceId();
        }
        if (mImei == null) {
            return Settings.System.getString(mContext.getContentResolver(), "android_id");
        }
        return mImei;
    }

    public static String getImsi(Context mContext) {
        if (mImsi == null) {
            mImsi = ((TelephonyManager) mContext.getSystemService("phone")).getSubscriberId();
        }
        return mImsi == null ? " " : mImsi.substring(0, 5);
    }

    public static String getSDKVersion() {
        return Build.VERSION.SDK;
    }

    public static String getPhoneModel() {
        return Build.MODEL;
    }

    public static String getNetType(Context mContext) {
        if (mNetType == null) {
            NetworkInfo mNetworkInfo = ((ConnectivityManager) mContext.getSystemService("connectivity")).getActiveNetworkInfo();
            mNetType = mNetworkInfo == null ? "" : mNetworkInfo.getTypeName();
        }
        return mNetType;
    }

    public static boolean isSDCardAvailable() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            return true;
        }
        return false;
    }

    public static long getSDCardFreeSpace() {
        StatFs stat = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return ((long) stat.getAvailableBlocks()) * ((long) stat.getBlockSize());
    }

    public static boolean isSDCardEnough(int size) {
        if (getSDCardFreeSpace() / 1048576 > ((long) size)) {
            return true;
        }
        return false;
    }

    public static boolean isSDCardHaveSameFile(String dir, String fileName) {
        if (new File(dir, fileName).exists()) {
            return true;
        }
        return false;
    }

    public static String getSDCardPath() {
        if (isSDCardAvailable()) {
            return Environment.getExternalStorageDirectory().toString();
        }
        return "";
    }
}
