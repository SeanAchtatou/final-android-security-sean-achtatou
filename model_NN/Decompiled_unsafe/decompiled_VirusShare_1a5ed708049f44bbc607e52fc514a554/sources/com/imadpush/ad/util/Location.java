package com.imadpush.ad.util;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

public class Location {
    public String location;
    private Context mActivity;

    public class SCell {
        public int CID;
        public int LAC;
        public int MCC;
        public int MNC;

        public SCell() {
        }
    }

    public class SItude {
        public String latitude;
        public String longitude;

        public SItude() {
        }
    }

    public Location(Context mActivity2) {
        this.mActivity = mActivity2;
        try {
            this.location = getLocation(getItude(getCellInfo()));
            Println.I("获得物理位置：" + this.location);
        } catch (Exception e) {
            Println.E("Error" + e.getMessage());
        }
    }

    private SCell getCellInfo() throws Exception {
        SCell cell = new SCell();
        TelephonyManager mTelNet = (TelephonyManager) this.mActivity.getSystemService("phone");
        GsmCellLocation location2 = (GsmCellLocation) mTelNet.getCellLocation();
        if (location2 == null) {
            throw new Exception("获取基站信息失败");
        }
        String operator = mTelNet.getNetworkOperator();
        int mcc = Integer.parseInt(operator.substring(0, 3));
        int mnc = Integer.parseInt(operator.substring(3));
        int cid = location2.getCid();
        int lac = location2.getLac();
        cell.MCC = mcc;
        cell.MNC = mnc;
        cell.LAC = lac;
        cell.CID = cid;
        return cell;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{org.json.JSONObject.put(java.lang.String, double):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, long):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, int):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, java.lang.Object):org.json.JSONObject throws org.json.JSONException}
      ClspMth{org.json.JSONObject.put(java.lang.String, boolean):org.json.JSONObject throws org.json.JSONException} */
    private SItude getItude(SCell cell) throws Exception {
        SItude itude = new SItude();
        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost("http://www.google.com/loc/json");
        try {
            JSONObject holder = new JSONObject();
            holder.put("version", "1.1.0");
            holder.put("host", "maps.google.com");
            holder.put("address_language", "zh_CN");
            holder.put("request_address", true);
            holder.put("radio_type", "gsm");
            holder.put("carrier", "HTC");
            JSONObject tower = new JSONObject();
            tower.put("mobile_country_code", cell.MCC);
            tower.put("mobile_network_code", cell.MNC);
            tower.put("cell_id", cell.CID);
            tower.put("location_area_code", cell.LAC);
            JSONArray towerarray = new JSONArray();
            towerarray.put(tower);
            holder.put("cell_towers", towerarray);
            post.setEntity(new StringEntity(holder.toString()));
            BufferedReader buffReader = new BufferedReader(new InputStreamReader(client.execute(post).getEntity().getContent()));
            StringBuffer strBuff = new StringBuffer();
            while (true) {
                String result = buffReader.readLine();
                if (result == null) {
                    JSONObject subjosn = new JSONObject(new JSONObject(strBuff.toString()).getString("location"));
                    itude.latitude = subjosn.getString("latitude");
                    itude.longitude = subjosn.getString("longitude");
                    Println.I("latitude: " + itude.latitude + "---longitude: " + itude.longitude);
                    post.abort();
                    return itude;
                }
                strBuff.append(result);
            }
        } catch (Exception e) {
            Println.E(e.getMessage());
            throw new Exception("获取经纬度出现错误:" + e.getMessage());
        } catch (Throwable th) {
            post.abort();
            throw th;
        }
    }

    private String getLocation(SItude itude) throws Exception {
        String urlString = String.format("http://maps.google.cn/maps/geo?key=abcdefg&q=%s,%s", itude.latitude, itude.longitude);
        Println.I("URL:" + urlString);
        HttpClient client = new DefaultHttpClient();
        HttpGet get = new HttpGet(urlString);
        try {
            BufferedReader buffReader = new BufferedReader(new InputStreamReader(client.execute(get).getEntity().getContent()));
            StringBuffer strBuff = new StringBuffer();
            while (true) {
                String result = buffReader.readLine();
                if (result == null) {
                    break;
                }
                strBuff.append(result);
            }
            String resultString = strBuff.toString();
            if (resultString != null && resultString.length() > 0) {
                JSONArray jsonArray = new JSONArray(new JSONObject(resultString).get("Placemark").toString());
                resultString = "";
                for (int i = 0; i < jsonArray.length(); i++) {
                    resultString = jsonArray.getJSONObject(i).getString("address");
                }
            }
            get.abort();
            return resultString;
        } catch (Exception e) {
            throw new Exception("获取物理位置出现错误:" + e.getMessage());
        } catch (Throwable th) {
            get.abort();
            throw th;
        }
    }
}
