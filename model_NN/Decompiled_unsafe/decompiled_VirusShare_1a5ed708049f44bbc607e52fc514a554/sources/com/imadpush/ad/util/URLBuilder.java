package com.imadpush.ad.util;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;

public class URLBuilder {
    public static final String HOST_ADDRESS = "ad.imadpush.com:7500/AppManager/index.php";
    public static final String URL_TEMPLET = "http://{0}/{1}";

    public static String getFormalUrl(String mRequest) {
        String mStr = MessageFormat.format(URL_TEMPLET, HOST_ADDRESS, mRequest);
        try {
            HttpURLConnection mConnection = (HttpURLConnection) new URL(mStr).openConnection();
            mConnection.setConnectTimeout(5000);
            mConnection.connect();
            Println.W(mStr);
            return mStr;
        } catch (MalformedURLException e) {
            Println.W("创建网络连接失败");
            e.printStackTrace();
            Println.W(mStr);
            return null;
        } catch (IOException e2) {
            Println.W("网络连接IO异常");
            e2.printStackTrace();
            Println.W(mStr);
            return null;
        } catch (Throwable th) {
            Println.W(mStr);
            throw th;
        }
    }
}
