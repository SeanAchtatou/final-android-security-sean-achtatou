package com.imadpush.ad.poster.convert;

import com.imadpush.ad.model.JmUser;
import com.imadpush.ad.model.PosterInfo;
import com.imadpush.ad.util.Constant;
import com.imadpush.ad.util.Println;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ConvertToObject {
    public static JmUser toJmUser(String mResult) {
        JSONObject mJo = ConvertToJson.toObject(mResult);
        JmUser dInfo = null;
        if (mJo != null && mJo.optInt("STATUS") == 1) {
            dInfo = new JmUser();
            dInfo.setmJmMoney(mJo.optDouble("JM_MONEY"));
            dInfo.setmPetName(mJo.optString("PET_NAME"));
            JSONObject mUserInfo = mJo.optJSONObject("USER_INFO");
            if (mUserInfo != null) {
                dInfo.setmName(mUserInfo.optString("USER_NAME"));
                dInfo.setmId(mUserInfo.optLong("USER_ID"));
                dInfo.setmUserUNI(mUserInfo.optString("USER_UNI"));
            }
        }
        return dInfo;
    }

    public static List<PosterInfo> toPosterInfoList(String mResult) {
        JSONArray mJa = ConvertToJson.toArray(mResult);
        List<PosterInfo> mList = null;
        if (mJa != null) {
            mList = new ArrayList<>();
            for (int i = 0; i < mJa.length(); i++) {
                JSONObject mJo = null;
                try {
                    mJo = mJa.getJSONObject(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Println.E("JSONObject获取数据失败->ConvertToObject->toAppInfoList");
                }
                if (mJo != null) {
                    PosterInfo psInfo = new PosterInfo();
                    psInfo.setId(Long.valueOf(mJo.optLong("id")));
                    psInfo.setName(mJo.optString(Constant.PARAMS_APPNAME));
                    psInfo.setDescription(mJo.optString(Constant.PARAMS_APPDESCR));
                    psInfo.setContent(mJo.optString("content"));
                    psInfo.setImg_url(mJo.optString("img_url"));
                    psInfo.setImgs_url1(mJo.optString("imgs_url1"));
                    psInfo.setImgs_url2(mJo.optString("imgs_url2"));
                    psInfo.setImgs_url3(mJo.optString("imgs_url3"));
                    psInfo.setImgs_url4(mJo.optString("imgs_url4"));
                    psInfo.setImgs_url5(mJo.optString("imgs_url5"));
                    psInfo.setFile_url(mJo.optString(Constant.PARAMS_APPURL));
                    psInfo.setPackagename(mJo.optString(Constant.PARAMS_PACKAGENAME));
                    psInfo.setNet_url(mJo.optString("net_url"));
                    psInfo.setPlay_url(mJo.optString("play_url"));
                    psInfo.setType(mJo.optInt("type"));
                    psInfo.setPoint(mJo.optInt("point"));
                    psInfo.setFile_size(mJo.optInt(Constant.PARAMS_APPSIZE));
                    psInfo.setCondition(mJo.optInt("condition"));
                    mList.add(psInfo);
                }
            }
        }
        return mList;
    }

    public static Date toDate(String mStrDate) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(mStrDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String toDateString(Date mDate) {
        return new SimpleDateFormat("yyyy年MM月dd日").format(mDate);
    }
}
