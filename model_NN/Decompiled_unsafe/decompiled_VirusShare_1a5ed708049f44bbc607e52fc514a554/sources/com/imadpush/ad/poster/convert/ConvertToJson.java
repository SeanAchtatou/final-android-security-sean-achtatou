package com.imadpush.ad.poster.convert;

import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class ConvertToJson {
    public static JSONObject toObject(String mContent) {
        if (mContent == null) {
            return null;
        }
        try {
            if (!mContent.equals("")) {
                return new JSONObject(mContent);
            }
            return null;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static JSONObject toObject(JSONTokener mTokener) {
        try {
            return new JSONObject(mTokener);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static JSONObject toObject(Map<String, String> mMap) {
        return new JSONObject(mMap);
    }

    public static JSONObject toObject(JSONObject mJsonObject, String[] mStringArray) {
        try {
            return new JSONObject(mJsonObject, mStringArray);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static JSONArray toArray(String mContent) {
        try {
            return new JSONArray(mContent);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
