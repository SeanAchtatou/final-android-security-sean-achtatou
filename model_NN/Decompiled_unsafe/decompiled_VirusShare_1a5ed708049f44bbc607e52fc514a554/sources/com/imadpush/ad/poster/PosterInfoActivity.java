package com.imadpush.ad.poster;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.imadpush.ad.model.PosterInfo;
import com.imadpush.ad.util.AppDownLoad;
import com.imadpush.ad.util.Constant;
import com.imadpush.ad.util.Equipment;
import com.imadpush.ad.util.HttpUtil;
import com.imadpush.ad.util.ImageAsyncTask;
import com.imadpush.ad.util.IoUtil;
import com.imadpush.ad.util.MyLayout;
import com.imadpush.ad.util.Println;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class PosterInfoActivity extends Activity {
    public int condition;
    private String content;
    public Context context = this;
    /* access modifiers changed from: private */
    public Long dId;
    private String description;
    /* access modifiers changed from: private */
    public String filename;
    /* access modifiers changed from: private */
    public boolean flag = false;
    private Handler handler;
    private String imgs_url1;
    private String imgs_url2;
    private String imgs_url3;
    private String imgs_url4;
    private String imgs_url5;
    private Intent intent;
    /* access modifiers changed from: private */
    public File mFile;
    public MyLayout myLayout;
    private Notification n;
    /* access modifiers changed from: private */
    public String net_url;
    /* access modifiers changed from: private */
    public NotificationManager nm;
    public PackageReceiver packageReceiver;
    private ProgressBar pb;
    /* access modifiers changed from: private */
    public Long psId;
    /* access modifiers changed from: private */
    public PosterInfo psInfo;
    private String ps_imagurl;
    private String[] s1;
    private String[] s2;
    /* access modifiers changed from: private */
    public String userId;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.myLayout = new MyLayout(this, null);
        this.pb = new ProgressBar(this);
        this.nm = AlarmService.nm;
        this.n = AlarmService.n;
        this.intent = getIntent();
        this.userId = this.intent.getExtras().getString("userId");
        this.dId = Long.valueOf(this.intent.getExtras().getLong("dId"));
        this.psInfo = (PosterInfo) this.intent.getExtras().getSerializable("push");
        this.ps_imagurl = this.psInfo.getImg_url();
        this.content = this.psInfo.getContent();
        this.description = this.psInfo.getDescription();
        this.imgs_url1 = this.psInfo.getImgs_url1();
        this.imgs_url2 = this.psInfo.getImgs_url2();
        this.imgs_url3 = this.psInfo.getImgs_url3();
        this.imgs_url4 = this.psInfo.getImgs_url4();
        this.imgs_url5 = this.psInfo.getImgs_url5();
        this.condition = this.psInfo.getCondition();
        this.net_url = this.psInfo.getNet_url();
        this.psId = this.psInfo.getId();
        this.s1 = this.psInfo.getFile_url().split("/");
        this.s2 = this.psInfo.getPlay_url().split("/");
        if (this.psInfo.getType() == 1) {
            this.filename = this.s1[this.s1.length - 1];
        } else if (this.psInfo.getType() == 2) {
            this.filename = this.s2[this.s2.length - 1];
        }
        this.packageReceiver = new PackageReceiver();
        IntentFilter mFilter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        mFilter.addDataScheme("package");
        registerReceiver(this.packageReceiver, mFilter);
        this.mFile = IoUtil.isExistApk(this.filename);
        if (this.mFile == null || this.psInfo.getType() != 1) {
            setContentView(this.myLayout);
            this.myLayout.img.setImageBitmap(getBmp("default_icon.png"));
            if (isExist(this.ps_imagurl)) {
                showImg(this.myLayout.img, this.ps_imagurl);
            }
            if (isExist(this.imgs_url1)) {
                showImg(this.myLayout.img1, this.imgs_url1);
                this.myLayout.img1.setVisibility(0);
            }
            if (isExist(this.imgs_url2)) {
                showImg(this.myLayout.img2, this.imgs_url2);
                this.myLayout.img2.setVisibility(0);
            }
            if (isExist(this.imgs_url3)) {
                showImg(this.myLayout.img3, this.imgs_url3);
                this.myLayout.img3.setVisibility(0);
            }
            if (isExist(this.imgs_url4)) {
                showImg(this.myLayout.img4, this.imgs_url4);
                this.myLayout.img4.setVisibility(0);
            }
            if (isExist(this.imgs_url5)) {
                showImg(this.myLayout.img5, this.imgs_url5);
                this.myLayout.img5.setVisibility(0);
            }
            if (this.description.equals("null") || this.description.length() == 0) {
                this.myLayout.tv_dec.setText("");
            } else {
                this.myLayout.tv_dec.setText(this.description);
            }
            if (this.description.equals("null") || this.content.length() == 0) {
                this.myLayout.tv_info.setText("");
            } else {
                this.myLayout.tv_info.setText(this.content);
            }
            this.handler = new Handler() {
                public void handleMessage(Message msg) {
                    super.handleMessage(msg);
                    switch (msg.what) {
                        case 0:
                            Toast.makeText(PosterInfoActivity.this, "网络链接出现问题,请重试.", 1).show();
                            break;
                        case 1:
                            Toast.makeText(PosterInfoActivity.this, "下载完成", 1).show();
                            break;
                        case 2:
                            Toast.makeText(PosterInfoActivity.this, "内存卡空间不够或内存卡不存在,请插入内存卡后重试.", 1).show();
                            break;
                        case 3:
                            Toast.makeText(PosterInfoActivity.this, "读写文件失败,请确保内存卡可以用.", 1).show();
                            break;
                    }
                    PosterInfoActivity.this.myLayout.btn_down.setEnabled(true);
                }
            };
            if (this.psInfo.getType() == 0) {
                this.myLayout.btn_down.setText("打开");
            } else if (this.psInfo.getType() == 1) {
                choiseDown();
            } else if (this.psInfo.getType() == 2) {
                this.myLayout.btn_down.setText("播放");
                if (this.mFile == null) {
                    choiseDown();
                }
            }
            this.myLayout.btn_cancel.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    PosterInfoActivity.this.nm.cancel(1);
                    PosterInfoActivity.this.updateNotify();
                    PosterInfoActivity.this.finish();
                }
            });
            this.myLayout.btn_down.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (PosterInfoActivity.this.psInfo.getType() == 0) {
                        Println.E(PosterInfoActivity.this.net_url);
                        Intent intent = new Intent();
                        intent.setAction("android.intent.action.VIEW");
                        intent.setData(Uri.parse(PosterInfoActivity.this.net_url));
                        PosterInfoActivity.this.startActivity(intent);
                    } else if (PosterInfoActivity.this.psInfo.getType() == 1) {
                        PosterInfoActivity.this.mFile = IoUtil.isExistApk(PosterInfoActivity.this.filename);
                        if (PosterInfoActivity.this.mFile != null) {
                            IoUtil.openFile(PosterInfoActivity.this, PosterInfoActivity.this.mFile);
                        } else {
                            PosterInfoActivity.this.downLoad();
                        }
                    } else if (PosterInfoActivity.this.psInfo.getType() == 2) {
                        PosterInfoActivity.this.mFile = IoUtil.isExistApk(PosterInfoActivity.this.filename);
                        if (PosterInfoActivity.this.mFile != null) {
                            Uri data = Uri.parse(String.valueOf(Constant.SAVE_PATH) + "/" + PosterInfoActivity.this.filename);
                            Intent intent2 = new Intent();
                            intent2.setAction("android.intent.action.VIEW");
                            intent2.setDataAndType(data, "video/*");
                            PosterInfoActivity.this.startActivity(intent2);
                            return;
                        }
                        Println.E("本地没有该视频开始下载");
                        PosterInfoActivity.this.downLoad();
                    }
                }
            });
            new PsLookLog().execute(null, 0, null);
            return;
        }
        this.flag = true;
        new PsLookLog().execute(null, 0, null);
        IoUtil.openFile(this, this.mFile);
        this.nm.cancel(1);
    }

    public class PsLookLog extends AsyncTask<Object, Integer, String> {
        public PsLookLog() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Object... params) {
            List<NameValuePair> mParams = new ArrayList<>();
            mParams.add(new BasicNameValuePair("psid", String.valueOf(PosterInfoActivity.this.psId)));
            mParams.add(new BasicNameValuePair("uid", PosterInfoActivity.this.userId));
            mParams.add(new BasicNameValuePair("dId", String.valueOf(PosterInfoActivity.this.dId)));
            String mResult = HttpUtil.getHttpJsonString("AppPoster/mgPlookLog/look", mParams);
            Println.I("成功后返回的信息:" + mResult);
            return mResult;
        }
    }

    public class PsInstallLog extends AsyncTask<Object, Integer, String> {
        public PsInstallLog() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Object... params) {
            List<NameValuePair> mParams = new ArrayList<>();
            mParams.add(new BasicNameValuePair("psid", String.valueOf(PosterInfoActivity.this.psId)));
            mParams.add(new BasicNameValuePair("uid", PosterInfoActivity.this.userId));
            mParams.add(new BasicNameValuePair("dId", String.valueOf(PosterInfoActivity.this.dId)));
            String mResult = HttpUtil.getHttpJsonString("AppPoster/mgPinstallLog/install", mParams);
            Println.I("成功后返回的信息:" + mResult);
            return mResult;
        }
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        return new AlertDialog.Builder(this).setIcon(17301659).setTitle("网络提示").setMessage("你使用的不是WIFI,是否继续下载！").setNegativeButton("取消", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                PosterInfoActivity.this.dismissDialog(1);
            }
        }).setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                PosterInfoActivity.this.downLoad();
            }
        }).create();
    }

    public class PackageReceiver extends BroadcastReceiver {
        public PackageReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("android.intent.action.PACKAGE_ADDED")) {
                Intent mIntent = context.getPackageManager().getLaunchIntentForPackage(intent.getDataString().substring(8));
                if (mIntent != null) {
                    PosterInfoActivity.this.startActivity(mIntent);
                }
                PosterInfoActivity.this.nm.cancel(1);
                new PsInstallLog().execute(null, 0, null);
                if (PosterInfoActivity.this.flag) {
                    PosterInfoActivity.this.finish();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        unregisterReceiver(this.packageReceiver);
        super.onDestroy();
    }

    public void wifiTs() {
        if (Equipment.checkNetWiFi(this.context)) {
            downLoad();
        } else {
            showDialog(1);
        }
    }

    public void choiseDown() {
        if (this.condition == 1) {
            Println.I("使用直接下载");
            downLoad();
        } else if (this.condition == 2) {
            Println.I("检测到使用wifi下载");
            if (Equipment.checkNetWiFi(this.context)) {
                downLoad();
            }
        }
    }

    public void downLoad() {
        new Thread(new AppDownLoad(this, this.handler, this.psInfo, this.pb, this.userId, this.dId, this.filename, this.myLayout.btn_down)).start();
    }

    public boolean isExist(String img) {
        String[] mFileName = img.split("/");
        if (!mFileName[mFileName.length - 1].equals("images")) {
            return true;
        }
        return false;
    }

    public Bitmap getBmp(String imgStr) {
        try {
            return BitmapFactory.decodeStream(getAssets().open(imgStr));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void showImg(ImageView imgeView, String imgurl) {
        if (!IoUtil.isLocTempExist(imgurl)) {
            imgeView.setTag(imgurl);
            try {
                new ImageAsyncTask().execute(imgeView);
                imgeView.setDrawingCacheEnabled(true);
            } catch (IllegalStateException e) {
                e.printStackTrace();
                Println.E("异步任务加载失败->BookListAdapter");
            }
        } else {
            imgeView.setImageBitmap(IoUtil.getFromLocTemp(imgurl));
        }
    }

    public void updateNotify() {
        this.nm = (NotificationManager) getSystemService("notification");
        this.n = new Notification();
        this.n.icon = 17301545;
        this.n.when = System.currentTimeMillis();
        this.n.setLatestEventInfo(this, this.psInfo.getName(), this.description, PendingIntent.getActivity(this, 0, this.intent, 134217728));
        this.nm.notify(1, this.n);
    }
}
