package com.imadpush.ad.poster;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import com.imadpush.ad.util.ImageAsyncTask;
import com.imadpush.ad.util.IoUtil;
import com.imadpush.ad.util.Println;
import java.util.List;

public class PosterAdapter extends BaseAdapter {
    private Context context;
    private List<String> imags;
    private ViewHolder viewHolder = new ViewHolder();

    class ViewHolder {
        ImageView imageView;

        ViewHolder() {
        }
    }

    public PosterAdapter(PosterInfoActivity posterInfoActivity, List<String> imags2) {
        this.context = posterInfoActivity;
        this.imags = imags2;
    }

    public int getCount() {
        return this.imags.size();
    }

    public Object getItem(int position) {
        return this.imags.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        this.viewHolder.imageView = new ImageView(this.context);
        this.viewHolder.imageView.setLayoutParams(new Gallery.LayoutParams(220, 300));
        if (!IoUtil.isLocTempExist(this.imags.get(position))) {
            this.viewHolder.imageView.setTag(this.imags.get(position));
            try {
                new ImageAsyncTask().execute(this.viewHolder.imageView);
                this.viewHolder.imageView.setDrawingCacheEnabled(true);
            } catch (IllegalStateException e) {
                e.printStackTrace();
                Println.E("异步任务加载失败->BookListAdapter");
            }
        } else {
            this.viewHolder.imageView.setImageBitmap(IoUtil.getFromLocTemp(this.imags.get(position)));
        }
        return this.viewHolder.imageView;
    }
}
