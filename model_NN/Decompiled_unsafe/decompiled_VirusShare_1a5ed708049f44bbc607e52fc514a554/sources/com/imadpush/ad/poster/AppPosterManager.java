package com.imadpush.ad.poster;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.DisplayMetrics;
import com.imadpush.ad.model.JmUser;
import com.imadpush.ad.poster.convert.ConvertToObject;
import com.imadpush.ad.util.AppPackageInfo;
import com.imadpush.ad.util.Constant;
import com.imadpush.ad.util.Equipment;
import com.imadpush.ad.util.HttpUtil;
import com.imadpush.ad.util.Println;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class AppPosterManager {
    public static int ID = 0;
    public static AlarmManager am;
    public static PendingIntent appIntent;
    public static Handler handler;
    public static Activity mActivity;
    private static AppPosterManager mAppPosterManager;
    public static JmUser mJmUser;
    public Long Channel = 123456L;
    private int height;
    public Notification n;
    public NotificationManager nm;
    /* access modifiers changed from: private */
    public String pixels;
    private int with;

    public AppPosterManager(Activity mActivity2, boolean debug) {
        setPoster(mActivity2, debug);
    }

    public AppPosterManager(Activity mActivity2) {
        setPoster(mActivity2, false);
    }

    private void setPoster(Activity mActivity2, boolean debug) {
        init(mActivity2, debug);
    }

    public void init(Activity mActivity2, boolean mDebug) {
        Println.DEBUG = mDebug;
        mActivity = mActivity2;
        DisplayMetrics dm = new DisplayMetrics();
        mActivity2.getWindowManager().getDefaultDisplay().getMetrics(dm);
        this.with = dm.widthPixels;
        this.height = dm.heightPixels;
        this.pixels = String.valueOf(this.with) + "*" + this.height;
        this.Channel = AppPackageInfo.getChannel(mActivity2);
        new LoadUserInfo().execute(null, 0, null);
    }

    public void addPoster() {
        setAlarmManager();
    }

    public class LoadUserInfo extends AsyncTask<Object, Integer, String> {
        public LoadUserInfo() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Object... params) {
            List<NameValuePair> mParams = HttpUtil.getHttpParams(AppPosterManager.mActivity);
            mParams.add(new BasicNameValuePair("dId", String.valueOf(AppPosterManager.this.Channel)));
            mParams.add(new BasicNameValuePair("version", Constant.VERSION));
            mParams.add(new BasicNameValuePair("pixels", AppPosterManager.this.pixels));
            mParams.add(new BasicNameValuePair("IMEI", Equipment.getImei(AppPosterManager.mActivity)));
            mParams.add(new BasicNameValuePair("IMSI", Equipment.getImsi(AppPosterManager.mActivity)));
            mParams.add(new BasicNameValuePair("IP", Equipment.getLocalIpAddress()));
            mParams.add(new BasicNameValuePair("MODEL", Equipment.getPhoneModel()));
            mParams.add(new BasicNameValuePair("login_way", "1"));
            mParams.add(new BasicNameValuePair("user_detal_info", "1"));
            String mResult = HttpUtil.getHttpJsonString("user/register/register", mParams);
            Println.I("获取快速登录的用户信息:" + mResult);
            if (mResult == null || mResult.equals("")) {
                return null;
            }
            AppPosterManager.mJmUser = ConvertToObject.toJmUser(mResult);
            Println.I("用户Id:" + AppPosterManager.mJmUser.getmId());
            AppPosterManager.this.addPoster();
            return null;
        }
    }

    private static void setAlarmManager() {
        appIntent = PendingIntent.getService(mActivity, 0, new Intent(mActivity, AlarmService.class), 0);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        Println.I(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date(calendar.getTimeInMillis())));
        am = (AlarmManager) mActivity.getSystemService("alarm");
        am.setRepeating(0, calendar.getTimeInMillis() + 30000, 900000, appIntent);
    }
}
