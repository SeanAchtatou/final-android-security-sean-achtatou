package com.imadpush.ad.poster;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.AsyncTask;
import android.os.IBinder;
import com.imadpush.ad.model.JmUser;
import com.imadpush.ad.model.PosterInfo;
import com.imadpush.ad.poster.convert.ConvertToObject;
import com.imadpush.ad.util.AppPackageInfo;
import com.imadpush.ad.util.Constant;
import com.imadpush.ad.util.Equipment;
import com.imadpush.ad.util.HttpUtil;
import com.imadpush.ad.util.Location;
import com.imadpush.ad.util.Println;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class AlarmService extends Service {
    public static Notification n;
    public static NotificationManager nm;
    /* access modifiers changed from: private */
    public Long dId;
    /* access modifiers changed from: private */
    public String imei;
    private JmUser mJmUser;
    /* access modifiers changed from: private */
    public String packageName;
    /* access modifiers changed from: private */
    public Long psId;
    /* access modifiers changed from: private */
    public String userId;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        this.dId = AppPackageInfo.getChannel(this);
        this.imei = Equipment.getImei(this);
        this.userId = this.imei;
        Println.I(this.imei);
        initNotify();
        new LoadPosterInfo().execute(null, 0, null);
    }

    public void initNotify() {
        nm = (NotificationManager) getSystemService("notification");
        n = new Notification();
        n.icon = 17301545;
        n.defaults |= 1;
        n.defaults |= 2;
        n.flags |= 2;
        n.flags |= 32;
        n.when = System.currentTimeMillis();
    }

    public class LoadPosterInfo extends AsyncTask<Object, Integer, String> {
        public LoadPosterInfo() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(Object... params) {
            List<NameValuePair> mParams = new ArrayList<>();
            mParams.add(new BasicNameValuePair(Constant.PARAMS_IMEI, AlarmService.this.imei));
            mParams.add(new BasicNameValuePair("dId", String.valueOf(AlarmService.this.dId)));
            mParams.add(new BasicNameValuePair("version", Constant.VERSION));
            mParams.add(new BasicNameValuePair("location", new Location(AlarmService.this).location));
            String mResult = HttpUtil.getHttpJsonString("AppPoster/mgPush/getPush", mParams);
            Println.I("获取用户Id:" + AlarmService.this.userId);
            Println.I("获取推送广告信息:" + mResult);
            if (mResult == null || mResult.equals("")) {
                return null;
            }
            sendNotification(ConvertToObject.toPosterInfoList(mResult));
            return null;
        }

        public class PsSendLog extends AsyncTask<Object, Integer, String> {
            public PsSendLog() {
            }

            /* access modifiers changed from: protected */
            public String doInBackground(Object... params) {
                List<NameValuePair> mParams = new ArrayList<>();
                mParams.add(new BasicNameValuePair("psid", String.valueOf(AlarmService.this.psId)));
                mParams.add(new BasicNameValuePair("uid", AlarmService.this.userId));
                mParams.add(new BasicNameValuePair("dId", String.valueOf(AlarmService.this.dId)));
                String mResult = HttpUtil.getHttpJsonString("AppPoster/mgPsendLog/send", mParams);
                Println.I("成功后返回的信息:" + mResult);
                return mResult;
            }
        }

        private void sendNotification(List<PosterInfo> adlist) {
            int i = 0;
            while (i < adlist.size()) {
                AlarmService.this.psId = adlist.get(i).getId();
                AlarmService.this.packageName = adlist.get(i).getPackagename();
                if (adlist.get(i).getType() != 1 || !isAvilible(AlarmService.this, AlarmService.this.packageName)) {
                    Intent intent = new Intent(AlarmService.this, PosterInfoActivity.class);
                    intent.putExtra("notifyId", 1);
                    intent.putExtra("userId", AlarmService.this.userId);
                    intent.putExtra("dId", AlarmService.this.dId);
                    intent.putExtra("push", adlist.get(i));
                    sendIntent(adlist.get(i), intent, 1);
                    new PsSendLog().execute(null, 0, null);
                    i++;
                } else {
                    Println.I("手机已经存在该应用");
                    new ReturnInfo().execute(null, 0, null);
                    return;
                }
            }
        }

        private void sendIntent(PosterInfo psInfo, Intent intent, int id) {
            String title = psInfo.getName();
            String description = psInfo.getDescription();
            AlarmService.n.tickerText = title;
            if (description.equals("null")) {
                description = "";
            }
            AlarmService.n.setLatestEventInfo(AlarmService.this, title, description, PendingIntent.getActivity(AlarmService.this, id, intent, 134217728));
            AlarmService.nm.notify(id, AlarmService.n);
        }

        public class ReturnInfo extends AsyncTask<Object, Integer, String> {
            public ReturnInfo() {
            }

            /* access modifiers changed from: protected */
            public String doInBackground(Object... params) {
                List<NameValuePair> mParams = new ArrayList<>();
                mParams.add(new BasicNameValuePair("psid", String.valueOf(AlarmService.this.psId)));
                mParams.add(new BasicNameValuePair("uid", AlarmService.this.userId));
                mParams.add(new BasicNameValuePair("dId", String.valueOf(AlarmService.this.dId)));
                String mResult = HttpUtil.getHttpJsonString("AppPoster/returnInfo/info", mParams);
                Println.I("成功后返回的信息:" + mResult);
                return mResult;
            }
        }

        private boolean isAvilible(Context context, String packageName) {
            List<PackageInfo> pinfo = context.getPackageManager().getInstalledPackages(0);
            List<String> pName = new ArrayList<>();
            if (pinfo != null) {
                for (int i = 0; i < pinfo.size(); i++) {
                    pName.add(pinfo.get(i).packageName);
                }
            }
            return pName.contains(packageName);
        }
    }
}
