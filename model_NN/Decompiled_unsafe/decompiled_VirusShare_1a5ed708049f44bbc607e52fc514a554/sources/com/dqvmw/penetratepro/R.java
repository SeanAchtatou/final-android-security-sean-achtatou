package com.dqvmw.penetratepro;

public final class R {

    public static final class attr {
    }

    public static final class drawable {
        public static final int ic_wifi_signal_1 = 2130837504;
        public static final int ic_wifi_signal_2 = 2130837505;
        public static final int ic_wifi_signal_3 = 2130837506;
        public static final int ic_wifi_signal_4 = 2130837507;
        public static final int icon = 2130837508;
        public static final int logo = 2130837509;
        public static final int wifi_signal_open = 2130837510;
    }

    public static final class id {
        public static final int keys = 2131165187;
        public static final int layout_info = 2131165191;
        public static final int logo = 2131165186;
        public static final int reversible = 2131165190;
        public static final int row_layout = 2131165189;
        public static final int row_right_bssid = 2131165193;
        public static final int row_right_level = 2131165194;
        public static final int row_right_ssid = 2131165192;
        public static final int share = 2131165188;
        public static final int text = 2131165184;
        public static final int warningText = 2131165185;
    }

    public static final class layout {
        public static final int bottom_ad_hook = 2130903040;
        public static final int main = 2130903041;
        public static final int results_dialog = 2130903042;
        public static final int row = 2130903043;
    }

    public static final class string {
        public static final int about_penetrate = 2131034112;
        public static final int aboutdialog_text = 2131034139;
        public static final int aboutdialog_title = 2131034138;
        public static final int app_name = 2131034120;
        public static final int dialog_close = 2131034135;
        public static final int dialog_dictionariesrequired = 2131034132;
        public static final int dialog_download = 2131034133;
        public static final int dialog_download_error = 2131034160;
        public static final int dialog_download_error_already_exists = 2131034164;
        public static final int dialog_download_error_failed = 2131034161;
        public static final int dialog_download_error_space = 2131034162;
        public static final int dialog_download_error_unwritable = 2131034163;
        public static final int dialog_download_progress_message = 2131034159;
        public static final int dialog_enable_wifi_message = 2131034166;
        public static final int dialog_enable_wifi_no = 2131034168;
        public static final int dialog_enable_wifi_title = 2131034165;
        public static final int dialog_enable_wifi_yes = 2131034167;
        public static final int dialog_instructions = 2131034134;
        public static final int dialog_nokeysfound = 2131034130;
        public static final int dialog_notreversible = 2131034131;
        public static final int dialog_search = 2131034136;
        public static final int dialog_website = 2131034137;
        public static final int download_dialog_abort = 2131034158;
        public static final int download_dialog_confirm = 2131034157;
        public static final int download_dialog_message = 2131034156;
        public static final int download_dialog_title = 2131034155;
        public static final int menu_about = 2131034127;
        public static final int menu_refresh = 2131034126;
        public static final int menu_search_ssid = 2131034128;
        public static final int menu_settings = 2131034129;
        public static final int oopsdialog_title = 2131034140;
        public static final int opendialog_link = 2131034149;
        public static final int opendialog_text = 2131034148;
        public static final int opendialog_title = 2131034147;
        public static final int preference_about = 2131034113;
        public static final int preference_category_about = 2131034114;
        public static final int preference_category_password_generation = 2131034119;
        public static final int preference_download_dictionaries = 2131034154;
        public static final int preference_download_dictionaries_key = 2131034153;
        public static final int preference_thomson_mode = 2131034118;
        public static final int preference_thomson_mode_dict = 2131034116;
        public static final int preference_thomson_mode_web = 2131034117;
        public static final int preference_toggle_thomson_mode = 2131034115;
        public static final int processing_results = 2131034125;
        public static final int querydialog_enter_ssid = 2131034146;
        public static final int querydialog_title = 2131034145;
        public static final int resultsdialog_instructions = 2131034144;
        public static final int resultsdialog_resultstitle = 2131034143;
        public static final int resultsdialog_share_prefix = 2131034141;
        public static final int resultsdialog_sharekeys = 2131034142;
        public static final int statusbar_enablewifi = 2131034122;
        public static final int statusbar_enablingwifi = 2131034123;
        public static final int statusbar_result = 2131034121;
        public static final int statusbar_scanning = 2131034124;
        public static final int url_dictionaries = 2131034151;
        public static final int url_main = 2131034150;
        public static final int warning_thomson = 2131034152;
    }

    public static final class style {
        public static final int TopBar = 2131099648;
        public static final int TopBarText = 2131099649;
        public static final int WarningText = 2131099650;
    }

    public static final class xml {
        public static final int preferences = 2130968576;
    }
}
