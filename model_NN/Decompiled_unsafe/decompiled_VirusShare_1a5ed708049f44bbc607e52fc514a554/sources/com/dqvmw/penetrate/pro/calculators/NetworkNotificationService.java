package com.dqvmw.penetrate.pro.calculators;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.IBinder;
import android.widget.Toast;
import com.dqvmw.penetrate.Penetrate;
import com.dqvmw.penetrate.lib.core.ApInfo;
import com.dqvmw.penetrate.lib.core.ReverseBroker;
import com.dqvmw.penetratepro.R;
import java.util.TimerTask;

public class NetworkNotificationService extends Service {
    private NetworkNotificationBinder mBinder = new NetworkNotificationBinder();
    /* access modifiers changed from: private */
    public ReverseBroker mBroker;
    /* access modifiers changed from: private */
    public WifiManager mManager;
    /* access modifiers changed from: private */
    public NotificationManager nManager;

    private class WifiReceiver extends BroadcastReceiver {
        private WifiReceiver() {
        }

        /* synthetic */ WifiReceiver(NetworkNotificationService networkNotificationService, WifiReceiver wifiReceiver) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            if (NetworkNotificationService.this.mManager.isWifiEnabled() && NetworkNotificationService.this.mManager.getConnectionInfo().getSupplicantState() != SupplicantState.COMPLETED) {
                int reversible = 0;
                ApInfo info = null;
                for (ScanResult sr : NetworkNotificationService.this.mManager.getScanResults()) {
                    if (info == null) {
                        info = new ApInfo(sr.SSID, sr.BSSID, sr.level);
                    } else {
                        info.SSID = sr.SSID;
                        info.BSSID = sr.BSSID;
                        info.level = sr.level;
                    }
                    if (NetworkNotificationService.this.mBroker.isReversible(info)) {
                        reversible++;
                    }
                }
                PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, new Intent(context, Penetrate.class), 1207959552);
                Notification notification = new Notification(R.drawable.icon, "Hi!!", System.currentTimeMillis());
                notification.setLatestEventInfo(context, "Boom", String.valueOf(reversible) + " networks found", pendingIntent);
                NetworkNotificationService.this.nManager.notify(1, notification);
            }
        }
    }

    class UpdateWifiTask extends TimerTask {
        UpdateWifiTask() {
        }

        public void run() {
            if (NetworkNotificationService.this.mManager.isWifiEnabled() && NetworkNotificationService.this.mManager.getConnectionInfo() == null) {
                NetworkNotificationService.this.mManager.startScan();
            }
        }
    }

    public class NetworkNotificationBinder extends Binder {
        public NetworkNotificationBinder() {
        }

        /* access modifiers changed from: package-private */
        public NetworkNotificationService getService() {
            return NetworkNotificationService.this;
        }
    }

    public IBinder onBind(Intent intent) {
        return this.mBinder;
    }

    public void onCreate() {
        super.onCreate();
        WifiReceiver mReceiver = new WifiReceiver(this, null);
        this.mBroker = new ReverseBroker();
        this.mManager = (WifiManager) getSystemService("wifi");
        this.nManager = (NotificationManager) getSystemService("notification");
        registerReceiver(mReceiver, new IntentFilter("android.net.wifi.SCAN_RESULTS"));
        Toast.makeText(this, "Service started", 0).show();
    }
}
