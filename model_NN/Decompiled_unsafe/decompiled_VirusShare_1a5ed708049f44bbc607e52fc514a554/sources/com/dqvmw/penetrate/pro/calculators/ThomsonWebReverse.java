package com.dqvmw.penetrate.pro.calculators;

import com.dqvmw.penetrate.lib.core.ApInfo;
import com.dqvmw.penetrate.lib.core.calculators.ThomsonReverse;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class ThomsonWebReverse extends ThomsonReverse {
    private static final int ID_SIZE = 6;
    private static final String SERVICE_ENDPOINT = "http://penetrate.underdev.org/s/thomson.service.php?id=";

    public boolean featureDetect() {
        return true;
    }

    public String[] reverse(ApInfo ap) {
        String contentResponse = fetchKeysFor(ap.SSID.substring(ap.SSID.length() - ID_SIZE));
        if (contentResponse.equals("")) {
            return null;
        }
        return prepareKeys(((ThomsonWebResults) new Gson().fromJson(contentResponse, ThomsonWebResults.class)).keys);
    }

    private String[] prepareKeys(String[] resultKeys) {
        String[] keys = new String[resultKeys.length];
        for (int i = 0; i < keys.length; i++) {
            keys[i] = Long.toHexString(Long.parseLong(resultKeys[i])).toUpperCase();
        }
        return keys;
    }

    private String fetchKeysFor(String id) {
        try {
            InputStream istream = new DefaultHttpClient().execute(new HttpGet(SERVICE_ENDPOINT + prepareId(id))).getEntity().getContent();
            String contentResponse = convertStreamToString(istream);
            istream.close();
            return contentResponse;
        } catch (ClientProtocolException e) {
            return "";
        } catch (IOException e2) {
            return "";
        }
    }

    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String line = reader.readLine();
                if (line == null) {
                    try {
                        break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    sb.append(line).append("\n");
                }
            } catch (IOException e2) {
                e2.printStackTrace();
                try {
                    is.close();
                } catch (IOException e3) {
                    e3.printStackTrace();
                }
            } catch (Throwable th) {
                try {
                    is.close();
                } catch (IOException e4) {
                    e4.printStackTrace();
                }
                throw th;
            }
        }
        is.close();
        return sb.toString();
    }

    private String prepareId(String id) {
        return String.valueOf(Long.parseLong(id, 16));
    }
}
