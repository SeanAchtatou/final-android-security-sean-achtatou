package com.dqvmw.penetrate.lib.core;

import java.io.Serializable;

public class ApInfo implements Serializable {
    private static final long serialVersionUID = -7016248014650674937L;
    public String BSSID;
    public String SSID;
    public int level;

    public ApInfo(String ssid, String bssid, int level2) {
        this.BSSID = bssid;
        this.SSID = ssid;
        this.level = level2;
    }
}
