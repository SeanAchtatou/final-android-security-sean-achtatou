package com.dqvmw.penetrate.lib.gui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import com.dqvmw.penetrate.lib.core.DownloadFileTask;
import com.dqvmw.penetrate.lib.gui.dialogs.AboutDialog;
import com.dqvmw.penetratepro.R;
import java.net.MalformedURLException;
import java.net.URL;

public class Preferences extends PreferenceActivity {
    private static final String DICTIONARIES_PATH = "http://public.underdev.org/thomson.zip";

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);
        findPreference(getString(R.string.preference_download_dictionaries_key)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                new AlertDialog.Builder(Preferences.this).setTitle((int) R.string.download_dialog_title).setIcon(17301543).setMessage(Preferences.this.getString(R.string.download_dialog_message)).setPositiveButton(Preferences.this.getString(R.string.download_dialog_confirm), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Preferences.this.startDownload();
                        dialogInterface.dismiss();
                    }
                }).setNegativeButton(Preferences.this.getString(R.string.download_dialog_abort), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).create().show();
                return false;
            }
        });
        findPreference(getString(R.string.preference_about)).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                new AboutDialog(Preferences.this).show();
                return false;
            }
        });
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.getBoolean("download", false)) {
            startDownload();
        }
    }

    /* access modifiers changed from: private */
    public void startDownload() {
        DownloadFileTask task = new DownloadFileTask(this);
        try {
            task.execute(new URL(DICTIONARIES_PATH));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
