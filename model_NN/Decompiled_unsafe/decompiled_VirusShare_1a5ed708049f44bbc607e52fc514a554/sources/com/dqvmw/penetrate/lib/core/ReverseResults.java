package com.dqvmw.penetrate.lib.core;

import java.io.Serializable;

public class ReverseResults implements Serializable {
    private static final long serialVersionUID = 3415346267804391933L;
    public ApInfo mApInfo;
    public String[] mResults;

    public ReverseResults(ApInfo info, String[] results) {
        this.mApInfo = info;
        this.mResults = results;
    }
}
