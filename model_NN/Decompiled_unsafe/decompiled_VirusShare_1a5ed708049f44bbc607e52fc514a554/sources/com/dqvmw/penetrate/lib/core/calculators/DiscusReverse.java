package com.dqvmw.penetrate.lib.core.calculators;

import com.dqvmw.penetrate.lib.core.AbstractReverseInterface;
import com.dqvmw.penetrate.lib.core.ApInfo;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DiscusReverse implements AbstractReverseInterface {
    private static final String MANUAL_ENTRY_PREFIX = "Discus--";
    private static final Pattern pattern = Pattern.compile("^Discus--([0-9A-F]{6})$");

    public boolean featureDetect() {
        return true;
    }

    public boolean isReversible(ApInfo ap) {
        return pattern.matcher(ap.SSID).matches();
    }

    public boolean manualEntryAvailable() {
        return true;
    }

    public String[] reverse(ApInfo ap) {
        String[] result = null;
        Matcher matcher = pattern.matcher(ap.SSID);
        if (!matcher.matches()) {
            return result;
        }
        return new String[]{String.format("YW0%d", Integer.valueOf((Integer.parseInt(matcher.group(1), 16) - 13691953) / 4))};
    }

    public String manualEntryPrefix() {
        return MANUAL_ENTRY_PREFIX;
    }
}
