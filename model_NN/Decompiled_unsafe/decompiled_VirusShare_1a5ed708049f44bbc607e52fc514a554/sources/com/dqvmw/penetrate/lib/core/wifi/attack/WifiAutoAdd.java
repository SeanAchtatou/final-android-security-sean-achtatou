package com.dqvmw.penetrate.lib.core.wifi.attack;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.util.Log;
import com.dqvmw.penetrate.lib.core.ApInfo;

public class WifiAutoAdd {
    private ApInfo mApInfo;
    private Context mContext;
    private WifiManager mWifiManager = ((WifiManager) this.mContext.getSystemService("wifi"));

    public WifiAutoAdd(Context context, ApInfo apInfo) {
        this.mContext = context;
        this.mApInfo = apInfo;
    }

    private void cleanupDuplicates() {
        String escapedSSID = escapeField(this.mApInfo.SSID);
        for (WifiConfiguration network : this.mWifiManager.getConfiguredNetworks()) {
            if (network.SSID.equals(escapedSSID)) {
                this.mWifiManager.removeNetwork(network.networkId);
            }
        }
    }

    private void disableDuplicates() {
        String escapedSSID = escapeField(this.mApInfo.SSID);
        for (WifiConfiguration network : this.mWifiManager.getConfiguredNetworks()) {
            if (network.SSID.equals(escapedSSID)) {
                this.mWifiManager.disableNetwork(network.networkId);
            }
        }
    }

    private void enableNetworks() {
        String escapedSSID = escapeField(this.mApInfo.SSID);
        for (WifiConfiguration network : this.mWifiManager.getConfiguredNetworks()) {
            if (network.SSID.equals(escapedSSID)) {
                this.mWifiManager.enableNetwork(network.networkId, false);
            }
        }
    }

    private String escapeField(String a) {
        StringBuffer result = new StringBuffer();
        result.append("\"");
        result.append(a);
        result.append("\"");
        return result.toString();
    }

    private WifiConfiguration buildNetworkInfo(String key) {
        String eSSID = escapeField(this.mApInfo.SSID);
        String eKey = escapeField(key);
        WifiConfiguration connection = new WifiConfiguration();
        connection.SSID = eSSID;
        connection.preSharedKey = eKey;
        connection.status = 2;
        return connection;
    }

    public int add(String key) {
        disableDuplicates();
        int networkId = this.mWifiManager.addNetwork(buildNetworkInfo(key));
        this.mWifiManager.saveConfiguration();
        this.mWifiManager.enableNetwork(networkId, true);
        Log.d("WIFIADD", "Sleeping..");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Log.d("WIFIADD", "Waking..");
        for (WifiConfiguration next : this.mWifiManager.getConfiguredNetworks()) {
        }
        enableNetworks();
        this.mWifiManager.reconnect();
        return networkId;
    }

    public void reset() {
        cleanupDuplicates();
    }
}
