package com.dqvmw.penetrate.lib.gui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.dqvmw.penetrate.lib.core.ApInfo;
import com.dqvmw.penetrate.lib.core.ReverseResults;
import com.dqvmw.penetratepro.R;

public abstract class Results extends Activity implements AdapterView.OnItemClickListener {
    /* access modifiers changed from: private */
    public ApInfo mApInfo;
    /* access modifiers changed from: private */
    public String[] mResults;
    private TextView mStatusBar;

    public abstract void onStartup();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.results_dialog);
        ReverseResults results = (ReverseResults) getIntent().getExtras().getSerializable("results");
        this.mResults = results.mResults;
        this.mApInfo = results.mApInfo;
        setContentView((int) R.layout.results_dialog);
        this.mStatusBar = (TextView) findViewById(R.id.text);
        prepareResults();
    }

    public void setStatus(String status) {
        this.mStatusBar.setText(status);
    }

    private void prepareResults() {
        setStatus(this.mApInfo.SSID);
        ListView results_list = (ListView) findViewById(R.id.keys);
        ((ImageView) findViewById(R.id.logo)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
        ((Button) findViewById(R.id.share)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent send = new Intent("android.intent.action.SEND");
                send.addCategory("android.intent.category.DEFAULT");
                send.setType("text/plain");
                String send_format = Results.this.getString(R.string.resultsdialog_share_prefix);
                StringBuffer send_buffer = new StringBuffer();
                for (int i = 0; i < Results.this.mResults.length; i++) {
                    send_buffer.append(Results.this.mResults[i]);
                    if (i < Results.this.mResults.length) {
                        send_buffer.append(",");
                    }
                }
                send.putExtra("android.intent.extra.TEXT", String.format(send_format, Results.this.mApInfo.SSID, send_buffer.toString()));
                Results.this.startActivity(Intent.createChooser(send, Results.this.getString(R.string.resultsdialog_sharekeys)));
            }
        });
        results_list.setAdapter((ListAdapter) new ArrayAdapter(this, 17367043, this.mResults));
        results_list.setOnItemClickListener(this);
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        ((ClipboardManager) getSystemService("clipboard")).setText((String) ((ArrayAdapter) adapterView.getAdapter()).getItem(position));
        startActivity(new Intent("android.settings.WIFI_SETTINGS"));
    }
}
