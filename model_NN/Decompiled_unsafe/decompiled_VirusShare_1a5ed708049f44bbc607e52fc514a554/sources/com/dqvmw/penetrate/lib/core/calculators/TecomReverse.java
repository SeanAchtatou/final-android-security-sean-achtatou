package com.dqvmw.penetrate.lib.core.calculators;

import com.dqvmw.penetrate.lib.core.AbstractReverseInterface;
import com.dqvmw.penetrate.lib.core.ApInfo;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Pattern;

public class TecomReverse implements AbstractReverseInterface {
    private static final Pattern TECOM_MATCHES = Pattern.compile("TECOM-AH4021-[0-9a-zA-Z]{6}|TECOM-AH4222-[0-9a-zA-Z]{6}");

    public boolean featureDetect() {
        return true;
    }

    public boolean isReversible(ApInfo ap) {
        if (ap.SSID == null) {
            return false;
        }
        return TECOM_MATCHES.matcher(ap.SSID).matches();
    }

    public boolean manualEntryAvailable() {
        return true;
    }

    public String manualEntryPrefix() {
        return "TECOM-";
    }

    public String[] reverse(ApInfo ap) {
        String[] result = new String[0];
        try {
            MessageDigest md = MessageDigest.getInstance("SHA1");
            md.update(ap.SSID.getBytes());
            byte[] digest = md.digest();
            StringBuffer hexStr = new StringBuffer();
            for (byte aDigest : digest) {
                hexStr.append(Integer.toString((aDigest & 255) + 256, 16).substring(1));
            }
            return new String[]{hexStr.toString().substring(0, 26)};
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return result;
        }
    }
}
