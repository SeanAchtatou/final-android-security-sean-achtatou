package com.dqvmw.penetrate.lib.core.calculators;

import android.util.Log;
import com.dqvmw.penetrate.lib.core.AbstractReverseInterface;
import com.dqvmw.penetrate.lib.core.ApInfo;
import java.util.regex.Pattern;

public class FiosReverse implements AbstractReverseInterface {
    private static final Pattern FIOS_NETWORK_MATCHES = Pattern.compile("^([0-9A-Z]{5})$");
    private static final String[] FIOS_REVERSIBLE_PREFIXES = {"1801", "1F90"};

    public boolean featureDetect() {
        return true;
    }

    public boolean isReversible(ApInfo ap) {
        boolean reversible;
        boolean reversible2 = false;
        if (ap.BSSID.equals("")) {
            return false;
        }
        String firstPart = getFirstPart(ap);
        for (String prefix : FIOS_REVERSIBLE_PREFIXES) {
            if (firstPart.equals(prefix)) {
                reversible2 = true;
                Log.d("FIOS", "Matching " + firstPart + " with " + prefix);
            }
        }
        if (!reversible2 || !FIOS_NETWORK_MATCHES.matcher(ap.SSID).matches()) {
            reversible = false;
        } else {
            reversible = true;
        }
        return reversible;
    }

    public boolean manualEntryAvailable() {
        return false;
    }

    public String[] reverse(ApInfo ap) {
        String firstPart = getFirstPart(ap);
        return new String[]{String.valueOf(firstPart) + getSecondPart(ap)};
    }

    private String getFirstPart(ApInfo ap) {
        return ap.BSSID.toUpperCase().replaceAll(":", "").substring(2, 6);
    }

    private String getSecondPart(ApInfo ap) {
        return Long.toHexString(Long.valueOf(new StringBuffer(ap.SSID).reverse().toString(), 36).longValue()).toUpperCase();
    }

    public String manualEntryPrefix() {
        return null;
    }
}
