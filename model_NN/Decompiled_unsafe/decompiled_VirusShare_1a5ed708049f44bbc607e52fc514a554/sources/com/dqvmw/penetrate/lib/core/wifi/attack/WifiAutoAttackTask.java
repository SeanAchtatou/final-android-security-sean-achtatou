package com.dqvmw.penetrate.lib.core.wifi.attack;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Looper;
import android.util.Log;
import com.dqvmw.penetrate.lib.core.ApInfo;

public class WifiAutoAttackTask extends AsyncTask<String[], Integer, Integer> {
    private ApInfo mApinfo;
    private Context mContext;

    public WifiAutoAttackTask(Context context, ApInfo apInfo) {
        this.mContext = context;
        this.mApinfo = apInfo;
    }

    /* access modifiers changed from: protected */
    public Integer doInBackground(String[]... keySets) {
        for (String[] keySet : keySets) {
            performAttack(keySet);
        }
        return null;
    }

    private void performAttack(String[] keySet) {
        resetNetworks();
        Looper.prepare();
        new WifiAutoStateMachine(this.mContext, this.mApinfo).performAttack(keySet);
    }

    private void addNetworkWithKey(String ssid, String key) {
        Log.d("WIFIADD", "Added with id: " + new WifiAutoAdd(this.mContext, this.mApinfo).add(key) + " " + this.mApinfo.SSID + " " + key);
    }

    private void resetNetworks() {
        new WifiAutoAdd(this.mContext, this.mApinfo).reset();
    }
}
