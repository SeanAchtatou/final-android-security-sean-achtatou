package com.dqvmw.penetrate.lib.gui.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import com.dqvmw.penetratepro.R;

public class OpenDialog extends AlertDialog {
    public OpenDialog(Context ctx) {
        super(ctx);
        setIcon((int) R.drawable.icon);
        setTitle(ctx.getString(R.string.opendialog_title));
        setMessage(ctx.getString(R.string.opendialog_text));
        setButton(-1, ctx.getString(R.string.opendialog_link), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                OpenDialog.this.getContext().startActivity(new Intent("android.intent.action.VIEW").setData(Uri.parse("market://details?id=com.dqvmw.penetratepro")));
            }
        });
        setButton(-2, ctx.getString(R.string.dialog_close), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }
}
