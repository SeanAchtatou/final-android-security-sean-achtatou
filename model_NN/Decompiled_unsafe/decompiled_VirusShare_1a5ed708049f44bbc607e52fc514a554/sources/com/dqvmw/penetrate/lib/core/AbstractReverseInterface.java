package com.dqvmw.penetrate.lib.core;

public interface AbstractReverseInterface {
    boolean featureDetect();

    boolean isReversible(ApInfo apInfo);

    boolean manualEntryAvailable();

    String manualEntryPrefix();

    String[] reverse(ApInfo apInfo);
}
