package com.dqvmw.penetrate.lib.core.calculators;

import com.dqvmw.penetrate.lib.core.AbstractReverseInterface;
import com.dqvmw.penetrate.lib.core.ApInfo;
import com.dqvmw.penetrate.lib.core.calculators.telsey.HashWord;
import java.util.regex.Pattern;

public class TelseyReverse implements AbstractReverseInterface {
    private static final Pattern FASTWEB_SSID_MATCHER = Pattern.compile("^Fastweb-1-([0-9A-F]{12})$", 2);
    private final String[] TELSEY_KNOWN_MAC_ADDRESSES = {"00036F", "002196"};

    public boolean manualEntryAvailable() {
        return false;
    }

    public boolean featureDetect() {
        return true;
    }

    public String[] reverse(ApInfo ap) {
        String ssid = ap.SSID.split("-")[2];
        byte[] ssidB = new byte[6];
        System.out.println(ssid);
        for (int i = 0; i < 6; i++) {
            ssidB[i] = (byte) ((Character.digit(ssid.charAt(i * 2), 16) << 4) & 240);
            ssidB[i] = (byte) (ssidB[i] | ((byte) (Character.digit(ssid.charAt((i * 2) + 1), 16) & 15)));
        }
        int[] block = generateTelseyPermutation(ssidB);
        int s1 = 0;
        int s2 = 0;
        for (int i2 = 0; i2 < 64; i2++) {
            s1 = HashWord.lookup3(block, 0, i2, s1);
        }
        for (int i3 = 0; i3 < 64; i3++) {
            if (i3 < 8) {
                block[i3] = block[i3] << 3;
            } else if (i3 < 16) {
                block[i3] = block[i3] >>> 5;
            } else if (i3 < 32) {
                block[i3] = block[i3] >>> 2;
            } else {
                block[i3] = block[i3] << 7;
            }
        }
        for (int i4 = 0; i4 < 64; i4++) {
            s2 = HashWord.lookup3(block, 0, i4, s2);
        }
        String firstPart = Integer.toHexString(s1);
        return new String[]{String.valueOf(firstPart.substring(firstPart.length() - 5)) + Integer.toHexString(s2).substring(0, 5)};
    }

    public boolean isReversible(ApInfo ap) {
        if (!FASTWEB_SSID_MATCHER.matcher(ap.SSID).matches()) {
            return false;
        }
        String ssid = ap.SSID.split("-")[2];
        for (String macPrefix : this.TELSEY_KNOWN_MAC_ADDRESSES) {
            if (ssid.startsWith(macPrefix)) {
                return true;
            }
        }
        return false;
    }

    public String manualEntryPrefix() {
        return null;
    }

    private int[] generateTelseyPermutation(byte[] macB) {
        int[] mac = new int[6];
        int[] out = new int[64];
        for (int i = 0; i < 6; i++) {
            mac[i] = macB[i] & 255;
        }
        out[0] = (mac[5] << 24) | (mac[1] << 16) | (mac[0] << 8) | mac[5];
        out[1] = (mac[1] << 24) | (mac[0] << 16) | (mac[1] << 8) | mac[5];
        out[2] = (mac[4] << 24) | (mac[2] << 16) | (mac[3] << 8) | mac[2];
        out[3] = (mac[4] << 24) | (mac[3] << 16) | (mac[2] << 8) | mac[2];
        out[4] = (mac[2] << 24) | (mac[4] << 16) | (mac[2] << 8) | mac[0];
        out[5] = (mac[2] << 24) | (mac[5] << 16) | (mac[3] << 8) | mac[1];
        out[6] = (mac[0] << 24) | (mac[4] << 16) | (mac[0] << 8) | mac[1];
        out[7] = (mac[1] << 24) | (mac[4] << 16) | (mac[1] << 8) | mac[0];
        out[8] = (mac[2] << 24) | (mac[4] << 16) | (mac[2] << 8) | mac[2];
        out[9] = (mac[3] << 24) | (mac[1] << 16) | (mac[3] << 8) | mac[4];
        out[10] = (mac[4] << 24) | (mac[1] << 16) | (mac[4] << 8) | mac[3];
        out[11] = (mac[5] << 24) | (mac[1] << 16) | (mac[5] << 8) | mac[5];
        out[12] = (mac[2] << 24) | (mac[1] << 16) | (mac[0] << 8) | mac[5];
        out[13] = (mac[1] << 24) | (mac[0] << 16) | (mac[1] << 8) | mac[1];
        out[14] = (mac[4] << 24) | (mac[2] << 16) | (mac[1] << 8) | mac[3];
        out[15] = (mac[3] << 24) | (mac[3] << 16) | (mac[5] << 8) | mac[2];
        out[16] = (mac[4] << 24) | (mac[4] << 16) | (mac[5] << 8) | mac[4];
        out[17] = (mac[5] << 24) | (mac[1] << 16) | (mac[4] << 8) | mac[0];
        out[18] = (mac[2] << 24) | (mac[5] << 16) | (mac[0] << 8) | mac[5];
        out[19] = (mac[2] << 24) | (mac[1] << 16) | (mac[3] << 8) | mac[5];
        out[20] = (mac[5] << 24) | (mac[2] << 16) | (mac[2] << 8) | mac[4];
        out[21] = (mac[2] << 24) | (mac[3] << 16) | (mac[1] << 8) | mac[4];
        out[22] = (mac[0] << 24) | (mac[4] << 16) | (mac[4] << 8) | mac[3];
        out[23] = (mac[3] << 24) | (mac[0] << 16) | (mac[5] << 8) | mac[3];
        out[24] = (mac[4] << 24) | (mac[3] << 16) | (mac[0] << 8) | mac[0];
        out[25] = (mac[3] << 24) | (mac[2] << 16) | (mac[1] << 8) | mac[1];
        out[26] = (mac[2] << 24) | (mac[1] << 16) | (mac[2] << 8) | mac[5];
        out[27] = (mac[1] << 24) | (mac[3] << 16) | (mac[4] << 8) | mac[3];
        out[28] = (mac[0] << 24) | (mac[2] << 16) | (mac[3] << 8) | mac[4];
        out[29] = (mac[0] << 24) | (mac[0] << 16) | (mac[2] << 8) | mac[2];
        out[30] = (mac[0] << 24) | (mac[0] << 16) | (mac[0] << 8) | mac[5];
        out[31] = (mac[1] << 24) | (mac[1] << 16) | (mac[1] << 8) | mac[4];
        out[32] = (mac[4] << 24) | (mac[0] << 16) | (mac[2] << 8) | mac[2];
        out[33] = (mac[3] << 24) | (mac[3] << 16) | (mac[3] << 8) | mac[0];
        out[34] = (mac[0] << 24) | (mac[2] << 16) | (mac[4] << 8) | mac[1];
        out[35] = (mac[5] << 24) | (mac[5] << 16) | (mac[5] << 8) | mac[0];
        out[36] = (mac[0] << 24) | (mac[4] << 16) | (mac[5] << 8) | mac[0];
        out[37] = (mac[1] << 24) | (mac[1] << 16) | (mac[5] << 8) | mac[2];
        out[38] = (mac[2] << 24) | (mac[2] << 16) | (mac[5] << 8) | mac[1];
        out[39] = (mac[3] << 24) | (mac[3] << 16) | (mac[2] << 8) | mac[3];
        out[40] = (mac[1] << 24) | (mac[0] << 16) | (mac[2] << 8) | mac[4];
        out[41] = (mac[1] << 24) | (mac[5] << 16) | (mac[2] << 8) | mac[5];
        out[42] = (mac[0] << 24) | (mac[1] << 16) | (mac[4] << 8) | mac[0];
        out[43] = (mac[1] << 24) | (mac[1] << 16) | (mac[1] << 8) | mac[4];
        out[44] = (mac[2] << 24) | (mac[2] << 16) | (mac[2] << 8) | mac[2];
        out[45] = (mac[3] << 24) | (mac[3] << 16) | (mac[3] << 8) | mac[3];
        out[46] = (mac[5] << 24) | (mac[4] << 16) | (mac[0] << 8) | mac[1];
        out[47] = (mac[4] << 24) | (mac[0] << 16) | (mac[5] << 8) | mac[5];
        out[48] = (mac[1] << 24) | (mac[0] << 16) | (mac[5] << 8) | mac[0];
        out[49] = (mac[0] << 24) | (mac[1] << 16) | (mac[5] << 8) | mac[1];
        out[50] = (mac[2] << 24) | (mac[2] << 16) | (mac[4] << 8) | mac[2];
        out[51] = (mac[3] << 24) | (mac[4] << 16) | (mac[4] << 8) | mac[3];
        out[52] = (mac[4] << 24) | (mac[3] << 16) | (mac[1] << 8) | mac[5];
        out[53] = (mac[5] << 24) | (mac[5] << 16) | (mac[1] << 8) | mac[4];
        out[54] = (mac[3] << 24) | (mac[0] << 16) | (mac[1] << 8) | mac[5];
        out[55] = (mac[3] << 24) | (mac[1] << 16) | (mac[0] << 8) | mac[4];
        out[56] = (mac[4] << 24) | (mac[2] << 16) | (mac[2] << 8) | mac[5];
        out[57] = (mac[4] << 24) | (mac[3] << 16) | (mac[3] << 8) | mac[1];
        out[58] = (mac[2] << 24) | (mac[4] << 16) | (mac[3] << 8) | mac[0];
        out[59] = (mac[2] << 24) | (mac[3] << 16) | (mac[5] << 8) | mac[1];
        out[60] = (mac[3] << 24) | (mac[1] << 16) | (mac[2] << 8) | mac[3];
        out[61] = (mac[5] << 24) | (mac[0] << 16) | (mac[1] << 8) | mac[2];
        out[62] = (mac[5] << 24) | (mac[3] << 16) | (mac[4] << 8) | mac[1];
        out[63] = (mac[0] << 24) | (mac[2] << 16) | (mac[3] << 8) | mac[0];
        for (int i2 = 0; i2 < 64; i2++) {
            out[i2] = out[i2] & -1;
        }
        return out;
    }
}
