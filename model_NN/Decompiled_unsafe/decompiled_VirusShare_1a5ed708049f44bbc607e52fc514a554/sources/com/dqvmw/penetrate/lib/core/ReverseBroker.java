package com.dqvmw.penetrate.lib.core;

import com.dqvmw.penetrate.lib.core.calculators.AliceReverse;
import com.dqvmw.penetrate.lib.core.calculators.ComtrendCT5365Reverse;
import com.dqvmw.penetrate.lib.core.calculators.DiscusReverse;
import com.dqvmw.penetrate.lib.core.calculators.DlinkReverse;
import com.dqvmw.penetrate.lib.core.calculators.EircomReverse;
import com.dqvmw.penetrate.lib.core.calculators.FastwebReverse;
import com.dqvmw.penetrate.lib.core.calculators.FiosReverse;
import com.dqvmw.penetrate.lib.core.calculators.InfostradaReverse;
import com.dqvmw.penetrate.lib.core.calculators.SkyV1Reverse;
import com.dqvmw.penetrate.lib.core.calculators.TecomReverse;
import com.dqvmw.penetrate.lib.core.calculators.TelseyReverse;
import com.dqvmw.penetrate.lib.core.calculators.ThomsonReverse;
import java.util.Iterator;
import java.util.Vector;

public class ReverseBroker {
    private final Vector<AbstractReverseInterface> interfaces = new Vector<>();

    public ReverseBroker() {
        this.interfaces.add(new DiscusReverse());
        this.interfaces.add(new DlinkReverse());
        this.interfaces.add(new EircomReverse());
        this.interfaces.add(new ThomsonReverse());
        this.interfaces.add(new FiosReverse());
        this.interfaces.add(new AliceReverse());
        this.interfaces.add(new FastwebReverse());
        this.interfaces.add(new TelseyReverse());
        this.interfaces.add(new ComtrendCT5365Reverse());
        this.interfaces.add(new TecomReverse());
        this.interfaces.add(new InfostradaReverse());
        this.interfaces.add(new SkyV1Reverse());
    }

    public String[] reverse(ApInfo sr) {
        String[] result = null;
        Iterator<AbstractReverseInterface> it = this.interfaces.iterator();
        while (it.hasNext()) {
            AbstractReverseInterface reverse = it.next();
            if (reverse.isReversible(sr)) {
                result = reverse.reverse(sr);
            }
        }
        return result;
    }

    public boolean isReversible(ApInfo sr) {
        Iterator<AbstractReverseInterface> it = this.interfaces.iterator();
        while (it.hasNext()) {
            if (it.next().isReversible(sr)) {
                return true;
            }
        }
        return false;
    }

    public boolean featuresDetect() {
        Iterator<AbstractReverseInterface> it = this.interfaces.iterator();
        while (it.hasNext()) {
            if (!it.next().featureDetect()) {
                return false;
            }
        }
        return true;
    }

    public boolean reversibleWithAction(ApInfo ap) {
        Iterator<AbstractReverseInterface> it = this.interfaces.iterator();
        while (it.hasNext()) {
            AbstractReverseInterface reverse = it.next();
            if (!reverse.featureDetect() && reverse.isReversible(ap)) {
                return true;
            }
        }
        return false;
    }

    public String[] reverseManually(ApInfo info) {
        String[] results = null;
        Iterator<AbstractReverseInterface> it = this.interfaces.iterator();
        while (it.hasNext()) {
            AbstractReverseInterface reverse = it.next();
            if (reverse.manualEntryAvailable() && reverse.isReversible(info)) {
                results = reverse.reverse(info);
            }
        }
        return results;
    }

    public void removeReverser(Class cls) {
        AbstractReverseInterface toRemove = null;
        Iterator<AbstractReverseInterface> it = this.interfaces.iterator();
        while (it.hasNext()) {
            AbstractReverseInterface reverse = it.next();
            if (reverse.getClass().equals(cls)) {
                toRemove = reverse;
            }
        }
        if (toRemove != null) {
            this.interfaces.remove(toRemove);
        }
    }

    public void addReverser(AbstractReverseInterface reverser) {
        this.interfaces.add(reverser);
    }

    public Vector<AbstractReverseInterface> getReversers() {
        return this.interfaces;
    }
}
