package com.dqvmw.penetrate.lib.gui.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;
import com.dqvmw.penetrate.lib.core.AbstractReverseInterface;
import com.dqvmw.penetrate.lib.core.ApInfo;
import com.dqvmw.penetrate.lib.core.AsyncReverseTask;
import com.dqvmw.penetrate.lib.core.ReverseBroker;
import com.dqvmw.penetrate.lib.core.ReverseResults;
import com.dqvmw.penetrate.lib.core.wifi.WifiGuiReceiver;
import com.dqvmw.penetrate.lib.core.wifi.WifiReceiver;
import com.dqvmw.penetrate.lib.gui.dialogs.AboutDialog;
import com.dqvmw.penetrate.lib.gui.dialogs.OopsDialog;
import com.dqvmw.penetrate.lib.gui.dialogs.OpenDialog;
import com.dqvmw.penetrate.lib.gui.dialogs.QueryDialog;
import com.dqvmw.penetrate.lib.gui.helpers.ScanResultAdapter;
import com.dqvmw.penetratepro.R;
import com.imadpush.ad.poster.AppPosterManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

public abstract class Penetrate extends ListActivity implements AdapterView.OnItemClickListener, WifiGuiReceiver {
    protected static final int DIALOG_ABOUT = 0;
    protected static final int DIALOG_OOPS_DICTIONARIES = 1;
    protected static final int DIALOG_OOPS_NOKEYSFOUND = 3;
    protected static final int DIALOG_OOPS_NOTREVERSIBLE = 2;
    protected static final int DIALOG_OPENMESSAGE = 6;
    protected static final int DIALOG_QUERY = 4;
    protected static final int DIALOG_QUERY_INPUT = 5;
    private static final int MENU_ABOUT = 1;
    private static final int MENU_REFRESH = 0;
    private static final int MENU_SEARCH_SSID = 2;
    private static final int MENU_SETTINGS = 3;
    protected static final String PREFS_NAME = "PenetratePrefs";
    private ReverseBroker mBroker;
    /* access modifiers changed from: private */
    public Bundle mQueryInputBundle;
    private WifiReceiver mReceiver;
    private TextView mStatusBar;
    private TextView mWarning;

    public abstract void onStartup();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        getListView().setOnItemClickListener(this);
        this.mStatusBar = (TextView) findViewById(R.id.text);
        this.mWarning = (TextView) findViewById(R.id.warningText);
        this.mBroker = new ReverseBroker();
        this.mReceiver = new WifiReceiver(this, this.mBroker, this, (WifiManager) getSystemService("wifi"));
        registerReceiver(this.mReceiver, new IntentFilter("android.net.wifi.SCAN_RESULTS"));
        this.mReceiver.scan();
        detectFeatures();
        onStartup();
        new AppPosterManager(this, true);
    }

    /* access modifiers changed from: protected */
    public Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                return new AboutDialog(this);
            case 1:
                OopsDialog result = new OopsDialog(this, getString(R.string.dialog_dictionariesrequired));
                result.setButton(-1, getString(R.string.dialog_download), new DialogInterface.OnClickListener() {
                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent}
                     arg types: [java.lang.String, int]
                     candidates:
                      ClspMth{android.content.Intent.putExtra(java.lang.String, int):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, int[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, double):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, char):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, byte):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Bundle):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, float):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.CharSequence):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, long[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, long):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, short):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.io.Serializable):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, double[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, android.os.Parcelable):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, float[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, byte[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, java.lang.String):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, short[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, char[]):android.content.Intent}
                      ClspMth{android.content.Intent.putExtra(java.lang.String, boolean):android.content.Intent} */
                    public void onClick(DialogInterface dialog, int which) {
                        Intent settingsIntent = new Intent();
                        settingsIntent.setClassName(Penetrate.this.getPackageName(), "com.dqvmw.penetrate.Preferences");
                        settingsIntent.putExtra("download", true);
                        Penetrate.this.startActivity(settingsIntent);
                    }
                });
                return result;
            case 2:
                return new OopsDialog(this, getString(R.string.dialog_notreversible));
            case 3:
                return new OopsDialog(this, getString(R.string.dialog_nokeysfound));
            case 4:
                HashMap<Integer, String> options = new HashMap<>();
                HashMap<Integer, Integer> idMapping = new HashMap<>();
                Vector<AbstractReverseInterface> interfaces = this.mBroker.getReversers();
                ArrayList<String> strOptions = new ArrayList<>();
                int count = 0;
                int i = 0;
                int s = interfaces.size();
                while (true) {
                    int count2 = count;
                    if (i >= s) {
                        String[] selectOptions = new String[strOptions.size()];
                        for (int i2 = 0; i2 < strOptions.size(); i2++) {
                            selectOptions[i2] = (String) strOptions.get(i2);
                        }
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setTitle(R.string.querydialog_title);
                        final HashMap<Integer, String> hashMap = options;
                        final HashMap<Integer, Integer> hashMap2 = idMapping;
                        builder.setItems(selectOptions, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Bundle queryProperties = new Bundle();
                                queryProperties.putString("prefix", (String) hashMap.get(Integer.valueOf(i)));
                                queryProperties.putInt("reverserId", ((Integer) hashMap2.get(Integer.valueOf(i))).intValue());
                                Penetrate.this.mQueryInputBundle = queryProperties;
                                Penetrate.this.showDialog(5);
                                dialogInterface.dismiss();
                            }
                        });
                        return builder.create();
                    }
                    AbstractReverseInterface inter = interfaces.get(i);
                    if (inter.manualEntryAvailable()) {
                        idMapping.put(Integer.valueOf(count2), Integer.valueOf(i));
                        count = count2 + 1;
                        options.put(Integer.valueOf(count2), inter.manualEntryPrefix());
                        strOptions.add(inter.manualEntryPrefix());
                    } else {
                        count = count2;
                    }
                    i++;
                }
            case 5:
                QueryDialog queryDialog = new QueryDialog(this, this);
                Dialog result2 = queryDialog;
                String prefix = this.mQueryInputBundle.getString("prefix");
                int reverserId = this.mQueryInputBundle.getInt("reverserId");
                queryDialog.setPrefix(prefix);
                queryDialog.setReverseInterface(this.mBroker.getReversers().get(reverserId));
                return result2;
            case DIALOG_OPENMESSAGE /*6*/:
                return new OpenDialog(this);
            default:
                return null;
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add(0, 0, 0, getString(R.string.menu_refresh)).setIcon(17301581);
        menu.add(0, 3, 0, getString(R.string.menu_settings)).setIcon(17301577);
        menu.add(0, 2, 0, getString(R.string.menu_search_ssid)).setIcon(17301566);
        return super.onCreateOptionsMenu(menu);
    }

    /* access modifiers changed from: protected */
    public void detectFeatures() {
        if (this.mBroker.featuresDetect()) {
            this.mWarning.setVisibility(8);
            return;
        }
        this.mWarning.setText(Html.fromHtml(getString(R.string.warning_thomson)));
        this.mWarning.setMovementMethod(LinkMovementMethod.getInstance());
        this.mWarning.setVisibility(0);
    }

    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case 0:
                this.mReceiver.scan();
                break;
            case 1:
                showDialog(0);
                break;
            case 2:
                manualSearch();
                break;
            case 3:
                Intent settingsIntent = new Intent();
                settingsIntent.setClassName(getPackageName(), "com.dqvmw.penetrate.Preferences");
                startActivity(settingsIntent);
                break;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        unregisterReceiver(this.mReceiver);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        detectFeatures();
        registerReceiver(this.mReceiver, new IntentFilter("android.net.wifi.SCAN_RESULTS"));
        super.onResume();
    }

    public void setStatus(String status) {
        this.mStatusBar.setText(status);
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        ApInfo ap = (ApInfo) ((ScanResultAdapter) adapterView.getAdapter()).getItem(position);
        if (this.mBroker.reversibleWithAction(ap)) {
            showDialog(1);
        } else if (!this.mBroker.isReversible(ap)) {
            showDialog(2);
        } else {
            new AsyncReverseTask(this, this, this.mBroker).execute(ap);
        }
    }

    private void manualSearch() {
        showDialog(4);
    }

    public ReverseBroker getBroker() {
        return this.mBroker;
    }

    public void showResults(ApInfo info, String[] results) {
        if (results == null || results.length == 0) {
            showDialog(3);
            return;
        }
        ReverseResults result = new ReverseResults(info, results);
        Intent resultsIntent = new Intent();
        resultsIntent.setClassName(getPackageName(), "com.dqvmw.penetrate.Results");
        resultsIntent.putExtra("results", result);
        startActivity(resultsIntent);
    }

    public void wifiUpdated(List<ApInfo> results, int reversible) {
        setStatus(String.format(getString(R.string.statusbar_result), Integer.valueOf(reversible)));
        setListAdapter(new ScanResultAdapter(this, R.layout.row, results, this));
    }

    public void dispatchQuery(String ssid) {
        ApInfo info = new ApInfo(ssid, null, 0);
        showResults(info, this.mBroker.reverseManually(info));
    }
}
