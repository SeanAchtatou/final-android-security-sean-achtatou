package com.dqvmw.penetrate.lib.core;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.PowerManager;
import android.os.StatFs;
import android.util.Log;
import com.dqvmw.penetratepro.R;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

public class DownloadFileTask extends AsyncTask<URL, Long, Integer> {
    private static final int DOWNLOAD_ALREADY_EXISTS = 5;
    private static final int DOWNLOAD_CANCELLED = 2;
    private static final int DOWNLOAD_FAILED = 1;
    private static final int DOWNLOAD_STORAGE_INSUFICIENT = 4;
    private static final int DOWNLOAD_STORAGE_UNWRITABLE = 3;
    private static final int DOWNLOAD_SUCCESS = 0;
    private Activity mActivity;
    private int mContentLength;
    private ProgressDialog mProgress;
    private PowerManager.WakeLock mWakeLock;

    public DownloadFileTask(Activity ctx) {
        this.mActivity = ctx;
        this.mWakeLock = ((PowerManager) ctx.getSystemService("power")).newWakeLock(268435482, "PenetrateDictionariesLock");
        this.mProgress = new ProgressDialog(ctx);
        this.mProgress.setIndeterminate(false);
        this.mProgress.setProgressStyle(1);
        this.mProgress.setMessage(ctx.getString(R.string.dialog_download_progress_message));
        this.mProgress.setMax(100);
        this.mProgress.setCancelable(false);
        this.mProgress.setButton(-3, ctx.getString(R.string.download_dialog_abort), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                DownloadFileTask.this.cancel(false);
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.mWakeLock.acquire();
        this.mProgress.show();
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(Integer result) {
        this.mProgress.hide();
        if (this.mWakeLock.isHeld()) {
            this.mWakeLock.release();
        }
        if (result.intValue() != 0 && result.intValue() != 2) {
            int messageId = 0;
            switch (result.intValue()) {
                case 1:
                    messageId = R.string.dialog_download_error_failed;
                    break;
                case 3:
                    messageId = R.string.dialog_download_error_unwritable;
                    break;
                case 4:
                    messageId = R.string.dialog_download_error_space;
                    break;
                case 5:
                    messageId = R.string.dialog_download_error_already_exists;
                    break;
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(this.mActivity);
            builder.setTitle((int) R.string.dialog_download_error);
            builder.setMessage(messageId);
            builder.setIcon(17301543);
            builder.create().show();
        }
    }

    /* access modifiers changed from: protected */
    public void onCancelled() {
        if (this.mWakeLock.isHeld()) {
            this.mWakeLock.release();
        }
    }

    /* access modifiers changed from: protected */
    public Integer doInBackground(URL... strings) {
        int count;
        int result = 0;
        int length = strings.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                break;
            }
            try {
                URLConnection connection = strings[i].openConnection();
                this.mContentLength = connection.getHeaderFieldInt("content-Length", 0);
                this.mProgress.setMax(this.mContentLength / 1024);
                if (Environment.getExternalStorageState().equals("mounted")) {
                    File storageDirectory = Environment.getExternalStorageDirectory();
                    StatFs statFs = new StatFs(storageDirectory.toString());
                    if (((long) this.mContentLength) < ((long) statFs.getFreeBlocks()) * ((long) statFs.getBlockSize())) {
                        File file = new File(String.valueOf(storageDirectory.toString()) + "/thomson/");
                        if (!file.exists()) {
                            file.mkdirs();
                        }
                        File file2 = new File(storageDirectory + "/thomson/thomson.zip");
                        Log.d("THOMSON_NEW", String.valueOf(file2.toString()) + " " + file2.length() + " " + this.mContentLength);
                        if (file2.exists() && file2.length() == ((long) this.mContentLength)) {
                            result = 5;
                            break;
                        }
                        BufferedInputStream bufferedInputStream = new BufferedInputStream(connection.getInputStream());
                        FileOutputStream fileOutputStream = new FileOutputStream(file2);
                        byte[] data = new byte[10240];
                        long total = 0;
                        int iterations = 0;
                        while (!isCancelled() && (count = bufferedInputStream.read(data)) != -1) {
                            fileOutputStream.write(data, 0, count);
                            total += (long) count;
                            if (iterations % 10 == 0) {
                                publishProgress(Long.valueOf(total));
                            }
                            iterations++;
                        }
                        if (isCancelled()) {
                            result = 2;
                            file2.delete();
                        }
                        bufferedInputStream.close();
                        i++;
                    } else {
                        result = 4;
                        break;
                    }
                } else {
                    result = 3;
                    break;
                }
            } catch (IOException e) {
                new File(Environment.getExternalStorageDirectory() + "/thomson/thomson.zip").delete();
                result = 1;
            }
        }
        return Integer.valueOf(result);
    }

    /* access modifiers changed from: protected */
    public void onProgressUpdate(Long... values) {
        super.onProgressUpdate((Object[]) values);
        for (Long longValue : values) {
            this.mProgress.setProgress((int) (longValue.longValue() / 1024));
        }
    }
}
