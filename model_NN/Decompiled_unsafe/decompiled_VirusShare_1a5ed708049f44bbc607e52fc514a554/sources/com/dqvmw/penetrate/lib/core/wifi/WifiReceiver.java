package com.dqvmw.penetrate.lib.core.wifi;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import com.dqvmw.penetrate.lib.core.ApInfo;
import com.dqvmw.penetrate.lib.core.ReverseBroker;
import com.dqvmw.penetratepro.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class WifiReceiver extends BroadcastReceiver {
    /* access modifiers changed from: private */
    public ReverseBroker mBroker;
    /* access modifiers changed from: private */
    public Context mContext;
    /* access modifiers changed from: private */
    public WifiManager mManager;
    /* access modifiers changed from: private */
    public WifiGuiReceiver mWifiGui;

    public WifiReceiver(Context ctx, ReverseBroker broker, WifiGuiReceiver wifiGui, WifiManager manager) {
        this.mBroker = broker;
        this.mManager = manager;
        this.mWifiGui = wifiGui;
        this.mContext = ctx;
    }

    public void onReceive(Context c, Intent intent) {
        List<ScanResult> wifiList = this.mManager.getScanResults();
        List<ApInfo> aps = new ArrayList<>();
        int reversible = 0;
        for (ScanResult sr : wifiList) {
            ApInfo info = new ApInfo(sr.SSID, sr.BSSID, sr.level);
            aps.add(info);
            if (this.mBroker.isReversible(info)) {
                reversible++;
            }
        }
        Collections.sort(aps, new Comparator<ApInfo>() {
            public int compare(ApInfo a, ApInfo b) {
                int is_rev1;
                int is_rev2;
                if (WifiReceiver.this.mBroker.isReversible(a)) {
                    is_rev1 = 1;
                } else {
                    is_rev1 = 0;
                }
                if (WifiReceiver.this.mBroker.isReversible(b)) {
                    is_rev2 = 1;
                } else {
                    is_rev2 = 0;
                }
                if (is_rev1 != is_rev2) {
                    return is_rev2 - is_rev1;
                }
                return WifiManager.compareSignalLevel(b.level, a.level);
            }
        });
        this.mWifiGui.wifiUpdated(aps, reversible);
    }

    public void scan() {
        if (!this.mManager.isWifiEnabled()) {
            this.mWifiGui.setStatus(this.mContext.getString(R.string.statusbar_enablewifi));
            AlertDialog.Builder enableWifiDialog = new AlertDialog.Builder(this.mContext);
            enableWifiDialog.setTitle((int) R.string.dialog_enable_wifi_title);
            enableWifiDialog.setMessage((int) R.string.dialog_enable_wifi_message);
            enableWifiDialog.setPositiveButton((int) R.string.dialog_enable_wifi_yes, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    WifiReceiver.this.mManager.setWifiEnabled(true);
                    WifiReceiver.this.mWifiGui.setStatus(WifiReceiver.this.mContext.getString(R.string.statusbar_enablingwifi));
                    dialogInterface.dismiss();
                }
            });
            enableWifiDialog.setNegativeButton((int) R.string.dialog_enable_wifi_no, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            enableWifiDialog.create().show();
            return;
        }
        this.mManager.startScan();
        this.mWifiGui.setStatus(this.mContext.getString(R.string.statusbar_scanning));
    }
}
