package com.dqvmw.penetrate.lib.gui.helpers;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.dqvmw.penetrate.lib.core.ApInfo;
import com.dqvmw.penetrate.lib.gui.activities.Penetrate;
import com.dqvmw.penetratepro.R;
import java.util.List;

public class ScanResultAdapter extends ArrayAdapter<ApInfo> {
    private List<ApInfo> mItems;
    private Penetrate mParent;

    public ScanResultAdapter(Context ctx, int textViewResourceId, List<ApInfo> items, Penetrate parent) {
        super(ctx, textViewResourceId, items);
        this.mParent = parent;
        this.mItems = items;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View currentView = convertView;
        if (currentView == null) {
            currentView = this.mParent.getLayoutInflater().inflate((int) R.layout.row, (ViewGroup) null);
        }
        ApInfo viewAp = this.mItems.get(position);
        if (viewAp != null) {
            TextView ssid = (TextView) currentView.findViewById(R.id.row_right_ssid);
            TextView bssid = (TextView) currentView.findViewById(R.id.row_right_bssid);
            View reversible = currentView.findViewById(R.id.reversible);
            ImageView level = (ImageView) currentView.findViewById(R.id.row_right_level);
            LinearLayout linearLayout = (LinearLayout) currentView.findViewById(R.id.row_layout);
            if (this.mParent.getBroker().isReversible(viewAp)) {
                reversible.setBackgroundColor(-7288444);
            } else {
                reversible.setBackgroundColor(-24416);
            }
            ssid.setText(viewAp.SSID);
            bssid.setText(viewAp.BSSID);
            level.setImageResource(R.drawable.wifi_signal_open);
            level.setImageLevel(WifiManager.calculateSignalLevel(viewAp.level, 4));
        }
        return currentView;
    }
}
