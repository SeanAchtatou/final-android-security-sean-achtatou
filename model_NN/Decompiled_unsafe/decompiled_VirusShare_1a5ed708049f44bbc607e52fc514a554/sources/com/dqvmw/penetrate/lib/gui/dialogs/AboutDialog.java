package com.dqvmw.penetrate.lib.gui.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import com.dqvmw.penetratepro.R;

public class AboutDialog extends AlertDialog {
    public AboutDialog(Context ctx) {
        super(ctx);
        setIcon((int) R.drawable.icon);
        setTitle(String.format(ctx.getString(R.string.aboutdialog_title), ctx.getString(R.string.app_name)));
        setMessage(ctx.getString(R.string.aboutdialog_text));
        setButton(-1, ctx.getString(R.string.dialog_website), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                AboutDialog.this.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(AboutDialog.this.getContext().getString(R.string.url_main))));
            }
        });
        setButton(-2, ctx.getString(R.string.dialog_close), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }
}
