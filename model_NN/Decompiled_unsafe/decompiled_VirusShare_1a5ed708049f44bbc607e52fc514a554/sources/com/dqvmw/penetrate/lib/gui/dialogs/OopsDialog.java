package com.dqvmw.penetrate.lib.gui.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import com.dqvmw.penetratepro.R;

public class OopsDialog extends AlertDialog {
    public OopsDialog(Context ctx, CharSequence message) {
        super(ctx);
        setTitle(ctx.getString(R.string.oopsdialog_title));
        setMessage(message);
        setButton(-2, ctx.getString(R.string.dialog_close), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }
}
