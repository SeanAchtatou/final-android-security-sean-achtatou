package com.dqvmw.penetrate.lib.core.calculators.telsey;

public class HashWord {
    public static int lookup3(int[] k, int offset, int length, int initval) {
        int c = -559038737 + (length << 2) + initval;
        int b = c;
        int a = c;
        int i = offset;
        while (length > 3) {
            int a2 = a + k[i];
            int b2 = b + k[i + 1];
            int c2 = c + k[i + 2];
            int a3 = (a2 - c2) ^ ((c2 << 4) | (c2 >>> -4));
            int c3 = c2 + b2;
            int b3 = (b2 - a3) ^ ((a3 << 6) | (a3 >>> -6));
            int a4 = a3 + c3;
            int c4 = (c3 - b3) ^ ((b3 << 8) | (b3 >>> -8));
            int b4 = b3 + a4;
            int a5 = (a4 - c4) ^ ((c4 << 16) | (c4 >>> -16));
            int c5 = c4 + b4;
            int b5 = (b4 - a5) ^ ((a5 << 19) | (a5 >>> -19));
            a = a5 + c5;
            c = (c5 - b5) ^ ((b5 << 4) | (b5 >>> -4));
            b = b5 + a;
            length -= 3;
            i += 3;
        }
        switch (length) {
            case 1:
                break;
            case 3:
                c += k[i + 2];
            case 2:
                b += k[i + 1];
                break;
            default:
                return c;
        }
        int c6 = (c ^ b) - ((b << 14) | (b >>> -14));
        int a6 = ((a + k[i + 0]) ^ c6) - ((c6 << 11) | (c6 >>> -11));
        int b6 = (b ^ a6) - ((a6 << 25) | (a6 >>> -25));
        int c7 = (c6 ^ b6) - ((b6 << 16) | (b6 >>> -16));
        int a7 = (a6 ^ c7) - ((c7 << 4) | (c7 >>> -4));
        int b7 = (b6 ^ a7) - ((a7 << 14) | (a7 >>> -14));
        return (c7 ^ b7) - ((b7 << 24) | (b7 >>> -24));
    }

    public static int lookup3ycs(int[] k, int offset, int length, int initval) {
        return lookup3(k, offset, length, initval - (length << 2));
    }

    public static int lookup3ycs(CharSequence s, int start, int end, int initval) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int c = -559038737 + initval;
        int b = c;
        int a = c;
        boolean mixed = true;
        int i6 = start;
        while (true) {
            if (i6 < end) {
                mixed = false;
                int i7 = i6 + 1;
                char ch = s.charAt(i6);
                if (!Character.isHighSurrogate(ch) || i7 >= end) {
                    i = ch;
                    i2 = i7;
                } else {
                    i2 = i7 + 1;
                    i = Character.toCodePoint(ch, s.charAt(i7));
                }
                a += i;
                if (i2 < end) {
                    int i8 = i2 + 1;
                    char ch2 = s.charAt(i2);
                    if (!Character.isHighSurrogate(ch2) || i8 >= end) {
                        i3 = ch2;
                        i4 = i8;
                    } else {
                        i4 = i8 + 1;
                        i3 = Character.toCodePoint(ch2, s.charAt(i8));
                    }
                    b += i3;
                    if (i4 < end) {
                        int i9 = i4 + 1;
                        char ch3 = s.charAt(i4);
                        if (!Character.isHighSurrogate(ch3) || i9 >= end) {
                            i5 = ch3;
                        } else {
                            i5 = Character.toCodePoint(ch3, s.charAt(i9));
                            i9++;
                        }
                        c += i5;
                        if (i9 >= end) {
                            break;
                        }
                        int a2 = (a - c) ^ ((c << 4) | (c >>> -4));
                        int c2 = c + b;
                        int b2 = (b - a2) ^ ((a2 << 6) | (a2 >>> -6));
                        int a3 = a2 + c2;
                        int c3 = (c2 - b2) ^ ((b2 << 8) | (b2 >>> -8));
                        int b3 = b2 + a3;
                        int a4 = (a3 - c3) ^ ((c3 << 16) | (c3 >>> -16));
                        int c4 = c3 + b3;
                        int b4 = (b3 - a4) ^ ((a4 << 19) | (a4 >>> -19));
                        a = a4 + c4;
                        c = (c4 - b4) ^ ((b4 << 4) | (b4 >>> -4));
                        b = b4 + a;
                        mixed = true;
                        i6 = i9;
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            } else {
                break;
            }
        }
        if (mixed) {
            return c;
        }
        int c5 = (c ^ b) - ((b << 14) | (b >>> -14));
        int a5 = (a ^ c5) - ((c5 << 11) | (c5 >>> -11));
        int b5 = (b ^ a5) - ((a5 << 25) | (a5 >>> -25));
        int c6 = (c5 ^ b5) - ((b5 << 16) | (b5 >>> -16));
        int a6 = (a5 ^ c6) - ((c6 << 4) | (c6 >>> -4));
        int b6 = (b5 ^ a6) - ((a6 << 14) | (a6 >>> -14));
        return (c6 ^ b6) - ((b6 << 24) | (b6 >>> -24));
    }

    /* JADX INFO: Multiple debug info for r9v3 int: [D('initval' long), D('c' int)] */
    /* JADX INFO: Multiple debug info for r7v1 int: [D('start' int), D('i' int)] */
    /* JADX INFO: Multiple debug info for r1v4 int: [D('i' int), D('mixed' boolean)] */
    /* JADX INFO: Multiple debug info for r0v4 char: [D('ch' char), D('i' int)] */
    /* JADX INFO: Multiple debug info for r0v7 char: [D('ch' char), D('i' int)] */
    /* JADX INFO: Multiple debug info for r0v10 char: [D('ch' char), D('i' int)] */
    public static long lookup3ycs64(CharSequence s, int start, int end, long initval) {
        int c;
        int a;
        boolean mixed;
        int b;
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int c2 = -559038737 + ((int) initval);
        int c3 = ((int) (initval >>> 32)) + c2;
        int b2 = c2;
        boolean mixed2 = true;
        int i6 = start;
        int a2 = c2;
        int i7 = i6;
        while (true) {
            if (i7 >= end) {
                c = c3;
                a = a2;
                mixed = mixed2;
                b = b2;
                break;
            }
            int i8 = i7 + 1;
            char ch = s.charAt(i7);
            if (!Character.isHighSurrogate(ch) || i8 >= end) {
                int i9 = ch;
                i = i8;
                i2 = i9;
            } else {
                int i10 = i8 + 1;
                i2 = Character.toCodePoint(ch, s.charAt(i8));
                i = i10;
            }
            int a3 = a2 + i2;
            if (i >= end) {
                c = c3;
                a = a3;
                mixed = false;
                b = b2;
                break;
            }
            int i11 = i + 1;
            char ch2 = s.charAt(i);
            if (!Character.isHighSurrogate(ch2) || i11 >= end) {
                int i12 = ch2;
                i3 = i11;
                i4 = i12;
            } else {
                int i13 = i11 + 1;
                i4 = Character.toCodePoint(ch2, s.charAt(i11));
                i3 = i13;
            }
            int b3 = b2 + i4;
            if (i3 >= end) {
                c = c3;
                a = a3;
                mixed = false;
                b = b3;
                break;
            }
            int i14 = i3 + 1;
            char ch3 = s.charAt(i3);
            if (!Character.isHighSurrogate(ch3) || i14 >= end) {
                int i15 = ch3;
                i7 = i14;
                i5 = i15;
            } else {
                int i16 = i14 + 1;
                i5 = Character.toCodePoint(ch3, s.charAt(i14));
                i7 = i16;
            }
            int c4 = c3 + i5;
            if (i7 >= end) {
                c = c4;
                a = a3;
                mixed = false;
                b = b3;
                break;
            }
            int a4 = (a3 - c4) ^ ((c4 << 4) | (c4 >>> -4));
            int c5 = c4 + b3;
            int b4 = (b3 - a4) ^ ((a4 << 6) | (a4 >>> -6));
            int a5 = a4 + c5;
            int c6 = (c5 - b4) ^ ((b4 << 8) | (b4 >>> -8));
            int b5 = b4 + a5;
            int a6 = (a5 - c6) ^ ((c6 << 16) | (c6 >>> -16));
            int c7 = c6 + b5;
            int b6 = (b5 - a6) ^ ((a6 << 19) | (a6 >>> -19));
            a2 = a6 + c7;
            c3 = (c7 - b6) ^ ((b6 << 4) | (b6 >>> -4));
            b2 = b6 + a2;
            mixed2 = true;
        }
        if (!mixed) {
            int c8 = (c ^ b) - ((b << 14) | (b >>> -14));
            int a7 = (a ^ c8) - ((c8 << 11) | (c8 >>> -11));
            int b7 = (b ^ a7) - ((a7 << 25) | (a7 >>> -25));
            int c9 = (c8 ^ b7) - ((b7 << 16) | (b7 >>> -16));
            int a8 = (a7 ^ c9) - ((c9 << 4) | (c9 >>> -4));
            b = (b7 ^ a8) - ((a8 << 14) | (a8 >>> -14));
            c = (c9 ^ b) - ((b << 24) | (b >>> -24));
        }
        return (((long) b) << 32) + ((long) c);
    }
}
