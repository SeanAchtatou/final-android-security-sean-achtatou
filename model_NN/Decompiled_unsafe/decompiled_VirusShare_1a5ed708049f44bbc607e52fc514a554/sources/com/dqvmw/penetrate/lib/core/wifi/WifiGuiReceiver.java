package com.dqvmw.penetrate.lib.core.wifi;

import com.dqvmw.penetrate.lib.core.ApInfo;
import java.util.List;

public interface WifiGuiReceiver {
    void dispatchQuery(String str);

    void setStatus(String str);

    void showResults(ApInfo apInfo, String[] strArr);

    void wifiUpdated(List<ApInfo> list, int i);
}
