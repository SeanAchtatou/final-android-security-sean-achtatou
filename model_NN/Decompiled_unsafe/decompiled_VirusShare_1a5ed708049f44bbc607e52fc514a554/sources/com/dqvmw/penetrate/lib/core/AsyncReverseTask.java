package com.dqvmw.penetrate.lib.core;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import com.dqvmw.penetrate.lib.core.wifi.WifiGuiReceiver;
import com.dqvmw.penetratepro.R;

public class AsyncReverseTask extends AsyncTask<ApInfo, Integer, String[]> {
    private ApInfo mApInfo;
    private ReverseBroker mBroker;
    private WifiGuiReceiver mCallback;
    private ProgressDialog mProgressDialog;

    public AsyncReverseTask(Context context, WifiGuiReceiver callback, ReverseBroker broker) {
        this.mProgressDialog = new ProgressDialog(context);
        this.mProgressDialog.setIndeterminate(true);
        this.mProgressDialog.setMessage(context.getString(R.string.processing_results));
        this.mBroker = broker;
        this.mCallback = callback;
    }

    /* access modifiers changed from: protected */
    public String[] doInBackground(ApInfo... params) {
        String[] result = null;
        for (ApInfo ap : params) {
            this.mApInfo = ap;
            result = this.mBroker.reverse(ap);
        }
        return result;
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        this.mProgressDialog.show();
    }

    /* access modifiers changed from: protected */
    public void onPostExecute(String[] result) {
        this.mProgressDialog.hide();
        this.mCallback.showResults(this.mApInfo, result);
    }
}
