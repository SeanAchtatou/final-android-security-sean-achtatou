package com.dqvmw.penetrate.lib.gui.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.EditText;
import com.dqvmw.penetrate.lib.core.AbstractReverseInterface;
import com.dqvmw.penetrate.lib.core.wifi.WifiGuiReceiver;
import com.dqvmw.penetratepro.R;

public class QueryDialog extends AlertDialog {
    /* access modifiers changed from: private */
    public WifiGuiReceiver mGuiReceiver;
    /* access modifiers changed from: private */
    public EditText mInput;
    /* access modifiers changed from: private */
    public String mPrefix;
    private AbstractReverseInterface mReverseInterface;

    public QueryDialog(Context ctx, WifiGuiReceiver guiReciever) {
        super(ctx);
        this.mGuiReceiver = guiReciever;
        setTitle(ctx.getString(R.string.querydialog_title));
        setMessage(String.valueOf(ctx.getString(R.string.querydialog_enter_ssid)) + "\n" + this.mPrefix);
        this.mInput = new EditText(ctx);
        setView(this.mInput, 5, 5, 5, 5);
        setButton(-1, ctx.getString(R.string.dialog_search), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                QueryDialog.this.mGuiReceiver.dispatchQuery(String.valueOf(QueryDialog.this.mPrefix) + QueryDialog.this.mInput.getText().toString());
                dialog.dismiss();
            }
        });
        setButton(-2, ctx.getString(R.string.dialog_close), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
    }

    public void setPrefix(String prefix) {
        this.mPrefix = prefix;
        setMessage(String.valueOf(getContext().getString(R.string.querydialog_enter_ssid)) + "\n" + this.mPrefix);
    }

    public void setReverseInterface(AbstractReverseInterface inter) {
        this.mReverseInterface = inter;
    }
}
