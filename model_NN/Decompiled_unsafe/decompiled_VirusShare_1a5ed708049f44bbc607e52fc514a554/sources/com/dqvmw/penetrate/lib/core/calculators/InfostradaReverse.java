package com.dqvmw.penetrate.lib.core.calculators;

import com.dqvmw.penetrate.lib.core.AbstractReverseInterface;
import com.dqvmw.penetrate.lib.core.ApInfo;
import java.util.regex.Pattern;

public class InfostradaReverse implements AbstractReverseInterface {
    private Pattern INFOSTRADA_MATCHES = Pattern.compile("InfostradaWiFi-[0-9a-zA-Z]{6}");

    public boolean featureDetect() {
        return true;
    }

    public boolean isReversible(ApInfo ap) {
        return this.INFOSTRADA_MATCHES.matcher(ap.SSID).matches();
    }

    public boolean manualEntryAvailable() {
        return false;
    }

    public String manualEntryPrefix() {
        return null;
    }

    public String[] reverse(ApInfo ap) {
        return new String[]{"2" + ap.BSSID.replace(":", "").toLowerCase()};
    }
}
