package com.dqvmw.penetrate.lib.core.calculators;

import com.dqvmw.penetrate.lib.core.AbstractReverseInterface;
import com.dqvmw.penetrate.lib.core.ApInfo;
import java.util.regex.Pattern;

public class DlinkReverse implements AbstractReverseInterface {
    private static final String dic_mapping = "XrqaHNpdSYw86215";
    private static final int[][] index_mapping;
    private static final Pattern pattern = Pattern.compile("^DLink-([0-9A-F]{6})$");

    static {
        int[] iArr = new int[2];
        iArr[1] = 16;
        index_mapping = new int[][]{new int[]{1}, new int[]{3, 12}, new int[]{5, 17}, new int[]{7}, new int[]{9, 18}, new int[]{11}, new int[]{10, 13}, new int[]{8}, new int[]{6, 14}, new int[]{15, 4}, new int[]{2, 19}, iArr};
    }

    public boolean featureDetect() {
        return true;
    }

    public boolean manualEntryAvailable() {
        return false;
    }

    public String[] reverse(ApInfo ap) {
        char[] result = new char[20];
        String bssid = ap.BSSID.toUpperCase().replaceAll(":", "");
        for (int i = 0; i < bssid.length(); i++) {
            char bssid_pos = bssid.charAt(i);
            int idx = bssid_pos - (bssid_pos <= '9' ? '0' : '7');
            for (int j : index_mapping[i]) {
                result[j] = dic_mapping.charAt(idx);
            }
        }
        return new String[]{new String(result)};
    }

    public boolean isReversible(ApInfo ap) {
        return pattern.matcher(ap.SSID).matches();
    }

    public String manualEntryPrefix() {
        return null;
    }
}
