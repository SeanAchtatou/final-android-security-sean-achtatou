package com.dqvmw.penetrate;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.dqvmw.penetrate.lib.core.calculators.ThomsonReverse;
import com.dqvmw.penetrate.pro.calculators.ThomsonWebReverse;
import com.dqvmw.penetratepro.R;

public class Penetrate extends com.dqvmw.penetrate.lib.gui.activities.Penetrate {
    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        setWebReverser(getPreferenceThomsonWebQuery());
        detectFeatures();
    }

    private boolean getPreferenceThomsonWebQuery() {
        return PreferenceManager.getDefaultSharedPreferences(getBaseContext()).getBoolean(getString(R.string.preference_toggle_thomson_mode), false);
    }

    public void onStartup() {
        SharedPreferences settings = getSharedPreferences("PenetratePrefs", 0);
        if (settings.getBoolean("firstRun", true)) {
            showDialog(6);
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("firstRun", false);
            editor.commit();
        }
    }

    public void setWebReverser(boolean enabled) {
        getBroker().removeReverser(ThomsonReverse.class);
        getBroker().removeReverser(ThomsonWebReverse.class);
        if (enabled) {
            getBroker().addReverser(new ThomsonWebReverse());
        } else {
            getBroker().addReverser(new ThomsonReverse());
        }
    }
}
