package com.google.gson;

import java.lang.reflect.Field;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;

final class TypeInfoFactory {
    private TypeInfoFactory() {
    }

    public static TypeInfoArray getTypeInfoForArray(Type type) {
        Preconditions.checkArgument(TypeUtils.isArray(type));
        return new TypeInfoArray(type);
    }

    public static TypeInfo getTypeInfoForField(Field f, Type typeDefiningF) {
        return new TypeInfo(getActualType(f.getGenericType(), typeDefiningF, TypeUtils.toRawClass(typeDefiningF)));
    }

    /* JADX INFO: Multiple debug info for r3v5 java.lang.reflect.WildcardType: [D('typeToEvaluate' java.lang.reflect.Type), D('castedType' java.lang.reflect.WildcardType)] */
    /* JADX INFO: Multiple debug info for r3v11 java.lang.reflect.TypeVariable[]: [D('typeToEvaluate' java.lang.reflect.Type), D('classTypeVariables' java.lang.reflect.TypeVariable<?>[])] */
    /* JADX INFO: Multiple debug info for r5v13 java.lang.reflect.ParameterizedType: [D('rawParentClass' java.lang.Class<?>), D('objParameterizedType' java.lang.reflect.ParameterizedType)] */
    /* JADX INFO: Multiple debug info for r4v6 int: [D('indexOfActualTypeArgument' int), D('parentType' java.lang.reflect.Type)] */
    /* JADX INFO: Multiple debug info for r3v12 java.lang.reflect.Type[]: [D('classTypeVariables' java.lang.reflect.TypeVariable<?>[]), D('actualTypeArguments' java.lang.reflect.Type[])] */
    /* JADX INFO: Multiple debug info for r3v13 java.lang.reflect.Type: [D('actualTypeArguments' java.lang.reflect.Type[]), D('typeToEvaluate' java.lang.reflect.Type)] */
    /* JADX INFO: Multiple debug info for r3v14 java.lang.reflect.Type: [D('actualType' java.lang.reflect.Type), D('typeToEvaluate' java.lang.reflect.Type)] */
    /* JADX INFO: Multiple debug info for r3v15 com.google.gson.GenericArrayTypeImpl: [D('actualType' java.lang.reflect.Type), D('typeToEvaluate' java.lang.reflect.Type)] */
    /* JADX INFO: Multiple debug info for r3v17 java.lang.Class<?>: [D('actualType' java.lang.reflect.Type), D('typeToEvaluate' java.lang.reflect.Type)] */
    /* JADX INFO: Multiple debug info for r3v18 java.lang.reflect.GenericArrayType: [D('actualType' java.lang.reflect.Type), D('typeToEvaluate' java.lang.reflect.Type)] */
    /* JADX INFO: Multiple debug info for r3v20 java.lang.reflect.Type[]: [D('typeToEvaluate' java.lang.reflect.Type), D('actualTypeParameters' java.lang.reflect.Type[])] */
    /* JADX INFO: Multiple debug info for r4v10 java.lang.reflect.Type: [D('parentType' java.lang.reflect.Type), D('rawType' java.lang.reflect.Type)] */
    /* JADX INFO: Multiple debug info for r3v21 com.google.gson.ParameterizedTypeImpl: [D('typeToEvaluate' java.lang.reflect.Type), D('actualTypeParameters' java.lang.reflect.Type[])] */
    private static Type getActualType(Type typeToEvaluate, Type parentType, Class<?> rawParentClass) {
        Type theSearchedType;
        if (typeToEvaluate instanceof Class) {
            return typeToEvaluate;
        }
        if (typeToEvaluate instanceof ParameterizedType) {
            ParameterizedType castedType = (ParameterizedType) typeToEvaluate;
            return new ParameterizedTypeImpl(castedType.getRawType(), extractRealTypes(castedType.getActualTypeArguments(), parentType, rawParentClass), castedType.getOwnerType());
        } else if (typeToEvaluate instanceof GenericArrayType) {
            GenericArrayType castedType2 = (GenericArrayType) typeToEvaluate;
            Type componentType = castedType2.getGenericComponentType();
            Type typeToEvaluate2 = getActualType(componentType, parentType, rawParentClass);
            if (componentType.equals(typeToEvaluate2)) {
                return castedType2;
            }
            return typeToEvaluate2 instanceof Class ? TypeUtils.wrapWithArray(TypeUtils.toRawClass(typeToEvaluate2)) : new GenericArrayTypeImpl(typeToEvaluate2);
        } else if (typeToEvaluate instanceof TypeVariable) {
            if (parentType instanceof ParameterizedType) {
                return ((ParameterizedType) parentType).getActualTypeArguments()[getIndex(rawParentClass.getTypeParameters(), (TypeVariable) typeToEvaluate)];
            }
            if (typeToEvaluate instanceof TypeVariable) {
                do {
                    theSearchedType = extractTypeForHierarchy(parentType, (TypeVariable) typeToEvaluate);
                    if (theSearchedType == null) {
                        break;
                    }
                } while (theSearchedType instanceof TypeVariable);
                if (theSearchedType != null) {
                    return theSearchedType;
                }
            }
            throw new UnsupportedOperationException("Expecting parameterized type, got " + parentType + ".\n Are you missing the use of TypeToken idiom?\n See " + "http://sites.google.com/site/gson/gson-user-guide#TOC-Serializing-and-Deserializing-Gener");
        } else if (typeToEvaluate instanceof WildcardType) {
            return getActualType(((WildcardType) typeToEvaluate).getUpperBounds()[0], parentType, rawParentClass);
        } else {
            throw new IllegalArgumentException("Type '" + typeToEvaluate + "' is not a Class, " + "ParameterizedType, GenericArrayType or TypeVariable. Can't extract type.");
        }
    }

    private static Type extractTypeForHierarchy(Type parentType, TypeVariable<?> typeToEvaluate) {
        Class<?> rawParentType;
        Type[] actualTypeArguments;
        if (parentType instanceof Class) {
            rawParentType = (Class) parentType;
        } else if (!(parentType instanceof ParameterizedType)) {
            return null;
        } else {
            rawParentType = (Class) ((ParameterizedType) parentType).getRawType();
        }
        Type superClass = rawParentType.getGenericSuperclass();
        if (!(superClass instanceof ParameterizedType) || ((ParameterizedType) superClass).getRawType() != typeToEvaluate.getGenericDeclaration()) {
            Type searchedType = null;
            if (superClass != null) {
                searchedType = extractTypeForHierarchy(superClass, typeToEvaluate);
            }
            return searchedType;
        }
        int indexOfActualTypeArgument = getIndex(((Class) ((ParameterizedType) superClass).getRawType()).getTypeParameters(), typeToEvaluate);
        if (parentType instanceof Class) {
            actualTypeArguments = ((ParameterizedType) superClass).getActualTypeArguments();
        } else if (!(parentType instanceof ParameterizedType)) {
            return null;
        } else {
            actualTypeArguments = ((ParameterizedType) parentType).getActualTypeArguments();
        }
        return actualTypeArguments[indexOfActualTypeArgument];
    }

    private static Type[] extractRealTypes(Type[] actualTypeArguments, Type parentType, Class<?> rawParentClass) {
        Preconditions.checkNotNull(actualTypeArguments);
        Type[] retTypes = new Type[actualTypeArguments.length];
        for (int i = 0; i < actualTypeArguments.length; i++) {
            retTypes[i] = getActualType(actualTypeArguments[i], parentType, rawParentClass);
        }
        return retTypes;
    }

    private static int getIndex(TypeVariable<?>[] types, TypeVariable<?> type) {
        for (int i = 0; i < types.length; i++) {
            if (type.equals(types[i])) {
                return i;
            }
        }
        throw new IllegalStateException("How can the type variable not be present in the class declaration!");
    }
}
