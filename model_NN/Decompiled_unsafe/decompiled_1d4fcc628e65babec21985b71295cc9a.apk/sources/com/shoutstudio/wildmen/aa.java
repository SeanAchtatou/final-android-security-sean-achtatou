package com.shoutstudio.wildmen;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class aa extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private static aa f199a;

    public static aa a() {
        return f199a;
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 8) {
            if (i2 != -1) {
                as.a(getApplicationContext());
            } else {
                Intent intent2 = new Intent(this, ma.class);
                intent2.addFlags(268435456);
                startActivity(intent2);
            }
            finish();
            return;
        }
        super.onActivityResult(i, i2, intent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Context applicationContext = getApplicationContext();
        f199a = this;
        boolean b = as.b(applicationContext);
        if (getIntent().getBooleanExtra(a.Z, true)) {
            Intent intent = new Intent(applicationContext, as.class);
            intent.setFlags(268435456);
            applicationContext.startService(intent);
            if (b) {
                finish();
                return;
            }
            return;
        }
        finish();
    }

    public void onPause() {
        super.onPause();
    }

    public void onResume() {
        super.onResume();
    }
}
