package com.shoutstudio.wildmen;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.delete687.delete910.R;

class i implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ da f204a;
    private Button b;
    /* access modifiers changed from: private */
    public View c;
    private Context d;
    /* access modifiers changed from: private */
    public WindowManager e;
    private TextView f;
    private LinearLayout g;

    i(da daVar, Context context, TextView textView, Button button, WindowManager windowManager, View view, LinearLayout linearLayout) {
        this.f204a = daVar;
        this.e = windowManager;
        this.b = button;
        this.c = view;
        this.g = linearLayout;
        this.d = context;
        this.f = textView;
    }

    public void onClick(View view) {
        this.f.setText(this.d.getString(R.string.ad_loading_text));
        this.b.setVisibility(8);
        this.g.setVisibility(0);
        new Handler().postDelayed(new j(this), 7000);
    }
}
