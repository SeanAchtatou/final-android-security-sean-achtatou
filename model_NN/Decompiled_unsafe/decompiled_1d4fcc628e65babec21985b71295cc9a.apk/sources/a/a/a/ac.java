package a.a.a;

import a.a.a.n.a;
import java.io.Serializable;

public class ac implements Serializable, Cloneable {
    protected final String d;
    protected final int e;
    protected final int f;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public ac(String str, int i, int i2) {
        this.d = (String) a.a((Object) str, "Protocol name");
        this.e = a.b(i, "Protocol minor version");
        this.f = a.b(i2, "Protocol minor version");
    }

    public ac a(int i, int i2) {
        return (i == this.e && i2 == this.f) ? this : new ac(this.d, i, i2);
    }

    public final String a() {
        return this.d;
    }

    public boolean a(ac acVar) {
        return acVar != null && this.d.equals(acVar.d);
    }

    public final int b() {
        return this.e;
    }

    public int b(ac acVar) {
        a.a(acVar, "Protocol version");
        a.a(this.d.equals(acVar.d), "Versions for different protocols cannot be compared: %s %s", this, acVar);
        int b = b() - acVar.b();
        return b == 0 ? c() - acVar.c() : b;
    }

    public final int c() {
        return this.f;
    }

    public final boolean c(ac acVar) {
        return a(acVar) && b(acVar) <= 0;
    }

    public Object clone() {
        return super.clone();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ac)) {
            return false;
        }
        ac acVar = (ac) obj;
        return this.d.equals(acVar.d) && this.e == acVar.e && this.f == acVar.f;
    }

    public final int hashCode() {
        return (this.d.hashCode() ^ (this.e * 100000)) ^ this.f;
    }

    public String toString() {
        return this.d + '/' + Integer.toString(this.e) + '.' + Integer.toString(this.f);
    }
}
