package a.a.a;

public class m extends Exception {
    public m() {
    }

    public m(String str) {
        super(str);
    }

    public m(String str, Throwable th) {
        super(str);
        initCause(th);
    }
}
