package a.a.a.g.a.a;

import a.a.a.c;
import a.a.a.n.a;
import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

public class e extends a {

    /* renamed from: a  reason: collision with root package name */
    private final byte[] f57a;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public e(String str, a.a.a.g.e eVar) {
        super(eVar);
        a.a((Object) str, "Text");
        Charset b = eVar.b();
        this.f57a = str.getBytes(b == null ? c.b : b);
    }

    public void a(OutputStream outputStream) {
        a.a(outputStream, "Output stream");
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(this.f57a);
        byte[] bArr = new byte[4096];
        while (true) {
            int read = byteArrayInputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                outputStream.flush();
                return;
            }
        }
    }

    public String d() {
        return null;
    }

    public String e() {
        return "8bit";
    }

    public long f() {
        return (long) this.f57a.length;
    }
}
