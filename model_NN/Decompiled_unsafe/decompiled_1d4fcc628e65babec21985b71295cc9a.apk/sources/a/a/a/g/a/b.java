package a.a.a.g.a;

import a.a.a.g.a.a.c;
import a.a.a.n.a;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private final String f58a;
    private final d b;
    private final c c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    b(String str, c cVar, d dVar) {
        a.a((Object) str, "Name");
        a.a(cVar, "Body");
        this.f58a = str;
        this.c = cVar;
        this.b = dVar == null ? new d() : dVar;
    }

    public c a() {
        return this.c;
    }

    public d b() {
        return this.b;
    }
}
