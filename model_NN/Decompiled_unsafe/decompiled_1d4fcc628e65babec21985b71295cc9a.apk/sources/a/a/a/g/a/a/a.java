package a.a.a.g.a.a;

import a.a.a.g.e;
import java.nio.charset.Charset;

public abstract class a implements c {

    /* renamed from: a  reason: collision with root package name */
    private final e f55a;

    public a(e eVar) {
        a.a.a.n.a.a(eVar, "Content type");
        this.f55a = eVar;
    }

    public e a() {
        return this.f55a;
    }

    public String b() {
        return this.f55a.a();
    }

    public String c() {
        Charset b = this.f55a.b();
        if (b != null) {
            return b.name();
        }
        return null;
    }
}
