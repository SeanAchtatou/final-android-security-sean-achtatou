package a.a.a.m;

import a.a.a.j;
import a.a.a.n;
import a.a.a.n.a;
import a.a.a.q;

public class f implements e {

    /* renamed from: a  reason: collision with root package name */
    private final e f186a;

    public f() {
        this.f186a = new a();
    }

    public f(e eVar) {
        this.f186a = eVar;
    }

    public static f b(e eVar) {
        a.a(eVar, "HTTP context");
        return eVar instanceof f ? (f) eVar : new f(eVar);
    }

    public Object a(String str) {
        return this.f186a.a(str);
    }

    public Object a(String str, Class cls) {
        a.a(cls, "Attribute class");
        Object a2 = a(str);
        if (a2 == null) {
            return null;
        }
        return cls.cast(a2);
    }

    public void a(String str, Object obj) {
        this.f186a.a(str, obj);
    }

    public j l() {
        return (j) a("http.connection", j.class);
    }

    public q m() {
        return (q) a("http.request", q.class);
    }

    public boolean n() {
        Boolean bool = (Boolean) a("http.request_sent", Boolean.class);
        return bool != null && bool.booleanValue();
    }

    public n o() {
        return (n) a("http.target_host", n.class);
    }
}
