package a.a.a.m;

import a.a.a.ac;
import a.a.a.l;
import a.a.a.n.a;
import a.a.a.q;
import a.a.a.r;
import a.a.a.v;

public class k implements r {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f190a;

    @Deprecated
    public k() {
        this(false);
    }

    public k(boolean z) {
        this.f190a = z;
    }

    public void a(q qVar, e eVar) {
        a.a(qVar, "HTTP request");
        if (!qVar.a("Expect") && (qVar instanceof l)) {
            ac b = qVar.g().b();
            a.a.a.k b2 = ((l) qVar).b();
            if (b2 != null && b2.c() != 0 && !b.c(v.b) && qVar.f().a("http.protocol.expect-continue", this.f190a)) {
                qVar.a("Expect", "100-continue");
            }
        }
    }
}
