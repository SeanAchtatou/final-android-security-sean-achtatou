package a.a.a.l;

import java.util.concurrent.TimeUnit;

public abstract class a {

    /* renamed from: a  reason: collision with root package name */
    private final String f181a;
    private final Object b;
    private final Object c;
    private final long d = System.currentTimeMillis();
    private final long e;
    private long f;
    private long g;
    private volatile Object h;

    public a(String str, Object obj, Object obj2, long j, TimeUnit timeUnit) {
        a.a.a.n.a.a(obj, "Route");
        a.a.a.n.a.a(obj2, "Connection");
        a.a.a.n.a.a(timeUnit, "Time unit");
        this.f181a = str;
        this.b = obj;
        this.c = obj2;
        if (j > 0) {
            this.e = this.d + timeUnit.toMillis(j);
        } else {
            this.e = Long.MAX_VALUE;
        }
        this.g = this.e;
    }

    public synchronized void a(long j, TimeUnit timeUnit) {
        a.a.a.n.a.a(timeUnit, "Time unit");
        this.f = System.currentTimeMillis();
        this.g = Math.min(j > 0 ? this.f + timeUnit.toMillis(j) : Long.MAX_VALUE, this.e);
    }

    public void a(Object obj) {
        this.h = obj;
    }

    public synchronized boolean a(long j) {
        return j >= this.g;
    }

    public Object f() {
        return this.b;
    }

    public Object g() {
        return this.c;
    }

    public synchronized long h() {
        return this.g;
    }

    public String toString() {
        return "[id:" + this.f181a + "][route:" + this.b + "][state:" + this.h + "]";
    }
}
