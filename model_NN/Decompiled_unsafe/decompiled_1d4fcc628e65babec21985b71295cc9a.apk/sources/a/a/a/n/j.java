package a.a.a.n;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;

public class j {

    /* renamed from: a  reason: collision with root package name */
    private final String f195a;
    private final String b;
    private final String c;
    private final String d;
    private final String e;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    protected j(String str, String str2, String str3, String str4, String str5) {
        a.a((Object) str, "Package identifier");
        this.f195a = str;
        this.b = str2 == null ? "UNAVAILABLE" : str2;
        this.c = str3 == null ? "UNAVAILABLE" : str3;
        this.d = str4 == null ? "UNAVAILABLE" : str4;
        this.e = str5 == null ? "UNAVAILABLE" : str5;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    public static j a(String str, ClassLoader classLoader) {
        Properties properties;
        InputStream resourceAsStream;
        a.a((Object) str, "Package identifier");
        if (classLoader == null) {
            classLoader = Thread.currentThread().getContextClassLoader();
        }
        try {
            resourceAsStream = classLoader.getResourceAsStream(str.replace('.', '/') + "/" + "version.properties");
            if (resourceAsStream != null) {
                properties = new Properties();
                properties.load(resourceAsStream);
                try {
                    resourceAsStream.close();
                } catch (IOException e2) {
                }
            } else {
                properties = null;
            }
        } catch (IOException e3) {
            properties = null;
        } catch (Throwable th) {
            resourceAsStream.close();
            throw th;
        }
        if (properties != null) {
            return a(str, properties, classLoader);
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    protected static j a(String str, Map map, ClassLoader classLoader) {
        String str2;
        String str3;
        String str4;
        String str5 = null;
        a.a((Object) str, "Package identifier");
        if (map != null) {
            String str6 = (String) map.get("info.module");
            String str7 = (str6 == null || str6.length() >= 1) ? str6 : null;
            String str8 = (String) map.get("info.release");
            String str9 = (str8 == null || (str8.length() >= 1 && !str8.equals("${pom.version}"))) ? str8 : null;
            String str10 = (String) map.get("info.timestamp");
            if (str10 == null || (str10.length() >= 1 && !str10.equals("${mvn.timestamp}"))) {
                str2 = str10;
                str3 = str9;
                str4 = str7;
            } else {
                str2 = null;
                str3 = str9;
                str4 = str7;
            }
        } else {
            str2 = null;
            str3 = null;
            str4 = null;
        }
        if (classLoader != null) {
            str5 = classLoader.toString();
        }
        return new j(str, str4, str3, str2, str5);
    }

    public static String a(String str, String str2, Class cls) {
        j a2 = a(str2, cls.getClassLoader());
        return String.format("%s/%s (Java/%s)", str, a2 != null ? a2.a() : "UNAVAILABLE", System.getProperty("java.version"));
    }

    public final String a() {
        return this.c;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(this.f195a.length() + 20 + this.b.length() + this.c.length() + this.d.length() + this.e.length());
        sb.append("VersionInfo(").append(this.f195a).append(':').append(this.b);
        if (!"UNAVAILABLE".equals(this.c)) {
            sb.append(':').append(this.c);
        }
        if (!"UNAVAILABLE".equals(this.d)) {
            sb.append(':').append(this.d);
        }
        sb.append(')');
        if (!"UNAVAILABLE".equals(this.e)) {
            sb.append('@').append(this.e);
        }
        return sb.toString();
    }
}
