package a.a.a.n;

public class b {
    public static void a(CharSequence charSequence, String str) {
        if (i.b(charSequence)) {
            throw new IllegalStateException(str + " is blank");
        }
    }

    public static void a(Object obj, String str) {
        if (obj == null) {
            throw new IllegalStateException(str + " is null");
        }
    }

    public static void a(boolean z, String str) {
        if (!z) {
            throw new IllegalStateException(str);
        }
    }
}
