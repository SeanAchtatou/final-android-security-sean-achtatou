package a.a.a.n;

import java.util.Collection;

public class a {
    public static int a(int i, String str) {
        if (i > 0) {
            return i;
        }
        throw new IllegalArgumentException(str + " may not be negative or zero");
    }

    public static long a(long j, String str) {
        if (j >= 0) {
            return j;
        }
        throw new IllegalArgumentException(str + " may not be negative");
    }

    public static CharSequence a(CharSequence charSequence, String str) {
        if (charSequence == null) {
            throw new IllegalArgumentException(str + " may not be null");
        } else if (!i.a(charSequence)) {
            return charSequence;
        } else {
            throw new IllegalArgumentException(str + " may not be empty");
        }
    }

    public static Object a(Object obj, String str) {
        if (obj != null) {
            return obj;
        }
        throw new IllegalArgumentException(str + " may not be null");
    }

    public static Collection a(Collection collection, String str) {
        if (collection == null) {
            throw new IllegalArgumentException(str + " may not be null");
        } else if (!collection.isEmpty()) {
            return collection;
        } else {
            throw new IllegalArgumentException(str + " may not be empty");
        }
    }

    public static void a(boolean z, String str) {
        if (!z) {
            throw new IllegalArgumentException(str);
        }
    }

    public static void a(boolean z, String str, Object... objArr) {
        if (!z) {
            throw new IllegalArgumentException(String.format(str, objArr));
        }
    }

    public static int b(int i, String str) {
        if (i >= 0) {
            return i;
        }
        throw new IllegalArgumentException(str + " may not be negative");
    }

    public static CharSequence b(CharSequence charSequence, String str) {
        if (charSequence == null) {
            throw new IllegalArgumentException(str + " may not be null");
        } else if (!i.b(charSequence)) {
            return charSequence;
        } else {
            throw new IllegalArgumentException(str + " may not be blank");
        }
    }

    public static CharSequence c(CharSequence charSequence, String str) {
        if (charSequence == null) {
            throw new IllegalArgumentException(str + " may not be null");
        } else if (!i.c(charSequence)) {
            return charSequence;
        } else {
            throw new IllegalArgumentException(str + " may not contain blanks");
        }
    }
}
