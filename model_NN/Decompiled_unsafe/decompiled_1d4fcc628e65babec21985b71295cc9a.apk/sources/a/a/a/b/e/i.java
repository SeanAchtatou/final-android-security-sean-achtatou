package a.a.a.b.e;

import a.a.a.m.e;
import a.a.a.n.a;
import a.a.a.q;

@Deprecated
public class i extends d {
    public void a(q qVar, e eVar) {
        a.a(qVar, "HTTP request");
        a.a(eVar, "HTTP context");
        if (!qVar.g().a().equalsIgnoreCase("CONNECT") && !qVar.a("Authorization")) {
            a.a.a.a.i iVar = (a.a.a.a.i) eVar.a("http.auth.target-scope");
            if (iVar == null) {
                this.f19a.debug("Target auth state not set in the context");
                return;
            }
            if (this.f19a.isDebugEnabled()) {
                this.f19a.debug("Target auth state: " + iVar.b());
            }
            a(iVar, qVar, eVar);
        }
    }
}
