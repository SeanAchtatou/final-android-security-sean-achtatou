package a.a.a.b.e;

import a.a.a.b.c.l;
import a.a.a.b.h;
import a.a.a.f.c;
import a.a.a.f.f;
import a.a.a.f.k;
import a.a.a.m.e;
import a.a.a.n;
import a.a.a.n.a;
import a.a.a.n.i;
import a.a.a.q;
import a.a.a.r;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class b implements r {

    /* renamed from: a  reason: collision with root package name */
    private final Log f17a = LogFactory.getLog(getClass());

    public void a(q qVar, e eVar) {
        URI uri;
        a.a.a.e b;
        boolean z = false;
        a.a(qVar, "HTTP request");
        a.a(eVar, "HTTP context");
        if (!qVar.g().a().equalsIgnoreCase("CONNECT")) {
            a a2 = a.a(eVar);
            h b2 = a2.b();
            if (b2 == null) {
                this.f17a.debug("Cookie store not specified in HTTP context");
                return;
            }
            a.a.a.d.a e = a2.e();
            if (e == null) {
                this.f17a.debug("CookieSpec registry not specified in HTTP context");
                return;
            }
            n o = a2.o();
            if (o == null) {
                this.f17a.debug("Target host not set in the context");
                return;
            }
            a.a.a.e.b.e a3 = a2.a();
            if (a3 == null) {
                this.f17a.debug("Connection route not set in the context");
                return;
            }
            String a4 = a2.k().a();
            String str = a4 == null ? "default" : a4;
            if (this.f17a.isDebugEnabled()) {
                this.f17a.debug("CookieSpec selected: " + str);
            }
            if (qVar instanceof l) {
                uri = ((l) qVar).i();
            } else {
                try {
                    uri = new URI(qVar.g().c());
                } catch (URISyntaxException e2) {
                    uri = null;
                }
            }
            String path = uri != null ? uri.getPath() : null;
            String a5 = o.a();
            int b3 = o.b();
            if (b3 < 0) {
                b3 = a3.a().b();
            }
            if (b3 < 0) {
                b3 = 0;
            }
            if (i.a(path)) {
                path = "/";
            }
            f fVar = new f(a5, b3, path, a3.g());
            k kVar = (k) e.b(str);
            if (kVar != null) {
                a.a.a.f.i a6 = kVar.a(a2);
                List<c> a7 = b2.a();
                ArrayList arrayList = new ArrayList();
                Date date = new Date();
                for (c cVar : a7) {
                    if (cVar.a(date)) {
                        if (this.f17a.isDebugEnabled()) {
                            this.f17a.debug("Cookie " + cVar + " expired");
                        }
                        z = true;
                    } else if (a6.b(cVar, fVar)) {
                        if (this.f17a.isDebugEnabled()) {
                            this.f17a.debug("Cookie " + cVar + " match " + fVar);
                        }
                        arrayList.add(cVar);
                    }
                }
                if (z) {
                    b2.a(date);
                }
                if (!arrayList.isEmpty()) {
                    for (a.a.a.e a8 : a6.a(arrayList)) {
                        qVar.a(a8);
                    }
                }
                if (a6.a() > 0 && (b = a6.b()) != null) {
                    qVar.a(b);
                }
                eVar.a("http.cookie-spec", a6);
                eVar.a("http.cookie-origin", fVar);
            } else if (this.f17a.isDebugEnabled()) {
                this.f17a.debug("Unsupported cookie policy: " + str);
            }
        }
    }
}
