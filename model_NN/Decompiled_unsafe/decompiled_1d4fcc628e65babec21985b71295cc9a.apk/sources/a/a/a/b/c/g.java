package a.a.a.b.c;

import a.a.a.b.f.a;
import a.a.a.e;
import a.a.a.k;
import a.a.a.l;

public abstract class g extends k implements l {
    private k c;

    public void a(k kVar) {
        this.c = kVar;
    }

    public boolean a() {
        e c2 = c("Expect");
        return c2 != null && "100-continue".equalsIgnoreCase(c2.d());
    }

    public k b() {
        return this.c;
    }

    public Object clone() {
        g gVar = (g) super.clone();
        if (this.c != null) {
            gVar.c = (k) a.a(this.c);
        }
        return gVar;
    }
}
