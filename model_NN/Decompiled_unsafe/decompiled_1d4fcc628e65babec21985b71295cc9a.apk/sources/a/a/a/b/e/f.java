package a.a.a.b.e;

import a.a.a.m.e;
import a.a.a.n.a;
import a.a.a.q;
import a.a.a.r;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class f implements r {

    /* renamed from: a  reason: collision with root package name */
    private final Log f21a = LogFactory.getLog(getClass());

    public void a(q qVar, e eVar) {
        a.a(qVar, "HTTP request");
        if (qVar.g().a().equalsIgnoreCase("CONNECT")) {
            qVar.b("Proxy-Connection", "Keep-Alive");
            return;
        }
        a.a.a.e.b.e a2 = a.a(eVar).a();
        if (a2 == null) {
            this.f21a.debug("Connection route not set in the context");
            return;
        }
        if ((a2.c() == 1 || a2.e()) && !qVar.a("Connection")) {
            qVar.a("Connection", "Keep-Alive");
        }
        if (a2.c() == 2 && !a2.e() && !qVar.a("Proxy-Connection")) {
            qVar.a("Proxy-Connection", "Keep-Alive");
        }
    }
}
