package a.a.a.b.a;

import a.a.a.n;
import java.net.InetAddress;
import java.util.Collection;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private boolean f13a;
    private n b;
    private InetAddress c;
    private boolean d = false;
    private String e;
    private boolean f = true;
    private boolean g = true;
    private boolean h;
    private int i = 50;
    private boolean j = true;
    private Collection k;
    private Collection l;
    private int m = -1;
    private int n = -1;
    private int o = -1;
    private boolean p = true;

    b() {
    }

    public a a() {
        return new a(this.f13a, this.b, this.c, this.d, this.e, this.f, this.g, this.h, this.i, this.j, this.k, this.l, this.m, this.n, this.o, this.p);
    }

    public b a(int i2) {
        this.i = i2;
        return this;
    }

    public b a(n nVar) {
        this.b = nVar;
        return this;
    }

    public b a(String str) {
        this.e = str;
        return this;
    }

    public b a(InetAddress inetAddress) {
        this.c = inetAddress;
        return this;
    }

    public b a(Collection collection) {
        this.k = collection;
        return this;
    }

    public b a(boolean z) {
        this.f13a = z;
        return this;
    }

    public b b(int i2) {
        this.m = i2;
        return this;
    }

    public b b(Collection collection) {
        this.l = collection;
        return this;
    }

    @Deprecated
    public b b(boolean z) {
        this.d = z;
        return this;
    }

    public b c(int i2) {
        this.n = i2;
        return this;
    }

    public b c(boolean z) {
        this.f = z;
        return this;
    }

    public b d(int i2) {
        this.o = i2;
        return this;
    }

    public b d(boolean z) {
        this.g = z;
        return this;
    }

    public b e(boolean z) {
        this.h = z;
        return this;
    }

    public b f(boolean z) {
        this.j = z;
        return this;
    }
}
