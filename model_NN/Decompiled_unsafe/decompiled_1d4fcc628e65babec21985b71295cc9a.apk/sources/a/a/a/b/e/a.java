package a.a.a.b.e;

import a.a.a.b.h;
import a.a.a.e.b.b;
import a.a.a.f.i;
import a.a.a.f.k;
import a.a.a.m.e;
import a.a.a.m.f;

public class a extends f {
    public a() {
    }

    public a(e eVar) {
        super(eVar);
    }

    public static a a(e eVar) {
        return eVar instanceof a ? (a) eVar : new a(eVar);
    }

    private a.a.a.d.a b(String str, Class cls) {
        return (a.a.a.d.a) a(str, a.a.a.d.a.class);
    }

    public a.a.a.e.b.e a() {
        return (a.a.a.e.b.e) a("http.route", b.class);
    }

    public void a(a.a.a.b.a aVar) {
        a("http.auth.auth-cache", aVar);
    }

    public h b() {
        return (h) a("http.cookie-store", h.class);
    }

    public i c() {
        return (i) a("http.cookie-spec", i.class);
    }

    public a.a.a.f.f d() {
        return (a.a.a.f.f) a("http.cookie-origin", a.a.a.f.f.class);
    }

    public a.a.a.d.a e() {
        return b("http.cookiespec-registry", k.class);
    }

    public a.a.a.d.a f() {
        return b("http.authscheme-registry", a.a.a.a.e.class);
    }

    public a.a.a.b.i g() {
        return (a.a.a.b.i) a("http.auth.credentials-provider", a.a.a.b.i.class);
    }

    public a.a.a.b.a h() {
        return (a.a.a.b.a) a("http.auth.auth-cache", a.a.a.b.a.class);
    }

    public a.a.a.a.i i() {
        return (a.a.a.a.i) a("http.auth.target-scope", a.a.a.a.i.class);
    }

    public a.a.a.a.i j() {
        return (a.a.a.a.i) a("http.auth.proxy-scope", a.a.a.a.i.class);
    }

    public a.a.a.b.a.a k() {
        a.a.a.b.a.a aVar = (a.a.a.b.a.a) a("http.request-config", a.a.a.b.a.a.class);
        return aVar != null ? aVar : a.a.a.b.a.a.f12a;
    }
}
