package a.a.a.b.e;

import a.a.a.e;
import a.a.a.f.c;
import a.a.a.f.f;
import a.a.a.f.i;
import a.a.a.f.n;
import a.a.a.h;
import a.a.a.n.a;
import a.a.a.s;
import a.a.a.u;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class j implements u {

    /* renamed from: a  reason: collision with root package name */
    private final Log f23a = LogFactory.getLog(getClass());

    private static String a(c cVar) {
        StringBuilder sb = new StringBuilder();
        sb.append(cVar.a());
        sb.append("=\"");
        String b = cVar.b();
        if (b != null) {
            if (b.length() > 100) {
                b = b.substring(0, 100) + "...";
            }
            sb.append(b);
        }
        sb.append("\"");
        sb.append(", version:");
        sb.append(Integer.toString(cVar.h()));
        sb.append(", domain:");
        sb.append(cVar.d());
        sb.append(", path:");
        sb.append(cVar.e());
        sb.append(", expiry:");
        sb.append(cVar.c());
        return sb.toString();
    }

    private void a(h hVar, i iVar, f fVar, a.a.a.b.h hVar2) {
        while (hVar.hasNext()) {
            e a2 = hVar.a();
            try {
                for (c cVar : iVar.a(a2, fVar)) {
                    try {
                        iVar.a(cVar, fVar);
                        hVar2.a(cVar);
                        if (this.f23a.isDebugEnabled()) {
                            this.f23a.debug("Cookie accepted [" + a(cVar) + "]");
                        }
                    } catch (n e) {
                        if (this.f23a.isWarnEnabled()) {
                            this.f23a.warn("Cookie rejected [" + a(cVar) + "] " + e.getMessage());
                        }
                    }
                }
            } catch (n e2) {
                if (this.f23a.isWarnEnabled()) {
                    this.f23a.warn("Invalid cookie header: \"" + a2 + "\". " + e2.getMessage());
                }
            }
        }
    }

    public void a(s sVar, a.a.a.m.e eVar) {
        a.a(sVar, "HTTP request");
        a.a(eVar, "HTTP context");
        a a2 = a.a(eVar);
        i c = a2.c();
        if (c == null) {
            this.f23a.debug("Cookie spec not specified in HTTP context");
            return;
        }
        a.a.a.b.h b = a2.b();
        if (b == null) {
            this.f23a.debug("Cookie store not specified in HTTP context");
            return;
        }
        f d = a2.d();
        if (d == null) {
            this.f23a.debug("Cookie origin not specified in HTTP context");
            return;
        }
        a(sVar.e("Set-Cookie"), c, d, b);
        if (c.a() > 0) {
            a(sVar.e("Set-Cookie2"), c, d, b);
        }
    }
}
