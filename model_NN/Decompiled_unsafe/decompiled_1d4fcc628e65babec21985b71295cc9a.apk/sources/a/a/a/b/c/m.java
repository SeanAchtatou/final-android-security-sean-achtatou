package a.a.a.b.c;

import a.a.a.ac;
import a.a.a.b.a.a;
import a.a.a.b.f.g;
import a.a.a.c;
import a.a.a.g.e;
import a.a.a.j.q;
import a.a.a.k;
import a.a.a.l;
import a.a.a.m.d;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.List;

public class m {

    /* renamed from: a  reason: collision with root package name */
    private String f16a;
    private Charset b;
    private ac c;
    private URI d;
    private q e;
    private k f;
    private List g;
    private a h;

    m() {
        this(null);
    }

    m(String str) {
        this.b = c.f28a;
        this.f16a = str;
    }

    public static m a(a.a.a.q qVar) {
        a.a.a.n.a.a(qVar, "HTTP request");
        return new m().b(qVar);
    }

    private m b(a.a.a.q qVar) {
        if (qVar != null) {
            this.f16a = qVar.g().a();
            this.c = qVar.g().b();
            if (this.e == null) {
                this.e = new q();
            }
            this.e.a();
            this.e.a(qVar.d());
            this.g = null;
            this.f = null;
            if (qVar instanceof l) {
                k b2 = ((l) qVar).b();
                e a2 = e.a(b2);
                if (a2 == null || !a2.a().equals(e.b.a())) {
                    this.f = b2;
                } else {
                    try {
                        List a3 = g.a(b2);
                        if (!a3.isEmpty()) {
                            this.g = a3;
                        }
                    } catch (IOException e2) {
                    }
                }
            }
            URI i = qVar instanceof l ? ((l) qVar).i() : URI.create(qVar.g().c());
            a.a.a.b.f.e eVar = new a.a.a.b.f.e(i);
            if (this.g == null) {
                List f2 = eVar.f();
                if (!f2.isEmpty()) {
                    this.g = f2;
                    eVar.b();
                } else {
                    this.g = null;
                }
            }
            try {
                this.d = eVar.a();
            } catch (URISyntaxException e3) {
                this.d = i;
            }
            if (qVar instanceof f) {
                this.h = ((f) qVar).b_();
            } else {
                this.h = null;
            }
        }
        return this;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    public l a() {
        URI uri;
        k kVar;
        URI create = this.d != null ? this.d : URI.create("/");
        k kVar2 = this.f;
        if (this.g == null || this.g.isEmpty()) {
            uri = create;
        } else if (kVar2 != null || (!"POST".equalsIgnoreCase(this.f16a) && !"PUT".equalsIgnoreCase(this.f16a))) {
            try {
                uri = new a.a.a.b.f.e(create).a(this.b).a(this.g).a();
            } catch (URISyntaxException e2) {
                uri = create;
            }
        } else {
            kVar2 = new a.a.a.b.b.a(this.g, d.f185a);
            uri = create;
        }
        if (kVar2 == null) {
            kVar = new o(this.f16a);
        } else {
            n nVar = new n(this.f16a);
            nVar.a(kVar2);
            kVar = nVar;
        }
        kVar.a(this.c);
        kVar.a(uri);
        if (this.e != null) {
            kVar.a(this.e.b());
        }
        kVar.a(this.h);
        return kVar;
    }

    public m a(URI uri) {
        this.d = uri;
        return this;
    }
}
