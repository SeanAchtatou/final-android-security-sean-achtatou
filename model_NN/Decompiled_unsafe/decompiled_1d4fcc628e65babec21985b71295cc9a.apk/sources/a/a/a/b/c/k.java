package a.a.a.b.c;

import a.a.a.ac;
import a.a.a.ae;
import a.a.a.b.a.a;
import a.a.a.j.m;
import a.a.a.k.f;
import java.net.URI;

public abstract class k extends b implements f, l {
    private ac c;
    private URI d;
    private a e;

    public void a(ac acVar) {
        this.c = acVar;
    }

    public void a(a aVar) {
        this.e = aVar;
    }

    public void a(URI uri) {
        this.d = uri;
    }

    public abstract String a_();

    public a b_() {
        return this.e;
    }

    public ac c() {
        return this.c != null ? this.c : f.b(f());
    }

    public ae g() {
        String a_ = a_();
        ac c2 = c();
        URI i = i();
        String str = null;
        if (i != null) {
            str = i.toASCIIString();
        }
        if (str == null || str.isEmpty()) {
            str = "/";
        }
        return new m(a_, str, c2);
    }

    public URI i() {
        return this.d;
    }

    public String toString() {
        return a_() + " " + i() + " " + c();
    }
}
