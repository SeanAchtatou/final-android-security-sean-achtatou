package a.a.a.h.b;

import a.a.a.a.h;
import a.a.a.a.j;
import a.a.a.b.c;
import a.a.a.b.i;
import a.a.a.m.e;
import a.a.a.n;
import a.a.a.n.a;
import a.a.a.s;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.Queue;
import org.apache.commons.logging.Log;

@Deprecated
class b implements c {

    /* renamed from: a  reason: collision with root package name */
    private final Log f96a;
    private final a.a.a.b.b b;

    private boolean a(a.a.a.a.c cVar) {
        if (cVar == null || !cVar.d()) {
            return false;
        }
        String a2 = cVar.a();
        return a2.equalsIgnoreCase("Basic") || a2.equalsIgnoreCase("Digest");
    }

    public a.a.a.b.b a() {
        return this.b;
    }

    public Queue a(Map map, n nVar, s sVar, e eVar) {
        a.a(map, "Map of auth challenges");
        a.a(nVar, "Host");
        a.a(sVar, "HTTP response");
        a.a(eVar, "HTTP context");
        LinkedList linkedList = new LinkedList();
        i iVar = (i) eVar.a("http.auth.credentials-provider");
        if (iVar == null) {
            this.f96a.debug("Credentials provider not set in the context");
            return linkedList;
        }
        try {
            a.a.a.a.c a2 = this.b.a(map, sVar, eVar);
            a2.a((a.a.a.e) map.get(a2.a().toLowerCase(Locale.ROOT)));
            a.a.a.a.n a3 = iVar.a(new h(nVar.a(), nVar.b(), a2.b(), a2.a()));
            if (a3 != null) {
                linkedList.add(new a.a.a.a.a(a2, a3));
            }
            return linkedList;
        } catch (j e) {
            if (this.f96a.isWarnEnabled()) {
                this.f96a.warn(e.getMessage(), e);
            }
            return linkedList;
        }
    }

    public void a(n nVar, a.a.a.a.c cVar, e eVar) {
        a.a.a.b.a aVar = (a.a.a.b.a) eVar.a("http.auth.auth-cache");
        if (a(cVar)) {
            if (aVar == null) {
                aVar = new d();
                eVar.a("http.auth.auth-cache", aVar);
            }
            if (this.f96a.isDebugEnabled()) {
                this.f96a.debug("Caching '" + cVar.a() + "' auth scheme for " + nVar);
            }
            aVar.a(nVar, cVar);
        }
    }

    public boolean a(n nVar, s sVar, e eVar) {
        return this.b.a(sVar, eVar);
    }

    public Map b(n nVar, s sVar, e eVar) {
        return this.b.b(sVar, eVar);
    }

    public void b(n nVar, a.a.a.a.c cVar, e eVar) {
        a.a.a.b.a aVar = (a.a.a.b.a) eVar.a("http.auth.auth-cache");
        if (aVar != null) {
            if (this.f96a.isDebugEnabled()) {
                this.f96a.debug("Removing from cache '" + cVar.a() + "' auth scheme for " + nVar);
            }
            aVar.b(nVar);
        }
    }
}
