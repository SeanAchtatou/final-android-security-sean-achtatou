package a.a.a.h.c;

import a.a.a.n.a;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import org.apache.commons.logging.Log;

public class o {

    /* renamed from: a  reason: collision with root package name */
    private final Log f128a;
    private final String b;

    public o(Log log) {
        this(log, "");
    }

    public o(Log log, String str) {
        this.f128a = log;
        this.b = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.insert(int, char):java.lang.StringBuilder}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.CharSequence):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.Object):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, float):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, java.lang.String):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, long):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, char[]):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, int):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, boolean):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, double):java.lang.StringBuilder}
      ClspMth{java.lang.StringBuilder.insert(int, char):java.lang.StringBuilder} */
    private void a(String str, InputStream inputStream) {
        StringBuilder sb = new StringBuilder();
        while (true) {
            int read = inputStream.read();
            if (read == -1) {
                break;
            } else if (read == 13) {
                sb.append("[\\r]");
            } else if (read == 10) {
                sb.append("[\\n]\"");
                sb.insert(0, "\"");
                sb.insert(0, str);
                this.f128a.debug(this.b + " " + sb.toString());
                sb.setLength(0);
            } else if (read < 32 || read > 127) {
                sb.append("[0x");
                sb.append(Integer.toHexString(read));
                sb.append("]");
            } else {
                sb.append((char) read);
            }
        }
        if (sb.length() > 0) {
            sb.append('\"');
            sb.insert(0, '\"');
            sb.insert(0, str);
            this.f128a.debug(this.b + " " + sb.toString());
        }
    }

    public void a(int i) {
        a(new byte[]{(byte) i});
    }

    public void a(byte[] bArr) {
        a.a(bArr, "Output");
        a(">> ", new ByteArrayInputStream(bArr));
    }

    public void a(byte[] bArr, int i, int i2) {
        a.a(bArr, "Output");
        a(">> ", new ByteArrayInputStream(bArr, i, i2));
    }

    public boolean a() {
        return this.f128a.isDebugEnabled();
    }

    public void b(int i) {
        b(new byte[]{(byte) i});
    }

    public void b(byte[] bArr) {
        a.a(bArr, "Input");
        a("<< ", new ByteArrayInputStream(bArr));
    }

    public void b(byte[] bArr, int i, int i2) {
        a.a(bArr, "Input");
        a("<< ", new ByteArrayInputStream(bArr, i, i2));
    }
}
