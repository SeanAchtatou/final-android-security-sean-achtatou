package a.a.a.h.a;

import a.a.a.a.b;
import a.a.a.b.c;
import a.a.a.m.e;
import a.a.a.n;
import a.a.a.s;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class i {

    /* renamed from: a  reason: collision with root package name */
    private final Log f80a;

    public i() {
        this(null);
    }

    public i(Log log) {
        this.f80a = log == null ? LogFactory.getLog(getClass()) : log;
    }

    public boolean a(n nVar, s sVar, c cVar, a.a.a.a.i iVar, e eVar) {
        if (cVar.a(nVar, sVar, eVar)) {
            this.f80a.debug("Authentication required");
            if (iVar.b() == b.SUCCESS) {
                cVar.b(nVar, iVar.c(), eVar);
            }
            return true;
        }
        switch (j.f81a[iVar.b().ordinal()]) {
            case 1:
            case 2:
                this.f80a.debug("Authentication succeeded");
                iVar.a(b.SUCCESS);
                cVar.a(nVar, iVar.c(), eVar);
                break;
            case 3:
                break;
            default:
                iVar.a(b.UNCHALLENGED);
                break;
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0052 A[Catch:{ q -> 0x0088 }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00cc A[Catch:{ q -> 0x0088 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean b(a.a.a.n r7, a.a.a.s r8, a.a.a.b.c r9, a.a.a.a.i r10, a.a.a.m.e r11) {
        /*
            r6 = this;
            r2 = 1
            r1 = 0
            org.apache.commons.logging.Log r0 = r6.f80a     // Catch:{ q -> 0x0088 }
            boolean r0 = r0.isDebugEnabled()     // Catch:{ q -> 0x0088 }
            if (r0 == 0) goto L_0x0026
            org.apache.commons.logging.Log r0 = r6.f80a     // Catch:{ q -> 0x0088 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ q -> 0x0088 }
            r3.<init>()     // Catch:{ q -> 0x0088 }
            java.lang.String r4 = r7.f()     // Catch:{ q -> 0x0088 }
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ q -> 0x0088 }
            java.lang.String r4 = " requested authentication"
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ q -> 0x0088 }
            java.lang.String r3 = r3.toString()     // Catch:{ q -> 0x0088 }
            r0.debug(r3)     // Catch:{ q -> 0x0088 }
        L_0x0026:
            java.util.Map r3 = r9.b(r7, r8, r11)     // Catch:{ q -> 0x0088 }
            boolean r0 = r3.isEmpty()     // Catch:{ q -> 0x0088 }
            if (r0 == 0) goto L_0x0039
            org.apache.commons.logging.Log r0 = r6.f80a     // Catch:{ q -> 0x0088 }
            java.lang.String r2 = "Response contains no authentication challenges"
            r0.debug(r2)     // Catch:{ q -> 0x0088 }
            r0 = r1
        L_0x0038:
            return r0
        L_0x0039:
            a.a.a.a.c r4 = r10.c()     // Catch:{ q -> 0x0088 }
            int[] r0 = a.a.a.h.a.j.f81a     // Catch:{ q -> 0x0088 }
            a.a.a.a.b r5 = r10.b()     // Catch:{ q -> 0x0088 }
            int r5 = r5.ordinal()     // Catch:{ q -> 0x0088 }
            r0 = r0[r5]     // Catch:{ q -> 0x0088 }
            switch(r0) {
                case 1: goto L_0x00b2;
                case 2: goto L_0x00b2;
                case 3: goto L_0x0084;
                case 4: goto L_0x0082;
                case 5: goto L_0x00ca;
                default: goto L_0x004c;
            }     // Catch:{ q -> 0x0088 }
        L_0x004c:
            java.util.Queue r0 = r9.a(r3, r7, r8, r11)     // Catch:{ q -> 0x0088 }
            if (r0 == 0) goto L_0x0114
            boolean r3 = r0.isEmpty()     // Catch:{ q -> 0x0088 }
            if (r3 != 0) goto L_0x0114
            org.apache.commons.logging.Log r3 = r6.f80a     // Catch:{ q -> 0x0088 }
            boolean r3 = r3.isDebugEnabled()     // Catch:{ q -> 0x0088 }
            if (r3 == 0) goto L_0x0078
            org.apache.commons.logging.Log r3 = r6.f80a     // Catch:{ q -> 0x0088 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ q -> 0x0088 }
            r4.<init>()     // Catch:{ q -> 0x0088 }
            java.lang.String r5 = "Selected authentication options: "
            java.lang.StringBuilder r4 = r4.append(r5)     // Catch:{ q -> 0x0088 }
            java.lang.StringBuilder r4 = r4.append(r0)     // Catch:{ q -> 0x0088 }
            java.lang.String r4 = r4.toString()     // Catch:{ q -> 0x0088 }
            r3.debug(r4)     // Catch:{ q -> 0x0088 }
        L_0x0078:
            a.a.a.a.b r3 = a.a.a.a.b.CHALLENGED     // Catch:{ q -> 0x0088 }
            r10.a(r3)     // Catch:{ q -> 0x0088 }
            r10.a(r0)     // Catch:{ q -> 0x0088 }
            r0 = r2
            goto L_0x0038
        L_0x0082:
            r0 = r1
            goto L_0x0038
        L_0x0084:
            r10.a()     // Catch:{ q -> 0x0088 }
            goto L_0x004c
        L_0x0088:
            r0 = move-exception
            org.apache.commons.logging.Log r2 = r6.f80a
            boolean r2 = r2.isWarnEnabled()
            if (r2 == 0) goto L_0x00ad
            org.apache.commons.logging.Log r2 = r6.f80a
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Malformed challenge: "
            java.lang.StringBuilder r3 = r3.append(r4)
            java.lang.String r0 = r0.getMessage()
            java.lang.StringBuilder r0 = r3.append(r0)
            java.lang.String r0 = r0.toString()
            r2.warn(r0)
        L_0x00ad:
            r10.a()
            r0 = r1
            goto L_0x0038
        L_0x00b2:
            if (r4 != 0) goto L_0x00ca
            org.apache.commons.logging.Log r0 = r6.f80a     // Catch:{ q -> 0x0088 }
            java.lang.String r2 = "Auth scheme is null"
            r0.debug(r2)     // Catch:{ q -> 0x0088 }
            r0 = 0
            r9.b(r7, r0, r11)     // Catch:{ q -> 0x0088 }
            r10.a()     // Catch:{ q -> 0x0088 }
            a.a.a.a.b r0 = a.a.a.a.b.FAILURE     // Catch:{ q -> 0x0088 }
            r10.a(r0)     // Catch:{ q -> 0x0088 }
            r0 = r1
            goto L_0x0038
        L_0x00ca:
            if (r4 == 0) goto L_0x004c
            java.lang.String r0 = r4.a()     // Catch:{ q -> 0x0088 }
            java.util.Locale r5 = java.util.Locale.ROOT     // Catch:{ q -> 0x0088 }
            java.lang.String r0 = r0.toLowerCase(r5)     // Catch:{ q -> 0x0088 }
            java.lang.Object r0 = r3.get(r0)     // Catch:{ q -> 0x0088 }
            a.a.a.e r0 = (a.a.a.e) r0     // Catch:{ q -> 0x0088 }
            if (r0 == 0) goto L_0x010f
            org.apache.commons.logging.Log r3 = r6.f80a     // Catch:{ q -> 0x0088 }
            java.lang.String r5 = "Authorization challenge processed"
            r3.debug(r5)     // Catch:{ q -> 0x0088 }
            r4.a(r0)     // Catch:{ q -> 0x0088 }
            boolean r0 = r4.d()     // Catch:{ q -> 0x0088 }
            if (r0 == 0) goto L_0x0107
            org.apache.commons.logging.Log r0 = r6.f80a     // Catch:{ q -> 0x0088 }
            java.lang.String r2 = "Authentication failed"
            r0.debug(r2)     // Catch:{ q -> 0x0088 }
            a.a.a.a.c r0 = r10.c()     // Catch:{ q -> 0x0088 }
            r9.b(r7, r0, r11)     // Catch:{ q -> 0x0088 }
            r10.a()     // Catch:{ q -> 0x0088 }
            a.a.a.a.b r0 = a.a.a.a.b.FAILURE     // Catch:{ q -> 0x0088 }
            r10.a(r0)     // Catch:{ q -> 0x0088 }
            r0 = r1
            goto L_0x0038
        L_0x0107:
            a.a.a.a.b r0 = a.a.a.a.b.HANDSHAKE     // Catch:{ q -> 0x0088 }
            r10.a(r0)     // Catch:{ q -> 0x0088 }
            r0 = r2
            goto L_0x0038
        L_0x010f:
            r10.a()     // Catch:{ q -> 0x0088 }
            goto L_0x004c
        L_0x0114:
            r0 = r1
            goto L_0x0038
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.h.a.i.b(a.a.a.n, a.a.a.s, a.a.a.b.c, a.a.a.a.i, a.a.a.m.e):boolean");
    }
}
