package a.a.a.h.c;

import a.a.a.e.c.g;
import a.a.a.e.d;
import a.a.a.e.j;
import a.a.a.e.q;
import a.a.a.k.c;
import a.a.a.n;
import a.a.a.n.a;
import a.a.a.n.b;
import java.net.InetAddress;
import java.net.Socket;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@Deprecated
public class e implements d {

    /* renamed from: a  reason: collision with root package name */
    protected final g f120a;
    protected final j b;
    private final Log c = LogFactory.getLog(getClass());

    public e(g gVar) {
        a.a(gVar, "Scheme registry");
        this.f120a = gVar;
        this.b = new n();
    }

    private g a(a.a.a.m.e eVar) {
        g gVar = (g) eVar.a("http.scheme-registry");
        return gVar == null ? this.f120a : gVar;
    }

    public q a() {
        return new d();
    }

    public void a(q qVar, n nVar, a.a.a.m.e eVar, a.a.a.k.e eVar2) {
        a.a(qVar, "Connection");
        a.a(nVar, "Target host");
        a.a(eVar2, "Parameters");
        b.a(qVar.c(), "Connection must be open");
        a.a.a.e.c.d a2 = a(eVar).a(nVar.c());
        b.a(a2.b() instanceof a.a.a.e.c.e, "Socket factory must implement SchemeLayeredSocketFactory");
        a.a.a.e.c.e eVar3 = (a.a.a.e.c.e) a2.b();
        Socket a3 = eVar3.a(qVar.i(), nVar.a(), a2.a(nVar.b()), eVar2);
        a(a3, eVar, eVar2);
        qVar.a(a3, nVar, eVar3.a(a3), eVar2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x00b5  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00d9 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(a.a.a.e.q r14, a.a.a.n r15, java.net.InetAddress r16, a.a.a.m.e r17, a.a.a.k.e r18) {
        /*
            r13 = this;
            java.lang.String r2 = "Connection"
            a.a.a.n.a.a(r14, r2)
            java.lang.String r2 = "Target host"
            a.a.a.n.a.a(r15, r2)
            java.lang.String r2 = "HTTP parameters"
            r0 = r18
            a.a.a.n.a.a(r0, r2)
            boolean r2 = r14.c()
            if (r2 != 0) goto L_0x00a0
            r2 = 1
        L_0x0018:
            java.lang.String r3 = "Connection must not be open"
            a.a.a.n.b.a(r2, r3)
            r0 = r17
            a.a.a.e.c.g r2 = r13.a(r0)
            java.lang.String r3 = r15.c()
            a.a.a.e.c.d r2 = r2.a(r3)
            a.a.a.e.c.h r6 = r2.b()
            java.lang.String r3 = r15.a()
            java.net.InetAddress[] r7 = r13.a(r3)
            int r3 = r15.b()
            int r8 = r2.a(r3)
            r2 = 0
        L_0x0040:
            int r3 = r7.length
            if (r2 >= r3) goto L_0x009f
            r4 = r7[r2]
            int r3 = r7.length
            int r3 = r3 + -1
            if (r2 != r3) goto L_0x00a3
            r3 = 1
        L_0x004b:
            r0 = r18
            java.net.Socket r5 = r6.a(r0)
            r14.a(r5, r15)
            a.a.a.e.m r9 = new a.a.a.e.m
            r9.<init>(r15, r4, r8)
            r4 = 0
            if (r16 == 0) goto L_0x0064
            java.net.InetSocketAddress r4 = new java.net.InetSocketAddress
            r10 = 0
            r0 = r16
            r4.<init>(r0, r10)
        L_0x0064:
            org.apache.commons.logging.Log r10 = r13.c
            boolean r10 = r10.isDebugEnabled()
            if (r10 == 0) goto L_0x0084
            org.apache.commons.logging.Log r10 = r13.c
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r12 = "Connecting to "
            java.lang.StringBuilder r11 = r11.append(r12)
            java.lang.StringBuilder r11 = r11.append(r9)
            java.lang.String r11 = r11.toString()
            r10.debug(r11)
        L_0x0084:
            r0 = r18
            java.net.Socket r4 = r6.a(r5, r9, r4, r0)     // Catch:{ ConnectException -> 0x00a5, f -> 0x00a9 }
            if (r5 == r4) goto L_0x00dd
            r14.a(r4, r15)     // Catch:{ ConnectException -> 0x00a5, f -> 0x00a9 }
        L_0x008f:
            r0 = r17
            r1 = r18
            r13.a(r4, r0, r1)     // Catch:{ ConnectException -> 0x00a5, f -> 0x00a9 }
            boolean r4 = r6.a(r4)     // Catch:{ ConnectException -> 0x00a5, f -> 0x00a9 }
            r0 = r18
            r14.a(r4, r0)     // Catch:{ ConnectException -> 0x00a5, f -> 0x00a9 }
        L_0x009f:
            return
        L_0x00a0:
            r2 = 0
            goto L_0x0018
        L_0x00a3:
            r3 = 0
            goto L_0x004b
        L_0x00a5:
            r4 = move-exception
            if (r3 == 0) goto L_0x00ad
            throw r4
        L_0x00a9:
            r4 = move-exception
            if (r3 == 0) goto L_0x00ad
            throw r4
        L_0x00ad:
            org.apache.commons.logging.Log r3 = r13.c
            boolean r3 = r3.isDebugEnabled()
            if (r3 == 0) goto L_0x00d9
            org.apache.commons.logging.Log r3 = r13.c
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Connect to "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.StringBuilder r4 = r4.append(r9)
            java.lang.String r5 = " timed out. "
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r5 = "Connection will be retried using another IP address"
            java.lang.StringBuilder r4 = r4.append(r5)
            java.lang.String r4 = r4.toString()
            r3.debug(r4)
        L_0x00d9:
            int r2 = r2 + 1
            goto L_0x0040
        L_0x00dd:
            r4 = r5
            goto L_0x008f
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.h.c.e.a(a.a.a.e.q, a.a.a.n, java.net.InetAddress, a.a.a.m.e, a.a.a.k.e):void");
    }

    /* access modifiers changed from: protected */
    public void a(Socket socket, a.a.a.m.e eVar, a.a.a.k.e eVar2) {
        socket.setTcpNoDelay(c.c(eVar2));
        socket.setSoTimeout(c.a(eVar2));
        int d = c.d(eVar2);
        if (d >= 0) {
            socket.setSoLinger(d > 0, d);
        }
    }

    /* access modifiers changed from: protected */
    public InetAddress[] a(String str) {
        return this.b.a(str);
    }
}
