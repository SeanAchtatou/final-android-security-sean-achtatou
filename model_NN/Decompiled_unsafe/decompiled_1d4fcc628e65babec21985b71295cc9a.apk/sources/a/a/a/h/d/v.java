package a.a.a.h.d;

import a.a.a.f.c;
import a.a.a.f.f;
import a.a.a.f.h;
import a.a.a.f.n;
import a.a.a.f.o;
import a.a.a.n.a;
import a.a.a.n.i;
import java.util.Locale;
import java.util.StringTokenizer;

public class v extends f {
    private static boolean a(String str) {
        String upperCase = str.toUpperCase(Locale.ROOT);
        return upperCase.endsWith(".COM") || upperCase.endsWith(".EDU") || upperCase.endsWith(".NET") || upperCase.endsWith(".GOV") || upperCase.endsWith(".MIL") || upperCase.endsWith(".ORG") || upperCase.endsWith(".INT");
    }

    public String a() {
        return "domain";
    }

    public void a(c cVar, f fVar) {
        String a2 = fVar.a();
        String d = cVar.d();
        if (!a2.equals(d) && !f.a(d, a2)) {
            throw new h("Illegal domain attribute \"" + d + "\". Domain of origin: \"" + a2 + "\"");
        } else if (a2.contains(".")) {
            int countTokens = new StringTokenizer(d, ".").countTokens();
            if (a(d)) {
                if (countTokens < 2) {
                    throw new h("Domain attribute \"" + d + "\" violates the Netscape cookie specification for " + "special domains");
                }
            } else if (countTokens < 3) {
                throw new h("Domain attribute \"" + d + "\" violates the Netscape cookie specification");
            }
        }
    }

    public void a(o oVar, String str) {
        a.a(oVar, "Cookie");
        if (i.b(str)) {
            throw new n("Blank or null value for domain attribute");
        }
        oVar.d(str);
    }

    public boolean b(c cVar, f fVar) {
        a.a(cVar, "Cookie");
        a.a(fVar, "Cookie origin");
        String a2 = fVar.a();
        String d = cVar.d();
        if (d == null) {
            return false;
        }
        return a2.endsWith(d);
    }
}
