package a.a.a.h.b;

import a.a.a.b.k;
import a.a.a.m.e;
import a.a.a.n.a;
import a.a.a.q;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ConnectException;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import javax.net.ssl.SSLException;

public class l implements k {

    /* renamed from: a  reason: collision with root package name */
    public static final l f105a = new l();
    private final int b;
    private final boolean c;
    private final Set d;

    public l() {
        this(3, false);
    }

    public l(int i, boolean z) {
        this(i, z, Arrays.asList(InterruptedIOException.class, UnknownHostException.class, ConnectException.class, SSLException.class));
    }

    protected l(int i, boolean z, Collection collection) {
        this.b = i;
        this.c = z;
        this.d = new HashSet();
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            this.d.add((Class) it.next());
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(q qVar) {
        return !(qVar instanceof a.a.a.l);
    }

    public boolean a(IOException iOException, int i, e eVar) {
        a.a(iOException, "Exception parameter");
        a.a(eVar, "HTTP context");
        if (i > this.b) {
            return false;
        }
        if (this.d.contains(iOException.getClass())) {
            return false;
        }
        for (Class isInstance : this.d) {
            if (isInstance.isInstance(iOException)) {
                return false;
            }
        }
        a.a.a.b.e.a a2 = a.a.a.b.e.a.a(eVar);
        q m = a2.m();
        if (b(m)) {
            return false;
        }
        if (a(m)) {
            return true;
        }
        return !a2.n() || this.c;
    }

    /* access modifiers changed from: protected */
    @Deprecated
    public boolean b(q qVar) {
        q l = qVar instanceof v ? ((v) qVar).l() : qVar;
        return (l instanceof a.a.a.b.c.l) && ((a.a.a.b.c.l) l).h();
    }
}
