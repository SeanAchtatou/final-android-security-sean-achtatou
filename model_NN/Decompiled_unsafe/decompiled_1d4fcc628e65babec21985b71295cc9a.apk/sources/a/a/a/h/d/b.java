package a.a.a.h.d;

import a.a.a.f.d;
import a.a.a.f.i;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class b implements i {

    /* renamed from: a  reason: collision with root package name */
    private final Map f133a;

    public b() {
        this.f133a = new ConcurrentHashMap(10);
    }

    protected b(a.a.a.f.b... bVarArr) {
        this.f133a = new ConcurrentHashMap(bVarArr.length);
        for (a.a.a.f.b bVar : bVarArr) {
            this.f133a.put(bVar.a(), bVar);
        }
    }

    /* access modifiers changed from: protected */
    public d a(String str) {
        return (d) this.f133a.get(str);
    }

    /* access modifiers changed from: protected */
    public Collection c() {
        return this.f133a.values();
    }
}
