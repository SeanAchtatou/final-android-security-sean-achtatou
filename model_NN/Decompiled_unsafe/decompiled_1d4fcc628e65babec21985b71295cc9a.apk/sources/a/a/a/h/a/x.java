package a.a.a.h.a;

import a.a.a.a.j;
import a.a.a.a.n;
import a.a.a.a.o;
import a.a.a.a.r;
import a.a.a.e;
import a.a.a.j.p;
import a.a.a.n.a;
import a.a.a.n.d;
import a.a.a.q;

public class x extends a {

    /* renamed from: a  reason: collision with root package name */
    private final n f92a;
    private y b;
    private String c;

    public x() {
        this(new p());
    }

    public x(n nVar) {
        a.a(nVar, "NTLM engine");
        this.f92a = nVar;
        this.b = y.UNINITIATED;
        this.c = null;
    }

    public e a(n nVar, q qVar) {
        String a2;
        try {
            r rVar = (r) nVar;
            if (this.b == y.FAILED) {
                throw new j("NTLM authentication failed");
            }
            if (this.b == y.CHALLENGE_RECEIVED) {
                a2 = this.f92a.a(rVar.d(), rVar.e());
                this.b = y.MSG_TYPE1_GENERATED;
            } else if (this.b == y.MSG_TYPE2_RECEVIED) {
                a2 = this.f92a.a(rVar.c(), rVar.b(), rVar.d(), rVar.e(), this.c);
                this.b = y.MSG_TYPE3_GENERATED;
            } else {
                throw new j("Unexpected state: " + this.b);
            }
            d dVar = new d(32);
            if (e()) {
                dVar.a("Proxy-Authorization");
            } else {
                dVar.a("Authorization");
            }
            dVar.a(": NTLM ");
            dVar.a(a2);
            return new p(dVar);
        } catch (ClassCastException e) {
            throw new o("Credentials cannot be used for NTLM authentication: " + nVar.getClass().getName());
        }
    }

    public String a() {
        return "ntlm";
    }

    /* access modifiers changed from: protected */
    public void a(d dVar, int i, int i2) {
        this.c = dVar.b(i, i2);
        if (this.c.isEmpty()) {
            if (this.b == y.UNINITIATED) {
                this.b = y.CHALLENGE_RECEIVED;
            } else {
                this.b = y.FAILED;
            }
        } else if (this.b.compareTo((Enum) y.MSG_TYPE1_GENERATED) < 0) {
            this.b = y.FAILED;
            throw new a.a.a.a.q("Out of sequence NTLM response message");
        } else if (this.b == y.MSG_TYPE1_GENERATED) {
            this.b = y.MSG_TYPE2_RECEVIED;
        }
    }

    public String b() {
        return null;
    }

    public boolean c() {
        return true;
    }

    public boolean d() {
        return this.b == y.MSG_TYPE3_GENERATED || this.b == y.FAILED;
    }
}
