package a.a.a.h.a;

import a.a.a.n.f;
import org.apache.commons.codec.binary.Base64;

class t {

    /* renamed from: a  reason: collision with root package name */
    private byte[] f88a = null;
    private int b = 0;

    t() {
    }

    t(String str, int i) {
        this.f88a = Base64.decodeBase64(str.getBytes(p.b));
        if (this.f88a.length < p.d.length) {
            throw new o("NTLM message decoding error - packet too short");
        }
        for (int i2 = 0; i2 < p.d.length; i2++) {
            if (this.f88a[i2] != p.d[i2]) {
                throw new o("NTLM message expected - instead got unrecognized bytes");
            }
        }
        int a2 = a(p.d.length);
        if (a2 != i) {
            throw new o("NTLM type " + Integer.toString(i) + " message expected - instead got type " + Integer.toString(a2));
        }
        this.b = this.f88a.length;
    }

    /* access modifiers changed from: protected */
    public int a() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public int a(int i) {
        return p.d(this.f88a, i);
    }

    /* access modifiers changed from: protected */
    public void a(byte b2) {
        this.f88a[this.b] = b2;
        this.b++;
    }

    /* access modifiers changed from: protected */
    public void a(int i, int i2) {
        this.f88a = new byte[i];
        this.b = 0;
        a(p.d);
        d(i2);
    }

    /* access modifiers changed from: protected */
    public void a(byte[] bArr) {
        if (bArr != null) {
            for (byte b2 : bArr) {
                this.f88a[this.b] = b2;
                this.b++;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(byte[] bArr, int i) {
        if (this.f88a.length < bArr.length + i) {
            throw new o("NTLM: Message too short");
        }
        System.arraycopy(this.f88a, i, bArr, 0, bArr.length);
    }

    /* access modifiers changed from: package-private */
    public String b() {
        byte[] bArr;
        if (this.f88a.length > this.b) {
            bArr = new byte[this.b];
            System.arraycopy(this.f88a, 0, bArr, 0, this.b);
        } else {
            bArr = this.f88a;
        }
        return f.a(Base64.encodeBase64(bArr));
    }

    /* access modifiers changed from: protected */
    public byte[] b(int i) {
        return p.f(this.f88a, i);
    }

    /* access modifiers changed from: protected */
    public void c(int i) {
        a((byte) (i & 255));
        a((byte) ((i >> 8) & 255));
    }

    /* access modifiers changed from: protected */
    public void d(int i) {
        a((byte) (i & 255));
        a((byte) ((i >> 8) & 255));
        a((byte) ((i >> 16) & 255));
        a((byte) ((i >> 24) & 255));
    }
}
