package a.a.a.h.d;

import a.a.a.f.p;
import java.util.Date;

public class d extends c implements p {

    /* renamed from: a  reason: collision with root package name */
    private String f135a;
    private int[] b;
    private boolean c;

    public d(String str, String str2) {
        super(str, str2);
    }

    public void a(int[] iArr) {
        this.b = iArr;
    }

    public boolean a(Date date) {
        return this.c || super.a(date);
    }

    public void a_(String str) {
        this.f135a = str;
    }

    public void b(boolean z) {
        this.c = z;
    }

    public Object clone() {
        d dVar = (d) super.clone();
        if (this.b != null) {
            dVar.b = (int[]) this.b.clone();
        }
        return dVar;
    }

    public int[] f() {
        return this.b;
    }
}
