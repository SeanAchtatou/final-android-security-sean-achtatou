package a.a.a.h;

import a.a.a.ad;
import a.a.a.af;
import a.a.a.j.h;
import a.a.a.m.e;
import a.a.a.n.a;
import a.a.a.s;
import a.a.a.t;
import java.util.Locale;

public class c implements t {

    /* renamed from: a  reason: collision with root package name */
    public static final c f116a = new c();
    protected final ad b;

    public c() {
        this(d.f129a);
    }

    public c(ad adVar) {
        this.b = (ad) a.a(adVar, "Reason phrase catalog");
    }

    public s a(af afVar, e eVar) {
        a.a(afVar, "Status line");
        return new h(afVar, this.b, a(eVar));
    }

    /* access modifiers changed from: protected */
    public Locale a(e eVar) {
        return Locale.getDefault();
    }
}
