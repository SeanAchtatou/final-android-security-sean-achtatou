package a.a.a.h.a;

import a.a.a.a.l;
import a.a.a.a.m;
import a.a.a.a.n;
import a.a.a.e;
import a.a.a.n.d;
import a.a.a.q;
import java.util.Locale;

public abstract class a implements m {

    /* renamed from: a  reason: collision with root package name */
    private l f70a;

    public e a(n nVar, q qVar, a.a.a.m.e eVar) {
        return a(nVar, qVar);
    }

    public void a(e eVar) {
        d dVar;
        int i;
        a.a.a.n.a.a(eVar, "Header");
        String c = eVar.c();
        if (c.equalsIgnoreCase("WWW-Authenticate")) {
            this.f70a = l.TARGET;
        } else if (c.equalsIgnoreCase("Proxy-Authenticate")) {
            this.f70a = l.PROXY;
        } else {
            throw new a.a.a.a.q("Unexpected header name: " + c);
        }
        if (eVar instanceof a.a.a.d) {
            dVar = ((a.a.a.d) eVar).a();
            i = ((a.a.a.d) eVar).b();
        } else {
            String d = eVar.d();
            if (d == null) {
                throw new a.a.a.a.q("Header value is null");
            }
            dVar = new d(d.length());
            dVar.a(d);
            i = 0;
        }
        while (i < dVar.length() && a.a.a.m.d.a(dVar.charAt(i))) {
            i++;
        }
        int i2 = i;
        while (i2 < dVar.length() && !a.a.a.m.d.a(dVar.charAt(i2))) {
            i2++;
        }
        String a2 = dVar.a(i, i2);
        if (!a2.equalsIgnoreCase(a())) {
            throw new a.a.a.a.q("Invalid scheme identifier: " + a2);
        }
        a(dVar, i2, dVar.length());
    }

    /* access modifiers changed from: protected */
    public abstract void a(d dVar, int i, int i2);

    public boolean e() {
        return this.f70a != null && this.f70a == l.PROXY;
    }

    public String toString() {
        String a2 = a();
        return a2 != null ? a2.toUpperCase(Locale.ROOT) : super.toString();
    }
}
