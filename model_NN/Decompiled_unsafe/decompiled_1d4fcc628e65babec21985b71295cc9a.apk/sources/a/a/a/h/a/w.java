package a.a.a.h.a;

import java.util.Locale;

class w extends t {

    /* renamed from: a  reason: collision with root package name */
    protected int f91a;
    protected byte[] b;
    protected byte[] c;
    protected byte[] d;
    protected byte[] e;
    protected byte[] f;
    protected byte[] g;

    w(String str, String str2, String str3, String str4, byte[] bArr, int i, String str5, byte[] bArr2) {
        byte[] t;
        this.f91a = i;
        String c2 = p.f(str2);
        String d2 = p.g(str);
        q qVar = new q(d2, str3, str4, bArr, str5, bArr2);
        if ((8388608 & i) != 0 && bArr2 != null && str5 != null) {
            try {
                this.f = qVar.l();
                this.e = qVar.m();
                t = (i & 128) != 0 ? qVar.t() : qVar.r();
            } catch (o e2) {
                this.f = new byte[0];
                this.e = qVar.e();
                t = (i & 128) != 0 ? qVar.t() : qVar.p();
            }
        } else if ((524288 & i) != 0) {
            this.f = qVar.n();
            this.e = qVar.o();
            t = (i & 128) != 0 ? qVar.t() : qVar.s();
        } else {
            this.f = qVar.g();
            this.e = qVar.e();
            t = (i & 128) != 0 ? qVar.t() : qVar.q();
        }
        if ((i & 16) == 0) {
            this.g = null;
        } else if ((1073741824 & i) != 0) {
            this.g = p.b(qVar.c(), t);
        } else {
            this.g = t;
        }
        if (p.f84a == null) {
            throw new o("Unicode not supported");
        }
        this.c = c2 != null ? c2.getBytes(p.f84a) : null;
        this.b = d2 != null ? d2.toUpperCase(Locale.ROOT).getBytes(p.f84a) : null;
        this.d = str3.getBytes(p.f84a);
    }

    /* access modifiers changed from: package-private */
    public String b() {
        int i = 0;
        int length = this.f.length;
        int length2 = this.e.length;
        int length3 = this.b != null ? this.b.length : 0;
        int length4 = this.c != null ? this.c.length : 0;
        int length5 = this.d.length;
        if (this.g != null) {
            i = this.g.length;
        }
        int i2 = length2 + 72;
        int i3 = i2 + length;
        int i4 = i3 + length3;
        int i5 = i4 + length5;
        int i6 = i5 + length4;
        a(i6 + i, 3);
        c(length2);
        c(length2);
        d(72);
        c(length);
        c(length);
        d(i2);
        c(length3);
        c(length3);
        d(i3);
        c(length5);
        c(length5);
        d(i4);
        c(length4);
        c(length4);
        d(i5);
        c(i);
        c(i);
        d(i6);
        d((this.f91a & 128) | (this.f91a & 512) | (this.f91a & 524288) | 33554432 | (this.f91a & 32768) | (this.f91a & 32) | (this.f91a & 16) | (this.f91a & 536870912) | (this.f91a & Integer.MIN_VALUE) | (this.f91a & 1073741824) | (this.f91a & 8388608) | (this.f91a & 1) | (this.f91a & 4));
        c(261);
        d(2600);
        c(3840);
        a(this.e);
        a(this.f);
        a(this.b);
        a(this.d);
        a(this.c);
        if (this.g != null) {
            a(this.g);
        }
        return super.b();
    }
}
