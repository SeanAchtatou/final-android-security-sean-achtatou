package a.a.a.h.d;

import a.a.a.f.i;
import a.a.a.f.j;
import a.a.a.f.k;
import a.a.a.k.e;
import java.util.Collection;

@Deprecated
public class ab implements j, k {

    /* renamed from: a  reason: collision with root package name */
    private final i f131a;

    public ab() {
        this(null, false);
    }

    public ab(String[] strArr, boolean z) {
        this.f131a = new aa(strArr, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.k.e.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      a.a.a.k.e.a(java.lang.String, int):int
      a.a.a.k.e.a(java.lang.String, long):long
      a.a.a.k.e.a(java.lang.String, java.lang.Object):a.a.a.k.e
      a.a.a.k.e.a(java.lang.String, boolean):boolean */
    public i a(e eVar) {
        if (eVar == null) {
            return new aa();
        }
        Collection collection = (Collection) eVar.a("http.protocol.cookie-datepatterns");
        return new aa(collection != null ? (String[]) collection.toArray(new String[collection.size()]) : null, eVar.a("http.protocol.single-cookie-header", false));
    }

    public i a(a.a.a.m.e eVar) {
        return this.f131a;
    }
}
