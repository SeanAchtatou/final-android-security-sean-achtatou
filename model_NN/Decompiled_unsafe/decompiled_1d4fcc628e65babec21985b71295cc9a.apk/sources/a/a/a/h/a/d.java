package a.a.a.h.a;

import a.a.a.a.j;
import a.a.a.a.n;
import a.a.a.c;
import a.a.a.e;
import a.a.a.j.p;
import a.a.a.k;
import a.a.a.l;
import a.a.a.m.a;
import a.a.a.n.f;
import a.a.a.q;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashSet;
import java.util.Locale;
import java.util.StringTokenizer;

public class d extends aa {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f75a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    private boolean b;
    private String c;
    private long d;
    private String e;
    private String f;
    private String g;

    public d() {
        this(c.b);
    }

    public d(Charset charset) {
        super(charset);
        this.b = false;
    }

    static String a(byte[] bArr) {
        int length = bArr.length;
        char[] cArr = new char[(length * 2)];
        for (int i = 0; i < length; i++) {
            cArr[i * 2] = f75a[(bArr[i] & 240) >> 4];
            cArr[(i * 2) + 1] = f75a[bArr[i] & 15];
        }
        return new String(cArr);
    }

    private e b(n nVar, q qVar) {
        char c2;
        String sb;
        String a2 = a("uri");
        String a3 = a("realm");
        String a4 = a("nonce");
        String a5 = a("opaque");
        String a6 = a("methodname");
        String a7 = a("algorithm");
        if (a7 == null) {
            a7 = "MD5";
        }
        HashSet hashSet = new HashSet(8);
        char c3 = 65535;
        String a8 = a("qop");
        if (a8 != null) {
            StringTokenizer stringTokenizer = new StringTokenizer(a8, ",");
            while (stringTokenizer.hasMoreTokens()) {
                hashSet.add(stringTokenizer.nextToken().trim().toLowerCase(Locale.ROOT));
            }
            if ((qVar instanceof l) && hashSet.contains("auth-int")) {
                c3 = 1;
            } else if (hashSet.contains("auth")) {
                c3 = 2;
            }
            c2 = c3;
        } else {
            c2 = 0;
        }
        if (c2 == 65535) {
            throw new j("None of the qop methods is supported: " + a8);
        }
        String a9 = a("charset");
        if (a9 == null) {
            a9 = "ISO-8859-1";
        }
        String str = a7.equalsIgnoreCase("MD5-sess") ? "MD5" : a7;
        try {
            MessageDigest b2 = b(str);
            String name = nVar.a().getName();
            String b3 = nVar.b();
            if (a4.equals(this.c)) {
                this.d = this.d + 1;
            } else {
                this.d = 1;
                this.e = null;
                this.c = a4;
            }
            StringBuilder sb2 = new StringBuilder(256);
            Formatter formatter = new Formatter(sb2, Locale.US);
            formatter.format("%08x", Long.valueOf(this.d));
            formatter.close();
            String sb3 = sb2.toString();
            if (this.e == null) {
                this.e = f();
            }
            this.f = null;
            this.g = null;
            if (a7.equalsIgnoreCase("MD5-sess")) {
                sb2.setLength(0);
                sb2.append(name).append(':').append(a3).append(':').append(b3);
                String a10 = a(b2.digest(f.a(sb2.toString(), a9)));
                sb2.setLength(0);
                sb2.append(a10).append(':').append(a4).append(':').append(this.e);
                this.f = sb2.toString();
            } else {
                sb2.setLength(0);
                sb2.append(name).append(':').append(a3).append(':').append(b3);
                this.f = sb2.toString();
            }
            String a11 = a(b2.digest(f.a(this.f, a9)));
            if (c2 == 2) {
                this.g = a6 + ':' + a2;
            } else if (c2 == 1) {
                k kVar = null;
                if (qVar instanceof l) {
                    kVar = ((l) qVar).b();
                }
                if (kVar == null || kVar.a()) {
                    k kVar2 = new k(b2);
                    if (kVar != null) {
                        try {
                            kVar.a(kVar2);
                        } catch (IOException e2) {
                            throw new j("I/O error reading entity content", e2);
                        }
                    }
                    kVar2.close();
                    this.g = a6 + ':' + a2 + ':' + a(kVar2.a());
                } else if (hashSet.contains("auth")) {
                    c2 = 2;
                    this.g = a6 + ':' + a2;
                } else {
                    throw new j("Qop auth-int cannot be used with a non-repeatable entity");
                }
            } else {
                this.g = a6 + ':' + a2;
            }
            String a12 = a(b2.digest(f.a(this.g, a9)));
            if (c2 == 0) {
                sb2.setLength(0);
                sb2.append(a11).append(':').append(a4).append(':').append(a12);
                sb = sb2.toString();
            } else {
                sb2.setLength(0);
                sb2.append(a11).append(':').append(a4).append(':').append(sb3).append(':').append(this.e).append(':').append(c2 == 1 ? "auth-int" : "auth").append(':').append(a12);
                sb = sb2.toString();
            }
            String a13 = a(b2.digest(f.a(sb)));
            a.a.a.n.d dVar = new a.a.a.n.d(128);
            if (e()) {
                dVar.a("Proxy-Authorization");
            } else {
                dVar.a("Authorization");
            }
            dVar.a(": Digest ");
            ArrayList arrayList = new ArrayList(20);
            arrayList.add(new a.a.a.j.l("username", name));
            arrayList.add(new a.a.a.j.l("realm", a3));
            arrayList.add(new a.a.a.j.l("nonce", a4));
            arrayList.add(new a.a.a.j.l("uri", a2));
            arrayList.add(new a.a.a.j.l("response", a13));
            if (c2 != 0) {
                arrayList.add(new a.a.a.j.l("qop", c2 == 1 ? "auth-int" : "auth"));
                arrayList.add(new a.a.a.j.l("nc", sb3));
                arrayList.add(new a.a.a.j.l("cnonce", this.e));
            }
            arrayList.add(new a.a.a.j.l("algorithm", a7));
            if (a5 != null) {
                arrayList.add(new a.a.a.j.l("opaque", a5));
            }
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= arrayList.size()) {
                    return new p(dVar);
                }
                a.a.a.j.l lVar = (a.a.a.j.l) arrayList.get(i2);
                if (i2 > 0) {
                    dVar.a(", ");
                }
                String a14 = lVar.a();
                a.a.a.j.e.b.a(dVar, lVar, !("nc".equals(a14) || "qop".equals(a14) || "algorithm".equals(a14)));
                i = i2 + 1;
            }
        } catch (ad e3) {
            throw new j("Unsuppported digest algorithm: " + str);
        }
    }

    private static MessageDigest b(String str) {
        try {
            return MessageDigest.getInstance(str);
        } catch (Exception e2) {
            throw new ad("Unsupported algorithm in HTTP Digest authentication: " + str);
        }
    }

    public static String f() {
        byte[] bArr = new byte[8];
        new SecureRandom().nextBytes(bArr);
        return a(bArr);
    }

    @Deprecated
    public e a(n nVar, q qVar) {
        return a(nVar, qVar, new a());
    }

    public e a(n nVar, q qVar, a.a.a.m.e eVar) {
        a.a.a.n.a.a(nVar, "Credentials");
        a.a.a.n.a.a(qVar, "HTTP request");
        if (a("realm") == null) {
            throw new j("missing realm in challenge");
        } else if (a("nonce") == null) {
            throw new j("missing nonce in challenge");
        } else {
            h().put("methodname", qVar.g().a());
            h().put("uri", qVar.g().c());
            if (a("charset") == null) {
                h().put("charset", a(qVar));
            }
            return b(nVar, qVar);
        }
    }

    public String a() {
        return "digest";
    }

    public void a(e eVar) {
        super.a(eVar);
        this.b = true;
        if (h().isEmpty()) {
            throw new a.a.a.a.q("Authentication challenge is empty");
        }
    }

    public boolean c() {
        return false;
    }

    public boolean d() {
        if ("true".equalsIgnoreCase(a("stale"))) {
            return false;
        }
        return this.b;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("DIGEST [complete=").append(this.b).append(", nonce=").append(this.c).append(", nc=").append(this.d).append("]");
        return sb.toString();
    }
}
