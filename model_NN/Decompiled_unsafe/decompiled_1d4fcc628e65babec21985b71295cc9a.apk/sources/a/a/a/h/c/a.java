package a.a.a.h.c;

import a.a.a.e.b;
import a.a.a.e.c.g;
import a.a.a.e.d;
import a.a.a.e.e;
import a.a.a.e.o;
import a.a.a.i;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

@Deprecated
public class a implements b {
    private static final AtomicLong b = new AtomicLong();

    /* renamed from: a  reason: collision with root package name */
    private final Log f117a;
    private final g c;
    private final d d;
    private i e;
    private l f;
    private volatile boolean g;

    public a() {
        this(m.a());
    }

    public a(g gVar) {
        this.f117a = LogFactory.getLog(getClass());
        a.a.a.n.a.a(gVar, "Scheme registry");
        this.c = gVar;
        this.d = a(gVar);
    }

    private void a(i iVar) {
        try {
            iVar.e();
        } catch (IOException e2) {
            if (this.f117a.isDebugEnabled()) {
                this.f117a.debug("I/O exception shutting down connection", e2);
            }
        }
    }

    private void c() {
        a.a.a.n.b.a(!this.g, "Connection manager has been shut down");
    }

    public g a() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public d a(g gVar) {
        return new e(gVar);
    }

    public final e a(a.a.a.e.b.b bVar, Object obj) {
        return new b(this, bVar, obj);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:65:?, code lost:
        return;
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(a.a.a.e.o r7, long r8, java.util.concurrent.TimeUnit r10) {
        /*
            r6 = this;
            boolean r0 = r7 instanceof a.a.a.h.c.l
            java.lang.String r1 = "Connection class mismatch, connection not obtained from this manager"
            a.a.a.n.a.a(r0, r1)
            r0 = r7
            a.a.a.h.c.l r0 = (a.a.a.h.c.l) r0
            monitor-enter(r0)
            org.apache.commons.logging.Log r1 = r6.f117a     // Catch:{ all -> 0x004a }
            boolean r1 = r1.isDebugEnabled()     // Catch:{ all -> 0x004a }
            if (r1 == 0) goto L_0x002b
            org.apache.commons.logging.Log r1 = r6.f117a     // Catch:{ all -> 0x004a }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x004a }
            r2.<init>()     // Catch:{ all -> 0x004a }
            java.lang.String r3 = "Releasing connection "
            java.lang.StringBuilder r2 = r2.append(r3)     // Catch:{ all -> 0x004a }
            java.lang.StringBuilder r2 = r2.append(r7)     // Catch:{ all -> 0x004a }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x004a }
            r1.debug(r2)     // Catch:{ all -> 0x004a }
        L_0x002b:
            a.a.a.h.c.i r1 = r0.n()     // Catch:{ all -> 0x004a }
            if (r1 != 0) goto L_0x0033
            monitor-exit(r0)     // Catch:{ all -> 0x004a }
        L_0x0032:
            return
        L_0x0033:
            a.a.a.e.b r1 = r0.p()     // Catch:{ all -> 0x004a }
            if (r1 != r6) goto L_0x004d
            r1 = 1
        L_0x003a:
            java.lang.String r2 = "Connection not obtained from this manager"
            a.a.a.n.b.a(r1, r2)     // Catch:{ all -> 0x004a }
            monitor-enter(r6)     // Catch:{ all -> 0x004a }
            boolean r1 = r6.g     // Catch:{ all -> 0x00dd }
            if (r1 == 0) goto L_0x004f
            r6.a(r0)     // Catch:{ all -> 0x00dd }
            monitor-exit(r6)     // Catch:{ all -> 0x00dd }
            monitor-exit(r0)     // Catch:{ all -> 0x004a }
            goto L_0x0032
        L_0x004a:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x004a }
            throw r1
        L_0x004d:
            r1 = 0
            goto L_0x003a
        L_0x004f:
            boolean r1 = r0.c()     // Catch:{ all -> 0x00ca }
            if (r1 == 0) goto L_0x005e
            boolean r1 = r0.q()     // Catch:{ all -> 0x00ca }
            if (r1 != 0) goto L_0x005e
            r6.a(r0)     // Catch:{ all -> 0x00ca }
        L_0x005e:
            boolean r1 = r0.q()     // Catch:{ all -> 0x00ca }
            if (r1 == 0) goto L_0x00af
            a.a.a.h.c.i r2 = r6.e     // Catch:{ all -> 0x00ca }
            if (r10 == 0) goto L_0x00c4
            r1 = r10
        L_0x0069:
            r2.a(r8, r1)     // Catch:{ all -> 0x00ca }
            org.apache.commons.logging.Log r1 = r6.f117a     // Catch:{ all -> 0x00ca }
            boolean r1 = r1.isDebugEnabled()     // Catch:{ all -> 0x00ca }
            if (r1 == 0) goto L_0x00af
            r2 = 0
            int r1 = (r8 > r2 ? 1 : (r8 == r2 ? 0 : -1))
            if (r1 <= 0) goto L_0x00c7
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ca }
            r1.<init>()     // Catch:{ all -> 0x00ca }
            java.lang.String r2 = "for "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r1 = r1.append(r8)     // Catch:{ all -> 0x00ca }
            java.lang.String r2 = " "
            java.lang.StringBuilder r1 = r1.append(r2)     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r1 = r1.append(r10)     // Catch:{ all -> 0x00ca }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00ca }
        L_0x0097:
            org.apache.commons.logging.Log r2 = r6.f117a     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ca }
            r3.<init>()     // Catch:{ all -> 0x00ca }
            java.lang.String r4 = "Connection can be kept alive "
            java.lang.StringBuilder r3 = r3.append(r4)     // Catch:{ all -> 0x00ca }
            java.lang.StringBuilder r1 = r3.append(r1)     // Catch:{ all -> 0x00ca }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00ca }
            r2.debug(r1)     // Catch:{ all -> 0x00ca }
        L_0x00af:
            r0.o()     // Catch:{ all -> 0x00dd }
            r1 = 0
            r6.f = r1     // Catch:{ all -> 0x00dd }
            a.a.a.h.c.i r1 = r6.e     // Catch:{ all -> 0x00dd }
            boolean r1 = r1.d()     // Catch:{ all -> 0x00dd }
            if (r1 == 0) goto L_0x00c0
            r1 = 0
            r6.e = r1     // Catch:{ all -> 0x00dd }
        L_0x00c0:
            monitor-exit(r6)     // Catch:{ all -> 0x00dd }
            monitor-exit(r0)     // Catch:{ all -> 0x004a }
            goto L_0x0032
        L_0x00c4:
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ all -> 0x00ca }
            goto L_0x0069
        L_0x00c7:
            java.lang.String r1 = "indefinitely"
            goto L_0x0097
        L_0x00ca:
            r1 = move-exception
            r0.o()     // Catch:{ all -> 0x00dd }
            r2 = 0
            r6.f = r2     // Catch:{ all -> 0x00dd }
            a.a.a.h.c.i r2 = r6.e     // Catch:{ all -> 0x00dd }
            boolean r2 = r2.d()     // Catch:{ all -> 0x00dd }
            if (r2 == 0) goto L_0x00dc
            r2 = 0
            r6.e = r2     // Catch:{ all -> 0x00dd }
        L_0x00dc:
            throw r1     // Catch:{ all -> 0x00dd }
        L_0x00dd:
            r1 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x00dd }
            throw r1     // Catch:{ all -> 0x004a }
        */
        throw new UnsupportedOperationException("Method not decompiled: a.a.a.h.c.a.a(a.a.a.e.o, long, java.util.concurrent.TimeUnit):void");
    }

    /* access modifiers changed from: package-private */
    public o b(a.a.a.e.b.b bVar, Object obj) {
        l lVar;
        a.a.a.n.a.a(bVar, "Route");
        synchronized (this) {
            c();
            if (this.f117a.isDebugEnabled()) {
                this.f117a.debug("Get connection for route " + bVar);
            }
            a.a.a.n.b.a(this.f == null, "Invalid use of BasicClientConnManager: connection still allocated.\nMake sure to release the connection before allocating another one.");
            if (this.e != null && !this.e.b().equals(bVar)) {
                this.e.e();
                this.e = null;
            }
            if (this.e == null) {
                this.e = new i(this.f117a, Long.toString(b.getAndIncrement()), bVar, this.d.a(), 0, TimeUnit.MILLISECONDS);
            }
            if (this.e.a(System.currentTimeMillis())) {
                this.e.e();
                this.e.a().h();
            }
            this.f = new l(this, this.d, this.e);
            lVar = this.f;
        }
        return lVar;
    }

    public void b() {
        synchronized (this) {
            this.g = true;
            try {
                if (this.e != null) {
                    this.e.e();
                }
                this.e = null;
                this.f = null;
            } catch (Throwable th) {
                this.e = null;
                this.f = null;
                throw th;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        try {
            b();
        } finally {
            super.finalize();
        }
    }
}
