package a.a.a.h.b;

import a.a.a.a.c;
import a.a.a.a.i;
import a.a.a.a.n;
import a.a.a.b.e.a;
import a.a.a.b.q;
import a.a.a.j;
import a.a.a.m.e;
import java.security.Principal;
import javax.net.ssl.SSLSession;

public class p implements q {

    /* renamed from: a  reason: collision with root package name */
    public static final p f109a = new p();

    private static Principal a(i iVar) {
        n d;
        c c = iVar.c();
        if (c == null || !c.d() || !c.c() || (d = iVar.d()) == null) {
            return null;
        }
        return d.a();
    }

    public Object a(e eVar) {
        SSLSession m;
        a a2 = a.a(eVar);
        Principal principal = null;
        i i = a2.i();
        if (i != null && (principal = a(i)) == null) {
            principal = a(a2.j());
        }
        if (principal == null) {
            j l = a2.l();
            if (l.c() && (l instanceof a.a.a.e.p) && (m = ((a.a.a.e.p) l).m()) != null) {
                return m.getLocalPrincipal();
            }
        }
        return principal;
    }
}
