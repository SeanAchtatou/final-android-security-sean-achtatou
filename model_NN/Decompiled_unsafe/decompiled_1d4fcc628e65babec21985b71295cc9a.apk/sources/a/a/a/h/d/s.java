package a.a.a.h.d;

import a.a.a.e;
import a.a.a.f.b;
import a.a.a.f.c;
import a.a.a.f.f;
import a.a.a.f.i;
import a.a.a.f.n;
import a.a.a.f.p;
import a.a.a.j.u;
import a.a.a.n.a;
import a.a.a.n.d;
import java.util.Iterator;
import java.util.List;

public class s implements i {

    /* renamed from: a  reason: collision with root package name */
    private final ah f141a;
    private final aa b;
    private final x c;

    public s(String[] strArr, boolean z) {
        this.f141a = new ah(z, new aj(), new i(), new af(), new ag(), new h(), new j(), new e(), new ad(), new ae());
        this.b = new aa(z, new ac(), new i(), new z(), new h(), new j(), new e());
        b[] bVarArr = new b[5];
        bVarArr[0] = new f();
        bVarArr[1] = new i();
        bVarArr[2] = new j();
        bVarArr[3] = new e();
        bVarArr[4] = new g(strArr != null ? (String[]) strArr.clone() : new String[]{"EEE, dd-MMM-yy HH:mm:ss z"});
        this.c = new x(bVarArr);
    }

    public int a() {
        return this.f141a.a();
    }

    public List a(e eVar, f fVar) {
        d dVar;
        u uVar;
        a.a(eVar, "Header");
        a.a(fVar, "Cookie origin");
        a.a.a.f[] e = eVar.e();
        boolean z = false;
        boolean z2 = false;
        for (a.a.a.f fVar2 : e) {
            if (fVar2.a("version") != null) {
                z2 = true;
            }
            if (fVar2.a("expires") != null) {
                z = true;
            }
        }
        if (!z && z2) {
            return "Set-Cookie2".equals(eVar.c()) ? this.f141a.a(e, fVar) : this.b.a(e, fVar);
        }
        w wVar = w.f142a;
        if (eVar instanceof a.a.a.d) {
            dVar = ((a.a.a.d) eVar).a();
            uVar = new u(((a.a.a.d) eVar).b(), dVar.length());
        } else {
            String d = eVar.d();
            if (d == null) {
                throw new n("Header value is null");
            }
            dVar = new d(d.length());
            dVar.a(d);
            uVar = new u(0, dVar.length());
        }
        return this.c.a(new a.a.a.f[]{wVar.a(dVar, uVar)}, fVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.util.List, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public List a(List list) {
        a.a((Object) list, "List of cookies");
        Iterator it = list.iterator();
        int i = Integer.MAX_VALUE;
        boolean z = true;
        while (it.hasNext()) {
            c cVar = (c) it.next();
            if (!(cVar instanceof p)) {
                z = false;
            }
            i = cVar.h() < i ? cVar.h() : i;
        }
        return i > 0 ? z ? this.f141a.a(list) : this.b.a(list) : this.c.a(list);
    }

    public void a(c cVar, f fVar) {
        a.a(cVar, "Cookie");
        a.a(fVar, "Cookie origin");
        if (cVar.h() <= 0) {
            this.c.a(cVar, fVar);
        } else if (cVar instanceof p) {
            this.f141a.a(cVar, fVar);
        } else {
            this.b.a(cVar, fVar);
        }
    }

    public e b() {
        return null;
    }

    public boolean b(c cVar, f fVar) {
        a.a(cVar, "Cookie");
        a.a(fVar, "Cookie origin");
        return cVar.h() > 0 ? cVar instanceof p ? this.f141a.b(cVar, fVar) : this.b.b(cVar, fVar) : this.c.b(cVar, fVar);
    }

    public String toString() {
        return "default";
    }
}
