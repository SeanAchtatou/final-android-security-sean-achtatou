package a.a.a.h.b;

import a.a.a.b.c.e;
import a.a.a.b.c.l;
import a.a.a.b.f.f;
import a.a.a.b.j;
import a.a.a.n;
import a.a.a.n.a;
import a.a.a.q;
import java.io.Closeable;
import java.net.URI;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public abstract class h implements j, Closeable {

    /* renamed from: a  reason: collision with root package name */
    private final Log f102a = LogFactory.getLog(getClass());

    private static n c(l lVar) {
        n nVar = null;
        URI i = lVar.i();
        if (!i.isAbsolute() || (nVar = f.b(i)) != null) {
            return nVar;
        }
        throw new a.a.a.b.f("URI does not specify a valid host name: " + i);
    }

    public e a(l lVar, a.a.a.m.e eVar) {
        a.a(lVar, "HTTP request");
        return a(c(lVar), lVar, eVar);
    }

    /* access modifiers changed from: protected */
    public abstract e a(n nVar, q qVar, a.a.a.m.e eVar);

    /* renamed from: b */
    public e a(l lVar) {
        return a(lVar, null);
    }
}
