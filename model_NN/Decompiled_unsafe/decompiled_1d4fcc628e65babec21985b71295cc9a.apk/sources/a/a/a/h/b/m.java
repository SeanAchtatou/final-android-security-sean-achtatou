package a.a.a.h.b;

import a.a.a.ab;
import a.a.a.b.c.h;
import a.a.a.b.c.l;
import a.a.a.b.f.e;
import a.a.a.b.f.f;
import a.a.a.b.o;
import a.a.a.n;
import a.a.a.n.a;
import a.a.a.n.b;
import a.a.a.n.i;
import a.a.a.q;
import a.a.a.s;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class m implements o {

    /* renamed from: a  reason: collision with root package name */
    public static final m f106a = new m();
    private static final String[] c = {"GET", "HEAD"};
    private final Log b = LogFactory.getLog(getClass());

    /* access modifiers changed from: protected */
    public URI a(String str) {
        try {
            e eVar = new e(new URI(str).normalize());
            String d = eVar.d();
            if (d != null) {
                eVar.c(d.toLowerCase(Locale.ROOT));
            }
            if (i.a(eVar.e())) {
                eVar.d("/");
            }
            return eVar.a();
        } catch (URISyntaxException e) {
            throw new ab("Invalid redirect URI: " + str, e);
        }
    }

    public boolean a(q qVar, s sVar, a.a.a.m.e eVar) {
        a.a(qVar, "HTTP request");
        a.a(sVar, "HTTP response");
        int b2 = sVar.a().b();
        String a2 = qVar.g().a();
        a.a.a.e c2 = sVar.c("location");
        switch (b2) {
            case 301:
            case 307:
                return b(a2);
            case 302:
                return b(a2) && c2 != null;
            case 303:
                return true;
            case 304:
            case 305:
            case 306:
            default:
                return false;
        }
    }

    public l b(q qVar, s sVar, a.a.a.m.e eVar) {
        URI c2 = c(qVar, sVar, eVar);
        String a2 = qVar.g().a();
        return a2.equalsIgnoreCase("HEAD") ? new a.a.a.b.c.i(c2) : a2.equalsIgnoreCase("GET") ? new h(c2) : sVar.a().b() == 307 ? a.a.a.b.c.m.a(qVar).a(c2).a() : new h(c2);
    }

    /* access modifiers changed from: protected */
    public boolean b(String str) {
        for (String equalsIgnoreCase : c) {
            if (equalsIgnoreCase.equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }

    public URI c(q qVar, s sVar, a.a.a.m.e eVar) {
        URI uri;
        a.a(qVar, "HTTP request");
        a.a(sVar, "HTTP response");
        a.a(eVar, "HTTP context");
        a.a.a.b.e.a a2 = a.a.a.b.e.a.a(eVar);
        a.a.a.e c2 = sVar.c("location");
        if (c2 == null) {
            throw new ab("Received redirect response " + sVar.a() + " but no location header");
        }
        String d = c2.d();
        if (this.b.isDebugEnabled()) {
            this.b.debug("Redirect requested to location '" + d + "'");
        }
        a.a.a.b.a.a k = a2.k();
        URI a3 = a(d);
        try {
            if (a3.isAbsolute()) {
                uri = a3;
            } else if (!k.b()) {
                throw new ab("Relative redirect location '" + a3 + "' not allowed");
            } else {
                n o = a2.o();
                b.a(o, "Target host");
                uri = f.a(f.a(new URI(qVar.g().c()), o, false), a3);
            }
            u uVar = (u) a2.a("http.protocol.redirect-locations");
            if (uVar == null) {
                uVar = new u();
                eVar.a("http.protocol.redirect-locations", uVar);
            }
            if (k.c() || !uVar.a(uri)) {
                uVar.b(uri);
                return uri;
            }
            throw new a.a.a.b.e("Circular redirect to '" + uri + "'");
        } catch (URISyntaxException e) {
            throw new ab(e.getMessage(), e);
        }
    }
}
