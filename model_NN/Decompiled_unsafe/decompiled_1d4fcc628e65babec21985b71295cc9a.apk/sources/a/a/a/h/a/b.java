package a.a.a.h.a;

import a.a.a.a.n;
import a.a.a.c;
import a.a.a.e;
import a.a.a.j.p;
import a.a.a.m.a;
import a.a.a.n.d;
import a.a.a.n.f;
import a.a.a.q;
import java.nio.charset.Charset;
import org.apache.commons.codec.binary.Base64;

public class b extends aa {

    /* renamed from: a  reason: collision with root package name */
    private boolean f73a;

    public b() {
        this(c.b);
    }

    public b(Charset charset) {
        super(charset);
        this.f73a = false;
    }

    @Deprecated
    public e a(n nVar, q qVar) {
        return a(nVar, qVar, new a());
    }

    public e a(n nVar, q qVar, a.a.a.m.e eVar) {
        a.a.a.n.a.a(nVar, "Credentials");
        a.a.a.n.a.a(qVar, "HTTP request");
        StringBuilder sb = new StringBuilder();
        sb.append(nVar.a().getName());
        sb.append(":");
        sb.append(nVar.b() == null ? "null" : nVar.b());
        byte[] encode = new Base64(0).encode(f.a(sb.toString(), a(qVar)));
        d dVar = new d(32);
        if (e()) {
            dVar.a("Proxy-Authorization");
        } else {
            dVar.a("Authorization");
        }
        dVar.a(": Basic ");
        dVar.a(encode, 0, encode.length);
        return new p(dVar);
    }

    public String a() {
        return "basic";
    }

    public void a(e eVar) {
        super.a(eVar);
        this.f73a = true;
    }

    public boolean c() {
        return false;
    }

    public boolean d() {
        return this.f73a;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("BASIC [complete=").append(this.f73a).append("]");
        return sb.toString();
    }
}
