package a.a.a.h.f;

import a.a.a.aa;
import a.a.a.ab;
import a.a.a.d.b;
import a.a.a.i.c;
import a.a.a.i.f;
import a.a.a.j.j;
import a.a.a.j.t;
import a.a.a.k.d;
import a.a.a.k.e;
import a.a.a.p;
import a.a.a.x;
import java.util.ArrayList;
import java.util.List;

public abstract class a implements c {

    /* renamed from: a  reason: collision with root package name */
    protected final t f150a;
    private final f b;
    private final b c;
    private final List d;
    private int e;
    private p f;

    @Deprecated
    public a(f fVar, t tVar, e eVar) {
        a.a.a.n.a.a(fVar, "Session input buffer");
        a.a.a.n.a.a(eVar, "HTTP parameters");
        this.b = fVar;
        this.c = d.a(eVar);
        this.f150a = tVar == null ? j.b : tVar;
        this.d = new ArrayList();
        this.e = 0;
    }

    public static a.a.a.e[] a(f fVar, int i, int i2, t tVar) {
        ArrayList arrayList = new ArrayList();
        if (tVar == null) {
            tVar = j.b;
        }
        return a(fVar, i, i2, tVar, arrayList);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.util.List, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public static a.a.a.e[] a(f fVar, int i, int i2, t tVar, List list) {
        a.a.a.n.d dVar;
        int i3 = 0;
        a.a.a.n.a.a(fVar, "Session input buffer");
        a.a.a.n.a.a(tVar, "Line parser");
        a.a.a.n.a.a((Object) list, "Header line list");
        a.a.a.n.d dVar2 = null;
        a.a.a.n.d dVar3 = null;
        while (true) {
            if (dVar3 == null) {
                dVar3 = new a.a.a.n.d(64);
            } else {
                dVar3.a();
            }
            if (fVar.a(dVar3) == -1 || dVar3.length() < 1) {
                a.a.a.e[] eVarArr = new a.a.a.e[list.size()];
            } else {
                if ((dVar3.charAt(0) == ' ' || dVar3.charAt(0) == 9) && dVar2 != null) {
                    int i4 = 0;
                    while (i4 < dVar3.length() && ((r5 = dVar3.charAt(i4)) == ' ' || r5 == 9)) {
                        i4++;
                    }
                    if (i2 <= 0 || ((dVar2.length() + 1) + dVar3.length()) - i4 <= i2) {
                        dVar2.a(' ');
                        dVar2.a(dVar3, i4, dVar3.length() - i4);
                        dVar = dVar3;
                        dVar3 = dVar2;
                    } else {
                        throw new x("Maximum line length limit exceeded");
                    }
                } else {
                    list.add(dVar3);
                    dVar = null;
                }
                if (i <= 0 || list.size() < i) {
                    dVar2 = dVar3;
                    dVar3 = dVar;
                } else {
                    throw new x("Maximum header count exceeded");
                }
            }
        }
        a.a.a.e[] eVarArr2 = new a.a.a.e[list.size()];
        while (i3 < list.size()) {
            try {
                eVarArr2[i3] = tVar.a((a.a.a.n.d) list.get(i3));
                i3++;
            } catch (aa e2) {
                throw new ab(e2.getMessage());
            }
        }
        return eVarArr2;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public p a() {
        switch (this.e) {
            case 0:
                try {
                    this.f = b(this.b);
                    this.e = 1;
                    break;
                } catch (aa e2) {
                    throw new ab(e2.getMessage(), e2);
                }
            case 1:
                break;
            default:
                throw new IllegalStateException("Inconsistent parser state");
        }
        this.f.a(a(this.b, this.c.b(), this.c.a(), this.f150a, this.d));
        p pVar = this.f;
        this.f = null;
        this.d.clear();
        this.e = 0;
        return pVar;
    }

    /* access modifiers changed from: protected */
    public abstract p b(f fVar);
}
