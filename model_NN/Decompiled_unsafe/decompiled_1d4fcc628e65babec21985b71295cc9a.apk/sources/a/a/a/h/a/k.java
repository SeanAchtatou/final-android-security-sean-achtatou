package a.a.a.h.a;

import java.io.IOException;
import java.io.OutputStream;
import java.security.MessageDigest;

class k extends OutputStream {

    /* renamed from: a  reason: collision with root package name */
    private final MessageDigest f82a;
    private boolean b;
    private byte[] c;

    k(MessageDigest messageDigest) {
        this.f82a = messageDigest;
        this.f82a.reset();
    }

    public byte[] a() {
        return this.c;
    }

    public void close() {
        if (!this.b) {
            this.b = true;
            this.c = this.f82a.digest();
            super.close();
        }
    }

    public void write(int i) {
        if (this.b) {
            throw new IOException("Stream has been already closed");
        }
        this.f82a.update((byte) i);
    }

    public void write(byte[] bArr, int i, int i2) {
        if (this.b) {
            throw new IOException("Stream has been already closed");
        }
        this.f82a.update(bArr, i, i2);
    }
}
