package a.a.a.h.f;

import java.io.InputStream;

public final class j extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    public static final j f158a = new j();

    private j() {
    }

    public int available() {
        return 0;
    }

    public void close() {
    }

    public void mark(int i) {
    }

    public boolean markSupported() {
        return true;
    }

    public int read() {
        return -1;
    }

    public int read(byte[] bArr) {
        return -1;
    }

    public int read(byte[] bArr, int i, int i2) {
        return -1;
    }

    public void reset() {
    }

    public long skip(long j) {
        return 0;
    }
}
