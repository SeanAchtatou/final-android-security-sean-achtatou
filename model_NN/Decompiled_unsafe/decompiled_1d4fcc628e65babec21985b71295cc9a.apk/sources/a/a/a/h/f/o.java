package a.a.a.h.f;

import a.a.a.i.b;
import a.a.a.k.e;
import a.a.a.n.a;
import java.net.Socket;

@Deprecated
public class o extends c implements b {

    /* renamed from: a  reason: collision with root package name */
    private final Socket f162a;
    private boolean b = false;

    public o(Socket socket, int i, e eVar) {
        int i2 = 1024;
        a.a(socket, "Socket");
        this.f162a = socket;
        int receiveBufferSize = i < 0 ? socket.getReceiveBufferSize() : i;
        a(socket.getInputStream(), receiveBufferSize >= 1024 ? receiveBufferSize : i2, eVar);
    }

    /* JADX INFO: finally extract failed */
    public boolean a(int i) {
        boolean g = g();
        if (!g) {
            int soTimeout = this.f162a.getSoTimeout();
            try {
                this.f162a.setSoTimeout(i);
                f();
                g = g();
                this.f162a.setSoTimeout(soTimeout);
            } catch (Throwable th) {
                this.f162a.setSoTimeout(soTimeout);
                throw th;
            }
        }
        return g;
    }

    public boolean c() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public int f() {
        int f = super.f();
        this.b = f == -1;
        return f;
    }
}
