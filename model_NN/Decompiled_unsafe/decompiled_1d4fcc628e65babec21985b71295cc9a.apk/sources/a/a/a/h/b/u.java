package a.a.a.h.b;

import java.net.URI;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class u extends AbstractList {

    /* renamed from: a  reason: collision with root package name */
    private final Set f112a = new HashSet();
    private final List b = new ArrayList();

    /* renamed from: a */
    public URI get(int i) {
        return (URI) this.b.get(i);
    }

    public boolean a(URI uri) {
        return this.f112a.contains(uri);
    }

    public void add(int i, Object obj) {
        this.b.add(i, (URI) obj);
        this.f112a.add((URI) obj);
    }

    /* renamed from: b */
    public URI remove(int i) {
        URI uri = (URI) this.b.remove(i);
        this.f112a.remove(uri);
        if (this.b.size() != this.f112a.size()) {
            this.f112a.addAll(this.b);
        }
        return uri;
    }

    public void b(URI uri) {
        this.f112a.add(uri);
        this.b.add(uri);
    }

    public boolean contains(Object obj) {
        return this.f112a.contains(obj);
    }

    public Object set(int i, Object obj) {
        URI uri = (URI) this.b.set(i, (URI) obj);
        this.f112a.remove(uri);
        this.f112a.add((URI) obj);
        if (this.b.size() != this.f112a.size()) {
            this.f112a.addAll(this.b);
        }
        return uri;
    }

    public int size() {
        return this.b.size();
    }
}
