package a.a.a.h.d;

import a.a.a.f;
import a.a.a.j.c;
import a.a.a.j.l;
import a.a.a.j.u;
import a.a.a.j.v;
import a.a.a.n.a;
import a.a.a.n.d;
import a.a.a.y;
import java.util.ArrayList;
import java.util.BitSet;

public class w {

    /* renamed from: a  reason: collision with root package name */
    public static final w f142a = new w();
    private static final BitSet b = v.a(61, 59);
    private static final BitSet c = v.a(59);
    private final v d = v.f179a;

    private y b(d dVar, u uVar) {
        String a2 = this.d.a(dVar, uVar, b);
        if (uVar.c()) {
            return new l(a2, null);
        }
        char charAt = dVar.charAt(uVar.b());
        uVar.a(uVar.b() + 1);
        if (charAt != '=') {
            return new l(a2, null);
        }
        String a3 = this.d.a(dVar, uVar, c);
        if (!uVar.c()) {
            uVar.a(uVar.b() + 1);
        }
        return new l(a2, a3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [a.a.a.n.d, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public f a(d dVar, u uVar) {
        a.a((Object) dVar, "Char array buffer");
        a.a(uVar, "Parser cursor");
        y b2 = b(dVar, uVar);
        ArrayList arrayList = new ArrayList();
        while (!uVar.c()) {
            arrayList.add(b(dVar, uVar));
        }
        return new c(b2.a(), b2.b(), (y[]) arrayList.toArray(new y[arrayList.size()]));
    }
}
