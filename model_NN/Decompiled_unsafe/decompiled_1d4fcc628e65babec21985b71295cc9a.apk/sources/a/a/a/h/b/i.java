package a.a.a.h.b;

import a.a.a.b.c.e;
import a.a.a.n.g;
import a.a.a.s;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

@Deprecated
class i implements InvocationHandler {

    /* renamed from: a  reason: collision with root package name */
    private static final Constructor f103a;
    private final s b;

    static {
        try {
            f103a = Proxy.getProxyClass(i.class.getClassLoader(), e.class).getConstructor(InvocationHandler.class);
        } catch (NoSuchMethodException e) {
            throw new IllegalStateException(e);
        }
    }

    i(s sVar) {
        this.b = sVar;
    }

    public static e a(s sVar) {
        try {
            return (e) f103a.newInstance(new i(sVar));
        } catch (InstantiationException e) {
            throw new IllegalStateException(e);
        } catch (InvocationTargetException e2) {
            throw new IllegalStateException(e2);
        } catch (IllegalAccessException e3) {
            throw new IllegalStateException(e3);
        }
    }

    public void a() {
        g.a(this.b.b());
    }

    public Object invoke(Object obj, Method method, Object[] objArr) {
        if (method.getName().equals("close")) {
            a();
            return null;
        }
        try {
            return method.invoke(this.b, objArr);
        } catch (InvocationTargetException e) {
            Throwable cause = e.getCause();
            if (cause != null) {
                throw cause;
            }
            throw e;
        }
    }
}
