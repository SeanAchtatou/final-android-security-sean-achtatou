package a.a.a.h.e;

import a.a.a.ab;
import a.a.a.e;
import a.a.a.n.a;
import a.a.a.p;
import a.a.a.v;

public class d implements a.a.a.g.d {

    /* renamed from: a  reason: collision with root package name */
    public static final d f148a = new d();
    private final int b;

    public d() {
        this(-1);
    }

    public d(int i) {
        this.b = i;
    }

    public long a(p pVar) {
        a.a(pVar, "HTTP message");
        e c = pVar.c("Transfer-Encoding");
        if (c != null) {
            String d = c.d();
            if ("chunked".equalsIgnoreCase(d)) {
                if (!pVar.c().c(v.b)) {
                    return -2;
                }
                throw new ab("Chunked transfer encoding not allowed for " + pVar.c());
            } else if ("identity".equalsIgnoreCase(d)) {
                return -1;
            } else {
                throw new ab("Unsupported transfer encoding: " + d);
            }
        } else {
            e c2 = pVar.c("Content-Length");
            if (c2 == null) {
                return (long) this.b;
            }
            String d2 = c2.d();
            try {
                long parseLong = Long.parseLong(d2);
                if (parseLong >= 0) {
                    return parseLong;
                }
                throw new ab("Negative content length: " + d2);
            } catch (NumberFormatException e) {
                throw new ab("Invalid content length: " + d2);
            }
        }
    }
}
