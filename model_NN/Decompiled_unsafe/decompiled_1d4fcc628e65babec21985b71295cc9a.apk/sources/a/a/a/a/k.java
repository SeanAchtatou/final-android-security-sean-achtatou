package a.a.a.a;

import a.a.a.n.a;
import a.a.a.n.h;
import java.io.Serializable;
import java.security.Principal;

public final class k implements Serializable, Principal {

    /* renamed from: a  reason: collision with root package name */
    private final String f6a;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public k(String str) {
        a.a((Object) str, "User name");
        this.f6a = str;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof k) && h.a(this.f6a, ((k) obj).f6a);
    }

    public String getName() {
        return this.f6a;
    }

    public int hashCode() {
        return h.a(17, this.f6a);
    }

    public String toString() {
        return "[principal: " + this.f6a + "]";
    }
}
