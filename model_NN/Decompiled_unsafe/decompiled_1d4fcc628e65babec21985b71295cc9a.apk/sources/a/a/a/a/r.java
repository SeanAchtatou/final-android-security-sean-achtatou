package a.a.a.a;

import a.a.a.n.h;
import java.io.Serializable;
import java.security.Principal;

public class r implements n, Serializable {

    /* renamed from: a  reason: collision with root package name */
    private final s f9a;
    private final String b;
    private final String c;

    public Principal a() {
        return this.f9a;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.f9a.b();
    }

    public String d() {
        return this.f9a.a();
    }

    public String e() {
        return this.c;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof r) {
            r rVar = (r) obj;
            return h.a(this.f9a, rVar.f9a) && h.a(this.c, rVar.c);
        }
    }

    public int hashCode() {
        return h.a(h.a(17, this.f9a), this.c);
    }

    public String toString() {
        return "[principal: " + this.f9a + "][workstation: " + this.c + "]";
    }
}
