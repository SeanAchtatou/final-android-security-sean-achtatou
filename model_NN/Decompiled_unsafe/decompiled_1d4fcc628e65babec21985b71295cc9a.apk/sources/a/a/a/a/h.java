package a.a.a.a;

import a.a.a.n;
import a.a.a.n.a;
import java.util.Locale;

public class h {

    /* renamed from: a  reason: collision with root package name */
    public static final String f4a = null;
    public static final String b = null;
    public static final String c = null;
    public static final h d = new h(f4a, -1, b, c);
    private final String e;
    private final String f;
    private final String g;
    private final int h;
    private final n i;

    public h(n nVar, String str, String str2) {
        a.a(nVar, "Host");
        this.g = nVar.a().toLowerCase(Locale.ROOT);
        this.h = nVar.b() < 0 ? -1 : nVar.b();
        this.f = str == null ? b : str;
        this.e = str2 == null ? c : str2.toUpperCase(Locale.ROOT);
        this.i = nVar;
    }

    public h(String str, int i2, String str2, String str3) {
        this.g = str == null ? f4a : str.toLowerCase(Locale.ROOT);
        this.h = i2 < 0 ? -1 : i2;
        this.f = str2 == null ? b : str2;
        this.e = str3 == null ? c : str3.toUpperCase(Locale.ROOT);
        this.i = null;
    }

    public int a(h hVar) {
        int i2 = 0;
        if (a.a.a.n.h.a(this.e, hVar.e)) {
            i2 = 1;
        } else if (!(this.e == c || hVar.e == c)) {
            return -1;
        }
        if (a.a.a.n.h.a(this.f, hVar.f)) {
            i2 += 2;
        } else if (!(this.f == b || hVar.f == b)) {
            return -1;
        }
        if (this.h == hVar.h) {
            i2 += 4;
        } else if (!(this.h == -1 || hVar.h == -1)) {
            return -1;
        }
        if (a.a.a.n.h.a(this.g, hVar.g)) {
            return i2 + 8;
        }
        if (this.g == f4a || hVar.g == f4a) {
            return i2;
        }
        return -1;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof h)) {
            return super.equals(obj);
        }
        h hVar = (h) obj;
        return a.a.a.n.h.a(this.g, hVar.g) && this.h == hVar.h && a.a.a.n.h.a(this.f, hVar.f) && a.a.a.n.h.a(this.e, hVar.e);
    }

    public int hashCode() {
        return a.a.a.n.h.a(a.a.a.n.h.a(a.a.a.n.h.a(a.a.a.n.h.a(17, this.g), this.h), this.f), this.e);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (this.e != null) {
            sb.append(this.e.toUpperCase(Locale.ROOT));
            sb.append(' ');
        }
        if (this.f != null) {
            sb.append('\'');
            sb.append(this.f);
            sb.append('\'');
        } else {
            sb.append("<any realm>");
        }
        if (this.g != null) {
            sb.append('@');
            sb.append(this.g);
            if (this.h >= 0) {
                sb.append(':');
                sb.append(this.h);
            }
        }
        return sb.toString();
    }
}
