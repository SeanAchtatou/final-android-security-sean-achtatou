package a.a.a.d;

public class b implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    public static final b f29a = new c().a();
    private final int b;
    private final int c;

    b(int i, int i2) {
        this.b = i;
        this.c = i2;
    }

    public static c d() {
        return new c();
    }

    public int a() {
        return this.b;
    }

    public int b() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public b clone() {
        return (b) super.clone();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[maxLineLength=").append(this.b).append(", maxHeaderCount=").append(this.c).append("]");
        return sb.toString();
    }
}
