package a.a.a.e;

import a.a.a.n.a;
import java.io.IOException;
import java.io.InputStream;

public class k extends InputStream implements i {

    /* renamed from: a  reason: collision with root package name */
    protected InputStream f47a;
    private boolean b = false;
    private final l c;

    public k(InputStream inputStream, l lVar) {
        a.a(inputStream, "Wrapped stream");
        this.f47a = inputStream;
        this.c = lVar;
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        if (this.f47a != null && i < 0) {
            boolean z = true;
            try {
                if (this.c != null) {
                    z = this.c.a(this.f47a);
                }
                if (z) {
                    this.f47a.close();
                }
            } finally {
                this.f47a = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean a() {
        if (!this.b) {
            return this.f47a != null;
        }
        throw new IOException("Attempted read on closed stream.");
    }

    public int available() {
        if (!a()) {
            return 0;
        }
        try {
            return this.f47a.available();
        } catch (IOException e) {
            c();
            throw e;
        }
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (this.f47a != null) {
            boolean z = true;
            try {
                if (this.c != null) {
                    z = this.c.b(this.f47a);
                }
                if (z) {
                    this.f47a.close();
                }
            } finally {
                this.f47a = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void c() {
        if (this.f47a != null) {
            boolean z = true;
            try {
                if (this.c != null) {
                    z = this.c.c(this.f47a);
                }
                if (z) {
                    this.f47a.close();
                }
            } finally {
                this.f47a = null;
            }
        }
    }

    public void close() {
        this.b = true;
        b();
    }

    public void h() {
        close();
    }

    public void i() {
        this.b = true;
        c();
    }

    public int read() {
        if (!a()) {
            return -1;
        }
        try {
            int read = this.f47a.read();
            a(read);
            return read;
        } catch (IOException e) {
            c();
            throw e;
        }
    }

    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    public int read(byte[] bArr, int i, int i2) {
        if (!a()) {
            return -1;
        }
        try {
            int read = this.f47a.read(bArr, i, i2);
            a(read);
            return read;
        } catch (IOException e) {
            c();
            throw e;
        }
    }
}
