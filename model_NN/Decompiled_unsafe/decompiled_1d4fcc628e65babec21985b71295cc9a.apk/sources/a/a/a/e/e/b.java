package a.a.a.e.e;

import java.net.IDN;
import java.util.Locale;
import java.util.Map;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private final Map f45a;
    private final Map b;

    public String a(String str) {
        if (str == null) {
            return null;
        }
        if (str.startsWith(".")) {
            return null;
        }
        String lowerCase = str.toLowerCase(Locale.ROOT);
        String str2 = null;
        while (lowerCase != null) {
            if (this.b == null || !this.b.containsKey(IDN.toUnicode(lowerCase))) {
                if (this.f45a.containsKey(IDN.toUnicode(lowerCase))) {
                    break;
                }
                int indexOf = lowerCase.indexOf(46);
                String substring = indexOf != -1 ? lowerCase.substring(indexOf + 1) : null;
                if (substring != null && this.f45a.containsKey("*." + IDN.toUnicode(substring))) {
                    break;
                }
                if (indexOf == -1) {
                    lowerCase = str2;
                }
                str2 = lowerCase;
                lowerCase = substring;
            } else {
                return lowerCase;
            }
        }
        return str2;
    }
}
