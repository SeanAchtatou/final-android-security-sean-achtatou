package a.a.a.e.c;

import a.a.a.k.e;
import java.net.InetSocketAddress;
import java.net.Socket;

@Deprecated
class f implements e {

    /* renamed from: a  reason: collision with root package name */
    private final b f39a;

    f(b bVar) {
        this.f39a = bVar;
    }

    public Socket a(e eVar) {
        return this.f39a.a(eVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.e.c.b.a(java.net.Socket, java.lang.String, int, boolean):java.net.Socket
     arg types: [java.net.Socket, java.lang.String, int, int]
     candidates:
      a.a.a.e.c.h.a(java.net.Socket, java.net.InetSocketAddress, java.net.InetSocketAddress, a.a.a.k.e):java.net.Socket
      a.a.a.e.c.b.a(java.net.Socket, java.lang.String, int, boolean):java.net.Socket */
    public Socket a(Socket socket, String str, int i, e eVar) {
        return this.f39a.a(socket, str, i, true);
    }

    public Socket a(Socket socket, InetSocketAddress inetSocketAddress, InetSocketAddress inetSocketAddress2, e eVar) {
        return this.f39a.a(socket, inetSocketAddress, inetSocketAddress2, eVar);
    }

    public boolean a(Socket socket) {
        return this.f39a.a(socket);
    }
}
