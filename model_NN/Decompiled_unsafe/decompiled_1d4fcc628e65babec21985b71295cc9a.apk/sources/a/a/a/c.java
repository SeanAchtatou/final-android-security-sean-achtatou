package a.a.a;

import java.nio.charset.Charset;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    public static final Charset f28a = Charset.forName("UTF-8");
    public static final Charset b = Charset.forName("US-ASCII");
    public static final Charset c = Charset.forName("ISO-8859-1");
}
