package a.a.a.j;

import a.a.a.f;
import a.a.a.n.a;
import a.a.a.n.d;
import a.a.a.y;

public class e {
    @Deprecated

    /* renamed from: a  reason: collision with root package name */
    public static final e f167a = new e();
    public static final e b = new e();

    /* access modifiers changed from: protected */
    public int a(f fVar) {
        if (fVar == null) {
            return 0;
        }
        int length = fVar.a().length();
        String b2 = fVar.b();
        if (b2 != null) {
            length += b2.length() + 3;
        }
        int d = fVar.d();
        if (d <= 0) {
            return length;
        }
        for (int i = 0; i < d; i++) {
            length += a(fVar.a(i)) + 2;
        }
        return length;
    }

    /* access modifiers changed from: protected */
    public int a(y yVar) {
        if (yVar == null) {
            return 0;
        }
        int length = yVar.a().length();
        String b2 = yVar.b();
        return b2 != null ? length + b2.length() + 3 : length;
    }

    /* access modifiers changed from: protected */
    public int a(y[] yVarArr) {
        int i = 0;
        if (yVarArr != null && yVarArr.length >= 1) {
            int length = yVarArr.length;
            i = (yVarArr.length - 1) * 2;
            int i2 = 0;
            while (i2 < length) {
                int a2 = a(yVarArr[i2]) + i;
                i2++;
                i = a2;
            }
        }
        return i;
    }

    public d a(d dVar, f fVar, boolean z) {
        a.a(fVar, "Header element");
        int a2 = a(fVar);
        if (dVar == null) {
            dVar = new d(a2);
        } else {
            dVar.a(a2);
        }
        dVar.a(fVar.a());
        String b2 = fVar.b();
        if (b2 != null) {
            dVar.a('=');
            a(dVar, b2, z);
        }
        int d = fVar.d();
        if (d > 0) {
            for (int i = 0; i < d; i++) {
                dVar.a("; ");
                a(dVar, fVar.a(i), z);
            }
        }
        return dVar;
    }

    public d a(d dVar, y yVar, boolean z) {
        a.a(yVar, "Name / value pair");
        int a2 = a(yVar);
        if (dVar == null) {
            dVar = new d(a2);
        } else {
            dVar.a(a2);
        }
        dVar.a(yVar.a());
        String b2 = yVar.b();
        if (b2 != null) {
            dVar.a('=');
            a(dVar, b2, z);
        }
        return dVar;
    }

    public d a(d dVar, y[] yVarArr, boolean z) {
        a.a(yVarArr, "Header parameter array");
        int a2 = a(yVarArr);
        if (dVar == null) {
            dVar = new d(a2);
        } else {
            dVar.a(a2);
        }
        for (int i = 0; i < yVarArr.length; i++) {
            if (i > 0) {
                dVar.a("; ");
            }
            a(dVar, yVarArr[i], z);
        }
        return dVar;
    }

    /* access modifiers changed from: protected */
    public void a(d dVar, String str, boolean z) {
        if (!z) {
            for (int i = 0; i < str.length() && !z; i++) {
                z = a(str.charAt(i));
            }
        }
        if (z) {
            dVar.a('\"');
        }
        for (int i2 = 0; i2 < str.length(); i2++) {
            char charAt = str.charAt(i2);
            if (b(charAt)) {
                dVar.a('\\');
            }
            dVar.a(charAt);
        }
        if (z) {
            dVar.a('\"');
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(char c) {
        return " ;,:@()<>\\\"/[]?={}\t".indexOf(c) >= 0;
    }

    /* access modifiers changed from: protected */
    public boolean b(char c) {
        return "\"\\".indexOf(c) >= 0;
    }
}
