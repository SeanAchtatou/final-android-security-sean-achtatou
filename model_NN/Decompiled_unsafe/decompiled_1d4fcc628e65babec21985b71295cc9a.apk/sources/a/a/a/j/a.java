package a.a.a.j;

import a.a.a.h;
import a.a.a.k.b;
import a.a.a.k.e;
import a.a.a.p;

public abstract class a implements p {

    /* renamed from: a  reason: collision with root package name */
    protected q f163a;
    @Deprecated
    protected e b;

    protected a() {
        this(null);
    }

    @Deprecated
    protected a(e eVar) {
        this.f163a = new q();
        this.b = eVar;
    }

    public void a(a.a.a.e eVar) {
        this.f163a.a(eVar);
    }

    @Deprecated
    public void a(e eVar) {
        this.b = (e) a.a.a.n.a.a(eVar, "HTTP parameters");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public void a(String str, String str2) {
        a.a.a.n.a.a((Object) str, "Header name");
        this.f163a.a(new b(str, str2));
    }

    public void a(a.a.a.e[] eVarArr) {
        this.f163a.a(eVarArr);
    }

    public boolean a(String str) {
        return this.f163a.c(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public void b(String str, String str2) {
        a.a.a.n.a.a((Object) str, "Header name");
        this.f163a.b(new b(str, str2));
    }

    public a.a.a.e[] b(String str) {
        return this.f163a.a(str);
    }

    public a.a.a.e c(String str) {
        return this.f163a.b(str);
    }

    public void d(String str) {
        if (str != null) {
            h c = this.f163a.c();
            while (c.hasNext()) {
                if (str.equalsIgnoreCase(c.a().c())) {
                    c.remove();
                }
            }
        }
    }

    public a.a.a.e[] d() {
        return this.f163a.b();
    }

    public h e() {
        return this.f163a.c();
    }

    public h e(String str) {
        return this.f163a.d(str);
    }

    @Deprecated
    public e f() {
        if (this.b == null) {
            this.b = new b();
        }
        return this.b;
    }
}
