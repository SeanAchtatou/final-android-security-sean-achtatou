package a.a.a.j;

import a.a.a.aa;
import a.a.a.ac;
import a.a.a.af;
import a.a.a.e;
import a.a.a.n.a;
import a.a.a.n.d;
import a.a.a.v;

public class j implements t {
    @Deprecated

    /* renamed from: a  reason: collision with root package name */
    public static final j f170a = new j();
    public static final j b = new j();
    protected final ac c;

    public j() {
        this(null);
    }

    public j(ac acVar) {
        this.c = acVar == null ? v.c : acVar;
    }

    /* access modifiers changed from: protected */
    public ac a(int i, int i2) {
        return this.c.a(i, i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [a.a.a.n.d, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public ac a(d dVar, u uVar) {
        boolean z = true;
        a.a((Object) dVar, "Char array buffer");
        a.a(uVar, "Parser cursor");
        String a2 = this.c.a();
        int length = a2.length();
        int b2 = uVar.b();
        int a3 = uVar.a();
        d(dVar, uVar);
        int b3 = uVar.b();
        if (b3 + length + 4 > a3) {
            throw new aa("Not a valid protocol version: " + dVar.a(b2, a3));
        }
        int i = 0;
        boolean z2 = true;
        while (z2 && i < length) {
            z2 = dVar.charAt(b3 + i) == a2.charAt(i);
            i++;
        }
        if (!z2) {
            z = z2;
        } else if (dVar.charAt(b3 + length) != '/') {
            z = false;
        }
        if (!z) {
            throw new aa("Not a valid protocol version: " + dVar.a(b2, a3));
        }
        int i2 = length + 1 + b3;
        int a4 = dVar.a(46, i2, a3);
        if (a4 == -1) {
            throw new aa("Invalid protocol version number: " + dVar.a(b2, a3));
        }
        try {
            int parseInt = Integer.parseInt(dVar.b(i2, a4));
            int i3 = a4 + 1;
            int a5 = dVar.a(32, i3, a3);
            if (a5 == -1) {
                a5 = a3;
            }
            try {
                int parseInt2 = Integer.parseInt(dVar.b(i3, a5));
                uVar.a(a5);
                return a(parseInt, parseInt2);
            } catch (NumberFormatException e) {
                throw new aa("Invalid protocol minor version number: " + dVar.a(b2, a3));
            }
        } catch (NumberFormatException e2) {
            throw new aa("Invalid protocol major version number: " + dVar.a(b2, a3));
        }
    }

    /* access modifiers changed from: protected */
    public af a(ac acVar, int i, String str) {
        return new n(acVar, i, str);
    }

    public e a(d dVar) {
        return new p(dVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [a.a.a.n.d, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public boolean b(d dVar, u uVar) {
        boolean z = true;
        a.a((Object) dVar, "Char array buffer");
        a.a(uVar, "Parser cursor");
        int b2 = uVar.b();
        String a2 = this.c.a();
        int length = a2.length();
        if (dVar.length() < length + 4) {
            return false;
        }
        if (b2 < 0) {
            b2 = (dVar.length() - 4) - length;
        } else if (b2 == 0) {
            while (b2 < dVar.length() && a.a.a.m.d.a(dVar.charAt(b2))) {
                b2++;
            }
        }
        if (b2 + length + 4 > dVar.length()) {
            return false;
        }
        int i = 0;
        boolean z2 = true;
        while (z2 && i < length) {
            z2 = dVar.charAt(b2 + i) == a2.charAt(i);
            i++;
        }
        if (!z2) {
            z = z2;
        } else if (dVar.charAt(b2 + length) != '/') {
            z = false;
        }
        return z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [a.a.a.n.d, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public af c(d dVar, u uVar) {
        a.a((Object) dVar, "Char array buffer");
        a.a(uVar, "Parser cursor");
        int b2 = uVar.b();
        int a2 = uVar.a();
        try {
            ac a3 = a(dVar, uVar);
            d(dVar, uVar);
            int b3 = uVar.b();
            int a4 = dVar.a(32, b3, a2);
            int i = a4 < 0 ? a2 : a4;
            String b4 = dVar.b(b3, i);
            for (int i2 = 0; i2 < b4.length(); i2++) {
                if (!Character.isDigit(b4.charAt(i2))) {
                    throw new aa("Status line contains invalid status code: " + dVar.a(b2, a2));
                }
            }
            return a(a3, Integer.parseInt(b4), i < a2 ? dVar.b(i, a2) : "");
        } catch (NumberFormatException e) {
            throw new aa("Status line contains invalid status code: " + dVar.a(b2, a2));
        } catch (IndexOutOfBoundsException e2) {
            throw new aa("Invalid status line: " + dVar.a(b2, a2));
        }
    }

    /* access modifiers changed from: protected */
    public void d(d dVar, u uVar) {
        int b2 = uVar.b();
        int a2 = uVar.a();
        while (b2 < a2 && a.a.a.m.d.a(dVar.charAt(b2))) {
            b2++;
        }
        uVar.a(b2);
    }
}
