package a.a.a.j;

import a.a.a.aa;
import a.a.a.ag;
import a.a.a.h;
import a.a.a.n.a;
import java.util.NoSuchElementException;

public class o implements ag {

    /* renamed from: a  reason: collision with root package name */
    protected final h f175a;
    protected String b;
    protected String c;
    protected int d = a(-1);

    public o(h hVar) {
        this.f175a = (h) a.a(hVar, "Header iterator");
    }

    /* access modifiers changed from: protected */
    public int a(int i) {
        int c2;
        if (i >= 0) {
            c2 = c(i);
        } else if (!this.f175a.hasNext()) {
            return -1;
        } else {
            this.b = this.f175a.a().d();
            c2 = 0;
        }
        int b2 = b(c2);
        if (b2 < 0) {
            this.c = null;
            return -1;
        }
        int d2 = d(b2);
        this.c = a(this.b, b2, d2);
        return d2;
    }

    public String a() {
        if (this.c == null) {
            throw new NoSuchElementException("Iteration already finished.");
        }
        String str = this.c;
        this.d = a(this.d);
        return str;
    }

    /* access modifiers changed from: protected */
    public String a(String str, int i, int i2) {
        return str.substring(i, i2);
    }

    /* access modifiers changed from: protected */
    public boolean a(char c2) {
        return c2 == ',';
    }

    /* access modifiers changed from: protected */
    public int b(int i) {
        int b2 = a.b(i, "Search position");
        boolean z = false;
        while (!z && this.b != null) {
            int length = this.b.length();
            boolean z2 = z;
            int i2 = b2;
            boolean z3 = z2;
            while (!z3 && i2 < length) {
                char charAt = this.b.charAt(i2);
                if (a(charAt) || b(charAt)) {
                    i2++;
                } else if (c(this.b.charAt(i2))) {
                    z3 = true;
                } else {
                    throw new aa("Invalid character before token (pos " + i2 + "): " + this.b);
                }
            }
            if (!z3) {
                if (this.f175a.hasNext()) {
                    this.b = this.f175a.a().d();
                    i2 = 0;
                } else {
                    this.b = null;
                }
            }
            boolean z4 = z3;
            b2 = i2;
            z = z4;
        }
        if (z) {
            return b2;
        }
        return -1;
    }

    /* access modifiers changed from: protected */
    public boolean b(char c2) {
        return c2 == 9 || Character.isSpaceChar(c2);
    }

    /* access modifiers changed from: protected */
    public int c(int i) {
        int b2 = a.b(i, "Search position");
        boolean z = false;
        int length = this.b.length();
        while (!z && b2 < length) {
            char charAt = this.b.charAt(b2);
            if (a(charAt)) {
                z = true;
            } else if (b(charAt)) {
                b2++;
            } else if (c(charAt)) {
                throw new aa("Tokens without separator (pos " + b2 + "): " + this.b);
            } else {
                throw new aa("Invalid character after token (pos " + b2 + "): " + this.b);
            }
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public boolean c(char c2) {
        if (Character.isLetterOrDigit(c2)) {
            return true;
        }
        if (Character.isISOControl(c2)) {
            return false;
        }
        return !d(c2);
    }

    /* access modifiers changed from: protected */
    public int d(int i) {
        a.b(i, "Search position");
        int length = this.b.length();
        int i2 = i + 1;
        while (i2 < length && c(this.b.charAt(i2))) {
            i2++;
        }
        return i2;
    }

    /* access modifiers changed from: protected */
    public boolean d(char c2) {
        return " ,;=()<>@:\\\"/[]?{}\t".indexOf(c2) >= 0;
    }

    public boolean hasNext() {
        return this.c != null;
    }

    public final Object next() {
        return a();
    }

    public final void remove() {
        throw new UnsupportedOperationException("Removing tokens is not supported.");
    }
}
