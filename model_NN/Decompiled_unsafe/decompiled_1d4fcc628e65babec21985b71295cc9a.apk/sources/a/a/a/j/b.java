package a.a.a.j;

import a.a.a.e;
import a.a.a.f;
import a.a.a.n.a;
import a.a.a.n.d;
import java.io.Serializable;

public class b implements e, Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    private final String f164a;
    private final String b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public b(String str, String str2) {
        this.f164a = (String) a.a((Object) str, "Name");
        this.b = str2;
    }

    public String c() {
        return this.f164a;
    }

    public Object clone() {
        return super.clone();
    }

    public String d() {
        return this.b;
    }

    public f[] e() {
        return this.b != null ? f.a(this.b, (r) null) : new f[0];
    }

    public String toString() {
        return i.b.a((d) null, this).toString();
    }
}
