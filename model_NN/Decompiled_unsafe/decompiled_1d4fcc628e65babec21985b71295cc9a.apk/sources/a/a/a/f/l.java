package a.a.a.f;

import a.a.a.d.a;
import a.a.a.k.e;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;

@Deprecated
public final class l implements a {

    /* renamed from: a  reason: collision with root package name */
    private final ConcurrentHashMap f51a = new ConcurrentHashMap();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public i a(String str, e eVar) {
        a.a.a.n.a.a((Object) str, "Name");
        j jVar = (j) this.f51a.get(str.toLowerCase(Locale.ENGLISH));
        if (jVar != null) {
            return jVar.a(eVar);
        }
        throw new IllegalStateException("Unsupported cookie spec: " + str);
    }

    /* renamed from: a */
    public k b(String str) {
        return new m(this, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public void a(String str, j jVar) {
        a.a.a.n.a.a((Object) str, "Name");
        a.a.a.n.a.a(jVar, "Cookie spec factory");
        this.f51a.put(str.toLowerCase(Locale.ENGLISH), jVar);
    }
}
