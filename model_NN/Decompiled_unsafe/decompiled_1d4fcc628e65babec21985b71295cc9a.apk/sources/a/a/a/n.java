package a.a.a;

import a.a.a.n.a;
import a.a.a.n.h;
import java.io.Serializable;
import java.net.InetAddress;
import java.util.Locale;

public final class n implements Serializable, Cloneable {

    /* renamed from: a  reason: collision with root package name */
    protected final String f192a;
    protected final String b;
    protected final int c;
    protected final String d;
    protected final InetAddress e;

    public n(String str, int i) {
        this(str, i, (String) null);
    }

    public n(String str, int i, String str2) {
        this.f192a = (String) a.c(str, "Host name");
        this.b = str.toLowerCase(Locale.ROOT);
        if (str2 != null) {
            this.d = str2.toLowerCase(Locale.ROOT);
        } else {
            this.d = "http";
        }
        this.c = i;
        this.e = null;
    }

    public n(InetAddress inetAddress, int i, String str) {
        this((InetAddress) a.a(inetAddress, "Inet address"), inetAddress.getHostName(), i, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object
     arg types: [java.lang.String, java.lang.String]
     candidates:
      a.a.a.n.a.a(int, java.lang.String):int
      a.a.a.n.a.a(long, java.lang.String):long
      a.a.a.n.a.a(java.lang.CharSequence, java.lang.String):java.lang.CharSequence
      a.a.a.n.a.a(java.util.Collection, java.lang.String):java.util.Collection
      a.a.a.n.a.a(boolean, java.lang.String):void
      a.a.a.n.a.a(java.lang.Object, java.lang.String):java.lang.Object */
    public n(InetAddress inetAddress, String str, int i, String str2) {
        this.e = (InetAddress) a.a(inetAddress, "Inet address");
        this.f192a = (String) a.a((Object) str, "Hostname");
        this.b = this.f192a.toLowerCase(Locale.ROOT);
        if (str2 != null) {
            this.d = str2.toLowerCase(Locale.ROOT);
        } else {
            this.d = "http";
        }
        this.c = i;
    }

    public String a() {
        return this.f192a;
    }

    public int b() {
        return this.c;
    }

    public String c() {
        return this.d;
    }

    public Object clone() {
        return super.clone();
    }

    public InetAddress d() {
        return this.e;
    }

    public String e() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.d);
        sb.append("://");
        sb.append(this.f192a);
        if (this.c != -1) {
            sb.append(':');
            sb.append(Integer.toString(this.c));
        }
        return sb.toString();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof n)) {
            return false;
        }
        n nVar = (n) obj;
        if (this.b.equals(nVar.b) && this.c == nVar.c && this.d.equals(nVar.d)) {
            if (this.e == null) {
                if (nVar.e == null) {
                    return true;
                }
            } else if (this.e.equals(nVar.e)) {
                return true;
            }
        }
        return false;
    }

    public String f() {
        if (this.c == -1) {
            return this.f192a;
        }
        StringBuilder sb = new StringBuilder(this.f192a.length() + 6);
        sb.append(this.f192a);
        sb.append(":");
        sb.append(Integer.toString(this.c));
        return sb.toString();
    }

    public int hashCode() {
        int a2 = h.a(h.a(h.a(17, this.b), this.c), this.d);
        return this.e != null ? h.a(a2, this.e) : a2;
    }

    public String toString() {
        return e();
    }
}
