package com.opera.install;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AgreementActivity extends Activity {
    Activity curAct = this;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.agreement);
        ((Button) findViewById(R.id.disagr)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AgreementActivity.this.finish();
            }
        });
        ((Button) findViewById(R.id.agr)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AgreementActivity.this.startActivity(new Intent(AgreementActivity.this.getBaseContext(), ConsoleActivity.class));
                AgreementActivity.this.finish();
            }
        });
    }
}
