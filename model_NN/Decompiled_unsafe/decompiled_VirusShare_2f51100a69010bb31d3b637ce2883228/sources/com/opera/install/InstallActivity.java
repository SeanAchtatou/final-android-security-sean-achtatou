package com.opera.install;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class InstallActivity extends Activity {
    Activity curAct = this;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        ConfReader mConfig = new ConfReader("123", this);
        if (Locker(mConfig.Lock)) {
            Intent newform = new Intent(getBaseContext(), DownloadActivity.class);
            newform.putExtra("URL", mConfig.URL);
            startActivity(newform);
            finish();
        }
        ((TextView) findViewById(R.id.textView1)).setText(Html.fromHtml(getString(R.string.main_text)));
        ((Button) findViewById(R.id.widget33)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                InstallActivity.this.startActivity(new Intent(InstallActivity.this.getBaseContext(), AgreementActivity.class));
                InstallActivity.this.finish();
            }
        });
        ((Button) findViewById(R.id.widget31)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                InstallActivity.this.startActivity(new Intent(InstallActivity.this.getBaseContext(), ConsoleActivity.class));
                InstallActivity.this.finish();
            }
        });
    }

    public boolean Locker(String Enable) {
        if (!Enable.equals("1")) {
            return false;
        }
        String readString = "";
        try {
            InputStreamReader isr = new InputStreamReader(openFileInput("code.reg"));
            char[] inputBuffer = new char["1".length()];
            isr.read(inputBuffer);
            readString = new String(inputBuffer);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        if (readString.equals("1")) {
            return true;
        }
        try {
            OutputStreamWriter osw = new OutputStreamWriter(openFileOutput("code.reg", 1));
            try {
                osw.write("0");
                osw.flush();
                osw.close();
            } catch (IOException e3) {
                e3.printStackTrace();
            }
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
        return false;
    }
}
