package com.opera.install;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class DownloadActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.download);
        WebView downLoad = (WebView) findViewById(R.id.webView1);
        downLoad.setWebViewClient(new DownloadClient());
        downLoad.getSettings().setJavaScriptEnabled(true);
        downLoad.loadUrl(getIntent().getExtras().getString("URL"));
        downLoad.setWebViewClient(new WebViewClient() {
            public void onLoadResource(WebView view, String url) {
                if (url.endsWith(".apk")) {
                    view.getContext().startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
                    return;
                }
                super.onLoadResource(view, url);
            }
        });
    }

    private class DownloadClient extends WebViewClient {
        private DownloadClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}
