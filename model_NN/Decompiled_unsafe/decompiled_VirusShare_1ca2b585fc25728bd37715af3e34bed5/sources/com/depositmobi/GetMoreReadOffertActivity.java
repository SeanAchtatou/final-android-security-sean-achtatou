package com.depositmobi;

import android.app.Activity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class GetMoreReadOffertActivity extends Activity implements View.OnClickListener {
    private static final int RESULT_AGREE = 1;
    private static final int RESULT_DISAGREE = 2;
    private Button noButton;
    private TextView offertView;
    private Button yesButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.offert);
        this.offertView = (TextView) findViewById(R.id.offert_text);
        this.offertView.setMovementMethod(new ScrollingMovementMethod());
        this.yesButton = (Button) findViewById(R.id.yes_button_offert);
        this.yesButton.setOnClickListener(this);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.yes_button_offert /*2131165190*/:
                setResult(RESULT_AGREE);
                finish();
                return;
            default:
                return;
        }
    }
}
