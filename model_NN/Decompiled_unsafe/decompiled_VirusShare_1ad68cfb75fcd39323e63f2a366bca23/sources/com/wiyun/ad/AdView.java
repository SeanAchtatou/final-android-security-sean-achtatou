package com.wiyun.ad;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.wiyun.ad.c;
import java.io.File;
import java.net.URLEncoder;
import java.util.Timer;
import java.util.TimerTask;

public class AdView extends FrameLayout implements View.OnClickListener {
    public static final int AD_TYPE_BANNER = 2;
    public static final int AD_TYPE_FULL_SCREEN = 3;
    public static final int AD_TYPE_TEXT = 1;
    public static final int TRANSITION_BOTTOM_PUSH = 6;
    public static final int TRANSITION_FADE = 7;
    public static final int TRANSITION_FLIP_X = 1;
    public static final int TRANSITION_FLIP_Y = 2;
    public static final int TRANSITION_LEFT_PUSH = 3;
    public static final int TRANSITION_RANDOM = 0;
    public static final int TRANSITION_RIGHT_PUSH = 4;
    public static final int TRANSITION_TOP_PUSH = 5;
    /* access modifiers changed from: private */
    public a a;
    private boolean b;
    /* access modifiers changed from: private */
    public boolean c;
    private int d;
    private Timer e;
    private int f;
    private int g;
    private boolean h;
    private boolean i;
    private int j;
    private boolean k;
    private int l;
    private String m;
    private String n;
    private String o;
    private boolean p;
    /* access modifiers changed from: private */
    public AdListener q;
    private m r;
    /* access modifiers changed from: private */
    public boolean s;
    /* access modifiers changed from: private */
    public q t;
    /* access modifiers changed from: private */
    public View u;
    private View v;
    private Drawable w;

    public interface AdListener {
        void onAdClicked();

        void onAdLoadFailed();

        void onAdLoaded();

        void onExitButtonClicked();
    }

    private final class a implements Runnable {
        /* access modifiers changed from: private */
        public a b;
        private int c;

        public a(a aVar, int i) {
            this.b = aVar;
            this.c = i;
        }

        public void run() {
            AdView.this.a.setVisibility(8);
            this.b.setVisibility(0);
            p pVar = new p(90.0f, 0.0f, ((float) AdView.this.getWidth()) / 2.0f, ((float) AdView.this.getHeight()) / 2.0f, -0.4f * ((float) (this.c == 2 ? AdView.this.getHeight() : AdView.this.getWidth())), false, this.c != 2);
            pVar.setDuration(700);
            pVar.setFillAfter(true);
            pVar.setInterpolator(new DecelerateInterpolator());
            pVar.setAnimationListener(new Animation.AnimationListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, boolean):void
                 arg types: [com.wiyun.ad.AdView, int]
                 candidates:
                  com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, android.view.View):void
                  com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, android.widget.LinearLayout):void
                  com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, com.wiyun.ad.a):void
                  com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, com.wiyun.ad.q):void
                  com.wiyun.ad.AdView.a(com.wiyun.ad.a, int):void
                  com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, boolean):void */
                public void onAnimationEnd(Animation animation) {
                    AdView.this.a.clearAnimation();
                    AdView.this.removeView(AdView.this.a);
                    AdView.this.e();
                    if (AdView.this.a.a() != null) {
                        AdView.this.a.a().a();
                    }
                    AdView.this.a = a.this.b;
                    AdView.this.c = false;
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationStart(Animation animation) {
                }
            });
            AdView.this.startAnimation(pVar);
        }
    }

    public AdView(Context context) {
        this(context, null, 0);
    }

    public AdView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AdView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.f = 0;
        this.g = 0;
        this.l = 2;
        this.m = "*/*";
        setFocusable(true);
        setDescendantFocusability(262144);
        setClickable(true);
        if (attributeSet != null) {
            String str = "http://schemas.android.com/apk/res/" + context.getPackageName();
            int attributeUnsignedIntValue = attributeSet.getAttributeUnsignedIntValue(str, "textColor", 0);
            int attributeUnsignedIntValue2 = attributeSet.getAttributeUnsignedIntValue(str, "backgroundColor", 0);
            if (attributeUnsignedIntValue != 0) {
                setTextColor(attributeUnsignedIntValue);
            }
            if (attributeUnsignedIntValue2 != 0) {
                setBackgroundColor(attributeUnsignedIntValue2);
            }
            this.n = attributeSet.getAttributeValue(str, "resId");
            if (this.n != null) {
                this.n = this.n.trim();
            }
            this.k = attributeSet.getAttributeBooleanValue(str, "testMode", false);
            this.l = attributeSet.getAttributeIntValue(str, "testAdType", 2);
            this.p = attributeSet.getAttributeBooleanValue(str, "showLoadingHint", false);
            setRefreshInterval(attributeSet.getAttributeIntValue(str, "refreshInterval", 0));
            setGoneIfFail(attributeSet.getAttributeBooleanValue(str, "goneIfFail", isGoneIfFail()));
            this.j = attributeSet.getAttributeIntValue(str, "transition", 1);
            this.i = attributeSet.getAttributeBooleanValue(str, "autoStart", false);
        }
        File[] listFiles = getContext().getCacheDir().listFiles();
        if (listFiles != null) {
            long currentTimeMillis = System.currentTimeMillis();
            for (File file : listFiles) {
                if (file.lastModified() - currentTimeMillis > 86400000) {
                    file.delete();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(final LinearLayout linearLayout) {
        if (!this.s) {
            this.s = true;
            TranslateAnimation translateAnimation = new TranslateAnimation(0, 0.0f, 0, 0.0f, 1, 0.0f, 1, -1.0f);
            translateAnimation.setDuration(200);
            translateAnimation.setFillAfter(true);
            translateAnimation.setInterpolator(new DecelerateInterpolator());
            translateAnimation.setAnimationListener(new Animation.AnimationListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.wiyun.ad.AdView.b(com.wiyun.ad.AdView, boolean):void
                 arg types: [com.wiyun.ad.AdView, int]
                 candidates:
                  com.wiyun.ad.AdView.b(com.wiyun.ad.a, int):void
                  com.wiyun.ad.AdView.b(com.wiyun.ad.AdView, boolean):void */
                public void onAnimationEnd(Animation animation) {
                    ViewGroup viewGroup = (ViewGroup) linearLayout.getParent();
                    if (viewGroup != null) {
                        viewGroup.removeView(linearLayout);
                    }
                    AdView.this.s = false;
                    AdView.this.u = (View) null;
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationStart(Animation animation) {
                }
            });
            linearLayout.startAnimation(translateAnimation);
        }
    }

    private void a(a aVar) {
        this.a = aVar;
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(350);
        alphaAnimation.startNow();
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        startAnimation(alphaAnimation);
    }

    private void a(final a aVar, int i2) {
        TranslateAnimation translateAnimation;
        TranslateAnimation translateAnimation2;
        switch (i2) {
            case 3:
                translateAnimation = new TranslateAnimation(1, 0.0f, 1, 1.0f, 1, 0.0f, 1, 0.0f);
                translateAnimation2 = new TranslateAnimation(1, -1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
                break;
            case 4:
                translateAnimation = new TranslateAnimation(1, 0.0f, 1, -1.0f, 1, 0.0f, 1, 0.0f);
                translateAnimation2 = new TranslateAnimation(1, 1.0f, 1, 0.0f, 1, 0.0f, 1, 0.0f);
                break;
            case 5:
                translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, 1.0f);
                translateAnimation2 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, -1.0f, 1, 0.0f);
                break;
            case 6:
                translateAnimation = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 0.0f, 1, -1.0f);
                translateAnimation2 = new TranslateAnimation(1, 0.0f, 1, 0.0f, 1, 1.0f, 1, 0.0f);
                break;
            default:
                translateAnimation = null;
                translateAnimation2 = null;
                break;
        }
        translateAnimation.setDuration(700);
        translateAnimation.setFillAfter(true);
        translateAnimation.setInterpolator(new DecelerateInterpolator());
        translateAnimation2.setDuration(700);
        translateAnimation2.setFillAfter(true);
        translateAnimation2.setInterpolator(new DecelerateInterpolator());
        translateAnimation2.setAnimationListener(new Animation.AnimationListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, boolean):void
             arg types: [com.wiyun.ad.AdView, int]
             candidates:
              com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, android.view.View):void
              com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, android.widget.LinearLayout):void
              com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, com.wiyun.ad.a):void
              com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, com.wiyun.ad.q):void
              com.wiyun.ad.AdView.a(com.wiyun.ad.a, int):void
              com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, boolean):void */
            public void onAnimationEnd(Animation animation) {
                AdView.this.a.clearAnimation();
                AdView.this.removeView(AdView.this.a);
                AdView.this.e();
                if (AdView.this.a.a() != null) {
                    AdView.this.a.a().a();
                }
                AdView.this.a = aVar;
                AdView.this.c = false;
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        this.a.startAnimation(translateAnimation);
        aVar.startAnimation(translateAnimation2);
    }

    private void a(boolean z) {
        synchronized (this) {
            if (z) {
                if (this.d > 0) {
                    if (this.e == null) {
                        this.e = new Timer();
                        this.e.schedule(new TimerTask() {
                            public void run() {
                                AdView.this.post(new Runnable() {
                                    public void run() {
                                        AdView.this.requestAd();
                                    }
                                });
                            }
                        }, (long) (this.d * 1000), (long) (this.d * 1000));
                    }
                }
            }
            if (this.e != null) {
                this.e.cancel();
                this.e = null;
            }
        }
    }

    private void b(a aVar) {
        if (!this.c) {
            this.c = true;
            int i2 = this.j;
            if (i2 == 0) {
                i2 = (Math.abs((int) (SystemClock.uptimeMillis() / 1000)) % 7) + 1;
            }
            switch (i2) {
                case 3:
                case 4:
                case 5:
                case 6:
                    a(aVar, i2);
                    return;
                case 7:
                    c(aVar);
                    return;
                default:
                    b(aVar, i2);
                    return;
            }
        }
    }

    private void b(final a aVar, final int i2) {
        aVar.setVisibility(8);
        p pVar = new p(0.0f, -90.0f, ((float) getWidth()) / 2.0f, ((float) getHeight()) / 2.0f, -0.4f * ((float) (i2 == 2 ? getHeight() : getWidth())), true, i2 != 2);
        pVar.setDuration(700);
        pVar.setFillAfter(true);
        pVar.setInterpolator(new AccelerateInterpolator());
        pVar.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                AdView.this.post(new a(aVar, i2));
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        startAnimation(pVar);
    }

    private void c(final a aVar) {
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(700);
        alphaAnimation.setFillAfter(true);
        alphaAnimation.setInterpolator(new AccelerateInterpolator());
        AlphaAnimation alphaAnimation2 = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation2.setDuration(700);
        alphaAnimation2.setFillAfter(true);
        alphaAnimation2.setInterpolator(new AccelerateInterpolator());
        alphaAnimation2.setAnimationListener(new Animation.AnimationListener() {
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, boolean):void
             arg types: [com.wiyun.ad.AdView, int]
             candidates:
              com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, android.view.View):void
              com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, android.widget.LinearLayout):void
              com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, com.wiyun.ad.a):void
              com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, com.wiyun.ad.q):void
              com.wiyun.ad.AdView.a(com.wiyun.ad.a, int):void
              com.wiyun.ad.AdView.a(com.wiyun.ad.AdView, boolean):void */
            public void onAnimationEnd(Animation animation) {
                AdView.this.a.clearAnimation();
                AdView.this.removeView(AdView.this.a);
                AdView.this.e();
                if (AdView.this.a.a() != null) {
                    AdView.this.a.a().a();
                }
                AdView.this.a = aVar;
                AdView.this.c = false;
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        this.a.startAnimation(alphaAnimation);
        aVar.startAnimation(alphaAnimation2);
    }

    /* access modifiers changed from: private */
    public void d() {
        if (!(this.r == null || this.r.getParent() == null)) {
            this.r.a(false);
            removeView(this.r);
        }
        if (this.a != null) {
            this.a.setVisibility(0);
        }
        if (this.t != null) {
            synchronized (this) {
                if (this.a == null || !this.t.equals(this.a.a())) {
                    boolean z = this.a == null;
                    a aVar = new a(getContext(), this.t, this.q);
                    int backgroundColor = getBackgroundColor();
                    if (backgroundColor == 0) {
                        backgroundColor = this.t.k;
                    }
                    if (backgroundColor == 0) {
                        backgroundColor = -16777216;
                    }
                    int textColor = getTextColor();
                    if (textColor == 0) {
                        textColor = this.t.h;
                    }
                    if (textColor == 0) {
                        textColor = -1;
                    }
                    aVar.setBackgroundColor(backgroundColor);
                    aVar.a(textColor);
                    int d2 = super.getVisibility();
                    aVar.setVisibility(d2);
                    try {
                        if (this.q != null) {
                            try {
                                this.q.onAdLoaded();
                            } catch (Exception e2) {
                                Log.w("WiYun", "Unhandled exception raised in your AdListener.onAdLoaded().", e2);
                            }
                        }
                        addView(aVar);
                        if (this.t.d == 3 && this.v == null) {
                            this.v = f();
                        }
                        if (d2 != 0) {
                            if (this.a != null) {
                                removeView(this.a);
                            }
                            e();
                            if (!(this.a == null || this.a.a() == null)) {
                                this.a.a().a();
                            }
                            this.a = aVar;
                        } else if (z) {
                            a(aVar);
                        } else if (this.p) {
                            removeView(this.a);
                            e();
                            if (this.a.a() != null) {
                                this.a.a().a();
                            }
                            a(aVar);
                        } else {
                            b(aVar);
                        }
                    } catch (Exception e3) {
                        Log.e("WiYun", "Unhandled exception placing AdContainer into AdView.", e3);
                    }
                } else {
                    if (this.a.getParent() == null) {
                        addView(this.a);
                    }
                    this.t.a();
                }
            }
        } else {
            if (this.a != null && this.a.getParent() == null) {
                addView(this.a);
            }
            if (this.q != null) {
                try {
                    this.q.onAdLoadFailed();
                } catch (Exception e4) {
                    Log.w("WiYun", "Unhandled exception raised in your AdListener.onLoadAdFailed().", e4);
                }
            }
        }
        this.b = false;
    }

    /* access modifiers changed from: private */
    public void e() {
        if (this.t != null && this.t.d != 3 && this.v != null) {
            removeView(this.v);
            this.v = null;
            if (this.w != null) {
                this.w.setCallback(null);
                this.w = null;
            }
        }
    }

    private View f() {
        Bitmap bitmap;
        Bitmap bitmap2;
        Bitmap bitmap3;
        if (this.w == null) {
            try {
                Bitmap decodeByteArray = BitmapFactory.decodeByteArray(i.c, 0, i.c.length);
                try {
                    bitmap2 = BitmapFactory.decodeByteArray(i.a, 0, i.a.length);
                    try {
                        Bitmap decodeByteArray2 = BitmapFactory.decodeByteArray(i.b, 0, i.b.length);
                        try {
                            BitmapDrawable bitmapDrawable = new BitmapDrawable(decodeByteArray);
                            BitmapDrawable bitmapDrawable2 = new BitmapDrawable(bitmap2);
                            BitmapDrawable bitmapDrawable3 = new BitmapDrawable(decodeByteArray2);
                            StateListDrawable stateListDrawable = new StateListDrawable();
                            stateListDrawable.addState(new int[]{16842919}, bitmapDrawable3);
                            stateListDrawable.addState(new int[]{16842908}, bitmapDrawable2);
                            stateListDrawable.addState(new int[]{16842913}, bitmapDrawable2);
                            stateListDrawable.addState(new int[]{16842919, 16842909}, bitmapDrawable3);
                            stateListDrawable.addState(new int[]{16842908, 16842909}, bitmapDrawable2);
                            stateListDrawable.addState(new int[]{16842913, 16842909}, bitmapDrawable2);
                            stateListDrawable.addState(new int[0], bitmapDrawable);
                            this.w = stateListDrawable;
                        } catch (OutOfMemoryError e2) {
                            Bitmap bitmap4 = decodeByteArray2;
                            bitmap = decodeByteArray;
                            bitmap3 = bitmap4;
                            if (bitmap != null && !bitmap.isRecycled()) {
                                bitmap.recycle();
                            }
                            if (bitmap2 != null && !bitmap2.isRecycled()) {
                                bitmap2.recycle();
                            }
                            if (bitmap3 != null && !bitmap3.isRecycled()) {
                                bitmap3.recycle();
                            }
                            Button button = new Button(getContext());
                            button.setBackgroundDrawable(this.w);
                            button.setOnClickListener(this);
                            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
                            layoutParams.gravity = 53;
                            addView(button, layoutParams);
                            return button;
                        }
                    } catch (OutOfMemoryError e3) {
                        bitmap = decodeByteArray;
                        bitmap3 = null;
                    }
                } catch (OutOfMemoryError e4) {
                    bitmap2 = null;
                    bitmap = decodeByteArray;
                    bitmap3 = null;
                }
            } catch (OutOfMemoryError e5) {
                bitmap3 = null;
                bitmap2 = null;
                bitmap = null;
            }
        }
        Button button2 = new Button(getContext());
        button2.setBackgroundDrawable(this.w);
        button2.setOnClickListener(this);
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(-2, -2);
        layoutParams2.gravity = 53;
        addView(button2, layoutParams2);
        return button2;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (!this.s) {
            this.s = true;
            final LinearLayout linearLayout = new LinearLayout(getContext());
            linearLayout.setGravity(16);
            linearLayout.setOrientation(0);
            linearLayout.setPadding(5, 5, 0, 0);
            linearLayout.setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{-3879462, -7167046}));
            final EditText editText = new EditText(getContext());
            editText.setTextAppearance(getContext(), 16973895);
            editText.setTypeface(Typeface.DEFAULT_BOLD);
            editText.setSingleLine(true);
            editText.setHint(this.t.g);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
            layoutParams.weight = 1.0f;
            editText.setLayoutParams(layoutParams);
            linearLayout.addView(editText);
            editText.setOnKeyListener(new View.OnKeyListener() {
                public boolean onKey(View view, int i, KeyEvent keyEvent) {
                    if (keyEvent.getAction() == 0) {
                        if (i == 66) {
                            String editable = ((EditText) view).getEditableText().toString();
                            if (!TextUtils.isEmpty(editable)) {
                                final Context applicationContext = AdView.this.getContext().getApplicationContext();
                                new Thread() {
                                    public void run() {
                                        d.a(applicationContext, AdView.this.t);
                                    }
                                }.start();
                                Intent intent = new Intent("android.intent.action.VIEW");
                                intent.addFlags(268435456);
                                try {
                                    intent.setData(Uri.parse(AdView.this.t.p.replace("%query%", URLEncoder.encode(editable, "utf-8"))));
                                    applicationContext.startActivity(intent);
                                } catch (Exception e) {
                                    Log.e("WiYun", "Could not open browser on ad click to " + AdView.this.t.p, e);
                                }
                                if (AdView.this.q != null) {
                                    AdView.this.q.onAdClicked();
                                }
                                AdView.this.a(linearLayout);
                            }
                            return true;
                        } else if (i == 4) {
                            AdView.this.a(linearLayout);
                            return true;
                        }
                    }
                    return false;
                }
            });
            editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                public void onFocusChange(final View view, boolean z) {
                    if (z) {
                        view.post(new Runnable() {
                            public void run() {
                                InputMethodManager inputMethodManager = (InputMethodManager) AdView.this.getContext().getSystemService("input_method");
                                if (inputMethodManager.isActive(view)) {
                                    inputMethodManager.showSoftInput(view, 0);
                                } else {
                                    view.postDelayed(this, 50);
                                }
                            }
                        });
                        return;
                    }
                    view.post(new Runnable() {
                        public void run() {
                            ((InputMethodManager) AdView.this.getContext().getSystemService("input_method")).hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                    });
                    if (view.getParent() != null) {
                        AdView.this.a(linearLayout);
                    }
                }
            });
            addView(linearLayout, new FrameLayout.LayoutParams(-1, getHeight()));
            TranslateAnimation translateAnimation = new TranslateAnimation(0, 0.0f, 0, 0.0f, 1, -1.0f, 1, 0.0f);
            translateAnimation.setDuration(200);
            translateAnimation.setFillAfter(true);
            translateAnimation.setInterpolator(new DecelerateInterpolator());
            translateAnimation.setAnimationListener(new Animation.AnimationListener() {
                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.wiyun.ad.AdView.b(com.wiyun.ad.AdView, boolean):void
                 arg types: [com.wiyun.ad.AdView, int]
                 candidates:
                  com.wiyun.ad.AdView.b(com.wiyun.ad.a, int):void
                  com.wiyun.ad.AdView.b(com.wiyun.ad.AdView, boolean):void */
                public void onAnimationEnd(Animation animation) {
                    editText.requestFocus();
                    AdView.this.s = false;
                }

                public void onAnimationRepeat(Animation animation) {
                }

                public void onAnimationStart(Animation animation) {
                }
            });
            editText.startAnimation(translateAnimation);
            this.u = editText;
        }
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.o;
    }

    public int getBackgroundColor() {
        return this.f;
    }

    public int getRefreshInterval() {
        return this.d;
    }

    public String getResId() {
        return this.n;
    }

    public int getTestAdType() {
        return this.l;
    }

    public int getTextColor() {
        return this.g;
    }

    public int getTransitionType() {
        return this.j;
    }

    public int getVisibility() {
        if (hasAd()) {
            return super.getVisibility();
        }
        if (!this.h) {
            return super.getVisibility();
        }
        if (!this.b || !this.p) {
            return 8;
        }
        return super.getVisibility();
    }

    public boolean hasAd() {
        return this.a != null;
    }

    public boolean isGoneIfFail() {
        return this.h;
    }

    public boolean isShowLoadingHint() {
        return this.p;
    }

    public boolean isTestMode() {
        return this.k;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        c.a(getContext(), (c.a) null);
        if (this.i) {
            requestAd();
        }
    }

    public void onClick(View view) {
        if (this.q != null) {
            this.q.onExitButtonClicked();
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        c.a();
        d.a = false;
        super.onDetachedFromWindow();
    }

    public void onWindowFocusChanged(boolean z) {
        a(z);
    }

    public void requestAd() {
        if (!this.b && !this.c) {
            this.t = null;
            this.b = true;
            if (this.p) {
                if (this.r == null) {
                    this.r = new m(l.a(), 1, getContext());
                    this.r.a(-1);
                    this.r.a(16.0f);
                }
                if (this.u != null) {
                    removeView(this.u);
                    this.u = null;
                }
                if (this.a != null) {
                    this.a.setVisibility(4);
                }
                if (this.r.getParent() == null) {
                    addView(this.r, new FrameLayout.LayoutParams(-1, -1));
                }
                this.r.a(true);
            }
            new Thread() {
                public void run() {
                    try {
                        AdView.this.t = d.a(AdView.this.getContext(), AdView.this);
                        Handler handler = AdView.this.getHandler();
                        while (handler == null) {
                            Thread.sleep(100);
                            handler = AdView.this.getHandler();
                        }
                        handler.post(new Runnable() {
                            public void run() {
                                AdView.this.d();
                            }
                        });
                    } catch (Exception e) {
                        Log.e("WiYun", "Unhandled exception requesting a fresh ad.", e);
                    }
                }
            }.start();
        }
    }

    public void setBackgroundColor(int i2) {
        super.setBackgroundColor(i2);
        this.f = i2;
        if (this.a != null) {
            this.a.setBackgroundColor(i2);
        }
        invalidate();
    }

    public void setGoneIfFail(boolean z) {
        this.h = z;
    }

    public void setListener(AdListener adListener) {
        synchronized (this) {
            this.q = adListener;
        }
    }

    public void setRefreshInterval(int i2) {
        int i3;
        if (i2 <= 0) {
            i3 = 0;
        } else {
            if (i2 < 30) {
                b.a("AdView.setRefreshInterval(" + i2 + ") seconds must be >= " + 30);
            }
            i3 = i2;
        }
        this.d = i3;
        if (i3 == 0) {
            a(false);
        } else {
            a(true);
        }
    }

    public void setResId(String str) {
        this.n = str;
    }

    public void setShowLoadingHint(boolean z) {
        this.p = z;
    }

    public void setTestAdType(int i2) {
        this.l = i2;
    }

    public void setTestMode(boolean z) {
        this.k = z;
    }

    public void setTextColor(int i2) {
        this.g = i2;
        if (this.a != null) {
            this.a.a(i2);
        }
        invalidate();
    }

    public void setTransitionType(int i2) {
        this.j = i2;
    }

    public void setVisibility(int i2) {
        if (super.getVisibility() != i2) {
            synchronized (this) {
                int childCount = getChildCount();
                for (int i3 = 0; i3 < childCount; i3++) {
                    getChildAt(i3).setVisibility(i2);
                }
                super.setVisibility(i2);
            }
        }
    }
}
