package com.wiyun.ad;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;

class o extends BroadcastReceiver {
    final /* synthetic */ c a;

    o(c cVar) {
        this.a = cVar;
    }

    public void onReceive(Context context, Intent intent) {
        NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra("networkInfo");
        boolean z = networkInfo != null && networkInfo.isConnected();
        if (this.a.c != z) {
            this.a.c = z;
            if (z) {
                if (this.a.d != null) {
                    this.a.d.a();
                }
            } else if (this.a.d != null) {
                this.a.d.b();
            }
        }
    }
}
