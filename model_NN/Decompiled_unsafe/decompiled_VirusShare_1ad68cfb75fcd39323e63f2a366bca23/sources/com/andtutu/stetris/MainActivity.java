package com.andtutu.stetris;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.view.Display;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import com.andtutu.stetris.FancyGestureDetector;
import java.util.List;

public class MainActivity extends Activity {
    public static final int HELP = 3;
    public static final int HIDDEN = 7;
    public static final int INSTAL = 2;
    public static final int LOADING = 4;
    public static final int MAIN = 0;
    public static final int QUIT = 5;
    public static final int SHOW = 6;
    public static final int START = 1;
    /* access modifiers changed from: private */
    public View currentView;
    protected Display display;
    private FancyGestureDetector fancyGestureDetector;
    /* access modifiers changed from: private */
    public boolean first = true;
    /* access modifiers changed from: private */
    public GameView gameView;
    private GestureDetector gestureDetector;
    public int height;
    /* access modifiers changed from: private */
    public HelpView helpView;
    /* access modifiers changed from: private */
    public InstalView instalView;
    /* access modifiers changed from: private */
    public MyAD mAd = null;
    /* access modifiers changed from: private */
    public RelativeLayout mLayout;
    /* access modifiers changed from: private */
    public SharedPreferences mSharedPreferences;
    public SoundPoolPlayer mSoundPlayer;
    /* access modifiers changed from: private */
    public MainView mainView;
    Handler myHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    if (MainActivity.this.mSharedPreferences.getBoolean(ConstantUtil.VEDIO_PREFS_WORLD_READ_WRITE, true)) {
                        Intent intent = new Intent(ConstantUtil.INTENT_ACTION_VIEW_SERVICE);
                        intent.putExtra(ConstantUtil.MUSIC_STATE, 1);
                        MainActivity.this.startService(intent);
                    }
                    MainActivity.this.mainView = null;
                    MainActivity.this.mainView = new MainView(MainActivity.this, MainActivity.this.first);
                    MainActivity.this.first = false;
                    MainActivity.this.currentView = MainActivity.this.mainView;
                    MainActivity.this.progressView = null;
                    MainActivity.this.setContentView(MainActivity.this.mainView);
                    return;
                case 1:
                    if (MainActivity.this.mSharedPreferences.getBoolean(ConstantUtil.VEDIO_PREFS_WORLD_READ_WRITE, true)) {
                        Intent intent2 = new Intent(ConstantUtil.INTENT_ACTION_VIEW_SERVICE);
                        intent2.putExtra(ConstantUtil.MUSIC_STATE, 2);
                        MainActivity.this.startService(intent2);
                    }
                    MainActivity.this.setContentView((int) R.layout.main);
                    MainActivity.this.gameView = (GameView) MainActivity.this.findViewById(R.id.gameView);
                    MainActivity.this.currentView = MainActivity.this.gameView;
                    MainActivity.this.mLayout = (RelativeLayout) MainActivity.this.findViewById(R.id.rtlayout);
                    MainActivity.this.mAd = new MyAD(MainActivity.this, 0, false, MainActivity.this.mLayout);
                    MainActivity.this.mAd.setForceTime(160);
                    MainActivity.this.mAd.setDebug(false);
                    MainActivity.this.mAd.initAdViews("5495232a5255f09");
                    MainActivity.this.mLayout.bringToFront();
                    return;
                case 2:
                    MainActivity.this.instalView = new InstalView(MainActivity.this);
                    MainActivity.this.currentView = MainActivity.this.instalView;
                    MainActivity.this.setContentView(MainActivity.this.instalView);
                    return;
                case 3:
                    MainActivity.this.helpView = new HelpView(MainActivity.this);
                    MainActivity.this.currentView = MainActivity.this.helpView;
                    MainActivity.this.setContentView(MainActivity.this.helpView);
                    return;
                case 4:
                    MainActivity.this.progressView = new ProgressView(MainActivity.this);
                    MainActivity.this.currentView = MainActivity.this.progressView;
                    MainActivity.this.setContentView(MainActivity.this.progressView);
                    new Thread() {
                        public void run() {
                            Looper.prepare();
                            BitmapManager.loadGameImages(MainActivity.this.getResources());
                            MainActivity.this.updateProgress(40);
                            BitmapManager.loadGamePublicImages(MainActivity.this.getResources());
                            MainActivity.this.gameView = null;
                            MainActivity.this.updateProgress(100);
                        }
                    }.start();
                    return;
                case 5:
                    MainActivity.this.quit();
                    return;
                case 6:
                    MainActivity.this.show();
                    return;
                case 7:
                    MainActivity.this.hidden();
                    return;
                default:
                    return;
            }
        }
    };
    /* access modifiers changed from: private */
    public ProgressView progressView;
    private SensorEventListener sensorListener;
    private SensorManager sensorManager;
    public int width;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        getWindow().setFlags(1024, 1024);
        this.mSharedPreferences = getSharedPreferences(ConstantUtil.GLOBAL_PREFS_WORLD_READ_WRITE, 3);
        this.display = getWindowManager().getDefaultDisplay();
        this.height = this.display.getHeight();
        this.width = this.display.getWidth();
        BitmapManager.loadMainImages(getResources());
        init();
        this.mSoundPlayer = new SoundPoolPlayer(this);
        this.myHandler.sendEmptyMessage(0);
        this.mLayout = (RelativeLayout) findViewById(R.id.rtlayout);
        this.mAd = new MyAD(this, 0, false, this.mLayout);
        this.mAd.setForceTime(160);
        this.mAd.setDebug(false);
        this.mAd.initAdViews("5495232a5255f09");
    }

    private void init() {
        this.gestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
            public void onLongPress(MotionEvent e) {
                MainActivity.this.gameView.playFallAnim();
            }

            public boolean onSingleTapConfirmed(MotionEvent e) {
                MainActivity.this.gameView.myTouchEvent(e);
                return true;
            }
        });
        this.fancyGestureDetector = new FancyGestureDetector(new FancyGestureDetector.OnGestureListener() {
            public void onGesture(String gesture, float path) {
                if (gesture.equals("down")) {
                    if (path > 100.0f) {
                        MainActivity.this.gameView.playFallAnim();
                    }
                } else if (gesture.equals("rotate")) {
                    MainActivity.this.gameView.move(12);
                } else if (gesture.equals("left")) {
                    if (path > 30.0f) {
                        MainActivity.this.gameView.move(10, (int) (path % 40.0f > 20.0f ? (path / 40.0f) + 1.0f : path / 40.0f));
                    }
                } else if (gesture.equals("right") && path > 30.0f) {
                    MainActivity.this.gameView.move(11, (int) (path % 40.0f > 20.0f ? (path / 40.0f) + 1.0f : path / 40.0f));
                }
            }
        });
        this.fancyGestureDetector.addGesture("left", new int[]{4});
        this.fancyGestureDetector.addGesture("right", new int[]{1});
        this.fancyGestureDetector.addGesture("down", new int[]{2});
        this.fancyGestureDetector.addGesture("rotate", new int[]{6});
        this.sensorManager = (SensorManager) getSystemService("sensor");
        this.sensorListener = new SensorEventListener() {
            public void onSensorChanged(SensorEvent event) {
                float value = event.values[2];
                if (MainActivity.this.gameView != null && MainActivity.this.currentView == MainActivity.this.gameView) {
                    if (value > 30.0f) {
                        MainActivity.this.gameView.move(10, (int) (value % 30.0f > 20.0f ? (value / 30.0f) + 1.0f : value / 30.0f));
                    } else if (value < -30.0f) {
                        MainActivity.this.gameView.move(11, (int) (value % 30.0f > 20.0f ? (value / 30.0f) + 1.0f : value / 30.0f));
                    }
                }
            }

            public void onAccuracyChanged(Sensor sensor, int accuracy) {
            }
        };
    }

    public void updateProgress(int progress) {
        if (this.progressView != null) {
            this.progressView.setProgress(progress);
        }
    }

    /* access modifiers changed from: private */
    public void quit() {
        Process.killProcess(Process.myPid());
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.currentView == this.mainView) {
            return this.mainView.myTouchEvent(event);
        }
        if (this.currentView == this.gameView) {
            return this.gestureDetector.onTouchEvent(event) || this.fancyGestureDetector.onTouchEvent(event);
        }
        if (this.currentView == this.instalView) {
            return this.instalView.myTouchEvent(event);
        }
        return true;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            if (this.currentView == this.helpView || this.currentView == this.instalView) {
                this.helpView = null;
                this.instalView = null;
                this.myHandler.sendEmptyMessage(0);
            }
            if (this.currentView == this.gameView) {
                this.gameView.back();
            }
            if (this.currentView == this.mainView) {
                this.mainView = null;
                quit();
            }
        }
        if (keyCode == 20) {
            this.gameView.playFallAnim();
        }
        if (keyCode == 19) {
            this.gameView.move(12);
        }
        if (keyCode == 21) {
            this.gameView.move(10);
        }
        if (keyCode == 22) {
            this.gameView.move(11);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        List<Sensor> sensors;
        super.onResume();
        if (!(this.sensorManager == null || (sensors = this.sensorManager.getSensorList(3)) == null || sensors.isEmpty())) {
            this.sensorManager.registerListener(this.sensorListener, sensors.get(0), 3);
        }
        if (this.mSharedPreferences.getBoolean(ConstantUtil.VEDIO_PREFS_WORLD_READ_WRITE, true)) {
            Intent intent = new Intent(ConstantUtil.INTENT_ACTION_VIEW_SERVICE);
            intent.putExtra(ConstantUtil.MUSIC_STATE, 5);
            startService(intent);
        }
        if (this.gameView != null && this.currentView == this.gameView) {
            this.gameView.resume();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (this.sensorManager != null) {
            this.sensorManager.unregisterListener(this.sensorListener);
        }
        if (this.gameView != null) {
            this.gameView.pause();
        }
        Intent intent = new Intent(ConstantUtil.INTENT_ACTION_VIEW_SERVICE);
        intent.putExtra(ConstantUtil.MUSIC_STATE, 3);
        startService(intent);
    }

    public void show() {
    }

    public void hidden() {
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (this.mAd == null || !this.mAd.dispatchTouchEvent(ev)) {
            return super.dispatchTouchEvent(ev);
        }
        return true;
    }
}
