package com.andtutu.stetris;

public class Coordinate {
    public int value;
    public int x;
    public int y;

    public Coordinate(int newX, int newY, int newValue) {
        this.x = newX;
        this.y = newY;
        this.value = newValue;
    }

    public boolean equals(Coordinate other) {
        if (this.x == other.x && this.y == other.y) {
            return true;
        }
        return false;
    }

    public String toString() {
        return "Coordinate: [" + this.x + "," + this.y + "]";
    }
}
