package com.andtutu.stetris;

import android.graphics.Point;
import java.util.Random;

public class TetrisPiece {
    public static final int I_PIECE = 2;
    public static final int J_PIECE = 1;
    public static final int L_PIECE = 0;
    public static final int O_PIECE = 5;
    public static final int S_PIECE = 4;
    public static final int T_PIECE = 6;
    public static final int Z_PIECE = 3;
    public int boomValue = 0;
    private Coordinate[] current = new Coordinate[4];
    private Coordinate[] fBlocks = new Coordinate[4];
    private final TetrisBoard fBoard;
    private Point fCentrePoint = new Point();
    private int fMaxRotate;
    private int fRotation;
    private int fType;
    private boolean getRandom = true;
    private Random r = new Random();
    private boolean result = true;
    private int value;

    public TetrisPiece(int type, TetrisBoard board) {
        this.fType = type;
        this.fBoard = board;
        initializeBlocks();
    }

    public Point getCentrePoint() {
        return this.fCentrePoint;
    }

    public void setCentrePoint(Point point) {
        this.fCentrePoint = point;
    }

    public int getType() {
        return this.fType;
    }

    public void setType(int type) {
        this.fType = type;
    }

    public Coordinate[] getRelativePoints() {
        return this.fBlocks;
    }

    public void setRelativePoints(Coordinate[] blocks) {
        if (blocks != null) {
            this.fBlocks = blocks;
        }
    }

    private void initializeBlocks() {
        switch (this.fType) {
            case 0:
                this.fBlocks[0] = new Coordinate(0, 0, getRandom());
                this.fBlocks[1] = new Coordinate(-1, 0, getRandom());
                this.fBlocks[2] = new Coordinate(-1, 1, getRandom());
                this.fBlocks[3] = new Coordinate(1, 0, getRandom());
                this.fMaxRotate = 4;
                resetRandom();
                return;
            case 1:
                this.fBlocks[0] = new Coordinate(0, 0, getRandom());
                this.fBlocks[1] = new Coordinate(-1, 0, getRandom());
                this.fBlocks[2] = new Coordinate(1, 0, getRandom());
                this.fBlocks[3] = new Coordinate(1, 1, getRandom());
                this.fMaxRotate = 4;
                resetRandom();
                return;
            case 2:
                this.fBlocks[0] = new Coordinate(0, 0, getRandom());
                this.fBlocks[1] = new Coordinate(-1, 0, getRandom());
                this.fBlocks[2] = new Coordinate(1, 0, getRandom());
                this.fBlocks[3] = new Coordinate(2, 0, getRandom());
                this.fMaxRotate = 2;
                resetRandom();
                return;
            case 3:
                this.fBlocks[0] = new Coordinate(0, 0, getRandom());
                this.fBlocks[1] = new Coordinate(-1, 0, getRandom());
                this.fBlocks[2] = new Coordinate(0, 1, getRandom());
                this.fBlocks[3] = new Coordinate(1, 1, getRandom());
                this.fMaxRotate = 2;
                resetRandom();
                return;
            case 4:
                this.fBlocks[0] = new Coordinate(0, 0, getRandom());
                this.fBlocks[1] = new Coordinate(1, 0, getRandom());
                this.fBlocks[2] = new Coordinate(0, 1, getRandom());
                this.fBlocks[3] = new Coordinate(-1, 1, getRandom());
                this.fMaxRotate = 2;
                resetRandom();
                return;
            case 5:
                this.fBlocks[0] = new Coordinate(0, 0, getRandom());
                this.fBlocks[1] = new Coordinate(0, 1, getRandom());
                this.fBlocks[2] = new Coordinate(-1, 0, getRandom());
                this.fBlocks[3] = new Coordinate(-1, 1, getRandom());
                this.fMaxRotate = 1;
                resetRandom();
                return;
            case 6:
                this.fBlocks[0] = new Coordinate(0, 0, getRandom());
                this.fBlocks[1] = new Coordinate(-1, 0, getRandom());
                this.fBlocks[2] = new Coordinate(1, 0, getRandom());
                this.fBlocks[3] = new Coordinate(0, 1, getRandom());
                this.fMaxRotate = 4;
                resetRandom();
                return;
            default:
                return;
        }
    }

    private int getRandom() {
        if (this.getRandom) {
            this.value = this.r.nextInt(50);
            if (this.value > 0 && this.value <= 7) {
                this.getRandom = false;
                if (this.value == 5 || this.value == 6) {
                    this.boomValue = this.value;
                }
                return this.value;
            }
        }
        return 0;
    }

    private void resetRandom() {
        this.getRandom = true;
        this.value = 0;
    }

    public static TetrisPiece getRandomPiece(TetrisBoard board) {
        return new TetrisPiece((int) (Math.random() * 7.0d), board);
    }

    public void rotateClockwise() {
        if (this.fMaxRotate > 1) {
            this.fRotation++;
            if (this.fMaxRotate == 2 && this.fRotation == 2) {
                rotateClockwiseNow();
                rotateClockwiseNow();
                rotateClockwiseNow();
            } else {
                rotateClockwiseNow();
            }
        }
        this.fRotation %= this.fMaxRotate;
    }

    private void rotateClockwiseNow() {
        for (int count = 0; count < 4; count++) {
            int temp = this.fBlocks[count].x;
            this.fBlocks[count].x = -this.fBlocks[count].y;
            this.fBlocks[count].y = temp;
        }
    }

    private void rotateAntiClockwise() {
        rotateClockwise();
        rotateClockwise();
        rotateClockwise();
    }

    public Coordinate[] getCurrentPiece() {
        Point centre = getCentrePoint();
        Coordinate[] blocks = getRelativePoints();
        for (int count = 0; count < 4; count++) {
            Coordinate c = blocks[count];
            this.current[count] = new Coordinate(centre.x + c.x, centre.y + c.y, c.value);
        }
        return this.current;
    }

    public boolean move(int direction, TetrisBoard mBoard) {
        this.result = true;
        switch (direction) {
            case 10:
                this.fCentrePoint.x--;
                break;
            case 11:
                this.fCentrePoint.x++;
                break;
            case ConstantUtil.ROTATE:
                rotateClockwise();
                break;
            case ConstantUtil.DOWN:
                this.fCentrePoint.y++;
                break;
        }
        if (!mBoard.willFit(this)) {
            this.result = false;
            switch (direction) {
                case 10:
                    this.fCentrePoint.x++;
                    break;
                case 11:
                    this.fCentrePoint.x--;
                    break;
                case ConstantUtil.ROTATE:
                    rotateAntiClockwise();
                    break;
                case ConstantUtil.DOWN:
                    this.fCentrePoint.y--;
                    break;
            }
        }
        return this.result;
    }

    public int getBoomRow() {
        for (Coordinate c : getCurrentPiece()) {
            if (c.value == 5 || c.value == 6) {
                return c.y;
            }
        }
        return 0;
    }

    public int getBoomCol() {
        for (Coordinate c : getCurrentPiece()) {
            if (c.value == 6) {
                return c.x;
            }
        }
        return 0;
    }
}
