package com.andtutu.stetris;

import android.view.MotionEvent;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FancyGestureDetector {
    private static final List<Double> ANGLES = new ArrayList<Double>() {
        {
            for (double i = -0.39269908169872414d; i <= 5.890486225480862d; i += 0.06283185307179587d) {
                add(Double.valueOf(Math.floor((0.39269908169872414d + i) / FancyGestureDetector.SECTOR_RAD)));
            }
        }
    };
    private static final int HIGHEST_SCORE = 100000;
    public static final Map<String, int[]> LETTER_GESTURES = Collections.unmodifiableMap(new HashMap<String, int[]>() {
        {
            put("A", new int[]{5, 3});
            int[] iArr = new int[12];
            iArr[0] = 2;
            iArr[1] = 6;
            iArr[3] = 1;
            iArr[4] = 2;
            iArr[5] = 3;
            iArr[6] = 4;
            iArr[8] = 1;
            iArr[9] = 2;
            iArr[10] = 3;
            iArr[11] = 4;
            put("B", iArr);
            int[] iArr2 = new int[5];
            iArr2[0] = 4;
            iArr2[1] = 3;
            iArr2[2] = 2;
            iArr2[3] = 1;
            put("C", iArr2);
            int[] iArr3 = new int[8];
            iArr3[0] = 2;
            iArr3[1] = 6;
            iArr3[2] = 7;
            iArr3[4] = 1;
            iArr3[5] = 2;
            iArr3[6] = 3;
            iArr3[7] = 4;
            put("D", iArr3);
            int[] iArr4 = new int[10];
            iArr4[0] = 4;
            iArr4[1] = 3;
            iArr4[2] = 2;
            iArr4[3] = 1;
            iArr4[5] = 4;
            iArr4[6] = 3;
            iArr4[7] = 2;
            iArr4[8] = 1;
            put("E", iArr4);
            put("F", new int[]{4, 2});
            int[] iArr5 = new int[9];
            iArr5[0] = 4;
            iArr5[1] = 3;
            iArr5[2] = 2;
            iArr5[3] = 1;
            iArr5[5] = 7;
            iArr5[6] = 6;
            iArr5[7] = 5;
            put("G", iArr5);
            int[] iArr6 = new int[6];
            iArr6[0] = 2;
            iArr6[1] = 6;
            iArr6[2] = 7;
            iArr6[4] = 1;
            iArr6[5] = 2;
            put("H", iArr6);
            put("I", new int[]{6});
            put("J", new int[]{2, 3, 4});
            int[] iArr7 = new int[7];
            iArr7[0] = 3;
            iArr7[1] = 4;
            iArr7[2] = 5;
            iArr7[3] = 6;
            iArr7[4] = 7;
            iArr7[6] = 1;
            put("K", iArr7);
            put("L", new int[]{4, 6});
            put("M", new int[]{6, 1, 7, 2});
            put("N", new int[]{6, 1, 6});
            int[] iArr8 = new int[9];
            iArr8[0] = 4;
            iArr8[1] = 3;
            iArr8[2] = 2;
            iArr8[3] = 1;
            iArr8[5] = 7;
            iArr8[6] = 6;
            iArr8[7] = 5;
            iArr8[8] = 4;
            put("O", iArr8);
            int[] iArr9 = new int[9];
            iArr9[0] = 6;
            iArr9[1] = 7;
            iArr9[3] = 1;
            iArr9[4] = 2;
            iArr9[5] = 3;
            iArr9[6] = 4;
            iArr9[7] = 5;
            iArr9[8] = 6;
            put("P", iArr9);
            int[] iArr10 = new int[10];
            iArr10[0] = 4;
            iArr10[1] = 3;
            iArr10[2] = 2;
            iArr10[3] = 1;
            iArr10[5] = 7;
            iArr10[6] = 6;
            iArr10[7] = 5;
            iArr10[8] = 4;
            put("Q", iArr10);
            int[] iArr11 = new int[9];
            iArr11[0] = 2;
            iArr11[1] = 6;
            iArr11[2] = 7;
            iArr11[4] = 1;
            iArr11[5] = 2;
            iArr11[6] = 3;
            iArr11[7] = 4;
            iArr11[8] = 1;
            put("R", iArr11);
            int[] iArr12 = new int[9];
            iArr12[0] = 4;
            iArr12[1] = 3;
            iArr12[2] = 2;
            iArr12[3] = 1;
            iArr12[5] = 1;
            iArr12[6] = 2;
            iArr12[7] = 3;
            iArr12[8] = 4;
            put("S", iArr12);
            int[] iArr13 = new int[2];
            iArr13[1] = 2;
            put("T", iArr13);
            int[] iArr14 = new int[5];
            iArr14[0] = 2;
            iArr14[1] = 1;
            iArr14[3] = 7;
            iArr14[4] = 6;
            put("U", iArr14);
            put("V", new int[]{3, 5});
            put("W", new int[]{2, 7, 1, 6});
            int[] iArr15 = new int[7];
            iArr15[0] = 1;
            iArr15[2] = 7;
            iArr15[3] = 6;
            iArr15[4] = 5;
            iArr15[5] = 4;
            iArr15[6] = 3;
            put("X", iArr15);
            int[] iArr16 = new int[11];
            iArr16[0] = 2;
            iArr16[1] = 1;
            iArr16[3] = 7;
            iArr16[4] = 6;
            iArr16[5] = 2;
            iArr16[6] = 3;
            iArr16[7] = 4;
            iArr16[8] = 5;
            iArr16[9] = 6;
            iArr16[10] = 7;
            put("Y", iArr16);
            int[] iArr17 = new int[3];
            iArr17[1] = 3;
            put("Z", iArr17);
            put(" ", new int[1]);
            int[] iArr18 = new int[7];
            iArr18[0] = 6;
            iArr18[1] = 7;
            iArr18[3] = 1;
            iArr18[4] = 2;
            iArr18[5] = 3;
            iArr18[6] = 2;
            put("?", iArr18);
        }
    });
    private static final double SECTOR_RAD = 0.7853981633974483d;
    private final Map<String, int[]> gesturesMap = new HashMap();
    private double lastPositionX;
    private double lastPositionY;
    private final List<Double> moves = new ArrayList();
    private final OnGestureListener onGestureListener;
    private double pathLength;
    private double tempLength;

    public interface OnGestureListener {
        void onGesture(String str, float f);
    }

    public FancyGestureDetector(OnGestureListener onGestureListener2) {
        this.onGestureListener = onGestureListener2;
    }

    public void addGesture(String name, int[] sequence) {
        this.gesturesMap.put(name, sequence);
    }

    public void addGestures(Map<String, int[]> gestures) {
        this.gesturesMap.putAll(gestures);
    }

    public int[] getGesture(String name) {
        return this.gesturesMap.get(name);
    }

    public Map<String, int[]> getGestures() {
        return Collections.unmodifiableMap(this.gesturesMap);
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
                setLastPosition(event.getX(), event.getY());
                return true;
            case 1:
                matchGesture();
                resetMoves();
                return true;
            case 2:
                addMove(event);
                return true;
            default:
                return false;
        }
    }

    public void removeGesture(String name) {
        this.gesturesMap.remove(name);
    }

    private void addMove(MotionEvent event) {
        double difx = ((double) event.getX()) - this.lastPositionX;
        double dify = ((double) event.getY()) - this.lastPositionY;
        double sqDist = (difx * difx) + (dify * dify);
        this.pathLength += Math.sqrt(sqDist);
        this.tempLength += Math.sqrt(sqDist);
        if (sqDist > 64.0d) {
            setLastPosition(event.getX(), event.getY());
            double angle = Math.atan2(dify, difx) + 0.39269908169872414d;
            if (angle < 0.0d) {
                angle += 6.283185307179586d;
            }
            this.moves.add(ANGLES.get((int) Math.floor((angle / 6.283185307179586d) * 100.0d)));
        }
    }

    private double costLeven(int[] a, Double[] b) {
        int y;
        int i;
        if (a[0] == -1) {
            if (b.length == 0) {
                i = 0;
            } else {
                i = HIGHEST_SCORE;
            }
            return (double) i;
        }
        double[][] d = (double[][]) Array.newInstance(Double.TYPE, a.length + 1, b.length + 1);
        double[][] w = (double[][]) Array.newInstance(Double.TYPE, a.length + 1, b.length + 1);
        for (int x = 1; x <= a.length; x++) {
            for (int y2 = 1; y2 < b.length; y2++) {
                d[x][y2] = difAngle(a[x - 1], b[y2 - 1].doubleValue());
            }
        }
        int y3 = 1;
        while (y <= b.length) {
            w[0][y] = 100000.0d;
            y3 = y + 1;
        }
        for (int x2 = 1; x2 <= a.length; x2++) {
            w[x2][0] = 100000.0d;
        }
        w[0][0] = 0.0d;
        int x3 = 1;
        while (x3 <= a.length) {
            y = 1;
            while (y < b.length) {
                double cost = d[x3][y];
                double pa = w[x3 - 1][y] + cost;
                double pb = w[x3][y - 1] + cost;
                w[x3][y] = Math.min(Math.min(pa, pb), w[x3 - 1][y - 1] + cost);
                y++;
            }
            x3++;
        }
        return w[x3 - 1][y - 1];
    }

    private double difAngle(int a, double b) {
        double dif = Math.abs(((double) a) - b);
        if (dif > 4.0d) {
            return 8.0d - dif;
        }
        return dif;
    }

    private void matchGesture() {
        double result = 100000.0d;
        String gesture = null;
        for (Map.Entry<String, int[]> entry : this.gesturesMap.entrySet()) {
            double res = costLeven((int[]) entry.getValue(), (Double[]) this.moves.toArray(new Double[this.moves.size()]));
            if (res < result && res < 30.0d) {
                result = res;
                gesture = entry.getKey();
            }
        }
        if (gesture != null && this.pathLength > 20.0d) {
            this.onGestureListener.onGesture(gesture, (float) this.pathLength);
        }
    }

    private void resetMoves() {
        this.moves.clear();
        setLastPosition(0.0f, 0.0f);
        this.pathLength = 0.0d;
    }

    private void setLastPosition(float x, float y) {
        this.lastPositionX = (double) x;
        this.lastPositionY = (double) y;
    }
}
