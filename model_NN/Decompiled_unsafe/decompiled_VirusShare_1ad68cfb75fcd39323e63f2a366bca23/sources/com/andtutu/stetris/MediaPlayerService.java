package com.andtutu.stetris;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class MediaPlayerService extends Service {
    private MediaPlayerMusic mediaPlayerMusic;

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        this.mediaPlayerMusic = new MediaPlayerMusic(this);
    }

    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        switch (intent.getIntExtra(ConstantUtil.MUSIC_STATE, 1)) {
            case 1:
                this.mediaPlayerMusic.playMainMusic();
                return;
            case 2:
                this.mediaPlayerMusic.stopMainMusic();
                stopSelf();
                return;
            case 3:
                this.mediaPlayerMusic.pauseMainMusic();
                return;
            case 4:
                this.mediaPlayerMusic.playPlayingMusic();
                return;
            case 5:
                this.mediaPlayerMusic.restartMainMusic();
                return;
            case 6:
                this.mediaPlayerMusic.stopPalyingMusic();
                this.mediaPlayerMusic.playMainMusic();
                return;
            default:
                return;
        }
    }
}
