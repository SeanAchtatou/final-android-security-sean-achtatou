package com.andtutu.stetris;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Vibrator;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class GameView extends SurfaceView implements SurfaceHolder.Callback {
    public static final int LOSE = 3;
    public static final int PAUSE = 1;
    public static final int QUIT = 4;
    public static final int READY = 0;
    public static final int RUNNING = 2;
    private int animFrame = 1;
    int boomCol = 0;
    private int boomIndex = 5;
    int boomRow = 0;
    private int booms = 0;
    private boolean change = false;
    private boolean completeLine = true;
    int completeLines = 0;
    public int count;
    int curFrame = 0;
    private int currDirection = 13;
    public int delay = ConstantUtil.THREAD_SLEEP_SPAN;
    private int downIndex = 7;
    private DrawThread dt;
    private TetrisPiece fCurrPiece;
    private boolean faileAnim = false;
    private MainActivity father;
    private int frame = 0;
    public int grade = 1;
    public int highScore = 300;
    private boolean isPlayBoomAnim = false;
    private boolean isPlayBoomVeAnim = false;
    private boolean isPlayVibration;
    private boolean isPlayVideo;
    private long lastTime = 0;
    private TetrisBoard mBoard;
    private int mMode = 0;
    private Paint mPaint = new Paint();
    private SharedPreferences mSharedPreferences;
    private SoundPoolPlayer mSoundPlayer;
    public boolean move = false;
    private TetrisPiece nextPiexe;
    public boolean pause = false;
    private boolean pauseAnim = false;
    private int pauseIndex = 4;
    public boolean playAnim = false;
    public boolean playing = true;
    private Rect rectBoom = new Rect(394, 711, 472, 789);
    private Rect rectDown = new Rect(313, 711, 391, 789);
    private Rect rectGoon = new Rect(137, 244, 336, 336);
    private Rect rectPause = new Rect(353, 628, 431, 706);
    private Rect rectQuit = new Rect(137, 372, 336, 434);
    private Rect rectRestart = new Rect(137, 338, 336, 370);
    public int score = 0;
    int startRow = 0;
    private Vibrator vibrator;

    /*  JADX ERROR: Method load error
        jadx.core.utils.exceptions.DecodeException: Load method exception: Method info already added: android.view.MotionEvent.getX():float in method: com.andtutu.stetris.GameView.myTouchEvent(android.view.MotionEvent):boolean, dex: classes.dex
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:154)
        	at jadx.core.dex.nodes.ClassNode.load(ClassNode.java:306)
        	at jadx.core.ProcessClass.process(ProcessClass.java:36)
        	at java.base/java.util.ArrayList.forEach(ArrayList.java:1540)
        	at jadx.core.ProcessClass.generateCode(ProcessClass.java:59)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:297)
        	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:276)
        Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Method info already added: android.view.MotionEvent.getX():float
        	at jadx.core.dex.info.InfoStorage.putMethod(InfoStorage.java:42)
        	at jadx.core.dex.info.MethodInfo.fromDex(MethodInfo.java:50)
        	at jadx.core.dex.instructions.InsnDecoder.invoke(InsnDecoder.java:678)
        	at jadx.core.dex.instructions.InsnDecoder.decode(InsnDecoder.java:540)
        	at jadx.core.dex.instructions.InsnDecoder.process(InsnDecoder.java:78)
        	at jadx.core.dex.nodes.MethodNode.load(MethodNode.java:139)
        	... 6 more
        */
    public boolean myTouchEvent(android.view.MotionEvent r1) {
        /*
            r8 = this;
            r3 = 6
            r7 = 2
            r6 = 7
            r5 = 3
            r4 = 1
            int r2 = r9.getAction()
            if (r2 != 0) goto L_0x0074
            float r2 = r9.getX()
            int r0 = (int) r2
            float r2 = r9.getY()
            int r1 = (int) r2
            int r2 = r8.mMode
            if (r2 != r7) goto L_0x0075
            android.graphics.Rect r2 = r8.rectPause
            boolean r2 = r2.contains(r0, r1)
            if (r2 == 0) goto L_0x0032
            r8.pauseIndex = r5
            r8.change = r4
            r8.changeState(r4)
            r8.playMusic(r5)
            com.andtutu.stetris.MainActivity r2 = r8.father
            android.os.Handler r2 = r2.myHandler
            r2.sendEmptyMessage(r3)
        L_0x0032:
            android.graphics.Rect r2 = r8.rectDown
            boolean r2 = r2.contains(r0, r1)
            if (r2 == 0) goto L_0x0046
            r2 = 8
            r8.downIndex = r2
            r8.change = r4
            r8.playFallAnim()
            r8.playMusic(r5)
        L_0x0046:
            android.graphics.Rect r2 = r8.rectBoom
            boolean r2 = r2.contains(r0, r1)
            if (r2 == 0) goto L_0x006b
            int r2 = r8.booms
            if (r2 < r4) goto L_0x006b
            int r2 = r8.booms
            int r2 = r2 - r4
            r8.booms = r2
            r8.boomIndex = r3
            r8.change = r4
            r2 = 26
            r8.startRow = r2
            r8.completeLines = r4
            int r2 = r8.startRow
            int r3 = r8.completeLines
            r8.boomAnim(r2, r3)
            r8.playMusic(r5)
        L_0x006b:
            boolean r2 = r8.change
            if (r2 != 0) goto L_0x0074
            r2 = 12
            r8.move(r2)
        L_0x0074:
            return r4
        L_0x0075:
            int r2 = r8.mMode
            if (r2 != r4) goto L_0x0088
            boolean r2 = r8.pauseAnim
            if (r2 == 0) goto L_0x0088
            r8.changeState(r7)
            com.andtutu.stetris.MainActivity r2 = r8.father
            android.os.Handler r2 = r2.myHandler
            r2.sendEmptyMessage(r6)
            goto L_0x0074
        L_0x0088:
            int r2 = r8.mMode
            if (r2 != r5) goto L_0x009b
            boolean r2 = r8.faileAnim
            if (r2 == 0) goto L_0x009b
            r8.restart()
            com.andtutu.stetris.MainActivity r2 = r8.father
            android.os.Handler r2 = r2.myHandler
            r2.sendEmptyMessage(r6)
            goto L_0x0074
        L_0x009b:
            int r2 = r8.mMode
            r3 = 4
            if (r2 != r3) goto L_0x0074
            android.graphics.Rect r2 = r8.rectGoon
            boolean r2 = r2.contains(r0, r1)
            if (r2 == 0) goto L_0x00b3
            r8.changeState(r7)
            com.andtutu.stetris.MainActivity r2 = r8.father
            android.os.Handler r2 = r2.myHandler
            r2.sendEmptyMessage(r6)
            goto L_0x0074
        L_0x00b3:
            android.graphics.Rect r2 = r8.rectRestart
            boolean r2 = r2.contains(r0, r1)
            if (r2 == 0) goto L_0x00c9
            r8.changeState(r7)
            r8.restart()
            com.andtutu.stetris.MainActivity r2 = r8.father
            android.os.Handler r2 = r2.myHandler
            r2.sendEmptyMessage(r6)
            goto L_0x0074
        L_0x00c9:
            android.graphics.Rect r2 = r8.rectQuit
            boolean r2 = r2.contains(r0, r1)
            if (r2 == 0) goto L_0x0074
            r8.tranMusic()
            com.andtutu.stetris.BitmapManager.recycleGame()
            com.andtutu.stetris.MainActivity r2 = r8.father
            android.os.Handler r2 = r2.myHandler
            r3 = 0
            r2.sendEmptyMessage(r3)
            com.andtutu.stetris.MainActivity r2 = r8.father
            android.os.Handler r2 = r2.myHandler
            r2.sendEmptyMessage(r6)
            goto L_0x0074
        */
        throw new UnsupportedOperationException("Method not decompiled: com.andtutu.stetris.GameView.myTouchEvent(android.view.MotionEvent):boolean");
    }

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.father = (MainActivity) context;
        getHolder().addCallback(this);
        this.dt = new DrawThread(this, getHolder());
        this.mBoard = new TetrisBoard(10, 27);
        addNextPiece();
        addPiece();
        this.mMode = 2;
        this.mSoundPlayer = this.father.mSoundPlayer;
        this.vibrator = (Vibrator) this.father.getSystemService("vibrator");
        this.mSharedPreferences = this.father.getSharedPreferences(ConstantUtil.GLOBAL_PREFS_WORLD_READ_WRITE, 3);
        this.isPlayVideo = this.mSharedPreferences.getBoolean(ConstantUtil.VEDIO_PREFS_WORLD_READ_WRITE, true);
        this.isPlayVibration = this.mSharedPreferences.getBoolean(ConstantUtil.VIBRATION_PREFS_WORLD_READ_WRITE, true);
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        if (!this.dt.isAlive()) {
            this.dt = new DrawThread(this, getHolder());
            this.dt.start();
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        if (this.dt.isAlive()) {
            this.dt.flag = false;
        }
    }

    private void reset() {
        if (this.mMode == 2) {
            this.pauseIndex = 4;
        }
        this.boomIndex = 5;
        this.downIndex = 7;
    }

    public TetrisPiece getCurrentPiece() {
        return this.fCurrPiece;
    }

    public void setCurrentPiece(TetrisPiece currPiece) {
        this.fCurrPiece = currPiece;
    }

    private void addPiece() {
        this.fCurrPiece = null;
        this.fCurrPiece = this.nextPiexe;
        addNextPiece();
        this.fCurrPiece.setCentrePoint(new Point(5, 1));
    }

    private void addNextPiece() {
        this.nextPiexe = null;
        this.nextPiexe = TetrisPiece.getRandomPiece(this.mBoard);
    }

    public boolean move(int direction) {
        if (direction != 13 && direction != 14) {
            this.fCurrPiece.move(direction, this.mBoard);
        } else if (!this.fCurrPiece.move(direction, this.mBoard)) {
            if (this.playAnim) {
                this.playAnim = false;
                this.delay = getDelay(this.grade);
            }
            this.mBoard.addPiece(this.fCurrPiece);
            if (this.fCurrPiece.boomValue == 5) {
                this.startRow = this.fCurrPiece.getBoomRow();
                this.completeLines = 1;
                boomAnim(this.startRow, this.completeLines);
                this.score += 50;
                playMusic(2);
            }
            if (this.fCurrPiece.boomValue == 6) {
                boomVeAnim();
                this.score += 45;
            }
            addPiece();
            if (!this.mBoard.willFit(this.fCurrPiece)) {
                this.father.myHandler.sendEmptyMessage(6);
                changeState(3);
            }
            return false;
        }
        return true;
    }

    public void move(int direction, int length) {
        this.currDirection = direction;
        this.count = length;
        this.move = true;
    }

    public void changeState(int state) {
        switch (state) {
            case 1:
                this.playing = false;
                this.pauseAnim = true;
                this.mMode = 1;
                return;
            case 2:
                this.pauseAnim = false;
                this.animFrame = 1;
                this.faileAnim = false;
                this.animFrame = 1;
                this.playing = true;
                this.mMode = 2;
                return;
            case 3:
                this.playing = false;
                this.faileAnim = true;
                this.mMode = 3;
                return;
            case 4:
                if (this.mMode == 2) {
                    this.playing = false;
                    this.mMode = 4;
                    return;
                }
                return;
            default:
                return;
        }
    }

    private void nextLevel() {
        this.score -= this.highScore;
        this.grade++;
        this.delay = getDelay(this.grade);
        this.highScore = getHighScore(this.grade);
    }

    public void playFallAnim() {
        this.playAnim = true;
        this.delay = 50;
    }

    private int getHighScore(int level) {
        return level * 300;
    }

    private int getDelay(int level) {
        if (ConstantUtil.THREAD_SLEEP_SPAN - ((level - 1) * 200) <= 0) {
            return 20;
        }
        return ConstantUtil.THREAD_SLEEP_SPAN - ((level - 1) * 200);
    }

    private void drawNum(int value, Canvas canvas, int x, int y, int type) {
        String num = String.valueOf(value);
        String[] strNums = new String[num.length()];
        if (strNums.length == 1) {
            BitmapManager.drawGameNum(0, canvas, x + 15, y, this.mPaint, type);
            BitmapManager.drawGameNum(Integer.valueOf(num.substring(0, 1)).intValue(), canvas, x + 30, y, this.mPaint, type);
            return;
        }
        for (int i = 0; i < num.length(); i++) {
            strNums[i] = num.substring(i, i + 1);
            BitmapManager.drawGameNum(Integer.valueOf(strNums[i]).intValue(), canvas, x + (i * 15), y, this.mPaint, type);
        }
    }

    public void drawPiece(Canvas canvas) {
        for (int count2 = 0; count2 < 4; count2++) {
            Coordinate c = this.fCurrPiece.getCurrentPiece()[count2];
            BitmapManager.drawPublicImages(c.value, canvas, c.x * 30, (c.y * 30) - 10, this.mPaint);
        }
    }

    public void drawSmallPiece(Canvas canvas) {
        for (int count2 = 0; count2 < 4; count2++) {
            Coordinate c = this.nextPiexe.getRelativePoints()[count2];
            BitmapManager.drawPublicImages(c.value, canvas, (c.x * 30) + 370, (c.y * 30) + 120, this.mPaint);
        }
    }

    public void lineComplete() {
        for (int row = 26; row >= 0; row--) {
            this.completeLine = true;
            for (int col = 9; col >= 0; col--) {
                if (this.mBoard.getValueAtBoard(col, row) == -1) {
                    this.completeLine = false;
                }
            }
            if (this.completeLine) {
                this.completeLines++;
                this.startRow = row;
                computeScore(this.startRow);
                playMusic(1);
                boomAnim(this.startRow, this.completeLines);
            }
        }
    }

    private void boomAnim(int startRow2, int completeLines2) {
        this.isPlayBoomAnim = true;
        for (int i = 0; i < completeLines2; i++) {
            this.mBoard.removeRow(startRow2 + i);
        }
    }

    private void boomVeAnim() {
        this.isPlayBoomVeAnim = true;
        this.boomRow = this.fCurrPiece.getBoomRow();
        this.boomCol = this.fCurrPiece.getBoomCol();
        this.mBoard.removeByPoint(this.boomCol, this.boomRow);
    }

    public void computeScore(int row) {
        int score2 = 50;
        for (int col = 9; col >= 0; col--) {
            switch (this.mBoard.getValueAtBoard(col, row)) {
                case 1:
                    score2 += 5;
                    break;
                case 2:
                    score2 += 10;
                    break;
                case 3:
                    score2 += 25;
                    break;
                case 4:
                    score2 += 50;
                    break;
                case 7:
                    this.booms++;
                    break;
            }
        }
        this.score = score2;
    }

    public void doDraw(Canvas canvas) {
        BitmapManager.drawGameImages(0, canvas, 0, 0, this.mPaint);
        BitmapManager.drawGameImages(1, canvas, 300, 0, this.mPaint);
        BitmapManager.drawGameImages(2, canvas, 308, 622, this.mPaint);
        BitmapManager.drawGameImages(12, canvas, 300, 450, this.mPaint);
        BitmapManager.drawGameImages(this.pauseIndex, canvas, 353, 628, this.mPaint);
        BitmapManager.drawGameImages(this.downIndex, canvas, 313, 711, this.mPaint);
        BitmapManager.drawGameImages(this.boomIndex, canvas, 394, 711, this.mPaint);
        if (this.change) {
            this.change = false;
        } else {
            reset();
        }
        drawNum(this.highScore, canvas, 362, 403, 1);
        drawNum(this.score, canvas, 362, 328, 1);
        drawNum(this.grade, canvas, 362, 250, 1);
        drawNum(this.booms, canvas, 403, 742, 2);
        long current = System.currentTimeMillis();
        for (int cols = 0; cols < 10; cols++) {
            for (int rows = 0; rows < 27; rows++) {
                int piece = this.mBoard.getValueAtBoard(cols, rows);
                if (piece != -1) {
                    BitmapManager.drawPublicImages(piece, canvas, cols * 30, (rows * 30) - 10, this.mPaint);
                }
            }
        }
        drawPiece(canvas);
        drawSmallPiece(canvas);
        if (this.score - this.highScore > 0) {
            playMusic(5);
            nextLevel();
        }
        if (this.mMode == 4) {
            BitmapManager.drawGameImages(14, canvas, 121, 231, this.mPaint);
            BitmapManager.drawGameImages(15, canvas, 146, 278, this.mPaint);
            BitmapManager.drawGameImages(17, canvas, 146, 331, this.mPaint);
            BitmapManager.drawGameImages(19, canvas, 146, 381, this.mPaint);
        }
        if (this.playing) {
            if (!this.isPlayBoomAnim) {
                lineComplete();
            }
            if (this.move && !this.playAnim) {
                if (this.count > 0) {
                    move(this.currDirection);
                    this.count--;
                } else {
                    this.move = false;
                }
            }
            if (this.isPlayBoomAnim) {
                if (this.frame < 3) {
                    for (int i = 0; i < this.completeLines; i++) {
                        BitmapManager.drawBoomAnim(this.frame, canvas, 0, ((this.startRow + i) * 30) - 10, this.mPaint);
                    }
                    this.frame++;
                } else {
                    for (int i2 = 0; i2 < this.completeLines; i2++) {
                        this.mBoard.moveDown(this.startRow + i2);
                    }
                    this.isPlayBoomAnim = false;
                    this.startRow = 0;
                    this.completeLines = 0;
                    this.frame = 0;
                }
            }
            if (this.isPlayBoomVeAnim) {
                if (this.curFrame == 1) {
                    playMusic(2);
                }
                if (this.curFrame < 3) {
                    for (int r = -1; r <= 1; r++) {
                        for (int c = -1; c <= 1; c++) {
                            BitmapManager.drawBoomVeAnim(this.curFrame, canvas, (this.boomCol + c) * 30, ((this.boomRow + r) * 30) - 10, this.mPaint);
                        }
                    }
                    this.curFrame++;
                } else {
                    this.isPlayBoomVeAnim = false;
                    this.boomRow = 0;
                    this.boomCol = 0;
                    this.curFrame = 0;
                }
            }
            if (current - this.lastTime > ((long) this.delay)) {
                this.lastTime = current;
                move(13);
            }
        }
        if (this.pauseAnim) {
            if (this.animFrame <= 9) {
                BitmapManager.drawPauseImages(14, canvas, 72, (this.animFrame - 1) * 20, this.animFrame);
                this.animFrame++;
            } else {
                BitmapManager.drawPauseImages(14, canvas, 72, 160, 9);
            }
        }
        if (!this.faileAnim) {
            return;
        }
        if (this.animFrame <= 9) {
            BitmapManager.drawFaileImages(15, canvas, 72, (this.animFrame - 1) * 20, this.animFrame);
            this.animFrame++;
            return;
        }
        BitmapManager.drawFaileImages(15, canvas, 72, 160, 9);
    }

    private void restart() {
        this.mBoard.resetBoard();
        addPiece();
        changeState(2);
        this.highScore = 300;
        this.delay = ConstantUtil.THREAD_SLEEP_SPAN;
        this.grade = 1;
        this.score = 0;
        this.booms = 0;
    }

    public void back() {
        changeState(4);
        this.father.myHandler.sendEmptyMessage(6);
    }

    public void tranMusic() {
        if (this.mSharedPreferences.getBoolean(ConstantUtil.VEDIO_PREFS_WORLD_READ_WRITE, true)) {
            Intent intent = new Intent(ConstantUtil.INTENT_ACTION_VIEW_SERVICE);
            intent.putExtra(ConstantUtil.MUSIC_STATE, 1);
            getContext().startService(intent);
        }
    }

    private void playMusic(int num) {
        if (this.isPlayVideo) {
            this.mSoundPlayer.playSound(num);
        }
        if (this.isPlayVibration) {
            this.vibrator.vibrate(100);
        }
    }

    public void pause() {
        this.pause = true;
    }

    public void resume() {
        this.pause = false;
    }
}
