package com.andtutu.stetris;

import android.content.Context;
import android.media.MediaPlayer;

public class MediaPlayerMusic {
    private Context context;
    private MediaPlayer mediaPlayer = null;

    public MediaPlayerMusic(Context context2) {
        this.context = context2;
    }

    public void playMainMusic() {
        if (this.mediaPlayer == null) {
            this.mediaPlayer = MediaPlayer.create(this.context, (int) R.raw.main);
            this.mediaPlayer.setLooping(true);
            this.mediaPlayer.start();
        }
    }

    public void playPlayingMusic() {
    }

    public void start() {
        this.mediaPlayer.start();
    }

    public void pauseMainMusic() {
        if (this.mediaPlayer != null) {
            this.mediaPlayer.pause();
        }
    }

    public void stopMainMusic() {
        if (this.mediaPlayer != null) {
            this.mediaPlayer.stop();
            releaseResource();
        }
    }

    public void stopPalyingMusic() {
        if (this.mediaPlayer != null) {
            this.mediaPlayer.stop();
            releaseResource();
        }
    }

    public void restartMainMusic() {
        if (this.mediaPlayer != null) {
            this.mediaPlayer.start();
        }
    }

    public void releaseResource() {
        if (this.mediaPlayer != null) {
            this.mediaPlayer.release();
            this.mediaPlayer = null;
        }
    }
}
