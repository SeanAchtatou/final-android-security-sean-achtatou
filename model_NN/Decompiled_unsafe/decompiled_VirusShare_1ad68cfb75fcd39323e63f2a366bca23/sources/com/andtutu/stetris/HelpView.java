package com.andtutu.stetris;

import android.graphics.Canvas;
import android.view.View;

public class HelpView extends View {
    private MainActivity father;

    public HelpView(MainActivity father2) {
        super(father2);
        this.father = father2;
        BitmapManager.loadHelpImages(getResources());
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        BitmapManager.drawHelpImages(0, canvas, 0, 0);
    }
}
