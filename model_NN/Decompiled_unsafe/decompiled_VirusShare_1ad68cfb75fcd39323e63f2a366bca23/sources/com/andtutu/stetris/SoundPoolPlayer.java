package com.andtutu.stetris;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import java.util.HashMap;

public class SoundPoolPlayer {
    private static SoundPool soundPool;
    private static HashMap<Integer, Integer> soundPoolMap;
    private Context context;

    public SoundPoolPlayer(Context context2) {
        this.context = context2;
        initSounds();
    }

    private void initSounds() {
        soundPool = new SoundPool(3, 1, 96);
        soundPoolMap = new HashMap<>();
        soundPoolMap.put(1, Integer.valueOf(soundPool.load(this.context, R.raw.bom, 1)));
        soundPoolMap.put(2, Integer.valueOf(soundPool.load(this.context, R.raw.anim, 2)));
        soundPoolMap.put(3, Integer.valueOf(soundPool.load(this.context, R.raw.click, 3)));
        soundPoolMap.put(4, Integer.valueOf(soundPool.load(this.context, R.raw.clickbom, 4)));
        soundPoolMap.put(5, Integer.valueOf(soundPool.load(this.context, R.raw.success, 5)));
    }

    public void playSound(int sound) {
        AudioManager mgr = (AudioManager) this.context.getSystemService("audio");
        float volume = ((float) mgr.getStreamVolume(1)) / ((float) mgr.getStreamMaxVolume(1));
        soundPool.play(soundPoolMap.get(Integer.valueOf(sound)).intValue(), volume, volume, 1, 0, 1.0f);
    }
}
