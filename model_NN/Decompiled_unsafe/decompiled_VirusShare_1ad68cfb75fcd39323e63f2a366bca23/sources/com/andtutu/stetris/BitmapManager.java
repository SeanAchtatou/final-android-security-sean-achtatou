package com.andtutu.stetris;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

public class BitmapManager {
    public static final int HEIGHT = 800;
    public static final int WIDTH = 480;
    private static Rect dst = new Rect();
    private static Bitmap[] faileAnimImages;
    private static Bitmap[] gameImages;
    private static Bitmap[] helpImages;
    private static Bitmap[] instalImages;
    private static Bitmap[] loadingImages;
    private static Bitmap[] mainImages;
    private static Bitmap[] pauseAnimImages;
    private static Bitmap[] publicImages;
    private static Bitmap[] smallBloackImages;
    private static Rect src = new Rect();
    private static Bitmap[] welcomeImages;

    public static void loadMainImages(Resources r) {
        mainImages = new Bitmap[11];
        mainImages[0] = BitmapFactory.decodeResource(r, R.drawable.main_bg);
        mainImages[1] = BitmapFactory.decodeResource(r, R.drawable.main_top);
        mainImages[2] = BitmapFactory.decodeResource(r, R.drawable.start_link);
        mainImages[3] = BitmapFactory.decodeResource(r, R.drawable.start_hover);
        mainImages[4] = BitmapFactory.decodeResource(r, R.drawable.help_link);
        mainImages[5] = BitmapFactory.decodeResource(r, R.drawable.help_hover);
        mainImages[6] = BitmapFactory.decodeResource(r, R.drawable.instal_link);
        mainImages[7] = BitmapFactory.decodeResource(r, R.drawable.instal_hover);
        mainImages[8] = BitmapFactory.decodeResource(r, R.drawable.quit_link);
        mainImages[9] = BitmapFactory.decodeResource(r, R.drawable.quit_hover);
        mainImages[10] = BitmapFactory.decodeResource(r, R.drawable.main_anim);
    }

    public static void loadLoadingImages(Resources r) {
        loadingImages = new Bitmap[2];
        loadingImages[0] = BitmapFactory.decodeResource(r, R.drawable.load_bg);
        loadingImages[1] = BitmapFactory.decodeResource(r, R.drawable.loading);
    }

    public static void drawMainAnim(int imgId, Canvas canvas, int x, int y, int frame) {
        src.left = frame * 335;
        src.top = 0;
        src.right = (frame + 1) * 335;
        src.bottom = 304;
        dst.left = x;
        dst.right = x + 335;
        dst.top = y;
        dst.bottom = y + 304;
        canvas.drawBitmap(mainImages[imgId], src, dst, (Paint) null);
    }

    public static void drawProcessImages(Canvas canvas, int x, int y, int w, int h) {
        src.left = 0;
        src.top = 0;
        src.right = w + 0;
        src.bottom = h;
        dst.left = x;
        dst.right = x + w;
        dst.top = y;
        dst.bottom = y + h;
        canvas.drawBitmap(loadingImages[1], src, dst, (Paint) null);
    }

    public static void loadInstalImages(Resources r) {
        instalImages = new Bitmap[5];
        instalImages[0] = BitmapFactory.decodeResource(r, R.drawable.instal_top);
        instalImages[1] = BitmapFactory.decodeResource(r, R.drawable.instal_on);
        instalImages[2] = BitmapFactory.decodeResource(r, R.drawable.instal_off);
        instalImages[3] = BitmapFactory.decodeResource(r, R.drawable.instal_txt_vedio);
        instalImages[4] = BitmapFactory.decodeResource(r, R.drawable.instal_txt_vib);
    }

    public static void loadSmallImages(Resources r) {
        smallBloackImages = new Bitmap[8];
        smallBloackImages[0] = BitmapFactory.decodeResource(r, R.drawable.small_block);
        smallBloackImages[1] = BitmapFactory.decodeResource(r, R.drawable.small_block_5);
        smallBloackImages[2] = BitmapFactory.decodeResource(r, R.drawable.small_block_10);
        smallBloackImages[3] = BitmapFactory.decodeResource(r, R.drawable.small_block_25);
        smallBloackImages[4] = BitmapFactory.decodeResource(r, R.drawable.small_block_50);
        smallBloackImages[5] = BitmapFactory.decodeResource(r, R.drawable.small_block_h);
        smallBloackImages[6] = BitmapFactory.decodeResource(r, R.drawable.small_block_v);
        smallBloackImages[7] = BitmapFactory.decodeResource(r, R.drawable.small_block_boom);
    }

    public static void loadHelpImages(Resources r) {
        helpImages = new Bitmap[1];
        helpImages[0] = BitmapFactory.decodeResource(r, R.drawable.help);
    }

    public static void loadGameImages(Resources r) {
        gameImages = new Bitmap[25];
        gameImages[0] = BitmapFactory.decodeResource(r, R.drawable.bg_left);
        gameImages[1] = BitmapFactory.decodeResource(r, R.drawable.bg_right);
        gameImages[2] = BitmapFactory.decodeResource(r, R.drawable.bg_bottom);
        gameImages[3] = BitmapFactory.decodeResource(r, R.drawable.big_start);
        gameImages[4] = BitmapFactory.decodeResource(r, R.drawable.big_pause);
        gameImages[5] = BitmapFactory.decodeResource(r, R.drawable.boom_link);
        gameImages[6] = BitmapFactory.decodeResource(r, R.drawable.boom_hover);
        gameImages[7] = BitmapFactory.decodeResource(r, R.drawable.down_link);
        gameImages[8] = BitmapFactory.decodeResource(r, R.drawable.down_hover);
        gameImages[9] = BitmapFactory.decodeResource(r, R.drawable.nubmers_black);
        gameImages[10] = BitmapFactory.decodeResource(r, R.drawable.nubmers_red);
        gameImages[11] = BitmapFactory.decodeResource(r, R.drawable.pause);
        gameImages[12] = BitmapFactory.decodeResource(r, R.drawable.score);
        gameImages[13] = BitmapFactory.decodeResource(r, R.drawable.booms);
        gameImages[14] = BitmapFactory.decodeResource(r, R.drawable.quit);
        gameImages[15] = BitmapFactory.decodeResource(r, R.drawable.goon_link);
        gameImages[16] = BitmapFactory.decodeResource(r, R.drawable.goon_hover);
        gameImages[17] = BitmapFactory.decodeResource(r, R.drawable.restart_link);
        gameImages[18] = BitmapFactory.decodeResource(r, R.drawable.restart_hover);
        gameImages[19] = BitmapFactory.decodeResource(r, R.drawable.back_link);
        gameImages[20] = BitmapFactory.decodeResource(r, R.drawable.back_hover);
        gameImages[21] = BitmapFactory.decodeResource(r, R.drawable.nubmers_red);
        gameImages[22] = BitmapFactory.decodeResource(r, R.drawable.nubmers_black);
        gameImages[23] = BitmapFactory.decodeResource(r, R.drawable.boom_anim);
        gameImages[24] = BitmapFactory.decodeResource(r, R.drawable.booms);
        loadGamePauseImages(r);
        loadGameFaileImages(r);
    }

    public static void loadGameFaileImages(Resources r) {
        faileAnimImages = new Bitmap[9];
        faileAnimImages[0] = BitmapFactory.decodeResource(r, R.drawable.fail_anim_1);
        faileAnimImages[1] = BitmapFactory.decodeResource(r, R.drawable.fail_anim_2);
        faileAnimImages[2] = BitmapFactory.decodeResource(r, R.drawable.fail_anim_3);
        faileAnimImages[3] = BitmapFactory.decodeResource(r, R.drawable.fail_anim_4);
        faileAnimImages[4] = BitmapFactory.decodeResource(r, R.drawable.fail_anim_5);
        faileAnimImages[5] = BitmapFactory.decodeResource(r, R.drawable.fail_anim_6);
        faileAnimImages[6] = BitmapFactory.decodeResource(r, R.drawable.fail_anim_7);
        faileAnimImages[7] = BitmapFactory.decodeResource(r, R.drawable.fail_anim_8);
        faileAnimImages[8] = BitmapFactory.decodeResource(r, R.drawable.fail_anim_9);
    }

    public static void loadGamePauseImages(Resources r) {
        pauseAnimImages = new Bitmap[9];
        pauseAnimImages[0] = BitmapFactory.decodeResource(r, R.drawable.pause_anim_1);
        pauseAnimImages[1] = BitmapFactory.decodeResource(r, R.drawable.pause_anim_2);
        pauseAnimImages[2] = BitmapFactory.decodeResource(r, R.drawable.pause_anim_3);
        pauseAnimImages[3] = BitmapFactory.decodeResource(r, R.drawable.pause_anim_4);
        pauseAnimImages[4] = BitmapFactory.decodeResource(r, R.drawable.pause_anim_5);
        pauseAnimImages[5] = BitmapFactory.decodeResource(r, R.drawable.pause_anim_6);
        pauseAnimImages[6] = BitmapFactory.decodeResource(r, R.drawable.pause_anim_7);
        pauseAnimImages[7] = BitmapFactory.decodeResource(r, R.drawable.pause_anim_8);
        pauseAnimImages[8] = BitmapFactory.decodeResource(r, R.drawable.pause_anim_9);
    }

    public static void loadGamePublicImages(Resources r) {
        publicImages = new Bitmap[8];
        publicImages[0] = BitmapFactory.decodeResource(r, R.drawable.block);
        publicImages[1] = BitmapFactory.decodeResource(r, R.drawable.block_5);
        publicImages[2] = BitmapFactory.decodeResource(r, R.drawable.block_10);
        publicImages[3] = BitmapFactory.decodeResource(r, R.drawable.block_25);
        publicImages[4] = BitmapFactory.decodeResource(r, R.drawable.block_50);
        publicImages[5] = BitmapFactory.decodeResource(r, R.drawable.block_h);
        publicImages[6] = BitmapFactory.decodeResource(r, R.drawable.block_v);
        publicImages[7] = BitmapFactory.decodeResource(r, R.drawable.block_boom);
    }

    public static void drawWelcomeImages(int imgId, Canvas canvas, int x, int y, Paint paint) {
        canvas.drawBitmap(welcomeImages[imgId], (float) x, (float) y, paint);
    }

    public static void drawMainImages(int imgId, Canvas canvas, int x, int y) {
        canvas.drawBitmap(mainImages[imgId], (float) x, (float) y, (Paint) null);
    }

    public static void drawLoadingImages(int imgId, Canvas canvas, int x, int y, Paint paint) {
        canvas.drawBitmap(loadingImages[imgId], (float) x, (float) y, (Paint) null);
    }

    public static void drawGameImages(int imgId, Canvas canvas, int x, int y, Paint mPaint) {
        canvas.drawBitmap(gameImages[imgId], (float) x, (float) y, (Paint) null);
    }

    public static void drawPublicImages(int imgId, Canvas canvas, int x, int y, Paint mPaint) {
        canvas.drawBitmap(publicImages[imgId], (float) x, (float) y, (Paint) null);
    }

    public static void drawGameNum(int imgId, Canvas canvas, int x, int y, Paint mPaint, int type) {
        if (type == 1) {
            src.left = imgId * 14;
            src.top = 0;
            src.right = (imgId + 1) * 14;
            src.bottom = 13;
            dst.left = x;
            dst.right = x + 14;
            dst.top = y;
            dst.bottom = y + 13;
            canvas.drawBitmap(gameImages[22], src, dst, (Paint) null);
            return;
        }
        src.left = imgId * 14;
        src.top = 0;
        src.right = (imgId + 1) * 14;
        src.bottom = 20;
        dst.left = x;
        dst.right = x + 14;
        dst.top = y;
        dst.bottom = y + 20;
        canvas.drawBitmap(gameImages[21], src, dst, (Paint) null);
    }

    public static void drawBoomAnim(int imgId, Canvas canvas, int x, int y, Paint mPaint) {
        src.left = imgId * 300;
        src.top = 0;
        src.right = (imgId + 1) * 300;
        src.bottom = 30;
        dst.left = x;
        dst.right = x + 300;
        dst.top = y;
        dst.bottom = y + 30;
        canvas.drawBitmap(gameImages[23], src, dst, (Paint) null);
    }

    public static void drawBoomVeAnim(int imgId, Canvas canvas, int x, int y, Paint mPaint) {
        if (y + 30 > 800) {
            y = 770;
        }
        src.left = imgId * 30;
        src.top = 0;
        src.right = (imgId + 1) * 30;
        src.bottom = 30;
        dst.left = x;
        dst.right = x + 30;
        dst.top = y;
        dst.bottom = y + 30;
        canvas.drawBitmap(gameImages[24], src, dst, (Paint) null);
    }

    public static void drawHelpImages(int imgId, Canvas canvas, int x, int y) {
        canvas.drawBitmap(helpImages[imgId], (float) x, (float) y, (Paint) null);
    }

    public static void drawInstalImages(int imgId, Canvas canvas, int x, int y) {
        canvas.drawBitmap(instalImages[imgId], (float) x, (float) y, (Paint) null);
    }

    public static void drawPauseImages(int imgId, Canvas canvas, int x, int y, int frame) {
        canvas.drawBitmap(pauseAnimImages[frame - 1], 72.0f, (float) (frame * 20), (Paint) null);
    }

    public static void drawFaileImages(int imgId, Canvas canvas, int x, int y, int frame) {
        canvas.drawBitmap(faileAnimImages[frame - 1], 72.0f, (float) (frame * 20), (Paint) null);
    }

    public static void drawSmallImages(int imgId, Canvas canvas, int x, int y) {
        canvas.drawBitmap(smallBloackImages[imgId], (float) x, (float) y, (Paint) null);
    }

    public static void cutBitmap(Bitmap[] imges, Bitmap source, int count, int w, int h) {
        for (int index = 0; index < count; index++) {
            imges[index] = Bitmap.createBitmap(source, index * w, 0, w, h);
        }
    }

    public static void recycleMain() {
        recycle(mainImages);
    }

    public static void recycleGame() {
        recycle(gameImages);
        recycle(faileAnimImages);
        recycle(pauseAnimImages);
        recycle(publicImages);
    }

    public static void recycle(Bitmap[] images) {
        for (Bitmap recycle : images) {
            recycle.recycle();
        }
    }
}
