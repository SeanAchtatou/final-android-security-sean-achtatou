package com.andtutu.stetris;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class DrawThread extends Thread {
    private GameView father;
    public boolean flag = true;
    public boolean isViewOn;
    private SurfaceHolder surfaceHolder;

    public DrawThread(GameView father2, SurfaceHolder surfaceHolder2) {
        this.father = father2;
        this.surfaceHolder = surfaceHolder2;
    }

    public void run() {
        while (this.flag) {
            Canvas canvas = null;
            try {
                canvas = this.surfaceHolder.lockCanvas();
                synchronized (this.surfaceHolder) {
                    if (!this.father.pause) {
                        this.father.doDraw(canvas);
                    }
                }
                if (canvas != null) {
                    this.surfaceHolder.unlockCanvasAndPost(canvas);
                }
            } catch (Exception e) {
                try {
                    e.printStackTrace();
                    if (canvas != null) {
                        this.surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                } catch (Throwable th) {
                    if (canvas != null) {
                        this.surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                    throw th;
                }
            }
            try {
                Thread.sleep(50);
            } catch (Exception e2) {
            }
        }
    }
}
