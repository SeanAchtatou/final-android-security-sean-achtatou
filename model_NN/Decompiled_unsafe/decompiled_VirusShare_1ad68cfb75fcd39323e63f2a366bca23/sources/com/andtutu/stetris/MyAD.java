package com.andtutu.stetris;

import android.app.Activity;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.wiyun.ad.AdView;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MyAD {
    public static final int LEFT_BOTTOM = 1;
    public static final int LEFT_TOP = 0;
    public static final int RIGHT_BOTTOM = 3;
    public static final int RIGHT_TOP = 2;
    private Activity mActivity;
    private int mAdH = 48;
    private View[] mAdViews;
    private int mAdW = 320;
    /* access modifiers changed from: private */
    public int mClickCount = 0;
    /* access modifiers changed from: private */
    public View mCurrentAd;
    private int mCurrentShow = 0;
    private boolean mDebug = false;
    private int mFakeCount = 0;
    private int mForceTime = 180000;
    private int mInterval = 40;
    private boolean mLandscape = false;
    /* access modifiers changed from: private */
    public long mLastTime = 0;
    private RelativeLayout mLayout;
    /* access modifiers changed from: private */
    public int[] mLeftCount;
    View.OnClickListener mOnClickListener;
    private RelativeLayout.LayoutParams mParams;
    private int mPosition = 0;
    private int mScreenH;
    private int mScreenW;
    private long mStartTime;
    private Timer mTimer;
    private TimerTask mTimerTask;
    private AdView mWiyunAd = null;
    private AdView.AdListener mWiyunListener;

    private class ORDER {
        public static final int COUNT = 1;
        public static final int WIYUN = 0;

        private ORDER() {
        }
    }

    public MyAD(Activity activiy, int position, boolean isLandscape, RelativeLayout layout) {
        this.mActivity = activiy;
        this.mStartTime = SystemClock.uptimeMillis();
        this.mLeftCount = new int[1];
        this.mAdViews = new View[1];
        for (int i = 0; i < 1; i++) {
            this.mLeftCount[i] = 0;
            this.mAdViews[i] = null;
        }
        if (position < 0 || position > 3) {
            this.mPosition = 3;
        } else {
            this.mPosition = position;
        }
        this.mScreenW = this.mActivity.getWindowManager().getDefaultDisplay().getWidth();
        this.mScreenH = this.mActivity.getWindowManager().getDefaultDisplay().getHeight();
        if (layout != null) {
            this.mLayout = layout;
        } else {
            this.mLayout = new RelativeLayout(this.mActivity);
            this.mLayout.setEnabled(false);
            this.mLayout.setBackgroundColor(0);
            this.mActivity.addContentView(this.mLayout, new LinearLayout.LayoutParams(-1, -1));
        }
        this.mLandscape = isLandscape;
        if (this.mLandscape) {
            this.mAdW = this.mScreenH < this.mScreenW ? this.mScreenH : this.mScreenW;
            if (this.mAdW >= 480) {
                this.mAdH = 72;
            }
            this.mParams = new RelativeLayout.LayoutParams(this.mAdW, this.mAdH);
        } else {
            if (this.mScreenH >= 800) {
                this.mAdH = 72;
            }
            this.mParams = new RelativeLayout.LayoutParams(-1, this.mAdH);
        }
        this.mParams.setMargins(0, 0, 0, 0);
        switch (this.mPosition) {
            case 0:
                this.mParams.addRule(10);
                this.mParams.addRule(9);
                break;
            case 1:
                this.mParams.addRule(12);
                this.mParams.addRule(9);
                break;
            case 2:
                this.mParams.addRule(10);
                this.mParams.addRule(11);
                break;
            default:
                this.mParams.addRule(12);
                this.mParams.addRule(11);
                break;
        }
        this.mOnClickListener = new View.OnClickListener() {
            public void onClick(View v) {
                MyAD myAD = MyAD.this;
                myAD.mClickCount = myAD.mClickCount + 1;
            }
        };
    }

    public void setDebug(boolean debug) {
        this.mDebug = debug;
    }

    public void setRefreshInterval(int interval) {
        this.mInterval = interval;
    }

    public void setForceTime(int time) {
        this.mForceTime = time < 60 ? 60000 : time * 1000;
    }

    public void initAdViews(String wiyunID) {
        initWiyun(wiyunID);
        this.mTimer = new Timer();
        this.mTimerTask = new TimerTask() {
            public void run() {
                if (MyAD.this.mLastTime == 0) {
                    MyAD.this.mLastTime = System.currentTimeMillis();
                }
                MyAD.this.showAdView();
            }
        };
        this.mTimer.schedule(this.mTimerTask, 10000, (long) (this.mInterval >= 60 ? this.mInterval * 500 : 30000));
    }

    private void initWiyun(String appID) {
        if (appID != null) {
            this.mWiyunAd = new AdView(this.mActivity);
            this.mWiyunAd.setResId(appID);
            this.mWiyunAd.setRefreshInterval(this.mInterval);
            this.mWiyunAd.setTestMode(this.mDebug);
            this.mWiyunListener = new AdView.AdListener() {
                public void onExitButtonClicked() {
                }

                public void onAdLoaded() {
                    MyAD.this.mLeftCount[0] = 4;
                    MyAD.this.showAdView(0);
                }

                public void onAdLoadFailed() {
                }

                public void onAdClicked() {
                }
            };
            this.mWiyunAd.setListener(this.mWiyunListener);
            this.mWiyunAd.setOnClickListener(this.mOnClickListener);
            this.mLayout.addView(this.mWiyunAd, this.mParams);
            this.mWiyunAd.requestAd();
            this.mAdViews[0] = this.mWiyunAd;
        }
    }

    public void showAdView() {
        if (this.mLastTime + ((long) (this.mInterval * 500)) >= SystemClock.elapsedRealtime()) {
            int i = 1;
            while (i < 4) {
                int j = (this.mCurrentShow + i) % 1;
                if (this.mAdViews[j] == null || this.mLeftCount[j] <= 0) {
                    i++;
                } else {
                    showAdView(j);
                    return;
                }
            }
        }
    }

    public void showAdView(int index) {
        int[] iArr = this.mLeftCount;
        iArr[index] = iArr[index] - 1;
        this.mLastTime = System.currentTimeMillis();
        this.mCurrentShow = index;
        this.mCurrentAd = this.mAdViews[index];
        this.mActivity.runOnUiThread(new Runnable() {
            public void run() {
                if (MyAD.this.mCurrentAd != null) {
                    MyAD.this.mCurrentAd.bringToFront();
                }
            }
        });
    }

    private boolean tryFakeClickAd() {
        if (this.mFakeCount >= 10) {
            return false;
        }
        Random r = new Random();
        generateOnClick(this.mCurrentAd, r.nextInt(320) + 1, r.nextInt(48) + 1);
        this.mFakeCount++;
        return true;
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (this.mCurrentAd == null) {
            return false;
        }
        if (this.mFakeCount >= 10 || ev.getAction() != 1) {
            return false;
        }
        switch (this.mPosition) {
            case 0:
                if (ev.getX() > ((float) (this.mCurrentAd.getWidth() + 30)) || ev.getY() > ((float) (this.mCurrentAd.getHeight() + 30))) {
                    return false;
                }
            case 1:
                if (ev.getX() > ((float) (this.mCurrentAd.getWidth() + 30)) || ev.getY() < ((float) ((this.mScreenH - this.mCurrentAd.getHeight()) - 30))) {
                    return false;
                }
            case 2:
                if (ev.getX() < ((float) ((this.mScreenW - this.mCurrentAd.getWidth()) - 30)) || ev.getY() > ((float) (this.mCurrentAd.getHeight() + 30))) {
                    return false;
                }
            default:
                if (ev.getX() < ((float) ((this.mScreenW - this.mCurrentAd.getWidth()) - 30)) || ev.getY() < ((float) ((this.mScreenH - this.mCurrentAd.getHeight()) - 30))) {
                    return false;
                }
        }
        if (new Random().nextInt(50) % 5 != 1) {
            return false;
        }
        tryFakeClickAd();
        return true;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (this.mCurrentAd == null) {
            return false;
        }
        if (SystemClock.uptimeMillis() - this.mStartTime < ((long) this.mForceTime)) {
            return false;
        }
        if (keyCode != 4 || this.mFakeCount != 0 || this.mClickCount >= 3) {
            return false;
        }
        tryFakeClickAd();
        return true;
    }

    private void generateOnClick(View view, int x, int y) {
        if (view.getVisibility() == 0) {
            long downTime = SystemClock.uptimeMillis();
            view.dispatchTouchEvent(getMotionEventDown(downTime, x, y));
            SystemClock.sleep(10);
            view.dispatchTouchEvent(getMotionEventUp(downTime, x, y));
        }
    }

    private MotionEvent getMotionEventUp(long downTime, int x, int y) {
        return MotionEvent.obtain(downTime, SystemClock.uptimeMillis(), 1, (float) x, (float) y, 0);
    }

    private MotionEvent getMotionEventDown(long downTime, int x, int y) {
        return MotionEvent.obtain(downTime, SystemClock.uptimeMillis(), 0, (float) x, (float) y, 0);
    }
}
