package com.andtutu.stetris;

import android.graphics.Point;
import android.util.Log;
import java.lang.reflect.Array;

public class TetrisBoard {
    public static final int EMPTY_BLOCK = -1;
    private int fColumns;
    private int[][] fMatrix;
    private int fRows;

    public TetrisBoard(int cols, int rows) {
        this.fColumns = cols;
        this.fRows = rows;
        resetBoard();
    }

    public void resetBoard() {
        this.fMatrix = (int[][]) Array.newInstance(Integer.TYPE, this.fColumns, this.fRows);
        for (int cols = 0; cols < this.fColumns; cols++) {
            for (int rows = 0; rows < this.fRows; rows++) {
                this.fMatrix[cols][rows] = -1;
            }
        }
    }

    public int getColumns() {
        return this.fColumns;
    }

    public void setColumns(int columns) {
        this.fColumns = columns;
        resetBoard();
    }

    public int getRows() {
        return this.fRows;
    }

    public void setRows(int rows) {
        this.fRows = rows;
        resetBoard();
    }

    public int getValueAtBoard(int x, int y) {
        return this.fMatrix[x][y];
    }

    public void setValueAtBoard(int x, int y, int value) {
        this.fMatrix[x][y] = value;
    }

    public void addPiece(TetrisPiece piece) {
        if (piece != null) {
            Point centre = piece.getCentrePoint();
            Coordinate[] blocks = piece.getRelativePoints();
            for (int count = 0; count < 4; count++) {
                Coordinate c = blocks[count];
                int x = centre.x + c.x;
                this.fMatrix[x][centre.y + c.y] = c.value;
            }
        }
    }

    public void removePiece(TetrisPiece piece) {
        if (piece != null) {
            Point centre = piece.getCentrePoint();
            Coordinate[] blocks = piece.getRelativePoints();
            for (int count = 0; count < 4; count++) {
                Coordinate c = blocks[count];
                int x = centre.x + c.x;
                this.fMatrix[x][centre.y + c.y] = -1;
            }
        }
    }

    public void removeRow(int row) {
        Log.v("removeRow", new StringBuilder(String.valueOf(row)).toString());
        for (int tempCol = 0; tempCol < this.fColumns; tempCol++) {
            this.fMatrix[tempCol][row] = -1;
        }
    }

    public void moveDown(int row) {
        Log.v("moveDown", new StringBuilder(String.valueOf(row)).toString());
        for (int tempRow = row; tempRow > 0; tempRow--) {
            for (int tempCol = 0; tempCol < this.fColumns; tempCol++) {
                this.fMatrix[tempCol][tempRow] = this.fMatrix[tempCol][tempRow - 1];
            }
        }
        for (int tempCol2 = 0; tempCol2 < this.fColumns; tempCol2++) {
            this.fMatrix[tempCol2][0] = -1;
        }
    }

    public boolean willFit(TetrisPiece piece) {
        if (piece != null) {
            Coordinate[] blocks = piece.getCurrentPiece();
            for (int count = 0; count < 4 && 1 != 0; count++) {
                int x = blocks[count].x;
                int y = blocks[count].y;
                if (x < 0 || x >= this.fColumns || y < 0 || y >= this.fRows) {
                    return false;
                }
                if (1 != 0 && this.fMatrix[x][y] != -1) {
                    return false;
                }
            }
        }
        return true;
    }

    public void removeByPoint(int col, int row) {
        for (int r = -1; r <= 1; r++) {
            for (int c = -1; c <= 1; c++) {
                int cols = col + c;
                int rows = row + r;
                if (cols < 0) {
                    cols = 0;
                }
                if (cols >= this.fColumns) {
                    cols = this.fColumns - 1;
                }
                if (rows < 0) {
                    rows = 0;
                }
                if (rows >= this.fRows) {
                    rows = this.fRows - 1;
                }
                this.fMatrix[cols][rows] = -1;
            }
        }
        this.fMatrix[col][row] = -1;
    }
}
