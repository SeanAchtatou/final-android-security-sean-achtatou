package com.andtutu.stetris;

import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;

public class MainView extends View {
    int animFrame = 0;
    int bottomFrame = 0;
    private MainActivity father;
    int helpIndex = 4;
    int instalIndex = 6;
    private boolean isPlayVideo;
    public RefreshAnimHandler mAnimHandler = new RefreshAnimHandler();
    private SharedPreferences mSharedPreferences;
    private SoundPoolPlayer mSoundPlayer;
    int quitIndex = 8;
    private Rect rectMenuHelp = new Rect(96, 520, 380, 625);
    private Rect rectMenuInstal = new Rect(96, 410, 380, 515);
    private Rect rectMenuQuit = new Rect(96, 630, 380, 735);
    private Rect rectMenuStart = new Rect(96, 300, 380, 405);
    private boolean start = true;
    int startIndex = 2;
    private int target = -1;
    int topFrame = 0;
    private int what = 0;

    class RefreshAnimHandler extends Handler {
        RefreshAnimHandler() {
        }

        public void handleMessage(Message msg) {
            MainView.this.postInvalidate();
        }

        public void sleep(long delayMillis) {
            removeMessages(1);
            sendMessageDelayed(obtainMessage(1), delayMillis);
        }
    }

    public MainView(MainActivity father2, boolean isStart) {
        super(father2);
        this.father = father2;
        this.start = isStart;
        this.mSoundPlayer = father2.mSoundPlayer;
        this.mSharedPreferences = father2.getSharedPreferences(ConstantUtil.GLOBAL_PREFS_WORLD_READ_WRITE, 3);
        this.isPlayVideo = this.mSharedPreferences.getBoolean(ConstantUtil.VEDIO_PREFS_WORLD_READ_WRITE, true);
    }

    private void drawPublicImages(Canvas canvas) {
        setBackgroundColor(-1);
        BitmapManager.drawMainImages(1, canvas, 0, 0);
        BitmapManager.drawMainImages(this.startIndex, canvas, 96, 300);
        BitmapManager.drawMainImages(this.instalIndex, canvas, 96, 410);
        BitmapManager.drawMainImages(this.helpIndex, canvas, 96, 520);
        BitmapManager.drawMainImages(this.quitIndex, canvas, 96, 630);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        BitmapManager.drawMainImages(0, canvas, 0, 0);
        if (!this.start) {
            drawPublicImages(canvas);
        } else if (this.animFrame < 8) {
            if (this.animFrame == 1 && this.isPlayVideo) {
                this.mSoundPlayer.playSound(1);
            }
            BitmapManager.drawMainAnim(10, canvas, 72, 0, this.animFrame);
            this.animFrame++;
            this.mAnimHandler.sleep(200);
        } else if (this.topFrame < 8) {
            BitmapManager.drawMainImages(1, canvas, 0, (this.topFrame * 40) - 320);
            this.topFrame++;
            this.mAnimHandler.sleep(200);
        } else if (this.bottomFrame < 8) {
            BitmapManager.drawMainImages(1, canvas, 0, 0);
            BitmapManager.drawMainImages(2, canvas, this.bottomFrame * 12, 300);
            BitmapManager.drawMainImages(6, canvas, this.bottomFrame * 12, 410);
            BitmapManager.drawMainImages(4, canvas, this.bottomFrame * 12, 520);
            BitmapManager.drawMainImages(8, canvas, this.bottomFrame * 12, 630);
            this.bottomFrame++;
            this.mAnimHandler.sleep(150);
        } else {
            this.start = false;
            drawPublicImages(canvas);
        }
    }

    public boolean myTouchEvent(MotionEvent event) {
        if (this.start) {
            return true;
        }
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (this.rectMenuStart.contains(x, y)) {
            this.startIndex = 3;
            this.what = 4;
        } else if (this.rectMenuInstal.contains(x, y)) {
            this.instalIndex = 7;
            this.what = 2;
        } else if (this.rectMenuHelp.contains(x, y)) {
            this.helpIndex = 5;
            this.what = 3;
        } else if (this.rectMenuQuit.contains(x, y)) {
            this.quitIndex = 9;
            this.what = 5;
        }
        invalidate();
        if (this.what == 0) {
            return true;
        }
        this.father.myHandler.sendEmptyMessage(this.what);
        return true;
    }
}
