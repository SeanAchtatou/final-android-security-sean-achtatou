package com.andtutu.stetris;

import android.graphics.Canvas;
import android.view.SurfaceHolder;

public class ProgressDrawThread extends Thread {
    private ProgressView father;
    boolean flag;
    int sleepSpan = 100;
    private SurfaceHolder surfaceHolder;

    public ProgressDrawThread(ProgressView father2, SurfaceHolder surfaceHolder2) {
        this.father = father2;
        this.surfaceHolder = surfaceHolder2;
    }

    public void run() {
        Canvas canvas = null;
        while (this.flag) {
            try {
                canvas = this.surfaceHolder.lockCanvas(null);
                synchronized (this.surfaceHolder) {
                    this.father.doDraw(canvas);
                }
                if (canvas != null) {
                    this.surfaceHolder.unlockCanvasAndPost(canvas);
                }
            } catch (Exception e) {
                try {
                    e.printStackTrace();
                    if (canvas != null) {
                        this.surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                } catch (Throwable th) {
                    if (canvas != null) {
                        this.surfaceHolder.unlockCanvasAndPost(canvas);
                    }
                    throw th;
                }
            }
            try {
                Thread.sleep((long) this.sleepSpan);
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
