package com.andtutu.stetris;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;

public class InstalView extends View {
    private MainActivity father;
    private boolean isVi = true;
    private boolean isVideo = true;
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor prefsWorldReadWrite;
    private Rect rectMenuVi = new Rect(260, 310, 341, 390);
    private Rect rectMenuVideo = new Rect(72, 310, 153, 390);
    private int vi = 1;
    private int video = 1;

    public InstalView(MainActivity father2) {
        super(father2);
        this.father = father2;
        BitmapManager.loadInstalImages(getResources());
        this.mSharedPreferences = father2.getSharedPreferences(ConstantUtil.GLOBAL_PREFS_WORLD_READ_WRITE, 3);
        this.prefsWorldReadWrite = this.mSharedPreferences.edit();
        this.isVideo = this.mSharedPreferences.getBoolean(ConstantUtil.VEDIO_PREFS_WORLD_READ_WRITE, true);
        this.isVi = this.mSharedPreferences.getBoolean(ConstantUtil.VIBRATION_PREFS_WORLD_READ_WRITE, true);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        BitmapManager.drawMainImages(0, canvas, 0, 0);
        BitmapManager.drawInstalImages(0, canvas, 0, 0);
        BitmapManager.drawInstalImages(this.video, canvas, 72, 310);
        BitmapManager.drawInstalImages(this.vi, canvas, 260, 310);
        BitmapManager.drawInstalImages(3, canvas, 150, 330);
        BitmapManager.drawInstalImages(4, canvas, 338, 330);
    }

    public boolean myTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();
        if (event.getAction() == 1) {
            if (this.rectMenuVideo.contains(x, y)) {
                if (this.isVideo) {
                    this.isVideo = false;
                    this.video = 2;
                    Intent intent = new Intent(ConstantUtil.INTENT_ACTION_VIEW_SERVICE);
                    intent.putExtra(ConstantUtil.MUSIC_STATE, 2);
                    this.father.startService(intent);
                } else {
                    this.isVideo = true;
                    this.video = 1;
                    Intent intent2 = new Intent(ConstantUtil.INTENT_ACTION_VIEW_SERVICE);
                    intent2.putExtra(ConstantUtil.MUSIC_STATE, 1);
                    this.father.startService(intent2);
                }
            }
            if (this.rectMenuVi.contains(x, y)) {
                if (this.isVi) {
                    this.isVi = false;
                    this.vi = 2;
                } else {
                    this.isVi = true;
                    this.vi = 1;
                }
            }
            this.prefsWorldReadWrite.putBoolean(ConstantUtil.VEDIO_PREFS_WORLD_READ_WRITE, this.isVideo);
            this.prefsWorldReadWrite.putBoolean(ConstantUtil.VIBRATION_PREFS_WORLD_READ_WRITE, this.isVi);
            this.prefsWorldReadWrite.commit();
            invalidate();
        }
        return true;
    }
}
