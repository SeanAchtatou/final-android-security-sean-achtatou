package com.andtutu.stetris;

import android.graphics.Canvas;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class ProgressView extends SurfaceView implements SurfaceHolder.Callback {
    private MainActivity father;
    private ProgressDrawThread pdt;
    private int processHeight = 65;
    private int processWidth = 411;
    private int progress;
    private int target = 1;

    public ProgressView(MainActivity father2) {
        super(father2);
        this.father = father2;
        BitmapManager.loadLoadingImages(getResources());
        getHolder().addCallback(this);
        this.pdt = new ProgressDrawThread(this, getHolder());
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        this.pdt.flag = true;
        if (!this.pdt.isAlive()) {
            this.pdt.start();
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        this.pdt.flag = false;
    }

    public void doDraw(Canvas canvas) {
        BitmapManager.drawMainImages(0, canvas, 0, 0);
        BitmapManager.drawLoadingImages(0, canvas, 39, 342, null);
        int p = this.progress;
        BitmapManager.drawProcessImages(canvas, 39, 342, (int) ((((double) p) / 100.0d) * ((double) this.processWidth)), this.processHeight);
        if (p == 100) {
            this.father.myHandler.sendEmptyMessage(this.target);
        }
    }

    public void setProgress(int progress2) {
        this.progress = progress2;
    }
}
