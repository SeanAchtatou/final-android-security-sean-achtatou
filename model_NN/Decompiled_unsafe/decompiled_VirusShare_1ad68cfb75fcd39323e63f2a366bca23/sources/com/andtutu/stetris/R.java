package com.andtutu.stetris;

public final class R {

    public static final class attr {
        public static final int backgroundColor = 2130771968;
        public static final int keywords = 2130771971;
        public static final int primaryTextColor = 2130771969;
        public static final int refreshInterval = 2130771972;
        public static final int secondaryTextColor = 2130771970;
    }

    public static final class drawable {
        public static final int back_hover = 2130837504;
        public static final int back_link = 2130837505;
        public static final int bg_bottom = 2130837506;
        public static final int bg_left = 2130837507;
        public static final int bg_right = 2130837508;
        public static final int big_pause = 2130837509;
        public static final int big_start = 2130837510;
        public static final int block = 2130837511;
        public static final int block_10 = 2130837512;
        public static final int block_25 = 2130837513;
        public static final int block_5 = 2130837514;
        public static final int block_50 = 2130837515;
        public static final int block_boom = 2130837516;
        public static final int block_h = 2130837517;
        public static final int block_v = 2130837518;
        public static final int boom_anim = 2130837519;
        public static final int boom_hover = 2130837520;
        public static final int boom_link = 2130837521;
        public static final int booms = 2130837522;
        public static final int down_hover = 2130837523;
        public static final int down_link = 2130837524;
        public static final int fail_anim_1 = 2130837525;
        public static final int fail_anim_2 = 2130837526;
        public static final int fail_anim_3 = 2130837527;
        public static final int fail_anim_4 = 2130837528;
        public static final int fail_anim_5 = 2130837529;
        public static final int fail_anim_6 = 2130837530;
        public static final int fail_anim_7 = 2130837531;
        public static final int fail_anim_8 = 2130837532;
        public static final int fail_anim_9 = 2130837533;
        public static final int goon_hover = 2130837534;
        public static final int goon_link = 2130837535;
        public static final int help = 2130837536;
        public static final int help_hover = 2130837537;
        public static final int help_link = 2130837538;
        public static final int icon = 2130837539;
        public static final int instal_hover = 2130837540;
        public static final int instal_link = 2130837541;
        public static final int instal_off = 2130837542;
        public static final int instal_on = 2130837543;
        public static final int instal_top = 2130837544;
        public static final int instal_txt_vedio = 2130837545;
        public static final int instal_txt_vib = 2130837546;
        public static final int load_bg = 2130837547;
        public static final int loading = 2130837548;
        public static final int main_anim = 2130837549;
        public static final int main_bg = 2130837550;
        public static final int main_top = 2130837551;
        public static final int nubmers_black = 2130837552;
        public static final int nubmers_red = 2130837553;
        public static final int pause = 2130837554;
        public static final int pause_anim_1 = 2130837555;
        public static final int pause_anim_2 = 2130837556;
        public static final int pause_anim_3 = 2130837557;
        public static final int pause_anim_4 = 2130837558;
        public static final int pause_anim_5 = 2130837559;
        public static final int pause_anim_6 = 2130837560;
        public static final int pause_anim_7 = 2130837561;
        public static final int pause_anim_8 = 2130837562;
        public static final int pause_anim_9 = 2130837563;
        public static final int quit = 2130837564;
        public static final int quit_hover = 2130837565;
        public static final int quit_link = 2130837566;
        public static final int restart_hover = 2130837567;
        public static final int restart_link = 2130837568;
        public static final int score = 2130837569;
        public static final int small_block = 2130837570;
        public static final int small_block_10 = 2130837571;
        public static final int small_block_25 = 2130837572;
        public static final int small_block_5 = 2130837573;
        public static final int small_block_50 = 2130837574;
        public static final int small_block_boom = 2130837575;
        public static final int small_block_h = 2130837576;
        public static final int small_block_v = 2130837577;
        public static final int start_hover = 2130837578;
        public static final int start_link = 2130837579;
    }

    public static final class id {
        public static final int gameView = 2131099648;
        public static final int rtlayout = 2131099649;
    }

    public static final class layout {
        public static final int main = 2130903040;
    }

    public static final class raw {
        public static final int anim = 2130968576;
        public static final int bom = 2130968577;
        public static final int click = 2130968578;
        public static final int clickbom = 2130968579;
        public static final int main = 2130968580;
        public static final int success = 2130968581;
    }

    public static final class string {
        public static final int app_name = 2131034112;
    }

    public static final class styleable {
        public static final int[] com_admob_android_ads_AdView = {R.attr.backgroundColor, R.attr.primaryTextColor, R.attr.secondaryTextColor, R.attr.keywords, R.attr.refreshInterval};
        public static final int com_admob_android_ads_AdView_backgroundColor = 0;
        public static final int com_admob_android_ads_AdView_keywords = 3;
        public static final int com_admob_android_ads_AdView_primaryTextColor = 1;
        public static final int com_admob_android_ads_AdView_refreshInterval = 4;
        public static final int com_admob_android_ads_AdView_secondaryTextColor = 2;
    }
}
