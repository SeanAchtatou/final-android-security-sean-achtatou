package uk.co.lilhermit.android.core;

import android.util.Log;

public class Native {
    public static final String LOGTAG = "lilhermitCore";
    private static boolean Loaded;

    public static native String getprop(String str);

    public static native int runcmd(String str, String str2);

    public static native int runcmd_simple(String str);

    static {
        Loaded = false;
        try {
            System.loadLibrary("native");
            Loaded = true;
        } catch (UnsatisfiedLinkError e) {
            e.printStackTrace();
            Loaded = false;
        }
    }

    public static String get_prop_wrapper(String paramString) {
        if (Loaded) {
            return getprop(paramString);
        }
        Log.e(LOGTAG, "Native:get_prop_wrapper - Can't run command Native library not loaded!");
        return null;
    }

    public static int runcmd_wrapper(String paramString1, String paramString2) {
        Log.e("msg", "runcmd_wrapper begin");
        if (Loaded) {
            return runcmd(paramString1, paramString2);
        }
        Log.e(LOGTAG, "Native:runcmd_wrapper - Can't run command Native library not loaded!");
        return -1;
    }
}
