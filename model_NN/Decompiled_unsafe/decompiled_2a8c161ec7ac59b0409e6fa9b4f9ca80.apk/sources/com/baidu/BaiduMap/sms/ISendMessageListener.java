package com.baidu.BaiduMap.sms;

public interface ISendMessageListener {
    void onLogSendMessageStatus(int i);

    void onSendFailed();

    void onSendSucceed();
}
