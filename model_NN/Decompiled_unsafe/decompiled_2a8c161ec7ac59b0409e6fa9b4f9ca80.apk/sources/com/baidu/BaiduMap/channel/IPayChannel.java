package com.baidu.BaiduMap.channel;

public interface IPayChannel {
    void addPayChannelListener(IPayChannelListener iPayChannelListener);

    void pay();

    void removePayChannelListener(IPayChannelListener iPayChannelListener);
}
