package com.baidu.BaiduMap.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;

public class NetWorkUtil {
    public static NetWorkUtil instance;

    public static NetWorkUtil getInstance(Context ctx) {
        if (instance == null) {
            instance = new NetWorkUtil();
        }
        return instance;
    }

    public static void setMobileData(Context pContext, boolean pBoolean) {
        try {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) pContext.getSystemService("connectivity");
            mConnectivityManager.getClass().getMethod("setMobileDataEnabled", Boolean.TYPE).invoke(mConnectivityManager, Boolean.valueOf(pBoolean));
            Log.i("aa", "移动数据设置成功 ");
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("aa", "移动数据设置错误: " + e.toString());
        }
    }

    public static boolean getMobileDataState(Context pContext, Object[] arg) {
        try {
            ConnectivityManager mConnectivityManager = (ConnectivityManager) pContext.getSystemService("connectivity");
            Class<?> cls = mConnectivityManager.getClass();
            Class[] argsClass = null;
            if (arg != null) {
                argsClass = new Class[]{arg.getClass()};
            }
            return ((Boolean) cls.getMethod("getMobileDataEnabled", argsClass).invoke(mConnectivityManager, arg)).booleanValue();
        } catch (Exception e) {
            Log.i("aa", "得到移动数据状态出错: " + e.toString());
            return false;
        }
    }
}
