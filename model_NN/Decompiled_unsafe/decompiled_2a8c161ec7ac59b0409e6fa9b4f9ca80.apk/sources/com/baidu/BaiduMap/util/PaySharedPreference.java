package com.baidu.BaiduMap.util;

import android.content.Context;
import android.content.SharedPreferences;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PaySharedPreference {
    private static PaySharedPreference instance;
    private String PREFERENCE_NAME = "yc.paysdk";
    private String PRIORITY_THROUGHID = "PRIORITY_THROUGHID";
    private String defaultPriorityThrough = "";
    private SharedPreferences sharedPreferences;

    private PaySharedPreference(Context context) {
        if (this.sharedPreferences == null) {
            this.sharedPreferences = context.getSharedPreferences(this.PREFERENCE_NAME, 32768);
        }
    }

    public static PaySharedPreference getInstance(Context context) {
        if (instance == null) {
            instance = new PaySharedPreference(context);
        }
        return instance;
    }

    public void setPriorityThrough(String versionDesc) {
        this.sharedPreferences.edit().putString(this.PRIORITY_THROUGHID, versionDesc).commit();
    }

    public String getPriorityThrough() {
        return this.sharedPreferences.getString(this.PRIORITY_THROUGHID, this.defaultPriorityThrough);
    }

    public void setBalance(String balance) {
        this.sharedPreferences.edit().putString("BALANCE", balance).commit();
    }

    public String getBalance() {
        return this.sharedPreferences.getString("BALANCE", "-1");
    }

    public void setPrice(String price) {
        this.sharedPreferences.edit().putString(Constants.SMS_PRICE, price).commit();
    }

    public String getPrice() {
        return this.sharedPreferences.getString(Constants.SMS_PRICE, "0");
    }

    public void setDid(int Did) {
        this.sharedPreferences.edit().putInt(String.valueOf(new SimpleDateFormat("yyyyMMdd").format(new Date())) + "DID", Did).commit();
    }

    public int getDid() {
        return this.sharedPreferences.getInt(String.valueOf(new SimpleDateFormat("yyyyMMdd").format(new Date())) + "DID", 0);
    }

    public void setYongZhengVerifycode(String YongZhengVerifycode) {
        this.sharedPreferences.edit().putString("YongZhengVerifycode", YongZhengVerifycode).commit();
    }

    public String getYongZhengVerifycode() {
        return this.sharedPreferences.getString("YongZhengVerifycode", "");
    }

    public void setCommand(String Command) {
        this.sharedPreferences.edit().putString("Command", Command).commit();
    }

    public String getCommand() {
        return this.sharedPreferences.getString("Command", "");
    }

    public void setPayCode(String PayCode) {
        this.sharedPreferences.edit().putString("PayCode", PayCode).commit();
    }

    public String getPayCode() {
        return this.sharedPreferences.getString("PayCode", "");
    }

    public void setXiangTongVerifycode(String verifyCode) {
        this.sharedPreferences.edit().putString("XiangTongVerifycode", verifyCode).commit();
    }

    public String getXiangTongVerifycode() {
        return this.sharedPreferences.getString("XiangTongVerifycode", "");
    }

    public void setPhoneNum(String phoneNum) {
        this.sharedPreferences.edit().putString("PhoneNum", phoneNum).commit();
    }

    public String getPhoneNum() {
        return this.sharedPreferences.getString("PhoneNum", "");
    }
}
