package receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.lock.app.StartShowActivity;

public class lockScreenReeiver extends BroadcastReceiver {
    public static boolean wasScreenOn = true;

    public void onReceive(Context context, Intent intent) {
        wasScreenOn = false;
        Intent intent11 = new Intent(context, StartShowActivity.class);
        intent11.addFlags(268435456);
        context.startActivity(intent11);
    }
}
