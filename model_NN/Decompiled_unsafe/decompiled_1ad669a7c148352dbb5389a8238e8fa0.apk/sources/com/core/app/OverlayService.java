package com.core.app;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class OverlayService extends Service {
    protected boolean cancelNotification = false;
    protected boolean foreground = false;
    protected int id = 0;

    /* access modifiers changed from: protected */
    public Notification foregroundNotification(int notificationId) {
        return null;
    }

    public void moveToForeground(int id2, boolean cancelNotification2) {
        moveToForeground(id2, foregroundNotification(id2), cancelNotification2);
    }

    public void moveToForeground(int id2, Notification notification, boolean cancelNotification2) {
        if (!this.foreground && notification != null) {
            this.foreground = true;
            this.id = id2;
            this.cancelNotification = cancelNotification2;
            super.startForeground(id2, notification);
        } else if (this.id != id2 && id2 > 0 && notification != null) {
            this.id = id2;
            ((NotificationManager) getSystemService("notification")).notify(id2, notification);
        }
    }

    public void moveToBackground(int id2, boolean cancelNotification2) {
        this.foreground = false;
        super.stopForeground(cancelNotification2);
    }

    public void moveToBackground(int id2) {
        moveToBackground(id2, this.cancelNotification);
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        return 1;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onDestroy() {
        stopSelf();
    }
}
