package com.lock.app;

import android.app.PendingIntent;
import com.core.app.OverlayService;

public class MyService extends OverlayService {
    public static MyService instance;
    private StartOvView overlayView;

    public void onCreate() {
        super.onCreate();
        instance = this;
        this.overlayView = new StartOvView(this);
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.overlayView != null) {
            this.overlayView.destory();
        }
    }

    public static void stop() {
        if (instance != null) {
            instance.stopSelf();
        }
    }

    private PendingIntent notificationIntent() {
        return null;
    }
}
