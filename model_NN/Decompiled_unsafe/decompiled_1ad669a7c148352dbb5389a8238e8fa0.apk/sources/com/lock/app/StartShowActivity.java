package com.lock.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class StartShowActivity extends Activity {
    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startService(new Intent(this, MyService.class));
    }
}
