import java.lang.reflect.Array;

/* renamed from: ˊ  reason: contains not printable characters */
public final class C0018 {

    /* renamed from: ˊ  reason: contains not printable characters */
    private static final long[][] f133 = ((long[][]) Array.newInstance(Long.TYPE, 8, 256));

    static {
        for (int i = 0; i < 256; i++) {
            long j = (long) i;
            for (int i2 = 0; i2 < 8; i2++) {
                if ((1 & j) == 1) {
                    j = -3932672073523589310L ^ (j >>> 1);
                } else {
                    j >>>= 1;
                }
            }
            f133[0][i] = j;
        }
        for (int i3 = 0; i3 < 256; i3++) {
            long j2 = f133[0][i3];
            for (int i4 = 1; i4 < 8; i4++) {
                j2 = f133[0][(int) (255 & j2)] ^ (j2 >>> 8);
                f133[i4][i3] = j2;
            }
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public static long m62(byte[] bArr) {
        int length = bArr.length;
        int i = 0;
        long j = -1;
        while (length >= 8) {
            long j2 = j;
            byte[] bArr2 = {bArr[i], bArr[i + 1], bArr[i + 2], bArr[i + 3], bArr[i + 4], bArr[i + 5], bArr[i + 6], bArr[i + 7]};
            long j3 = 0;
            for (int i2 = 0; i2 < 8; i2++) {
                j3 |= (((long) bArr2[i2]) & 255) << (i2 * 8);
            }
            long j4 = j2 ^ j3;
            j = ((((((f133[7][(int) (255 & j4)] ^ f133[6][(int) ((j4 >>> 8) & 255)]) ^ f133[5][(int) ((j4 >>> 16) & 255)]) ^ f133[4][(int) ((j4 >>> 24) & 255)]) ^ f133[3][(int) ((j4 >>> 32) & 255)]) ^ f133[2][(int) ((j4 >>> 40) & 255)]) ^ f133[1][(int) ((j4 >>> 48) & 255)]) ^ f133[0][(int) ((j4 >>> 56) & 255)];
            i += 8;
            length -= 8;
        }
        while (length != 0) {
            int i3 = i;
            i++;
            j = f133[0][(int) ((((long) bArr[i3]) ^ j) & 255)] ^ (j >>> 8);
            length--;
        }
        return -1 ^ j;
    }
}
