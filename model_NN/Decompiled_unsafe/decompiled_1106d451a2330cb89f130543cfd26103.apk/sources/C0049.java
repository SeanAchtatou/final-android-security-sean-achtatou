import java.io.ByteArrayInputStream;

/* renamed from: ᗮ  reason: contains not printable characters */
public final class C0049 {

    /* renamed from: ˊ  reason: contains not printable characters */
    public int f308;

    /* renamed from: ˋ  reason: contains not printable characters */
    public int f309;

    /* renamed from: ˎ  reason: contains not printable characters */
    public ByteArrayInputStream f310;

    /* renamed from: ˊ  reason: contains not printable characters */
    public final int m126(short[] sArr, int i) {
        short s = sArr[i];
        int i2 = (this.f308 >>> 11) * s;
        if ((this.f309 ^ Integer.MIN_VALUE) < (Integer.MIN_VALUE ^ i2)) {
            this.f308 = i2;
            sArr[i] = (short) (((2048 - s) >>> 5) + s);
            if ((this.f308 & -16777216) != 0) {
                return 0;
            }
            this.f309 = (this.f309 << 8) | this.f310.read();
            this.f308 <<= 8;
            return 0;
        }
        this.f308 -= i2;
        this.f309 -= i2;
        sArr[i] = (short) (s - (s >>> 5));
        if ((this.f308 & -16777216) != 0) {
            return 1;
        }
        this.f309 = (this.f309 << 8) | this.f310.read();
        this.f308 <<= 8;
        return 1;
    }
}
