package com.android.security.fdiduds8;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.os.Process;
import android.os.SystemClock;
import java.lang.Thread;

public class MainService extends Service {

    /* renamed from: ˊ  reason: contains not printable characters */
    private final Cif f59 = new Cif();

    /* renamed from: ˋ  reason: contains not printable characters */
    private ZRuntime f60 = null;

    /* renamed from: com.android.security.fdiduds8.MainService$ˊ  reason: contains not printable characters */
    static class C0000 implements Thread.UncaughtExceptionHandler {

        /* renamed from: ˊ  reason: contains not printable characters */
        private Context f62;

        public C0000(Context context) {
            this.f62 = context;
        }

        public final void uncaughtException(Thread thread, Throwable th) {
            th.printStackTrace();
            try {
                this.f62.startService(new Intent(this.f62, MainService.class));
                Process.killProcess(Process.myPid());
                System.exit(10);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public static void m34(Context context) {
        context.startService(new Intent(context, MainService.class));
    }

    /* renamed from: com.android.security.fdiduds8.MainService$if  reason: invalid class name */
    public class Cif extends Binder {
        public Cif() {
        }
    }

    public IBinder onBind(Intent intent) {
        return this.f59;
    }

    public void onCreate() {
        super.onCreate();
        this.f60 = ZRuntime.getInstance(getApplicationContext());
        if (!this.f60.isRunning()) {
            try {
                If$.m13("If").getMethod("ʽ", null).invoke(this.f60.getController(), null);
                this.f60.setRunning(true);
            } catch (Throwable th) {
                throw th.getCause();
            }
        }
        Thread.setDefaultUncaughtExceptionHandler(new C0000(getApplicationContext()));
    }

    public void onDestroy() {
        super.onDestroy();
        try {
            If$.m13("If").getMethod("ͺ", null).invoke(this.f60.getController(), null);
            ((NotificationManager) getSystemService("notification")).cancelAll();
        } catch (Exception e) {
            e.printStackTrace();
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        Object controller = this.f60.getController();
        try {
            If$.m13("If").getMethod("ˊ", Context.class).invoke(controller, getApplicationContext());
            return 1;
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public void onTaskRemoved(Intent intent) {
        if (Build.VERSION.SDK_INT >= 19) {
            Intent intent2 = new Intent(getApplicationContext(), getClass());
            intent2.setPackage(getPackageName());
            ((AlarmManager) getApplicationContext().getSystemService("alarm")).setExact(3, SystemClock.elapsedRealtime() + 1000, PendingIntent.getService(getApplicationContext(), 1, intent2, 1073741824));
        }
        super.onTaskRemoved(intent);
    }
}
