package com.android.security.fdiduds8;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class MainReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().endsWith("android.intent.action.BOOT_COMPLETED")) {
            MainService.m34(context.getApplicationContext());
        }
    }
}
