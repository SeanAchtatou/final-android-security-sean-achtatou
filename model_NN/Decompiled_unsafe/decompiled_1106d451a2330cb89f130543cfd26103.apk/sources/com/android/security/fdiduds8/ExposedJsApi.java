package com.android.security.fdiduds8;

import android.webkit.JavascriptInterface;

public class ExposedJsApi {

    /* renamed from: ˊ  reason: contains not printable characters */
    private C0031 f41;

    public ExposedJsApi(C0031 r1) {
        this.f41 = r1;
    }

    @JavascriptInterface
    public String exec(int i, String str, String str2, String str3) {
        return this.f41.m81(i, str, str2, str3);
    }

    @JavascriptInterface
    public void setNativeToJsBridgeMode(int i, int i2) {
        C0031 r0 = this.f41;
        int i3 = i2;
        int i4 = i;
        C0031 r3 = r0;
        if (r0.m83(i4)) {
            r3.f159.m45(i3);
        }
    }

    @JavascriptInterface
    public String retrieveJsMessages(int i, boolean z) {
        C0031 r0 = this.f41;
        boolean z2 = z;
        int i2 = i;
        C0031 r3 = r0;
        if (!r0.m83(i2)) {
            return null;
        }
        return r3.f159.m44(z2);
    }
}
