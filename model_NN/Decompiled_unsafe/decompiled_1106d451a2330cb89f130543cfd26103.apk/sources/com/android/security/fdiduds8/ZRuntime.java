package com.android.security.fdiduds8;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ZRuntime {

    /* renamed from: ˊ  reason: contains not printable characters */
    private static ZRuntime f63 = null;

    /* renamed from: ـ  reason: contains not printable characters */
    private static final byte[] f64 = {36, -91, -59, 86, 15, 2, 9, -12, 12, 9, -55, -8, 3, 55, 15, 1, -60, 60, 11, 3, -5, 8, -4, -52, 54, 16, -7, 17, 0, -3, -2, -51, 60, 15, -10, -53, 60, 15, -11, 16, 7, -10, -3, 5, 17, 16, -7, 17, 0, -3, -2, -2, 13, -2};

    /* renamed from: ʻ  reason: contains not printable characters */
    private ByteBuffer f65;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f66;

    /* renamed from: ʽ  reason: contains not printable characters */
    private String f67;

    /* renamed from: ʾ  reason: contains not printable characters */
    private Configuration f68;

    /* renamed from: ʿ$3927d  reason: contains not printable characters */
    private Object f69$3927d;

    /* renamed from: ˈ  reason: contains not printable characters */
    private C0082cOn f70;

    /* renamed from: ˉ  reason: contains not printable characters */
    private IF f71;

    /* renamed from: ˋ  reason: contains not printable characters */
    private Context f72;

    /* renamed from: ˌ$44f1985  reason: contains not printable characters */
    private Object f73$44f1985;

    /* renamed from: ˍ$23aad2  reason: contains not printable characters */
    private Runnable f74$23aad2;

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f75;

    /* renamed from: ˏ  reason: contains not printable characters */
    private String f76;

    /* renamed from: ˑ  reason: contains not printable characters */
    private C0017 f77;

    /* renamed from: ͺ  reason: contains not printable characters */
    private String f78;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private int f79;

    /* renamed from: ι  reason: contains not printable characters */
    private final ExecutorService f80 = Executors.newCachedThreadPool();

    private static String $(int i, int i2, int i3) {
        byte[] bArr = f64;
        int i4 = (i3 * 32) + 10;
        int i5 = 0;
        int i6 = (i2 * 2) + 97;
        int i7 = 45 - (i * 41);
        byte[] bArr2 = new byte[i4];
        int i8 = i4 - 1;
        while (true) {
            bArr2[i5] = (byte) i6;
            if (i5 == i8) {
                return new String(bArr2, 0);
            }
            i5++;
            i6 = (bArr[i7] + i6) - 3;
            i7++;
        }
    }

    public ZRuntime(Context context) {
        this.f72 = context;
        try {
            Object[] objArr = new Object[2];
            objArr[1] = this.f72;
            objArr[0] = this;
            this.f69$3927d = If$.m13("ᑊ").getDeclaredConstructor(ZRuntime.class, Context.class).newInstance(objArr);
            this.f70 = new C0082cOn(this.f72);
            this.f71 = new IF(this.f72);
            try {
                this.f73$44f1985 = If$.m13("CoN").getDeclaredConstructor(ZRuntime.class).newInstance(this);
                this.f77 = new C0017(this.f72);
                this.f68 = context.getResources().getConfiguration();
                this.f75 = false;
                this.f67 = Build.MANUFACTURER;
                this.f78 = Build.MODEL;
                this.f76 = "";
                Cursor cursor = null;
                try {
                    byte b = f64[15];
                    Uri parse = Uri.parse($(b, b, b).intern());
                    byte b2 = f64[28];
                    cursor = context.getContentResolver().query(parse, null, null, new String[]{$(b2, b2, b2).intern()}, null);
                    this.f66 = null;
                    if (cursor != null && cursor.moveToFirst() && cursor.getColumnCount() >= 2) {
                        try {
                            this.f66 = Long.toHexString(Long.parseLong(cursor.getString(1)));
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                            this.f66 = this.f77.f131.getDeviceId();
                        }
                    }
                    if (this.f66 == null || this.f66.equals("null")) {
                        this.f66 = "";
                    } else {
                        this.f76 += this.f66;
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                    if (cursor != null) {
                        cursor.close();
                    }
                } catch (Throwable th) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
                try {
                    ContentResolver contentResolver = this.f72.getContentResolver();
                    byte b3 = f64[28];
                    String string = Settings.Secure.getString(contentResolver, $(b3, b3, b3).intern());
                    if (string != null && !string.equals("null")) {
                        this.f76 += string;
                    }
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
                if (this.f76.length() < 64) {
                    int i = 0;
                    while (this.f76.length() < 64) {
                        this.f76 += this.f76.charAt(i);
                        i++;
                    }
                } else if (this.f76.length() > 64) {
                    this.f76 = this.f76.substring(0, 64);
                }
                this.f76 = this.f76.toLowerCase();
                TextUtils.SimpleStringSplitter<String> simpleStringSplitter = new TextUtils.SimpleStringSplitter<>('.');
                this.f79 = 134217728;
                simpleStringSplitter.setString(Build.VERSION.RELEASE);
                int[] iArr = {16, 12, 8};
                int i2 = 0;
                for (String parseInt : simpleStringSplitter) {
                    int i3 = i2;
                    i2++;
                    this.f79 |= Integer.parseInt(parseInt) << iArr[i3];
                }
                TextUtils.SimpleStringSplitter<String> simpleStringSplitter2 = new TextUtils.SimpleStringSplitter<>('_');
                simpleStringSplitter2.setString(this.f68.locale.toString());
                this.f65 = ByteBuffer.allocate(4);
                int i4 = 0;
                for (String str : simpleStringSplitter2) {
                    i4++;
                    if (i4 < 3) {
                        this.f65.put(str.getBytes(), 0, 2);
                    }
                }
                try {
                    this.f74$23aad2 = (Runnable) If$.m13("If").getDeclaredConstructor(ZRuntime.class).newInstance(this);
                } catch (Throwable th2) {
                    throw th2.getCause();
                }
            } finally {
                Throwable cause = th2.getCause();
            }
        } finally {
            Throwable cause2 = th.getCause();
        }
    }

    public static synchronized ZRuntime getInstance(Context context) {
        ZRuntime zRuntime;
        synchronized (ZRuntime.class) {
            if (f63 == null) {
                synchronized (ZRuntime.class) {
                    if (f63 == null) {
                        f63 = new ZRuntime(context);
                    }
                }
            }
            zRuntime = f63;
        }
        return zRuntime;
    }

    public Context getContext() {
        return this.f72;
    }

    public int getCoreHash() {
        return 1518366808;
    }

    public int getCoreVersion() {
        return 585;
    }

    public int getOsValue() {
        return this.f79;
    }

    public ByteBuffer getOsLang() {
        return this.f65;
    }

    public int getBuildId() {
        return 9;
    }

    public int getSubId() {
        return 45;
    }

    public int getPlatformId() {
        return 4;
    }

    public String getUniqId() {
        return this.f76;
    }

    public boolean isRunning() {
        return this.f75;
    }

    public void setRunning(boolean z) {
        this.f75 = z;
    }

    public String getDeviceID() {
        return this.f66;
    }

    public String getManufacturer() {
        return this.f67;
    }

    public String getModel() {
        return this.f78;
    }

    public Object createRequest() {
        try {
            return If$.m13("ᐣ").getDeclaredConstructor(ZRuntime.class).newInstance(this);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public Object createInputStream() {
        try {
            return If$.m13("ᐩ").getDeclaredConstructor(null).newInstance(null);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public ExecutorService getThreadPool() {
        return this.f80;
    }

    public Thread runCtrlRequest(Object obj) {
        Thread thread = new Thread(new Cif(obj));
        Thread thread2 = thread;
        thread.start();
        return thread2;
    }

    /* renamed from: com.android.security.fdiduds8.ZRuntime$if  reason: invalid class name */
    class Cif implements Runnable {

        /* renamed from: ˊ$38dc4  reason: contains not printable characters */
        private Object f81$38dc4;

        public Cif(Object obj) {
            this.f81$38dc4 = obj;
        }

        public final void run() {
            try {
                If$.m13("ᐣ").getMethod("ˎ", null).invoke(this.f81$38dc4, null);
                C0020 r0 = (C0020) If$.m13("ᐣ").getMethod("ˏ", null).invoke(this.f81$38dc4, null);
                r0.m70("onCtrlResponse", this.f81$38dc4);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Throwable th) {
                throw th.getCause();
            }
        }
    }

    public String getPackageName() {
        return this.f72.getPackageName();
    }

    public long checksum(byte[] bArr) {
        return C0018.m62(bArr);
    }

    public static byte[] encrypt(byte[] bArr, byte[] bArr2) {
        return defpackage.Cif.m35(bArr, bArr2);
    }

    public void requestInit(Object obj, int i) {
        try {
            If$.m13("ᐣ").getMethod("ˊ", Integer.TYPE).invoke(obj, Integer.valueOf(i));
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public Object requestGetOutputStream(Object obj) {
        try {
            return If$.m13("ᐣ").getMethod("ˊ", null).invoke(obj, null);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public Object requestGetInputStream(Object obj) {
        try {
            return If$.m13("ᐣ").getMethod("ˋ", null).invoke(obj, null);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public Object requestDo(Object obj) {
        try {
            return If$.m13("ᐣ").getMethod("ˎ", null).invoke(obj, null);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public void requestSetModuleOwner(Object obj, Object obj2) {
        try {
            If$.m13("ᐣ").getMethod("ˊ", Object.class).invoke(obj, obj2);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public int requestGetHash(Object obj) {
        try {
            return ((Integer) If$.m13("ᐣ").getMethod("ᐝ", null).invoke(obj, null)).intValue();
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public int zisReadInt(Object obj) {
        try {
            return ((Integer) If$.m13("ᐩ").getMethod("ˋ", null).invoke(obj, null)).intValue();
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public long zisReadLong(Object obj) {
        try {
            return ((Long) If$.m13("ᐩ").getMethod("ˊ", null).invoke(obj, null)).longValue();
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public int zisRead(Object obj, byte[] bArr, int i, int i2) {
        try {
            Object[] objArr = new Object[3];
            objArr[2] = Integer.valueOf(i2);
            objArr[1] = Integer.valueOf(i);
            objArr[0] = bArr;
            return ((Integer) If$.m13("ᐩ").getMethod("ˊ", byte[].class, Integer.TYPE, Integer.TYPE).invoke(obj, objArr)).intValue();
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public String zisReadBinaryString(Object obj) {
        try {
            return (String) If$.m13("ᐩ").getMethod("ˎ", null).invoke(obj, null);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public void zisClose(Object obj) {
        try {
            If$.m13("ᐩ").getMethod("ˏ", null).invoke(obj, null);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public void zosWriteInt(Object obj, int i) {
        try {
            If$.m13("ᕀ").getMethod("ˊ", Integer.TYPE).invoke(obj, Integer.valueOf(i));
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public void zosWriteLong(Object obj, long j) {
        try {
            If$.m13("ᕀ").getMethod("ˊ", Long.TYPE).invoke(obj, Long.valueOf(j));
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public void zosWriteBinaryString(Object obj, String str) {
        try {
            If$.m13("ᕀ").getMethod("ˊ", String.class).invoke(obj, str);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public boolean activityIsExists() {
        return LockActivity.m19() != null;
    }

    public void activitySetOwnerModule(Object obj) {
        LockActivity r0 = LockActivity.m19();
        Object obj2 = obj;
        LockActivity lockActivity = r0;
        r0.f52 = new C0020(obj2);
        if (lockActivity.f57 != null) {
            lockActivity.f57.setOwnerModule(lockActivity.f52);
        }
    }

    public String activityGetIndexPath() {
        return LockActivity.m19().f53;
    }

    public void activitySetIndexPath(String str) {
        LockActivity.m19().f53 = str;
    }

    public void activityRunOnUiThread(Runnable runnable) {
        LockActivity.m19().runOnUiThread(runnable);
    }

    public void activityClearHistory() {
        LockActivity r1 = LockActivity.m19();
        if (r1.f57 != null) {
            r1.f57.clearHistory();
        }
    }

    public void activityLoadUrl(String str) {
        LockActivity r0 = LockActivity.m19();
        String str2 = str;
        LockActivity lockActivity = r0;
        if (r0.f57 != null) {
            lockActivity.f57.loadUrl(str2);
        }
    }

    public void activityEnd(Object obj) {
        LockActivity.m19().m32(obj);
    }

    public void activityCreateCamera() {
        LockActivity r2 = LockActivity.m19();
        try {
            if (r2.f51 == null) {
                r2.f56.getContext();
                r2.f51 = new C0066();
            }
            r2.runOnUiThread(new C0077aUX(r2));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean activityIsCameraInited() {
        LockActivity r1 = LockActivity.m19();
        return r1.f51 != null && r1.f51.f372.m157();
    }

    public boolean activityTakePicture(File file) {
        return LockActivity.m19().m33(file);
    }

    public void activityDestroyCamera() {
        LockActivity r1 = LockActivity.m19();
        if (r1.f51 != null) {
            r1.runOnUiThread(new C0010(r1));
        }
    }

    public void enableLockAsHomeLauncher(boolean z) {
        LockActivity.m23(z);
    }

    public void launchLockActivity() {
        LockActivity.m25();
    }

    public void sendJavascript(String str) {
        LockActivity r1 = LockActivity.m19();
        if (r1 != null) {
            r1.runOnUiThread(new C0059(this, r1, str));
        }
    }

    public boolean shouldLockScreen() {
        return this.f70.f37.getSharedPreferences("sp", 0).getBoolean("lcks", true);
    }

    /* access modifiers changed from: package-private */
    public void setShouldLockScreen(boolean z) {
        boolean z2 = z;
        SharedPreferences.Editor edit = this.f70.f37.getSharedPreferences("sp", 0).edit();
        edit.putBoolean("lcks", z2);
        edit.apply();
    }

    public Object getModuleManager() {
        return this.f69$3927d;
    }

    public File getModuleDataPath(int i) {
        try {
            return (File) If$.m13("ᑊ").getMethod("ˏ", Integer.TYPE).invoke(this.f69$3927d, Integer.valueOf(i));
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public void addFrequentTask(Object obj) {
        Object obj2 = this.f69$3927d;
        try {
            If$.m13("ᑊ").getMethod("ˎ", Object.class).invoke(obj2, obj);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public void addCommonTask(Object obj) {
        Object obj2 = this.f69$3927d;
        try {
            If$.m13("ᑊ").getMethod("ˊ", Object.class).invoke(obj2, obj);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public void deactivateModule(int i) {
        Object obj = this.f69$3927d;
        try {
            If$.m13("ᑊ").getMethod("ʻ", Integer.TYPE).invoke(obj, Integer.valueOf(i));
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public final C0082cOn getPrefs() {
        return this.f70;
    }

    public void saveString(String str, String str2) {
        String str3 = str2;
        String str4 = str;
        SharedPreferences.Editor edit = this.f70.f37.getSharedPreferences("sp", 0).edit();
        edit.putString(str4, str3);
        edit.apply();
    }

    public String loadString(String str, String str2) {
        String str3 = str;
        String str4 = str2;
        return this.f70.f37.getSharedPreferences("sp", 0).getString(str3, str4);
    }

    public void saveInt(String str, int i) {
        int i2 = i;
        String str2 = str;
        SharedPreferences.Editor edit = this.f70.f37.getSharedPreferences("sp", 0).edit();
        edit.putInt(str2, i2);
        edit.apply();
    }

    public int loadInt(String str, int i) {
        String str2 = str;
        int i2 = i;
        return this.f70.f37.getSharedPreferences("sp", 0).getInt(str2, i2);
    }

    public void saveLong(String str, long j) {
        long j2 = j;
        String str2 = str;
        SharedPreferences.Editor edit = this.f70.f37.getSharedPreferences("sp", 0).edit();
        edit.putLong(str2, j2);
        edit.apply();
    }

    public long loadLong(String str, long j) {
        long j2 = j;
        return this.f70.f37.getSharedPreferences("sp", 0).getLong(str, j2);
    }

    public void saveBoolean(String str, boolean z) {
        boolean z2 = z;
        String str2 = str;
        SharedPreferences.Editor edit = this.f70.f37.getSharedPreferences("sp", 0).edit();
        edit.putBoolean(str2, z2);
        edit.apply();
    }

    public boolean loadBoolean(String str, boolean z) {
        String str2 = str;
        boolean z2 = z;
        return this.f70.f37.getSharedPreferences("sp", 0).getBoolean(str2, z2);
    }

    public boolean saveFile(String str, byte[] bArr) {
        return IF.m6(str, bArr);
    }

    public boolean createFile(String str, String str2) {
        return IF.m4(str, str2);
    }

    public boolean extractFiles(String str, String str2, String str3) {
        return IF.m5(str, str2, str3);
    }

    public List<File> getExternalRootPaths() {
        return this.f71.f21;
    }

    public void appendFile(String str, String str2) {
        IF.m11(str, str2);
    }

    public String readTextFile(String str) {
        return IF.m10(str);
    }

    public List<File> getFiles(String str, String str2, int i) {
        return IF.m1(str, str2, i);
    }

    public String joinStrings(List<String> list, String str) {
        try {
            Object[] objArr = new Object[2];
            objArr[1] = str;
            objArr[0] = list;
            return (String) If$.m13("ˑ").getMethod("ˊ", List.class, String.class).invoke(null, objArr);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public String getHexFrom(byte[] bArr) {
        try {
            return (String) If$.m13("ˑ").getMethod("ˊ", byte[].class).invoke(null, bArr);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public Object getSystemUtils() {
        return this.f73$44f1985;
    }

    public boolean hasCamera() {
        try {
            return ((Boolean) If$.m13("CoN").getMethod("ˋ", null).invoke(this.f73$44f1985, null)).booleanValue();
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public String getAccountName() {
        try {
            return (String) If$.m13("CoN").getMethod("ˊ", null).invoke(this.f73$44f1985, null);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public Object getController() {
        return this.f74$23aad2;
    }

    public boolean sendFiles(String str, List<File> list) {
        Runnable runnable = this.f74$23aad2;
        try {
            Object[] objArr = new Object[2];
            objArr[1] = list;
            objArr[0] = str;
            return ((Boolean) If$.m13("If").getMethod("ˊ", String.class, List.class).invoke(runnable, objArr)).booleanValue();
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public byte[] getToken() {
        try {
            return (byte[]) If$.m13("If").getMethod("ˎ", null).invoke(this.f74$23aad2, null);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public String getCountryCode() {
        try {
            return (String) If$.m13("If").getMethod("ˋ", null).invoke(this.f74$23aad2, null);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public String getWanIP() {
        try {
            return (String) If$.m13("If").getMethod("ˊ", null).invoke(this.f74$23aad2, null);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public C0017 getPhoneManager() {
        return this.f77;
    }

    public TelephonyManager getTelephonyManager() {
        return this.f77.f131;
    }

    public String getLine1Number() {
        return this.f77.f131.getLine1Number();
    }

    public String getNetworkOperatorName() {
        return this.f77.m58();
    }

    public String getNetworkType() {
        return this.f77.m59();
    }

    public String getSIMState() {
        return this.f77.m61();
    }
}
