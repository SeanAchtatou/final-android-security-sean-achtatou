import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: ﹶ  reason: contains not printable characters */
public final class C0065 extends WebViewClient {

    /* renamed from: ˊ  reason: contains not printable characters */
    boolean f369;

    /* renamed from: ˋ  reason: contains not printable characters */
    private C0050 f370;

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f371 = false;

    public C0065(C0050 r2) {
        this.f370 = r2;
    }

    public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
        super.onPageStarted(webView, str, bitmap);
        this.f369 = true;
        C0031 r2 = this.f370.f317;
        C0011 r4 = r2.f159;
        C0011 r3 = r4;
        synchronized (r4) {
            r3.f109.clear();
            r3.m45(-1);
        }
        r2.f160 = -1;
    }

    public final void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        if (this.f369 || str.startsWith("about:")) {
            this.f369 = false;
            this.f370.f316++;
            str.equals("about:blank");
        }
    }

    public final void onReceivedError(WebView webView, int i, String str, String str2) {
        if (this.f369) {
            this.f370.f316++;
            if (i == -10) {
                if (webView.canGoBack()) {
                    webView.goBack();
                    return;
                }
                super.onReceivedError(webView, i, str, str2);
            }
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("errorCode", i);
                jSONObject.put("description", str);
                jSONObject.put("url", str2);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
