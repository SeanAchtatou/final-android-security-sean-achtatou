import java.io.ByteArrayOutputStream;

/* renamed from: ᴶ  reason: contains not printable characters */
public final class C0051 {

    /* renamed from: ʼ  reason: contains not printable characters */
    private static int[] f318 = new int[512];

    /* renamed from: ʻ  reason: contains not printable characters */
    public long f319;

    /* renamed from: ˊ  reason: contains not printable characters */
    public ByteArrayOutputStream f320;

    /* renamed from: ˋ  reason: contains not printable characters */
    public long f321;

    /* renamed from: ˎ  reason: contains not printable characters */
    public int f322;

    /* renamed from: ˏ  reason: contains not printable characters */
    public int f323;

    /* renamed from: ᐝ  reason: contains not printable characters */
    public int f324;

    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m135() {
        int i;
        int i2 = (int) (this.f321 >>> 32);
        int i3 = i2;
        if (i2 != 0 || this.f321 < 4278190080L) {
            this.f319 += (long) this.f323;
            int i4 = this.f324;
            do {
                this.f320.write(i4 + i3);
                i4 = 255;
                i = this.f323 - 1;
                this.f323 = i;
            } while (i != 0);
            this.f324 = ((int) this.f321) >>> 24;
        }
        this.f323++;
        this.f321 = (this.f321 & 16777215) << 8;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m136(int i, int i2) {
        for (int i3 = i2 - 1; i3 >= 0; i3--) {
            this.f322 >>>= 1;
            if (((i >>> i3) & 1) == 1) {
                this.f321 += (long) this.f322;
            }
            if ((this.f322 & -16777216) == 0) {
                this.f322 <<= 8;
                m135();
            }
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public static void m132(short[] sArr) {
        for (int i = 0; i < sArr.length; i++) {
            sArr[i] = 1024;
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m137(short[] sArr, int i, int i2) {
        short s = sArr[i];
        int i3 = (this.f322 >>> 11) * s;
        if (i2 == 0) {
            this.f322 = i3;
            sArr[i] = (short) (((2048 - s) >>> 5) + s);
        } else {
            this.f321 += ((long) i3) & 4294967295L;
            this.f322 -= i3;
            sArr[i] = (short) (s - (s >>> 5));
        }
        if ((this.f322 & -16777216) == 0) {
            this.f322 <<= 8;
            m135();
        }
    }

    static {
        for (int i = 8; i >= 0; i--) {
            int i2 = 1 << (9 - i);
            for (int i3 = 1 << ((9 - i) - 1); i3 < i2; i3++) {
                f318[i3] = (i << 6) + (((i2 - i3) << 6) >>> ((9 - i) - 1));
            }
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public static int m134(int i, int i2) {
        return f318[(((i - i2) ^ (-i2)) & 2047) >>> 2];
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public static int m131(int i) {
        return f318[i >>> 2];
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public static int m133(int i) {
        return f318[(2048 - i) >>> 2];
    }
}
