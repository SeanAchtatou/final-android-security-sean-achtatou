import android.content.DialogInterface;
import android.view.KeyEvent;
import android.webkit.JsResult;

/* renamed from: ᐨ  reason: contains not printable characters */
class C0041 implements DialogInterface.OnKeyListener {

    /* renamed from: ˊ  reason: contains not printable characters */
    private /* synthetic */ JsResult f189;

    /* renamed from: ˋ  reason: contains not printable characters */
    private /* synthetic */ C0081cON f190;

    C0041(C0081cON con, JsResult jsResult) {
        this.f190 = con;
        this.f189 = jsResult;
    }

    public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (i != 4) {
            return true;
        }
        this.f189.confirm();
        return false;
    }
}
