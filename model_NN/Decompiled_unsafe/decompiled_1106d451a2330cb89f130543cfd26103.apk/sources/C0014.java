import C0066;
import android.graphics.SurfaceTexture;
import android.view.TextureView;

/* renamed from: ˆ  reason: contains not printable characters */
class C0014 implements TextureView.SurfaceTextureListener {

    /* renamed from: ˊ  reason: contains not printable characters */
    private /* synthetic */ C0066.C0067 f126;

    C0014(C0066.C0067 r1) {
        this.f126 = r1;
    }

    public final void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        this.f126.m172(i, i2);
    }

    public final void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
        this.f126.m177(i, i2);
    }

    public final boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        return true;
    }

    public final void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }
}
