package defpackage;

import java.util.Comparator;

/* renamed from: ᑊ$if  reason: invalid class name and default package */
class $if implements Comparator<C0020> {

    /* renamed from: ˊ$3927d  reason: contains not printable characters */
    private /* synthetic */ Object f221$3927d;

    private $if(Object obj) {
        this.f221$3927d = obj;
    }

    public /* synthetic */ $if(Object obj, byte b) {
        this(obj);
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        C0020 r7 = (C0020) obj2;
        try {
            try {
                return Integer.valueOf(((Integer) If$.m13("ᑊ").getMethod("ˊ", Integer.TYPE).invoke(this.f221$3927d, Integer.valueOf(((Integer) ((C0020) obj).m70("getHash", new Object[0]).f135).intValue()))).intValue()).compareTo(Integer.valueOf(((Integer) If$.m13("ᑊ").getMethod("ˊ", Integer.TYPE).invoke(this.f221$3927d, Integer.valueOf(((Integer) r7.m70("getHash", new Object[0]).f135).intValue()))).intValue()));
            } catch (Throwable th) {
                throw th.getCause();
            }
        } finally {
            Throwable cause = th.getCause();
        }
    }
}
