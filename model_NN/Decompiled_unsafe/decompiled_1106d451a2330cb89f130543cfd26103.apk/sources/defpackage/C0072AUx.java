package defpackage;

import com.android.security.fdiduds8.LockActivity;

/* renamed from: AUx  reason: default package and case insensitive filesystem */
public class C0072AUx implements Runnable {

    /* renamed from: ˊ  reason: contains not printable characters */
    private /* synthetic */ LockActivity f12;

    /* renamed from: ˋ  reason: contains not printable characters */
    private /* synthetic */ String f13;

    /* renamed from: ˎ  reason: contains not printable characters */
    private /* synthetic */ LockActivity f14;

    public C0072AUx(LockActivity lockActivity, LockActivity lockActivity2, String str) {
        this.f14 = lockActivity;
        this.f12 = lockActivity2;
        this.f13 = str;
    }

    public final void run() {
        LockActivity lockActivity = this.f12;
        if (lockActivity.f57 != null) {
            lockActivity.f57.clearHistory();
        }
        if ((this.f14.f52 == null || this.f14.f52.f135 == null) && !this.f13.equals("")) {
            LockActivity lockActivity2 = this.f12;
            String str = this.f13;
            if (lockActivity2.f57 != null) {
                lockActivity2.f57.loadUrl(str);
            }
            String unused = this.f14.f53 = this.f13;
            return;
        }
        LockActivity lockActivity3 = this.f12;
        String r3 = this.f14.f53;
        if (lockActivity3.f57 != null) {
            lockActivity3.f57.loadUrl(r3);
        }
    }
}
