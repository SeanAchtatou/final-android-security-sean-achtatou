package defpackage;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.GeolocationPermissions;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.android.security.fdiduds8.LockActivity;

/* renamed from: cON  reason: default package and case insensitive filesystem */
public final class C0081cON extends WebChromeClient {

    /* renamed from: ˊ  reason: contains not printable characters */
    public AlertDialog f34;

    /* renamed from: ˋ  reason: contains not printable characters */
    private C0050 f35;

    /* renamed from: ˎ  reason: contains not printable characters */
    private LinearLayout f36;

    public C0081cON(C0050 r1) {
        this.f35 = r1;
    }

    public final boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
        AlertDialog.Builder builder = new AlertDialog.Builder(LockActivity.m19());
        builder.setMessage(str2);
        builder.setTitle("Alert");
        builder.setCancelable(true);
        builder.setPositiveButton(17039370, new C0040(this, jsResult));
        builder.setOnCancelListener(new CON(this, jsResult));
        builder.setOnKeyListener(new C0041(this, jsResult));
        this.f34 = builder.show();
        return true;
    }

    public final boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
        AlertDialog.Builder builder = new AlertDialog.Builder(LockActivity.m19());
        builder.setMessage(str2);
        builder.setTitle("Confirm");
        builder.setCancelable(true);
        builder.setPositiveButton(17039370, new C0064(this, jsResult));
        builder.setNegativeButton(17039360, new C0070(this, jsResult));
        builder.setOnCancelListener(new C0006(this, jsResult));
        builder.setOnKeyListener(new C0029(this, jsResult));
        this.f34 = builder.show();
        return true;
    }

    public final boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
        String r3 = this.f35.f317.m82(str, str2, str3);
        if (r3 != null) {
            jsPromptResult.confirm(r3);
            return true;
        }
        JsPromptResult jsPromptResult2 = jsPromptResult;
        AlertDialog.Builder builder = new AlertDialog.Builder(LockActivity.m19());
        builder.setMessage(str2);
        EditText editText = new EditText(LockActivity.m19());
        if (str3 != null) {
            editText.setText(str3);
        }
        builder.setView(editText);
        builder.setCancelable(false);
        builder.setPositiveButton(17039370, new C0030(this, editText, jsPromptResult2));
        builder.setNegativeButton(17039360, new C0032(this, jsPromptResult2));
        this.f34 = builder.show();
        return true;
    }

    public final void onExceededDatabaseQuota(String str, String str2, long j, long j2, long j3, WebStorage.QuotaUpdater quotaUpdater) {
        if (quotaUpdater != null) {
            quotaUpdater.updateQuota(104857600);
        }
    }

    @TargetApi(8)
    public final boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        return super.onConsoleMessage(consoleMessage);
    }

    public final void onGeolocationPermissionsShowPrompt(String str, GeolocationPermissions.Callback callback) {
        super.onGeolocationPermissionsShowPrompt(str, callback);
        callback.invoke(str, true, false);
    }

    public final View getVideoLoadingProgressView() {
        if (this.f36 == null) {
            LinearLayout linearLayout = new LinearLayout(this.f35.getContext());
            linearLayout.setOrientation(1);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(13);
            linearLayout.setLayoutParams(layoutParams);
            ProgressBar progressBar = new ProgressBar(this.f35.getContext());
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-2, -2);
            layoutParams2.gravity = 17;
            progressBar.setLayoutParams(layoutParams2);
            linearLayout.addView(progressBar);
            this.f36 = linearLayout;
        }
        return this.f36;
    }
}
