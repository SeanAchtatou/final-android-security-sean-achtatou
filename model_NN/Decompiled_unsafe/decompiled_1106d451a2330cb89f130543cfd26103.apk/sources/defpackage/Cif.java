package defpackage;

import android.annotation.TargetApi;

@TargetApi(17)
/* renamed from: if  reason: invalid class name and default package */
public final class Cif {

    /* renamed from: ˊ  reason: contains not printable characters */
    final String f85;

    /* renamed from: ˋ  reason: contains not printable characters */
    final AUX f86;

    public Cif() {
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public static byte[] m35(byte[] bArr, byte[] bArr2) {
        byte[] bArr3 = new byte[256];
        byte[] bArr4 = new byte[256];
        int length = bArr2.length;
        for (int i = 0; i < 256; i++) {
            bArr3[i] = (byte) i;
            bArr4[i] = bArr2[i % length];
        }
        byte b = 0;
        for (int i2 = 0; i2 < 256; i2++) {
            b = (bArr3[i2] + b + bArr4[i2]) & 255;
            byte b2 = bArr3[i2];
            bArr3[i2] = bArr3[b];
            bArr3[b] = b2;
        }
        byte[] bArr5 = new byte[bArr.length];
        int i3 = 0;
        byte b3 = 0;
        for (int i4 = 0; i4 < bArr.length; i4++) {
            i3 = (i3 + 1) & 255;
            b3 = (bArr3[i3] + b3) & 255;
            byte b4 = bArr3[i3];
            bArr3[i3] = bArr3[b3];
            bArr3[b3] = b4;
            bArr5[i4] = (byte) (bArr[i4] ^ bArr3[(bArr3[i3] + bArr3[b3]) & 255]);
        }
        return bArr5;
    }

    Cif(String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f85 = str;
        this.f86 = null;
    }

    Cif(AUX aux, String str) {
        if (str == null) {
            throw new NullPointerException();
        }
        this.f85 = str;
        this.f86 = aux;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final int m36() {
        if (this.f86 == null) {
            return this.f85.length() + 1;
        }
        return String.valueOf(this.f86.f1).length() + 2 + 1 + this.f85.length() + 1 + this.f86.f2.length() + 1;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0046  */
    /* renamed from: ˊ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m37(java.lang.StringBuilder r5) {
        /*
            r4 = this;
            AUX r0 = r4.f86
            if (r0 != 0) goto L_0x000a
            java.lang.String r0 = r4.f85
            r5.append(r0)
            return
        L_0x000a:
            AUX r0 = r4.f86
            int r0 = r0.f1
            r2 = r0
            int r1 = defpackage.AUX.Cif.f8
            r1 = 1
            if (r0 == r1) goto L_0x0018
            int r0 = defpackage.AUX.Cif.f7
            if (r2 != 0) goto L_0x001a
        L_0x0018:
            r3 = 1
            goto L_0x001b
        L_0x001a:
            r3 = 0
        L_0x001b:
            java.lang.String r0 = "cordova.callbackFromNative('"
            java.lang.StringBuilder r0 = r5.append(r0)
            java.lang.String r1 = r4.f85
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r1 = "',"
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r1 = ","
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.StringBuilder r0 = r0.append(r2)
            java.lang.String r1 = ",["
            r0.append(r1)
            AUX r2 = r4.f86
            java.lang.String r0 = r2.f3
            if (r0 != 0) goto L_0x004e
            java.lang.String r0 = r2.f2
            java.lang.String r0 = org.json.JSONObject.quote(r0)
            r2.f3 = r0
        L_0x004e:
            java.lang.String r0 = r2.f3
            r5.append(r0)
            java.lang.String r0 = "],false);"
            r5.append(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: defpackage.Cif.m37(java.lang.StringBuilder):void");
    }
}
