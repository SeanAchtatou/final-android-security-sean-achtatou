package defpackage;

import defpackage.IF;
import java.io.FileInputStream;
import java.util.Iterator;
import java.util.LinkedList;

/* renamed from: aUx  reason: default package and case insensitive filesystem */
class C0078aUx extends C0080aux {

    /* renamed from: ˋ  reason: contains not printable characters */
    private /* synthetic */ FileInputStream f30;

    C0078aUx(FileInputStream fileInputStream) {
        this.f30 = fileInputStream;
    }

    public final void run() {
        LinkedList linkedList = new LinkedList();
        int i = 0;
        int i2 = 0;
        do {
            try {
                byte[] bArr = new byte[8192];
                int read = this.f30.read(bArr, 0, 8192);
                i = read;
                if (read > 0) {
                    i2 += i;
                    linkedList.add(new IF.Cif(bArr, Integer.valueOf(i)));
                    continue;
                } else {
                    continue;
                }
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
        } while (i > 0);
        try {
            this.f30.close();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        this.f33 = new byte[i2];
        int i3 = 0;
        Iterator it = linkedList.iterator();
        while (it.hasNext()) {
            IF.Cif ifVar = (IF.Cif) it.next();
            System.arraycopy(ifVar.f24, 0, this.f33, i3, ((Integer) ifVar.f25).intValue());
            i3 += ((Integer) ifVar.f25).intValue();
        }
    }
}
