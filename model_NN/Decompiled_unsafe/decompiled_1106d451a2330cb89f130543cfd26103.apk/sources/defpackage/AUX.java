package defpackage;

/* renamed from: AUX  reason: default package */
public final class AUX {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static String[] f0 = {"No result", "OK", "Class not found", "Illegal access", "Instantiation error", "Malformed url", "IO error", "Invalid action", "JSON error", "Error"};

    /* renamed from: ˊ  reason: contains not printable characters */
    final int f1;

    /* renamed from: ˋ  reason: contains not printable characters */
    String f2;

    /* renamed from: ˎ  reason: contains not printable characters */
    String f3;

    /* renamed from: ˏ  reason: contains not printable characters */
    private final int f4;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private boolean f5;

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    /* renamed from: AUX$if  reason: invalid class name */
    public static final class Cif extends Enum<Cif> {

        /* renamed from: ʻ  reason: contains not printable characters */
        public static final int f6 = 10;

        /* renamed from: ˊ  reason: contains not printable characters */
        public static final int f7 = 1;

        /* renamed from: ˋ  reason: contains not printable characters */
        public static final int f8 = 2;

        /* renamed from: ˎ  reason: contains not printable characters */
        public static final int f9 = 3;

        /* renamed from: ˏ  reason: contains not printable characters */
        public static final int f10 = 8;

        /* renamed from: ᐝ  reason: contains not printable characters */
        public static final int f11 = 9;
    }

    public AUX(int i) {
        this(i, f0[i - 1]);
    }

    public AUX(int i, String str) {
        this.f5 = false;
        this.f1 = i - 1;
        this.f4 = 1;
        this.f2 = str;
    }
}
