package defpackage;

import java.util.Comparator;

/* renamed from: If$if  reason: default package */
class If$if implements Comparator<Long> {
    public final /* synthetic */ int compare(Object obj, Object obj2) {
        return Long.signum(((Long) obj2).longValue() - ((Long) obj).longValue());
    }
}
