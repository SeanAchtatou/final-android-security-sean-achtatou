package defpackage;

import android.content.DialogInterface;
import android.webkit.JsResult;

/* renamed from: CON  reason: default package */
class CON implements DialogInterface.OnCancelListener {

    /* renamed from: ˊ  reason: contains not printable characters */
    private /* synthetic */ JsResult f17;

    /* renamed from: ˋ  reason: contains not printable characters */
    private /* synthetic */ C0081cON f18;

    CON(C0081cON con, JsResult jsResult) {
        this.f18 = con;
        this.f17 = jsResult;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.f17.cancel();
    }
}
