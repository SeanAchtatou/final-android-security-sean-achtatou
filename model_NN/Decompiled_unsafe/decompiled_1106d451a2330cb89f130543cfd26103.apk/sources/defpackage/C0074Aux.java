package defpackage;

import java.io.File;
import java.util.Comparator;

/* renamed from: Aux  reason: default package and case insensitive filesystem */
class C0074Aux implements Comparator<File> {
    C0074Aux() {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        return (int) (((File) obj2).lastModified() - ((File) obj).lastModified());
    }
}
