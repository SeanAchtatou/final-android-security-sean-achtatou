package defpackage;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.os.storage.StorageManager;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/* renamed from: IF  reason: default package */
public final class IF {

    /* renamed from: ˏ  reason: contains not printable characters */
    private static final byte[] f20 = {115, -75, -115, 114, 0, 17, -28, 27, -1, 11, -6, -6, -19, 19, 21, -10, 13};

    /* renamed from: ˊ  reason: contains not printable characters */
    public List<File> f21 = new ArrayList();

    /* renamed from: ˋ  reason: contains not printable characters */
    private File f22;

    /* renamed from: ˎ  reason: contains not printable characters */
    private final Context f23;

    /* renamed from: ˊ  reason: contains not printable characters */
    private static String m0(int i, int i2, int i3) {
        int i4 = 0;
        byte[] bArr = f20;
        int i5 = (i * 3) + 103;
        int i6 = 14 - (i3 * 2);
        int i7 = i2 + 4;
        byte[] bArr2 = new byte[i6];
        int i8 = i6 - 1;
        while (true) {
            bArr2[i4] = (byte) i5;
            int i9 = i4;
            i4++;
            if (i9 == i8) {
                return new String(bArr2, 0);
            }
            i7++;
            i5 = (bArr[i7] + i5) - 2;
        }
    }

    /* JADX WARN: Type inference failed for: r0v15, types: [java.util.List<java.io.File>, java.util.ArrayList] */
    public IF(Context context) {
        this.f23 = context;
        try {
            if (Build.VERSION.SDK_INT >= 14) {
                StorageManager storageManager = (StorageManager) this.f23.getSystemService("storage");
                for (String file : (String[]) storageManager.getClass().getMethod(m0(f20[4], f20[8], f20[4]).intern(), new Class[0]).invoke(storageManager, new Object[0])) {
                    File file2 = new File(file);
                    if (!m7((List<File>) this.f21, file2)) {
                        this.f21.add(file2);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (this.f21.size() == 0) {
            this.f21.add(new File(Environment.getExternalStorageDirectory().getAbsolutePath()));
        }
        this.f22 = new File(Environment.getRootDirectory().getAbsolutePath());
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public static void m2(FilterInputStream filterInputStream, OutputStream outputStream) {
        byte[] bArr = new byte[2048];
        while (true) {
            int read = filterInputStream.read(bArr);
            int i = read;
            if (read != -1) {
                outputStream.write(bArr, 0, i);
            } else {
                return;
            }
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public static boolean m5(String str, String str2, String str3) {
        try {
            ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(str));
            while (true) {
                ZipEntry nextEntry = zipInputStream.getNextEntry();
                if (nextEntry == null) {
                    return true;
                }
                if (nextEntry.getName().startsWith(str2)) {
                    if (nextEntry.isDirectory()) {
                        File file = new File(str3 + "/" + nextEntry.getName());
                        File file2 = file;
                        if (!file.isDirectory()) {
                            file2.mkdirs();
                        }
                    } else {
                        File file3 = new File(str3, nextEntry.getName());
                        File file4 = file3;
                        if (file3.exists()) {
                            file4.delete();
                        }
                        FileOutputStream fileOutputStream = new FileOutputStream(file4.getAbsolutePath());
                        m2(zipInputStream, fileOutputStream);
                        zipInputStream.closeEntry();
                        fileOutputStream.flush();
                        fileOutputStream.close();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public static boolean m6(String str, byte[] bArr) {
        int length = bArr.length;
        int i = 0;
        try {
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(str));
            while (length > 0) {
                int min = Math.min(1024, length);
                bufferedOutputStream.write(bArr, i, min);
                length -= min;
                i += min;
            }
            bufferedOutputStream.flush();
            bufferedOutputStream.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static boolean m7(List<File> list, File file) {
        if (list == null || !file.exists() || !file.isDirectory() || !file.canRead() || file.getTotalSpace() <= 0 || file.getName().equalsIgnoreCase("tmp") || list.contains(file)) {
            return true;
        }
        for (File next : list) {
            if (next.getFreeSpace() == file.getFreeSpace() && next.getUsableSpace() == file.getUsableSpace()) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public static boolean m3(File file) {
        if (file.exists()) {
            File[] listFiles = file.listFiles();
            if (listFiles == null) {
                return true;
            }
            for (File file2 : listFiles) {
                if (file2.isDirectory()) {
                    m3(file2);
                } else {
                    file2.delete();
                }
            }
        }
        return file.delete();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public static boolean m4(String str, String str2) {
        return m12(str, str2.getBytes());
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private static boolean m12(String str, byte[] bArr) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(new File(str));
            FileOutputStream fileOutputStream2 = fileOutputStream;
            fileOutputStream.write(bArr);
            fileOutputStream2.flush();
            fileOutputStream2.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("");
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public static byte[] m9(String str) {
        try {
            return m8(new FileInputStream(new File(str)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("");
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public static String m10(String str) {
        return new String(m9(str));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* renamed from: ˋ  reason: contains not printable characters */
    public static void m11(String str, String str2) {
        String str3 = str;
        byte[] bytes = str2.getBytes();
        String str4 = str3;
        File file = new File(str3);
        if (!(file.exists() && file.isFile())) {
            throw new RuntimeException("");
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(new File(str4), true);
            FileOutputStream fileOutputStream2 = fileOutputStream;
            fileOutputStream.write(bytes);
            fileOutputStream2.flush();
            fileOutputStream2.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("");
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public static List<File> m1(String str, String str2, int i) {
        Object obj;
        File file = new File(str);
        List<File> list = null;
        if (str2 != null) {
            File[] listFiles = file.listFiles(new C0023(str2));
            if (listFiles != null) {
                list = Arrays.asList(listFiles);
            }
        } else {
            File[] listFiles2 = file.listFiles();
            if (listFiles2 != null) {
                list = Arrays.asList(listFiles2);
            }
        }
        if (i != -1) {
            switch (i) {
                case 0:
                    obj = new C0036();
                    break;
                case 1:
                    obj = new C0074Aux();
                    break;
                case 2:
                    obj = new C0007();
                    break;
                default:
                    obj = null;
                    break;
            }
            Collections.sort(list, obj);
        }
        return list;
    }

    /* renamed from: IF$if  reason: invalid class name */
    static class Cif<T, S> implements Serializable {

        /* renamed from: ˊ  reason: contains not printable characters */
        public final T f24;

        /* renamed from: ˋ  reason: contains not printable characters */
        public final S f25;

        public Cif() {
            this.f24 = null;
            this.f25 = null;
        }

        public Cif(T t, S s) {
            this.f24 = t;
            this.f25 = s;
        }

        public final boolean equals(Object obj) {
            if (!(obj instanceof Cif)) {
                return false;
            }
            return this.f24.equals(((Cif) obj).f24) && this.f25.equals(((Cif) obj).f25);
        }

        public final int hashCode() {
            return this.f24.hashCode() << (this.f25.hashCode() + 16);
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static byte[] m8(FileInputStream fileInputStream) {
        C0078aUx aux = new C0078aUx(fileInputStream);
        C0078aUx aux2 = aux;
        aux.start();
        try {
            aux2.join();
            return aux2.f33;
        } catch (InterruptedException e) {
            e.printStackTrace();
            throw new RuntimeException("");
        }
    }
}
