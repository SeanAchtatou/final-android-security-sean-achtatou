package defpackage;

import android.os.Build;
import com.android.security.fdiduds8.LockActivity;

/* renamed from: AuX  reason: default package and case insensitive filesystem */
public class C0073AuX implements Runnable {

    /* renamed from: ˊ  reason: contains not printable characters */
    private /* synthetic */ LockActivity f15;

    /* renamed from: ˋ  reason: contains not printable characters */
    private /* synthetic */ LockActivity f16;

    public C0073AuX(LockActivity lockActivity, LockActivity lockActivity2) {
        this.f16 = lockActivity;
        this.f15 = lockActivity2;
    }

    public final void run() {
        LockActivity lockActivity = this.f15;
        if (Build.VERSION.SDK_INT >= 19) {
            lockActivity.runOnUiThread(new C0079auX(lockActivity, lockActivity));
        }
    }
}
