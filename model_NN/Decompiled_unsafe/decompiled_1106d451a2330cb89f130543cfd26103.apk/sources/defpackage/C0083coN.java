package defpackage;

import android.os.SystemClock;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/* renamed from: coN  reason: default package and case insensitive filesystem */
public final class C0083coN {

    /* renamed from: ˊ  reason: contains not printable characters */
    public long f38;

    /* renamed from: ˋ  reason: contains not printable characters */
    public long f39;

    /* renamed from: ˎ  reason: contains not printable characters */
    private long f40;

    /* renamed from: ˊ  reason: contains not printable characters */
    public final boolean m17(String str) {
        DatagramSocket datagramSocket = null;
        try {
            DatagramSocket datagramSocket2 = new DatagramSocket();
            datagramSocket = datagramSocket2;
            datagramSocket2.setSoTimeout(7000);
            byte[] bArr = new byte[48];
            DatagramPacket datagramPacket = new DatagramPacket(bArr, 48, InetAddress.getByName(str), 123);
            bArr[0] = 27;
            long currentTimeMillis = System.currentTimeMillis();
            long elapsedRealtime = SystemClock.elapsedRealtime();
            byte[] bArr2 = bArr;
            long j = currentTimeMillis / 1000;
            long j2 = currentTimeMillis - (1000 * j);
            long j3 = j + 2208988800L;
            bArr2[40] = (byte) ((int) (j3 >> 24));
            bArr2[41] = (byte) ((int) (j3 >> 16));
            bArr2[42] = (byte) ((int) (j3 >> 8));
            bArr2[43] = (byte) ((int) j3);
            long j4 = (4294967296L * j2) / 1000;
            bArr2[44] = (byte) ((int) (j4 >> 24));
            bArr2[45] = (byte) ((int) (j4 >> 16));
            bArr2[46] = (byte) ((int) (j4 >> 8));
            bArr2[47] = (byte) ((int) (Math.random() * 255.0d));
            datagramSocket.send(datagramPacket);
            datagramSocket.receive(new DatagramPacket(bArr, 48));
            long elapsedRealtime2 = SystemClock.elapsedRealtime();
            long j5 = currentTimeMillis + (elapsedRealtime2 - elapsedRealtime);
            long r15 = m16(bArr, 24);
            long r17 = m16(bArr, 32);
            long r19 = m16(bArr, 40);
            this.f38 = j5 + (((r17 - r15) + (r19 - j5)) / 2);
            this.f39 = elapsedRealtime2;
            this.f40 = (elapsedRealtime2 - elapsedRealtime) - (r19 - r17);
            datagramSocket.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            if (datagramSocket == null) {
                return false;
            }
            datagramSocket.close();
            return false;
        } catch (Throwable th) {
            if (datagramSocket != null) {
                datagramSocket.close();
            }
            throw th;
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static long m15(byte[] bArr, int i) {
        byte b = bArr[i];
        byte b2 = bArr[i + 1];
        byte b3 = bArr[i + 2];
        byte b4 = bArr[i + 3];
        return (((long) ((b & 128) == 128 ? (b & Byte.MAX_VALUE) + 128 : b)) << 24) + (((long) ((b2 & 128) == 128 ? (b2 & Byte.MAX_VALUE) + 128 : b2)) << 16) + (((long) ((b3 & 128) == 128 ? (b3 & Byte.MAX_VALUE) + 128 : b3)) << 8) + ((long) ((b4 & 128) == 128 ? (b4 & Byte.MAX_VALUE) + 128 : b4));
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private static long m16(byte[] bArr, int i) {
        return ((m15(bArr, i) - 2208988800L) * 1000) + ((1000 * m15(bArr, i + 4)) / 4294967296L);
    }
}
