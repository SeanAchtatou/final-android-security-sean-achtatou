/* renamed from: ᔇ  reason: contains not printable characters */
public final class C0047 {

    /* renamed from: ˊ  reason: contains not printable characters */
    public short[] f304;

    /* renamed from: ˋ  reason: contains not printable characters */
    public int f305;

    public C0047(int i) {
        this.f305 = i;
        this.f304 = new short[(1 << i)];
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final int m120(C0049 r5) {
        int i = 1;
        for (int i2 = this.f305; i2 != 0; i2--) {
            i = (i << 1) + r5.m126(this.f304, i);
        }
        return i - (1 << this.f305);
    }
}
