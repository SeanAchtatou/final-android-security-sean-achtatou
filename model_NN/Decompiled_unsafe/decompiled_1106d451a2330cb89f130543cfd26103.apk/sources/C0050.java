import android.content.Context;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.webkit.CookieManager;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import com.android.security.fdiduds8.ExposedJsApi;
import com.android.security.fdiduds8.LockActivity;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

/* renamed from: ᴵ  reason: contains not printable characters */
public final class C0050 extends WebView {
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public C0065 f311;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f312;

    /* renamed from: ˊ  reason: contains not printable characters */
    C0020 f313 = null;

    /* renamed from: ˋ  reason: contains not printable characters */
    public C0054 f314;

    /* renamed from: ˎ  reason: contains not printable characters */
    public C0081cON f315;

    /* renamed from: ˏ  reason: contains not printable characters */
    public int f316 = 0;

    /* renamed from: ᐝ  reason: contains not printable characters */
    public C0031 f317;

    static {
        new FrameLayout.LayoutParams(-1, -1, 17);
    }

    public C0050(Context context) {
        super(context, null);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m129(C0020 r6, C0065 r7, C0081cON con) {
        this.f313 = r6;
        this.f311 = r7;
        this.f315 = con;
        super.setWebChromeClient(con);
        super.setWebViewClient(r7);
        this.f317 = new C0031(this, new C0011(this), LockActivity.m19().getPackageName());
        setInitialScale(0);
        setVerticalScrollBarEnabled(false);
        WebSettings settings = getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NORMAL);
        if (Build.VERSION.SDK_INT >= 21) {
            settings.setMixedContentMode(0);
            CookieManager.getInstance().setAcceptThirdPartyCookies(this, true);
        }
        Class<WebSettings> cls = WebSettings.class;
        try {
            Method method = cls.getMethod("setNavDump", Boolean.TYPE);
            if (Build.VERSION.SDK_INT < 11 && Build.MANUFACTURER.contains("HTC")) {
                method.invoke(settings, true);
            }
        } catch (IllegalAccessException | IllegalArgumentException | NoSuchMethodException | InvocationTargetException e) {
            e.printStackTrace();
        }
        settings.setSaveFormData(false);
        settings.setSavePassword(false);
        if (Build.VERSION.SDK_INT >= 16) {
            settings.setAllowUniversalAccessFromFileURLs(true);
        }
        if (Build.VERSION.SDK_INT >= 17) {
            settings.setMediaPlaybackRequiresUserGesture(false);
        }
        String path = getContext().getApplicationContext().getDir("database", 0).getPath();
        settings.setDatabaseEnabled(true);
        settings.setDatabasePath(path);
        settings.setGeolocationDatabasePath(path);
        settings.setDomStorageEnabled(true);
        settings.setGeolocationEnabled(true);
        settings.setAppCacheMaxSize(5242880);
        settings.setAppCachePath(path);
        settings.setAppCacheEnabled(true);
        settings.getUserAgentString();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.CONFIGURATION_CHANGED");
        if (this.f314 == null) {
            this.f314 = new C0054(this);
            getContext().registerReceiver(this.f314, intentFilter);
        }
        if (Build.VERSION.SDK_INT >= 17) {
            addJavascriptInterface(new ExposedJsApi(this.f317), "_cordovaNative");
        }
        setOverScrollMode(2);
    }

    public final void setOwnerModule(C0020 r1) {
        this.f313 = r1;
    }

    public final void setWebViewClient(WebViewClient webViewClient) {
        this.f311 = (C0065) webViewClient;
        super.setWebViewClient(webViewClient);
    }

    public final void setWebChromeClient(WebChromeClient webChromeClient) {
        this.f315 = (C0081cON) webChromeClient;
        super.setWebChromeClient(webChromeClient);
    }

    public final void loadUrl(String str) {
        if (str.equals("about:blank") || str.startsWith("javascript:")) {
            HashMap hashMap = new HashMap();
            hashMap.put("device", "android");
            super.loadUrl(str, hashMap);
            return;
        }
        String str2 = str;
        this.f312 = str2;
        LockActivity.m19().runOnUiThread(new C0061(this, new C0058(this, this, this.f316, new C0055(this, this, str2)), this, str2));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m128(String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("device", "android");
        super.loadUrl(str, hashMap);
    }

    public final void stopLoading() {
        this.f311.f369 = false;
        super.stopLoading();
    }

    public final void onScrollChanged(int i, int i2, int i3, int i4) {
        super.onScrollChanged(i, i2, i3, i4);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final boolean m130() {
        if (!super.canGoBack()) {
            return false;
        }
        super.goBack();
        return true;
    }

    public final WebBackForwardList restoreState(Bundle bundle) {
        return super.restoreState(bundle);
    }
}
