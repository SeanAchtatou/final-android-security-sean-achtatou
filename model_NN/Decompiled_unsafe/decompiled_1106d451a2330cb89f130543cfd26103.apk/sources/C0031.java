import defpackage.AUX;
import java.security.SecureRandom;
import org.json.JSONArray;
import org.json.JSONException;

/* renamed from: ـ  reason: contains not printable characters */
public final class C0031 {

    /* renamed from: ˊ  reason: contains not printable characters */
    public C0011 f159;

    /* renamed from: ˋ  reason: contains not printable characters */
    volatile int f160 = -1;

    /* renamed from: ˎ  reason: contains not printable characters */
    private C0050 f161;

    /* renamed from: ˏ  reason: contains not printable characters */
    private String f162;

    public C0031(C0050 r6, C0011 r7, String str) {
        this.f161 = r6;
        this.f159 = r7;
        this.f162 = "content://" + str + new String(".".getBytes(), 0).intern();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final String m81(int i, String str, String str2, String str3) {
        C0050 r6;
        if (!m83(i)) {
            return null;
        }
        if (str3 == null) {
            return "@Null arguments.";
        }
        this.f159.m47(true);
        try {
            r6 = this.f161;
            if (r6.f313 == null || r6.f313.f135 == null) {
                String str4 = str2;
                AUX aux = new AUX(AUX.Cif.f9);
                C0011 r7 = r6.f317.f159;
                if (str4 != null) {
                    int i2 = AUX.Cif.f7;
                    r7.m46(new Cif(aux, str4));
                }
            } else {
                if (((Boolean) r6.f313.m70("uiExecute", str, str3, str2).f135).booleanValue()) {
                    String str5 = str2;
                    AUX aux2 = new AUX(AUX.Cif.f8);
                    C0011 r72 = r6.f317.f159;
                    if (str5 != null) {
                        int i3 = AUX.Cif.f7;
                        r72.m46(new Cif(aux2, str5));
                    }
                } else {
                    String str6 = str2;
                    AUX aux3 = new AUX(AUX.Cif.f10);
                    C0011 r73 = r6.f317.f159;
                    if (str6 != null) {
                        int i4 = AUX.Cif.f7;
                        r73.m46(new Cif(aux3, str6));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (e.getCause() instanceof JSONException) {
                String str7 = str2;
                AUX aux4 = new AUX(AUX.Cif.f11);
                C0011 r74 = r6.f317.f159;
                if (str7 != null) {
                    int i5 = AUX.Cif.f7;
                    r74.m46(new Cif(aux4, str7));
                }
            } else {
                String str8 = str2;
                AUX aux5 = new AUX(AUX.Cif.f6, e.getMessage());
                C0011 r75 = r6.f317.f159;
                if (str8 != null) {
                    int i6 = AUX.Cif.f7;
                    r75.m46(new Cif(aux5, str8));
                }
            }
        } catch (Throwable th) {
            try {
                th.printStackTrace();
                return "";
            } finally {
                this.f159.m47(false);
            }
        }
        String r62 = this.f159.m44(false);
        this.f159.m47(false);
        return r62;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final boolean m83(int i) {
        if (!(this.f159.f110 != null)) {
            return false;
        }
        if (this.f160 >= 0 && i == this.f160) {
            return true;
        }
        this.f160 = -1;
        throw new IllegalAccessException();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final String m82(String str, String str2, String str3) {
        String str4;
        if (str3 != null && str3.length() > 3 && str3.startsWith("gap:")) {
            try {
                JSONArray jSONArray = new JSONArray(str3.substring(4));
                String r4 = m81(jSONArray.getInt(0), jSONArray.getString(1), jSONArray.getString(2), str2);
                return r4 == null ? "" : r4;
            } catch (IllegalAccessException | JSONException e) {
                e.printStackTrace();
                return "";
            }
        } else if (str3 != null && str3.startsWith("gap_bridge_mode:")) {
            try {
                int parseInt = Integer.parseInt(str3.substring(16));
                int parseInt2 = Integer.parseInt(str2);
                if (!m83(parseInt)) {
                    return "";
                }
                this.f159.m45(parseInt2);
                return "";
            } catch (IllegalAccessException | NumberFormatException e2) {
                e2.printStackTrace();
                return "";
            }
        } else if (str3 != null && str3.startsWith("gap_poll:")) {
            int parseInt3 = Integer.parseInt(str3.substring(9));
            try {
                boolean equals = "1".equals(str2);
                if (!m83(parseInt3)) {
                    str4 = null;
                } else {
                    str4 = this.f159.m44(equals);
                }
                return str4 == null ? "" : str4;
            } catch (IllegalAccessException e3) {
                e3.printStackTrace();
                return "";
            }
        } else if (str3 == null || !str3.startsWith("gap_init:")) {
            return null;
        } else {
            if (!str.startsWith("file:") && !str.startsWith(this.f162)) {
                return "";
            }
            this.f159.m45(Integer.parseInt(str3.substring(9)));
            this.f160 = new SecureRandom().nextInt(Integer.MAX_VALUE);
            return String.valueOf(this.f160);
        }
    }
}
