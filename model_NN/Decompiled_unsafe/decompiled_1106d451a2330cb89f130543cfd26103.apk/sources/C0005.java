import C0066;
import android.annotation.TargetApi;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;

/* renamed from: ʴ  reason: contains not printable characters */
class C0005 extends CameraCaptureSession.CaptureCallback {

    /* renamed from: ˊ  reason: contains not printable characters */
    private /* synthetic */ C0066.C0067 f98;

    C0005(C0066.C0067 r1) {
        this.f98 = r1;
    }

    @TargetApi(21)
    /* renamed from: ˊ  reason: contains not printable characters */
    private void m38(CaptureResult captureResult) {
        switch (this.f98.f395) {
            case 0:
                boolean unused = this.f98.f396 = true;
                return;
            case 1:
                Integer num = (Integer) captureResult.get(CaptureResult.CONTROL_AE_STATE);
                if (num == null || num.intValue() == 2) {
                    int unused2 = this.f98.f395 = 3;
                    C0066.C0067.m160(this.f98);
                    return;
                }
                C0066.C0067.m179(this.f98);
                return;
            case 2:
                Integer num2 = (Integer) captureResult.get(CaptureResult.CONTROL_AE_STATE);
                if (num2 == null || num2.intValue() == 5 || num2.intValue() == 4) {
                    int unused3 = this.f98.f395 = 3;
                    return;
                }
                return;
            case 3:
                Integer num3 = (Integer) captureResult.get(CaptureResult.CONTROL_AE_STATE);
                if (num3 == null || num3.intValue() != 5) {
                    int unused4 = this.f98.f395 = 4;
                    C0066.C0067.m160(this.f98);
                    return;
                }
                return;
            default:
                return;
        }
    }

    public final void onCaptureProgressed(CameraCaptureSession cameraCaptureSession, CaptureRequest captureRequest, CaptureResult captureResult) {
        m38(captureResult);
    }

    public final void onCaptureCompleted(CameraCaptureSession cameraCaptureSession, CaptureRequest captureRequest, TotalCaptureResult totalCaptureResult) {
        m38(totalCaptureResult);
    }
}
