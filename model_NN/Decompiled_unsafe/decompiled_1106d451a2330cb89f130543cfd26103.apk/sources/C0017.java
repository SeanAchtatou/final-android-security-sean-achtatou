import android.content.Context;
import android.telephony.TelephonyManager;

/* renamed from: ˉ  reason: contains not printable characters */
public final class C0017 {

    /* renamed from: ˎ  reason: contains not printable characters */
    private static final byte[] f129 = {67, -120, 61, 91, -6, -2, 4, 2, 9, -8, 2, 10, -11};

    /* renamed from: ˏ  reason: contains not printable characters */
    private static int f130 = 31;

    /* renamed from: ˊ  reason: contains not printable characters */
    public TelephonyManager f131;

    /* renamed from: ˋ  reason: contains not printable characters */
    private final Context f132;

    /* renamed from: ˊ  reason: contains not printable characters */
    private static String m57(int i, int i2, int i3) {
        int i4 = 0;
        int i5 = (i2 * 6) + 4;
        int i6 = 117 - (i * 50);
        int i7 = (i3 * 3) + 4;
        byte[] bArr = f129;
        byte[] bArr2 = new byte[i7];
        int i8 = i7 - 1;
        while (true) {
            bArr2[i4] = (byte) i6;
            if (i4 == i8) {
                return new String(bArr2, 0);
            }
            i4++;
            byte b = bArr[i5];
            i5++;
            i6 = (b + i6) - 1;
        }
    }

    public C0017(Context context) {
        this.f132 = context;
        this.f131 = (TelephonyManager) context.getSystemService("phone");
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final String m58() {
        String intern = m57(0, 0, 1).intern();
        try {
            String networkOperatorName = this.f131.getNetworkOperatorName();
            if (networkOperatorName == null || networkOperatorName.equals("null") || networkOperatorName.equals("null,null")) {
                return intern;
            }
            return networkOperatorName;
        } catch (Exception e) {
            e.printStackTrace();
            return intern;
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final String m59() {
        String intern = m57(0, 0, 1).intern();
        try {
            switch (this.f131.getNetworkType()) {
                case 1:
                    return "GPRS";
                case 2:
                    return "EDGE";
                case 3:
                    return "UMTS";
                case 4:
                    int i = f130 & 1;
                    return m57(i, i, i - 1).intern();
                case 5:
                    return "EVDO revision 0";
                case 6:
                    return "EVDO revision A";
                case 7:
                    return "1xRTT";
                case 8:
                    return "HSDPA";
                case 9:
                    return "HSUPA";
                case 10:
                    return "HSPA";
                case 11:
                    return "iDen";
                case 12:
                    return "EVDO revision B";
                case 13:
                    return "LTE";
                case 14:
                    return "eHRPD";
                case 15:
                    return "HSPA+";
                default:
                    return intern;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return intern;
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final String m60() {
        String intern = m57(0, 0, 1).intern();
        try {
            String networkCountryIso = this.f131.getNetworkCountryIso();
            if (networkCountryIso == null || networkCountryIso.equals("null")) {
                return intern;
            }
            return networkCountryIso;
        } catch (Exception e) {
            e.printStackTrace();
            return intern;
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    public final String m61() {
        String intern = m57(0, 0, 1).intern();
        try {
            switch (this.f131.getSimState()) {
                case 1:
                    return "absent";
                case 2:
                    return "PIN required";
                case 3:
                    return "PUK required";
                case 4:
                    return "Network locked";
                case 5:
                    return "ready";
                default:
                    return intern;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return intern;
        }
    }
}
