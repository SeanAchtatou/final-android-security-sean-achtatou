import android.content.DialogInterface;
import android.view.KeyEvent;
import android.webkit.JsResult;

/* renamed from: ՙ  reason: contains not printable characters */
class C0029 implements DialogInterface.OnKeyListener {

    /* renamed from: ˊ  reason: contains not printable characters */
    private /* synthetic */ JsResult f151;

    /* renamed from: ˋ  reason: contains not printable characters */
    private /* synthetic */ C0081cON f152;

    C0029(C0081cON con, JsResult jsResult) {
        this.f152 = con;
        this.f151 = jsResult;
    }

    public final boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (i != 4) {
            return true;
        }
        this.f151.cancel();
        return false;
    }
}
