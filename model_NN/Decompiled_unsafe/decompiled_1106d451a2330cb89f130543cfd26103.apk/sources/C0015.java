import C0066;
import android.annotation.TargetApi;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CaptureRequest;

/* renamed from: ˇ  reason: contains not printable characters */
class C0015 extends CameraCaptureSession.StateCallback {

    /* renamed from: ˊ  reason: contains not printable characters */
    private /* synthetic */ C0066.C0067 f127;

    C0015(C0066.C0067 r1) {
        this.f127 = r1;
    }

    @TargetApi(21)
    public final void onConfigured(CameraCaptureSession cameraCaptureSession) {
        if (this.f127.f399 != null) {
            CameraCaptureSession unused = this.f127.f404 = cameraCaptureSession;
            try {
                this.f127.f390.set(CaptureRequest.CONTROL_AF_MODE, 4);
                this.f127.f390.set(CaptureRequest.CONTROL_AE_MODE, 2);
                CaptureRequest unused2 = this.f127.f392 = this.f127.f390.build();
                if (this.f127.f404 != null) {
                    this.f127.f404.setRepeatingRequest(this.f127.f392, this.f127.f405, this.f127.f398);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public final void onConfigureFailed(CameraCaptureSession cameraCaptureSession) {
    }
}
