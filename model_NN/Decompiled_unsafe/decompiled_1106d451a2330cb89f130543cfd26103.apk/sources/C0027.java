import C0066;
import android.annotation.TargetApi;
import android.hardware.camera2.CaptureRequest;

/* renamed from: ˮ  reason: contains not printable characters */
class C0027 implements Runnable {

    /* renamed from: ˊ  reason: contains not printable characters */
    private /* synthetic */ C0066.C0067 f147;

    /* renamed from: ˋ  reason: contains not printable characters */
    private /* synthetic */ C0066.C0067 f148;

    C0027(C0066.C0067 r1, C0066.C0067 r2) {
        this.f148 = r1;
        this.f147 = r2;
    }

    @TargetApi(21)
    public final void run() {
        try {
            this.f147.f390.set(CaptureRequest.CONTROL_AF_TRIGGER, 1);
            int unused = this.f147.f395 = 1;
            this.f147.f404.setRepeatingRequest(this.f147.f390.build(), this.f148.f405, this.f147.f398);
        } catch (Exception e) {
            e.printStackTrace();
            this.f147.f401.countDown();
        }
    }
}
