/* renamed from: יּ  reason: contains not printable characters */
public final class C0063 extends C0037 {

    /* renamed from: ˈ  reason: contains not printable characters */
    public static final int[] f354 = new int[256];

    /* renamed from: ʻ  reason: contains not printable characters */
    public int f355 = 255;

    /* renamed from: ʼ  reason: contains not printable characters */
    public int f356;

    /* renamed from: ʽ  reason: contains not printable characters */
    public int f357 = 0;

    /* renamed from: ʾ  reason: contains not printable characters */
    public int f358 = 4;

    /* renamed from: ʿ  reason: contains not printable characters */
    public int f359 = 66560;

    /* renamed from: ˊ  reason: contains not printable characters */
    public int f360;

    /* renamed from: ˋ  reason: contains not printable characters */
    public int f361 = 0;

    /* renamed from: ˎ  reason: contains not printable characters */
    public int f362;

    /* renamed from: ˏ  reason: contains not printable characters */
    public int[] f363;

    /* renamed from: ͺ  reason: contains not printable characters */
    public boolean f364 = true;

    /* renamed from: ᐝ  reason: contains not printable characters */
    public int[] f365;

    /* renamed from: ι  reason: contains not printable characters */
    public int f366 = 0;

    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m143() {
        super.m91();
        for (int i = 0; i < this.f357; i++) {
            this.f365[i] = 0;
        }
        this.f360 = 0;
        m92(-1);
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m144() {
        int i = this.f360 + 1;
        this.f360 = i;
        if (i >= this.f361) {
            this.f360 = 0;
        }
        super.m93();
        if (this.f176 == 1073741823) {
            int i2 = this.f176 - this.f361;
            m142(this.f363, this.f361 * 2, i2);
            m142(this.f365, this.f357, i2);
            m92(i2);
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static void m142(int[] iArr, int i, int i2) {
        int i3;
        for (int i4 = 0; i4 < i; i4++) {
            int i5 = iArr[i4];
            int i6 = i5;
            if (i5 <= i2) {
                i3 = 0;
            } else {
                i3 = i6 - i2;
            }
            iArr[i4] = i3;
        }
    }

    static {
        for (int i = 0; i < 256; i++) {
            int i2 = i;
            for (int i3 = 0; i3 < 8; i3++) {
                if ((i2 & 1) != 0) {
                    i2 = (i2 >>> 1) ^ -306674912;
                } else {
                    i2 >>>= 1;
                }
            }
            f354[i] = i2;
        }
    }
}
