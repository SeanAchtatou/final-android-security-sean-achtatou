/* renamed from: ᔈ  reason: contains not printable characters */
public final class C0048 {

    /* renamed from: ˊ  reason: contains not printable characters */
    public short[] f306;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f307;

    public C0048(int i) {
        this.f307 = i;
        this.f306 = new short[(1 << i)];
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m123(C0051 r6, int i) {
        int i2 = 1;
        int i3 = this.f307;
        while (i3 != 0) {
            i3--;
            int i4 = (i >>> i3) & 1;
            r6.m137(this.f306, i2, i4);
            i2 = (i2 << 1) | i4;
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m125(C0051 r5, int i) {
        int i2 = 1;
        for (int i3 = 0; i3 < this.f307; i3++) {
            int i4 = i & 1;
            r5.m137(this.f306, i2, i4);
            i2 = (i2 << 1) | i4;
            i >>= 1;
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final int m122(int i) {
        int i2 = 0;
        int i3 = 1;
        int i4 = this.f307;
        while (i4 != 0) {
            i4--;
            int i5 = (i >>> i4) & 1;
            i2 += C0051.m134(this.f306[i3], i5);
            i3 = (i3 << 1) + i5;
        }
        return i2;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final int m124(int i) {
        int i2 = 0;
        int i3 = 1;
        for (int i4 = this.f307; i4 != 0; i4--) {
            int i5 = i & 1;
            i >>>= 1;
            i2 += C0051.m134(this.f306[i3], i5);
            i3 = (i3 << 1) | i5;
        }
        return i2;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public static int m121(short[] sArr, int i, int i2, int i3) {
        int i4 = 0;
        int i5 = 1;
        while (i2 != 0) {
            int i6 = i3 & 1;
            i3 >>>= 1;
            i4 += C0051.m134(sArr[i + i5], i6);
            i5 = (i5 << 1) | i6;
            i2--;
        }
        return i4;
    }
}
