import android.content.DialogInterface;
import android.webkit.JsPromptResult;
import android.widget.EditText;

/* renamed from: י  reason: contains not printable characters */
class C0030 implements DialogInterface.OnClickListener {

    /* renamed from: ˊ  reason: contains not printable characters */
    private /* synthetic */ EditText f156;

    /* renamed from: ˋ  reason: contains not printable characters */
    private /* synthetic */ JsPromptResult f157;

    /* renamed from: ˎ  reason: contains not printable characters */
    private /* synthetic */ C0081cON f158;

    C0030(C0081cON con, EditText editText, JsPromptResult jsPromptResult) {
        this.f158 = con;
        this.f156 = editText;
        this.f157 = jsPromptResult;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f157.confirm(this.f156.getText().toString());
    }
}
