import java.lang.reflect.Method;
import java.util.Arrays;

/* renamed from: ۦ  reason: contains not printable characters */
public final class C0034 {

    /* renamed from: ˊ  reason: contains not printable characters */
    public final Object f167;

    /* renamed from: ˋ  reason: contains not printable characters */
    private final boolean f168 = false;

    public C0034(Object obj) {
        this.f167 = obj;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static boolean m87(Class<?>[] clsArr, Class<?>[] clsArr2) {
        if (clsArr.length != clsArr2.length) {
            return false;
        }
        for (int i = 0; i < clsArr2.length; i++) {
            if (clsArr2[i] != Cif.class && !m84(clsArr[i]).isAssignableFrom(m84(clsArr2[i]))) {
                return false;
            }
        }
        return true;
    }

    public final int hashCode() {
        return this.f167.hashCode();
    }

    public final boolean equals(Object obj) {
        if (obj instanceof C0034) {
            return this.f167.equals(((C0034) obj).f167);
        }
        return false;
    }

    public final String toString() {
        return this.f167.toString();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static Class<?> m84(Class<?> cls) {
        if (cls == null) {
            return null;
        }
        if (cls.isPrimitive()) {
            if (Boolean.TYPE == cls) {
                return Boolean.class;
            }
            if (Integer.TYPE == cls) {
                return Integer.class;
            }
            if (Long.TYPE == cls) {
                return Long.class;
            }
            if (Short.TYPE == cls) {
                return Short.class;
            }
            if (Byte.TYPE == cls) {
                return Byte.class;
            }
            if (Double.TYPE == cls) {
                return Double.class;
            }
            if (Float.TYPE == cls) {
                return Float.class;
            }
            if (Character.TYPE == cls) {
                return Character.class;
            }
            if (Void.TYPE == cls) {
                return Void.class;
            }
        }
        return cls;
    }

    /* renamed from: ۦ$if  reason: invalid class name */
    static class Cif {
        private Cif() {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ۦ.ˊ(java.lang.String, java.lang.Class<?>[]):java.lang.reflect.Method
     arg types: [java.lang.String, java.lang.Class[]]
     candidates:
      ۦ.ˊ(java.lang.Class<?>[], java.lang.Class<?>[]):boolean
      ۦ.ˊ(java.lang.String, java.lang.Object[]):ۦ
      ۦ.ˊ(java.lang.String, java.lang.Class<?>[]):java.lang.reflect.Method */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final C0034 m88(String str, Object... objArr) {
        Method method;
        Object[] objArr2 = objArr;
        Class[] clsArr = new Class[objArr.length];
        for (int i = 0; i < objArr2.length; i++) {
            Object obj = objArr2[i];
            clsArr[i] = obj == null ? Cif.class : obj.getClass();
        }
        Class[] clsArr2 = clsArr;
        try {
            return m86(m85(str, (Class<?>[]) clsArr2), this.f167, objArr);
        } catch (NoSuchMethodException unused) {
            Class[] clsArr3 = clsArr2;
            String str2 = str;
            Class<?> cls = this.f167.getClass();
            Method[] methods = cls.getMethods();
            int length = methods.length;
            int i2 = 0;
            while (true) {
                if (i2 < length) {
                    Method method2 = methods[i2];
                    Method method3 = method2;
                    if (method2.getName().equals(str2) && m87(method2.getParameterTypes(), clsArr3)) {
                        method = method3;
                        break;
                    }
                    i2++;
                } else {
                    do {
                        Method[] declaredMethods = cls.getDeclaredMethods();
                        int length2 = declaredMethods.length;
                        int i3 = 0;
                        while (i3 < length2) {
                            Method method4 = declaredMethods[i3];
                            Method method5 = method4;
                            if (method4.getName().equals(str2) && m87(method4.getParameterTypes(), clsArr3)) {
                                method = method5;
                            } else {
                                i3++;
                            }
                        }
                        cls = cls.getSuperclass();
                    } while (cls != null);
                    throw new NoSuchMethodException("No similar method " + str2 + " with params " + Arrays.toString(clsArr3) + " could be found on type " + this.f167.getClass() + new String(".".getBytes(), 0).intern());
                }
            }
            return m86(method, this.f167, objArr);
        } catch (NoSuchMethodException e) {
            throw new C0035(e);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001b, code lost:
        throw new java.lang.NoSuchMethodException();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000f, code lost:
        return r1.getDeclaredMethod(r3, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0010, code lost:
        r1 = r1.getSuperclass();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0014, code lost:
        if (r1 == null) goto L_0x0016;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:4:0x000b */
    /* renamed from: ˊ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.reflect.Method m85(java.lang.String r3, java.lang.Class<?>[] r4) {
        /*
            r2 = this;
            java.lang.Object r0 = r2.f167
            java.lang.Class r1 = r0.getClass()
            java.lang.reflect.Method r0 = r1.getMethod(r3, r4)     // Catch:{ NoSuchMethodException -> 0x000b }
            return r0
        L_0x000b:
            java.lang.reflect.Method r0 = r1.getDeclaredMethod(r3, r4)     // Catch:{ NoSuchMethodException -> 0x0010 }
            return r0
        L_0x0010:
            java.lang.Class r1 = r1.getSuperclass()
            if (r1 != 0) goto L_0x000b
            java.lang.NoSuchMethodException r0 = new java.lang.NoSuchMethodException
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: C0034.m85(java.lang.String, java.lang.Class[]):java.lang.reflect.Method");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0021, code lost:
        if (java.lang.reflect.Modifier.isPublic(r3.getDeclaringClass().getModifiers()) != false) goto L_0x002d;
     */
    /* renamed from: ˊ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static C0034 m86(java.lang.reflect.Method r4, java.lang.Object r5, java.lang.Object... r6) {
        /*
            r2 = r4
            if (r4 == 0) goto L_0x002d
            boolean r0 = r2 instanceof java.lang.reflect.Member
            if (r0 == 0) goto L_0x0023
            r0 = r2
            java.lang.reflect.Member r0 = (java.lang.reflect.Member) r0     // Catch:{ Exception -> 0x0049 }
            r3 = r0
            int r0 = r3.getModifiers()     // Catch:{ Exception -> 0x0049 }
            boolean r0 = java.lang.reflect.Modifier.isPublic(r0)     // Catch:{ Exception -> 0x0049 }
            if (r0 == 0) goto L_0x0023
            java.lang.Class r0 = r3.getDeclaringClass()     // Catch:{ Exception -> 0x0049 }
            int r0 = r0.getModifiers()     // Catch:{ Exception -> 0x0049 }
            boolean r0 = java.lang.reflect.Modifier.isPublic(r0)     // Catch:{ Exception -> 0x0049 }
            if (r0 != 0) goto L_0x002d
        L_0x0023:
            boolean r0 = r2.isAccessible()     // Catch:{ Exception -> 0x0049 }
            if (r0 != 0) goto L_0x002d
            r0 = 1
            r2.setAccessible(r0)     // Catch:{ Exception -> 0x0049 }
        L_0x002d:
            java.lang.Class r0 = r4.getReturnType()     // Catch:{ Exception -> 0x0049 }
            java.lang.Class r1 = java.lang.Void.TYPE     // Catch:{ Exception -> 0x0049 }
            if (r0 != r1) goto L_0x003f
            r4.invoke(r5, r6)     // Catch:{ Exception -> 0x0049 }
            r4 = r5
            ۦ r0 = new ۦ     // Catch:{ Exception -> 0x0049 }
            r0.<init>(r4)     // Catch:{ Exception -> 0x0049 }
            return r0
        L_0x003f:
            java.lang.Object r4 = r4.invoke(r5, r6)     // Catch:{ Exception -> 0x0049 }
            ۦ r0 = new ۦ     // Catch:{ Exception -> 0x0049 }
            r0.<init>(r4)     // Catch:{ Exception -> 0x0049 }
            return r0
        L_0x0049:
            r4 = move-exception
            เ r0 = new เ
            r0.<init>(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: C0034.m86(java.lang.reflect.Method, java.lang.Object, java.lang.Object[]):ۦ");
    }
}
