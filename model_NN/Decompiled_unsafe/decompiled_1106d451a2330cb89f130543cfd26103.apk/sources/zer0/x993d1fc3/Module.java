package zer0.x993d1fc3;

public class Module {

    /* renamed from: ˊ$4a74f  reason: contains not printable characters */
    private final Object f87$4a74f;

    Module() {
        try {
            this.f87$4a74f = C0087$.m139("ᴸ").getDeclaredConstructor(Module.class).newInstance(this);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public int getVersion() {
        return 279;
    }

    public int getHash() {
        try {
            return ((Integer) C0087$.m139("ᴸ").getMethod("ˊ", null).invoke(this.f87$4a74f, null)).intValue();
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public int getState() {
        try {
            return ((Integer) C0087$.m139("ᴸ").getMethod("ˋ", null).invoke(this.f87$4a74f, null)).intValue();
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public void onInit(Object obj) {
        Object obj2 = this.f87$4a74f;
        try {
            C0087$.m139("ᴸ").getMethod("ˊ", Object.class).invoke(obj2, obj);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public void onLoad() {
        try {
            C0087$.m139("ᴸ").getMethod("ˎ", null).invoke(this.f87$4a74f, null);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public void onUnload() {
        try {
            C0087$.m139("ᴸ").getMethod("ˏ", null).invoke(this.f87$4a74f, null);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public void doCommonJob(long j) {
        Object obj = this.f87$4a74f;
        try {
            C0087$.m139("ᴸ").getMethod("ˊ", Long.TYPE).invoke(obj, Long.valueOf(j));
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public boolean uiExecute(String str, String str2, String str3) {
        Object obj = this.f87$4a74f;
        try {
            Object[] objArr = new Object[3];
            objArr[2] = str3;
            objArr[1] = str2;
            objArr[0] = str;
            return ((Boolean) C0087$.m139("ᴸ").getMethod("ˊ", String.class, String.class, String.class).invoke(obj, objArr)).booleanValue();
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public void onCtrlResponse(Object obj) {
        Object obj2 = this.f87$4a74f;
        try {
            C0087$.m139("ᴸ").getMethod("ˋ", Object.class).invoke(obj2, obj);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }
}
