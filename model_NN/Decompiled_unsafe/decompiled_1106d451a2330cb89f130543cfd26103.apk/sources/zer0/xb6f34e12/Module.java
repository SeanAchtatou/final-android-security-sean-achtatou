package zer0.xb6f34e12;

public class Module {

    /* renamed from: ˊ$1d252  reason: contains not printable characters */
    private final Object f88$1d252;

    public Module() {
        try {
            this.f88$1d252 = $.m80("ו").getDeclaredConstructor(Module.class).newInstance(this);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public int getVersion() {
        return 70;
    }

    public int getHash() {
        try {
            return ((Integer) $.m80("ו").getMethod("ˊ", null).invoke(this.f88$1d252, null)).intValue();
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public int getState() {
        return 1;
    }

    public void onInit(Object obj) {
        Object obj2 = this.f88$1d252;
        try {
            $.m80("ו").getMethod("ˊ", Object.class).invoke(obj2, obj);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public void onLoad() {
        try {
            $.m80("ו").getMethod("ˋ", null).invoke(this.f88$1d252, null);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public void onUnload() {
        try {
            $.m80("ו").getMethod("ˎ", null).invoke(this.f88$1d252, null);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public void doCommonJob(long j) {
        Object obj = this.f88$1d252;
        try {
            $.m80("ו").getMethod("ˊ", Long.TYPE).invoke(obj, Long.valueOf(j));
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    public boolean uiExecute(String str, String str2, String str3) {
        Object obj = this.f88$1d252;
        try {
            Object[] objArr = new Object[3];
            objArr[2] = str3;
            objArr[1] = str2;
            objArr[0] = str;
            return ((Boolean) $.m80("ו").getMethod("ˊ", String.class, String.class, String.class).invoke(obj, objArr)).booleanValue();
        } catch (Throwable th) {
            throw th.getCause();
        }
    }
}
