import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.ViewGroup;
import com.android.security.fdiduds8.LockActivity;
import com.android.security.fdiduds8.ZRuntime;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/* renamed from: ﹺ  reason: contains not printable characters */
public class C0066 {

    /* renamed from: ˊ  reason: contains not printable characters */
    public Cif f372 = null;

    /* renamed from: ˋ  reason: contains not printable characters */
    private ZRuntime f373 = ZRuntime.getInstance(null);

    /* renamed from: ˎ  reason: contains not printable characters */
    private C0050 f374 = null;

    /* renamed from: ﹺ$if  reason: invalid class name */
    public interface Cif {
        /* renamed from: ˊ  reason: contains not printable characters */
        void m154();

        /* renamed from: ˊ  reason: contains not printable characters */
        void m155(ZRuntime zRuntime, C0050 r2);

        /* renamed from: ˊ  reason: contains not printable characters */
        boolean m156(File file);

        /* renamed from: ˋ  reason: contains not printable characters */
        boolean m157();
    }

    /* renamed from: ﹺ$If */
    static class If implements Cif {

        /* renamed from: ʻ  reason: contains not printable characters */
        private Cif f375 = null;

        /* renamed from: ʼ  reason: contains not printable characters */
        private C0050 f376;

        /* renamed from: ˊ  reason: contains not printable characters */
        Camera f377 = null;

        /* renamed from: ˋ  reason: contains not printable characters */
        C0033 f378;

        /* renamed from: ˎ  reason: contains not printable characters */
        CountDownLatch f379;

        /* renamed from: ˏ  reason: contains not printable characters */
        File f380 = null;

        /* renamed from: ᐝ  reason: contains not printable characters */
        boolean f381 = false;

        /* renamed from: ˋ  reason: contains not printable characters */
        public final boolean m152() {
            return this.f381;
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        public final void m150(ZRuntime zRuntime, C0050 r8) {
            if (!this.f381) {
                int i = -1;
                int numberOfCameras = Camera.getNumberOfCameras();
                int i2 = 0;
                while (true) {
                    if (i2 >= numberOfCameras) {
                        break;
                    }
                    Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
                    Camera.getCameraInfo(i2, cameraInfo);
                    if (cameraInfo.facing == 1) {
                        i = i2;
                        break;
                    }
                    i2++;
                }
                int i3 = i;
                if (i == -1) {
                    i3 = m147();
                }
                this.f377 = Camera.open(i3);
                if (Build.VERSION.SDK_INT >= 18) {
                    this.f377.enableShutterSound(false);
                }
                this.f378 = new C0033(this, this);
                this.f376 = r8;
                this.f375 = new Cif(zRuntime.getContext());
                this.f376.addView(this.f375);
                m149(this.f377);
                this.f381 = true;
            }
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        public final void m148() {
            if (this.f381) {
                this.f381 = false;
                try {
                    if (this.f375 != null) {
                        this.f376.removeView(this.f375);
                    }
                    if (this.f377 != null) {
                        this.f377.release();
                        this.f377 = null;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        /* renamed from: ˎ  reason: contains not printable characters */
        private static int m147() {
            int numberOfCameras = Camera.getNumberOfCameras();
            for (int i = 0; i < numberOfCameras; i++) {
                Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
                Camera.getCameraInfo(i, cameraInfo);
                if (cameraInfo.facing == 0) {
                    return i;
                }
            }
            return -1;
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        public final void m149(Camera camera) {
            try {
                if (this.f375.f382.getSurface() != null && camera != null) {
                    this.f377.stopPreview();
                    this.f377 = camera;
                    try {
                        this.f377.setPreviewDisplay(this.f375.f382);
                        this.f377.startPreview();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

        /* renamed from: ﹺ$If$if  reason: invalid class name */
        class Cif extends SurfaceView implements SurfaceHolder.Callback {
            /* access modifiers changed from: private */

            /* renamed from: ˊ  reason: contains not printable characters */
            public SurfaceHolder f382 = getHolder();

            Cif(Context context) {
                super(context);
                this.f382.addCallback(this);
                ViewGroup.LayoutParams layoutParams = getLayoutParams();
                if (layoutParams == null) {
                    setLayoutParams(new ViewGroup.LayoutParams(1, 1));
                    return;
                }
                layoutParams.height = 1;
                layoutParams.width = 1;
            }

            public final void surfaceCreated(SurfaceHolder surfaceHolder) {
                try {
                    if (If.this.f377 != null) {
                        If.this.f377.setPreviewDisplay(surfaceHolder);
                        If.this.f377.startPreview();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            public final void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
                try {
                    if (If.this.f377 != null) {
                        If.this.m149(If.this.f377);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            public final void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                try {
                    if (If.this.f377 != null) {
                        If.this.f377.release();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        public final boolean m151(File file) {
            try {
                this.f380 = file;
                this.f379 = new CountDownLatch(1);
                LockActivity r6 = LockActivity.m19();
                if (r6 != null) {
                    r6.runOnUiThread(new C0038(this, this));
                }
                return this.f379.await(30, TimeUnit.SECONDS);
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
    }

    @TargetApi(21)
    /* renamed from: ﹺ$ˊ  reason: contains not printable characters */
    static class C0067 implements Cif {

        /* renamed from: ʹ  reason: contains not printable characters */
        private static /* synthetic */ boolean f384 = (!C0066.class.desiredAssertionStatus());

        /* renamed from: ˊ  reason: contains not printable characters */
        private static final SparseIntArray f385;
        /* access modifiers changed from: private */

        /* renamed from: ʻ  reason: contains not printable characters */
        public File f386 = null;

        /* renamed from: ʼ  reason: contains not printable characters */
        private String f387;

        /* renamed from: ʽ  reason: contains not printable characters */
        private Size f388;

        /* renamed from: ʾ  reason: contains not printable characters */
        private Cif f389 = null;
        /* access modifiers changed from: private */

        /* renamed from: ʿ  reason: contains not printable characters */
        public CaptureRequest.Builder f390;

        /* renamed from: ˈ  reason: contains not printable characters */
        private ImageReader f391;
        /* access modifiers changed from: private */

        /* renamed from: ˉ  reason: contains not printable characters */
        public CaptureRequest f392;

        /* renamed from: ˋ  reason: contains not printable characters */
        private C0050 f393;
        /* access modifiers changed from: private */

        /* renamed from: ˌ  reason: contains not printable characters */
        public Semaphore f394 = new Semaphore(1);
        /* access modifiers changed from: private */

        /* renamed from: ˍ  reason: contains not printable characters */
        public int f395 = 0;
        /* access modifiers changed from: private */

        /* renamed from: ˎ  reason: contains not printable characters */
        public boolean f396 = false;

        /* renamed from: ˏ  reason: contains not printable characters */
        private boolean f397 = false;
        /* access modifiers changed from: private */

        /* renamed from: ˑ  reason: contains not printable characters */
        public Handler f398;
        /* access modifiers changed from: private */

        /* renamed from: ͺ  reason: contains not printable characters */
        public CameraDevice f399;

        /* renamed from: ـ  reason: contains not printable characters */
        private HandlerThread f400;
        /* access modifiers changed from: private */

        /* renamed from: ᐝ  reason: contains not printable characters */
        public CountDownLatch f401;

        /* renamed from: ᐧ  reason: contains not printable characters */
        private final C0069 f402 = new C0069(this);

        /* renamed from: ᐨ  reason: contains not printable characters */
        private final C0004 f403 = new C0004(this);
        /* access modifiers changed from: private */

        /* renamed from: ι  reason: contains not printable characters */
        public CameraCaptureSession f404;
        /* access modifiers changed from: private */

        /* renamed from: ﹳ  reason: contains not printable characters */
        public final C0005 f405 = new C0005(this);

        /* renamed from: ﾞ  reason: contains not printable characters */
        private final C0014 f406 = new C0014(this);

        static {
            SparseIntArray sparseIntArray = new SparseIntArray();
            f385 = sparseIntArray;
            sparseIntArray.append(0, 90);
            f385.append(1, 0);
            f385.append(2, 270);
            f385.append(3, 180);
        }

        /* renamed from: ˋ  reason: contains not printable characters */
        public final boolean m185() {
            return this.f396;
        }

        /* renamed from: ﹺ$ˊ$ˊ  reason: contains not printable characters */
        static class C0068 implements Comparator<Size> {
            C0068() {
            }

            @TargetApi(21)
            public final /* synthetic */ int compare(Object obj, Object obj2) {
                Size size = (Size) obj2;
                Size size2 = (Size) obj;
                return Long.signum((((long) size.getWidth()) * ((long) size.getHeight())) - (((long) size2.getWidth()) * ((long) size2.getHeight())));
            }
        }

        /* renamed from: ﹺ$ˊ$If */
        static class If implements Runnable {

            /* renamed from: ˊ  reason: contains not printable characters */
            private CountDownLatch f407;

            /* renamed from: ˋ  reason: contains not printable characters */
            private final Image f408;

            /* renamed from: ˎ  reason: contains not printable characters */
            private final File f409;

            public If(CountDownLatch countDownLatch, Image image, File file) {
                this.f408 = image;
                this.f409 = file;
                this.f407 = countDownLatch;
            }

            @TargetApi(19)
            public final void run() {
                ByteBuffer buffer = this.f408.getPlanes()[0].getBuffer();
                byte[] bArr = new byte[buffer.remaining()];
                buffer.get(bArr);
                FileOutputStream fileOutputStream = null;
                try {
                    FileOutputStream fileOutputStream2 = new FileOutputStream(this.f409);
                    fileOutputStream = fileOutputStream2;
                    fileOutputStream2.write(bArr);
                    this.f407.countDown();
                    this.f408.close();
                    try {
                        fileOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e2) {
                    e2.printStackTrace();
                    this.f408.close();
                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (IOException e3) {
                            e3.printStackTrace();
                        }
                    }
                } catch (Throwable th) {
                    this.f408.close();
                    if (fileOutputStream != null) {
                        try {
                            fileOutputStream.close();
                        } catch (IOException e4) {
                            e4.printStackTrace();
                        }
                    }
                    throw th;
                }
            }
        }

        @TargetApi(21)
        /* renamed from: ˊ  reason: contains not printable characters */
        private void m170(int i, int i2) {
            Size size;
            CameraManager cameraManager = (CameraManager) LockActivity.m19().getSystemService("camera");
            try {
                String[] cameraIdList = cameraManager.getCameraIdList();
                String str = cameraIdList[0];
                for (String str2 : cameraIdList) {
                    str = str2;
                    if (((Integer) cameraManager.getCameraCharacteristics(str2).get(CameraCharacteristics.LENS_FACING)).intValue() == 0) {
                        break;
                    }
                }
                StreamConfigurationMap streamConfigurationMap = (StreamConfigurationMap) cameraManager.getCameraCharacteristics(str).get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                List asList = Arrays.asList(streamConfigurationMap.getOutputSizes(256));
                Size size2 = (Size) asList.get(0);
                Iterator it = asList.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    Size size3 = (Size) it.next();
                    if (size3.getWidth() <= 800) {
                        size2 = size3;
                        break;
                    }
                }
                this.f391 = ImageReader.newInstance(size2.getWidth(), size2.getHeight(), 256, 2);
                this.f391.setOnImageAvailableListener(this.f403, this.f398);
                Size size4 = size2;
                int i3 = i2;
                int i4 = i;
                Size[] outputSizes = streamConfigurationMap.getOutputSizes(SurfaceTexture.class);
                ArrayList arrayList = new ArrayList();
                int width = size4.getWidth();
                int height = size4.getHeight();
                Size[] sizeArr = outputSizes;
                int length = outputSizes.length;
                for (int i5 = 0; i5 < length; i5++) {
                    Size size5 = sizeArr[i5];
                    if (size5.getHeight() == (size5.getWidth() * height) / width && size5.getWidth() >= i4 && size5.getHeight() >= i3) {
                        arrayList.add(size5);
                    }
                }
                if (arrayList.size() > 0) {
                    size = (Size) Collections.min(arrayList, new C0068());
                } else {
                    size = outputSizes[0];
                }
                this.f388 = size;
                if (this.f389.getResources().getConfiguration().orientation == 2) {
                    this.f389.setAspectRatio(this.f388.getWidth(), this.f388.getHeight());
                } else {
                    this.f389.setAspectRatio(this.f388.getHeight(), this.f388.getWidth());
                }
                this.f387 = str;
            } catch (CameraAccessException | NullPointerException e) {
                e.printStackTrace();
            }
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        public final void m183(ZRuntime zRuntime, C0050 r4) {
            if (!this.f397) {
                this.f397 = true;
                this.f400 = new HandlerThread("CameraBackground");
                this.f400.start();
                this.f398 = new Handler(this.f400.getLooper());
                if (this.f389 == null) {
                    this.f389 = new Cif(this, zRuntime.getContext());
                }
                this.f393 = r4;
                r4.addView(this.f389);
                if (this.f389.isAvailable()) {
                    m172(r4.getWidth(), r4.getHeight());
                } else {
                    this.f389.setSurfaceTextureListener(this.f406);
                }
            }
        }

        @TargetApi(18)
        /* renamed from: ˊ  reason: contains not printable characters */
        public final void m182() {
            if (this.f396) {
                try {
                    this.f397 = false;
                    this.f396 = false;
                    this.f394.acquire();
                    if (this.f404 != null) {
                        this.f404.close();
                        this.f404 = null;
                    }
                    if (this.f399 != null) {
                        this.f399.close();
                        this.f399 = null;
                    }
                    if (this.f391 != null) {
                        this.f391.close();
                        this.f391 = null;
                    }
                    this.f394.release();
                    this.f400.quitSafely();
                    try {
                        this.f400.join();
                        this.f400 = null;
                        this.f398 = null;
                        if (this.f393 != null) {
                            this.f393.removeView(this.f389);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        if (this.f393 != null) {
                            this.f393.removeView(this.f389);
                        }
                    } catch (Throwable th) {
                        if (this.f393 != null) {
                            this.f393.removeView(this.f389);
                        }
                        throw th;
                    }
                } catch (InterruptedException e2) {
                    throw new RuntimeException("Interrupted while trying to lock camera closing.", e2);
                } catch (Throwable th2) {
                    this.f394.release();
                    throw th2;
                }
            }
        }

        /* access modifiers changed from: private */
        @TargetApi(21)
        /* renamed from: ˋ  reason: contains not printable characters */
        public void m172(int i, int i2) {
            m170(i, i2);
            m177(i, i2);
            CameraManager cameraManager = (CameraManager) LockActivity.m19().getSystemService("camera");
            try {
                if (!this.f394.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                    throw new RuntimeException("Time out waiting to lock camera opening.");
                }
                cameraManager.openCamera(this.f387, this.f402, this.f398);
            } catch (CameraAccessException | RuntimeException e) {
                e.printStackTrace();
            } catch (InterruptedException e2) {
                throw new RuntimeException("Interrupted while trying to lock camera opening.", e2);
            }
        }

        /* renamed from: ﹺ$ˊ$if  reason: invalid class name */
        class Cif extends TextureView {

            /* renamed from: ˊ  reason: contains not printable characters */
            private int f410;

            /* renamed from: ˋ  reason: contains not printable characters */
            private int f411;

            public Cif(C0067 r2, Context context) {
                this(r2, context, (byte) 0);
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: ﹺ.ˊ.if.<init>(ﹺ$ˊ, android.content.Context, char):void
             arg types: [ﹺ$ˊ, android.content.Context, int]
             candidates:
              ﹺ.ˊ.if.<init>(ﹺ$ˊ, android.content.Context, byte):void
              ﹺ.ˊ.if.<init>(ﹺ$ˊ, android.content.Context, char):void */
            private Cif(C0067 r2, Context context, byte b) {
                this(context, 0);
            }

            private Cif(Context context, char c) {
                super(context, null, 0);
                this.f410 = 0;
                this.f411 = 0;
            }

            public final void setAspectRatio(int i, int i2) {
                if (i < 0 || i2 < 0) {
                    throw new IllegalArgumentException("Size cannot be negative.");
                }
                this.f410 = i;
                this.f411 = i2;
                requestLayout();
            }

            /* access modifiers changed from: protected */
            public final void onMeasure(int i, int i2) {
                super.onMeasure(i, i2);
                setMeasuredDimension(1, 1);
            }
        }

        /* access modifiers changed from: private */
        @TargetApi(21)
        /* renamed from: ˎ  reason: contains not printable characters */
        public void m177(int i, int i2) {
            LockActivity r4 = LockActivity.m19();
            if (this.f389 != null && r4 != null) {
                int rotation = r4.getWindowManager().getDefaultDisplay().getRotation();
                Matrix matrix = new Matrix();
                RectF rectF = new RectF(0.0f, 0.0f, (float) i, (float) i2);
                RectF rectF2 = new RectF(0.0f, 0.0f, (float) this.f388.getHeight(), (float) this.f388.getWidth());
                float centerX = rectF.centerX();
                float centerY = rectF.centerY();
                if (1 == rotation || 3 == rotation) {
                    rectF2.offset(centerX - rectF2.centerX(), centerY - rectF2.centerY());
                    matrix.setRectToRect(rectF, rectF2, Matrix.ScaleToFit.FILL);
                    float max = Math.max((float) i2, (float) i);
                    matrix.postScale(max, max, centerX, centerY);
                    matrix.postRotate((float) ((rotation - 2) * 90), centerX, centerY);
                }
                this.f389.setTransform(matrix);
            }
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        public final boolean m184(File file) {
            try {
                this.f386 = file;
                this.f401 = new CountDownLatch(1);
                LockActivity r6 = LockActivity.m19();
                if (r6 != null) {
                    r6.runOnUiThread(new C0027(this, this));
                }
                return this.f401.await(30, TimeUnit.SECONDS);
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        /* renamed from: ˋ  reason: contains not printable characters */
        static /* synthetic */ void m173(C0067 r5) {
            try {
                SurfaceTexture surfaceTexture = r5.f389.getSurfaceTexture();
                if (f384 || surfaceTexture != null) {
                    surfaceTexture.setDefaultBufferSize(r5.f388.getWidth(), r5.f388.getHeight());
                    Surface surface = new Surface(surfaceTexture);
                    r5.f390 = r5.f399.createCaptureRequest(1);
                    r5.f390.addTarget(surface);
                    r5.f399.createCaptureSession(Arrays.asList(surface, r5.f391.getSurface()), new C0015(r5), null);
                    return;
                }
                throw new AssertionError();
            } catch (Exception e) {
                e.printStackTrace();
                r5.m182();
            }
        }

        /* renamed from: ʽ  reason: contains not printable characters */
        static /* synthetic */ void m160(C0067 r5) {
            try {
                LockActivity r3 = LockActivity.m19();
                if (r3 != null && r5.f399 != null) {
                    CaptureRequest.Builder createCaptureRequest = r5.f399.createCaptureRequest(2);
                    createCaptureRequest.addTarget(r5.f391.getSurface());
                    createCaptureRequest.set(CaptureRequest.CONTROL_AF_MODE, 4);
                    createCaptureRequest.set(CaptureRequest.CONTROL_AE_MODE, 2);
                    createCaptureRequest.set(CaptureRequest.JPEG_ORIENTATION, Integer.valueOf(f385.get(r3.getWindowManager().getDefaultDisplay().getRotation())));
                    C0025 r32 = new C0025(r5);
                    if (r5.f404 != null) {
                        r5.f404.stopRepeating();
                        r5.f404.capture(createCaptureRequest.build(), r32, null);
                    }
                }
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }

        /* renamed from: ͺ  reason: contains not printable characters */
        static /* synthetic */ void m179(C0067 r4) {
            try {
                r4.f390.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER, 1);
                r4.f395 = 2;
                if (r4.f404 != null) {
                    r4.f404.capture(r4.f390.build(), r4.f405, r4.f398);
                }
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }

        /* renamed from: ˌ  reason: contains not printable characters */
        static /* synthetic */ void m175(C0067 r4) {
            try {
                r4.f390.set(CaptureRequest.CONTROL_AF_TRIGGER, 2);
                r4.f390.set(CaptureRequest.CONTROL_AE_MODE, 2);
                r4.f404.capture(r4.f390.build(), r4.f405, r4.f398);
                r4.f395 = 0;
                r4.f404.setRepeatingRequest(r4.f392, r4.f405, r4.f398);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public C0066() {
        if (Build.VERSION.SDK_INT >= 21) {
            this.f372 = new C0067();
        } else {
            this.f372 = new If();
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final synchronized boolean m146(C0050 r5) {
        try {
            if (!((Boolean) If$.m13("CoN").getMethod("ˋ", null).invoke(this.f373.getSystemUtils(), null)).booleanValue()) {
                return false;
            }
            this.f374 = r5;
            this.f372.m155(this.f373, this.f374);
        } catch (Exception e) {
            e.printStackTrace();
        } catch (Throwable th) {
            throw th.getCause();
        }
        return true;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final synchronized void m145() {
        try {
            this.f372.m154();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
