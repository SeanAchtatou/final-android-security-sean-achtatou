import java.io.ByteArrayInputStream;

/* renamed from: ᐟ  reason: contains not printable characters */
public class C0037 {

    /* renamed from: ˉ  reason: contains not printable characters */
    public byte[] f169;

    /* renamed from: ˊ  reason: contains not printable characters */
    private int f170;

    /* renamed from: ˋ  reason: contains not printable characters */
    private boolean f171;

    /* renamed from: ˌ  reason: contains not printable characters */
    public ByteArrayInputStream f172;

    /* renamed from: ˍ  reason: contains not printable characters */
    public int f173;

    /* renamed from: ˑ  reason: contains not printable characters */
    public int f174;

    /* renamed from: ـ  reason: contains not printable characters */
    public int f175;

    /* renamed from: ᐧ  reason: contains not printable characters */
    public int f176;

    /* renamed from: ᐨ  reason: contains not printable characters */
    public int f177;

    /* renamed from: ﹳ  reason: contains not printable characters */
    public int f178;

    /* renamed from: ﾞ  reason: contains not printable characters */
    public int f179;

    /* renamed from: ˎ  reason: contains not printable characters */
    private void m89() {
        if (!this.f171) {
            while (true) {
                int i = ((0 - this.f174) + this.f175) - this.f179;
                int i2 = i;
                if (i != 0) {
                    int read = this.f172.read(this.f169, this.f174 + this.f179, i2);
                    int i3 = read;
                    if (read == -1) {
                        this.f170 = this.f179;
                        if (this.f174 + this.f170 > this.f173) {
                            this.f170 = this.f173 - this.f174;
                        }
                        this.f171 = true;
                        return;
                    }
                    this.f179 += i3;
                    if (this.f179 >= this.f176 + this.f178) {
                        this.f170 = this.f179 - this.f178;
                    }
                } else {
                    return;
                }
            }
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public void m91() {
        this.f174 = 0;
        this.f176 = 0;
        this.f179 = 0;
        this.f171 = false;
        m89();
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public void m93() {
        this.f176++;
        if (this.f176 > this.f170) {
            if (this.f174 + this.f176 > this.f173) {
                int i = (this.f174 + this.f176) - this.f177;
                int i2 = i;
                if (i > 0) {
                    i2--;
                }
                int i3 = (this.f174 + this.f179) - i2;
                for (int i4 = 0; i4 < i3; i4++) {
                    byte[] bArr = this.f169;
                    bArr[i4] = bArr[i2 + i4];
                }
                this.f174 -= i2;
            }
            m89();
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final int m90(int i, int i2, int i3) {
        if (this.f171 && this.f176 + i + i3 > this.f179) {
            i3 = this.f179 - (this.f176 + i);
        }
        int i4 = i2 + 1;
        int i5 = i + this.f174 + this.f176;
        int i6 = 0;
        while (i6 < i3 && this.f169[i5 + i6] == this.f169[(i5 + i6) - i4]) {
            i6++;
        }
        return i6;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m92(int i) {
        this.f174 += i;
        this.f170 -= i;
        this.f176 -= i;
        this.f179 -= i;
    }
}
