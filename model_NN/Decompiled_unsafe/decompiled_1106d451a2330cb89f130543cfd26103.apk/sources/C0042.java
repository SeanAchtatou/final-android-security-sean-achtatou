import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/* renamed from: ᐪ  reason: contains not printable characters */
public final class C0042 {

    /* renamed from: ʻ  reason: contains not printable characters */
    private short[] f191 = new short[12];

    /* renamed from: ʼ  reason: contains not printable characters */
    private short[] f192 = new short[12];

    /* renamed from: ʽ  reason: contains not printable characters */
    private short[] f193 = new short[192];

    /* renamed from: ʾ  reason: contains not printable characters */
    private C0047 f194 = new C0047(4);

    /* renamed from: ʿ  reason: contains not printable characters */
    private Cif f195 = new Cif();

    /* renamed from: ˈ  reason: contains not printable characters */
    private Cif f196 = new Cif();

    /* renamed from: ˉ  reason: contains not printable characters */
    private C0043 f197 = new C0043();

    /* renamed from: ˊ  reason: contains not printable characters */
    private C0039 f198 = new C0039();

    /* renamed from: ˋ  reason: contains not printable characters */
    private C0049 f199 = new C0049();

    /* renamed from: ˌ  reason: contains not printable characters */
    private int f200 = -1;

    /* renamed from: ˍ  reason: contains not printable characters */
    private int f201 = -1;

    /* renamed from: ˎ  reason: contains not printable characters */
    private short[] f202 = new short[192];

    /* renamed from: ˏ  reason: contains not printable characters */
    private short[] f203 = new short[12];

    /* renamed from: ˑ  reason: contains not printable characters */
    private int f204;

    /* renamed from: ͺ  reason: contains not printable characters */
    private C0047[] f205 = new C0047[4];

    /* renamed from: ᐝ  reason: contains not printable characters */
    private short[] f206 = new short[12];

    /* renamed from: ι  reason: contains not printable characters */
    private short[] f207 = new short[114];

    /* renamed from: ᐪ$if  reason: invalid class name */
    class Cif {

        /* renamed from: ˊ  reason: contains not printable characters */
        private short[] f209 = new short[2];

        /* renamed from: ˋ  reason: contains not printable characters */
        private C0047[] f210 = new C0047[16];

        /* renamed from: ˎ  reason: contains not printable characters */
        private C0047[] f211 = new C0047[16];

        /* renamed from: ˏ  reason: contains not printable characters */
        private C0047 f212 = new C0047(8);

        /* renamed from: ᐝ  reason: contains not printable characters */
        private int f213 = 0;

        Cif() {
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        public final void m99(int i) {
            while (this.f213 < i) {
                this.f210[this.f213] = new C0047(3);
                this.f211[this.f213] = new C0047(3);
                this.f213++;
            }
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        public final void m98() {
            short[] sArr = this.f209;
            for (int i = 0; i < sArr.length; i++) {
                sArr[i] = 1024;
            }
            for (int i2 = 0; i2 < this.f213; i2++) {
                short[] sArr2 = this.f210[i2].f304;
                for (int i3 = 0; i3 < sArr2.length; i3++) {
                    sArr2[i3] = 1024;
                }
                short[] sArr3 = this.f211[i2].f304;
                for (int i4 = 0; i4 < sArr3.length; i4++) {
                    sArr3[i4] = 1024;
                }
            }
            short[] sArr4 = this.f212.f304;
            for (int i5 = 0; i5 < sArr4.length; i5++) {
                sArr4[i5] = 1024;
            }
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        public final int m97(C0049 r3, int i) {
            if (r3.m126(this.f209, 0) == 0) {
                return this.f210[i].m120(r3);
            }
            if (r3.m126(this.f209, 1) == 0) {
                return this.f211[i].m120(r3) + 8;
            }
            return this.f212.m120(r3) + 8 + 8;
        }
    }

    /* renamed from: ᐪ$ˊ  reason: contains not printable characters */
    class C0043 {

        /* renamed from: ˊ  reason: contains not printable characters */
        Cif[] f214;

        /* renamed from: ˋ  reason: contains not printable characters */
        int f215;

        /* renamed from: ˎ  reason: contains not printable characters */
        int f216;

        /* renamed from: ˏ  reason: contains not printable characters */
        int f217;

        C0043() {
        }

        /* renamed from: ᐪ$ˊ$if  reason: invalid class name */
        class Cif {

            /* renamed from: ˊ  reason: contains not printable characters */
            short[] f219 = new short[768];

            Cif() {
            }
        }
    }

    public C0042() {
        for (int i = 0; i < 4; i++) {
            this.f205[i] = new C0047(6);
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final boolean m95(ByteArrayInputStream byteArrayInputStream, ByteArrayOutputStream byteArrayOutputStream, long j) {
        int i;
        int i2;
        int i3;
        int r0;
        this.f199.f310 = byteArrayInputStream;
        C0039 r16 = this.f198;
        r16.m94();
        r16.f186 = null;
        r16.f186 = byteArrayOutputStream;
        C0039 r13 = this.f198;
        r13.f185 = 0;
        r13.f183 = 0;
        short[] sArr = this.f202;
        for (int i4 = 0; i4 < sArr.length; i4++) {
            sArr[i4] = 1024;
        }
        short[] sArr2 = this.f193;
        for (int i5 = 0; i5 < sArr2.length; i5++) {
            sArr2[i5] = 1024;
        }
        short[] sArr3 = this.f203;
        for (int i6 = 0; i6 < sArr3.length; i6++) {
            sArr3[i6] = 1024;
        }
        short[] sArr4 = this.f206;
        for (int i7 = 0; i7 < sArr4.length; i7++) {
            sArr4[i7] = 1024;
        }
        short[] sArr5 = this.f191;
        for (int i8 = 0; i8 < sArr5.length; i8++) {
            sArr5[i8] = 1024;
        }
        short[] sArr6 = this.f192;
        for (int i9 = 0; i9 < sArr6.length; i9++) {
            sArr6[i9] = 1024;
        }
        short[] sArr7 = this.f207;
        for (int i10 = 0; i10 < sArr7.length; i10++) {
            sArr7[i10] = 1024;
        }
        C0043 r132 = this.f197;
        int i11 = 1 << (r132.f215 + r132.f216);
        for (int i12 = 0; i12 < i11; i12++) {
            short[] sArr8 = r132.f214[i12].f219;
            for (int i13 = 0; i13 < sArr8.length; i13++) {
                sArr8[i13] = 1024;
            }
        }
        for (int i14 = 0; i14 < 4; i14++) {
            short[] sArr9 = this.f205[i14].f304;
            for (int i15 = 0; i15 < sArr9.length; i15++) {
                sArr9[i15] = 1024;
            }
        }
        this.f195.m98();
        this.f196.m98();
        short[] sArr10 = this.f194.f304;
        for (int i16 = 0; i16 < sArr10.length; i16++) {
            sArr10[i16] = 1024;
        }
        C0049 r133 = this.f199;
        r133.f309 = 0;
        r133.f308 = -1;
        for (int i17 = 0; i17 < 5; i17++) {
            r133.f309 = (r133.f309 << 8) | r133.f310.read();
        }
        int i18 = 0;
        int i19 = 0;
        int i20 = 0;
        int i21 = 0;
        int i22 = 0;
        long j2 = 0;
        byte b = 0;
        while (true) {
            if (j >= 0 && j2 >= j) {
                break;
            }
            int i23 = ((int) j2) & this.f204;
            if (this.f199.m126(this.f202, (i18 << 4) + i23) == 0) {
                C0043 r9 = this.f197;
                C0043.Cif ifVar = r9.f214[((r9.f217 & ((int) j2)) << r9.f215) + ((b & 255) >>> (8 - r9.f215))];
                if (!(i18 < 7)) {
                    C0043.Cif ifVar2 = ifVar;
                    C0049 r12 = this.f199;
                    C0039 r162 = this.f198;
                    int i24 = (r162.f183 - i19) - 1;
                    int i25 = i24;
                    if (i24 < 0) {
                        i25 += r162.f184;
                    }
                    byte b2 = r162.f182[i25];
                    int i26 = 1;
                    while (true) {
                        int i27 = (b2 >> 7) & 1;
                        b2 = (byte) (b2 << 1);
                        int r163 = r12.m126(ifVar2.f219, ((i27 + 1) << 8) + i26);
                        i26 = (i26 << 1) | r163;
                        if (i27 == r163) {
                            if (i26 >= 256) {
                                break;
                            }
                        } else {
                            while (i26 < 256) {
                                i26 = (i26 << 1) | r12.m126(ifVar2.f219, i26);
                            }
                        }
                    }
                    b = (byte) i26;
                } else {
                    C0049 r122 = this.f199;
                    C0043.Cif ifVar3 = ifVar;
                    int i28 = 1;
                    do {
                        r0 = (i28 << 1) | r122.m126(ifVar3.f219, i28);
                        i28 = r0;
                    } while (r0 < 256);
                    b = (byte) i28;
                }
                C0039 r92 = this.f198;
                byte[] bArr = r92.f182;
                int i29 = r92.f183;
                r92.f183 = i29 + 1;
                bArr[i29] = b;
                if (r92.f183 >= r92.f184) {
                    r92.m94();
                }
                int i30 = i18;
                if (i18 < 4) {
                    i18 = 0;
                } else if (i30 < 10) {
                    i18 = i30 - 3;
                } else {
                    i18 = i30 - 6;
                }
                j2++;
            } else {
                if (this.f199.m126(this.f203, i18) == 1) {
                    i = 0;
                    if (this.f199.m126(this.f206, i18) != 0) {
                        if (this.f199.m126(this.f191, i18) == 0) {
                            i3 = i20;
                        } else {
                            if (this.f199.m126(this.f192, i18) == 0) {
                                i3 = i21;
                            } else {
                                i3 = i22;
                                i22 = i21;
                            }
                            i21 = i20;
                        }
                        i20 = i19;
                        i19 = i3;
                    } else if (this.f199.m126(this.f193, (i18 << 4) + i23) == 0) {
                        i18 = i18 < 7 ? 9 : 11;
                        i = 1;
                    }
                    if (i == 0) {
                        i = this.f196.m97(this.f199, i23) + 2;
                        i18 = i18 < 7 ? 8 : 11;
                    }
                } else {
                    i22 = i21;
                    i21 = i20;
                    i20 = i19;
                    i = this.f195.m97(this.f199, i23) + 2;
                    i18 = i18 < 7 ? 7 : 10;
                    C0047[] r02 = this.f205;
                    int i31 = i - 2;
                    if (i31 < 4) {
                        i2 = i31;
                    } else {
                        i2 = 3;
                    }
                    int r03 = r02[i2].m120(this.f199);
                    int i32 = r03;
                    if (r03 >= 4) {
                        int i33 = (i32 >> 1) - 1;
                        int i34 = ((i32 & 1) | 2) << i33;
                        if (i32 < 14) {
                            int i35 = i34;
                            short[] sArr11 = this.f207;
                            int i36 = (i34 - i32) - 1;
                            C0049 r134 = this.f199;
                            int i37 = i33;
                            short[] sArr12 = sArr11;
                            int i38 = 1;
                            int i39 = 0;
                            for (int i40 = 0; i40 < i37; i40++) {
                                int r17 = r134.m126(sArr12, i36 + i38);
                                i38 = (i38 << 1) + r17;
                                i39 |= r17 << i40;
                            }
                            i19 = i35 + i39;
                        } else {
                            int i41 = i33 - 4;
                            C0049 r93 = this.f199;
                            int i42 = 0;
                            for (int i43 = i41; i43 != 0; i43--) {
                                r93.f308 >>>= 1;
                                int i44 = (r93.f309 - r93.f308) >>> 31;
                                r93.f309 -= r93.f308 & (i44 - 1);
                                i42 = (i42 << 1) | (1 - i44);
                                if ((r93.f308 & -16777216) == 0) {
                                    r93.f309 = (r93.f309 << 8) | r93.f310.read();
                                    r93.f308 <<= 8;
                                }
                            }
                            int i45 = (i42 << 4) + i34;
                            C0047 r94 = this.f194;
                            C0049 r123 = this.f199;
                            int i46 = 1;
                            int i47 = 0;
                            for (int i48 = 0; i48 < r94.f305; i48++) {
                                int r164 = r123.m126(r94.f304, i46);
                                i46 = (i46 << 1) + r164;
                                i47 |= r164 << i48;
                            }
                            int i49 = i45 + i47;
                            i19 = i49;
                            if (i49 < 0) {
                                if (i19 != -1) {
                                    return false;
                                }
                            }
                        }
                    } else {
                        i19 = i32;
                    }
                }
                if (((long) i19) >= j2 || i19 >= this.f201) {
                    return false;
                }
                C0039 r95 = this.f198;
                int i50 = (r95.f183 - i19) - 1;
                int i51 = i50;
                if (i50 < 0) {
                    i51 += r95.f184;
                }
                for (int i52 = i; i52 != 0; i52--) {
                    if (i51 >= r95.f184) {
                        i51 = 0;
                    }
                    byte[] bArr2 = r95.f182;
                    int i53 = r95.f183;
                    r95.f183 = i53 + 1;
                    int i54 = i51;
                    i51++;
                    bArr2[i53] = r95.f182[i54];
                    if (r95.f183 >= r95.f184) {
                        r95.m94();
                    }
                }
                j2 += (long) i;
                C0039 r165 = this.f198;
                int i55 = (r165.f183 + 0) - 1;
                int i56 = i55;
                if (i55 < 0) {
                    i56 += r165.f184;
                }
                b = r165.f182[i56];
            }
        }
        this.f198.m94();
        C0039 r166 = this.f198;
        r166.m94();
        r166.f186 = null;
        this.f199.f310 = null;
        return true;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final boolean m96(byte[] bArr) {
        boolean z;
        boolean z2;
        byte b = bArr[0] & 255;
        int i = b % 9;
        int i2 = b / 9;
        int i3 = i2 % 5;
        int i4 = i2 / 5;
        int i5 = 0;
        for (int i6 = 0; i6 < 4; i6++) {
            i5 += (bArr[i6 + 1] & 255) << (i6 * 8);
        }
        int i7 = i;
        int i8 = i3;
        int i9 = i4;
        int i10 = i8;
        int i11 = i7;
        if (i11 > 8 || i10 > 4 || i9 > 4) {
            z = false;
        } else {
            C0043 r0 = this.f197;
            int i12 = i11;
            C0043 r2 = r0;
            if (!(r0.f214 != null && r2.f215 == i12 && r2.f216 == i10)) {
                r2.f216 = i10;
                r2.f217 = (1 << i10) - 1;
                r2.f215 = i12;
                int i13 = 1 << (r2.f215 + r2.f216);
                r2.f214 = new C0043.Cif[i13];
                for (int i14 = 0; i14 < i13; i14++) {
                    r2.f214[i14] = new C0043.Cif();
                }
            }
            int i15 = 1 << i9;
            this.f195.m99(i15);
            this.f196.m99(i15);
            this.f204 = i15 - 1;
            z = true;
        }
        if (!z) {
            return false;
        }
        int i16 = i5;
        if (i16 < 0) {
            z2 = false;
        } else {
            if (this.f200 != i16) {
                this.f200 = i16;
                this.f201 = Math.max(this.f200, 1);
                C0039 r3 = this.f198;
                int max = Math.max(this.f201, 4096);
                if (r3.f182 == null || r3.f184 != max) {
                    r3.f182 = new byte[max];
                }
                r3.f184 = max;
                r3.f183 = 0;
                r3.f185 = 0;
            }
            z2 = true;
        }
        return z2;
    }
}
