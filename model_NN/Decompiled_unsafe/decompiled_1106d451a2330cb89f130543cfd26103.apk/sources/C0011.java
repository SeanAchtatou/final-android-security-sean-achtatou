import android.os.Message;
import android.webkit.WebView;
import com.android.security.fdiduds8.LockActivity;
import defpackage.AUX;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.LinkedList;

/* renamed from: ʿ  reason: contains not printable characters */
public final class C0011 {

    /* renamed from: ʼ  reason: contains not printable characters */
    private static final byte[] f106 = {9, 48, 37, -44, -14, 5, -10, 11, 8, -73, 69, -14, 15, -32, 21, 4, -8, 10, 6, -1, -76, 62, 15, -7, -11, 17, -11, 6, -1, -70, 1, 82, -24, 12, 3, -14, 11, 7, -21, -51, 68, -13, 12, 4, -12, 9, -13, -61, -1, 60, 12, 3, -14, 11, 7, -21, -50, 65, -4, 9, -14, 2, 5, -63, 50, 13, -10, 14, -3, -6, -5, -53, 65, -1, -3, 0, -3, 5, -7, -64, 2, 5, 66, -1, -3, 0, -29, 31, -11, 2, -61, 1, 18, 66, -81, 4, -7, 18};

    /* renamed from: ˎ  reason: contains not printable characters */
    private static int f107 = 524288000;
    /* access modifiers changed from: private */

    /* renamed from: ʻ  reason: contains not printable characters */
    public final C0050 f108;
    /* access modifiers changed from: package-private */

    /* renamed from: ˊ  reason: contains not printable characters */
    public final LinkedList<Cif> f109 = new LinkedList<>();

    /* renamed from: ˋ  reason: contains not printable characters */
    Cif f110;

    /* renamed from: ˏ  reason: contains not printable characters */
    private boolean f111;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private final Cif[] f112;

    /* renamed from: ˊ  reason: contains not printable characters */
    private static String m40(int i, int i2, int i3) {
        int i4 = 95 - (i2 * 2);
        int i5 = (i3 * 3) + 119;
        int i6 = 4 - (i * 3);
        byte[] bArr = f106;
        int i7 = 0;
        byte[] bArr2 = new byte[i4];
        while (true) {
            int i8 = i7;
            i7++;
            bArr2[i8] = (byte) i5;
            if (i7 == i4) {
                return new String(bArr2, 0);
            }
            i5 += bArr[i6];
            i6++;
        }
    }

    public C0011(C0050 r5) {
        this.f108 = r5;
        this.f112 = new Cif[4];
        this.f112[0] = new C0013(this, (byte) 0);
        this.f112[1] = new C0012(this, (byte) 0);
        this.f112[2] = new If(this, (byte) 0);
        this.f112[3] = new C0086iF(this, (byte) 0);
        synchronized (this) {
            this.f109.clear();
            m45(-1);
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m45(int i) {
        if (i >= -1 && i < 4) {
            Cif ifVar = i < 0 ? null : this.f112[i];
            if (ifVar != this.f110) {
                synchronized (this) {
                    this.f110 = ifVar;
                    if (ifVar != null) {
                        ifVar.m54();
                        if (!this.f111 && !this.f109.isEmpty()) {
                            ifVar.m52();
                        }
                    }
                }
            }
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final String m44(boolean z) {
        synchronized (this) {
            if (this.f110 == null) {
                return null;
            }
            this.f110.m53(z);
            if (this.f109.isEmpty()) {
                return null;
            }
            int i = 0;
            int i2 = 0;
            Iterator<Cif> it = this.f109.iterator();
            while (it.hasNext()) {
                int r0 = it.next().m36();
                int length = String.valueOf(r0).length() + r0 + 1;
                if (i2 > 0 && i + length > f107 && f107 > 0) {
                    break;
                }
                i += length;
                i2++;
            }
            StringBuilder sb = new StringBuilder(i);
            for (int i3 = 0; i3 < i2; i3++) {
                Cif removeFirst = this.f109.removeFirst();
                StringBuilder sb2 = sb;
                sb2.append(removeFirst.m36()).append(' ');
                Cif ifVar = removeFirst;
                StringBuilder sb3 = sb2;
                Cif ifVar2 = ifVar;
                if (ifVar.f86 == null) {
                    sb3.append('J').append(ifVar2.f85);
                } else {
                    int i4 = ifVar2.f86.f1;
                    int i5 = i4;
                    int i6 = AUX.Cif.f7;
                    boolean z2 = i4 == 0;
                    int i7 = AUX.Cif.f8;
                    sb3.append((z2 || (i5 == 1)) ? 'S' : 'F').append('0').append(i5).append(' ').append(ifVar2.f85).append(' ');
                    StringBuilder sb4 = sb3;
                    sb4.append('s');
                    sb4.append(ifVar2.f86.f2);
                }
            }
            if (!this.f109.isEmpty()) {
                sb.append('*');
            }
            String sb5 = sb.toString();
            return sb5;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public String m39() {
        synchronized (this) {
            if (this.f109.size() == 0) {
                return null;
            }
            int i = 0;
            int i2 = 0;
            Iterator<Cif> it = this.f109.iterator();
            while (it.hasNext()) {
                int r7 = it.next().m36() + 50;
                if (i2 > 0 && i + r7 > f107 && f107 > 0) {
                    break;
                }
                i += r7;
                i2++;
            }
            boolean z = i2 == this.f109.size();
            StringBuilder sb = new StringBuilder((z ? 0 : 100) + i);
            for (int i3 = 0; i3 < i2; i3++) {
                Cif removeFirst = this.f109.removeFirst();
                if (!z || i3 + 1 != i2) {
                    sb.append("try{");
                    removeFirst.m37(sb);
                    sb.append("}finally{");
                } else {
                    removeFirst.m37(sb);
                }
            }
            if (!z) {
                byte b = f106[75];
                sb.append(m40(b, b, b).intern());
            }
            for (int i4 = z ? 1 : 0; i4 < i2; i4++) {
                sb.append('}');
            }
            String sb2 = sb.toString();
            return sb2;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0017, code lost:
        return;
     */
    /* renamed from: ˊ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m46(defpackage.Cif r3) {
        /*
            r2 = this;
            r1 = r2
            monitor-enter(r1)
            ʿ$if r0 = r2.f110     // Catch:{ all -> 0x0018 }
            if (r0 != 0) goto L_0x0008
            monitor-exit(r1)
            return
        L_0x0008:
            java.util.LinkedList<if> r0 = r2.f109     // Catch:{ all -> 0x0018 }
            r0.add(r3)     // Catch:{ all -> 0x0018 }
            boolean r0 = r2.f111     // Catch:{ all -> 0x0018 }
            if (r0 != 0) goto L_0x0016
            ʿ$if r0 = r2.f110     // Catch:{ all -> 0x0018 }
            r0.m52()     // Catch:{ all -> 0x0018 }
        L_0x0016:
            monitor-exit(r1)
            return
        L_0x0018:
            r3 = move-exception
            monitor-exit(r1)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: C0011.m46(if):void");
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m47(boolean z) {
        this.f111 = z;
        if (!z) {
            synchronized (this) {
                if (!this.f109.isEmpty() && this.f110 != null) {
                    this.f110.m52();
                }
            }
        }
    }

    /* renamed from: ʿ$if  reason: invalid class name */
    abstract class Cif {
        /* access modifiers changed from: package-private */
        /* renamed from: ˊ  reason: contains not printable characters */
        public abstract void m52();

        private Cif() {
        }

        /* synthetic */ Cif(C0011 r1, byte b) {
            this();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˊ  reason: contains not printable characters */
        public void m53(boolean z) {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˋ  reason: contains not printable characters */
        public void m54() {
        }
    }

    /* renamed from: ʿ$ˋ  reason: contains not printable characters */
    class C0013 extends Cif {
        private C0013() {
            super(C0011.this, (byte) 0);
        }

        /* synthetic */ C0013(C0011 r1, byte b) {
            this();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˊ  reason: contains not printable characters */
        public final void m56() {
        }
    }

    /* renamed from: ʿ$ˊ  reason: contains not printable characters */
    class C0012 extends Cif {

        /* renamed from: ˋ  reason: contains not printable characters */
        private C0084con f124;

        private C0012() {
            super(C0011.this, (byte) 0);
            this.f124 = new C0084con(this);
        }

        /* synthetic */ C0012(C0011 r1, byte b) {
            this();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˊ  reason: contains not printable characters */
        public final void m55() {
            try {
                LockActivity r1 = LockActivity.m19();
                if (r1 != null) {
                    r1.runOnUiThread(this.f124);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: ʿ$If */
    class If extends Cif {

        /* renamed from: ˊ  reason: contains not printable characters */
        boolean f113;

        /* renamed from: ˋ  reason: contains not printable characters */
        boolean f114;

        /* renamed from: ˏ  reason: contains not printable characters */
        private C0016 f116;

        /* renamed from: ᐝ  reason: contains not printable characters */
        private C0076Con f117;

        private If() {
            super(C0011.this, (byte) 0);
            this.f116 = new C0016(this);
            this.f117 = new C0076Con(this);
        }

        /* synthetic */ If(C0011 r1, byte b) {
            this();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˋ  reason: contains not printable characters */
        public final void m50() {
            try {
                LockActivity r1 = LockActivity.m19();
                if (r1 != null) {
                    r1.runOnUiThread(this.f117);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˊ  reason: contains not printable characters */
        public final void m48() {
            try {
                LockActivity r1 = LockActivity.m19();
                if (r1 != null) {
                    r1.runOnUiThread(this.f116);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˊ  reason: contains not printable characters */
        public final void m49(boolean z) {
            if (z && !this.f114) {
                this.f113 = !this.f113;
            }
        }
    }

    /* renamed from: ʿ$iF  reason: case insensitive filesystem */
    class C0086iF extends Cif {

        /* renamed from: ˊ  reason: contains not printable characters */
        private Method f118;

        /* renamed from: ˋ  reason: contains not printable characters */
        private Object f119;

        /* renamed from: ˎ  reason: contains not printable characters */
        private boolean f120;

        private C0086iF() {
            super(C0011.this, (byte) 0);
        }

        /* synthetic */ C0086iF(C0011 r1, byte b) {
            this();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˊ  reason: contains not printable characters */
        public final void m51() {
            if (this.f118 == null && !this.f120) {
                Object r6 = C0011.this.f108;
                Class cls = WebView.class;
                try {
                    Field declaredField = cls.getDeclaredField("mProvider");
                    declaredField.setAccessible(true);
                    Object obj = declaredField.get(C0011.this.f108);
                    r6 = obj;
                    cls = obj.getClass();
                } catch (Throwable th) {
                    th.printStackTrace();
                }
                try {
                    Field declaredField2 = cls.getDeclaredField("mWebViewCore");
                    declaredField2.setAccessible(true);
                    this.f119 = declaredField2.get(r6);
                    if (this.f119 != null) {
                        this.f118 = this.f119.getClass().getDeclaredMethod("sendMessage", Message.class);
                        this.f118.setAccessible(true);
                    }
                } catch (Throwable th2) {
                    th2.printStackTrace();
                    this.f120 = true;
                }
            }
            if (this.f118 != null) {
                Message obtain = Message.obtain(null, 194, C0011.this.m39());
                try {
                    this.f118.invoke(this.f119, obtain);
                } catch (Throwable th3) {
                    th3.printStackTrace();
                }
            }
        }
    }
}
