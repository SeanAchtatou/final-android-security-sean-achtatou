import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/* renamed from: ᒽ  reason: contains not printable characters */
public final class C0044 {

    /* renamed from: ˊ  reason: contains not printable characters */
    private static byte[] f222 = new byte[2048];

    /* renamed from: ʳ  reason: contains not printable characters */
    private int f223 = 0;

    /* renamed from: ʴ  reason: contains not printable characters */
    private int f224 = 3;

    /* renamed from: ʹ  reason: contains not printable characters */
    private int f225;

    /* renamed from: ʻ  reason: contains not printable characters */
    private C0063 f226 = null;

    /* renamed from: ʼ  reason: contains not printable characters */
    private C0051 f227 = new C0051();

    /* renamed from: ʽ  reason: contains not printable characters */
    private short[] f228 = new short[192];

    /* renamed from: ʾ  reason: contains not printable characters */
    private short[] f229 = new short[12];

    /* renamed from: ʿ  reason: contains not printable characters */
    private short[] f230 = new short[12];

    /* renamed from: ˆ  reason: contains not printable characters */
    private int f231 = 4194304;

    /* renamed from: ˇ  reason: contains not printable characters */
    private int f232 = -1;

    /* renamed from: ˈ  reason: contains not printable characters */
    private short[] f233 = new short[192];

    /* renamed from: ˉ  reason: contains not printable characters */
    private C0048[] f234 = new C0048[4];

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f235 = 0;

    /* renamed from: ˌ  reason: contains not printable characters */
    private short[] f236 = new short[114];

    /* renamed from: ˍ  reason: contains not printable characters */
    private C0048 f237 = new C0048(4);

    /* renamed from: ˎ  reason: contains not printable characters */
    private byte f238;

    /* renamed from: ˏ  reason: contains not printable characters */
    private int[] f239 = new int[4];

    /* renamed from: ˑ  reason: contains not printable characters */
    private C0045 f240 = new C0045();

    /* renamed from: ˡ  reason: contains not printable characters */
    private int f241 = -1;

    /* renamed from: ˮ  reason: contains not printable characters */
    private long f242;

    /* renamed from: ͺ  reason: contains not printable characters */
    private short[] f243 = new short[12];

    /* renamed from: ՙ  reason: contains not printable characters */
    private int f244;

    /* renamed from: י  reason: contains not printable characters */
    private int f245;

    /* renamed from: ـ  reason: contains not printable characters */
    private C0045 f246 = new C0045();

    /* renamed from: ٴ  reason: contains not printable characters */
    private int f247;

    /* renamed from: ۥ  reason: contains not printable characters */
    private boolean f248;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private C0046[] f249 = new C0046[4096];

    /* renamed from: ᐟ  reason: contains not printable characters */
    private byte[] f250 = new byte[5];

    /* renamed from: ᐠ  reason: contains not printable characters */
    private ByteArrayInputStream f251;

    /* renamed from: ᐡ  reason: contains not printable characters */
    private int[] f252 = new int[128];

    /* renamed from: ᐣ  reason: contains not printable characters */
    private int f253 = 1;

    /* renamed from: ᐧ  reason: contains not printable characters */
    private If f254 = new If();

    /* renamed from: ᐨ  reason: contains not printable characters */
    private int[] f255 = new int[548];

    /* renamed from: ᐩ  reason: contains not printable characters */
    private boolean f256 = false;

    /* renamed from: ᐪ  reason: contains not printable characters */
    private int f257;

    /* renamed from: ᑊ  reason: contains not printable characters */
    private int[] f258 = new int[4];

    /* renamed from: ᕀ  reason: contains not printable characters */
    private int[] f259 = new int[4];

    /* renamed from: ᴵ  reason: contains not printable characters */
    private boolean f260;

    /* renamed from: ᵎ  reason: contains not printable characters */
    private int[] f261 = new int[256];

    /* renamed from: ᵔ  reason: contains not printable characters */
    private int[] f262 = new int[512];

    /* renamed from: ᵕ  reason: contains not printable characters */
    private int f263;

    /* renamed from: ᵢ  reason: contains not printable characters */
    private int[] f264 = new int[16];

    /* renamed from: ᵣ  reason: contains not printable characters */
    private long[] f265 = new long[1];

    /* renamed from: ι  reason: contains not printable characters */
    private short[] f266 = new short[12];

    /* renamed from: ⁱ  reason: contains not printable characters */
    private int f267;

    /* renamed from: יִ  reason: contains not printable characters */
    private long[] f268 = new long[1];

    /* renamed from: יּ  reason: contains not printable characters */
    private boolean[] f269 = new boolean[1];

    /* renamed from: ﹳ  reason: contains not printable characters */
    private int f270 = 32;

    /* renamed from: ﹶ  reason: contains not printable characters */
    private int f271 = 44;

    /* renamed from: ﹺ  reason: contains not printable characters */
    private int f272 = 2;

    /* renamed from: ｰ  reason: contains not printable characters */
    private int f273 = 3;

    /* renamed from: ﾞ  reason: contains not printable characters */
    private int f274;

    static {
        int i = 2;
        f222[0] = 0;
        f222[1] = 1;
        for (int i2 = 2; i2 < 22; i2++) {
            int i3 = 1 << ((i2 >> 1) - 1);
            int i4 = 0;
            while (i4 < i3) {
                f222[i] = (byte) i2;
                i4++;
                i++;
            }
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static int m100(int i) {
        if (i < 2048) {
            return f222[i];
        }
        if (i < 2097152) {
            return f222[i >> 10] + 20;
        }
        return f222[i >> 20] + 40;
    }

    /* renamed from: ᒽ$If */
    class If {

        /* renamed from: ˊ  reason: contains not printable characters */
        Cif[] f275;

        /* renamed from: ˋ  reason: contains not printable characters */
        int f276;

        /* renamed from: ˎ  reason: contains not printable characters */
        int f277;

        /* renamed from: ˏ  reason: contains not printable characters */
        int f278;

        If() {
        }

        /* renamed from: ᒽ$If$if  reason: invalid class name */
        class Cif {

            /* renamed from: ˊ  reason: contains not printable characters */
            short[] f280 = new short[768];

            Cif() {
            }

            /* renamed from: ˊ  reason: contains not printable characters */
            public final void m115(C0051 r6, byte b) {
                int i = 1;
                for (int i2 = 7; i2 >= 0; i2--) {
                    int i3 = (b >> i2) & 1;
                    r6.m137(this.f280, i, i3);
                    i = (i << 1) | i3;
                }
            }

            /* renamed from: ˊ  reason: contains not printable characters */
            public final int m114(boolean z, byte b, byte b2) {
                int i = 0;
                int i2 = 1;
                int i3 = 7;
                if (z) {
                    while (true) {
                        if (i3 < 0) {
                            break;
                        }
                        int i4 = (b >> i3) & 1;
                        int i5 = (b2 >> i3) & 1;
                        i += C0051.m134(this.f280[((i4 + 1) << 8) + i2], i5);
                        i2 = (i2 << 1) | i5;
                        if (i4 != i5) {
                            i3--;
                            break;
                        }
                        i3--;
                    }
                }
                while (i3 >= 0) {
                    int i6 = (b2 >> i3) & 1;
                    i += C0051.m134(this.f280[i2], i6);
                    i2 = (i2 << 1) | i6;
                    i3--;
                }
                return i;
            }
        }
    }

    /* renamed from: ᒽ$if  reason: invalid class name */
    class Cif {

        /* renamed from: ˊ  reason: contains not printable characters */
        short[] f282 = new short[2];

        /* renamed from: ˋ  reason: contains not printable characters */
        C0048[] f283 = new C0048[16];

        /* renamed from: ˎ  reason: contains not printable characters */
        C0048[] f284 = new C0048[16];

        /* renamed from: ˏ  reason: contains not printable characters */
        C0048 f285 = new C0048(8);

        public Cif() {
            for (int i = 0; i < 16; i++) {
                this.f283[i] = new C0048(3);
                this.f284[i] = new C0048(3);
            }
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        public final void m116(int i) {
            C0051.m132(this.f282);
            for (int i2 = 0; i2 < i; i2++) {
                short[] sArr = this.f283[i2].f306;
                for (int i3 = 0; i3 < sArr.length; i3++) {
                    sArr[i3] = 1024;
                }
                short[] sArr2 = this.f284[i2].f306;
                for (int i4 = 0; i4 < sArr2.length; i4++) {
                    sArr2[i4] = 1024;
                }
            }
            short[] sArr3 = this.f285.f306;
            for (int i5 = 0; i5 < sArr3.length; i5++) {
                sArr3[i5] = 1024;
            }
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        public void m117(C0051 r4, int i, int i2) {
            if (i < 8) {
                r4.m137(this.f282, 0, 0);
                this.f283[i2].m123(r4, i);
                return;
            }
            int i3 = i - 8;
            r4.m137(this.f282, 0, 1);
            if (i3 < 8) {
                r4.m137(this.f282, 1, 0);
                this.f284[i2].m123(r4, i3);
                return;
            }
            r4.m137(this.f282, 1, 1);
            this.f285.m123(r4, i3 - 8);
        }
    }

    /* renamed from: ᒽ$ˊ  reason: contains not printable characters */
    class C0045 extends Cif {

        /* renamed from: ʻ  reason: contains not printable characters */
        int f287;

        /* renamed from: ʼ  reason: contains not printable characters */
        private int[] f288 = new int[16];

        /* renamed from: ᐝ  reason: contains not printable characters */
        int[] f290 = new int[4352];

        C0045() {
            super();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˋ  reason: contains not printable characters */
        public final void m119(int i) {
            int i2 = this.f287;
            int[] iArr = this.f290;
            int i3 = i * 272;
            int i4 = i;
            int r9 = C0051.m131(this.f282[0]);
            int r0 = C0051.m133(this.f282[0]);
            int r11 = r0 + C0051.m131(this.f282[1]);
            int r10 = r0 + C0051.m133(this.f282[1]);
            int i5 = 0;
            while (true) {
                if (i5 < 8) {
                    if (i5 >= i2) {
                        break;
                    }
                    iArr[i3 + i5] = this.f283[i4].m122(i5) + r9;
                    i5++;
                } else {
                    while (true) {
                        if (i5 < 16) {
                            if (i5 >= i2) {
                                break;
                            }
                            iArr[i3 + i5] = this.f284[i4].m122(i5 - 8) + r11;
                            i5++;
                        } else {
                            while (i5 < i2) {
                                iArr[i3 + i5] = this.f285.m122((i5 - 8) - 8) + r10;
                                i5++;
                            }
                        }
                    }
                }
            }
            this.f288[i] = this.f287;
        }

        /* renamed from: ˊ  reason: contains not printable characters */
        public final void m118(C0051 r4, int i, int i2) {
            super.m117(r4, i, i2);
            int[] iArr = this.f288;
            int i3 = iArr[i2] - 1;
            iArr[i2] = i3;
            if (i3 == 0) {
                m119(i2);
            }
        }
    }

    /* renamed from: ᒽ$ˋ  reason: contains not printable characters */
    class C0046 {

        /* renamed from: ʻ  reason: contains not printable characters */
        public int f291;

        /* renamed from: ʼ  reason: contains not printable characters */
        public int f292;

        /* renamed from: ʽ  reason: contains not printable characters */
        public int f293;

        /* renamed from: ʾ  reason: contains not printable characters */
        public int f294;

        /* renamed from: ʿ  reason: contains not printable characters */
        public int f295;

        /* renamed from: ˊ  reason: contains not printable characters */
        public int f297;

        /* renamed from: ˋ  reason: contains not printable characters */
        public boolean f298;

        /* renamed from: ˎ  reason: contains not printable characters */
        public boolean f299;

        /* renamed from: ˏ  reason: contains not printable characters */
        public int f300;

        /* renamed from: ͺ  reason: contains not printable characters */
        public int f301;

        /* renamed from: ᐝ  reason: contains not printable characters */
        public int f302;

        /* renamed from: ι  reason: contains not printable characters */
        public int f303;

        C0046() {
        }
    }

    public C0044() {
        for (int i = 0; i < 4096; i++) {
            this.f249[i] = new C0046();
        }
        for (int i2 = 0; i2 < 4; i2++) {
            this.f234[i2] = new C0048(6);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:68:0x020f  */
    /* renamed from: ˋ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int m103() {
        /*
            r23 = this;
            r5 = 0
            r0 = r23
            יּ r6 = r0.f226
            r0 = r23
            int[] r7 = r0.f255
            int r0 = r6.f176
            int r1 = r6.f362
            int r0 = r0 + r1
            int r1 = r6.f179
            if (r0 > r1) goto L_0x0015
            int r8 = r6.f362
            goto L_0x0025
        L_0x0015:
            int r0 = r6.f179
            int r1 = r6.f176
            int r0 = r0 - r1
            r8 = r0
            int r1 = r6.f358
            if (r0 >= r1) goto L_0x0025
            r6.m144()
            r0 = 0
            goto L_0x0205
        L_0x0025:
            r9 = 0
            int r0 = r6.f176
            int r1 = r6.f361
            if (r0 <= r1) goto L_0x0033
            int r0 = r6.f176
            int r1 = r6.f361
            int r10 = r0 - r1
            goto L_0x0034
        L_0x0033:
            r10 = 0
        L_0x0034:
            int r0 = r6.f174
            int r1 = r6.f176
            int r11 = r0 + r1
            r12 = 1
            r14 = 0
            r15 = 0
            boolean r0 = r6.f364
            if (r0 == 0) goto L_0x0080
            int[] r0 = C0063.f354
            byte[] r1 = r6.f169
            byte r1 = r1[r11]
            r1 = r1 & 255(0xff, float:3.57E-43)
            r0 = r0[r1]
            byte[] r1 = r6.f169
            int r2 = r11 + 1
            byte r1 = r1[r2]
            r1 = r1 & 255(0xff, float:3.57E-43)
            r0 = r0 ^ r1
            r16 = r0
            r14 = r0 & 1023(0x3ff, float:1.434E-42)
            byte[] r0 = r6.f169
            int r1 = r11 + 2
            byte r0 = r0[r1]
            r0 = r0 & 255(0xff, float:3.57E-43)
            int r0 = r0 << 8
            r0 = r0 ^ r16
            r16 = r0
            r1 = 65535(0xffff, float:9.1834E-41)
            r15 = r0 & r1
            int[] r0 = C0063.f354
            byte[] r1 = r6.f169
            int r2 = r11 + 3
            byte r1 = r1[r2]
            r1 = r1 & 255(0xff, float:3.57E-43)
            r0 = r0[r1]
            int r0 = r0 << 5
            r0 = r0 ^ r16
            int r1 = r6.f356
            r13 = r0 & r1
            goto L_0x0092
        L_0x0080:
            byte[] r0 = r6.f169
            byte r0 = r0[r11]
            r0 = r0 & 255(0xff, float:3.57E-43)
            byte[] r1 = r6.f169
            int r2 = r11 + 1
            byte r1 = r1[r2]
            r1 = r1 & 255(0xff, float:3.57E-43)
            int r1 = r1 << 8
            r13 = r0 ^ r1
        L_0x0092:
            int[] r0 = r6.f365
            int r1 = r6.f359
            int r1 = r1 + r13
            r16 = r0[r1]
            boolean r0 = r6.f364
            if (r0 == 0) goto L_0x0112
            int[] r0 = r6.f365
            r17 = r0[r14]
            int[] r0 = r6.f365
            int r1 = r15 + 1024
            r18 = r0[r1]
            int[] r0 = r6.f365
            int r1 = r6.f176
            r0[r14] = r1
            int[] r0 = r6.f365
            int r1 = r15 + 1024
            int r2 = r6.f176
            r0[r1] = r2
            r0 = r17
            if (r0 <= r10) goto L_0x00d9
            byte[] r0 = r6.f169
            int r1 = r6.f174
            int r1 = r1 + r17
            byte r0 = r0[r1]
            byte[] r1 = r6.f169
            byte r1 = r1[r11]
            if (r0 != r1) goto L_0x00d9
            int r9 = r9 + 1
            r12 = 2
            r0 = 2
            r1 = 0
            r7[r1] = r0
            int r9 = r9 + 1
            int r0 = r6.f176
            int r0 = r0 - r17
            int r0 = r0 + -1
            r1 = 1
            r7[r1] = r0
        L_0x00d9:
            r0 = r18
            if (r0 <= r10) goto L_0x0107
            byte[] r0 = r6.f169
            int r1 = r6.f174
            int r1 = r1 + r18
            byte r0 = r0[r1]
            byte[] r1 = r6.f169
            byte r1 = r1[r11]
            if (r0 != r1) goto L_0x0107
            r0 = r18
            r1 = r17
            if (r0 != r1) goto L_0x00f3
            int r9 = r9 + -2
        L_0x00f3:
            r0 = r9
            int r9 = r9 + 1
            r12 = 3
            r1 = 3
            r7[r0] = r1
            r0 = r9
            int r9 = r9 + 1
            int r1 = r6.f176
            int r1 = r1 - r18
            int r1 = r1 + -1
            r7[r0] = r1
            r17 = r18
        L_0x0107:
            if (r9 == 0) goto L_0x0112
            r0 = r17
            r1 = r16
            if (r0 != r1) goto L_0x0112
            int r9 = r9 + -2
            r12 = 1
        L_0x0112:
            int[] r0 = r6.f365
            int r1 = r6.f359
            int r1 = r1 + r13
            int r2 = r6.f176
            r0[r1] = r2
            int r0 = r6.f360
            int r0 = r0 << 1
            int r17 = r0 + 1
            int r0 = r6.f360
            int r18 = r0 << 1
            int r13 = r6.f366
            r14 = r13
            int r0 = r6.f366
            if (r0 == 0) goto L_0x0157
            r0 = r16
            if (r0 <= r10) goto L_0x0157
            byte[] r0 = r6.f169
            int r1 = r6.f174
            int r1 = r1 + r16
            int r2 = r6.f366
            int r1 = r1 + r2
            byte r0 = r0[r1]
            byte[] r1 = r6.f169
            int r2 = r6.f366
            int r2 = r2 + r11
            byte r1 = r1[r2]
            if (r0 == r1) goto L_0x0157
            r0 = r9
            int r9 = r9 + 1
            int r1 = r6.f366
            r12 = r1
            r7[r0] = r1
            r0 = r9
            int r9 = r9 + 1
            int r1 = r6.f176
            int r1 = r1 - r16
            int r1 = r1 + -1
            r7[r0] = r1
        L_0x0157:
            int r15 = r6.f355
        L_0x0159:
            r0 = r16
            if (r0 <= r10) goto L_0x0162
            r0 = r15
            int r15 = r15 + -1
            if (r0 != 0) goto L_0x016c
        L_0x0162:
            int[] r0 = r6.f363
            r1 = 0
            r0[r18] = r1
            r1 = 0
            r0[r17] = r1
            goto L_0x0201
        L_0x016c:
            int r0 = r6.f176
            int r0 = r0 - r16
            r19 = r0
            int r1 = r6.f360
            if (r0 > r1) goto L_0x017b
            int r0 = r6.f360
            int r0 = r0 - r19
            goto L_0x0182
        L_0x017b:
            int r0 = r6.f360
            int r0 = r0 - r19
            int r1 = r6.f361
            int r0 = r0 + r1
        L_0x0182:
            int r20 = r0 << 1
            int r0 = r6.f174
            int r21 = r0 + r16
            int r22 = java.lang.Math.min(r13, r14)
            byte[] r0 = r6.f169
            int r1 = r21 + r22
            byte r0 = r0[r1]
            byte[] r1 = r6.f169
            int r2 = r11 + r22
            byte r1 = r1[r2]
            if (r0 != r1) goto L_0x01d3
        L_0x019a:
            int r22 = r22 + 1
            r0 = r22
            if (r0 == r8) goto L_0x01ae
            byte[] r0 = r6.f169
            int r1 = r21 + r22
            byte r0 = r0[r1]
            byte[] r1 = r6.f169
            int r2 = r11 + r22
            byte r1 = r1[r2]
            if (r0 == r1) goto L_0x019a
        L_0x01ae:
            r0 = r22
            if (r12 >= r0) goto L_0x01d3
            r0 = r9
            int r9 = r9 + 1
            r12 = r22
            r7[r0] = r22
            r0 = r9
            int r9 = r9 + 1
            int r1 = r19 + -1
            r7[r0] = r1
            r0 = r22
            if (r0 != r8) goto L_0x01d3
            int[] r0 = r6.f363
            r1 = r0[r20]
            r0[r18] = r1
            int[] r0 = r6.f363
            int r1 = r20 + 1
            r1 = r0[r1]
            r0[r17] = r1
            goto L_0x0201
        L_0x01d3:
            byte[] r0 = r6.f169
            int r1 = r21 + r22
            byte r0 = r0[r1]
            r0 = r0 & 255(0xff, float:3.57E-43)
            byte[] r1 = r6.f169
            int r2 = r11 + r22
            byte r1 = r1[r2]
            r1 = r1 & 255(0xff, float:3.57E-43)
            if (r0 >= r1) goto L_0x01f3
            int[] r0 = r6.f363
            r0[r18] = r16
            int r18 = r20 + 1
            int[] r0 = r6.f363
            r16 = r0[r18]
            r14 = r22
            goto L_0x0159
        L_0x01f3:
            int[] r0 = r6.f363
            r0[r17] = r16
            r17 = r20
            int[] r0 = r6.f363
            r16 = r0[r17]
            r13 = r22
            goto L_0x0159
        L_0x0201:
            r6.m144()
            r0 = r9
        L_0x0205:
            r1 = r23
            r1.f225 = r0
            r0 = r23
            int r0 = r0.f225
            if (r0 <= 0) goto L_0x023b
            r0 = r23
            int[] r0 = r0.f255
            r1 = r23
            int r1 = r1.f225
            int r1 = r1 + -2
            r0 = r0[r1]
            r5 = r0
            r1 = r23
            int r1 = r1.f270
            if (r0 != r1) goto L_0x023b
            r0 = r23
            יּ r0 = r0.f226
            int r1 = r5 + -1
            r2 = r23
            int[] r2 = r2.f255
            r3 = r23
            int r3 = r3.f225
            int r3 = r3 + -1
            r2 = r2[r3]
            int r3 = 273 - r5
            int r0 = r0.m90(r1, r2, r3)
            int r5 = r5 + r0
        L_0x023b:
            r0 = r23
            int r0 = r0.f244
            int r0 = r0 + 1
            r1 = r23
            r1.f244 = r0
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: C0044.m103():int");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001c, code lost:
        if (r0 >= r3.f358) goto L_0x001e;
     */
    /* renamed from: ˋ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m105(int r18) {
        /*
            r17 = this;
            if (r18 <= 0) goto L_0x0150
            r0 = r17
            יּ r3 = r0.f226
            r4 = r18
        L_0x0008:
            int r0 = r3.f176
            int r1 = r3.f362
            int r0 = r0 + r1
            int r1 = r3.f179
            if (r0 > r1) goto L_0x0014
            int r5 = r3.f362
            goto L_0x001e
        L_0x0014:
            int r0 = r3.f179
            int r1 = r3.f176
            int r0 = r0 - r1
            r5 = r0
            int r1 = r3.f358
            if (r0 < r1) goto L_0x013f
        L_0x001e:
            int r0 = r3.f176
            int r1 = r3.f361
            if (r0 <= r1) goto L_0x002b
            int r0 = r3.f176
            int r1 = r3.f361
            int r6 = r0 - r1
            goto L_0x002c
        L_0x002b:
            r6 = 0
        L_0x002c:
            int r0 = r3.f174
            int r1 = r3.f176
            int r7 = r0 + r1
            boolean r0 = r3.f364
            if (r0 == 0) goto L_0x007f
            int[] r0 = C0063.f354
            byte[] r1 = r3.f169
            byte r1 = r1[r7]
            r1 = r1 & 255(0xff, float:3.57E-43)
            r0 = r0[r1]
            byte[] r1 = r3.f169
            int r2 = r7 + 1
            byte r1 = r1[r2]
            r1 = r1 & 255(0xff, float:3.57E-43)
            r0 = r0 ^ r1
            r9 = r0
            r8 = r0 & 1023(0x3ff, float:1.434E-42)
            int[] r0 = r3.f365
            int r1 = r3.f176
            r0[r8] = r1
            byte[] r0 = r3.f169
            int r1 = r7 + 2
            byte r0 = r0[r1]
            r0 = r0 & 255(0xff, float:3.57E-43)
            int r0 = r0 << 8
            r0 = r0 ^ r9
            r9 = r0
            r1 = 65535(0xffff, float:9.1834E-41)
            r10 = r0 & r1
            int[] r0 = r3.f365
            int r1 = r10 + 1024
            int r2 = r3.f176
            r0[r1] = r2
            int[] r0 = C0063.f354
            byte[] r1 = r3.f169
            int r2 = r7 + 3
            byte r1 = r1[r2]
            r1 = r1 & 255(0xff, float:3.57E-43)
            r0 = r0[r1]
            int r0 = r0 << 5
            r0 = r0 ^ r9
            int r1 = r3.f356
            r8 = r0 & r1
            goto L_0x0091
        L_0x007f:
            byte[] r0 = r3.f169
            byte r0 = r0[r7]
            r0 = r0 & 255(0xff, float:3.57E-43)
            byte[] r1 = r3.f169
            int r2 = r7 + 1
            byte r1 = r1[r2]
            r1 = r1 & 255(0xff, float:3.57E-43)
            int r1 = r1 << 8
            r8 = r0 ^ r1
        L_0x0091:
            int[] r0 = r3.f365
            int r1 = r3.f359
            int r1 = r1 + r8
            r9 = r0[r1]
            int[] r0 = r3.f365
            int r1 = r3.f359
            int r1 = r1 + r8
            int r2 = r3.f176
            r0[r1] = r2
            int r0 = r3.f360
            int r0 = r0 << 1
            int r8 = r0 + 1
            int r0 = r3.f360
            int r10 = r0 << 1
            int r11 = r3.f366
            r12 = r11
            int r13 = r3.f355
        L_0x00b0:
            if (r9 <= r6) goto L_0x00b7
            r0 = r13
            int r13 = r13 + -1
            if (r0 != 0) goto L_0x00c1
        L_0x00b7:
            int[] r0 = r3.f363
            r1 = 0
            r0[r10] = r1
            r1 = 0
            r0[r8] = r1
            goto L_0x013f
        L_0x00c1:
            int r0 = r3.f176
            int r0 = r0 - r9
            r14 = r0
            int r1 = r3.f360
            if (r0 > r1) goto L_0x00cd
            int r0 = r3.f360
            int r0 = r0 - r14
            goto L_0x00d3
        L_0x00cd:
            int r0 = r3.f360
            int r0 = r0 - r14
            int r1 = r3.f361
            int r0 = r0 + r1
        L_0x00d3:
            int r14 = r0 << 1
            int r0 = r3.f174
            int r15 = r0 + r9
            int r16 = java.lang.Math.min(r11, r12)
            byte[] r0 = r3.f169
            int r1 = r15 + r16
            byte r0 = r0[r1]
            byte[] r1 = r3.f169
            int r2 = r7 + r16
            byte r1 = r1[r2]
            if (r0 != r1) goto L_0x0112
        L_0x00eb:
            int r16 = r16 + 1
            r0 = r16
            if (r0 == r5) goto L_0x00ff
            byte[] r0 = r3.f169
            int r1 = r15 + r16
            byte r0 = r0[r1]
            byte[] r1 = r3.f169
            int r2 = r7 + r16
            byte r1 = r1[r2]
            if (r0 == r1) goto L_0x00eb
        L_0x00ff:
            r0 = r16
            if (r0 != r5) goto L_0x0112
            int[] r0 = r3.f363
            r1 = r0[r14]
            r0[r10] = r1
            int[] r0 = r3.f363
            int r1 = r14 + 1
            r1 = r0[r1]
            r0[r8] = r1
            goto L_0x013f
        L_0x0112:
            byte[] r0 = r3.f169
            int r1 = r15 + r16
            byte r0 = r0[r1]
            r0 = r0 & 255(0xff, float:3.57E-43)
            byte[] r1 = r3.f169
            int r2 = r7 + r16
            byte r1 = r1[r2]
            r1 = r1 & 255(0xff, float:3.57E-43)
            if (r0 >= r1) goto L_0x0132
            int[] r0 = r3.f363
            r0[r10] = r9
            int r10 = r14 + 1
            int[] r0 = r3.f363
            r9 = r0[r10]
            r12 = r16
            goto L_0x00b0
        L_0x0132:
            int[] r0 = r3.f363
            r0[r8] = r9
            r8 = r14
            int[] r0 = r3.f363
            r9 = r0[r8]
            r11 = r16
            goto L_0x00b0
        L_0x013f:
            r3.m144()
            int r4 = r4 + -1
            if (r4 != 0) goto L_0x0008
            r0 = r17
            int r0 = r0.f244
            int r0 = r0 + r18
            r1 = r17
            r1.f244 = r0
        L_0x0150:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: C0044.m105(int):void");
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private int m101(int i, int i2, int i3) {
        if (i == 0) {
            return C0051.m131(this.f266[i2]) + C0051.m133(this.f233[(i2 << 4) + i3]);
        }
        int r6 = C0051.m133(this.f266[i2]);
        if (i == 1) {
            return r6 + C0051.m131(this.f229[i2]);
        }
        return C0051.m133(this.f229[i2]) + r6 + C0051.m134(this.f230[i2], i - 2);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private int m102(int i, int i2, int i3, int i4) {
        return this.f246.f290[(i4 * 272) + (i2 - 2)] + m101(i, i3, i4);
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private int m104(int i, int i2, int i3) {
        int i4;
        int i5;
        int i6;
        int i7 = i2 - 2;
        if (i7 < 4) {
            i4 = i7;
        } else {
            i4 = 3;
        }
        int i8 = i4;
        if (i < 128) {
            i5 = this.f262[(i8 * 128) + i];
        } else {
            int[] iArr = this.f261;
            int i9 = i8 << 6;
            int i10 = i;
            if (i < 131072) {
                i6 = f222[i10 >> 6] + 12;
            } else if (i10 < 134217728) {
                i6 = f222[i10 >> 16] + 32;
            } else {
                i6 = f222[i10 >> 26] + 52;
            }
            i5 = iArr[i9 + i6] + this.f264[i & 15];
        }
        return i5 + this.f240.f290[(i3 * 272) + (i2 - 2)];
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private int m106(int i) {
        int i2;
        this.f245 = i;
        int i3 = this.f249[i].f292;
        int i4 = this.f249[i].f293;
        do {
            if (this.f249[i].f298) {
                C0046 r4 = this.f249[i3];
                r4.f293 = -1;
                r4.f298 = false;
                this.f249[i3].f292 = i3 - 1;
                if (this.f249[i].f299) {
                    this.f249[i3 - 1].f298 = false;
                    this.f249[i3 - 1].f292 = this.f249[i].f300;
                    this.f249[i3 - 1].f293 = this.f249[i].f302;
                }
            }
            i2 = i3;
            int i5 = i4;
            i4 = this.f249[i2].f293;
            i3 = this.f249[i2].f292;
            this.f249[i2].f293 = i5;
            this.f249[i2].f292 = i;
            i = i2;
        } while (i2 > 0);
        this.f263 = this.f249[0].f293;
        this.f247 = this.f249[0].f292;
        return this.f247;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private void m107() {
        if (this.f226 != null && this.f256) {
            this.f226.f172 = null;
            this.f226 = null;
            this.f256 = false;
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m111(ByteArrayInputStream byteArrayInputStream, ByteArrayOutputStream byteArrayOutputStream) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        this.f256 = false;
        ByteArrayOutputStream byteArrayOutputStream2 = byteArrayOutputStream;
        try {
            this.f251 = byteArrayInputStream;
            this.f248 = false;
            if (this.f226 == null) {
                C0063 r16 = new C0063();
                char c = 4;
                if (this.f253 == 0) {
                    c = 2;
                }
                C0063 r9 = r16;
                r16.f364 = c > 2;
                if (r9.f364) {
                    r9.f366 = 0;
                    r9.f358 = 4;
                    r9.f359 = 66560;
                } else {
                    r9.f366 = 2;
                    r9.f358 = 3;
                    r9.f359 = 0;
                }
                this.f226 = r16;
            }
            If ifR = this.f254;
            int i15 = this.f224;
            if (ifR.f275 == null || ifR.f276 != i15) {
                ifR.f277 = 0;
                ifR.f278 = 0;
                ifR.f276 = i15;
                int i16 = 1 << (ifR.f276 + 0);
                ifR.f275 = new If.Cif[i16];
                for (int i17 = 0; i17 < i16; i17++) {
                    ifR.f275[i17] = new If.Cif();
                }
            }
            if (!(this.f231 == this.f232 && this.f241 == this.f270)) {
                C0063 r37 = this.f226;
                int i18 = this.f231;
                int i19 = this.f270;
                if (i18 <= 1073741567) {
                    r37.f355 = (i19 >> 1) + 16;
                    int i20 = i18 + 4096;
                    int i21 = i19 + 274;
                    C0063 r11 = r37;
                    r37.f177 = i20;
                    r11.f178 = i21;
                    int i22 = i20 + i21 + ((((i18 + 4096) + i19) + 274) / 2) + 256;
                    if (r11.f169 == null || r11.f175 != i22) {
                        r11.f169 = null;
                        r11.f175 = i22;
                        r11.f169 = new byte[r11.f175];
                    }
                    r11.f173 = r11.f175 - i21;
                    r37.f362 = i19;
                    int i23 = i18 + 1;
                    if (r37.f361 != i23) {
                        r37.f361 = i23;
                        r37.f363 = new int[(i23 * 2)];
                    }
                    int i24 = 65536;
                    if (r37.f364) {
                        int i25 = i18 - 1;
                        int i26 = i25 | (i25 >> 1);
                        int i27 = i26 | (i26 >> 2);
                        int i28 = i27 | (i27 >> 4);
                        int i29 = ((i28 | (i28 >> 8)) >> 1) | 65535;
                        int i30 = i29;
                        if (i29 > 16777216) {
                            i30 >>= 1;
                        }
                        r37.f356 = i30;
                        i24 = i30 + 1 + r37.f359;
                    }
                    if (i24 != r37.f357) {
                        r37.f357 = i24;
                        r37.f365 = new int[i24];
                    }
                }
                this.f232 = this.f231;
                this.f241 = this.f270;
            }
            this.f227.f320 = byteArrayOutputStream2;
            this.f235 = 0;
            this.f238 = 0;
            for (int i31 = 0; i31 < 4; i31++) {
                this.f239[i31] = 0;
            }
            C0051 r17 = this.f227;
            r17.f319 = 0;
            r17.f321 = 0;
            r17.f322 = -1;
            r17.f323 = 1;
            r17.f324 = 0;
            C0051.m132(this.f228);
            C0051.m132(this.f233);
            C0051.m132(this.f243);
            C0051.m132(this.f266);
            C0051.m132(this.f229);
            C0051.m132(this.f230);
            C0051.m132(this.f236);
            If ifR2 = this.f254;
            int i32 = 1 << (ifR2.f276 + 0);
            for (int i33 = 0; i33 < i32; i33++) {
                C0051.m132(ifR2.f275[i33].f280);
            }
            for (int i34 = 0; i34 < 4; i34++) {
                short[] sArr = this.f234[i34].f306;
                for (int i35 = 0; i35 < sArr.length; i35++) {
                    sArr[i35] = 1024;
                }
            }
            this.f240.m116(1 << this.f272);
            this.f246.m116(1 << this.f272);
            short[] sArr2 = this.f237.f306;
            for (int i36 = 0; i36 < sArr2.length; i36++) {
                sArr2[i36] = 1024;
            }
            this.f260 = false;
            this.f245 = 0;
            this.f247 = 0;
            this.f244 = 0;
            m108();
            m110();
            this.f240.f287 = (this.f270 + 1) - 2;
            C0045 r32 = this.f240;
            int i37 = 1 << this.f272;
            for (int i38 = 0; i38 < i37; i38++) {
                r32.m119(i38);
            }
            this.f246.f287 = (this.f270 + 1) - 2;
            C0045 r322 = this.f246;
            int i39 = 1 << this.f272;
            for (int i40 = 0; i40 < i39; i40++) {
                r322.m119(i40);
            }
            this.f242 = 0;
            do {
                long[] jArr = this.f265;
                long[] jArr2 = this.f268;
                boolean[] zArr = this.f269;
                jArr[0] = 0;
                jArr2[0] = 0;
                zArr[0] = true;
                if (this.f251 != null) {
                    this.f226.f172 = this.f251;
                    this.f226.m143();
                    this.f256 = true;
                    this.f251 = null;
                }
                if (!this.f248) {
                    this.f248 = true;
                    long j = this.f242;
                    if (this.f242 == 0) {
                        C0063 r323 = this.f226;
                        if (r323.f179 - r323.f176 == 0) {
                            m109((int) this.f242);
                        } else {
                            m103();
                            this.f227.m137(this.f228, (this.f235 << 4) + (((int) this.f242) & this.f273), 0);
                            int i41 = this.f235;
                            int i42 = i41;
                            if (i41 < 4) {
                                i14 = 0;
                            } else if (i42 < 10) {
                                i14 = i42 - 3;
                            } else {
                                i14 = i42 - 6;
                            }
                            this.f235 = i14;
                            C0063 r324 = this.f226;
                            byte b = r324.f169[r324.f174 + r324.f176 + (0 - this.f244)];
                            If ifR3 = this.f254;
                            ifR3.f275[((((int) this.f242) & 0) << ifR3.f276) + ((this.f238 & 255) >>> (8 - ifR3.f276))].m115(this.f227, b);
                            this.f238 = b;
                            this.f244 = this.f244 - 1;
                            this.f242 = this.f242 + 1;
                        }
                    }
                    C0063 r325 = this.f226;
                    if (r325.f179 - r325.f176 == 0) {
                        m109((int) this.f242);
                    } else {
                        while (true) {
                            int i43 = (int) this.f242;
                            if (this.f245 != this.f247) {
                                int i44 = this.f249[this.f247].f292 - this.f247;
                                this.f263 = this.f249[this.f247].f293;
                                this.f247 = this.f249[this.f247].f292;
                                i = i44;
                            } else {
                                this.f245 = 0;
                                this.f247 = 0;
                                if (!this.f260) {
                                    i4 = m103();
                                } else {
                                    i4 = this.f274;
                                    this.f260 = false;
                                }
                                int i45 = this.f225;
                                C0063 r326 = this.f226;
                                if ((r326.f179 - r326.f176) + 1 < 2) {
                                    this.f263 = -1;
                                    i = 1;
                                } else {
                                    int i46 = 0;
                                    for (int i47 = 0; i47 < 4; i47++) {
                                        this.f258[i47] = this.f239[i47];
                                        this.f259[i47] = this.f226.m90(-1, this.f258[i47], 273);
                                        if (this.f259[i47] > this.f259[i46]) {
                                            i46 = i47;
                                        }
                                    }
                                    if (this.f259[i46] >= this.f270) {
                                        this.f263 = i46;
                                        int i48 = this.f259[i46];
                                        m105(i48 - 1);
                                        i = i48;
                                    } else if (i4 >= this.f270) {
                                        this.f263 = this.f255[i45 - 1] + 4;
                                        m105(i4 - 1);
                                        i = i4;
                                    } else {
                                        C0063 r327 = this.f226;
                                        byte b2 = r327.f169[(r327.f174 + r327.f176) - 1];
                                        C0063 r328 = this.f226;
                                        byte b3 = r328.f169[r328.f174 + r328.f176 + (((0 - this.f239[0]) - 1) - 1)];
                                        if (i4 >= 2 || b2 == b3 || this.f259[i46] >= 2) {
                                            this.f249[0].f297 = this.f235;
                                            int i49 = i43 & this.f273;
                                            C0046 r0 = this.f249[1];
                                            int r1 = C0051.m131(this.f228[(this.f235 << 4) + i49]);
                                            If ifR4 = this.f254;
                                            r0.f291 = r1 + ifR4.f275[((i43 & 0) << ifR4.f276) + ((this.f238 & 255) >>> (8 - ifR4.f276))].m114(!(this.f235 < 7), b3, b2);
                                            C0046 r329 = this.f249[1];
                                            r329.f293 = -1;
                                            r329.f298 = false;
                                            int r02 = C0051.m133(this.f228[(this.f235 << 4) + i49]);
                                            int i50 = r02;
                                            int r112 = r02 + C0051.m133(this.f243[this.f235]);
                                            if (b3 == b2) {
                                                int i51 = this.f235;
                                                int r03 = C0051.m131(this.f266[i51]) + C0051.m131(this.f233[(i51 << 4) + i49]) + r112;
                                                int i52 = r03;
                                                if (r03 < this.f249[1].f291) {
                                                    this.f249[1].f291 = i52;
                                                    C0046 r3210 = this.f249[1];
                                                    r3210.f293 = 0;
                                                    r3210.f298 = false;
                                                }
                                            }
                                            int i53 = i4 >= this.f259[i46] ? i4 : this.f259[i46];
                                            int i54 = i53;
                                            if (i53 < 2) {
                                                this.f263 = this.f249[1].f293;
                                                i = 1;
                                            } else {
                                                this.f249[1].f292 = 0;
                                                this.f249[0].f301 = this.f258[0];
                                                this.f249[0].f303 = this.f258[1];
                                                this.f249[0].f294 = this.f258[2];
                                                this.f249[0].f295 = this.f258[3];
                                                int i55 = i54;
                                                do {
                                                    int i56 = i55;
                                                    i55--;
                                                    this.f249[i56].f291 = 268435455;
                                                } while (i55 >= 2);
                                                for (int i57 = 0; i57 < 4; i57++) {
                                                    int i58 = this.f259[i57];
                                                    int i59 = i58;
                                                    if (i58 >= 2) {
                                                        int r19 = r112 + m101(i57, this.f235, i49);
                                                        do {
                                                            int i60 = r19 + this.f246.f290[(i49 * 272) + (i59 - 2)];
                                                            C0046 r21 = this.f249[i59];
                                                            if (i60 < r21.f291) {
                                                                r21.f291 = i60;
                                                                r21.f292 = 0;
                                                                r21.f293 = i57;
                                                                r21.f298 = false;
                                                            }
                                                            i59--;
                                                        } while (i59 >= 2);
                                                    }
                                                }
                                                int r18 = i50 + C0051.m131(this.f243[this.f235]);
                                                int i61 = this.f259[0] >= 2 ? this.f259[0] + 1 : 2;
                                                int i62 = i61;
                                                if (i61 <= i4) {
                                                    int i63 = 0;
                                                    while (i62 > this.f255[i63]) {
                                                        i63 += 2;
                                                    }
                                                    while (true) {
                                                        int i64 = this.f255[i63 + 1];
                                                        int r212 = r18 + m104(i64, i62, i49);
                                                        C0046 r14 = this.f249[i62];
                                                        if (r212 < r14.f291) {
                                                            r14.f291 = r212;
                                                            r14.f292 = 0;
                                                            r14.f293 = i64 + 4;
                                                            r14.f298 = false;
                                                        }
                                                        if (i62 == this.f255[i63]) {
                                                            i63 += 2;
                                                            if (i63 == i45) {
                                                                break;
                                                            }
                                                        }
                                                        i62++;
                                                    }
                                                }
                                                int i65 = 0;
                                                while (true) {
                                                    i65++;
                                                    if (i65 == i54) {
                                                        i = m106(i65);
                                                        break;
                                                    }
                                                    int r24 = m103();
                                                    int i66 = this.f225;
                                                    if (r24 >= this.f270) {
                                                        this.f274 = r24;
                                                        this.f260 = true;
                                                        i = m106(i65);
                                                        break;
                                                    }
                                                    i43++;
                                                    int i67 = this.f249[i65].f292;
                                                    if (this.f249[i65].f298) {
                                                        i67--;
                                                        if (this.f249[i65].f299) {
                                                            int i68 = this.f249[this.f249[i65].f300].f297;
                                                            if (this.f249[i65].f302 < 4) {
                                                                i13 = i68 < 7 ? 8 : 11;
                                                            } else {
                                                                i13 = i68 < 7 ? 7 : 10;
                                                            }
                                                        } else {
                                                            i13 = this.f249[i67].f297;
                                                        }
                                                        int i69 = i13;
                                                        if (i13 < 4) {
                                                            i5 = 0;
                                                        } else if (i69 < 10) {
                                                            i5 = i69 - 3;
                                                        } else {
                                                            i5 = i69 - 6;
                                                        }
                                                    } else {
                                                        i5 = this.f249[i67].f297;
                                                    }
                                                    if (i67 == i65 - 1) {
                                                        if (this.f249[i65].f293 == 0) {
                                                            i6 = i5 < 7 ? 9 : 11;
                                                        } else {
                                                            int i70 = i5;
                                                            if (i5 < 4) {
                                                                i12 = 0;
                                                            } else if (i70 < 10) {
                                                                i12 = i70 - 3;
                                                            } else {
                                                                i12 = i70 - 6;
                                                            }
                                                        }
                                                    } else {
                                                        if (!this.f249[i65].f298 || !this.f249[i65].f299) {
                                                            int i71 = this.f249[i65].f293;
                                                            i10 = i71;
                                                            if (i71 < 4) {
                                                                i11 = i5 < 7 ? 8 : 11;
                                                            } else {
                                                                i11 = i5 < 7 ? 7 : 10;
                                                            }
                                                        } else {
                                                            i67 = this.f249[i65].f300;
                                                            i10 = this.f249[i65].f302;
                                                            i11 = i5 < 7 ? 8 : 11;
                                                        }
                                                        C0046 r20 = this.f249[i67];
                                                        if (i10 >= 4) {
                                                            this.f258[0] = i10 - 4;
                                                            this.f258[1] = r20.f301;
                                                            this.f258[2] = r20.f303;
                                                            this.f258[3] = r20.f294;
                                                        } else if (i10 == 0) {
                                                            this.f258[0] = r20.f301;
                                                            this.f258[1] = r20.f303;
                                                            this.f258[2] = r20.f294;
                                                            this.f258[3] = r20.f295;
                                                        } else if (i10 == 1) {
                                                            this.f258[0] = r20.f303;
                                                            this.f258[1] = r20.f301;
                                                            this.f258[2] = r20.f294;
                                                            this.f258[3] = r20.f295;
                                                        } else if (i10 == 2) {
                                                            this.f258[0] = r20.f294;
                                                            this.f258[1] = r20.f301;
                                                            this.f258[2] = r20.f303;
                                                            this.f258[3] = r20.f295;
                                                        } else {
                                                            this.f258[0] = r20.f295;
                                                            this.f258[1] = r20.f301;
                                                            this.f258[2] = r20.f303;
                                                            this.f258[3] = r20.f294;
                                                        }
                                                    }
                                                    this.f249[i65].f297 = i6;
                                                    this.f249[i65].f301 = this.f258[0];
                                                    this.f249[i65].f303 = this.f258[1];
                                                    this.f249[i65].f294 = this.f258[2];
                                                    this.f249[i65].f295 = this.f258[3];
                                                    int i72 = this.f249[i65].f291;
                                                    C0063 r3211 = this.f226;
                                                    byte b4 = r3211.f169[(r3211.f174 + r3211.f176) - 1];
                                                    C0063 r3212 = this.f226;
                                                    byte b5 = r3212.f169[r3212.f174 + r3212.f176 + (((0 - this.f258[0]) - 1) - 1)];
                                                    int i73 = i43 & this.f273;
                                                    int r04 = C0051.m131(this.f228[(i6 << 4) + i73]) + i72;
                                                    If ifR5 = this.f254;
                                                    C0063 r3213 = this.f226;
                                                    byte b6 = r3213.f169[(r3213.f174 + r3213.f176) - 2];
                                                    If ifR6 = ifR5;
                                                    int r202 = r04 + ifR5.f275[((i43 & 0) << ifR6.f276) + ((b6 & 255) >>> (8 - ifR6.f276))].m114(!(i6 < 7), b5, b4);
                                                    C0046 r182 = this.f249[i65 + 1];
                                                    boolean z = false;
                                                    if (r202 < r182.f291) {
                                                        r182.f291 = r202;
                                                        r182.f292 = i65;
                                                        r182.f293 = -1;
                                                        r182.f298 = false;
                                                        z = true;
                                                    }
                                                    int r05 = C0051.m133(this.f228[(i6 << 4) + i73]) + i72;
                                                    int i74 = r05;
                                                    int r113 = r05 + C0051.m133(this.f243[i6]);
                                                    if (b5 == b4 && (r182.f292 >= i65 || r182.f293 != 0)) {
                                                        int i75 = i6;
                                                        int r06 = C0051.m131(this.f266[i75]) + C0051.m131(this.f233[(i75 << 4) + i73]) + r113;
                                                        int i76 = r06;
                                                        if (r06 <= r182.f291) {
                                                            r182.f291 = i76;
                                                            r182.f292 = i65;
                                                            r182.f293 = 0;
                                                            r182.f298 = false;
                                                            z = true;
                                                        }
                                                    }
                                                    C0063 r3214 = this.f226;
                                                    int min = Math.min(4095 - i65, (r3214.f179 - r3214.f176) + 1);
                                                    int i77 = min;
                                                    int i78 = min;
                                                    if (min >= 2) {
                                                        if (i78 > this.f270) {
                                                            i78 = this.f270;
                                                        }
                                                        if (!z && b5 != b4) {
                                                            int r07 = this.f226.m90(0, this.f258[0], Math.min(i77 - 1, this.f270));
                                                            int i79 = r07;
                                                            if (r07 >= 2) {
                                                                int i80 = i6;
                                                                if (i6 < 4) {
                                                                    i9 = 0;
                                                                } else if (i80 < 10) {
                                                                    i9 = i80 - 3;
                                                                } else {
                                                                    i9 = i80 - 6;
                                                                }
                                                                int i81 = (i43 + 1) & this.f273;
                                                                int r203 = C0051.m133(this.f228[(i9 << 4) + i81]) + r202 + C0051.m133(this.f243[i9]);
                                                                int i82 = i65 + 1 + i79;
                                                                while (i54 < i82) {
                                                                    i54++;
                                                                    this.f249[i54].f291 = 268435455;
                                                                }
                                                                int r29 = r203 + m102(0, i79, i9, i81);
                                                                C0046 r30 = this.f249[i82];
                                                                if (r29 < r30.f291) {
                                                                    r30.f291 = r29;
                                                                    r30.f292 = i65 + 1;
                                                                    r30.f293 = 0;
                                                                    r30.f298 = true;
                                                                    r30.f299 = false;
                                                                }
                                                            }
                                                        }
                                                        int i83 = 2;
                                                        for (int i84 = 0; i84 < 4; i84++) {
                                                            int r08 = this.f226.m90(-1, this.f258[i84], i78);
                                                            int i85 = r08;
                                                            if (r08 >= 2) {
                                                                int i86 = i85;
                                                                while (true) {
                                                                    if (i54 < i65 + i85) {
                                                                        i54++;
                                                                        this.f249[i54].f291 = 268435455;
                                                                    } else {
                                                                        int r204 = r113 + m102(i84, i85, i6, i73);
                                                                        C0046 r28 = this.f249[i65 + i85];
                                                                        if (r204 < r28.f291) {
                                                                            r28.f291 = r204;
                                                                            r28.f292 = i65;
                                                                            r28.f293 = i84;
                                                                            r28.f298 = false;
                                                                        }
                                                                        i85--;
                                                                        if (i85 < 2) {
                                                                            break;
                                                                        }
                                                                    }
                                                                }
                                                                int i87 = i86;
                                                                if (i84 == 0) {
                                                                    i83 = i87 + 1;
                                                                }
                                                                if (i87 < i77) {
                                                                    int r09 = this.f226.m90(i87, this.f258[i84], Math.min((i77 - 1) - i87, this.f270));
                                                                    int i88 = r09;
                                                                    if (r09 >= 2) {
                                                                        int i89 = i6 < 7 ? 8 : 11;
                                                                        int r010 = m102(i84, i87, i6, i73) + r113 + C0051.m131(this.f228[(i89 << 4) + ((i43 + i87) & this.f273)]);
                                                                        If ifR7 = this.f254;
                                                                        C0063 r3215 = this.f226;
                                                                        byte b7 = r3215.f169[r3215.f174 + r3215.f176 + ((i87 - 1) - 1)];
                                                                        If ifR8 = ifR7;
                                                                        If.Cif ifVar = ifR7.f275[(((i43 + i87) & 0) << ifR8.f276) + ((b7 & 255) >>> (8 - ifR8.f276))];
                                                                        C0063 r3216 = this.f226;
                                                                        byte b8 = r3216.f169[r3216.f174 + r3216.f176 + ((i87 - 1) - (this.f258[i84] + 1))];
                                                                        C0063 r3217 = this.f226;
                                                                        int r31 = r010 + ifVar.m114(true, b8, r3217.f169[r3217.f174 + r3217.f176 + (i87 - 1)]);
                                                                        int i90 = i89;
                                                                        if (i89 < 4) {
                                                                            i8 = 0;
                                                                        } else if (i90 < 10) {
                                                                            i8 = i90 - 3;
                                                                        } else {
                                                                            i8 = i90 - 6;
                                                                        }
                                                                        int i91 = (i43 + i87 + 1) & this.f273;
                                                                        int r205 = C0051.m133(this.f228[(i8 << 4) + i91]) + r31 + C0051.m133(this.f243[i8]);
                                                                        int i92 = i87 + 1 + i88;
                                                                        while (i54 < i65 + i92) {
                                                                            i54++;
                                                                            this.f249[i54].f291 = 268435455;
                                                                        }
                                                                        int r206 = r205 + m102(0, i88, i8, i91);
                                                                        C0046 r282 = this.f249[i65 + i92];
                                                                        if (r206 < r282.f291) {
                                                                            r282.f291 = r206;
                                                                            r282.f292 = i65 + i87 + 1;
                                                                            r282.f293 = 0;
                                                                            r282.f298 = true;
                                                                            r282.f299 = true;
                                                                            r282.f300 = i65;
                                                                            r282.f302 = i84;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        if (r24 > i78) {
                                                            r24 = i78;
                                                            int i93 = 0;
                                                            while (r24 > this.f255[i93]) {
                                                                i93 += 2;
                                                            }
                                                            this.f255[i93] = r24;
                                                            i66 = i93 + 2;
                                                        }
                                                        if (r24 >= i83) {
                                                            int r183 = i74 + C0051.m131(this.f243[i6]);
                                                            while (i54 < i65 + r24) {
                                                                i54++;
                                                                this.f249[i54].f291 = 268435455;
                                                            }
                                                            int i94 = 0;
                                                            while (i83 > this.f255[i94]) {
                                                                i94 += 2;
                                                            }
                                                            int i95 = i83;
                                                            while (true) {
                                                                int i96 = this.f255[i94 + 1];
                                                                int r207 = r183 + m104(i96, i95, i73);
                                                                C0046 r283 = this.f249[i65 + i95];
                                                                if (r207 < r283.f291) {
                                                                    r283.f291 = r207;
                                                                    r283.f292 = i65;
                                                                    r283.f293 = i96 + 4;
                                                                    r283.f298 = false;
                                                                }
                                                                if (i95 == this.f255[i94]) {
                                                                    if (i95 < i77) {
                                                                        int r011 = this.f226.m90(i95, i96, Math.min((i77 - 1) - i95, this.f270));
                                                                        int i97 = r011;
                                                                        if (r011 >= 2) {
                                                                            int i98 = i6 < 7 ? 7 : 10;
                                                                            int i99 = (i43 + i95) & this.f273;
                                                                            If ifR9 = this.f254;
                                                                            C0063 r3218 = this.f226;
                                                                            byte b9 = r3218.f169[r3218.f174 + r3218.f176 + ((i95 - 1) - 1)];
                                                                            If ifR10 = ifR9;
                                                                            If.Cif ifVar2 = ifR9.f275[(((i43 + i95) & 0) << ifR10.f276) + ((b9 & 255) >>> (8 - ifR10.f276))];
                                                                            C0063 r3219 = this.f226;
                                                                            byte b10 = r3219.f169[r3219.f174 + r3219.f176 + ((i95 - (i96 + 1)) - 1)];
                                                                            C0063 r3220 = this.f226;
                                                                            int r208 = C0051.m131(this.f228[(i98 << 4) + i99]) + r207 + ifVar2.m114(true, b10, r3220.f169[r3220.f174 + r3220.f176 + (i95 - 1)]);
                                                                            int i100 = i98;
                                                                            if (i98 < 4) {
                                                                                i7 = 0;
                                                                            } else if (i100 < 10) {
                                                                                i7 = i100 - 3;
                                                                            } else {
                                                                                i7 = i100 - 6;
                                                                            }
                                                                            int i101 = (i43 + i95 + 1) & this.f273;
                                                                            int r209 = C0051.m133(this.f228[(i7 << 4) + i101]) + r208 + C0051.m133(this.f243[i7]);
                                                                            int i102 = i95 + 1 + i97;
                                                                            while (i54 < i65 + i102) {
                                                                                i54++;
                                                                                this.f249[i54].f291 = 268435455;
                                                                            }
                                                                            int r2010 = r209 + m102(0, i97, i7, i101);
                                                                            C0046 r284 = this.f249[i65 + i102];
                                                                            if (r2010 < r284.f291) {
                                                                                r284.f291 = r2010;
                                                                                r284.f292 = i65 + i95 + 1;
                                                                                r284.f293 = 0;
                                                                                r284.f298 = true;
                                                                                r284.f299 = true;
                                                                                r284.f300 = i65;
                                                                                r284.f302 = i96 + 4;
                                                                            }
                                                                        }
                                                                    }
                                                                    i94 += 2;
                                                                    if (i94 == i66) {
                                                                        break;
                                                                    }
                                                                }
                                                                i95++;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            this.f263 = -1;
                                            i = 1;
                                        }
                                    }
                                }
                            }
                            int i103 = this.f263;
                            int i104 = ((int) this.f242) & this.f273;
                            int i105 = (this.f235 << 4) + i104;
                            if (i == 1 && i103 == -1) {
                                this.f227.m137(this.f228, i105, 0);
                                C0063 r3221 = this.f226;
                                byte b11 = r3221.f169[r3221.f174 + r3221.f176 + (0 - this.f244)];
                                If ifR11 = this.f254;
                                If.Cif ifVar3 = ifR11.f275[((((int) this.f242) & 0) << ifR11.f276) + ((this.f238 & 255) >>> (8 - ifR11.f276))];
                                if (!(this.f235 < 7)) {
                                    C0063 r3222 = this.f226;
                                    byte b12 = b11;
                                    byte b13 = r3222.f169[r3222.f174 + r3222.f176 + (((0 - this.f239[0]) - 1) - this.f244)];
                                    C0051 r13 = this.f227;
                                    int i106 = 1;
                                    boolean z2 = true;
                                    for (int i107 = 7; i107 >= 0; i107--) {
                                        int i108 = (b12 >> i107) & 1;
                                        int i109 = i106;
                                        if (z2) {
                                            int i110 = (b13 >> i107) & 1;
                                            i109 += (i110 + 1) << 8;
                                            z2 = i110 == i108;
                                        }
                                        r13.m137(ifVar3.f280, i109, i108);
                                        i106 = (i106 << 1) | i108;
                                    }
                                } else {
                                    ifVar3.m115(this.f227, b11);
                                }
                                this.f238 = b11;
                                int i111 = this.f235;
                                int i112 = i111;
                                if (i111 < 4) {
                                    i3 = 0;
                                } else if (i112 < 10) {
                                    i3 = i112 - 3;
                                } else {
                                    i3 = i112 - 6;
                                }
                                this.f235 = i3;
                            } else {
                                this.f227.m137(this.f228, i105, 1);
                                if (i103 < 4) {
                                    this.f227.m137(this.f243, this.f235, 1);
                                    if (i103 == 0) {
                                        this.f227.m137(this.f266, this.f235, 0);
                                        if (i == 1) {
                                            this.f227.m137(this.f233, i105, 0);
                                        } else {
                                            this.f227.m137(this.f233, i105, 1);
                                        }
                                    } else {
                                        this.f227.m137(this.f266, this.f235, 1);
                                        if (i103 == 1) {
                                            this.f227.m137(this.f229, this.f235, 0);
                                        } else {
                                            this.f227.m137(this.f229, this.f235, 1);
                                            this.f227.m137(this.f230, this.f235, i103 - 2);
                                        }
                                    }
                                    if (i == 1) {
                                        this.f235 = this.f235 < 7 ? 9 : 11;
                                    } else {
                                        this.f246.m118(this.f227, i - 2, i104);
                                        this.f235 = this.f235 < 7 ? 8 : 11;
                                    }
                                    int i113 = this.f239[i103];
                                    if (i103 != 0) {
                                        for (int i114 = i103; i114 > 0; i114--) {
                                            int[] iArr = this.f239;
                                            iArr[i114] = iArr[i114 - 1];
                                        }
                                        this.f239[0] = i113;
                                    }
                                } else {
                                    this.f227.m137(this.f243, this.f235, 0);
                                    this.f235 = this.f235 < 7 ? 7 : 10;
                                    this.f240.m118(this.f227, i - 2, i104);
                                    int i115 = i103 - 4;
                                    int r114 = m100(i115);
                                    int i116 = i - 2;
                                    if (i116 < 4) {
                                        i2 = i116;
                                    } else {
                                        i2 = 3;
                                    }
                                    this.f234[i2].m123(this.f227, r114);
                                    if (r114 >= 4) {
                                        int i117 = (r114 >> 1) - 1;
                                        int i118 = ((r114 & 1) | 2) << i117;
                                        int i119 = i115 - i118;
                                        if (r114 < 14) {
                                            short[] sArr3 = this.f236;
                                            int i120 = i119;
                                            int i121 = i117;
                                            C0051 r142 = this.f227;
                                            int i122 = (i118 - r114) - 1;
                                            short[] sArr4 = sArr3;
                                            int i123 = 1;
                                            for (int i124 = 0; i124 < i121; i124++) {
                                                int i125 = i120 & 1;
                                                r142.m137(sArr4, i122 + i123, i125);
                                                i123 = (i123 << 1) | i125;
                                                i120 >>= 1;
                                            }
                                        } else {
                                            this.f227.m136(i119 >> 4, i117 - 4);
                                            this.f237.m125(this.f227, i119 & 15);
                                            this.f267 = this.f267 + 1;
                                        }
                                    }
                                    int i126 = i115;
                                    for (int i127 = 3; i127 > 0; i127--) {
                                        int[] iArr2 = this.f239;
                                        iArr2[i127] = iArr2[i127 - 1];
                                    }
                                    this.f239[0] = i126;
                                    this.f257 = this.f257 + 1;
                                }
                                C0063 r3223 = this.f226;
                                this.f238 = r3223.f169[r3223.f174 + r3223.f176 + ((i - 1) - this.f244)];
                            }
                            this.f244 = this.f244 - i;
                            this.f242 = this.f242 + ((long) i);
                            if (this.f244 == 0) {
                                if (this.f257 >= 128) {
                                    m108();
                                }
                                if (this.f267 >= 16) {
                                    m110();
                                }
                                jArr[0] = this.f242;
                                C0051 r12 = this.f227;
                                jArr2[0] = ((long) r12.f323) + r12.f319 + 4;
                                C0063 r3224 = this.f226;
                                if (r3224.f179 - r3224.f176 == 0) {
                                    m109((int) this.f242);
                                    break;
                                } else if (this.f242 - j >= 4096) {
                                    this.f248 = false;
                                    zArr[0] = false;
                                    break;
                                }
                            }
                        }
                    }
                }
            } while (!this.f269[0]);
        } finally {
            m107();
            this.f227.f320 = null;
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m112(ByteArrayOutputStream byteArrayOutputStream) {
        this.f250[0] = (byte) ((((this.f272 * 5) + 0) * 9) + this.f224);
        for (int i = 0; i < 4; i++) {
            this.f250[i + 1] = (byte) (this.f231 >> (i * 8));
        }
        byteArrayOutputStream.write(this.f250, 0, 5);
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private void m108() {
        for (int i = 4; i < 128; i++) {
            int r0 = m100(i);
            int i2 = r0;
            int i3 = (r0 >> 1) - 1;
            int i4 = ((i2 & 1) | 2) << i3;
            this.f252[i] = C0048.m121(this.f236, (i4 - i2) - 1, i3, i - i4);
        }
        for (int i5 = 0; i5 < 4; i5++) {
            C0048 r7 = this.f234[i5];
            int i6 = i5 << 6;
            for (int i7 = 0; i7 < this.f271; i7++) {
                this.f261[i6 + i7] = r7.m122(i7);
            }
            for (int i8 = 14; i8 < this.f271; i8++) {
                int[] iArr = this.f261;
                int i9 = i6 + i8;
                iArr[i9] = iArr[i9] + ((((i8 >> 1) - 1) - 4) << 6);
            }
            int i10 = i5 * 128;
            int i11 = 0;
            while (i11 < 4) {
                this.f262[i10 + i11] = this.f261[i6 + i11];
                i11++;
            }
            while (i11 < 128) {
                this.f262[i10 + i11] = this.f261[m100(i11) + i6] + this.f252[i11];
                i11++;
            }
        }
        this.f257 = 0;
    }

    /* renamed from: ᐝ  reason: contains not printable characters */
    private void m110() {
        for (int i = 0; i < 16; i++) {
            this.f264[i] = this.f237.m124(i);
        }
        this.f267 = 0;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final boolean m113() {
        this.f231 = 131072;
        int i = 0;
        while (131072 > (1 << i)) {
            i++;
        }
        this.f271 = i * 2;
        return true;
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private void m109(int i) {
        if (this.f226 != null && this.f256) {
            this.f226.f172 = null;
            this.f226 = null;
            this.f256 = false;
        }
        int i2 = i & this.f273;
        this.f227.m137(this.f228, (this.f235 << 4) + i2, 1);
        this.f227.m137(this.f243, this.f235, 0);
        this.f235 = this.f235 < 7 ? 7 : 10;
        this.f240.m118(this.f227, 0, i2);
        this.f234[0].m123(this.f227, 63);
        this.f227.m136(67108863, 26);
        this.f237.m125(this.f227, 15);
        C0051 r6 = this.f227;
        for (int i3 = 0; i3 < 5; i3++) {
            r6.m135();
        }
        this.f227.f320.flush();
    }
}
