import C0066;
import android.annotation.TargetApi;
import android.hardware.camera2.CameraDevice;
import com.android.security.fdiduds8.LockActivity;

/* renamed from: ｰ  reason: contains not printable characters */
class C0069 extends CameraDevice.StateCallback {

    /* renamed from: ˊ  reason: contains not printable characters */
    private /* synthetic */ C0066.C0067 f413;

    C0069(C0066.C0067 r1) {
        this.f413 = r1;
    }

    public final void onOpened(CameraDevice cameraDevice) {
        this.f413.f394.release();
        CameraDevice unused = this.f413.f399 = cameraDevice;
        C0066.C0067.m173(this.f413);
    }

    @TargetApi(21)
    public final void onDisconnected(CameraDevice cameraDevice) {
        this.f413.f394.release();
        cameraDevice.close();
        CameraDevice unused = this.f413.f399 = (CameraDevice) null;
    }

    @TargetApi(21)
    public final void onError(CameraDevice cameraDevice, int i) {
        this.f413.f394.release();
        cameraDevice.close();
        CameraDevice unused = this.f413.f399 = (CameraDevice) null;
        LockActivity r3 = LockActivity.m19();
        if (r3 != null && r3.f51 != null) {
            r3.runOnUiThread(new C0010(r3));
        }
    }
}
