import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Arrays;

/* renamed from: ˌ  reason: contains not printable characters */
public final class C0020 {

    /* renamed from: ˊ  reason: contains not printable characters */
    public final Object f135;

    /* renamed from: ˋ  reason: contains not printable characters */
    private final boolean f136 = true;

    /*  JADX ERROR: JadxRuntimeException in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxRuntimeException: Not class type: T
        	at jadx.core.dex.info.ClassInfo.checkClassType(ClassInfo.java:60)
        	at jadx.core.dex.info.ClassInfo.fromType(ClassInfo.java:31)
        	at jadx.core.dex.nodes.DexNode.resolveClass(DexNode.java:143)
        	at jadx.core.dex.nodes.RootNode.resolveClass(RootNode.java:183)
        	at jadx.core.dex.nodes.utils.MethodUtils.processMethodArgsOverloaded(MethodUtils.java:75)
        	at jadx.core.dex.nodes.utils.MethodUtils.collectOverloadedMethods(MethodUtils.java:54)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processOverloaded(MethodInvokeVisitor.java:106)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInvoke(MethodInvokeVisitor.java:99)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.processInsn(MethodInvokeVisitor.java:70)
        	at jadx.core.dex.visitors.MethodInvokeVisitor.visit(MethodInvokeVisitor.java:63)
        */
    /* renamed from: ˊ  reason: contains not printable characters */
    private static <T extends java.lang.reflect.AccessibleObject> T m64(T r2) {
        /*
            if (r2 != 0) goto L_0x0004
            r0 = 0
            return r0
        L_0x0004:
            boolean r0 = r2 instanceof java.lang.reflect.Member
            if (r0 == 0) goto L_0x0025
            r0 = r2
            java.lang.reflect.Member r0 = (java.lang.reflect.Member) r0
            r1 = r0
            int r0 = r1.getModifiers()
            boolean r0 = java.lang.reflect.Modifier.isPublic(r0)
            if (r0 == 0) goto L_0x0025
            java.lang.Class r0 = r1.getDeclaringClass()
            int r0 = r0.getModifiers()
            boolean r0 = java.lang.reflect.Modifier.isPublic(r0)
            if (r0 == 0) goto L_0x0025
            return r2
        L_0x0025:
            boolean r0 = r2.isAccessible()
            if (r0 != 0) goto L_0x002f
            r0 = 1
            r2.setAccessible(r0)
        L_0x002f:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: C0020.m64(java.lang.reflect.AccessibleObject):java.lang.reflect.AccessibleObject");
    }

    public C0020(Class<?> cls) {
        this.f135 = cls;
    }

    public C0020(Object obj) {
        this.f135 = obj;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final C0020 m70(String str, Object... objArr) {
        Class<?> cls;
        Class<?> cls2;
        Method method;
        Class<?>[] r6 = m69(objArr);
        try {
            return m67(m65(str, r6), this.f135, objArr);
        } catch (NoSuchMethodException unused) {
            Class<?>[] clsArr = r6;
            String str2 = str;
            if (this.f136) {
                cls = (Class) this.f135;
            } else {
                cls = this.f135.getClass();
            }
            Method[] methods = cls.getMethods();
            int length = methods.length;
            int i = 0;
            while (true) {
                if (i < length) {
                    Method method2 = methods[i];
                    Method method3 = method2;
                    if (method2.getName().equals(str2) && m68(method2.getParameterTypes(), clsArr)) {
                        method = method3;
                        break;
                    }
                    i++;
                } else {
                    do {
                        Method[] declaredMethods = cls.getDeclaredMethods();
                        int length2 = declaredMethods.length;
                        int i2 = 0;
                        while (i2 < length2) {
                            Method method4 = declaredMethods[i2];
                            Method method5 = method4;
                            if (method4.getName().equals(str2) && m68(method4.getParameterTypes(), clsArr)) {
                                method = method5;
                            } else {
                                i2++;
                            }
                        }
                        cls = cls.getSuperclass();
                    } while (cls != null);
                    StringBuilder append = new StringBuilder("No similar method ").append(str2).append(" with params ").append(Arrays.toString(clsArr)).append(" could be found on type ");
                    if (this.f136) {
                        cls2 = (Class) this.f135;
                    } else {
                        cls2 = this.f135.getClass();
                    }
                    throw new NoSuchMethodException(append.append(cls2).append(new String(".".getBytes(), 0).intern()).toString());
                }
            }
            return m67(method, this.f135, objArr);
        } catch (NoSuchMethodException e) {
            throw new C0075COn(e);
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final C0020 m71(Object... objArr) {
        Class<?> cls;
        Class<?> cls2;
        Class<?>[] r1 = m69(objArr);
        try {
            if (this.f136) {
                cls2 = (Class) this.f135;
            } else {
                cls2 = this.f135.getClass();
            }
            return m66(cls2.getDeclaredConstructor(r1), objArr);
        } catch (NoSuchMethodException e) {
            if (this.f136) {
                cls = (Class) this.f135;
            } else {
                cls = this.f135.getClass();
            }
            for (Constructor<?> constructor : cls.getDeclaredConstructors()) {
                if (m68(constructor.getParameterTypes(), r1)) {
                    return m66(constructor, objArr);
                }
            }
            throw new C0075COn(e);
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static boolean m68(Class<?>[] clsArr, Class<?>[] clsArr2) {
        if (clsArr.length != clsArr2.length) {
            return false;
        }
        for (int i = 0; i < clsArr2.length; i++) {
            if (clsArr2[i] != Cif.class && !m63(clsArr[i]).isAssignableFrom(m63(clsArr2[i]))) {
                return false;
            }
        }
        return true;
    }

    public final int hashCode() {
        return this.f135.hashCode();
    }

    public final boolean equals(Object obj) {
        if (obj instanceof C0020) {
            return this.f135.equals(((C0020) obj).f135);
        }
        return false;
    }

    public final String toString() {
        return this.f135.toString();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static C0020 m66(Constructor<?> constructor, Object... objArr) {
        try {
            return new C0020(((Constructor) m64(constructor)).newInstance(objArr));
        } catch (Exception e) {
            throw new C0075COn(e);
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static C0020 m67(Method method, Object obj, Object... objArr) {
        try {
            m64(method);
            if (method.getReturnType() != Void.TYPE) {
                return new C0020(method.invoke(obj, objArr));
            }
            method.invoke(obj, objArr);
            return new C0020(obj);
        } catch (Exception e) {
            throw new C0075COn(e);
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private static Class<?>[] m69(Object... objArr) {
        if (objArr == null) {
            return new Class[0];
        }
        Class<?>[] clsArr = new Class[objArr.length];
        for (int i = 0; i < objArr.length; i++) {
            Object obj = objArr[i];
            clsArr[i] = obj == null ? Cif.class : obj.getClass();
        }
        return clsArr;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static Class<?> m63(Class<?> cls) {
        if (cls == null) {
            return null;
        }
        if (cls.isPrimitive()) {
            if (Boolean.TYPE == cls) {
                return Boolean.class;
            }
            if (Integer.TYPE == cls) {
                return Integer.class;
            }
            if (Long.TYPE == cls) {
                return Long.class;
            }
            if (Short.TYPE == cls) {
                return Short.class;
            }
            if (Byte.TYPE == cls) {
                return Byte.class;
            }
            if (Double.TYPE == cls) {
                return Double.class;
            }
            if (Float.TYPE == cls) {
                return Float.class;
            }
            if (Character.TYPE == cls) {
                return Character.class;
            }
            if (Void.TYPE == cls) {
                return Void.class;
            }
        }
        return cls;
    }

    /* renamed from: ˌ$if  reason: invalid class name */
    static class Cif {
        private Cif() {
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:7|8|15|9) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x001b, code lost:
        r1 = r1.getSuperclass();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        if (r1 != null) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0026, code lost:
        throw new java.lang.NoSuchMethodException();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001a, code lost:
        return r1.getDeclaredMethod(r3, r4);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0016 */
    /* renamed from: ˊ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.reflect.Method m65(java.lang.String r3, java.lang.Class<?>[] r4) {
        /*
            r2 = this;
            r1 = r2
            boolean r0 = r2.f136
            if (r0 == 0) goto L_0x000b
            java.lang.Object r0 = r1.f135
            java.lang.Class r0 = (java.lang.Class) r0
            r1 = r0
            goto L_0x0011
        L_0x000b:
            java.lang.Object r0 = r1.f135
            java.lang.Class r1 = r0.getClass()
        L_0x0011:
            java.lang.reflect.Method r0 = r1.getMethod(r3, r4)     // Catch:{ NoSuchMethodException -> 0x0016 }
            return r0
        L_0x0016:
            java.lang.reflect.Method r0 = r1.getDeclaredMethod(r3, r4)     // Catch:{ NoSuchMethodException -> 0x001b }
            return r0
        L_0x001b:
            java.lang.Class r1 = r1.getSuperclass()
            if (r1 != 0) goto L_0x0016
            java.lang.NoSuchMethodException r0 = new java.lang.NoSuchMethodException
            r0.<init>()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: C0020.m65(java.lang.String, java.lang.Class[]):java.lang.reflect.Method");
    }
}
