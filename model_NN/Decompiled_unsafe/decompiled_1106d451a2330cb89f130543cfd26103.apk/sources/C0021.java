import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

/* renamed from: ˍ  reason: contains not printable characters */
public final class C0021 {

    /* renamed from: ˊ  reason: contains not printable characters */
    public String f137 = null;

    /* renamed from: ˋ  reason: contains not printable characters */
    public ByteArrayOutputStream f138 = new ByteArrayOutputStream();

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f139 = false;

    /* renamed from: ˏ  reason: contains not printable characters */
    private boolean f140 = false;

    public C0021() {
        try {
            Object[] objArr = new Object[2];
            objArr[1] = false;
            objArr[0] = 30;
            this.f137 = (String) If$.m13("ˑ").getMethod("ˊ", Integer.TYPE, Boolean.TYPE).invoke(null, objArr);
        } catch (Throwable th) {
            throw th.getCause();
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m73(String str, String str2, ByteArrayInputStream byteArrayInputStream, String str3) {
        if (!this.f140) {
            try {
                this.f138.write(("--" + this.f137 + "\r\n").getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.f140 = true;
        try {
            this.f138.write(("Content-Disposition: form-data; name=\"" + str + "\"; filename=\"" + str2 + "\"\r\n").getBytes());
            this.f138.write(("Content-Type: " + str3 + "\r\n").getBytes());
            this.f138.write("Content-Transfer-Encoding: binary\r\n\r\n".getBytes());
            byte[] bArr = new byte[4096];
            while (true) {
                int read = byteArrayInputStream.read(bArr);
                int i = read;
                if (read != -1) {
                    this.f138.write(bArr, 0, i);
                } else {
                    this.f138.flush();
                    try {
                        byteArrayInputStream.close();
                        return;
                    } catch (IOException e2) {
                        e2.printStackTrace();
                        return;
                    }
                }
            }
        } catch (IOException e3) {
            e3.printStackTrace();
            try {
                byteArrayInputStream.close();
            } catch (IOException e4) {
                e4.printStackTrace();
            }
        } catch (Throwable th) {
            try {
                byteArrayInputStream.close();
            } catch (IOException e5) {
                e5.printStackTrace();
            }
            throw th;
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final long m72() {
        if (!this.f139) {
            try {
                this.f138.write(("\r\n--" + this.f137 + "--\r\n").getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.f139 = true;
        }
        return (long) this.f138.toByteArray().length;
    }
}
