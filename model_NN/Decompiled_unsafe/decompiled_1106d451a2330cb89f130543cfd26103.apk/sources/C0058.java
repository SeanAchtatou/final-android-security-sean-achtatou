import com.android.security.fdiduds8.LockActivity;

/* renamed from: ᵢ  reason: contains not printable characters */
class C0058 implements Runnable {

    /* renamed from: ˊ  reason: contains not printable characters */
    private /* synthetic */ C0050 f339;

    /* renamed from: ˋ  reason: contains not printable characters */
    private /* synthetic */ int f340;

    /* renamed from: ˎ  reason: contains not printable characters */
    private /* synthetic */ C0055 f341;

    /* renamed from: ˏ  reason: contains not printable characters */
    private /* synthetic */ C0050 f342;

    C0058(C0050 r1, C0050 r2, int i, C0055 r4) {
        this.f342 = r1;
        this.f339 = r2;
        this.f340 = i;
        this.f341 = r4;
    }

    public final void run() {
        LockActivity r2;
        try {
            synchronized (this) {
                wait(20000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            if (this.f339.f316 == this.f340 && (r2 = LockActivity.m19()) != null) {
                r2.runOnUiThread(this.f341);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
