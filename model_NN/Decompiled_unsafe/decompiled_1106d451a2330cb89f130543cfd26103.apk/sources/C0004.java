import C0066;
import android.annotation.TargetApi;
import android.media.ImageReader;

/* renamed from: ʳ  reason: contains not printable characters */
class C0004 implements ImageReader.OnImageAvailableListener {

    /* renamed from: ˊ  reason: contains not printable characters */
    private /* synthetic */ C0066.C0067 f97;

    C0004(C0066.C0067 r1) {
        this.f97 = r1;
    }

    @TargetApi(19)
    public final void onImageAvailable(ImageReader imageReader) {
        this.f97.f398.post(new C0066.C0067.If(this.f97.f401, imageReader.acquireNextImage(), this.f97.f386));
    }
}
