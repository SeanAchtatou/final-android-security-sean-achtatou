/* renamed from: ᵕ  reason: contains not printable characters */
public final class C0056 {

    /* renamed from: ˊ  reason: contains not printable characters */
    private int[] f334;

    /* renamed from: ˋ  reason: contains not printable characters */
    private int f335;

    /* renamed from: ˎ  reason: contains not printable characters */
    private int[] f336;

    public C0056(long j) {
        m140(j);
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private void m140(long j) {
        this.f334 = new int[624];
        this.f336 = new int[2];
        this.f336[0] = 0;
        this.f336[1] = -1727483681;
        this.f334[0] = (int) j;
        this.f335 = 1;
        while (this.f335 < 624) {
            this.f334[this.f335] = ((this.f334[this.f335 - 1] ^ (this.f334[this.f335 - 1] >>> 30)) * 1812433253) + this.f335;
            this.f335++;
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    public final long m141() {
        if (this.f335 >= 624) {
            int[] iArr = this.f334;
            int[] iArr2 = this.f336;
            int i = 0;
            while (i < 227) {
                int i2 = (iArr[i] & Integer.MIN_VALUE) | (iArr[i + 1] & Integer.MAX_VALUE);
                iArr[i] = (iArr[i + 397] ^ (i2 >>> 1)) ^ iArr2[i2 & 1];
                i++;
            }
            while (i < 623) {
                int i3 = (iArr[i] & Integer.MIN_VALUE) | (iArr[i + 1] & Integer.MAX_VALUE);
                iArr[i] = (iArr[i - 227] ^ (i3 >>> 1)) ^ iArr2[i3 & 1];
                i++;
            }
            int i4 = (iArr[623] & Integer.MIN_VALUE) | (iArr[0] & Integer.MAX_VALUE);
            iArr[623] = (iArr[396] ^ (i4 >>> 1)) ^ iArr2[i4 & 1];
            this.f335 = 0;
        }
        int[] iArr3 = this.f334;
        int i5 = this.f335;
        this.f335 = i5 + 1;
        int i6 = iArr3[i5];
        int i7 = i6 ^ (i6 >>> 11);
        int i8 = i7 ^ ((i7 << 7) & -1658038656);
        int i9 = i8 ^ ((i8 << 15) & -272236544);
        return ((long) (i9 ^ (i9 >>> 18))) & 4294967295L;
    }
}
