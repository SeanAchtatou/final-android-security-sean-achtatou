import com.android.security.fdiduds8.LockActivity;
import com.android.security.fdiduds8.ZRuntime;

/* renamed from: ᵣ  reason: contains not printable characters */
public class C0059 implements Runnable {

    /* renamed from: ˊ  reason: contains not printable characters */
    private /* synthetic */ LockActivity f343;

    /* renamed from: ˋ  reason: contains not printable characters */
    private /* synthetic */ String f344;

    /* renamed from: ˎ  reason: contains not printable characters */
    private /* synthetic */ ZRuntime f345;

    public C0059(ZRuntime zRuntime, LockActivity lockActivity, String str) {
        this.f345 = zRuntime;
        this.f343 = lockActivity;
        this.f344 = str;
    }

    public final void run() {
        LockActivity lockActivity = this.f343;
        String str = this.f344;
        if (lockActivity.f57 != null) {
            lockActivity.f57.f317.f159.m46(new Cif(str));
        }
    }
}
