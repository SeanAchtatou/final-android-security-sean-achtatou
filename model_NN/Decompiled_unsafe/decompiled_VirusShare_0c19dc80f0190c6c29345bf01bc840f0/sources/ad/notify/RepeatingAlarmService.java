package ad.notify;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class RepeatingAlarmService extends BroadcastReceiver {
    public void onReceive(Context arg0, Intent arg1) {
        System.out.println("RepeatingAlarmService START !!!)");
        Intent i = new Intent(arg0, NotificationActivity.class);
        i.addFlags(268435456);
        arg0.startActivity(i);
    }
}
