package org.htmlcleaner;

import com.fsck.k9.Account;
import java.io.IOException;
import java.io.Writer;
import org.apache.commons.io.IOUtils;

public class DoctypeToken extends BaseTokenImpl implements HtmlNode {
    public static final int HTML4_0 = 10;
    public static final int HTML4_01 = 20;
    public static final int HTML4_01_FRAMESET = 23;
    public static final int HTML4_01_STRICT = 21;
    public static final int HTML4_01_TRANSITIONAL = 22;
    public static final int HTML5 = 60;
    public static final int HTML5_LEGACY_TOOL_COMPATIBLE = 61;
    public static final int UNKNOWN = 0;
    public static final int XHTML1_0_FRAMESET = 33;
    public static final int XHTML1_0_STRICT = 31;
    public static final int XHTML1_0_TRANSITIONAL = 32;
    public static final int XHTML1_1 = 40;
    public static final int XHTML1_1_BASIC = 41;
    private String part1;
    private String part2;
    private String part3;
    private String part4;
    private Integer type = null;
    private Boolean valid = null;

    public DoctypeToken(String part12, String part22, String part32, String part42) {
        this.part1 = part12;
        this.part2 = part22 != null ? part22.toUpperCase() : part22;
        this.part3 = clean(part32);
        this.part4 = clean(part42);
        validate();
    }

    public DoctypeToken(String part12, String part22, String part32, String part42, String part5) {
        this.part1 = part12;
        this.part2 = part22 != null ? part22.toUpperCase() : part22;
        this.part3 = clean(part32);
        this.part4 = clean(part5);
        validate();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    private String clean(String s) {
        if (s != null) {
            return s.replace('>', ' ').replace('<', ' ').replace('&', ' ').replace('\'', ' ').replace('\"', ' ');
        }
        return s;
    }

    public boolean isValid() {
        return this.valid.booleanValue();
    }

    private void validate() {
        if (!"public".equalsIgnoreCase(this.part2) && !"system".equalsIgnoreCase(this.part2) && "html".equalsIgnoreCase(this.part1) && this.part2 == null) {
            this.type = 60;
            this.valid = true;
        }
        if ("public".equalsIgnoreCase(this.part2)) {
            if ("-//W3C//DTD HTML 4.0//EN".equals(getPublicId())) {
                this.type = 10;
                if ("http://www.w3.org/TR/REC-html40/strict.dtd".equals(this.part4) || "".equals(getSystemId())) {
                    this.valid = true;
                } else {
                    this.valid = false;
                }
            }
            if ("-//W3C//DTD HTML 4.01//EN".equals(getPublicId())) {
                this.type = 21;
                if ("http://www.w3.org/TR/html4/strict.dtd".equals(this.part4) || "".equals(getSystemId())) {
                    this.valid = true;
                } else {
                    this.valid = false;
                }
            }
            if ("-//W3C//DTD HTML 4.01 Transitional//EN".equals(getPublicId())) {
                this.type = 22;
                if ("http://www.w3.org/TR/html4/loose.dtd".equals(getSystemId())) {
                    this.valid = true;
                } else {
                    this.valid = false;
                }
            }
            if ("-//W3C//DTD HTML 4.01 Frameset//EN".equals(getPublicId())) {
                this.type = 23;
                if ("http://www.w3.org/TR/html4/frameset.dtd".equals(getSystemId())) {
                    this.valid = true;
                } else {
                    this.valid = false;
                }
            }
            if ("-//W3C//DTD XHTML 1.0 Strict//EN".equals(getPublicId())) {
                this.type = 31;
                if ("http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd".equals(getSystemId())) {
                    this.valid = true;
                } else {
                    this.valid = false;
                }
            }
            if ("-//W3C//DTD XHTML 1.0 Transitional//EN".equals(getPublicId())) {
                this.type = 32;
                if ("http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd".equals(getSystemId())) {
                    this.valid = true;
                } else {
                    this.valid = false;
                }
            }
            if ("-//W3C//DTD XHTML 1.0 Frameset//EN".equals(getPublicId())) {
                this.type = 33;
                if ("http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd".equals(getSystemId())) {
                    this.valid = true;
                } else {
                    this.valid = false;
                }
            }
            if ("-//W3C//DTD XHTML 1.1//EN".equals(getPublicId())) {
                this.type = 40;
                if ("http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd".equals(getSystemId())) {
                    this.valid = true;
                } else {
                    this.valid = false;
                }
            }
            if ("-//W3C//DTD XHTML Basic 1.1//EN".equals(getPublicId())) {
                this.type = 41;
                if ("http://www.w3.org/TR/xhtml11/DTD/xhtml-basic11.dtd".equals(getSystemId())) {
                    this.valid = true;
                } else {
                    this.valid = false;
                }
            }
        }
        if ("system".equalsIgnoreCase(this.part2) && "about:legacy-compat".equals(getPublicId())) {
            this.type = 61;
            this.valid = true;
        }
        if (this.type == null) {
            this.type = 0;
            this.valid = false;
        }
    }

    public String getContent() {
        String result;
        if (this.type.intValue() == 0) {
            result = "<!DOCTYPE " + this.part1;
        } else if (this.type.intValue() >= 30) {
            result = "<!DOCTYPE " + "html";
        } else {
            result = "<!DOCTYPE " + "HTML";
        }
        if (this.part2 != null) {
            result = result + " " + this.part2 + " \"" + this.part3 + "\"";
            if (!"".equals(this.part4)) {
                result = result + " \"" + this.part4 + "\"";
            }
        }
        return result + Account.DEFAULT_QUOTE_PREFIX;
    }

    public String toString() {
        return getContent();
    }

    public int getType() {
        return this.type.intValue();
    }

    public String getName() {
        return "";
    }

    public void serialize(Serializer serializer, Writer writer) throws IOException {
        writer.write(getContent() + IOUtils.LINE_SEPARATOR_UNIX);
    }

    public String getPublicId() {
        return this.part3;
    }

    public String getSystemId() {
        return this.part4;
    }

    public String getPart1() {
        return this.part1;
    }

    public String getPart2() {
        return this.part2;
    }

    @Deprecated
    public String getPart3() {
        return this.part3;
    }

    @Deprecated
    public String getPart4() {
        return this.part4;
    }
}
