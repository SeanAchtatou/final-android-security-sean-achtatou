package org.htmlcleaner;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

public class SimpleXmlSerializer extends XmlSerializer {
    public SimpleXmlSerializer(CleanerProperties props) {
        super(props);
    }

    /* access modifiers changed from: protected */
    public void serialize(TagNode tagNode, Writer writer) throws IOException {
        serializeOpenTag(tagNode, writer, false);
        List<? extends BaseToken> tagChildren = tagNode.getAllChildren();
        if (!isMinimizedTagSyntax(tagNode)) {
            for (Object item : tagChildren) {
                if (item != null) {
                    if (item instanceof CData) {
                        serializeCData((CData) item, tagNode, writer);
                    } else if (item instanceof ContentNode) {
                        serializeContentToken((ContentNode) item, tagNode, writer);
                    } else {
                        ((BaseToken) item).serialize(this, writer);
                    }
                }
            }
            serializeEndTag(tagNode, writer, false);
        }
    }
}
