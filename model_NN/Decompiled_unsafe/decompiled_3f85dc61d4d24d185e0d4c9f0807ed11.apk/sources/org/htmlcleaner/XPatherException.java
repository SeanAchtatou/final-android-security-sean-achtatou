package org.htmlcleaner;

public class XPatherException extends Exception {
    public XPatherException() {
        this("Error in evaluating XPath expression!");
    }

    public XPatherException(Throwable cause) {
        super(cause);
    }

    public XPatherException(String message) {
        super(message);
    }

    public XPatherException(String message, Throwable cause) {
        super(message, cause);
    }
}
