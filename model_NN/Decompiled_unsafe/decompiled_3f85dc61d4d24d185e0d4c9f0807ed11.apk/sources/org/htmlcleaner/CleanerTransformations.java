package org.htmlcleaner;

import exts.whats.Constants;
import java.util.HashMap;
import java.util.Map;

public class CleanerTransformations {
    private TagTransformation globalTransformations = new TagTransformation();
    private Map mappings = new HashMap();

    public CleanerTransformations() {
    }

    public CleanerTransformations(Map transInfos) {
        updateTagTransformations(transInfos);
    }

    public void addTransformation(TagTransformation tagTransformation) {
        if (tagTransformation != null) {
            this.mappings.put(tagTransformation.getSourceTag(), tagTransformation);
        }
    }

    public void addGlobalTransformation(AttributeTransformation attributeTransformation) {
        this.globalTransformations.addAttributePatternTransformation(attributeTransformation);
    }

    public boolean hasTransformationForTag(String tagName) {
        return tagName != null && this.mappings.containsKey(tagName.toLowerCase());
    }

    public TagTransformation getTransformation(String tagName) {
        if (tagName != null) {
            return (TagTransformation) this.mappings.get(tagName.toLowerCase());
        }
        return null;
    }

    public void updateTagTransformations(String key, String value) {
        if (key.indexOf(46) <= 0) {
            String destTag = null;
            boolean preserveSourceAtts = true;
            if (value != null) {
                String[] tokens = Utils.tokenize(value, ",;");
                if (tokens.length > 0) {
                    destTag = tokens[0];
                }
                if (tokens.length > 1) {
                    preserveSourceAtts = "true".equalsIgnoreCase(tokens[1]) || "yes".equalsIgnoreCase(tokens[1]) || Constants.INSTALL_ID.equals(tokens[1]);
                }
            }
            addTransformation(new TagTransformation(key, destTag, preserveSourceAtts));
            return;
        }
        String[] parts = Utils.tokenize(key, ".");
        TagTransformation trans = getTransformation(parts[0]);
        if (trans != null) {
            trans.addAttributeTransformation(parts[1], value);
        }
    }

    public void updateTagTransformations(Map transInfos) {
        for (Map.Entry entry : transInfos.entrySet()) {
            updateTagTransformations((String) entry.getKey(), (String) entry.getValue());
        }
    }

    public Map<String, String> transformAttributes(String originalTagName, Map<String, String> attributes) {
        Map<String, String> results;
        TagTransformation tagTrans = getTransformation(originalTagName);
        if (tagTrans != null) {
            results = tagTrans.applyTagTransformations(attributes);
        } else {
            results = attributes;
        }
        return this.globalTransformations.applyTagTransformations(results);
    }

    public String getTagName(String tagName) {
        TagTransformation tagTransformation;
        if (!hasTransformationForTag(tagName) || (tagTransformation = getTransformation(tagName)) == null) {
            return tagName;
        }
        return tagTransformation.getDestTag();
    }

    public void clear() {
        this.mappings.clear();
    }
}
