package org.htmlcleaner;

import com.fsck.k9.Account;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Map;
import org.apache.commons.io.IOUtils;

public abstract class XmlSerializer extends Serializer {
    public static final String XMLNS_NAMESPACE = "xmlns";
    private boolean creatingHtmlDom;

    protected XmlSerializer(CleanerProperties props) {
        super(props);
    }

    public void setCreatingHtmlDom(boolean creatingHtmlDom2) {
        this.creatingHtmlDom = creatingHtmlDom2;
    }

    public boolean isCreatingHtmlDom() {
        return this.creatingHtmlDom;
    }

    @Deprecated
    public void writeXmlToStream(TagNode tagNode, OutputStream out, String charset) throws IOException {
        super.writeToStream(tagNode, out, charset);
    }

    @Deprecated
    public void writeXmlToStream(TagNode tagNode, OutputStream out) throws IOException {
        super.writeToStream(tagNode, out);
    }

    @Deprecated
    public void writeXmlToFile(TagNode tagNode, String fileName, String charset) throws IOException {
        super.writeToFile(tagNode, fileName, charset);
    }

    @Deprecated
    public void writeXmlToFile(TagNode tagNode, String fileName) throws IOException {
        super.writeToFile(tagNode, fileName);
    }

    @Deprecated
    public String getXmlAsString(TagNode tagNode, String charset) {
        return super.getAsString(tagNode, charset);
    }

    @Deprecated
    public String getXmlAsString(TagNode tagNode) {
        return super.getAsString(tagNode);
    }

    @Deprecated
    public void writeXml(TagNode tagNode, Writer writer, String charset) throws IOException {
        super.write(tagNode, writer, charset);
    }

    /* access modifiers changed from: protected */
    public String escapeXml(String xmlContent) {
        return Utils.escapeXml(xmlContent, this.props, isCreatingHtmlDom());
    }

    /* access modifiers changed from: protected */
    public boolean dontEscape(TagNode tagNode) {
        return this.props.isUseCdataFor(tagNode.getName());
    }

    /* access modifiers changed from: protected */
    public boolean isMinimizedTagSyntax(TagNode tagNode) {
        TagInfo tagInfo = this.props.getTagInfoProvider().getTagInfo(tagNode.getName());
        return tagNode.isEmpty() && (tagInfo == null || tagInfo.isMinimizedTagPermitted()) && (this.props.isUseEmptyElementTags() || (tagInfo != null && tagInfo.isEmptyTag()));
    }

    /* access modifiers changed from: protected */
    public void serializeOpenTag(TagNode tagNode, Writer writer) throws IOException {
        serializeOpenTag(tagNode, writer, true);
    }

    /* access modifiers changed from: protected */
    public void serializeCData(CData item, TagNode tagNode, Writer writer) throws IOException {
        if (dontEscape(tagNode)) {
            writer.write(item.getContentWithoutStartAndEndTokens());
        } else {
            writer.write(escapeXml(item.getContentWithStartAndEndTokens()));
        }
    }

    /* access modifiers changed from: protected */
    public void serializeContentToken(ContentNode item, TagNode tagNode, Writer writer) throws IOException {
        if (dontEscape(tagNode)) {
            writer.write(item.getContent());
        } else {
            writer.write(escapeXml(item.getContent()));
        }
    }

    /* access modifiers changed from: protected */
    public void serializeOpenTag(TagNode tagNode, Writer writer, boolean newLine) throws IOException {
        char firstchar;
        if (!isForbiddenTag(tagNode)) {
            String tagName = tagNode.getName();
            Map<String, String> tagAtttributes = tagNode.getAttributes();
            if (this.props.isAddNewlineToHeadAndBody() && isHeadOrBody(tagName)) {
                writer.write(IOUtils.LINE_SEPARATOR_UNIX);
            }
            writer.write("<" + tagName);
            for (Map.Entry<String, String> entry : tagAtttributes.entrySet()) {
                serializeAttribute(tagNode, writer, (String) entry.getKey(), (String) entry.getValue());
            }
            if (isMinimizedTagSyntax(tagNode)) {
                writer.write(" />");
                if (newLine) {
                    writer.write(IOUtils.LINE_SEPARATOR_UNIX);
                }
            } else if (dontEscape(tagNode)) {
                writer.write(Account.DEFAULT_QUOTE_PREFIX);
                if (!tagNode.getText().toString().startsWith(CData.SAFE_BEGIN_CDATA)) {
                    writer.write(CData.SAFE_BEGIN_CDATA);
                    if (!tagNode.getText().toString().equals("") && (firstchar = tagNode.getText().toString().charAt(0)) != 10 && firstchar != 13) {
                        writer.write(IOUtils.LINE_SEPARATOR_UNIX);
                    }
                }
            } else {
                writer.write(Account.DEFAULT_QUOTE_PREFIX);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean isForbiddenTag(TagNode tagNode) {
        return tagNode.getName() == null;
    }

    /* access modifiers changed from: protected */
    public boolean isHeadOrBody(String tagName) {
        return "head".equalsIgnoreCase(tagName) || "body".equalsIgnoreCase(tagName);
    }

    /* access modifiers changed from: protected */
    public void serializeAttribute(TagNode tagNode, Writer writer, String attName, String attValue) throws IOException {
        if (!isForbiddenAttribute(tagNode, attName, attValue)) {
            writer.write(" " + attName + "=\"" + escapeXml(attValue) + "\"");
        }
    }

    /* access modifiers changed from: protected */
    public boolean isForbiddenAttribute(TagNode tagNode, String attName, String value) {
        return !this.props.isNamespacesAware() && ("xmlns".equals(attName) || attName.startsWith("xmlns:"));
    }

    /* access modifiers changed from: protected */
    public void serializeEndTag(TagNode tagNode, Writer writer) throws IOException {
        serializeEndTag(tagNode, writer, true);
    }

    /* access modifiers changed from: protected */
    public void serializeEndTag(TagNode tagNode, Writer writer, boolean newLine) throws IOException {
        char lastchar;
        if (!isForbiddenTag(tagNode)) {
            String tagName = tagNode.getName();
            if (dontEscape(tagNode) && !tagNode.getText().toString().trim().endsWith(CData.SAFE_END_CDATA)) {
                if (!(tagNode.getText().toString().length() <= 0 || (lastchar = tagNode.getText().toString().charAt(tagNode.getText().toString().length() - 1)) == 10 || lastchar == 13)) {
                    writer.write(IOUtils.LINE_SEPARATOR_UNIX);
                }
                writer.write(CData.SAFE_END_CDATA);
            }
            writer.write("</" + tagName + Account.DEFAULT_QUOTE_PREFIX);
            if (newLine) {
                writer.write(IOUtils.LINE_SEPARATOR_UNIX);
            }
        }
    }
}
