package org.htmlcleaner;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.ListIterator;
import org.apache.commons.io.IOUtils;

public class CompactXmlSerializer extends XmlSerializer {
    public CompactXmlSerializer(CleanerProperties props) {
        super(props);
    }

    /* access modifiers changed from: protected */
    public void serialize(TagNode tagNode, Writer writer) throws IOException {
        serializeOpenTag(tagNode, writer, false);
        List<? extends BaseToken> tagChildren = tagNode.getAllChildren();
        if (!isMinimizedTagSyntax(tagNode)) {
            ListIterator<? extends BaseToken> childrenIt = tagChildren.listIterator();
            while (childrenIt.hasNext()) {
                Object item = childrenIt.next();
                if (item != null) {
                    if (item instanceof ContentNode) {
                        String content = ((ContentNode) item).getContent().trim();
                        writer.write(dontEscape(tagNode) ? content.replaceAll(CData.END_CDATA, "]]&gt;") : escapeXml(content));
                        if (childrenIt.hasNext()) {
                            if (!isWhitespaceString(childrenIt.next())) {
                                writer.write(IOUtils.LINE_SEPARATOR_UNIX);
                            }
                            childrenIt.previous();
                        }
                    } else if (item instanceof CommentNode) {
                        writer.write(((CommentNode) item).getCommentedContent().trim());
                    } else {
                        ((BaseToken) item).serialize(this, writer);
                    }
                }
            }
            serializeEndTag(tagNode, writer, false);
        }
    }

    private boolean isWhitespaceString(Object object) {
        String s;
        if (object == null || (s = object.toString()) == null || !"".equals(s.trim())) {
            return false;
        }
        return true;
    }
}
