package org.htmlcleaner;

public interface ITagInfoProvider {
    TagInfo getTagInfo(String str);
}
