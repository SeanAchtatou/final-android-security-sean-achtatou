package org.htmlcleaner;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import org.apache.commons.io.IOUtils;

public class PrettyHtmlSerializer extends HtmlSerializer {
    private static final String DEFAULT_INDENTATION_STRING = "\t";
    private String indentString;
    private List<String> indents;

    public PrettyHtmlSerializer(CleanerProperties props) {
        this(props, DEFAULT_INDENTATION_STRING);
    }

    public PrettyHtmlSerializer(CleanerProperties props, String indentString2) {
        super(props);
        this.indentString = DEFAULT_INDENTATION_STRING;
        this.indents = new ArrayList();
        this.indentString = indentString2;
    }

    /* access modifiers changed from: protected */
    public void serialize(TagNode tagNode, Writer writer) throws IOException {
        serializePrettyHtml(tagNode, writer, 0, false, true);
    }

    private synchronized String getIndent(int level) {
        int size = this.indents.size();
        if (size <= level) {
            String prevIndent = size == 0 ? null : this.indents.get(size - 1);
            for (int i = size; i <= level; i++) {
                String currIndent = prevIndent == null ? "" : prevIndent + this.indentString;
                this.indents.add(currIndent);
                prevIndent = currIndent;
            }
        }
        return this.indents.get(level);
    }

    private String getIndentedText(String content, int level) {
        String indent = getIndent(level);
        StringBuilder result = new StringBuilder(content.length());
        StringTokenizer tokenizer = new StringTokenizer(content, "\n\r");
        while (tokenizer.hasMoreTokens()) {
            String line = tokenizer.nextToken().trim();
            if (!"".equals(line)) {
                result.append(indent).append(line).append(IOUtils.LINE_SEPARATOR_UNIX);
            }
        }
        return result.toString();
    }

    private String getSingleLineOfChildren(List<? extends BaseToken> children) {
        StringBuilder result = new StringBuilder();
        Iterator<? extends BaseToken> childrenIt = children.iterator();
        boolean isFirst = true;
        while (childrenIt.hasNext()) {
            Object child = childrenIt.next();
            if (!(child instanceof ContentNode)) {
                return null;
            }
            String content = child.toString();
            if (isFirst) {
                content = Utils.ltrim(content);
            }
            if (!childrenIt.hasNext()) {
                content = Utils.rtrim(content);
            }
            if (content.indexOf(IOUtils.LINE_SEPARATOR_UNIX) >= 0 || content.indexOf("\r") >= 0) {
                return null;
            }
            result.append(content);
            isFirst = false;
        }
        return result.toString();
    }

    /* access modifiers changed from: protected */
    public void serializePrettyHtml(TagNode tagNode, Writer writer, int level, boolean isPreserveWhitespaces, boolean isLastNewLine) throws IOException {
        String str;
        List<? extends BaseToken> tagChildren = tagNode.getAllChildren();
        String tagName = tagNode.getName();
        boolean isHeadlessNode = Utils.isEmptyString(tagName);
        String indent = isHeadlessNode ? "" : getIndent(level);
        if (!isPreserveWhitespaces) {
            if (!isLastNewLine) {
                writer.write(IOUtils.LINE_SEPARATOR_UNIX);
            }
            writer.write(indent);
        }
        serializeOpenTag(tagNode, writer, true);
        boolean preserveWhitespaces = isPreserveWhitespaces || "pre".equalsIgnoreCase(tagName);
        boolean lastWasNewLine = false;
        if (!isMinimizedTagSyntax(tagNode)) {
            String singleLine = getSingleLineOfChildren(tagChildren);
            boolean dontEscape = dontEscape(tagNode);
            if (preserveWhitespaces || singleLine == null) {
                Iterator<? extends BaseToken> childIterator = tagChildren.iterator();
                while (childIterator.hasNext()) {
                    Object next = childIterator.next();
                    if (next instanceof TagNode) {
                        serializePrettyHtml((TagNode) next, writer, isHeadlessNode ? level : level + 1, preserveWhitespaces, lastWasNewLine);
                        lastWasNewLine = false;
                    } else if (next instanceof ContentNode) {
                        String content = dontEscape ? next.toString() : escapeText(next.toString());
                        if (content.length() > 0) {
                            if (dontEscape || preserveWhitespaces) {
                                writer.write(content);
                            } else if (Character.isWhitespace(content.charAt(0))) {
                                if (!lastWasNewLine) {
                                    writer.write(IOUtils.LINE_SEPARATOR_UNIX);
                                    lastWasNewLine = false;
                                }
                                if (content.trim().length() > 0) {
                                    writer.write(getIndentedText(Utils.rtrim(content), isHeadlessNode ? level : level + 1));
                                } else {
                                    lastWasNewLine = true;
                                }
                            } else {
                                if (content.trim().length() > 0) {
                                    writer.write(Utils.rtrim(content));
                                }
                                if (!childIterator.hasNext()) {
                                    writer.write(IOUtils.LINE_SEPARATOR_UNIX);
                                    lastWasNewLine = true;
                                }
                            }
                        }
                    } else if (next instanceof CommentNode) {
                        if (!lastWasNewLine && !preserveWhitespaces) {
                            writer.write(IOUtils.LINE_SEPARATOR_UNIX);
                            lastWasNewLine = false;
                        }
                        String content2 = ((CommentNode) next).getCommentedContent();
                        if (!dontEscape) {
                            content2 = getIndentedText(content2, isHeadlessNode ? level : level + 1);
                        }
                        writer.write(content2);
                    }
                }
            } else {
                if (!dontEscape(tagNode)) {
                    str = escapeText(singleLine);
                } else {
                    str = singleLine;
                }
                writer.write(str);
            }
            if (singleLine == null && !preserveWhitespaces) {
                if (!lastWasNewLine) {
                    writer.write(IOUtils.LINE_SEPARATOR_UNIX);
                }
                writer.write(indent);
            }
            serializeEndTag(tagNode, writer, false);
        }
    }
}
