package org.htmlcleaner;

public enum OptionalOutput {
    omit,
    preserve,
    alwaysOutput
}
