package org.htmlcleaner;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.jdom2.JDOMConstants;

public class DefaultTagProvider implements ITagInfoProvider {
    private static final String CLOSE_BEFORE_COPY_INSIDE_TAGS = "bdo,strong,em,q,b,i,u,tt,sub,sup,big,small,strike,s,font";
    private static final String CLOSE_BEFORE_TAGS = "h1,h2,h3,h4,h5,h6,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml";
    public static final DefaultTagProvider INSTANCE = new DefaultTagProvider();
    private static final String MEDIA_TAGS = "audio,video";
    private static final String PHRASING_TAGS = "a,abbr,area,audio,b,bdi,bdo,br,button,canvas,cite,code,data,datalist,del,dfn,em,embed,i,iframe,img,input,ins,kbd,keygen,label,link,map,mark,math,meta,meter,noscript,object,output,progress,q,ruby,s,samp,script,select,small,span,strong,sub,sup,svg,template,textarea,time,u,var,video,wbr";
    private static final String STRONG = "strong";
    private ConcurrentMap<String, TagInfo> tagInfoMap = new ConcurrentHashMap();

    public DefaultTagProvider() {
        TagInfo tagInfo = new TagInfo("div", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("div", tagInfo);
        TagInfo tagInfo2 = new TagInfo("aside", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo2.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo2.defineCloseBeforeTags("p");
        put("aside", tagInfo2);
        TagInfo tagInfo3 = new TagInfo("section", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo3.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo3.defineCloseBeforeTags("p");
        put("section", tagInfo3);
        TagInfo tagInfo4 = new TagInfo("article", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo4.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo4.defineCloseBeforeTags("p");
        put("article", tagInfo4);
        TagInfo tagInfo5 = new TagInfo("main", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo5.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo5.defineCloseBeforeTags("p");
        put("main", tagInfo5);
        TagInfo tagInfo6 = new TagInfo("nav", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo6.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo6.defineCloseBeforeTags("p");
        put("nav", tagInfo6);
        TagInfo tagInfo7 = new TagInfo("details", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo7.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo7.defineCloseBeforeTags("p");
        put("details", tagInfo7);
        TagInfo tagInfo8 = new TagInfo("summary", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo8.defineRequiredEnclosingTags("details");
        tagInfo8.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo8.defineCloseBeforeTags("p");
        put("summary", tagInfo8);
        TagInfo tagInfo9 = new TagInfo("figure", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo9.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo9.defineCloseBeforeTags("p");
        put("figure", tagInfo9);
        TagInfo tagInfo10 = new TagInfo("figcaption", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any);
        tagInfo10.defineRequiredEnclosingTags("figure");
        put("figcaption", tagInfo10);
        TagInfo tagInfo11 = new TagInfo("header", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo11.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo11.defineCloseBeforeTags("p,header,footer,main");
        put("header", tagInfo11);
        TagInfo tagInfo12 = new TagInfo("footer", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo12.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo12.defineCloseBeforeTags("p,header,footer,main");
        put("footer", tagInfo12);
        TagInfo tagInfo13 = new TagInfo("mark", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo13.defineAllowedChildrenTags(PHRASING_TAGS);
        put("mark", tagInfo13);
        TagInfo tagInfo14 = new TagInfo("bdi", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo14.defineAllowedChildrenTags(PHRASING_TAGS);
        put("bdi", tagInfo14);
        TagInfo tagInfo15 = new TagInfo("time", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo15.defineAllowedChildrenTags(PHRASING_TAGS);
        put("time", tagInfo15);
        TagInfo tagInfo16 = new TagInfo("meter", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo16.defineAllowedChildrenTags(PHRASING_TAGS);
        tagInfo16.defineCloseBeforeTags("meter");
        put("meter", tagInfo16);
        TagInfo tagInfo17 = new TagInfo("ruby", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo17.defineAllowedChildrenTags("rt,rp");
        put("ruby", tagInfo17);
        TagInfo tagInfo18 = new TagInfo("rt", ContentType.text, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.inline);
        tagInfo18.defineAllowedChildrenTags(PHRASING_TAGS);
        put("rt", tagInfo18);
        TagInfo tagInfo19 = new TagInfo("rp", ContentType.text, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.inline);
        tagInfo19.defineAllowedChildrenTags(PHRASING_TAGS);
        put("rp", tagInfo19);
        TagInfo tagInfo20 = new TagInfo("audio", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any);
        tagInfo20.defineCloseInsideCopyAfterTags(MEDIA_TAGS);
        put("audio", tagInfo20);
        TagInfo tagInfo21 = new TagInfo("video", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any);
        tagInfo21.defineCloseInsideCopyAfterTags(MEDIA_TAGS);
        put("video", tagInfo21);
        TagInfo tagInfo22 = new TagInfo("source", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.any);
        tagInfo22.defineRequiredEnclosingTags(MEDIA_TAGS);
        put("source", tagInfo22);
        TagInfo tagInfo23 = new TagInfo("track", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.any);
        tagInfo23.defineRequiredEnclosingTags(MEDIA_TAGS);
        put("track", tagInfo23);
        put("canvas", new TagInfo("canvas", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any));
        put("dialog", new TagInfo("dialog", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any));
        TagInfo tagInfo24 = new TagInfo("progress", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any);
        tagInfo24.defineAllowedChildrenTags(PHRASING_TAGS);
        tagInfo24.defineCloseBeforeTags("progress");
        put("progress", tagInfo24);
        put("span", new TagInfo("span", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("meta", new TagInfo("meta", ContentType.none, BelongsTo.HEAD, false, false, false, CloseTag.forbidden, Display.none));
        put("link", new TagInfo("link", ContentType.none, BelongsTo.HEAD, false, false, false, CloseTag.forbidden, Display.none));
        put("title", new TagInfo("title", ContentType.text, BelongsTo.HEAD, false, true, false, CloseTag.required, Display.none));
        put("style", new TagInfo("style", ContentType.text, BelongsTo.HEAD, false, false, false, CloseTag.required, Display.none));
        put("bgsound", new TagInfo("bgsound", ContentType.none, BelongsTo.HEAD, false, false, false, CloseTag.forbidden, Display.none));
        TagInfo tagInfo25 = new TagInfo("h1", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo25.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo25.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("h1", tagInfo25);
        TagInfo tagInfo26 = new TagInfo("h2", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo26.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo26.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("h2", tagInfo26);
        TagInfo tagInfo27 = new TagInfo("h3", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo27.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo27.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("h3", tagInfo27);
        TagInfo tagInfo28 = new TagInfo("h4", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo28.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo28.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("h4", tagInfo28);
        TagInfo tagInfo29 = new TagInfo("h5", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo29.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo29.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("h5", tagInfo29);
        TagInfo tagInfo30 = new TagInfo("h6", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo30.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo30.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("h6", tagInfo30);
        TagInfo tagInfo31 = new TagInfo("p", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo31.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo31.defineCloseBeforeTags("p,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("p", tagInfo31);
        put(STRONG, new TagInfo(STRONG, ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("em", new TagInfo("em", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("abbr", new TagInfo("abbr", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("acronym", new TagInfo("acronym", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        TagInfo tagInfo32 = new TagInfo("address", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo32.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo32.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("address", tagInfo32);
        put("bdo", new TagInfo("bdo", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        TagInfo tagInfo33 = new TagInfo("blockquote", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo33.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo33.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("blockquote", tagInfo33);
        put("cite", new TagInfo("cite", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("q", new TagInfo("q", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("code", new TagInfo("code", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("ins", new TagInfo("ins", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any));
        put("del", new TagInfo("del", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any));
        put("dfn", new TagInfo("dfn", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("kbd", new TagInfo("kbd", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        TagInfo tagInfo34 = new TagInfo("pre", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo34.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo34.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("pre", tagInfo34);
        put("samp", new TagInfo("samp", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        TagInfo tagInfo35 = new TagInfo("listing", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo35.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo35.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("listing", tagInfo35);
        put("var", new TagInfo("var", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("br", new TagInfo("br", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.none));
        put("wbr", new TagInfo("wbr", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.none));
        TagInfo tagInfo36 = new TagInfo("nobr", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo36.defineCloseBeforeTags("nobr");
        put("nobr", tagInfo36);
        put("xmp", new TagInfo("xmp", ContentType.text, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        TagInfo tagInfo37 = new TagInfo("a", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo37.defineCloseBeforeTags("a");
        put("a", tagInfo37);
        put("base", new TagInfo("base", ContentType.none, BelongsTo.HEAD, false, false, false, CloseTag.forbidden, Display.none));
        put("img", new TagInfo("img", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.inline));
        TagInfo tagInfo38 = new TagInfo("area", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.none);
        tagInfo38.defineFatalTags("map");
        tagInfo38.defineCloseBeforeTags("area");
        put("area", tagInfo38);
        TagInfo tagInfo39 = new TagInfo("map", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any);
        tagInfo39.defineCloseBeforeTags("map");
        put("map", tagInfo39);
        put("object", new TagInfo("object", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any));
        TagInfo tagInfo40 = new TagInfo("param", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.none);
        tagInfo40.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo40.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("param", tagInfo40);
        put("applet", new TagInfo("applet", ContentType.all, BelongsTo.BODY, true, false, false, CloseTag.required, Display.any));
        put(JDOMConstants.NS_PREFIX_XML, new TagInfo(JDOMConstants.NS_PREFIX_XML, ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.none));
        TagInfo tagInfo41 = new TagInfo("ul", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo41.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo41.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("ul", tagInfo41);
        TagInfo tagInfo42 = new TagInfo("ol", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo42.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo42.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("ol", tagInfo42);
        TagInfo tagInfo43 = new TagInfo("li", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo43.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo43.defineCloseBeforeTags("li,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("li", tagInfo43);
        TagInfo tagInfo44 = new TagInfo("dl", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo44.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo44.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("dl", tagInfo44);
        TagInfo tagInfo45 = new TagInfo("dt", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo45.defineCloseBeforeTags("dt,dd");
        put("dt", tagInfo45);
        TagInfo tagInfo46 = new TagInfo("dd", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo46.defineCloseBeforeTags("dt,dd");
        put("dd", tagInfo46);
        TagInfo tagInfo47 = new TagInfo("menu", ContentType.all, BelongsTo.BODY, true, false, false, CloseTag.required, Display.block);
        tagInfo47.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo47.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("menu", tagInfo47);
        TagInfo tagInfo48 = new TagInfo("dir", ContentType.all, BelongsTo.BODY, true, false, false, CloseTag.required, Display.block);
        tagInfo48.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo48.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("dir", tagInfo48);
        TagInfo tagInfo49 = new TagInfo("table", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo49.defineAllowedChildrenTags("tr,tbody,thead,tfoot,colgroup,caption");
        tagInfo49.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo49.defineCloseBeforeTags("tr,thead,tbody,tfoot,caption,colgroup,table,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("table", tagInfo49);
        TagInfo tagInfo50 = new TagInfo("tr", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo50.defineFatalTags("table");
        tagInfo50.defineRequiredEnclosingTags("tbody");
        tagInfo50.defineAllowedChildrenTags("td,th");
        tagInfo50.defineHigherLevelTags("thead,tfoot");
        tagInfo50.defineCloseBeforeTags("tr,td,th,caption,colgroup");
        put("tr", tagInfo50);
        TagInfo tagInfo51 = new TagInfo("td", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo51.defineFatalTags("table");
        tagInfo51.defineRequiredEnclosingTags("tr");
        tagInfo51.defineCloseBeforeTags("td,th,caption,colgroup");
        put("td", tagInfo51);
        TagInfo tagInfo52 = new TagInfo("th", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo52.defineFatalTags("table");
        tagInfo52.defineRequiredEnclosingTags("tr");
        tagInfo52.defineCloseBeforeTags("td,th,caption,colgroup");
        put("th", tagInfo52);
        TagInfo tagInfo53 = new TagInfo("tbody", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo53.defineFatalTags("table");
        tagInfo53.defineAllowedChildrenTags("tr,form");
        tagInfo53.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
        put("tbody", tagInfo53);
        TagInfo tagInfo54 = new TagInfo("thead", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo54.defineFatalTags("table");
        tagInfo54.defineAllowedChildrenTags("tr,form");
        tagInfo54.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
        put("thead", tagInfo54);
        TagInfo tagInfo55 = new TagInfo("tfoot", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo55.defineFatalTags("table");
        tagInfo55.defineAllowedChildrenTags("tr,form");
        tagInfo55.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
        put("tfoot", tagInfo55);
        TagInfo tagInfo56 = new TagInfo("col", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.block);
        tagInfo56.defineFatalTags("colgroup");
        put("col", tagInfo56);
        TagInfo tagInfo57 = new TagInfo("colgroup", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo57.defineFatalTags("table");
        tagInfo57.defineAllowedChildrenTags("col");
        tagInfo57.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
        put("colgroup", tagInfo57);
        TagInfo tagInfo58 = new TagInfo("caption", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo58.defineFatalTags("table");
        tagInfo58.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
        put("caption", tagInfo58);
        TagInfo tagInfo59 = new TagInfo("form", ContentType.all, BelongsTo.BODY, false, false, true, CloseTag.required, Display.block);
        tagInfo59.defineForbiddenTags("form");
        tagInfo59.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo59.defineCloseBeforeTags("option,optgroup,textarea,select,fieldset,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("form", tagInfo59);
        TagInfo tagInfo60 = new TagInfo("input", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.inline);
        tagInfo60.defineCloseBeforeTags("select,optgroup,option");
        put("input", tagInfo60);
        TagInfo tagInfo61 = new TagInfo("textarea", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo61.defineCloseBeforeTags("select,optgroup,option");
        put("textarea", tagInfo61);
        TagInfo tagInfo62 = new TagInfo("select", ContentType.all, BelongsTo.BODY, false, false, true, CloseTag.required, Display.inline);
        tagInfo62.defineAllowedChildrenTags("option,optgroup");
        tagInfo62.defineCloseBeforeTags("option,optgroup,select");
        put("select", tagInfo62);
        TagInfo tagInfo63 = new TagInfo("option", ContentType.text, BelongsTo.BODY, false, false, true, CloseTag.optional, Display.inline);
        tagInfo63.defineFatalTags("select");
        tagInfo63.defineCloseBeforeTags("option");
        put("option", tagInfo63);
        TagInfo tagInfo64 = new TagInfo("optgroup", ContentType.all, BelongsTo.BODY, false, false, true, CloseTag.required, Display.inline);
        tagInfo64.defineFatalTags("select");
        tagInfo64.defineAllowedChildrenTags("option");
        tagInfo64.defineCloseBeforeTags("optgroup");
        put("optgroup", tagInfo64);
        TagInfo tagInfo65 = new TagInfo("button", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any);
        tagInfo65.defineCloseBeforeTags("select,optgroup,option");
        put("button", tagInfo65);
        put("label", new TagInfo("label", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        TagInfo tagInfo66 = new TagInfo("legend", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo66.defineAllowedChildrenTags(PHRASING_TAGS);
        put("legend", tagInfo66);
        TagInfo tagInfo67 = new TagInfo("fieldset", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo67.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo67.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("fieldset", tagInfo67);
        TagInfo tagInfo68 = new TagInfo("isindex", ContentType.none, BelongsTo.BODY, true, false, false, CloseTag.forbidden, Display.block);
        tagInfo68.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo68.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("isindex", tagInfo68);
        put("script", new TagInfo("script", ContentType.all, BelongsTo.HEAD_AND_BODY, false, false, false, CloseTag.required, Display.none));
        put("noscript", new TagInfo("noscript", ContentType.all, BelongsTo.HEAD_AND_BODY, false, false, false, CloseTag.required, Display.block));
        TagInfo tagInfo69 = new TagInfo("b", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo69.defineCloseInsideCopyAfterTags("u,i,tt,sub,sup,big,small,strike,blink,s");
        put("b", tagInfo69);
        TagInfo tagInfo70 = new TagInfo("i", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo70.defineCloseInsideCopyAfterTags("b,u,tt,sub,sup,big,small,strike,blink,s");
        put("i", tagInfo70);
        TagInfo tagInfo71 = new TagInfo("u", ContentType.all, BelongsTo.BODY, true, false, false, CloseTag.required, Display.inline);
        tagInfo71.defineCloseInsideCopyAfterTags("b,i,tt,sub,sup,big,small,strike,blink,s");
        put("u", tagInfo71);
        TagInfo tagInfo72 = new TagInfo("tt", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo72.defineCloseInsideCopyAfterTags("b,u,i,sub,sup,big,small,strike,blink,s");
        put("tt", tagInfo72);
        TagInfo tagInfo73 = new TagInfo("sub", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo73.defineCloseInsideCopyAfterTags("b,u,i,tt,sup,big,small,strike,blink,s");
        put("sub", tagInfo73);
        TagInfo tagInfo74 = new TagInfo("sup", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo74.defineCloseInsideCopyAfterTags("b,u,i,tt,sub,big,small,strike,blink,s");
        put("sup", tagInfo74);
        TagInfo tagInfo75 = new TagInfo("big", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo75.defineCloseInsideCopyAfterTags("b,u,i,tt,sub,sup,small,strike,blink,s");
        put("big", tagInfo75);
        TagInfo tagInfo76 = new TagInfo("small", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo76.defineCloseInsideCopyAfterTags("b,u,i,tt,sub,sup,big,strike,blink,s");
        put("small", tagInfo76);
        TagInfo tagInfo77 = new TagInfo("strike", ContentType.all, BelongsTo.BODY, true, false, false, CloseTag.required, Display.inline);
        tagInfo77.defineCloseInsideCopyAfterTags("b,u,i,tt,sub,sup,big,small,blink,s");
        put("strike", tagInfo77);
        TagInfo tagInfo78 = new TagInfo("blink", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo78.defineCloseInsideCopyAfterTags("b,u,i,tt,sub,sup,big,small,strike,s");
        put("blink", tagInfo78);
        TagInfo tagInfo79 = new TagInfo("marquee", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo79.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo79.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("marquee", tagInfo79);
        TagInfo tagInfo80 = new TagInfo("s", ContentType.all, BelongsTo.BODY, true, false, false, CloseTag.required, Display.inline);
        tagInfo80.defineCloseInsideCopyAfterTags("b,u,i,tt,sub,sup,big,small,strike,blink");
        put("s", tagInfo80);
        TagInfo tagInfo81 = new TagInfo("hr", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.block);
        tagInfo81.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo81.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("hr", tagInfo81);
        put("font", new TagInfo("font", ContentType.all, BelongsTo.BODY, true, false, false, CloseTag.required, Display.inline));
        put("basefont", new TagInfo("basefont", ContentType.none, BelongsTo.BODY, true, false, false, CloseTag.forbidden, Display.none));
        TagInfo tagInfo82 = new TagInfo("center", ContentType.all, BelongsTo.BODY, true, false, false, CloseTag.required, Display.block);
        tagInfo82.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo82.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("center", tagInfo82);
        put("comment", new TagInfo("comment", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.none));
        put("server", new TagInfo("server", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.none));
        put("iframe", new TagInfo("iframe", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any));
        TagInfo tagInfo83 = new TagInfo("embed", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.block);
        tagInfo83.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo83.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("embed", tagInfo83);
    }

    /* access modifiers changed from: protected */
    public void put(String tagName, TagInfo tagInfo) {
        this.tagInfoMap.put(tagName, tagInfo);
    }

    public TagInfo getTagInfo(String tagName) {
        if (tagName == null) {
            return null;
        }
        return this.tagInfoMap.get(tagName);
    }
}
