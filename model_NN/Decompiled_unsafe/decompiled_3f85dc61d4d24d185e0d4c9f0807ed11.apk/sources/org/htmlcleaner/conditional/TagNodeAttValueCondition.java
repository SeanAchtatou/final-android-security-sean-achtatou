package org.htmlcleaner.conditional;

import org.htmlcleaner.TagNode;

public class TagNodeAttValueCondition implements ITagNodeCondition {
    private String attName;
    private String attValue;
    private boolean isCaseSensitive;

    public TagNodeAttValueCondition(String attName2, String attValue2, boolean isCaseSensitive2) {
        this.attName = attName2;
        this.attValue = attValue2;
        this.isCaseSensitive = isCaseSensitive2;
    }

    public boolean satisfy(TagNode tagNode) {
        if (tagNode == null || this.attName == null || this.attValue == null) {
            return false;
        }
        if (this.isCaseSensitive) {
            return this.attValue.equals(tagNode.getAttributeByName(this.attName));
        }
        return this.attValue.equalsIgnoreCase(tagNode.getAttributeByName(this.attName));
    }
}
