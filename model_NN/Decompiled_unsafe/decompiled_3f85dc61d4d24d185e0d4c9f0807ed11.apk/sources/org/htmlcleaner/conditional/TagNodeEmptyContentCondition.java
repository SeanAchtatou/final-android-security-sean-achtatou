package org.htmlcleaner.conditional;

import java.util.HashSet;
import java.util.Set;
import org.htmlcleaner.ContentNode;
import org.htmlcleaner.Display;
import org.htmlcleaner.ITagInfoProvider;
import org.htmlcleaner.TagInfo;
import org.htmlcleaner.TagNode;
import org.htmlcleaner.Utils;

public class TagNodeEmptyContentCondition implements ITagNodeCondition {
    private static final String ID_ATTRIBUTE_NAME = "id";
    private static final Set<String> unsafeBlockElements = new HashSet();
    private ITagInfoProvider tagInfoProvider;

    static {
        unsafeBlockElements.add("td");
        unsafeBlockElements.add("th");
    }

    public TagNodeEmptyContentCondition(ITagInfoProvider provider) {
        this.tagInfoProvider = provider;
    }

    public boolean satisfy(TagNode tagNode) {
        return satisfy(tagNode, false);
    }

    private boolean satisfy(TagNode tagNode, boolean override) {
        String name = tagNode.getName();
        TagInfo tagInfo = this.tagInfoProvider.getTagInfo(name);
        if (tagInfo == null || hasIdAttributeSet(tagNode) || Display.none == tagInfo.getDisplay() || tagInfo.isEmptyTag() || ((!override && unsafeBlockElements.contains(name)) || !Utils.isEmptyString(tagNode.getText()))) {
            return false;
        }
        if (tagNode.isEmpty()) {
            return true;
        }
        for (Object child : tagNode.getAllChildren()) {
            if (child instanceof TagNode) {
                if (!satisfy((TagNode) child, true)) {
                    return false;
                }
            } else if (!(child instanceof ContentNode)) {
                return false;
            } else {
                if (!((ContentNode) child).isBlank()) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean hasIdAttributeSet(TagNode tagNode) {
        return !Utils.isEmptyString(tagNode.getAttributes().get("id"));
    }
}
