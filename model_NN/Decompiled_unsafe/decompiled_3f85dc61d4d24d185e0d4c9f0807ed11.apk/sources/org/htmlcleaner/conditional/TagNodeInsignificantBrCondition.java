package org.htmlcleaner.conditional;

import java.util.List;
import org.htmlcleaner.TagNode;

public class TagNodeInsignificantBrCondition implements ITagNodeCondition {
    private static final String BR_TAG = "br";

    public boolean satisfy(TagNode tagNode) {
        if (!isBrNode(tagNode)) {
            return false;
        }
        List children = tagNode.getParent().getAllChildren();
        int brIndex = children.indexOf(tagNode);
        if (checkSublist(0, brIndex, children) || checkSublist(brIndex, children.size(), children)) {
            return true;
        }
        return false;
    }

    private boolean isBrNode(TagNode tagNode) {
        return tagNode != null && BR_TAG.equals(tagNode.getName());
    }

    private boolean checkSublist(int start, int end, List list) {
        for (Object object : list.subList(start, end)) {
            if (!(object instanceof TagNode)) {
                return false;
            }
            TagNode node = (TagNode) object;
            if (!isBrNode(node) && !node.isPruned()) {
                return false;
            }
        }
        return true;
    }
}
