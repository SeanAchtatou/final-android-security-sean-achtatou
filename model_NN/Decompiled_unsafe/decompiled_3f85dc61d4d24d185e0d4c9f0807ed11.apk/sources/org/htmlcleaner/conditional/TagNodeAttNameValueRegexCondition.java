package org.htmlcleaner.conditional;

import java.util.Map;
import java.util.regex.Pattern;
import org.htmlcleaner.TagNode;

public class TagNodeAttNameValueRegexCondition implements ITagNodeCondition {
    private Pattern attNameRegex;
    private Pattern attValueRegex;

    public TagNodeAttNameValueRegexCondition(Pattern attNameRegex2, Pattern attValueRegex2) {
        this.attNameRegex = attNameRegex2;
        this.attValueRegex = attValueRegex2;
    }

    public boolean satisfy(TagNode tagNode) {
        if (tagNode != null) {
            for (Map.Entry<String, String> entry : tagNode.getAttributes().entrySet()) {
                if ((this.attNameRegex == null || this.attNameRegex.matcher((CharSequence) entry.getKey()).find()) && (this.attValueRegex == null || this.attValueRegex.matcher((CharSequence) entry.getValue()).find())) {
                    return true;
                }
            }
        }
        return false;
    }
}
