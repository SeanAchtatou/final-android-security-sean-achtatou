package org.htmlcleaner.conditional;

import org.htmlcleaner.TagNode;

public class TagNodeNameCondition implements ITagNodeCondition {
    private String name;

    public TagNodeNameCondition(String name2) {
        this.name = name2;
    }

    public boolean satisfy(TagNode tagNode) {
        if (tagNode == null) {
            return false;
        }
        return tagNode.getName().equalsIgnoreCase(this.name);
    }
}
