package org.htmlcleaner;

import com.fsck.k9.Account;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import org.htmlcleaner.audit.ErrorType;
import org.htmlcleaner.conditional.ITagNodeCondition;
import org.jdom2.JDOMConstants;

public class HtmlCleaner {
    public static int HTML_4 = 4;
    public static int HTML_5 = 5;
    private static final String MARKER_ATTRIBUTE = "htmlcleaner_marker";
    private CleanerProperties properties;
    private CleanerTransformations transformations;

    private class TagPos {
        /* access modifiers changed from: private */
        public TagInfo info;
        /* access modifiers changed from: private */
        public String name;
        /* access modifiers changed from: private */
        public int position;

        TagPos(int position2, String name2) {
            this.position = position2;
            this.name = name2;
            this.info = HtmlCleaner.this.getTagInfoProvider().getTagInfo(name2);
        }
    }

    private class ChildBreaks {
        private Stack<TagPos> breakingTags;
        /* access modifiers changed from: private */
        public Stack<TagPos> closedByChildBreak;

        private ChildBreaks() {
            this.closedByChildBreak = new Stack<>();
            this.breakingTags = new Stack<>();
        }

        public void addBreak(TagPos closedPos, TagPos breakPos) {
            this.closedByChildBreak.add(closedPos);
            this.breakingTags.add(breakPos);
        }

        public boolean isEmpty() {
            return this.closedByChildBreak.isEmpty();
        }

        public String getLastBreakingTag() {
            return this.breakingTags.peek().name;
        }

        public TagPos pop() {
            this.breakingTags.pop();
            return this.closedByChildBreak.pop();
        }

        public int getLastBreakingTagPosition() {
            if (this.breakingTags.isEmpty()) {
                return -1;
            }
            return this.breakingTags.peek().position;
        }
    }

    protected class NestingState {
        private ChildBreaks childBreaks = new ChildBreaks();
        private OpenTags openTags = new OpenTags();

        protected NestingState() {
        }

        public OpenTags getOpenTags() {
            return this.openTags;
        }

        public ChildBreaks getChildBreaks() {
            return this.childBreaks;
        }
    }

    class OpenTags {
        private TagPos last;
        /* access modifiers changed from: private */
        public List<TagPos> list = new ArrayList();
        private Set<String> set = new HashSet();

        OpenTags() {
        }

        /* access modifiers changed from: private */
        public boolean isEmpty() {
            return this.list.isEmpty();
        }

        /* access modifiers changed from: private */
        public void addTag(String tagName, int position) {
            this.last = new TagPos(position, tagName);
            this.list.add(this.last);
            this.set.add(tagName);
        }

        /* access modifiers changed from: private */
        public void removeTag(String tagName) {
            ListIterator<TagPos> it = this.list.listIterator(this.list.size());
            while (true) {
                if (it.hasPrevious()) {
                    if (!Thread.currentThread().isInterrupted()) {
                        if (tagName.equals(it.previous().name)) {
                            it.remove();
                            break;
                        }
                    } else {
                        HtmlCleaner.this.handleInterruption();
                        break;
                    }
                } else {
                    break;
                }
            }
            this.last = this.list.isEmpty() ? null : this.list.get(this.list.size() - 1);
        }

        /* access modifiers changed from: private */
        public TagPos findFirstTagPos() {
            if (this.list.isEmpty()) {
                return null;
            }
            return this.list.get(0);
        }

        /* access modifiers changed from: private */
        public TagPos getLastTagPos() {
            return this.last;
        }

        /* access modifiers changed from: private */
        public TagPos findTag(String tagName) {
            if (tagName != null) {
                ListIterator<TagPos> it = this.list.listIterator(this.list.size());
                TagInfo fatalInfo = HtmlCleaner.this.getTagInfoProvider().getTagInfo(tagName);
                while (it.hasPrevious()) {
                    if (Thread.currentThread().isInterrupted()) {
                        HtmlCleaner.this.handleInterruption();
                        return null;
                    }
                    TagPos currTagPos = it.previous();
                    if (tagName.equals(currTagPos.name)) {
                        return currTagPos;
                    }
                    if (fatalInfo != null && fatalInfo.isFatalTag(currTagPos.name)) {
                        return null;
                    }
                }
            }
            return null;
        }

        /* access modifiers changed from: private */
        public boolean tagExists(String tagName) {
            return findTag(tagName) != null;
        }

        /* access modifiers changed from: private */
        public TagPos findTagToPlaceRubbish() {
            TagPos result = null;
            TagPos prev = null;
            if (!isEmpty()) {
                ListIterator<TagPos> it = this.list.listIterator(this.list.size());
                while (it.hasPrevious()) {
                    if (Thread.currentThread().isInterrupted()) {
                        HtmlCleaner.this.handleInterruption();
                        return null;
                    }
                    result = it.previous();
                    if ((result.info == null || result.info.allowsAnything()) && prev != null) {
                        return prev;
                    }
                    prev = result;
                }
            }
            return result;
        }

        /* access modifiers changed from: private */
        public boolean tagEncountered(String tagName) {
            return this.set.contains(tagName);
        }

        /* access modifiers changed from: private */
        public boolean someAlreadyOpen(Set<String> tags) {
            for (TagPos curr : this.list) {
                if (tags.contains(curr.name)) {
                    return true;
                }
            }
            return false;
        }
    }

    public HtmlCleaner() {
        this(null, null);
    }

    public HtmlCleaner(ITagInfoProvider tagInfoProvider) {
        this(tagInfoProvider, null);
    }

    public HtmlCleaner(CleanerProperties properties2) {
        this(null, properties2);
    }

    public HtmlCleaner(ITagInfoProvider tagInfoProvider, CleanerProperties properties2) {
        this.properties = properties2 == null ? new CleanerProperties() : properties2;
        if (tagInfoProvider == null && this.properties.getTagInfoProvider() == null) {
            if (this.properties.getHtmlVersion() == HTML_4) {
                this.properties.setTagInfoProvider(Html4TagProvider.INSTANCE);
            } else {
                this.properties.setTagInfoProvider(Html5TagProvider.INSTANCE);
            }
        } else if (tagInfoProvider != null) {
            this.properties.setTagInfoProvider(tagInfoProvider == null ? Html4TagProvider.INSTANCE : tagInfoProvider);
        }
    }

    public TagNode clean(String htmlContent) {
        try {
            return clean(new StringReader(htmlContent), new CleanTimeValues());
        } catch (IOException e) {
            throw new HtmlCleanerException(e);
        }
    }

    public TagNode clean(File file, String charset) throws IOException {
        FileInputStream in = new FileInputStream(file);
        Reader reader = null;
        try {
            Reader reader2 = new InputStreamReader(in, charset);
            try {
                TagNode clean = clean(reader2, new CleanTimeValues());
                if (reader2 != null) {
                    try {
                        reader2.close();
                    } catch (IOException e) {
                    }
                }
                try {
                    in.close();
                } catch (IOException e2) {
                }
                return clean;
            } catch (Throwable th) {
                th = th;
                reader = reader2;
            }
        } catch (Throwable th2) {
            th = th2;
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e3) {
                }
            }
            try {
                in.close();
            } catch (IOException e4) {
            }
            throw th;
        }
    }

    public TagNode clean(File file) throws IOException {
        return clean(file, this.properties.getCharset());
    }

    @Deprecated
    public TagNode clean(URL url, String charset) throws IOException {
        return clean(new StringReader(Utils.readUrl(url, charset).toString()), new CleanTimeValues());
    }

    public TagNode clean(URL url) throws IOException {
        return clean(url, this.properties.getCharset());
    }

    public TagNode clean(InputStream in, String charset) throws IOException {
        return clean(new InputStreamReader(in, charset), new CleanTimeValues());
    }

    public TagNode clean(InputStream in) throws IOException {
        return clean(in, this.properties.getCharset());
    }

    public TagNode clean(Reader reader) throws IOException {
        return clean(reader, new CleanTimeValues());
    }

    /* access modifiers changed from: protected */
    public TagNode clean(Reader reader, CleanTimeValues cleanTimeValues) throws IOException {
        pushNesting(cleanTimeValues);
        cleanTimeValues._headOpened = false;
        cleanTimeValues._bodyOpened = false;
        cleanTimeValues._headTags.clear();
        cleanTimeValues.allTags.clear();
        cleanTimeValues.pruneTagSet = new HashSet(this.properties.getPruneTagSet());
        cleanTimeValues.allowTagSet = new HashSet(this.properties.getAllowTagSet());
        this.transformations = this.properties.getCleanerTransformations();
        cleanTimeValues.pruneNodeSet.clear();
        cleanTimeValues.htmlNode = newTagNode("html");
        cleanTimeValues.bodyNode = newTagNode("body");
        cleanTimeValues.headNode = newTagNode("head");
        cleanTimeValues.rootNode = null;
        cleanTimeValues.htmlNode.addChild(cleanTimeValues.headNode);
        cleanTimeValues.htmlNode.addChild(cleanTimeValues.bodyNode);
        HtmlTokenizer htmlTokenizer = new HtmlTokenizer(this, reader, cleanTimeValues);
        htmlTokenizer.start();
        if (Thread.currentThread().isInterrupted()) {
            handleInterruption();
            return null;
        }
        List nodeList = htmlTokenizer.getTokenList();
        closeAll(nodeList, cleanTimeValues);
        if (Thread.currentThread().isInterrupted()) {
            handleInterruption();
            return null;
        }
        createDocumentNodes(nodeList, cleanTimeValues);
        if (Thread.currentThread().isInterrupted()) {
            handleInterruption();
            return null;
        }
        calculateRootNode(cleanTimeValues, htmlTokenizer.getNamespacePrefixes());
        if (Thread.currentThread().isInterrupted()) {
            handleInterruption();
            return null;
        }
        while (markNodesToPrune(nodeList, cleanTimeValues)) {
            if (Thread.currentThread().isInterrupted()) {
                handleInterruption();
                return null;
            }
        }
        if (cleanTimeValues.pruneNodeSet != null && !cleanTimeValues.pruneNodeSet.isEmpty()) {
            for (TagNode tagNode : cleanTimeValues.pruneNodeSet) {
                if (Thread.currentThread().isInterrupted()) {
                    handleInterruption();
                    return null;
                }
                TagNode parent = tagNode.getParent();
                if (parent != null) {
                    parent.removeChild(tagNode);
                }
            }
        }
        cleanTimeValues.rootNode.setDocType(htmlTokenizer.getDocType());
        popNesting(cleanTimeValues);
        return cleanTimeValues.rootNode;
    }

    private boolean markNodesToPrune(List nodeList, CleanTimeValues cleanTimeValues) {
        boolean nodesPruned = false;
        for (Object next : nodeList) {
            if ((next instanceof TagNode) && !cleanTimeValues.pruneNodeSet.contains(next)) {
                TagNode node = (TagNode) next;
                if (addIfNeededToPruneSet(node, cleanTimeValues)) {
                    nodesPruned = true;
                } else if (!node.isEmpty()) {
                    nodesPruned |= markNodesToPrune(node.getAllChildren(), cleanTimeValues);
                }
            }
        }
        return nodesPruned;
    }

    private void calculateRootNode(CleanTimeValues cleanTimeValues, Set<String> namespacePrefixes) {
        cleanTimeValues.rootNode = cleanTimeValues.htmlNode;
        if (this.properties.isOmitHtmlEnvelope()) {
            List<Object> bodyChildren = cleanTimeValues.bodyNode.getAllChildren();
            cleanTimeValues.rootNode = new TagNode(null);
            if (bodyChildren != null) {
                for (Object currChild : bodyChildren) {
                    cleanTimeValues.rootNode.addChild(currChild);
                }
            }
        }
        Map<String, String> atts = cleanTimeValues.rootNode.getAttributes();
        if (cleanTimeValues.rootNode.hasAttribute("xmlns")) {
            cleanTimeValues.rootNode.addNamespaceDeclaration("", cleanTimeValues.rootNode.getAttributeByName("xmlns"));
        }
        if (this.properties.isNamespacesAware() && namespacePrefixes != null) {
            for (String prefix : namespacePrefixes) {
                if (Thread.currentThread().isInterrupted()) {
                    handleInterruption();
                    return;
                }
                String xmlnsAtt = "xmlns:" + prefix;
                if (!atts.containsKey(xmlnsAtt) && !prefix.equals(JDOMConstants.NS_PREFIX_XML)) {
                    cleanTimeValues.rootNode.addAttribute(xmlnsAtt, prefix);
                }
            }
        }
    }

    private void addAttributesToTag(TagNode tag, Map<String, String> attributes) {
        if (attributes != null) {
            Map<String, String> tagAttributes = tag.getAttributes();
            for (Map.Entry<String, String> currEntry : attributes.entrySet()) {
                String attName = (String) currEntry.getKey();
                if (!tagAttributes.containsKey(attName)) {
                    tag.addAttribute(attName, (String) currEntry.getValue());
                }
            }
        }
    }

    private boolean isFatalTagSatisfied(TagInfo tag, CleanTimeValues cleanTimeValues) {
        boolean fatal = true;
        if (tag != null) {
            if (tag.getFatalTags().isEmpty()) {
                return true;
            }
            fatal = false;
            for (String fatalTagName : tag.getFatalTags()) {
                if (getOpenTags(cleanTimeValues).tagExists(fatalTagName)) {
                    fatal = true;
                }
            }
        }
        return fatal;
    }

    private boolean mustAddRequiredParent(TagInfo tag, CleanTimeValues cleanTimeValues) {
        TagPos currTagPos;
        TagPos tagPos;
        boolean z = true;
        if (tag == null || tag.getRequiredParentTags().isEmpty()) {
            return false;
        }
        int fatalTagPosition = -1;
        for (String fatalTag : tag.getFatalTags()) {
            if (!(fatalTag == null || (tagPos = getOpenTags(cleanTimeValues).findTag(fatalTag)) == null)) {
                fatalTagPosition = tagPos.position;
            }
        }
        boolean requiredTagMissing = true;
        for (String requiredTag : tag.getRequiredParentTags()) {
            if (!(requiredTag == null || (currTagPos = getOpenTags(cleanTimeValues).findTag(requiredTag)) == null)) {
                requiredTagMissing = currTagPos.position <= fatalTagPosition;
            }
        }
        if (!requiredTagMissing) {
            return false;
        }
        ListIterator it = getOpenTags(cleanTimeValues).list.listIterator(getOpenTags(cleanTimeValues).list.size());
        while (it.hasPrevious()) {
            TagPos currTagPos2 = (TagPos) it.previous();
            if (Thread.currentThread().isInterrupted()) {
                handleInterruption();
                if (currTagPos2.position > fatalTagPosition) {
                    z = false;
                }
                return z;
            } else if (tag.isHigher(currTagPos2.name)) {
                if (currTagPos2.position > fatalTagPosition) {
                    z = false;
                }
                return z;
            }
        }
        return true;
    }

    private TagNode newTagNode(String tagName) {
        return new TagNode(tagName);
    }

    private TagNode createTagNode(TagNode startTagToken) {
        startTagToken.setFormed();
        return startTagToken;
    }

    private boolean isAllowedInLastOpenTag(BaseToken token, CleanTimeValues cleanTimeValues) {
        TagPos last = getOpenTags(cleanTimeValues).getLastTagPos();
        if (last == null || last.info == null) {
            return true;
        }
        return last.info.allowsItem(token);
    }

    private void saveToLastOpenTag(List nodeList, Object tokenToAdd, CleanTimeValues cleanTimeValues) {
        TagPos rubbishPos;
        TagNode startTagToken;
        TagPos last = getOpenTags(cleanTimeValues).getLastTagPos();
        if ((last == null || last.info == null || !last.info.isIgnorePermitted()) && (rubbishPos = getOpenTags(cleanTimeValues).findTagToPlaceRubbish()) != null && (startTagToken = (TagNode) nodeList.get(rubbishPos.position)) != null) {
            startTagToken.addItemForMoving(tokenToAdd);
        }
    }

    private boolean isStartToken(Object o) {
        return (o instanceof TagNode) && !((TagNode) o).isFormed();
    }

    private boolean isAllowedAsForeignMarkup(String tagname, CleanTimeValues cleanTimeValues) {
        String ns;
        if (!this.properties.isNamespacesAware() || tagname == null) {
            return false;
        }
        if (tagname.contains(":")) {
            return true;
        }
        if (cleanTimeValues.namespace == null || cleanTimeValues.namespace.size() == 0 || (ns = cleanTimeValues.namespace.peek()) == null || ns.equals("http://www.w3.org/1999/xhtml")) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void makeTree(List nodeList, ListIterator<BaseToken> nodeIterator, CleanTimeValues cleanTimeValues) {
        TagInfo lastTagInfo;
        while (nodeIterator.hasNext()) {
            if (Thread.currentThread().isInterrupted()) {
                handleInterruption();
                return;
            }
            BaseToken token = nodeIterator.next();
            if (token instanceof EndTagToken) {
                EndTagToken endTagToken = (EndTagToken) token;
                String tagName = endTagToken.getName();
                TagInfo tag = getTagInfo(tagName, cleanTimeValues);
                if ((tag == null && this.properties.isOmitUnknownTags() && !isAllowedAsForeignMarkup(tagName, cleanTimeValues)) || (tag != null && tag.isDeprecated() && this.properties.isOmitDeprecatedTags())) {
                    nodeIterator.set(null);
                } else if (tag == null || tag.allowsBody()) {
                    TagPos matchingPosition = getOpenTags(cleanTimeValues).findTag(tagName);
                    if (matchingPosition != null) {
                        List closed = closeSnippet(nodeList, matchingPosition, endTagToken, cleanTimeValues);
                        if (closed.size() > 0 && closed.get(0).hasAttribute("xmlns")) {
                            cleanTimeValues.namespace.pop();
                        }
                        nodeIterator.set(null);
                        for (int i = closed.size() - 1; i >= 0; i--) {
                            TagNode closedTag = closed.get(i);
                            if (i > 0 && tag != null && tag.isContinueAfter(closedTag.getName())) {
                                TagNode cloned = closedTag.makeCopy();
                                cloned.setAutoGenerated(true);
                                nodeIterator.add(cloned);
                                nodeIterator.previous();
                            }
                        }
                        if (!getChildBreaks(cleanTimeValues).isEmpty()) {
                            while (matchingPosition.position < getChildBreaks(cleanTimeValues).getLastBreakingTagPosition()) {
                                getChildBreaks(cleanTimeValues).pop();
                            }
                        }
                        while (!getChildBreaks(cleanTimeValues).isEmpty() && tagName.equals(getChildBreaks(cleanTimeValues).getLastBreakingTag()) && matchingPosition.position == getChildBreaks(cleanTimeValues).getLastBreakingTagPosition()) {
                            if (nodeList.get(((TagPos) getChildBreaks(cleanTimeValues).closedByChildBreak.peek()).position) != null) {
                                int position = getChildBreaks(cleanTimeValues).pop().position;
                                Object toReopen = nodeList.get(position);
                                if (toReopen instanceof TagNode) {
                                    reopenBrokenNode(nodeIterator, (TagNode) toReopen, cleanTimeValues);
                                } else if (toReopen instanceof List) {
                                    for (TagNode n : (List) toReopen) {
                                        if (!Thread.currentThread().isInterrupted()) {
                                            nodeIterator.add(n);
                                            makeTree(nodeList, nodeList.listIterator(nodeList.size() - 1), cleanTimeValues);
                                        } else {
                                            return;
                                        }
                                    }
                                    nodeList.set(position, null);
                                } else {
                                    continue;
                                }
                            } else {
                                getChildBreaks(cleanTimeValues).pop();
                            }
                        }
                    } else {
                        continue;
                    }
                } else {
                    nodeIterator.set(null);
                }
            } else if (isStartToken(token)) {
                TagNode startTagToken = (TagNode) token;
                String tagName2 = startTagToken.getName();
                TagInfo tag2 = getTagInfo(tagName2, cleanTimeValues);
                TagPos lastTagPos = getOpenTags(cleanTimeValues).isEmpty() ? null : getOpenTags(cleanTimeValues).getLastTagPos();
                if (lastTagPos == null) {
                    lastTagInfo = null;
                } else {
                    lastTagInfo = getTagInfo(lastTagPos.name, cleanTimeValues);
                }
                cleanTimeValues.allTags.add(tagName2);
                if (startTagToken.hasAttribute("xmlns")) {
                    String ns = startTagToken.getAttributeByName("xmlns");
                    if (ns.equals("https://www.w3.org/1999/xhtml") || ns.equals("http://w3.org/1999/xhtml")) {
                        ns = "http://www.w3.org/1999/xhtml";
                        Map<String, String> attributes = startTagToken.getAttributes();
                        attributes.put("xmlns", "http://www.w3.org/1999/xhtml");
                        startTagToken.setAttributes(attributes);
                    }
                    if ("html".equals(tagName2) && ns.equals("http://www.w3.org/TR/REC-html40")) {
                        startTagToken.removeAttribute("xmlns");
                    } else if (ns.trim().isEmpty()) {
                        startTagToken.removeAttribute("xmlns");
                    } else {
                        cleanTimeValues.namespace.push(ns);
                        startTagToken.addNamespaceDeclaration("", ns);
                    }
                }
                if (isAllowedAsForeignMarkup(tagName2, cleanTimeValues)) {
                    startTagToken.setForeignMarkup(true);
                } else {
                    startTagToken.setForeignMarkup(false);
                }
                if ("html".equals(tagName2)) {
                    addAttributesToTag(cleanTimeValues.htmlNode, startTagToken.getAttributes());
                    nodeIterator.set(null);
                } else if ("body".equals(tagName2)) {
                    cleanTimeValues._bodyOpened = true;
                    addAttributesToTag(cleanTimeValues.bodyNode, startTagToken.getAttributes());
                    nodeIterator.set(null);
                } else if ("head".equals(tagName2)) {
                    cleanTimeValues._headOpened = true;
                    addAttributesToTag(cleanTimeValues.headNode, startTagToken.getAttributes());
                    nodeIterator.set(null);
                } else if (tag2 == null && this.properties.isOmitUnknownTags() && !isAllowedAsForeignMarkup(tagName2, cleanTimeValues)) {
                    nodeIterator.set(null);
                    this.properties.fireUglyHtml(true, startTagToken, ErrorType.Unknown);
                } else if (tag2 != null && tag2.isDeprecated() && this.properties.isOmitDeprecatedTags()) {
                    nodeIterator.set(null);
                    this.properties.fireUglyHtml(true, startTagToken, ErrorType.Deprecated);
                } else if (tag2 == null && lastTagInfo != null && !lastTagInfo.allowsAnything()) {
                    closeSnippet(nodeList, lastTagPos, startTagToken, cleanTimeValues);
                    nodeIterator.previous();
                } else if (tag2 != null && tag2.hasPermittedTags() && getOpenTags(cleanTimeValues).someAlreadyOpen(tag2.getPermittedTags())) {
                    nodeIterator.set(null);
                } else if (tag2 != null && tag2.isUnique() && getOpenTags(cleanTimeValues).tagEncountered(tagName2)) {
                    nodeIterator.set(null);
                    this.properties.fireHtmlError(true, startTagToken, ErrorType.UniqueTagDuplicated);
                } else if (!isFatalTagSatisfied(tag2, cleanTimeValues)) {
                    nodeIterator.set(null);
                    this.properties.fireHtmlError(true, startTagToken, ErrorType.FatalTagMissing);
                } else if (mustAddRequiredParent(tag2, cleanTimeValues)) {
                    TagNode requiredParentStartToken = newTagNode(tag2.getRequiredParentTags().iterator().next());
                    if (isAllowedInLastOpenTag(requiredParentStartToken, cleanTimeValues)) {
                        requiredParentStartToken.setAutoGenerated(true);
                        nodeIterator.previous();
                        nodeIterator.add(requiredParentStartToken);
                        nodeIterator.previous();
                        this.properties.fireHtmlError(true, startTagToken, ErrorType.RequiredParentMissing);
                    } else {
                        saveToLastOpenTag(nodeList, token, cleanTimeValues);
                        nodeIterator.set(null);
                    }
                } else if (tag2 != null && lastTagPos != null && tag2.isMustCloseTag(lastTagInfo)) {
                    getChildBreaks(cleanTimeValues).addBreak(lastTagPos, new TagPos(nodeIterator.previousIndex(), tag2.getName()));
                    this.properties.fireHtmlError(!startTagToken.hasAttribute("id"), (TagNode) nodeList.get(lastTagPos.position), ErrorType.UnpermittedChild);
                    List closed2 = closeSnippet(nodeList, lastTagPos, startTagToken, cleanTimeValues);
                    int closedCount = closed2.size();
                    if (tag2.hasCopyTags() && closedCount > 0) {
                        ListIterator closedIt = closed2.listIterator(closedCount);
                        ArrayList arrayList = new ArrayList();
                        while (closedIt.hasPrevious()) {
                            if (!Thread.currentThread().isInterrupted()) {
                                TagNode currStartToken = closedIt.previous();
                                if (!tag2.isCopy(currStartToken.getName())) {
                                    break;
                                }
                                arrayList.add(0, currStartToken);
                            } else {
                                return;
                            }
                        }
                        if (arrayList.size() > 0) {
                            Iterator copyIt = arrayList.iterator();
                            while (copyIt.hasNext()) {
                                if (!Thread.currentThread().isInterrupted()) {
                                    TagNode currStartToken2 = (TagNode) copyIt.next();
                                    if (!isCopiedTokenEqualToNextThreeCopiedTokens(currStartToken2, nodeIterator)) {
                                        nodeIterator.add(currStartToken2.makeCopy());
                                    } else {
                                        copyIt.remove();
                                    }
                                } else {
                                    return;
                                }
                            }
                            for (int i2 = 0; i2 < arrayList.size(); i2++) {
                                nodeIterator.previous();
                            }
                        }
                    }
                    nodeIterator.previous();
                } else if (!isAllowedInLastOpenTag(token, cleanTimeValues)) {
                    saveToLastOpenTag(nodeList, token, cleanTimeValues);
                    nodeIterator.set(null);
                } else if (tag2 == null || tag2.allowsBody()) {
                    getOpenTags(cleanTimeValues).addTag(tagName2, nodeIterator.previousIndex());
                } else {
                    TagNode newTagNode = createTagNode(startTagToken);
                    addPossibleHeadCandidate(tag2, newTagNode, cleanTimeValues);
                    nodeIterator.set(newTagNode);
                }
            } else {
                if (cleanTimeValues._headOpened && !cleanTimeValues._bodyOpened && this.properties.isKeepWhitespaceAndCommentsInHead()) {
                    if (token instanceof CommentNode) {
                        if (getOpenTags(cleanTimeValues).getLastTagPos() == null) {
                            cleanTimeValues._headTags.add(new ProxyTagNode((CommentNode) token, cleanTimeValues.bodyNode));
                        }
                    } else if (token instanceof ContentNode) {
                        ContentNode contentNode = (ContentNode) token;
                        if (contentNode.isBlank() && ((BaseToken) nodeList.get(nodeList.size() - 1)) == token) {
                            cleanTimeValues._headTags.add(new ProxyTagNode(contentNode, cleanTimeValues.bodyNode));
                        }
                    }
                }
                if (!isAllowedInLastOpenTag(token, cleanTimeValues)) {
                    saveToLastOpenTag(nodeList, token, cleanTimeValues);
                    nodeIterator.set(null);
                }
            }
        }
    }

    private static boolean isCopiedTokenEqualToNextThreeCopiedTokens(TagNode copiedStartToken, ListIterator<BaseToken> nodeIterator) {
        int steps = 0;
        int matches = 0;
        while (nodeIterator.hasNext() && steps < 3) {
            BaseToken nextToken = nodeIterator.next();
            steps++;
            if (!(nextToken instanceof TagNode) || !((TagNode) nextToken).isCopy() || !areCopiedTokensEqual((TagNode) nextToken, copiedStartToken)) {
                break;
            }
            matches++;
        }
        for (int i = 0; i < steps; i++) {
            nodeIterator.previous();
        }
        return matches == 3;
    }

    private static boolean areCopiedTokensEqual(TagNode token1, TagNode token2) {
        return token1.name.equals(token2.name) && token1.getAttributes().equals(token2.getAttributes());
    }

    private void reopenBrokenNode(ListIterator<BaseToken> nodeIterator, TagNode toReopen, CleanTimeValues cleanTimeValues) {
        TagNode closedByPresidence = toReopen;
        TagNode copy = closedByPresidence.makeCopy();
        copy.setAutoGenerated(true);
        copy.removeAttribute("id");
        nodeIterator.add(copy);
        getOpenTags(cleanTimeValues).addTag(closedByPresidence.getName(), nodeIterator.previousIndex());
    }

    /* access modifiers changed from: protected */
    public boolean isRemovingNodeReasonablySafe(TagNode startTagToken) {
        return !startTagToken.hasAttribute("id") && !startTagToken.hasAttribute("name") && !startTagToken.hasAttribute("class");
    }

    private void createDocumentNodes(List listNodes, CleanTimeValues cleanTimeValues) {
        for (Object child : listNodes) {
            if (child != null) {
                boolean toAdd = true;
                if (child instanceof TagNode) {
                    TagNode node = (TagNode) child;
                    addPossibleHeadCandidate(getTagInfoProvider().getTagInfo(node.getName()), node, cleanTimeValues);
                } else if (child instanceof ContentNode) {
                    toAdd = !"".equals(child.toString());
                }
                if (toAdd) {
                    cleanTimeValues.bodyNode.addChild(child);
                }
            }
        }
        for (TagNode headCandidateNode : cleanTimeValues._headTags) {
            if (Thread.currentThread().isInterrupted()) {
                handleInterruption();
                return;
            }
            TagNode parent = headCandidateNode.getParent();
            boolean toMove = true;
            while (true) {
                if (parent == null) {
                    break;
                } else if (cleanTimeValues._headTags.contains(parent)) {
                    toMove = false;
                    break;
                } else {
                    parent = parent.getParent();
                }
            }
            if (toMove) {
                headCandidateNode.removeFromTree();
                cleanTimeValues.headNode.addChild(headCandidateNode);
            }
        }
    }

    private List<TagNode> closeSnippet(List nodeList, TagPos tagPos, Object toNode, CleanTimeValues cleanTimeValues) {
        List<TagNode> closed = new ArrayList<>();
        ListIterator it = nodeList.listIterator(tagPos.position);
        TagNode tagNode = null;
        Object item = it.next();
        boolean isListEnd = false;
        while (true) {
            if ((toNode != null || isListEnd) && (toNode == null || item == toNode)) {
                break;
            } else if (Thread.currentThread().isInterrupted()) {
                handleInterruption();
                break;
            } else {
                if (isStartToken(item)) {
                    TagNode startTagToken = (TagNode) item;
                    closed.add(startTagToken);
                    List itemsToMove = startTagToken.getItemsToMove();
                    if (itemsToMove != null) {
                        pushNesting(cleanTimeValues);
                        makeTree(itemsToMove, itemsToMove.listIterator(0), cleanTimeValues);
                        closeAll(itemsToMove, cleanTimeValues);
                        startTagToken.setItemsToMove(null);
                        popNesting(cleanTimeValues);
                    }
                    TagNode newTagNode = createTagNode(startTagToken);
                    addPossibleHeadCandidate(getTagInfo(newTagNode.getName(), cleanTimeValues), newTagNode, cleanTimeValues);
                    if (tagNode != null) {
                        tagNode.addChildren(itemsToMove);
                        tagNode.addChild(newTagNode);
                        it.set(null);
                    } else if (itemsToMove != null) {
                        itemsToMove.add(newTagNode);
                        it.set(itemsToMove);
                    } else {
                        it.set(newTagNode);
                    }
                    getOpenTags(cleanTimeValues).removeTag(newTagNode.getName());
                    tagNode = newTagNode;
                } else if (tagNode != null) {
                    it.set(null);
                    if (item != null) {
                        tagNode.addChild(item);
                    }
                }
                if (it.hasNext()) {
                    item = it.next();
                } else {
                    isListEnd = true;
                }
            }
        }
        return closed;
    }

    private void closeAll(List nodeList, CleanTimeValues cleanTimeValues) {
        TagPos firstTagPos = getOpenTags(cleanTimeValues).findFirstTagPos();
        for (TagPos pos : getOpenTags(cleanTimeValues).list) {
            if (Thread.currentThread().isInterrupted()) {
                handleInterruption();
                return;
            }
            this.properties.fireHtmlError(true, (TagNode) nodeList.get(pos.position), ErrorType.UnclosedTag);
        }
        if (firstTagPos != null) {
            closeSnippet(nodeList, firstTagPos, null, cleanTimeValues);
        }
    }

    private void addPossibleHeadCandidate(TagInfo tagInfo, TagNode tagNode, CleanTimeValues cleanTimeValues) {
        if (tagInfo != null && tagNode != null) {
            if (tagInfo.isHeadTag() || (tagInfo.isHeadAndBodyTag() && cleanTimeValues._headOpened && !cleanTimeValues._bodyOpened)) {
                cleanTimeValues._headTags.add(tagNode);
            }
        }
    }

    public CleanerProperties getProperties() {
        return this.properties;
    }

    /* access modifiers changed from: protected */
    public Set<ITagNodeCondition> getPruneTagSet(CleanTimeValues cleanTimeValues) {
        return cleanTimeValues.pruneTagSet;
    }

    /* access modifiers changed from: protected */
    public Set<ITagNodeCondition> getAllowTagSet(CleanTimeValues cleanTimeValues) {
        return cleanTimeValues.allowTagSet;
    }

    /* access modifiers changed from: protected */
    public void addPruneNode(TagNode node, CleanTimeValues cleanTimeValues) {
        node.setPruned(true);
        cleanTimeValues.pruneNodeSet.add(node);
    }

    private TagInfo getTagInfo(String tagName, CleanTimeValues cleanTimeValues) {
        if (!isAllowedAsForeignMarkup(tagName, cleanTimeValues)) {
            return getTagInfoProvider().getTagInfo(tagName);
        }
        return null;
    }

    private boolean addIfNeededToPruneSet(TagNode tagNode, CleanTimeValues cleanTimeValues) {
        if (cleanTimeValues.pruneTagSet != null) {
            for (ITagNodeCondition condition : cleanTimeValues.pruneTagSet) {
                if (condition.satisfy(tagNode)) {
                    addPruneNode(tagNode, cleanTimeValues);
                    this.properties.fireConditionModification(condition, tagNode);
                    return true;
                }
            }
        }
        if (cleanTimeValues.allowTagSet == null || cleanTimeValues.allowTagSet.isEmpty()) {
            return false;
        }
        for (ITagNodeCondition condition2 : cleanTimeValues.allowTagSet) {
            if (condition2.satisfy(tagNode)) {
                return false;
            }
        }
        if (!tagNode.isAutoGenerated()) {
            this.properties.fireUserDefinedModification(true, tagNode, ErrorType.NotAllowedTag);
        }
        addPruneNode(tagNode, cleanTimeValues);
        return true;
    }

    /* access modifiers changed from: protected */
    public Set<String> getAllTags(CleanTimeValues cleanTimeValues) {
        return cleanTimeValues.allTags;
    }

    public ITagInfoProvider getTagInfoProvider() {
        return this.properties.getTagInfoProvider();
    }

    public CleanerTransformations getTransformations() {
        return this.transformations;
    }

    public String getInnerHtml(TagNode node) {
        if (node != null) {
            String content = new SimpleXmlSerializer(this.properties).getAsString(node);
            int index1 = content.indexOf(62, content.indexOf("<" + node.getName()) + 1);
            int index2 = content.lastIndexOf(60);
            if (index1 < 0 || index1 > index2) {
                return null;
            }
            return content.substring(index1 + 1, index2);
        }
        throw new HtmlCleanerException("Cannot return inner html of the null node!");
    }

    public void setInnerHtml(TagNode node, String content) {
        if (node != null) {
            String nodeName = node.getName();
            StringBuilder html = new StringBuilder();
            html.append("<").append(nodeName).append(" htmlcleaner_marker=''>").append(content).append("</").append(nodeName).append(Account.DEFAULT_QUOTE_PREFIX);
            for (TagNode parent = node.getParent(); parent != null; parent = parent.getParent()) {
                String parentName = parent.getName();
                html.insert(0, "<" + parentName + Account.DEFAULT_QUOTE_PREFIX);
                html.append("</").append(parentName).append(Account.DEFAULT_QUOTE_PREFIX);
            }
            TagNode cleanedNode = clean(html.toString()).findElementHavingAttribute(MARKER_ATTRIBUTE, true);
            if (cleanedNode != null) {
                node.setChildren(cleanedNode.getAllChildren());
            }
        }
    }

    public void initCleanerTransformations(Map transInfos) {
        this.transformations = new CleanerTransformations(transInfos);
    }

    private OpenTags getOpenTags(CleanTimeValues cleanTimeValues) {
        return cleanTimeValues.nestingStates.peek().getOpenTags();
    }

    private ChildBreaks getChildBreaks(CleanTimeValues cleanTimeValues) {
        return cleanTimeValues.nestingStates.peek().getChildBreaks();
    }

    private NestingState pushNesting(CleanTimeValues cleanTimeValues) {
        return cleanTimeValues.nestingStates.push(new NestingState());
    }

    private NestingState popNesting(CleanTimeValues cleanTimeValues) {
        return cleanTimeValues.nestingStates.pop();
    }

    /* access modifiers changed from: private */
    public void handleInterruption() {
    }
}
