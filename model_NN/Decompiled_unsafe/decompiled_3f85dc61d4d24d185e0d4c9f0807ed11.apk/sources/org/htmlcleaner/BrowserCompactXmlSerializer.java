package org.htmlcleaner;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.StringTokenizer;

public class BrowserCompactXmlSerializer extends XmlSerializer {
    private static final String BR_TAG = "<br />";
    private static final String LINE_BREAK = "\n";
    private static final String PRE_TAG = "pre";

    public BrowserCompactXmlSerializer(CleanerProperties props) {
        super(props);
    }

    /* access modifiers changed from: protected */
    public void serialize(TagNode tagNode, Writer writer) throws IOException {
        serializeOpenTag(tagNode, writer, false);
        TagInfo tagInfo = this.props.getTagInfoProvider().getTagInfo(tagNode.getName());
        String tagName = tagInfo != null ? tagInfo.getName() : null;
        List<? extends BaseToken> tagChildren = new ArrayList<>(tagNode.getAllChildren());
        if (!isMinimizedTagSyntax(tagNode)) {
            ListIterator<? extends BaseToken> childrenIt = tagChildren.listIterator();
            while (childrenIt.hasNext()) {
                Object item = childrenIt.next();
                if (item != null) {
                    if ((item instanceof ContentNode) && !PRE_TAG.equals(tagName)) {
                        String content = ((ContentNode) item).getContent();
                        String content2 = (dontEscape(tagNode) ? content.replaceAll(CData.END_CDATA, "]]&gt;") : escapeXml(content)).replaceAll("^ +", " ").replaceAll(" +$", " ");
                        boolean whitespaceAllowed = tagInfo != null && tagInfo.getDisplay().isLeadingAndEndWhitespacesAllowed();
                        boolean writeLeadingSpace = content2.length() > 0 && Character.isWhitespace(content2.charAt(0));
                        boolean writeEndingSpace = content2.length() > 1 && Character.isWhitespace(content2.charAt(content2.length() + -1));
                        String content3 = content2.trim();
                        if (content3.length() != 0) {
                            boolean hasPrevContent = false;
                            int order = tagChildren.indexOf(item);
                            if (order >= 2) {
                                hasPrevContent = isContentOrInline(tagChildren.get(order - 1));
                            }
                            if (writeLeadingSpace && (whitespaceAllowed || hasPrevContent)) {
                                writer.write(32);
                            }
                            StringTokenizer stringTokenizer = new StringTokenizer(content3, "\n", true);
                            String prevToken = "";
                            while (stringTokenizer.hasMoreTokens()) {
                                String token = stringTokenizer.nextToken();
                                if (prevToken.equals(token) && prevToken.equals("\n")) {
                                    writer.write(BR_TAG);
                                } else if ("\n".equals(token)) {
                                    writer.write(32);
                                } else {
                                    writer.write(token.trim());
                                }
                                prevToken = token;
                            }
                            boolean hasFollowingContent = false;
                            if (childrenIt.hasNext()) {
                                hasFollowingContent = isContentOrInline(childrenIt.next());
                                childrenIt.previous();
                            }
                            if (writeEndingSpace && (whitespaceAllowed || hasFollowingContent)) {
                                writer.write(32);
                            }
                        } else {
                            childrenIt.remove();
                        }
                    } else if (item instanceof ContentNode) {
                        writer.write(((ContentNode) item).getContent());
                    } else if (item instanceof CommentNode) {
                        writer.write(((CommentNode) item).getCommentedContent().trim());
                    } else {
                        ((BaseToken) item).serialize(this, writer);
                    }
                }
            }
            serializeEndTag(tagNode, writer, tagInfo != null && tagInfo.getDisplay().isAfterTagLineBreakNeeded());
        }
    }

    private boolean isContentOrInline(Object node) {
        TagInfo nextInfo;
        if (node instanceof ContentNode) {
            return true;
        }
        return (node instanceof TagNode) && (nextInfo = this.props.getTagInfoProvider().getTagInfo(((TagNode) node).getName())) != null && nextInfo.getDisplay() == Display.inline;
    }
}
