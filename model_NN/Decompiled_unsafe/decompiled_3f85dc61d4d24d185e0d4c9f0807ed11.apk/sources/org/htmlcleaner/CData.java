package org.htmlcleaner;

public class CData extends ContentNode implements HtmlNode {
    public static final String BEGIN_CDATA = "<![CDATA[";
    public static final String END_CDATA = "]]>";
    public static final String SAFE_BEGIN_CDATA = "/*<![CDATA[*/";
    public static final String SAFE_BEGIN_CDATA_ALT = "//<![CDATA[";
    public static final String SAFE_END_CDATA = "/*]]>*/";
    public static final String SAFE_END_CDATA_ALT = "//]]>";

    public CData(String content) {
        super(content);
    }

    public String getContentWithoutStartAndEndTokens() {
        return this.content;
    }

    public String getContent() {
        return getContentWithoutStartAndEndTokens();
    }

    public String toString() {
        return getContentWithStartAndEndTokens();
    }

    public String getContentWithStartAndEndTokens() {
        return SAFE_BEGIN_CDATA + this.content + SAFE_END_CDATA;
    }
}
