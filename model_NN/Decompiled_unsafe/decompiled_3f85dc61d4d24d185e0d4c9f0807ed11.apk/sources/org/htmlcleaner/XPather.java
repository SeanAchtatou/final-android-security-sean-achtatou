package org.htmlcleaner;

import com.fsck.k9.Account;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.StringTokenizer;
import me.leolin.shortcutbadger.impl.NewHtcHomeBadger;

public class XPather {
    private static final int C0 = 48;
    private static final int C9 = 57;
    private static final int CD = 46;
    private static final int CM = 45;
    private static final int CP = 43;
    private static final int CS = 32;
    private String[] tokenArray;

    public XPather(String expression) {
        StringTokenizer tokenizer = new StringTokenizer(expression, "/()[]\"'=<>", true);
        this.tokenArray = new String[tokenizer.countTokens()];
        int index = 0;
        while (tokenizer.hasMoreTokens()) {
            this.tokenArray[index] = tokenizer.nextToken();
            index++;
        }
    }

    public Object[] evaluateAgainstNode(TagNode node) throws XPatherException {
        if (node == null) {
            throw new XPatherException("Cannot evaluate XPath expression against null value!");
        }
        Collection<Object> collectionResult = evaluateAgainst(singleton(node), 0, this.tokenArray.length - 1, false, 1, 0, false, null);
        Object[] array = new Object[collectionResult.size()];
        int index = 0;
        for (Object obj : collectionResult) {
            array[index] = obj;
            index++;
        }
        return array;
    }

    private void throwStandardException() throws XPatherException {
        throw new XPatherException();
    }

    private Collection evaluateAgainst(Collection object, int from, int to, boolean isRecursive, int position, int last, boolean isFilterContext, Collection filterSource) throws XPatherException {
        boolean logicValue;
        if (from < 0 || to >= this.tokenArray.length || from > to) {
            return object;
        }
        if ("".equals(this.tokenArray[from].trim())) {
            return evaluateAgainst(object, from + 1, to, isRecursive, position, last, isFilterContext, filterSource);
        }
        if (isToken("(", from)) {
            int closingBracket = findClosingIndex(from, to);
            if (closingBracket > 0) {
                return evaluateAgainst(evaluateAgainst(object, from + 1, closingBracket - 1, false, position, last, isFilterContext, filterSource), closingBracket + 1, to, false, position, last, isFilterContext, filterSource);
            }
            throwStandardException();
        } else if (isToken("[", from)) {
            int closingBracket2 = findClosingIndex(from, to);
            if (closingBracket2 <= 0 || object == null) {
                throwStandardException();
            } else {
                return evaluateAgainst(filterByCondition(object, from + 1, closingBracket2 - 1), closingBracket2 + 1, to, false, position, last, isFilterContext, filterSource);
            }
        } else if (isToken("\"", from) || isToken("'", from)) {
            int closingQuote = findClosingIndex(from, to);
            if (closingQuote > from) {
                return evaluateAgainst(singleton(flatten(from + 1, closingQuote - 1)), closingQuote + 1, to, false, position, last, isFilterContext, filterSource);
            }
            throwStandardException();
        } else if ((isToken("=", from) || isToken("<", from) || isToken(Account.DEFAULT_QUOTE_PREFIX, from)) && isFilterContext) {
            if (!isToken("=", from + 1) || (!isToken("<", from) && !isToken(Account.DEFAULT_QUOTE_PREFIX, from))) {
                logicValue = evaluateLogic(object, evaluateAgainst(filterSource, from + 1, to, false, position, last, isFilterContext, filterSource), this.tokenArray[from]);
            } else {
                logicValue = evaluateLogic(object, evaluateAgainst(filterSource, from + 2, to, false, position, last, isFilterContext, filterSource), this.tokenArray[from] + this.tokenArray[from + 1]);
            }
            return singleton(new Boolean(logicValue));
        } else if (isToken("/", from)) {
            boolean goRecursive = isToken("/", from + 1);
            if (goRecursive) {
                from++;
            }
            if (from < to) {
                int toIndex = findClosingIndex(from, to) - 1;
                if (toIndex <= from) {
                    toIndex = to;
                }
                return evaluateAgainst(evaluateAgainst(object, from + 1, toIndex, goRecursive, 1, last, isFilterContext, filterSource), toIndex + 1, to, false, 1, last, isFilterContext, filterSource);
            }
            throwStandardException();
        } else if (isFunctionCall(from, to)) {
            return evaluateAgainst(evaluateFunction(object, from, to, position, last, isFilterContext), findClosingIndex(from + 1, to) + 1, to, false, 1, last, isFilterContext, filterSource);
        } else if (isValidInteger(this.tokenArray[from])) {
            return evaluateAgainst(singleton(Integer.valueOf(this.tokenArray[from])), from + 1, to, false, position, last, isFilterContext, filterSource);
        } else if (!isValidDouble(this.tokenArray[from])) {
            return getElementsByName(object, from, to, isRecursive, isFilterContext);
        } else {
            return evaluateAgainst(singleton(Double.valueOf(this.tokenArray[from])), from + 1, to, false, position, last, isFilterContext, filterSource);
        }
        throw new XPatherException();
    }

    private String flatten(int from, int to) {
        if (from > to) {
            return "";
        }
        StringBuffer result = new StringBuffer();
        for (int i = from; i <= to; i++) {
            result.append(this.tokenArray[i]);
        }
        return result.toString();
    }

    private static boolean isValidInteger(String value) {
        int l = value.length();
        if (l <= 0) {
            return false;
        }
        int c = value.charAt(0);
        if (c != 43 && c != 45 && (c < 48 || c > 57)) {
            return false;
        }
        for (int i = 1; i < l; i++) {
            int c2 = value.charAt(i);
            if (c2 < 48 || c2 > 57) {
                return false;
            }
        }
        return true;
    }

    private boolean isValidDouble(String value) {
        int l = value.length();
        if (l <= 0) {
            return false;
        }
        int c = value.charAt(0);
        if (c != 43 && c != 45 && c != 32 && (c < 48 || c > 57)) {
            return false;
        }
        for (int i = 1; i < l; i++) {
            int c2 = value.charAt(i);
            if (c2 != 46 && (c2 < 48 || c2 > 57)) {
                return false;
            }
        }
        return true;
    }

    private boolean isIdentifier(String s) {
        if (s != null) {
            String s2 = s.trim();
            if (s2.length() > 0 && Character.isLetter(s2.charAt(0))) {
                int i = 1;
                while (i < s2.length() && ((ch = s2.charAt(i)) == '_' || ch == '-' || Character.isLetterOrDigit(ch))) {
                    i++;
                }
            }
        }
        return false;
    }

    private boolean isFunctionCall(int from, int to) {
        if ((isIdentifier(this.tokenArray[from]) || isToken("(", from + 1)) && findClosingIndex(from + 1, to) > from + 1) {
            return true;
        }
        return false;
    }

    private Collection evaluateFunction(Collection source, int from, int to, int position, int last, boolean isFilterContext) throws XPatherException {
        String name = this.tokenArray[from].trim();
        ArrayList result = new ArrayList();
        int size = source.size();
        int index = 0;
        for (Object curr : source) {
            index++;
            if ("last".equals(name)) {
                result.add(Integer.valueOf(isFilterContext ? last : size));
            } else if ("position".equals(name)) {
                result.add(Integer.valueOf(isFilterContext ? position : index));
            } else if ("text".equals(name)) {
                if (curr instanceof TagNode) {
                    result.add(((TagNode) curr).getText());
                } else if (curr instanceof String) {
                    result.add(curr.toString());
                }
            } else if (NewHtcHomeBadger.COUNT.equals(name)) {
                result.add(Integer.valueOf(evaluateAgainst(source, from + 2, to - 1, false, position, 0, isFilterContext, null).size()));
            } else if ("data".equals(name)) {
                for (Object elem : evaluateAgainst(source, from + 2, to - 1, false, position, 0, isFilterContext, null)) {
                    if (elem instanceof TagNode) {
                        result.add(((TagNode) elem).getText());
                    } else if (elem instanceof String) {
                        result.add(elem.toString());
                    }
                }
            } else {
                throw new XPatherException("Unknown function " + name + "!");
            }
        }
        return result;
    }

    private Collection filterByCondition(Collection source, int from, int to) throws XPatherException {
        ArrayList result = new ArrayList();
        int index = 0;
        int size = source.size();
        for (Object curr : source) {
            index++;
            ArrayList logicValueList = new ArrayList(evaluateAgainst(singleton(curr), from, to, false, index, size, true, singleton(curr)));
            if (logicValueList.size() >= 1) {
                Object first = logicValueList.get(0);
                if (first instanceof Boolean) {
                    if (((Boolean) first).booleanValue()) {
                        result.add(curr);
                    }
                } else if (!(first instanceof Integer)) {
                    result.add(curr);
                } else if (((Integer) first).intValue() == index) {
                    result.add(curr);
                }
            }
        }
        return result;
    }

    private boolean isToken(String token, int index) {
        return index >= 0 && index < this.tokenArray.length && this.tokenArray[index].trim().equals(token.trim());
    }

    private int findClosingIndex(int from, int to) {
        int brackets;
        int angleBrackets;
        int slashes;
        if (from < to) {
            String currToken = this.tokenArray[from];
            if ("\"".equals(currToken)) {
                for (int i = from + 1; i <= to; i++) {
                    if ("\"".equals(this.tokenArray[i])) {
                        return i;
                    }
                }
            } else if ("'".equals(currToken)) {
                for (int i2 = from + 1; i2 <= to; i2++) {
                    if ("'".equals(this.tokenArray[i2])) {
                        return i2;
                    }
                }
            } else if ("(".equals(currToken) || "[".equals(currToken) || "/".equals(currToken)) {
                boolean isQuoteClosed = true;
                boolean isAposClosed = true;
                if ("(".equals(currToken)) {
                    brackets = 1;
                } else {
                    brackets = 0;
                }
                if ("[".equals(currToken)) {
                    angleBrackets = 1;
                } else {
                    angleBrackets = 0;
                }
                if ("/".equals(currToken)) {
                    slashes = 1;
                } else {
                    slashes = 0;
                }
                for (int i3 = from + 1; i3 <= to; i3++) {
                    if ("\"".equals(this.tokenArray[i3])) {
                        if (!isQuoteClosed) {
                            isQuoteClosed = true;
                        } else {
                            isQuoteClosed = false;
                        }
                    } else if ("'".equals(this.tokenArray[i3])) {
                        isAposClosed = !isAposClosed;
                    } else if ("(".equals(this.tokenArray[i3]) && isQuoteClosed && isAposClosed) {
                        brackets++;
                    } else if (")".equals(this.tokenArray[i3]) && isQuoteClosed && isAposClosed) {
                        brackets--;
                    } else if ("[".equals(this.tokenArray[i3]) && isQuoteClosed && isAposClosed) {
                        angleBrackets++;
                    } else if ("]".equals(this.tokenArray[i3]) && isQuoteClosed && isAposClosed) {
                        angleBrackets--;
                    } else if ("/".equals(this.tokenArray[i3]) && isQuoteClosed && isAposClosed && brackets == 0 && angleBrackets == 0) {
                        slashes--;
                    }
                    if (isQuoteClosed && isAposClosed && brackets == 0 && angleBrackets == 0 && slashes == 0) {
                        return i3;
                    }
                }
            }
        }
        return -1;
    }

    private boolean isAtt(String token) {
        return token != null && token.length() > 1 && token.startsWith("@");
    }

    private Collection singleton(Object element) {
        ArrayList result = new ArrayList();
        result.add(element);
        return result;
    }

    private Collection getElementsByName(Collection source, int from, int to, boolean isRecursive, boolean isFilterContext) throws XPatherException {
        Collection subnodes;
        Collection nodes;
        String name = this.tokenArray[from].trim();
        if (isAtt(name)) {
            String name2 = name.substring(1);
            Collection result = new ArrayList();
            if (isRecursive) {
                nodes = new LinkedHashSet();
                for (Object next : source) {
                    if (next instanceof TagNode) {
                        nodes.addAll(((TagNode) next).getAllElementsList(true));
                    }
                }
            } else {
                nodes = source;
            }
            for (Object next2 : nodes) {
                if (next2 instanceof TagNode) {
                    TagNode node = (TagNode) next2;
                    if ("*".equals(name2)) {
                        result.addAll(evaluateAgainst(node.getAttributes().values(), from + 1, to, false, 1, 1, isFilterContext, null));
                    } else {
                        String attValue = node.getAttributeByName(name2);
                        if (attValue != null) {
                            result.addAll(evaluateAgainst(singleton(attValue), from + 1, to, false, 1, 1, isFilterContext, null));
                        }
                    }
                } else {
                    throwStandardException();
                }
            }
            return result;
        }
        Collection result2 = new LinkedHashSet();
        int index = 0;
        for (Object next3 : source) {
            if (next3 instanceof TagNode) {
                TagNode node2 = (TagNode) next3;
                index++;
                boolean isSelf = ".".equals(name);
                boolean isParent = "..".equals(name);
                boolean isAll = "*".equals(name);
                if (isSelf) {
                    subnodes = singleton(node2);
                } else if (isParent) {
                    TagNode parent = node2.getParent();
                    subnodes = parent != null ? singleton(parent) : new ArrayList();
                } else if (isAll) {
                    subnodes = node2.getChildTagList();
                } else {
                    subnodes = node2.getElementListByName(name, false);
                }
                LinkedHashSet nodeSet = new LinkedHashSet(subnodes);
                Collection refinedSubnodes = evaluateAgainst(nodeSet, from + 1, to, false, index, nodeSet.size(), isFilterContext, null);
                if (isRecursive) {
                    List childTags = node2.getChildTagList();
                    if (isSelf || isParent || isAll) {
                        result2.addAll(refinedSubnodes);
                    }
                    for (TagNode childTag : childTags) {
                        Collection childrenByName = getElementsByName(singleton(childTag), from, to, isRecursive, isFilterContext);
                        if (!isSelf && !isParent && !isAll && refinedSubnodes.contains(childTag)) {
                            result2.add(childTag);
                        }
                        result2.addAll(childrenByName);
                    }
                } else {
                    result2.addAll(refinedSubnodes);
                }
            } else {
                throwStandardException();
            }
        }
        return result2;
    }

    private boolean evaluateLogic(Collection first, Collection second, String logicOperator) {
        if (first == null || first.size() == 0 || second == null || second.size() == 0) {
            return false;
        }
        Object elem1 = first.iterator().next();
        Object elem2 = second.iterator().next();
        if (!(elem1 instanceof Number) || !(elem2 instanceof Number)) {
            int result = toText(elem1).compareTo(toText(elem2));
            if ("=".equals(logicOperator)) {
                if (result != 0) {
                    return false;
                }
                return true;
            } else if ("<".equals(logicOperator)) {
                if (result >= 0) {
                    return false;
                }
                return true;
            } else if (Account.DEFAULT_QUOTE_PREFIX.equals(logicOperator)) {
                if (result <= 0) {
                    return false;
                }
                return true;
            } else if ("<=".equals(logicOperator)) {
                if (result > 0) {
                    return false;
                }
                return true;
            } else if (">=".equals(logicOperator)) {
                if (result < 0) {
                    return false;
                }
                return true;
            }
        } else {
            double d1 = ((Number) elem1).doubleValue();
            double d2 = ((Number) elem2).doubleValue();
            if ("=".equals(logicOperator)) {
                if (d1 != d2) {
                    return false;
                }
                return true;
            } else if ("<".equals(logicOperator)) {
                if (d1 >= d2) {
                    return false;
                }
                return true;
            } else if (Account.DEFAULT_QUOTE_PREFIX.equals(logicOperator)) {
                if (d1 <= d2) {
                    return false;
                }
                return true;
            } else if ("<=".equals(logicOperator)) {
                if (d1 > d2) {
                    return false;
                }
                return true;
            } else if (">=".equals(logicOperator)) {
                if (d1 < d2) {
                    return false;
                }
                return true;
            }
        }
        return false;
    }

    private String toText(Object o) {
        if (o == null) {
            return "";
        }
        if (o instanceof TagNode) {
            return ((TagNode) o).getText().toString();
        }
        return o.toString();
    }
}
