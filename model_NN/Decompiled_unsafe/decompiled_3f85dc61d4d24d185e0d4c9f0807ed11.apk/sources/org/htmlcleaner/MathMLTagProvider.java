package org.htmlcleaner;

import java.util.concurrent.ConcurrentMap;

public class MathMLTagProvider {
    private static final String CLOSE_BEFORE_TAGS = "menclose,mpadded,mphantom,mfenced,mstyle,merror,msqrt,mroot,mtd,mtr,maligngroup,malignmark,mlabeledtr,ms,mi,mo,mn,mfrac,mrow,mtext,mspace,mglyph,p,details,summary,menuitem,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml";

    public MathMLTagProvider(TagInfo tagInfo, ConcurrentMap<String, TagInfo> tagInfoMap) {
        presentationMarkup(tagInfo, tagInfoMap);
    }

    public void presentationMarkup(TagInfo tagInfo, ConcurrentMap<String, TagInfo> tagInfoMap) {
        tokenElements(tagInfo, tagInfoMap);
        layoutElements(tagInfo, tagInfoMap);
        scriptElements(tagInfo, tagInfoMap);
        tableElements(tagInfo, tagInfoMap);
        TagInfo tagInfo2 = new TagInfo("maction", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo2.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("maction", tagInfo2, tagInfoMap);
    }

    public void tokenElements(TagInfo tagInfo, ConcurrentMap<String, TagInfo> tagInfoMap) {
        TagInfo tagInfo2 = new TagInfo("mi", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo2.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("mi", tagInfo2, tagInfoMap);
        TagInfo tagInfo3 = new TagInfo("mn", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo3.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("mn", tagInfo3, tagInfoMap);
        TagInfo tagInfo4 = new TagInfo("mo", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo4.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("mo", tagInfo4, tagInfoMap);
        TagInfo tagInfo5 = new TagInfo("mtext", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo5.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("mtext", tagInfo5, tagInfoMap);
        TagInfo tagInfo6 = new TagInfo("mspace", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo6.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("mspace", tagInfo6, tagInfoMap);
        TagInfo tagInfo7 = new TagInfo("ms", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo7.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("ms", tagInfo7, tagInfoMap);
        TagInfo tagInfo8 = new TagInfo("mglyph", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo8.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("mglyph", tagInfo8, tagInfoMap);
    }

    public void layoutElements(TagInfo tagInfo, ConcurrentMap<String, TagInfo> tagInfoMap) {
        TagInfo tagInfo2 = new TagInfo("mrow", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo2.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("mrow", tagInfo2, tagInfoMap);
        TagInfo tagInfo3 = new TagInfo("mfrac", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo3.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("mfrac", tagInfo3, tagInfoMap);
        TagInfo tagInfo4 = new TagInfo("msqrt", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo4.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("msqrt", tagInfo4, tagInfoMap);
        TagInfo tagInfo5 = new TagInfo("mroot", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo5.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("mroot", tagInfo5, tagInfoMap);
        TagInfo tagInfo6 = new TagInfo("mstyle", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo6.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("mstyle", tagInfo6, tagInfoMap);
        TagInfo tagInfo7 = new TagInfo("merror", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo7.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("merror", tagInfo7, tagInfoMap);
        TagInfo tagInfo8 = new TagInfo("mpadded", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo8.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("mpadded", tagInfo8, tagInfoMap);
        TagInfo tagInfo9 = new TagInfo("mphantom", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo9.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("mphantom", tagInfo9, tagInfoMap);
        TagInfo tagInfo10 = new TagInfo("mfenced", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo10.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("mfenced", tagInfo10, tagInfoMap);
        TagInfo tagInfo11 = new TagInfo("menclose", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo11.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("menclose", tagInfo11, tagInfoMap);
    }

    public void scriptElements(TagInfo tagInfo, ConcurrentMap<String, TagInfo> tagInfoMap) {
        TagInfo tagInfo2 = new TagInfo("msub", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo2.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("msub", tagInfo2, tagInfoMap);
        TagInfo tagInfo3 = new TagInfo("msup", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo3.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("msup", tagInfo3, tagInfoMap);
        TagInfo tagInfo4 = new TagInfo("msubsup", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo4.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("msubsup", tagInfo4, tagInfoMap);
        TagInfo tagInfo5 = new TagInfo("munder", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo5.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("munder", tagInfo5, tagInfoMap);
        TagInfo tagInfo6 = new TagInfo("mover", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo6.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("mover", tagInfo6, tagInfoMap);
        TagInfo tagInfo7 = new TagInfo("munderover", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo7.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("munderover", tagInfo7, tagInfoMap);
        TagInfo tagInfo8 = new TagInfo("mmultiscripts", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo8.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("mmultiscripts", tagInfo8, tagInfoMap);
    }

    public void tableElements(TagInfo tagInfo, ConcurrentMap<String, TagInfo> tagInfoMap) {
        TagInfo tagInfo2 = new TagInfo("mtable", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo2.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        tagInfo2.defineAllowedChildrenTags("mtr,mtd,mo,mn,mlabeledtr");
        put("mtable", tagInfo2, tagInfoMap);
        TagInfo tagInfo3 = new TagInfo("mlabeledtr", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo3.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        tagInfo3.defineRequiredEnclosingTags("mtable");
        tagInfo3.defineFatalTags("mtable");
        put("mlabeledtr", tagInfo3, tagInfoMap);
        TagInfo tagInfo4 = new TagInfo("mtr", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo4.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        tagInfo4.defineAllowedChildrenTags("mtd,mlabeledtr");
        tagInfo4.defineRequiredEnclosingTags("mtable");
        put("mtr", tagInfo4, tagInfoMap);
        TagInfo tagInfo5 = new TagInfo("mtd", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo5.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        tagInfo5.defineRequiredEnclosingTags("mtr");
        tagInfo5.defineFatalTags("mtable");
        put("mtd", tagInfo5, tagInfoMap);
        TagInfo tagInfo6 = new TagInfo("maligngroup", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo6.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("maligngroup", tagInfo6, tagInfoMap);
        TagInfo tagInfo7 = new TagInfo("malignmark", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo7.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("malignmark", tagInfo7, tagInfoMap);
    }

    /* access modifiers changed from: protected */
    public void put(String tagName, TagInfo tagInfo, ConcurrentMap<String, TagInfo> tagInfoMap) {
        tagInfoMap.put(tagName, tagInfo);
    }

    public TagInfo getTagInfo(String tagName, ConcurrentMap<String, TagInfo> tagInfoMap) {
        if (tagName == null) {
            return null;
        }
        return tagInfoMap.get(tagName);
    }
}
