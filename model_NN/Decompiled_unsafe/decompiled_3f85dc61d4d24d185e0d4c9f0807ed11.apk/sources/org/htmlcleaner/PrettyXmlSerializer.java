package org.htmlcleaner;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import org.apache.commons.io.IOUtils;

public class PrettyXmlSerializer extends XmlSerializer {
    private static final String DEFAULT_INDENTATION_STRING = "\t";
    private String indentString;
    private List<String> indents;

    public PrettyXmlSerializer(CleanerProperties props) {
        this(props, DEFAULT_INDENTATION_STRING);
    }

    public PrettyXmlSerializer(CleanerProperties props, String indentString2) {
        super(props);
        this.indentString = DEFAULT_INDENTATION_STRING;
        this.indents = new ArrayList();
        this.indentString = indentString2;
    }

    /* access modifiers changed from: protected */
    public void serialize(TagNode tagNode, Writer writer) throws IOException {
        serializePrettyXml(tagNode, writer, 0);
    }

    private synchronized String getIndent(int level) {
        int size = this.indents.size();
        if (size <= level) {
            String prevIndent = size == 0 ? null : this.indents.get(size - 1);
            for (int i = size; i <= level; i++) {
                String currIndent = prevIndent == null ? "" : prevIndent + this.indentString;
                this.indents.add(currIndent);
                prevIndent = currIndent;
            }
        }
        return this.indents.get(level);
    }

    private String getIndentedText(String content, int level) {
        String indent = getIndent(level);
        StringBuilder result = new StringBuilder(content.length());
        StringTokenizer tokenizer = new StringTokenizer(content, "\n\r");
        while (tokenizer.hasMoreTokens()) {
            String line = tokenizer.nextToken().trim();
            if (!"".equals(line)) {
                result.append(indent).append(line).append(IOUtils.LINE_SEPARATOR_UNIX);
            }
        }
        return result.toString();
    }

    private String getSingleLineOfChildren(List<? extends BaseToken> children) {
        StringBuilder result = new StringBuilder();
        Iterator<? extends BaseToken> childrenIt = children.iterator();
        boolean isFirst = true;
        while (childrenIt.hasNext()) {
            Object child = childrenIt.next();
            if (!(child instanceof ContentNode)) {
                return null;
            }
            String content = child.toString();
            if (isFirst) {
                content = ltrim(content);
            }
            if (!childrenIt.hasNext()) {
                content = rtrim(content);
            }
            if (content.indexOf(IOUtils.LINE_SEPARATOR_UNIX) >= 0 || content.indexOf("\r") >= 0) {
                return null;
            }
            result.append(content);
            isFirst = false;
        }
        return result.toString();
    }

    /* access modifiers changed from: protected */
    public void serializePrettyXml(TagNode tagNode, Writer writer, int level) throws IOException {
        List<? extends BaseToken> tagChildren = tagNode.getAllChildren();
        boolean isHeadlessNode = Utils.isEmptyString(tagNode.getName());
        String indent = isHeadlessNode ? "" : getIndent(level);
        writer.write(indent);
        serializeOpenTag(tagNode, writer, true);
        if (!isMinimizedTagSyntax(tagNode)) {
            String singleLine = getSingleLineOfChildren(tagChildren);
            boolean dontEscape = dontEscape(tagNode);
            if (singleLine == null) {
                if (!isHeadlessNode) {
                    writer.write(IOUtils.LINE_SEPARATOR_UNIX);
                }
                for (Object next : tagChildren) {
                    if (next instanceof TagNode) {
                        serializePrettyXml((TagNode) next, writer, isHeadlessNode ? level : level + 1);
                    } else if (next instanceof CData) {
                        serializeCData((CData) next, tagNode, writer);
                    } else if (next instanceof ContentNode) {
                        writer.write(getIndentedText(dontEscape ? next.toString().replaceAll(CData.END_CDATA, "]]&gt;") : escapeXml(next.toString()), isHeadlessNode ? level : level + 1));
                    } else if (next instanceof CommentNode) {
                        writer.write(getIndentedText(((CommentNode) next).getCommentedContent(), isHeadlessNode ? level : level + 1));
                    }
                }
            } else if (!dontEscape(tagNode)) {
                writer.write(escapeXml(singleLine));
            } else {
                writer.write(singleLine.replaceAll(CData.END_CDATA, "]]&gt;"));
            }
            if (singleLine == null) {
                writer.write(indent);
            }
            serializeEndTag(tagNode, writer, true);
        }
    }

    private String ltrim(String s) {
        if (s == null) {
            return null;
        }
        int index = 0;
        int len = s.length();
        while (index < len && Character.isWhitespace(s.charAt(index))) {
            index++;
        }
        return index >= len ? "" : s.substring(index);
    }

    private String rtrim(String s) {
        if (s == null) {
            return null;
        }
        int index = s.length();
        while (index > 0 && Character.isWhitespace(s.charAt(index - 1))) {
            index--;
        }
        return index <= 0 ? "" : s.substring(0, index);
    }
}
