package org.htmlcleaner;

class ProxyTagNode extends TagNode {
    private TagNode bodyNode;
    private CommentNode comment;
    private ContentNode token;

    public ProxyTagNode(ContentNode token2, TagNode bodyNode2) {
        super("");
        this.token = token2;
        this.bodyNode = bodyNode2;
    }

    public ProxyTagNode(CommentNode comment2, TagNode bodyNode2) {
        super("");
        this.comment = comment2;
        this.bodyNode = bodyNode2;
    }

    public TagNode getParent() {
        return null;
    }

    public boolean removeFromTree() {
        this.bodyNode.removeChild(getToken());
        return true;
    }

    public BaseToken getToken() {
        return this.token != null ? this.token : this.comment;
    }

    public String getContent() {
        return this.token != null ? this.token.getContent() : this.comment.getContent();
    }
}
