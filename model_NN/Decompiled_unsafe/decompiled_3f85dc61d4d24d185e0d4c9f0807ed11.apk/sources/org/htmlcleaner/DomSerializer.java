package org.htmlcleaner;

import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;

public class DomSerializer {
    protected boolean escapeXml;
    protected CleanerProperties props;

    public DomSerializer(CleanerProperties props2, boolean escapeXml2) {
        this.escapeXml = true;
        this.props = props2;
        this.escapeXml = escapeXml2;
    }

    public DomSerializer(CleanerProperties props2) {
        this(props2, true);
    }

    public Document createDOM(TagNode rootNode) throws ParserConfigurationException {
        Document document;
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        DOMImplementation impl = builder.getDOMImplementation();
        if (rootNode.getDocType() != null) {
            String qualifiedName = rootNode.getDocType().getPart1();
            String publicId = rootNode.getDocType().getPublicId();
            String systemId = rootNode.getDocType().getSystemId();
            if (qualifiedName == null) {
                qualifiedName = "html";
            }
            DocumentType documentType = impl.createDocumentType(qualifiedName, publicId, systemId);
            if (qualifiedName.equals("HTML")) {
                qualifiedName = "html";
            }
            document = impl.createDocument(rootNode.getNamespaceURIOnPath(""), qualifiedName, documentType);
        } else {
            document = builder.newDocument();
            document.appendChild(document.createElement(rootNode.getName()));
        }
        for (Map.Entry<String, String> entry : rootNode.getAttributes().entrySet()) {
            String attrName = (String) entry.getKey();
            String attrValue = (String) entry.getValue();
            if (this.escapeXml) {
                attrValue = Utils.escapeXml(attrValue, this.props, true);
            }
            document.getDocumentElement().setAttribute(attrName, attrValue);
            if (attrName.equalsIgnoreCase("id")) {
                document.getDocumentElement().setIdAttribute(attrName, true);
            }
        }
        createSubnodes(document, document.getDocumentElement(), rootNode.getAllChildren());
        return document;
    }

    /* access modifiers changed from: protected */
    public boolean isScriptOrStyle(Element element) {
        String tagName = element.getNodeName();
        return "script".equalsIgnoreCase(tagName) || "style".equalsIgnoreCase(tagName);
    }

    /* access modifiers changed from: protected */
    public boolean dontEscape(Element element) {
        return this.props.isUseCdataFor(element.getNodeName()) && (!element.hasChildNodes() || element.getTextContent() == null || element.getTextContent().trim().length() == 0);
    }

    /* access modifiers changed from: protected */
    public String outputCData(CData cdata) {
        return cdata.getContentWithoutStartAndEndTokens();
    }

    private void createSubnodes(Document document, Element element, List<? extends BaseToken> tagChildren) {
        if (tagChildren != null) {
            for (Object next : tagChildren) {
                if (next instanceof CommentNode) {
                    element.appendChild(document.createComment(((CommentNode) next).getContent()));
                } else if (next instanceof CData) {
                    if (this.props.isUseCdataFor(element.getNodeName())) {
                        element.appendChild(document.createCDATASection(outputCData((CData) next)));
                    }
                } else if (next instanceof ContentNode) {
                    String content = ((ContentNode) next).getContent();
                    boolean specialCase = dontEscape(element);
                    if (this.escapeXml && !specialCase) {
                        content = Utils.escapeXml(content, this.props, true);
                    }
                    element.appendChild(specialCase ? document.createCDATASection(content) : document.createTextNode(content));
                } else if (next instanceof TagNode) {
                    TagNode subTagNode = (TagNode) next;
                    Element subelement = document.createElement(subTagNode.getName());
                    for (Map.Entry<String, String> entry : subTagNode.getAttributes().entrySet()) {
                        String attrName = (String) entry.getKey();
                        String attrValue = (String) entry.getValue();
                        if (this.escapeXml) {
                            attrValue = Utils.escapeXml(attrValue, this.props, true);
                        }
                        subelement.setAttribute(attrName, attrValue);
                        if (attrName.equalsIgnoreCase("id")) {
                            subelement.setIdAttribute(attrName, true);
                        }
                    }
                    createSubnodes(document, subelement, subTagNode.getAllChildren());
                    element.appendChild(subelement);
                } else if (next instanceof List) {
                    createSubnodes(document, element, (List) next);
                }
            }
        }
    }
}
