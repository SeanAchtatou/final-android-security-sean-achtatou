package org.htmlcleaner;

public class SpecialEntity {
    private final String escapedXmlString;
    private boolean htmlSpecialEntity;
    private final String htmlString;
    private final int intCode;
    private final String key;

    public SpecialEntity(String key2, int intCode2, String htmlString2, boolean htmlSpecialEntity2) {
        this.key = key2;
        this.intCode = intCode2;
        String str = "&" + key2 + ";";
        if (htmlString2 != null) {
            this.htmlString = htmlString2;
        } else {
            this.htmlString = str;
        }
        if (htmlSpecialEntity2) {
            this.escapedXmlString = String.valueOf((char) this.intCode);
        } else {
            this.escapedXmlString = str;
        }
        this.htmlSpecialEntity = htmlSpecialEntity2;
    }

    public String getKey() {
        return this.key;
    }

    public int intValue() {
        return this.intCode;
    }

    public String getHtmlString() {
        return this.htmlString;
    }

    public String getEscapedXmlString() {
        return this.escapedXmlString;
    }

    public String getEscaped(boolean htmlEscaped) {
        return htmlEscaped ? getHtmlString() : getEscapedXmlString();
    }

    public boolean isHtmlSpecialEntity() {
        return this.htmlSpecialEntity;
    }

    public char charValue() {
        return (char) intValue();
    }

    public String getDecimalNCR() {
        return "&#" + this.intCode + ";";
    }

    public String getHexNCR() {
        return "&#x" + Integer.toHexString(this.intCode) + ";";
    }

    public String getEscapedValue() {
        return "&" + this.key + ";";
    }
}
