package org.htmlcleaner;

public abstract class BaseTokenImpl implements BaseToken {
    private int col;
    private int row;

    protected BaseTokenImpl() {
    }

    protected BaseTokenImpl(int row2, int col2) {
        this.row = row2;
        this.col = col2;
    }

    public int getRow() {
        return this.row;
    }

    public void setRow(int row2) {
        this.row = row2;
    }

    public int getCol() {
        return this.col;
    }

    public void setCol(int col2) {
        this.col = col2;
    }

    public String toString() {
        return "(line=" + getRow() + ", col=" + getCol() + ")";
    }
}
