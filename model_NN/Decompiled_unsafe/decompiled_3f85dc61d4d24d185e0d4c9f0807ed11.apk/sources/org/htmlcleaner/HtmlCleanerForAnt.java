package org.htmlcleaner;

import org.apache.tools.ant.Task;

public class HtmlCleanerForAnt extends Task {
    private boolean advancedxmlescape = true;
    private boolean allowhtmlinsideattributes = false;
    private boolean allowmultiwordattributes = true;
    private String booleanatts = CleanerProperties.BOOL_ATT_SELF;
    private String dest;
    private String hyphenreplacement = "=";
    private boolean ignoreqe = false;
    private String incharset = "UTF-8";
    private boolean namespacesaware = true;
    private String nodebyxpath = null;
    private boolean omitcomments = false;
    private boolean omitdeprtags = false;
    private boolean omitdoctypedecl = true;
    private boolean omithtmlenvelope = false;
    private boolean omitunknowntags = false;
    private boolean omitxmldecl = false;
    private String outcharset = "UTF-8";
    private String outputtype = "simple";
    private String prunetags = "";
    private boolean specialentities = true;
    private String src;
    private String taginfofile = null;
    private String text;
    private String transform = null;
    private boolean treatdeprtagsascontent = false;
    private boolean treatunknowntagsascontent = false;
    private boolean unicodechars = true;
    private boolean usecdata = true;
    private String usecdatafor = "script,style";
    private boolean useemptyelementtags = true;

    public void setText(String text2) {
        this.text = text2;
    }

    public void setSrc(String src2) {
        this.src = src2;
    }

    public void setDest(String dest2) {
        this.dest = dest2;
    }

    public void setIncharset(String incharset2) {
        this.incharset = incharset2;
    }

    public void setOutcharset(String outcharset2) {
        this.outcharset = outcharset2;
    }

    public void setTaginfofile(String taginfofile2) {
        this.taginfofile = taginfofile2;
    }

    public void setOutputtype(String outputtype2) {
        this.outputtype = outputtype2;
    }

    public void setAdvancedxmlescape(boolean advancedxmlescape2) {
        this.advancedxmlescape = advancedxmlescape2;
    }

    public void setUsecdata(boolean usecdata2) {
        this.usecdata = usecdata2;
    }

    public void setUsecdatafor(String usecdatafor2) {
        this.usecdatafor = usecdatafor2;
    }

    public void setSpecialentities(boolean specialentities2) {
        this.specialentities = specialentities2;
    }

    public void setUnicodechars(boolean unicodechars2) {
        this.unicodechars = unicodechars2;
    }

    public void setOmitunknowntags(boolean omitunknowntags2) {
        this.omitunknowntags = omitunknowntags2;
    }

    public void setTreatunknowntagsascontent(boolean treatunknowntagsascontent2) {
        this.treatunknowntagsascontent = treatunknowntagsascontent2;
    }

    public void setOmitdeprtags(boolean omitdeprtags2) {
        this.omitdeprtags = omitdeprtags2;
    }

    public void setTreatdeprtagsascontent(boolean treatdeprtagsascontent2) {
        this.treatdeprtagsascontent = treatdeprtagsascontent2;
    }

    public void setOmitcomments(boolean omitcomments2) {
        this.omitcomments = omitcomments2;
    }

    public void setOmitxmldecl(boolean omitxmldecl2) {
        this.omitxmldecl = omitxmldecl2;
    }

    public void setOmitdoctypedecl(boolean omitdoctypedecl2) {
        this.omitdoctypedecl = omitdoctypedecl2;
    }

    public void setOmithtmlenvelope(boolean omithtmlenvelope2) {
        this.omithtmlenvelope = omithtmlenvelope2;
    }

    public void setUseemptyelementtags(boolean useemptyelementtags2) {
        this.useemptyelementtags = useemptyelementtags2;
    }

    public void setAllowmultiwordattributes(boolean allowmultiwordattributes2) {
        this.allowmultiwordattributes = allowmultiwordattributes2;
    }

    public void setAllowhtmlinsideattributes(boolean allowhtmlinsideattributes2) {
        this.allowhtmlinsideattributes = allowhtmlinsideattributes2;
    }

    public void setIgnoreqe(boolean ignoreqe2) {
        this.ignoreqe = ignoreqe2;
    }

    public void setNamespacesaware(boolean namespacesaware2) {
        this.namespacesaware = namespacesaware2;
    }

    public void setHyphenreplacement(String hyphenreplacement2) {
        this.hyphenreplacement = hyphenreplacement2;
    }

    public void setPrunetags(String prunetags2) {
        this.prunetags = prunetags2;
    }

    public void setBooleanatts(String booleanatts2) {
        this.booleanatts = booleanatts2;
    }

    public void setNodebyxpath(String nodebyxpath2) {
        this.nodebyxpath = nodebyxpath2;
    }

    public void setTransform(String transform2) {
        this.transform = transform2;
    }

    public void addText(String text2) {
        this.text = text2;
    }

    /* JADX WARN: Type inference failed for: r13v0, types: [java.lang.Object[]] */
    /* JADX WARN: Type inference failed for: r3v0 */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x01e8, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x01ee, code lost:
        throw new org.apache.tools.ant.BuildException(r2);
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x01e8 A[ExcHandler: XPatherException (r2v0 'e' org.htmlcleaner.XPatherException A[CUSTOM_DECLARE]), Splitter:B:24:0x0117] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void execute() throws org.apache.tools.ant.BuildException {
        /*
            r17 = this;
            r0 = r17
            java.lang.String r14 = r0.taginfofile
            if (r14 == 0) goto L_0x002f
            org.htmlcleaner.HtmlCleaner r1 = new org.htmlcleaner.HtmlCleaner
            org.htmlcleaner.ConfigFileTagProvider r14 = new org.htmlcleaner.ConfigFileTagProvider
            java.io.File r15 = new java.io.File
            r0 = r17
            java.lang.String r0 = r0.taginfofile
            r16 = r0
            r15.<init>(r16)
            r14.<init>(r15)
            r1.<init>(r14)
        L_0x001b:
            r0 = r17
            java.lang.String r14 = r0.text
            if (r14 != 0) goto L_0x0035
            r0 = r17
            java.lang.String r14 = r0.src
            if (r14 != 0) goto L_0x0035
            org.apache.tools.ant.BuildException r14 = new org.apache.tools.ant.BuildException
            java.lang.String r15 = "Eather attribute 'src' or text body containing HTML must be specified!"
            r14.<init>(r15)
            throw r14
        L_0x002f:
            org.htmlcleaner.HtmlCleaner r1 = new org.htmlcleaner.HtmlCleaner
            r1.<init>()
            goto L_0x001b
        L_0x0035:
            org.htmlcleaner.CleanerProperties r9 = r1.getProperties()
            r0 = r17
            boolean r14 = r0.advancedxmlescape
            r9.setAdvancedXmlEscape(r14)
            r0 = r17
            java.lang.String r14 = r0.usecdatafor
            r9.setUseCdataFor(r14)
            r0 = r17
            boolean r14 = r0.usecdata
            r9.setUseCdataForScriptAndStyle(r14)
            r0 = r17
            boolean r14 = r0.specialentities
            r9.setTranslateSpecialEntities(r14)
            r0 = r17
            boolean r14 = r0.unicodechars
            r9.setRecognizeUnicodeChars(r14)
            r0 = r17
            boolean r14 = r0.omitunknowntags
            r9.setOmitUnknownTags(r14)
            r0 = r17
            boolean r14 = r0.treatunknowntagsascontent
            r9.setTreatUnknownTagsAsContent(r14)
            r0 = r17
            boolean r14 = r0.omitdeprtags
            r9.setOmitDeprecatedTags(r14)
            r0 = r17
            boolean r14 = r0.treatdeprtagsascontent
            r9.setTreatDeprecatedTagsAsContent(r14)
            r0 = r17
            boolean r14 = r0.omitcomments
            r9.setOmitComments(r14)
            r0 = r17
            boolean r14 = r0.omitxmldecl
            r9.setOmitXmlDeclaration(r14)
            r0 = r17
            boolean r14 = r0.omitdoctypedecl
            r9.setOmitDoctypeDeclaration(r14)
            r0 = r17
            boolean r14 = r0.omithtmlenvelope
            r9.setOmitHtmlEnvelope(r14)
            r0 = r17
            boolean r14 = r0.useemptyelementtags
            r9.setUseEmptyElementTags(r14)
            r0 = r17
            boolean r14 = r0.allowmultiwordattributes
            r9.setAllowMultiWordAttributes(r14)
            r0 = r17
            boolean r14 = r0.allowhtmlinsideattributes
            r9.setAllowHtmlInsideAttributes(r14)
            r0 = r17
            boolean r14 = r0.ignoreqe
            r9.setIgnoreQuestAndExclam(r14)
            r0 = r17
            boolean r14 = r0.namespacesaware
            r9.setNamespacesAware(r14)
            r0 = r17
            java.lang.String r14 = r0.hyphenreplacement
            r9.setHyphenReplacementInComment(r14)
            r0 = r17
            java.lang.String r14 = r0.prunetags
            r9.setPruneTags(r14)
            r0 = r17
            java.lang.String r14 = r0.booleanatts
            r9.setBooleanAttributeValues(r14)
            r0 = r17
            java.lang.String r14 = r0.transform
            boolean r14 = org.htmlcleaner.Utils.isEmptyString(r14)
            if (r14 != 0) goto L_0x0115
            r0 = r17
            java.lang.String r14 = r0.transform
            java.lang.String r15 = "|"
            java.lang.String[] r11 = org.htmlcleaner.Utils.tokenize(r14, r15)
            java.util.TreeMap r10 = new java.util.TreeMap
            r10.<init>()
            int r15 = r11.length
            r14 = 0
        L_0x00e8:
            if (r14 >= r15) goto L_0x0112
            r5 = r11[r14]
            r16 = 61
            r0 = r16
            int r4 = r5.indexOf(r0)
            if (r4 > 0) goto L_0x0100
            r6 = r5
        L_0x00f7:
            if (r4 > 0) goto L_0x0109
            r12 = 0
        L_0x00fa:
            r10.put(r6, r12)
            int r14 = r14 + 1
            goto L_0x00e8
        L_0x0100:
            r16 = 0
            r0 = r16
            java.lang.String r6 = r5.substring(r0, r4)
            goto L_0x00f7
        L_0x0109:
            int r16 = r4 + 1
            r0 = r16
            java.lang.String r12 = r5.substring(r0)
            goto L_0x00fa
        L_0x0112:
            r1.initCleanerTransformations(r10)
        L_0x0115:
            r0 = r17
            java.lang.String r14 = r0.src     // Catch:{ IOException -> 0x01b4, XPatherException -> 0x01e8 }
            if (r14 == 0) goto L_0x0193
            r0 = r17
            java.lang.String r14 = r0.src     // Catch:{ IOException -> 0x01b4, XPatherException -> 0x01e8 }
            java.lang.String r15 = "http://"
            boolean r14 = r14.startsWith(r15)     // Catch:{ IOException -> 0x01b4, XPatherException -> 0x01e8 }
            if (r14 != 0) goto L_0x0133
            r0 = r17
            java.lang.String r14 = r0.src     // Catch:{ IOException -> 0x01b4, XPatherException -> 0x01e8 }
            java.lang.String r15 = "https://"
            boolean r14 = r14.startsWith(r15)     // Catch:{ IOException -> 0x01b4, XPatherException -> 0x01e8 }
            if (r14 == 0) goto L_0x0193
        L_0x0133:
            java.net.URL r14 = new java.net.URL     // Catch:{ IOException -> 0x01b4, XPatherException -> 0x01e8 }
            r0 = r17
            java.lang.String r15 = r0.src     // Catch:{ IOException -> 0x01b4, XPatherException -> 0x01e8 }
            r14.<init>(r15)     // Catch:{ IOException -> 0x01b4, XPatherException -> 0x01e8 }
            r0 = r17
            java.lang.String r15 = r0.incharset     // Catch:{ IOException -> 0x01b4, XPatherException -> 0x01e8 }
            org.htmlcleaner.TagNode r7 = r1.clean(r14, r15)     // Catch:{ IOException -> 0x01b4, XPatherException -> 0x01e8 }
        L_0x0144:
            r0 = r17
            java.lang.String r14 = r0.nodebyxpath     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            if (r14 == 0) goto L_0x0162
            r0 = r17
            java.lang.String r14 = r0.nodebyxpath     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            java.lang.Object[] r13 = r7.evaluateXPath(r14)     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            int r15 = r13.length     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            r14 = 0
        L_0x0154:
            if (r14 >= r15) goto L_0x0162
            r3 = r13[r14]     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            boolean r0 = r3 instanceof org.htmlcleaner.TagNode     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            r16 = r0
            if (r16 == 0) goto L_0x01c2
            r0 = r3
            org.htmlcleaner.TagNode r0 = (org.htmlcleaner.TagNode) r0     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            r7 = r0
        L_0x0162:
            r0 = r17
            java.lang.String r14 = r0.dest     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            if (r14 == 0) goto L_0x0178
            java.lang.String r14 = ""
            r0 = r17
            java.lang.String r15 = r0.dest     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            java.lang.String r15 = r15.trim()     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            boolean r14 = r14.equals(r15)     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            if (r14 == 0) goto L_0x01c5
        L_0x0178:
            java.io.PrintStream r8 = java.lang.System.out     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
        L_0x017a:
            java.lang.String r14 = "compact"
            r0 = r17
            java.lang.String r15 = r0.outputtype     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            boolean r14 = r14.equals(r15)     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            if (r14 == 0) goto L_0x01cf
            org.htmlcleaner.CompactXmlSerializer r14 = new org.htmlcleaner.CompactXmlSerializer     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            r14.<init>(r9)     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            r0 = r17
            java.lang.String r15 = r0.outcharset     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            r14.writeToStream(r7, r8, r15)     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
        L_0x0192:
            return
        L_0x0193:
            r0 = r17
            java.lang.String r14 = r0.src     // Catch:{ IOException -> 0x01b4, XPatherException -> 0x01e8 }
            if (r14 == 0) goto L_0x01ab
            java.io.File r14 = new java.io.File     // Catch:{ IOException -> 0x01b4, XPatherException -> 0x01e8 }
            r0 = r17
            java.lang.String r15 = r0.src     // Catch:{ IOException -> 0x01b4, XPatherException -> 0x01e8 }
            r14.<init>(r15)     // Catch:{ IOException -> 0x01b4, XPatherException -> 0x01e8 }
            r0 = r17
            java.lang.String r15 = r0.incharset     // Catch:{ IOException -> 0x01b4, XPatherException -> 0x01e8 }
            org.htmlcleaner.TagNode r7 = r1.clean(r14, r15)     // Catch:{ IOException -> 0x01b4, XPatherException -> 0x01e8 }
            goto L_0x0144
        L_0x01ab:
            r0 = r17
            java.lang.String r14 = r0.text     // Catch:{ IOException -> 0x01b4, XPatherException -> 0x01e8 }
            org.htmlcleaner.TagNode r7 = r1.clean(r14)     // Catch:{ IOException -> 0x01b4, XPatherException -> 0x01e8 }
            goto L_0x0144
        L_0x01b4:
            r2 = move-exception
            org.apache.tools.ant.BuildException r14 = new org.apache.tools.ant.BuildException     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            r14.<init>(r2)     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            throw r14     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
        L_0x01bb:
            r2 = move-exception
            org.apache.tools.ant.BuildException r14 = new org.apache.tools.ant.BuildException
            r14.<init>(r2)
            throw r14
        L_0x01c2:
            int r14 = r14 + 1
            goto L_0x0154
        L_0x01c5:
            java.io.FileOutputStream r8 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            r0 = r17
            java.lang.String r14 = r0.dest     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            r8.<init>(r14)     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            goto L_0x017a
        L_0x01cf:
            java.lang.String r14 = "browser-compact"
            r0 = r17
            java.lang.String r15 = r0.outputtype     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            boolean r14 = r14.equals(r15)     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            if (r14 == 0) goto L_0x01ef
            org.htmlcleaner.BrowserCompactXmlSerializer r14 = new org.htmlcleaner.BrowserCompactXmlSerializer     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            r14.<init>(r9)     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            r0 = r17
            java.lang.String r15 = r0.outcharset     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            r14.writeToStream(r7, r8, r15)     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            goto L_0x0192
        L_0x01e8:
            r2 = move-exception
            org.apache.tools.ant.BuildException r14 = new org.apache.tools.ant.BuildException
            r14.<init>(r2)
            throw r14
        L_0x01ef:
            java.lang.String r14 = "pretty"
            r0 = r17
            java.lang.String r15 = r0.outputtype     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            boolean r14 = r14.equals(r15)     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            if (r14 == 0) goto L_0x0208
            org.htmlcleaner.PrettyXmlSerializer r14 = new org.htmlcleaner.PrettyXmlSerializer     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            r14.<init>(r9)     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            r0 = r17
            java.lang.String r15 = r0.outcharset     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            r14.writeToStream(r7, r8, r15)     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            goto L_0x0192
        L_0x0208:
            org.htmlcleaner.SimpleXmlSerializer r14 = new org.htmlcleaner.SimpleXmlSerializer     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            r14.<init>(r9)     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            r0 = r17
            java.lang.String r15 = r0.outcharset     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            r14.writeToStream(r7, r8, r15)     // Catch:{ IOException -> 0x01bb, XPatherException -> 0x01e8 }
            goto L_0x0192
        */
        throw new UnsupportedOperationException("Method not decompiled: org.htmlcleaner.HtmlCleanerForAnt.execute():void");
    }
}
