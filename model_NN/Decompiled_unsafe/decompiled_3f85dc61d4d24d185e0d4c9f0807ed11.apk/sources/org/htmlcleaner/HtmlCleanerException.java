package org.htmlcleaner;

public class HtmlCleanerException extends RuntimeException {
    public HtmlCleanerException() {
        this("HtmlCleaner expression occureed!");
    }

    public HtmlCleanerException(Throwable cause) {
        super(cause);
    }

    public HtmlCleanerException(String message) {
        super(message);
    }

    public HtmlCleanerException(String message, Throwable cause) {
        super(message, cause);
    }
}
