package org.htmlcleaner;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class Html5TagProvider implements ITagInfoProvider {
    private static final String CLOSE_BEFORE_COPY_INSIDE_TAGS = "bdo,strong,em,q,b,i,sub,sup,small,s";
    private static final String CLOSE_BEFORE_TAGS = "p,summary,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml";
    public static final Html5TagProvider INSTANCE = new Html5TagProvider();
    private static final String MEDIA_TAGS = "audio,video,object,source";
    private static final String PHRASING_TAGS = "a,abbr,area,audio,b,bdi,bdo,br,button,canvas,cite,code,command,datalist,del,dfn,em,i,input,ins,kbd,keygen,label,link,map,mark,meta,meter,noscript,output,progress,p,ruby,samp,s,script,select,small,span,strong,sub,sup,template,textarea,time,u,var,wbr";
    private static final String STRONG = "strong";
    public MathMLTagProvider INSTANCE2;
    private ConcurrentMap<String, TagInfo> tagInfoMap = new ConcurrentHashMap();

    public Html5TagProvider() {
        semanticFlowTags(null);
        interactiveTags(null);
        groupingTags(null);
        phrasingTags(null);
        mediaTags(null);
        editTags(null);
        formTags(null);
        tableTags(null);
        metadataTags(null);
        scriptingTags(null);
        this.INSTANCE2 = new MathMLTagProvider(null, this.tagInfoMap);
    }

    public void semanticFlowTags(TagInfo tagInfo) {
        TagInfo tagInfo2 = new TagInfo("math", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo2.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo2.defineCloseBeforeTags("math,p,summary,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("math", tagInfo2);
        TagInfo tagInfo3 = new TagInfo("section", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo3.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo3.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("section", tagInfo3);
        TagInfo tagInfo4 = new TagInfo("nav", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo4.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo4.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("nav", tagInfo4);
        TagInfo tagInfo5 = new TagInfo("article", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo5.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo5.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        tagInfo5.defineForbiddenTags("menu");
        put("article", tagInfo5);
        TagInfo tagInfo6 = new TagInfo("aside", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo6.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo6.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        tagInfo6.defineForbiddenTags("menu");
        tagInfo6.defineForbiddenTags("address");
        put("aside", tagInfo6);
        TagInfo tagInfo7 = new TagInfo("h1", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo7.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo7.defineCloseBeforeTags("p,summary,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml,h1,h2,h3,h4,h5,h6");
        put("h1", tagInfo7);
        TagInfo tagInfo8 = new TagInfo("h2", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo8.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo8.defineCloseBeforeTags("p,summary,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml,h1,h2,h3,h4,h5,h6");
        put("h2", tagInfo8);
        TagInfo tagInfo9 = new TagInfo("h3", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo9.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo9.defineCloseBeforeTags("p,summary,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml,h1,h2,h3,h4,h5,h6");
        put("h3", tagInfo9);
        TagInfo tagInfo10 = new TagInfo("h4", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo10.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo10.defineCloseBeforeTags("p,summary,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml,h1,h2,h3,h4,h5,h6");
        put("h4", tagInfo10);
        TagInfo tagInfo11 = new TagInfo("h5", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo11.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo11.defineCloseBeforeTags("p,summary,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml,h1,h2,h3,h4,h5,h6");
        put("h5", tagInfo11);
        TagInfo tagInfo12 = new TagInfo("h6", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo12.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo12.defineCloseBeforeTags("p,summary,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml,h1,h2,h3,h4,h5,h6");
        put("h6", tagInfo12);
        TagInfo tagInfo13 = new TagInfo("hgroup", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo13.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo13.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        tagInfo13.defineAllowedChildrenTags("h1,h2,h3,h4,h5,h6");
        put("hgroup", tagInfo13);
        TagInfo tagInfo14 = new TagInfo("header", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo14.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo14.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        tagInfo14.defineForbiddenTags("menu,header,footer");
        put("header", tagInfo14);
        TagInfo tagInfo15 = new TagInfo("footer", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo15.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo15.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        tagInfo15.defineForbiddenTags("menu,header,footer");
        put("footer", tagInfo15);
        TagInfo tagInfo16 = new TagInfo("main", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo16.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo16.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("main", tagInfo16);
        TagInfo tagInfo17 = new TagInfo("address", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo17.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo17.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        tagInfo17.defineForbiddenTags("address");
        put("address", tagInfo17);
    }

    public void interactiveTags(TagInfo tagInfo) {
        TagInfo tagInfo2 = new TagInfo("details", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo2.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo2.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("details", tagInfo2);
        TagInfo tagInfo3 = new TagInfo("summary", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo3.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo3.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        tagInfo3.defineRequiredEnclosingTags("details");
        tagInfo3.defineForbiddenTags("summary");
        put("summary", tagInfo3);
        TagInfo tagInfo4 = new TagInfo("command", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo4.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo4.defineForbiddenTags("command");
        tagInfo4.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("command", tagInfo4);
        TagInfo tagInfo5 = new TagInfo("menu", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo5.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo5.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        tagInfo5.defineAllowedChildrenTags("menuitem,li");
        put("menu", tagInfo5);
        TagInfo tagInfo6 = new TagInfo("menuitem", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo6.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo6.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        tagInfo6.defineRequiredEnclosingTags("menu");
        put("menuitem", tagInfo6);
        TagInfo tagInfo7 = new TagInfo("dialog", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any);
        tagInfo7.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("dialog", tagInfo7);
    }

    public void groupingTags(TagInfo tagInfo) {
        TagInfo tagInfo2 = new TagInfo("div", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo2.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo2.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("div", tagInfo2);
        TagInfo tagInfo3 = new TagInfo("figure", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo3.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo3.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("figure", tagInfo3);
        TagInfo tagInfo4 = new TagInfo("figcaption", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any);
        tagInfo4.defineRequiredEnclosingTags("figure");
        put("figcaption", tagInfo4);
        TagInfo tagInfo5 = new TagInfo("p", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo5.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo5.defineCloseBeforeTags("p,address,summary,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml,time");
        put("p", tagInfo5);
        TagInfo tagInfo6 = new TagInfo("pre", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo6.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo6.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("pre", tagInfo6);
        TagInfo tagInfo7 = new TagInfo("ul", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo7.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo7.defineCloseBeforeTags("dl,p,summary,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        tagInfo7.defineAllowedChildrenTags("li,ul,ol,div");
        put("ul", tagInfo7);
        TagInfo tagInfo8 = new TagInfo("ol", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo8.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo8.defineCloseBeforeTags("dl,p,summary,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        tagInfo8.defineAllowedChildrenTags("li,ul,ol,div");
        put("ol", tagInfo8);
        TagInfo tagInfo9 = new TagInfo("li", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo9.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo9.defineCloseBeforeTags("li,p,summary,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        tagInfo9.defineRequiredEnclosingTags("ol,menu,ul");
        put("li", tagInfo9);
        TagInfo tagInfo10 = new TagInfo("dl", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo10.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo10.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        tagInfo10.defineAllowedChildrenTags("dt,dd");
        put("dl", tagInfo10);
        TagInfo tagInfo11 = new TagInfo("dt", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo11.defineCloseBeforeTags("dt,dd");
        tagInfo11.defineRequiredEnclosingTags("dl");
        put("dt", tagInfo11);
        TagInfo tagInfo12 = new TagInfo("dd", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo12.defineCloseBeforeTags("dt,dd");
        tagInfo12.defineRequiredEnclosingTags("dl");
        put("dd", tagInfo12);
        TagInfo tagInfo13 = new TagInfo("hr", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.block);
        tagInfo13.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo13.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("hr", tagInfo13);
        TagInfo tagInfo14 = new TagInfo("blockquote", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo14.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo14.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("blockquote", tagInfo14);
    }

    public void phrasingTags(TagInfo tagInfo) {
        put("em", new TagInfo("em", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put(STRONG, new TagInfo(STRONG, ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        TagInfo tagInfo2 = new TagInfo("small", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo2.defineCloseInsideCopyAfterTags("b,u,i,sub,sup,blink,s");
        put("small", tagInfo2);
        TagInfo tagInfo3 = new TagInfo("s", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo3.defineCloseInsideCopyAfterTags("b,u,i,sub,sup,small,blink");
        put("s", tagInfo3);
        TagInfo tagInfo4 = new TagInfo("a", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo4.defineCloseBeforeTags("a");
        put("a", tagInfo4);
        put("wbr", new TagInfo("wbr", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.none));
        TagInfo tagInfo5 = new TagInfo("mark", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo5.defineAllowedChildrenTags(PHRASING_TAGS);
        put("mark", tagInfo5);
        TagInfo tagInfo6 = new TagInfo("bdi", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo6.defineAllowedChildrenTags(PHRASING_TAGS);
        put("bdi", tagInfo6);
        TagInfo tagInfo7 = new TagInfo("time", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo7.defineAllowedChildrenTags(PHRASING_TAGS);
        put("time", tagInfo7);
        TagInfo tagInfo8 = new TagInfo("data", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo8.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("data", tagInfo8);
        put("cite", new TagInfo("cite", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("q", new TagInfo("q", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("code", new TagInfo("code", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("span", new TagInfo("span", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("bdo", new TagInfo("bdo", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("dfn", new TagInfo("dfn", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("kbd", new TagInfo("kbd", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("abbr", new TagInfo("abbr", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("var", new TagInfo("var", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("samp", new TagInfo("samp", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        put("br", new TagInfo("br", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.none));
        TagInfo tagInfo9 = new TagInfo("sub", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo9.defineCloseInsideCopyAfterTags("b,u,i,sup,small,blink,s");
        put("sub", tagInfo9);
        TagInfo tagInfo10 = new TagInfo("sup", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo10.defineCloseInsideCopyAfterTags("b,u,i,sub,small,blink,s");
        put("sup", tagInfo10);
        TagInfo tagInfo11 = new TagInfo("b", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo11.defineCloseInsideCopyAfterTags("u,i,sub,sup,small,blink,s");
        put("b", tagInfo11);
        TagInfo tagInfo12 = new TagInfo("i", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo12.defineCloseInsideCopyAfterTags("b,u,sub,sup,small,blink,s");
        put("i", tagInfo12);
        TagInfo tagInfo13 = new TagInfo("u", ContentType.all, BelongsTo.BODY, true, false, false, CloseTag.required, Display.inline);
        tagInfo13.defineCloseInsideCopyAfterTags("b,i,sub,sup,small,blink,s");
        put("u", tagInfo13);
        TagInfo tagInfo14 = new TagInfo("ruby", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo14.defineAllowedChildrenTags("rt,rp,rb,rtc");
        put("ruby", tagInfo14);
        TagInfo tagInfo15 = new TagInfo("rtc", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.inline);
        tagInfo15.defineRequiredEnclosingTags("ruby");
        tagInfo15.defineAllowedChildrenTags("rt,a,abbr,area,audio,b,bdi,bdo,br,button,canvas,cite,code,command,datalist,del,dfn,em,i,input,ins,kbd,keygen,label,link,map,mark,meta,meter,noscript,output,progress,p,ruby,samp,s,script,select,small,span,strong,sub,sup,template,textarea,time,u,var,wbr");
        put("rtc", tagInfo15);
        TagInfo tagInfo16 = new TagInfo("rb", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.inline);
        tagInfo16.defineRequiredEnclosingTags("ruby");
        put("rb", tagInfo16);
        TagInfo tagInfo17 = new TagInfo("rt", ContentType.text, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.inline);
        tagInfo17.defineRequiredEnclosingTags("ruby");
        tagInfo17.defineAllowedChildrenTags(PHRASING_TAGS);
        put("rt", tagInfo17);
        TagInfo tagInfo18 = new TagInfo("rp", ContentType.text, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.inline);
        tagInfo18.defineRequiredEnclosingTags("ruby");
        tagInfo18.defineAllowedChildrenTags(PHRASING_TAGS);
        put("rp", tagInfo18);
    }

    public void mediaTags(TagInfo tagInfo) {
        put("img", new TagInfo("img", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.inline));
        put("iframe", new TagInfo("iframe", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any));
        TagInfo tagInfo2 = new TagInfo("embed", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.block);
        tagInfo2.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo2.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        put("embed", tagInfo2);
        put("object", new TagInfo("object", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any));
        TagInfo tagInfo3 = new TagInfo("param", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.none);
        tagInfo3.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo3.defineCloseBeforeTags(CLOSE_BEFORE_TAGS);
        tagInfo3.defineRequiredEnclosingTags("object");
        put("param", tagInfo3);
        TagInfo tagInfo4 = new TagInfo("audio", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any);
        tagInfo4.defineCloseInsideCopyAfterTags(MEDIA_TAGS);
        put("audio", tagInfo4);
        TagInfo tagInfo5 = new TagInfo("picture", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any);
        tagInfo5.defineCloseInsideCopyAfterTags(MEDIA_TAGS);
        put("picture", tagInfo5);
        TagInfo tagInfo6 = new TagInfo("video", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any);
        tagInfo6.defineCloseInsideCopyAfterTags(MEDIA_TAGS);
        put("video", tagInfo6);
        TagInfo tagInfo7 = new TagInfo("source", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.any);
        tagInfo7.defineRequiredEnclosingTags("audio,video,object");
        put("source", tagInfo7);
        TagInfo tagInfo8 = new TagInfo("track", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.any);
        tagInfo8.defineRequiredEnclosingTags(MEDIA_TAGS);
        put("track", tagInfo8);
        put("canvas", new TagInfo("canvas", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any));
        TagInfo tagInfo9 = new TagInfo("area", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.none);
        tagInfo9.defineFatalTags("map");
        tagInfo9.defineCloseBeforeTags("area");
        put("area", tagInfo9);
        TagInfo tagInfo10 = new TagInfo("map", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any);
        tagInfo10.defineCloseBeforeTags("map");
        tagInfo10.defineAllowedChildrenTags("area");
        put("map", tagInfo10);
    }

    public void editTags(TagInfo tagInfo) {
        put("ins", new TagInfo("ins", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any));
        put("del", new TagInfo("del", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any));
    }

    public void tableTags(TagInfo tagInfo) {
        TagInfo tagInfo2 = new TagInfo("table", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo2.defineAllowedChildrenTags("tr,tbody,thead,tfoot,col,colgroup,caption");
        tagInfo2.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo2.defineCloseBeforeTags("tr,thead,tbody,tfoot,caption,colgroup,table,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("table", tagInfo2);
        TagInfo tagInfo3 = new TagInfo("tr", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo3.defineFatalTags("table");
        tagInfo3.defineRequiredEnclosingTags("tbody");
        tagInfo3.defineAllowedChildrenTags("td,th");
        tagInfo3.defineHigherLevelTags("thead,tfoot");
        tagInfo3.defineCloseBeforeTags("tr,td,th,caption,colgroup");
        put("tr", tagInfo3);
        TagInfo tagInfo4 = new TagInfo("td", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo4.defineFatalTags("table");
        tagInfo4.defineRequiredEnclosingTags("tr");
        tagInfo4.defineCloseBeforeTags("td,th,caption,colgroup");
        put("td", tagInfo4);
        TagInfo tagInfo5 = new TagInfo("th", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo5.defineFatalTags("table");
        tagInfo5.defineRequiredEnclosingTags("tr");
        tagInfo5.defineCloseBeforeTags("td,th,caption,colgroup");
        put("th", tagInfo5);
        TagInfo tagInfo6 = new TagInfo("tbody", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo6.defineFatalTags("table");
        tagInfo6.defineAllowedChildrenTags("tr,form");
        tagInfo6.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
        put("tbody", tagInfo6);
        TagInfo tagInfo7 = new TagInfo("thead", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo7.defineFatalTags("table");
        tagInfo7.defineAllowedChildrenTags("tr,form");
        tagInfo7.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
        put("thead", tagInfo7);
        TagInfo tagInfo8 = new TagInfo("tfoot", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo8.defineFatalTags("table");
        tagInfo8.defineAllowedChildrenTags("tr,form");
        tagInfo8.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
        put("tfoot", tagInfo8);
        TagInfo tagInfo9 = new TagInfo("col", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.block);
        tagInfo9.defineFatalTags("colgroup");
        put("col", tagInfo9);
        TagInfo tagInfo10 = new TagInfo("colgroup", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.optional, Display.block);
        tagInfo10.defineFatalTags("table");
        tagInfo10.defineAllowedChildrenTags("col");
        tagInfo10.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
        put("colgroup", tagInfo10);
        TagInfo tagInfo11 = new TagInfo("caption", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo11.defineFatalTags("table");
        tagInfo11.defineCloseBeforeTags("td,th,tr,tbody,thead,tfoot,caption,colgroup");
        put("caption", tagInfo11);
    }

    public void formTags(TagInfo tagInfo) {
        TagInfo tagInfo2 = new TagInfo("meter", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo2.defineAllowedChildrenTags(PHRASING_TAGS);
        tagInfo2.defineCloseBeforeTags("meter");
        put("meter", tagInfo2);
        TagInfo tagInfo3 = new TagInfo("form", ContentType.all, BelongsTo.BODY, false, false, true, CloseTag.required, Display.block);
        tagInfo3.defineForbiddenTags("form");
        tagInfo3.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo3.defineCloseBeforeTags("option,optgroup,textarea,select,fieldset,p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("form", tagInfo3);
        TagInfo tagInfo4 = new TagInfo("input", ContentType.none, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.inline);
        tagInfo4.defineCloseBeforeTags("select,optgroup,option");
        put("input", tagInfo4);
        TagInfo tagInfo5 = new TagInfo("textarea", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline);
        tagInfo5.defineCloseBeforeTags("select,optgroup,option");
        put("textarea", tagInfo5);
        TagInfo tagInfo6 = new TagInfo("select", ContentType.all, BelongsTo.BODY, false, false, true, CloseTag.required, Display.inline);
        tagInfo6.defineAllowedChildrenTags("option,optgroup");
        tagInfo6.defineCloseBeforeTags("option,optgroup,select");
        put("select", tagInfo6);
        TagInfo tagInfo7 = new TagInfo("option", ContentType.text, BelongsTo.BODY, false, false, true, CloseTag.optional, Display.inline);
        tagInfo7.defineFatalTags("select,datalist");
        tagInfo7.defineCloseBeforeTags("option");
        put("option", tagInfo7);
        TagInfo tagInfo8 = new TagInfo("optgroup", ContentType.all, BelongsTo.BODY, false, false, true, CloseTag.required, Display.inline);
        tagInfo8.defineFatalTags("select");
        tagInfo8.defineAllowedChildrenTags("option");
        tagInfo8.defineCloseBeforeTags("optgroup");
        put("optgroup", tagInfo8);
        TagInfo tagInfo9 = new TagInfo("button", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any);
        tagInfo9.defineCloseBeforeTags("select,optgroup,option");
        put("button", tagInfo9);
        put("label", new TagInfo("label", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.inline));
        TagInfo tagInfo10 = new TagInfo("legend", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo10.defineRequiredEnclosingTags("fieldset");
        tagInfo10.defineAllowedChildrenTags(PHRASING_TAGS);
        put("legend", tagInfo10);
        TagInfo tagInfo11 = new TagInfo("fieldset", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.block);
        tagInfo11.defineCloseBeforeCopyInsideTags(CLOSE_BEFORE_COPY_INSIDE_TAGS);
        tagInfo11.defineCloseBeforeTags("p,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("fieldset", tagInfo11);
        TagInfo tagInfo12 = new TagInfo("progress", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any);
        tagInfo12.defineAllowedChildrenTags(PHRASING_TAGS);
        tagInfo12.defineCloseBeforeTags("progress");
        put("progress", tagInfo12);
        TagInfo tagInfo13 = new TagInfo("datalist", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any);
        tagInfo13.defineAllowedChildrenTags("option");
        tagInfo13.defineCloseBeforeTags("datalist");
        put("datalist", tagInfo13);
        put("keygen", new TagInfo("keygen", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.forbidden, Display.any));
        TagInfo tagInfo14 = new TagInfo("output", ContentType.all, BelongsTo.BODY, false, false, false, CloseTag.required, Display.any);
        tagInfo14.defineCloseBeforeTags("output,p,summary,address,label,abbr,acronym,dfn,kbd,samp,var,cite,code,param,xml");
        put("output", tagInfo14);
    }

    public void metadataTags(TagInfo tagInfo) {
        put("meta", new TagInfo("meta", ContentType.none, BelongsTo.HEAD_AND_BODY, false, false, false, CloseTag.forbidden, Display.none));
        put("link", new TagInfo("link", ContentType.none, BelongsTo.HEAD_AND_BODY, false, false, false, CloseTag.forbidden, Display.none));
        put("title", new TagInfo("title", ContentType.text, BelongsTo.HEAD, false, true, false, CloseTag.required, Display.none));
        put("style", new TagInfo("style", ContentType.text, BelongsTo.HEAD_AND_BODY, false, false, false, CloseTag.required, Display.none));
        put("base", new TagInfo("base", ContentType.none, BelongsTo.HEAD, false, false, false, CloseTag.forbidden, Display.none));
    }

    public void scriptingTags(TagInfo tagInfo) {
        put("script", new TagInfo("script", ContentType.all, BelongsTo.HEAD_AND_BODY, false, false, false, CloseTag.required, Display.none));
        put("noscript", new TagInfo("noscript", ContentType.all, BelongsTo.HEAD_AND_BODY, false, false, false, CloseTag.required, Display.block));
    }

    /* access modifiers changed from: protected */
    public void put(String tagName, TagInfo tagInfo) {
        this.tagInfoMap.put(tagName, tagInfo);
    }

    public TagInfo getTagInfo(String tagName) {
        if (tagName == null) {
            return null;
        }
        return this.tagInfoMap.get(tagName);
    }
}
