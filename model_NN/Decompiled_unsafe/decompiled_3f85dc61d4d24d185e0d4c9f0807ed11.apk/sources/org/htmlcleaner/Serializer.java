package org.htmlcleaner;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;
import org.apache.commons.io.IOUtils;

public abstract class Serializer {
    protected CleanerProperties props;

    /* access modifiers changed from: protected */
    public abstract void serialize(TagNode tagNode, Writer writer) throws IOException;

    private class HeadlessTagNode extends TagNode {
        private HeadlessTagNode(TagNode wrappedNode) {
            super("");
            Map<String, String> wrappedNSDecls;
            getAttributes().putAll(wrappedNode.getAttributes());
            addChildren(wrappedNode.getAllChildren());
            setDocType(wrappedNode.getDocType());
            Map<String, String> nsDecls = getNamespaceDeclarations();
            if (nsDecls != null && (wrappedNSDecls = wrappedNode.getNamespaceDeclarations()) != null) {
                nsDecls.putAll(wrappedNSDecls);
            }
        }
    }

    protected Serializer(CleanerProperties props2) {
        this.props = props2;
    }

    public void writeToStream(TagNode tagNode, OutputStream out, String charset, boolean omitEnvelope) throws IOException {
        write(tagNode, new OutputStreamWriter(out, charset), charset, omitEnvelope);
    }

    public void writeToStream(TagNode tagNode, OutputStream out, String charset) throws IOException {
        writeToStream(tagNode, out, charset, false);
    }

    public void writeToStream(TagNode tagNode, OutputStream out, boolean omitEnvelope) throws IOException {
        writeToStream(tagNode, out, this.props.getCharset(), omitEnvelope);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.htmlcleaner.Serializer.writeToStream(org.htmlcleaner.TagNode, java.io.OutputStream, boolean):void
     arg types: [org.htmlcleaner.TagNode, java.io.OutputStream, int]
     candidates:
      org.htmlcleaner.Serializer.writeToStream(org.htmlcleaner.TagNode, java.io.OutputStream, java.lang.String):void
      org.htmlcleaner.Serializer.writeToStream(org.htmlcleaner.TagNode, java.io.OutputStream, boolean):void */
    public void writeToStream(TagNode tagNode, OutputStream out) throws IOException {
        writeToStream(tagNode, out, false);
    }

    public void writeToFile(TagNode tagNode, String fileName, String charset, boolean omitEnvelope) throws IOException {
        writeToStream(tagNode, new FileOutputStream(fileName), charset, omitEnvelope);
    }

    public void writeToFile(TagNode tagNode, String fileName, String charset) throws IOException {
        writeToFile(tagNode, fileName, charset, false);
    }

    public void writeToFile(TagNode tagNode, String fileName, boolean omitEnvelope) throws IOException {
        writeToFile(tagNode, fileName, this.props.getCharset(), omitEnvelope);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.htmlcleaner.Serializer.writeToFile(org.htmlcleaner.TagNode, java.lang.String, boolean):void
     arg types: [org.htmlcleaner.TagNode, java.lang.String, int]
     candidates:
      org.htmlcleaner.Serializer.writeToFile(org.htmlcleaner.TagNode, java.lang.String, java.lang.String):void
      org.htmlcleaner.Serializer.writeToFile(org.htmlcleaner.TagNode, java.lang.String, boolean):void */
    public void writeToFile(TagNode tagNode, String fileName) throws IOException {
        writeToFile(tagNode, fileName, false);
    }

    public String getAsString(TagNode tagNode, String charset, boolean omitEnvelope) {
        StringWriter writer = new StringWriter();
        try {
            write(tagNode, writer, charset, omitEnvelope);
            return writer.getBuffer().toString();
        } catch (IOException e) {
            throw new HtmlCleanerException(e);
        }
    }

    public String getAsString(TagNode tagNode, String charset) {
        return getAsString(tagNode, charset, false);
    }

    public String getAsString(TagNode tagNode, boolean omitEnvelope) {
        return getAsString(tagNode, this.props.getCharset(), omitEnvelope);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.htmlcleaner.Serializer.getAsString(org.htmlcleaner.TagNode, boolean):java.lang.String
     arg types: [org.htmlcleaner.TagNode, int]
     candidates:
      org.htmlcleaner.Serializer.getAsString(org.htmlcleaner.TagNode, java.lang.String):java.lang.String
      org.htmlcleaner.Serializer.getAsString(org.htmlcleaner.TagNode, boolean):java.lang.String */
    public String getAsString(TagNode tagNode) {
        return getAsString(tagNode, false);
    }

    public String getAsString(String htmlContent) {
        return getAsString(new HtmlCleaner(this.props).clean(htmlContent), this.props.getCharset());
    }

    public void write(TagNode tagNode, Writer writer, String charset) throws IOException {
        write(tagNode, writer, charset, false);
    }

    public void write(TagNode tagNode, Writer writer, String charset, boolean omitEnvelope) throws IOException {
        DoctypeToken doctypeToken;
        if (omitEnvelope) {
            tagNode = new HeadlessTagNode(tagNode);
        }
        Writer writer2 = new BufferedWriter(writer);
        if (!this.props.isOmitXmlDeclaration()) {
            String declaration = "<?xml version=\"1.0\"";
            if (charset != null) {
                declaration = declaration + " encoding=\"" + charset + "\"";
            }
            writer2.write((declaration + "?>") + IOUtils.LINE_SEPARATOR_UNIX);
        }
        if (!this.props.isOmitDoctypeDeclaration() && (doctypeToken = tagNode.getDocType()) != null) {
            doctypeToken.serialize(this, writer2);
        }
        serialize(tagNode, writer2);
        writer2.flush();
        writer2.close();
    }

    /* access modifiers changed from: protected */
    public boolean isScriptOrStyle(TagNode tagNode) {
        String tagName = tagNode.getName();
        return "script".equalsIgnoreCase(tagName) || "style".equalsIgnoreCase(tagName);
    }
}
