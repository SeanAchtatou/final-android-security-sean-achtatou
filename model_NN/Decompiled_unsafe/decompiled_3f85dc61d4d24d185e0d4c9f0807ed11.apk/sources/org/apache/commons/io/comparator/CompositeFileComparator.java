package org.apache.commons.io.comparator;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class CompositeFileComparator extends AbstractFileComparator implements Serializable {
    private static final Comparator<?>[] NO_COMPARATORS = new Comparator[0];
    private final Comparator<File>[] delegates;

    public /* bridge */ /* synthetic */ List sort(List x0) {
        return super.sort(x0);
    }

    public /* bridge */ /* synthetic */ File[] sort(File[] x0) {
        return super.sort(x0);
    }

    public CompositeFileComparator(Comparator<File>... delegates2) {
        if (delegates2 == null) {
            this.delegates = (Comparator[]) NO_COMPARATORS;
            return;
        }
        this.delegates = (Comparator[]) new Comparator[delegates2.length];
        System.arraycopy(delegates2, 0, this.delegates, 0, delegates2.length);
    }

    public CompositeFileComparator(Iterable<Comparator<File>> delegates2) {
        if (delegates2 == null) {
            this.delegates = (Comparator[]) NO_COMPARATORS;
            return;
        }
        List<Comparator<File>> list = new ArrayList<>();
        for (Comparator<File> comparator : delegates2) {
            list.add(comparator);
        }
        this.delegates = (Comparator[]) list.toArray(new Comparator[list.size()]);
    }

    public int compare(File file1, File file2) {
        int result = 0;
        for (Comparator<File> delegate : this.delegates) {
            result = delegate.compare(file1, file2);
            if (result != 0) {
                break;
            }
        }
        return result;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(super.toString());
        builder.append('{');
        for (int i = 0; i < this.delegates.length; i++) {
            if (i > 0) {
                builder.append(',');
            }
            builder.append(this.delegates[i]);
        }
        builder.append('}');
        return builder.toString();
    }
}
