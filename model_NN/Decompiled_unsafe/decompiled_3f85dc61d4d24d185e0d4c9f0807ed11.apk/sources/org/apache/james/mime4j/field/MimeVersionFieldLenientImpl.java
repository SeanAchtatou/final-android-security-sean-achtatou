package org.apache.james.mime4j.field;

import java.util.BitSet;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.field.MimeVersionField;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.stream.ParserCursor;
import org.apache.james.mime4j.stream.RawField;
import org.apache.james.mime4j.stream.RawFieldParser;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.util.ContentUtil;

public class MimeVersionFieldLenientImpl extends AbstractField implements MimeVersionField {
    public static final int DEFAULT_MAJOR_VERSION = 1;
    public static final int DEFAULT_MINOR_VERSION = 0;
    private static final BitSet DELIM = RawFieldParser.INIT_BITSET(46);
    private static final int FULL_STOP = 46;
    public static final FieldParser<MimeVersionField> PARSER = new FieldParser<MimeVersionField>() {
        public MimeVersionField parse(Field rawField, DecodeMonitor monitor) {
            return new MimeVersionFieldLenientImpl(rawField, monitor);
        }
    };
    private int major = 1;
    private int minor = 0;
    private boolean parsed = false;

    MimeVersionFieldLenientImpl(Field rawField, DecodeMonitor monitor) {
        super(rawField, monitor);
    }

    private void parse() {
        this.parsed = true;
        this.major = 1;
        this.minor = 0;
        RawField f = getRawField();
        ByteSequence buf = f.getRaw();
        int pos = f.getDelimiterIdx() + 1;
        if (buf == null) {
            String body = f.getBody();
            if (body != null) {
                buf = ContentUtil.encode(body);
                pos = 0;
            } else {
                return;
            }
        }
        RawFieldParser parser = RawFieldParser.DEFAULT;
        ParserCursor cursor = new ParserCursor(pos, buf.length());
        try {
            this.major = Integer.parseInt(parser.parseValue(buf, cursor, DELIM));
            if (this.major < 0) {
                this.major = 0;
            }
        } catch (NumberFormatException e) {
        }
        if (!cursor.atEnd() && buf.byteAt(cursor.getPos()) == 46) {
            cursor.updatePos(cursor.getPos() + 1);
        }
        try {
            this.minor = Integer.parseInt(parser.parseValue(buf, cursor, null));
            if (this.minor < 0) {
                this.minor = 0;
            }
        } catch (NumberFormatException e2) {
        }
    }

    public int getMinorVersion() {
        if (!this.parsed) {
            parse();
        }
        return this.minor;
    }

    public int getMajorVersion() {
        if (!this.parsed) {
            parse();
        }
        return this.major;
    }
}
