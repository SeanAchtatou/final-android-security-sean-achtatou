package org.apache.james.mime4j.stream;

class BasicBodyDescriptor implements BodyDescriptor {
    private final String boundary;
    private final String charset;
    private final long contentLength;
    private final String mediaType;
    private final String mimeType;
    private final String subType;
    private final String transferEncoding;

    BasicBodyDescriptor(String mimeType2, String mediaType2, String subType2, String boundary2, String charset2, String transferEncoding2, long contentLength2) {
        this.mimeType = mimeType2;
        this.mediaType = mediaType2;
        this.subType = subType2;
        this.boundary = boundary2;
        this.charset = charset2;
        this.transferEncoding = transferEncoding2;
        this.contentLength = contentLength2;
    }

    public String getMimeType() {
        return this.mimeType;
    }

    public String getMediaType() {
        return this.mediaType;
    }

    public String getSubType() {
        return this.subType;
    }

    public String getBoundary() {
        return this.boundary;
    }

    public String getCharset() {
        return this.charset;
    }

    public String getTransferEncoding() {
        return this.transferEncoding;
    }

    public long getContentLength() {
        return this.contentLength;
    }

    public String toString() {
        return "[mimeType=" + this.mimeType + ", mediaType=" + this.mediaType + ", subType=" + this.subType + ", boundary=" + this.boundary + ", charset=" + this.charset + "]";
    }
}
