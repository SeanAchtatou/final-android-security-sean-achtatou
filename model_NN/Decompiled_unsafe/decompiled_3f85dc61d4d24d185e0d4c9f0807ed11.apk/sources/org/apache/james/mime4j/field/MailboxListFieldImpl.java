package org.apache.james.mime4j.field;

import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.address.MailboxList;
import org.apache.james.mime4j.dom.field.MailboxListField;
import org.apache.james.mime4j.field.address.AddressBuilder;
import org.apache.james.mime4j.field.address.ParseException;
import org.apache.james.mime4j.stream.Field;

public class MailboxListFieldImpl extends AbstractField implements MailboxListField {
    public static final FieldParser<MailboxListField> PARSER = new FieldParser<MailboxListField>() {
        public MailboxListField parse(Field rawField, DecodeMonitor monitor) {
            return new MailboxListFieldImpl(rawField, monitor);
        }
    };
    private MailboxList mailboxList;
    private ParseException parseException;
    private boolean parsed = false;

    MailboxListFieldImpl(Field rawField, DecodeMonitor monitor) {
        super(rawField, monitor);
    }

    public MailboxList getMailboxList() {
        if (!this.parsed) {
            parse();
        }
        return this.mailboxList;
    }

    public ParseException getParseException() {
        if (!this.parsed) {
            parse();
        }
        return this.parseException;
    }

    private void parse() {
        try {
            this.mailboxList = AddressBuilder.DEFAULT.parseAddressList(getBody(), this.monitor).flatten();
        } catch (ParseException e) {
            this.parseException = e;
        }
        this.parsed = true;
    }
}
