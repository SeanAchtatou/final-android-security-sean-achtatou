package org.apache.james.mime4j.message;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.field.DefaultFieldParser;
import org.apache.james.mime4j.stream.BodyDescriptor;
import org.apache.james.mime4j.stream.BodyDescriptorBuilder;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.stream.RawField;
import org.apache.james.mime4j.util.MimeUtil;

public class DefaultBodyDescriptorBuilder implements BodyDescriptorBuilder {
    private static final String CONTENT_TYPE = "Content-Type".toLowerCase(Locale.US);
    private static final String DEFAULT_MEDIA_TYPE = "text";
    private static final String DEFAULT_MIME_TYPE = "text/plain";
    private static final String DEFAULT_SUB_TYPE = "plain";
    private static final String EMAIL_MESSAGE_MIME_TYPE = "message/rfc822";
    private static final String MEDIA_TYPE_MESSAGE = "message";
    private static final String MEDIA_TYPE_TEXT = "text";
    private static final String SUB_TYPE_EMAIL = "rfc822";
    private static final String US_ASCII = "us-ascii";
    private final FieldParser<? extends ParsedField> fieldParser;
    private final Map<String, ParsedField> fields;
    private final DecodeMonitor monitor;
    private final String parentMimeType;

    public DefaultBodyDescriptorBuilder() {
        this(null);
    }

    public DefaultBodyDescriptorBuilder(String parentMimeType2) {
        this(parentMimeType2, null, null);
    }

    public DefaultBodyDescriptorBuilder(String parentMimeType2, FieldParser<? extends ParsedField> fieldParser2, DecodeMonitor monitor2) {
        this.parentMimeType = parentMimeType2;
        this.fieldParser = fieldParser2 == null ? DefaultFieldParser.getParser() : fieldParser2;
        this.monitor = monitor2 == null ? DecodeMonitor.SILENT : monitor2;
        this.fields = new HashMap();
    }

    public void reset() {
        this.fields.clear();
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [org.apache.james.mime4j.dom.field.ParsedField, java.lang.Object, org.apache.james.mime4j.stream.Field] */
    public Field addField(RawField rawfield) throws MimeException {
        ? parse = this.fieldParser.parse(rawfield, this.monitor);
        String name = parse.getName().toLowerCase(Locale.US);
        if (!this.fields.containsKey(name)) {
            this.fields.put(name, parse);
        }
        return parse;
    }

    public BodyDescriptor build() {
        String actualMimeType = null;
        String actualMediaType = null;
        String actualSubType = null;
        String actualCharset = null;
        String actualBoundary = null;
        ContentTypeField contentTypeField = (ContentTypeField) this.fields.get(CONTENT_TYPE);
        if (contentTypeField != null) {
            actualMimeType = contentTypeField.getMimeType();
            actualMediaType = contentTypeField.getMediaType();
            actualSubType = contentTypeField.getSubType();
            actualCharset = contentTypeField.getCharset();
            actualBoundary = contentTypeField.getBoundary();
        }
        if (actualMimeType == null) {
            if (MimeUtil.isSameMimeType(ContentTypeField.TYPE_MULTIPART_DIGEST, this.parentMimeType)) {
                actualMimeType = "message/rfc822";
                actualMediaType = MEDIA_TYPE_MESSAGE;
                actualSubType = SUB_TYPE_EMAIL;
            } else {
                actualMimeType = "text/plain";
                actualMediaType = "text";
                actualSubType = DEFAULT_SUB_TYPE;
            }
        }
        if (actualCharset == null && "text".equals(actualMediaType)) {
            actualCharset = US_ASCII;
        }
        if (!MimeUtil.isMultipart(actualMimeType)) {
            actualBoundary = null;
        }
        return new MaximalBodyDescriptor(actualMimeType, actualMediaType, actualSubType, actualBoundary, actualCharset, this.fields);
    }

    public BodyDescriptorBuilder newChild() {
        String actualMimeType;
        ContentTypeField contentTypeField = (ContentTypeField) this.fields.get(CONTENT_TYPE);
        if (contentTypeField != null) {
            actualMimeType = contentTypeField.getMimeType();
        } else if (MimeUtil.isSameMimeType(ContentTypeField.TYPE_MULTIPART_DIGEST, this.parentMimeType)) {
            actualMimeType = "message/rfc822";
        } else {
            actualMimeType = "text/plain";
        }
        return new DefaultBodyDescriptorBuilder(actualMimeType, this.fieldParser, this.monitor);
    }
}
