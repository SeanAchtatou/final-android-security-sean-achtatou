package org.apache.james.mime4j.dom;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.james.mime4j.stream.Field;

public interface MessageWriter {
    void writeBody(Body body, OutputStream outputStream) throws IOException;

    void writeEntity(Entity entity, OutputStream outputStream) throws IOException;

    void writeField(Field field, OutputStream outputStream) throws IOException;

    void writeHeader(Header header, OutputStream outputStream) throws IOException;

    void writeMessage(Message message, OutputStream outputStream) throws IOException;

    void writeMultipart(Multipart multipart, OutputStream outputStream) throws IOException;
}
