package org.apache.james.mime4j.message;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.TimeZone;
import org.apache.james.mime4j.dom.Header;
import org.apache.james.mime4j.dom.Message;
import org.apache.james.mime4j.dom.address.Address;
import org.apache.james.mime4j.dom.address.AddressList;
import org.apache.james.mime4j.dom.address.Mailbox;
import org.apache.james.mime4j.dom.address.MailboxList;
import org.apache.james.mime4j.dom.field.AddressListField;
import org.apache.james.mime4j.dom.field.DateTimeField;
import org.apache.james.mime4j.dom.field.FieldName;
import org.apache.james.mime4j.dom.field.MailboxField;
import org.apache.james.mime4j.dom.field.MailboxListField;
import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.dom.field.UnstructuredField;
import org.apache.james.mime4j.stream.Field;

public abstract class AbstractMessage extends AbstractEntity implements Message {
    /* access modifiers changed from: protected */
    public abstract AddressListField newAddressList(String str, Collection<? extends Address> collection);

    /* access modifiers changed from: protected */
    public abstract DateTimeField newDate(Date date, TimeZone timeZone);

    /* access modifiers changed from: protected */
    public abstract MailboxField newMailbox(String str, Mailbox mailbox);

    /* access modifiers changed from: protected */
    public abstract MailboxListField newMailboxList(String str, Collection<Mailbox> collection);

    /* access modifiers changed from: protected */
    public abstract ParsedField newMessageId(String str);

    /* access modifiers changed from: protected */
    public abstract UnstructuredField newSubject(String str);

    public String getMessageId() {
        Field field = obtainField(FieldName.MESSAGE_ID);
        if (field == null) {
            return null;
        }
        return field.getBody();
    }

    public void createMessageId(String hostname) {
        obtainHeader().setField(newMessageId(hostname));
    }

    public String getSubject() {
        UnstructuredField field = (UnstructuredField) obtainField(FieldName.SUBJECT);
        if (field == null) {
            return null;
        }
        return field.getValue();
    }

    public void setSubject(String subject) {
        Header header = obtainHeader();
        if (subject == null) {
            header.removeFields(FieldName.SUBJECT);
        } else {
            header.setField(newSubject(subject));
        }
    }

    public Date getDate() {
        DateTimeField dateField = (DateTimeField) obtainField(FieldName.DATE);
        if (dateField == null) {
            return null;
        }
        return dateField.getDate();
    }

    public void setDate(Date date) {
        setDate(date, null);
    }

    public void setDate(Date date, TimeZone zone) {
        Header header = obtainHeader();
        if (date == null) {
            header.removeFields(FieldName.DATE);
        } else {
            header.setField(newDate(date, zone));
        }
    }

    public Mailbox getSender() {
        return getMailbox(FieldName.SENDER);
    }

    public void setSender(Mailbox sender) {
        setMailbox(FieldName.SENDER, sender);
    }

    public MailboxList getFrom() {
        return getMailboxList(FieldName.FROM);
    }

    public void setFrom(Mailbox from) {
        setMailboxList(FieldName.FROM, from);
    }

    public void setFrom(Mailbox... from) {
        setMailboxList(FieldName.FROM, from);
    }

    public void setFrom(Collection<Mailbox> from) {
        setMailboxList(FieldName.FROM, from);
    }

    public AddressList getTo() {
        return getAddressList(FieldName.TO);
    }

    public void setTo(Address to) {
        setAddressList(FieldName.TO, to);
    }

    public void setTo(Address... to) {
        setAddressList(FieldName.TO, to);
    }

    public void setTo(Collection<? extends Address> to) {
        setAddressList(FieldName.TO, to);
    }

    public AddressList getCc() {
        return getAddressList(FieldName.CC);
    }

    public void setCc(Address cc) {
        setAddressList(FieldName.CC, cc);
    }

    public void setCc(Address... cc) {
        setAddressList(FieldName.CC, cc);
    }

    public void setCc(Collection<? extends Address> cc) {
        setAddressList(FieldName.CC, cc);
    }

    public AddressList getBcc() {
        return getAddressList(FieldName.BCC);
    }

    public void setBcc(Address bcc) {
        setAddressList(FieldName.BCC, bcc);
    }

    public void setBcc(Address... bcc) {
        setAddressList(FieldName.BCC, bcc);
    }

    public void setBcc(Collection<? extends Address> bcc) {
        setAddressList(FieldName.BCC, bcc);
    }

    public AddressList getReplyTo() {
        return getAddressList(FieldName.REPLY_TO);
    }

    public void setReplyTo(Address replyTo) {
        setAddressList(FieldName.REPLY_TO, replyTo);
    }

    public void setReplyTo(Address... replyTo) {
        setAddressList(FieldName.REPLY_TO, replyTo);
    }

    public void setReplyTo(Collection<? extends Address> replyTo) {
        setAddressList(FieldName.REPLY_TO, replyTo);
    }

    private Mailbox getMailbox(String fieldName) {
        MailboxField field = (MailboxField) obtainField(fieldName);
        if (field == null) {
            return null;
        }
        return field.getMailbox();
    }

    private void setMailbox(String fieldName, Mailbox mailbox) {
        Header header = obtainHeader();
        if (mailbox == null) {
            header.removeFields(fieldName);
        } else {
            header.setField(newMailbox(fieldName, mailbox));
        }
    }

    private MailboxList getMailboxList(String fieldName) {
        MailboxListField field = (MailboxListField) obtainField(fieldName);
        if (field == null) {
            return null;
        }
        return field.getMailboxList();
    }

    private void setMailboxList(String fieldName, Mailbox mailbox) {
        setMailboxList(fieldName, mailbox == null ? null : Collections.singleton(mailbox));
    }

    private void setMailboxList(String fieldName, Mailbox... mailboxes) {
        setMailboxList(fieldName, mailboxes == null ? null : Arrays.asList(mailboxes));
    }

    private void setMailboxList(String fieldName, Collection<Mailbox> mailboxes) {
        Header header = obtainHeader();
        if (mailboxes == null || mailboxes.isEmpty()) {
            header.removeFields(fieldName);
        } else {
            header.setField(newMailboxList(fieldName, mailboxes));
        }
    }

    private AddressList getAddressList(String fieldName) {
        AddressListField field = (AddressListField) obtainField(fieldName);
        if (field == null) {
            return null;
        }
        return field.getAddressList();
    }

    private void setAddressList(String fieldName, Address address) {
        setAddressList(fieldName, address == null ? null : Collections.singleton(address));
    }

    private void setAddressList(String fieldName, Address... addresses) {
        setAddressList(fieldName, addresses == null ? null : Arrays.asList(addresses));
    }

    private void setAddressList(String fieldName, Collection<? extends Address> addresses) {
        Header header = obtainHeader();
        if (addresses == null || addresses.isEmpty()) {
            header.removeFields(fieldName);
        } else {
            header.setField(newAddressList(fieldName, addresses));
        }
    }
}
