package org.apache.james.mime4j.stream;

import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.util.CharsetUtil;
import org.apache.james.mime4j.util.ContentUtil;
import org.apache.james.mime4j.util.MimeUtil;

public final class RawField implements Field {
    private final String body;
    private final int delimiterIdx;
    private final String name;
    private final ByteSequence raw;

    RawField(ByteSequence raw2, int delimiterIdx2, String name2, String body2) {
        if (name2 == null) {
            throw new IllegalArgumentException("Field may not be null");
        }
        this.raw = raw2;
        this.delimiterIdx = delimiterIdx2;
        this.name = name2.trim();
        this.body = body2;
    }

    public RawField(String name2, String body2) {
        this(null, -1, name2, body2);
    }

    public ByteSequence getRaw() {
        return this.raw;
    }

    public String getName() {
        return this.name;
    }

    public String getBody() {
        if (this.body != null) {
            return this.body;
        }
        if (this.raw == null) {
            return null;
        }
        int len = this.raw.length();
        int off = this.delimiterIdx + 1;
        if (len > off + 1 && CharsetUtil.isWhitespace((char) (this.raw.byteAt(off) & 255))) {
            off++;
        }
        return MimeUtil.unfold(ContentUtil.decode(this.raw, off, len - off));
    }

    public int getDelimiterIdx() {
        return this.delimiterIdx;
    }

    public String toString() {
        if (this.raw != null) {
            return ContentUtil.decode(this.raw);
        }
        StringBuilder buf = new StringBuilder();
        buf.append(this.name);
        buf.append(": ");
        if (this.body != null) {
            buf.append(this.body);
        }
        return buf.toString();
    }
}
