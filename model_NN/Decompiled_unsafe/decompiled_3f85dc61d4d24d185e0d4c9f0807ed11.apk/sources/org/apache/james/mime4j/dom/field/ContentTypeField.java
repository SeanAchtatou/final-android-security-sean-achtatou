package org.apache.james.mime4j.dom.field;

import java.util.Map;

public interface ContentTypeField extends ParsedField {
    public static final String PARAM_BOUNDARY = "boundary";
    public static final String PARAM_CHARSET = "charset";
    public static final String TYPE_MESSAGE_RFC822 = "message/rfc822";
    public static final String TYPE_MULTIPART_DIGEST = "multipart/digest";
    public static final String TYPE_MULTIPART_PREFIX = "multipart/";
    public static final String TYPE_TEXT_PLAIN = "text/plain";

    String getBoundary();

    String getCharset();

    String getMediaType();

    String getMimeType();

    String getParameter(String str);

    Map<String, String> getParameters();

    String getSubType();

    boolean isMimeType(String str);

    boolean isMultipart();
}
