package org.apache.james.mime4j.message;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.james.mime4j.dom.Body;
import org.apache.james.mime4j.dom.Entity;
import org.apache.james.mime4j.dom.Header;
import org.apache.james.mime4j.dom.Message;
import org.apache.james.mime4j.dom.Multipart;
import org.apache.james.mime4j.dom.TextBody;
import org.apache.james.mime4j.dom.field.ContentDispositionField;
import org.apache.james.mime4j.dom.field.ContentTransferEncodingField;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import org.apache.james.mime4j.dom.field.ParsedField;

public abstract class AbstractEntity implements Entity {
    private Body body = null;
    private Header header = null;
    private Entity parent = null;

    /* access modifiers changed from: protected */
    public abstract String calcCharset(ContentTypeField contentTypeField);

    /* access modifiers changed from: protected */
    public abstract String calcMimeType(ContentTypeField contentTypeField, ContentTypeField contentTypeField2);

    /* access modifiers changed from: protected */
    public abstract String calcTransferEncoding(ContentTransferEncodingField contentTransferEncodingField);

    /* access modifiers changed from: protected */
    public abstract ContentDispositionField newContentDisposition(String str, String str2, long j, Date date, Date date2, Date date3);

    /* access modifiers changed from: protected */
    public abstract ContentDispositionField newContentDisposition(String str, Map<String, String> map);

    /* access modifiers changed from: protected */
    public abstract ContentTransferEncodingField newContentTransferEncoding(String str);

    /* access modifiers changed from: protected */
    public abstract ContentTypeField newContentType(String str, Map<String, String> map);

    /* access modifiers changed from: protected */
    public abstract String newUniqueBoundary();

    protected AbstractEntity() {
    }

    public Entity getParent() {
        return this.parent;
    }

    public void setParent(Entity parent2) {
        this.parent = parent2;
    }

    public Header getHeader() {
        return this.header;
    }

    public void setHeader(Header header2) {
        this.header = header2;
    }

    public Body getBody() {
        return this.body;
    }

    public void setBody(Body body2) {
        if (this.body != null) {
            throw new IllegalStateException("body already set");
        }
        this.body = body2;
        body2.setParent(this);
    }

    public Body removeBody() {
        if (this.body == null) {
            return null;
        }
        Body body2 = this.body;
        this.body = null;
        body2.setParent(null);
        return body2;
    }

    public void setMessage(Message message) {
        setBody(message, ContentTypeField.TYPE_MESSAGE_RFC822, null);
    }

    public void setMultipart(Multipart multipart) {
        setBody(multipart, ContentTypeField.TYPE_MULTIPART_PREFIX + multipart.getSubType(), Collections.singletonMap(ContentTypeField.PARAM_BOUNDARY, newUniqueBoundary()));
    }

    public void setMultipart(Multipart multipart, Map<String, String> parameters) {
        String mimeType = ContentTypeField.TYPE_MULTIPART_PREFIX + multipart.getSubType();
        if (!parameters.containsKey(ContentTypeField.PARAM_BOUNDARY)) {
            Map<String, String> parameters2 = new HashMap<>(parameters);
            parameters2.put(ContentTypeField.PARAM_BOUNDARY, newUniqueBoundary());
            parameters = parameters2;
        }
        setBody(multipart, mimeType, parameters);
    }

    public void setText(TextBody textBody) {
        setText(textBody, "plain");
    }

    public void setText(TextBody textBody, String subtype) {
        String mimeType = "text/" + subtype;
        Map<String, String> parameters = null;
        String mimeCharset = textBody.getMimeCharset();
        if (mimeCharset != null && !mimeCharset.equalsIgnoreCase("us-ascii")) {
            parameters = Collections.singletonMap("charset", mimeCharset);
        }
        setBody(textBody, mimeType, parameters);
    }

    public void setBody(Body body2, String mimeType) {
        setBody(body2, mimeType, null);
    }

    public void setBody(Body body2, String mimeType, Map<String, String> parameters) {
        setBody(body2);
        obtainHeader().setField(newContentType(mimeType, parameters));
    }

    public String getMimeType() {
        return calcMimeType(getContentTypeField(), getParent() != null ? (ContentTypeField) getParent().getHeader().getField("Content-Type") : null);
    }

    private ContentTypeField getContentTypeField() {
        return (ContentTypeField) getHeader().getField("Content-Type");
    }

    public String getCharset() {
        return calcCharset((ContentTypeField) getHeader().getField("Content-Type"));
    }

    public String getContentTransferEncoding() {
        return calcTransferEncoding((ContentTransferEncodingField) getHeader().getField("Content-Transfer-Encoding"));
    }

    public void setContentTransferEncoding(String contentTransferEncoding) {
        obtainHeader().setField(newContentTransferEncoding(contentTransferEncoding));
    }

    public String getDispositionType() {
        ContentDispositionField field = (ContentDispositionField) obtainField("Content-Disposition");
        if (field == null) {
            return null;
        }
        return field.getDispositionType();
    }

    public void setContentDisposition(String dispositionType) {
        obtainHeader().setField(newContentDisposition(dispositionType, null, -1, null, null, null));
    }

    public void setContentDisposition(String dispositionType, String filename) {
        obtainHeader().setField(newContentDisposition(dispositionType, filename, -1, null, null, null));
    }

    public void setContentDisposition(String dispositionType, String filename, long size) {
        obtainHeader().setField(newContentDisposition(dispositionType, filename, size, null, null, null));
    }

    public void setContentDisposition(String dispositionType, String filename, long size, Date creationDate, Date modificationDate, Date readDate) {
        obtainHeader().setField(newContentDisposition(dispositionType, filename, size, creationDate, modificationDate, readDate));
    }

    public String getFilename() {
        ContentDispositionField field = (ContentDispositionField) obtainField("Content-Disposition");
        if (field == null) {
            return null;
        }
        return field.getFilename();
    }

    public void setFilename(String filename) {
        Header header2 = obtainHeader();
        ContentDispositionField field = (ContentDispositionField) header2.getField("Content-Disposition");
        if (field != null) {
            String dispositionType = field.getDispositionType();
            Map<String, String> parameters = new HashMap<>(field.getParameters());
            if (filename == null) {
                parameters.remove(ContentDispositionField.PARAM_FILENAME);
            } else {
                parameters.put(ContentDispositionField.PARAM_FILENAME, filename);
            }
            header2.setField(newContentDisposition(dispositionType, parameters));
        } else if (filename != null) {
            header2.setField(newContentDisposition(ContentDispositionField.DISPOSITION_TYPE_ATTACHMENT, filename, -1, null, null, null));
        }
    }

    public boolean isMimeType(String type) {
        return getMimeType().equalsIgnoreCase(type);
    }

    public boolean isMultipart() {
        ContentTypeField f = getContentTypeField();
        return (f == null || f.getBoundary() == null || !getMimeType().startsWith(ContentTypeField.TYPE_MULTIPART_PREFIX)) ? false : true;
    }

    public void dispose() {
        if (this.body != null) {
            this.body.dispose();
        }
    }

    /* access modifiers changed from: package-private */
    public Header obtainHeader() {
        if (this.header == null) {
            this.header = new HeaderImpl();
        }
        return this.header;
    }

    /* access modifiers changed from: package-private */
    public <F extends ParsedField> F obtainField(String fieldName) {
        Header header2 = getHeader();
        if (header2 == null) {
            return null;
        }
        return (ParsedField) header2.getField(fieldName);
    }
}
