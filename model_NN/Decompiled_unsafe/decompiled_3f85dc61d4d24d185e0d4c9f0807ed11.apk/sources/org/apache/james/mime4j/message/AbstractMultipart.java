package org.apache.james.mime4j.message;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.apache.james.mime4j.dom.Entity;
import org.apache.james.mime4j.dom.Multipart;

public abstract class AbstractMultipart implements Multipart {
    protected List<Entity> bodyParts = new LinkedList();
    private Entity parent = null;
    private String subType;

    public abstract String getEpilogue();

    public abstract String getPreamble();

    public abstract void setEpilogue(String str);

    public abstract void setPreamble(String str);

    public AbstractMultipart(String subType2) {
        this.subType = subType2;
    }

    public String getSubType() {
        return this.subType;
    }

    public void setSubType(String subType2) {
        this.subType = subType2;
    }

    public Entity getParent() {
        return this.parent;
    }

    public void setParent(Entity parent2) {
        this.parent = parent2;
        for (Entity bodyPart : this.bodyParts) {
            bodyPart.setParent(parent2);
        }
    }

    public int getCount() {
        return this.bodyParts.size();
    }

    public List<Entity> getBodyParts() {
        return Collections.unmodifiableList(this.bodyParts);
    }

    public void setBodyParts(List<Entity> bodyParts2) {
        this.bodyParts = bodyParts2;
        for (Entity bodyPart : bodyParts2) {
            bodyPart.setParent(this.parent);
        }
    }

    public void addBodyPart(Entity bodyPart) {
        if (bodyPart == null) {
            throw new IllegalArgumentException();
        }
        this.bodyParts.add(bodyPart);
        bodyPart.setParent(this.parent);
    }

    public void addBodyPart(Entity bodyPart, int index) {
        if (bodyPart == null) {
            throw new IllegalArgumentException();
        }
        this.bodyParts.add(index, bodyPart);
        bodyPart.setParent(this.parent);
    }

    public Entity removeBodyPart(int index) {
        Entity bodyPart = this.bodyParts.remove(index);
        bodyPart.setParent(null);
        return bodyPart;
    }

    public Entity replaceBodyPart(Entity bodyPart, int index) {
        if (bodyPart == null) {
            throw new IllegalArgumentException();
        }
        Entity replacedEntity = this.bodyParts.set(index, bodyPart);
        if (bodyPart == replacedEntity) {
            throw new IllegalArgumentException("Cannot replace body part with itself");
        }
        bodyPart.setParent(this.parent);
        replacedEntity.setParent(null);
        return replacedEntity;
    }

    public void dispose() {
        for (Entity bodyPart : this.bodyParts) {
            bodyPart.dispose();
        }
    }
}
