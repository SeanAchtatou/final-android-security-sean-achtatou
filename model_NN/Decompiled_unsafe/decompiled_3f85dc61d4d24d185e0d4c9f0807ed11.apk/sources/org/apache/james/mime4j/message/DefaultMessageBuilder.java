package org.apache.james.mime4j.message;

import java.io.IOException;
import java.io.InputStream;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.MimeIOException;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.Body;
import org.apache.james.mime4j.dom.Entity;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.Header;
import org.apache.james.mime4j.dom.Message;
import org.apache.james.mime4j.dom.MessageBuilder;
import org.apache.james.mime4j.dom.Multipart;
import org.apache.james.mime4j.dom.SingleBody;
import org.apache.james.mime4j.dom.field.ParsedField;
import org.apache.james.mime4j.field.DefaultFieldParser;
import org.apache.james.mime4j.field.LenientFieldParser;
import org.apache.james.mime4j.parser.AbstractContentHandler;
import org.apache.james.mime4j.parser.MimeStreamParser;
import org.apache.james.mime4j.stream.BodyDescriptorBuilder;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.stream.MimeConfig;

public class DefaultMessageBuilder implements MessageBuilder {
    private BodyDescriptorBuilder bodyDescBuilder = null;
    private BodyFactory bodyFactory = null;
    private MimeConfig config = null;
    private boolean contentDecoding = true;
    private FieldParser<? extends ParsedField> fieldParser = null;
    private boolean flatMode = false;
    private DecodeMonitor monitor = null;

    public void setFieldParser(FieldParser<? extends ParsedField> fieldParser2) {
        this.fieldParser = fieldParser2;
    }

    public void setBodyFactory(BodyFactory bodyFactory2) {
        this.bodyFactory = bodyFactory2;
    }

    public void setMimeEntityConfig(MimeConfig config2) {
        this.config = config2;
    }

    public void setBodyDescriptorBuilder(BodyDescriptorBuilder bodyDescBuilder2) {
        this.bodyDescBuilder = bodyDescBuilder2;
    }

    public void setDecodeMonitor(DecodeMonitor monitor2) {
        this.monitor = monitor2;
    }

    public void setContentDecoding(boolean contentDecoding2) {
        this.contentDecoding = contentDecoding2;
    }

    public void setFlatMode(boolean flatMode2) {
        this.flatMode = flatMode2;
    }

    public Header copy(Header other) {
        HeaderImpl copy = new HeaderImpl();
        for (Field otherField : other.getFields()) {
            copy.addField(otherField);
        }
        return copy;
    }

    public BodyPart copy(Entity other) {
        BodyPart copy = new BodyPart();
        if (other.getHeader() != null) {
            copy.setHeader(copy(other.getHeader()));
        }
        if (other.getBody() != null) {
            copy.setBody(copy(other.getBody()));
        }
        return copy;
    }

    public Multipart copy(Multipart other) {
        MultipartImpl copy = new MultipartImpl(other.getSubType());
        for (Entity otherBodyPart : other.getBodyParts()) {
            copy.addBodyPart(copy(otherBodyPart));
        }
        copy.setPreamble(other.getPreamble());
        copy.setEpilogue(other.getEpilogue());
        return copy;
    }

    public Body copy(Body body) {
        if (body == null) {
            throw new IllegalArgumentException("Body is null");
        } else if (body instanceof Message) {
            return copy((Message) body);
        } else {
            if (body instanceof Multipart) {
                return copy((Multipart) body);
            }
            if (body instanceof SingleBody) {
                return ((SingleBody) body).copy();
            }
            throw new IllegalArgumentException("Unsupported body class");
        }
    }

    public Message copy(Message other) {
        MessageImpl copy = new MessageImpl();
        if (other.getHeader() != null) {
            copy.setHeader(copy(other.getHeader()));
        }
        if (other.getBody() != null) {
            copy.setBody(copy(other.getBody()));
        }
        return copy;
    }

    public Header newHeader() {
        return new HeaderImpl();
    }

    public Header newHeader(Header source) {
        return copy(source);
    }

    public Multipart newMultipart(String subType) {
        return new MultipartImpl(subType);
    }

    public Multipart newMultipart(Multipart source) {
        return copy(source);
    }

    public Header parseHeader(InputStream is) throws IOException, MimeIOException {
        final FieldParser<? extends ParsedField> fp;
        boolean strict = (this.config != null ? this.config : new MimeConfig()).isStrictParsing();
        final DecodeMonitor mon = this.monitor != null ? this.monitor : strict ? DecodeMonitor.STRICT : DecodeMonitor.SILENT;
        if (this.fieldParser != null) {
            fp = this.fieldParser;
        } else {
            fp = strict ? DefaultFieldParser.getParser() : LenientFieldParser.getParser();
        }
        final HeaderImpl header = new HeaderImpl();
        final MimeStreamParser parser = new MimeStreamParser();
        parser.setContentHandler(new AbstractContentHandler() {
            public void endHeader() {
                parser.stop();
            }

            public void field(Field field) throws MimeException {
                ParsedField parsedField;
                if (field instanceof ParsedField) {
                    parsedField = (ParsedField) field;
                } else {
                    parsedField = fp.parse(field, mon);
                }
                header.addField(parsedField);
            }
        });
        try {
            parser.parse(is);
            return header;
        } catch (MimeException ex) {
            throw new MimeIOException(ex);
        }
    }

    public Message newMessage() {
        return new MessageImpl();
    }

    public Message newMessage(Message source) {
        return copy(source);
    }

    public Message parseMessage(InputStream is) throws IOException, MimeIOException {
        MimeConfig cfg;
        BodyDescriptorBuilder bdb;
        try {
            MessageImpl message = new MessageImpl();
            if (this.config != null) {
                cfg = this.config;
            } else {
                cfg = new MimeConfig();
            }
            boolean strict = cfg.isStrictParsing();
            DecodeMonitor mon = this.monitor != null ? this.monitor : strict ? DecodeMonitor.STRICT : DecodeMonitor.SILENT;
            if (this.bodyDescBuilder != null) {
                bdb = this.bodyDescBuilder;
            } else {
                bdb = new DefaultBodyDescriptorBuilder(null, this.fieldParser != null ? this.fieldParser : strict ? DefaultFieldParser.getParser() : LenientFieldParser.getParser(), mon);
            }
            BodyFactory bf = this.bodyFactory != null ? this.bodyFactory : new BasicBodyFactory();
            MimeStreamParser parser = new MimeStreamParser(cfg, mon, bdb);
            parser.setContentHandler(new EntityBuilder(message, bf));
            parser.setContentDecoding(this.contentDecoding);
            if (this.flatMode) {
                parser.setFlat();
            } else {
                parser.setRecurse();
            }
            parser.parse(is);
            return message;
        } catch (MimeException e) {
            throw new MimeIOException(e);
        }
    }
}
