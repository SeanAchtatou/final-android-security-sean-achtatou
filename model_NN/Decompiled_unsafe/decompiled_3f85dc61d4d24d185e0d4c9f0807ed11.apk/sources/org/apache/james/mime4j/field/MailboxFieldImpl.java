package org.apache.james.mime4j.field;

import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.address.Mailbox;
import org.apache.james.mime4j.dom.field.MailboxField;
import org.apache.james.mime4j.field.address.AddressBuilder;
import org.apache.james.mime4j.field.address.ParseException;
import org.apache.james.mime4j.stream.Field;

public class MailboxFieldImpl extends AbstractField implements MailboxField {
    public static final FieldParser<MailboxField> PARSER = new FieldParser<MailboxField>() {
        public MailboxField parse(Field rawField, DecodeMonitor monitor) {
            return new MailboxFieldImpl(rawField, monitor);
        }
    };
    private Mailbox mailbox;
    private ParseException parseException;
    private boolean parsed = false;

    MailboxFieldImpl(Field rawField, DecodeMonitor monitor) {
        super(rawField, monitor);
    }

    public Mailbox getMailbox() {
        if (!this.parsed) {
            parse();
        }
        return this.mailbox;
    }

    public ParseException getParseException() {
        if (!this.parsed) {
            parse();
        }
        return this.parseException;
    }

    private void parse() {
        try {
            this.mailbox = AddressBuilder.DEFAULT.parseMailbox(getBody(), this.monitor);
        } catch (ParseException e) {
            this.parseException = e;
        }
        this.parsed = true;
    }
}
