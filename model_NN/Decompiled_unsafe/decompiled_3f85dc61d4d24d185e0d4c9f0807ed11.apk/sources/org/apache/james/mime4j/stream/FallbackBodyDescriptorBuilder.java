package org.apache.james.mime4j.stream;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.field.ContentTypeField;
import org.apache.james.mime4j.util.MimeUtil;

class FallbackBodyDescriptorBuilder implements BodyDescriptorBuilder {
    private static final String DEFAULT_MEDIA_TYPE = "text";
    private static final String DEFAULT_MIME_TYPE = "text/plain";
    private static final String DEFAULT_SUB_TYPE = "plain";
    private static final String EMAIL_MESSAGE_MIME_TYPE = "message/rfc822";
    private static final String MEDIA_TYPE_MESSAGE = "message";
    private static final String MEDIA_TYPE_TEXT = "text";
    private static final String SUB_TYPE_EMAIL = "rfc822";
    private static final String US_ASCII = "us-ascii";
    private String boundary;
    private String charset;
    private long contentLength;
    private String mediaType;
    private String mimeType;
    private final DecodeMonitor monitor;
    private final String parentMimeType;
    private String subType;
    private String transferEncoding;

    public FallbackBodyDescriptorBuilder() {
        this(null, null);
    }

    public FallbackBodyDescriptorBuilder(String parentMimeType2, DecodeMonitor monitor2) {
        this.parentMimeType = parentMimeType2;
        this.monitor = monitor2 == null ? DecodeMonitor.SILENT : monitor2;
        reset();
    }

    public void reset() {
        this.mimeType = null;
        this.subType = null;
        this.mediaType = null;
        this.boundary = null;
        this.charset = null;
        this.transferEncoding = null;
        this.contentLength = -1;
    }

    public BodyDescriptorBuilder newChild() {
        return new FallbackBodyDescriptorBuilder(this.mimeType, this.monitor);
    }

    public BodyDescriptor build() {
        String str;
        String actualMimeType = this.mimeType;
        String actualMediaType = this.mediaType;
        String actualSubType = this.subType;
        String actualCharset = this.charset;
        if (actualMimeType == null) {
            if (MimeUtil.isSameMimeType(ContentTypeField.TYPE_MULTIPART_DIGEST, this.parentMimeType)) {
                actualMimeType = "message/rfc822";
                actualMediaType = MEDIA_TYPE_MESSAGE;
                actualSubType = SUB_TYPE_EMAIL;
            } else {
                actualMimeType = "text/plain";
                actualMediaType = "text";
                actualSubType = DEFAULT_SUB_TYPE;
            }
        }
        if (actualCharset == null && "text".equals(actualMediaType)) {
            actualCharset = US_ASCII;
        }
        String str2 = this.boundary;
        if (this.transferEncoding != null) {
            str = this.transferEncoding;
        } else {
            str = MimeUtil.ENC_7BIT;
        }
        return new BasicBodyDescriptor(actualMimeType, actualMediaType, actualSubType, str2, actualCharset, str, this.contentLength);
    }

    public Field addField(RawField field) throws MimeException {
        String name = field.getName().toLowerCase(Locale.US);
        if (name.equals("content-transfer-encoding") && this.transferEncoding == null) {
            String value = field.getBody();
            if (value == null) {
                return null;
            }
            String value2 = value.trim().toLowerCase(Locale.US);
            if (value2.length() <= 0) {
                return null;
            }
            this.transferEncoding = value2;
            return null;
        } else if (name.equals("content-length") && this.contentLength == -1) {
            String value3 = field.getBody();
            if (value3 == null) {
                return null;
            }
            String value4 = value3.trim();
            try {
                this.contentLength = Long.parseLong(value4.trim());
                return null;
            } catch (NumberFormatException e) {
                if (!this.monitor.warn("Invalid content length: " + value4, "ignoring Content-Length header")) {
                    return null;
                }
                throw new MimeException("Invalid Content-Length header: " + value4);
            }
        } else if (!name.equals("content-type") || this.mimeType != null) {
            return null;
        } else {
            parseContentType(field);
            return null;
        }
    }

    private void parseContentType(Field field) throws MimeException {
        RawField rawfield;
        if (field instanceof RawField) {
            rawfield = (RawField) field;
        } else {
            rawfield = new RawField(field.getName(), field.getBody());
        }
        RawBody body = RawFieldParser.DEFAULT.parseRawBody(rawfield);
        String main = body.getValue();
        Map<String, String> params = new HashMap<>();
        for (NameValuePair nmp : body.getParams()) {
            params.put(nmp.getName().toLowerCase(Locale.US), nmp.getValue());
        }
        String type = null;
        String subtype = null;
        if (main != null) {
            main = main.toLowerCase().trim();
            int index = main.indexOf(47);
            boolean valid = false;
            if (index != -1) {
                type = main.substring(0, index).trim();
                subtype = main.substring(index + 1).trim();
                if (type.length() > 0 && subtype.length() > 0) {
                    main = type + "/" + subtype;
                    valid = true;
                }
            }
            if (!valid) {
                main = null;
                type = null;
                subtype = null;
            }
        }
        String b = (String) params.get(ContentTypeField.PARAM_BOUNDARY);
        if (main != null && ((main.startsWith(ContentTypeField.TYPE_MULTIPART_PREFIX) && b != null) || !main.startsWith(ContentTypeField.TYPE_MULTIPART_PREFIX))) {
            this.mimeType = main;
            this.mediaType = type;
            this.subType = subtype;
        }
        if (MimeUtil.isMultipart(this.mimeType)) {
            this.boundary = b;
        }
        String c = (String) params.get("charset");
        this.charset = null;
        if (c != null) {
            String c2 = c.trim();
            if (c2.length() > 0) {
                this.charset = c2;
            }
        }
    }
}
