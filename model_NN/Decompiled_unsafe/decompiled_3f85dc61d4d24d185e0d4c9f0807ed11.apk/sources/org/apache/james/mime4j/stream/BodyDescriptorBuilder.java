package org.apache.james.mime4j.stream;

import org.apache.james.mime4j.MimeException;

public interface BodyDescriptorBuilder {
    Field addField(RawField rawField) throws MimeException;

    BodyDescriptor build();

    BodyDescriptorBuilder newChild();

    void reset();
}
