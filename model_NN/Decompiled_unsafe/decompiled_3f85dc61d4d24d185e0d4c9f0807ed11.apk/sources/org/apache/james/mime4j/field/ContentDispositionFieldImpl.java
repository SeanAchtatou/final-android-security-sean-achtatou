package org.apache.james.mime4j.field;

import java.io.StringReader;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.field.ContentDispositionField;
import org.apache.james.mime4j.field.contentdisposition.parser.ContentDispositionParser;
import org.apache.james.mime4j.field.contentdisposition.parser.ParseException;
import org.apache.james.mime4j.field.contentdisposition.parser.TokenMgrError;
import org.apache.james.mime4j.field.datetime.parser.DateTimeParser;
import org.apache.james.mime4j.stream.Field;

public class ContentDispositionFieldImpl extends AbstractField implements ContentDispositionField {
    public static final FieldParser<ContentDispositionField> PARSER = new FieldParser<ContentDispositionField>() {
        public ContentDispositionField parse(Field rawField, DecodeMonitor monitor) {
            return new ContentDispositionFieldImpl(rawField, monitor);
        }
    };
    private Date creationDate;
    private boolean creationDateParsed;
    private String dispositionType = "";
    private Date modificationDate;
    private boolean modificationDateParsed;
    private Map<String, String> parameters = new HashMap();
    private ParseException parseException;
    private boolean parsed = false;
    private Date readDate;
    private boolean readDateParsed;

    ContentDispositionFieldImpl(Field rawField, DecodeMonitor monitor) {
        super(rawField, monitor);
    }

    public ParseException getParseException() {
        if (!this.parsed) {
            parse();
        }
        return this.parseException;
    }

    public String getDispositionType() {
        if (!this.parsed) {
            parse();
        }
        return this.dispositionType;
    }

    public String getParameter(String name) {
        if (!this.parsed) {
            parse();
        }
        return this.parameters.get(name.toLowerCase());
    }

    public Map<String, String> getParameters() {
        if (!this.parsed) {
            parse();
        }
        return Collections.unmodifiableMap(this.parameters);
    }

    public boolean isDispositionType(String dispositionType2) {
        if (!this.parsed) {
            parse();
        }
        return this.dispositionType.equalsIgnoreCase(dispositionType2);
    }

    public boolean isInline() {
        if (!this.parsed) {
            parse();
        }
        return this.dispositionType.equals(ContentDispositionField.DISPOSITION_TYPE_INLINE);
    }

    public boolean isAttachment() {
        if (!this.parsed) {
            parse();
        }
        return this.dispositionType.equals(ContentDispositionField.DISPOSITION_TYPE_ATTACHMENT);
    }

    public String getFilename() {
        return getParameter(ContentDispositionField.PARAM_FILENAME);
    }

    public Date getCreationDate() {
        if (!this.creationDateParsed) {
            this.creationDate = parseDate(ContentDispositionField.PARAM_CREATION_DATE);
            this.creationDateParsed = true;
        }
        return this.creationDate;
    }

    public Date getModificationDate() {
        if (!this.modificationDateParsed) {
            this.modificationDate = parseDate(ContentDispositionField.PARAM_MODIFICATION_DATE);
            this.modificationDateParsed = true;
        }
        return this.modificationDate;
    }

    public Date getReadDate() {
        if (!this.readDateParsed) {
            this.readDate = parseDate(ContentDispositionField.PARAM_READ_DATE);
            this.readDateParsed = true;
        }
        return this.readDate;
    }

    public long getSize() {
        String value = getParameter(ContentDispositionField.PARAM_SIZE);
        if (value == null) {
            return -1;
        }
        try {
            long size = Long.parseLong(value);
            if (size < 0) {
                size = -1;
            }
            return size;
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    private Date parseDate(String paramName) {
        String value = getParameter(paramName);
        if (value == null) {
            this.monitor.warn("Parsing " + paramName + " null", "returning null");
            return null;
        }
        try {
            return new DateTimeParser(new StringReader(value)).parseAll().getDate();
        } catch (org.apache.james.mime4j.field.datetime.parser.ParseException e) {
            if (!this.monitor.isListening()) {
                return null;
            }
            this.monitor.warn(paramName + " parameter is invalid: " + value, paramName + " parameter is ignored");
            return null;
        } catch (TokenMgrError e2) {
            this.monitor.warn(paramName + " parameter is invalid: " + value, paramName + "parameter is ignored");
            return null;
        }
    }

    private void parse() {
        ContentDispositionParser parser = new ContentDispositionParser(new StringReader(getBody()));
        try {
            parser.parseAll();
        } catch (ParseException e) {
            this.parseException = e;
        } catch (TokenMgrError e2) {
            this.parseException = new ParseException(e2.getMessage());
        }
        String dispositionType2 = parser.getDispositionType();
        if (dispositionType2 != null) {
            this.dispositionType = dispositionType2.toLowerCase(Locale.US);
            List<String> paramNames = parser.getParamNames();
            List<String> paramValues = parser.getParamValues();
            if (!(paramNames == null || paramValues == null)) {
                int len = Math.min(paramNames.size(), paramValues.size());
                for (int i = 0; i < len; i++) {
                    this.parameters.put(paramNames.get(i).toLowerCase(Locale.US), paramValues.get(i));
                }
            }
        }
        this.parsed = true;
    }
}
