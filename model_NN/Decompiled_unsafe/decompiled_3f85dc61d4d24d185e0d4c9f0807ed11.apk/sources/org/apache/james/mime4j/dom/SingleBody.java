package org.apache.james.mime4j.dom;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class SingleBody implements Body {
    static final int DEFAULT_ENCODING_BUFFER_SIZE = 1024;
    private Entity parent = null;

    public abstract InputStream getInputStream() throws IOException;

    protected SingleBody() {
    }

    public Entity getParent() {
        return this.parent;
    }

    public void setParent(Entity parent2) {
        this.parent = parent2;
    }

    public void writeTo(OutputStream out) throws IOException {
        if (out == null) {
            throw new IllegalArgumentException();
        }
        InputStream in = getInputStream();
        copy(in, out);
        in.close();
    }

    public SingleBody copy() {
        throw new UnsupportedOperationException();
    }

    public void dispose() {
    }

    private static void copy(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        while (true) {
            int inputLength = in.read(buffer);
            if (-1 != inputLength) {
                out.write(buffer, 0, inputLength);
            } else {
                return;
            }
        }
    }
}
