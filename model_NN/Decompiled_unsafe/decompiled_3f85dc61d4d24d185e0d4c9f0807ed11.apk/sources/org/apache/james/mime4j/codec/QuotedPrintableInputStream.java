package org.apache.james.mime4j.codec;

import java.io.IOException;
import java.io.InputStream;
import org.apache.james.mime4j.util.ByteArrayBuffer;

public class QuotedPrintableInputStream extends InputStream {
    private static final byte CR = 13;
    private static final int DEFAULT_BUFFER_SIZE = 2048;
    private static final byte EQ = 61;
    private static final byte LF = 10;
    private final ByteArrayBuffer blanks;
    private boolean closed;
    private final ByteArrayBuffer decodedBuf;
    private final byte[] encoded;
    private final InputStream in;
    private int limit;
    private final DecodeMonitor monitor;
    private int pos;
    private final byte[] singleByte;

    public QuotedPrintableInputStream(InputStream in2, DecodeMonitor monitor2) {
        this(2048, in2, monitor2);
    }

    protected QuotedPrintableInputStream(int bufsize, InputStream in2, DecodeMonitor monitor2) {
        this.singleByte = new byte[1];
        this.pos = 0;
        this.limit = 0;
        this.in = in2;
        this.encoded = new byte[bufsize];
        this.decodedBuf = new ByteArrayBuffer(512);
        this.blanks = new ByteArrayBuffer(512);
        this.closed = false;
        this.monitor = monitor2;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    protected QuotedPrintableInputStream(int bufsize, InputStream in2, boolean strict) {
        this(bufsize, in2, strict ? DecodeMonitor.STRICT : DecodeMonitor.SILENT);
    }

    public QuotedPrintableInputStream(InputStream in2, boolean strict) {
        this(2048, in2, strict);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.james.mime4j.codec.QuotedPrintableInputStream.<init>(java.io.InputStream, boolean):void
     arg types: [java.io.InputStream, int]
     candidates:
      org.apache.james.mime4j.codec.QuotedPrintableInputStream.<init>(java.io.InputStream, org.apache.james.mime4j.codec.DecodeMonitor):void
      org.apache.james.mime4j.codec.QuotedPrintableInputStream.<init>(java.io.InputStream, boolean):void */
    public QuotedPrintableInputStream(InputStream in2) {
        this(in2, false);
    }

    public void close() throws IOException {
        this.closed = true;
    }

    private int fillBuffer() throws IOException {
        int bytesRead = 0;
        if (this.pos < this.limit) {
            System.arraycopy(this.encoded, this.pos, this.encoded, 0, this.limit - this.pos);
            this.limit -= this.pos;
            this.pos = 0;
        } else {
            this.limit = 0;
            this.pos = 0;
        }
        int capacity = this.encoded.length - this.limit;
        if (capacity > 0 && (bytesRead = this.in.read(this.encoded, this.limit, capacity)) > 0) {
            this.limit += bytesRead;
        }
        return bytesRead;
    }

    private int getnext() {
        if (this.pos >= this.limit) {
            return -1;
        }
        byte b = this.encoded[this.pos];
        this.pos++;
        return b & 255;
    }

    private int peek(int i) {
        if (this.pos + i < this.limit) {
            return this.encoded[this.pos + i] & 255;
        }
        return -1;
    }

    private int transfer(int b, byte[] buffer, int from, int to, boolean keepblanks) throws IOException {
        int index;
        int index2 = from;
        if (!keepblanks || this.blanks.length() <= 0) {
            if (this.blanks.length() > 0 && !keepblanks) {
                StringBuilder sb = new StringBuilder(this.blanks.length() * 3);
                for (int i = 0; i < this.blanks.length(); i++) {
                    sb.append(" " + ((int) this.blanks.byteAt(i)));
                }
                if (this.monitor.warn("ignored blanks", sb.toString())) {
                    throw new IOException("ignored blanks");
                }
            }
            index = index2;
        } else {
            int chunk = Math.min(this.blanks.length(), to - index2);
            System.arraycopy(this.blanks.buffer(), 0, buffer, index2, chunk);
            int index3 = index2 + chunk;
            int remaining = this.blanks.length() - chunk;
            if (remaining > 0) {
                this.decodedBuf.append(this.blanks.buffer(), chunk, remaining);
            }
            this.blanks.clear();
            index = index3;
        }
        if (b != -1) {
            if (index < to) {
                int index4 = index + 1;
                buffer[index] = (byte) b;
                return index4;
            }
            this.decodedBuf.append(b);
        }
        return index;
    }

    private int read0(byte[] buffer, int off, int len) throws IOException {
        boolean eof = false;
        int from = off;
        int to = off + len;
        int index = off;
        if (this.decodedBuf.length() > 0) {
            int chunk = Math.min(this.decodedBuf.length(), to - index);
            System.arraycopy(this.decodedBuf.buffer(), 0, buffer, index, chunk);
            this.decodedBuf.remove(0, chunk);
            index += chunk;
        }
        while (index < to) {
            if (this.limit - this.pos < 3) {
                eof = fillBuffer() == -1;
            }
            if (this.limit - this.pos != 0 || !eof) {
                boolean lastWasCR = false;
                while (true) {
                    if (this.pos >= this.limit || index >= to) {
                        break;
                    }
                    byte[] bArr = this.encoded;
                    int i = this.pos;
                    this.pos = i + 1;
                    int b = bArr[i] & 255;
                    if (!lastWasCR || b == 10) {
                        if (!lastWasCR && b == 10 && this.monitor.warn("Found LF without CR", "Translating to CRLF")) {
                            throw new IOException("Found LF without CR");
                        }
                    } else if (this.monitor.warn("Found CR without LF", "Leaving it as is")) {
                        throw new IOException("Found CR without LF");
                    } else {
                        index = transfer(13, buffer, index, to, false);
                    }
                    if (b == 13) {
                        lastWasCR = true;
                    } else {
                        lastWasCR = false;
                        if (b == 10) {
                            if (this.blanks.length() == 0) {
                                index = transfer(10, buffer, transfer(13, buffer, index, to, false), to, false);
                            } else if (this.blanks.byteAt(0) != 61) {
                                index = transfer(10, buffer, transfer(13, buffer, index, to, false), to, false);
                            }
                            this.blanks.clear();
                        } else if (b == 61) {
                            if (this.limit - this.pos < 2 && !eof) {
                                this.pos = this.pos - 1;
                                break;
                            }
                            int b2 = getnext();
                            if (b2 == 61) {
                                index = transfer(b2, buffer, index, to, true);
                                int bb1 = peek(0);
                                int bb2 = peek(1);
                                if (bb1 == 10 || (bb1 == 13 && bb2 == 10)) {
                                    this.monitor.warn("Unexpected ==EOL encountered", "== 0x" + bb1 + " 0x" + bb2);
                                    this.blanks.append(b2);
                                } else {
                                    this.monitor.warn("Unexpected == encountered", "==");
                                }
                            } else if (Character.isWhitespace((char) b2)) {
                                index = transfer(-1, buffer, index, to, true);
                                if (b2 != 10) {
                                    this.blanks.append(b);
                                    this.blanks.append(b2);
                                }
                            } else {
                                int b3 = getnext();
                                int upper = convert(b2);
                                int lower = convert(b3);
                                if (upper < 0 || lower < 0) {
                                    this.monitor.warn("Malformed encoded value encountered", "leaving =" + ((char) b2) + ((char) b3) + " as is");
                                    index = transfer(b3, buffer, transfer(b2, buffer, transfer(61, buffer, index, to, true), to, false), to, false);
                                } else {
                                    index = transfer((upper << 4) | lower, buffer, index, to, true);
                                }
                            }
                        } else if (Character.isWhitespace(b)) {
                            this.blanks.append(b);
                        } else {
                            index = transfer(b & 255, buffer, index, to, true);
                        }
                    }
                }
            } else if (index == from) {
                return -1;
            } else {
                return index - from;
            }
        }
        return to - from;
    }

    private int convert(int c) {
        if (c >= 48 && c <= 57) {
            return c - 48;
        }
        if (c >= 65 && c <= 70) {
            return (c - 65) + 10;
        }
        if (c < 97 || c > 102) {
            return -1;
        }
        return (c - 97) + 10;
    }

    public int read() throws IOException {
        int bytes;
        if (this.closed) {
            throw new IOException("Stream has been closed");
        }
        do {
            bytes = read(this.singleByte, 0, 1);
            if (bytes == -1) {
                return -1;
            }
        } while (bytes != 1);
        return this.singleByte[0] & 255;
    }

    public int read(byte[] b, int off, int len) throws IOException {
        if (!this.closed) {
            return read0(b, off, len);
        }
        throw new IOException("Stream has been closed");
    }
}
