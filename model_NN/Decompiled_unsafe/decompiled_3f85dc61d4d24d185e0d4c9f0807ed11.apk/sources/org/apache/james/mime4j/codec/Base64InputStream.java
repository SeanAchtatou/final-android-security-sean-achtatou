package org.apache.james.mime4j.codec;

import java.io.IOException;
import java.io.InputStream;
import org.apache.james.mime4j.util.ByteArrayBuffer;

public class Base64InputStream extends InputStream {
    static final /* synthetic */ boolean $assertionsDisabled = (!Base64InputStream.class.desiredAssertionStatus());
    private static final int[] BASE64_DECODE = new int[256];
    private static final byte BASE64_PAD = 61;
    private static final int ENCODED_BUFFER_SIZE = 1536;
    private static final int EOF = -1;
    private boolean closed;
    private final ByteArrayBuffer decodedBuf;
    private final byte[] encoded;
    private boolean eof;
    private final InputStream in;
    private final DecodeMonitor monitor;
    private int position;
    private final byte[] singleByte;
    private int size;

    static {
        for (int i = 0; i < 256; i++) {
            BASE64_DECODE[i] = -1;
        }
        for (int i2 = 0; i2 < Base64OutputStream.BASE64_TABLE.length; i2++) {
            BASE64_DECODE[Base64OutputStream.BASE64_TABLE[i2] & 255] = i2;
        }
    }

    public Base64InputStream(InputStream in2, DecodeMonitor monitor2) {
        this(ENCODED_BUFFER_SIZE, in2, monitor2);
    }

    protected Base64InputStream(int bufsize, InputStream in2, DecodeMonitor monitor2) {
        this.singleByte = new byte[1];
        this.position = 0;
        this.size = 0;
        this.closed = false;
        if (in2 == null) {
            throw new IllegalArgumentException();
        }
        this.encoded = new byte[bufsize];
        this.decodedBuf = new ByteArrayBuffer(512);
        this.in = in2;
        this.monitor = monitor2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.apache.james.mime4j.codec.Base64InputStream.<init>(java.io.InputStream, boolean):void
     arg types: [java.io.InputStream, int]
     candidates:
      org.apache.james.mime4j.codec.Base64InputStream.<init>(java.io.InputStream, org.apache.james.mime4j.codec.DecodeMonitor):void
      org.apache.james.mime4j.codec.Base64InputStream.<init>(java.io.InputStream, boolean):void */
    public Base64InputStream(InputStream in2) {
        this(in2, false);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public Base64InputStream(InputStream in2, boolean strict) {
        this(ENCODED_BUFFER_SIZE, in2, strict ? DecodeMonitor.STRICT : DecodeMonitor.SILENT);
    }

    public int read() throws IOException {
        int bytes;
        if (this.closed) {
            throw new IOException("Stream has been closed");
        }
        do {
            bytes = read0(this.singleByte, 0, 1);
            if (bytes == -1) {
                return -1;
            }
        } while (bytes != 1);
        return this.singleByte[0] & 255;
    }

    public int read(byte[] buffer) throws IOException {
        if (this.closed) {
            throw new IOException("Stream has been closed");
        } else if (buffer == null) {
            throw new NullPointerException();
        } else if (buffer.length == 0) {
            return 0;
        } else {
            return read0(buffer, 0, buffer.length);
        }
    }

    public int read(byte[] buffer, int offset, int length) throws IOException {
        if (this.closed) {
            throw new IOException("Stream has been closed");
        } else if (buffer == null) {
            throw new NullPointerException();
        } else if (offset < 0 || length < 0 || offset + length > buffer.length) {
            throw new IndexOutOfBoundsException();
        } else if (length == 0) {
            return 0;
        } else {
            return read0(buffer, offset, length);
        }
    }

    public void close() throws IOException {
        if (!this.closed) {
            this.closed = true;
        }
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 185 */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x009b, code lost:
        r3 = (r3 << 6) | r12;
        r4 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00a2, code lost:
        if (r4 != 4) goto L_0x00bd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00a4, code lost:
        r4 = 0;
        r8 = (byte) (r3 >>> 16);
        r9 = (byte) (r3 >>> 8);
        r10 = (byte) r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00ae, code lost:
        if (r6 >= (r7 - 2)) goto L_0x013b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00b0, code lost:
        r14 = r6 + 1;
        r20[r6] = r8;
        r6 = r14 + 1;
        r20[r14] = r9;
        r20[r6] = r10;
        r6 = r6 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c5, code lost:
        if (r0.position >= r0.size) goto L_0x0044;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00c7, code lost:
        if (r6 >= r7) goto L_0x0044;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00c9, code lost:
        r2 = r0.encoded;
        r5 = r0.position;
        r1.position = r5 + 1;
        r16 = r2[r5] & 255;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00e3, code lost:
        if (r16 != 61) goto L_0x00f1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00e5, code lost:
        r2 = decodePad(r3, r4, r20, r6, r7) - r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00f1, code lost:
        r12 = org.apache.james.mime4j.codec.Base64InputStream.BASE64_DECODE[r16];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00f5, code lost:
        if (r12 >= 0) goto L_0x009b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00fb, code lost:
        if (r16 == 13) goto L_0x00bd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0101, code lost:
        if (r16 == 10) goto L_0x00bd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0107, code lost:
        if (r16 == 32) goto L_0x00bd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0131, code lost:
        if (r0.monitor.warn("Unexpected base64 byte: " + ((int) ((byte) r16)), "ignoring.") == false) goto L_0x00bd;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x013a, code lost:
        throw new java.io.IOException("Unexpected base64 byte");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x013d, code lost:
        if (r6 >= (r7 - 1)) goto L_0x015a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x013f, code lost:
        r14 = r6 + 1;
        r20[r6] = r8;
        r6 = r14 + 1;
        r20[r14] = r9;
        r0.decodedBuf.append(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0150, code lost:
        if (org.apache.james.mime4j.codec.Base64InputStream.$assertionsDisabled != false) goto L_0x0186;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0152, code lost:
        if (r6 == r7) goto L_0x0186;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0159, code lost:
        throw new java.lang.AssertionError();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x015a, code lost:
        if (r6 >= r7) goto L_0x0170;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x015c, code lost:
        r20[r6] = r8;
        r0.decodedBuf.append(r9);
        r0.decodedBuf.append(r10);
        r6 = r6 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0170, code lost:
        r0.decodedBuf.append(r8);
        r0.decodedBuf.append(r9);
        r0.decodedBuf.append(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0186, code lost:
        r2 = r7 - r13;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0044, code lost:
        continue;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int read0(byte[] r20, int r21, int r22) throws java.io.IOException {
        /*
            r19 = this;
            r13 = r21
            int r7 = r21 + r22
            r6 = r21
            r0 = r19
            org.apache.james.mime4j.util.ByteArrayBuffer r2 = r0.decodedBuf
            int r2 = r2.length()
            if (r2 <= 0) goto L_0x0035
            r0 = r19
            org.apache.james.mime4j.util.ByteArrayBuffer r2 = r0.decodedBuf
            int r2 = r2.length()
            r0 = r22
            int r11 = java.lang.Math.min(r2, r0)
            r0 = r19
            org.apache.james.mime4j.util.ByteArrayBuffer r2 = r0.decodedBuf
            byte[] r2 = r2.buffer()
            r5 = 0
            r0 = r20
            java.lang.System.arraycopy(r2, r5, r0, r6, r11)
            r0 = r19
            org.apache.james.mime4j.util.ByteArrayBuffer r2 = r0.decodedBuf
            r5 = 0
            r2.remove(r5, r11)
            int r6 = r6 + r11
        L_0x0035:
            r0 = r19
            boolean r2 = r0.eof
            if (r2 == 0) goto L_0x0042
            if (r6 != r13) goto L_0x003f
            r2 = -1
        L_0x003e:
            return r2
        L_0x003f:
            int r2 = r6 - r13
            goto L_0x003e
        L_0x0042:
            r3 = 0
            r4 = 0
        L_0x0044:
            if (r6 >= r7) goto L_0x018a
        L_0x0046:
            r0 = r19
            int r2 = r0.position
            r0 = r19
            int r5 = r0.size
            if (r2 != r5) goto L_0x00bd
            r0 = r19
            java.io.InputStream r2 = r0.in
            r0 = r19
            byte[] r5 = r0.encoded
            r17 = 0
            r0 = r19
            byte[] r0 = r0.encoded
            r18 = r0
            r0 = r18
            int r0 = r0.length
            r18 = r0
            r0 = r17
            r1 = r18
            int r15 = r2.read(r5, r0, r1)
            r2 = -1
            if (r15 != r2) goto L_0x0083
            r2 = 1
            r0 = r19
            r0.eof = r2
            if (r4 == 0) goto L_0x007c
            r0 = r19
            r0.handleUnexpectedEof(r4)
        L_0x007c:
            if (r6 != r13) goto L_0x0080
            r2 = -1
            goto L_0x003e
        L_0x0080:
            int r2 = r6 - r13
            goto L_0x003e
        L_0x0083:
            if (r15 <= 0) goto L_0x008f
            r2 = 0
            r0 = r19
            r0.position = r2
            r0 = r19
            r0.size = r15
            goto L_0x0046
        L_0x008f:
            boolean r2 = org.apache.james.mime4j.codec.Base64InputStream.$assertionsDisabled
            if (r2 != 0) goto L_0x0046
            if (r15 == 0) goto L_0x0046
            java.lang.AssertionError r2 = new java.lang.AssertionError
            r2.<init>()
            throw r2
        L_0x009b:
            int r2 = r3 << 6
            r3 = r2 | r12
            int r4 = r4 + 1
            r2 = 4
            if (r4 != r2) goto L_0x00bd
            r4 = 0
            int r2 = r3 >>> 16
            byte r8 = (byte) r2
            int r2 = r3 >>> 8
            byte r9 = (byte) r2
            byte r10 = (byte) r3
            int r2 = r7 + -2
            if (r6 >= r2) goto L_0x013b
            int r14 = r6 + 1
            r20[r6] = r8
            int r6 = r14 + 1
            r20[r14] = r9
            int r14 = r6 + 1
            r20[r6] = r10
            r6 = r14
        L_0x00bd:
            r0 = r19
            int r2 = r0.position
            r0 = r19
            int r5 = r0.size
            if (r2 >= r5) goto L_0x0044
            if (r6 >= r7) goto L_0x0044
            r0 = r19
            byte[] r2 = r0.encoded
            r0 = r19
            int r5 = r0.position
            int r17 = r5 + 1
            r0 = r17
            r1 = r19
            r1.position = r0
            byte r2 = r2[r5]
            r0 = r2 & 255(0xff, float:3.57E-43)
            r16 = r0
            r2 = 61
            r0 = r16
            if (r0 != r2) goto L_0x00f1
            r2 = r19
            r5 = r20
            int r6 = r2.decodePad(r3, r4, r5, r6, r7)
            int r2 = r6 - r13
            goto L_0x003e
        L_0x00f1:
            int[] r2 = org.apache.james.mime4j.codec.Base64InputStream.BASE64_DECODE
            r12 = r2[r16]
            if (r12 >= 0) goto L_0x009b
            r2 = 13
            r0 = r16
            if (r0 == r2) goto L_0x00bd
            r2 = 10
            r0 = r16
            if (r0 == r2) goto L_0x00bd
            r2 = 32
            r0 = r16
            if (r0 == r2) goto L_0x00bd
            r0 = r19
            org.apache.james.mime4j.codec.DecodeMonitor r2 = r0.monitor
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r17 = "Unexpected base64 byte: "
            r0 = r17
            java.lang.StringBuilder r5 = r5.append(r0)
            r0 = r16
            byte r0 = (byte) r0
            r17 = r0
            r0 = r17
            java.lang.StringBuilder r5 = r5.append(r0)
            java.lang.String r5 = r5.toString()
            java.lang.String r17 = "ignoring."
            r0 = r17
            boolean r2 = r2.warn(r5, r0)
            if (r2 == 0) goto L_0x00bd
            java.io.IOException r2 = new java.io.IOException
            java.lang.String r5 = "Unexpected base64 byte"
            r2.<init>(r5)
            throw r2
        L_0x013b:
            int r2 = r7 + -1
            if (r6 >= r2) goto L_0x015a
            int r14 = r6 + 1
            r20[r6] = r8
            int r6 = r14 + 1
            r20[r14] = r9
            r0 = r19
            org.apache.james.mime4j.util.ByteArrayBuffer r2 = r0.decodedBuf
            r2.append(r10)
        L_0x014e:
            boolean r2 = org.apache.james.mime4j.codec.Base64InputStream.$assertionsDisabled
            if (r2 != 0) goto L_0x0186
            if (r6 == r7) goto L_0x0186
            java.lang.AssertionError r2 = new java.lang.AssertionError
            r2.<init>()
            throw r2
        L_0x015a:
            if (r6 >= r7) goto L_0x0170
            int r14 = r6 + 1
            r20[r6] = r8
            r0 = r19
            org.apache.james.mime4j.util.ByteArrayBuffer r2 = r0.decodedBuf
            r2.append(r9)
            r0 = r19
            org.apache.james.mime4j.util.ByteArrayBuffer r2 = r0.decodedBuf
            r2.append(r10)
            r6 = r14
            goto L_0x014e
        L_0x0170:
            r0 = r19
            org.apache.james.mime4j.util.ByteArrayBuffer r2 = r0.decodedBuf
            r2.append(r8)
            r0 = r19
            org.apache.james.mime4j.util.ByteArrayBuffer r2 = r0.decodedBuf
            r2.append(r9)
            r0 = r19
            org.apache.james.mime4j.util.ByteArrayBuffer r2 = r0.decodedBuf
            r2.append(r10)
            goto L_0x014e
        L_0x0186:
            int r2 = r7 - r13
            goto L_0x003e
        L_0x018a:
            boolean r2 = org.apache.james.mime4j.codec.Base64InputStream.$assertionsDisabled
            if (r2 != 0) goto L_0x0196
            if (r4 == 0) goto L_0x0196
            java.lang.AssertionError r2 = new java.lang.AssertionError
            r2.<init>()
            throw r2
        L_0x0196:
            boolean r2 = org.apache.james.mime4j.codec.Base64InputStream.$assertionsDisabled
            if (r2 != 0) goto L_0x01a2
            if (r6 == r7) goto L_0x01a2
            java.lang.AssertionError r2 = new java.lang.AssertionError
            r2.<init>()
            throw r2
        L_0x01a2:
            int r2 = r7 - r13
            goto L_0x003e
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.codec.Base64InputStream.read0(byte[], int, int):int");
    }

    private int decodePad(int data, int sextets, byte[] buffer, int index, int end) throws IOException {
        this.eof = true;
        if (sextets == 2) {
            byte b = (byte) (data >>> 4);
            if (index < end) {
                buffer[index] = b;
                return index + 1;
            }
            this.decodedBuf.append(b);
            return index;
        } else if (sextets == 3) {
            byte b1 = (byte) (data >>> 10);
            byte b2 = (byte) ((data >>> 2) & 255);
            if (index < end - 1) {
                int index2 = index + 1;
                buffer[index] = b1;
                int index3 = index2 + 1;
                buffer[index2] = b2;
                return index3;
            } else if (index < end) {
                buffer[index] = b1;
                this.decodedBuf.append(b2);
                return index + 1;
            } else {
                this.decodedBuf.append(b1);
                this.decodedBuf.append(b2);
                return index;
            }
        } else {
            handleUnexpecedPad(sextets);
            return index;
        }
    }

    private void handleUnexpectedEof(int sextets) throws IOException {
        if (this.monitor.warn("Unexpected end of BASE64 stream", "dropping " + sextets + " sextet(s)")) {
            throw new IOException("Unexpected end of BASE64 stream");
        }
    }

    private void handleUnexpecedPad(int sextets) throws IOException {
        if (this.monitor.warn("Unexpected padding character", "dropping " + sextets + " sextet(s)")) {
            throw new IOException("Unexpected padding character");
        }
    }
}
