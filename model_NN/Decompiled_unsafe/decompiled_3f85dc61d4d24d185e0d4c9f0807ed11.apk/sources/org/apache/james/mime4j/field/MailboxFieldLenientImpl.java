package org.apache.james.mime4j.field;

import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.address.Mailbox;
import org.apache.james.mime4j.dom.field.MailboxField;
import org.apache.james.mime4j.field.address.LenientAddressBuilder;
import org.apache.james.mime4j.stream.Field;
import org.apache.james.mime4j.stream.ParserCursor;
import org.apache.james.mime4j.stream.RawField;
import org.apache.james.mime4j.util.ByteSequence;
import org.apache.james.mime4j.util.ContentUtil;

public class MailboxFieldLenientImpl extends AbstractField implements MailboxField {
    public static final FieldParser<MailboxField> PARSER = new FieldParser<MailboxField>() {
        public MailboxField parse(Field rawField, DecodeMonitor monitor) {
            return new MailboxFieldLenientImpl(rawField, monitor);
        }
    };
    private Mailbox mailbox;
    private boolean parsed = false;

    MailboxFieldLenientImpl(Field rawField, DecodeMonitor monitor) {
        super(rawField, monitor);
    }

    public Mailbox getMailbox() {
        if (!this.parsed) {
            parse();
        }
        return this.mailbox;
    }

    private void parse() {
        this.parsed = true;
        RawField f = getRawField();
        ByteSequence buf = f.getRaw();
        int pos = f.getDelimiterIdx() + 1;
        if (buf == null) {
            String body = f.getBody();
            if (body != null) {
                buf = ContentUtil.encode(body);
                pos = 0;
            } else {
                return;
            }
        }
        this.mailbox = LenientAddressBuilder.DEFAULT.parseMailbox(buf, new ParserCursor(pos, buf.length()), null);
    }
}
