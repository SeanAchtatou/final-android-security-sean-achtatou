package org.apache.james.mime4j.dom;

import java.util.Iterator;
import java.util.List;
import org.apache.james.mime4j.stream.Field;

public interface Header extends Iterable<Field> {
    void addField(Field field);

    Field getField(String str);

    List<Field> getFields();

    List<Field> getFields(String str);

    Iterator<Field> iterator();

    int removeFields(String str);

    void setField(Field field);
}
