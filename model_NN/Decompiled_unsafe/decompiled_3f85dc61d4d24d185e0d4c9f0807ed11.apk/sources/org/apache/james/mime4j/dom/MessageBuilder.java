package org.apache.james.mime4j.dom;

import java.io.IOException;
import java.io.InputStream;
import org.apache.james.mime4j.MimeException;

public interface MessageBuilder {
    Header newHeader();

    Header newHeader(Header header);

    Message newMessage();

    Message newMessage(Message message);

    Multipart newMultipart(String str);

    Multipart newMultipart(Multipart multipart);

    Header parseHeader(InputStream inputStream) throws MimeException, IOException;

    Message parseMessage(InputStream inputStream) throws MimeException, IOException;
}
