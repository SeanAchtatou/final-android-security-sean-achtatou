package org.apache.james.mime4j.dom.address;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class Group extends Address {
    private static final long serialVersionUID = 1;
    private final MailboxList mailboxList;
    private final String name;

    public Group(String name2, MailboxList mailboxes) {
        if (name2 == null) {
            throw new IllegalArgumentException();
        } else if (mailboxes == null) {
            throw new IllegalArgumentException();
        } else {
            this.name = name2;
            this.mailboxList = mailboxes;
        }
    }

    public Group(String name2, Mailbox... mailboxes) {
        this(name2, new MailboxList(Arrays.asList(mailboxes), true));
    }

    public Group(String name2, Collection<Mailbox> mailboxes) {
        this(name2, new MailboxList(new ArrayList(mailboxes), true));
    }

    public String getName() {
        return this.name;
    }

    public MailboxList getMailboxes() {
        return this.mailboxList;
    }

    /* access modifiers changed from: protected */
    public void doAddMailboxesTo(List<Mailbox> results) {
        Iterator i$ = this.mailboxList.iterator();
        while (i$.hasNext()) {
            results.add((Mailbox) i$.next());
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.name);
        sb.append(':');
        boolean first = true;
        Iterator i$ = this.mailboxList.iterator();
        while (i$.hasNext()) {
            Mailbox mailbox = (Mailbox) i$.next();
            if (first) {
                first = false;
            } else {
                sb.append(',');
            }
            sb.append(' ');
            sb.append(mailbox);
        }
        sb.append(";");
        return sb.toString();
    }
}
