package org.apache.james.mime4j.stream;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.LinkedList;
import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.io.LineNumberInputStream;
import org.apache.james.mime4j.io.LineNumberSource;
import org.apache.james.mime4j.util.CharsetUtil;

public class MimeTokenStream {
    private final BodyDescriptorBuilder bodyDescBuilder;
    private final MimeConfig config;
    private EntityStateMachine currentStateMachine;
    private final LinkedList<EntityStateMachine> entities;
    private final FieldBuilder fieldBuilder;
    private final DecodeMonitor monitor;
    private RecursionMode recursionMode;
    private MimeEntity rootentity;
    private EntityState state;

    public MimeTokenStream() {
        this(null);
    }

    public MimeTokenStream(MimeConfig config2) {
        this(config2, null, null, null);
    }

    public MimeTokenStream(MimeConfig config2, BodyDescriptorBuilder bodyDescBuilder2) {
        this(config2, null, null, bodyDescBuilder2);
    }

    public MimeTokenStream(MimeConfig config2, DecodeMonitor monitor2, BodyDescriptorBuilder bodyDescBuilder2) {
        this(config2, monitor2, null, bodyDescBuilder2);
    }

    public MimeTokenStream(MimeConfig config2, DecodeMonitor monitor2, FieldBuilder fieldBuilder2, BodyDescriptorBuilder bodyDescBuilder2) {
        this.entities = new LinkedList<>();
        this.state = EntityState.T_END_OF_STREAM;
        this.recursionMode = RecursionMode.M_RECURSE;
        this.config = config2 == null ? new MimeConfig() : config2;
        this.fieldBuilder = fieldBuilder2 == null ? new DefaultFieldBuilder(this.config.getMaxHeaderLen()) : fieldBuilder2;
        this.monitor = monitor2 == null ? this.config.isStrictParsing() ? DecodeMonitor.STRICT : DecodeMonitor.SILENT : monitor2;
        this.bodyDescBuilder = bodyDescBuilder2 == null ? new FallbackBodyDescriptorBuilder() : bodyDescBuilder2;
    }

    public void parse(InputStream stream) {
        doParse(stream, EntityState.T_START_MESSAGE);
    }

    public Field parseHeadless(InputStream stream, String contentType) {
        if (contentType == null) {
            throw new IllegalArgumentException("Content type may not be null");
        }
        try {
            RawField rawContentType = new RawField("Content-Type", contentType);
            Field newContentType = this.bodyDescBuilder.addField(rawContentType);
            if (newContentType == null) {
                newContentType = rawContentType;
            }
            doParse(stream, EntityState.T_END_HEADER);
            try {
                next();
                return newContentType;
            } catch (IOException e) {
                throw new IllegalStateException(e);
            } catch (MimeException e2) {
                throw new IllegalStateException(e2);
            }
        } catch (MimeException ex) {
            throw new IllegalArgumentException(ex.getMessage());
        }
    }

    private void doParse(InputStream stream, EntityState start) {
        LineNumberSource lineSource = null;
        if (this.config.isCountLineNumbers()) {
            LineNumberSource lineInput = new LineNumberInputStream(stream);
            lineSource = lineInput;
            stream = lineInput;
        }
        this.rootentity = new MimeEntity(lineSource, stream, this.config, start, EntityState.T_END_MESSAGE, this.monitor, this.fieldBuilder, this.bodyDescBuilder);
        this.rootentity.setRecursionMode(this.recursionMode);
        this.currentStateMachine = this.rootentity;
        this.entities.clear();
        this.entities.add(this.currentStateMachine);
        this.state = this.currentStateMachine.getState();
    }

    public boolean isRaw() {
        return this.recursionMode == RecursionMode.M_RAW;
    }

    public RecursionMode getRecursionMode() {
        return this.recursionMode;
    }

    public void setRecursionMode(RecursionMode mode) {
        this.recursionMode = mode;
        if (this.currentStateMachine != null) {
            this.currentStateMachine.setRecursionMode(mode);
        }
    }

    public void stop() {
        this.rootentity.stop();
    }

    public EntityState getState() {
        return this.state;
    }

    public InputStream getInputStream() {
        return this.currentStateMachine.getContentStream();
    }

    public InputStream getDecodedInputStream() {
        return this.currentStateMachine.getDecodedContentStream();
    }

    public Reader getReader() {
        Charset charset;
        String mimeCharset = getBodyDescriptor().getCharset();
        if (mimeCharset == null || "".equals(mimeCharset)) {
            charset = CharsetUtil.US_ASCII;
        } else {
            charset = Charset.forName(mimeCharset);
        }
        return new InputStreamReader(getDecodedInputStream(), charset);
    }

    public BodyDescriptor getBodyDescriptor() {
        return this.currentStateMachine.getBodyDescriptor();
    }

    public Field getField() {
        return this.currentStateMachine.getField();
    }

    /* JADX WARNING: CFG modification limit reached, blocks count: 124 */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0046, code lost:
        r3.currentStateMachine = r3.entities.getLast();
        r3.currentStateMachine.setRecursionMode(r3.recursionMode);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.apache.james.mime4j.stream.EntityState next() throws java.io.IOException, org.apache.james.mime4j.MimeException {
        /*
            r3 = this;
            org.apache.james.mime4j.stream.EntityState r1 = r3.state
            org.apache.james.mime4j.stream.EntityState r2 = org.apache.james.mime4j.stream.EntityState.T_END_OF_STREAM
            if (r1 == r2) goto L_0x000a
            org.apache.james.mime4j.stream.EntityStateMachine r1 = r3.currentStateMachine
            if (r1 != 0) goto L_0x0022
        L_0x000a:
            java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
            java.lang.String r2 = "No more tokens are available."
            r1.<init>(r2)
            throw r1
        L_0x0012:
            java.util.LinkedList<org.apache.james.mime4j.stream.EntityStateMachine> r1 = r3.entities
            r1.removeLast()
            java.util.LinkedList<org.apache.james.mime4j.stream.EntityStateMachine> r1 = r3.entities
            boolean r1 = r1.isEmpty()
            if (r1 == 0) goto L_0x0046
            r1 = 0
            r3.currentStateMachine = r1
        L_0x0022:
            org.apache.james.mime4j.stream.EntityStateMachine r1 = r3.currentStateMachine
            if (r1 == 0) goto L_0x0058
            org.apache.james.mime4j.stream.EntityStateMachine r1 = r3.currentStateMachine
            org.apache.james.mime4j.stream.EntityStateMachine r0 = r1.advance()
            if (r0 == 0) goto L_0x0035
            java.util.LinkedList<org.apache.james.mime4j.stream.EntityStateMachine> r1 = r3.entities
            r1.add(r0)
            r3.currentStateMachine = r0
        L_0x0035:
            org.apache.james.mime4j.stream.EntityStateMachine r1 = r3.currentStateMachine
            org.apache.james.mime4j.stream.EntityState r1 = r1.getState()
            r3.state = r1
            org.apache.james.mime4j.stream.EntityState r1 = r3.state
            org.apache.james.mime4j.stream.EntityState r2 = org.apache.james.mime4j.stream.EntityState.T_END_OF_STREAM
            if (r1 == r2) goto L_0x0012
            org.apache.james.mime4j.stream.EntityState r1 = r3.state
        L_0x0045:
            return r1
        L_0x0046:
            java.util.LinkedList<org.apache.james.mime4j.stream.EntityStateMachine> r1 = r3.entities
            java.lang.Object r1 = r1.getLast()
            org.apache.james.mime4j.stream.EntityStateMachine r1 = (org.apache.james.mime4j.stream.EntityStateMachine) r1
            r3.currentStateMachine = r1
            org.apache.james.mime4j.stream.EntityStateMachine r1 = r3.currentStateMachine
            org.apache.james.mime4j.stream.RecursionMode r2 = r3.recursionMode
            r1.setRecursionMode(r2)
            goto L_0x0022
        L_0x0058:
            org.apache.james.mime4j.stream.EntityState r1 = org.apache.james.mime4j.stream.EntityState.T_END_OF_STREAM
            r3.state = r1
            org.apache.james.mime4j.stream.EntityState r1 = r3.state
            goto L_0x0045
        */
        throw new UnsupportedOperationException("Method not decompiled: org.apache.james.mime4j.stream.MimeTokenStream.next():org.apache.james.mime4j.stream.EntityState");
    }

    public static final String stateToString(EntityState state2) {
        return MimeEntity.stateToString(state2);
    }

    public MimeConfig getConfig() {
        return this.config;
    }
}
