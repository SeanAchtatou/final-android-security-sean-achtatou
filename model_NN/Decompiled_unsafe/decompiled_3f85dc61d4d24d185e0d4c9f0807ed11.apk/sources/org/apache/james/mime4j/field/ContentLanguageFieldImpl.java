package org.apache.james.mime4j.field;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.field.ContentLanguageField;
import org.apache.james.mime4j.field.language.parser.ContentLanguageParser;
import org.apache.james.mime4j.field.language.parser.ParseException;
import org.apache.james.mime4j.stream.Field;

public class ContentLanguageFieldImpl extends AbstractField implements ContentLanguageField {
    public static final FieldParser<ContentLanguageField> PARSER = new FieldParser<ContentLanguageField>() {
        public ContentLanguageField parse(Field rawField, DecodeMonitor monitor) {
            return new ContentLanguageFieldImpl(rawField, monitor);
        }
    };
    private List<String> languages;
    private ParseException parseException;
    private boolean parsed = false;

    ContentLanguageFieldImpl(Field rawField, DecodeMonitor monitor) {
        super(rawField, monitor);
    }

    private void parse() {
        this.parsed = true;
        this.languages = Collections.emptyList();
        String body = getBody();
        if (body != null) {
            try {
                this.languages = new ContentLanguageParser(new StringReader(body)).parse();
            } catch (ParseException ex) {
                this.parseException = ex;
            }
        }
    }

    public org.apache.james.mime4j.dom.field.ParseException getParseException() {
        return this.parseException;
    }

    public List<String> getLanguages() {
        if (!this.parsed) {
            parse();
        }
        return new ArrayList(this.languages);
    }
}
