package org.apache.james.mime4j.field.contenttype.parser;

import java.io.Serializable;

public class Token implements Serializable {
    private static final long serialVersionUID = 1;
    public int beginColumn;
    public int beginLine;
    public int endColumn;
    public int endLine;
    public String image;
    public int kind;
    public Token next;
    public Token specialToken;

    public Object getValue() {
        return null;
    }

    public Token() {
    }

    public Token(int kind2) {
        this(kind2, null);
    }

    public Token(int kind2, String image2) {
        this.kind = kind2;
        this.image = image2;
    }

    public String toString() {
        return this.image;
    }

    public static Token newToken(int ofKind, String image2) {
        return new Token(ofKind, image2);
    }

    public static Token newToken(int ofKind) {
        return newToken(ofKind, null);
    }
}
