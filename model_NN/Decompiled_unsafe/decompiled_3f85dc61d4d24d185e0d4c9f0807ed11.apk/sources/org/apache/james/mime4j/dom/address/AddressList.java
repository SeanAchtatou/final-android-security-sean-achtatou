package org.apache.james.mime4j.dom.address;

import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class AddressList extends AbstractList<Address> implements Serializable {
    private static final long serialVersionUID = 1;
    private final List<? extends Address> addresses;

    public AddressList(List<? extends Address> addresses2, boolean dontCopy) {
        if (addresses2 != null) {
            this.addresses = !dontCopy ? new ArrayList<>(addresses2) : addresses2;
        } else {
            this.addresses = Collections.emptyList();
        }
    }

    public int size() {
        return this.addresses.size();
    }

    public Address get(int index) {
        return (Address) this.addresses.get(index);
    }

    public MailboxList flatten() {
        boolean groupDetected = false;
        Iterator i$ = this.addresses.iterator();
        while (true) {
            if (i$.hasNext()) {
                if (!(((Address) i$.next()) instanceof Mailbox)) {
                    groupDetected = true;
                    break;
                }
            } else {
                break;
            }
        }
        if (!groupDetected) {
            return new MailboxList(this.addresses, true);
        }
        List<Mailbox> results = new ArrayList<>();
        for (Address addr : this.addresses) {
            addr.addMailboxesTo(results);
        }
        return new MailboxList(results, false);
    }
}
