package org.apache.james.mime4j.field;

import org.apache.james.mime4j.codec.DecodeMonitor;
import org.apache.james.mime4j.dom.FieldParser;
import org.apache.james.mime4j.dom.field.ContentIdField;
import org.apache.james.mime4j.stream.Field;

public class ContentIdFieldImpl extends AbstractField implements ContentIdField {
    public static final FieldParser<ContentIdField> PARSER = new FieldParser<ContentIdField>() {
        public ContentIdField parse(Field rawField, DecodeMonitor monitor) {
            return new ContentIdFieldImpl(rawField, monitor);
        }
    };
    private String id;
    private boolean parsed = false;

    ContentIdFieldImpl(Field rawField, DecodeMonitor monitor) {
        super(rawField, monitor);
    }

    private void parse() {
        this.parsed = true;
        String body = getBody();
        if (body != null) {
            this.id = body.trim();
        } else {
            this.id = null;
        }
    }

    public String getId() {
        if (!this.parsed) {
            parse();
        }
        return this.id;
    }
}
