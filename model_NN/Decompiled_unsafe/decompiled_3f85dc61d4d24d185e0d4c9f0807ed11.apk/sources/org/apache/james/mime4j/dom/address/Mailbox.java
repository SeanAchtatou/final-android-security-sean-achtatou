package org.apache.james.mime4j.dom.address;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import org.apache.james.mime4j.util.LangUtils;

public class Mailbox extends Address {
    private static final DomainList EMPTY_ROUTE_LIST = new DomainList(Collections.emptyList(), true);
    private static final long serialVersionUID = 1;
    private final String domain;
    private final String localPart;
    private final String name;
    private final DomainList route;

    public Mailbox(String name2, DomainList route2, String localPart2, String domain2) {
        if (localPart2 == null) {
            throw new IllegalArgumentException();
        }
        this.name = (name2 == null || name2.length() == 0) ? null : name2;
        this.route = route2 == null ? EMPTY_ROUTE_LIST : route2;
        this.localPart = localPart2;
        this.domain = (domain2 == null || domain2.length() == 0) ? null : domain2;
    }

    Mailbox(String name2, Mailbox baseMailbox) {
        this(name2, baseMailbox.getRoute(), baseMailbox.getLocalPart(), baseMailbox.getDomain());
    }

    public Mailbox(String localPart2, String domain2) {
        this(null, null, localPart2, domain2);
    }

    public Mailbox(DomainList route2, String localPart2, String domain2) {
        this(null, route2, localPart2, domain2);
    }

    public Mailbox(String name2, String localPart2, String domain2) {
        this(name2, null, localPart2, domain2);
    }

    public String getName() {
        return this.name;
    }

    public DomainList getRoute() {
        return this.route;
    }

    public String getLocalPart() {
        return this.localPart;
    }

    public String getDomain() {
        return this.domain;
    }

    public String getAddress() {
        if (this.domain == null) {
            return this.localPart;
        }
        return this.localPart + '@' + this.domain;
    }

    /* access modifiers changed from: protected */
    public final void doAddMailboxesTo(List<Mailbox> results) {
        results.add(this);
    }

    public int hashCode() {
        return LangUtils.hashCode(LangUtils.hashCode(17, this.localPart), this.domain != null ? this.domain.toLowerCase(Locale.US) : null);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Mailbox)) {
            return false;
        }
        Mailbox that = (Mailbox) obj;
        if (!LangUtils.equals(this.localPart, that.localPart) || !LangUtils.equalsIgnoreCase(this.domain, that.domain)) {
            return false;
        }
        return true;
    }

    public String toString() {
        return getAddress();
    }
}
