package org.jdom2;

import org.jdom2.Content;
import org.jdom2.output.XMLOutputter;

public class DocType extends Content {
    private static final long serialVersionUID = 200;
    protected String elementName;
    protected String internalSubset;
    protected String publicID;
    protected String systemID;

    protected DocType() {
        super(Content.CType.DocType);
    }

    public DocType(String elementName2, String publicID2, String systemID2) {
        super(Content.CType.DocType);
        setElementName(elementName2);
        setPublicID(publicID2);
        setSystemID(systemID2);
    }

    public DocType(String elementName2, String systemID2) {
        this(elementName2, null, systemID2);
    }

    public DocType(String elementName2) {
        this(elementName2, null, null);
    }

    public String getElementName() {
        return this.elementName;
    }

    public DocType setElementName(String elementName2) {
        String reason = Verifier.checkXMLName(elementName2);
        if (reason != null) {
            throw new IllegalNameException(elementName2, "DocType", reason);
        }
        this.elementName = elementName2;
        return this;
    }

    public String getPublicID() {
        return this.publicID;
    }

    public DocType setPublicID(String publicID2) {
        String reason = Verifier.checkPublicID(publicID2);
        if (reason != null) {
            throw new IllegalDataException(publicID2, "DocType", reason);
        }
        this.publicID = publicID2;
        return this;
    }

    public String getSystemID() {
        return this.systemID;
    }

    public DocType setSystemID(String systemID2) {
        String reason = Verifier.checkSystemLiteral(systemID2);
        if (reason != null) {
            throw new IllegalDataException(systemID2, "DocType", reason);
        }
        this.systemID = systemID2;
        return this;
    }

    public String getValue() {
        return "";
    }

    public void setInternalSubset(String newData) {
        this.internalSubset = newData;
    }

    public String getInternalSubset() {
        return this.internalSubset;
    }

    public String toString() {
        return "[DocType: " + new XMLOutputter().outputString(this) + "]";
    }

    public DocType clone() {
        return (DocType) super.clone();
    }

    public DocType detach() {
        return (DocType) super.detach();
    }

    /* access modifiers changed from: protected */
    public DocType setParent(Parent parent) {
        return (DocType) super.setParent(parent);
    }

    public Document getParent() {
        return (Document) super.getParent();
    }
}
