package org.jdom2;

import java.io.InvalidObjectException;
import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public final class Namespace implements Serializable {
    public static final Namespace NO_NAMESPACE = new Namespace("", "");
    public static final Namespace XML_NAMESPACE = new Namespace(JDOMConstants.NS_PREFIX_XML, JDOMConstants.NS_URI_XML);
    private static final ConcurrentMap<String, ConcurrentMap<String, Namespace>> namespacemap = new ConcurrentHashMap(512, 0.75f, 64);
    private static final long serialVersionUID = 200;
    private final transient String prefix;
    private final transient String uri;

    static {
        ConcurrentMap<String, Namespace> nmap = new ConcurrentHashMap<>();
        nmap.put(NO_NAMESPACE.getPrefix(), NO_NAMESPACE);
        namespacemap.put(NO_NAMESPACE.getURI(), nmap);
        ConcurrentMap<String, Namespace> xmap = new ConcurrentHashMap<>();
        xmap.put(XML_NAMESPACE.getPrefix(), XML_NAMESPACE);
        namespacemap.put(XML_NAMESPACE.getURI(), xmap);
    }

    public static Namespace getNamespace(String prefix2, String uri2) {
        String str;
        String pfx;
        if (uri2 != null) {
            ConcurrentMap<String, Namespace> urimap = namespacemap.get(uri2);
            if (urimap == null) {
                String reason = Verifier.checkNamespaceURI(uri2);
                if (reason != null) {
                    throw new IllegalNameException(uri2, "Namespace URI", reason);
                }
                urimap = new ConcurrentHashMap<>();
                ConcurrentMap<String, Namespace> xmap = namespacemap.putIfAbsent(uri2, urimap);
                if (xmap != null) {
                    urimap = xmap;
                }
            }
            if (prefix2 == null) {
                str = "";
            } else {
                str = prefix2;
            }
            Namespace ns = (Namespace) urimap.get(str);
            if (ns != null) {
                return ns;
            }
            if ("".equals(uri2)) {
                throw new IllegalNameException("", "namespace", "Namespace URIs must be non-null and non-empty Strings");
            } else if (JDOMConstants.NS_URI_XML.equals(uri2)) {
                throw new IllegalNameException(uri2, "Namespace URI", "The http://www.w3.org/XML/1998/namespace must be bound to only the 'xml' prefix.");
            } else {
                if (prefix2 == null) {
                    pfx = "";
                } else {
                    pfx = prefix2;
                }
                String reason2 = Verifier.checkNamespacePrefix(pfx);
                if (reason2 != null) {
                    throw new IllegalNameException(pfx, "Namespace prefix", reason2);
                }
                Namespace ns2 = new Namespace(pfx, uri2);
                Namespace prev = (Namespace) urimap.putIfAbsent(pfx, ns2);
                if (prev != null) {
                    return prev;
                }
                return ns2;
            }
        } else if (prefix2 == null || "".equals(prefix2)) {
            return NO_NAMESPACE;
        } else {
            throw new IllegalNameException("", "namespace", "Namespace URIs must be non-null and non-empty Strings");
        }
    }

    public static Namespace getNamespace(String uri2) {
        return getNamespace("", uri2);
    }

    private Namespace(String prefix2, String uri2) {
        this.prefix = prefix2;
        this.uri = uri2;
    }

    public String getPrefix() {
        return this.prefix;
    }

    public String getURI() {
        return this.uri;
    }

    public boolean equals(Object ob) {
        if (this == ob) {
            return true;
        }
        if (ob instanceof Namespace) {
            return this.uri.equals(((Namespace) ob).uri);
        }
        return false;
    }

    public String toString() {
        return "[Namespace: prefix \"" + this.prefix + "\" is mapped to URI \"" + this.uri + "\"]";
    }

    public int hashCode() {
        return this.uri.hashCode();
    }

    private static final class NamespaceSerializationProxy implements Serializable {
        private static final long serialVersionUID = 200;
        private final String pprefix;
        private final String puri;

        public NamespaceSerializationProxy(String pprefix2, String puri2) {
            this.pprefix = pprefix2;
            this.puri = puri2;
        }

        private Object readResolve() {
            return Namespace.getNamespace(this.pprefix, this.puri);
        }
    }

    private Object writeReplace() {
        return new NamespaceSerializationProxy(this.prefix, this.uri);
    }

    private Object readResolve() throws InvalidObjectException {
        throw new InvalidObjectException("Namespace is serialized through a proxy");
    }
}
