package org.jdom2;

import org.jdom2.Content;
import org.jdom2.output.XMLOutputter;

public class Comment extends Content {
    private static final long serialVersionUID = 200;
    protected String text;

    protected Comment() {
        super(Content.CType.Comment);
    }

    public Comment(String text2) {
        super(Content.CType.Comment);
        setText(text2);
    }

    public String getValue() {
        return this.text;
    }

    public String getText() {
        return this.text;
    }

    public Comment setText(String text2) {
        String reason = Verifier.checkCommentData(text2);
        if (reason != null) {
            throw new IllegalDataException(text2, "comment", reason);
        }
        this.text = text2;
        return this;
    }

    public Comment clone() {
        return (Comment) super.clone();
    }

    public Comment detach() {
        return (Comment) super.detach();
    }

    /* access modifiers changed from: protected */
    public Comment setParent(Parent parent) {
        return (Comment) super.setParent(parent);
    }

    public String toString() {
        return "[Comment: " + new XMLOutputter().outputString(this) + "]";
    }
}
