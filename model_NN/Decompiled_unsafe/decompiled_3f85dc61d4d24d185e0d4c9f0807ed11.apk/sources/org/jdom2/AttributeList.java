package org.jdom2;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.RandomAccess;
import org.jdom2.internal.ArrayCopy;

final class AttributeList extends AbstractList<Attribute> implements RandomAccess {
    private static final int INITIAL_ARRAY_SIZE = 4;
    /* access modifiers changed from: private */
    public Attribute[] attributeData;
    private final Element parent;
    /* access modifiers changed from: private */
    public int size;

    AttributeList(Element parent2) {
        this.parent = parent2;
    }

    /* access modifiers changed from: package-private */
    public final void uncheckedAddAttribute(Attribute a) {
        a.parent = this.parent;
        ensureCapacity(this.size + 1);
        Attribute[] attributeArr = this.attributeData;
        int i = this.size;
        this.size = i + 1;
        attributeArr[i] = a;
        this.modCount++;
    }

    public boolean add(Attribute attribute) {
        if (attribute.getParent() != null) {
            throw new IllegalAddException("The attribute already has an existing parent \"" + attribute.getParent().getQualifiedName() + "\"");
        } else if (Verifier.checkNamespaceCollision(attribute, this.parent) != null) {
            throw new IllegalAddException(this.parent, attribute, Verifier.checkNamespaceCollision(attribute, this.parent));
        } else {
            int duplicate = indexOfDuplicate(attribute);
            if (duplicate < 0) {
                attribute.setParent(this.parent);
                ensureCapacity(this.size + 1);
                Attribute[] attributeArr = this.attributeData;
                int i = this.size;
                this.size = i + 1;
                attributeArr[i] = attribute;
                this.modCount++;
                return true;
            }
            this.attributeData[duplicate].setParent(null);
            this.attributeData[duplicate] = attribute;
            attribute.setParent(this.parent);
            return true;
        }
    }

    public void add(int index, Attribute attribute) {
        if (index < 0 || index > this.size) {
            throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
        } else if (attribute.getParent() != null) {
            throw new IllegalAddException("The attribute already has an existing parent \"" + attribute.getParent().getQualifiedName() + "\"");
        } else if (indexOfDuplicate(attribute) >= 0) {
            throw new IllegalAddException("Cannot add duplicate attribute");
        } else {
            String reason = Verifier.checkNamespaceCollision(attribute, this.parent);
            if (reason != null) {
                throw new IllegalAddException(this.parent, attribute, reason);
            }
            attribute.setParent(this.parent);
            ensureCapacity(this.size + 1);
            if (index == this.size) {
                Attribute[] attributeArr = this.attributeData;
                int i = this.size;
                this.size = i + 1;
                attributeArr[i] = attribute;
            } else {
                System.arraycopy(this.attributeData, index, this.attributeData, index + 1, this.size - index);
                this.attributeData[index] = attribute;
                this.size++;
            }
            this.modCount++;
        }
    }

    public boolean addAll(Collection<? extends Attribute> collection) {
        return addAll(size(), collection);
    }

    /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    public boolean addAll(int r10, java.util.Collection<? extends org.jdom2.Attribute> r11) {
        /*
            r9 = this;
            r7 = 1
            if (r10 < 0) goto L_0x0007
            int r6 = r9.size
            if (r10 <= r6) goto L_0x002e
        L_0x0007:
            java.lang.IndexOutOfBoundsException r6 = new java.lang.IndexOutOfBoundsException
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r8 = "Index: "
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.StringBuilder r7 = r7.append(r10)
            java.lang.String r8 = " Size: "
            java.lang.StringBuilder r7 = r7.append(r8)
            int r8 = r9.size()
            java.lang.StringBuilder r7 = r7.append(r8)
            java.lang.String r7 = r7.toString()
            r6.<init>(r7)
            throw r6
        L_0x002e:
            if (r11 != 0) goto L_0x0038
            java.lang.NullPointerException r6 = new java.lang.NullPointerException
            java.lang.String r7 = "Can not add a null Collection to AttributeList"
            r6.<init>(r7)
            throw r6
        L_0x0038:
            int r0 = r11.size()
            if (r0 != 0) goto L_0x0040
            r6 = 0
        L_0x003f:
            return r6
        L_0x0040:
            if (r0 != r7) goto L_0x0051
            java.util.Iterator r6 = r11.iterator()
            java.lang.Object r6 = r6.next()
            org.jdom2.Attribute r6 = (org.jdom2.Attribute) r6
            r9.add(r10, r6)
            r6 = r7
            goto L_0x003f
        L_0x0051:
            int r6 = r9.size()
            int r6 = r6 + r0
            r9.ensureCapacity(r6)
            int r5 = r9.modCount
            r4 = 0
            r2 = 0
            java.util.Iterator r3 = r11.iterator()     // Catch:{ all -> 0x0086 }
        L_0x0061:
            boolean r6 = r3.hasNext()     // Catch:{ all -> 0x0086 }
            if (r6 == 0) goto L_0x0075
            java.lang.Object r1 = r3.next()     // Catch:{ all -> 0x0086 }
            org.jdom2.Attribute r1 = (org.jdom2.Attribute) r1     // Catch:{ all -> 0x0086 }
            int r6 = r10 + r2
            r9.add(r6, r1)     // Catch:{ all -> 0x0086 }
            int r2 = r2 + 1
            goto L_0x0061
        L_0x0075:
            r4 = 1
            if (r4 != 0) goto L_0x0084
        L_0x0078:
            int r2 = r2 + -1
            if (r2 < 0) goto L_0x0082
            int r6 = r10 + r2
            r9.remove(r6)
            goto L_0x0078
        L_0x0082:
            r9.modCount = r5
        L_0x0084:
            r6 = r7
            goto L_0x003f
        L_0x0086:
            r6 = move-exception
            if (r4 != 0) goto L_0x0095
        L_0x0089:
            int r2 = r2 + -1
            if (r2 < 0) goto L_0x0093
            int r7 = r10 + r2
            r9.remove(r7)
            goto L_0x0089
        L_0x0093:
            r9.modCount = r5
        L_0x0095:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jdom2.AttributeList.addAll(int, java.util.Collection):boolean");
    }

    public void clear() {
        if (this.attributeData != null) {
            while (this.size > 0) {
                this.size--;
                this.attributeData[this.size].setParent(null);
                this.attributeData[this.size] = null;
            }
        }
        this.modCount++;
    }

    /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    void clearAndSet(java.util.Collection<? extends org.jdom2.Attribute> r9) {
        /*
            r8 = this;
            r6 = 0
            r5 = 0
            if (r9 == 0) goto L_0x000a
            boolean r4 = r9.isEmpty()
            if (r4 == 0) goto L_0x000e
        L_0x000a:
            r8.clear()
        L_0x000d:
            return
        L_0x000e:
            org.jdom2.Attribute[] r1 = r8.attributeData
            int r3 = r8.size
            int r2 = r8.modCount
        L_0x0014:
            int r4 = r8.size
            if (r4 <= 0) goto L_0x0024
            int r4 = r8.size
            int r4 = r4 + -1
            r8.size = r4
            r4 = r1[r4]
            r4.setParent(r6)
            goto L_0x0014
        L_0x0024:
            r8.size = r5
            r8.attributeData = r6
            r0 = 0
            r4 = 0
            r8.addAll(r4, r9)     // Catch:{ all -> 0x0049 }
            r0 = 1
            if (r0 != 0) goto L_0x000d
            r8.attributeData = r1
        L_0x0032:
            int r4 = r8.size
            if (r4 >= r3) goto L_0x0046
            org.jdom2.Attribute[] r4 = r8.attributeData
            int r5 = r8.size
            int r6 = r5 + 1
            r8.size = r6
            r4 = r4[r5]
            org.jdom2.Element r5 = r8.parent
            r4.setParent(r5)
            goto L_0x0032
        L_0x0046:
            r8.modCount = r2
            goto L_0x000d
        L_0x0049:
            r4 = move-exception
            if (r0 != 0) goto L_0x0064
            r8.attributeData = r1
        L_0x004e:
            int r5 = r8.size
            if (r5 >= r3) goto L_0x0062
            org.jdom2.Attribute[] r5 = r8.attributeData
            int r6 = r8.size
            int r7 = r6 + 1
            r8.size = r7
            r5 = r5[r6]
            org.jdom2.Element r6 = r8.parent
            r5.setParent(r6)
            goto L_0x004e
        L_0x0062:
            r8.modCount = r2
        L_0x0064:
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jdom2.AttributeList.clearAndSet(java.util.Collection):void");
    }

    private void ensureCapacity(int minCapacity) {
        if (this.attributeData == null) {
            this.attributeData = new Attribute[Math.max(minCapacity, 4)];
        } else if (minCapacity >= this.attributeData.length) {
            this.attributeData = (Attribute[]) ArrayCopy.copyOf(this.attributeData, ((minCapacity + 4) >>> 1) << 1);
        }
    }

    public Attribute get(int index) {
        if (index >= 0 && index < this.size) {
            return this.attributeData[index];
        }
        throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
    }

    /* access modifiers changed from: package-private */
    public Attribute get(String name, Namespace namespace) {
        int index = indexOf(name, namespace);
        if (index < 0) {
            return null;
        }
        return this.attributeData[index];
    }

    /* access modifiers changed from: package-private */
    public int indexOf(String name, Namespace namespace) {
        if (this.attributeData != null) {
            if (namespace == null) {
                return indexOf(name, Namespace.NO_NAMESPACE);
            }
            String uri = namespace.getURI();
            for (int i = 0; i < this.size; i++) {
                Attribute att = this.attributeData[i];
                if (uri.equals(att.getNamespaceURI()) && name.equals(att.getName())) {
                    return i;
                }
            }
        }
        return -1;
    }

    public Attribute remove(int index) {
        if (index < 0 || index >= this.size) {
            throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
        }
        Attribute old = this.attributeData[index];
        old.setParent(null);
        System.arraycopy(this.attributeData, index + 1, this.attributeData, index, (this.size - index) - 1);
        Attribute[] attributeArr = this.attributeData;
        int i = this.size - 1;
        this.size = i;
        attributeArr[i] = null;
        this.modCount++;
        return old;
    }

    /* access modifiers changed from: package-private */
    public boolean remove(String name, Namespace namespace) {
        int index = indexOf(name, namespace);
        if (index < 0) {
            return false;
        }
        remove(index);
        return true;
    }

    public Attribute set(int index, Attribute attribute) {
        if (index < 0 || index >= this.size) {
            throw new IndexOutOfBoundsException("Index: " + index + " Size: " + size());
        } else if (attribute.getParent() != null) {
            throw new IllegalAddException("The attribute already has an existing parent \"" + attribute.getParent().getQualifiedName() + "\"");
        } else {
            int duplicate = indexOfDuplicate(attribute);
            if (duplicate < 0 || duplicate == index) {
                String reason = Verifier.checkNamespaceCollision(attribute, this.parent, index);
                if (reason != null) {
                    throw new IllegalAddException(this.parent, attribute, reason);
                }
                Attribute old = this.attributeData[index];
                old.setParent(null);
                this.attributeData[index] = attribute;
                attribute.setParent(this.parent);
                return old;
            }
            throw new IllegalAddException("Cannot set duplicate attribute");
        }
    }

    private int indexOfDuplicate(Attribute attribute) {
        return indexOf(attribute.getName(), attribute.getNamespace());
    }

    public Iterator<Attribute> iterator() {
        return new ALIterator();
    }

    public int size() {
        return this.size;
    }

    public boolean isEmpty() {
        return this.size == 0;
    }

    public String toString() {
        return super.toString();
    }

    private final int binarySearch(int[] indexes, int len, int val, Comparator<? super Attribute> comp) {
        int left = 0;
        int right = len - 1;
        Attribute base = this.attributeData[val];
        while (left <= right) {
            int mid = (left + right) >>> 1;
            int cmp = comp.compare(base, this.attributeData[indexes[mid]]);
            if (cmp == 0) {
                while (cmp == 0 && mid < right && comp.compare(base, this.attributeData[indexes[mid + 1]]) == 0) {
                    mid++;
                }
                return mid + 1;
            } else if (cmp < 0) {
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return left;
    }

    private void sortInPlace(int[] indexes) {
        int[] unsorted = ArrayCopy.copyOf(indexes, indexes.length);
        Arrays.sort(unsorted);
        Attribute[] usc = new Attribute[unsorted.length];
        for (int i = 0; i < usc.length; i++) {
            usc[i] = this.attributeData[indexes[i]];
        }
        for (int i2 = 0; i2 < indexes.length; i2++) {
            this.attributeData[unsorted[i2]] = usc[i2];
        }
    }

    /* access modifiers changed from: package-private */
    public void sort(Comparator<? super Attribute> comp) {
        int sz = this.size;
        int[] indexes = new int[sz];
        for (int i = 0; i < sz; i++) {
            int ip = binarySearch(indexes, i, i, comp);
            if (ip < i) {
                System.arraycopy(indexes, ip, indexes, ip + 1, i - ip);
            }
            indexes[ip] = i;
        }
        sortInPlace(indexes);
    }

    private final class ALIterator implements Iterator<Attribute> {
        private boolean canremove;
        private int cursor;
        private int expect;

        private ALIterator() {
            this.expect = -1;
            this.cursor = 0;
            this.canremove = false;
            this.expect = AttributeList.this.modCount;
        }

        public boolean hasNext() {
            return this.cursor < AttributeList.this.size;
        }

        public Attribute next() {
            if (AttributeList.this.modCount != this.expect) {
                throw new ConcurrentModificationException("ContentList was modified outside of this Iterator");
            } else if (this.cursor >= AttributeList.this.size) {
                throw new NoSuchElementException("Iterated beyond the end of the ContentList.");
            } else {
                this.canremove = true;
                Attribute[] access$400 = AttributeList.this.attributeData;
                int i = this.cursor;
                this.cursor = i + 1;
                return access$400[i];
            }
        }

        public void remove() {
            if (AttributeList.this.modCount != this.expect) {
                throw new ConcurrentModificationException("ContentList was modified outside of this Iterator");
            } else if (!this.canremove) {
                throw new IllegalStateException("Can only remove() content after a call to next()");
            } else {
                AttributeList attributeList = AttributeList.this;
                int i = this.cursor - 1;
                this.cursor = i;
                attributeList.remove(i);
                this.expect = AttributeList.this.modCount;
                this.canremove = false;
            }
        }
    }
}
