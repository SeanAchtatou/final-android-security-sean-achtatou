package org.jdom2.internal;

import java.lang.reflect.InvocationTargetException;

public class ReflectionConstructor {
    public static final <E> E construct(String classname, Class<E> targetclass) {
        try {
            Class<?> sclass = Class.forName(classname);
            if (targetclass.isAssignableFrom(sclass)) {
                return targetclass.cast(sclass.getConstructor(new Class[0]).newInstance(new Object[0]));
            }
            throw new ClassCastException("Class '" + classname + "' is not assignable to '" + targetclass.getName() + "'.");
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException("Unable to locate class '" + classname + "'.", e);
        } catch (NoSuchMethodException e2) {
            throw new IllegalArgumentException("Unable to locate class no-arg constructor '" + classname + "'.", e2);
        } catch (SecurityException e3) {
            throw new IllegalStateException("Unable to access class constructor '" + classname + "'.", e3);
        } catch (IllegalAccessException e4) {
            throw new IllegalStateException("Unable to access class constructor '" + classname + "'.", e4);
        } catch (InstantiationException e5) {
            throw new IllegalStateException("Unable to instantiate class '" + classname + "'.", e5);
        } catch (InvocationTargetException e6) {
            throw new IllegalStateException("Unable to call class constructor '" + classname + "'.", e6);
        }
    }
}
