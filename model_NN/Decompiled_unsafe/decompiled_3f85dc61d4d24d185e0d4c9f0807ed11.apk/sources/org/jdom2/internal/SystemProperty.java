package org.jdom2.internal;

public final class SystemProperty {
    public static final String get(String property, String def) {
        try {
            return System.getProperty(property, def);
        } catch (SecurityException e) {
            return def;
        }
    }
}
