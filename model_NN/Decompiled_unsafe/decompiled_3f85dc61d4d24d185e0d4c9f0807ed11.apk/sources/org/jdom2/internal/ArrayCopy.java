package org.jdom2.internal;

import java.lang.reflect.Array;

public final class ArrayCopy {
    private ArrayCopy() {
    }

    public static final <E> E[] copyOf(Object[] objArr, int len) {
        E[] dest = (Object[]) ((Object[]) Array.newInstance(objArr.getClass().getComponentType(), len));
        if (len >= objArr.length) {
            len = objArr.length;
        }
        System.arraycopy(objArr, 0, dest, 0, len);
        return dest;
    }

    public static final <E> E[] copyOfRange(E[] source, int from, int to) {
        int tocopy;
        int len = to - from;
        if (len < 0) {
            throw new IllegalArgumentException("From(" + from + ") > To (" + to + ")");
        }
        E[] dest = (Object[]) ((Object[]) Array.newInstance(source.getClass().getComponentType(), len));
        if (from + len > source.length) {
            tocopy = source.length - from;
        } else {
            tocopy = len;
        }
        System.arraycopy(source, from, dest, 0, tocopy);
        return dest;
    }

    public static final char[] copyOf(char[] source, int len) {
        char[] dest = new char[len];
        if (len >= source.length) {
            len = source.length;
        }
        System.arraycopy(source, 0, dest, 0, len);
        return dest;
    }

    public static final int[] copyOf(int[] source, int len) {
        int[] dest = new int[len];
        if (len >= source.length) {
            len = source.length;
        }
        System.arraycopy(source, 0, dest, 0, len);
        return dest;
    }

    public static final boolean[] copyOf(boolean[] source, int len) {
        boolean[] dest = new boolean[len];
        if (len >= source.length) {
            len = source.length;
        }
        System.arraycopy(source, 0, dest, 0, len);
        return dest;
    }
}
