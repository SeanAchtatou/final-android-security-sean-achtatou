package org.jdom2.input;

import java.util.HashMap;
import java.util.Iterator;
import org.jdom2.CDATA;
import org.jdom2.Comment;
import org.jdom2.DefaultJDOMFactory;
import org.jdom2.DocType;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.EntityRef;
import org.jdom2.JDOMFactory;
import org.jdom2.Namespace;
import org.jdom2.ProcessingInstruction;
import org.jdom2.Text;
import org.w3c.dom.Attr;
import org.w3c.dom.CDATASection;
import org.w3c.dom.DocumentType;
import org.w3c.dom.EntityReference;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DOMBuilder {
    private JDOMFactory factory = new DefaultJDOMFactory();

    public void setFactory(JDOMFactory factory2) {
        this.factory = factory2;
    }

    public JDOMFactory getFactory() {
        return this.factory;
    }

    public Document build(org.w3c.dom.Document domDocument) {
        Document doc = this.factory.document(null);
        buildTree(domDocument, doc, null, true);
        return doc;
    }

    public Element build(org.w3c.dom.Element domElement) {
        Document doc = this.factory.document(null);
        buildTree(domElement, doc, null, true);
        return doc.getRootElement();
    }

    public CDATA build(CDATASection cdata) {
        return this.factory.cdata(cdata.getNodeValue());
    }

    public Text build(org.w3c.dom.Text text) {
        return this.factory.text(text.getNodeValue());
    }

    public Comment build(org.w3c.dom.Comment comment) {
        return this.factory.comment(comment.getNodeValue());
    }

    public ProcessingInstruction build(org.w3c.dom.ProcessingInstruction pi) {
        return this.factory.processingInstruction(pi.getTarget(), pi.getData());
    }

    public EntityRef build(EntityReference er) {
        return this.factory.entityRef(er.getNodeName());
    }

    public DocType build(DocumentType doctype) {
        String publicID = doctype.getPublicId();
        String systemID = doctype.getSystemId();
        String internalDTD = doctype.getInternalSubset();
        DocType docType = this.factory.docType(doctype.getName());
        docType.setPublicID(publicID);
        docType.setSystemID(systemID);
        docType.setInternalSubset(internalDTD);
        return docType;
    }

    private void buildTree(Node node, Document doc, Element current, boolean atRoot) {
        Namespace ns;
        switch (node.getNodeType()) {
            case 1:
                String nodeName = node.getNodeName();
                String prefix = "";
                String localName = nodeName;
                int colon = nodeName.indexOf(58);
                if (colon >= 0) {
                    prefix = nodeName.substring(0, colon);
                    localName = nodeName.substring(colon + 1);
                }
                String uri = node.getNamespaceURI();
                if (uri == null) {
                    ns = current == null ? Namespace.NO_NAMESPACE : current.getNamespace(prefix);
                } else {
                    ns = Namespace.getNamespace(prefix, uri);
                }
                Element element = this.factory.element(localName, ns);
                if (atRoot) {
                    this.factory.setRoot(doc, element);
                } else {
                    this.factory.addContent(current, element);
                }
                NamedNodeMap attributeList = node.getAttributes();
                int attsize = attributeList.getLength();
                for (int i = 0; i < attsize; i++) {
                    Attr att = (Attr) attributeList.item(i);
                    String attname = att.getName();
                    if (attname.startsWith("xmlns")) {
                        String attPrefix = "";
                        int colon2 = attname.indexOf(58);
                        if (colon2 >= 0) {
                            attPrefix = attname.substring(colon2 + 1);
                        }
                        Namespace declaredNS = Namespace.getNamespace(attPrefix, att.getValue());
                        if (prefix.equals(attPrefix)) {
                            element.setNamespace(declaredNS);
                        } else {
                            this.factory.addNamespaceDeclaration(element, declaredNS);
                        }
                    }
                }
                for (int i2 = 0; i2 < attsize; i2++) {
                    Attr att2 = (Attr) attributeList.item(i2);
                    String attname2 = att2.getName();
                    if (!attname2.startsWith("xmlns")) {
                        String attPrefix2 = "";
                        String attLocalName = attname2;
                        int colon3 = attname2.indexOf(58);
                        if (colon3 >= 0) {
                            attPrefix2 = attname2.substring(0, colon3);
                            attLocalName = attname2.substring(colon3 + 1);
                        }
                        String attvalue = att2.getValue();
                        Namespace attNS = null;
                        String attURI = att2.getNamespaceURI();
                        if (attURI == null || "".equals(attURI)) {
                            attNS = Namespace.NO_NAMESPACE;
                        } else if (attPrefix2.length() > 0) {
                            attNS = Namespace.getNamespace(attPrefix2, attURI);
                        } else {
                            HashMap<String, Namespace> tmpmap = new HashMap<>();
                            Iterator i$ = element.getNamespacesInScope().iterator();
                            while (true) {
                                if (i$.hasNext()) {
                                    Namespace nss = i$.next();
                                    if (nss.getPrefix().length() <= 0 || !nss.getURI().equals(attURI)) {
                                        tmpmap.put(nss.getPrefix(), nss);
                                    } else {
                                        attNS = nss;
                                    }
                                }
                            }
                            if (attNS == null) {
                                int cnt = 0;
                                String pfx = "attns" + 0;
                                while (tmpmap.containsKey(pfx)) {
                                    cnt++;
                                    pfx = "attns" + cnt;
                                }
                                attNS = Namespace.getNamespace(pfx, attURI);
                            }
                        }
                        this.factory.setAttribute(element, this.factory.attribute(attLocalName, attvalue, attNS));
                    }
                }
                NodeList children = node.getChildNodes();
                if (children != null) {
                    int size = children.getLength();
                    for (int i3 = 0; i3 < size; i3++) {
                        Node item = children.item(i3);
                        if (item != null) {
                            buildTree(item, doc, element, false);
                        }
                    }
                    return;
                }
                return;
            case 2:
            case 6:
            default:
                return;
            case 3:
                this.factory.addContent(current, build((org.w3c.dom.Text) node));
                return;
            case 4:
                this.factory.addContent(current, build((CDATASection) node));
                return;
            case 5:
                this.factory.addContent(current, build((EntityReference) node));
                return;
            case 7:
                if (atRoot) {
                    this.factory.addContent(doc, build((org.w3c.dom.ProcessingInstruction) node));
                    return;
                } else {
                    this.factory.addContent(current, build((org.w3c.dom.ProcessingInstruction) node));
                    return;
                }
            case 8:
                if (atRoot) {
                    this.factory.addContent(doc, build((org.w3c.dom.Comment) node));
                    return;
                } else {
                    this.factory.addContent(current, build((org.w3c.dom.Comment) node));
                    return;
                }
            case 9:
                NodeList nodes = node.getChildNodes();
                int size2 = nodes.getLength();
                for (int i4 = 0; i4 < size2; i4++) {
                    buildTree(nodes.item(i4), doc, current, true);
                }
                return;
            case 10:
                this.factory.addContent(doc, build((DocumentType) node));
                return;
        }
    }
}
