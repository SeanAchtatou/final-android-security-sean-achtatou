package org.jdom2.input;

import java.util.ArrayList;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import org.jdom2.AttributeType;
import org.jdom2.Content;
import org.jdom2.DefaultJDOMFactory;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.JDOMFactory;
import org.jdom2.Namespace;
import org.jdom2.Verifier;
import org.jdom2.input.stax.DTDParser;
import org.jdom2.input.stax.StAXFilter;

public class StAXStreamBuilder {
    private JDOMFactory builderfactory = new DefaultJDOMFactory();

    private static final Document process(JDOMFactory factory, XMLStreamReader stream) throws JDOMException {
        try {
            int state = stream.getEventType();
            if (7 != state) {
                throw new JDOMException("JDOM requires that XMLStreamReaders are at their beginning when being processed.");
            }
            Document document = factory.document(null);
            while (state != 8) {
                switch (state) {
                    case 1:
                        document.setRootElement(processElementFragment(factory, stream));
                        break;
                    case 2:
                        throw new JDOMException("Unexpected XMLStream event at Document level: END_ELEMENT");
                    case 3:
                        document.addContent((Content) factory.processingInstruction(stream.getPITarget(), stream.getPIData()));
                        break;
                    case 4:
                        String badtxt = stream.getText();
                        if (!Verifier.isAllXMLWhitespace(badtxt)) {
                            throw new JDOMException("Unexpected XMLStream event at Document level: CHARACTERS (" + badtxt + ")");
                        }
                        break;
                    case 5:
                        document.addContent((Content) factory.comment(stream.getText()));
                        break;
                    case 6:
                        break;
                    case 7:
                        document.setBaseURI(stream.getLocation().getSystemId());
                        document.setProperty("ENCODING_SCHEME", stream.getCharacterEncodingScheme());
                        document.setProperty("STANDALONE", String.valueOf(stream.isStandalone()));
                        document.setProperty("ENCODING", stream.getEncoding());
                        break;
                    case 8:
                    case 10:
                    default:
                        throw new JDOMException("Unexpected XMLStream event " + state);
                    case 9:
                        throw new JDOMException("Unexpected XMLStream event at Document level: ENTITY_REFERENCE");
                    case 11:
                        document.setDocType(DTDParser.parse(stream.getText(), factory));
                        break;
                    case 12:
                        throw new JDOMException("Unexpected XMLStream event at Document level: CDATA");
                }
                if (stream.hasNext()) {
                    state = stream.next();
                } else {
                    throw new JDOMException("Unexpected end-of-XMLStreamReader");
                }
            }
            return document;
        } catch (XMLStreamException xse) {
            throw new JDOMException("Unable to process XMLStream. See Cause.", xse);
        }
    }

    private List<Content> processFragments(JDOMFactory factory, XMLStreamReader stream, StAXFilter filter) throws JDOMException {
        int state;
        if (7 != stream.getEventType()) {
            throw new JDOMException("JDOM requires that XMLStreamReaders are at their beginning when being processed.");
        }
        List<Content> ret = new ArrayList<>();
        int depth = 0;
        while (stream.hasNext() && (state = stream.next()) != 8) {
            try {
                switch (state) {
                    case 1:
                        QName qn = stream.getName();
                        if (filter.includeElement(depth, qn.getLocalPart(), Namespace.getNamespace(qn.getPrefix(), qn.getNamespaceURI()))) {
                            ret.add(processPrunableElement(factory, stream, depth, filter));
                            break;
                        } else {
                            int back = depth;
                            depth++;
                            while (depth > back && stream.hasNext()) {
                                int state2 = stream.next();
                                if (state2 == 1) {
                                    depth++;
                                } else if (state2 == 2) {
                                    depth--;
                                }
                            }
                        }
                        break;
                    case 2:
                        throw new JDOMException("Illegal state for XMLStreamReader. Cannot get XML Fragment for state END_ELEMENT");
                    case 3:
                        if (!filter.includeProcessingInstruction(depth, stream.getPITarget())) {
                            break;
                        } else {
                            ret.add(factory.processingInstruction(stream.getPITarget(), stream.getPIData()));
                            break;
                        }
                    case 4:
                    case 6:
                        String text = filter.includeText(depth, stream.getText());
                        if (text == null) {
                            break;
                        } else {
                            ret.add(factory.text(text));
                            break;
                        }
                    case 5:
                        String text2 = filter.includeComment(depth, stream.getText());
                        if (text2 == null) {
                            break;
                        } else {
                            ret.add(factory.comment(text2));
                            break;
                        }
                    case 7:
                        throw new JDOMException("Illegal state for XMLStreamReader. Cannot get XML Fragment for state START_DOCUMENT");
                    case 8:
                        throw new JDOMException("Illegal state for XMLStreamReader. Cannot get XML Fragment for state END_DOCUMENT");
                    case 9:
                        if (!filter.includeEntityRef(depth, stream.getLocalName())) {
                            break;
                        } else {
                            ret.add(factory.entityRef(stream.getLocalName()));
                            break;
                        }
                    case 10:
                    default:
                        throw new JDOMException("Unexpected XMLStream event " + stream.getEventType());
                    case 11:
                        if (!filter.includeDocType()) {
                            break;
                        } else {
                            ret.add(DTDParser.parse(stream.getText(), factory));
                            break;
                        }
                    case 12:
                        String text3 = filter.includeCDATA(depth, stream.getText());
                        if (text3 == null) {
                            break;
                        } else {
                            ret.add(factory.cdata(text3));
                            break;
                        }
                }
            } catch (XMLStreamException e) {
                throw new JDOMException("Unable to process fragments from XMLStreamReader.", e);
            }
        }
        return ret;
    }

    private static final Element processPrunableElement(JDOMFactory factory, XMLStreamReader reader, int topdepth, StAXFilter filter) throws XMLStreamException, JDOMException {
        if (1 != reader.getEventType()) {
            throw new JDOMException("JDOM requires that the XMLStreamReader is at the START_ELEMENT state when retrieving an Element Fragment.");
        }
        Element fragment = processElement(factory, reader);
        Element current = fragment;
        int depth = topdepth + 1;
        while (depth > topdepth && reader.hasNext()) {
            switch (reader.next()) {
                case 1:
                    QName qn = reader.getName();
                    if (filter.pruneElement(depth, qn.getLocalPart(), Namespace.getNamespace(qn.getPrefix(), qn.getNamespaceURI()))) {
                        int edepth = depth;
                        depth++;
                        while (depth > edepth && reader.hasNext()) {
                            int state = reader.next();
                            if (state == 8) {
                                break;
                            } else if (state == 1) {
                                depth++;
                            } else if (state == 2) {
                                depth--;
                            }
                        }
                    } else {
                        Element tmp = processElement(factory, reader);
                        current.addContent((Content) tmp);
                        current = tmp;
                        depth++;
                        break;
                    }
                    break;
                case 2:
                    current = current.getParentElement();
                    depth--;
                    break;
                case 3:
                    if (filter.pruneProcessingInstruction(depth, reader.getPITarget())) {
                        break;
                    } else {
                        current.addContent((Content) factory.processingInstruction(reader.getPITarget(), reader.getPIData()));
                        break;
                    }
                case 4:
                case 6:
                    String text = filter.pruneText(depth, reader.getText());
                    if (text == null) {
                        break;
                    } else {
                        current.addContent((Content) factory.text(text));
                        break;
                    }
                case 5:
                    String text2 = filter.pruneComment(depth, reader.getText());
                    if (text2 == null) {
                        break;
                    } else {
                        current.addContent((Content) factory.comment(text2));
                        break;
                    }
                case 7:
                case 8:
                case 10:
                case 11:
                default:
                    throw new JDOMException("Unexpected XMLStream event " + reader.getEventType());
                case 9:
                    if (filter.pruneEntityRef(depth, reader.getLocalName())) {
                        break;
                    } else {
                        current.addContent((Content) factory.entityRef(reader.getLocalName()));
                        break;
                    }
                case 12:
                    String text3 = filter.pruneCDATA(depth, reader.getText());
                    if (text3 == null) {
                        break;
                    } else {
                        current.addContent((Content) factory.cdata(text3));
                        break;
                    }
            }
        }
        return fragment;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    private static final Content processFragment(JDOMFactory factory, XMLStreamReader stream) throws JDOMException {
        try {
            switch (stream.getEventType()) {
                case 1:
                    Element emt = processElementFragment(factory, stream);
                    stream.next();
                    return emt;
                case 2:
                    throw new JDOMException("Illegal state for XMLStreamReader. Cannot get XML Fragment for state END_ELEMENT");
                case 3:
                    Content pi = factory.processingInstruction(stream.getPITarget(), stream.getPIData());
                    stream.next();
                    return pi;
                case 4:
                case 6:
                    Content txt = factory.text(stream.getText());
                    stream.next();
                    return txt;
                case 5:
                    Content comment = factory.comment(stream.getText());
                    stream.next();
                    return comment;
                case 7:
                    throw new JDOMException("Illegal state for XMLStreamReader. Cannot get XML Fragment for state START_DOCUMENT");
                case 8:
                    throw new JDOMException("Illegal state for XMLStreamReader. Cannot get XML Fragment for state END_DOCUMENT");
                case 9:
                    Content er = factory.entityRef(stream.getLocalName());
                    stream.next();
                    return er;
                case 10:
                default:
                    throw new JDOMException("Unexpected XMLStream event " + stream.getEventType());
                case 11:
                    Content dt = DTDParser.parse(stream.getText(), factory);
                    stream.next();
                    return dt;
                case 12:
                    Content cd = factory.cdata(stream.getText());
                    stream.next();
                    return cd;
            }
        } catch (XMLStreamException xse) {
            throw new JDOMException("Unable to process XMLStream. See Cause.", xse);
        }
        throw new JDOMException("Unable to process XMLStream. See Cause.", xse);
    }

    private static final Element processElementFragment(JDOMFactory factory, XMLStreamReader reader) throws XMLStreamException, JDOMException {
        if (1 != reader.getEventType()) {
            throw new JDOMException("JDOM requires that the XMLStreamReader is at the START_ELEMENT state when retrieving an Element Fragment.");
        }
        Element fragment = processElement(factory, reader);
        Element current = fragment;
        int depth = 1;
        while (depth > 0 && reader.hasNext()) {
            switch (reader.next()) {
                case 1:
                    Element tmp = processElement(factory, reader);
                    current.addContent((Content) tmp);
                    current = tmp;
                    depth++;
                    break;
                case 2:
                    current = current.getParentElement();
                    depth--;
                    break;
                case 3:
                    current.addContent((Content) factory.processingInstruction(reader.getPITarget(), reader.getPIData()));
                    break;
                case 4:
                case 6:
                    current.addContent((Content) factory.text(reader.getText()));
                    break;
                case 5:
                    current.addContent((Content) factory.comment(reader.getText()));
                    break;
                case 7:
                case 8:
                case 10:
                case 11:
                default:
                    throw new JDOMException("Unexpected XMLStream event " + reader.getEventType());
                case 9:
                    current.addContent((Content) factory.entityRef(reader.getLocalName()));
                    break;
                case 12:
                    current.addContent((Content) factory.cdata(reader.getText()));
                    break;
            }
        }
        return fragment;
    }

    private static final Element processElement(JDOMFactory factory, XMLStreamReader reader) {
        Element element = factory.element(reader.getLocalName(), Namespace.getNamespace(reader.getPrefix(), reader.getNamespaceURI()));
        int len = reader.getAttributeCount();
        for (int i = 0; i < len; i++) {
            factory.setAttribute(element, factory.attribute(reader.getAttributeLocalName(i), reader.getAttributeValue(i), AttributeType.getAttributeType(reader.getAttributeType(i)), Namespace.getNamespace(reader.getAttributePrefix(i), reader.getAttributeNamespace(i))));
        }
        int len2 = reader.getNamespaceCount();
        for (int i2 = 0; i2 < len2; i2++) {
            element.addNamespaceDeclaration(Namespace.getNamespace(reader.getNamespacePrefix(i2), reader.getNamespaceURI(i2)));
        }
        return element;
    }

    public JDOMFactory getFactory() {
        return this.builderfactory;
    }

    public void setFactory(JDOMFactory factory) {
        this.builderfactory = factory;
    }

    public Document build(XMLStreamReader reader) throws JDOMException {
        return process(this.builderfactory, reader);
    }

    public List<Content> buildFragments(XMLStreamReader reader, StAXFilter filter) throws JDOMException {
        return processFragments(this.builderfactory, reader, filter);
    }

    public Content fragment(XMLStreamReader reader) throws JDOMException {
        return processFragment(this.builderfactory, reader);
    }
}
