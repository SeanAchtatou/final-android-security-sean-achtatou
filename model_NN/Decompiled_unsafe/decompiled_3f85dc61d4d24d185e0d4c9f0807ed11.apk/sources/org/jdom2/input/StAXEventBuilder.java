package org.jdom2.input;

import java.util.Iterator;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.Comment;
import javax.xml.stream.events.DTD;
import javax.xml.stream.events.EntityReference;
import javax.xml.stream.events.StartDocument;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import org.jdom2.AttributeType;
import org.jdom2.Content;
import org.jdom2.DefaultJDOMFactory;
import org.jdom2.DocType;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.JDOMFactory;
import org.jdom2.Namespace;
import org.jdom2.ProcessingInstruction;
import org.jdom2.input.stax.DTDParser;

public class StAXEventBuilder {
    private JDOMFactory factory = new DefaultJDOMFactory();

    private static final Document process(JDOMFactory factory2, XMLEventReader events) throws JDOMException {
        try {
            Document document = factory2.document(null);
            Element current = null;
            XMLEvent event = events.peek();
            if (7 != event.getEventType()) {
                throw new JDOMException("JDOM requires that XMLStreamReaders are at their beginning when being processed.");
            }
            while (event.getEventType() != 8) {
                if (event.isStartDocument()) {
                    document.setBaseURI(event.getLocation().getSystemId());
                    document.setProperty("ENCODING_SCHEME", ((StartDocument) event).getCharacterEncodingScheme());
                    document.setProperty("STANDALONE", String.valueOf(((StartDocument) event).isStandalone()));
                } else if (event instanceof DTD) {
                    document.setDocType(DTDParser.parse(((DTD) event).getDocumentTypeDeclaration(), factory2));
                } else if (event.isStartElement()) {
                    Element emt = processElement(factory2, event.asStartElement());
                    if (current == null) {
                        document.setRootElement(emt);
                        DocType dt = document.getDocType();
                        if (dt != null) {
                            dt.setElementName(emt.getName());
                        }
                    } else {
                        current.addContent((Content) emt);
                    }
                    current = emt;
                } else if (!event.isCharacters() || current == null) {
                    if (event instanceof Comment) {
                        org.jdom2.Comment comment = factory2.comment(((Comment) event).getText());
                        if (current == null) {
                            document.addContent((Content) comment);
                        } else {
                            current.addContent((Content) comment);
                        }
                    } else if (event.isEntityReference()) {
                        current.addContent((Content) factory2.entityRef(((EntityReference) event).getName()));
                    } else if (event.isProcessingInstruction()) {
                        ProcessingInstruction pi = factory2.processingInstruction(event.getTarget(), ((javax.xml.stream.events.ProcessingInstruction) event).getData());
                        if (current == null) {
                            document.addContent((Content) pi);
                        } else {
                            current.addContent((Content) pi);
                        }
                    } else if (event.isEndElement()) {
                        current = current.getParentElement();
                    }
                } else if (event.asCharacters().isCData()) {
                    current.addContent((Content) factory2.cdata(((Characters) event).getData()));
                } else {
                    current.addContent((Content) factory2.text(((Characters) event).getData()));
                }
                if (!events.hasNext()) {
                    break;
                }
                event = events.nextEvent();
            }
            return document;
        } catch (XMLStreamException xse) {
            throw new JDOMException("Unable to process XMLStream. See Cause.", xse);
        }
    }

    private static final Element processElement(JDOMFactory factory2, StartElement event) {
        QName qname = event.getName();
        Element element = factory2.element(qname.getLocalPart(), Namespace.getNamespace(qname.getPrefix(), qname.getNamespaceURI()));
        Iterator<?> it = event.getAttributes();
        while (it.hasNext()) {
            Attribute att = (Attribute) it.next();
            QName aqname = att.getName();
            factory2.setAttribute(element, factory2.attribute(aqname.getLocalPart(), att.getValue(), AttributeType.getAttributeType(att.getDTDType()), Namespace.getNamespace(aqname.getPrefix(), aqname.getNamespaceURI())));
        }
        Iterator<?> it2 = event.getNamespaces();
        while (it2.hasNext()) {
            javax.xml.stream.events.Namespace ns = (javax.xml.stream.events.Namespace) it2.next();
            element.addNamespaceDeclaration(Namespace.getNamespace(ns.getPrefix(), ns.getNamespaceURI()));
        }
        return element;
    }

    public JDOMFactory getFactory() {
        return this.factory;
    }

    public void setFactory(JDOMFactory factory2) {
        this.factory = factory2;
    }

    public Document build(XMLEventReader events) throws JDOMException {
        return process(this.factory, events);
    }
}
