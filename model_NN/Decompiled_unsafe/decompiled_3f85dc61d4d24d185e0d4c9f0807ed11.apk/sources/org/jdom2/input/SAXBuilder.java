package org.jdom2.input;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import org.jdom2.DefaultJDOMFactory;
import org.jdom2.Document;
import org.jdom2.JDOMConstants;
import org.jdom2.JDOMException;
import org.jdom2.JDOMFactory;
import org.jdom2.Verifier;
import org.jdom2.input.sax.BuilderErrorHandler;
import org.jdom2.input.sax.DefaultSAXHandlerFactory;
import org.jdom2.input.sax.SAXBuilderEngine;
import org.jdom2.input.sax.SAXEngine;
import org.jdom2.input.sax.SAXHandler;
import org.jdom2.input.sax.SAXHandlerFactory;
import org.jdom2.input.sax.XMLReaderJDOMFactory;
import org.jdom2.input.sax.XMLReaderSAX2Factory;
import org.jdom2.input.sax.XMLReaders;
import org.xml.sax.DTDHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.XMLFilter;
import org.xml.sax.XMLReader;

public class SAXBuilder implements SAXEngine {
    private static final JDOMFactory DEFAULTJDOMFAC = new DefaultJDOMFactory();
    private static final SAXHandlerFactory DEFAULTSAXHANDLERFAC = new DefaultSAXHandlerFactory();
    private SAXEngine engine;
    private boolean expand;
    private final HashMap<String, Boolean> features;
    private SAXHandlerFactory handlerfac;
    private boolean ignoringBoundaryWhite;
    private boolean ignoringWhite;
    private JDOMFactory jdomfac;
    private final HashMap<String, Object> properties;
    private XMLReaderJDOMFactory readerfac;
    private boolean reuseParser;
    private DTDHandler saxDTDHandler;
    private EntityResolver saxEntityResolver;
    private ErrorHandler saxErrorHandler;
    private XMLFilter saxXMLFilter;

    public SAXBuilder() {
        this(null, null, null);
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    @Deprecated
    public SAXBuilder(boolean validate) {
        this(validate ? XMLReaders.DTDVALIDATING : XMLReaders.NONVALIDATING, null, null);
    }

    @Deprecated
    public SAXBuilder(String saxDriverClass) {
        this(saxDriverClass, false);
    }

    @Deprecated
    public SAXBuilder(String saxDriverClass, boolean validate) {
        this(new XMLReaderSAX2Factory(validate, saxDriverClass), null, null);
    }

    public SAXBuilder(XMLReaderJDOMFactory readersouce) {
        this(readersouce, null, null);
    }

    public SAXBuilder(XMLReaderJDOMFactory xmlreaderfactory, SAXHandlerFactory handlerfactory, JDOMFactory jdomfactory) {
        this.readerfac = null;
        this.handlerfac = null;
        this.jdomfac = null;
        this.features = new HashMap<>(5);
        this.properties = new HashMap<>(5);
        this.saxErrorHandler = null;
        this.saxEntityResolver = null;
        this.saxDTDHandler = null;
        this.saxXMLFilter = null;
        this.expand = true;
        this.ignoringWhite = false;
        this.ignoringBoundaryWhite = false;
        this.reuseParser = true;
        this.engine = null;
        this.readerfac = xmlreaderfactory == null ? XMLReaders.NONVALIDATING : xmlreaderfactory;
        this.handlerfac = handlerfactory == null ? DEFAULTSAXHANDLERFAC : handlerfactory;
        this.jdomfac = jdomfactory == null ? DEFAULTJDOMFAC : jdomfactory;
    }

    @Deprecated
    public String getDriverClass() {
        if (this.readerfac instanceof XMLReaderSAX2Factory) {
            return ((XMLReaderSAX2Factory) this.readerfac).getDriverClassName();
        }
        return null;
    }

    @Deprecated
    public JDOMFactory getFactory() {
        return getJDOMFactory();
    }

    public JDOMFactory getJDOMFactory() {
        return this.jdomfac;
    }

    @Deprecated
    public void setFactory(JDOMFactory factory) {
        setJDOMFactory(factory);
    }

    public void setJDOMFactory(JDOMFactory factory) {
        this.jdomfac = factory;
        this.engine = null;
    }

    public XMLReaderJDOMFactory getXMLReaderFactory() {
        return this.readerfac;
    }

    public void setXMLReaderFactory(XMLReaderJDOMFactory rfac) {
        if (rfac == null) {
            rfac = XMLReaders.NONVALIDATING;
        }
        this.readerfac = rfac;
        this.engine = null;
    }

    public SAXHandlerFactory getSAXHandlerFactory() {
        return this.handlerfac;
    }

    public void setSAXHandlerFactory(SAXHandlerFactory factory) {
        if (factory == null) {
            factory = DEFAULTSAXHANDLERFAC;
        }
        this.handlerfac = factory;
        this.engine = null;
    }

    @Deprecated
    public boolean getValidation() {
        return isValidating();
    }

    public boolean isValidating() {
        return this.readerfac.isValidating();
    }

    @Deprecated
    public void setValidation(boolean validate) {
        setXMLReaderFactory(validate ? XMLReaders.DTDVALIDATING : XMLReaders.NONVALIDATING);
    }

    public ErrorHandler getErrorHandler() {
        return this.saxErrorHandler;
    }

    public void setErrorHandler(ErrorHandler errorHandler) {
        this.saxErrorHandler = errorHandler;
        this.engine = null;
    }

    public EntityResolver getEntityResolver() {
        return this.saxEntityResolver;
    }

    public void setEntityResolver(EntityResolver entityResolver) {
        this.saxEntityResolver = entityResolver;
        this.engine = null;
    }

    public DTDHandler getDTDHandler() {
        return this.saxDTDHandler;
    }

    public void setDTDHandler(DTDHandler dtdHandler) {
        this.saxDTDHandler = dtdHandler;
        this.engine = null;
    }

    public XMLFilter getXMLFilter() {
        return this.saxXMLFilter;
    }

    public void setXMLFilter(XMLFilter xmlFilter) {
        this.saxXMLFilter = xmlFilter;
        this.engine = null;
    }

    public boolean getIgnoringElementContentWhitespace() {
        return this.ignoringWhite;
    }

    public void setIgnoringElementContentWhitespace(boolean ignoringWhite2) {
        this.ignoringWhite = ignoringWhite2;
        this.engine = null;
    }

    public boolean getIgnoringBoundaryWhitespace() {
        return this.ignoringBoundaryWhite;
    }

    public void setIgnoringBoundaryWhitespace(boolean ignoringBoundaryWhite2) {
        this.ignoringBoundaryWhite = ignoringBoundaryWhite2;
        this.engine = null;
    }

    public boolean getExpandEntities() {
        return this.expand;
    }

    public void setExpandEntities(boolean expand2) {
        this.expand = expand2;
        this.engine = null;
    }

    public boolean getReuseParser() {
        return this.reuseParser;
    }

    public void setReuseParser(boolean reuseParser2) {
        this.reuseParser = reuseParser2;
        if (!reuseParser2) {
            this.engine = null;
        }
    }

    @Deprecated
    public void setFastReconfigure(boolean fastReconfigure) {
    }

    public void setFeature(String name, boolean value) {
        this.features.put(name, value ? Boolean.TRUE : Boolean.FALSE);
        this.engine = null;
    }

    public void setProperty(String name, Object value) {
        this.properties.put(name, value);
        this.engine = null;
    }

    public SAXEngine buildEngine() throws JDOMException {
        SAXHandler contentHandler = this.handlerfac.createSAXHandler(this.jdomfac);
        contentHandler.setExpandEntities(this.expand);
        contentHandler.setIgnoringElementContentWhitespace(this.ignoringWhite);
        contentHandler.setIgnoringBoundaryWhitespace(this.ignoringBoundaryWhite);
        XMLReader parser = createParser();
        configureParser(parser, contentHandler);
        return new SAXBuilderEngine(parser, contentHandler, this.readerfac.isValidating());
    }

    /* access modifiers changed from: protected */
    public XMLReader createParser() throws JDOMException {
        XMLReader parser = this.readerfac.createXMLReader();
        if (this.saxXMLFilter == null) {
            return parser;
        }
        XMLFilter root = this.saxXMLFilter;
        while (root.getParent() instanceof XMLFilter) {
            root = (XMLFilter) root.getParent();
        }
        root.setParent(parser);
        return this.saxXMLFilter;
    }

    private SAXEngine getEngine() throws JDOMException {
        if (this.engine != null) {
            return this.engine;
        }
        this.engine = buildEngine();
        return this.engine;
    }

    /* access modifiers changed from: protected */
    public void configureParser(XMLReader parser, SAXHandler contentHandler) throws JDOMException {
        parser.setContentHandler(contentHandler);
        if (this.saxEntityResolver != null) {
            parser.setEntityResolver(this.saxEntityResolver);
        }
        if (this.saxDTDHandler != null) {
            parser.setDTDHandler(this.saxDTDHandler);
        } else {
            parser.setDTDHandler(contentHandler);
        }
        if (this.saxErrorHandler != null) {
            parser.setErrorHandler(this.saxErrorHandler);
        } else {
            parser.setErrorHandler(new BuilderErrorHandler());
        }
        boolean success = false;
        try {
            parser.setProperty(JDOMConstants.SAX_PROPERTY_LEXICAL_HANDLER, contentHandler);
            success = true;
        } catch (SAXNotRecognizedException | SAXNotSupportedException e) {
        }
        if (!success) {
            try {
                parser.setProperty(JDOMConstants.SAX_PROPERTY_LEXICAL_HANDLER_ALT, contentHandler);
            } catch (SAXNotRecognizedException | SAXNotSupportedException e2) {
            }
        }
        for (Map.Entry<String, Boolean> me2 : this.features.entrySet()) {
            internalSetFeature(parser, (String) me2.getKey(), ((Boolean) me2.getValue()).booleanValue(), (String) me2.getKey());
        }
        for (Map.Entry<String, Object> me3 : this.properties.entrySet()) {
            internalSetProperty(parser, (String) me3.getKey(), me3.getValue(), (String) me3.getKey());
        }
        try {
            if (parser.getFeature(JDOMConstants.SAX_FEATURE_EXTERNAL_ENT) != this.expand) {
                parser.setFeature(JDOMConstants.SAX_FEATURE_EXTERNAL_ENT, this.expand);
            }
        } catch (SAXException e3) {
        }
        if (!this.expand) {
            try {
                parser.setProperty(JDOMConstants.SAX_PROPERTY_DECLARATION_HANDLER, contentHandler);
            } catch (SAXNotRecognizedException | SAXNotSupportedException e4) {
            }
        }
    }

    private void internalSetFeature(XMLReader parser, String feature, boolean value, String displayName) throws JDOMException {
        try {
            parser.setFeature(feature, value);
        } catch (SAXNotSupportedException e) {
            throw new JDOMException(displayName + " feature not supported for SAX driver " + parser.getClass().getName());
        } catch (SAXNotRecognizedException e2) {
            throw new JDOMException(displayName + " feature not recognized for SAX driver " + parser.getClass().getName());
        }
    }

    private void internalSetProperty(XMLReader parser, String property, Object value, String displayName) throws JDOMException {
        try {
            parser.setProperty(property, value);
        } catch (SAXNotSupportedException e) {
            throw new JDOMException(displayName + " property not supported for SAX driver " + parser.getClass().getName());
        } catch (SAXNotRecognizedException e2) {
            throw new JDOMException(displayName + " property not recognized for SAX driver " + parser.getClass().getName());
        }
    }

    public Document build(InputSource in) throws JDOMException, IOException {
        try {
            return getEngine().build(in);
        } finally {
            if (!this.reuseParser) {
                this.engine = null;
            }
        }
    }

    public Document build(InputStream in) throws JDOMException, IOException {
        try {
            return getEngine().build(in);
        } finally {
            if (!this.reuseParser) {
                this.engine = null;
            }
        }
    }

    public Document build(File file) throws JDOMException, IOException {
        try {
            return getEngine().build(file);
        } finally {
            if (!this.reuseParser) {
                this.engine = null;
            }
        }
    }

    public Document build(URL url) throws JDOMException, IOException {
        try {
            return getEngine().build(url);
        } finally {
            if (!this.reuseParser) {
                this.engine = null;
            }
        }
    }

    public Document build(InputStream in, String systemId) throws JDOMException, IOException {
        try {
            return getEngine().build(in, systemId);
        } finally {
            if (!this.reuseParser) {
                this.engine = null;
            }
        }
    }

    public Document build(Reader characterStream) throws JDOMException, IOException {
        try {
            return getEngine().build(characterStream);
        } finally {
            if (!this.reuseParser) {
                this.engine = null;
            }
        }
    }

    public Document build(Reader characterStream, String systemId) throws JDOMException, IOException {
        try {
            return getEngine().build(characterStream, systemId);
        } finally {
            if (!this.reuseParser) {
                this.engine = null;
            }
        }
    }

    public Document build(String systemId) throws JDOMException, IOException {
        if (systemId == null) {
            throw new NullPointerException("Unable to build a URI from a null systemID.");
        }
        try {
            Document build = getEngine().build(systemId);
            if (!this.reuseParser) {
                this.engine = null;
            }
            return build;
        } catch (IOException ioe) {
            int len = systemId.length();
            int i = 0;
            while (i < len && Verifier.isXMLWhitespace(systemId.charAt(i))) {
                i++;
            }
            if (i >= len || '<' != systemId.charAt(i)) {
                throw ioe;
            }
            MalformedURLException mx = new MalformedURLException("SAXBuilder.build(String) expects the String to be a systemID, but in this instance it appears to be actual XML data.");
            mx.initCause(ioe);
            throw mx;
        } catch (Throwable th) {
            if (!this.reuseParser) {
                this.engine = null;
            }
            throw th;
        }
    }
}
