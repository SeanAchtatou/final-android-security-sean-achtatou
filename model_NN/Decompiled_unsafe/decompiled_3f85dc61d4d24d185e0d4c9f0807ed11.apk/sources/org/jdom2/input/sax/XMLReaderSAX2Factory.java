package org.jdom2.input.sax;

import org.jdom2.JDOMConstants;
import org.jdom2.JDOMException;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class XMLReaderSAX2Factory implements XMLReaderJDOMFactory {
    private final String saxdriver;
    private final boolean validate;

    public XMLReaderSAX2Factory(boolean validate2) {
        this(validate2, null);
    }

    public XMLReaderSAX2Factory(boolean validate2, String saxdriver2) {
        this.validate = validate2;
        this.saxdriver = saxdriver2;
    }

    public XMLReader createXMLReader() throws JDOMException {
        XMLReader reader;
        try {
            if (this.saxdriver == null) {
                reader = XMLReaderFactory.createXMLReader();
            } else {
                reader = XMLReaderFactory.createXMLReader(this.saxdriver);
            }
            reader.setFeature(JDOMConstants.SAX_FEATURE_VALIDATION, this.validate);
            reader.setFeature(JDOMConstants.SAX_FEATURE_NAMESPACES, true);
            reader.setFeature(JDOMConstants.SAX_FEATURE_NAMESPACE_PREFIXES, true);
            return reader;
        } catch (SAXException e) {
            throw new JDOMException("Unable to create SAX2 XMLReader.", e);
        }
    }

    public String getDriverClassName() {
        return this.saxdriver;
    }

    public boolean isValidating() {
        return this.validate;
    }
}
