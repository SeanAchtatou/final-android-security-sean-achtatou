package org.jdom2.input.sax;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.SchemaFactory;
import org.jdom2.JDOMException;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

public enum XMLReaders implements XMLReaderJDOMFactory {
    NONVALIDATING(0),
    DTDVALIDATING(1),
    XSDVALIDATING(2);
    
    private final Exception failcause;
    private final SAXParserFactory jaxpfactory;
    private final boolean validates;

    private XMLReaders(int validate) {
        SAXParserFactory fac = SAXParserFactory.newInstance();
        boolean val = false;
        Exception problem = null;
        fac.setNamespaceAware(true);
        switch (validate) {
            case 0:
                fac.setValidating(false);
                break;
            case 1:
                fac.setValidating(true);
                val = true;
                break;
            case 2:
                fac.setValidating(false);
                try {
                    fac.setSchema(SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema").newSchema());
                    val = true;
                    break;
                } catch (SAXException se) {
                    fac = null;
                    problem = se;
                    break;
                } catch (IllegalArgumentException iae) {
                    fac = null;
                    problem = iae;
                    break;
                } catch (UnsupportedOperationException uoe) {
                    fac = null;
                    problem = uoe;
                    break;
                }
        }
        this.jaxpfactory = fac;
        this.validates = val;
        this.failcause = problem;
    }

    public XMLReader createXMLReader() throws JDOMException {
        if (this.jaxpfactory == null) {
            throw new JDOMException("It was not possible to configure a suitable XMLReader to support " + this, this.failcause);
        }
        try {
            return this.jaxpfactory.newSAXParser().getXMLReader();
        } catch (SAXException e) {
            throw new JDOMException("Unable to create a new XMLReader instance", e);
        } catch (ParserConfigurationException e2) {
            throw new JDOMException("Unable to create a new XMLReader instance", e2);
        }
    }

    public boolean isValidating() {
        return this.validates;
    }
}
