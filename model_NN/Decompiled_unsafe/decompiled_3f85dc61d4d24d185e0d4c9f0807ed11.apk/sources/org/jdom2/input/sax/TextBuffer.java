package org.jdom2.input.sax;

import org.jdom2.Verifier;
import org.jdom2.internal.ArrayCopy;

final class TextBuffer {
    private char[] array = new char[1024];
    private int arraySize = 0;

    TextBuffer() {
    }

    /* access modifiers changed from: package-private */
    public void append(char[] source, int start, int count) {
        if (this.arraySize + count > this.array.length) {
            this.array = ArrayCopy.copyOf(this.array, this.arraySize + count + (this.array.length >> 2));
        }
        System.arraycopy(source, start, this.array, this.arraySize, count);
        this.arraySize += count;
    }

    /* access modifiers changed from: package-private */
    public void clear() {
        this.arraySize = 0;
    }

    /* access modifiers changed from: package-private */
    public boolean isAllWhitespace() {
        int i = this.arraySize;
        do {
            i--;
            if (i < 0) {
                return true;
            }
        } while (Verifier.isXMLWhitespace(this.array[i]));
        return false;
    }

    public String toString() {
        if (this.arraySize == 0) {
            return "";
        }
        return String.valueOf(this.array, 0, this.arraySize);
    }
}
