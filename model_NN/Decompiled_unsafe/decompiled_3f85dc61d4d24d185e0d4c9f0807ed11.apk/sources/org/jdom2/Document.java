package org.jdom2;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.jdom2.filter.Filter;
import org.jdom2.util.IteratorIterable;

public class Document extends CloneBase implements Parent {
    private static final long serialVersionUID = 200;
    protected String baseURI;
    transient ContentList content;
    private transient HashMap<String, Object> propertyMap;

    public Document() {
        this.content = new ContentList(this);
        this.baseURI = null;
        this.propertyMap = null;
    }

    public Document(Element rootElement, DocType docType, String baseURI2) {
        this.content = new ContentList(this);
        this.baseURI = null;
        this.propertyMap = null;
        if (rootElement != null) {
            setRootElement(rootElement);
        }
        if (docType != null) {
            setDocType(docType);
        }
        if (baseURI2 != null) {
            setBaseURI(baseURI2);
        }
    }

    public Document(Element rootElement, DocType docType) {
        this(rootElement, docType, null);
    }

    public Document(Element rootElement) {
        this(rootElement, null, null);
    }

    public Document(List<? extends Content> content2) {
        this.content = new ContentList(this);
        this.baseURI = null;
        this.propertyMap = null;
        setContent(content2);
    }

    public int getContentSize() {
        return this.content.size();
    }

    public int indexOf(Content child) {
        return this.content.indexOf(child);
    }

    public boolean hasRootElement() {
        return this.content.indexOfFirstElement() >= 0;
    }

    public Element getRootElement() {
        int index = this.content.indexOfFirstElement();
        if (index >= 0) {
            return (Element) this.content.get(index);
        }
        throw new IllegalStateException("Root element not set");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jdom2.ContentList.set(int, org.jdom2.Content):org.jdom2.Content
     arg types: [int, org.jdom2.Element]
     candidates:
      org.jdom2.ContentList.set(int, java.lang.Object):java.lang.Object
      ClspMth{java.util.AbstractList.set(int, java.lang.Object):E}
      ClspMth{java.util.List.set(int, java.lang.Object):E}
      org.jdom2.ContentList.set(int, org.jdom2.Content):org.jdom2.Content */
    public Document setRootElement(Element rootElement) {
        int index = this.content.indexOfFirstElement();
        if (index < 0) {
            this.content.add(rootElement);
        } else {
            this.content.set(index, (Content) rootElement);
        }
        return this;
    }

    public Element detachRootElement() {
        int index = this.content.indexOfFirstElement();
        if (index < 0) {
            return null;
        }
        return (Element) removeContent(index);
    }

    public DocType getDocType() {
        int index = this.content.indexOfDocType();
        if (index < 0) {
            return null;
        }
        return (DocType) this.content.get(index);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jdom2.ContentList.add(int, org.jdom2.Content):void
     arg types: [int, org.jdom2.DocType]
     candidates:
      org.jdom2.ContentList.add(int, java.lang.Object):void
      ClspMth{java.util.AbstractList.add(int, java.lang.Object):void}
      ClspMth{java.util.List.add(int, java.lang.Object):void}
      org.jdom2.ContentList.add(int, org.jdom2.Content):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jdom2.ContentList.set(int, org.jdom2.Content):org.jdom2.Content
     arg types: [int, org.jdom2.DocType]
     candidates:
      org.jdom2.ContentList.set(int, java.lang.Object):java.lang.Object
      ClspMth{java.util.AbstractList.set(int, java.lang.Object):E}
      ClspMth{java.util.List.set(int, java.lang.Object):E}
      org.jdom2.ContentList.set(int, org.jdom2.Content):org.jdom2.Content */
    public Document setDocType(DocType docType) {
        if (docType == null) {
            int docTypeIndex = this.content.indexOfDocType();
            if (docTypeIndex >= 0) {
                this.content.remove(docTypeIndex);
            }
        } else if (docType.getParent() != null) {
            throw new IllegalAddException(docType, "The DocType already is attached to a document");
        } else {
            int docTypeIndex2 = this.content.indexOfDocType();
            if (docTypeIndex2 < 0) {
                this.content.add(0, (Content) docType);
            } else {
                this.content.set(docTypeIndex2, (Content) docType);
            }
        }
        return this;
    }

    public Document addContent(Content child) {
        this.content.add(child);
        return this;
    }

    public Document addContent(Collection<? extends Content> c) {
        this.content.addAll(c);
        return this;
    }

    public Document addContent(int index, Content child) {
        this.content.add(index, child);
        return this;
    }

    public Document addContent(int index, Collection<? extends Content> c) {
        this.content.addAll(index, c);
        return this;
    }

    public List<Content> cloneContent() {
        int size = getContentSize();
        List<Content> list = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            list.add(getContent(i).clone());
        }
        return list;
    }

    public Content getContent(int index) {
        return this.content.get(index);
    }

    public List<Content> getContent() {
        if (hasRootElement()) {
            return this.content;
        }
        throw new IllegalStateException("Root element not set");
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [org.jdom2.filter.Filter, org.jdom2.filter.Filter<F>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <F extends org.jdom2.Content> java.util.List<F> getContent(org.jdom2.filter.Filter<F> r3) {
        /*
            r2 = this;
            boolean r0 = r2.hasRootElement()
            if (r0 != 0) goto L_0x000e
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Root element not set"
            r0.<init>(r1)
            throw r0
        L_0x000e:
            org.jdom2.ContentList r0 = r2.content
            java.util.List r0 = r0.getView(r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jdom2.Document.getContent(org.jdom2.filter.Filter):java.util.List");
    }

    public List<Content> removeContent() {
        List<Content> old = new ArrayList<>(this.content);
        this.content.clear();
        return old;
    }

    public <F extends Content> List<F> removeContent(Filter<F> filter) {
        List<F> old = new ArrayList<>();
        Iterator<E> it = this.content.getView(filter).iterator();
        while (it.hasNext()) {
            old.add((Content) it.next());
            it.remove();
        }
        return old;
    }

    public Document setContent(Collection<? extends Content> newContent) {
        this.content.clearAndSet(newContent);
        return this;
    }

    public final void setBaseURI(String uri) {
        this.baseURI = uri;
    }

    public final String getBaseURI() {
        return this.baseURI;
    }

    public Document setContent(int index, Content child) {
        this.content.set(index, child);
        return this;
    }

    public Document setContent(int index, Collection<? extends Content> collection) {
        this.content.remove(index);
        this.content.addAll(index, collection);
        return this;
    }

    public boolean removeContent(Content child) {
        return this.content.remove(child);
    }

    public Content removeContent(int index) {
        return this.content.remove(index);
    }

    public Document setContent(Content child) {
        this.content.clear();
        this.content.add(child);
        return this;
    }

    public String toString() {
        StringBuilder stringForm = new StringBuilder().append("[Document: ");
        DocType docType = getDocType();
        if (docType != null) {
            stringForm.append(docType.toString()).append(", ");
        } else {
            stringForm.append(" No DOCTYPE declaration, ");
        }
        Element rootElement = hasRootElement() ? getRootElement() : null;
        if (rootElement != null) {
            stringForm.append("Root is ").append(rootElement.toString());
        } else {
            stringForm.append(" No root element");
        }
        stringForm.append("]");
        return stringForm.toString();
    }

    public final boolean equals(Object ob) {
        return ob == this;
    }

    public final int hashCode() {
        return super.hashCode();
    }

    public Document clone() {
        Document doc = (Document) super.clone();
        doc.content = new ContentList(doc);
        for (int i = 0; i < this.content.size(); i++) {
            Content obj = this.content.get(i);
            if (obj instanceof Element) {
                doc.content.add(((Element) obj).clone());
            } else if (obj instanceof Comment) {
                doc.content.add(((Comment) obj).clone());
            } else if (obj instanceof ProcessingInstruction) {
                doc.content.add(((ProcessingInstruction) obj).clone());
            } else if (obj instanceof DocType) {
                doc.content.add(((DocType) obj).clone());
            }
        }
        return doc;
    }

    public IteratorIterable<Content> getDescendants() {
        return new DescendantIterator(this);
    }

    public <F extends Content> IteratorIterable<F> getDescendants(Filter<F> filter) {
        return new FilterIterator(new DescendantIterator(this), filter);
    }

    public Parent getParent() {
        return null;
    }

    public Document getDocument() {
        return this;
    }

    public void setProperty(String id, Object value) {
        if (this.propertyMap == null) {
            this.propertyMap = new HashMap<>();
        }
        this.propertyMap.put(id, value);
    }

    public Object getProperty(String id) {
        if (this.propertyMap == null) {
            return null;
        }
        return this.propertyMap.get(id);
    }

    public void canContainContent(Content child, int index, boolean replace) {
        if (child instanceof Element) {
            int cre = this.content.indexOfFirstElement();
            if (replace && cre == index) {
                return;
            }
            if (cre >= 0) {
                throw new IllegalAddException("Cannot add a second root element, only one is allowed");
            } else if (this.content.indexOfDocType() >= index) {
                throw new IllegalAddException("A root element cannot be added before the DocType");
            }
        }
        if (child instanceof DocType) {
            int cdt = this.content.indexOfDocType();
            if (replace && cdt == index) {
                return;
            }
            if (cdt >= 0) {
                throw new IllegalAddException("Cannot add a second doctype, only one is allowed");
            }
            int firstElt = this.content.indexOfFirstElement();
            if (firstElt != -1 && firstElt < index) {
                throw new IllegalAddException("A DocType cannot be added after the root element");
            }
        }
        if (child instanceof CDATA) {
            throw new IllegalAddException("A CDATA is not allowed at the document root");
        } else if (child instanceof Text) {
            throw new IllegalAddException("A Text is not allowed at the document root");
        } else if (child instanceof EntityRef) {
            throw new IllegalAddException("An EntityRef is not allowed at the document root");
        }
    }

    public List<Namespace> getNamespacesInScope() {
        return Collections.unmodifiableList(Arrays.asList(Namespace.NO_NAMESPACE, Namespace.XML_NAMESPACE));
    }

    public List<Namespace> getNamespacesIntroduced() {
        return Collections.unmodifiableList(Arrays.asList(Namespace.NO_NAMESPACE, Namespace.XML_NAMESPACE));
    }

    public List<Namespace> getNamespacesInherited() {
        return Collections.emptyList();
    }

    private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
        int cs = this.content.size();
        out.writeInt(cs);
        for (int i = 0; i < cs; i++) {
            out.writeObject(getContent(i));
        }
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        this.content = new ContentList(this);
        int cs = in.readInt();
        while (true) {
            cs--;
            if (cs >= 0) {
                addContent((Content) in.readObject());
            } else {
                return;
            }
        }
    }
}
