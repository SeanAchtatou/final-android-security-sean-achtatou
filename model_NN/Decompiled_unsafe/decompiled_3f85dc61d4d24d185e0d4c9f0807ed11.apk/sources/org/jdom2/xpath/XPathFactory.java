package org.jdom2.xpath;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import org.jdom2.JDOMConstants;
import org.jdom2.Namespace;
import org.jdom2.filter.Filter;
import org.jdom2.filter.Filters;
import org.jdom2.internal.ReflectionConstructor;
import org.jdom2.internal.SystemProperty;
import org.jdom2.xpath.jaxen.JaxenXPathFactory;

public abstract class XPathFactory {
    private static final String DEFAULTFACTORY = SystemProperty.get(JDOMConstants.JDOM2_PROPERTY_XPATH_FACTORY, null);
    private static final Namespace[] EMPTYNS = new Namespace[0];
    private static final AtomicReference<XPathFactory> defaultreference = new AtomicReference<>();

    public abstract <T> XPathExpression<T> compile(String str, Filter<T> filter, Map<String, Object> map, Namespace... namespaceArr);

    public static final XPathFactory instance() {
        XPathFactory ret = defaultreference.get();
        if (ret != null) {
            return ret;
        }
        XPathFactory fac = DEFAULTFACTORY == null ? new JaxenXPathFactory() : newInstance(DEFAULTFACTORY);
        if (defaultreference.compareAndSet(null, fac)) {
            return fac;
        }
        return defaultreference.get();
    }

    public static final XPathFactory newInstance(String factoryclass) {
        return (XPathFactory) ReflectionConstructor.construct(factoryclass, XPathFactory.class);
    }

    public <T> XPathExpression<T> compile(String expression, Filter<T> filter, Map<String, Object> variables, Collection<Namespace> namespaces) {
        return compile(expression, filter, variables, (Namespace[]) namespaces.toArray(EMPTYNS));
    }

    public <T> XPathExpression<T> compile(String expression, Filter<T> filter) {
        return compile(expression, filter, (Map<String, Object>) null, EMPTYNS);
    }

    public XPathExpression<Object> compile(String expression) {
        return compile(expression, Filters.fpassthrough(), (Map<String, Object>) null, EMPTYNS);
    }
}
