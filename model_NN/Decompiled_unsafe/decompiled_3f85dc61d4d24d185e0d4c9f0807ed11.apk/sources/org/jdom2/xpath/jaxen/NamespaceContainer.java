package org.jdom2.xpath.jaxen;

import org.jdom2.Element;
import org.jdom2.Namespace;

final class NamespaceContainer {
    private final Element emt;
    private final Namespace ns;

    public NamespaceContainer(Namespace ns2, Element emt2) {
        this.ns = ns2;
        this.emt = emt2;
    }

    public Namespace getNamespace() {
        return this.ns;
    }

    public Element getParentElement() {
        return this.emt;
    }

    public String toString() {
        return this.ns.getPrefix() + "=" + this.ns.getURI();
    }
}
