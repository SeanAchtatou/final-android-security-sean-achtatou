package org.jdom2.xpath.jaxen;

import java.util.ArrayList;
import java.util.List;
import org.jaxen.BaseXPath;
import org.jaxen.JaxenException;
import org.jaxen.SimpleVariableContext;
import org.jaxen.VariableContext;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.xpath.XPath;

@Deprecated
public class JDOMXPath extends XPath {
    private static final long serialVersionUID = 200;
    private final JDOMNavigator navigator = new JDOMNavigator();
    private transient org.jaxen.XPath xPath;

    private static final Object unWrapNS(Object o) {
        if (o instanceof NamespaceContainer) {
            return ((NamespaceContainer) o).getNamespace();
        }
        return o;
    }

    private static final List<Object> unWrap(List<?> results) {
        ArrayList<Object> ret = new ArrayList<>(results.size());
        for (Object unWrapNS : results) {
            ret.add(unWrapNS(unWrapNS));
        }
        return ret;
    }

    public JDOMXPath(String expr) throws JDOMException {
        setXPath(expr);
    }

    public List<?> selectNodes(Object context) throws JDOMException {
        try {
            this.navigator.setContext(context);
            List<?> unWrap = unWrap(this.xPath.selectNodes(context));
            this.navigator.reset();
            return unWrap;
        } catch (JaxenException ex1) {
            throw new JDOMException("XPath error while evaluating \"" + this.xPath.toString() + "\": " + ex1.getMessage(), ex1);
        } catch (Throwable th) {
            this.navigator.reset();
            throw th;
        }
    }

    public Object selectSingleNode(Object context) throws JDOMException {
        try {
            this.navigator.setContext(context);
            Object unWrapNS = unWrapNS(this.xPath.selectSingleNode(context));
            this.navigator.reset();
            return unWrapNS;
        } catch (JaxenException ex1) {
            throw new JDOMException("XPath error while evaluating \"" + this.xPath.toString() + "\": " + ex1.getMessage(), ex1);
        } catch (Throwable th) {
            this.navigator.reset();
            throw th;
        }
    }

    public String valueOf(Object context) throws JDOMException {
        try {
            this.navigator.setContext(context);
            String stringValueOf = this.xPath.stringValueOf(context);
            this.navigator.reset();
            return stringValueOf;
        } catch (JaxenException ex1) {
            throw new JDOMException("XPath error while evaluating \"" + this.xPath.toString() + "\": " + ex1.getMessage(), ex1);
        } catch (Throwable th) {
            this.navigator.reset();
            throw th;
        }
    }

    public Number numberValueOf(Object context) throws JDOMException {
        try {
            this.navigator.setContext(context);
            Number numberValueOf = this.xPath.numberValueOf(context);
            this.navigator.reset();
            return numberValueOf;
        } catch (JaxenException ex1) {
            throw new JDOMException("XPath error while evaluating \"" + this.xPath.toString() + "\": " + ex1.getMessage(), ex1);
        } catch (Throwable th) {
            this.navigator.reset();
            throw th;
        }
    }

    public void setVariable(String name, Object value) throws IllegalArgumentException {
        VariableContext o = this.xPath.getVariableContext();
        if (o instanceof SimpleVariableContext) {
            ((SimpleVariableContext) o).setVariableValue((String) null, name, value);
        }
    }

    public void addNamespace(Namespace namespace) {
        this.navigator.includeNamespace(namespace);
    }

    public String getXPath() {
        return this.xPath.toString();
    }

    private void setXPath(String expr) throws JDOMException {
        try {
            this.xPath = new BaseXPath(expr, this.navigator);
            this.xPath.setNamespaceContext(this.navigator);
        } catch (Exception ex1) {
            throw new JDOMException("Invalid XPath expression: \"" + expr + "\"", ex1);
        }
    }

    public String toString() {
        return String.format("[XPath: %s]", this.xPath.toString());
    }
}
