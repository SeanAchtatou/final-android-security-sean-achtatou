package org.jdom2.xpath;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.jdom2.Namespace;
import org.jdom2.filter.Filter;

public class XPathBuilder<T> {
    private final String expression;
    private final Filter<T> filter;
    private Map<String, Namespace> namespaces;
    private Map<String, Object> variables;

    public XPathBuilder(String expression2, Filter<T> filter2) {
        if (expression2 == null) {
            throw new NullPointerException("Null expression");
        } else if (filter2 == null) {
            throw new NullPointerException("Null filter");
        } else {
            this.filter = filter2;
            this.expression = expression2;
        }
    }

    public boolean setVariable(String qname, Object value) {
        if (qname == null) {
            throw new NullPointerException("Null variable name");
        }
        if (this.variables == null) {
            this.variables = new HashMap();
        }
        return this.variables.put(qname, value) == null;
    }

    public boolean setNamespace(String prefix, String uri) {
        if (prefix == null) {
            throw new NullPointerException("Null prefix");
        } else if (uri != null) {
            return setNamespace(Namespace.getNamespace(prefix, uri));
        } else {
            throw new NullPointerException("Null URI");
        }
    }

    public boolean setNamespace(Namespace namespace) {
        if (namespace == null) {
            throw new NullPointerException("Null Namespace");
        } else if (!"".equals(namespace.getPrefix())) {
            if (this.namespaces == null) {
                this.namespaces = new HashMap();
            }
            if (this.namespaces.put(namespace.getPrefix(), namespace) == null) {
                return true;
            }
            return false;
        } else if (Namespace.NO_NAMESPACE == namespace) {
            return false;
        } else {
            throw new IllegalArgumentException("Cannot set a Namespace URI in XPath for the \"\" prefix.");
        }
    }

    public boolean setNamespaces(Collection<Namespace> namespaces2) {
        if (namespaces2 == null) {
            throw new NullPointerException("Null namespaces Collection");
        }
        boolean ret = false;
        for (Namespace ns : namespaces2) {
            if (setNamespace(ns)) {
                ret = true;
            }
        }
        return ret;
    }

    public Object getVariable(String qname) {
        if (qname == null) {
            throw new NullPointerException("Null qname");
        } else if (this.variables == null) {
            return null;
        } else {
            return this.variables.get(qname);
        }
    }

    public Namespace getNamespace(String prefix) {
        if (prefix == null) {
            throw new NullPointerException("Null prefix");
        } else if ("".equals(prefix)) {
            return Namespace.NO_NAMESPACE;
        } else {
            if (this.namespaces == null) {
                return null;
            }
            return this.namespaces.get(prefix);
        }
    }

    public Filter<T> getFilter() {
        return this.filter;
    }

    public String getExpression() {
        return this.expression;
    }

    public XPathExpression<T> compileWith(XPathFactory factory) {
        if (this.namespaces == null) {
            return factory.compile(this.expression, this.filter, this.variables, new Namespace[0]);
        }
        return factory.compile(this.expression, this.filter, this.variables, (Namespace[]) this.namespaces.values().toArray(new Namespace[this.namespaces.size()]));
    }
}
