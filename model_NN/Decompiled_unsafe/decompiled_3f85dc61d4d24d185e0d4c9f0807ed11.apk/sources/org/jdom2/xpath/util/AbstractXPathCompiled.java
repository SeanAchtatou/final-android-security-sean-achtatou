package org.jdom2.xpath.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jdom2.Namespace;
import org.jdom2.Verifier;
import org.jdom2.filter.Filter;
import org.jdom2.xpath.XPathDiagnostic;
import org.jdom2.xpath.XPathExpression;

public abstract class AbstractXPathCompiled<T> implements XPathExpression<T> {
    private static final NamespaceComparator NSSORT = new NamespaceComparator();
    private final Filter<T> xfilter;
    private final Map<String, Namespace> xnamespaces = new HashMap();
    private final String xquery;
    private Map<String, Map<String, Object>> xvariables = new HashMap();

    /* access modifiers changed from: protected */
    public abstract List<?> evaluateRawAll(Object obj);

    /* access modifiers changed from: protected */
    public abstract Object evaluateRawFirst(Object obj);

    private static final class NamespaceComparator implements Comparator<Namespace> {
        private NamespaceComparator() {
        }

        public int compare(Namespace ns1, Namespace ns2) {
            return ns1.getPrefix().compareTo(ns2.getPrefix());
        }
    }

    public AbstractXPathCompiled(String query, Filter<T> filter, Map<String, Object> variables, Namespace[] namespaces) {
        if (query == null) {
            throw new NullPointerException("Null query");
        } else if (filter == null) {
            throw new NullPointerException("Null filter");
        } else {
            this.xnamespaces.put(Namespace.NO_NAMESPACE.getPrefix(), Namespace.NO_NAMESPACE);
            if (namespaces != null) {
                Namespace[] arr$ = namespaces;
                int len$ = arr$.length;
                int i$ = 0;
                while (i$ < len$) {
                    Namespace ns = arr$[i$];
                    if (ns == null) {
                        throw new NullPointerException("Null namespace");
                    }
                    Namespace oldns = this.xnamespaces.put(ns.getPrefix(), ns);
                    if (oldns == null || oldns == ns) {
                        i$++;
                    } else if (oldns == Namespace.NO_NAMESPACE) {
                        throw new IllegalArgumentException("The default (no prefix) Namespace URI for XPath queries is always '' and it cannot be redefined to '" + ns.getURI() + "'.");
                    } else {
                        throw new IllegalArgumentException("A Namespace with the prefix '" + ns.getPrefix() + "' has already been declared.");
                    }
                }
            }
            if (variables != null) {
                for (Map.Entry<String, Object> me2 : variables.entrySet()) {
                    String qname = (String) me2.getKey();
                    if (qname == null) {
                        throw new NullPointerException("Variable with a null name");
                    }
                    int p = qname.indexOf(58);
                    String pfx = p < 0 ? "" : qname.substring(0, p);
                    String lname = p < 0 ? qname : qname.substring(p + 1);
                    String vpfxmsg = Verifier.checkNamespacePrefix(pfx);
                    if (vpfxmsg != null) {
                        throw new IllegalArgumentException("Prefix '" + pfx + "' for variable " + qname + " is illegal: " + vpfxmsg);
                    }
                    String vnamemsg = Verifier.checkXMLName(lname);
                    if (vnamemsg != null) {
                        throw new IllegalArgumentException("Variable name '" + lname + "' for variable " + qname + " is illegal: " + vnamemsg);
                    }
                    Namespace ns2 = this.xnamespaces.get(pfx);
                    if (ns2 == null) {
                        throw new IllegalArgumentException("Prefix '" + pfx + "' for variable " + qname + " has not been assigned a Namespace.");
                    }
                    Map<String, Object> vmap = this.xvariables.get(ns2.getURI());
                    if (vmap == null) {
                        vmap = new HashMap<>();
                        this.xvariables.put(ns2.getURI(), vmap);
                    }
                    if (vmap.put(lname, me2.getValue()) != null) {
                        throw new IllegalArgumentException("Variable with name " + ((String) me2.getKey()) + "' has already been defined.");
                    }
                }
            }
            this.xquery = query;
            this.xfilter = filter;
        }
    }

    public XPathExpression<T> clone() {
        try {
            AbstractXPathCompiled<T> ret = (AbstractXPathCompiled) super.clone();
            Map<String, Map<String, Object>> vmt = new HashMap<>();
            for (Map.Entry<String, Map<String, Object>> me2 : this.xvariables.entrySet()) {
                Map<String, Object> cmap = new HashMap<>();
                for (Map.Entry<String, Object> ne : ((Map) me2.getValue()).entrySet()) {
                    cmap.put(ne.getKey(), ne.getValue());
                }
                vmt.put(me2.getKey(), cmap);
            }
            ret.xvariables = vmt;
            return ret;
        } catch (CloneNotSupportedException cnse) {
            throw new IllegalStateException("Should never be getting a CloneNotSupportedException!", cnse);
        }
    }

    public final String getExpression() {
        return this.xquery;
    }

    public final Namespace getNamespace(String prefix) {
        Namespace ns = this.xnamespaces.get(prefix);
        if (ns != null) {
            return ns;
        }
        throw new IllegalArgumentException("Namespace with prefix '" + prefix + "' has not been declared.");
    }

    public Namespace[] getNamespaces() {
        Namespace[] nsa = (Namespace[]) this.xnamespaces.values().toArray(new Namespace[this.xnamespaces.size()]);
        Arrays.sort(nsa, NSSORT);
        return nsa;
    }

    public final Object getVariable(String name, Namespace uri) {
        Map<String, Object> vmap = this.xvariables.get(uri == null ? "" : uri.getURI());
        if (vmap == null) {
            throw new IllegalArgumentException("Variable with name '" + name + "' in namespace '" + uri.getURI() + "' has not been declared.");
        }
        Object ret = vmap.get(name);
        if (ret != null) {
            return ret;
        }
        if (vmap.containsKey(name)) {
            return null;
        }
        throw new IllegalArgumentException("Variable with name '" + name + "' in namespace '" + uri.getURI() + "' has not been declared.");
    }

    public Object getVariable(String qname) {
        if (qname == null) {
            throw new NullPointerException("Cannot get variable value for null qname");
        }
        int pos = qname.indexOf(58);
        if (pos >= 0) {
            return getVariable(qname.substring(pos + 1), getNamespace(qname.substring(0, pos)));
        }
        return getVariable(qname, Namespace.NO_NAMESPACE);
    }

    public Object setVariable(String name, Namespace uri, Object value) {
        Object ret = getVariable(name, uri);
        this.xvariables.get(uri.getURI()).put(name, value);
        return ret;
    }

    public Object setVariable(String qname, Object value) {
        if (qname == null) {
            throw new NullPointerException("Cannot get variable value for null qname");
        }
        int pos = qname.indexOf(58);
        if (pos >= 0) {
            return setVariable(qname.substring(pos + 1), getNamespace(qname.substring(0, pos)), value);
        }
        return setVariable(qname, Namespace.NO_NAMESPACE, value);
    }

    public final Filter<T> getFilter() {
        return this.xfilter;
    }

    public List<T> evaluate(Object context) {
        return this.xfilter.filter(evaluateRawAll(context));
    }

    public T evaluateFirst(Object context) {
        Object raw = evaluateRawFirst(context);
        if (raw == null) {
            return null;
        }
        return this.xfilter.filter(raw);
    }

    public XPathDiagnostic<T> diagnose(Object context, boolean firstonly) {
        return new XPathDiagnosticImpl(context, this, firstonly ? Collections.singletonList(evaluateRawFirst(context)) : evaluateRawAll(context), firstonly);
    }

    public String toString() {
        int nscnt = this.xnamespaces.size();
        int vcnt = 0;
        for (Map<String, Object> cmap : this.xvariables.values()) {
            vcnt += cmap.size();
        }
        return String.format("[XPathExpression: %d namespaces and %d variables for query %s]", Integer.valueOf(nscnt), Integer.valueOf(vcnt), getExpression());
    }
}
