package org.jdom2;

import org.jdom2.Content;

public class EntityRef extends Content {
    private static final long serialVersionUID = 200;
    protected String name;
    protected String publicID;
    protected String systemID;

    protected EntityRef() {
        super(Content.CType.EntityRef);
    }

    public EntityRef(String name2) {
        this(name2, null, null);
    }

    public EntityRef(String name2, String systemID2) {
        this(name2, null, systemID2);
    }

    public EntityRef(String name2, String publicID2, String systemID2) {
        super(Content.CType.EntityRef);
        setName(name2);
        setPublicID(publicID2);
        setSystemID(systemID2);
    }

    public String getName() {
        return this.name;
    }

    public String getValue() {
        return "";
    }

    public String getPublicID() {
        return this.publicID;
    }

    public String getSystemID() {
        return this.systemID;
    }

    public EntityRef setName(String name2) {
        String reason = Verifier.checkXMLName(name2);
        if (reason != null) {
            throw new IllegalNameException(name2, "EntityRef", reason);
        }
        this.name = name2;
        return this;
    }

    public EntityRef setPublicID(String publicID2) {
        String reason = Verifier.checkPublicID(publicID2);
        if (reason != null) {
            throw new IllegalDataException(publicID2, "EntityRef", reason);
        }
        this.publicID = publicID2;
        return this;
    }

    public EntityRef setSystemID(String systemID2) {
        String reason = Verifier.checkSystemLiteral(systemID2);
        if (reason != null) {
            throw new IllegalDataException(systemID2, "EntityRef", reason);
        }
        this.systemID = systemID2;
        return this;
    }

    public String toString() {
        return "[EntityRef: " + "&" + this.name + ";" + "]";
    }

    public EntityRef detach() {
        return (EntityRef) super.detach();
    }

    /* access modifiers changed from: protected */
    public EntityRef setParent(Parent parent) {
        return (EntityRef) super.setParent(parent);
    }

    public Element getParent() {
        return (Element) super.getParent();
    }

    public EntityRef clone() {
        return (EntityRef) super.clone();
    }
}
