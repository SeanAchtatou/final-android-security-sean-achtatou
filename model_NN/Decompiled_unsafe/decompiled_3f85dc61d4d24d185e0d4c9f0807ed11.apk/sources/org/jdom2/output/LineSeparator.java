package org.jdom2.output;

import com.fsck.k9.remotecontrol.K9RemoteControl;
import org.apache.commons.io.IOUtils;
import org.jdom2.JDOMConstants;
import org.jdom2.internal.SystemProperty;

public enum LineSeparator {
    CRNL("\r\n"),
    NL(IOUtils.LINE_SEPARATOR_UNIX),
    CR("\r"),
    DOS("\r\n"),
    UNIX(IOUtils.LINE_SEPARATOR_UNIX),
    SYSTEM(SystemProperty.get("line.separator", "\r\n")),
    NONE(null),
    DEFAULT(getDefaultLineSeparator());
    
    private final String value;

    private static String getDefaultLineSeparator() {
        String prop = SystemProperty.get(JDOMConstants.JDOM2_PROPERTY_LINE_SEPARATOR, "DEFAULT");
        if ("DEFAULT".equals(prop)) {
            return "\r\n";
        }
        if ("SYSTEM".equals(prop)) {
            return System.getProperty("line.separator");
        }
        if ("CRNL".equals(prop)) {
            return "\r\n";
        }
        if ("NL".equals(prop)) {
            return IOUtils.LINE_SEPARATOR_UNIX;
        }
        if ("CR".equals(prop)) {
            return "\r";
        }
        if ("DOS".equals(prop)) {
            return "\r\n";
        }
        if ("UNIX".equals(prop)) {
            return IOUtils.LINE_SEPARATOR_UNIX;
        }
        if (K9RemoteControl.K9_FOLDERS_NONE.equals(prop)) {
            return null;
        }
        return prop;
    }

    private LineSeparator(String value2) {
        this.value = value2;
    }

    public String value() {
        return this.value;
    }
}
