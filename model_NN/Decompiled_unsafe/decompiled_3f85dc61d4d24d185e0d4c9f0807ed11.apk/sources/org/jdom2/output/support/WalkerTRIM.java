package org.jdom2.output.support;

import java.util.List;
import org.jdom2.Content;
import org.jdom2.Text;
import org.jdom2.Verifier;
import org.jdom2.output.support.AbstractFormattedWalker;

public class WalkerTRIM extends AbstractFormattedWalker {
    public WalkerTRIM(List<? extends Content> content, FormatStack fstack, boolean escape) {
        super(content, fstack, escape);
    }

    /* access modifiers changed from: protected */
    public void analyzeMultiText(AbstractFormattedWalker.MultiText mtext, int offset, int len) {
        while (len > 0) {
            Content c = get(offset);
            if (!(c instanceof Text) || !Verifier.isAllXMLWhitespace(c.getValue())) {
                break;
            }
            offset++;
            len--;
        }
        while (len > 0) {
            Content c2 = get((offset + len) - 1);
            if (!(c2 instanceof Text) || !Verifier.isAllXMLWhitespace(c2.getValue())) {
                break;
            }
            len--;
        }
        for (int i = 0; i < len; i++) {
            AbstractFormattedWalker.Trim trim = AbstractFormattedWalker.Trim.NONE;
            if (i + 1 == len) {
                trim = AbstractFormattedWalker.Trim.RIGHT;
            }
            if (i == 0) {
                trim = AbstractFormattedWalker.Trim.LEFT;
            }
            if (len == 1) {
                trim = AbstractFormattedWalker.Trim.BOTH;
            }
            Content c3 = get(offset + i);
            switch (c3.getCType()) {
                case Text:
                    mtext.appendText(trim, c3.getValue());
                    break;
                case CDATA:
                    mtext.appendCDATA(trim, c3.getValue());
                    break;
                default:
                    mtext.appendRaw(c3);
                    break;
            }
        }
    }
}
