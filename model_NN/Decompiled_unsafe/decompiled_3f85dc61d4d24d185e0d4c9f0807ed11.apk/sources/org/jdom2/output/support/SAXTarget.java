package org.jdom2.output.support;

import org.jdom2.output.JDOMLocator;
import org.xml.sax.ContentHandler;
import org.xml.sax.DTDHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.ext.DeclHandler;
import org.xml.sax.ext.LexicalHandler;

public final class SAXTarget {
    private final ContentHandler contentHandler;
    private final DeclHandler declHandler;
    private final boolean declareNamespaces;
    private final DTDHandler dtdHandler;
    private final EntityResolver entityResolver;
    private final ErrorHandler errorHandler;
    private final LexicalHandler lexicalHandler;
    private final SAXLocator locator;
    private final boolean reportDtdEvents;

    public static final class SAXLocator implements JDOMLocator {
        private Object node = null;
        private final String publicid;
        private final String systemid;

        public SAXLocator(String publicid2, String systemid2) {
            this.publicid = publicid2;
            this.systemid = systemid2;
        }

        public int getColumnNumber() {
            return -1;
        }

        public int getLineNumber() {
            return -1;
        }

        public String getPublicId() {
            return this.publicid;
        }

        public String getSystemId() {
            return this.systemid;
        }

        public Object getNode() {
            return this.node;
        }

        public void setNode(Object node2) {
            this.node = node2;
        }
    }

    public SAXTarget(ContentHandler contentHandler2, ErrorHandler errorHandler2, DTDHandler dtdHandler2, EntityResolver entityResolver2, LexicalHandler lexicalHandler2, DeclHandler declHandler2, boolean declareNamespaces2, boolean reportDtdEvents2, String publicID, String systemID) {
        this.contentHandler = contentHandler2;
        this.errorHandler = errorHandler2;
        this.dtdHandler = dtdHandler2;
        this.entityResolver = entityResolver2;
        this.lexicalHandler = lexicalHandler2;
        this.declHandler = declHandler2;
        this.declareNamespaces = declareNamespaces2;
        this.reportDtdEvents = reportDtdEvents2;
        this.locator = new SAXLocator(publicID, systemID);
    }

    public ContentHandler getContentHandler() {
        return this.contentHandler;
    }

    public ErrorHandler getErrorHandler() {
        return this.errorHandler;
    }

    public DTDHandler getDTDHandler() {
        return this.dtdHandler;
    }

    public EntityResolver getEntityResolver() {
        return this.entityResolver;
    }

    public LexicalHandler getLexicalHandler() {
        return this.lexicalHandler;
    }

    public DeclHandler getDeclHandler() {
        return this.declHandler;
    }

    public boolean isDeclareNamespaces() {
        return this.declareNamespaces;
    }

    public boolean isReportDTDEvents() {
        return this.reportDtdEvents;
    }

    public SAXLocator getLocator() {
        return this.locator;
    }
}
