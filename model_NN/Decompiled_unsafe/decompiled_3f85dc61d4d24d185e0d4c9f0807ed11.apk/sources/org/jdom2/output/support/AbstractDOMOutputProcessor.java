package org.jdom2.output.support;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.jdom2.Attribute;
import org.jdom2.CDATA;
import org.jdom2.Content;
import org.jdom2.EntityRef;
import org.jdom2.JDOMConstants;
import org.jdom2.Namespace;
import org.jdom2.output.Format;
import org.jdom2.util.NamespaceStack;
import org.w3c.dom.Attr;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.EntityReference;
import org.w3c.dom.Node;
import org.w3c.dom.ProcessingInstruction;
import org.w3c.dom.Text;

public abstract class AbstractDOMOutputProcessor extends AbstractOutputProcessor implements DOMOutputProcessor {
    private static String getXmlnsTagFor(Namespace ns) {
        if (ns.getPrefix().equals("")) {
            return "xmlns";
        }
        return ("xmlns" + ":") + ns.getPrefix();
    }

    public Document process(Document basedoc, Format format, org.jdom2.Document doc) {
        return printDocument(new FormatStack(format), new NamespaceStack(), basedoc, doc);
    }

    public Element process(Document basedoc, Format format, org.jdom2.Element element) {
        return printElement(new FormatStack(format), new NamespaceStack(), basedoc, element);
    }

    public List<Node> process(Document basedoc, Format format, List<? extends Content> list) {
        List<Node> ret = new ArrayList<>(list.size());
        FormatStack fstack = new FormatStack(format);
        NamespaceStack nstack = new NamespaceStack();
        for (Content c : list) {
            fstack.push();
            try {
                Node node = helperContentDispatcher(fstack, nstack, basedoc, c);
                if (node != null) {
                    ret.add(node);
                }
            } finally {
                fstack.pop();
            }
        }
        return ret;
    }

    public CDATASection process(Document basedoc, Format format, CDATA cdata) {
        List<CDATA> list = Collections.singletonList(cdata);
        FormatStack fstack = new FormatStack(format);
        Walker walker = buildWalker(fstack, list, false);
        if (walker.hasNext()) {
            Content c = walker.next();
            if (c == null) {
                return printCDATA(fstack, basedoc, new CDATA(walker.text()));
            }
            if (c.getCType() == Content.CType.CDATA) {
                return printCDATA(fstack, basedoc, (CDATA) c);
            }
        }
        return null;
    }

    public Text process(Document basedoc, Format format, org.jdom2.Text text) {
        List<org.jdom2.Text> list = Collections.singletonList(text);
        FormatStack fstack = new FormatStack(format);
        Walker walker = buildWalker(fstack, list, false);
        if (walker.hasNext()) {
            Content c = walker.next();
            if (c == null) {
                return printText(fstack, basedoc, new org.jdom2.Text(walker.text()));
            }
            if (c.getCType() == Content.CType.Text) {
                return printText(fstack, basedoc, (org.jdom2.Text) c);
            }
        }
        return null;
    }

    public Comment process(Document basedoc, Format format, org.jdom2.Comment comment) {
        return printComment(new FormatStack(format), basedoc, comment);
    }

    public ProcessingInstruction process(Document basedoc, Format format, org.jdom2.ProcessingInstruction pi) {
        return printProcessingInstruction(new FormatStack(format), basedoc, pi);
    }

    public EntityReference process(Document basedoc, Format format, EntityRef entity) {
        return printEntityRef(new FormatStack(format), basedoc, entity);
    }

    public Attr process(Document basedoc, Format format, Attribute attribute) {
        return printAttribute(new FormatStack(format), basedoc, attribute);
    }

    /* access modifiers changed from: protected */
    public Document printDocument(FormatStack fstack, NamespaceStack nstack, Document basedoc, org.jdom2.Document doc) {
        if (!fstack.isOmitDeclaration()) {
            basedoc.setXmlVersion("1.0");
        }
        int sz = doc.getContentSize();
        if (sz > 0) {
            for (int i = 0; i < sz; i++) {
                Content c = doc.getContent(i);
                Node n = null;
                switch (c.getCType()) {
                    case Comment:
                        n = printComment(fstack, basedoc, (org.jdom2.Comment) c);
                        break;
                    case Element:
                        n = printElement(fstack, nstack, basedoc, (org.jdom2.Element) c);
                        break;
                    case ProcessingInstruction:
                        n = printProcessingInstruction(fstack, basedoc, (org.jdom2.ProcessingInstruction) c);
                        break;
                }
                if (n != null) {
                    basedoc.appendChild(n);
                }
            }
        }
        return basedoc;
    }

    /* access modifiers changed from: protected */
    public ProcessingInstruction printProcessingInstruction(FormatStack fstack, Document basedoc, org.jdom2.ProcessingInstruction pi) {
        String target = pi.getTarget();
        String rawData = pi.getData();
        if (rawData == null || rawData.trim().length() == 0) {
            rawData = "";
        }
        return basedoc.createProcessingInstruction(target, rawData);
    }

    /* access modifiers changed from: protected */
    public Comment printComment(FormatStack fstack, Document basedoc, org.jdom2.Comment comment) {
        return basedoc.createComment(comment.getText());
    }

    /* access modifiers changed from: protected */
    public EntityReference printEntityRef(FormatStack fstack, Document basedoc, EntityRef entity) {
        return basedoc.createEntityReference(entity.getName());
    }

    /* access modifiers changed from: protected */
    public CDATASection printCDATA(FormatStack fstack, Document basedoc, CDATA cdata) {
        return basedoc.createCDATASection(cdata.getText());
    }

    /* access modifiers changed from: protected */
    public Text printText(FormatStack fstack, Document basedoc, org.jdom2.Text text) {
        return basedoc.createTextNode(text.getText());
    }

    /* access modifiers changed from: protected */
    public Attr printAttribute(FormatStack fstack, Document basedoc, Attribute attribute) {
        if (!attribute.isSpecified() && fstack.isSpecifiedAttributesOnly()) {
            return null;
        }
        Attr attr = basedoc.createAttributeNS(attribute.getNamespaceURI(), attribute.getQualifiedName());
        attr.setValue(attribute.getValue());
        return attr;
    }

    /* access modifiers changed from: protected */
    public Element printElement(FormatStack fstack, NamespaceStack nstack, Document basedoc, org.jdom2.Element element) {
        nstack.push(element);
        try {
            Format.TextMode textmode = fstack.getTextMode();
            String space = element.getAttributeValue("space", Namespace.XML_NAMESPACE);
            if ("default".equals(space)) {
                textmode = fstack.getDefaultMode();
            } else if ("preserve".equals(space)) {
                textmode = Format.TextMode.PRESERVE;
            }
            Element ret = basedoc.createElementNS(element.getNamespaceURI(), element.getQualifiedName());
            for (Namespace ns : nstack.addedForward()) {
                if (ns != Namespace.XML_NAMESPACE) {
                    ret.setAttributeNS(JDOMConstants.NS_URI_XMLNS, getXmlnsTagFor(ns), ns.getURI());
                }
            }
            if (element.hasAttributes()) {
                for (Attribute att : element.getAttributes()) {
                    Attr a = printAttribute(fstack, basedoc, att);
                    if (a != null) {
                        ret.setAttributeNodeNS(a);
                    }
                }
            }
            List<Content> content = element.getContent();
            if (!content.isEmpty()) {
                fstack.push();
                fstack.setTextMode(textmode);
                Walker walker = buildWalker(fstack, content, false);
                if (!walker.isAllText() && fstack.getPadBetween() != null) {
                    ret.appendChild(basedoc.createTextNode(fstack.getPadBetween()));
                }
                printContent(fstack, nstack, basedoc, ret, walker);
                if (!walker.isAllText() && fstack.getPadLast() != null) {
                    ret.appendChild(basedoc.createTextNode(fstack.getPadLast()));
                }
                fstack.pop();
            }
            nstack.pop();
            return ret;
        } catch (Throwable th) {
            nstack.pop();
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public void printContent(FormatStack fstack, NamespaceStack nstack, Document basedoc, Node target, Walker walker) {
        Node n;
        while (walker.hasNext()) {
            Content c = walker.next();
            if (c == null) {
                String text = walker.text();
                if (walker.isCDATA()) {
                    n = printCDATA(fstack, basedoc, new CDATA(text));
                } else {
                    n = printText(fstack, basedoc, new org.jdom2.Text(text));
                }
            } else {
                n = helperContentDispatcher(fstack, nstack, basedoc, c);
            }
            if (n != null) {
                target.appendChild(n);
            }
        }
    }

    /* access modifiers changed from: protected */
    public Node helperContentDispatcher(FormatStack fstack, NamespaceStack nstack, Document basedoc, Content content) {
        switch (content.getCType()) {
            case Comment:
                return printComment(fstack, basedoc, (org.jdom2.Comment) content);
            case DocType:
                return null;
            case Element:
                return printElement(fstack, nstack, basedoc, (org.jdom2.Element) content);
            case ProcessingInstruction:
                return printProcessingInstruction(fstack, basedoc, (org.jdom2.ProcessingInstruction) content);
            case CDATA:
                return printCDATA(fstack, basedoc, (CDATA) content);
            case EntityRef:
                return printEntityRef(fstack, basedoc, (EntityRef) content);
            case Text:
                return printText(fstack, basedoc, (org.jdom2.Text) content);
            default:
                throw new IllegalStateException("Unexpected Content " + content.getCType());
        }
    }
}
