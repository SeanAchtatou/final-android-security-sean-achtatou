package org.jdom2.output.support;

import java.util.List;
import org.jdom2.Content;

public abstract class AbstractOutputProcessor {
    /* access modifiers changed from: protected */
    public Walker buildWalker(FormatStack fstack, List<? extends Content> content, boolean escape) {
        switch (fstack.getTextMode()) {
            case PRESERVE:
                return new WalkerPRESERVE(content);
            case NORMALIZE:
                return new WalkerNORMALIZE(content, fstack, escape);
            case TRIM:
                return new WalkerTRIM(content, fstack, escape);
            case TRIM_FULL_WHITE:
                return new WalkerTRIM_FULL_WHITE(content, fstack, escape);
            default:
                return new WalkerPRESERVE(content);
        }
    }
}
