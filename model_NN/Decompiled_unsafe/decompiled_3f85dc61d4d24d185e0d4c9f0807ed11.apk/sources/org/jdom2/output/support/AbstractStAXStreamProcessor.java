package org.jdom2.output.support;

import com.fsck.k9.Account;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import org.jdom2.Attribute;
import org.jdom2.CDATA;
import org.jdom2.Comment;
import org.jdom2.Content;
import org.jdom2.DocType;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.EntityRef;
import org.jdom2.Namespace;
import org.jdom2.ProcessingInstruction;
import org.jdom2.Text;
import org.jdom2.Verifier;
import org.jdom2.output.Format;
import org.jdom2.util.NamespaceStack;

public abstract class AbstractStAXStreamProcessor extends AbstractOutputProcessor implements StAXStreamProcessor {
    public void process(XMLStreamWriter out, Format format, Document doc) throws XMLStreamException {
        printDocument(out, new FormatStack(format), new NamespaceStack(), doc);
        out.flush();
    }

    public void process(XMLStreamWriter out, Format format, DocType doctype) throws XMLStreamException {
        printDocType(out, new FormatStack(format), doctype);
        out.flush();
    }

    public void process(XMLStreamWriter out, Format format, Element element) throws XMLStreamException {
        printElement(out, new FormatStack(format), new NamespaceStack(), element);
        out.flush();
    }

    public void process(XMLStreamWriter out, Format format, List<? extends Content> list) throws XMLStreamException {
        FormatStack fstack = new FormatStack(format);
        printContent(out, fstack, new NamespaceStack(), buildWalker(fstack, list, false));
        out.flush();
    }

    public void process(XMLStreamWriter out, Format format, CDATA cdata) throws XMLStreamException {
        List<CDATA> list = Collections.singletonList(cdata);
        FormatStack fstack = new FormatStack(format);
        Walker walker = buildWalker(fstack, list, false);
        if (walker.hasNext()) {
            Content c = walker.next();
            if (c == null) {
                printCDATA(out, fstack, new CDATA(walker.text()));
            } else if (c.getCType() == Content.CType.CDATA) {
                printCDATA(out, fstack, (CDATA) c);
            }
        }
        out.flush();
    }

    public void process(XMLStreamWriter out, Format format, Text text) throws XMLStreamException {
        List<Text> list = Collections.singletonList(text);
        FormatStack fstack = new FormatStack(format);
        Walker walker = buildWalker(fstack, list, false);
        if (walker.hasNext()) {
            Content c = walker.next();
            if (c == null) {
                printText(out, fstack, new Text(walker.text()));
            } else if (c.getCType() == Content.CType.Text) {
                printText(out, fstack, (Text) c);
            }
        }
        out.flush();
    }

    public void process(XMLStreamWriter out, Format format, Comment comment) throws XMLStreamException {
        printComment(out, new FormatStack(format), comment);
        out.flush();
    }

    public void process(XMLStreamWriter out, Format format, ProcessingInstruction pi) throws XMLStreamException {
        FormatStack fstack = new FormatStack(format);
        fstack.setIgnoreTrAXEscapingPIs(true);
        printProcessingInstruction(out, fstack, pi);
        out.flush();
    }

    public void process(XMLStreamWriter out, Format format, EntityRef entity) throws XMLStreamException {
        printEntityRef(out, new FormatStack(format), entity);
        out.flush();
    }

    /* access modifiers changed from: protected */
    public void printDocument(XMLStreamWriter out, FormatStack fstack, NamespaceStack nstack, Document doc) throws XMLStreamException {
        if (fstack.isOmitDeclaration()) {
            out.writeStartDocument();
            if (fstack.getLineSeparator() != null) {
                out.writeCharacters(fstack.getLineSeparator());
            }
        } else if (fstack.isOmitEncoding()) {
            out.writeStartDocument("1.0");
            if (fstack.getLineSeparator() != null) {
                out.writeCharacters(fstack.getLineSeparator());
            }
        } else {
            out.writeStartDocument(fstack.getEncoding(), "1.0");
            if (fstack.getLineSeparator() != null) {
                out.writeCharacters(fstack.getLineSeparator());
            }
        }
        List<Content> list = doc.hasRootElement() ? doc.getContent() : new ArrayList<>(doc.getContentSize());
        if (list.isEmpty()) {
            int sz = doc.getContentSize();
            for (int i = 0; i < sz; i++) {
                list.add(doc.getContent(i));
            }
        }
        Walker walker = buildWalker(fstack, list, false);
        if (walker.hasNext()) {
            while (walker.hasNext()) {
                Content c = walker.next();
                if (c != null) {
                    switch (c.getCType()) {
                        case Comment:
                            printComment(out, fstack, (Comment) c);
                            continue;
                        case DocType:
                            printDocType(out, fstack, (DocType) c);
                            continue;
                        case Element:
                            printElement(out, fstack, nstack, (Element) c);
                            continue;
                        case ProcessingInstruction:
                            printProcessingInstruction(out, fstack, (ProcessingInstruction) c);
                            continue;
                        case Text:
                            String padding = ((Text) c).getText();
                            if (padding != null && Verifier.isAllXMLWhitespace(padding)) {
                                out.writeCharacters(padding);
                                break;
                            }
                    }
                } else {
                    String padding2 = walker.text();
                    if (padding2 != null && Verifier.isAllXMLWhitespace(padding2) && !walker.isCDATA()) {
                        out.writeCharacters(padding2);
                    }
                }
            }
            if (fstack.getLineSeparator() != null) {
                out.writeCharacters(fstack.getLineSeparator());
            }
        }
        out.writeEndDocument();
    }

    /* access modifiers changed from: protected */
    public void printDocType(XMLStreamWriter out, FormatStack fstack, DocType docType) throws XMLStreamException {
        String publicID = docType.getPublicID();
        String systemID = docType.getSystemID();
        String internalSubset = docType.getInternalSubset();
        boolean hasPublic = false;
        StringWriter sw = new StringWriter();
        sw.write("<!DOCTYPE ");
        sw.write(docType.getElementName());
        if (publicID != null) {
            sw.write(" PUBLIC \"");
            sw.write(publicID);
            sw.write("\"");
            hasPublic = true;
        }
        if (systemID != null) {
            if (!hasPublic) {
                sw.write(" SYSTEM");
            }
            sw.write(" \"");
            sw.write(systemID);
            sw.write("\"");
        }
        if (internalSubset != null && !internalSubset.equals("")) {
            sw.write(" [");
            sw.write(fstack.getLineSeparator());
            sw.write(docType.getInternalSubset());
            sw.write("]");
        }
        sw.write(Account.DEFAULT_QUOTE_PREFIX);
        out.writeDTD(sw.toString());
    }

    /* access modifiers changed from: protected */
    public void printProcessingInstruction(XMLStreamWriter out, FormatStack fstack, ProcessingInstruction pi) throws XMLStreamException {
        String target = pi.getTarget();
        String rawData = pi.getData();
        if (rawData == null || rawData.trim().length() <= 0) {
            out.writeProcessingInstruction(target);
        } else {
            out.writeProcessingInstruction(target, rawData);
        }
    }

    /* access modifiers changed from: protected */
    public void printComment(XMLStreamWriter out, FormatStack fstack, Comment comment) throws XMLStreamException {
        out.writeComment(comment.getText());
    }

    /* access modifiers changed from: protected */
    public void printEntityRef(XMLStreamWriter out, FormatStack fstack, EntityRef entity) throws XMLStreamException {
        out.writeEntityRef(entity.getName());
    }

    /* access modifiers changed from: protected */
    public void printCDATA(XMLStreamWriter out, FormatStack fstack, CDATA cdata) throws XMLStreamException {
        out.writeCData(cdata.getText());
    }

    /* access modifiers changed from: protected */
    public void printText(XMLStreamWriter out, FormatStack fstack, Text text) throws XMLStreamException {
        out.writeCharacters(text.getText());
    }

    /* access modifiers changed from: protected */
    public void printElement(XMLStreamWriter out, FormatStack fstack, NamespaceStack nstack, Element element) throws XMLStreamException {
        ArrayList<String> restore = new ArrayList<>();
        nstack.push(element);
        for (Namespace nsa : nstack.addedForward()) {
            restore.add(nsa.getPrefix());
            if ("".equals(nsa.getPrefix())) {
                out.setDefaultNamespace(nsa.getURI());
            } else {
                out.setPrefix(nsa.getPrefix(), nsa.getURI());
            }
        }
        List<Content> content = element.getContent();
        Format.TextMode textmode = fstack.getTextMode();
        Walker walker = null;
        if (!content.isEmpty()) {
            String space = element.getAttributeValue("space", Namespace.XML_NAMESPACE);
            if ("default".equals(space)) {
                textmode = fstack.getDefaultMode();
            } else if ("preserve".equals(space)) {
                textmode = Format.TextMode.PRESERVE;
            }
            fstack.push();
            try {
                fstack.setTextMode(textmode);
                walker = buildWalker(fstack, content, false);
                if (!walker.hasNext()) {
                    walker = null;
                }
                fstack.pop();
            } catch (Throwable th) {
                nstack.pop();
                Iterator it = restore.iterator();
                while (it.hasNext()) {
                    String pfx = (String) it.next();
                    Iterator i$ = nstack.iterator();
                    while (true) {
                        if (!i$.hasNext()) {
                            break;
                        }
                        Namespace nsa2 = i$.next();
                        if (nsa2.getPrefix().equals(pfx)) {
                            if ("".equals(nsa2.getPrefix())) {
                                out.setDefaultNamespace(nsa2.getURI());
                            } else {
                                out.setPrefix(nsa2.getPrefix(), nsa2.getURI());
                            }
                        }
                    }
                }
                throw th;
            }
        }
        boolean expandit = walker != null || fstack.isExpandEmptyElements();
        Namespace ns = element.getNamespace();
        if (expandit) {
            out.writeStartElement(ns.getPrefix(), element.getName(), ns.getURI());
            for (Namespace nsd : nstack.addedForward()) {
                printNamespace(out, fstack, nsd);
            }
            if (element.hasAttributes()) {
                for (Attribute attribute : element.getAttributes()) {
                    printAttribute(out, fstack, attribute);
                }
            }
            out.writeCharacters("");
            if (walker != null) {
                fstack.push();
                fstack.setTextMode(textmode);
                if (!walker.isAllText() && fstack.getPadBetween() != null) {
                    printText(out, fstack, new Text(fstack.getPadBetween()));
                }
                printContent(out, fstack, nstack, walker);
                if (!walker.isAllText() && fstack.getPadLast() != null) {
                    printText(out, fstack, new Text(fstack.getPadLast()));
                }
                fstack.pop();
            }
            out.writeEndElement();
        } else {
            out.writeEmptyElement(ns.getPrefix(), element.getName(), ns.getURI());
            for (Namespace nsd2 : nstack.addedForward()) {
                printNamespace(out, fstack, nsd2);
            }
            for (Attribute attribute2 : element.getAttributes()) {
                printAttribute(out, fstack, attribute2);
            }
            out.writeCharacters("");
        }
        nstack.pop();
        Iterator i$2 = restore.iterator();
        while (i$2.hasNext()) {
            String pfx2 = (String) i$2.next();
            Iterator i$3 = nstack.iterator();
            while (true) {
                if (!i$3.hasNext()) {
                    break;
                }
                Namespace nsa3 = i$3.next();
                if (nsa3.getPrefix().equals(pfx2)) {
                    if ("".equals(nsa3.getPrefix())) {
                        out.setDefaultNamespace(nsa3.getURI());
                    } else {
                        out.setPrefix(nsa3.getPrefix(), nsa3.getURI());
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void printContent(XMLStreamWriter out, FormatStack fstack, NamespaceStack nstack, Walker walker) throws XMLStreamException {
        while (walker.hasNext()) {
            Content content = walker.next();
            if (content != null) {
                switch (content.getCType()) {
                    case Comment:
                        printComment(out, fstack, (Comment) content);
                        continue;
                    case DocType:
                        printDocType(out, fstack, (DocType) content);
                        continue;
                    case Element:
                        printElement(out, fstack, nstack, (Element) content);
                        continue;
                    case ProcessingInstruction:
                        printProcessingInstruction(out, fstack, (ProcessingInstruction) content);
                        continue;
                    case Text:
                        printText(out, fstack, (Text) content);
                        continue;
                    case CDATA:
                        printCDATA(out, fstack, (CDATA) content);
                        continue;
                    case EntityRef:
                        printEntityRef(out, fstack, (EntityRef) content);
                        continue;
                    default:
                        throw new IllegalStateException("Unexpected Content " + content.getCType());
                }
            } else if (walker.isCDATA()) {
                printCDATA(out, fstack, new CDATA(walker.text()));
            } else {
                printText(out, fstack, new Text(walker.text()));
            }
        }
    }

    /* access modifiers changed from: protected */
    public void printNamespace(XMLStreamWriter out, FormatStack fstack, Namespace ns) throws XMLStreamException {
        out.writeNamespace(ns.getPrefix(), ns.getURI());
    }

    /* access modifiers changed from: protected */
    public void printAttribute(XMLStreamWriter out, FormatStack fstack, Attribute attribute) throws XMLStreamException {
        if (attribute.isSpecified() || !fstack.isSpecifiedAttributesOnly()) {
            Namespace ns = attribute.getNamespace();
            if (ns == Namespace.NO_NAMESPACE) {
                out.writeAttribute(attribute.getName(), attribute.getValue());
            } else {
                out.writeAttribute(ns.getPrefix(), ns.getURI(), attribute.getName(), attribute.getValue());
            }
        }
    }
}
