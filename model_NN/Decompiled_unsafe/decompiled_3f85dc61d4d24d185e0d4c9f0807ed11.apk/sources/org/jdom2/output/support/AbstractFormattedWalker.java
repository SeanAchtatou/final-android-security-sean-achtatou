package org.jdom2.output.support;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.jdom2.CDATA;
import org.jdom2.Content;
import org.jdom2.internal.ArrayCopy;
import org.jdom2.output.EscapeStrategy;
import org.jdom2.output.Format;

public abstract class AbstractFormattedWalker implements Walker {
    /* access modifiers changed from: private */
    public static final CDATA CDATATOKEN = new CDATA("");
    private static final Iterator<Content> EMPTYIT = new Iterator<Content>() {
        public boolean hasNext() {
            return false;
        }

        public Content next() {
            throw new NoSuchElementException("Cannot call next() on an empty iterator.");
        }

        public void remove() {
            throw new UnsupportedOperationException("Cannot remove from an empty iterator.");
        }
    };
    private final boolean alltext;
    private final boolean allwhite;
    private final Iterator<? extends Content> content;
    /* access modifiers changed from: private */
    public final String endofline;
    /* access modifiers changed from: private */
    public final EscapeStrategy escape;
    /* access modifiers changed from: private */
    public final FormatStack fstack;
    private boolean hasnext = true;
    private final MultiText holdingmt = new MultiText();
    /* access modifiers changed from: private */
    public final StringBuilder mtbuffer = new StringBuilder();
    /* access modifiers changed from: private */
    public Content[] mtdata = new Content[8];
    /* access modifiers changed from: private */
    public boolean mtgottext = false;
    private int mtpos = -1;
    /* access modifiers changed from: private */
    public boolean mtpostpad;
    /* access modifiers changed from: private */
    public int mtsize = 0;
    private Content[] mtsource = new Content[8];
    private int mtsourcesize = 0;
    /* access modifiers changed from: private */
    public String[] mttext = new String[8];
    private Boolean mtwasescape;
    private MultiText multitext = null;
    /* access modifiers changed from: private */
    public final String newlineindent;
    private Content pending = null;
    private MultiText pendingmt = null;

    protected enum Trim {
        LEFT,
        RIGHT,
        BOTH,
        COMPACT,
        NONE
    }

    /* access modifiers changed from: protected */
    public abstract void analyzeMultiText(MultiText multiText, int i, int i2);

    static /* synthetic */ int access$008(AbstractFormattedWalker x0) {
        int i = x0.mtsize;
        x0.mtsize = i + 1;
        return i;
    }

    protected final class MultiText {
        private MultiText() {
        }

        private void ensurespace() {
            if (AbstractFormattedWalker.this.mtsize >= AbstractFormattedWalker.this.mtdata.length) {
                Content[] unused = AbstractFormattedWalker.this.mtdata = (Content[]) ArrayCopy.copyOf(AbstractFormattedWalker.this.mtdata, AbstractFormattedWalker.this.mtsize + 1 + (AbstractFormattedWalker.this.mtsize / 2));
                String[] unused2 = AbstractFormattedWalker.this.mttext = (String[]) ArrayCopy.copyOf(AbstractFormattedWalker.this.mttext, AbstractFormattedWalker.this.mtdata.length);
            }
        }

        private void closeText() {
            if (AbstractFormattedWalker.this.mtbuffer.length() != 0) {
                ensurespace();
                AbstractFormattedWalker.this.mtdata[AbstractFormattedWalker.this.mtsize] = null;
                AbstractFormattedWalker.this.mttext[AbstractFormattedWalker.access$008(AbstractFormattedWalker.this)] = AbstractFormattedWalker.this.mtbuffer.toString();
                AbstractFormattedWalker.this.mtbuffer.setLength(0);
            }
        }

        public void appendText(Trim trim, String text) {
            if (text.length() != 0) {
                String toadd = null;
                switch (trim) {
                    case NONE:
                        toadd = text;
                        break;
                    case BOTH:
                        toadd = Format.trimBoth(text);
                        break;
                    case LEFT:
                        toadd = Format.trimLeft(text);
                        break;
                    case RIGHT:
                        toadd = Format.trimRight(text);
                        break;
                    case COMPACT:
                        toadd = Format.compact(text);
                        break;
                }
                if (toadd != null) {
                    AbstractFormattedWalker.this.mtbuffer.append(escapeText(toadd));
                    boolean unused = AbstractFormattedWalker.this.mtgottext = true;
                }
            }
        }

        private String escapeText(String text) {
            return (AbstractFormattedWalker.this.escape == null || !AbstractFormattedWalker.this.fstack.getEscapeOutput()) ? text : Format.escapeText(AbstractFormattedWalker.this.escape, AbstractFormattedWalker.this.endofline, text);
        }

        private String escapeCDATA(String text) {
            if (AbstractFormattedWalker.this.escape == null) {
            }
            return text;
        }

        public void appendCDATA(Trim trim, String text) {
            closeText();
            String toadd = null;
            switch (trim) {
                case NONE:
                    toadd = text;
                    break;
                case BOTH:
                    toadd = Format.trimBoth(text);
                    break;
                case LEFT:
                    toadd = Format.trimLeft(text);
                    break;
                case RIGHT:
                    toadd = Format.trimRight(text);
                    break;
                case COMPACT:
                    toadd = Format.compact(text);
                    break;
            }
            String toadd2 = escapeCDATA(toadd);
            ensurespace();
            AbstractFormattedWalker.this.mtdata[AbstractFormattedWalker.this.mtsize] = AbstractFormattedWalker.CDATATOKEN;
            AbstractFormattedWalker.this.mttext[AbstractFormattedWalker.access$008(AbstractFormattedWalker.this)] = toadd2;
            boolean unused = AbstractFormattedWalker.this.mtgottext = true;
        }

        /* access modifiers changed from: private */
        public void forceAppend(String text) {
            boolean unused = AbstractFormattedWalker.this.mtgottext = true;
            AbstractFormattedWalker.this.mtbuffer.append(text);
        }

        public void appendRaw(Content c) {
            closeText();
            ensurespace();
            AbstractFormattedWalker.this.mttext[AbstractFormattedWalker.this.mtsize] = null;
            AbstractFormattedWalker.this.mtdata[AbstractFormattedWalker.access$008(AbstractFormattedWalker.this)] = c;
            AbstractFormattedWalker.this.mtbuffer.setLength(0);
        }

        public void done() {
            if (AbstractFormattedWalker.this.mtpostpad && AbstractFormattedWalker.this.newlineindent != null) {
                AbstractFormattedWalker.this.mtbuffer.append(AbstractFormattedWalker.this.newlineindent);
            }
            if (AbstractFormattedWalker.this.mtgottext) {
                closeText();
            }
            AbstractFormattedWalker.this.mtbuffer.setLength(0);
        }
    }

    public AbstractFormattedWalker(List<? extends Content> xx, FormatStack fstack2, boolean doescape) {
        EscapeStrategy escapeStrategy;
        boolean z = false;
        this.fstack = fstack2;
        this.content = xx.isEmpty() ? EMPTYIT : xx.iterator();
        if (doescape) {
            escapeStrategy = fstack2.getEscapeStrategy();
        } else {
            escapeStrategy = null;
        }
        this.escape = escapeStrategy;
        this.newlineindent = fstack2.getPadBetween();
        this.endofline = fstack2.getLevelEOL();
        if (!this.content.hasNext()) {
            this.alltext = true;
            this.allwhite = true;
        } else {
            boolean atext = false;
            boolean awhite = false;
            this.pending = (Content) this.content.next();
            if (isTextLike(this.pending)) {
                this.pendingmt = buildMultiText(true);
                analyzeMultiText(this.pendingmt, 0, this.mtsourcesize);
                this.pendingmt.done();
                if (this.pending == null) {
                    atext = true;
                    awhite = this.mtsize == 0;
                }
                if (this.mtsize == 0) {
                    this.pendingmt = null;
                }
            }
            this.alltext = atext;
            this.allwhite = awhite;
        }
        this.hasnext = (this.pendingmt == null && this.pending == null) ? z : true;
    }

    public final Content next() {
        Content content2;
        Content ret;
        boolean z;
        boolean z2 = true;
        if (!this.hasnext) {
            throw new NoSuchElementException("Cannot walk off end of Content");
        }
        if (this.multitext != null && this.mtpos + 1 >= this.mtsize) {
            this.multitext = null;
            resetMultiText();
        }
        if (this.pendingmt != null) {
            if (!(this.mtwasescape == null || this.fstack.getEscapeOutput() == this.mtwasescape.booleanValue())) {
                this.mtsize = 0;
                this.mtwasescape = Boolean.valueOf(this.fstack.getEscapeOutput());
                analyzeMultiText(this.pendingmt, 0, this.mtsourcesize);
                this.pendingmt.done();
            }
            this.multitext = this.pendingmt;
            this.pendingmt = null;
        }
        if (this.multitext != null) {
            this.mtpos++;
            if (this.mttext[this.mtpos] == null) {
                ret = this.mtdata[this.mtpos];
            } else {
                ret = null;
            }
            if (this.mtpos + 1 < this.mtsize || this.pending != null) {
                z = true;
            } else {
                z = false;
            }
            this.hasnext = z;
            return ret;
        }
        Content ret2 = this.pending;
        if (this.content.hasNext()) {
            content2 = (Content) this.content.next();
        } else {
            content2 = null;
        }
        this.pending = content2;
        if (this.pending == null) {
            this.hasnext = false;
        } else if (isTextLike(this.pending)) {
            this.pendingmt = buildMultiText(false);
            analyzeMultiText(this.pendingmt, 0, this.mtsourcesize);
            this.pendingmt.done();
            if (this.mtsize > 0) {
                this.hasnext = true;
            } else if (this.pending == null || this.newlineindent == null) {
                this.pendingmt = null;
                if (this.pending == null) {
                    z2 = false;
                }
                this.hasnext = z2;
            } else {
                resetMultiText();
                this.pendingmt = this.holdingmt;
                this.pendingmt.forceAppend(this.newlineindent);
                this.pendingmt.done();
                this.hasnext = true;
            }
        } else {
            if (this.newlineindent != null) {
                resetMultiText();
                this.pendingmt = this.holdingmt;
                this.pendingmt.forceAppend(this.newlineindent);
                this.pendingmt.done();
            }
            this.hasnext = true;
        }
        return ret2;
    }

    private void resetMultiText() {
        this.mtsourcesize = 0;
        this.mtpos = -1;
        this.mtsize = 0;
        this.mtgottext = false;
        this.mtpostpad = false;
        this.mtwasescape = null;
        this.mtbuffer.setLength(0);
    }

    /* access modifiers changed from: protected */
    public final Content get(int index) {
        return this.mtsource[index];
    }

    public final boolean isAllText() {
        return this.alltext;
    }

    public final boolean hasNext() {
        return this.hasnext;
    }

    private final MultiText buildMultiText(boolean first) {
        boolean z;
        if (!first && this.newlineindent != null) {
            this.mtbuffer.append(this.newlineindent);
        }
        this.mtsourcesize = 0;
        do {
            if (this.mtsourcesize >= this.mtsource.length) {
                this.mtsource = (Content[]) ArrayCopy.copyOf(this.mtsource, this.mtsource.length * 2);
            }
            Content[] contentArr = this.mtsource;
            int i = this.mtsourcesize;
            this.mtsourcesize = i + 1;
            contentArr[i] = this.pending;
            this.pending = this.content.hasNext() ? (Content) this.content.next() : null;
            if (this.pending == null) {
                break;
            }
        } while (isTextLike(this.pending));
        if (this.pending != null) {
            z = true;
        } else {
            z = false;
        }
        this.mtpostpad = z;
        this.mtwasescape = Boolean.valueOf(this.fstack.getEscapeOutput());
        return this.holdingmt;
    }

    public final String text() {
        if (this.multitext == null || this.mtpos >= this.mtsize) {
            return null;
        }
        return this.mttext[this.mtpos];
    }

    public final boolean isCDATA() {
        if (this.multitext == null || this.mtpos >= this.mtsize || this.mttext[this.mtpos] == null || this.mtdata[this.mtpos] != CDATATOKEN) {
            return false;
        }
        return true;
    }

    public final boolean isAllWhitespace() {
        return this.allwhite;
    }

    private final boolean isTextLike(Content c) {
        switch (c.getCType()) {
            case Text:
            case CDATA:
            case EntityRef:
                return true;
            default:
                return false;
        }
    }
}
