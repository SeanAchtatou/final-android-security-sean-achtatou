package org.jdom2.output.support;

import java.util.List;
import org.jdom2.Content;
import org.jdom2.Verifier;
import org.jdom2.output.support.AbstractFormattedWalker;

public class WalkerNORMALIZE extends AbstractFormattedWalker {
    public WalkerNORMALIZE(List<? extends Content> content, FormatStack fstack, boolean escape) {
        super(content, fstack, escape);
    }

    private boolean isSpaceFirst(String text) {
        if (text.length() > 0) {
            return Verifier.isXMLWhitespace(text.charAt(0));
        }
        return false;
    }

    private boolean isSpaceLast(String text) {
        int tlen = text.length();
        if (tlen <= 0 || !Verifier.isXMLWhitespace(text.charAt(tlen - 1))) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void analyzeMultiText(AbstractFormattedWalker.MultiText mtext, int offset, int len) {
        boolean needspace = false;
        boolean between = false;
        for (int i = 0; i < len; i++) {
            Content c = get(offset + i);
            switch (c.getCType()) {
                case Text:
                    String ttext = c.getValue();
                    if (Verifier.isAllXMLWhitespace(ttext)) {
                        if (between && ttext.length() > 0) {
                            needspace = true;
                            break;
                        }
                    } else {
                        if (between && (needspace || isSpaceFirst(ttext))) {
                            mtext.appendText(AbstractFormattedWalker.Trim.NONE, " ");
                        }
                        mtext.appendText(AbstractFormattedWalker.Trim.COMPACT, ttext);
                        between = true;
                        needspace = isSpaceLast(ttext);
                        break;
                    }
                case CDATA:
                    String ttext2 = c.getValue();
                    if (Verifier.isAllXMLWhitespace(ttext2)) {
                        if (between && ttext2.length() > 0) {
                            needspace = true;
                            break;
                        }
                    } else {
                        if (between && (needspace || isSpaceFirst(ttext2))) {
                            mtext.appendText(AbstractFormattedWalker.Trim.NONE, " ");
                        }
                        mtext.appendCDATA(AbstractFormattedWalker.Trim.COMPACT, ttext2);
                        between = true;
                        needspace = isSpaceLast(ttext2);
                        break;
                    }
                default:
                    if (between && needspace) {
                        mtext.appendText(AbstractFormattedWalker.Trim.NONE, " ");
                    }
                    mtext.appendRaw(c);
                    between = true;
                    needspace = false;
                    break;
            }
        }
    }
}
