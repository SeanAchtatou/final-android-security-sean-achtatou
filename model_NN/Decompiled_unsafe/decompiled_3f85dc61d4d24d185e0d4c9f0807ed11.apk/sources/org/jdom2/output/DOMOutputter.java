package org.jdom2.output;

import java.util.List;
import org.jdom2.Attribute;
import org.jdom2.CDATA;
import org.jdom2.Content;
import org.jdom2.DocType;
import org.jdom2.EntityRef;
import org.jdom2.JDOMException;
import org.jdom2.adapters.DOMAdapter;
import org.jdom2.adapters.JAXPDOMAdapter;
import org.jdom2.internal.ReflectionConstructor;
import org.jdom2.output.support.AbstractDOMOutputProcessor;
import org.jdom2.output.support.DOMOutputProcessor;
import org.w3c.dom.Attr;
import org.w3c.dom.CDATASection;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.w3c.dom.EntityReference;
import org.w3c.dom.Node;
import org.w3c.dom.ProcessingInstruction;
import org.w3c.dom.Text;

public class DOMOutputter {
    private static final DOMAdapter DEFAULT_ADAPTER = new JAXPDOMAdapter();
    private static final DOMOutputProcessor DEFAULT_PROCESSOR = new DefaultDOMOutputProcessor();
    private DOMAdapter adapter;
    private Format format;
    private DOMOutputProcessor processor;

    private static final class DefaultDOMOutputProcessor extends AbstractDOMOutputProcessor {
        private DefaultDOMOutputProcessor() {
        }
    }

    public DOMOutputter() {
        this(null, null, null);
    }

    public DOMOutputter(DOMOutputProcessor processor2) {
        this(null, null, processor2);
    }

    public DOMOutputter(DOMAdapter adapter2, Format format2, DOMOutputProcessor processor2) {
        this.adapter = adapter2 == null ? DEFAULT_ADAPTER : adapter2;
        this.format = format2 == null ? Format.getRawFormat() : format2;
        this.processor = processor2 == null ? DEFAULT_PROCESSOR : processor2;
    }

    @Deprecated
    public DOMOutputter(String adapterClass) {
        if (adapterClass == null) {
            this.adapter = DEFAULT_ADAPTER;
        } else {
            this.adapter = (DOMAdapter) ReflectionConstructor.construct(adapterClass, DOMAdapter.class);
        }
    }

    public DOMOutputter(DOMAdapter adapter2) {
        this.adapter = adapter2 == null ? DEFAULT_ADAPTER : adapter2;
    }

    public DOMAdapter getDOMAdapter() {
        return this.adapter;
    }

    public void setDOMAdapter(DOMAdapter adapter2) {
        if (adapter2 == null) {
            adapter2 = DEFAULT_ADAPTER;
        }
        this.adapter = adapter2;
    }

    public Format getFormat() {
        return this.format;
    }

    public void setFormat(Format format2) {
        if (format2 == null) {
            format2 = Format.getRawFormat();
        }
        this.format = format2;
    }

    public DOMOutputProcessor getDOMOutputProcessor() {
        return this.processor;
    }

    public void setDOMOutputProcessor(DOMOutputProcessor processor2) {
        if (processor2 == null) {
            processor2 = DEFAULT_PROCESSOR;
        }
        this.processor = processor2;
    }

    @Deprecated
    public void setForceNamespaceAware(boolean flag) {
    }

    @Deprecated
    public boolean getForceNamespaceAware() {
        return true;
    }

    public Document output(org.jdom2.Document document) throws JDOMException {
        return this.processor.process(this.adapter.createDocument(document.getDocType()), this.format, document);
    }

    public DocumentType output(DocType doctype) throws JDOMException {
        return this.adapter.createDocument(doctype).getDoctype();
    }

    public Element output(org.jdom2.Element element) throws JDOMException {
        return this.processor.process(this.adapter.createDocument(), this.format, element);
    }

    public Text output(org.jdom2.Text text) throws JDOMException {
        return this.processor.process(this.adapter.createDocument(), this.format, text);
    }

    public CDATASection output(CDATA cdata) throws JDOMException {
        return this.processor.process(this.adapter.createDocument(), this.format, cdata);
    }

    public ProcessingInstruction output(org.jdom2.ProcessingInstruction pi) throws JDOMException {
        return this.processor.process(this.adapter.createDocument(), this.format, pi);
    }

    public Comment output(org.jdom2.Comment comment) throws JDOMException {
        return this.processor.process(this.adapter.createDocument(), this.format, comment);
    }

    public EntityReference output(EntityRef entity) throws JDOMException {
        return this.processor.process(this.adapter.createDocument(), this.format, entity);
    }

    public Attr output(Attribute attribute) throws JDOMException {
        return this.processor.process(this.adapter.createDocument(), this.format, attribute);
    }

    public List<Node> output(List<? extends Content> list) throws JDOMException {
        return this.processor.process(this.adapter.createDocument(), this.format, list);
    }

    public Element output(Document basedoc, org.jdom2.Element element) throws JDOMException {
        return this.processor.process(basedoc, this.format, element);
    }

    public Text output(Document basedoc, org.jdom2.Text text) throws JDOMException {
        return this.processor.process(basedoc, this.format, text);
    }

    public CDATASection output(Document basedoc, CDATA cdata) throws JDOMException {
        return this.processor.process(basedoc, this.format, cdata);
    }

    public ProcessingInstruction output(Document basedoc, org.jdom2.ProcessingInstruction pi) throws JDOMException {
        return this.processor.process(basedoc, this.format, pi);
    }

    public Comment output(Document basedoc, org.jdom2.Comment comment) throws JDOMException {
        return this.processor.process(basedoc, this.format, comment);
    }

    public EntityReference output(Document basedoc, EntityRef entity) throws JDOMException {
        return this.processor.process(basedoc, this.format, entity);
    }

    public Attr output(Document basedoc, Attribute attribute) throws JDOMException {
        return this.processor.process(basedoc, this.format, attribute);
    }

    public List<Node> output(Document basedoc, List<? extends Content> list) throws JDOMException {
        return this.processor.process(basedoc, this.format, list);
    }
}
