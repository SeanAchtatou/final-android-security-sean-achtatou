package org.jdom2.output;

import java.util.List;
import org.jdom2.CDATA;
import org.jdom2.Comment;
import org.jdom2.Content;
import org.jdom2.DocType;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.EntityRef;
import org.jdom2.JDOMConstants;
import org.jdom2.JDOMException;
import org.jdom2.ProcessingInstruction;
import org.jdom2.Text;
import org.jdom2.output.support.AbstractSAXOutputProcessor;
import org.jdom2.output.support.SAXOutputProcessor;
import org.jdom2.output.support.SAXTarget;
import org.xml.sax.ContentHandler;
import org.xml.sax.DTDHandler;
import org.xml.sax.EntityResolver;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.SAXParseException;
import org.xml.sax.ext.DeclHandler;
import org.xml.sax.ext.LexicalHandler;

public class SAXOutputter {
    private static final SAXOutputProcessor DEFAULT_PROCESSOR = new DefaultSAXOutputProcessor();
    private ContentHandler contentHandler;
    private DeclHandler declHandler;
    private boolean declareNamespaces;
    private DTDHandler dtdHandler;
    private EntityResolver entityResolver;
    private ErrorHandler errorHandler;
    private Format format;
    private LexicalHandler lexicalHandler;
    private SAXOutputProcessor processor;
    private boolean reportDtdEvents;

    private static final class DefaultSAXOutputProcessor extends AbstractSAXOutputProcessor {
        private DefaultSAXOutputProcessor() {
        }
    }

    public SAXOutputter() {
        this.declareNamespaces = false;
        this.reportDtdEvents = true;
        this.processor = DEFAULT_PROCESSOR;
        this.format = Format.getRawFormat();
    }

    public SAXOutputter(ContentHandler contentHandler2) {
        this(contentHandler2, null, null, null, null);
    }

    public SAXOutputter(ContentHandler contentHandler2, ErrorHandler errorHandler2, DTDHandler dtdHandler2, EntityResolver entityResolver2) {
        this(contentHandler2, errorHandler2, dtdHandler2, entityResolver2, null);
    }

    public SAXOutputter(ContentHandler contentHandler2, ErrorHandler errorHandler2, DTDHandler dtdHandler2, EntityResolver entityResolver2, LexicalHandler lexicalHandler2) {
        this.declareNamespaces = false;
        this.reportDtdEvents = true;
        this.processor = DEFAULT_PROCESSOR;
        this.format = Format.getRawFormat();
        this.contentHandler = contentHandler2;
        this.errorHandler = errorHandler2;
        this.dtdHandler = dtdHandler2;
        this.entityResolver = entityResolver2;
        this.lexicalHandler = lexicalHandler2;
    }

    public SAXOutputter(SAXOutputProcessor processor2, Format format2, ContentHandler contentHandler2, ErrorHandler errorHandler2, DTDHandler dtdHandler2, EntityResolver entityResolver2, LexicalHandler lexicalHandler2) {
        this.declareNamespaces = false;
        this.reportDtdEvents = true;
        this.processor = DEFAULT_PROCESSOR;
        this.format = Format.getRawFormat();
        this.processor = processor2 == null ? DEFAULT_PROCESSOR : processor2;
        this.format = format2 == null ? Format.getRawFormat() : format2;
        this.contentHandler = contentHandler2;
        this.errorHandler = errorHandler2;
        this.dtdHandler = dtdHandler2;
        this.entityResolver = entityResolver2;
        this.lexicalHandler = lexicalHandler2;
    }

    public void setContentHandler(ContentHandler contentHandler2) {
        this.contentHandler = contentHandler2;
    }

    public ContentHandler getContentHandler() {
        return this.contentHandler;
    }

    public void setErrorHandler(ErrorHandler errorHandler2) {
        this.errorHandler = errorHandler2;
    }

    public ErrorHandler getErrorHandler() {
        return this.errorHandler;
    }

    public void setDTDHandler(DTDHandler dtdHandler2) {
        this.dtdHandler = dtdHandler2;
    }

    public DTDHandler getDTDHandler() {
        return this.dtdHandler;
    }

    public void setEntityResolver(EntityResolver entityResolver2) {
        this.entityResolver = entityResolver2;
    }

    public EntityResolver getEntityResolver() {
        return this.entityResolver;
    }

    public void setLexicalHandler(LexicalHandler lexicalHandler2) {
        this.lexicalHandler = lexicalHandler2;
    }

    public LexicalHandler getLexicalHandler() {
        return this.lexicalHandler;
    }

    public void setDeclHandler(DeclHandler declHandler2) {
        this.declHandler = declHandler2;
    }

    public DeclHandler getDeclHandler() {
        return this.declHandler;
    }

    public boolean getReportNamespaceDeclarations() {
        return this.declareNamespaces;
    }

    public void setReportNamespaceDeclarations(boolean declareNamespaces2) {
        this.declareNamespaces = declareNamespaces2;
    }

    public boolean getReportDTDEvents() {
        return this.reportDtdEvents;
    }

    public void setReportDTDEvents(boolean reportDtdEvents2) {
        this.reportDtdEvents = reportDtdEvents2;
    }

    public void setFeature(String name, boolean value) throws SAXNotRecognizedException, SAXNotSupportedException {
        if (JDOMConstants.SAX_FEATURE_NAMESPACE_PREFIXES.equals(name)) {
            setReportNamespaceDeclarations(value);
        } else if (JDOMConstants.SAX_FEATURE_NAMESPACES.equals(name)) {
            if (!value) {
                throw new SAXNotSupportedException(name);
            }
        } else if (JDOMConstants.SAX_FEATURE_VALIDATION.equals(name)) {
            setReportDTDEvents(value);
        } else {
            throw new SAXNotRecognizedException(name);
        }
    }

    public boolean getFeature(String name) throws SAXNotRecognizedException, SAXNotSupportedException {
        if (JDOMConstants.SAX_FEATURE_NAMESPACE_PREFIXES.equals(name)) {
            return this.declareNamespaces;
        }
        if (JDOMConstants.SAX_FEATURE_NAMESPACES.equals(name)) {
            return true;
        }
        if (JDOMConstants.SAX_FEATURE_VALIDATION.equals(name)) {
            return this.reportDtdEvents;
        }
        throw new SAXNotRecognizedException(name);
    }

    public void setProperty(String name, Object value) throws SAXNotRecognizedException, SAXNotSupportedException {
        if (JDOMConstants.SAX_PROPERTY_LEXICAL_HANDLER.equals(name) || JDOMConstants.SAX_PROPERTY_LEXICAL_HANDLER_ALT.equals(name)) {
            setLexicalHandler((LexicalHandler) value);
        } else if (JDOMConstants.SAX_PROPERTY_DECLARATION_HANDLER.equals(name) || JDOMConstants.SAX_PROPERTY_DECLARATION_HANDLER_ALT.equals(name)) {
            setDeclHandler((DeclHandler) value);
        } else {
            throw new SAXNotRecognizedException(name);
        }
    }

    public Object getProperty(String name) throws SAXNotRecognizedException, SAXNotSupportedException {
        if (JDOMConstants.SAX_PROPERTY_LEXICAL_HANDLER.equals(name) || JDOMConstants.SAX_PROPERTY_LEXICAL_HANDLER_ALT.equals(name)) {
            return getLexicalHandler();
        }
        if (JDOMConstants.SAX_PROPERTY_DECLARATION_HANDLER.equals(name) || JDOMConstants.SAX_PROPERTY_DECLARATION_HANDLER_ALT.equals(name)) {
            return getDeclHandler();
        }
        throw new SAXNotRecognizedException(name);
    }

    public SAXOutputProcessor getSAXOutputProcessor() {
        return this.processor;
    }

    public void setSAXOutputProcessor(SAXOutputProcessor processor2) {
        if (processor2 == null) {
            processor2 = DEFAULT_PROCESSOR;
        }
        this.processor = processor2;
    }

    public Format getFormat() {
        return this.format;
    }

    public void setFormat(Format format2) {
        if (format2 == null) {
            format2 = Format.getRawFormat();
        }
        this.format = format2;
    }

    private final SAXTarget buildTarget(Document doc) {
        DocType dt;
        String publicID = null;
        String systemID = null;
        if (!(doc == null || (dt = doc.getDocType()) == null)) {
            publicID = dt.getPublicID();
            systemID = dt.getSystemID();
        }
        return new SAXTarget(this.contentHandler, this.errorHandler, this.dtdHandler, this.entityResolver, this.lexicalHandler, this.declHandler, this.declareNamespaces, this.reportDtdEvents, publicID, systemID);
    }

    public void output(Document document) throws JDOMException {
        this.processor.process(buildTarget(document), this.format, document);
    }

    public void output(List<? extends Content> nodes) throws JDOMException {
        this.processor.processAsDocument(buildTarget(null), this.format, nodes);
    }

    public void output(Element node) throws JDOMException {
        this.processor.processAsDocument(buildTarget(null), this.format, node);
    }

    public void outputFragment(List<? extends Content> nodes) throws JDOMException {
        if (nodes != null) {
            this.processor.process(buildTarget(null), this.format, nodes);
        }
    }

    public void outputFragment(Content node) throws JDOMException {
        if (node != null) {
            SAXTarget out = buildTarget(null);
            switch (node.getCType()) {
                case CDATA:
                    this.processor.process(out, this.format, (CDATA) node);
                    return;
                case Comment:
                    this.processor.process(out, this.format, (Comment) node);
                    return;
                case Element:
                    this.processor.process(out, this.format, (Element) node);
                    return;
                case EntityRef:
                    this.processor.process(out, this.format, (EntityRef) node);
                    return;
                case ProcessingInstruction:
                    this.processor.process(out, this.format, (ProcessingInstruction) node);
                    return;
                case Text:
                    this.processor.process(out, this.format, (Text) node);
                    return;
                default:
                    handleError(new JDOMException("Invalid element content: " + node));
                    return;
            }
        }
    }

    private void handleError(JDOMException exception) throws JDOMException {
        if (this.errorHandler != null) {
            try {
                this.errorHandler.error(new SAXParseException(exception.getMessage(), null, exception));
            } catch (SAXException se) {
                if (se.getException() instanceof JDOMException) {
                    throw ((JDOMException) se.getException());
                }
                throw new JDOMException(se.getMessage(), se);
            }
        } else {
            throw exception;
        }
    }

    @Deprecated
    public JDOMLocator getLocator() {
        return null;
    }
}
