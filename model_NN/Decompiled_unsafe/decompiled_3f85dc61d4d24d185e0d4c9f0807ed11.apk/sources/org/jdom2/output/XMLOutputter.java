package org.jdom2.output;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.List;
import org.jdom2.CDATA;
import org.jdom2.Comment;
import org.jdom2.Content;
import org.jdom2.DocType;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.EntityRef;
import org.jdom2.ProcessingInstruction;
import org.jdom2.Text;
import org.jdom2.output.support.AbstractXMLOutputProcessor;
import org.jdom2.output.support.FormatStack;
import org.jdom2.output.support.XMLOutputProcessor;

public final class XMLOutputter implements Cloneable {
    private static final DefaultXMLProcessor DEFAULTPROCESSOR = new DefaultXMLProcessor();
    private Format myFormat;
    private XMLOutputProcessor myProcessor;

    private static final Writer makeWriter(OutputStream out, Format format) throws UnsupportedEncodingException {
        return new BufferedWriter(new OutputStreamWriter(new BufferedOutputStream(out), format.getEncoding()));
    }

    private static final class DefaultXMLProcessor extends AbstractXMLOutputProcessor {
        private DefaultXMLProcessor() {
        }

        public String escapeAttributeEntities(String str, Format format) {
            StringWriter sw = new StringWriter();
            try {
                super.attributeEscapedEntitiesFilter(sw, new FormatStack(format), str);
            } catch (IOException e) {
            }
            return sw.toString();
        }

        public final String escapeElementEntities(String str, Format format) {
            return Format.escapeText(format.getEscapeStrategy(), format.getLineSeparator(), str);
        }
    }

    public XMLOutputter(Format format, XMLOutputProcessor processor) {
        this.myFormat = null;
        this.myProcessor = null;
        this.myFormat = format == null ? Format.getRawFormat() : format.clone();
        this.myProcessor = processor == null ? DEFAULTPROCESSOR : processor;
    }

    public XMLOutputter() {
        this(null, null);
    }

    public XMLOutputter(XMLOutputter that) {
        this(that.myFormat, null);
    }

    public XMLOutputter(Format format) {
        this(format, null);
    }

    public XMLOutputter(XMLOutputProcessor processor) {
        this(null, processor);
    }

    public void setFormat(Format newFormat) {
        this.myFormat = newFormat.clone();
    }

    public Format getFormat() {
        return this.myFormat;
    }

    public XMLOutputProcessor getXMLOutputProcessor() {
        return this.myProcessor;
    }

    public void setXMLOutputProcessor(XMLOutputProcessor processor) {
        this.myProcessor = processor;
    }

    public final void output(Document doc, OutputStream out) throws IOException {
        output(doc, makeWriter(out, this.myFormat));
    }

    public final void output(DocType doctype, OutputStream out) throws IOException {
        output(doctype, makeWriter(out, this.myFormat));
    }

    public final void output(Element element, OutputStream out) throws IOException {
        output(element, makeWriter(out, this.myFormat));
    }

    public final void outputElementContent(Element element, OutputStream out) throws IOException {
        outputElementContent(element, makeWriter(out, this.myFormat));
    }

    public final void output(List<? extends Content> list, OutputStream out) throws IOException {
        output(list, makeWriter(out, this.myFormat));
    }

    public final void output(CDATA cdata, OutputStream out) throws IOException {
        output(cdata, makeWriter(out, this.myFormat));
    }

    public final void output(Text text, OutputStream out) throws IOException {
        output(text, makeWriter(out, this.myFormat));
    }

    public final void output(Comment comment, OutputStream out) throws IOException {
        output(comment, makeWriter(out, this.myFormat));
    }

    public final void output(ProcessingInstruction pi, OutputStream out) throws IOException {
        output(pi, makeWriter(out, this.myFormat));
    }

    public void output(EntityRef entity, OutputStream out) throws IOException {
        output(entity, makeWriter(out, this.myFormat));
    }

    public final String outputString(Document doc) {
        StringWriter out = new StringWriter();
        try {
            output(doc, out);
        } catch (IOException e) {
        }
        return out.toString();
    }

    public final String outputString(DocType doctype) {
        StringWriter out = new StringWriter();
        try {
            output(doctype, out);
        } catch (IOException e) {
        }
        return out.toString();
    }

    public final String outputString(Element element) {
        StringWriter out = new StringWriter();
        try {
            output(element, out);
        } catch (IOException e) {
        }
        return out.toString();
    }

    public final String outputString(List<? extends Content> list) {
        StringWriter out = new StringWriter();
        try {
            output(list, out);
        } catch (IOException e) {
        }
        return out.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: org.jdom2.output.XMLOutputter.output(org.jdom2.CDATA, java.io.Writer):void
     arg types: [org.jdom2.CDATA, java.io.StringWriter]
     candidates:
      org.jdom2.output.XMLOutputter.output(java.util.List<? extends org.jdom2.Content>, java.io.OutputStream):void
      org.jdom2.output.XMLOutputter.output(java.util.List<? extends org.jdom2.Content>, java.io.Writer):void
      org.jdom2.output.XMLOutputter.output(org.jdom2.CDATA, java.io.OutputStream):void
      org.jdom2.output.XMLOutputter.output(org.jdom2.Comment, java.io.OutputStream):void
      org.jdom2.output.XMLOutputter.output(org.jdom2.Comment, java.io.Writer):void
      org.jdom2.output.XMLOutputter.output(org.jdom2.DocType, java.io.OutputStream):void
      org.jdom2.output.XMLOutputter.output(org.jdom2.DocType, java.io.Writer):void
      org.jdom2.output.XMLOutputter.output(org.jdom2.Document, java.io.OutputStream):void
      org.jdom2.output.XMLOutputter.output(org.jdom2.Document, java.io.Writer):void
      org.jdom2.output.XMLOutputter.output(org.jdom2.Element, java.io.OutputStream):void
      org.jdom2.output.XMLOutputter.output(org.jdom2.Element, java.io.Writer):void
      org.jdom2.output.XMLOutputter.output(org.jdom2.EntityRef, java.io.OutputStream):void
      org.jdom2.output.XMLOutputter.output(org.jdom2.EntityRef, java.io.Writer):void
      org.jdom2.output.XMLOutputter.output(org.jdom2.ProcessingInstruction, java.io.OutputStream):void
      org.jdom2.output.XMLOutputter.output(org.jdom2.ProcessingInstruction, java.io.Writer):void
      org.jdom2.output.XMLOutputter.output(org.jdom2.Text, java.io.OutputStream):void
      org.jdom2.output.XMLOutputter.output(org.jdom2.Text, java.io.Writer):void
      org.jdom2.output.XMLOutputter.output(org.jdom2.CDATA, java.io.Writer):void */
    public final String outputString(CDATA cdata) {
        StringWriter out = new StringWriter();
        try {
            output(cdata, (Writer) out);
        } catch (IOException e) {
        }
        return out.toString();
    }

    public final String outputString(Text text) {
        StringWriter out = new StringWriter();
        try {
            output(text, out);
        } catch (IOException e) {
        }
        return out.toString();
    }

    public final String outputString(Comment comment) {
        StringWriter out = new StringWriter();
        try {
            output(comment, out);
        } catch (IOException e) {
        }
        return out.toString();
    }

    public final String outputString(ProcessingInstruction pi) {
        StringWriter out = new StringWriter();
        try {
            output(pi, out);
        } catch (IOException e) {
        }
        return out.toString();
    }

    public final String outputString(EntityRef entity) {
        StringWriter out = new StringWriter();
        try {
            output(entity, out);
        } catch (IOException e) {
        }
        return out.toString();
    }

    public final String outputElementContentString(Element element) {
        StringWriter out = new StringWriter();
        try {
            outputElementContent(element, out);
        } catch (IOException e) {
        }
        return out.toString();
    }

    public final void output(Document doc, Writer out) throws IOException {
        this.myProcessor.process(out, this.myFormat, doc);
        out.flush();
    }

    public final void output(DocType doctype, Writer out) throws IOException {
        this.myProcessor.process(out, this.myFormat, doctype);
        out.flush();
    }

    public final void output(Element element, Writer out) throws IOException {
        this.myProcessor.process(out, this.myFormat, element);
        out.flush();
    }

    public final void outputElementContent(Element element, Writer out) throws IOException {
        this.myProcessor.process(out, this.myFormat, element.getContent());
        out.flush();
    }

    public final void output(List<? extends Content> list, Writer out) throws IOException {
        this.myProcessor.process(out, this.myFormat, list);
        out.flush();
    }

    public final void output(CDATA cdata, Writer out) throws IOException {
        this.myProcessor.process(out, this.myFormat, cdata);
        out.flush();
    }

    public final void output(Text text, Writer out) throws IOException {
        this.myProcessor.process(out, this.myFormat, text);
        out.flush();
    }

    public final void output(Comment comment, Writer out) throws IOException {
        this.myProcessor.process(out, this.myFormat, comment);
        out.flush();
    }

    public final void output(ProcessingInstruction pi, Writer out) throws IOException {
        this.myProcessor.process(out, this.myFormat, pi);
        out.flush();
    }

    public final void output(EntityRef entity, Writer out) throws IOException {
        this.myProcessor.process(out, this.myFormat, entity);
        out.flush();
    }

    public String escapeAttributeEntities(String str) {
        return DEFAULTPROCESSOR.escapeAttributeEntities(str, this.myFormat);
    }

    public String escapeElementEntities(String str) {
        return DEFAULTPROCESSOR.escapeElementEntities(str, this.myFormat);
    }

    public XMLOutputter clone() {
        try {
            return (XMLOutputter) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e.toString());
        }
    }

    public String toString() {
        StringBuilder buffer = new StringBuilder();
        buffer.append("XMLOutputter[omitDeclaration = ");
        buffer.append(this.myFormat.omitDeclaration);
        buffer.append(", ");
        buffer.append("encoding = ");
        buffer.append(this.myFormat.encoding);
        buffer.append(", ");
        buffer.append("omitEncoding = ");
        buffer.append(this.myFormat.omitEncoding);
        buffer.append(", ");
        buffer.append("indent = '");
        buffer.append(this.myFormat.indent);
        buffer.append("'");
        buffer.append(", ");
        buffer.append("expandEmptyElements = ");
        buffer.append(this.myFormat.expandEmptyElements);
        buffer.append(", ");
        buffer.append("lineSeparator = '");
        for (char ch : this.myFormat.lineSeparator.toCharArray()) {
            switch (ch) {
                case 9:
                    buffer.append("\\t");
                    break;
                case 10:
                    buffer.append("\\n");
                    break;
                case 11:
                case 12:
                default:
                    buffer.append("[" + ((int) ch) + "]");
                    break;
                case 13:
                    buffer.append("\\r");
                    break;
            }
        }
        buffer.append("', ");
        buffer.append("textMode = ");
        buffer.append(this.myFormat.mode + "]");
        return buffer.toString();
    }
}
