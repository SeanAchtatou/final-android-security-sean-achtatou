package org.jdom2;

import org.jdom2.internal.ArrayCopy;

final class StringBin {
    private static final int DEFAULTCAP = 1023;
    private static final int GROW = 4;
    private static final int MAXBUCKET = 64;
    private String[][] buckets;
    private int[] lengths;
    private int mask;

    public StringBin() {
        this(DEFAULTCAP);
    }

    public StringBin(int capacity) {
        this.mask = 0;
        if (capacity < 0) {
            throw new IllegalArgumentException("Can not have a negative capacity");
        }
        int capacity2 = capacity - 1;
        int capacity3 = (capacity2 < DEFAULTCAP ? DEFAULTCAP : capacity2) / 3;
        int shift = 0;
        while (capacity3 != 0) {
            capacity3 >>>= 1;
            shift++;
        }
        this.mask = (1 << shift) - 1;
        this.buckets = new String[(this.mask + 1)][];
        this.lengths = new int[this.buckets.length];
    }

    private final int locate(int hash, String value, String[] bucket, int length) {
        int cmp;
        int cmp2;
        int left = 0;
        int right = length - 1;
        while (left <= right) {
            int mid = (left + right) >>> 1;
            if (bucket[mid].hashCode() > hash) {
                right = mid - 1;
            } else if (bucket[mid].hashCode() < hash) {
                left = mid + 1;
            } else {
                int cmp3 = value.compareTo(bucket[mid]);
                if (cmp3 == 0) {
                    return mid;
                }
                if (cmp3 < 0) {
                    do {
                        mid--;
                        if (mid < left || bucket[mid].hashCode() != hash) {
                            return (-(mid + 1)) - 1;
                        }
                        cmp2 = value.compareTo(bucket[mid]);
                        if (cmp2 == 0) {
                            return mid;
                        }
                    } while (cmp2 <= 0);
                    return (-(mid + 1)) - 1;
                }
                do {
                    mid++;
                    if (mid > right || bucket[mid].hashCode() != hash) {
                        return (-mid) - 1;
                    }
                    cmp = value.compareTo(bucket[mid]);
                    if (cmp == 0) {
                        return mid;
                    }
                } while (cmp >= 0);
                return (-mid) - 1;
            }
        }
        return (-left) - 1;
    }

    public String reuse(String value) {
        if (value == null) {
            return null;
        }
        int hash = value.hashCode();
        int bucketid = ((hash >>> 16) ^ hash) & this.mask;
        int length = this.lengths[bucketid];
        if (length == 0) {
            String v = compact(value);
            this.buckets[bucketid] = new String[4];
            this.buckets[bucketid][0] = v;
            this.lengths[bucketid] = 1;
            return v;
        }
        String[] bucket = this.buckets[bucketid];
        int ip = (-locate(hash, value, bucket, length)) - 1;
        if (ip < 0) {
            return bucket[(-ip) - 1];
        }
        if (length >= 64) {
            rehash();
            return reuse(value);
        }
        if (length == bucket.length) {
            bucket = (String[]) ArrayCopy.copyOf(bucket, length + 4);
            this.buckets[bucketid] = bucket;
        }
        System.arraycopy(bucket, ip, bucket, ip + 1, length - ip);
        String v2 = compact(value);
        bucket[ip] = v2;
        int[] iArr = this.lengths;
        iArr[bucketid] = iArr[bucketid] + 1;
        return v2;
    }

    private void rehash() {
        String[][] olddata = this.buckets;
        this.mask = ((this.mask + 1) << 2) - 1;
        this.buckets = new String[(this.mask + 1)][];
        this.lengths = new int[this.buckets.length];
        for (String[] ob : olddata) {
            if (ob != null) {
                for (String val : ob) {
                    if (val == null) {
                        break;
                    }
                    int hash = val.hashCode();
                    int bucketid = ((hash >>> 16) ^ hash) & this.mask;
                    int length = this.lengths[bucketid];
                    if (length == 0) {
                        this.buckets[bucketid] = new String[((ob.length + 4) / 4)];
                        this.buckets[bucketid][0] = val;
                    } else {
                        if (this.buckets[bucketid].length == length) {
                            this.buckets[bucketid] = (String[]) ArrayCopy.copyOf(this.buckets[bucketid], this.lengths[bucketid] + 4);
                        }
                        this.buckets[bucketid][length] = val;
                    }
                    int[] iArr = this.lengths;
                    iArr[bucketid] = iArr[bucketid] + 1;
                }
            }
        }
    }

    private static final String compact(String input) {
        return new String(input.toCharArray());
    }

    public int size() {
        int sum = 0;
        for (int l : this.lengths) {
            sum += l;
        }
        return sum;
    }
}
