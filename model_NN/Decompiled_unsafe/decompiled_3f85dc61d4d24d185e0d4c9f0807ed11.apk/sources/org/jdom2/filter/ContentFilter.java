package org.jdom2.filter;

import org.jdom2.CDATA;
import org.jdom2.Comment;
import org.jdom2.Content;
import org.jdom2.DocType;
import org.jdom2.Element;
import org.jdom2.EntityRef;
import org.jdom2.ProcessingInstruction;
import org.jdom2.Text;

public class ContentFilter extends AbstractFilter<Content> {
    public static final int CDATA = 2;
    public static final int COMMENT = 8;
    public static final int DOCTYPE = 128;
    public static final int DOCUMENT = 64;
    public static final int ELEMENT = 1;
    public static final int ENTITYREF = 32;
    public static final int PI = 16;
    public static final int TEXT = 4;
    private static final long serialVersionUID = 200;
    private int filterMask;

    public ContentFilter() {
        setDefaultMask();
    }

    public ContentFilter(boolean allVisible) {
        if (allVisible) {
            setDefaultMask();
        } else {
            this.filterMask &= this.filterMask ^ -1;
        }
    }

    public ContentFilter(int mask) {
        setFilterMask(mask);
    }

    public int getFilterMask() {
        return this.filterMask;
    }

    public void setFilterMask(int mask) {
        setDefaultMask();
        this.filterMask &= mask;
    }

    public void setDefaultMask() {
        this.filterMask = 255;
    }

    public void setDocumentContent() {
        this.filterMask = 153;
    }

    public void setElementContent() {
        this.filterMask = 63;
    }

    public void setElementVisible(boolean visible) {
        if (visible) {
            this.filterMask |= 1;
        } else {
            this.filterMask &= -2;
        }
    }

    public void setCDATAVisible(boolean visible) {
        if (visible) {
            this.filterMask |= 2;
        } else {
            this.filterMask &= -3;
        }
    }

    public void setTextVisible(boolean visible) {
        if (visible) {
            this.filterMask |= 4;
        } else {
            this.filterMask &= -5;
        }
    }

    public void setCommentVisible(boolean visible) {
        if (visible) {
            this.filterMask |= 8;
        } else {
            this.filterMask &= -9;
        }
    }

    public void setPIVisible(boolean visible) {
        if (visible) {
            this.filterMask |= 16;
        } else {
            this.filterMask &= -17;
        }
    }

    public void setEntityRefVisible(boolean visible) {
        if (visible) {
            this.filterMask |= 32;
        } else {
            this.filterMask &= -33;
        }
    }

    public void setDocTypeVisible(boolean visible) {
        if (visible) {
            this.filterMask |= 128;
        } else {
            this.filterMask &= -129;
        }
    }

    public Content filter(Object obj) {
        if (obj == null || !Content.class.isInstance(obj)) {
            return null;
        }
        Content content = (Content) obj;
        if (content instanceof Element) {
            if ((this.filterMask & 1) == 0) {
                return null;
            }
            return content;
        } else if (content instanceof CDATA) {
            if ((this.filterMask & 2) == 0) {
                return null;
            }
            return content;
        } else if (content instanceof Text) {
            if ((this.filterMask & 4) == 0) {
                return null;
            }
            return content;
        } else if (content instanceof Comment) {
            if ((this.filterMask & 8) == 0) {
                return null;
            }
            return content;
        } else if (content instanceof ProcessingInstruction) {
            if ((this.filterMask & 16) == 0) {
                return null;
            }
            return content;
        } else if (content instanceof EntityRef) {
            if ((this.filterMask & 32) == 0) {
                return null;
            }
            return content;
        } else if (!(content instanceof DocType)) {
            return null;
        } else {
            if ((this.filterMask & 128) == 0) {
                return null;
            }
            return content;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ContentFilter)) {
            return false;
        }
        if (this.filterMask != ((ContentFilter) obj).filterMask) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.filterMask;
    }
}
