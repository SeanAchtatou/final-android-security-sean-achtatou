package org.jdom2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.jdom2.Content;
import org.jdom2.output.XMLOutputter;

public class ProcessingInstruction extends Content {
    private static final long serialVersionUID = 200;
    protected transient Map<String, String> mapData;
    protected String rawData;
    protected String target;

    protected ProcessingInstruction() {
        super(Content.CType.ProcessingInstruction);
        this.mapData = null;
    }

    public ProcessingInstruction(String target2) {
        this(target2, "");
    }

    public ProcessingInstruction(String target2, Map<String, String> data) {
        super(Content.CType.ProcessingInstruction);
        this.mapData = null;
        setTarget(target2);
        setData(data);
    }

    public ProcessingInstruction(String target2, String data) {
        super(Content.CType.ProcessingInstruction);
        this.mapData = null;
        setTarget(target2);
        setData(data);
    }

    public ProcessingInstruction setTarget(String newTarget) {
        String reason = Verifier.checkProcessingInstructionTarget(newTarget);
        if (reason != null) {
            throw new IllegalTargetException(newTarget, reason);
        }
        this.target = newTarget;
        return this;
    }

    public String getValue() {
        return this.rawData;
    }

    public String getTarget() {
        return this.target;
    }

    public String getData() {
        return this.rawData;
    }

    public List<String> getPseudoAttributeNames() {
        return new ArrayList(this.mapData.keySet());
    }

    public ProcessingInstruction setData(String data) {
        String reason = Verifier.checkProcessingInstructionData(data);
        if (reason != null) {
            throw new IllegalDataException(data, reason);
        }
        this.rawData = data;
        this.mapData = parseData(data);
        return this;
    }

    public ProcessingInstruction setData(Map<String, String> data) {
        String temp = toString(data);
        String reason = Verifier.checkProcessingInstructionData(temp);
        if (reason != null) {
            throw new IllegalDataException(temp, reason);
        }
        this.rawData = temp;
        this.mapData = new LinkedHashMap(data);
        return this;
    }

    public String getPseudoAttributeValue(String name) {
        return this.mapData.get(name);
    }

    public ProcessingInstruction setPseudoAttribute(String name, String value) {
        String reason = Verifier.checkProcessingInstructionData(name);
        if (reason != null) {
            throw new IllegalDataException(name, reason);
        }
        String reason2 = Verifier.checkProcessingInstructionData(value);
        if (reason2 != null) {
            throw new IllegalDataException(value, reason2);
        }
        this.mapData.put(name, value);
        this.rawData = toString(this.mapData);
        return this;
    }

    public boolean removePseudoAttribute(String name) {
        if (this.mapData.remove(name) == null) {
            return false;
        }
        this.rawData = toString(this.mapData);
        return true;
    }

    private static final String toString(Map<String, String> pmapData) {
        StringBuilder stringData = new StringBuilder();
        for (Map.Entry<String, String> me2 : pmapData.entrySet()) {
            stringData.append((String) me2.getKey()).append("=\"").append((String) me2.getValue()).append("\" ");
        }
        if (stringData.length() > 0) {
            stringData.setLength(stringData.length() - 1);
        }
        return stringData.toString();
    }

    private Map<String, String> parseData(String prawData) {
        Map<String, String> data = new LinkedHashMap<>();
        String inputData = prawData.trim();
        while (!inputData.trim().equals("")) {
            String name = "";
            String value = "";
            int startName = 0;
            char previousChar = inputData.charAt(0);
            int pos = 1;
            while (true) {
                if (pos >= inputData.length()) {
                    break;
                }
                char currentChar = inputData.charAt(pos);
                if (currentChar == '=') {
                    name = inputData.substring(startName, pos).trim();
                    int[] bounds = extractQuotedString(inputData.substring(pos + 1));
                    if (bounds == null) {
                        return Collections.emptyMap();
                    }
                    value = inputData.substring(bounds[0] + pos + 1, bounds[1] + pos + 1);
                    pos += bounds[1] + 1;
                } else {
                    if (Character.isWhitespace(previousChar) && !Character.isWhitespace(currentChar)) {
                        startName = pos;
                    }
                    previousChar = currentChar;
                    pos++;
                }
            }
            inputData = inputData.substring(pos);
            if (name.length() > 0) {
                data.put(name, value);
            }
        }
        return data;
    }

    private static int[] extractQuotedString(String rawData2) {
        boolean inQuotes = false;
        char quoteChar = '\"';
        int start = 0;
        for (int pos = 0; pos < rawData2.length(); pos++) {
            char currentChar = rawData2.charAt(pos);
            if (currentChar == '\"' || currentChar == '\'') {
                if (!inQuotes) {
                    quoteChar = currentChar;
                    inQuotes = true;
                    start = pos + 1;
                } else if (quoteChar == currentChar) {
                    return new int[]{start, pos};
                }
            }
        }
        return null;
    }

    public String toString() {
        return "[ProcessingInstruction: " + new XMLOutputter().outputString(this) + "]";
    }

    public ProcessingInstruction clone() {
        ProcessingInstruction pi = (ProcessingInstruction) super.clone();
        pi.mapData = parseData(this.rawData);
        return pi;
    }

    public ProcessingInstruction detach() {
        return (ProcessingInstruction) super.detach();
    }

    /* access modifiers changed from: protected */
    public ProcessingInstruction setParent(Parent parent) {
        return (ProcessingInstruction) super.setParent(parent);
    }
}
