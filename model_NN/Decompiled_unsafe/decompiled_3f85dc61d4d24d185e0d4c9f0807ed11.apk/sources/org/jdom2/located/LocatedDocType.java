package org.jdom2.located;

import org.jdom2.DocType;

public class LocatedDocType extends DocType implements Located {
    private static final long serialVersionUID = 200;
    private int col;
    private int line;

    public LocatedDocType(String elementName, String publicID, String systemID) {
        super(elementName, publicID, systemID);
    }

    public LocatedDocType(String elementName, String systemID) {
        super(elementName, systemID);
    }

    public LocatedDocType(String elementName) {
        super(elementName);
    }

    public int getLine() {
        return this.line;
    }

    public int getColumn() {
        return this.col;
    }

    public void setLine(int line2) {
        this.line = line2;
    }

    public void setColumn(int col2) {
        this.col = col2;
    }
}
