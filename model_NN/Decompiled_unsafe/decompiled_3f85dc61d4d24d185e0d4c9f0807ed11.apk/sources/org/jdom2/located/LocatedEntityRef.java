package org.jdom2.located;

import org.jdom2.EntityRef;

public class LocatedEntityRef extends EntityRef implements Located {
    private static final long serialVersionUID = 200;
    private int col;
    private int line;

    public LocatedEntityRef(String name) {
        super(name);
    }

    public LocatedEntityRef(String name, String systemID) {
        super(name, systemID);
    }

    public LocatedEntityRef(String name, String publicID, String systemID) {
        super(name, publicID, systemID);
    }

    public int getLine() {
        return this.line;
    }

    public int getColumn() {
        return this.col;
    }

    public void setLine(int line2) {
        this.line = line2;
    }

    public void setColumn(int col2) {
        this.col = col2;
    }
}
