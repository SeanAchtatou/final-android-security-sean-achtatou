package org.jdom2.located;

import org.jdom2.Comment;

public class LocatedComment extends Comment implements Located {
    private static final long serialVersionUID = 200;
    private int col;
    private int line;

    public LocatedComment(String text) {
        super(text);
    }

    public int getLine() {
        return this.line;
    }

    public int getColumn() {
        return this.col;
    }

    public void setLine(int line2) {
        this.line = line2;
    }

    public void setColumn(int col2) {
        this.col = col2;
    }
}
