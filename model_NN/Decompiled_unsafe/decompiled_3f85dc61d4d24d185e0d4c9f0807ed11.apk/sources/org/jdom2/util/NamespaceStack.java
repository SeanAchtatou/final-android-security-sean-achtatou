package org.jdom2.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.jdom2.Attribute;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.internal.ArrayCopy;

public final class NamespaceStack implements Iterable<Namespace> {
    private static final Namespace[] DEFAULTSEED = {Namespace.NO_NAMESPACE, Namespace.XML_NAMESPACE};
    private static final Namespace[] EMPTY = new Namespace[0];
    private static final Iterable<Namespace> EMPTYITER = new EmptyIterable();
    private static final Comparator<Namespace> NSCOMP = new Comparator<Namespace>() {
        public int compare(Namespace ns1, Namespace ns2) {
            return ns1.getPrefix().compareTo(ns2.getPrefix());
        }
    };
    private Namespace[][] added;
    private int depth;
    private Namespace[][] scope;

    private static final class ForwardWalker implements Iterator<Namespace> {
        int cursor = 0;
        private final Namespace[] namespaces;

        public ForwardWalker(Namespace[] namespaces2) {
            this.namespaces = namespaces2;
        }

        public boolean hasNext() {
            return this.cursor < this.namespaces.length;
        }

        public Namespace next() {
            if (this.cursor >= this.namespaces.length) {
                throw new NoSuchElementException("Cannot over-iterate...");
            }
            Namespace[] namespaceArr = this.namespaces;
            int i = this.cursor;
            this.cursor = i + 1;
            return namespaceArr[i];
        }

        public void remove() {
            throw new UnsupportedOperationException("Cannot remove Namespaces from iterator");
        }
    }

    private static final class BackwardWalker implements Iterator<Namespace> {
        int cursor = -1;
        private final Namespace[] namespaces;

        public BackwardWalker(Namespace[] namespaces2) {
            this.namespaces = namespaces2;
            this.cursor = namespaces2.length - 1;
        }

        public boolean hasNext() {
            return this.cursor >= 0;
        }

        public Namespace next() {
            if (this.cursor < 0) {
                throw new NoSuchElementException("Cannot over-iterate...");
            }
            Namespace[] namespaceArr = this.namespaces;
            int i = this.cursor;
            this.cursor = i - 1;
            return namespaceArr[i];
        }

        public void remove() {
            throw new UnsupportedOperationException("Cannot remove Namespaces from iterator");
        }
    }

    private static final class NamespaceIterable implements Iterable<Namespace> {
        private final boolean forward;
        private final Namespace[] namespaces;

        public NamespaceIterable(Namespace[] data, boolean forward2) {
            this.forward = forward2;
            this.namespaces = data;
        }

        public Iterator<Namespace> iterator() {
            return this.forward ? new ForwardWalker(this.namespaces) : new BackwardWalker(this.namespaces);
        }
    }

    private static final class EmptyIterable implements Iterable<Namespace>, Iterator<Namespace> {
        private EmptyIterable() {
        }

        public Iterator<Namespace> iterator() {
            return this;
        }

        public boolean hasNext() {
            return false;
        }

        public Namespace next() {
            throw new NoSuchElementException("Can not call next() on an empty Iterator.");
        }

        public void remove() {
            throw new UnsupportedOperationException("Cannot remove Namespaces from iterator");
        }
    }

    private static final int binarySearch(Namespace[] data, int left, int right, Namespace key) {
        int right2 = right - 1;
        while (left <= right2) {
            int mid = (left + right2) >>> 1;
            if (data[mid] == key) {
                return mid;
            }
            int cmp = NSCOMP.compare(data[mid], key);
            if (cmp < 0) {
                left = mid + 1;
            } else if (cmp <= 0) {
                return mid;
            } else {
                right2 = mid - 1;
            }
        }
        return (-left) - 1;
    }

    public NamespaceStack() {
        this(DEFAULTSEED);
    }

    public NamespaceStack(Namespace[] seed) {
        this.added = new Namespace[10][];
        this.scope = new Namespace[10][];
        this.depth = -1;
        this.depth++;
        this.added[this.depth] = seed;
        this.scope[this.depth] = this.added[this.depth];
    }

    private static final Namespace[] checkNamespace(List<Namespace> store, Namespace namespace, Namespace[] scope2) {
        if (namespace == scope2[0]) {
            return scope2;
        }
        if (namespace.getPrefix().equals(scope2[0].getPrefix())) {
            store.add(namespace);
            Namespace[] nscope = (Namespace[]) ArrayCopy.copyOf(scope2, scope2.length);
            nscope[0] = namespace;
            return nscope;
        }
        int ip = binarySearch(scope2, 1, scope2.length, namespace);
        if (ip >= 0 && namespace == scope2[ip]) {
            return scope2;
        }
        store.add(namespace);
        if (ip >= 0) {
            Namespace[] nscope2 = (Namespace[]) ArrayCopy.copyOf(scope2, scope2.length);
            nscope2[ip] = namespace;
            return nscope2;
        }
        Namespace[] nscope3 = (Namespace[]) ArrayCopy.copyOf(scope2, scope2.length + 1);
        int ip2 = (-ip) - 1;
        System.arraycopy(nscope3, ip2, nscope3, ip2 + 1, (nscope3.length - ip2) - 1);
        nscope3[ip2] = namespace;
        return nscope3;
    }

    public void push(Element element) {
        List<Namespace> toadd = new ArrayList<>(8);
        Namespace mns = element.getNamespace();
        Namespace[] newscope = checkNamespace(toadd, mns, this.scope[this.depth]);
        if (element.hasAdditionalNamespaces()) {
            for (Namespace ns : element.getAdditionalNamespaces()) {
                if (ns != mns) {
                    newscope = checkNamespace(toadd, ns, newscope);
                }
            }
        }
        if (element.hasAttributes()) {
            for (Attribute a : element.getAttributes()) {
                Namespace ns2 = a.getNamespace();
                if (!(ns2 == Namespace.NO_NAMESPACE || ns2 == mns)) {
                    newscope = checkNamespace(toadd, ns2, newscope);
                }
            }
        }
        pushStack(mns, newscope, toadd);
    }

    public void push(Attribute att) {
        List<Namespace> toadd = new ArrayList<>(1);
        Namespace mns = att.getNamespace();
        pushStack(mns, checkNamespace(toadd, mns, this.scope[this.depth]), toadd);
    }

    private final void pushStack(Namespace mns, Namespace[] newscope, List<Namespace> toadd) {
        this.depth++;
        if (this.depth >= this.scope.length) {
            this.scope = (Namespace[][]) ArrayCopy.copyOf(this.scope, this.scope.length * 2);
            this.added = (Namespace[][]) ArrayCopy.copyOf(this.added, this.scope.length);
        }
        if (toadd.isEmpty()) {
            this.added[this.depth] = EMPTY;
        } else {
            this.added[this.depth] = (Namespace[]) toadd.toArray(new Namespace[toadd.size()]);
            if (this.added[this.depth][0] == mns) {
                Arrays.sort(this.added[this.depth], 1, this.added[this.depth].length, NSCOMP);
            } else {
                Arrays.sort(this.added[this.depth], NSCOMP);
            }
        }
        if (mns != newscope[0]) {
            if (toadd.isEmpty()) {
                newscope = (Namespace[]) ArrayCopy.copyOf(newscope, newscope.length);
            }
            Namespace tmp = newscope[0];
            int ip = ((-binarySearch(newscope, 1, newscope.length, tmp)) - 1) - 1;
            System.arraycopy(newscope, 1, newscope, 0, ip);
            newscope[ip] = tmp;
            System.arraycopy(newscope, 0, newscope, 1, binarySearch(newscope, 0, newscope.length, mns));
            newscope[0] = mns;
        }
        this.scope[this.depth] = newscope;
    }

    public void pop() {
        if (this.depth <= 0) {
            throw new IllegalStateException("Cannot over-pop the stack.");
        }
        this.scope[this.depth] = null;
        this.added[this.depth] = null;
        this.depth--;
    }

    public Iterable<Namespace> addedForward() {
        if (this.added[this.depth].length == 0) {
            return EMPTYITER;
        }
        return new NamespaceIterable(this.added[this.depth], true);
    }

    public Iterable<Namespace> addedReverse() {
        if (this.added[this.depth].length == 0) {
            return EMPTYITER;
        }
        return new NamespaceIterable(this.added[this.depth], false);
    }

    public Iterator<Namespace> iterator() {
        return new ForwardWalker(this.scope[this.depth]);
    }

    public Namespace[] getScope() {
        return (Namespace[]) ArrayCopy.copyOf(this.scope[this.depth], this.scope[this.depth].length);
    }

    public boolean isInScope(Namespace ns) {
        if (ns == this.scope[this.depth][0]) {
            return true;
        }
        int ip = binarySearch(this.scope[this.depth], 1, this.scope[this.depth].length, ns);
        if (ip < 0) {
            return false;
        }
        if (ns != this.scope[this.depth][ip]) {
            return false;
        }
        return true;
    }
}
