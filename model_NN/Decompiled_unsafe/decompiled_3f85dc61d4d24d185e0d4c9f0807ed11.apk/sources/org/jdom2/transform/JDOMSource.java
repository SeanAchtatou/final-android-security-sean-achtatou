package org.jdom2.transform;

import java.io.InputStream;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.xml.transform.sax.SAXSource;
import org.jdom2.Content;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.output.SAXOutputter;
import org.jdom2.output.XMLOutputter;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotSupportedException;
import org.xml.sax.XMLFilter;
import org.xml.sax.XMLReader;

public class JDOMSource extends SAXSource {
    public static final String JDOM_FEATURE = "http://jdom.org/jdom2/transform/JDOMSource/feature";
    private EntityResolver resolver;
    private XMLReader xmlReader;

    public JDOMSource(Document source) {
        this(source, null);
    }

    public JDOMSource(List<? extends Content> source) {
        this.xmlReader = null;
        this.resolver = null;
        setNodes(source);
    }

    public JDOMSource(Element source) {
        this.xmlReader = null;
        this.resolver = null;
        List<Content> nodes = new ArrayList<>();
        nodes.add(source);
        setNodes(nodes);
    }

    public JDOMSource(Document source, EntityResolver resolver2) {
        this.xmlReader = null;
        this.resolver = null;
        setDocument(source);
        this.resolver = resolver2;
        if (source != null && source.getBaseURI() != null) {
            super.setSystemId(source.getBaseURI());
        }
    }

    public void setDocument(Document source) {
        super.setInputSource(new JDOMInputSource(source));
    }

    public Document getDocument() {
        Object src = ((JDOMInputSource) getInputSource()).getSource();
        if (src instanceof Document) {
            return (Document) src;
        }
        return null;
    }

    public void setNodes(List<? extends Content> source) {
        super.setInputSource(new JDOMInputSource(source));
    }

    public List<? extends Content> getNodes() {
        return ((JDOMInputSource) getInputSource()).getListSource();
    }

    public void setInputSource(InputSource inputSource) throws UnsupportedOperationException {
        throw new UnsupportedOperationException();
    }

    public void setXMLReader(XMLReader reader) throws UnsupportedOperationException {
        if (reader instanceof XMLFilter) {
            XMLFilter filter = reader;
            while (true) {
                XMLFilter filter2 = (XMLFilter) filter;
                if (filter2.getParent() instanceof XMLFilter) {
                    filter = (XMLFilter) filter2.getParent();
                } else {
                    filter2.setParent(buildDocumentReader());
                    this.xmlReader = reader;
                    return;
                }
            }
        } else {
            throw new UnsupportedOperationException();
        }
    }

    public XMLReader getXMLReader() {
        if (this.xmlReader == null) {
            this.xmlReader = buildDocumentReader();
        }
        return this.xmlReader;
    }

    private XMLReader buildDocumentReader() {
        DocumentReader reader = new DocumentReader();
        if (this.resolver != null) {
            reader.setEntityResolver(this.resolver);
        }
        return reader;
    }

    private static class JDOMInputSource extends InputSource {
        private Document docsource = null;
        private List<? extends Content> listsource = null;

        public JDOMInputSource(Document document) {
            this.docsource = document;
        }

        public JDOMInputSource(List<? extends Content> nodes) {
            this.listsource = nodes;
        }

        public Object getSource() {
            return this.docsource == null ? this.listsource : this.docsource;
        }

        public void setCharacterStream(Reader characterStream) throws UnsupportedOperationException {
            throw new UnsupportedOperationException();
        }

        public Reader getCharacterStream() {
            if (this.docsource != null) {
                return new StringReader(new XMLOutputter().outputString(this.docsource));
            }
            if (this.listsource != null) {
                return new StringReader(new XMLOutputter().outputString(this.listsource));
            }
            return null;
        }

        public void setByteStream(InputStream byteStream) throws UnsupportedOperationException {
            throw new UnsupportedOperationException();
        }

        public Document getDocumentSource() {
            return this.docsource;
        }

        public List<? extends Content> getListSource() {
            return this.listsource;
        }
    }

    private static class DocumentReader extends SAXOutputter implements XMLReader {
        public void parse(String systemId) throws SAXNotSupportedException {
            throw new SAXNotSupportedException("Only JDOM Documents are supported as input");
        }

        public void parse(InputSource input) throws SAXException {
            if (input instanceof JDOMInputSource) {
                try {
                    Document docsource = ((JDOMInputSource) input).getDocumentSource();
                    if (docsource != null) {
                        output(docsource);
                    } else {
                        output(((JDOMInputSource) input).getListSource());
                    }
                } catch (JDOMException e) {
                    throw new SAXException(e.getMessage(), e);
                }
            } else {
                throw new SAXNotSupportedException("Only JDOM Documents are supported as input");
            }
        }
    }
}
